﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Colors_Adjust_FullColors
struct  CameraFilterPack_Colors_Adjust_FullColors_t1115807031  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Colors_Adjust_FullColors::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Colors_Adjust_FullColors::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Colors_Adjust_FullColors::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_R
	float ___Red_R_6;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_G
	float ___Red_G_7;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_B
	float ___Red_B_8;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_Constant
	float ___Red_Constant_9;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_R
	float ___Green_R_10;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_G
	float ___Green_G_11;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_B
	float ___Green_B_12;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_Constant
	float ___Green_Constant_13;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_R
	float ___Blue_R_14;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_G
	float ___Blue_G_15;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_B
	float ___Blue_B_16;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_Constant
	float ___Blue_Constant_17;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Red_R_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Red_R_6)); }
	inline float get_Red_R_6() const { return ___Red_R_6; }
	inline float* get_address_of_Red_R_6() { return &___Red_R_6; }
	inline void set_Red_R_6(float value)
	{
		___Red_R_6 = value;
	}

	inline static int32_t get_offset_of_Red_G_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Red_G_7)); }
	inline float get_Red_G_7() const { return ___Red_G_7; }
	inline float* get_address_of_Red_G_7() { return &___Red_G_7; }
	inline void set_Red_G_7(float value)
	{
		___Red_G_7 = value;
	}

	inline static int32_t get_offset_of_Red_B_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Red_B_8)); }
	inline float get_Red_B_8() const { return ___Red_B_8; }
	inline float* get_address_of_Red_B_8() { return &___Red_B_8; }
	inline void set_Red_B_8(float value)
	{
		___Red_B_8 = value;
	}

	inline static int32_t get_offset_of_Red_Constant_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Red_Constant_9)); }
	inline float get_Red_Constant_9() const { return ___Red_Constant_9; }
	inline float* get_address_of_Red_Constant_9() { return &___Red_Constant_9; }
	inline void set_Red_Constant_9(float value)
	{
		___Red_Constant_9 = value;
	}

	inline static int32_t get_offset_of_Green_R_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Green_R_10)); }
	inline float get_Green_R_10() const { return ___Green_R_10; }
	inline float* get_address_of_Green_R_10() { return &___Green_R_10; }
	inline void set_Green_R_10(float value)
	{
		___Green_R_10 = value;
	}

	inline static int32_t get_offset_of_Green_G_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Green_G_11)); }
	inline float get_Green_G_11() const { return ___Green_G_11; }
	inline float* get_address_of_Green_G_11() { return &___Green_G_11; }
	inline void set_Green_G_11(float value)
	{
		___Green_G_11 = value;
	}

	inline static int32_t get_offset_of_Green_B_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Green_B_12)); }
	inline float get_Green_B_12() const { return ___Green_B_12; }
	inline float* get_address_of_Green_B_12() { return &___Green_B_12; }
	inline void set_Green_B_12(float value)
	{
		___Green_B_12 = value;
	}

	inline static int32_t get_offset_of_Green_Constant_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Green_Constant_13)); }
	inline float get_Green_Constant_13() const { return ___Green_Constant_13; }
	inline float* get_address_of_Green_Constant_13() { return &___Green_Constant_13; }
	inline void set_Green_Constant_13(float value)
	{
		___Green_Constant_13 = value;
	}

	inline static int32_t get_offset_of_Blue_R_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Blue_R_14)); }
	inline float get_Blue_R_14() const { return ___Blue_R_14; }
	inline float* get_address_of_Blue_R_14() { return &___Blue_R_14; }
	inline void set_Blue_R_14(float value)
	{
		___Blue_R_14 = value;
	}

	inline static int32_t get_offset_of_Blue_G_15() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Blue_G_15)); }
	inline float get_Blue_G_15() const { return ___Blue_G_15; }
	inline float* get_address_of_Blue_G_15() { return &___Blue_G_15; }
	inline void set_Blue_G_15(float value)
	{
		___Blue_G_15 = value;
	}

	inline static int32_t get_offset_of_Blue_B_16() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Blue_B_16)); }
	inline float get_Blue_B_16() const { return ___Blue_B_16; }
	inline float* get_address_of_Blue_B_16() { return &___Blue_B_16; }
	inline void set_Blue_B_16(float value)
	{
		___Blue_B_16 = value;
	}

	inline static int32_t get_offset_of_Blue_Constant_17() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_FullColors_t1115807031, ___Blue_Constant_17)); }
	inline float get_Blue_Constant_17() const { return ___Blue_Constant_17; }
	inline float* get_address_of_Blue_Constant_17() { return &___Blue_Constant_17; }
	inline void set_Blue_Constant_17(float value)
	{
		___Blue_Constant_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
