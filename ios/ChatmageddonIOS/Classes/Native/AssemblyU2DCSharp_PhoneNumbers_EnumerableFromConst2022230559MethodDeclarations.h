﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.EnumerableFromConstructor`1<System.Object>
struct EnumerableFromConstructor_1_t2022230559;
// System.Func`1<System.Collections.Generic.IEnumerator`1<System.Object>>
struct Func_1_t2119365804;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.EnumerableFromConstructor`1<System.Object>::.ctor(System.Func`1<System.Collections.Generic.IEnumerator`1<T>>)
extern "C"  void EnumerableFromConstructor_1__ctor_m2151719767_gshared (EnumerableFromConstructor_1_t2022230559 * __this, Func_1_t2119365804 * ___fn0, const MethodInfo* method);
#define EnumerableFromConstructor_1__ctor_m2151719767(__this, ___fn0, method) ((  void (*) (EnumerableFromConstructor_1_t2022230559 *, Func_1_t2119365804 *, const MethodInfo*))EnumerableFromConstructor_1__ctor_m2151719767_gshared)(__this, ___fn0, method)
// System.Collections.Generic.IEnumerator`1<T> PhoneNumbers.EnumerableFromConstructor`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* EnumerableFromConstructor_1_GetEnumerator_m1562695206_gshared (EnumerableFromConstructor_1_t2022230559 * __this, const MethodInfo* method);
#define EnumerableFromConstructor_1_GetEnumerator_m1562695206(__this, method) ((  Il2CppObject* (*) (EnumerableFromConstructor_1_t2022230559 *, const MethodInfo*))EnumerableFromConstructor_1_GetEnumerator_m1562695206_gshared)(__this, method)
// System.Collections.IEnumerator PhoneNumbers.EnumerableFromConstructor`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * EnumerableFromConstructor_1_System_Collections_IEnumerable_GetEnumerator_m1507026405_gshared (EnumerableFromConstructor_1_t2022230559 * __this, const MethodInfo* method);
#define EnumerableFromConstructor_1_System_Collections_IEnumerable_GetEnumerator_m1507026405(__this, method) ((  Il2CppObject * (*) (EnumerableFromConstructor_1_t2022230559 *, const MethodInfo*))EnumerableFromConstructor_1_System_Collections_IEnumerable_GetEnumerator_m1507026405_gshared)(__this, method)
