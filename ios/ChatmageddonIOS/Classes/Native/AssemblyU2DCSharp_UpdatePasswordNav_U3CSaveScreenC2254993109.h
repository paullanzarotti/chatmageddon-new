﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// UpdatePasswordNav
struct UpdatePasswordNav_t2102371893;
// System.Object
struct Il2CppObject;
// UpdatePasswordNav/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1
struct U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdatePasswordNav/<SaveScreenContent>c__Iterator0
struct  U3CSaveScreenContentU3Ec__Iterator0_t2254993109  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> UpdatePasswordNav/<SaveScreenContent>c__Iterator0::savedAction
	Action_1_t3627374100 * ___savedAction_0;
	// UpdatePasswordNav UpdatePasswordNav/<SaveScreenContent>c__Iterator0::$this
	UpdatePasswordNav_t2102371893 * ___U24this_1;
	// System.Object UpdatePasswordNav/<SaveScreenContent>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean UpdatePasswordNav/<SaveScreenContent>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UpdatePasswordNav/<SaveScreenContent>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// UpdatePasswordNav/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1 UpdatePasswordNav/<SaveScreenContent>c__Iterator0::$locvar0
	U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * ___U24locvar0_5;

public:
	inline static int32_t get_offset_of_savedAction_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t2254993109, ___savedAction_0)); }
	inline Action_1_t3627374100 * get_savedAction_0() const { return ___savedAction_0; }
	inline Action_1_t3627374100 ** get_address_of_savedAction_0() { return &___savedAction_0; }
	inline void set_savedAction_0(Action_1_t3627374100 * value)
	{
		___savedAction_0 = value;
		Il2CppCodeGenWriteBarrier(&___savedAction_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t2254993109, ___U24this_1)); }
	inline UpdatePasswordNav_t2102371893 * get_U24this_1() const { return ___U24this_1; }
	inline UpdatePasswordNav_t2102371893 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UpdatePasswordNav_t2102371893 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t2254993109, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t2254993109, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t2254993109, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t2254993109, ___U24locvar0_5)); }
	inline U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * get_U24locvar0_5() const { return ___U24locvar0_5; }
	inline U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 ** get_address_of_U24locvar0_5() { return &___U24locvar0_5; }
	inline void set_U24locvar0_5(U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * value)
	{
		___U24locvar0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
