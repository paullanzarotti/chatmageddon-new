﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemsManager
struct ItemsManager_t2342504123;
// Missile
struct Missile_t813944928;
// System.String
struct String_t;
// Shield
struct Shield_t3327121081;
// Mine
struct Mine_t2729441277;
// Fuel
struct Fuel_t1015546524;
// PurchaseableItem
struct PurchaseableItem_t3351122996;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PurchaseableItem3351122996.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"

// System.Void ItemsManager::.ctor()
extern "C"  void ItemsManager__ctor_m1935362266 (ItemsManager_t2342504123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemsManager::Init()
extern "C"  void ItemsManager_Init_m1124782466 (ItemsManager_t2342504123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemsManager::ClearItemsData()
extern "C"  void ItemsManager_ClearItemsData_m1119946677 (ItemsManager_t2342504123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Missile ItemsManager::GetMissile(System.String)
extern "C"  Missile_t813944928 * ItemsManager_GetMissile_m2560672341 (ItemsManager_t2342504123 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Shield ItemsManager::GetShield(System.String)
extern "C"  Shield_t3327121081 * ItemsManager_GetShield_m3643334613 (ItemsManager_t2342504123 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mine ItemsManager::GetMine(System.String)
extern "C"  Mine_t2729441277 * ItemsManager_GetMine_m2206333013 (ItemsManager_t2342504123 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Fuel ItemsManager::GetFuel(System.String)
extern "C"  Fuel_t1015546524 * ItemsManager_GetFuel_m3605383413 (ItemsManager_t2342504123 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemsManager::AddPurchaseableItem(PurchaseableItem)
extern "C"  void ItemsManager_AddPurchaseableItem_m355111551 (ItemsManager_t2342504123 * __this, PurchaseableItem_t3351122996 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ItemsManager::GetDescriptionTitle(PurchaseableCategory,System.Int32)
extern "C"  String_t* ItemsManager_GetDescriptionTitle_m3295294715 (ItemsManager_t2342504123 * __this, int32_t ___item0, int32_t ___section1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ItemsManager::GetDescription(PurchaseableCategory,System.String,System.Int32)
extern "C"  String_t* ItemsManager_GetDescription_m4289109081 (ItemsManager_t2342504123 * __this, int32_t ___item0, String_t* ___internalName1, int32_t ___section2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
