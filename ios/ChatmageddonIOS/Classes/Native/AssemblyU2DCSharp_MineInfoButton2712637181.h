﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineModal
struct MineModal_t1141032720;

#include "AssemblyU2DCSharp_RotateButton2565449149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineInfoButton
struct  MineInfoButton_t2712637181  : public RotateButton_t2565449149
{
public:
	// MineModal MineInfoButton::modal
	MineModal_t1141032720 * ___modal_10;

public:
	inline static int32_t get_offset_of_modal_10() { return static_cast<int32_t>(offsetof(MineInfoButton_t2712637181, ___modal_10)); }
	inline MineModal_t1141032720 * get_modal_10() const { return ___modal_10; }
	inline MineModal_t1141032720 ** get_address_of_modal_10() { return &___modal_10; }
	inline void set_modal_10(MineModal_t1141032720 * value)
	{
		___modal_10 = value;
		Il2CppCodeGenWriteBarrier(&___modal_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
