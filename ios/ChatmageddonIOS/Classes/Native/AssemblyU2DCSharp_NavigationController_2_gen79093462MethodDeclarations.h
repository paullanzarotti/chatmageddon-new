﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3781907348MethodDeclarations.h"

// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::.ctor()
#define NavigationController_2__ctor_m1505926908(__this, method) ((  void (*) (NavigationController_2_t79093462 *, const MethodInfo*))NavigationController_2__ctor_m1781176995_gshared)(__this, method)
// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m307245442(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t79093462 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m3816322973_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m2062233525(__this, method) ((  void (*) (NavigationController_2_t79093462 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m949364036_gshared)(__this, method)
// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m4285360236(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t79093462 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m2240014625_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m3979817392(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t79093462 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m1138610217_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m64926539(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t79093462 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m928326242_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m2118091142(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t79093462 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3218964345_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m203666872(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t79093462 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m1612430877_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<FriendHomeNavigationController,FriendHomeNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m28775056(__this, ___screen0, method) ((  void (*) (NavigationController_2_t79093462 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2788814773_gshared)(__this, ___screen0, method)
