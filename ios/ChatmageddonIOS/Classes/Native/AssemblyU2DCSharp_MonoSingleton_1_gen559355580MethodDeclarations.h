﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ChatmageddonGamestate>::.ctor()
#define MonoSingleton_1__ctor_m2408442014(__this, method) ((  void (*) (MonoSingleton_1_t559355580 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonGamestate>::Awake()
#define MonoSingleton_1_Awake_m1781978657(__this, method) ((  void (*) (MonoSingleton_1_t559355580 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ChatmageddonGamestate>::get_Instance()
#define MonoSingleton_1_get_Instance_m3268274223(__this /* static, unused */, method) ((  ChatmageddonGamestate_t808689860 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ChatmageddonGamestate>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m216729639(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ChatmageddonGamestate>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1566416952(__this, method) ((  void (*) (MonoSingleton_1_t559355580 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonGamestate>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m4055514116(__this, method) ((  void (*) (MonoSingleton_1_t559355580 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonGamestate>::.cctor()
#define MonoSingleton_1__cctor_m3402272911(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
