﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1<System.Object>
struct ChatServer_1_t3248875550;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatServer`1<System.Object>::.ctor()
extern "C"  void ChatServer_1__ctor_m1982702137_gshared (ChatServer_1_t3248875550 * __this, const MethodInfo* method);
#define ChatServer_1__ctor_m1982702137(__this, method) ((  void (*) (ChatServer_1_t3248875550 *, const MethodInfo*))ChatServer_1__ctor_m1982702137_gshared)(__this, method)
// System.Void ChatServer`1<System.Object>::OpenNewThread(System.Collections.Generic.List`1<System.String>,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_OpenNewThread_m907651558_gshared (ChatServer_1_t3248875550 * __this, List_1_t1398341365 * ___userIDList0, String_t* ___message1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method);
#define ChatServer_1_OpenNewThread_m907651558(__this, ___userIDList0, ___message1, ___callBack2, method) ((  void (*) (ChatServer_1_t3248875550 *, List_1_t1398341365 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_OpenNewThread_m907651558_gshared)(__this, ___userIDList0, ___message1, ___callBack2, method)
// System.Void ChatServer`1<System.Object>::RetrieveOpenThreads(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_RetrieveOpenThreads_m637939567_gshared (ChatServer_1_t3248875550 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define ChatServer_1_RetrieveOpenThreads_m637939567(__this, ___callBack0, method) ((  void (*) (ChatServer_1_t3248875550 *, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_RetrieveOpenThreads_m637939567_gshared)(__this, ___callBack0, method)
// System.Void ChatServer`1<System.Object>::RetrieveOpenThreadInfo(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_RetrieveOpenThreadInfo_m1820464480_gshared (ChatServer_1_t3248875550 * __this, String_t* ___threadID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define ChatServer_1_RetrieveOpenThreadInfo_m1820464480(__this, ___threadID0, ___callBack1, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_RetrieveOpenThreadInfo_m1820464480_gshared)(__this, ___threadID0, ___callBack1, method)
// System.Void ChatServer`1<System.Object>::RetrieveThreadMessages(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>,System.String)
extern "C"  void ChatServer_1_RetrieveThreadMessages_m256300356_gshared (ChatServer_1_t3248875550 * __this, String_t* ___threadID0, Action_3_t3681841185 * ___callBack1, String_t* ___timestamp2, const MethodInfo* method);
#define ChatServer_1_RetrieveThreadMessages_m256300356(__this, ___threadID0, ___callBack1, ___timestamp2, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, Action_3_t3681841185 *, String_t*, const MethodInfo*))ChatServer_1_RetrieveThreadMessages_m256300356_gshared)(__this, ___threadID0, ___callBack1, ___timestamp2, method)
// System.Void ChatServer`1<System.Object>::RetrieveThreadMessagesForUser(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>,System.String)
extern "C"  void ChatServer_1_RetrieveThreadMessagesForUser_m2574775060_gshared (ChatServer_1_t3248875550 * __this, String_t* ___userID0, Action_3_t3681841185 * ___callBack1, String_t* ___timestamp2, const MethodInfo* method);
#define ChatServer_1_RetrieveThreadMessagesForUser_m2574775060(__this, ___userID0, ___callBack1, ___timestamp2, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, Action_3_t3681841185 *, String_t*, const MethodInfo*))ChatServer_1_RetrieveThreadMessagesForUser_m2574775060_gshared)(__this, ___userID0, ___callBack1, ___timestamp2, method)
// System.Void ChatServer`1<System.Object>::AddUserToThread(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_AddUserToThread_m1040780865_gshared (ChatServer_1_t3248875550 * __this, String_t* ___threadID0, String_t* ___userID1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method);
#define ChatServer_1_AddUserToThread_m1040780865(__this, ___threadID0, ___userID1, ___callBack2, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_AddUserToThread_m1040780865_gshared)(__this, ___threadID0, ___userID1, ___callBack2, method)
// System.Void ChatServer`1<System.Object>::PostMessageToThread(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_PostMessageToThread_m1683284752_gshared (ChatServer_1_t3248875550 * __this, String_t* ___threadID0, String_t* ___message1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method);
#define ChatServer_1_PostMessageToThread_m1683284752(__this, ___threadID0, ___message1, ___callBack2, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_PostMessageToThread_m1683284752_gshared)(__this, ___threadID0, ___message1, ___callBack2, method)
// System.Void ChatServer`1<System.Object>::UpdateThreadRead(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_UpdateThreadRead_m3951537979_gshared (ChatServer_1_t3248875550 * __this, String_t* ___threadID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define ChatServer_1_UpdateThreadRead_m3951537979(__this, ___threadID0, ___callBack1, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_UpdateThreadRead_m3951537979_gshared)(__this, ___threadID0, ___callBack1, method)
// System.Void ChatServer`1<System.Object>::DeleteThread(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatServer_1_DeleteThread_m602312621_gshared (ChatServer_1_t3248875550 * __this, String_t* ___threadID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define ChatServer_1_DeleteThread_m602312621(__this, ___threadID0, ___callBack1, method) ((  void (*) (ChatServer_1_t3248875550 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_DeleteThread_m602312621_gshared)(__this, ___threadID0, ___callBack1, method)
