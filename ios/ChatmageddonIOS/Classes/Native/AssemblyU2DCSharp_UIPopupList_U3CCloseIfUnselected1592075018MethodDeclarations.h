﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPopupList/<CloseIfUnselected>c__Iterator1
struct U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPopupList/<CloseIfUnselected>c__Iterator1::.ctor()
extern "C"  void U3CCloseIfUnselectedU3Ec__Iterator1__ctor_m749761889 (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList/<CloseIfUnselected>c__Iterator1::MoveNext()
extern "C"  bool U3CCloseIfUnselectedU3Ec__Iterator1_MoveNext_m303847919 (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<CloseIfUnselected>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCloseIfUnselectedU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3484743259 (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<CloseIfUnselected>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCloseIfUnselectedU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1507145011 (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<CloseIfUnselected>c__Iterator1::Dispose()
extern "C"  void U3CCloseIfUnselectedU3Ec__Iterator1_Dispose_m565634564 (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<CloseIfUnselected>c__Iterator1::Reset()
extern "C"  void U3CCloseIfUnselectedU3Ec__Iterator1_Reset_m1468931862 (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
