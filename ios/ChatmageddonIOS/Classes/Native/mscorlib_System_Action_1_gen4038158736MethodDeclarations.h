﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen4135621323MethodDeclarations.h"

// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m4074360668(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t4038158736 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m684242267_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::Invoke(T)
#define Action_1_Invoke_m3154492912(__this, ___obj0, method) ((  void (*) (Action_1_t4038158736 *, KeyValuePair_2_t4236359354 , const MethodInfo*))Action_1_Invoke_m93096007_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m3989950769(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t4038158736 *, KeyValuePair_2_t4236359354 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m2506333398_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m222933294(__this, ___result0, method) ((  void (*) (Action_1_t4038158736 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m596970257_gshared)(__this, ___result0, method)
