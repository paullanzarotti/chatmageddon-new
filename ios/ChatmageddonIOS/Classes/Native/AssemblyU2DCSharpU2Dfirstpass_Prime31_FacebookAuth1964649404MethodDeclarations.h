﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookAuthHelper
struct FacebookAuthHelper_t1964649404;
// System.Action
struct Action_t3226471752;
// Prime31.P31Error
struct P31Error_t2856600608;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "P31RestKit_Prime31_P31Error2856600608.h"

// System.Void Prime31.FacebookAuthHelper::.ctor(System.Boolean,System.Action)
extern "C"  void FacebookAuthHelper__ctor_m2219996062 (FacebookAuthHelper_t1964649404 * __this, bool ___requiresPublishPermissions0, Action_t3226471752 * ___afterAuthAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::Finalize()
extern "C"  void FacebookAuthHelper_Finalize_m3020465606 (FacebookAuthHelper_t1964649404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::cleanup()
extern "C"  void FacebookAuthHelper_cleanup_m1238229378 (FacebookAuthHelper_t1964649404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::start()
extern "C"  void FacebookAuthHelper_start_m3402689470 (FacebookAuthHelper_t1964649404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::sessionOpenedEvent()
extern "C"  void FacebookAuthHelper_sessionOpenedEvent_m4113198411 (FacebookAuthHelper_t1964649404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::loginFailedEvent(Prime31.P31Error)
extern "C"  void FacebookAuthHelper_loginFailedEvent_m157533977 (FacebookAuthHelper_t1964649404 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::reauthorizationSucceededEvent()
extern "C"  void FacebookAuthHelper_reauthorizationSucceededEvent_m3002559579 (FacebookAuthHelper_t1964649404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookAuthHelper::reauthorizationFailedEvent(Prime31.P31Error)
extern "C"  void FacebookAuthHelper_reauthorizationFailedEvent_m1657547946 (FacebookAuthHelper_t1964649404 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
