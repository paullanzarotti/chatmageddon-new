﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigExcC14NWithCommentsTransform
struct XmlDsigExcC14NWithCommentsTransform_t1136189132;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigExcC14NWithCommentsTransform::.ctor()
extern "C"  void XmlDsigExcC14NWithCommentsTransform__ctor_m3332296190 (XmlDsigExcC14NWithCommentsTransform_t1136189132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
