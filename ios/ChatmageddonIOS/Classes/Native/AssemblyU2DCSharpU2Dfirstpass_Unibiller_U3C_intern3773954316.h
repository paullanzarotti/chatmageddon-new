﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.BillerFactory
struct BillerFactory_t1450757922;
// Unibill.Biller
struct Biller_t1615588570;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibiller/<_internal_hook_events>c__AnonStorey0
struct  U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316  : public Il2CppObject
{
public:
	// Unibill.Impl.BillerFactory Unibiller/<_internal_hook_events>c__AnonStorey0::factory
	BillerFactory_t1450757922 * ___factory_0;
	// Unibill.Biller Unibiller/<_internal_hook_events>c__AnonStorey0::biller
	Biller_t1615588570 * ___biller_1;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316, ___factory_0)); }
	inline BillerFactory_t1450757922 * get_factory_0() const { return ___factory_0; }
	inline BillerFactory_t1450757922 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(BillerFactory_t1450757922 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier(&___factory_0, value);
	}

	inline static int32_t get_offset_of_biller_1() { return static_cast<int32_t>(offsetof(U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316, ___biller_1)); }
	inline Biller_t1615588570 * get_biller_1() const { return ___biller_1; }
	inline Biller_t1615588570 ** get_address_of_biller_1() { return &___biller_1; }
	inline void set_biller_1(Biller_t1615588570 * value)
	{
		___biller_1 = value;
		Il2CppCodeGenWriteBarrier(&___biller_1, value);
	}
};

struct U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316_StaticFields
{
public:
	// System.Action`2<System.String,System.String> Unibiller/<_internal_hook_events>c__AnonStorey0::<>f__am$cache0
	Action_2_t4234541925 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_2_t4234541925 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_2_t4234541925 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_2_t4234541925 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
