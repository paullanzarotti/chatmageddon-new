﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Drawing_CellShading
struct  CameraFilterPack_Drawing_CellShading_t478413216  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Drawing_CellShading::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_CellShading::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Drawing_CellShading::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Drawing_CellShading::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Drawing_CellShading::EdgeSize
	float ___EdgeSize_6;
	// System.Single CameraFilterPack_Drawing_CellShading::ColorLevel
	float ___ColorLevel_7;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_EdgeSize_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216, ___EdgeSize_6)); }
	inline float get_EdgeSize_6() const { return ___EdgeSize_6; }
	inline float* get_address_of_EdgeSize_6() { return &___EdgeSize_6; }
	inline void set_EdgeSize_6(float value)
	{
		___EdgeSize_6 = value;
	}

	inline static int32_t get_offset_of_ColorLevel_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216, ___ColorLevel_7)); }
	inline float get_ColorLevel_7() const { return ___ColorLevel_7; }
	inline float* get_address_of_ColorLevel_7() { return &___ColorLevel_7; }
	inline void set_ColorLevel_7(float value)
	{
		___ColorLevel_7 = value;
	}
};

struct CameraFilterPack_Drawing_CellShading_t478413216_StaticFields
{
public:
	// System.Single CameraFilterPack_Drawing_CellShading::ChangeEdgeSize
	float ___ChangeEdgeSize_8;
	// System.Single CameraFilterPack_Drawing_CellShading::ChangeColorLevel
	float ___ChangeColorLevel_9;

public:
	inline static int32_t get_offset_of_ChangeEdgeSize_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216_StaticFields, ___ChangeEdgeSize_8)); }
	inline float get_ChangeEdgeSize_8() const { return ___ChangeEdgeSize_8; }
	inline float* get_address_of_ChangeEdgeSize_8() { return &___ChangeEdgeSize_8; }
	inline void set_ChangeEdgeSize_8(float value)
	{
		___ChangeEdgeSize_8 = value;
	}

	inline static int32_t get_offset_of_ChangeColorLevel_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_CellShading_t478413216_StaticFields, ___ChangeColorLevel_9)); }
	inline float get_ChangeColorLevel_9() const { return ___ChangeColorLevel_9; }
	inline float* get_address_of_ChangeColorLevel_9() { return &___ChangeColorLevel_9; }
	inline void set_ChangeColorLevel_9(float value)
	{
		___ChangeColorLevel_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
