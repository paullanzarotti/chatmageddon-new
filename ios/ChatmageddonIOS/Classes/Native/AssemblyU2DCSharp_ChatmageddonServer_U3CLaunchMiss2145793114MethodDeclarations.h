﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<LaunchMissile>c__AnonStorey23
struct U3CLaunchMissileU3Ec__AnonStorey23_t2145793114;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<LaunchMissile>c__AnonStorey23::.ctor()
extern "C"  void U3CLaunchMissileU3Ec__AnonStorey23__ctor_m2644755737 (U3CLaunchMissileU3Ec__AnonStorey23_t2145793114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<LaunchMissile>c__AnonStorey23::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLaunchMissileU3Ec__AnonStorey23_U3CU3Em__0_m4089160434 (U3CLaunchMissileU3Ec__AnonStorey23_t2145793114 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
