﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// MissileArriveTimer/OnZeroHit
struct OnZeroHit_t3980744303;
// MissileArriveTimer/OnTimerUpdate
struct OnTimerUpdate_t2669454040;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileArriveTimer
struct  MissileArriveTimer_t44929408  : public MonoBehaviour_t1158329972
{
public:
	// UILabel MissileArriveTimer::timeLabel
	UILabel_t1795115428 * ___timeLabel_2;
	// LaunchedMissile MissileArriveTimer::missileItem
	LaunchedMissile_t1542515810 * ___missileItem_3;
	// UnityEngine.Coroutine MissileArriveTimer::calcRoutine
	Coroutine_t2299508840 * ___calcRoutine_4;
	// MissileArriveTimer/OnZeroHit MissileArriveTimer::onZeroHit
	OnZeroHit_t3980744303 * ___onZeroHit_5;
	// MissileArriveTimer/OnTimerUpdate MissileArriveTimer::onTimerUpdate
	OnTimerUpdate_t2669454040 * ___onTimerUpdate_6;

public:
	inline static int32_t get_offset_of_timeLabel_2() { return static_cast<int32_t>(offsetof(MissileArriveTimer_t44929408, ___timeLabel_2)); }
	inline UILabel_t1795115428 * get_timeLabel_2() const { return ___timeLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_2() { return &___timeLabel_2; }
	inline void set_timeLabel_2(UILabel_t1795115428 * value)
	{
		___timeLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_2, value);
	}

	inline static int32_t get_offset_of_missileItem_3() { return static_cast<int32_t>(offsetof(MissileArriveTimer_t44929408, ___missileItem_3)); }
	inline LaunchedMissile_t1542515810 * get_missileItem_3() const { return ___missileItem_3; }
	inline LaunchedMissile_t1542515810 ** get_address_of_missileItem_3() { return &___missileItem_3; }
	inline void set_missileItem_3(LaunchedMissile_t1542515810 * value)
	{
		___missileItem_3 = value;
		Il2CppCodeGenWriteBarrier(&___missileItem_3, value);
	}

	inline static int32_t get_offset_of_calcRoutine_4() { return static_cast<int32_t>(offsetof(MissileArriveTimer_t44929408, ___calcRoutine_4)); }
	inline Coroutine_t2299508840 * get_calcRoutine_4() const { return ___calcRoutine_4; }
	inline Coroutine_t2299508840 ** get_address_of_calcRoutine_4() { return &___calcRoutine_4; }
	inline void set_calcRoutine_4(Coroutine_t2299508840 * value)
	{
		___calcRoutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___calcRoutine_4, value);
	}

	inline static int32_t get_offset_of_onZeroHit_5() { return static_cast<int32_t>(offsetof(MissileArriveTimer_t44929408, ___onZeroHit_5)); }
	inline OnZeroHit_t3980744303 * get_onZeroHit_5() const { return ___onZeroHit_5; }
	inline OnZeroHit_t3980744303 ** get_address_of_onZeroHit_5() { return &___onZeroHit_5; }
	inline void set_onZeroHit_5(OnZeroHit_t3980744303 * value)
	{
		___onZeroHit_5 = value;
		Il2CppCodeGenWriteBarrier(&___onZeroHit_5, value);
	}

	inline static int32_t get_offset_of_onTimerUpdate_6() { return static_cast<int32_t>(offsetof(MissileArriveTimer_t44929408, ___onTimerUpdate_6)); }
	inline OnTimerUpdate_t2669454040 * get_onTimerUpdate_6() const { return ___onTimerUpdate_6; }
	inline OnTimerUpdate_t2669454040 ** get_address_of_onTimerUpdate_6() { return &___onTimerUpdate_6; }
	inline void set_onTimerUpdate_6(OnTimerUpdate_t2669454040 * value)
	{
		___onTimerUpdate_6 = value;
		Il2CppCodeGenWriteBarrier(&___onTimerUpdate_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
