﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookBaseDTO
struct FacebookBaseDTO_t79607460;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookBaseDTO::.ctor()
extern "C"  void FacebookBaseDTO__ctor_m1169550408 (FacebookBaseDTO_t79607460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookBaseDTO::ToString()
extern "C"  String_t* FacebookBaseDTO_ToString_m36948903 (FacebookBaseDTO_t79607460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
