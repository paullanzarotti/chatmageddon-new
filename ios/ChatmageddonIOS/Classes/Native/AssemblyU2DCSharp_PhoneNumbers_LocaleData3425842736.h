﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t1563811461;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.LocaleData
struct  LocaleData_t3425842736  : public Il2CppObject
{
public:

public:
};

struct LocaleData_t3425842736_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> PhoneNumbers.LocaleData::_Data
	Dictionary_2_t1563811461 * ____Data_0;
	// System.Func`2<System.String,System.Boolean> PhoneNumbers.LocaleData::<>f__am$cache0
	Func_2_t1989381442 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of__Data_0() { return static_cast<int32_t>(offsetof(LocaleData_t3425842736_StaticFields, ____Data_0)); }
	inline Dictionary_2_t1563811461 * get__Data_0() const { return ____Data_0; }
	inline Dictionary_2_t1563811461 ** get_address_of__Data_0() { return &____Data_0; }
	inline void set__Data_0(Dictionary_2_t1563811461 * value)
	{
		____Data_0 = value;
		Il2CppCodeGenWriteBarrier(&____Data_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(LocaleData_t3425842736_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t1989381442 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t1989381442 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t1989381442 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
