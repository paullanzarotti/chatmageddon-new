﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar/OnProgressUpdated
struct OnProgressUpdated_t2843909178;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ProgressBar/OnProgressUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void OnProgressUpdated__ctor_m236832301 (OnProgressUpdated_t2843909178 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar/OnProgressUpdated::Invoke(System.Single)
extern "C"  void OnProgressUpdated_Invoke_m1388408686 (OnProgressUpdated_t2843909178 * __this, float ___currentPercent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ProgressBar/OnProgressUpdated::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnProgressUpdated_BeginInvoke_m4033815667 (OnProgressUpdated_t2843909178 * __this, float ___currentPercent0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar/OnProgressUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void OnProgressUpdated_EndInvoke_m3704585683 (OnProgressUpdated_t2843909178 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
