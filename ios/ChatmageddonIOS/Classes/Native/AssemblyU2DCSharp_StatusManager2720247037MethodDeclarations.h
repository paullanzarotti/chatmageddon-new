﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusManager
struct StatusManager_t2720247037;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StatusListType2488921270.h"

// System.Void StatusManager::.ctor()
extern "C"  void StatusManager__ctor_m4129893492 (StatusManager_t2720247037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusManager::StartList(StatusListType)
extern "C"  void StatusManager_StartList_m904555342 (StatusManager_t2720247037 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusManager::SetListDrawn(StatusListType)
extern "C"  void StatusManager_SetListDrawn_m695246176 (StatusManager_t2720247037 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
