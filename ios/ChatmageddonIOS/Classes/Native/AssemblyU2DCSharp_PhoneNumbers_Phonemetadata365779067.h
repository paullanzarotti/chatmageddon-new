﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.Phonemetadata
struct  Phonemetadata_t365779067  : public Il2CppObject
{
public:

public:
};

struct Phonemetadata_t365779067_StaticFields
{
public:
	// System.Object PhoneNumbers.Phonemetadata::Descriptor
	Il2CppObject * ___Descriptor_0;

public:
	inline static int32_t get_offset_of_Descriptor_0() { return static_cast<int32_t>(offsetof(Phonemetadata_t365779067_StaticFields, ___Descriptor_0)); }
	inline Il2CppObject * get_Descriptor_0() const { return ___Descriptor_0; }
	inline Il2CppObject ** get_address_of_Descriptor_0() { return &___Descriptor_0; }
	inline void set_Descriptor_0(Il2CppObject * value)
	{
		___Descriptor_0 = value;
		Il2CppCodeGenWriteBarrier(&___Descriptor_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
