﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsNavigateForwardsButton
struct FriendsNavigateForwardsButton_t433492960;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsNavigateForwardsButton::.ctor()
extern "C"  void FriendsNavigateForwardsButton__ctor_m2441649631 (FriendsNavigateForwardsButton_t433492960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigateForwardsButton::OnButtonClick()
extern "C"  void FriendsNavigateForwardsButton_OnButtonClick_m3099114184 (FriendsNavigateForwardsButton_t433492960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigateForwardsButton::SetButtonActive(System.Boolean)
extern "C"  void FriendsNavigateForwardsButton_SetButtonActive_m4091613288 (FriendsNavigateForwardsButton_t433492960 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
