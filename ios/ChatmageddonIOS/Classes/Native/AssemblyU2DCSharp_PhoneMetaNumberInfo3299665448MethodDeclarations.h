﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneMetaNumberInfo
struct PhoneMetaNumberInfo_t3299665448;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneMetaNumberInfo::.ctor()
extern "C"  void PhoneMetaNumberInfo__ctor_m2509070283 (PhoneMetaNumberInfo_t3299665448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
