﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>
struct Collection_1_t805212004;
// System.Collections.Generic.IList`1<PhoneNumberNavScreen>
struct IList_1_t1804407851;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PhoneNumberNavScreen[]
struct PhoneNumberNavScreenU5BU5D_t4021408007;
// System.Collections.Generic.IEnumerator`1<PhoneNumberNavScreen>
struct IEnumerator_1_t3033958373;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"

// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3847036755_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3847036755(__this, method) ((  void (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1__ctor_m3847036755_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1008630456_gshared (Collection_1_t805212004 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1008630456(__this, ___list0, method) ((  void (*) (Collection_1_t805212004 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1008630456_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m266476354_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m266476354(__this, method) ((  bool (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m266476354_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3379275791_gshared (Collection_1_t805212004 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3379275791(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t805212004 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3379275791_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2492210214_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2492210214(__this, method) ((  Il2CppObject * (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2492210214_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2706962095_gshared (Collection_1_t805212004 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2706962095(__this, ___value0, method) ((  int32_t (*) (Collection_1_t805212004 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2706962095_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1031609735_gshared (Collection_1_t805212004 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1031609735(__this, ___value0, method) ((  bool (*) (Collection_1_t805212004 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1031609735_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1131781405_gshared (Collection_1_t805212004 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1131781405(__this, ___value0, method) ((  int32_t (*) (Collection_1_t805212004 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1131781405_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1856433660_gshared (Collection_1_t805212004 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1856433660(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t805212004 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1856433660_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3183679890_gshared (Collection_1_t805212004 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3183679890(__this, ___value0, method) ((  void (*) (Collection_1_t805212004 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3183679890_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2438915655_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2438915655(__this, method) ((  bool (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2438915655_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3014376643_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3014376643(__this, method) ((  Il2CppObject * (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3014376643_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m817548136_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m817548136(__this, method) ((  bool (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m817548136_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2839461259_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2839461259(__this, method) ((  bool (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2839461259_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4215753894_gshared (Collection_1_t805212004 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4215753894(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4215753894_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m708153393_gshared (Collection_1_t805212004 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m708153393(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t805212004 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m708153393_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m1618985960_gshared (Collection_1_t805212004 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1618985960(__this, ___item0, method) ((  void (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_Add_m1618985960_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m3916846316_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3916846316(__this, method) ((  void (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_Clear_m3916846316_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2863203918_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2863203918(__this, method) ((  void (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_ClearItems_m2863203918_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m2123850882_gshared (Collection_1_t805212004 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2123850882(__this, ___item0, method) ((  bool (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_Contains_m2123850882_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3162456388_gshared (Collection_1_t805212004 * __this, PhoneNumberNavScreenU5BU5D_t4021408007* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3162456388(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t805212004 *, PhoneNumberNavScreenU5BU5D_t4021408007*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3162456388_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3878570639_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3878570639(__this, method) ((  Il2CppObject* (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_GetEnumerator_m3878570639_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m916287874_gshared (Collection_1_t805212004 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m916287874(__this, ___item0, method) ((  int32_t (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m916287874_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m233227157_gshared (Collection_1_t805212004 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m233227157(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t805212004 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m233227157_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1890869398_gshared (Collection_1_t805212004 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1890869398(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t805212004 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1890869398_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m3376137669_gshared (Collection_1_t805212004 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3376137669(__this, ___item0, method) ((  bool (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_Remove_m3376137669_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2036329465_gshared (Collection_1_t805212004 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2036329465(__this, ___index0, method) ((  void (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2036329465_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2347085517_gshared (Collection_1_t805212004 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2347085517(__this, ___index0, method) ((  void (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2347085517_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2763026047_gshared (Collection_1_t805212004 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2763026047(__this, method) ((  int32_t (*) (Collection_1_t805212004 *, const MethodInfo*))Collection_1_get_Count_m2763026047_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m2406165989_gshared (Collection_1_t805212004 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2406165989(__this, ___index0, method) ((  int32_t (*) (Collection_1_t805212004 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2406165989_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3516873126_gshared (Collection_1_t805212004 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3516873126(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t805212004 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3516873126_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2834721361_gshared (Collection_1_t805212004 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2834721361(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t805212004 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m2834721361_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m988622948_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m988622948(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m988622948_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m1372740804_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1372740804(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1372740804_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1933586608_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1933586608(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1933586608_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3788242686_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3788242686(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3788242686_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PhoneNumberNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m4197663997_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m4197663997(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m4197663997_gshared)(__this /* static, unused */, ___list0, method)
