﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlSecureResolver
struct XmlSecureResolver_t2018216936;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Security.Policy.Evidence
struct Evidence_t1407710183;
// System.Object
struct Il2CppObject;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "mscorlib_System_Security_Policy_Evidence1407710183.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Xml.XmlSecureResolver::.ctor(System.Xml.XmlResolver,System.Security.Policy.Evidence)
extern "C"  void XmlSecureResolver__ctor_m1300417422 (XmlSecureResolver_t2018216936 * __this, XmlResolver_t2024571559 * ___resolver0, Evidence_t1407710183 * ___evidence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XmlSecureResolver::GetEntity(System.Uri,System.String,System.Type)
extern "C"  Il2CppObject * XmlSecureResolver_GetEntity_m2427217525 (XmlSecureResolver_t2018216936 * __this, Uri_t19570940 * ___absoluteUri0, String_t* ___role1, Type_t * ___ofObjectToReturn2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Xml.XmlSecureResolver::ResolveUri(System.Uri,System.String)
extern "C"  Uri_t19570940 * XmlSecureResolver_ResolveUri_m2141318978 (XmlSecureResolver_t2018216936 * __this, Uri_t19570940 * ___baseUri0, String_t* ___relativeUri1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
