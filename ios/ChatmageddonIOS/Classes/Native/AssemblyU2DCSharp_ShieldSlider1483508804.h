﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SliderController2812089311.h"
#include "AssemblyU2DCSharp_ShieldSliderType2078175364.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldSlider
struct  ShieldSlider_t1483508804  : public SliderController_t2812089311
{
public:
	// ShieldSliderType ShieldSlider::type
	int32_t ___type_13;

public:
	inline static int32_t get_offset_of_type_13() { return static_cast<int32_t>(offsetof(ShieldSlider_t1483508804, ___type_13)); }
	inline int32_t get_type_13() const { return ___type_13; }
	inline int32_t* get_address_of_type_13() { return &___type_13; }
	inline void set_type_13(int32_t value)
	{
		___type_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
