﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsProfileContent
struct SettingsProfileContent_t3280006997;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsProfileContent::.ctor()
extern "C"  void SettingsProfileContent__ctor_m1661625182 (SettingsProfileContent_t3280006997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
