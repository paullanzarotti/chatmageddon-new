﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuelButton
struct FuelButton_t3326934984;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void FuelButton::.ctor()
extern "C"  void FuelButton__ctor_m3276806263 (FuelButton_t3326934984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelButton::OnButtonClick()
extern "C"  void FuelButton_OnButtonClick_m599129420 (FuelButton_t3326934984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FuelButton::WaitToAllowToast()
extern "C"  Il2CppObject * FuelButton_WaitToAllowToast_m2977718457 (FuelButton_t3326934984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
