﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberMatch
struct  PhoneNumberMatch_t2163858580  : public Il2CppObject
{
public:
	// System.Int32 PhoneNumbers.PhoneNumberMatch::<Start>k__BackingField
	int32_t ___U3CStartU3Ek__BackingField_0;
	// System.String PhoneNumbers.PhoneNumberMatch::<RawString>k__BackingField
	String_t* ___U3CRawStringU3Ek__BackingField_1;
	// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberMatch::<Number>k__BackingField
	PhoneNumber_t814071929 * ___U3CNumberU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PhoneNumberMatch_t2163858580, ___U3CStartU3Ek__BackingField_0)); }
	inline int32_t get_U3CStartU3Ek__BackingField_0() const { return ___U3CStartU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStartU3Ek__BackingField_0() { return &___U3CStartU3Ek__BackingField_0; }
	inline void set_U3CStartU3Ek__BackingField_0(int32_t value)
	{
		___U3CStartU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRawStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PhoneNumberMatch_t2163858580, ___U3CRawStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CRawStringU3Ek__BackingField_1() const { return ___U3CRawStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRawStringU3Ek__BackingField_1() { return &___U3CRawStringU3Ek__BackingField_1; }
	inline void set_U3CRawStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CRawStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRawStringU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CNumberU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PhoneNumberMatch_t2163858580, ___U3CNumberU3Ek__BackingField_2)); }
	inline PhoneNumber_t814071929 * get_U3CNumberU3Ek__BackingField_2() const { return ___U3CNumberU3Ek__BackingField_2; }
	inline PhoneNumber_t814071929 ** get_address_of_U3CNumberU3Ek__BackingField_2() { return &___U3CNumberU3Ek__BackingField_2; }
	inline void set_U3CNumberU3Ek__BackingField_2(PhoneNumber_t814071929 * value)
	{
		___U3CNumberU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNumberU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
