﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatNavigateBackwardButton
struct  ChatNavigateBackwardButton_t516150148  : public MonoBehaviour_t1158329972
{
public:
	// SFX ChatNavigateBackwardButton::sfxToPlay
	int32_t ___sfxToPlay_2;
	// UISprite ChatNavigateBackwardButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_3;

public:
	inline static int32_t get_offset_of_sfxToPlay_2() { return static_cast<int32_t>(offsetof(ChatNavigateBackwardButton_t516150148, ___sfxToPlay_2)); }
	inline int32_t get_sfxToPlay_2() const { return ___sfxToPlay_2; }
	inline int32_t* get_address_of_sfxToPlay_2() { return &___sfxToPlay_2; }
	inline void set_sfxToPlay_2(int32_t value)
	{
		___sfxToPlay_2 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_3() { return static_cast<int32_t>(offsetof(ChatNavigateBackwardButton_t516150148, ___buttonSprite_3)); }
	inline UISprite_t603616735 * get_buttonSprite_3() const { return ___buttonSprite_3; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_3() { return &___buttonSprite_3; }
	inline void set_buttonSprite_3(UISprite_t603616735 * value)
	{
		___buttonSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
