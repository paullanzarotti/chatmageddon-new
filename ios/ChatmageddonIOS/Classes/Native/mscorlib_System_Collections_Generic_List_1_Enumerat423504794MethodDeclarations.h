﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m845703150(__this, ___l0, method) ((  void (*) (Enumerator_t423504794 *, List_1_t888775120 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3487212684(__this, method) ((  void (*) (Enumerator_t423504794 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m976020648(__this, method) ((  Il2CppObject * (*) (Enumerator_t423504794 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::Dispose()
#define Enumerator_Dispose_m974547147(__this, method) ((  void (*) (Enumerator_t423504794 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::VerifyState()
#define Enumerator_VerifyState_m261228704(__this, method) ((  void (*) (Enumerator_t423504794 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::MoveNext()
#define Enumerator_MoveNext_m4132528607(__this, method) ((  bool (*) (Enumerator_t423504794 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Unibill.ProductDefinition>::get_Current()
#define Enumerator_get_Current_m2408243075(__this, method) ((  ProductDefinition_t1519653988 * (*) (Enumerator_t423504794 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
