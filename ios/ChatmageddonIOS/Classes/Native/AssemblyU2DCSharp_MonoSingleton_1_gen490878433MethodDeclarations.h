﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ChatmageddonDeviceShareManager>::.ctor()
#define MonoSingleton_1__ctor_m1232648683(__this, method) ((  void (*) (MonoSingleton_1_t490878433 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonDeviceShareManager>::Awake()
#define MonoSingleton_1_Awake_m1418826506(__this, method) ((  void (*) (MonoSingleton_1_t490878433 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ChatmageddonDeviceShareManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2076036508(__this /* static, unused */, method) ((  ChatmageddonDeviceShareManager_t740212713 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ChatmageddonDeviceShareManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3461848540(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ChatmageddonDeviceShareManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m4127085497(__this, method) ((  void (*) (MonoSingleton_1_t490878433 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonDeviceShareManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2424314129(__this, method) ((  void (*) (MonoSingleton_1_t490878433 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonDeviceShareManager>::.cctor()
#define MonoSingleton_1__cctor_m2361668380(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
