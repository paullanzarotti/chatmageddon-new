﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountdownTimer/OnTimerFinished
struct OnTimerFinished_t1234642247;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void CountdownTimer/OnTimerFinished::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTimerFinished__ctor_m740271598 (OnTimerFinished_t1234642247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer/OnTimerFinished::Invoke()
extern "C"  void OnTimerFinished_Invoke_m2261567156 (OnTimerFinished_t1234642247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult CountdownTimer/OnTimerFinished::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnTimerFinished_BeginInvoke_m3588422921 (OnTimerFinished_t1234642247 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer/OnTimerFinished::EndInvoke(System.IAsyncResult)
extern "C"  void OnTimerFinished_EndInvoke_m228301928 (OnTimerFinished_t1234642247 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
