﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AboutAndHelpNavScreen
struct AboutAndHelpNavScreen_t3086328594;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AboutAndHelpNavScreen::.ctor()
extern "C"  void AboutAndHelpNavScreen__ctor_m652826081 (AboutAndHelpNavScreen_t3086328594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutAndHelpNavScreen::Start()
extern "C"  void AboutAndHelpNavScreen_Start_m3654085977 (AboutAndHelpNavScreen_t3086328594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutAndHelpNavScreen::UIClosing()
extern "C"  void AboutAndHelpNavScreen_UIClosing_m2062159332 (AboutAndHelpNavScreen_t3086328594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutAndHelpNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AboutAndHelpNavScreen_ScreenLoad_m1488210843 (AboutAndHelpNavScreen_t3086328594 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutAndHelpNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AboutAndHelpNavScreen_ScreenUnload_m3620323675 (AboutAndHelpNavScreen_t3086328594 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AboutAndHelpNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AboutAndHelpNavScreen_ValidateScreenNavigation_m2161703406 (AboutAndHelpNavScreen_t3086328594 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
