﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SliderController
struct SliderController_t2812089311;

#include "codegen/il2cpp-codegen.h"

// System.Void SliderController::.ctor()
extern "C"  void SliderController__ctor_m3702861044 (SliderController_t2812089311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderController::Awake()
extern "C"  void SliderController_Awake_m3071087887 (SliderController_t2812089311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderController::UpdatePoints()
extern "C"  void SliderController_UpdatePoints_m718637968 (SliderController_t2812089311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderController::CalculateClosestPoint(System.Boolean)
extern "C"  void SliderController_CalculateClosestPoint_m3882238834 (SliderController_t2812089311 * __this, bool ___dragging0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderController::JumpToPoint(System.Int32)
extern "C"  void SliderController_JumpToPoint_m2924304972 (SliderController_t2812089311 * __this, int32_t ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderController::UpdateLabels()
extern "C"  void SliderController_UpdateLabels_m736760812 (SliderController_t2812089311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderController::LabelsUpdated(System.Int32)
extern "C"  void SliderController_LabelsUpdated_m3755265263 (SliderController_t2812089311 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SliderController::GetClosestPoint()
extern "C"  int32_t SliderController_GetClosestPoint_m3199339955 (SliderController_t2812089311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
