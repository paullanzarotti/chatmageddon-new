﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1
struct U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1::.ctor()
extern "C"  void U3CAcceptFriendRequestU3Ec__AnonStorey1__ctor_m3802880148 (U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CAcceptFriendRequestU3Ec__AnonStorey1_U3CU3Em__0_m882801692 (U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
