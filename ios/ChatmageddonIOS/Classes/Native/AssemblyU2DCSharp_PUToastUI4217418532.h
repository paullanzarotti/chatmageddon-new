﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UILabel
struct UILabel_t1795115428;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;
// PUBread
struct PUBread_t493011471;
// TweenScale
struct TweenScale_t2697902175;
// System.Collections.Generic.List`1<TweenAlpha>
struct List_1_t1790639767;

#include "AssemblyU2DCSharp_ToastUI270783635.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PUToastUI
struct  PUToastUI_t4217418532  : public ToastUI_t270783635
{
public:
	// UnityEngine.Transform PUToastUI::content
	Transform_t3275118058 * ___content_6;
	// UILabel PUToastUI::titleLabel
	UILabel_t1795115428 * ___titleLabel_7;
	// UITexture PUToastUI::puTexture
	UITexture_t2537039969 * ___puTexture_8;
	// UnityEngine.BoxCollider PUToastUI::buttonCollider
	BoxCollider_t22920061 * ___buttonCollider_9;
	// UISprite PUToastUI::buttonSprite
	UISprite_t603616735 * ___buttonSprite_10;
	// UILabel PUToastUI::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_11;
	// PUBread PUToastUI::puBread
	PUBread_t493011471 * ___puBread_12;
	// System.Int32 PUToastUI::puPadding
	int32_t ___puPadding_13;
	// System.Int32 PUToastUI::messagePadding
	int32_t ___messagePadding_14;
	// System.Int32 PUToastUI::titlePadding
	int32_t ___titlePadding_15;
	// System.Int32 PUToastUI::texturePadding
	int32_t ___texturePadding_16;
	// System.Int32 PUToastUI::buttonPadding
	int32_t ___buttonPadding_17;
	// System.Int32 PUToastUI::buttonLabelPadding
	int32_t ___buttonLabelPadding_18;
	// TweenScale PUToastUI::scaleTween
	TweenScale_t2697902175 * ___scaleTween_19;
	// System.Collections.Generic.List`1<TweenAlpha> PUToastUI::alphaTweens
	List_1_t1790639767 * ___alphaTweens_20;
	// System.Boolean PUToastUI::closing
	bool ___closing_21;

public:
	inline static int32_t get_offset_of_content_6() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___content_6)); }
	inline Transform_t3275118058 * get_content_6() const { return ___content_6; }
	inline Transform_t3275118058 ** get_address_of_content_6() { return &___content_6; }
	inline void set_content_6(Transform_t3275118058 * value)
	{
		___content_6 = value;
		Il2CppCodeGenWriteBarrier(&___content_6, value);
	}

	inline static int32_t get_offset_of_titleLabel_7() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___titleLabel_7)); }
	inline UILabel_t1795115428 * get_titleLabel_7() const { return ___titleLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_7() { return &___titleLabel_7; }
	inline void set_titleLabel_7(UILabel_t1795115428 * value)
	{
		___titleLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_7, value);
	}

	inline static int32_t get_offset_of_puTexture_8() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___puTexture_8)); }
	inline UITexture_t2537039969 * get_puTexture_8() const { return ___puTexture_8; }
	inline UITexture_t2537039969 ** get_address_of_puTexture_8() { return &___puTexture_8; }
	inline void set_puTexture_8(UITexture_t2537039969 * value)
	{
		___puTexture_8 = value;
		Il2CppCodeGenWriteBarrier(&___puTexture_8, value);
	}

	inline static int32_t get_offset_of_buttonCollider_9() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___buttonCollider_9)); }
	inline BoxCollider_t22920061 * get_buttonCollider_9() const { return ___buttonCollider_9; }
	inline BoxCollider_t22920061 ** get_address_of_buttonCollider_9() { return &___buttonCollider_9; }
	inline void set_buttonCollider_9(BoxCollider_t22920061 * value)
	{
		___buttonCollider_9 = value;
		Il2CppCodeGenWriteBarrier(&___buttonCollider_9, value);
	}

	inline static int32_t get_offset_of_buttonSprite_10() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___buttonSprite_10)); }
	inline UISprite_t603616735 * get_buttonSprite_10() const { return ___buttonSprite_10; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_10() { return &___buttonSprite_10; }
	inline void set_buttonSprite_10(UISprite_t603616735 * value)
	{
		___buttonSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_10, value);
	}

	inline static int32_t get_offset_of_buttonLabel_11() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___buttonLabel_11)); }
	inline UILabel_t1795115428 * get_buttonLabel_11() const { return ___buttonLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_11() { return &___buttonLabel_11; }
	inline void set_buttonLabel_11(UILabel_t1795115428 * value)
	{
		___buttonLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_11, value);
	}

	inline static int32_t get_offset_of_puBread_12() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___puBread_12)); }
	inline PUBread_t493011471 * get_puBread_12() const { return ___puBread_12; }
	inline PUBread_t493011471 ** get_address_of_puBread_12() { return &___puBread_12; }
	inline void set_puBread_12(PUBread_t493011471 * value)
	{
		___puBread_12 = value;
		Il2CppCodeGenWriteBarrier(&___puBread_12, value);
	}

	inline static int32_t get_offset_of_puPadding_13() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___puPadding_13)); }
	inline int32_t get_puPadding_13() const { return ___puPadding_13; }
	inline int32_t* get_address_of_puPadding_13() { return &___puPadding_13; }
	inline void set_puPadding_13(int32_t value)
	{
		___puPadding_13 = value;
	}

	inline static int32_t get_offset_of_messagePadding_14() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___messagePadding_14)); }
	inline int32_t get_messagePadding_14() const { return ___messagePadding_14; }
	inline int32_t* get_address_of_messagePadding_14() { return &___messagePadding_14; }
	inline void set_messagePadding_14(int32_t value)
	{
		___messagePadding_14 = value;
	}

	inline static int32_t get_offset_of_titlePadding_15() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___titlePadding_15)); }
	inline int32_t get_titlePadding_15() const { return ___titlePadding_15; }
	inline int32_t* get_address_of_titlePadding_15() { return &___titlePadding_15; }
	inline void set_titlePadding_15(int32_t value)
	{
		___titlePadding_15 = value;
	}

	inline static int32_t get_offset_of_texturePadding_16() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___texturePadding_16)); }
	inline int32_t get_texturePadding_16() const { return ___texturePadding_16; }
	inline int32_t* get_address_of_texturePadding_16() { return &___texturePadding_16; }
	inline void set_texturePadding_16(int32_t value)
	{
		___texturePadding_16 = value;
	}

	inline static int32_t get_offset_of_buttonPadding_17() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___buttonPadding_17)); }
	inline int32_t get_buttonPadding_17() const { return ___buttonPadding_17; }
	inline int32_t* get_address_of_buttonPadding_17() { return &___buttonPadding_17; }
	inline void set_buttonPadding_17(int32_t value)
	{
		___buttonPadding_17 = value;
	}

	inline static int32_t get_offset_of_buttonLabelPadding_18() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___buttonLabelPadding_18)); }
	inline int32_t get_buttonLabelPadding_18() const { return ___buttonLabelPadding_18; }
	inline int32_t* get_address_of_buttonLabelPadding_18() { return &___buttonLabelPadding_18; }
	inline void set_buttonLabelPadding_18(int32_t value)
	{
		___buttonLabelPadding_18 = value;
	}

	inline static int32_t get_offset_of_scaleTween_19() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___scaleTween_19)); }
	inline TweenScale_t2697902175 * get_scaleTween_19() const { return ___scaleTween_19; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_19() { return &___scaleTween_19; }
	inline void set_scaleTween_19(TweenScale_t2697902175 * value)
	{
		___scaleTween_19 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_19, value);
	}

	inline static int32_t get_offset_of_alphaTweens_20() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___alphaTweens_20)); }
	inline List_1_t1790639767 * get_alphaTweens_20() const { return ___alphaTweens_20; }
	inline List_1_t1790639767 ** get_address_of_alphaTweens_20() { return &___alphaTweens_20; }
	inline void set_alphaTweens_20(List_1_t1790639767 * value)
	{
		___alphaTweens_20 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTweens_20, value);
	}

	inline static int32_t get_offset_of_closing_21() { return static_cast<int32_t>(offsetof(PUToastUI_t4217418532, ___closing_21)); }
	inline bool get_closing_21() const { return ___closing_21; }
	inline bool* get_address_of_closing_21() { return &___closing_21; }
	inline void set_closing_21(bool value)
	{
		___closing_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
