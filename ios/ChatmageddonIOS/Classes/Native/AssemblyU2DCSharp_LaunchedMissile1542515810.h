﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Friend
struct Friend_t3555014108;

#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_MissileStatus1722383730.h"
#include "AssemblyU2DCSharp_MissileResultStatus3272208871.h"
#include "AssemblyU2DCSharp_LaunchDirection3410220992.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"
#include "AssemblyU2DCSharp_WarefareState701596006.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchedMissile
struct  LaunchedMissile_t1542515810  : public Missile_t813944928
{
public:
	// MissileStatus LaunchedMissile::status
	int32_t ___status_10;
	// MissileResultStatus LaunchedMissile::resultStatus
	int32_t ___resultStatus_11;
	// LaunchDirection LaunchedMissile::direction
	int32_t ___direction_12;
	// System.Int32 LaunchedMissile::firstStrikePoints
	int32_t ___firstStrikePoints_13;
	// System.Boolean LaunchedMissile::perfectLaunch
	bool ___perfectLaunch_14;
	// System.Boolean LaunchedMissile::backfired
	bool ___backfired_15;
	// System.Single LaunchedMissile::launchAccuracy
	float ___launchAccuracy_16;
	// System.Single LaunchedMissile::launchPoints
	float ___launchPoints_17;
	// System.DateTime LaunchedMissile::launchedTime
	DateTime_t693205669  ___launchedTime_18;
	// System.DateTime LaunchedMissile::arriveTime
	DateTime_t693205669  ___arriveTime_19;
	// System.DateTime LaunchedMissile::visibleTime
	DateTime_t693205669  ___visibleTime_20;
	// System.String LaunchedMissile::defenderId
	String_t* ___defenderId_21;
	// Friend LaunchedMissile::friend
	Friend_t3555014108 * ___friend_22;
	// UnityEngine.LocationInfo LaunchedMissile::startLocation
	LocationInfo_t1364725149  ___startLocation_23;
	// System.DateTime LaunchedMissile::completeTime
	DateTime_t693205669  ___completeTime_24;
	// System.Single LaunchedMissile::maxResultPoints
	float ___maxResultPoints_25;
	// System.Int32 LaunchedMissile::resultPoints
	int32_t ___resultPoints_26;
	// WarefareState LaunchedMissile::resultAwardTo
	int32_t ___resultAwardTo_27;
	// System.Boolean LaunchedMissile::defendAttempted
	bool ___defendAttempted_28;
	// System.Int32 LaunchedMissile::defendAttemptCount
	int32_t ___defendAttemptCount_29;
	// System.DateTime LaunchedMissile::defendedTime
	DateTime_t693205669  ___defendedTime_30;
	// System.Boolean LaunchedMissile::defended
	bool ___defended_31;
	// System.String LaunchedMissile::defendTimeString
	String_t* ___defendTimeString_32;
	// System.Int32 LaunchedMissile::scorezoneDefendedIn
	int32_t ___scorezoneDefendedIn_33;

public:
	inline static int32_t get_offset_of_status_10() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___status_10)); }
	inline int32_t get_status_10() const { return ___status_10; }
	inline int32_t* get_address_of_status_10() { return &___status_10; }
	inline void set_status_10(int32_t value)
	{
		___status_10 = value;
	}

	inline static int32_t get_offset_of_resultStatus_11() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___resultStatus_11)); }
	inline int32_t get_resultStatus_11() const { return ___resultStatus_11; }
	inline int32_t* get_address_of_resultStatus_11() { return &___resultStatus_11; }
	inline void set_resultStatus_11(int32_t value)
	{
		___resultStatus_11 = value;
	}

	inline static int32_t get_offset_of_direction_12() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___direction_12)); }
	inline int32_t get_direction_12() const { return ___direction_12; }
	inline int32_t* get_address_of_direction_12() { return &___direction_12; }
	inline void set_direction_12(int32_t value)
	{
		___direction_12 = value;
	}

	inline static int32_t get_offset_of_firstStrikePoints_13() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___firstStrikePoints_13)); }
	inline int32_t get_firstStrikePoints_13() const { return ___firstStrikePoints_13; }
	inline int32_t* get_address_of_firstStrikePoints_13() { return &___firstStrikePoints_13; }
	inline void set_firstStrikePoints_13(int32_t value)
	{
		___firstStrikePoints_13 = value;
	}

	inline static int32_t get_offset_of_perfectLaunch_14() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___perfectLaunch_14)); }
	inline bool get_perfectLaunch_14() const { return ___perfectLaunch_14; }
	inline bool* get_address_of_perfectLaunch_14() { return &___perfectLaunch_14; }
	inline void set_perfectLaunch_14(bool value)
	{
		___perfectLaunch_14 = value;
	}

	inline static int32_t get_offset_of_backfired_15() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___backfired_15)); }
	inline bool get_backfired_15() const { return ___backfired_15; }
	inline bool* get_address_of_backfired_15() { return &___backfired_15; }
	inline void set_backfired_15(bool value)
	{
		___backfired_15 = value;
	}

	inline static int32_t get_offset_of_launchAccuracy_16() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___launchAccuracy_16)); }
	inline float get_launchAccuracy_16() const { return ___launchAccuracy_16; }
	inline float* get_address_of_launchAccuracy_16() { return &___launchAccuracy_16; }
	inline void set_launchAccuracy_16(float value)
	{
		___launchAccuracy_16 = value;
	}

	inline static int32_t get_offset_of_launchPoints_17() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___launchPoints_17)); }
	inline float get_launchPoints_17() const { return ___launchPoints_17; }
	inline float* get_address_of_launchPoints_17() { return &___launchPoints_17; }
	inline void set_launchPoints_17(float value)
	{
		___launchPoints_17 = value;
	}

	inline static int32_t get_offset_of_launchedTime_18() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___launchedTime_18)); }
	inline DateTime_t693205669  get_launchedTime_18() const { return ___launchedTime_18; }
	inline DateTime_t693205669 * get_address_of_launchedTime_18() { return &___launchedTime_18; }
	inline void set_launchedTime_18(DateTime_t693205669  value)
	{
		___launchedTime_18 = value;
	}

	inline static int32_t get_offset_of_arriveTime_19() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___arriveTime_19)); }
	inline DateTime_t693205669  get_arriveTime_19() const { return ___arriveTime_19; }
	inline DateTime_t693205669 * get_address_of_arriveTime_19() { return &___arriveTime_19; }
	inline void set_arriveTime_19(DateTime_t693205669  value)
	{
		___arriveTime_19 = value;
	}

	inline static int32_t get_offset_of_visibleTime_20() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___visibleTime_20)); }
	inline DateTime_t693205669  get_visibleTime_20() const { return ___visibleTime_20; }
	inline DateTime_t693205669 * get_address_of_visibleTime_20() { return &___visibleTime_20; }
	inline void set_visibleTime_20(DateTime_t693205669  value)
	{
		___visibleTime_20 = value;
	}

	inline static int32_t get_offset_of_defenderId_21() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___defenderId_21)); }
	inline String_t* get_defenderId_21() const { return ___defenderId_21; }
	inline String_t** get_address_of_defenderId_21() { return &___defenderId_21; }
	inline void set_defenderId_21(String_t* value)
	{
		___defenderId_21 = value;
		Il2CppCodeGenWriteBarrier(&___defenderId_21, value);
	}

	inline static int32_t get_offset_of_friend_22() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___friend_22)); }
	inline Friend_t3555014108 * get_friend_22() const { return ___friend_22; }
	inline Friend_t3555014108 ** get_address_of_friend_22() { return &___friend_22; }
	inline void set_friend_22(Friend_t3555014108 * value)
	{
		___friend_22 = value;
		Il2CppCodeGenWriteBarrier(&___friend_22, value);
	}

	inline static int32_t get_offset_of_startLocation_23() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___startLocation_23)); }
	inline LocationInfo_t1364725149  get_startLocation_23() const { return ___startLocation_23; }
	inline LocationInfo_t1364725149 * get_address_of_startLocation_23() { return &___startLocation_23; }
	inline void set_startLocation_23(LocationInfo_t1364725149  value)
	{
		___startLocation_23 = value;
	}

	inline static int32_t get_offset_of_completeTime_24() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___completeTime_24)); }
	inline DateTime_t693205669  get_completeTime_24() const { return ___completeTime_24; }
	inline DateTime_t693205669 * get_address_of_completeTime_24() { return &___completeTime_24; }
	inline void set_completeTime_24(DateTime_t693205669  value)
	{
		___completeTime_24 = value;
	}

	inline static int32_t get_offset_of_maxResultPoints_25() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___maxResultPoints_25)); }
	inline float get_maxResultPoints_25() const { return ___maxResultPoints_25; }
	inline float* get_address_of_maxResultPoints_25() { return &___maxResultPoints_25; }
	inline void set_maxResultPoints_25(float value)
	{
		___maxResultPoints_25 = value;
	}

	inline static int32_t get_offset_of_resultPoints_26() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___resultPoints_26)); }
	inline int32_t get_resultPoints_26() const { return ___resultPoints_26; }
	inline int32_t* get_address_of_resultPoints_26() { return &___resultPoints_26; }
	inline void set_resultPoints_26(int32_t value)
	{
		___resultPoints_26 = value;
	}

	inline static int32_t get_offset_of_resultAwardTo_27() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___resultAwardTo_27)); }
	inline int32_t get_resultAwardTo_27() const { return ___resultAwardTo_27; }
	inline int32_t* get_address_of_resultAwardTo_27() { return &___resultAwardTo_27; }
	inline void set_resultAwardTo_27(int32_t value)
	{
		___resultAwardTo_27 = value;
	}

	inline static int32_t get_offset_of_defendAttempted_28() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___defendAttempted_28)); }
	inline bool get_defendAttempted_28() const { return ___defendAttempted_28; }
	inline bool* get_address_of_defendAttempted_28() { return &___defendAttempted_28; }
	inline void set_defendAttempted_28(bool value)
	{
		___defendAttempted_28 = value;
	}

	inline static int32_t get_offset_of_defendAttemptCount_29() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___defendAttemptCount_29)); }
	inline int32_t get_defendAttemptCount_29() const { return ___defendAttemptCount_29; }
	inline int32_t* get_address_of_defendAttemptCount_29() { return &___defendAttemptCount_29; }
	inline void set_defendAttemptCount_29(int32_t value)
	{
		___defendAttemptCount_29 = value;
	}

	inline static int32_t get_offset_of_defendedTime_30() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___defendedTime_30)); }
	inline DateTime_t693205669  get_defendedTime_30() const { return ___defendedTime_30; }
	inline DateTime_t693205669 * get_address_of_defendedTime_30() { return &___defendedTime_30; }
	inline void set_defendedTime_30(DateTime_t693205669  value)
	{
		___defendedTime_30 = value;
	}

	inline static int32_t get_offset_of_defended_31() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___defended_31)); }
	inline bool get_defended_31() const { return ___defended_31; }
	inline bool* get_address_of_defended_31() { return &___defended_31; }
	inline void set_defended_31(bool value)
	{
		___defended_31 = value;
	}

	inline static int32_t get_offset_of_defendTimeString_32() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___defendTimeString_32)); }
	inline String_t* get_defendTimeString_32() const { return ___defendTimeString_32; }
	inline String_t** get_address_of_defendTimeString_32() { return &___defendTimeString_32; }
	inline void set_defendTimeString_32(String_t* value)
	{
		___defendTimeString_32 = value;
		Il2CppCodeGenWriteBarrier(&___defendTimeString_32, value);
	}

	inline static int32_t get_offset_of_scorezoneDefendedIn_33() { return static_cast<int32_t>(offsetof(LaunchedMissile_t1542515810, ___scorezoneDefendedIn_33)); }
	inline int32_t get_scorezoneDefendedIn_33() const { return ___scorezoneDefendedIn_33; }
	inline int32_t* get_address_of_scorezoneDefendedIn_33() { return &___scorezoneDefendedIn_33; }
	inline void set_scorezoneDefendedIn_33(int32_t value)
	{
		___scorezoneDefendedIn_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
