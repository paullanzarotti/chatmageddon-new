﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.Facebook
struct Facebook_t3852591386;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Action`2<System.String,System.Object>
struct Action_2_t599803691;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Collections.Generic.IEnumerable`1<Prime31.FacebookBatchRequest>
struct IEnumerable_1_t2468319148;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Action`2<System.String,Prime31.FacebookMeResult>
struct Action_2_t2393018193;
// System.Action`2<System.String,Prime31.FacebookFriendsResult>
struct Action_2_t2665012870;
// System.Action`1<System.Nullable`1<System.DateTime>>
struct Action_1_t3053038662;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Action_2_t3603663057;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "P31RestKit_Prime31_HTTPVerb1395544859.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Void Prime31.Facebook::.ctor()
extern "C"  void Facebook__ctor_m3406851204 (Facebook_t3852591386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Prime31.Facebook Prime31.Facebook::get_instance()
extern "C"  Facebook_t3852591386 * Facebook_get_instance_m2007010298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Prime31.Facebook::send(System.String,Prime31.HTTPVerb,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Action`2<System.String,System.Object>)
extern "C"  Il2CppObject * Facebook_send_m1775062534 (Facebook_t3852591386 * __this, String_t* ___path0, int32_t ___httpVerb1, Dictionary_2_t309261261 * ___parameters2, Action_2_t599803691 * ___onComplete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Prime31.Facebook::getHeadersFromForm(UnityEngine.WWWForm)
extern "C"  Il2CppObject * Facebook_getHeadersFromForm_m2396574222 (Facebook_t3852591386 * __this, WWWForm_t3950226929 * ___form0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.Facebook::shouldSendRequest()
extern "C"  bool Facebook_shouldSendRequest_m3461037588 (Facebook_t3852591386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::prepareForMetroUse(UnityEngine.GameObject,UnityEngine.MonoBehaviour)
extern "C"  void Facebook_prepareForMetroUse_m4212963807 (Facebook_t3852591386 * __this, GameObject_t1756533147 * ___go0, MonoBehaviour_t1158329972 * ___mb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::graphRequest(System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_graphRequest_m84754454 (Facebook_t3852591386 * __this, String_t* ___path0, Action_2_t599803691 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::graphRequest(System.String,Prime31.HTTPVerb,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_graphRequest_m1355371560 (Facebook_t3852591386 * __this, String_t* ___path0, int32_t ___verb1, Action_2_t599803691 * ___completionHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::graphRequest(System.String,Prime31.HTTPVerb,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_graphRequest_m2306350215 (Facebook_t3852591386 * __this, String_t* ___path0, int32_t ___verb1, Dictionary_2_t309261261 * ___parameters2, Action_2_t599803691 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::graphRequestBatch(System.Collections.Generic.IEnumerable`1<Prime31.FacebookBatchRequest>,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_graphRequestBatch_m1750699711 (Facebook_t3852591386 * __this, Il2CppObject* ___requests0, Action_2_t599803691 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::fetchProfileImageForUserId(System.String,System.Action`1<UnityEngine.Texture2D>)
extern "C"  void Facebook_fetchProfileImageForUserId_m3363642055 (Facebook_t3852591386 * __this, String_t* ___userId0, Action_1_t3344795111 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Prime31.Facebook::fetchImageAtUrl(System.String,System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * Facebook_fetchImageAtUrl_m2206099527 (Facebook_t3852591386 * __this, String_t* ___url0, Action_1_t3344795111 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::postMessage(System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_postMessage_m4134237144 (Facebook_t3852591386 * __this, String_t* ___message0, Action_2_t599803691 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::postMessageWithLink(System.String,System.String,System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_postMessageWithLink_m1232139476 (Facebook_t3852591386 * __this, String_t* ___message0, String_t* ___link1, String_t* ___linkName2, Action_2_t599803691 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::postMessageWithLinkAndLinkToImage(System.String,System.String,System.String,System.String,System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_postMessageWithLinkAndLinkToImage_m4106188991 (Facebook_t3852591386 * __this, String_t* ___message0, String_t* ___link1, String_t* ___linkName2, String_t* ___linkToImage3, String_t* ___caption4, Action_2_t599803691 * ___completionHandler5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::postImage(System.Byte[],System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_postImage_m4220276817 (Facebook_t3852591386 * __this, ByteU5BU5D_t3397334013* ___image0, String_t* ___message1, Action_2_t599803691 * ___completionHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::postImageToAlbum(System.Byte[],System.String,System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_postImageToAlbum_m3158229855 (Facebook_t3852591386 * __this, ByteU5BU5D_t3397334013* ___image0, String_t* ___caption1, String_t* ___albumId2, Action_2_t599803691 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::getMe(System.Action`2<System.String,Prime31.FacebookMeResult>)
extern "C"  void Facebook_getMe_m3526235565 (Facebook_t3852591386 * __this, Action_2_t2393018193 * ___completionHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::getFriends(System.Action`2<System.String,Prime31.FacebookFriendsResult>)
extern "C"  void Facebook_getFriends_m2090908585 (Facebook_t3852591386 * __this, Action_2_t2665012870 * ___completionHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::extendAccessToken(System.String,System.String,System.Action`1<System.Nullable`1<System.DateTime>>)
extern "C"  void Facebook_extendAccessToken_m3302352454 (Facebook_t3852591386 * __this, String_t* ___appId0, String_t* ___appSecret1, Action_1_t3053038662 * ___completionHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::checkSessionValidityOnServer(System.Action`1<System.Boolean>)
extern "C"  void Facebook_checkSessionValidityOnServer_m2387782153 (Facebook_t3852591386 * __this, Action_1_t3627374100 * ___completionHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::getSessionPermissionsOnServer(System.Action`2<System.String,System.Collections.Generic.List`1<System.String>>)
extern "C"  void Facebook_getSessionPermissionsOnServer_m3849489237 (Facebook_t3852591386 * __this, Action_2_t3603663057 * ___completionHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::getAppAccessToken(System.String,System.String,System.Action`1<System.String>)
extern "C"  void Facebook_getAppAccessToken_m239788336 (Facebook_t3852591386 * __this, String_t* ___appId0, String_t* ___appSecret1, Action_1_t1831019615 * ___completionHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::postScore(System.Int32,System.Action`1<System.Boolean>)
extern "C"  void Facebook_postScore_m3637822332 (Facebook_t3852591386 * __this, int32_t ___score0, Action_1_t3627374100 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::getScores(System.String,System.Action`2<System.String,System.Object>)
extern "C"  void Facebook_getScores_m1467754986 (Facebook_t3852591386 * __this, String_t* ___userId0, Action_2_t599803691 * ___onComplete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook::.cctor()
extern "C"  void Facebook__cctor_m2451234677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
