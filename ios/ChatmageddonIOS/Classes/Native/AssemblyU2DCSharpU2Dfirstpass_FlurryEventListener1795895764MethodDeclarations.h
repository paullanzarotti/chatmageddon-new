﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryEventListener
struct FlurryEventListener_t1795895764;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FlurryEventListener::.ctor()
extern "C"  void FlurryEventListener__ctor_m1011700661 (FlurryEventListener_t1795895764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::OnEnable()
extern "C"  void FlurryEventListener_OnEnable_m3551656221 (FlurryEventListener_t1795895764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::OnDisable()
extern "C"  void FlurryEventListener_OnDisable_m2214339046 (FlurryEventListener_t1795895764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::spaceDidDismissEvent(System.String)
extern "C"  void FlurryEventListener_spaceDidDismissEvent_m2075629444 (FlurryEventListener_t1795895764 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::spaceWillLeaveApplicationEvent(System.String)
extern "C"  void FlurryEventListener_spaceWillLeaveApplicationEvent_m1634780848 (FlurryEventListener_t1795895764 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::spaceDidFailToRenderEvent(System.String)
extern "C"  void FlurryEventListener_spaceDidFailToRenderEvent_m1660991629 (FlurryEventListener_t1795895764 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::spaceDidReceiveAdEvent(System.String)
extern "C"  void FlurryEventListener_spaceDidReceiveAdEvent_m135393256 (FlurryEventListener_t1795895764 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::spaceDidFailToReceiveAdEvent(System.String)
extern "C"  void FlurryEventListener_spaceDidFailToReceiveAdEvent_m874721249 (FlurryEventListener_t1795895764 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryEventListener::videoDidFinishEvent(System.String)
extern "C"  void FlurryEventListener_videoDidFinishEvent_m4189984754 (FlurryEventListener_t1795895764 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
