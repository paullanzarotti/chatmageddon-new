﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeConstructor
struct CodeConstructor_t2209283105;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeConstructor::.ctor()
extern "C"  void CodeConstructor__ctor_m557571861 (CodeConstructor_t2209283105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
