﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumSwitch`1<System.Object>
struct  EnumSwitch_1_t1692272294  : public MonoBehaviour_t1158329972
{
public:
	// SwitchState EnumSwitch`1::currentState
	Il2CppObject * ___currentState_2;

public:
	inline static int32_t get_offset_of_currentState_2() { return static_cast<int32_t>(offsetof(EnumSwitch_1_t1692272294, ___currentState_2)); }
	inline Il2CppObject * get_currentState_2() const { return ___currentState_2; }
	inline Il2CppObject ** get_address_of_currentState_2() { return &___currentState_2; }
	inline void set_currentState_2(Il2CppObject * value)
	{
		___currentState_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentState_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
