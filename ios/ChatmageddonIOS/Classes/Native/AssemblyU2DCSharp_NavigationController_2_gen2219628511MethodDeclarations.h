﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,AttackNavScreen>
struct NavigationController_2_t2219628511;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,AttackNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2423512526_gshared (NavigationController_2_t2219628511 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m2423512526(__this, method) ((  void (*) (NavigationController_2_t2219628511 *, const MethodInfo*))NavigationController_2__ctor_m2423512526_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,AttackNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1224047836_gshared (NavigationController_2_t2219628511 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1224047836(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2219628511 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1224047836_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,AttackNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m312182917_gshared (NavigationController_2_t2219628511 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m312182917(__this, method) ((  void (*) (NavigationController_2_t2219628511 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m312182917_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,AttackNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1564361018_gshared (NavigationController_2_t2219628511 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m1564361018(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2219628511 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1564361018_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,AttackNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m3710279390_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m3710279390(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2219628511 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m3710279390_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,AttackNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m775835947_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m775835947(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2219628511 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m775835947_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,AttackNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m2565550832_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m2565550832(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2219628511 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m2565550832_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,AttackNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m1108255294_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m1108255294(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2219628511 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m1108255294_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,AttackNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m236222014_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m236222014(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2219628511 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m236222014_gshared)(__this, ___screen0, method)
