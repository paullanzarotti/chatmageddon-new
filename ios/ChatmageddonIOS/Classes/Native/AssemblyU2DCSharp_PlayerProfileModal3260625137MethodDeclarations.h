﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileModal
struct PlayerProfileModal_t3260625137;
// User
struct User_t719925459;
// System.Collections.Generic.List`1<UISprite>
struct List_1_t4267705163;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayerProfileModal::.ctor()
extern "C"  void PlayerProfileModal__ctor_m1559975990 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::LoadUserProfileData(User)
extern "C"  void PlayerProfileModal_LoadUserProfileData_m3440176173 (PlayerProfileModal_t3260625137 * __this, User_t719925459 * ___profileUser0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::UpdateProfile()
extern "C"  void PlayerProfileModal_UpdateProfile_m2200646896 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::UpdateAvatar()
extern "C"  void PlayerProfileModal_UpdateAvatar_m3903136612 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::SetStealthStatus()
extern "C"  void PlayerProfileModal_SetStealthStatus_m1331136769 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::CheckUserBlockedStatus()
extern "C"  void PlayerProfileModal_CheckUserBlockedStatus_m2701161691 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::SwitchProfileUpdated()
extern "C"  void PlayerProfileModal_SwitchProfileUpdated_m3213345052 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::OnUIDeactivate()
extern "C"  void PlayerProfileModal_OnUIDeactivate_m1296436273 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::OnUICleared()
extern "C"  void PlayerProfileModal_OnUICleared_m3823317989 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::SendFriendRequest()
extern "C"  void PlayerProfileModal_SendFriendRequest_m4089970807 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::AcceptFriendRequest()
extern "C"  void PlayerProfileModal_AcceptFriendRequest_m2685214375 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::DeclineFriendRequest()
extern "C"  void PlayerProfileModal_DeclineFriendRequest_m3147866519 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::FriendProfileUpdate()
extern "C"  void PlayerProfileModal_FriendProfileUpdate_m3386536726 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::OutgoingFriendRequestUpdate(System.Boolean)
extern "C"  void PlayerProfileModal_OutgoingFriendRequestUpdate_m1376181379 (PlayerProfileModal_t3260625137 * __this, bool ___accepted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::IncomingFriendRequestUpdate()
extern "C"  void PlayerProfileModal_IncomingFriendRequestUpdate_m1278855402 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::TweenToFriendProfile(System.Boolean)
extern "C"  void PlayerProfileModal_TweenToFriendProfile_m671000218 (PlayerProfileModal_t3260625137 * __this, bool ___decline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::RequestRemovedTweenFinished()
extern "C"  void PlayerProfileModal_RequestRemovedTweenFinished_m3257969410 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::TweenToFriendRequestProfile()
extern "C"  void PlayerProfileModal_TweenToFriendRequestProfile_m1992441350 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::RequestAddedTweenFinished()
extern "C"  void PlayerProfileModal_RequestAddedTweenFinished_m2525785364 (PlayerProfileModal_t3260625137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::SetUISceneSprites(System.Collections.Generic.List`1<UISprite>)
extern "C"  void PlayerProfileModal_SetUISceneSprites_m3895718037 (PlayerProfileModal_t3260625137 * __this, List_1_t4267705163 * ___spriteList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal::<LoadUserProfileData>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void PlayerProfileModal_U3CLoadUserProfileDataU3Em__0_m1252736564 (PlayerProfileModal_t3260625137 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
