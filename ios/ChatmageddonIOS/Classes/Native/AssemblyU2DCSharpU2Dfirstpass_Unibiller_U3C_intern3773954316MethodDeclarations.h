﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibiller/<_internal_hook_events>c__AnonStorey0
struct U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibiller/<_internal_hook_events>c__AnonStorey0::.ctor()
extern "C"  void U3C_internal_hook_eventsU3Ec__AnonStorey0__ctor_m1476766237 (U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller/<_internal_hook_events>c__AnonStorey0::<>m__0(System.Boolean)
extern "C"  void U3C_internal_hook_eventsU3Ec__AnonStorey0_U3CU3Em__0_m3675137421 (U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller/<_internal_hook_events>c__AnonStorey0::<>m__1(System.String,System.String)
extern "C"  void U3C_internal_hook_eventsU3Ec__AnonStorey0_U3CU3Em__1_m1784970131 (Il2CppObject * __this /* static, unused */, String_t* ___item0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
