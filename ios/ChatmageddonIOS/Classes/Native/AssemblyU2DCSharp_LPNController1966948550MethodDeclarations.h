﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LPNController
struct LPNController_t1966948550;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;

#include "codegen/il2cpp-codegen.h"

// System.Void LPNController::.ctor()
extern "C"  void LPNController__ctor_m4174698733 (LPNController_t1966948550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LPNController::Start()
extern "C"  void LPNController_Start_m602166285 (LPNController_t1966948550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LPNController::SetupFixture()
extern "C"  void LPNController_SetupFixture_m1864084143 (LPNController_t1966948550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil LPNController::InitializePhoneUtilForTesting()
extern "C"  PhoneNumberUtil_t4155573397 * LPNController_InitializePhoneUtilForTesting_m127972870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LPNController::TryValidation()
extern "C"  void LPNController_TryValidation_m4260332149 (LPNController_t1966948550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LPNController::.cctor()
extern "C"  void LPNController__cctor_m1573771464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
