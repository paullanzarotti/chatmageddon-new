﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.BindingList`1<System.Object>
struct BindingList_1_t3356767716;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.ComponentModel.AddingNewEventHandler
struct AddingNewEventHandler_t1821432365;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t2276411942;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t4250402154;
// System.Object
struct Il2CppObject;
// System.ComponentModel.AddingNewEventArgs
struct AddingNewEventArgs_t3938289828;
// System.ComponentModel.ListChangedEventArgs
struct ListChangedEventArgs_t3132270315;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_AddingNewEventHandler1821432365.h"
#include "System_System_ComponentModel_ListChangedEventHandl2276411942.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_ListSortDirection4186912589.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_ComponentModel_AddingNewEventArgs3938289828.h"
#include "System_System_ComponentModel_ListChangedEventArgs3132270315.h"

// System.Void System.ComponentModel.BindingList`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void BindingList_1__ctor_m355976882_gshared (BindingList_1_t3356767716 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define BindingList_1__ctor_m355976882(__this, ___list0, method) ((  void (*) (BindingList_1_t3356767716 *, Il2CppObject*, const MethodInfo*))BindingList_1__ctor_m355976882_gshared)(__this, ___list0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::.ctor()
extern "C"  void BindingList_1__ctor_m3274776741_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1__ctor_m3274776741(__this, method) ((  void (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1__ctor_m3274776741_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::add_AddingNew(System.ComponentModel.AddingNewEventHandler)
extern "C"  void BindingList_1_add_AddingNew_m3057814562_gshared (BindingList_1_t3356767716 * __this, AddingNewEventHandler_t1821432365 * ___value0, const MethodInfo* method);
#define BindingList_1_add_AddingNew_m3057814562(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, AddingNewEventHandler_t1821432365 *, const MethodInfo*))BindingList_1_add_AddingNew_m3057814562_gshared)(__this, ___value0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::remove_AddingNew(System.ComponentModel.AddingNewEventHandler)
extern "C"  void BindingList_1_remove_AddingNew_m4213243661_gshared (BindingList_1_t3356767716 * __this, AddingNewEventHandler_t1821432365 * ___value0, const MethodInfo* method);
#define BindingList_1_remove_AddingNew_m4213243661(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, AddingNewEventHandler_t1821432365 *, const MethodInfo*))BindingList_1_remove_AddingNew_m4213243661_gshared)(__this, ___value0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::add_ListChanged(System.ComponentModel.ListChangedEventHandler)
extern "C"  void BindingList_1_add_ListChanged_m121556610_gshared (BindingList_1_t3356767716 * __this, ListChangedEventHandler_t2276411942 * ___value0, const MethodInfo* method);
#define BindingList_1_add_ListChanged_m121556610(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, ListChangedEventHandler_t2276411942 *, const MethodInfo*))BindingList_1_add_ListChanged_m121556610_gshared)(__this, ___value0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::remove_ListChanged(System.ComponentModel.ListChangedEventHandler)
extern "C"  void BindingList_1_remove_ListChanged_m1808337325_gshared (BindingList_1_t3356767716 * __this, ListChangedEventHandler_t2276411942 * ___value0, const MethodInfo* method);
#define BindingList_1_remove_ListChanged_m1808337325(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, ListChangedEventHandler_t2276411942 *, const MethodInfo*))BindingList_1_remove_ListChanged_m1808337325_gshared)(__this, ___value0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.AddIndex(System.ComponentModel.PropertyDescriptor)
extern "C"  void BindingList_1_System_ComponentModel_IBindingList_AddIndex_m3441689520_gshared (BindingList_1_t3356767716 * __this, PropertyDescriptor_t4250402154 * ___index0, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_AddIndex_m3441689520(__this, ___index0, method) ((  void (*) (BindingList_1_t3356767716 *, PropertyDescriptor_t4250402154 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_AddIndex_m3441689520_gshared)(__this, ___index0, method)
// System.Object System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.AddNew()
extern "C"  Il2CppObject * BindingList_1_System_ComponentModel_IBindingList_AddNew_m502692828_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_AddNew_m502692828(__this, method) ((  Il2CppObject * (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_AddNew_m502692828_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.ApplySort(System.ComponentModel.PropertyDescriptor,System.ComponentModel.ListSortDirection)
extern "C"  void BindingList_1_System_ComponentModel_IBindingList_ApplySort_m3837916391_gshared (BindingList_1_t3356767716 * __this, PropertyDescriptor_t4250402154 * ___property0, int32_t ___direction1, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_ApplySort_m3837916391(__this, ___property0, ___direction1, method) ((  void (*) (BindingList_1_t3356767716 *, PropertyDescriptor_t4250402154 *, int32_t, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_ApplySort_m3837916391_gshared)(__this, ___property0, ___direction1, method)
// System.Int32 System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.Find(System.ComponentModel.PropertyDescriptor,System.Object)
extern "C"  int32_t BindingList_1_System_ComponentModel_IBindingList_Find_m2753941184_gshared (BindingList_1_t3356767716 * __this, PropertyDescriptor_t4250402154 * ___property0, Il2CppObject * ___key1, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_Find_m2753941184(__this, ___property0, ___key1, method) ((  int32_t (*) (BindingList_1_t3356767716 *, PropertyDescriptor_t4250402154 *, Il2CppObject *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_Find_m2753941184_gshared)(__this, ___property0, ___key1, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.RemoveIndex(System.ComponentModel.PropertyDescriptor)
extern "C"  void BindingList_1_System_ComponentModel_IBindingList_RemoveIndex_m1631857977_gshared (BindingList_1_t3356767716 * __this, PropertyDescriptor_t4250402154 * ___property0, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_RemoveIndex_m1631857977(__this, ___property0, method) ((  void (*) (BindingList_1_t3356767716 *, PropertyDescriptor_t4250402154 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_RemoveIndex_m1631857977_gshared)(__this, ___property0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.RemoveSort()
extern "C"  void BindingList_1_System_ComponentModel_IBindingList_RemoveSort_m3417188930_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_RemoveSort_m3417188930(__this, method) ((  void (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_RemoveSort_m3417188930_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_IsSorted()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_IsSorted_m213545022_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_IsSorted_m213545022(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_IsSorted_m213545022_gshared)(__this, method)
// System.ComponentModel.ListSortDirection System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_SortDirection()
extern "C"  int32_t BindingList_1_System_ComponentModel_IBindingList_get_SortDirection_m672532041_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_SortDirection_m672532041(__this, method) ((  int32_t (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_SortDirection_m672532041_gshared)(__this, method)
// System.ComponentModel.PropertyDescriptor System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_SortProperty()
extern "C"  PropertyDescriptor_t4250402154 * BindingList_1_System_ComponentModel_IBindingList_get_SortProperty_m3807273828_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_SortProperty_m3807273828(__this, method) ((  PropertyDescriptor_t4250402154 * (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_SortProperty_m3807273828_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_AllowEdit()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_AllowEdit_m3861063710_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_AllowEdit_m3861063710(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_AllowEdit_m3861063710_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_AllowNew()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_AllowNew_m582530806_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_AllowNew_m582530806(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_AllowNew_m582530806_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_AllowRemove()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_AllowRemove_m3736593282_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_AllowRemove_m3736593282(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_AllowRemove_m3736593282_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_SupportsChangeNotification()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_SupportsChangeNotification_m3078684920_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_SupportsChangeNotification_m3078684920(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_SupportsChangeNotification_m3078684920_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_SupportsSearching()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_SupportsSearching_m1576522655_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_SupportsSearching_m1576522655(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_SupportsSearching_m1576522655_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IBindingList.get_SupportsSorting()
extern "C"  bool BindingList_1_System_ComponentModel_IBindingList_get_SupportsSorting_m1531920851_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IBindingList_get_SupportsSorting_m1531920851(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IBindingList_get_SupportsSorting_m1531920851_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::System.ComponentModel.IRaiseItemChangedEvents.get_RaisesItemChangedEvents()
extern "C"  bool BindingList_1_System_ComponentModel_IRaiseItemChangedEvents_get_RaisesItemChangedEvents_m1844839073_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_System_ComponentModel_IRaiseItemChangedEvents_get_RaisesItemChangedEvents_m1844839073(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_System_ComponentModel_IRaiseItemChangedEvents_get_RaisesItemChangedEvents_m1844839073_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::CheckType()
extern "C"  void BindingList_1_CheckType_m2372268731_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_CheckType_m2372268731(__this, method) ((  void (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_CheckType_m2372268731_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_AllowEdit()
extern "C"  bool BindingList_1_get_AllowEdit_m289293953_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_AllowEdit_m289293953(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_AllowEdit_m289293953_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::set_AllowEdit(System.Boolean)
extern "C"  void BindingList_1_set_AllowEdit_m641178496_gshared (BindingList_1_t3356767716 * __this, bool ___value0, const MethodInfo* method);
#define BindingList_1_set_AllowEdit_m641178496(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, bool, const MethodInfo*))BindingList_1_set_AllowEdit_m641178496_gshared)(__this, ___value0, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_AllowNew()
extern "C"  bool BindingList_1_get_AllowNew_m665127049_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_AllowNew_m665127049(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_AllowNew_m665127049_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::set_AllowNew(System.Boolean)
extern "C"  void BindingList_1_set_AllowNew_m2892621202_gshared (BindingList_1_t3356767716 * __this, bool ___value0, const MethodInfo* method);
#define BindingList_1_set_AllowNew_m2892621202(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, bool, const MethodInfo*))BindingList_1_set_AllowNew_m2892621202_gshared)(__this, ___value0, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_AllowRemove()
extern "C"  bool BindingList_1_get_AllowRemove_m1537533723_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_AllowRemove_m1537533723(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_AllowRemove_m1537533723_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::set_AllowRemove(System.Boolean)
extern "C"  void BindingList_1_set_AllowRemove_m1163525364_gshared (BindingList_1_t3356767716 * __this, bool ___value0, const MethodInfo* method);
#define BindingList_1_set_AllowRemove_m1163525364(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, bool, const MethodInfo*))BindingList_1_set_AllowRemove_m1163525364_gshared)(__this, ___value0, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_IsSortedCore()
extern "C"  bool BindingList_1_get_IsSortedCore_m2900219428_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_IsSortedCore_m2900219428(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_IsSortedCore_m2900219428_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_RaiseListChangedEvents()
extern "C"  bool BindingList_1_get_RaiseListChangedEvents_m782766791_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_RaiseListChangedEvents_m782766791(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_RaiseListChangedEvents_m782766791_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::set_RaiseListChangedEvents(System.Boolean)
extern "C"  void BindingList_1_set_RaiseListChangedEvents_m714035466_gshared (BindingList_1_t3356767716 * __this, bool ___value0, const MethodInfo* method);
#define BindingList_1_set_RaiseListChangedEvents_m714035466(__this, ___value0, method) ((  void (*) (BindingList_1_t3356767716 *, bool, const MethodInfo*))BindingList_1_set_RaiseListChangedEvents_m714035466_gshared)(__this, ___value0, method)
// System.ComponentModel.ListSortDirection System.ComponentModel.BindingList`1<System.Object>::get_SortDirectionCore()
extern "C"  int32_t BindingList_1_get_SortDirectionCore_m1518999239_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_SortDirectionCore_m1518999239(__this, method) ((  int32_t (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_SortDirectionCore_m1518999239_gshared)(__this, method)
// System.ComponentModel.PropertyDescriptor System.ComponentModel.BindingList`1<System.Object>::get_SortPropertyCore()
extern "C"  PropertyDescriptor_t4250402154 * BindingList_1_get_SortPropertyCore_m719722394_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_SortPropertyCore_m719722394(__this, method) ((  PropertyDescriptor_t4250402154 * (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_SortPropertyCore_m719722394_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_SupportsChangeNotificationCore()
extern "C"  bool BindingList_1_get_SupportsChangeNotificationCore_m3764776846_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_SupportsChangeNotificationCore_m3764776846(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_SupportsChangeNotificationCore_m3764776846_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_SupportsSearchingCore()
extern "C"  bool BindingList_1_get_SupportsSearchingCore_m488111589_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_SupportsSearchingCore_m488111589(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_SupportsSearchingCore_m488111589_gshared)(__this, method)
// System.Boolean System.ComponentModel.BindingList`1<System.Object>::get_SupportsSortingCore()
extern "C"  bool BindingList_1_get_SupportsSortingCore_m1317005313_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_get_SupportsSortingCore_m1317005313(__this, method) ((  bool (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_get_SupportsSortingCore_m1317005313_gshared)(__this, method)
// T System.ComponentModel.BindingList`1<System.Object>::AddNew()
extern "C"  Il2CppObject * BindingList_1_AddNew_m2865638651_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_AddNew_m2865638651(__this, method) ((  Il2CppObject * (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_AddNew_m2865638651_gshared)(__this, method)
// System.Object System.ComponentModel.BindingList`1<System.Object>::AddNewCore()
extern "C"  Il2CppObject * BindingList_1_AddNewCore_m4165954568_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_AddNewCore_m4165954568(__this, method) ((  Il2CppObject * (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_AddNewCore_m4165954568_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::ApplySortCore(System.ComponentModel.PropertyDescriptor,System.ComponentModel.ListSortDirection)
extern "C"  void BindingList_1_ApplySortCore_m3211801685_gshared (BindingList_1_t3356767716 * __this, PropertyDescriptor_t4250402154 * ___prop0, int32_t ___direction1, const MethodInfo* method);
#define BindingList_1_ApplySortCore_m3211801685(__this, ___prop0, ___direction1, method) ((  void (*) (BindingList_1_t3356767716 *, PropertyDescriptor_t4250402154 *, int32_t, const MethodInfo*))BindingList_1_ApplySortCore_m3211801685_gshared)(__this, ___prop0, ___direction1, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::CancelNew(System.Int32)
extern "C"  void BindingList_1_CancelNew_m2039098894_gshared (BindingList_1_t3356767716 * __this, int32_t ___itemIndex0, const MethodInfo* method);
#define BindingList_1_CancelNew_m2039098894(__this, ___itemIndex0, method) ((  void (*) (BindingList_1_t3356767716 *, int32_t, const MethodInfo*))BindingList_1_CancelNew_m2039098894_gshared)(__this, ___itemIndex0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::ClearItems()
extern "C"  void BindingList_1_ClearItems_m4061802040_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_ClearItems_m4061802040(__this, method) ((  void (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_ClearItems_m4061802040_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::EndNew(System.Int32)
extern "C"  void BindingList_1_EndNew_m2034078043_gshared (BindingList_1_t3356767716 * __this, int32_t ___itemIndex0, const MethodInfo* method);
#define BindingList_1_EndNew_m2034078043(__this, ___itemIndex0, method) ((  void (*) (BindingList_1_t3356767716 *, int32_t, const MethodInfo*))BindingList_1_EndNew_m2034078043_gshared)(__this, ___itemIndex0, method)
// System.Int32 System.ComponentModel.BindingList`1<System.Object>::FindCore(System.ComponentModel.PropertyDescriptor,System.Object)
extern "C"  int32_t BindingList_1_FindCore_m1068564430_gshared (BindingList_1_t3356767716 * __this, PropertyDescriptor_t4250402154 * ___prop0, Il2CppObject * ___key1, const MethodInfo* method);
#define BindingList_1_FindCore_m1068564430(__this, ___prop0, ___key1, method) ((  int32_t (*) (BindingList_1_t3356767716 *, PropertyDescriptor_t4250402154 *, Il2CppObject *, const MethodInfo*))BindingList_1_FindCore_m1068564430_gshared)(__this, ___prop0, ___key1, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void BindingList_1_InsertItem_m2866800708_gshared (BindingList_1_t3356767716 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define BindingList_1_InsertItem_m2866800708(__this, ___index0, ___item1, method) ((  void (*) (BindingList_1_t3356767716 *, int32_t, Il2CppObject *, const MethodInfo*))BindingList_1_InsertItem_m2866800708_gshared)(__this, ___index0, ___item1, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::OnAddingNew(System.ComponentModel.AddingNewEventArgs)
extern "C"  void BindingList_1_OnAddingNew_m1960680624_gshared (BindingList_1_t3356767716 * __this, AddingNewEventArgs_t3938289828 * ___e0, const MethodInfo* method);
#define BindingList_1_OnAddingNew_m1960680624(__this, ___e0, method) ((  void (*) (BindingList_1_t3356767716 *, AddingNewEventArgs_t3938289828 *, const MethodInfo*))BindingList_1_OnAddingNew_m1960680624_gshared)(__this, ___e0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::OnListChanged(System.ComponentModel.ListChangedEventArgs)
extern "C"  void BindingList_1_OnListChanged_m814509712_gshared (BindingList_1_t3356767716 * __this, ListChangedEventArgs_t3132270315 * ___e0, const MethodInfo* method);
#define BindingList_1_OnListChanged_m814509712(__this, ___e0, method) ((  void (*) (BindingList_1_t3356767716 *, ListChangedEventArgs_t3132270315 *, const MethodInfo*))BindingList_1_OnListChanged_m814509712_gshared)(__this, ___e0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void BindingList_1_RemoveItem_m2975138967_gshared (BindingList_1_t3356767716 * __this, int32_t ___index0, const MethodInfo* method);
#define BindingList_1_RemoveItem_m2975138967(__this, ___index0, method) ((  void (*) (BindingList_1_t3356767716 *, int32_t, const MethodInfo*))BindingList_1_RemoveItem_m2975138967_gshared)(__this, ___index0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::RemoveSortCore()
extern "C"  void BindingList_1_RemoveSortCore_m1486867720_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_RemoveSortCore_m1486867720(__this, method) ((  void (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_RemoveSortCore_m1486867720_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::ResetBindings()
extern "C"  void BindingList_1_ResetBindings_m1318828426_gshared (BindingList_1_t3356767716 * __this, const MethodInfo* method);
#define BindingList_1_ResetBindings_m1318828426(__this, method) ((  void (*) (BindingList_1_t3356767716 *, const MethodInfo*))BindingList_1_ResetBindings_m1318828426_gshared)(__this, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::ResetItem(System.Int32)
extern "C"  void BindingList_1_ResetItem_m2846636526_gshared (BindingList_1_t3356767716 * __this, int32_t ___position0, const MethodInfo* method);
#define BindingList_1_ResetItem_m2846636526(__this, ___position0, method) ((  void (*) (BindingList_1_t3356767716 *, int32_t, const MethodInfo*))BindingList_1_ResetItem_m2846636526_gshared)(__this, ___position0, method)
// System.Void System.ComponentModel.BindingList`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void BindingList_1_SetItem_m2890718975_gshared (BindingList_1_t3356767716 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define BindingList_1_SetItem_m2890718975(__this, ___index0, ___item1, method) ((  void (*) (BindingList_1_t3356767716 *, int32_t, Il2CppObject *, const MethodInfo*))BindingList_1_SetItem_m2890718975_gshared)(__this, ___index0, ___item1, method)
