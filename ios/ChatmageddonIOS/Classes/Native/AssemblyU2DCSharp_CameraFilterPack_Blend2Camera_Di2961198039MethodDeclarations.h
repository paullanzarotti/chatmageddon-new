﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Difference
struct CameraFilterPack_Blend2Camera_Difference_t2961198039;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Difference::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Difference__ctor_m1601174696 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Difference::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Difference_get_material_m4101317611 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Difference_Start_m902716068 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Difference_OnRenderImage_m518415540 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Difference_OnValidate_m1034616433 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Difference_Update_m2169894935 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Difference_OnEnable_m3269045828 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Difference_OnDisable_m2956489191 (CameraFilterPack_Blend2Camera_Difference_t2961198039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
