﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetTouchCallback
struct GetTouchCallback_t2038115210;
// System.Object
struct Il2CppObject;
// UICamera/Touch
struct Touch_t560313529;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICamera/GetTouchCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTouchCallback__ctor_m832037175 (GetTouchCallback_t2038115210 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/Touch UICamera/GetTouchCallback::Invoke(System.Int32)
extern "C"  Touch_t560313529 * GetTouchCallback_Invoke_m72737544 (GetTouchCallback_t2038115210 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICamera/GetTouchCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTouchCallback_BeginInvoke_m3819460805 (GetTouchCallback_t2038115210 * __this, int32_t ___index0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/Touch UICamera/GetTouchCallback::EndInvoke(System.IAsyncResult)
extern "C"  Touch_t560313529 * GetTouchCallback_EndInvoke_m393098297 (GetTouchCallback_t2038115210 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
