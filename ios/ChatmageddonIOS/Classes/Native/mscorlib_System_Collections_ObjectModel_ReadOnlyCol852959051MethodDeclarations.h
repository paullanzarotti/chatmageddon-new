﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>
struct ReadOnlyCollection_1_t852959051;
// System.Collections.Generic.IList`1<RegNavScreen>
struct IList_1_t1208113960;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// RegNavScreen[]
struct RegNavScreenU5BU5D_t3187910006;
// System.Collections.Generic.IEnumerator`1<RegNavScreen>
struct IEnumerator_1_t2437664482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1450101961_gshared (ReadOnlyCollection_1_t852959051 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1450101961(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1450101961_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m390133999_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m390133999(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m390133999_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4229876435_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4229876435(__this, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4229876435_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3526867008_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3526867008(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3526867008_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3264646982_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3264646982(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3264646982_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1661809020_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1661809020(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1661809020_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2595603944_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2595603944(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2595603944_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1622474179_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1622474179(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1622474179_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1100855719_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1100855719(__this, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1100855719_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3297438150_gshared (ReadOnlyCollection_1_t852959051 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3297438150(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3297438150_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1192047315_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1192047315(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1192047315_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1792951418_gshared (ReadOnlyCollection_1_t852959051 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1792951418(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t852959051 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1792951418_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2819172608_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2819172608(__this, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2819172608_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2787335076_gshared (ReadOnlyCollection_1_t852959051 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2787335076(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2787335076_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2746572944_gshared (ReadOnlyCollection_1_t852959051 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2746572944(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t852959051 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2746572944_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m213084915_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m213084915(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m213084915_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1407349667_gshared (ReadOnlyCollection_1_t852959051 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1407349667(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1407349667_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2361533401_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2361533401(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2361533401_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2227142914_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2227142914(__this, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2227142914_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3931632172_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3931632172(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3931632172_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m393212627_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m393212627(__this, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m393212627_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1814315870_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1814315870(__this, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1814315870_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3556767711_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3556767711(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3556767711_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2865418736_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2865418736(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2865418736_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2364546653_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2364546653(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m2364546653_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m639606603_gshared (ReadOnlyCollection_1_t852959051 * __this, RegNavScreenU5BU5D_t3187910006* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m639606603(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t852959051 *, RegNavScreenU5BU5D_t3187910006*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m639606603_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3377768374_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3377768374(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3377768374_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m70705207_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m70705207(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m70705207_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m544913370_gshared (ReadOnlyCollection_1_t852959051 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m544913370(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t852959051 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m544913370_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3289146644_gshared (ReadOnlyCollection_1_t852959051 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3289146644(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t852959051 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3289146644_gshared)(__this, ___index0, method)
