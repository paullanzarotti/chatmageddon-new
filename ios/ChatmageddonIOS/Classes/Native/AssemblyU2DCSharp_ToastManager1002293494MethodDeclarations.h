﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToastManager
struct ToastManager_t1002293494;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

// System.Void ToastManager::.ctor()
extern "C"  void ToastManager__ctor_m3877080113 (ToastManager_t1002293494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::Init()
extern "C"  void ToastManager_Init_m53477719 (ToastManager_t1002293494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::OpenPopUpToast(System.String,System.String,System.String,UnityEngine.Texture,System.Boolean,SFX)
extern "C"  void ToastManager_OpenPopUpToast_m3149388726 (ToastManager_t1002293494 * __this, String_t* ___message0, String_t* ___closeText1, String_t* ___title2, Texture_t2243626319 * ___texture3, bool ___showFade4, int32_t ___sfx5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::OpenSmallToast(System.String)
extern "C"  void ToastManager_OpenSmallToast_m805283205 (ToastManager_t1002293494 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::EatToast(System.Int32)
extern "C"  void ToastManager_EatToast_m2432953073 (ToastManager_t1002293494 * __this, int32_t ___toastID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::TurnToasterOn(System.Boolean)
extern "C"  void ToastManager_TurnToasterOn_m2554345876 (ToastManager_t1002293494 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::CheckActiveToaster()
extern "C"  void ToastManager_CheckActiveToaster_m456929745 (ToastManager_t1002293494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastManager::RemoveToasterFromScene()
extern "C"  void ToastManager_RemoveToasterFromScene_m3510332877 (ToastManager_t1002293494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
