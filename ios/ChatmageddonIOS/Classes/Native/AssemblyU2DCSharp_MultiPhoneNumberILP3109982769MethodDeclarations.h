﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiPhoneNumberILP
struct MultiPhoneNumberILP_t3109982769;
// OtherFriend
struct OtherFriend_t3976068630;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OtherFriend3976068630.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void MultiPhoneNumberILP::.ctor()
extern "C"  void MultiPhoneNumberILP__ctor_m1413363520 (MultiPhoneNumberILP_t3109982769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::Awake()
extern "C"  void MultiPhoneNumberILP_Awake_m1075491297 (MultiPhoneNumberILP_t3109982769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::UIClosing()
extern "C"  void MultiPhoneNumberILP_UIClosing_m2843916675 (MultiPhoneNumberILP_t3109982769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::OpenUI(OtherFriend,System.Boolean)
extern "C"  void MultiPhoneNumberILP_OpenUI_m1407306457 (MultiPhoneNumberILP_t3109982769 * __this, OtherFriend_t3976068630 * ___oFriend0, bool ___attack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::CloseUI()
extern "C"  void MultiPhoneNumberILP_CloseUI_m2453716882 (MultiPhoneNumberILP_t3109982769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::StartIL(OtherFriend)
extern "C"  void MultiPhoneNumberILP_StartIL_m359623575 (MultiPhoneNumberILP_t3109982769 * __this, OtherFriend_t3976068630 * ___oFriend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::SetDataArray(System.Collections.Generic.List`1<System.String>)
extern "C"  void MultiPhoneNumberILP_SetDataArray_m523912671 (MultiPhoneNumberILP_t3109982769 * __this, List_1_t1398341365 * ___numbersList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void MultiPhoneNumberILP_InitListItemWithIndex_m3856454926 (MultiPhoneNumberILP_t3109982769 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void MultiPhoneNumberILP_PopulateListItemWithIndex_m3445778255 (MultiPhoneNumberILP_t3109982769 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
