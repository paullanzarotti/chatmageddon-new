﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Luminosity
struct CameraFilterPack_Blend2Camera_Luminosity_t1256870415;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Luminosity::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity__ctor_m463832398 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Luminosity::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Luminosity_get_material_m196689187 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity_Start_m2285281274 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity_OnRenderImage_m4149773026 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity_OnValidate_m1189149833 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity_Update_m2105192631 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity_OnEnable_m3496032570 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Luminosity_OnDisable_m1908557927 (CameraFilterPack_Blend2Camera_Luminosity_t1256870415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
