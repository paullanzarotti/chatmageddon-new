﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeNavScreen
struct StealthModeNavScreen_t2705902227;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "mscorlib_System_String2029220233.h"

// System.Void StealthModeNavScreen::.ctor()
extern "C"  void StealthModeNavScreen__ctor_m3146609568 (StealthModeNavScreen_t2705902227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::Start()
extern "C"  void StealthModeNavScreen_Start_m583752372 (StealthModeNavScreen_t2705902227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::UIClosing()
extern "C"  void StealthModeNavScreen_UIClosing_m377844301 (StealthModeNavScreen_t2705902227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void StealthModeNavScreen_ScreenLoad_m2858100344 (StealthModeNavScreen_t2705902227 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void StealthModeNavScreen_ScreenUnload_m736399396 (StealthModeNavScreen_t2705902227 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StealthModeNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool StealthModeNavScreen_ValidateScreenNavigation_m1701956097 (StealthModeNavScreen_t2705902227 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StealthModeNavScreen::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * StealthModeNavScreen_SaveScreenContent_m1873953872 (StealthModeNavScreen_t2705902227 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::UpdateEndTime()
extern "C"  void StealthModeNavScreen_UpdateEndTime_m2607548185 (StealthModeNavScreen_t2705902227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::SetEndTime(System.Int32,System.Int32,System.Int32)
extern "C"  void StealthModeNavScreen_SetEndTime_m3161572211 (StealthModeNavScreen_t2705902227 * __this, int32_t ___startHour0, int32_t ___startMin1, int32_t ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen::SetEndTimeLabel(System.String,System.String)
extern "C"  void StealthModeNavScreen_SetEndTimeLabel_m1501555924 (StealthModeNavScreen_t2705902227 * __this, String_t* ___endHour0, String_t* ___endMin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
