﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardNavigateForwardsButton
struct LeaderboardNavigateForwardsButton_t1837924610;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardNavigateForwardsButton::.ctor()
extern "C"  void LeaderboardNavigateForwardsButton__ctor_m3304402003 (LeaderboardNavigateForwardsButton_t1837924610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigateForwardsButton::OnButtonClick()
extern "C"  void LeaderboardNavigateForwardsButton_OnButtonClick_m3406338874 (LeaderboardNavigateForwardsButton_t1837924610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
