﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageKeyboardInput
struct MessageKeyboardInput_t4224908926;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageKeyboardInput::.ctor()
extern "C"  void MessageKeyboardInput__ctor_m1210931295 (MessageKeyboardInput_t4224908926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageKeyboardInput::ClearInput()
extern "C"  void MessageKeyboardInput_ClearInput_m1535865774 (MessageKeyboardInput_t4224908926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageKeyboardInput::OnInputSubmit()
extern "C"  void MessageKeyboardInput_OnInputSubmit_m241233674 (MessageKeyboardInput_t4224908926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
