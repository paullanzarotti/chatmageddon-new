﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonLoadingIndicator
struct ChatmageddonLoadingIndicator_t472672888;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatmageddonLoadingIndicator::.ctor()
extern "C"  void ChatmageddonLoadingIndicator__ctor_m905202813 (ChatmageddonLoadingIndicator_t472672888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonLoadingIndicator::Start()
extern "C"  void ChatmageddonLoadingIndicator_Start_m4148304261 (ChatmageddonLoadingIndicator_t472672888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonLoadingIndicator::OnShowIndicator(System.Boolean)
extern "C"  void ChatmageddonLoadingIndicator_OnShowIndicator_m2550911929 (ChatmageddonLoadingIndicator_t472672888 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonLoadingIndicator::OnHideIndicator(System.Boolean)
extern "C"  void ChatmageddonLoadingIndicator_OnHideIndicator_m1767059432 (ChatmageddonLoadingIndicator_t472672888 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonLoadingIndicator::PlayRotation()
extern "C"  void ChatmageddonLoadingIndicator_PlayRotation_m2916393173 (ChatmageddonLoadingIndicator_t472672888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonLoadingIndicator::ScaleTweenFinished()
extern "C"  void ChatmageddonLoadingIndicator_ScaleTweenFinished_m319906240 (ChatmageddonLoadingIndicator_t472672888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
