﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.BoxCollider>
struct List_1_t3687008489;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<UISprite>
struct List_1_t4267705163;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AAndHUIScaler
struct  AAndHUIScaler_t3026888490  : public ChatmageddonUIScaler_t3374094653
{
public:
	// System.Collections.Generic.List`1<UnityEngine.BoxCollider> AAndHUIScaler::colliders
	List_1_t3687008489 * ___colliders_14;
	// System.Collections.Generic.List`1<UnityEngine.Transform> AAndHUIScaler::leftTransforms
	List_1_t2644239190 * ___leftTransforms_15;
	// System.Collections.Generic.List`1<UnityEngine.Transform> AAndHUIScaler::rightTransforms
	List_1_t2644239190 * ___rightTransforms_16;
	// System.Collections.Generic.List`1<UISprite> AAndHUIScaler::dividers
	List_1_t4267705163 * ___dividers_17;

public:
	inline static int32_t get_offset_of_colliders_14() { return static_cast<int32_t>(offsetof(AAndHUIScaler_t3026888490, ___colliders_14)); }
	inline List_1_t3687008489 * get_colliders_14() const { return ___colliders_14; }
	inline List_1_t3687008489 ** get_address_of_colliders_14() { return &___colliders_14; }
	inline void set_colliders_14(List_1_t3687008489 * value)
	{
		___colliders_14 = value;
		Il2CppCodeGenWriteBarrier(&___colliders_14, value);
	}

	inline static int32_t get_offset_of_leftTransforms_15() { return static_cast<int32_t>(offsetof(AAndHUIScaler_t3026888490, ___leftTransforms_15)); }
	inline List_1_t2644239190 * get_leftTransforms_15() const { return ___leftTransforms_15; }
	inline List_1_t2644239190 ** get_address_of_leftTransforms_15() { return &___leftTransforms_15; }
	inline void set_leftTransforms_15(List_1_t2644239190 * value)
	{
		___leftTransforms_15 = value;
		Il2CppCodeGenWriteBarrier(&___leftTransforms_15, value);
	}

	inline static int32_t get_offset_of_rightTransforms_16() { return static_cast<int32_t>(offsetof(AAndHUIScaler_t3026888490, ___rightTransforms_16)); }
	inline List_1_t2644239190 * get_rightTransforms_16() const { return ___rightTransforms_16; }
	inline List_1_t2644239190 ** get_address_of_rightTransforms_16() { return &___rightTransforms_16; }
	inline void set_rightTransforms_16(List_1_t2644239190 * value)
	{
		___rightTransforms_16 = value;
		Il2CppCodeGenWriteBarrier(&___rightTransforms_16, value);
	}

	inline static int32_t get_offset_of_dividers_17() { return static_cast<int32_t>(offsetof(AAndHUIScaler_t3026888490, ___dividers_17)); }
	inline List_1_t4267705163 * get_dividers_17() const { return ___dividers_17; }
	inline List_1_t4267705163 ** get_address_of_dividers_17() { return &___dividers_17; }
	inline void set_dividers_17(List_1_t4267705163 * value)
	{
		___dividers_17 = value;
		Il2CppCodeGenWriteBarrier(&___dividers_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
