﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlitchEffect
struct GlitchEffect_t4241067900;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void GlitchEffect::.ctor()
extern "C"  void GlitchEffect__ctor_m2809592153 (GlitchEffect_t4241067900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlitchEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void GlitchEffect_OnRenderImage_m3663406273 (GlitchEffect_t4241067900 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
