﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuyItemButton
struct BuyItemButton_t2593913003;
// PurchaseableItem
struct PurchaseableItem_t3351122996;
// QuantitativeItem
struct QuantitativeItem_t3036513780;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PurchaseableItem3351122996.h"
#include "AssemblyU2DCSharp_QuantitativeItem3036513780.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void BuyItemButton::.ctor()
extern "C"  void BuyItemButton__ctor_m1333494186 (BuyItemButton_t2593913003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::SetButtonActive(System.Int32,System.Boolean)
extern "C"  void BuyItemButton_SetButtonActive_m2769692120 (BuyItemButton_t2593913003 * __this, int32_t ___inventoryAmount0, bool ___locked1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::OnButtonClick()
extern "C"  void BuyItemButton_OnButtonClick_m3002484659 (BuyItemButton_t2593913003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::PurchaseShieldItem(PurchaseableItem)
extern "C"  void BuyItemButton_PurchaseShieldItem_m3646799195 (BuyItemButton_t2593913003 * __this, PurchaseableItem_t3351122996 * ___cPurchaseableItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::PurchaseStaticItem(PurchaseableItem)
extern "C"  void BuyItemButton_PurchaseStaticItem_m1142610702 (BuyItemButton_t2593913003 * __this, PurchaseableItem_t3351122996 * ___cPurchaseableItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::AddQuantitativeItemToInventory(QuantitativeItem)
extern "C"  void BuyItemButton_AddQuantitativeItemToInventory_m1075911414 (BuyItemButton_t2593913003 * __this, QuantitativeItem_t3036513780 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::AddPurchasedItemsToInventory(System.Collections.Hashtable)
extern "C"  void BuyItemButton_AddPurchasedItemsToInventory_m3800022303 (BuyItemButton_t2593913003 * __this, Hashtable_t909839986 * ___itemsInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemButton::SendFluffyPurchaseEvent(PurchaseableItem)
extern "C"  void BuyItemButton_SendFluffyPurchaseEvent_m4001819979 (BuyItemButton_t2593913003 * __this, PurchaseableItem_t3351122996 * ___purchasedItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
