﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppleAppStoreCallbackMonoBehaviour
struct AppleAppStoreCallbackMonoBehaviour_t2011772955;
// Unibill.Impl.AppleAppStoreBillingService
struct AppleAppStoreBillingService_t956296338;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_AppleApp956296338.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AppleAppStoreCallbackMonoBehaviour::.ctor()
extern "C"  void AppleAppStoreCallbackMonoBehaviour__ctor_m1302491856 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::Awake()
extern "C"  void AppleAppStoreCallbackMonoBehaviour_Awake_m673557661 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::initialise(Unibill.Impl.AppleAppStoreBillingService)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_initialise_m195044326 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, AppleAppStoreBillingService_t956296338 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onProductListReceived(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onProductListReceived_m3180007637 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___productList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onProductPurchaseSuccess(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onProductPurchaseSuccess_m2994473438 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onProductPurchaseCancelled(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onProductPurchaseCancelled_m1718697482 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onProductPurchaseFailed(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onProductPurchaseFailed_m2285170156 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onProductPurchaseDeferred(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onProductPurchaseDeferred_m3806663374 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onTransactionsRestoredSuccess(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onTransactionsRestoredSuccess_m3599527211 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onTransactionsRestoredFail(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onTransactionsRestoredFail_m569084990 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppleAppStoreCallbackMonoBehaviour::onFailedToRetrieveProductList(System.String)
extern "C"  void AppleAppStoreCallbackMonoBehaviour_onFailedToRetrieveProductList_m2641238758 (AppleAppStoreCallbackMonoBehaviour_t2011772955 * __this, String_t* ___nop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
