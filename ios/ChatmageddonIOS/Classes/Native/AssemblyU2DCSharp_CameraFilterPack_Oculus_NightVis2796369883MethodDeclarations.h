﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Oculus_NightVision2
struct CameraFilterPack_Oculus_NightVision2_t2796369883;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Oculus_NightVision2::.ctor()
extern "C"  void CameraFilterPack_Oculus_NightVision2__ctor_m3272075982 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Oculus_NightVision2_get_material_m1488340355 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::ChangeFilters()
extern "C"  void CameraFilterPack_Oculus_NightVision2_ChangeFilters_m376875167 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::Start()
extern "C"  void CameraFilterPack_Oculus_NightVision2_Start_m1082442394 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Oculus_NightVision2_OnRenderImage_m69942626 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::OnValidate()
extern "C"  void CameraFilterPack_Oculus_NightVision2_OnValidate_m284050093 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::Update()
extern "C"  void CameraFilterPack_Oculus_NightVision2_Update_m205168903 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::OnDisable()
extern "C"  void CameraFilterPack_Oculus_NightVision2_OnDisable_m3825878487 (CameraFilterPack_Oculus_NightVision2_t2796369883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
