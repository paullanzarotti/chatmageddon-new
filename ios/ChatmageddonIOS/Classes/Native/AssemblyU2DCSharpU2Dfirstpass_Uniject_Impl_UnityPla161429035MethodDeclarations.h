﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Uniject.Impl.UnityPlayerPrefsStorage
struct UnityPlayerPrefsStorage_t161429035;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Uniject.Impl.UnityPlayerPrefsStorage::.ctor()
extern "C"  void UnityPlayerPrefsStorage__ctor_m915678086 (UnityPlayerPrefsStorage_t161429035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Uniject.Impl.UnityPlayerPrefsStorage::GetInt(System.String,System.Int32)
extern "C"  int32_t UnityPlayerPrefsStorage_GetInt_m524052482 (UnityPlayerPrefsStorage_t161429035 * __this, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityPlayerPrefsStorage::SetInt(System.String,System.Int32)
extern "C"  void UnityPlayerPrefsStorage_SetInt_m3412903368 (UnityPlayerPrefsStorage_t161429035 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Uniject.Impl.UnityPlayerPrefsStorage::GetString(System.String,System.String)
extern "C"  String_t* UnityPlayerPrefsStorage_GetString_m398935054 (UnityPlayerPrefsStorage_t161429035 * __this, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityPlayerPrefsStorage::SetString(System.String,System.String)
extern "C"  void UnityPlayerPrefsStorage_SetString_m1942742307 (UnityPlayerPrefsStorage_t161429035 * __this, String_t* ___key0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
