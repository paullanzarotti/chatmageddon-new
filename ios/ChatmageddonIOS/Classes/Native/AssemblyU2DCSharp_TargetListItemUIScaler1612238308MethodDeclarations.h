﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetListItemUIScaler
struct TargetListItemUIScaler_t1612238308;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetListItemUIScaler::.ctor()
extern "C"  void TargetListItemUIScaler__ctor_m4222887011 (TargetListItemUIScaler_t1612238308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetListItemUIScaler::GlobalUIScale()
extern "C"  void TargetListItemUIScaler_GlobalUIScale_m2800098110 (TargetListItemUIScaler_t1612238308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
