﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Light_Water2
struct CameraFilterPack_Light_Water2_t3567750323;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Light_Water2::.ctor()
extern "C"  void CameraFilterPack_Light_Water2__ctor_m2211383702 (CameraFilterPack_Light_Water2_t3567750323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Water2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Light_Water2_get_material_m1873698475 (CameraFilterPack_Light_Water2_t3567750323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::Start()
extern "C"  void CameraFilterPack_Light_Water2_Start_m2599112826 (CameraFilterPack_Light_Water2_t3567750323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Light_Water2_OnRenderImage_m2169516466 (CameraFilterPack_Light_Water2_t3567750323 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::OnValidate()
extern "C"  void CameraFilterPack_Light_Water2_OnValidate_m2422794761 (CameraFilterPack_Light_Water2_t3567750323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::Update()
extern "C"  void CameraFilterPack_Light_Water2_Update_m1744983795 (CameraFilterPack_Light_Water2_t3567750323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::OnDisable()
extern "C"  void CameraFilterPack_Light_Water2_OnDisable_m1167878483 (CameraFilterPack_Light_Water2_t3567750323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
