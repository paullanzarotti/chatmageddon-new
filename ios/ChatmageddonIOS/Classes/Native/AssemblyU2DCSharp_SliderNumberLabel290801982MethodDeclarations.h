﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SliderNumberLabel
struct SliderNumberLabel_t290801982;

#include "codegen/il2cpp-codegen.h"

// System.Void SliderNumberLabel::.ctor()
extern "C"  void SliderNumberLabel__ctor_m2086856403 (SliderNumberLabel_t290801982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderNumberLabel::UpdateUnits()
extern "C"  void SliderNumberLabel_UpdateUnits_m4121879435 (SliderNumberLabel_t290801982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderNumberLabel::UpdateLabel(System.Int32)
extern "C"  void SliderNumberLabel_UpdateLabel_m3070479129 (SliderNumberLabel_t290801982 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
