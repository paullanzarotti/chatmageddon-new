﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3309526906MethodDeclarations.h"

// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::.ctor()
#define NavigationController_2__ctor_m1329824286(__this, method) ((  void (*) (NavigationController_2_t3268140962 *, const MethodInfo*))NavigationController_2__ctor_m2166287613_gshared)(__this, method)
// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m799250776(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3268140962 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m2761873091_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m2323641301(__this, method) ((  void (*) (NavigationController_2_t3268140962 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m538863782_gshared)(__this, method)
// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3257245486(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3268140962 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m2294274539_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<ProfileNavigationController,ProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m1654519050(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3268140962 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m4024368191_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<ProfileNavigationController,ProfileNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m2029269519(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3268140962 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1522047260_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m599872324(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3268140962 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m286403891_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m699015554(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3268140962 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2954494359_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<ProfileNavigationController,ProfileNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m3256455770(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3268140962 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2444270883_gshared)(__this, ___screen0, method)
