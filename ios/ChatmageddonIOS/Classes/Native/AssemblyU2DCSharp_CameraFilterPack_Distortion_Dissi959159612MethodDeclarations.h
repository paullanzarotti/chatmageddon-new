﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Dissipation
struct CameraFilterPack_Distortion_Dissipation_t959159612;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Dissipation::.ctor()
extern "C"  void CameraFilterPack_Distortion_Dissipation__ctor_m740346297 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Dissipation::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Dissipation_get_material_m185962292 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::Start()
extern "C"  void CameraFilterPack_Distortion_Dissipation_Start_m2166820289 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Dissipation_OnRenderImage_m876553657 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Dissipation_OnValidate_m2907623548 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::Update()
extern "C"  void CameraFilterPack_Distortion_Dissipation_Update_m2953143686 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Dissipation_OnDisable_m996466624 (CameraFilterPack_Distortion_Dissipation_t959159612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
