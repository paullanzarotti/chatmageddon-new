﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>
struct KeyCollection_t2766651291;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct Dictionary_2_t283153520;
// System.Collections.Generic.IEnumerator`1<Unibill.Impl.BillingPlatform>
struct IEnumerator_1_t2322550357;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Unibill.Impl.BillingPlatform[]
struct BillingPlatformU5BU5D_t1616278231;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2972656958.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3838914573_gshared (KeyCollection_t2766651291 * __this, Dictionary_2_t283153520 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3838914573(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2766651291 *, Dictionary_2_t283153520 *, const MethodInfo*))KeyCollection__ctor_m3838914573_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2987932827_gshared (KeyCollection_t2766651291 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2987932827(__this, ___item0, method) ((  void (*) (KeyCollection_t2766651291 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2987932827_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1722159286_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1722159286(__this, method) ((  void (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1722159286_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1396153341_gshared (KeyCollection_t2766651291 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1396153341(__this, ___item0, method) ((  bool (*) (KeyCollection_t2766651291 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1396153341_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3920768710_gshared (KeyCollection_t2766651291 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3920768710(__this, ___item0, method) ((  bool (*) (KeyCollection_t2766651291 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3920768710_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2220136930_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2220136930(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2220136930_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3823530176_gshared (KeyCollection_t2766651291 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3823530176(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2766651291 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3823530176_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2718737043_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2718737043(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2718737043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2556458832_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2556458832(__this, method) ((  bool (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2556458832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497235644_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497235644(__this, method) ((  bool (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497235644_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3123193354_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3123193354(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3123193354_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3539548866_gshared (KeyCollection_t2766651291 * __this, BillingPlatformU5BU5D_t1616278231* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3539548866(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2766651291 *, BillingPlatformU5BU5D_t1616278231*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3539548866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2972656958  KeyCollection_GetEnumerator_m1419934541_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1419934541(__this, method) ((  Enumerator_t2972656958  (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_GetEnumerator_m1419934541_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m95902596_gshared (KeyCollection_t2766651291 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m95902596(__this, method) ((  int32_t (*) (KeyCollection_t2766651291 *, const MethodInfo*))KeyCollection_get_Count_m95902596_gshared)(__this, method)
