﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TilesetFadeExample3521105452.h"
#include "AssemblyU2DCSharp_TilesetFadeExample_U3COnChangeMa2046870675.h"
#include "AssemblyU2DCSharp_TilesetFadeExampleItem1982028563.h"
#include "AssemblyU2DCSharp_TilesetOverlayExample4212851882.h"
#include "AssemblyU2DCSharp_TooltipWithoutBackgroundExample2651718405.h"
#include "AssemblyU2DCSharp_ExampleGUI2253512165.h"
#include "AssemblyU2DCSharp_GUITextureSize1753924715.h"
#include "AssemblyU2DCSharp_OnlineMapsControlBase473237564.h"
#include "AssemblyU2DCSharp_OnlineMapsControlBase_U3CDoReduce676239010.h"
#include "AssemblyU2DCSharp_OnlineMapsControlBase_U3CMoveToP1503452301.h"
#include "AssemblyU2DCSharp_OnlineMapsControlBase_U3CWaitLon2282204602.h"
#include "AssemblyU2DCSharp_OnlineMapsControlBase2D1235229338.h"
#include "AssemblyU2DCSharp_OnlineMapsControlBase3D2801313279.h"
#include "AssemblyU2DCSharp_OnlineMapsDFGUITextureControl2511974367.h"
#include "AssemblyU2DCSharp_OnlineMapsGUITextureControl39450173.h"
#include "AssemblyU2DCSharp_OnlineMapsIGUITextureControl458085378.h"
#include "AssemblyU2DCSharp_OnlineMapsNGUITextureControl321561671.h"
#include "AssemblyU2DCSharp_OnlineMapsSpriteRendererControl1477560197.h"
#include "AssemblyU2DCSharp_OnlineMapsTextureControl1046647040.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl3368302803.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_CheckMa3802502927.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_GetFlatM860568800.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_Tileset3550885087.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_Tileset2480816995.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_OnlineM3256055626.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_OnlineM3481217700.h"
#include "AssemblyU2DCSharp_OnlineMapsUIImageControl605851132.h"
#include "AssemblyU2DCSharp_OnlineMapsUIRawImageControl3061318298.h"
#include "AssemblyU2DCSharp_OnlineMapsDrawingElement539447654.h"
#include "AssemblyU2DCSharp_OnlineMapsDrawingLine2877694824.h"
#include "AssemblyU2DCSharp_OnlineMapsDrawingPoly2218936412.h"
#include "AssemblyU2DCSharp_OnlineMapsDrawingRect2548315780.h"
#include "AssemblyU2DCSharp_OnlineMapsFindAutocomplete4286140857.h"
#include "AssemblyU2DCSharp_OnlineMapsFindAutocompleteResult2619648090.h"
#include "AssemblyU2DCSharp_OnlineMapsFindAutocompleteResult3836860852.h"
#include "AssemblyU2DCSharp_OnlineMapsFindAutocompleteResult3111891439.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirection2666247748.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionAdvanced3669500598.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionMode1690934541.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionAvoid3411768659.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionUnits4069044639.h"
#include "AssemblyU2DCSharp_OnlineMapsFindLocation1727203720.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaceDetails3448794556.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaceDetailsResult4015393875.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaces49118535.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaces_OnlineMapsFi761642977.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaces_OnlineMapsFi153906316.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlacesResult686120750.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlacesResultPhoto2950683050.h"
#include "AssemblyU2DCSharp_OnlineMapsGetElevation3666698847.h"
#include "AssemblyU2DCSharp_OnlineMapsGetElevationResult3115336078.h"
#include "AssemblyU2DCSharp_OnlineMapsGoogleAPIQuery356009153.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMAPIQuery1899738209.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase540795536.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase_U3CGetTagValue1279171491.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase_U3CHasTagU3Ec__816421953.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase_U3CHasTagsU3Ec2665984195.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase_U3CHasTagsU3Ec_516336015.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase_U3CHasTagKeyU31621641509.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMBase_U3CHasTagValueU620528620.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMNode3383990403.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMWay3319895272.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMWay_U3CGetNodesU3Ec2656895175.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMRelation1871982797.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMRelationMember1040319881.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3492166682.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3D576815539.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3DInstance3567168358.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3DInstance_U3CWa3738279470.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerBillboard495103289.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerInstanceBase538187336.h"
#include "AssemblyU2DCSharp_OnlineMaps1893290312.h"
#include "AssemblyU2DCSharp_OnlineMaps_OnPrepareTooltipStyleH626313099.h"
#include "AssemblyU2DCSharp_OnlineMaps_U3CGetDrawingElementU2400602976.h"
#include "AssemblyU2DCSharp_OnlineMaps_U3CGetMarkerFromScree3674250235.h"
#include "AssemblyU2DCSharp_OnlineMapsBuffer2643049474.h"
#include "AssemblyU2DCSharp_OnlineMapsBuffer_SortMarkersDele3426682085.h"
#include "AssemblyU2DCSharp_OnlineMapsBuffer_U3CCreateTilePa2137665930.h"
#include "AssemblyU2DCSharp_OnlineMapsBufferZoom2072536377.h"
#include "AssemblyU2DCSharp_OnlineMapsAlign3858887827.h"
#include "AssemblyU2DCSharp_OnlineMapsBufferStatus201731616.h"
#include "AssemblyU2DCSharp_OnlineMapsEvents850667009.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker2DMode2893795757.h"
#include "AssemblyU2DCSharp_OnlineMapsLocationServiceMarkerT2500453164.h"
#include "AssemblyU2DCSharp_OnlineMapsProviderEnum1784547284.h"
#include "AssemblyU2DCSharp_OnlineMapsPositionRangeType3691382528.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryStatus3400398814.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"
#include "AssemblyU2DCSharp_OnlineMapsRedrawType1698729245.h"
#include "AssemblyU2DCSharp_OnlineMapsTarget2604759619.h"
#include "AssemblyU2DCSharp_OnlineMapsTilesetCheckMarker2DVi1564781124.h"
#include "AssemblyU2DCSharp_OnlineMapsShowMarkerTooltip3336221582.h"
#include "AssemblyU2DCSharp_OnlineMapsSource4087519963.h"
#include "AssemblyU2DCSharp_OnlineMapsTileStatus3389371110.h"
#include "AssemblyU2DCSharp_OnlineMapsPositionRange2844425400.h"
#include "AssemblyU2DCSharp_OnlineMapsRange3791609909.h"
#include "AssemblyU2DCSharp_OnlineMapsThreadManager2145254635.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (TilesetFadeExample_t3521105452), -1, sizeof(TilesetFadeExample_t3521105452_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4900[3] = 
{
	TilesetFadeExample_t3521105452::get_offset_of_items_2(),
	TilesetFadeExample_t3521105452_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
	TilesetFadeExample_t3521105452_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4901[1] = 
{
	U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675::get_offset_of_tile_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (TilesetFadeExampleItem_t1982028563), -1, sizeof(TilesetFadeExampleItem_t1982028563_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4902[8] = 
{
	0,
	TilesetFadeExampleItem_t1982028563_StaticFields::get_offset_of_fromColor_1(),
	TilesetFadeExampleItem_t1982028563_StaticFields::get_offset_of_toColor_2(),
	TilesetFadeExampleItem_t1982028563::get_offset_of_finished_3(),
	TilesetFadeExampleItem_t1982028563::get_offset_of_material_4(),
	TilesetFadeExampleItem_t1982028563::get_offset_of_tile_5(),
	TilesetFadeExampleItem_t1982028563::get_offset_of_alpha_6(),
	TilesetFadeExampleItem_t1982028563::get_offset_of_startTicks_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (TilesetOverlayExample_t4212851882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4903[5] = 
{
	TilesetOverlayExample_t4212851882::get_offset_of_texture_2(),
	TilesetOverlayExample_t4212851882::get_offset_of_alpha_3(),
	TilesetOverlayExample_t4212851882::get_offset_of_overlayMesh_4(),
	TilesetOverlayExample_t4212851882::get_offset_of_material_5(),
	TilesetOverlayExample_t4212851882::get_offset_of_tilesetCollider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (TooltipWithoutBackgroundExample_t2651718405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (ExampleGUI_t2253512165), -1, sizeof(ExampleGUI_t2253512165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4905[4] = 
{
	ExampleGUI_t2253512165::get_offset_of_api_2(),
	ExampleGUI_t2253512165::get_offset_of_rowStyle_3(),
	ExampleGUI_t2253512165::get_offset_of_search_4(),
	ExampleGUI_t2253512165_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (GUITextureSize_t1753924715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { sizeof (OnlineMapsControlBase_t473237564), -1, sizeof(OnlineMapsControlBase_t473237564_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4907[31] = 
{
	OnlineMapsControlBase_t473237564_StaticFields::get_offset_of_longPressDelay_2(),
	OnlineMapsControlBase_t473237564_StaticFields::get_offset_of__instance_3(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapClick_4(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapDoubleClick_5(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapDrag_6(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapLongPress_7(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapPress_8(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapRelease_9(),
	OnlineMapsControlBase_t473237564::get_offset_of_OnMapZoom_10(),
	OnlineMapsControlBase_t473237564::get_offset_of_activeTexture_11(),
	OnlineMapsControlBase_t473237564::get_offset_of_allowAddMarkerByM_12(),
	OnlineMapsControlBase_t473237564::get_offset_of_allowZoom_13(),
	OnlineMapsControlBase_t473237564::get_offset_of_allowUserControl_14(),
	OnlineMapsControlBase_t473237564::get_offset_of_isMapDrag_15(),
	OnlineMapsControlBase_t473237564::get_offset_of_invertTouchZoom_16(),
	OnlineMapsControlBase_t473237564::get_offset_of_zoomInOnDoubleClick_17(),
	OnlineMapsControlBase_t473237564::get_offset_of__screenRect_18(),
	OnlineMapsControlBase_t473237564::get_offset_of_api_19(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastGestureDistance_20(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastMousePosition_21(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastTouchCount_22(),
	OnlineMapsControlBase_t473237564::get_offset_of__dragMarker_23(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastClickTimes_24(),
	OnlineMapsControlBase_t473237564::get_offset_of_pressPoint_25(),
	OnlineMapsControlBase_t473237564::get_offset_of_longPressEnumenator_26(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastGestureCenter_27(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastPositionLng_28(),
	OnlineMapsControlBase_t473237564::get_offset_of_lastPositionLat_29(),
	OnlineMapsControlBase_t473237564::get_offset_of_dragVelocityRoutine_30(),
	OnlineMapsControlBase_t473237564::get_offset_of_startPressX_31(),
	OnlineMapsControlBase_t473237564::get_offset_of_startPressY_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { sizeof (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4908[13] = 
{
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U3CtotalTimeU3E__0_0(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U3CdifferenceTimeU3E__1_1(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_distanceX_2(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U3CoffsetXU3E__2_3(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_distanceY_4(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U3CoffsetYU3E__3_5(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U3CpxU3E__4_6(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U3CpyU3E__5_7(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_durationTime_8(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U24this_9(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U24current_10(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U24disposing_11(),
	U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { sizeof (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4909[15] = 
{
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CpointXU3E__0_0(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CpointYU3E__1_1(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_distanceX_2(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_distanceY_3(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CtotalTimeU3E__2_4(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_durationTime_5(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CpercentageIncreaseU3E__3_6(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CoffsetXU3E__4_7(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CoffsetYU3E__5_8(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CpxU3E__6_9(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U3CpyU3E__7_10(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U24this_11(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U24current_12(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U24disposing_13(),
	U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { sizeof (U3CWaitLongPressU3Ec__Iterator2_t2282204602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4910[5] = 
{
	U3CWaitLongPressU3Ec__Iterator2_t2282204602::get_offset_of_U3CmarkerU3E__0_0(),
	U3CWaitLongPressU3Ec__Iterator2_t2282204602::get_offset_of_U24this_1(),
	U3CWaitLongPressU3Ec__Iterator2_t2282204602::get_offset_of_U24current_2(),
	U3CWaitLongPressU3Ec__Iterator2_t2282204602::get_offset_of_U24disposing_3(),
	U3CWaitLongPressU3Ec__Iterator2_t2282204602::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { sizeof (OnlineMapsControlBase2D_t1235229338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { sizeof (OnlineMapsControlBase3D_t2801313279), -1, sizeof(OnlineMapsControlBase3D_t2801313279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4912[20] = 
{
	OnlineMapsControlBase3D_t2801313279::get_offset_of_OnCameraControl_33(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_activeCamera_34(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_allowDefaultMarkerEvents_35(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_allowCameraControl_36(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_cameraDistance_37(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_cameraRotation_38(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_cameraSpeed_39(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_marker2DMode_40(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_marker2DSize_41(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_markers3D_42(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_marker3DScale_43(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_allowAddMarker3DByN_44(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_markersGameObject_45(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_markersMesh_46(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_markersRenderer_47(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_isCameraControl_48(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of_markerBillboards_49(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of__cl_50(),
	OnlineMapsControlBase3D_t2801313279::get_offset_of__renderer_51(),
	OnlineMapsControlBase3D_t2801313279_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { sizeof (OnlineMapsDFGUITextureControl_t2511974367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { sizeof (OnlineMapsGUITextureControl_t39450173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4914[1] = 
{
	OnlineMapsGUITextureControl_t39450173::get_offset_of__gTexture_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { sizeof (OnlineMapsIGUITextureControl_t458085378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { sizeof (OnlineMapsNGUITextureControl_t321561671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { sizeof (OnlineMapsSpriteRendererControl_t1477560197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4917[3] = 
{
	OnlineMapsSpriteRendererControl_t1477560197::get_offset_of__cl_33(),
	OnlineMapsSpriteRendererControl_t1477560197::get_offset_of__cl2D_34(),
	OnlineMapsSpriteRendererControl_t1477560197::get_offset_of_spriteRenderer_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { sizeof (OnlineMapsTextureControl_t1046647040), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { sizeof (OnlineMapsTileSetControl_t3368302803), -1, sizeof(OnlineMapsTileSetControl_t3368302803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4919[46] = 
{
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnChangeMaterialTexture_53(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnCheckMarker2DVisibility_54(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnGetElevation_55(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnGetFlatMarkerOffsetY_56(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnSmoothZoomBegin_57(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnSmoothZoomFinish_58(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_OnSmoothZoomProcess_59(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_bingAPI_60(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_checkMarker2DVisibility_61(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_colliderType_62(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_drawingsGameObject_63(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_drawingShader_64(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationZoomRange_65(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationScale_66(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_markerComparer_67(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_markerMaterial_68(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_markerShader_69(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_smoothZoom_70(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_smoothZoomMode_71(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_tileMaterial_72(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_tilesetShader_73(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_useElevation_74(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of__useElevation_75(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of__bufferPosition_76(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationRequest_77(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationRequestRect_78(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationData_79(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationRect_80(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_meshCollider_81(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_ignoreGetElevation_82(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_tilesetMesh_83(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_triangles_84(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_uv_85(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_vertices_86(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_elevationBufferPosition_87(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_smoothZoomStarted_88(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_smoothZoomPoint_89(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_originalPosition_90(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_smoothZoomOffset_91(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_smoothZoomHitPoint_92(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_firstUpdate_93(),
	OnlineMapsTileSetControl_t3368302803::get_offset_of_usedMarkers_94(),
	OnlineMapsTileSetControl_t3368302803_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_95(),
	OnlineMapsTileSetControl_t3368302803_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_96(),
	OnlineMapsTileSetControl_t3368302803_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_97(),
	OnlineMapsTileSetControl_t3368302803_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_98(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { sizeof (CheckMarker2DVisibilityDelegate_t3802502927), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { sizeof (GetFlatMarkerOffsetYDelegate_t860568800), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { sizeof (TilesetFlatMarker_t3550885087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4922[2] = 
{
	TilesetFlatMarker_t3550885087::get_offset_of_marker_0(),
	TilesetFlatMarker_t3550885087::get_offset_of_rect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { sizeof (TilesetSortedMarker_t2480816995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4923[2] = 
{
	TilesetSortedMarker_t2480816995::get_offset_of_marker_0(),
	TilesetSortedMarker_t2480816995::get_offset_of_offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { sizeof (OnlineMapsColliderType_t3256055626)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4924[3] = 
{
	OnlineMapsColliderType_t3256055626::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { sizeof (OnlineMapsSmoothZoomMode_t3481217700)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4925[3] = 
{
	OnlineMapsSmoothZoomMode_t3481217700::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (OnlineMapsUIImageControl_t605851132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4926[1] = 
{
	OnlineMapsUIImageControl_t605851132::get_offset_of_image_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (OnlineMapsUIRawImageControl_t3061318298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4927[1] = 
{
	OnlineMapsUIRawImageControl_t3061318298::get_offset_of_image_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (OnlineMapsDrawingElement_t539447654), -1, sizeof(OnlineMapsDrawingElement_t539447654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4928[21] = 
{
	OnlineMapsDrawingElement_t539447654_StaticFields::get_offset_of_halfVector_0(),
	OnlineMapsDrawingElement_t539447654_StaticFields::get_offset_of_sampleV1_1(),
	OnlineMapsDrawingElement_t539447654_StaticFields::get_offset_of_sampleV2_2(),
	OnlineMapsDrawingElement_t539447654_StaticFields::get_offset_of_sampleV3_3(),
	OnlineMapsDrawingElement_t539447654_StaticFields::get_offset_of_sampleV4_4(),
	OnlineMapsDrawingElement_t539447654_StaticFields::get_offset_of_OnElementDrawTooltip_5(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_OnClick_6(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_OnDoubleClick_7(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_OnDrawTooltip_8(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_OnPress_9(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_OnRelease_10(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_customData_11(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_tooltip_12(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_mesh_13(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_gameObject_14(),
	OnlineMapsDrawingElement_t539447654::get_offset_of__visible_15(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_bestElevationYScale_16(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_tlx_17(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_tly_18(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_brx_19(),
	OnlineMapsDrawingElement_t539447654::get_offset_of_bry_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (OnlineMapsDrawingLine_t2877694824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4929[4] = 
{
	OnlineMapsDrawingLine_t2877694824::get_offset_of_color_21(),
	OnlineMapsDrawingLine_t2877694824::get_offset_of_points_22(),
	OnlineMapsDrawingLine_t2877694824::get_offset_of_texture_23(),
	OnlineMapsDrawingLine_t2877694824::get_offset_of_weight_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (OnlineMapsDrawingPoly_t2218936412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4930[4] = 
{
	OnlineMapsDrawingPoly_t2218936412::get_offset_of_backgroundColor_21(),
	OnlineMapsDrawingPoly_t2218936412::get_offset_of_borderColor_22(),
	OnlineMapsDrawingPoly_t2218936412::get_offset_of_borderWeight_23(),
	OnlineMapsDrawingPoly_t2218936412::get_offset_of_points_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (OnlineMapsDrawingRect_t2548315780), -1, sizeof(OnlineMapsDrawingRect_t2548315780_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4931[12] = 
{
	OnlineMapsDrawingRect_t2548315780::get_offset_of_backgroundColor_21(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of_borderColor_22(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of_borderWeight_23(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of_points_24(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of__height_25(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of__width_26(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of__x_27(),
	OnlineMapsDrawingRect_t2548315780::get_offset_of__y_28(),
	OnlineMapsDrawingRect_t2548315780_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_29(),
	OnlineMapsDrawingRect_t2548315780_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_30(),
	OnlineMapsDrawingRect_t2548315780_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_31(),
	OnlineMapsDrawingRect_t2548315780_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (OnlineMapsFindAutocomplete_t4286140857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { sizeof (OnlineMapsFindAutocompleteResult_t2619648090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4933[7] = 
{
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_description_0(),
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_reference_1(),
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_id_2(),
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_place_id_3(),
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_types_4(),
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_terms_5(),
	OnlineMapsFindAutocompleteResult_t2619648090::get_offset_of_matchedSubstring_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { sizeof (OnlineMapsFindAutocompleteResultTerm_t3836860852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4934[2] = 
{
	OnlineMapsFindAutocompleteResultTerm_t3836860852::get_offset_of_value_0(),
	OnlineMapsFindAutocompleteResultTerm_t3836860852::get_offset_of_offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (OnlineMapsFindAutocompleteResultMatchedSubstring_t3111891439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4935[2] = 
{
	OnlineMapsFindAutocompleteResultMatchedSubstring_t3111891439::get_offset_of_offset_0(),
	OnlineMapsFindAutocompleteResultMatchedSubstring_t3111891439::get_offset_of_length_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (OnlineMapsFindDirection_t2666247748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (OnlineMapsFindDirectionAdvanced_t3669500598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (OnlineMapsFindDirectionMode_t1690934541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4938[5] = 
{
	OnlineMapsFindDirectionMode_t1690934541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (OnlineMapsFindDirectionAvoid_t3411768659)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4939[5] = 
{
	OnlineMapsFindDirectionAvoid_t3411768659::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { sizeof (OnlineMapsFindDirectionUnits_t4069044639)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4940[3] = 
{
	OnlineMapsFindDirectionUnits_t4069044639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (OnlineMapsFindLocation_t1727203720), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (OnlineMapsFindPlaceDetails_t3448794556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { sizeof (OnlineMapsFindPlaceDetailsResult_t4015393875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4943[18] = 
{
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_formatted_address_0(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_formatted_phone_number_1(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_location_2(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_icon_3(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_id_4(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_international_phone_number_5(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_name_6(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_node_7(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_photos_8(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_place_id_9(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_price_level_10(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_rating_11(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_reference_12(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_types_13(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_url_14(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_utc_offset_15(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_vicinity_16(),
	OnlineMapsFindPlaceDetailsResult_t4015393875::get_offset_of_website_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (OnlineMapsFindPlaces_t49118535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (OnlineMapsFindPlacesType_t761642977)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4945[4] = 
{
	OnlineMapsFindPlacesType_t761642977::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (OnlineMapsFindPlacesRankBy_t153906316)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4946[3] = 
{
	OnlineMapsFindPlacesRankBy_t153906316::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (OnlineMapsFindPlacesResult_t686120750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4947[15] = 
{
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_location_0(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_icon_1(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_id_2(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_formatted_address_3(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_name_4(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_place_id_5(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_reference_6(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_types_7(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_vicinity_8(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_price_level_9(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_rating_10(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_open_now_11(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_scope_12(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_weekday_text_13(),
	OnlineMapsFindPlacesResult_t686120750::get_offset_of_photos_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (OnlineMapsFindPlacesResultPhoto_t2950683050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4948[4] = 
{
	OnlineMapsFindPlacesResultPhoto_t2950683050::get_offset_of_width_0(),
	OnlineMapsFindPlacesResultPhoto_t2950683050::get_offset_of_height_1(),
	OnlineMapsFindPlacesResultPhoto_t2950683050::get_offset_of_photo_reference_2(),
	OnlineMapsFindPlacesResultPhoto_t2950683050::get_offset_of_html_attributions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (OnlineMapsGetElevation_t3666698847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { sizeof (OnlineMapsGetElevationResult_t3115336078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4950[3] = 
{
	OnlineMapsGetElevationResult_t3115336078::get_offset_of_elevation_0(),
	OnlineMapsGetElevationResult_t3115336078::get_offset_of_location_1(),
	OnlineMapsGetElevationResult_t3115336078::get_offset_of_resolution_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (OnlineMapsGoogleAPIQuery_t356009153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4951[6] = 
{
	OnlineMapsGoogleAPIQuery_t356009153::get_offset_of_OnComplete_0(),
	OnlineMapsGoogleAPIQuery_t356009153::get_offset_of_OnDispose_1(),
	OnlineMapsGoogleAPIQuery_t356009153::get_offset_of_OnFinish_2(),
	OnlineMapsGoogleAPIQuery_t356009153::get_offset_of__status_3(),
	OnlineMapsGoogleAPIQuery_t356009153::get_offset_of_www_4(),
	OnlineMapsGoogleAPIQuery_t356009153::get_offset_of__response_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (OnlineMapsOSMAPIQuery_t1899738209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { sizeof (OnlineMapsOSMBase_t540795536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4953[2] = 
{
	OnlineMapsOSMBase_t540795536::get_offset_of_id_0(),
	OnlineMapsOSMBase_t540795536::get_offset_of_tags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (U3CGetTagValueU3Ec__AnonStorey0_t1279171491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4954[1] = 
{
	U3CGetTagValueU3Ec__AnonStorey0_t1279171491::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (U3CHasTagU3Ec__AnonStorey1_t816421953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4955[2] = 
{
	U3CHasTagU3Ec__AnonStorey1_t816421953::get_offset_of_key_0(),
	U3CHasTagU3Ec__AnonStorey1_t816421953::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (U3CHasTagsU3Ec__AnonStorey4_t2665984195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4956[2] = 
{
	U3CHasTagsU3Ec__AnonStorey4_t2665984195::get_offset_of_key_0(),
	U3CHasTagsU3Ec__AnonStorey4_t2665984195::get_offset_of_values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (U3CHasTagsU3Ec__AnonStorey5_t516336015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4957[2] = 
{
	U3CHasTagsU3Ec__AnonStorey5_t516336015::get_offset_of_tag_0(),
	U3CHasTagsU3Ec__AnonStorey5_t516336015::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (U3CHasTagKeyU3Ec__AnonStorey2_t1621641509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4958[1] = 
{
	U3CHasTagKeyU3Ec__AnonStorey2_t1621641509::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (U3CHasTagValueU3Ec__AnonStorey3_t620528620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4959[1] = 
{
	U3CHasTagValueU3Ec__AnonStorey3_t620528620::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { sizeof (OnlineMapsOSMNode_t3383990403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4960[2] = 
{
	OnlineMapsOSMNode_t3383990403::get_offset_of_lat_2(),
	OnlineMapsOSMNode_t3383990403::get_offset_of_lon_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { sizeof (OnlineMapsOSMWay_t3319895272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4961[1] = 
{
	OnlineMapsOSMWay_t3319895272::get_offset_of_nodeRefs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { sizeof (U3CGetNodesU3Ec__AnonStorey0_t2656895175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4962[1] = 
{
	U3CGetNodesU3Ec__AnonStorey0_t2656895175::get_offset_of_nRef_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { sizeof (OnlineMapsOSMRelation_t1871982797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4963[1] = 
{
	OnlineMapsOSMRelation_t1871982797::get_offset_of_members_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (OnlineMapsOSMRelationMember_t1040319881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4964[3] = 
{
	OnlineMapsOSMRelationMember_t1040319881::get_offset_of_reference_0(),
	OnlineMapsOSMRelationMember_t1040319881::get_offset_of_role_1(),
	OnlineMapsOSMRelationMember_t1040319881::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (OnlineMapsOSMTag_t3629071465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4965[2] = 
{
	OnlineMapsOSMTag_t3629071465::get_offset_of_key_0(),
	OnlineMapsOSMTag_t3629071465::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (OnlineMapsMarker_t3492166682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4966[11] = 
{
	OnlineMapsMarker_t3492166682::get_offset_of_align_17(),
	OnlineMapsMarker_t3492166682::get_offset_of_locked_18(),
	OnlineMapsMarker_t3492166682::get_offset_of_markerColliderRect_19(),
	OnlineMapsMarker_t3492166682::get_offset_of_texture_20(),
	OnlineMapsMarker_t3492166682::get_offset_of__colors_21(),
	OnlineMapsMarker_t3492166682::get_offset_of__height_22(),
	OnlineMapsMarker_t3492166682::get_offset_of__rotation_23(),
	OnlineMapsMarker_t3492166682::get_offset_of__rotatedColors_24(),
	OnlineMapsMarker_t3492166682::get_offset_of__textureHeight_25(),
	OnlineMapsMarker_t3492166682::get_offset_of__textureWidth_26(),
	OnlineMapsMarker_t3492166682::get_offset_of__width_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (OnlineMapsMarker3D_t576815539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4967[8] = 
{
	OnlineMapsMarker3D_t576815539::get_offset_of_OnPositionChanged_17(),
	OnlineMapsMarker3D_t576815539::get_offset_of_allowDefaultMarkerEvents_18(),
	OnlineMapsMarker3D_t576815539::get_offset_of_control_19(),
	OnlineMapsMarker3D_t576815539::get_offset_of_inited_20(),
	OnlineMapsMarker3D_t576815539::get_offset_of_instance_21(),
	OnlineMapsMarker3D_t576815539::get_offset_of_prefab_22(),
	OnlineMapsMarker3D_t576815539::get_offset_of__prefab_23(),
	OnlineMapsMarker3D_t576815539::get_offset_of__relativePosition_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (OnlineMapsMarker3DInstance_t3567168358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4968[6] = 
{
	OnlineMapsMarker3DInstance_t3567168358::get_offset_of__position_3(),
	OnlineMapsMarker3DInstance_t3567168358::get_offset_of__scale_4(),
	OnlineMapsMarker3DInstance_t3567168358::get_offset_of_lastTouchCount_5(),
	OnlineMapsMarker3DInstance_t3567168358::get_offset_of_isPressed_6(),
	OnlineMapsMarker3DInstance_t3567168358::get_offset_of_lastClickTimes_7(),
	OnlineMapsMarker3DInstance_t3567168358::get_offset_of_longPressEnumenator_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (U3CWaitLongPressU3Ec__Iterator0_t3738279470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4969[4] = 
{
	U3CWaitLongPressU3Ec__Iterator0_t3738279470::get_offset_of_U24this_0(),
	U3CWaitLongPressU3Ec__Iterator0_t3738279470::get_offset_of_U24current_1(),
	U3CWaitLongPressU3Ec__Iterator0_t3738279470::get_offset_of_U24disposing_2(),
	U3CWaitLongPressU3Ec__Iterator0_t3738279470::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (OnlineMapsMarkerBase_t3900955221), -1, sizeof(OnlineMapsMarkerBase_t3900955221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4970[17] = 
{
	OnlineMapsMarkerBase_t3900955221_StaticFields::get_offset_of_OnMarkerDrawTooltip_0(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnClick_1(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnDoubleClick_2(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnDrag_3(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnDrawTooltip_4(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnEnabledChange_5(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnLongPress_6(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnPress_7(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnRelease_8(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnRollOut_9(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_OnRollOver_10(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_customData_11(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_label_12(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_position_13(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_range_14(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of_scale_15(),
	OnlineMapsMarkerBase_t3900955221::get_offset_of__enabled_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { sizeof (OnlineMapsMarkerBillboard_t495103289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4971[1] = 
{
	OnlineMapsMarkerBillboard_t495103289::get_offset_of_used_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { sizeof (OnlineMapsMarkerInstanceBase_t538187336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4972[1] = 
{
	OnlineMapsMarkerInstanceBase_t538187336::get_offset_of_marker_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (OnlineMaps_t1893290312), -1, sizeof(OnlineMaps_t1893290312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4973[81] = 
{
	0,
	OnlineMaps_t1893290312_StaticFields::get_offset_of_maxTileDownloads_3(),
	OnlineMaps_t1893290312::get_offset_of_OnChangePosition_4(),
	OnlineMaps_t1893290312::get_offset_of_OnChangeZoom_5(),
	OnlineMaps_t1893290312::get_offset_of_OnFindLocationAfter_6(),
	OnlineMaps_t1893290312::get_offset_of_OnMapUpdated_7(),
	OnlineMaps_t1893290312::get_offset_of_OnPrepareTooltipStyle_8(),
	OnlineMaps_t1893290312::get_offset_of_OnStartDownloadTile_9(),
	OnlineMaps_t1893290312::get_offset_of_OnUpdateBefore_10(),
	OnlineMaps_t1893290312::get_offset_of_OnUpdateLate_11(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of_isUserControl_12(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of__instance_13(),
	OnlineMaps_t1893290312::get_offset_of_allowRedraw_14(),
	OnlineMaps_t1893290312::get_offset_of_control_15(),
	OnlineMaps_t1893290312::get_offset_of_customProviderURL_16(),
	OnlineMaps_t1893290312::get_offset_of_defaultMarkerAlign_17(),
	OnlineMaps_t1893290312::get_offset_of_defaultMarkerTexture_18(),
	OnlineMaps_t1893290312::get_offset_of_defaultTileTexture_19(),
	OnlineMaps_t1893290312::get_offset_of_dispatchEvents_20(),
	OnlineMaps_t1893290312::get_offset_of_drawingElements_21(),
	OnlineMaps_t1893290312::get_offset_of_emptyColor_22(),
	OnlineMaps_t1893290312::get_offset_of_height_23(),
	OnlineMaps_t1893290312::get_offset_of_labels_24(),
	OnlineMaps_t1893290312::get_offset_of_language_25(),
	OnlineMaps_t1893290312::get_offset_of_lockRedraw_26(),
	OnlineMaps_t1893290312::get_offset_of_markers_27(),
	OnlineMaps_t1893290312::get_offset_of_needGC_28(),
	OnlineMaps_t1893290312::get_offset_of_needRedraw_29(),
	OnlineMaps_t1893290312::get_offset_of_positionRange_30(),
	OnlineMaps_t1893290312::get_offset_of_provider_31(),
	OnlineMaps_t1893290312::get_offset_of_redrawOnPlay_32(),
	OnlineMaps_t1893290312::get_offset_of_renderInThread_33(),
	OnlineMaps_t1893290312::get_offset_of_resourcesPath_34(),
	OnlineMaps_t1893290312::get_offset_of_showMarkerTooltip_35(),
	OnlineMaps_t1893290312::get_offset_of_skin_36(),
	OnlineMaps_t1893290312::get_offset_of_smartTexture_37(),
	OnlineMaps_t1893290312::get_offset_of_source_38(),
	OnlineMaps_t1893290312::get_offset_of_target_39(),
	OnlineMaps_t1893290312::get_offset_of_texture_40(),
	OnlineMaps_t1893290312::get_offset_of_tilesetWidth_41(),
	OnlineMaps_t1893290312::get_offset_of_tilesetHeight_42(),
	OnlineMaps_t1893290312::get_offset_of_tilesetSize_43(),
	OnlineMaps_t1893290312::get_offset_of_tooltip_44(),
	OnlineMaps_t1893290312::get_offset_of_tooltipDrawingElement_45(),
	OnlineMaps_t1893290312::get_offset_of_tooltipMarker_46(),
	OnlineMaps_t1893290312::get_offset_of_traffic_47(),
	OnlineMaps_t1893290312::get_offset_of_type_48(),
	OnlineMaps_t1893290312::get_offset_of_useSoftwareJPEGDecoder_49(),
	OnlineMaps_t1893290312::get_offset_of_useSmartTexture_50(),
	OnlineMaps_t1893290312::get_offset_of_webplayerProxyURL_51(),
	OnlineMaps_t1893290312::get_offset_of_width_52(),
	OnlineMaps_t1893290312::get_offset_of_zoomRange_53(),
	OnlineMaps_t1893290312::get_offset_of_latitude_54(),
	OnlineMaps_t1893290312::get_offset_of_longitude_55(),
	OnlineMaps_t1893290312::get_offset_of__position_56(),
	OnlineMaps_t1893290312::get_offset_of__zoom_57(),
	OnlineMaps_t1893290312::get_offset_of__buffer_58(),
	OnlineMaps_t1893290312::get_offset_of__googleQueries_59(),
	OnlineMaps_t1893290312::get_offset_of__labels_60(),
	OnlineMaps_t1893290312::get_offset_of__language_61(),
	OnlineMaps_t1893290312::get_offset_of__provider_62(),
	OnlineMaps_t1893290312::get_offset_of__traffic_63(),
	OnlineMaps_t1893290312::get_offset_of__type_64(),
	OnlineMaps_t1893290312::get_offset_of_activeTexture_65(),
	OnlineMaps_t1893290312::get_offset_of_checkConnectioCallback_66(),
	OnlineMaps_t1893290312::get_offset_of_checkConnectionWWW_67(),
	OnlineMaps_t1893290312::get_offset_of_defaultColors_68(),
	OnlineMaps_t1893290312::get_offset_of_downloads_69(),
	OnlineMaps_t1893290312::get_offset_of_lastGC_70(),
	OnlineMaps_t1893290312::get_offset_of_redrawType_71(),
	OnlineMaps_t1893290312::get_offset_of_renderThread_72(),
	OnlineMaps_t1893290312::get_offset_of_rolledMarker_73(),
	OnlineMaps_t1893290312::get_offset_of_bottomRightLatitude_74(),
	OnlineMaps_t1893290312::get_offset_of_bottomRightLongitude_75(),
	OnlineMaps_t1893290312::get_offset_of_topLeftLatitude_76(),
	OnlineMaps_t1893290312::get_offset_of_topLeftLongitude_77(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_78(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_79(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_80(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_81(),
	OnlineMaps_t1893290312_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_82(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { sizeof (OnPrepareTooltipStyleHandler_t626313099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { sizeof (U3CGetDrawingElementU3Ec__AnonStorey0_t2400602976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4975[2] = 
{
	U3CGetDrawingElementU3Ec__AnonStorey0_t2400602976::get_offset_of_screenPosition_0(),
	U3CGetDrawingElementU3Ec__AnonStorey0_t2400602976::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { sizeof (U3CGetMarkerFromScreenU3Ec__AnonStorey1_t3674250235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4976[2] = 
{
	U3CGetMarkerFromScreenU3Ec__AnonStorey1_t3674250235::get_offset_of_screenPosition_0(),
	U3CGetMarkerFromScreenU3Ec__AnonStorey1_t3674250235::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { sizeof (OnlineMapsBuffer_t2643049474), -1, sizeof(OnlineMapsBuffer_t2643049474_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4977[24] = 
{
	OnlineMapsBuffer_t2643049474_StaticFields::get_offset_of_OnSortMarker_0(),
	OnlineMapsBuffer_t2643049474::get_offset_of_api_1(),
	OnlineMapsBuffer_t2643049474::get_offset_of_apiZoom_2(),
	OnlineMapsBuffer_t2643049474::get_offset_of_bufferPosition_3(),
	OnlineMapsBuffer_t2643049474::get_offset_of_frontBuffer_4(),
	OnlineMapsBuffer_t2643049474::get_offset_of_generateSmartBuffer_5(),
	OnlineMapsBuffer_t2643049474::get_offset_of_height_6(),
	OnlineMapsBuffer_t2643049474::get_offset_of_newTiles_7(),
	OnlineMapsBuffer_t2643049474::get_offset_of_redrawType_8(),
	OnlineMapsBuffer_t2643049474::get_offset_of_status_9(),
	OnlineMapsBuffer_t2643049474::get_offset_of_smartBuffer_10(),
	OnlineMapsBuffer_t2643049474::get_offset_of_updateBackBuffer_11(),
	OnlineMapsBuffer_t2643049474::get_offset_of_width_12(),
	OnlineMapsBuffer_t2643049474::get_offset_of_backBuffer_13(),
	OnlineMapsBuffer_t2643049474::get_offset_of_bufferZoom_14(),
	OnlineMapsBuffer_t2643049474::get_offset_of_disposed_15(),
	OnlineMapsBuffer_t2643049474::get_offset_of_frontBufferPosition_16(),
	OnlineMapsBuffer_t2643049474::get_offset_of_zooms_17(),
	OnlineMapsBuffer_t2643049474::get_offset_of_needUnloadTiles_18(),
	OnlineMapsBuffer_t2643049474::get_offset_of_apiLongitude_19(),
	OnlineMapsBuffer_t2643049474::get_offset_of_apiLatitude_20(),
	OnlineMapsBuffer_t2643049474_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_21(),
	OnlineMapsBuffer_t2643049474_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_22(),
	OnlineMapsBuffer_t2643049474_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (SortMarkersDelegate_t3426682085), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (U3CCreateTileParentU3Ec__AnonStorey0_t2137665930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4979[2] = 
{
	U3CCreateTileParentU3Ec__AnonStorey0_t2137665930::get_offset_of_px_0(),
	U3CCreateTileParentU3Ec__AnonStorey0_t2137665930::get_offset_of_py_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (OnlineMapsBufferZoom_t2072536377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4980[2] = 
{
	OnlineMapsBufferZoom_t2072536377::get_offset_of_id_0(),
	OnlineMapsBufferZoom_t2072536377::get_offset_of_tiles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { sizeof (OnlineMapsAlign_t3858887827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4981[10] = 
{
	OnlineMapsAlign_t3858887827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { sizeof (OnlineMapsBufferStatus_t201731616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4982[6] = 
{
	OnlineMapsBufferStatus_t201731616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (OnlineMapsEvents_t850667009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4983[3] = 
{
	OnlineMapsEvents_t850667009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (OnlineMapsMarker2DMode_t2893795757)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4984[3] = 
{
	OnlineMapsMarker2DMode_t2893795757::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (OnlineMapsLocationServiceMarkerType_t2500453164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4985[3] = 
{
	OnlineMapsLocationServiceMarkerType_t2500453164::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (OnlineMapsProviderEnum_t1784547284)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4986[9] = 
{
	OnlineMapsProviderEnum_t1784547284::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { sizeof (OnlineMapsPositionRangeType_t3691382528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4987[3] = 
{
	OnlineMapsPositionRangeType_t3691382528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { sizeof (OnlineMapsQueryStatus_t3400398814)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4988[5] = 
{
	OnlineMapsQueryStatus_t3400398814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (OnlineMapsQueryType_t3894089788)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4989[10] = 
{
	OnlineMapsQueryType_t3894089788::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (OnlineMapsRedrawType_t1698729245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4990[5] = 
{
	OnlineMapsRedrawType_t1698729245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (OnlineMapsTarget_t2604759619)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4991[3] = 
{
	OnlineMapsTarget_t2604759619::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (OnlineMapsTilesetCheckMarker2DVisibility_t1564781124)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4992[3] = 
{
	OnlineMapsTilesetCheckMarker2DVisibility_t1564781124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (OnlineMapsShowMarkerTooltip_t3336221582)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4993[5] = 
{
	OnlineMapsShowMarkerTooltip_t3336221582::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (OnlineMapsSource_t4087519963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4994[4] = 
{
	OnlineMapsSource_t4087519963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (OnlineMapsTileStatus_t3389371110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4995[7] = 
{
	OnlineMapsTileStatus_t3389371110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (OnlineMapsPositionRange_t2844425400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4996[5] = 
{
	OnlineMapsPositionRange_t2844425400::get_offset_of_minLat_0(),
	OnlineMapsPositionRange_t2844425400::get_offset_of_minLng_1(),
	OnlineMapsPositionRange_t2844425400::get_offset_of_maxLat_2(),
	OnlineMapsPositionRange_t2844425400::get_offset_of_maxLng_3(),
	OnlineMapsPositionRange_t2844425400::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (OnlineMapsRange_t3791609909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4997[2] = 
{
	OnlineMapsRange_t3791609909::get_offset_of_max_0(),
	OnlineMapsRange_t3791609909::get_offset_of_min_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (OnlineMapsThreadManager_t2145254635), -1, sizeof(OnlineMapsThreadManager_t2145254635_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4998[3] = 
{
	OnlineMapsThreadManager_t2145254635_StaticFields::get_offset_of_thread_0(),
	OnlineMapsThreadManager_t2145254635_StaticFields::get_offset_of_threadActions_1(),
	OnlineMapsThreadManager_t2145254635_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (OnlineMapsTile_t21329940), -1, sizeof(OnlineMapsTile_t21329940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4999[38] = 
{
	OnlineMapsTile_t21329940_StaticFields::get_offset_of_defaultColors_0(),
	OnlineMapsTile_t21329940_StaticFields::get_offset_of_OnGetResourcesPath_1(),
	OnlineMapsTile_t21329940_StaticFields::get_offset_of_OnTileDownloaded_2(),
	OnlineMapsTile_t21329940_StaticFields::get_offset_of_OnTrafficDownloaded_3(),
	OnlineMapsTile_t21329940_StaticFields::get_offset_of__tiles_4(),
	OnlineMapsTile_t21329940::get_offset_of_OnSetColor_5(),
	OnlineMapsTile_t21329940_StaticFields::get_offset_of_api_6(),
	OnlineMapsTile_t21329940::get_offset_of_bottomRight_7(),
	OnlineMapsTile_t21329940::get_offset_of_customData_8(),
	OnlineMapsTile_t21329940::get_offset_of_data_9(),
	OnlineMapsTile_t21329940::get_offset_of_globalPosition_10(),
	OnlineMapsTile_t21329940::get_offset_of_hasColors_11(),
	OnlineMapsTile_t21329940::get_offset_of_isMapTile_12(),
	OnlineMapsTile_t21329940::get_offset_of_labels_13(),
	OnlineMapsTile_t21329940::get_offset_of_language_14(),
	OnlineMapsTile_t21329940::get_offset_of_parent_15(),
	OnlineMapsTile_t21329940::get_offset_of_priority_16(),
	OnlineMapsTile_t21329940::get_offset_of_provider_17(),
	OnlineMapsTile_t21329940::get_offset_of_status_18(),
	OnlineMapsTile_t21329940::get_offset_of_texture_19(),
	OnlineMapsTile_t21329940::get_offset_of_topLeft_20(),
	OnlineMapsTile_t21329940::get_offset_of_trafficTexture_21(),
	OnlineMapsTile_t21329940::get_offset_of_trafficURL_22(),
	OnlineMapsTile_t21329940::get_offset_of_trafficWWW_23(),
	OnlineMapsTile_t21329940::get_offset_of_type_24(),
	OnlineMapsTile_t21329940::get_offset_of_used_25(),
	OnlineMapsTile_t21329940::get_offset_of_www_26(),
	OnlineMapsTile_t21329940::get_offset_of_x_27(),
	OnlineMapsTile_t21329940::get_offset_of_y_28(),
	OnlineMapsTile_t21329940::get_offset_of_zoom_29(),
	OnlineMapsTile_t21329940::get_offset_of__cacheFilename_30(),
	OnlineMapsTile_t21329940::get_offset_of__colors_31(),
	OnlineMapsTile_t21329940::get_offset_of__url_32(),
	OnlineMapsTile_t21329940::get_offset_of_childs_33(),
	OnlineMapsTile_t21329940::get_offset_of_hasChilds_34(),
	OnlineMapsTile_t21329940::get_offset_of_labelData_35(),
	OnlineMapsTile_t21329940::get_offset_of_labelColors_36(),
	OnlineMapsTile_t21329940_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_37(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
