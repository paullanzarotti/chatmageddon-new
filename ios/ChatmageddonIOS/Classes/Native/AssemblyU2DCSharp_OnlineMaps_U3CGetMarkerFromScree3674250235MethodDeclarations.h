﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMaps/<GetMarkerFromScreen>c__AnonStorey1
struct U3CGetMarkerFromScreenU3Ec__AnonStorey1_t3674250235;
// OnlineMapsMarker
struct OnlineMapsMarker_t3492166682;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3492166682.h"

// System.Void OnlineMaps/<GetMarkerFromScreen>c__AnonStorey1::.ctor()
extern "C"  void U3CGetMarkerFromScreenU3Ec__AnonStorey1__ctor_m2672233454 (U3CGetMarkerFromScreenU3Ec__AnonStorey1_t3674250235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps/<GetMarkerFromScreen>c__AnonStorey1::<>m__0(OnlineMapsMarker)
extern "C"  bool U3CGetMarkerFromScreenU3Ec__AnonStorey1_U3CU3Em__0_m871503647 (U3CGetMarkerFromScreenU3Ec__AnonStorey1_t3674250235 * __this, OnlineMapsMarker_t3492166682 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
