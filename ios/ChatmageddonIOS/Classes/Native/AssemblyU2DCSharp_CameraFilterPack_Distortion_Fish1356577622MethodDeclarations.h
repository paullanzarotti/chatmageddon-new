﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_FishEye
struct CameraFilterPack_Distortion_FishEye_t1356577622;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_FishEye::.ctor()
extern "C"  void CameraFilterPack_Distortion_FishEye__ctor_m297122121 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_FishEye::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_FishEye_get_material_m3335006830 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::Start()
extern "C"  void CameraFilterPack_Distortion_FishEye_Start_m3890438833 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_FishEye_OnRenderImage_m2445556481 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::OnValidate()
extern "C"  void CameraFilterPack_Distortion_FishEye_OnValidate_m3045367106 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::Update()
extern "C"  void CameraFilterPack_Distortion_FishEye_Update_m3425783700 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::OnDisable()
extern "C"  void CameraFilterPack_Distortion_FishEye_OnDisable_m2795926162 (CameraFilterPack_Distortion_FishEye_t1356577622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::.cctor()
extern "C"  void CameraFilterPack_Distortion_FishEye__cctor_m3456051688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
