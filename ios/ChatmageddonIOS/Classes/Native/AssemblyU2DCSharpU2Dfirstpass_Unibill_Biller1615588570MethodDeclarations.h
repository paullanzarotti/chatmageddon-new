﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Biller
struct Biller_t1615588570;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Unibill.Impl.IBillingService
struct IBillingService_t403880577;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.HelpCentre
struct HelpCentre_t214342444;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.CurrencyManager
struct CurrencyManager_t3009003778;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<PurchaseEvent>
struct Action_1_t545353811;
// System.Action`1<PurchasableItem>
struct Action_1_t3765153281;
// System.Collections.Generic.List`1<UnibillError>
struct List_1_t1122980919;
// System.String[]
struct StringU5BU5D_t1642385972;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TransactionDatabase201476183.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_HelpCent214342444.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Currenc3009003778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillerS4181920983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void Unibill.Biller::.ctor(Unibill.Impl.UnibillConfiguration,TransactionDatabase,Unibill.Impl.IBillingService,Uniject.ILogger,Unibill.Impl.HelpCentre,Unibill.Impl.ProductIdRemapper,Unibill.Impl.CurrencyManager)
extern "C"  void Biller__ctor_m3438633421 (Biller_t1615588570 * __this, UnibillConfiguration_t2915611853 * ___config0, TransactionDatabase_t201476183 * ___tDb1, Il2CppObject * ___billingSubsystem2, Il2CppObject * ___logger3, HelpCentre_t214342444 * ___help4, ProductIdRemapper_t3313438456 * ___remapper5, CurrencyManager_t3009003778 * ___currencyManager6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.UnibillConfiguration Unibill.Biller::get_InventoryDatabase()
extern "C"  UnibillConfiguration_t2915611853 * Biller_get_InventoryDatabase_m2849952559 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::set_InventoryDatabase(Unibill.Impl.UnibillConfiguration)
extern "C"  void Biller_set_InventoryDatabase_m2275396000 (Biller_t1615588570 * __this, UnibillConfiguration_t2915611853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IBillingService Unibill.Biller::get_billingSubsystem()
extern "C"  Il2CppObject * Biller_get_billingSubsystem_m1185989822 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::set_billingSubsystem(Unibill.Impl.IBillingService)
extern "C"  void Biller_set_billingSubsystem_m1687900787 (Biller_t1615588570 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onBillerReady(System.Action`1<System.Boolean>)
extern "C"  void Biller_add_onBillerReady_m1259471591 (Biller_t1615588570 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onBillerReady(System.Action`1<System.Boolean>)
extern "C"  void Biller_remove_onBillerReady_m1155234880 (Biller_t1615588570 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onPurchaseComplete(System.Action`1<PurchaseEvent>)
extern "C"  void Biller_add_onPurchaseComplete_m3772903026 (Biller_t1615588570 * __this, Action_1_t545353811 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onPurchaseComplete(System.Action`1<PurchaseEvent>)
extern "C"  void Biller_remove_onPurchaseComplete_m3435642425 (Biller_t1615588570 * __this, Action_1_t545353811 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onTransactionRestoreBegin(System.Action`1<System.Boolean>)
extern "C"  void Biller_add_onTransactionRestoreBegin_m3502818583 (Biller_t1615588570 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onTransactionRestoreBegin(System.Action`1<System.Boolean>)
extern "C"  void Biller_remove_onTransactionRestoreBegin_m2196535734 (Biller_t1615588570 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onTransactionsRestored(System.Action`1<System.Boolean>)
extern "C"  void Biller_add_onTransactionsRestored_m3368693499 (Biller_t1615588570 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onTransactionsRestored(System.Action`1<System.Boolean>)
extern "C"  void Biller_remove_onTransactionsRestored_m3645294116 (Biller_t1615588570 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onPurchaseCancelled(System.Action`1<PurchasableItem>)
extern "C"  void Biller_add_onPurchaseCancelled_m2545642190 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onPurchaseCancelled(System.Action`1<PurchasableItem>)
extern "C"  void Biller_remove_onPurchaseCancelled_m4085519021 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onPurchaseRefunded(System.Action`1<PurchasableItem>)
extern "C"  void Biller_add_onPurchaseRefunded_m2697054608 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onPurchaseRefunded(System.Action`1<PurchasableItem>)
extern "C"  void Biller_remove_onPurchaseRefunded_m1923086229 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onPurchaseFailed(System.Action`1<PurchasableItem>)
extern "C"  void Biller_add_onPurchaseFailed_m2437644758 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onPurchaseFailed(System.Action`1<PurchasableItem>)
extern "C"  void Biller_remove_onPurchaseFailed_m3796217805 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::add_onPurchaseDeferred(System.Action`1<PurchasableItem>)
extern "C"  void Biller_add_onPurchaseDeferred_m3036785380 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::remove_onPurchaseDeferred(System.Action`1<PurchasableItem>)
extern "C"  void Biller_remove_onPurchaseDeferred_m1861602829 (Biller_t1615588570 * __this, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.BillerState Unibill.Biller::get_State()
extern "C"  int32_t Biller_get_State_m2290508785 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::set_State(Unibill.Impl.BillerState)
extern "C"  void Biller_set_State_m538899908 (Biller_t1615588570 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnibillError> Unibill.Biller::get_Errors()
extern "C"  List_1_t1122980919 * Biller_get_Errors_m3358685896 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::set_Errors(System.Collections.Generic.List`1<UnibillError>)
extern "C"  void Biller_set_Errors_m2519901667 (Biller_t1615588570 * __this, List_1_t1122980919 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Biller::get_Ready()
extern "C"  bool Biller_get_Ready_m2802980888 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Unibill.Biller::get_CurrencyIdentifiers()
extern "C"  StringU5BU5D_t1642385972* Biller_get_CurrencyIdentifiers_m133263467 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::Initialise()
extern "C"  void Biller_Initialise_m4112138135 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Biller::getPurchaseHistory(PurchasableItem)
extern "C"  int32_t Biller_getPurchaseHistory_m2048139902 (Biller_t1615588570 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Biller::getPurchaseHistory(System.String)
extern "C"  int32_t Biller_getPurchaseHistory_m3703773425 (Biller_t1615588570 * __this, String_t* ___purchasableId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Unibill.Biller::getCurrencyBalance(System.String)
extern "C"  Decimal_t724701077  Biller_getCurrencyBalance_m277350680 (Biller_t1615588570 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::creditCurrencyBalance(System.String,System.Decimal)
extern "C"  void Biller_creditCurrencyBalance_m2346379040 (Biller_t1615588570 * __this, String_t* ___identifier0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Biller::debitCurrencyBalance(System.String,System.Decimal)
extern "C"  bool Biller_debitCurrencyBalance_m726892875 (Biller_t1615588570 * __this, String_t* ___identifier0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::purchase(PurchasableItem,System.String)
extern "C"  void Biller_purchase_m305008582 (Biller_t1615588570 * __this, PurchasableItem_t3963353899 * ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::purchase(System.String,System.String)
extern "C"  void Biller_purchase_m4025737645 (Biller_t1615588570 * __this, String_t* ___purchasableId0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::restoreTransactions()
extern "C"  void Biller_restoreTransactions_m3411728989 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onPurchaseSucceeded(System.String,System.String)
extern "C"  void Biller_onPurchaseSucceeded_m99602967 (Biller_t1615588570 * __this, String_t* ___id0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onSetupComplete(System.Boolean)
extern "C"  void Biller_onSetupComplete_m1817548662 (Biller_t1615588570 * __this, bool ___available0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onPurchaseCancelledEvent(System.String)
extern "C"  void Biller_onPurchaseCancelledEvent_m3084596569 (Biller_t1615588570 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onPurchaseDeferredEvent(System.String)
extern "C"  void Biller_onPurchaseDeferredEvent_m4152530341 (Biller_t1615588570 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onPurchaseRefundedEvent(System.String)
extern "C"  void Biller_onPurchaseRefundedEvent_m2622892029 (Biller_t1615588570 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onPurchaseFailedEvent(System.String)
extern "C"  void Biller_onPurchaseFailedEvent_m3742909845 (Biller_t1615588570 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onTransactionsRestoredSuccess()
extern "C"  void Biller_onTransactionsRestoredSuccess_m2603018443 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::ClearPurchases()
extern "C"  void Biller_ClearPurchases_m2992179831 (Biller_t1615588570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onTransactionsRestoredFail(System.String)
extern "C"  void Biller_onTransactionsRestoredFail_m2866927008 (Biller_t1615588570 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Biller::isOwned(PurchasableItem)
extern "C"  bool Biller_isOwned_m3564916528 (Biller_t1615588570 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::setAppReceipt(System.String)
extern "C"  void Biller_setAppReceipt_m2769155303 (Biller_t1615588570 * __this, String_t* ___receipt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onActiveSubscriptionsRetrieved(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C"  void Biller_onActiveSubscriptionsRetrieved_m284539998 (Biller_t1615588570 * __this, Il2CppObject* ___subscriptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::logError(UnibillError)
extern "C"  void Biller_logError_m2421958687 (Biller_t1615588570 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::logError(UnibillError,System.Object[])
extern "C"  void Biller_logError_m1906067119 (Biller_t1615588570 * __this, int32_t ___error0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Biller::onPurchaseReceiptRetrieved(System.String,System.String)
extern "C"  void Biller_onPurchaseReceiptRetrieved_m2798008052 (Biller_t1615588570 * __this, String_t* ___platformSpecificItemId0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Biller::verifyPlatformId(System.String)
extern "C"  bool Biller_verifyPlatformId_m3989165441 (Biller_t1615588570 * __this, String_t* ___platformId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
