﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t1266514268;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICamera/GetKeyStateFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void GetKeyStateFunc__ctor_m1858103281 (GetKeyStateFunc_t1266514268 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/GetKeyStateFunc::Invoke(UnityEngine.KeyCode)
extern "C"  bool GetKeyStateFunc_Invoke_m3808364256 (GetKeyStateFunc_t1266514268 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICamera/GetKeyStateFunc::BeginInvoke(UnityEngine.KeyCode,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetKeyStateFunc_BeginInvoke_m3075688667 (GetKeyStateFunc_t1266514268 * __this, int32_t ___key0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/GetKeyStateFunc::EndInvoke(System.IAsyncResult)
extern "C"  bool GetKeyStateFunc_EndInvoke_m3010295507 (GetKeyStateFunc_t1266514268 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
