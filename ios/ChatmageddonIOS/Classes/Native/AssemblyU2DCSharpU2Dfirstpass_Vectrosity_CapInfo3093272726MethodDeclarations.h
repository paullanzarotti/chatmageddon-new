﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vectrosity.CapInfo
struct CapInfo_t3093272726;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_EndCap4262812865.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void Vectrosity.CapInfo::.ctor(Vectrosity.EndCap,UnityEngine.Material,UnityEngine.Texture2D,System.Single,System.Single,System.Single)
extern "C"  void CapInfo__ctor_m1728792067 (CapInfo_t3093272726 * __this, int32_t ___capType0, Material_t193706927 * ___material1, Texture2D_t3542995729 * ___texture2, float ___ratio13, float ___ratio24, float ___offset5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
