﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Unibill.Impl.BillingPlatform,System.Object,Unibill.Impl.BillingPlatform>
struct Transform_1_t1416689791;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Unibill.Impl.BillingPlatform,System.Object,Unibill.Impl.BillingPlatform>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1519785139_gshared (Transform_1_t1416689791 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1519785139(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1416689791 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1519785139_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Unibill.Impl.BillingPlatform,System.Object,Unibill.Impl.BillingPlatform>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1863359495_gshared (Transform_1_t1416689791 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1863359495(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t1416689791 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1863359495_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Unibill.Impl.BillingPlatform,System.Object,Unibill.Impl.BillingPlatform>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m485172880_gshared (Transform_1_t1416689791 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m485172880(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1416689791 *, int32_t, Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m485172880_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Unibill.Impl.BillingPlatform,System.Object,Unibill.Impl.BillingPlatform>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1065247657_gshared (Transform_1_t1416689791 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1065247657(__this, ___result0, method) ((  int32_t (*) (Transform_1_t1416689791 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1065247657_gshared)(__this, ___result0, method)
