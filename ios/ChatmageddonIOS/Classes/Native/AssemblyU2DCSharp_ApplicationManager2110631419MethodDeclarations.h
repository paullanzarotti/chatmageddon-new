﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApplicationManager
struct ApplicationManager_t2110631419;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Missile
struct Missile_t813944928;
// Friend
struct Friend_t3555014108;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;
// UISprite
struct UISprite_t603616735;
// System.Collections.Generic.List`1<UISprite>
struct List_1_t4267705163;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnchorPoint970610655.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_UILabel1795115428.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_UISprite603616735.h"

// System.Void ApplicationManager::.ctor()
extern "C"  void ApplicationManager__ctor_m953288780 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::Init()
extern "C"  void ApplicationManager_Init_m3162790976 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::LogoutOfApp()
extern "C"  void ApplicationManager_LogoutOfApp_m541204046 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::LogoutFromServer(System.Boolean)
extern "C"  void ApplicationManager_LogoutFromServer_m2479042082 (ApplicationManager_t2110631419 * __this, bool ___forced0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::ClearData()
extern "C"  void ApplicationManager_ClearData_m2456465683 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
extern "C"  void ApplicationManager_AddObjectToScene_m2917142854 (ApplicationManager_t2110631419 * __this, int32_t ___anchor0, GameObject_t1756533147 * ___iObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::CheckFirstPlayOfDay()
extern "C"  void ApplicationManager_CheckFirstPlayOfDay_m1041101535 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::DisplayFirstPlayDayToast()
extern "C"  void ApplicationManager_DisplayFirstPlayDayToast_m3696921867 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::ResetAttckStarts()
extern "C"  void ApplicationManager_ResetAttckStarts_m3048755929 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::ActivateAttackPanel(AttackNavScreen,Missile,Friend)
extern "C"  void ApplicationManager_ActivateAttackPanel_m2053542556 (ApplicationManager_t2110631419 * __this, int32_t ___startScreen0, Missile_t813944928 * ___weapon1, Friend_t3555014108 * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::ResetFiredMissile()
extern "C"  void ApplicationManager_ResetFiredMissile_m1227812411 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::UpdateAvatars(Friend)
extern "C"  void ApplicationManager_UpdateAvatars_m3701950467 (ApplicationManager_t2110631419 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ApplicationManager::IsDivisble(System.Int32,System.Int32)
extern "C"  bool ApplicationManager_IsDivisble_m4260811748 (ApplicationManager_t2110631419 * __this, int32_t ___x0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::ElipseUILabel(UILabel,System.Int32)
extern "C"  void ApplicationManager_ElipseUILabel_m3149932957 (ApplicationManager_t2110631419 * __this, UILabel_t1795115428 * ___label0, int32_t ___maxLabelWidth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ApplicationManager::CaptialiseString(System.String)
extern "C"  String_t* ApplicationManager_CaptialiseString_m3429704797 (ApplicationManager_t2110631419 * __this, String_t* ___stringToCapitalise0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ApplicationManager::GetAtlasString()
extern "C"  String_t* ApplicationManager_GetAtlasString_m765432559 (ApplicationManager_t2110631419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::SetAtlas(UISprite,System.String)
extern "C"  void ApplicationManager_SetAtlas_m2521262752 (ApplicationManager_t2110631419 * __this, UISprite_t603616735 * ___sprite0, String_t* ___atlasName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::SetAtlas(System.Collections.Generic.List`1<UISprite>,System.String)
extern "C"  void ApplicationManager_SetAtlas_m2218545386 (ApplicationManager_t2110631419 * __this, List_1_t4267705163 * ___sprites0, String_t* ___atlasName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ApplicationManager::ScaleAndHide(UnityEngine.GameObject[],System.Single)
extern "C"  Il2CppObject * ApplicationManager_ScaleAndHide_m2103992760 (ApplicationManager_t2110631419 * __this, GameObjectU5BU5D_t3057952154* ___gameobjects0, float ___scaleTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ApplicationManager::ScaleAndShow(UnityEngine.GameObject[],System.Single)
extern "C"  Il2CppObject * ApplicationManager_ScaleAndShow_m2716179447 (ApplicationManager_t2110631419 * __this, GameObjectU5BU5D_t3057952154* ___gameobjects0, float ___scaleTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
