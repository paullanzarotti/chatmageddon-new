﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TilesetFadeExampleItem
struct TilesetFadeExampleItem_t1982028563;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"
#include "UnityEngine_UnityEngine_Material193706927.h"

// System.Void TilesetFadeExampleItem::.ctor(OnlineMapsTile,UnityEngine.Material)
extern "C"  void TilesetFadeExampleItem__ctor_m2994230270 (TilesetFadeExampleItem_t1982028563 * __this, OnlineMapsTile_t21329940 * ___tile0, Material_t193706927 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TilesetFadeExampleItem::get_disposed()
extern "C"  bool TilesetFadeExampleItem_get_disposed_m833112756 (TilesetFadeExampleItem_t1982028563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetFadeExampleItem::Update()
extern "C"  void TilesetFadeExampleItem_Update_m757102203 (TilesetFadeExampleItem_t1982028563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetFadeExampleItem::.cctor()
extern "C"  void TilesetFadeExampleItem__cctor_m3818568301 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
