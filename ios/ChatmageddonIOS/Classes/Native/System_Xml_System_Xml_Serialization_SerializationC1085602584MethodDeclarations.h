﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.SerializationCodeGenerator
struct SerializationCodeGenerator_t1085602584;
// System.Xml.Serialization.XmlMapping[]
struct XmlMappingU5BU5D_t3375978586;
// System.Xml.Serialization.SerializerInfo
struct SerializerInfo_t3482254802;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Xml.Serialization.GenerationResult[]
struct GenerationResultU5BU5D_t2584896472;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Xml.Serialization.XmlMembersMapping
struct XmlMembersMapping_t1818148568;
// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t315595419;
// System.Xml.Serialization.XmlMapping
struct XmlMapping_t1597064667;
// System.Xml.Serialization.XmlTypeMapElementInfo
struct XmlTypeMapElementInfo_t3381496463;
// System.Xml.Serialization.XmlTypeMapMemberAnyElement
struct XmlTypeMapMemberAnyElement_t1726696875;
// System.Xml.Serialization.TypeData
struct TypeData_t3979356678;
// System.Xml.Serialization.ListMap
struct ListMap_t1787375712;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Serialization.XmlTypeMapMember
struct XmlTypeMapMember_t3057402259;
// System.Xml.Serialization.ClassMap
struct ClassMap_t1647926812;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_SerializerInfo3482254802.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_Serialization_XmlMembersMapp1818148568.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapping315595419.h"
#include "System_Xml_System_Xml_Serialization_XmlMapping1597064667.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapElem3381496463.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1726696875.h"
#include "System_Xml_System_Xml_Serialization_TypeData3979356678.h"
#include "System_Xml_System_Xml_Serialization_ListMap1787375712.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3057402259.h"
#include "System_Xml_System_Xml_Serialization_ClassMap1647926812.h"
#include "System_Xml_System_Xml_Serialization_HookType2601538049.h"
#include "System_Xml_System_Xml_Serialization_XmlMappingAcce3616682003.h"
#include "System_Xml_System_Xml_Serialization_HookAction2799868295.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Xml.Serialization.SerializationCodeGenerator::.ctor(System.Xml.Serialization.XmlMapping[])
extern "C"  void SerializationCodeGenerator__ctor_m3497993604 (SerializationCodeGenerator_t1085602584 * __this, XmlMappingU5BU5D_t3375978586* ___xmlMaps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::.ctor(System.Xml.Serialization.XmlMapping[],System.Xml.Serialization.SerializerInfo)
extern "C"  void SerializationCodeGenerator__ctor_m1611803114 (SerializationCodeGenerator_t1085602584 * __this, XmlMappingU5BU5D_t3375978586* ___xmlMaps0, SerializerInfo_t3482254802 * ___config1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateSerializers(System.IO.TextWriter)
extern "C"  void SerializationCodeGenerator_GenerateSerializers_m1946759998 (SerializationCodeGenerator_t1085602584 * __this, TextWriter_t4027217640 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Serialization.GenerationResult[] System.Xml.Serialization.SerializationCodeGenerator::get_GenerationResults()
extern "C"  GenerationResultU5BU5D_t2584896472* SerializationCodeGenerator_get_GenerationResults_m4108499888 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Xml.Serialization.SerializationCodeGenerator::get_ReferencedTypes()
extern "C"  ArrayList_t4252133567 * SerializationCodeGenerator_get_ReferencedTypes_m708088882 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::UpdateGeneratedTypes(System.Collections.ArrayList)
extern "C"  void SerializationCodeGenerator_UpdateGeneratedTypes_m689099551 (SerializationCodeGenerator_t1085602584 * __this, ArrayList_t4252133567 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::ToCSharpFullName(System.Type)
extern "C"  String_t* SerializationCodeGenerator_ToCSharpFullName_m1142359623 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateContract(System.Collections.ArrayList)
extern "C"  void SerializationCodeGenerator_GenerateContract_m157427327 (SerializationCodeGenerator_t1085602584 * __this, ArrayList_t4252133567 * ___generatedMaps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriter(System.String,System.Collections.ArrayList)
extern "C"  void SerializationCodeGenerator_GenerateWriter_m2876707262 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___writerClassName0, ArrayList_t4252133567 * ___maps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteRoot()
extern "C"  void SerializationCodeGenerator_GenerateWriteRoot_m1930646549 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteMessage(System.Xml.Serialization.XmlMembersMapping)
extern "C"  void SerializationCodeGenerator_GenerateWriteMessage_m144538768 (SerializationCodeGenerator_t1085602584 * __this, XmlMembersMapping_t1818148568 * ___membersMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateGetXmlEnumValue(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_GenerateGetXmlEnumValue_m2367843096 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___map0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteObject(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_GenerateWriteObject_m2072139487 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteObjectElement(System.Xml.Serialization.XmlMapping,System.String,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateWriteObjectElement_m4018858300 (SerializationCodeGenerator_t1085602584 * __this, XmlMapping_t1597064667 * ___xmlMap0, String_t* ___ob1, bool ___isValueList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteMemberElement(System.Xml.Serialization.XmlTypeMapElementInfo,System.String)
extern "C"  void SerializationCodeGenerator_GenerateWriteMemberElement_m775498382 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapElementInfo_t3381496463 * ___elem0, String_t* ___memberValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteListElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateWriteListElement_m298751486 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteAnyElementContent(System.Xml.Serialization.XmlTypeMapMemberAnyElement,System.String)
extern "C"  void SerializationCodeGenerator_GenerateWriteAnyElementContent_m599426125 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapMemberAnyElement_t1726696875 * ___member0, String_t* ___memberValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWritePrimitiveElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateWritePrimitiveElement_m2790838879 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteEnumElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateWriteEnumElement_m2204195789 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateGetStringValue(System.Xml.Serialization.XmlTypeMapping,System.Xml.Serialization.TypeData,System.String,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateGetStringValue_m346450415 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, TypeData_t3979356678 * ___type1, String_t* ___value2, bool ___isNullable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateGetEnumXmlValue(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  String_t* SerializationCodeGenerator_GenerateGetEnumXmlValue_m1796742319 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateGetListCount(System.Xml.Serialization.TypeData,System.String)
extern "C"  String_t* SerializationCodeGenerator_GenerateGetListCount_m436509966 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___listType0, String_t* ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateGetArrayType(System.Xml.Serialization.ListMap,System.String,System.String&,System.String&)
extern "C"  void SerializationCodeGenerator_GenerateGetArrayType_m3171588501 (SerializationCodeGenerator_t1085602584 * __this, ListMap_t1787375712 * ___map0, String_t* ___itemCount1, String_t** ___localName2, String_t** ___ns3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteListContent(System.String,System.Xml.Serialization.TypeData,System.Xml.Serialization.ListMap,System.String,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateWriteListContent_m326347450 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___container0, TypeData_t3979356678 * ___listType1, ListMap_t1787375712 * ___map2, String_t* ___ob3, bool ___writeToString4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateListLoop(System.String,System.Xml.Serialization.ListMap,System.String,System.String,System.Xml.Serialization.TypeData,System.String)
extern "C"  void SerializationCodeGenerator_GenerateListLoop_m3153675682 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___container0, ListMap_t1787375712 * ___map1, String_t* ___item2, String_t* ___index3, TypeData_t3979356678 * ___itemTypeData4, String_t* ___targetString5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWritePrimitiveValueLiteral(System.String,System.String,System.String,System.Xml.Serialization.XmlTypeMapping,System.Xml.Serialization.TypeData,System.Boolean,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateWritePrimitiveValueLiteral_m2563396641 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___memberValue0, String_t* ___name1, String_t* ___ns2, XmlTypeMapping_t315595419 * ___mappedType3, TypeData_t3979356678 * ___typeData4, bool ___wrapped5, bool ___isNullable6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWritePrimitiveValueEncoded(System.String,System.String,System.String,System.Xml.XmlQualifiedName,System.Xml.Serialization.XmlTypeMapping,System.Xml.Serialization.TypeData,System.Boolean,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateWritePrimitiveValueEncoded_m2594250492 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___memberValue0, String_t* ___name1, String_t* ___ns2, XmlQualifiedName_t1944712516 * ___xsiType3, XmlTypeMapping_t315595419 * ___mappedType4, TypeData_t3979356678 * ___typeData5, bool ___wrapped6, bool ___isNullable7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateGetMemberValue(System.Xml.Serialization.XmlTypeMapMember,System.String,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateGetMemberValue_m1059783034 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapMember_t3057402259 * ___member0, String_t* ___ob1, bool ___isValueList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateMemberHasValueCondition(System.Xml.Serialization.XmlTypeMapMember,System.String,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateMemberHasValueCondition_m2662338029 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapMember_t3057402259 * ___member0, String_t* ___ob1, bool ___isValueList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteInitCallbacks()
extern "C"  void SerializationCodeGenerator_GenerateWriteInitCallbacks_m2372915959 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteWriteEnumCallback(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_WriteWriteEnumCallback_m3002217804 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___map0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteWriteObjectCallback(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_WriteWriteObjectCallback_m4148105290 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___map0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReader(System.String,System.Collections.ArrayList)
extern "C"  void SerializationCodeGenerator_GenerateReader_m3358063362 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___readerClassName0, ArrayList_t4252133567 * ___maps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadRoot()
extern "C"  void SerializationCodeGenerator_GenerateReadRoot_m3711232202 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateReadMessage(System.Xml.Serialization.XmlMembersMapping)
extern "C"  String_t* SerializationCodeGenerator_GenerateReadMessage_m3098323934 (SerializationCodeGenerator_t1085602584 * __this, XmlMembersMapping_t1818148568 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadObject(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_GenerateReadObject_m1841402208 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadClassInstance(System.Xml.Serialization.XmlTypeMapping,System.String,System.String)
extern "C"  void SerializationCodeGenerator_GenerateReadClassInstance_m3632740538 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___isNullable1, String_t* ___checkType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadMembers(System.Xml.Serialization.XmlMapping,System.Xml.Serialization.ClassMap,System.String,System.Boolean,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateReadMembers_m1442021260 (SerializationCodeGenerator_t1085602584 * __this, XmlMapping_t1597064667 * ___xmlMap0, ClassMap_t1647926812 * ___map1, String_t* ___ob2, bool ___isValueList3, bool ___readByOrder4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadAttributeMembers(System.Xml.Serialization.XmlMapping,System.Xml.Serialization.ClassMap,System.String,System.Boolean,System.Boolean&)
extern "C"  void SerializationCodeGenerator_GenerateReadAttributeMembers_m2570545918 (SerializationCodeGenerator_t1085602584 * __this, XmlMapping_t1597064667 * ___xmlMap0, ClassMap_t1647926812 * ___map1, String_t* ___ob2, bool ___isValueList3, bool* ___first4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateSetListMembersDefaults(System.Xml.Serialization.XmlTypeMapping,System.Xml.Serialization.ClassMap,System.String,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateSetListMembersDefaults_m3780994267 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, ClassMap_t1647926812 * ___map1, String_t* ___ob2, bool ___isValueList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::IsReadOnly(System.Xml.Serialization.XmlTypeMapping,System.Xml.Serialization.XmlTypeMapMember,System.Xml.Serialization.TypeData,System.Boolean)
extern "C"  bool SerializationCodeGenerator_IsReadOnly_m3538573092 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___map0, XmlTypeMapMember_t3057402259 * ___member1, TypeData_t3979356678 * ___memType2, bool ___isValueList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateSetMemberValue(System.Xml.Serialization.XmlTypeMapMember,System.String,System.String,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateSetMemberValue_m3387830817 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapMember_t3057402259 * ___member0, String_t* ___ob1, String_t* ___value2, bool ___isValueList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateSetMemberValueFromAttr(System.Xml.Serialization.XmlTypeMapMember,System.String,System.String,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateSetMemberValueFromAttr_m1767357426 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapMember_t3057402259 * ___member0, String_t* ___ob1, String_t* ___value2, bool ___isValueList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateReadObjectElement(System.Xml.Serialization.XmlTypeMapElementInfo)
extern "C"  String_t* SerializationCodeGenerator_GenerateReadObjectElement_m3692722075 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapElementInfo_t3381496463 * ___elem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateReadPrimitiveValue(System.Xml.Serialization.XmlTypeMapElementInfo)
extern "C"  String_t* SerializationCodeGenerator_GenerateReadPrimitiveValue_m326517252 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapElementInfo_t3381496463 * ___elem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateGetValueFromXmlString(System.String,System.Xml.Serialization.TypeData,System.Xml.Serialization.XmlTypeMapping,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateGetValueFromXmlString_m3202188178 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___value0, TypeData_t3979356678 * ___typeData1, XmlTypeMapping_t315595419 * ___typeMap2, bool ___isNullable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateReadListElement(System.Xml.Serialization.XmlTypeMapping,System.String,System.String,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateReadListElement_m2646307975 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___list1, String_t* ___isNullable2, bool ___canCreateInstance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateReadListString(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  String_t* SerializationCodeGenerator_GenerateReadListString_m354130073 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetArrayDeclaration(System.Type,System.String)
extern "C"  String_t* SerializationCodeGenerator_GetArrayDeclaration_m1933690112 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___type0, String_t* ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateAddListValue(System.Xml.Serialization.TypeData,System.String,System.String,System.String,System.Boolean)
extern "C"  void SerializationCodeGenerator_GenerateAddListValue_m2810293391 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___listType0, String_t* ___list1, String_t* ___index2, String_t* ___value3, bool ___canCreateInstance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateCreateList(System.Type)
extern "C"  String_t* SerializationCodeGenerator_GenerateCreateList_m234379498 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateInitializeList(System.Xml.Serialization.TypeData)
extern "C"  String_t* SerializationCodeGenerator_GenerateInitializeList_m437417153 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateFillerCallbacks()
extern "C"  void SerializationCodeGenerator_GenerateFillerCallbacks_m2110732974 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadXmlNodeElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateReadXmlNodeElement_m1346522408 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___isNullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadPrimitiveElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateReadPrimitiveElement_m4150987340 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___isNullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadEnumElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateReadEnumElement_m1220677234 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___isNullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GenerateGetEnumValue(System.Xml.Serialization.XmlTypeMapping,System.String,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GenerateGetEnumValue_m188617945 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___val1, bool ___isNullable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateGetEnumValueMethod(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_GenerateGetEnumValueMethod_m1383051540 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateGetSingleEnumValue(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateGetSingleEnumValue_m239751105 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadXmlSerializableElement(System.Xml.Serialization.XmlTypeMapping,System.String)
extern "C"  void SerializationCodeGenerator_GenerateReadXmlSerializableElement_m2426216965 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___isNullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateReadInitCallbacks()
extern "C"  void SerializationCodeGenerator_GenerateReadInitCallbacks_m1774107534 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateFixupCallbacks()
extern "C"  void SerializationCodeGenerator_GenerateFixupCallbacks_m1657623044 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetReadXmlNode(System.Xml.Serialization.TypeData,System.Boolean)
extern "C"  String_t* SerializationCodeGenerator_GetReadXmlNode_m2840230772 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___type0, bool ___wrapped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::InitHooks()
extern "C"  void SerializationCodeGenerator_InitHooks_m1630491459 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::PushHookContext()
extern "C"  void SerializationCodeGenerator_PushHookContext_m4054291771 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::PopHookContext()
extern "C"  void SerializationCodeGenerator_PopHookContext_m1916927086 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::SetHookVar(System.String,System.String)
extern "C"  void SerializationCodeGenerator_SetHookVar_m1308218273 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___var0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateReadHook(System.Xml.Serialization.HookType,System.Type)
extern "C"  bool SerializationCodeGenerator_GenerateReadHook_m1935076135 (SerializationCodeGenerator_t1085602584 * __this, int32_t ___hookType0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteHook(System.Xml.Serialization.HookType,System.Type)
extern "C"  bool SerializationCodeGenerator_GenerateWriteHook_m1496945652 (SerializationCodeGenerator_t1085602584 * __this, int32_t ___hookType0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateWriteMemberHook(System.Type,System.Xml.Serialization.XmlTypeMapMember)
extern "C"  bool SerializationCodeGenerator_GenerateWriteMemberHook_m1665342854 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___type0, XmlTypeMapMember_t3057402259 * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateReadMemberHook(System.Type,System.Xml.Serialization.XmlTypeMapMember)
extern "C"  bool SerializationCodeGenerator_GenerateReadMemberHook_m2671812957 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___type0, XmlTypeMapMember_t3057402259 * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateReadArrayMemberHook(System.Type,System.Xml.Serialization.XmlTypeMapMember,System.String)
extern "C"  bool SerializationCodeGenerator_GenerateReadArrayMemberHook_m1977001864 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___type0, XmlTypeMapMember_t3057402259 * ___member1, String_t* ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::MemberHasReadReplaceHook(System.Type,System.Xml.Serialization.XmlTypeMapMember)
extern "C"  bool SerializationCodeGenerator_MemberHasReadReplaceHook_m3973217112 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___type0, XmlTypeMapMember_t3057402259 * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateHook(System.Xml.Serialization.HookType,System.Xml.Serialization.XmlMappingAccess,System.Type,System.String)
extern "C"  bool SerializationCodeGenerator_GenerateHook_m3294783756 (SerializationCodeGenerator_t1085602584 * __this, int32_t ___hookType0, int32_t ___dir1, Type_t * ___type2, String_t* ___member3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::GenerateEndHook()
extern "C"  void SerializationCodeGenerator_GenerateEndHook_m703738794 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.SerializationCodeGenerator::GenerateHooks(System.Xml.Serialization.HookType,System.Xml.Serialization.XmlMappingAccess,System.Type,System.String,System.Xml.Serialization.HookAction)
extern "C"  bool SerializationCodeGenerator_GenerateHooks_m2974118602 (SerializationCodeGenerator_t1085602584 * __this, int32_t ___hookType0, int32_t ___dir1, Type_t * ___type2, String_t* ___member3, int32_t ___action4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetRootTypeName()
extern "C"  String_t* SerializationCodeGenerator_GetRootTypeName_m4118143839 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetNumTempVar()
extern "C"  String_t* SerializationCodeGenerator_GetNumTempVar_m2249994893 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetObTempVar()
extern "C"  String_t* SerializationCodeGenerator_GetObTempVar_m1054296102 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetStrTempVar()
extern "C"  String_t* SerializationCodeGenerator_GetStrTempVar_m1232415702 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetBoolTempVar()
extern "C"  String_t* SerializationCodeGenerator_GetBoolTempVar_m3001193841 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetUniqueName(System.String,System.Object,System.String)
extern "C"  String_t* SerializationCodeGenerator_GetUniqueName_m3759879820 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___uniqueGroup0, Il2CppObject * ___ob1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::RegisterReferencingMap(System.Xml.Serialization.XmlTypeMapping)
extern "C"  void SerializationCodeGenerator_RegisterReferencingMap_m519450419 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetWriteObjectName(System.Xml.Serialization.XmlTypeMapping)
extern "C"  String_t* SerializationCodeGenerator_GetWriteObjectName_m1648260168 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetReadObjectName(System.Xml.Serialization.XmlTypeMapping)
extern "C"  String_t* SerializationCodeGenerator_GetReadObjectName_m2275546099 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetGetEnumValueName(System.Xml.Serialization.XmlTypeMapping)
extern "C"  String_t* SerializationCodeGenerator_GetGetEnumValueName_m1516173248 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetWriteObjectCallbackName(System.Xml.Serialization.XmlTypeMapping)
extern "C"  String_t* SerializationCodeGenerator_GetWriteObjectCallbackName_m2420701689 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetFixupCallbackName(System.Xml.Serialization.XmlMapping)
extern "C"  String_t* SerializationCodeGenerator_GetFixupCallbackName_m28440259 (SerializationCodeGenerator_t1085602584 * __this, XmlMapping_t1597064667 * ___typeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetUniqueClassName(System.String)
extern "C"  String_t* SerializationCodeGenerator_GetUniqueClassName_m3268280414 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetReadObjectCall(System.Xml.Serialization.XmlTypeMapping,System.String,System.String)
extern "C"  String_t* SerializationCodeGenerator_GetReadObjectCall_m1666741654 (SerializationCodeGenerator_t1085602584 * __this, XmlTypeMapping_t315595419 * ___typeMap0, String_t* ___isNullable1, String_t* ___checkType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetFillListName(System.Xml.Serialization.TypeData)
extern "C"  String_t* SerializationCodeGenerator_GetFillListName_m898221070 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___td0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetCast(System.Xml.Serialization.TypeData,System.Xml.Serialization.TypeData,System.String)
extern "C"  String_t* SerializationCodeGenerator_GetCast_m3445720341 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___td0, TypeData_t3979356678 * ___tdval1, String_t* ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetCast(System.Xml.Serialization.TypeData,System.String)
extern "C"  String_t* SerializationCodeGenerator_GetCast_m1407687973 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___td0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetCast(System.Type,System.String)
extern "C"  String_t* SerializationCodeGenerator_GetCast_m519852940 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___td0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetTypeOf(System.Xml.Serialization.TypeData)
extern "C"  String_t* SerializationCodeGenerator_GetTypeOf_m2022445519 (SerializationCodeGenerator_t1085602584 * __this, TypeData_t3979356678 * ___td0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetTypeOf(System.Type)
extern "C"  String_t* SerializationCodeGenerator_GetTypeOf_m3393720254 (SerializationCodeGenerator_t1085602584 * __this, Type_t * ___td0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::GetLiteral(System.Object)
extern "C"  String_t* SerializationCodeGenerator_GetLiteral_m547665067 (SerializationCodeGenerator_t1085602584 * __this, Il2CppObject * ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteLineInd(System.String)
extern "C"  void SerializationCodeGenerator_WriteLineInd_m3192867135 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteLineUni(System.String)
extern "C"  void SerializationCodeGenerator_WriteLineUni_m252856522 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::Write(System.String)
extern "C"  void SerializationCodeGenerator_Write_m3071336596 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteUni(System.String)
extern "C"  void SerializationCodeGenerator_WriteUni_m718738484 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteLine(System.String)
extern "C"  void SerializationCodeGenerator_WriteLine_m1204859350 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteMultilineCode(System.String)
extern "C"  void SerializationCodeGenerator_WriteMultilineCode_m170859096 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.SerializationCodeGenerator::Params(System.String[])
extern "C"  String_t* SerializationCodeGenerator_Params_m4122478806 (SerializationCodeGenerator_t1085602584 * __this, StringU5BU5D_t1642385972* ___pars0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::WriteMetCall(System.String,System.String[])
extern "C"  void SerializationCodeGenerator_WriteMetCall_m4183199420 (SerializationCodeGenerator_t1085602584 * __this, String_t* ___method0, StringU5BU5D_t1642385972* ___pars1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.SerializationCodeGenerator::Unindent()
extern "C"  void SerializationCodeGenerator_Unindent_m2312405848 (SerializationCodeGenerator_t1085602584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
