﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchUIScaler
struct AttackSearchUIScaler_t3747490330;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackSearchUIScaler::.ctor()
extern "C"  void AttackSearchUIScaler__ctor_m2112096583 (AttackSearchUIScaler_t3747490330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchUIScaler::GlobalUIScale()
extern "C"  void AttackSearchUIScaler_GlobalUIScale_m2483849116 (AttackSearchUIScaler_t3747490330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
