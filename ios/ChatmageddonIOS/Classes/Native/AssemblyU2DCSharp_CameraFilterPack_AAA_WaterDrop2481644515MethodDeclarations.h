﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_AAA_WaterDrop
struct CameraFilterPack_AAA_WaterDrop_t2481644515;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_AAA_WaterDrop::.ctor()
extern "C"  void CameraFilterPack_AAA_WaterDrop__ctor_m771023386 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_AAA_WaterDrop::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_AAA_WaterDrop_get_material_m2805606135 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::Start()
extern "C"  void CameraFilterPack_AAA_WaterDrop_Start_m890526094 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_AAA_WaterDrop_OnRenderImage_m3609347102 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::OnValidate()
extern "C"  void CameraFilterPack_AAA_WaterDrop_OnValidate_m1754993565 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::Update()
extern "C"  void CameraFilterPack_AAA_WaterDrop_Update_m1866908011 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::OnDisable()
extern "C"  void CameraFilterPack_AAA_WaterDrop_OnDisable_m3163673515 (CameraFilterPack_AAA_WaterDrop_t2481644515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
