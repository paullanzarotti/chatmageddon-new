﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ForgotPasswordButton
struct ForgotPasswordButton_t3035499862;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ForgotPasswordButton::.ctor()
extern "C"  void ForgotPasswordButton__ctor_m2346802769 (ForgotPasswordButton_t3035499862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordButton::Awake()
extern "C"  void ForgotPasswordButton_Awake_m456980278 (ForgotPasswordButton_t3035499862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordButton::OnButtonClick()
extern "C"  void ForgotPasswordButton_OnButtonClick_m1527627690 (ForgotPasswordButton_t3035499862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordButton::TextEntered(System.String)
extern "C"  void ForgotPasswordButton_TextEntered_m3532597391 (ForgotPasswordButton_t3035499862 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordButton::PromptCancelled()
extern "C"  void ForgotPasswordButton_PromptCancelled_m1916850848 (ForgotPasswordButton_t3035499862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordButton::<TextEntered>m__0(System.Boolean,System.String)
extern "C"  void ForgotPasswordButton_U3CTextEnteredU3Em__0_m1877107479 (Il2CppObject * __this /* static, unused */, bool ___success0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
