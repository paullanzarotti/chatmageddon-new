template <typename T> void RegisterClass();
template <typename T> void RegisterStrippedTypeInfo(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_ParticlesLegacy();
	RegisterModule_ParticlesLegacy();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_Terrain();
	RegisterModule_Terrain();

	void RegisterModule_TerrainPhysics();
	RegisterModule_TerrainPhysics();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

}

class EditorExtension; template <> void RegisterClass<EditorExtension>();
namespace Unity { class Component; } template <> void RegisterClass<Unity::Component>();
class Behaviour; template <> void RegisterClass<Behaviour>();
class Animation; template <> void RegisterClass<Animation>();
class AudioBehaviour; template <> void RegisterClass<AudioBehaviour>();
class AudioListener; template <> void RegisterClass<AudioListener>();
class AudioSource; template <> void RegisterClass<AudioSource>();
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterClass<Camera>();
namespace UI { class Canvas; } template <> void RegisterClass<UI::Canvas>();
namespace UI { class CanvasGroup; } template <> void RegisterClass<UI::CanvasGroup>();
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterClass<Collider2D>();
class BoxCollider2D; template <> void RegisterClass<BoxCollider2D>();
class CapsuleCollider2D; 
class CircleCollider2D; 
class EdgeCollider2D; template <> void RegisterClass<EdgeCollider2D>();
class PolygonCollider2D; template <> void RegisterClass<PolygonCollider2D>();
class ConstantForce; 
class DirectorPlayer; template <> void RegisterClass<DirectorPlayer>();
class Animator; template <> void RegisterClass<Animator>();
class Effector2D; 
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; 
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; template <> void RegisterClass<FlareLayer>();
class GUIElement; template <> void RegisterClass<GUIElement>();
namespace TextRenderingPrivate { class GUIText; } 
class GUITexture; template <> void RegisterClass<GUITexture>();
class GUILayer; template <> void RegisterClass<GUILayer>();
class Halo; 
class HaloLayer; 
class Joint2D; 
class AnchoredJoint2D; 
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; 
class SliderJoint2D; 
class SpringJoint2D; 
class WheelJoint2D; 
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterClass<Light>();
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterClass<MonoBehaviour>();
class NavMeshAgent; 
class NavMeshObstacle; 
class NetworkView; template <> void RegisterClass<NetworkView>();
class OffMeshLink; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class Projector; 
class ReflectionProbe; 
class Skybox; template <> void RegisterClass<Skybox>();
class Terrain; template <> void RegisterClass<Terrain>();
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterClass<UI::CanvasRenderer>();
class Collider; template <> void RegisterClass<Collider>();
class BoxCollider; template <> void RegisterClass<BoxCollider>();
class CapsuleCollider; 
class CharacterController; 
class MeshCollider; template <> void RegisterClass<MeshCollider>();
class SphereCollider; 
class TerrainCollider; template <> void RegisterClass<TerrainCollider>();
class WheelCollider; 
namespace Unity { class Joint; } 
namespace Unity { class CharacterJoint; } 
namespace Unity { class ConfigurableJoint; } 
namespace Unity { class FixedJoint; } 
namespace Unity { class HingeJoint; } 
namespace Unity { class SpringJoint; } 
class LODGroup; 
class MeshFilter; template <> void RegisterClass<MeshFilter>();
class OcclusionArea; 
class OcclusionPortal; 
class ParticleAnimator; template <> void RegisterClass<ParticleAnimator>();
class ParticleEmitter; template <> void RegisterClass<ParticleEmitter>();
class EllipsoidParticleEmitter; template <> void RegisterClass<EllipsoidParticleEmitter>();
class MeshParticleEmitter; 
class ParticleSystem; 
class Renderer; template <> void RegisterClass<Renderer>();
class BillboardRenderer; 
class LineRenderer; 
class MeshRenderer; template <> void RegisterClass<MeshRenderer>();
class ParticleRenderer; template <> void RegisterClass<ParticleRenderer>();
class ParticleSystemRenderer; 
class SkinnedMeshRenderer; 
class SpriteRenderer; template <> void RegisterClass<SpriteRenderer>();
class TrailRenderer; template <> void RegisterClass<TrailRenderer>();
class Rigidbody; template <> void RegisterClass<Rigidbody>();
class Rigidbody2D; template <> void RegisterClass<Rigidbody2D>();
namespace TextRenderingPrivate { class TextMesh; } 
class Transform; template <> void RegisterClass<Transform>();
namespace UI { class RectTransform; } template <> void RegisterClass<UI::RectTransform>();
class Tree; 
class WorldAnchor; 
class WorldParticleCollider; 
class GameObject; template <> void RegisterClass<GameObject>();
class NamedObject; template <> void RegisterClass<NamedObject>();
class AssetBundle; template <> void RegisterClass<AssetBundle>();
class AssetBundleManifest; 
class AudioMixer; 
class AudioMixerController; 
class AudioMixerGroup; 
class AudioMixerGroupController; 
class AudioMixerSnapshot; 
class AudioMixerSnapshotController; 
class Avatar; template <> void RegisterClass<Avatar>();
class BillboardAsset; 
class ComputeShader; 
class Flare; 
namespace TextRendering { class Font; } template <> void RegisterClass<TextRendering::Font>();
class LightProbes; 
class Material; template <> void RegisterClass<Material>();
class ProceduralMaterial; 
class Mesh; template <> void RegisterClass<Mesh>();
class Motion; template <> void RegisterClass<Motion>();
class AnimationClip; template <> void RegisterClass<AnimationClip>();
class NavMeshData; 
class OcclusionCullingData; 
class PhysicMaterial; 
class PhysicsMaterial2D; template <> void RegisterClass<PhysicsMaterial2D>();
class PreloadData; template <> void RegisterClass<PreloadData>();
class RuntimeAnimatorController; template <> void RegisterClass<RuntimeAnimatorController>();
class AnimatorController; 
class AnimatorOverrideController; 
class SampleClip; template <> void RegisterClass<SampleClip>();
class AudioClip; template <> void RegisterClass<AudioClip>();
class Shader; template <> void RegisterClass<Shader>();
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterClass<Sprite>();
class SubstanceArchive; 
class TerrainData; template <> void RegisterClass<TerrainData>();
class TextAsset; template <> void RegisterClass<TextAsset>();
class CGProgram; 
class MonoScript; template <> void RegisterClass<MonoScript>();
class Texture; template <> void RegisterClass<Texture>();
class BaseVideoTexture; 
class MovieTexture; 
class CubemapArray; 
class ProceduralTexture; 
class RenderTexture; template <> void RegisterClass<RenderTexture>();
class SparseTexture; 
class Texture2D; template <> void RegisterClass<Texture2D>();
class Cubemap; template <> void RegisterClass<Cubemap>();
class Texture2DArray; template <> void RegisterClass<Texture2DArray>();
class Texture3D; template <> void RegisterClass<Texture3D>();
class WebCamTexture; 
class GameManager; template <> void RegisterClass<GameManager>();
class GlobalGameManager; template <> void RegisterClass<GlobalGameManager>();
class AudioManager; template <> void RegisterClass<AudioManager>();
class BuildSettings; template <> void RegisterClass<BuildSettings>();
class CloudWebServicesManager; 
class CrashReportManager; 
class DelayedCallManager; template <> void RegisterClass<DelayedCallManager>();
class GraphicsSettings; template <> void RegisterClass<GraphicsSettings>();
class InputManager; template <> void RegisterClass<InputManager>();
class MasterServerInterface; template <> void RegisterClass<MasterServerInterface>();
class MonoManager; template <> void RegisterClass<MonoManager>();
class NavMeshProjectSettings; 
class NetworkManager; template <> void RegisterClass<NetworkManager>();
class Physics2DSettings; template <> void RegisterClass<Physics2DSettings>();
class PhysicsManager; template <> void RegisterClass<PhysicsManager>();
class PlayerSettings; template <> void RegisterClass<PlayerSettings>();
class QualitySettings; template <> void RegisterClass<QualitySettings>();
class ResourceManager; template <> void RegisterClass<ResourceManager>();
class RuntimeInitializeOnLoadManager; template <> void RegisterClass<RuntimeInitializeOnLoadManager>();
class ScriptMapper; template <> void RegisterClass<ScriptMapper>();
class TagManager; template <> void RegisterClass<TagManager>();
class TimeManager; template <> void RegisterClass<TimeManager>();
class UnityAdsManager; 
class UnityAnalyticsManager; 
class UnityConnectSettings; 
class LevelGameManager; template <> void RegisterClass<LevelGameManager>();
class LightmapSettings; template <> void RegisterClass<LightmapSettings>();
class NavMeshSettings; 
class OcclusionCullingSettings; 
class RenderSettings; template <> void RegisterClass<RenderSettings>();
class NScreenBridge; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 90 non stripped classes
	//0. AssetBundle
	RegisterClass<AssetBundle>();
	//1. NamedObject
	RegisterClass<NamedObject>();
	//2. EditorExtension
	RegisterClass<EditorExtension>();
	//3. Behaviour
	RegisterClass<Behaviour>();
	//4. Unity::Component
	RegisterClass<Unity::Component>();
	//5. Camera
	RegisterClass<Camera>();
	//6. GameObject
	RegisterClass<GameObject>();
	//7. RenderSettings
	RegisterClass<RenderSettings>();
	//8. LevelGameManager
	RegisterClass<LevelGameManager>();
	//9. GameManager
	RegisterClass<GameManager>();
	//10. QualitySettings
	RegisterClass<QualitySettings>();
	//11. GlobalGameManager
	RegisterClass<GlobalGameManager>();
	//12. MeshFilter
	RegisterClass<MeshFilter>();
	//13. Renderer
	RegisterClass<Renderer>();
	//14. Skybox
	RegisterClass<Skybox>();
	//15. GUIElement
	RegisterClass<GUIElement>();
	//16. GUITexture
	RegisterClass<GUITexture>();
	//17. GUILayer
	RegisterClass<GUILayer>();
	//18. Light
	RegisterClass<Light>();
	//19. Mesh
	RegisterClass<Mesh>();
	//20. MonoBehaviour
	RegisterClass<MonoBehaviour>();
	//21. NetworkView
	RegisterClass<NetworkView>();
	//22. UI::RectTransform
	RegisterClass<UI::RectTransform>();
	//23. Transform
	RegisterClass<Transform>();
	//24. Shader
	RegisterClass<Shader>();
	//25. Material
	RegisterClass<Material>();
	//26. Sprite
	RegisterClass<Sprite>();
	//27. SpriteRenderer
	RegisterClass<SpriteRenderer>();
	//28. TextAsset
	RegisterClass<TextAsset>();
	//29. Texture
	RegisterClass<Texture>();
	//30. Texture2D
	RegisterClass<Texture2D>();
	//31. Texture3D
	RegisterClass<Texture3D>();
	//32. RenderTexture
	RegisterClass<RenderTexture>();
	//33. Rigidbody
	RegisterClass<Rigidbody>();
	//34. Collider
	RegisterClass<Collider>();
	//35. BoxCollider
	RegisterClass<BoxCollider>();
	//36. MeshCollider
	RegisterClass<MeshCollider>();
	//37. Collider2D
	RegisterClass<Collider2D>();
	//38. BoxCollider2D
	RegisterClass<BoxCollider2D>();
	//39. EdgeCollider2D
	RegisterClass<EdgeCollider2D>();
	//40. PolygonCollider2D
	RegisterClass<PolygonCollider2D>();
	//41. AudioClip
	RegisterClass<AudioClip>();
	//42. SampleClip
	RegisterClass<SampleClip>();
	//43. AudioSource
	RegisterClass<AudioSource>();
	//44. AudioBehaviour
	RegisterClass<AudioBehaviour>();
	//45. Animation
	RegisterClass<Animation>();
	//46. Animator
	RegisterClass<Animator>();
	//47. DirectorPlayer
	RegisterClass<DirectorPlayer>();
	//48. TextRendering::Font
	RegisterClass<TextRendering::Font>();
	//49. UI::Canvas
	RegisterClass<UI::Canvas>();
	//50. UI::CanvasGroup
	RegisterClass<UI::CanvasGroup>();
	//51. UI::CanvasRenderer
	RegisterClass<UI::CanvasRenderer>();
	//52. MeshRenderer
	RegisterClass<MeshRenderer>();
	//53. AudioListener
	RegisterClass<AudioListener>();
	//54. Rigidbody2D
	RegisterClass<Rigidbody2D>();
	//55. RuntimeAnimatorController
	RegisterClass<RuntimeAnimatorController>();
	//56. PhysicsMaterial2D
	RegisterClass<PhysicsMaterial2D>();
	//57. PreloadData
	RegisterClass<PreloadData>();
	//58. Cubemap
	RegisterClass<Cubemap>();
	//59. Texture2DArray
	RegisterClass<Texture2DArray>();
	//60. TimeManager
	RegisterClass<TimeManager>();
	//61. AudioManager
	RegisterClass<AudioManager>();
	//62. InputManager
	RegisterClass<InputManager>();
	//63. Physics2DSettings
	RegisterClass<Physics2DSettings>();
	//64. GraphicsSettings
	RegisterClass<GraphicsSettings>();
	//65. PhysicsManager
	RegisterClass<PhysicsManager>();
	//66. TagManager
	RegisterClass<TagManager>();
	//67. ScriptMapper
	RegisterClass<ScriptMapper>();
	//68. DelayedCallManager
	RegisterClass<DelayedCallManager>();
	//69. MonoScript
	RegisterClass<MonoScript>();
	//70. MonoManager
	RegisterClass<MonoManager>();
	//71. PlayerSettings
	RegisterClass<PlayerSettings>();
	//72. BuildSettings
	RegisterClass<BuildSettings>();
	//73. ResourceManager
	RegisterClass<ResourceManager>();
	//74. NetworkManager
	RegisterClass<NetworkManager>();
	//75. MasterServerInterface
	RegisterClass<MasterServerInterface>();
	//76. RuntimeInitializeOnLoadManager
	RegisterClass<RuntimeInitializeOnLoadManager>();
	//77. ParticleAnimator
	RegisterClass<ParticleAnimator>();
	//78. EllipsoidParticleEmitter
	RegisterClass<EllipsoidParticleEmitter>();
	//79. ParticleEmitter
	RegisterClass<ParticleEmitter>();
	//80. ParticleRenderer
	RegisterClass<ParticleRenderer>();
	//81. AnimationClip
	RegisterClass<AnimationClip>();
	//82. Motion
	RegisterClass<Motion>();
	//83. Avatar
	RegisterClass<Avatar>();
	//84. TrailRenderer
	RegisterClass<TrailRenderer>();
	//85. FlareLayer
	RegisterClass<FlareLayer>();
	//86. TerrainCollider
	RegisterClass<TerrainCollider>();
	//87. TerrainData
	RegisterClass<TerrainData>();
	//88. LightmapSettings
	RegisterClass<LightmapSettings>();
	//89. Terrain
	RegisterClass<Terrain>();

}
