﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationManager/<ClearOverTime>c__Iterator0
struct U3CClearOverTimeU3Ec__Iterator0_t370995214;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationManager/<ClearOverTime>c__Iterator0::.ctor()
extern "C"  void U3CClearOverTimeU3Ec__Iterator0__ctor_m1293908035 (U3CClearOverTimeU3Ec__Iterator0_t370995214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NotificationManager/<ClearOverTime>c__Iterator0::MoveNext()
extern "C"  bool U3CClearOverTimeU3Ec__Iterator0_MoveNext_m378329289 (U3CClearOverTimeU3Ec__Iterator0_t370995214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NotificationManager/<ClearOverTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CClearOverTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2169747497 (U3CClearOverTimeU3Ec__Iterator0_t370995214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NotificationManager/<ClearOverTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CClearOverTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2936928721 (U3CClearOverTimeU3Ec__Iterator0_t370995214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager/<ClearOverTime>c__Iterator0::Dispose()
extern "C"  void U3CClearOverTimeU3Ec__Iterator0_Dispose_m273857216 (U3CClearOverTimeU3Ec__Iterator0_t370995214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager/<ClearOverTime>c__Iterator0::Reset()
extern "C"  void U3CClearOverTimeU3Ec__Iterator0_Reset_m3925192174 (U3CClearOverTimeU3Ec__Iterator0_t370995214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
