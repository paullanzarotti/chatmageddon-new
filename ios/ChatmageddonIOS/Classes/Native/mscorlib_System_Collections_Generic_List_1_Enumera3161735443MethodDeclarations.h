﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AttackSearchNav>
struct List_1_t3627005769;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3161735443.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m319860196_gshared (Enumerator_t3161735443 * __this, List_1_t3627005769 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m319860196(__this, ___l0, method) ((  void (*) (Enumerator_t3161735443 *, List_1_t3627005769 *, const MethodInfo*))Enumerator__ctor_m319860196_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m485996606_gshared (Enumerator_t3161735443 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m485996606(__this, method) ((  void (*) (Enumerator_t3161735443 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m485996606_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2792021110_gshared (Enumerator_t3161735443 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2792021110(__this, method) ((  Il2CppObject * (*) (Enumerator_t3161735443 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2792021110_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::Dispose()
extern "C"  void Enumerator_Dispose_m1949378705_gshared (Enumerator_t3161735443 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1949378705(__this, method) ((  void (*) (Enumerator_t3161735443 *, const MethodInfo*))Enumerator_Dispose_m1949378705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2340865278_gshared (Enumerator_t3161735443 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2340865278(__this, method) ((  void (*) (Enumerator_t3161735443 *, const MethodInfo*))Enumerator_VerifyState_m2340865278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3199975082_gshared (Enumerator_t3161735443 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3199975082(__this, method) ((  bool (*) (Enumerator_t3161735443 *, const MethodInfo*))Enumerator_MoveNext_m3199975082_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AttackSearchNav>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1781575985_gshared (Enumerator_t3161735443 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1781575985(__this, method) ((  int32_t (*) (Enumerator_t3161735443 *, const MethodInfo*))Enumerator_get_Current_m1781575985_gshared)(__this, method)
