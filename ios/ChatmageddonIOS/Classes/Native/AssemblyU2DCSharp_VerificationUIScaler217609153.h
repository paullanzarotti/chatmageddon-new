﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VerificationUIScaler
struct  VerificationUIScaler_t217609153  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite VerificationUIScaler::topSeperator
	UISprite_t603616735 * ___topSeperator_14;
	// UISprite VerificationUIScaler::bottomSeperator
	UISprite_t603616735 * ___bottomSeperator_15;
	// UnityEngine.BoxCollider VerificationUIScaler::codeCollider
	BoxCollider_t22920061 * ___codeCollider_16;
	// UnityEngine.BoxCollider VerificationUIScaler::resendCollider
	BoxCollider_t22920061 * ___resendCollider_17;
	// UISprite VerificationUIScaler::verifyButton
	UISprite_t603616735 * ___verifyButton_18;
	// UnityEngine.BoxCollider VerificationUIScaler::verifyCollider
	BoxCollider_t22920061 * ___verifyCollider_19;

public:
	inline static int32_t get_offset_of_topSeperator_14() { return static_cast<int32_t>(offsetof(VerificationUIScaler_t217609153, ___topSeperator_14)); }
	inline UISprite_t603616735 * get_topSeperator_14() const { return ___topSeperator_14; }
	inline UISprite_t603616735 ** get_address_of_topSeperator_14() { return &___topSeperator_14; }
	inline void set_topSeperator_14(UISprite_t603616735 * value)
	{
		___topSeperator_14 = value;
		Il2CppCodeGenWriteBarrier(&___topSeperator_14, value);
	}

	inline static int32_t get_offset_of_bottomSeperator_15() { return static_cast<int32_t>(offsetof(VerificationUIScaler_t217609153, ___bottomSeperator_15)); }
	inline UISprite_t603616735 * get_bottomSeperator_15() const { return ___bottomSeperator_15; }
	inline UISprite_t603616735 ** get_address_of_bottomSeperator_15() { return &___bottomSeperator_15; }
	inline void set_bottomSeperator_15(UISprite_t603616735 * value)
	{
		___bottomSeperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSeperator_15, value);
	}

	inline static int32_t get_offset_of_codeCollider_16() { return static_cast<int32_t>(offsetof(VerificationUIScaler_t217609153, ___codeCollider_16)); }
	inline BoxCollider_t22920061 * get_codeCollider_16() const { return ___codeCollider_16; }
	inline BoxCollider_t22920061 ** get_address_of_codeCollider_16() { return &___codeCollider_16; }
	inline void set_codeCollider_16(BoxCollider_t22920061 * value)
	{
		___codeCollider_16 = value;
		Il2CppCodeGenWriteBarrier(&___codeCollider_16, value);
	}

	inline static int32_t get_offset_of_resendCollider_17() { return static_cast<int32_t>(offsetof(VerificationUIScaler_t217609153, ___resendCollider_17)); }
	inline BoxCollider_t22920061 * get_resendCollider_17() const { return ___resendCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_resendCollider_17() { return &___resendCollider_17; }
	inline void set_resendCollider_17(BoxCollider_t22920061 * value)
	{
		___resendCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___resendCollider_17, value);
	}

	inline static int32_t get_offset_of_verifyButton_18() { return static_cast<int32_t>(offsetof(VerificationUIScaler_t217609153, ___verifyButton_18)); }
	inline UISprite_t603616735 * get_verifyButton_18() const { return ___verifyButton_18; }
	inline UISprite_t603616735 ** get_address_of_verifyButton_18() { return &___verifyButton_18; }
	inline void set_verifyButton_18(UISprite_t603616735 * value)
	{
		___verifyButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___verifyButton_18, value);
	}

	inline static int32_t get_offset_of_verifyCollider_19() { return static_cast<int32_t>(offsetof(VerificationUIScaler_t217609153, ___verifyCollider_19)); }
	inline BoxCollider_t22920061 * get_verifyCollider_19() const { return ___verifyCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_verifyCollider_19() { return &___verifyCollider_19; }
	inline void set_verifyCollider_19(BoxCollider_t22920061 * value)
	{
		___verifyCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___verifyCollider_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
