﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BestHTTP.Extensions.HeaderValue
struct HeaderValue_t822462144;
// System.String
struct String_t;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>
struct List_1_t191583276;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeaderValue822462144.h"

// System.Void BestHTTP.Extensions.HeaderValue::.ctor()
extern "C"  void HeaderValue__ctor_m2693857737 (HeaderValue_t822462144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.HeaderValue::.ctor(System.String)
extern "C"  void HeaderValue__ctor_m1439430031 (HeaderValue_t822462144 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BestHTTP.Extensions.HeaderValue::get_Key()
extern "C"  String_t* HeaderValue_get_Key_m1022996344 (HeaderValue_t822462144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.HeaderValue::set_Key(System.String)
extern "C"  void HeaderValue_set_Key_m806168933 (HeaderValue_t822462144 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BestHTTP.Extensions.HeaderValue::get_Value()
extern "C"  String_t* HeaderValue_get_Value_m4036815990 (HeaderValue_t822462144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.HeaderValue::set_Value(System.String)
extern "C"  void HeaderValue_set_Value_m1310045597 (HeaderValue_t822462144 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.HeaderValue::get_Options()
extern "C"  List_1_t191583276 * HeaderValue_get_Options_m3999575283 (HeaderValue_t822462144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.HeaderValue::set_Options(System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>)
extern "C"  void HeaderValue_set_Options_m64328346 (HeaderValue_t822462144 * __this, List_1_t191583276 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.Extensions.HeaderValue::get_HasValue()
extern "C"  bool HeaderValue_get_HasValue_m1855363501 (HeaderValue_t822462144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.HeaderValue::Parse(System.String,System.Int32&)
extern "C"  void HeaderValue_Parse_m521499149 (HeaderValue_t822462144 * __this, String_t* ___headerStr0, int32_t* ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.Extensions.HeaderValue::TryGetOption(System.String,BestHTTP.Extensions.HeaderValue&)
extern "C"  bool HeaderValue_TryGetOption_m36506345 (HeaderValue_t822462144 * __this, String_t* ___key0, HeaderValue_t822462144 ** ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.HeaderValue::ParseImplementation(System.String,System.Int32&,System.Boolean)
extern "C"  void HeaderValue_ParseImplementation_m2967611566 (HeaderValue_t822462144 * __this, String_t* ___headerStr0, int32_t* ___pos1, bool ___isOptionIsAnOption2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BestHTTP.Extensions.HeaderValue::ToString()
extern "C"  String_t* HeaderValue_ToString_m3237000106 (HeaderValue_t822462144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.Extensions.HeaderValue::<ParseImplementation>m__0(System.Char)
extern "C"  bool HeaderValue_U3CParseImplementationU3Em__0_m806889310 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
