﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProfileNavigateBackwardsButton
struct  ProfileNavigateBackwardsButton_t4036937052  : public SFXButton_t792651341
{
public:
	// UISprite ProfileNavigateBackwardsButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_5;
	// UILabel ProfileNavigateBackwardsButton::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_6;

public:
	inline static int32_t get_offset_of_buttonSprite_5() { return static_cast<int32_t>(offsetof(ProfileNavigateBackwardsButton_t4036937052, ___buttonSprite_5)); }
	inline UISprite_t603616735 * get_buttonSprite_5() const { return ___buttonSprite_5; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_5() { return &___buttonSprite_5; }
	inline void set_buttonSprite_5(UISprite_t603616735 * value)
	{
		___buttonSprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_5, value);
	}

	inline static int32_t get_offset_of_buttonLabel_6() { return static_cast<int32_t>(offsetof(ProfileNavigateBackwardsButton_t4036937052, ___buttonLabel_6)); }
	inline UILabel_t1795115428 * get_buttonLabel_6() const { return ___buttonLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_6() { return &___buttonLabel_6; }
	inline void set_buttonLabel_6(UILabel_t1795115428 * value)
	{
		___buttonLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
