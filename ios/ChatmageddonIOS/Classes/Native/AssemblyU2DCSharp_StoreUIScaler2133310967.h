﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.Transform
struct Transform_t3275118058;
// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// ItemSwipeController
struct ItemSwipeController_t3011021799;
// TweenPosition
struct TweenPosition_t1144714832;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoreUIScaler
struct  StoreUIScaler_t2133310967  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite StoreUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UITexture StoreUIScaler::backgroundHatching
	UITexture_t2537039969 * ___backgroundHatching_15;
	// UISprite StoreUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_16;
	// UnityEngine.Transform StoreUIScaler::navigateBackButton
	Transform_t3275118058 * ___navigateBackButton_17;
	// UISprite StoreUIScaler::tabBackground
	UISprite_t603616735 * ___tabBackground_18;
	// UIPanel StoreUIScaler::tabPanel
	UIPanel_t1795085332 * ___tabPanel_19;
	// UISprite StoreUIScaler::bottomSeperator
	UISprite_t603616735 * ___bottomSeperator_20;
	// UIPanel StoreUIScaler::popUpPanel
	UIPanel_t1795085332 * ___popUpPanel_21;
	// UISprite StoreUIScaler::popUpBackground
	UISprite_t603616735 * ___popUpBackground_22;
	// UnityEngine.BoxCollider StoreUIScaler::popUpCollider
	BoxCollider_t22920061 * ___popUpCollider_23;
	// UnityEngine.Transform StoreUIScaler::itemDescriptionAmount
	Transform_t3275118058 * ___itemDescriptionAmount_24;
	// UnityEngine.Transform StoreUIScaler::infinitySprite
	Transform_t3275118058 * ___infinitySprite_25;
	// UnityEngine.Transform StoreUIScaler::itemScaleMeasure
	Transform_t3275118058 * ___itemScaleMeasure_26;
	// UnityEngine.Transform StoreUIScaler::itemInfoButton
	Transform_t3275118058 * ___itemInfoButton_27;
	// ItemSwipeController StoreUIScaler::swipe
	ItemSwipeController_t3011021799 * ___swipe_28;
	// UnityEngine.BoxCollider StoreUIScaler::containerCollider
	BoxCollider_t22920061 * ___containerCollider_29;
	// UISprite StoreUIScaler::horizLine
	UISprite_t603616735 * ___horizLine_30;
	// TweenPosition StoreUIScaler::buttonsTween
	TweenPosition_t1144714832 * ___buttonsTween_31;
	// UnityEngine.BoxCollider StoreUIScaler::buyButtonCollider
	BoxCollider_t22920061 * ___buyButtonCollider_32;
	// UISprite StoreUIScaler::buyButtonBackground
	UISprite_t603616735 * ___buyButtonBackground_33;
	// UILabel StoreUIScaler::buyButtonText
	UILabel_t1795115428 * ___buyButtonText_34;
	// TweenPosition StoreUIScaler::buyButtonTextTween
	TweenPosition_t1144714832 * ___buyButtonTextTween_35;
	// UnityEngine.BoxCollider StoreUIScaler::targetButtonCollider
	BoxCollider_t22920061 * ___targetButtonCollider_36;
	// UISprite StoreUIScaler::targetButtonBackground
	UISprite_t603616735 * ___targetButtonBackground_37;
	// UILabel StoreUIScaler::targetButtonText
	UILabel_t1795115428 * ___targetButtonText_38;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundHatching_15() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___backgroundHatching_15)); }
	inline UITexture_t2537039969 * get_backgroundHatching_15() const { return ___backgroundHatching_15; }
	inline UITexture_t2537039969 ** get_address_of_backgroundHatching_15() { return &___backgroundHatching_15; }
	inline void set_backgroundHatching_15(UITexture_t2537039969 * value)
	{
		___backgroundHatching_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundHatching_15, value);
	}

	inline static int32_t get_offset_of_titleSeperator_16() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___titleSeperator_16)); }
	inline UISprite_t603616735 * get_titleSeperator_16() const { return ___titleSeperator_16; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_16() { return &___titleSeperator_16; }
	inline void set_titleSeperator_16(UISprite_t603616735 * value)
	{
		___titleSeperator_16 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_16, value);
	}

	inline static int32_t get_offset_of_navigateBackButton_17() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___navigateBackButton_17)); }
	inline Transform_t3275118058 * get_navigateBackButton_17() const { return ___navigateBackButton_17; }
	inline Transform_t3275118058 ** get_address_of_navigateBackButton_17() { return &___navigateBackButton_17; }
	inline void set_navigateBackButton_17(Transform_t3275118058 * value)
	{
		___navigateBackButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___navigateBackButton_17, value);
	}

	inline static int32_t get_offset_of_tabBackground_18() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___tabBackground_18)); }
	inline UISprite_t603616735 * get_tabBackground_18() const { return ___tabBackground_18; }
	inline UISprite_t603616735 ** get_address_of_tabBackground_18() { return &___tabBackground_18; }
	inline void set_tabBackground_18(UISprite_t603616735 * value)
	{
		___tabBackground_18 = value;
		Il2CppCodeGenWriteBarrier(&___tabBackground_18, value);
	}

	inline static int32_t get_offset_of_tabPanel_19() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___tabPanel_19)); }
	inline UIPanel_t1795085332 * get_tabPanel_19() const { return ___tabPanel_19; }
	inline UIPanel_t1795085332 ** get_address_of_tabPanel_19() { return &___tabPanel_19; }
	inline void set_tabPanel_19(UIPanel_t1795085332 * value)
	{
		___tabPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___tabPanel_19, value);
	}

	inline static int32_t get_offset_of_bottomSeperator_20() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___bottomSeperator_20)); }
	inline UISprite_t603616735 * get_bottomSeperator_20() const { return ___bottomSeperator_20; }
	inline UISprite_t603616735 ** get_address_of_bottomSeperator_20() { return &___bottomSeperator_20; }
	inline void set_bottomSeperator_20(UISprite_t603616735 * value)
	{
		___bottomSeperator_20 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSeperator_20, value);
	}

	inline static int32_t get_offset_of_popUpPanel_21() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___popUpPanel_21)); }
	inline UIPanel_t1795085332 * get_popUpPanel_21() const { return ___popUpPanel_21; }
	inline UIPanel_t1795085332 ** get_address_of_popUpPanel_21() { return &___popUpPanel_21; }
	inline void set_popUpPanel_21(UIPanel_t1795085332 * value)
	{
		___popUpPanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___popUpPanel_21, value);
	}

	inline static int32_t get_offset_of_popUpBackground_22() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___popUpBackground_22)); }
	inline UISprite_t603616735 * get_popUpBackground_22() const { return ___popUpBackground_22; }
	inline UISprite_t603616735 ** get_address_of_popUpBackground_22() { return &___popUpBackground_22; }
	inline void set_popUpBackground_22(UISprite_t603616735 * value)
	{
		___popUpBackground_22 = value;
		Il2CppCodeGenWriteBarrier(&___popUpBackground_22, value);
	}

	inline static int32_t get_offset_of_popUpCollider_23() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___popUpCollider_23)); }
	inline BoxCollider_t22920061 * get_popUpCollider_23() const { return ___popUpCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of_popUpCollider_23() { return &___popUpCollider_23; }
	inline void set_popUpCollider_23(BoxCollider_t22920061 * value)
	{
		___popUpCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&___popUpCollider_23, value);
	}

	inline static int32_t get_offset_of_itemDescriptionAmount_24() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___itemDescriptionAmount_24)); }
	inline Transform_t3275118058 * get_itemDescriptionAmount_24() const { return ___itemDescriptionAmount_24; }
	inline Transform_t3275118058 ** get_address_of_itemDescriptionAmount_24() { return &___itemDescriptionAmount_24; }
	inline void set_itemDescriptionAmount_24(Transform_t3275118058 * value)
	{
		___itemDescriptionAmount_24 = value;
		Il2CppCodeGenWriteBarrier(&___itemDescriptionAmount_24, value);
	}

	inline static int32_t get_offset_of_infinitySprite_25() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___infinitySprite_25)); }
	inline Transform_t3275118058 * get_infinitySprite_25() const { return ___infinitySprite_25; }
	inline Transform_t3275118058 ** get_address_of_infinitySprite_25() { return &___infinitySprite_25; }
	inline void set_infinitySprite_25(Transform_t3275118058 * value)
	{
		___infinitySprite_25 = value;
		Il2CppCodeGenWriteBarrier(&___infinitySprite_25, value);
	}

	inline static int32_t get_offset_of_itemScaleMeasure_26() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___itemScaleMeasure_26)); }
	inline Transform_t3275118058 * get_itemScaleMeasure_26() const { return ___itemScaleMeasure_26; }
	inline Transform_t3275118058 ** get_address_of_itemScaleMeasure_26() { return &___itemScaleMeasure_26; }
	inline void set_itemScaleMeasure_26(Transform_t3275118058 * value)
	{
		___itemScaleMeasure_26 = value;
		Il2CppCodeGenWriteBarrier(&___itemScaleMeasure_26, value);
	}

	inline static int32_t get_offset_of_itemInfoButton_27() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___itemInfoButton_27)); }
	inline Transform_t3275118058 * get_itemInfoButton_27() const { return ___itemInfoButton_27; }
	inline Transform_t3275118058 ** get_address_of_itemInfoButton_27() { return &___itemInfoButton_27; }
	inline void set_itemInfoButton_27(Transform_t3275118058 * value)
	{
		___itemInfoButton_27 = value;
		Il2CppCodeGenWriteBarrier(&___itemInfoButton_27, value);
	}

	inline static int32_t get_offset_of_swipe_28() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___swipe_28)); }
	inline ItemSwipeController_t3011021799 * get_swipe_28() const { return ___swipe_28; }
	inline ItemSwipeController_t3011021799 ** get_address_of_swipe_28() { return &___swipe_28; }
	inline void set_swipe_28(ItemSwipeController_t3011021799 * value)
	{
		___swipe_28 = value;
		Il2CppCodeGenWriteBarrier(&___swipe_28, value);
	}

	inline static int32_t get_offset_of_containerCollider_29() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___containerCollider_29)); }
	inline BoxCollider_t22920061 * get_containerCollider_29() const { return ___containerCollider_29; }
	inline BoxCollider_t22920061 ** get_address_of_containerCollider_29() { return &___containerCollider_29; }
	inline void set_containerCollider_29(BoxCollider_t22920061 * value)
	{
		___containerCollider_29 = value;
		Il2CppCodeGenWriteBarrier(&___containerCollider_29, value);
	}

	inline static int32_t get_offset_of_horizLine_30() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___horizLine_30)); }
	inline UISprite_t603616735 * get_horizLine_30() const { return ___horizLine_30; }
	inline UISprite_t603616735 ** get_address_of_horizLine_30() { return &___horizLine_30; }
	inline void set_horizLine_30(UISprite_t603616735 * value)
	{
		___horizLine_30 = value;
		Il2CppCodeGenWriteBarrier(&___horizLine_30, value);
	}

	inline static int32_t get_offset_of_buttonsTween_31() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___buttonsTween_31)); }
	inline TweenPosition_t1144714832 * get_buttonsTween_31() const { return ___buttonsTween_31; }
	inline TweenPosition_t1144714832 ** get_address_of_buttonsTween_31() { return &___buttonsTween_31; }
	inline void set_buttonsTween_31(TweenPosition_t1144714832 * value)
	{
		___buttonsTween_31 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsTween_31, value);
	}

	inline static int32_t get_offset_of_buyButtonCollider_32() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___buyButtonCollider_32)); }
	inline BoxCollider_t22920061 * get_buyButtonCollider_32() const { return ___buyButtonCollider_32; }
	inline BoxCollider_t22920061 ** get_address_of_buyButtonCollider_32() { return &___buyButtonCollider_32; }
	inline void set_buyButtonCollider_32(BoxCollider_t22920061 * value)
	{
		___buyButtonCollider_32 = value;
		Il2CppCodeGenWriteBarrier(&___buyButtonCollider_32, value);
	}

	inline static int32_t get_offset_of_buyButtonBackground_33() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___buyButtonBackground_33)); }
	inline UISprite_t603616735 * get_buyButtonBackground_33() const { return ___buyButtonBackground_33; }
	inline UISprite_t603616735 ** get_address_of_buyButtonBackground_33() { return &___buyButtonBackground_33; }
	inline void set_buyButtonBackground_33(UISprite_t603616735 * value)
	{
		___buyButtonBackground_33 = value;
		Il2CppCodeGenWriteBarrier(&___buyButtonBackground_33, value);
	}

	inline static int32_t get_offset_of_buyButtonText_34() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___buyButtonText_34)); }
	inline UILabel_t1795115428 * get_buyButtonText_34() const { return ___buyButtonText_34; }
	inline UILabel_t1795115428 ** get_address_of_buyButtonText_34() { return &___buyButtonText_34; }
	inline void set_buyButtonText_34(UILabel_t1795115428 * value)
	{
		___buyButtonText_34 = value;
		Il2CppCodeGenWriteBarrier(&___buyButtonText_34, value);
	}

	inline static int32_t get_offset_of_buyButtonTextTween_35() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___buyButtonTextTween_35)); }
	inline TweenPosition_t1144714832 * get_buyButtonTextTween_35() const { return ___buyButtonTextTween_35; }
	inline TweenPosition_t1144714832 ** get_address_of_buyButtonTextTween_35() { return &___buyButtonTextTween_35; }
	inline void set_buyButtonTextTween_35(TweenPosition_t1144714832 * value)
	{
		___buyButtonTextTween_35 = value;
		Il2CppCodeGenWriteBarrier(&___buyButtonTextTween_35, value);
	}

	inline static int32_t get_offset_of_targetButtonCollider_36() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___targetButtonCollider_36)); }
	inline BoxCollider_t22920061 * get_targetButtonCollider_36() const { return ___targetButtonCollider_36; }
	inline BoxCollider_t22920061 ** get_address_of_targetButtonCollider_36() { return &___targetButtonCollider_36; }
	inline void set_targetButtonCollider_36(BoxCollider_t22920061 * value)
	{
		___targetButtonCollider_36 = value;
		Il2CppCodeGenWriteBarrier(&___targetButtonCollider_36, value);
	}

	inline static int32_t get_offset_of_targetButtonBackground_37() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___targetButtonBackground_37)); }
	inline UISprite_t603616735 * get_targetButtonBackground_37() const { return ___targetButtonBackground_37; }
	inline UISprite_t603616735 ** get_address_of_targetButtonBackground_37() { return &___targetButtonBackground_37; }
	inline void set_targetButtonBackground_37(UISprite_t603616735 * value)
	{
		___targetButtonBackground_37 = value;
		Il2CppCodeGenWriteBarrier(&___targetButtonBackground_37, value);
	}

	inline static int32_t get_offset_of_targetButtonText_38() { return static_cast<int32_t>(offsetof(StoreUIScaler_t2133310967, ___targetButtonText_38)); }
	inline UILabel_t1795115428 * get_targetButtonText_38() const { return ___targetButtonText_38; }
	inline UILabel_t1795115428 ** get_address_of_targetButtonText_38() { return &___targetButtonText_38; }
	inline void set_targetButtonText_38(UILabel_t1795115428 * value)
	{
		___targetButtonText_38 = value;
		Il2CppCodeGenWriteBarrier(&___targetButtonText_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
