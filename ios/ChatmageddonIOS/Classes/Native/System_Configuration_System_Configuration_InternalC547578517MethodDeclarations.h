﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.InternalConfigurationRoot
struct InternalConfigurationRoot_t547578517;
// System.Configuration.Internal.InternalConfigEventHandler
struct InternalConfigEventHandler_t2492130711;
// System.Configuration.Internal.IInternalConfigRecord
struct IInternalConfigRecord_t3033563987;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Configuration.Internal.IInternalConfigHost
struct IInternalConfigHost_t3115158152;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Internal2492130711.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.InternalConfigurationRoot::.ctor()
extern "C"  void InternalConfigurationRoot__ctor_m903766256 (InternalConfigurationRoot_t547578517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationRoot::add_ConfigChanged(System.Configuration.Internal.InternalConfigEventHandler)
extern "C"  void InternalConfigurationRoot_add_ConfigChanged_m199362411 (InternalConfigurationRoot_t547578517 * __this, InternalConfigEventHandler_t2492130711 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationRoot::remove_ConfigChanged(System.Configuration.Internal.InternalConfigEventHandler)
extern "C"  void InternalConfigurationRoot_remove_ConfigChanged_m1105808396 (InternalConfigurationRoot_t547578517 * __this, InternalConfigEventHandler_t2492130711 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationRoot::add_ConfigRemoved(System.Configuration.Internal.InternalConfigEventHandler)
extern "C"  void InternalConfigurationRoot_add_ConfigRemoved_m2119136459 (InternalConfigurationRoot_t547578517 * __this, InternalConfigEventHandler_t2492130711 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationRoot::remove_ConfigRemoved(System.Configuration.Internal.InternalConfigEventHandler)
extern "C"  void InternalConfigurationRoot_remove_ConfigRemoved_m2066022004 (InternalConfigurationRoot_t547578517 * __this, InternalConfigEventHandler_t2492130711 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Internal.IInternalConfigRecord System.Configuration.InternalConfigurationRoot::GetConfigRecord(System.String)
extern "C"  Il2CppObject * InternalConfigurationRoot_GetConfigRecord_m98675873 (InternalConfigurationRoot_t547578517 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InternalConfigurationRoot::GetSection(System.String,System.String)
extern "C"  Il2CppObject * InternalConfigurationRoot_GetSection_m3749470748 (InternalConfigurationRoot_t547578517 * __this, String_t* ___section0, String_t* ___configPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationRoot::GetUniqueConfigPath(System.String)
extern "C"  String_t* InternalConfigurationRoot_GetUniqueConfigPath_m207925969 (InternalConfigurationRoot_t547578517 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Internal.IInternalConfigRecord System.Configuration.InternalConfigurationRoot::GetUniqueConfigRecord(System.String)
extern "C"  Il2CppObject * InternalConfigurationRoot_GetUniqueConfigRecord_m313352234 (InternalConfigurationRoot_t547578517 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationRoot::Init(System.Configuration.Internal.IInternalConfigHost,System.Boolean)
extern "C"  void InternalConfigurationRoot_Init_m2920331433 (InternalConfigurationRoot_t547578517 * __this, Il2CppObject * ___host0, bool ___isDesignTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationRoot::RemoveConfig(System.String)
extern "C"  void InternalConfigurationRoot_RemoveConfig_m404680430 (InternalConfigurationRoot_t547578517 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationRoot::get_IsDesignTime()
extern "C"  bool InternalConfigurationRoot_get_IsDesignTime_m4167312316 (InternalConfigurationRoot_t547578517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
