﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookResultsPaging
struct FacebookResultsPaging_t3277319338;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookResultsPaging::.ctor()
extern "C"  void FacebookResultsPaging__ctor_m3893277304 (FacebookResultsPaging_t3277319338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
