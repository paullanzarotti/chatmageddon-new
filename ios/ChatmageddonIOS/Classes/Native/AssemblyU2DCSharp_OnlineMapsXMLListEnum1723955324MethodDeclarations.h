﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsXMLListEnum
struct OnlineMapsXMLListEnum_t1723955324;
// OnlineMapsXMLList
struct OnlineMapsXMLList_t1635558505;
// System.Object
struct Il2CppObject;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsXMLList1635558505.h"

// System.Void OnlineMapsXMLListEnum::.ctor(OnlineMapsXMLList)
extern "C"  void OnlineMapsXMLListEnum__ctor_m1725512304 (OnlineMapsXMLListEnum_t1723955324 * __this, OnlineMapsXMLList_t1635558505 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsXMLListEnum::MoveNext()
extern "C"  bool OnlineMapsXMLListEnum_MoveNext_m2286056333 (OnlineMapsXMLListEnum_t1723955324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsXMLListEnum::Reset()
extern "C"  void OnlineMapsXMLListEnum_Reset_m4260783644 (OnlineMapsXMLListEnum_t1723955324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsXMLListEnum::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * OnlineMapsXMLListEnum_System_Collections_IEnumerator_get_Current_m3164947221 (OnlineMapsXMLListEnum_t1723955324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsXML OnlineMapsXMLListEnum::get_Current()
extern "C"  OnlineMapsXML_t3341520387 * OnlineMapsXMLListEnum_get_Current_m1605351101 (OnlineMapsXMLListEnum_t1723955324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
