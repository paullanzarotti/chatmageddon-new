﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.AsYouTypeFormatter
struct AsYouTypeFormatter_t993724987;
// System.String
struct String_t;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"

// System.Void PhoneNumbers.AsYouTypeFormatter::.ctor(System.String)
extern "C"  void AsYouTypeFormatter__ctor_m1449226232 (AsYouTypeFormatter_t993724987 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.AsYouTypeFormatter::GetMetadataForRegion(System.String)
extern "C"  PhoneMetadata_t366861403 * AsYouTypeFormatter_GetMetadataForRegion_m3616715162 (AsYouTypeFormatter_t993724987 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::MaybeCreateNewTemplate()
extern "C"  bool AsYouTypeFormatter_MaybeCreateNewTemplate_m1129706202 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.AsYouTypeFormatter::GetAvailableFormats(System.String)
extern "C"  void AsYouTypeFormatter_GetAvailableFormats_m2497814733 (AsYouTypeFormatter_t993724987 * __this, String_t* ___leadingThreeDigits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::IsFormatEligible(System.String)
extern "C"  bool AsYouTypeFormatter_IsFormatEligible_m2470264162 (AsYouTypeFormatter_t993724987 * __this, String_t* ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.AsYouTypeFormatter::NarrowDownPossibleFormats(System.String)
extern "C"  void AsYouTypeFormatter_NarrowDownPossibleFormats_m723339812 (AsYouTypeFormatter_t993724987 * __this, String_t* ___leadingDigits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::CreateFormattingTemplate(PhoneNumbers.NumberFormat)
extern "C"  bool AsYouTypeFormatter_CreateFormattingTemplate_m3863797665 (AsYouTypeFormatter_t993724987 * __this, NumberFormat_t441739224 * ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::GetFormattingTemplate(System.String,System.String)
extern "C"  String_t* AsYouTypeFormatter_GetFormattingTemplate_m3068997600 (AsYouTypeFormatter_t993724987 * __this, String_t* ___numberPattern0, String_t* ___numberFormat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.AsYouTypeFormatter::Clear()
extern "C"  void AsYouTypeFormatter_Clear_m2845948781 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::InputDigit(System.Char)
extern "C"  String_t* AsYouTypeFormatter_InputDigit_m4166031323 (AsYouTypeFormatter_t993724987 * __this, Il2CppChar ___nextChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::InputDigitAndRememberPosition(System.Char)
extern "C"  String_t* AsYouTypeFormatter_InputDigitAndRememberPosition_m4278777422 (AsYouTypeFormatter_t993724987 * __this, Il2CppChar ___nextChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::InputDigitWithOptionToRememberPosition(System.Char,System.Boolean)
extern "C"  String_t* AsYouTypeFormatter_InputDigitWithOptionToRememberPosition_m352439170 (AsYouTypeFormatter_t993724987 * __this, Il2CppChar ___nextChar0, bool ___rememberPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::AttemptToChoosePatternWithPrefixExtracted()
extern "C"  String_t* AsYouTypeFormatter_AttemptToChoosePatternWithPrefixExtracted_m2302810308 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::AbleToExtractLongerNdd()
extern "C"  bool AsYouTypeFormatter_AbleToExtractLongerNdd_m2130432003 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::IsDigitOrLeadingPlusSign(System.Char)
extern "C"  bool AsYouTypeFormatter_IsDigitOrLeadingPlusSign_m3448296942 (AsYouTypeFormatter_t993724987 * __this, Il2CppChar ___nextChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::AttemptToFormatAccruedDigits()
extern "C"  String_t* AsYouTypeFormatter_AttemptToFormatAccruedDigits_m1523259063 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.AsYouTypeFormatter::GetRememberedPosition()
extern "C"  int32_t AsYouTypeFormatter_GetRememberedPosition_m1016258775 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::AttemptToChooseFormattingPattern()
extern "C"  String_t* AsYouTypeFormatter_AttemptToChooseFormattingPattern_m2193295097 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::InputAccruedNationalNumber()
extern "C"  String_t* AsYouTypeFormatter_InputAccruedNationalNumber_m4172552549 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::RemoveNationalPrefixFromNationalNumber()
extern "C"  String_t* AsYouTypeFormatter_RemoveNationalPrefixFromNationalNumber_m475216722 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::AttemptToExtractIdd()
extern "C"  bool AsYouTypeFormatter_AttemptToExtractIdd_m272303728 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.AsYouTypeFormatter::AttemptToExtractCountryCallingCode()
extern "C"  bool AsYouTypeFormatter_AttemptToExtractCountryCallingCode_m2941401056 (AsYouTypeFormatter_t993724987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char PhoneNumbers.AsYouTypeFormatter::NormalizeAndAccrueDigitsAndPlusSign(System.Char,System.Boolean)
extern "C"  Il2CppChar AsYouTypeFormatter_NormalizeAndAccrueDigitsAndPlusSign_m2074308439 (AsYouTypeFormatter_t993724987 * __this, Il2CppChar ___nextChar0, bool ___rememberPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AsYouTypeFormatter::InputDigitHelper(System.Char)
extern "C"  String_t* AsYouTypeFormatter_InputDigitHelper_m852538205 (AsYouTypeFormatter_t993724987 * __this, Il2CppChar ___nextChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.AsYouTypeFormatter::.cctor()
extern "C"  void AsYouTypeFormatter__cctor_m228184631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
