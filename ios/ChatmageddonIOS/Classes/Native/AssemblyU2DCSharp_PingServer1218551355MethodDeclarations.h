﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PingServer
struct PingServer_t1218551355;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PingServer::.ctor()
extern "C"  void PingServer__ctor_m2712435226 (PingServer_t1218551355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PingServer::OnButtonClick()
extern "C"  void PingServer_OnButtonClick_m3501695727 (PingServer_t1218551355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PingServer::<OnButtonClick>m__0(System.Boolean,System.String)
extern "C"  void PingServer_U3COnButtonClickU3Em__0_m2096735133 (Il2CppObject * __this /* static, unused */, bool ___success0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
