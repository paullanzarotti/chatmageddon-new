﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen766945646.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingPopUp
struct  LoadingPopUp_t1016279926  : public MonoSingleton_1_t766945646
{
public:
	// UnityEngine.GameObject LoadingPopUp::content
	GameObject_t1756533147 * ___content_3;
	// UILabel LoadingPopUp::loadingLabel
	UILabel_t1795115428 * ___loadingLabel_4;
	// TweenAlpha LoadingPopUp::fadeTween
	TweenAlpha_t2421518635 * ___fadeTween_5;

public:
	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(LoadingPopUp_t1016279926, ___content_3)); }
	inline GameObject_t1756533147 * get_content_3() const { return ___content_3; }
	inline GameObject_t1756533147 ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(GameObject_t1756533147 * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier(&___content_3, value);
	}

	inline static int32_t get_offset_of_loadingLabel_4() { return static_cast<int32_t>(offsetof(LoadingPopUp_t1016279926, ___loadingLabel_4)); }
	inline UILabel_t1795115428 * get_loadingLabel_4() const { return ___loadingLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_loadingLabel_4() { return &___loadingLabel_4; }
	inline void set_loadingLabel_4(UILabel_t1795115428 * value)
	{
		___loadingLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___loadingLabel_4, value);
	}

	inline static int32_t get_offset_of_fadeTween_5() { return static_cast<int32_t>(offsetof(LoadingPopUp_t1016279926, ___fadeTween_5)); }
	inline TweenAlpha_t2421518635 * get_fadeTween_5() const { return ___fadeTween_5; }
	inline TweenAlpha_t2421518635 ** get_address_of_fadeTween_5() { return &___fadeTween_5; }
	inline void set_fadeTween_5(TweenAlpha_t2421518635 * value)
	{
		___fadeTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___fadeTween_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
