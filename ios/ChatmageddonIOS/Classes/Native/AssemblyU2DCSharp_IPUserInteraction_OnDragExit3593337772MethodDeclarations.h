﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPUserInteraction/OnDragExit
struct OnDragExit_t3593337772;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void IPUserInteraction/OnDragExit::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDragExit__ctor_m692735203 (OnDragExit_t3593337772 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction/OnDragExit::Invoke()
extern "C"  void OnDragExit_Invoke_m1235404143 (OnDragExit_t3593337772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IPUserInteraction/OnDragExit::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDragExit_BeginInvoke_m1874663598 (OnDragExit_t3593337772 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction/OnDragExit::EndInvoke(System.IAsyncResult)
extern "C"  void OnDragExit_EndInvoke_m529202253 (OnDragExit_t3593337772 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
