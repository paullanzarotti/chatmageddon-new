﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookBaseDT79607460.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookMeResult/FacebookMeHometown
struct  FacebookMeHometown_t3213935844  : public FacebookBaseDTO_t79607460
{
public:
	// System.String Prime31.FacebookMeResult/FacebookMeHometown::id
	String_t* ___id_0;
	// System.String Prime31.FacebookMeResult/FacebookMeHometown::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FacebookMeHometown_t3213935844, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(FacebookMeHometown_t3213935844, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
