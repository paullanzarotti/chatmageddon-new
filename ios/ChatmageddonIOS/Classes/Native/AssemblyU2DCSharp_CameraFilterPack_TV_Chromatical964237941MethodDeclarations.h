﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Chromatical
struct CameraFilterPack_TV_Chromatical_t964237941;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Chromatical::.ctor()
extern "C"  void CameraFilterPack_TV_Chromatical__ctor_m1763612162 (CameraFilterPack_TV_Chromatical_t964237941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Chromatical::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Chromatical_get_material_m2644083697 (CameraFilterPack_TV_Chromatical_t964237941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::Start()
extern "C"  void CameraFilterPack_TV_Chromatical_Start_m1536006010 (CameraFilterPack_TV_Chromatical_t964237941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Chromatical_OnRenderImage_m1362922034 (CameraFilterPack_TV_Chromatical_t964237941 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::Update()
extern "C"  void CameraFilterPack_TV_Chromatical_Update_m3405795485 (CameraFilterPack_TV_Chromatical_t964237941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::OnDisable()
extern "C"  void CameraFilterPack_TV_Chromatical_OnDisable_m2696326921 (CameraFilterPack_TV_Chromatical_t964237941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
