﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeSlider
struct StealthModeSlider_t1957547627;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeSlider::.ctor()
extern "C"  void StealthModeSlider__ctor_m1684442312 (StealthModeSlider_t1957547627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeSlider::LabelsUpdated(System.Int32)
extern "C"  void StealthModeSlider_LabelsUpdated_m3286860087 (StealthModeSlider_t1957547627 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
