﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShakeBarController
struct ShakeBarController_t4159664603;

#include "AssemblyU2DCSharp_DefendMiniGameController1844356491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShakeMiniGame
struct  ShakeMiniGame_t406515851  : public DefendMiniGameController_t1844356491
{
public:
	// ShakeBarController ShakeMiniGame::shakeController
	ShakeBarController_t4159664603 * ___shakeController_6;

public:
	inline static int32_t get_offset_of_shakeController_6() { return static_cast<int32_t>(offsetof(ShakeMiniGame_t406515851, ___shakeController_6)); }
	inline ShakeBarController_t4159664603 * get_shakeController_6() const { return ___shakeController_6; }
	inline ShakeBarController_t4159664603 ** get_address_of_shakeController_6() { return &___shakeController_6; }
	inline void set_shakeController_6(ShakeBarController_t4159664603 * value)
	{
		___shakeController_6 = value;
		Il2CppCodeGenWriteBarrier(&___shakeController_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
