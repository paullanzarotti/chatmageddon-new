﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,FriendSearchNavScreen>
struct NavigationController_2_t906290919;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2342486618_gshared (NavigationController_2_t906290919 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m2342486618(__this, method) ((  void (*) (NavigationController_2_t906290919 *, const MethodInfo*))NavigationController_2__ctor_m2342486618_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1017759524_gshared (NavigationController_2_t906290919 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1017759524(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t906290919 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1017759524_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m182819309_gshared (NavigationController_2_t906290919 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m182819309(__this, method) ((  void (*) (NavigationController_2_t906290919 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m182819309_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1335549778_gshared (NavigationController_2_t906290919 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m1335549778(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t906290919 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1335549778_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,FriendSearchNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m670594270_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m670594270(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t906290919 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m670594270_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,FriendSearchNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m901421583_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m901421583(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t906290919 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m901421583_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1791692064_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m1791692064(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t906290919 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1791692064_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m937344406_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m937344406(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t906290919 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m937344406_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2780911014_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m2780911014(__this, ___screen0, method) ((  void (*) (NavigationController_2_t906290919 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2780911014_gshared)(__this, ___screen0, method)
