﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusTabButton
struct StatusTabButton_t3949570411;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusTabButton::.ctor()
extern "C"  void StatusTabButton__ctor_m361372224 (StatusTabButton_t3949570411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusTabButton::OnEnable()
extern "C"  void StatusTabButton_OnEnable_m2183843964 (StatusTabButton_t3949570411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusTabButton::OnDisable()
extern "C"  void StatusTabButton_OnDisable_m1538573787 (StatusTabButton_t3949570411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StatusTabButton::IncomingStatusUpdate()
extern "C"  Il2CppObject * StatusTabButton_IncomingStatusUpdate_m1365797443 (StatusTabButton_t3949570411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusTabButton::UpdateStaus()
extern "C"  void StatusTabButton_UpdateStaus_m839514931 (StatusTabButton_t3949570411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusTabButton::OnButtonClick()
extern "C"  void StatusTabButton_OnButtonClick_m1722329087 (StatusTabButton_t3949570411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
