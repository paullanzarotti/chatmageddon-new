﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen677391278.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrefabResource
struct  PrefabResource_t926725558  : public MonoSingleton_1_t677391278
{
public:
	// System.String PrefabResource::basePath
	String_t* ___basePath_3;
	// System.String PrefabResource::clipPanel
	String_t* ___clipPanel_4;
	// System.String PrefabResource::attackPanel
	String_t* ___attackPanel_5;
	// System.String PrefabResource::friendsPanel
	String_t* ___friendsPanel_6;
	// System.String PrefabResource::storePanel
	String_t* ___storePanel_7;
	// System.String PrefabResource::statusPanel
	String_t* ___statusPanel_8;
	// System.String PrefabResource::leaderBoardPanel
	String_t* ___leaderBoardPanel_9;

public:
	inline static int32_t get_offset_of_basePath_3() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___basePath_3)); }
	inline String_t* get_basePath_3() const { return ___basePath_3; }
	inline String_t** get_address_of_basePath_3() { return &___basePath_3; }
	inline void set_basePath_3(String_t* value)
	{
		___basePath_3 = value;
		Il2CppCodeGenWriteBarrier(&___basePath_3, value);
	}

	inline static int32_t get_offset_of_clipPanel_4() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___clipPanel_4)); }
	inline String_t* get_clipPanel_4() const { return ___clipPanel_4; }
	inline String_t** get_address_of_clipPanel_4() { return &___clipPanel_4; }
	inline void set_clipPanel_4(String_t* value)
	{
		___clipPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___clipPanel_4, value);
	}

	inline static int32_t get_offset_of_attackPanel_5() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___attackPanel_5)); }
	inline String_t* get_attackPanel_5() const { return ___attackPanel_5; }
	inline String_t** get_address_of_attackPanel_5() { return &___attackPanel_5; }
	inline void set_attackPanel_5(String_t* value)
	{
		___attackPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___attackPanel_5, value);
	}

	inline static int32_t get_offset_of_friendsPanel_6() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___friendsPanel_6)); }
	inline String_t* get_friendsPanel_6() const { return ___friendsPanel_6; }
	inline String_t** get_address_of_friendsPanel_6() { return &___friendsPanel_6; }
	inline void set_friendsPanel_6(String_t* value)
	{
		___friendsPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___friendsPanel_6, value);
	}

	inline static int32_t get_offset_of_storePanel_7() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___storePanel_7)); }
	inline String_t* get_storePanel_7() const { return ___storePanel_7; }
	inline String_t** get_address_of_storePanel_7() { return &___storePanel_7; }
	inline void set_storePanel_7(String_t* value)
	{
		___storePanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___storePanel_7, value);
	}

	inline static int32_t get_offset_of_statusPanel_8() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___statusPanel_8)); }
	inline String_t* get_statusPanel_8() const { return ___statusPanel_8; }
	inline String_t** get_address_of_statusPanel_8() { return &___statusPanel_8; }
	inline void set_statusPanel_8(String_t* value)
	{
		___statusPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___statusPanel_8, value);
	}

	inline static int32_t get_offset_of_leaderBoardPanel_9() { return static_cast<int32_t>(offsetof(PrefabResource_t926725558, ___leaderBoardPanel_9)); }
	inline String_t* get_leaderBoardPanel_9() const { return ___leaderBoardPanel_9; }
	inline String_t** get_address_of_leaderBoardPanel_9() { return &___leaderBoardPanel_9; }
	inline void set_leaderBoardPanel_9(String_t* value)
	{
		___leaderBoardPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___leaderBoardPanel_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
