﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// libphonenumber_csharp_portable.XDocumentLegacy/<GetElementsByTagName>c__AnonStorey1
struct U3CGetElementsByTagNameU3Ec__AnonStorey1_t3380385870;
// System.Xml.Linq.XElement
struct XElement_t553821050;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"

// System.Void libphonenumber_csharp_portable.XDocumentLegacy/<GetElementsByTagName>c__AnonStorey1::.ctor()
extern "C"  void U3CGetElementsByTagNameU3Ec__AnonStorey1__ctor_m2041789361 (U3CGetElementsByTagNameU3Ec__AnonStorey1_t3380385870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean libphonenumber_csharp_portable.XDocumentLegacy/<GetElementsByTagName>c__AnonStorey1::<>m__0(System.Xml.Linq.XElement)
extern "C"  bool U3CGetElementsByTagNameU3Ec__AnonStorey1_U3CU3Em__0_m4050740594 (U3CGetElementsByTagNameU3Ec__AnonStorey1_t3380385870 * __this, XElement_t553821050 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
