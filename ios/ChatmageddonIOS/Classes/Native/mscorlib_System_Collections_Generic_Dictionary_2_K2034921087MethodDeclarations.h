﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>
struct KeyCollection_t2034921087;
// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Collections.Generic.IEnumerator`1<PanelType>
struct IEnumerator_1_t2253260353;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PanelType[]
struct PanelTypeU5BU5D_t884548027;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2240926754.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3725338696_gshared (KeyCollection_t2034921087 * __this, Dictionary_2_t3846390612 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3725338696(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2034921087 *, Dictionary_2_t3846390612 *, const MethodInfo*))KeyCollection__ctor_m3725338696_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3774205578_gshared (KeyCollection_t2034921087 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3774205578(__this, ___item0, method) ((  void (*) (KeyCollection_t2034921087 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3774205578_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m856998823_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m856998823(__this, method) ((  void (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m856998823_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2636661350_gshared (KeyCollection_t2034921087 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2636661350(__this, ___item0, method) ((  bool (*) (KeyCollection_t2034921087 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2636661350_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m874614855_gshared (KeyCollection_t2034921087 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m874614855(__this, ___item0, method) ((  bool (*) (KeyCollection_t2034921087 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m874614855_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1391786117_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1391786117(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1391786117_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m103082201_gshared (KeyCollection_t2034921087 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m103082201(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2034921087 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m103082201_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14508810_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14508810(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14508810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m891212851_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m891212851(__this, method) ((  bool (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m891212851_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m931222373_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m931222373(__this, method) ((  bool (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m931222373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1071395449_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1071395449(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1071395449_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3388582647_gshared (KeyCollection_t2034921087 * __this, PanelTypeU5BU5D_t884548027* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3388582647(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2034921087 *, PanelTypeU5BU5D_t884548027*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3388582647_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2240926754  KeyCollection_GetEnumerator_m3504578684_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3504578684(__this, method) ((  Enumerator_t2240926754  (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_GetEnumerator_m3504578684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3858243205_gshared (KeyCollection_t2034921087 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3858243205(__this, method) ((  int32_t (*) (KeyCollection_t2034921087 *, const MethodInfo*))KeyCollection_get_Count_m3858243205_gshared)(__this, method)
