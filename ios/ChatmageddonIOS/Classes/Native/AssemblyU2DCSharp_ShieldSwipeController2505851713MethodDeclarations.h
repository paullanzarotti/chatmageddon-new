﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldSwipeController
struct ShieldSwipeController_t2505851713;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldSwipeController::.ctor()
extern "C"  void ShieldSwipeController__ctor_m1224756412 (ShieldSwipeController_t2505851713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldSwipeController::DisplayItemModel()
extern "C"  void ShieldSwipeController_DisplayItemModel_m3296313560 (ShieldSwipeController_t2505851713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
