﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_t4065209745;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>
struct  U3CMinU3Ec__AnonStorey2E_1_t1049469630  : public Il2CppObject
{
public:
	// System.Func`2<TSource,System.Single> System.Linq.Enumerable/<Min>c__AnonStorey2E`1::selector
	Func_2_t4065209745 * ___selector_0;

public:
	inline static int32_t get_offset_of_selector_0() { return static_cast<int32_t>(offsetof(U3CMinU3Ec__AnonStorey2E_1_t1049469630, ___selector_0)); }
	inline Func_2_t4065209745 * get_selector_0() const { return ___selector_0; }
	inline Func_2_t4065209745 ** get_address_of_selector_0() { return &___selector_0; }
	inline void set_selector_0(Func_2_t4065209745 * value)
	{
		___selector_0 = value;
		Il2CppCodeGenWriteBarrier(&___selector_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
