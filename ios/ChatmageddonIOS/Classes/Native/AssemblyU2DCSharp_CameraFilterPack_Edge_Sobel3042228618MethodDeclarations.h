﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Edge_Sobel
struct CameraFilterPack_Edge_Sobel_t3042228618;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Edge_Sobel::.ctor()
extern "C"  void CameraFilterPack_Edge_Sobel__ctor_m1530507239 (CameraFilterPack_Edge_Sobel_t3042228618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Sobel::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Edge_Sobel_get_material_m4001053622 (CameraFilterPack_Edge_Sobel_t3042228618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::Start()
extern "C"  void CameraFilterPack_Edge_Sobel_Start_m2594261051 (CameraFilterPack_Edge_Sobel_t3042228618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Edge_Sobel_OnRenderImage_m1444707083 (CameraFilterPack_Edge_Sobel_t3042228618 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::Update()
extern "C"  void CameraFilterPack_Edge_Sobel_Update_m4053071636 (CameraFilterPack_Edge_Sobel_t3042228618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::OnDisable()
extern "C"  void CameraFilterPack_Edge_Sobel_OnDisable_m3907852466 (CameraFilterPack_Edge_Sobel_t3042228618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
