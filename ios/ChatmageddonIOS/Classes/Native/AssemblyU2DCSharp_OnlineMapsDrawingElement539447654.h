﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<OnlineMapsDrawingElement>
struct Action_1_t341247036;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsDrawingElement
struct  OnlineMapsDrawingElement_t539447654  : public Il2CppObject
{
public:
	// System.Action`1<OnlineMapsDrawingElement> OnlineMapsDrawingElement::OnClick
	Action_1_t341247036 * ___OnClick_6;
	// System.Action`1<OnlineMapsDrawingElement> OnlineMapsDrawingElement::OnDoubleClick
	Action_1_t341247036 * ___OnDoubleClick_7;
	// System.Action`1<OnlineMapsDrawingElement> OnlineMapsDrawingElement::OnDrawTooltip
	Action_1_t341247036 * ___OnDrawTooltip_8;
	// System.Action`1<OnlineMapsDrawingElement> OnlineMapsDrawingElement::OnPress
	Action_1_t341247036 * ___OnPress_9;
	// System.Action`1<OnlineMapsDrawingElement> OnlineMapsDrawingElement::OnRelease
	Action_1_t341247036 * ___OnRelease_10;
	// System.Object OnlineMapsDrawingElement::customData
	Il2CppObject * ___customData_11;
	// System.String OnlineMapsDrawingElement::tooltip
	String_t* ___tooltip_12;
	// UnityEngine.Mesh OnlineMapsDrawingElement::mesh
	Mesh_t1356156583 * ___mesh_13;
	// UnityEngine.GameObject OnlineMapsDrawingElement::gameObject
	GameObject_t1756533147 * ___gameObject_14;
	// System.Boolean OnlineMapsDrawingElement::_visible
	bool ____visible_15;
	// System.Single OnlineMapsDrawingElement::bestElevationYScale
	float ___bestElevationYScale_16;
	// System.Double OnlineMapsDrawingElement::tlx
	double ___tlx_17;
	// System.Double OnlineMapsDrawingElement::tly
	double ___tly_18;
	// System.Double OnlineMapsDrawingElement::brx
	double ___brx_19;
	// System.Double OnlineMapsDrawingElement::bry
	double ___bry_20;

public:
	inline static int32_t get_offset_of_OnClick_6() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___OnClick_6)); }
	inline Action_1_t341247036 * get_OnClick_6() const { return ___OnClick_6; }
	inline Action_1_t341247036 ** get_address_of_OnClick_6() { return &___OnClick_6; }
	inline void set_OnClick_6(Action_1_t341247036 * value)
	{
		___OnClick_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnClick_6, value);
	}

	inline static int32_t get_offset_of_OnDoubleClick_7() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___OnDoubleClick_7)); }
	inline Action_1_t341247036 * get_OnDoubleClick_7() const { return ___OnDoubleClick_7; }
	inline Action_1_t341247036 ** get_address_of_OnDoubleClick_7() { return &___OnDoubleClick_7; }
	inline void set_OnDoubleClick_7(Action_1_t341247036 * value)
	{
		___OnDoubleClick_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnDoubleClick_7, value);
	}

	inline static int32_t get_offset_of_OnDrawTooltip_8() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___OnDrawTooltip_8)); }
	inline Action_1_t341247036 * get_OnDrawTooltip_8() const { return ___OnDrawTooltip_8; }
	inline Action_1_t341247036 ** get_address_of_OnDrawTooltip_8() { return &___OnDrawTooltip_8; }
	inline void set_OnDrawTooltip_8(Action_1_t341247036 * value)
	{
		___OnDrawTooltip_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnDrawTooltip_8, value);
	}

	inline static int32_t get_offset_of_OnPress_9() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___OnPress_9)); }
	inline Action_1_t341247036 * get_OnPress_9() const { return ___OnPress_9; }
	inline Action_1_t341247036 ** get_address_of_OnPress_9() { return &___OnPress_9; }
	inline void set_OnPress_9(Action_1_t341247036 * value)
	{
		___OnPress_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnPress_9, value);
	}

	inline static int32_t get_offset_of_OnRelease_10() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___OnRelease_10)); }
	inline Action_1_t341247036 * get_OnRelease_10() const { return ___OnRelease_10; }
	inline Action_1_t341247036 ** get_address_of_OnRelease_10() { return &___OnRelease_10; }
	inline void set_OnRelease_10(Action_1_t341247036 * value)
	{
		___OnRelease_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnRelease_10, value);
	}

	inline static int32_t get_offset_of_customData_11() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___customData_11)); }
	inline Il2CppObject * get_customData_11() const { return ___customData_11; }
	inline Il2CppObject ** get_address_of_customData_11() { return &___customData_11; }
	inline void set_customData_11(Il2CppObject * value)
	{
		___customData_11 = value;
		Il2CppCodeGenWriteBarrier(&___customData_11, value);
	}

	inline static int32_t get_offset_of_tooltip_12() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___tooltip_12)); }
	inline String_t* get_tooltip_12() const { return ___tooltip_12; }
	inline String_t** get_address_of_tooltip_12() { return &___tooltip_12; }
	inline void set_tooltip_12(String_t* value)
	{
		___tooltip_12 = value;
		Il2CppCodeGenWriteBarrier(&___tooltip_12, value);
	}

	inline static int32_t get_offset_of_mesh_13() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___mesh_13)); }
	inline Mesh_t1356156583 * get_mesh_13() const { return ___mesh_13; }
	inline Mesh_t1356156583 ** get_address_of_mesh_13() { return &___mesh_13; }
	inline void set_mesh_13(Mesh_t1356156583 * value)
	{
		___mesh_13 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_13, value);
	}

	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___gameObject_14)); }
	inline GameObject_t1756533147 * get_gameObject_14() const { return ___gameObject_14; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(GameObject_t1756533147 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of__visible_15() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ____visible_15)); }
	inline bool get__visible_15() const { return ____visible_15; }
	inline bool* get_address_of__visible_15() { return &____visible_15; }
	inline void set__visible_15(bool value)
	{
		____visible_15 = value;
	}

	inline static int32_t get_offset_of_bestElevationYScale_16() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___bestElevationYScale_16)); }
	inline float get_bestElevationYScale_16() const { return ___bestElevationYScale_16; }
	inline float* get_address_of_bestElevationYScale_16() { return &___bestElevationYScale_16; }
	inline void set_bestElevationYScale_16(float value)
	{
		___bestElevationYScale_16 = value;
	}

	inline static int32_t get_offset_of_tlx_17() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___tlx_17)); }
	inline double get_tlx_17() const { return ___tlx_17; }
	inline double* get_address_of_tlx_17() { return &___tlx_17; }
	inline void set_tlx_17(double value)
	{
		___tlx_17 = value;
	}

	inline static int32_t get_offset_of_tly_18() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___tly_18)); }
	inline double get_tly_18() const { return ___tly_18; }
	inline double* get_address_of_tly_18() { return &___tly_18; }
	inline void set_tly_18(double value)
	{
		___tly_18 = value;
	}

	inline static int32_t get_offset_of_brx_19() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___brx_19)); }
	inline double get_brx_19() const { return ___brx_19; }
	inline double* get_address_of_brx_19() { return &___brx_19; }
	inline void set_brx_19(double value)
	{
		___brx_19 = value;
	}

	inline static int32_t get_offset_of_bry_20() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654, ___bry_20)); }
	inline double get_bry_20() const { return ___bry_20; }
	inline double* get_address_of_bry_20() { return &___bry_20; }
	inline void set_bry_20(double value)
	{
		___bry_20 = value;
	}
};

struct OnlineMapsDrawingElement_t539447654_StaticFields
{
public:
	// UnityEngine.Vector2 OnlineMapsDrawingElement::halfVector
	Vector2_t2243707579  ___halfVector_0;
	// UnityEngine.Vector2 OnlineMapsDrawingElement::sampleV1
	Vector2_t2243707579  ___sampleV1_1;
	// UnityEngine.Vector2 OnlineMapsDrawingElement::sampleV2
	Vector2_t2243707579  ___sampleV2_2;
	// UnityEngine.Vector2 OnlineMapsDrawingElement::sampleV3
	Vector2_t2243707579  ___sampleV3_3;
	// UnityEngine.Vector2 OnlineMapsDrawingElement::sampleV4
	Vector2_t2243707579  ___sampleV4_4;
	// System.Action`1<OnlineMapsDrawingElement> OnlineMapsDrawingElement::OnElementDrawTooltip
	Action_1_t341247036 * ___OnElementDrawTooltip_5;

public:
	inline static int32_t get_offset_of_halfVector_0() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654_StaticFields, ___halfVector_0)); }
	inline Vector2_t2243707579  get_halfVector_0() const { return ___halfVector_0; }
	inline Vector2_t2243707579 * get_address_of_halfVector_0() { return &___halfVector_0; }
	inline void set_halfVector_0(Vector2_t2243707579  value)
	{
		___halfVector_0 = value;
	}

	inline static int32_t get_offset_of_sampleV1_1() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654_StaticFields, ___sampleV1_1)); }
	inline Vector2_t2243707579  get_sampleV1_1() const { return ___sampleV1_1; }
	inline Vector2_t2243707579 * get_address_of_sampleV1_1() { return &___sampleV1_1; }
	inline void set_sampleV1_1(Vector2_t2243707579  value)
	{
		___sampleV1_1 = value;
	}

	inline static int32_t get_offset_of_sampleV2_2() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654_StaticFields, ___sampleV2_2)); }
	inline Vector2_t2243707579  get_sampleV2_2() const { return ___sampleV2_2; }
	inline Vector2_t2243707579 * get_address_of_sampleV2_2() { return &___sampleV2_2; }
	inline void set_sampleV2_2(Vector2_t2243707579  value)
	{
		___sampleV2_2 = value;
	}

	inline static int32_t get_offset_of_sampleV3_3() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654_StaticFields, ___sampleV3_3)); }
	inline Vector2_t2243707579  get_sampleV3_3() const { return ___sampleV3_3; }
	inline Vector2_t2243707579 * get_address_of_sampleV3_3() { return &___sampleV3_3; }
	inline void set_sampleV3_3(Vector2_t2243707579  value)
	{
		___sampleV3_3 = value;
	}

	inline static int32_t get_offset_of_sampleV4_4() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654_StaticFields, ___sampleV4_4)); }
	inline Vector2_t2243707579  get_sampleV4_4() const { return ___sampleV4_4; }
	inline Vector2_t2243707579 * get_address_of_sampleV4_4() { return &___sampleV4_4; }
	inline void set_sampleV4_4(Vector2_t2243707579  value)
	{
		___sampleV4_4 = value;
	}

	inline static int32_t get_offset_of_OnElementDrawTooltip_5() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingElement_t539447654_StaticFields, ___OnElementDrawTooltip_5)); }
	inline Action_1_t341247036 * get_OnElementDrawTooltip_5() const { return ___OnElementDrawTooltip_5; }
	inline Action_1_t341247036 ** get_address_of_OnElementDrawTooltip_5() { return &___OnElementDrawTooltip_5; }
	inline void set_OnElementDrawTooltip_5(Action_1_t341247036 * value)
	{
		___OnElementDrawTooltip_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnElementDrawTooltip_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
