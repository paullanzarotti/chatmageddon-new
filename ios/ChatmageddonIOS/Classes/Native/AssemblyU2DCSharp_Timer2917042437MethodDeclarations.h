﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Timer
struct Timer_t2917042437;

#include "codegen/il2cpp-codegen.h"

// System.Void Timer::.ctor()
extern "C"  void Timer__ctor_m128890648 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Timer::UpdateTimer()
extern "C"  void Timer_UpdateTimer_m1556172186 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Timer::checkTimer()
extern "C"  bool Timer_checkTimer_m1391103361 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Timer::checkTimerNum()
extern "C"  int32_t Timer_checkTimerNum_m3546344901 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Timer::checkFullTimerNum()
extern "C"  float Timer_checkFullTimerNum_m1323800980 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Timer::pauseTimer()
extern "C"  void Timer_pauseTimer_m3581486773 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Timer::startTimer()
extern "C"  void Timer_startTimer_m3504227943 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Timer::setTimer(System.Single)
extern "C"  void Timer_setTimer_m3135925636 (Timer_t2917042437 * __this, float ___newTimeToCountDown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Timer::resetTimer(System.Single)
extern "C"  void Timer_resetTimer_m85897959 (Timer_t2917042437 * __this, float ___newTimeToCountDown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
