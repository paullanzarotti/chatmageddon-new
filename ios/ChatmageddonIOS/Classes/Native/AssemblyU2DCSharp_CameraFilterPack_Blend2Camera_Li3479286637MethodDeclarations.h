﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Lighten
struct CameraFilterPack_Blend2Camera_Lighten_t3479286637;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Lighten::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Lighten__ctor_m1998864784 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Lighten::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Lighten_get_material_m2871664049 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Lighten_Start_m4124854536 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Lighten_OnRenderImage_m2568851768 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Lighten_OnValidate_m376884667 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Lighten_Update_m3087595501 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Lighten_OnEnable_m398102856 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Lighten_OnDisable_m2162099673 (CameraFilterPack_Blend2Camera_Lighten_t3479286637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
