﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffectBase898523515.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlitchEffect
struct  GlitchEffect_t4241067900  : public ImageEffectBase_t898523515
{
public:
	// UnityEngine.Texture2D GlitchEffect::displacementMap
	Texture2D_t3542995729 * ___displacementMap_4;
	// System.Single GlitchEffect::glitchup
	float ___glitchup_5;
	// System.Single GlitchEffect::glitchdown
	float ___glitchdown_6;
	// System.Single GlitchEffect::flicker
	float ___flicker_7;
	// System.Single GlitchEffect::glitchupTime
	float ___glitchupTime_8;
	// System.Single GlitchEffect::glitchdownTime
	float ___glitchdownTime_9;
	// System.Single GlitchEffect::flickerTime
	float ___flickerTime_10;
	// System.Single GlitchEffect::intensity
	float ___intensity_11;

public:
	inline static int32_t get_offset_of_displacementMap_4() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___displacementMap_4)); }
	inline Texture2D_t3542995729 * get_displacementMap_4() const { return ___displacementMap_4; }
	inline Texture2D_t3542995729 ** get_address_of_displacementMap_4() { return &___displacementMap_4; }
	inline void set_displacementMap_4(Texture2D_t3542995729 * value)
	{
		___displacementMap_4 = value;
		Il2CppCodeGenWriteBarrier(&___displacementMap_4, value);
	}

	inline static int32_t get_offset_of_glitchup_5() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___glitchup_5)); }
	inline float get_glitchup_5() const { return ___glitchup_5; }
	inline float* get_address_of_glitchup_5() { return &___glitchup_5; }
	inline void set_glitchup_5(float value)
	{
		___glitchup_5 = value;
	}

	inline static int32_t get_offset_of_glitchdown_6() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___glitchdown_6)); }
	inline float get_glitchdown_6() const { return ___glitchdown_6; }
	inline float* get_address_of_glitchdown_6() { return &___glitchdown_6; }
	inline void set_glitchdown_6(float value)
	{
		___glitchdown_6 = value;
	}

	inline static int32_t get_offset_of_flicker_7() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___flicker_7)); }
	inline float get_flicker_7() const { return ___flicker_7; }
	inline float* get_address_of_flicker_7() { return &___flicker_7; }
	inline void set_flicker_7(float value)
	{
		___flicker_7 = value;
	}

	inline static int32_t get_offset_of_glitchupTime_8() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___glitchupTime_8)); }
	inline float get_glitchupTime_8() const { return ___glitchupTime_8; }
	inline float* get_address_of_glitchupTime_8() { return &___glitchupTime_8; }
	inline void set_glitchupTime_8(float value)
	{
		___glitchupTime_8 = value;
	}

	inline static int32_t get_offset_of_glitchdownTime_9() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___glitchdownTime_9)); }
	inline float get_glitchdownTime_9() const { return ___glitchdownTime_9; }
	inline float* get_address_of_glitchdownTime_9() { return &___glitchdownTime_9; }
	inline void set_glitchdownTime_9(float value)
	{
		___glitchdownTime_9 = value;
	}

	inline static int32_t get_offset_of_flickerTime_10() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___flickerTime_10)); }
	inline float get_flickerTime_10() const { return ___flickerTime_10; }
	inline float* get_address_of_flickerTime_10() { return &___flickerTime_10; }
	inline void set_flickerTime_10(float value)
	{
		___flickerTime_10 = value;
	}

	inline static int32_t get_offset_of_intensity_11() { return static_cast<int32_t>(offsetof(GlitchEffect_t4241067900, ___intensity_11)); }
	inline float get_intensity_11() const { return ___intensity_11; }
	inline float* get_address_of_intensity_11() { return &___intensity_11; }
	inline void set_intensity_11(float value)
	{
		___intensity_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
