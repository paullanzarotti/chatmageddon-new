﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Uniject.ILogger
struct ILogger_t2858843691;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PurchasableItem>
struct List_1_t3332475031;
// System.Collections.Generic.List`1<VirtualCurrency>
struct List_1_t484619012;
// System.Predicate`1<PurchasableItem>
struct Predicate_1_t2406324014;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2745005863.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.UnibillConfiguration
struct  UnibillConfiguration_t2915611853  : public Il2CppObject
{
public:
	// Uniject.ILogger Unibill.Impl.UnibillConfiguration::logger
	Il2CppObject * ___logger_0;
	// Unibill.Impl.BillingPlatform Unibill.Impl.UnibillConfiguration::<CurrentPlatform>k__BackingField
	int32_t ___U3CCurrentPlatformU3Ek__BackingField_1;
	// System.String Unibill.Impl.UnibillConfiguration::<iOSSKU>k__BackingField
	String_t* ___U3CiOSSKUU3Ek__BackingField_2;
	// System.String Unibill.Impl.UnibillConfiguration::<macAppStoreSKU>k__BackingField
	String_t* ___U3CmacAppStoreSKUU3Ek__BackingField_3;
	// Unibill.Impl.BillingPlatform Unibill.Impl.UnibillConfiguration::<AndroidBillingPlatform>k__BackingField
	int32_t ___U3CAndroidBillingPlatformU3Ek__BackingField_4;
	// System.String Unibill.Impl.UnibillConfiguration::<GooglePlayPublicKey>k__BackingField
	String_t* ___U3CGooglePlayPublicKeyU3Ek__BackingField_5;
	// System.Boolean Unibill.Impl.UnibillConfiguration::<AmazonSandboxEnabled>k__BackingField
	bool ___U3CAmazonSandboxEnabledU3Ek__BackingField_6;
	// System.Boolean Unibill.Impl.UnibillConfiguration::<WP8SandboxEnabled>k__BackingField
	bool ___U3CWP8SandboxEnabledU3Ek__BackingField_7;
	// System.Boolean Unibill.Impl.UnibillConfiguration::<UseHostedConfig>k__BackingField
	bool ___U3CUseHostedConfigU3Ek__BackingField_8;
	// System.String Unibill.Impl.UnibillConfiguration::<HostedConfigUrl>k__BackingField
	String_t* ___U3CHostedConfigUrlU3Ek__BackingField_9;
	// System.String Unibill.Impl.UnibillConfiguration::<UnibillAnalyticsAppId>k__BackingField
	String_t* ___U3CUnibillAnalyticsAppIdU3Ek__BackingField_10;
	// System.String Unibill.Impl.UnibillConfiguration::<UnibillAnalyticsAppSecret>k__BackingField
	String_t* ___U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11;
	// System.Boolean Unibill.Impl.UnibillConfiguration::<UseWin8_1Sandbox>k__BackingField
	bool ___U3CUseWin8_1SandboxU3Ek__BackingField_12;
	// Unibill.Impl.SamsungAppsMode Unibill.Impl.UnibillConfiguration::<SamsungAppsMode>k__BackingField
	int32_t ___U3CSamsungAppsModeU3Ek__BackingField_13;
	// System.String Unibill.Impl.UnibillConfiguration::<SamsungItemGroupId>k__BackingField
	String_t* ___U3CSamsungItemGroupIdU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::inventory
	List_1_t3332475031 * ___inventory_15;
	// System.Collections.Generic.List`1<VirtualCurrency> Unibill.Impl.UnibillConfiguration::currencies
	List_1_t484619012 * ___currencies_16;

public:
	inline static int32_t get_offset_of_logger_0() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___logger_0)); }
	inline Il2CppObject * get_logger_0() const { return ___logger_0; }
	inline Il2CppObject ** get_address_of_logger_0() { return &___logger_0; }
	inline void set_logger_0(Il2CppObject * value)
	{
		___logger_0 = value;
		Il2CppCodeGenWriteBarrier(&___logger_0, value);
	}

	inline static int32_t get_offset_of_U3CCurrentPlatformU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CCurrentPlatformU3Ek__BackingField_1)); }
	inline int32_t get_U3CCurrentPlatformU3Ek__BackingField_1() const { return ___U3CCurrentPlatformU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCurrentPlatformU3Ek__BackingField_1() { return &___U3CCurrentPlatformU3Ek__BackingField_1; }
	inline void set_U3CCurrentPlatformU3Ek__BackingField_1(int32_t value)
	{
		___U3CCurrentPlatformU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CiOSSKUU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CiOSSKUU3Ek__BackingField_2)); }
	inline String_t* get_U3CiOSSKUU3Ek__BackingField_2() const { return ___U3CiOSSKUU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CiOSSKUU3Ek__BackingField_2() { return &___U3CiOSSKUU3Ek__BackingField_2; }
	inline void set_U3CiOSSKUU3Ek__BackingField_2(String_t* value)
	{
		___U3CiOSSKUU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CiOSSKUU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CmacAppStoreSKUU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CmacAppStoreSKUU3Ek__BackingField_3)); }
	inline String_t* get_U3CmacAppStoreSKUU3Ek__BackingField_3() const { return ___U3CmacAppStoreSKUU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CmacAppStoreSKUU3Ek__BackingField_3() { return &___U3CmacAppStoreSKUU3Ek__BackingField_3; }
	inline void set_U3CmacAppStoreSKUU3Ek__BackingField_3(String_t* value)
	{
		___U3CmacAppStoreSKUU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmacAppStoreSKUU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CAndroidBillingPlatformU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CAndroidBillingPlatformU3Ek__BackingField_4)); }
	inline int32_t get_U3CAndroidBillingPlatformU3Ek__BackingField_4() const { return ___U3CAndroidBillingPlatformU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CAndroidBillingPlatformU3Ek__BackingField_4() { return &___U3CAndroidBillingPlatformU3Ek__BackingField_4; }
	inline void set_U3CAndroidBillingPlatformU3Ek__BackingField_4(int32_t value)
	{
		___U3CAndroidBillingPlatformU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGooglePlayPublicKeyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CGooglePlayPublicKeyU3Ek__BackingField_5)); }
	inline String_t* get_U3CGooglePlayPublicKeyU3Ek__BackingField_5() const { return ___U3CGooglePlayPublicKeyU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CGooglePlayPublicKeyU3Ek__BackingField_5() { return &___U3CGooglePlayPublicKeyU3Ek__BackingField_5; }
	inline void set_U3CGooglePlayPublicKeyU3Ek__BackingField_5(String_t* value)
	{
		___U3CGooglePlayPublicKeyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGooglePlayPublicKeyU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CAmazonSandboxEnabledU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CAmazonSandboxEnabledU3Ek__BackingField_6)); }
	inline bool get_U3CAmazonSandboxEnabledU3Ek__BackingField_6() const { return ___U3CAmazonSandboxEnabledU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CAmazonSandboxEnabledU3Ek__BackingField_6() { return &___U3CAmazonSandboxEnabledU3Ek__BackingField_6; }
	inline void set_U3CAmazonSandboxEnabledU3Ek__BackingField_6(bool value)
	{
		___U3CAmazonSandboxEnabledU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CWP8SandboxEnabledU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CWP8SandboxEnabledU3Ek__BackingField_7)); }
	inline bool get_U3CWP8SandboxEnabledU3Ek__BackingField_7() const { return ___U3CWP8SandboxEnabledU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CWP8SandboxEnabledU3Ek__BackingField_7() { return &___U3CWP8SandboxEnabledU3Ek__BackingField_7; }
	inline void set_U3CWP8SandboxEnabledU3Ek__BackingField_7(bool value)
	{
		___U3CWP8SandboxEnabledU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUseHostedConfigU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CUseHostedConfigU3Ek__BackingField_8)); }
	inline bool get_U3CUseHostedConfigU3Ek__BackingField_8() const { return ___U3CUseHostedConfigU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CUseHostedConfigU3Ek__BackingField_8() { return &___U3CUseHostedConfigU3Ek__BackingField_8; }
	inline void set_U3CUseHostedConfigU3Ek__BackingField_8(bool value)
	{
		___U3CUseHostedConfigU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CHostedConfigUrlU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CHostedConfigUrlU3Ek__BackingField_9)); }
	inline String_t* get_U3CHostedConfigUrlU3Ek__BackingField_9() const { return ___U3CHostedConfigUrlU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CHostedConfigUrlU3Ek__BackingField_9() { return &___U3CHostedConfigUrlU3Ek__BackingField_9; }
	inline void set_U3CHostedConfigUrlU3Ek__BackingField_9(String_t* value)
	{
		___U3CHostedConfigUrlU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHostedConfigUrlU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CUnibillAnalyticsAppIdU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CUnibillAnalyticsAppIdU3Ek__BackingField_10)); }
	inline String_t* get_U3CUnibillAnalyticsAppIdU3Ek__BackingField_10() const { return ___U3CUnibillAnalyticsAppIdU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUnibillAnalyticsAppIdU3Ek__BackingField_10() { return &___U3CUnibillAnalyticsAppIdU3Ek__BackingField_10; }
	inline void set_U3CUnibillAnalyticsAppIdU3Ek__BackingField_10(String_t* value)
	{
		___U3CUnibillAnalyticsAppIdU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnibillAnalyticsAppIdU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11)); }
	inline String_t* get_U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11() const { return ___U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11() { return &___U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11; }
	inline void set_U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11(String_t* value)
	{
		___U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CUseWin8_1SandboxU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CUseWin8_1SandboxU3Ek__BackingField_12)); }
	inline bool get_U3CUseWin8_1SandboxU3Ek__BackingField_12() const { return ___U3CUseWin8_1SandboxU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CUseWin8_1SandboxU3Ek__BackingField_12() { return &___U3CUseWin8_1SandboxU3Ek__BackingField_12; }
	inline void set_U3CUseWin8_1SandboxU3Ek__BackingField_12(bool value)
	{
		___U3CUseWin8_1SandboxU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CSamsungAppsModeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CSamsungAppsModeU3Ek__BackingField_13)); }
	inline int32_t get_U3CSamsungAppsModeU3Ek__BackingField_13() const { return ___U3CSamsungAppsModeU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CSamsungAppsModeU3Ek__BackingField_13() { return &___U3CSamsungAppsModeU3Ek__BackingField_13; }
	inline void set_U3CSamsungAppsModeU3Ek__BackingField_13(int32_t value)
	{
		___U3CSamsungAppsModeU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSamsungItemGroupIdU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___U3CSamsungItemGroupIdU3Ek__BackingField_14)); }
	inline String_t* get_U3CSamsungItemGroupIdU3Ek__BackingField_14() const { return ___U3CSamsungItemGroupIdU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CSamsungItemGroupIdU3Ek__BackingField_14() { return &___U3CSamsungItemGroupIdU3Ek__BackingField_14; }
	inline void set_U3CSamsungItemGroupIdU3Ek__BackingField_14(String_t* value)
	{
		___U3CSamsungItemGroupIdU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSamsungItemGroupIdU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_inventory_15() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___inventory_15)); }
	inline List_1_t3332475031 * get_inventory_15() const { return ___inventory_15; }
	inline List_1_t3332475031 ** get_address_of_inventory_15() { return &___inventory_15; }
	inline void set_inventory_15(List_1_t3332475031 * value)
	{
		___inventory_15 = value;
		Il2CppCodeGenWriteBarrier(&___inventory_15, value);
	}

	inline static int32_t get_offset_of_currencies_16() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853, ___currencies_16)); }
	inline List_1_t484619012 * get_currencies_16() const { return ___currencies_16; }
	inline List_1_t484619012 ** get_address_of_currencies_16() { return &___currencies_16; }
	inline void set_currencies_16(List_1_t484619012 * value)
	{
		___currencies_16 = value;
		Il2CppCodeGenWriteBarrier(&___currencies_16, value);
	}
};

struct UnibillConfiguration_t2915611853_StaticFields
{
public:
	// System.Predicate`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::<>f__am$cache0
	Predicate_1_t2406324014 * ___U3CU3Ef__amU24cache0_17;
	// System.Predicate`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::<>f__am$cache1
	Predicate_1_t2406324014 * ___U3CU3Ef__amU24cache1_18;
	// System.Predicate`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::<>f__am$cache2
	Predicate_1_t2406324014 * ___U3CU3Ef__amU24cache2_19;
	// System.Predicate`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::<>f__am$cache3
	Predicate_1_t2406324014 * ___U3CU3Ef__amU24cache3_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Predicate_1_t2406324014 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Predicate_1_t2406324014 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Predicate_1_t2406324014 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_18() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853_StaticFields, ___U3CU3Ef__amU24cache1_18)); }
	inline Predicate_1_t2406324014 * get_U3CU3Ef__amU24cache1_18() const { return ___U3CU3Ef__amU24cache1_18; }
	inline Predicate_1_t2406324014 ** get_address_of_U3CU3Ef__amU24cache1_18() { return &___U3CU3Ef__amU24cache1_18; }
	inline void set_U3CU3Ef__amU24cache1_18(Predicate_1_t2406324014 * value)
	{
		___U3CU3Ef__amU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_19() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853_StaticFields, ___U3CU3Ef__amU24cache2_19)); }
	inline Predicate_1_t2406324014 * get_U3CU3Ef__amU24cache2_19() const { return ___U3CU3Ef__amU24cache2_19; }
	inline Predicate_1_t2406324014 ** get_address_of_U3CU3Ef__amU24cache2_19() { return &___U3CU3Ef__amU24cache2_19; }
	inline void set_U3CU3Ef__amU24cache2_19(Predicate_1_t2406324014 * value)
	{
		___U3CU3Ef__amU24cache2_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_20() { return static_cast<int32_t>(offsetof(UnibillConfiguration_t2915611853_StaticFields, ___U3CU3Ef__amU24cache3_20)); }
	inline Predicate_1_t2406324014 * get_U3CU3Ef__amU24cache3_20() const { return ___U3CU3Ef__amU24cache3_20; }
	inline Predicate_1_t2406324014 ** get_address_of_U3CU3Ef__amU24cache3_20() { return &___U3CU3Ef__amU24cache3_20; }
	inline void set_U3CU3Ef__amU24cache3_20(Predicate_1_t2406324014 * value)
	{
		___U3CU3Ef__amU24cache3_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
