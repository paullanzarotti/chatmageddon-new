﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackMiniGameController
struct AttackMiniGameController_t105133407;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// FireGuage
struct FireGuage_t4001636259;
// TweenPosition
struct TweenPosition_t1144714832;
// TweenColor
struct TweenColor_t3390486518;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackMiniGameStateButton
struct  AttackMiniGameStateButton_t3439103466  : public SFXButton_t792651341
{
public:
	// AttackMiniGameController AttackMiniGameStateButton::gameController
	AttackMiniGameController_t105133407 * ___gameController_5;
	// UnityEngine.GameObject AttackMiniGameStateButton::topLine
	GameObject_t1756533147 * ___topLine_6;
	// FireGuage AttackMiniGameStateButton::fireGuage
	FireGuage_t4001636259 * ___fireGuage_7;
	// TweenPosition AttackMiniGameStateButton::buttonsTween
	TweenPosition_t1144714832 * ___buttonsTween_8;
	// TweenColor AttackMiniGameStateButton::colourTween
	TweenColor_t3390486518 * ___colourTween_9;
	// UILabel AttackMiniGameStateButton::buttonlabel
	UILabel_t1795115428 * ___buttonlabel_10;
	// System.Boolean AttackMiniGameStateButton::buttonsClosing
	bool ___buttonsClosing_11;

public:
	inline static int32_t get_offset_of_gameController_5() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___gameController_5)); }
	inline AttackMiniGameController_t105133407 * get_gameController_5() const { return ___gameController_5; }
	inline AttackMiniGameController_t105133407 ** get_address_of_gameController_5() { return &___gameController_5; }
	inline void set_gameController_5(AttackMiniGameController_t105133407 * value)
	{
		___gameController_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameController_5, value);
	}

	inline static int32_t get_offset_of_topLine_6() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___topLine_6)); }
	inline GameObject_t1756533147 * get_topLine_6() const { return ___topLine_6; }
	inline GameObject_t1756533147 ** get_address_of_topLine_6() { return &___topLine_6; }
	inline void set_topLine_6(GameObject_t1756533147 * value)
	{
		___topLine_6 = value;
		Il2CppCodeGenWriteBarrier(&___topLine_6, value);
	}

	inline static int32_t get_offset_of_fireGuage_7() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___fireGuage_7)); }
	inline FireGuage_t4001636259 * get_fireGuage_7() const { return ___fireGuage_7; }
	inline FireGuage_t4001636259 ** get_address_of_fireGuage_7() { return &___fireGuage_7; }
	inline void set_fireGuage_7(FireGuage_t4001636259 * value)
	{
		___fireGuage_7 = value;
		Il2CppCodeGenWriteBarrier(&___fireGuage_7, value);
	}

	inline static int32_t get_offset_of_buttonsTween_8() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___buttonsTween_8)); }
	inline TweenPosition_t1144714832 * get_buttonsTween_8() const { return ___buttonsTween_8; }
	inline TweenPosition_t1144714832 ** get_address_of_buttonsTween_8() { return &___buttonsTween_8; }
	inline void set_buttonsTween_8(TweenPosition_t1144714832 * value)
	{
		___buttonsTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsTween_8, value);
	}

	inline static int32_t get_offset_of_colourTween_9() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___colourTween_9)); }
	inline TweenColor_t3390486518 * get_colourTween_9() const { return ___colourTween_9; }
	inline TweenColor_t3390486518 ** get_address_of_colourTween_9() { return &___colourTween_9; }
	inline void set_colourTween_9(TweenColor_t3390486518 * value)
	{
		___colourTween_9 = value;
		Il2CppCodeGenWriteBarrier(&___colourTween_9, value);
	}

	inline static int32_t get_offset_of_buttonlabel_10() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___buttonlabel_10)); }
	inline UILabel_t1795115428 * get_buttonlabel_10() const { return ___buttonlabel_10; }
	inline UILabel_t1795115428 ** get_address_of_buttonlabel_10() { return &___buttonlabel_10; }
	inline void set_buttonlabel_10(UILabel_t1795115428 * value)
	{
		___buttonlabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___buttonlabel_10, value);
	}

	inline static int32_t get_offset_of_buttonsClosing_11() { return static_cast<int32_t>(offsetof(AttackMiniGameStateButton_t3439103466, ___buttonsClosing_11)); }
	inline bool get_buttonsClosing_11() const { return ___buttonsClosing_11; }
	inline bool* get_address_of_buttonsClosing_11() { return &___buttonsClosing_11; }
	inline void set_buttonsClosing_11(bool value)
	{
		___buttonsClosing_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
