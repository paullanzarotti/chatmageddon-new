﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookBatchRequest
struct FacebookBatchRequest_t2176192103;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.FacebookBatchRequest::.ctor(System.String,System.String)
extern "C"  void FacebookBatchRequest__ctor_m86235855 (FacebookBatchRequest_t2176192103 * __this, String_t* ___relativeUrl0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBatchRequest::addParameter(System.String,System.String)
extern "C"  void FacebookBatchRequest_addParameter_m708711251 (FacebookBatchRequest_t2176192103 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.FacebookBatchRequest::requestDictionary()
extern "C"  Dictionary_2_t309261261 * FacebookBatchRequest_requestDictionary_m4077402106 (FacebookBatchRequest_t2176192103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
