﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeUIScaler
struct AttackHomeUIScaler_t339922071;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackHomeUIScaler::.ctor()
extern "C"  void AttackHomeUIScaler__ctor_m3398950908 (AttackHomeUIScaler_t339922071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeUIScaler::GlobalUIScale()
extern "C"  void AttackHomeUIScaler_GlobalUIScale_m3978512529 (AttackHomeUIScaler_t339922071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
