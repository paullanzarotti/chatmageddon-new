﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecursiveCharacterPopulator/OnPopulationComplete
struct OnPopulationComplete_t1952569739;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void RecursiveCharacterPopulator/OnPopulationComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPopulationComplete__ctor_m3380470832 (OnPopulationComplete_t1952569739 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator/OnPopulationComplete::Invoke()
extern "C"  void OnPopulationComplete_Invoke_m2883168618 (OnPopulationComplete_t1952569739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult RecursiveCharacterPopulator/OnPopulationComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPopulationComplete_BeginInvoke_m1953313581 (OnPopulationComplete_t1952569739 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator/OnPopulationComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnPopulationComplete_EndInvoke_m1664890190 (OnPopulationComplete_t1952569739 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
