﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LauncherLoaderUIScaler
struct LauncherLoaderUIScaler_t1442786027;

#include "codegen/il2cpp-codegen.h"

// System.Void LauncherLoaderUIScaler::.ctor()
extern "C"  void LauncherLoaderUIScaler__ctor_m2541160904 (LauncherLoaderUIScaler_t1442786027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LauncherLoaderUIScaler::GlobalUIScale()
extern "C"  void LauncherLoaderUIScaler_GlobalUIScale_m3445324653 (LauncherLoaderUIScaler_t1442786027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
