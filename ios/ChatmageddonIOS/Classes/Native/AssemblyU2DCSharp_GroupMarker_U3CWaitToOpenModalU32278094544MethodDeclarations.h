﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GroupMarker/<WaitToOpenModal>c__Iterator0
struct U3CWaitToOpenModalU3Ec__Iterator0_t2278094544;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GroupMarker/<WaitToOpenModal>c__Iterator0::.ctor()
extern "C"  void U3CWaitToOpenModalU3Ec__Iterator0__ctor_m2570814969 (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GroupMarker/<WaitToOpenModal>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToOpenModalU3Ec__Iterator0_MoveNext_m173618767 (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GroupMarker/<WaitToOpenModal>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToOpenModalU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m557947083 (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GroupMarker/<WaitToOpenModal>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToOpenModalU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2867552611 (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker/<WaitToOpenModal>c__Iterator0::Dispose()
extern "C"  void U3CWaitToOpenModalU3Ec__Iterator0_Dispose_m1909285242 (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker/<WaitToOpenModal>c__Iterator0::Reset()
extern "C"  void U3CWaitToOpenModalU3Ec__Iterator0_Reset_m1504200444 (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
