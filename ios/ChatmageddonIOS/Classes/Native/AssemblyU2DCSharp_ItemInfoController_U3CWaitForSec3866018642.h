﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// ItemInfoController
struct ItemInfoController_t4231956141;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemInfoController/<WaitForSeconds>c__Iterator0
struct  U3CWaitForSecondsU3Ec__Iterator0_t3866018642  : public Il2CppObject
{
public:
	// System.Single ItemInfoController/<WaitForSeconds>c__Iterator0::waitTime
	float ___waitTime_0;
	// System.Action`1<System.Boolean> ItemInfoController/<WaitForSeconds>c__Iterator0::action
	Action_1_t3627374100 * ___action_1;
	// ItemInfoController ItemInfoController/<WaitForSeconds>c__Iterator0::$this
	ItemInfoController_t4231956141 * ___U24this_2;
	// System.Object ItemInfoController/<WaitForSeconds>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean ItemInfoController/<WaitForSeconds>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 ItemInfoController/<WaitForSeconds>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator0_t3866018642, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator0_t3866018642, ___action_1)); }
	inline Action_1_t3627374100 * get_action_1() const { return ___action_1; }
	inline Action_1_t3627374100 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3627374100 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator0_t3866018642, ___U24this_2)); }
	inline ItemInfoController_t4231956141 * get_U24this_2() const { return ___U24this_2; }
	inline ItemInfoController_t4231956141 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ItemInfoController_t4231956141 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator0_t3866018642, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator0_t3866018642, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator0_t3866018642, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
