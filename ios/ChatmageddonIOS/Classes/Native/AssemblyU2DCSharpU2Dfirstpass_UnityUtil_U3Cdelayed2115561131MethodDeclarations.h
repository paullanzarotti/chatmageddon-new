﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityUtil/<delayedCoroutine>c__Iterator0
struct U3CdelayedCoroutineU3Ec__Iterator0_t2115561131;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityUtil/<delayedCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CdelayedCoroutineU3Ec__Iterator0__ctor_m1849262376 (U3CdelayedCoroutineU3Ec__Iterator0_t2115561131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityUtil/<delayedCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CdelayedCoroutineU3Ec__Iterator0_MoveNext_m1983502656 (U3CdelayedCoroutineU3Ec__Iterator0_t2115561131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityUtil/<delayedCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdelayedCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m133572036 (U3CdelayedCoroutineU3Ec__Iterator0_t2115561131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityUtil/<delayedCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdelayedCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1305165708 (U3CdelayedCoroutineU3Ec__Iterator0_t2115561131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil/<delayedCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CdelayedCoroutineU3Ec__Iterator0_Dispose_m2912627403 (U3CdelayedCoroutineU3Ec__Iterator0_t2115561131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil/<delayedCoroutine>c__Iterator0::Reset()
extern "C"  void U3CdelayedCoroutineU3Ec__Iterator0_Reset_m164682513 (U3CdelayedCoroutineU3Ec__Iterator0_t2115561131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
