﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AttackHomeNav>
struct List_1_t3314011636;
// System.Collections.Generic.IEnumerable`1<AttackHomeNav>
struct IEnumerable_1_t4237017549;
// AttackHomeNav[]
struct AttackHomeNavU5BU5D_t3086173273;
// System.Collections.Generic.IEnumerator`1<AttackHomeNav>
struct IEnumerator_1_t1420414331;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<AttackHomeNav>
struct ICollection_1_t601998513;
// System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>
struct ReadOnlyCollection_1_t4130676196;
// System.Predicate`1<AttackHomeNav>
struct Predicate_1_t2387860619;
// System.Action`1<AttackHomeNav>
struct Action_1_t3746689886;
// System.Collections.Generic.IComparer`1<AttackHomeNav>
struct IComparer_1_t1899353626;
// System.Comparison`1<AttackHomeNav>
struct Comparison_1_t911662059;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2848741310.h"

// System.Void System.Collections.Generic.List`1<AttackHomeNav>::.ctor()
extern "C"  void List_1__ctor_m3589476732_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1__ctor_m3589476732(__this, method) ((  void (*) (List_1_t3314011636 *, const MethodInfo*))List_1__ctor_m3589476732_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1618041863_gshared (List_1_t3314011636 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1618041863(__this, ___collection0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1618041863_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1365369313_gshared (List_1_t3314011636 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1365369313(__this, ___capacity0, method) ((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1__ctor_m1365369313_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m4002614211_gshared (List_1_t3314011636 * __this, AttackHomeNavU5BU5D_t3086173273* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m4002614211(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3314011636 *, AttackHomeNavU5BU5D_t3086173273*, int32_t, const MethodInfo*))List_1__ctor_m4002614211_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::.cctor()
extern "C"  void List_1__cctor_m2139913911_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2139913911(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2139913911_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1948637114_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1948637114(__this, method) ((  Il2CppObject* (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1948637114_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3319329924_gshared (List_1_t3314011636 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3319329924(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3314011636 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3319329924_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1685776033_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1685776033(__this, method) ((  Il2CppObject * (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1685776033_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1782285788_gshared (List_1_t3314011636 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1782285788(__this, ___item0, method) ((  int32_t (*) (List_1_t3314011636 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1782285788_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2186429082_gshared (List_1_t3314011636 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2186429082(__this, ___item0, method) ((  bool (*) (List_1_t3314011636 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2186429082_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1205791094_gshared (List_1_t3314011636 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1205791094(__this, ___item0, method) ((  int32_t (*) (List_1_t3314011636 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1205791094_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3159227153_gshared (List_1_t3314011636 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3159227153(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3314011636 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3159227153_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m619931313_gshared (List_1_t3314011636 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m619931313(__this, ___item0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m619931313_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1856328069_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1856328069(__this, method) ((  bool (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1856328069_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m476097988_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m476097988(__this, method) ((  bool (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m476097988_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2720831780_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2720831780(__this, method) ((  Il2CppObject * (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2720831780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3626799877_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3626799877(__this, method) ((  bool (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3626799877_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3029421128_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3029421128(__this, method) ((  bool (*) (List_1_t3314011636 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3029421128_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m943382237_gshared (List_1_t3314011636 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m943382237(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m943382237_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3673619586_gshared (List_1_t3314011636 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3673619586(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3314011636 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3673619586_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Add(T)
extern "C"  void List_1_Add_m2504544237_gshared (List_1_t3314011636 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2504544237(__this, ___item0, method) ((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_Add_m2504544237_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3476691886_gshared (List_1_t3314011636 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3476691886(__this, ___newCount0, method) ((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3476691886_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1468379777_gshared (List_1_t3314011636 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1468379777(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3314011636 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1468379777_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2662512670_gshared (List_1_t3314011636 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2662512670(__this, ___collection0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2662512670_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3835195198_gshared (List_1_t3314011636 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3835195198(__this, ___enumerable0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3835195198_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1350203153_gshared (List_1_t3314011636 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1350203153(__this, ___collection0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1350203153_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AttackHomeNav>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4130676196 * List_1_AsReadOnly_m741877968_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m741877968(__this, method) ((  ReadOnlyCollection_1_t4130676196 * (*) (List_1_t3314011636 *, const MethodInfo*))List_1_AsReadOnly_m741877968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Clear()
extern "C"  void List_1_Clear_m3315540193_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_Clear_m3315540193(__this, method) ((  void (*) (List_1_t3314011636 *, const MethodInfo*))List_1_Clear_m3315540193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::Contains(T)
extern "C"  bool List_1_Contains_m825671431_gshared (List_1_t3314011636 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m825671431(__this, ___item0, method) ((  bool (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_Contains_m825671431_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2147757100_gshared (List_1_t3314011636 * __this, AttackHomeNavU5BU5D_t3086173273* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m2147757100(__this, ___array0, method) ((  void (*) (List_1_t3314011636 *, AttackHomeNavU5BU5D_t3086173273*, const MethodInfo*))List_1_CopyTo_m2147757100_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1981208037_gshared (List_1_t3314011636 * __this, AttackHomeNavU5BU5D_t3086173273* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1981208037(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3314011636 *, AttackHomeNavU5BU5D_t3086173273*, int32_t, const MethodInfo*))List_1_CopyTo_m1981208037_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m107075141_gshared (List_1_t3314011636 * __this, int32_t ___index0, AttackHomeNavU5BU5D_t3086173273* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m107075141(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3314011636 *, int32_t, AttackHomeNavU5BU5D_t3086173273*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m107075141_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m703040861_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_Exists_m703040861(__this, ___match0, method) ((  bool (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_Exists_m703040861_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<AttackHomeNav>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2467047523_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_Find_m2467047523(__this, ___match0, method) ((  int32_t (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_Find_m2467047523_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1976159756_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1976159756(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2387860619 *, const MethodInfo*))List_1_CheckMatch_m1976159756_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackHomeNav>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3314011636 * List_1_FindAll_m3187024882_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m3187024882(__this, ___match0, method) ((  List_1_t3314011636 * (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_FindAll_m3187024882_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackHomeNav>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3314011636 * List_1_FindAllStackBits_m1455540906_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1455540906(__this, ___match0, method) ((  List_1_t3314011636 * (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_FindAllStackBits_m1455540906_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackHomeNav>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3314011636 * List_1_FindAllList_m1784317130_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m1784317130(__this, ___match0, method) ((  List_1_t3314011636 * (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_FindAllList_m1784317130_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m198333878_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m198333878(__this, ___match0, method) ((  int32_t (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_FindIndex_m198333878_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m386138247_gshared (List_1_t3314011636 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2387860619 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m386138247(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3314011636 *, int32_t, int32_t, Predicate_1_t2387860619 *, const MethodInfo*))List_1_GetIndex_m386138247_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m141988928_gshared (List_1_t3314011636 * __this, Action_1_t3746689886 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m141988928(__this, ___action0, method) ((  void (*) (List_1_t3314011636 *, Action_1_t3746689886 *, const MethodInfo*))List_1_ForEach_m141988928_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AttackHomeNav>::GetEnumerator()
extern "C"  Enumerator_t2848741310  List_1_GetEnumerator_m3057714012_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3057714012(__this, method) ((  Enumerator_t2848741310  (*) (List_1_t3314011636 *, const MethodInfo*))List_1_GetEnumerator_m3057714012_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2641867025_gshared (List_1_t3314011636 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2641867025(__this, ___item0, method) ((  int32_t (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_IndexOf_m2641867025_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3873679160_gshared (List_1_t3314011636 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3873679160(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3314011636 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3873679160_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m65028601_gshared (List_1_t3314011636 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m65028601(__this, ___index0, method) ((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_CheckIndex_m65028601_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m369647842_gshared (List_1_t3314011636 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m369647842(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3314011636 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m369647842_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3407553659_gshared (List_1_t3314011636 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3407553659(__this, ___collection0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3407553659_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<AttackHomeNav>::Remove(T)
extern "C"  bool List_1_Remove_m3957550484_gshared (List_1_t3314011636 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3957550484(__this, ___item0, method) ((  bool (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_Remove_m3957550484_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2880703788_gshared (List_1_t3314011636 * __this, Predicate_1_t2387860619 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2880703788(__this, ___match0, method) ((  int32_t (*) (List_1_t3314011636 *, Predicate_1_t2387860619 *, const MethodInfo*))List_1_RemoveAll_m2880703788_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2078239366_gshared (List_1_t3314011636 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2078239366(__this, ___index0, method) ((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2078239366_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m4189366165_gshared (List_1_t3314011636 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m4189366165(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3314011636 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m4189366165_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Reverse()
extern "C"  void List_1_Reverse_m567826418_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_Reverse_m567826418(__this, method) ((  void (*) (List_1_t3314011636 *, const MethodInfo*))List_1_Reverse_m567826418_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Sort()
extern "C"  void List_1_Sort_m4031313742_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_Sort_m4031313742(__this, method) ((  void (*) (List_1_t3314011636 *, const MethodInfo*))List_1_Sort_m4031313742_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2772871730_gshared (List_1_t3314011636 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2772871730(__this, ___comparer0, method) ((  void (*) (List_1_t3314011636 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2772871730_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1955481871_gshared (List_1_t3314011636 * __this, Comparison_1_t911662059 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1955481871(__this, ___comparison0, method) ((  void (*) (List_1_t3314011636 *, Comparison_1_t911662059 *, const MethodInfo*))List_1_Sort_m1955481871_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<AttackHomeNav>::ToArray()
extern "C"  AttackHomeNavU5BU5D_t3086173273* List_1_ToArray_m2925158887_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_ToArray_m2925158887(__this, method) ((  AttackHomeNavU5BU5D_t3086173273* (*) (List_1_t3314011636 *, const MethodInfo*))List_1_ToArray_m2925158887_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3067450725_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3067450725(__this, method) ((  void (*) (List_1_t3314011636 *, const MethodInfo*))List_1_TrimExcess_m3067450725_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m189358863_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m189358863(__this, method) ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))List_1_get_Capacity_m189358863_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4284242110_gshared (List_1_t3314011636 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4284242110(__this, ___value0, method) ((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4284242110_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<AttackHomeNav>::get_Count()
extern "C"  int32_t List_1_get_Count_m729052860_gshared (List_1_t3314011636 * __this, const MethodInfo* method);
#define List_1_get_Count_m729052860(__this, method) ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))List_1_get_Count_m729052860_gshared)(__this, method)
// T System.Collections.Generic.List`1<AttackHomeNav>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2270644624_gshared (List_1_t3314011636 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2270644624(__this, ___index0, method) ((  int32_t (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))List_1_get_Item_m2270644624_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackHomeNav>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3267783013_gshared (List_1_t3314011636 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3267783013(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3314011636 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3267783013_gshared)(__this, ___index0, ___value1, method)
