﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsNavigationController/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1
struct U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsNavigationController/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__AnonStorey1__ctor_m1142093251 (U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1::<>m__0(System.Boolean)
extern "C"  void U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m3972615651 (U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008 * __this, bool ___isSaved0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
