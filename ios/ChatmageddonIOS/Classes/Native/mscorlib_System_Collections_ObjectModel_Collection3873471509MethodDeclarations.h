﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<AttackNavScreen>
struct Collection_1_t3873471509;
// System.Collections.Generic.IList`1<AttackNavScreen>
struct IList_1_t577700060;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AttackNavScreen[]
struct AttackNavScreenU5BU5D_t3684257394;
// System.Collections.Generic.IEnumerator`1<AttackNavScreen>
struct IEnumerator_1_t1807250582;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"

// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m877275210_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1__ctor_m877275210(__this, method) ((  void (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1__ctor_m877275210_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m898628503_gshared (Collection_1_t3873471509 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m898628503(__this, ___list0, method) ((  void (*) (Collection_1_t3873471509 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m898628503_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1964101009_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1964101009(__this, method) ((  bool (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1964101009_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1278355986_gshared (Collection_1_t3873471509 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1278355986(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3873471509 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1278355986_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3766199237_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3766199237(__this, method) ((  Il2CppObject * (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3766199237_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3635483394_gshared (Collection_1_t3873471509 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3635483394(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3873471509 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3635483394_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2801273452_gshared (Collection_1_t3873471509 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2801273452(__this, ___value0, method) ((  bool (*) (Collection_1_t3873471509 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2801273452_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m983734224_gshared (Collection_1_t3873471509 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m983734224(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3873471509 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m983734224_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2237649373_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2237649373(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2237649373_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1781328301_gshared (Collection_1_t3873471509 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1781328301(__this, ___value0, method) ((  void (*) (Collection_1_t3873471509 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1781328301_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1001027026_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1001027026(__this, method) ((  bool (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1001027026_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3403146370_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3403146370(__this, method) ((  Il2CppObject * (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3403146370_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2261193617_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2261193617(__this, method) ((  bool (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2261193617_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3077330342_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3077330342(__this, method) ((  bool (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3077330342_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2734789321_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2734789321(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2734789321_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1653465212_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1653465212(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1653465212_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m813032409_gshared (Collection_1_t3873471509 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m813032409(__this, ___item0, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_Add_m813032409_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m1149705301_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1149705301(__this, method) ((  void (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_Clear_m1149705301_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4244237587_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4244237587(__this, method) ((  void (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_ClearItems_m4244237587_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m3356080487_gshared (Collection_1_t3873471509 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3356080487(__this, ___item0, method) ((  bool (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_Contains_m3356080487_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1310517729_gshared (Collection_1_t3873471509 * __this, AttackNavScreenU5BU5D_t3684257394* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1310517729(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3873471509 *, AttackNavScreenU5BU5D_t3684257394*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1310517729_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<AttackNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3082831262_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3082831262(__this, method) ((  Il2CppObject* (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_GetEnumerator_m3082831262_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2096772085_gshared (Collection_1_t3873471509 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2096772085(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2096772085_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2526896692_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2526896692(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m2526896692_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1204175847_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1204175847(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1204175847_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m245149394_gshared (Collection_1_t3873471509 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m245149394(__this, ___item0, method) ((  bool (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_Remove_m245149394_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1818685488_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1818685488(__this, ___index0, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1818685488_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3018694226_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3018694226(__this, ___index0, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3018694226_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3781389906_gshared (Collection_1_t3873471509 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3781389906(__this, method) ((  int32_t (*) (Collection_1_t3873471509 *, const MethodInfo*))Collection_1_get_Count_m3781389906_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<AttackNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m1074803678_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1074803678(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3873471509 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1074803678_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m4086724329_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m4086724329(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m4086724329_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m802764796_gshared (Collection_1_t3873471509 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m802764796(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3873471509 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m802764796_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3845268107_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3845268107(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3845268107_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<AttackNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m1468885611_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1468885611(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1468885611_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3794595895_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3794595895(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3794595895_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3135805967_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3135805967(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3135805967_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1247493648_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1247493648(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1247493648_gshared)(__this /* static, unused */, ___list0, method)
