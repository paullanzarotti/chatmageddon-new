﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<FriendsSearchNavigationController>::.ctor()
#define MonoSingleton_1__ctor_m3966894401(__this, method) ((  void (*) (MonoSingleton_1_t764597815 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendsSearchNavigationController>::Awake()
#define MonoSingleton_1_Awake_m1557654862(__this, method) ((  void (*) (MonoSingleton_1_t764597815 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<FriendsSearchNavigationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m2254490390(__this /* static, unused */, method) ((  FriendsSearchNavigationController_t1013932095 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<FriendsSearchNavigationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2551028020(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<FriendsSearchNavigationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m675270199(__this, method) ((  void (*) (MonoSingleton_1_t764597815 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendsSearchNavigationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m10651867(__this, method) ((  void (*) (MonoSingleton_1_t764597815 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendsSearchNavigationController>::.cctor()
#define MonoSingleton_1__cctor_m176159280(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
