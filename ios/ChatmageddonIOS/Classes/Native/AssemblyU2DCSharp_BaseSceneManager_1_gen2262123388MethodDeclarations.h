﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<LeaderboardSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m2512464364(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m2187642909(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::Start()
#define BaseSceneManager_1_Start_m1061913604(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m3272151894(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m1168100590(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m2630324282(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<LeaderboardSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m1628838068(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m1511118091(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<LeaderboardSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m1270886616(__this, method) ((  void (*) (BaseSceneManager_1_t2262123388 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
