﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Hexagon
struct CameraFilterPack_FX_Hexagon_t382883320;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Hexagon::.ctor()
extern "C"  void CameraFilterPack_FX_Hexagon__ctor_m2473193203 (CameraFilterPack_FX_Hexagon_t382883320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Hexagon::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Hexagon_get_material_m2558316916 (CameraFilterPack_FX_Hexagon_t382883320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::Start()
extern "C"  void CameraFilterPack_FX_Hexagon_Start_m1046714623 (CameraFilterPack_FX_Hexagon_t382883320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Hexagon_OnRenderImage_m3531626031 (CameraFilterPack_FX_Hexagon_t382883320 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::Update()
extern "C"  void CameraFilterPack_FX_Hexagon_Update_m2772455574 (CameraFilterPack_FX_Hexagon_t382883320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::OnDisable()
extern "C"  void CameraFilterPack_FX_Hexagon_OnDisable_m4011728696 (CameraFilterPack_FX_Hexagon_t382883320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
