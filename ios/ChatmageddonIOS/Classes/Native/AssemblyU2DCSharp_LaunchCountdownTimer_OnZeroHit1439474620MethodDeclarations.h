﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchCountdownTimer/OnZeroHit
struct OnZeroHit_t1439474620;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void LaunchCountdownTimer/OnZeroHit::.ctor(System.Object,System.IntPtr)
extern "C"  void OnZeroHit__ctor_m1394836405 (OnZeroHit_t1439474620 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer/OnZeroHit::Invoke()
extern "C"  void OnZeroHit_Invoke_m2888582853 (OnZeroHit_t1439474620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult LaunchCountdownTimer/OnZeroHit::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnZeroHit_BeginInvoke_m2288262642 (OnZeroHit_t1439474620 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer/OnZeroHit::EndInvoke(System.IAsyncResult)
extern "C"  void OnZeroHit_EndInvoke_m2456741203 (OnZeroHit_t1439474620 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
