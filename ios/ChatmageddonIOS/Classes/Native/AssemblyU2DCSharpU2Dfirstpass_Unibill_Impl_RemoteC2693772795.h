﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Uniject.IStorage
struct IStorage_t1347868490;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.RemoteConfigManager
struct  RemoteConfigManager_t2693772795  : public Il2CppObject
{
public:
	// Uniject.IStorage Unibill.Impl.RemoteConfigManager::storage
	Il2CppObject * ___storage_1;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.RemoteConfigManager::<Config>k__BackingField
	UnibillConfiguration_t2915611853 * ___U3CConfigU3Ek__BackingField_2;
	// System.String Unibill.Impl.RemoteConfigManager::XML
	String_t* ___XML_3;

public:
	inline static int32_t get_offset_of_storage_1() { return static_cast<int32_t>(offsetof(RemoteConfigManager_t2693772795, ___storage_1)); }
	inline Il2CppObject * get_storage_1() const { return ___storage_1; }
	inline Il2CppObject ** get_address_of_storage_1() { return &___storage_1; }
	inline void set_storage_1(Il2CppObject * value)
	{
		___storage_1 = value;
		Il2CppCodeGenWriteBarrier(&___storage_1, value);
	}

	inline static int32_t get_offset_of_U3CConfigU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemoteConfigManager_t2693772795, ___U3CConfigU3Ek__BackingField_2)); }
	inline UnibillConfiguration_t2915611853 * get_U3CConfigU3Ek__BackingField_2() const { return ___U3CConfigU3Ek__BackingField_2; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_U3CConfigU3Ek__BackingField_2() { return &___U3CConfigU3Ek__BackingField_2; }
	inline void set_U3CConfigU3Ek__BackingField_2(UnibillConfiguration_t2915611853 * value)
	{
		___U3CConfigU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConfigU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_XML_3() { return static_cast<int32_t>(offsetof(RemoteConfigManager_t2693772795, ___XML_3)); }
	inline String_t* get_XML_3() const { return ___XML_3; }
	inline String_t** get_address_of_XML_3() { return &___XML_3; }
	inline void set_XML_3(String_t* value)
	{
		___XML_3 = value;
		Il2CppCodeGenWriteBarrier(&___XML_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
