﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraManager
struct EtceteraManager_t1020444134;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`1<System.Collections.IDictionary>
struct Action_1_t397957987;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EtceteraManager::.cctor()
extern "C"  void EtceteraManager__cctor_m2974790278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::.ctor()
extern "C"  void EtceteraManager__ctor_m2544675623 (EtceteraManager_t1020444134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_dismissingViewControllerEvent(System.Action)
extern "C"  void EtceteraManager_add_dismissingViewControllerEvent_m1895466851 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_dismissingViewControllerEvent(System.Action)
extern "C"  void EtceteraManager_remove_dismissingViewControllerEvent_m1639320614 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_imagePickerCancelledEvent(System.Action)
extern "C"  void EtceteraManager_add_imagePickerCancelledEvent_m714506116 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_imagePickerCancelledEvent(System.Action)
extern "C"  void EtceteraManager_remove_imagePickerCancelledEvent_m1069072417 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_imagePickerChoseImageEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_imagePickerChoseImageEvent_m4190564237 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_imagePickerChoseImageEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_imagePickerChoseImageEvent_m434502586 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_saveImageToPhotoAlbumSucceededEvent(System.Action)
extern "C"  void EtceteraManager_add_saveImageToPhotoAlbumSucceededEvent_m1157422625 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_saveImageToPhotoAlbumSucceededEvent(System.Action)
extern "C"  void EtceteraManager_remove_saveImageToPhotoAlbumSucceededEvent_m3589555086 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_saveImageToPhotoAlbumFailedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_saveImageToPhotoAlbumFailedEvent_m2951780030 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_saveImageToPhotoAlbumFailedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_saveImageToPhotoAlbumFailedEvent_m2495052491 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_alertButtonClickedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_alertButtonClickedEvent_m797830972 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_alertButtonClickedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_alertButtonClickedEvent_m4071241533 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_promptCancelledEvent(System.Action)
extern "C"  void EtceteraManager_add_promptCancelledEvent_m2536316643 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_promptCancelledEvent(System.Action)
extern "C"  void EtceteraManager_remove_promptCancelledEvent_m3408050156 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_singleFieldPromptTextEnteredEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_singleFieldPromptTextEnteredEvent_m774495537 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_singleFieldPromptTextEnteredEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_singleFieldPromptTextEnteredEvent_m3583305758 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_twoFieldPromptTextEnteredEvent(System.Action`2<System.String,System.String>)
extern "C"  void EtceteraManager_add_twoFieldPromptTextEnteredEvent_m129941374 (Il2CppObject * __this /* static, unused */, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_twoFieldPromptTextEnteredEvent(System.Action`2<System.String,System.String>)
extern "C"  void EtceteraManager_remove_twoFieldPromptTextEnteredEvent_m3229533913 (Il2CppObject * __this /* static, unused */, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_remoteRegistrationSucceededEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_remoteRegistrationSucceededEvent_m3322314583 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_remoteRegistrationSucceededEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_remoteRegistrationSucceededEvent_m3176849756 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_remoteRegistrationFailedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_remoteRegistrationFailedEvent_m1489462543 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_remoteRegistrationFailedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_remoteRegistrationFailedEvent_m3574810640 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_urbanAirshipRegistrationSucceededEvent(System.Action)
extern "C"  void EtceteraManager_add_urbanAirshipRegistrationSucceededEvent_m674317126 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_urbanAirshipRegistrationSucceededEvent(System.Action)
extern "C"  void EtceteraManager_remove_urbanAirshipRegistrationSucceededEvent_m1135983459 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_urbanAirshipRegistrationFailedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_urbanAirshipRegistrationFailedEvent_m219894693 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_urbanAirshipRegistrationFailedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_urbanAirshipRegistrationFailedEvent_m202838972 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_pushIORegistrationCompletedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_pushIORegistrationCompletedEvent_m1786863053 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_pushIORegistrationCompletedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_pushIORegistrationCompletedEvent_m3707236946 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_remoteNotificationReceivedEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_add_remoteNotificationReceivedEvent_m3615021460 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_remoteNotificationReceivedEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_remove_remoteNotificationReceivedEvent_m1155674923 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_remoteNotificationReceivedAtLaunchEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_add_remoteNotificationReceivedAtLaunchEvent_m2455424678 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_remoteNotificationReceivedAtLaunchEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_remove_remoteNotificationReceivedAtLaunchEvent_m2896113143 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_localNotificationWasReceivedEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_add_localNotificationWasReceivedEvent_m1220370680 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_localNotificationWasReceivedEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_remove_localNotificationWasReceivedEvent_m3465484719 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_localNotificationWasReceivedAtLaunchEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_add_localNotificationWasReceivedAtLaunchEvent_m1012180874 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_localNotificationWasReceivedAtLaunchEvent(System.Action`1<System.Collections.IDictionary>)
extern "C"  void EtceteraManager_remove_localNotificationWasReceivedAtLaunchEvent_m3051447291 (Il2CppObject * __this /* static, unused */, Action_1_t397957987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_mailComposerFinishedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_mailComposerFinishedEvent_m2562433504 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_mailComposerFinishedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_mailComposerFinishedEvent_m2222432955 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::add_smsComposerFinishedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_add_smsComposerFinishedEvent_m229206882 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remove_smsComposerFinishedEvent(System.Action`1<System.String>)
extern "C"  void EtceteraManager_remove_smsComposerFinishedEvent_m570570763 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraManager::get_deviceToken()
extern "C"  String_t* EtceteraManager_get_deviceToken_m804770290 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::set_deviceToken(System.String)
extern "C"  void EtceteraManager_set_deviceToken_m3538610175 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::dismissingViewController()
extern "C"  void EtceteraManager_dismissingViewController_m3334055578 (EtceteraManager_t1020444134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::imagePickerDidCancel(System.String)
extern "C"  void EtceteraManager_imagePickerDidCancel_m574462823 (EtceteraManager_t1020444134 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::imageSavedToDocuments(System.String)
extern "C"  void EtceteraManager_imageSavedToDocuments_m2755928348 (EtceteraManager_t1020444134 * __this, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::saveImageToPhotoAlbumFailed(System.String)
extern "C"  void EtceteraManager_saveImageToPhotoAlbumFailed_m389056408 (EtceteraManager_t1020444134 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::saveImageToPhotoAlbumSucceeded(System.String)
extern "C"  void EtceteraManager_saveImageToPhotoAlbumSucceeded_m2191176056 (EtceteraManager_t1020444134 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraManager::textureFromFileAtPath(System.String,System.Action`1<UnityEngine.Texture2D>,System.Action`1<System.String>)
extern "C"  Il2CppObject * EtceteraManager_textureFromFileAtPath_m4026404094 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, Action_1_t3344795111 * ___del1, Action_1_t1831019615 * ___errorDel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::alertViewClickedButton(System.String)
extern "C"  void EtceteraManager_alertViewClickedButton_m3082589039 (EtceteraManager_t1020444134 * __this, String_t* ___buttonTitle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::alertPromptCancelled(System.String)
extern "C"  void EtceteraManager_alertPromptCancelled_m649187950 (EtceteraManager_t1020444134 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::alertPromptEnteredText(System.String)
extern "C"  void EtceteraManager_alertPromptEnteredText_m1001122139 (EtceteraManager_t1020444134 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remoteRegistrationDidSucceed(System.String)
extern "C"  void EtceteraManager_remoteRegistrationDidSucceed_m4215582203 (EtceteraManager_t1020444134 * __this, String_t* ___deviceToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraManager::registerDeviceWithPushIO()
extern "C"  Il2CppObject * EtceteraManager_registerDeviceWithPushIO_m2662766072 (EtceteraManager_t1020444134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remoteRegistrationDidFail(System.String)
extern "C"  void EtceteraManager_remoteRegistrationDidFail_m632393759 (EtceteraManager_t1020444134 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::urbanAirshipRegistrationDidSucceed(System.String)
extern "C"  void EtceteraManager_urbanAirshipRegistrationDidSucceed_m2321414953 (EtceteraManager_t1020444134 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::urbanAirshipRegistrationDidFail(System.String)
extern "C"  void EtceteraManager_urbanAirshipRegistrationDidFail_m1121923045 (EtceteraManager_t1020444134 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remoteNotificationWasReceived(System.String)
extern "C"  void EtceteraManager_remoteNotificationWasReceived_m3639647028 (EtceteraManager_t1020444134 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::remoteNotificationWasReceivedAtLaunch(System.String)
extern "C"  void EtceteraManager_remoteNotificationWasReceivedAtLaunch_m1805275246 (EtceteraManager_t1020444134 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::localNotificationWasReceived(System.String)
extern "C"  void EtceteraManager_localNotificationWasReceived_m4266089739 (EtceteraManager_t1020444134 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::localNotificationWasReceivedAtLaunch(System.String)
extern "C"  void EtceteraManager_localNotificationWasReceivedAtLaunch_m928436791 (EtceteraManager_t1020444134 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::mailComposerFinishedWithResult(System.String)
extern "C"  void EtceteraManager_mailComposerFinishedWithResult_m3027415171 (EtceteraManager_t1020444134 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager::smsComposerFinishedWithResult(System.String)
extern "C"  void EtceteraManager_smsComposerFinishedWithResult_m176216175 (EtceteraManager_t1020444134 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
