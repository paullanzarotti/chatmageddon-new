﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GpsService
struct GpsService_t3478080739;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GpsService/<TrackDistance>c__Iterator0
struct  U3CTrackDistanceU3Ec__Iterator0_t1867411235  : public Il2CppObject
{
public:
	// System.Single GpsService/<TrackDistance>c__Iterator0::<dLat>__0
	float ___U3CdLatU3E__0_0;
	// System.Single GpsService/<TrackDistance>c__Iterator0::<dLon>__1
	float ___U3CdLonU3E__1_1;
	// System.Single GpsService/<TrackDistance>c__Iterator0::<a>__2
	float ___U3CaU3E__2_2;
	// System.Single GpsService/<TrackDistance>c__Iterator0::<c>__3
	float ___U3CcU3E__3_3;
	// GpsService GpsService/<TrackDistance>c__Iterator0::$this
	GpsService_t3478080739 * ___U24this_4;
	// System.Object GpsService/<TrackDistance>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean GpsService/<TrackDistance>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 GpsService/<TrackDistance>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CdLatU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U3CdLatU3E__0_0)); }
	inline float get_U3CdLatU3E__0_0() const { return ___U3CdLatU3E__0_0; }
	inline float* get_address_of_U3CdLatU3E__0_0() { return &___U3CdLatU3E__0_0; }
	inline void set_U3CdLatU3E__0_0(float value)
	{
		___U3CdLatU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdLonU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U3CdLonU3E__1_1)); }
	inline float get_U3CdLonU3E__1_1() const { return ___U3CdLonU3E__1_1; }
	inline float* get_address_of_U3CdLonU3E__1_1() { return &___U3CdLonU3E__1_1; }
	inline void set_U3CdLonU3E__1_1(float value)
	{
		___U3CdLonU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CaU3E__2_2() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U3CaU3E__2_2)); }
	inline float get_U3CaU3E__2_2() const { return ___U3CaU3E__2_2; }
	inline float* get_address_of_U3CaU3E__2_2() { return &___U3CaU3E__2_2; }
	inline void set_U3CaU3E__2_2(float value)
	{
		___U3CaU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_3() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U3CcU3E__3_3)); }
	inline float get_U3CcU3E__3_3() const { return ___U3CcU3E__3_3; }
	inline float* get_address_of_U3CcU3E__3_3() { return &___U3CcU3E__3_3; }
	inline void set_U3CcU3E__3_3(float value)
	{
		___U3CcU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U24this_4)); }
	inline GpsService_t3478080739 * get_U24this_4() const { return ___U24this_4; }
	inline GpsService_t3478080739 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GpsService_t3478080739 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CTrackDistanceU3Ec__Iterator0_t1867411235, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
