﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoCtrl
struct DemoCtrl_t4011351802;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void DemoCtrl::.ctor()
extern "C"  void DemoCtrl__ctor_m1288828333 (DemoCtrl_t4011351802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable DemoCtrl::ImportJsonString(System.String)
extern "C"  Hashtable_t909839986 * DemoCtrl_ImportJsonString_m3252277462 (DemoCtrl_t4011351802 * __this, String_t* ___aString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DemoCtrl::ExportXml(System.Collections.Hashtable)
extern "C"  String_t* DemoCtrl_ExportXml_m3279807233 (DemoCtrl_t4011351802 * __this, Hashtable_t909839986 * ___aHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DemoCtrl::ExportJson(System.Collections.Hashtable)
extern "C"  String_t* DemoCtrl_ExportJson_m236362052 (DemoCtrl_t4011351802 * __this, Hashtable_t909839986 * ___aHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::FindNodesWithTagPath(System.String[])
extern "C"  void DemoCtrl_FindNodesWithTagPath_m815587968 (DemoCtrl_t4011351802 * __this, StringU5BU5D_t1642385972* ___aPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::FindNodesWithProperty(System.String,System.String)
extern "C"  void DemoCtrl_FindNodesWithProperty_m1973400310 (DemoCtrl_t4011351802 * __this, String_t* ___aKey0, String_t* ___aValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::Start()
extern "C"  void DemoCtrl_Start_m2737890085 (DemoCtrl_t4011351802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::Update()
extern "C"  void DemoCtrl_Update_m403833308 (DemoCtrl_t4011351802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::OnGUI()
extern "C"  void DemoCtrl_OnGUI_m4227745891 (DemoCtrl_t4011351802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::Reset()
extern "C"  void DemoCtrl_Reset_m493412430 (DemoCtrl_t4011351802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DemoCtrl::Screenshot()
extern "C"  Il2CppObject * DemoCtrl_Screenshot_m3246744487 (DemoCtrl_t4011351802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DemoCtrl::DownloadJsonFile(System.String)
extern "C"  Il2CppObject * DemoCtrl_DownloadJsonFile_m2593293899 (DemoCtrl_t4011351802 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DemoCtrl::DownloadFile(System.String,System.Action`1<System.String>)
extern "C"  Il2CppObject * DemoCtrl_DownloadFile_m2153865651 (DemoCtrl_t4011351802 * __this, String_t* ___url0, Action_1_t1831019615 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::ExportToFile(System.String,System.String,System.Boolean)
extern "C"  void DemoCtrl_ExportToFile_m3660798827 (DemoCtrl_t4011351802 * __this, String_t* ___exportString0, String_t* ___path1, bool ___addXmlProlog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl::AddToLog(System.String)
extern "C"  void DemoCtrl_AddToLog_m2976845677 (DemoCtrl_t4011351802 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DemoCtrl::PathToString(System.String[])
extern "C"  String_t* DemoCtrl_PathToString_m1079078607 (DemoCtrl_t4011351802 * __this, StringU5BU5D_t1642385972* ___aPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DemoCtrl::CreateJsonLogString(System.Collections.Hashtable)
extern "C"  String_t* DemoCtrl_CreateJsonLogString_m2172642357 (DemoCtrl_t4011351802 * __this, Hashtable_t909839986 * ___aNode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DemoCtrl::CreateJsonLogString(System.Collections.ArrayList)
extern "C"  String_t* DemoCtrl_CreateJsonLogString_m1149102644 (DemoCtrl_t4011351802 * __this, ArrayList_t4252133567 * ___aNodeArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DemoCtrl::TruncateStringForEditor(System.String)
extern "C"  String_t* DemoCtrl_TruncateStringForEditor_m2782951859 (DemoCtrl_t4011351802 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
