﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeUIScaler
struct StealthModeUIScaler_t786227838;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeUIScaler::.ctor()
extern "C"  void StealthModeUIScaler__ctor_m4269426989 (StealthModeUIScaler_t786227838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUIScaler::GlobalUIScale()
extern "C"  void StealthModeUIScaler_GlobalUIScale_m2816406992 (StealthModeUIScaler_t786227838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
