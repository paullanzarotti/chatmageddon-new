﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int16>
struct List_1_t3410367046;
// System.Collections.Generic.IEnumerable`1<System.Int16>
struct IEnumerable_1_t38405663;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_t1516769741;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Int16>
struct ICollection_1_t698353923;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>
struct ReadOnlyCollection_1_t4227031606;
// System.Predicate`1<System.Int16>
struct Predicate_1_t2484216029;
// System.Action`1<System.Int16>
struct Action_1_t3843045296;
// System.Collections.Generic.IComparer`1<System.Int16>
struct IComparer_1_t1995709036;
// System.Comparison`1<System.Int16>
struct Comparison_1_t1008017469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2945096720.h"

// System.Void System.Collections.Generic.List`1<System.Int16>::.ctor()
extern "C"  void List_1__ctor_m4292035141_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1__ctor_m4292035141(__this, method) ((  void (*) (List_1_t3410367046 *, const MethodInfo*))List_1__ctor_m4292035141_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3023772036_gshared (List_1_t3410367046 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3023772036(__this, ___collection0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3023772036_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1499764302_gshared (List_1_t3410367046 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1499764302(__this, ___capacity0, method) ((  void (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1__ctor_m1499764302_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m3199522310_gshared (List_1_t3410367046 * __this, Int16U5BU5D_t3104283263* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m3199522310(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3410367046 *, Int16U5BU5D_t3104283263*, int32_t, const MethodInfo*))List_1__ctor_m3199522310_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::.cctor()
extern "C"  void List_1__cctor_m1811157986_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1811157986(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1811157986_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int16>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2246728181_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2246728181(__this, method) ((  Il2CppObject* (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2246728181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m834453045_gshared (List_1_t3410367046 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m834453045(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3410367046 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m834453045_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int16>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3540762516_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3540762516(__this, method) ((  Il2CppObject * (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3540762516_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2489194021_gshared (List_1_t3410367046 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2489194021(__this, ___item0, method) ((  int32_t (*) (List_1_t3410367046 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2489194021_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3033228645_gshared (List_1_t3410367046 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3033228645(__this, ___item0, method) ((  bool (*) (List_1_t3410367046 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3033228645_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m905629851_gshared (List_1_t3410367046 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m905629851(__this, ___item0, method) ((  int32_t (*) (List_1_t3410367046 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m905629851_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2853652294_gshared (List_1_t3410367046 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2853652294(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3410367046 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2853652294_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m743772532_gshared (List_1_t3410367046 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m743772532(__this, ___item0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m743772532_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15037276_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15037276(__this, method) ((  bool (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15037276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m4267188641_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4267188641(__this, method) ((  bool (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m4267188641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int16>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2791547737_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2791547737(__this, method) ((  Il2CppObject * (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2791547737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3298158574_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3298158574(__this, method) ((  bool (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3298158574_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2210433645_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2210433645(__this, method) ((  bool (*) (List_1_t3410367046 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2210433645_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1244714940_gshared (List_1_t3410367046 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1244714940(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1244714940_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m30469395_gshared (List_1_t3410367046 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m30469395(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3410367046 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m30469395_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Add(T)
extern "C"  void List_1_Add_m2709685082_gshared (List_1_t3410367046 * __this, int16_t ___item0, const MethodInfo* method);
#define List_1_Add_m2709685082(__this, ___item0, method) ((  void (*) (List_1_t3410367046 *, int16_t, const MethodInfo*))List_1_Add_m2709685082_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m667378573_gshared (List_1_t3410367046 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m667378573(__this, ___newCount0, method) ((  void (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m667378573_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3364156462_gshared (List_1_t3410367046 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3364156462(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3410367046 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3364156462_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3680756029_gshared (List_1_t3410367046 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3680756029(__this, ___collection0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3680756029_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2522558813_gshared (List_1_t3410367046 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2522558813(__this, ___enumerable0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2522558813_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3361023532_gshared (List_1_t3410367046 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3361023532(__this, ___collection0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3361023532_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int16>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4227031606 * List_1_AsReadOnly_m491759909_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m491759909(__this, method) ((  ReadOnlyCollection_1_t4227031606 * (*) (List_1_t3410367046 *, const MethodInfo*))List_1_AsReadOnly_m491759909_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Clear()
extern "C"  void List_1_Clear_m803982230_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_Clear_m803982230(__this, method) ((  void (*) (List_1_t3410367046 *, const MethodInfo*))List_1_Clear_m803982230_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::Contains(T)
extern "C"  bool List_1_Contains_m3702689848_gshared (List_1_t3410367046 * __this, int16_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3702689848(__this, ___item0, method) ((  bool (*) (List_1_t3410367046 *, int16_t, const MethodInfo*))List_1_Contains_m3702689848_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2731170485_gshared (List_1_t3410367046 * __this, Int16U5BU5D_t3104283263* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m2731170485(__this, ___array0, method) ((  void (*) (List_1_t3410367046 *, Int16U5BU5D_t3104283263*, const MethodInfo*))List_1_CopyTo_m2731170485_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1048822330_gshared (List_1_t3410367046 * __this, Int16U5BU5D_t3104283263* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1048822330(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3410367046 *, Int16U5BU5D_t3104283263*, int32_t, const MethodInfo*))List_1_CopyTo_m1048822330_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m108410256_gshared (List_1_t3410367046 * __this, int32_t ___index0, Int16U5BU5D_t3104283263* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m108410256(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3410367046 *, int32_t, Int16U5BU5D_t3104283263*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m108410256_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m1743065028_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_Exists_m1743065028(__this, ___match0, method) ((  bool (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_Exists_m1743065028_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<System.Int16>::Find(System.Predicate`1<T>)
extern "C"  int16_t List_1_Find_m4248344228_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_Find_m4248344228(__this, ___match0, method) ((  int16_t (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_Find_m4248344228_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2355732237_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2355732237(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2484216029 *, const MethodInfo*))List_1_CheckMatch_m2355732237_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int16>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3410367046 * List_1_FindAll_m4293920401_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m4293920401(__this, ___match0, method) ((  List_1_t3410367046 * (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_FindAll_m4293920401_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int16>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3410367046 * List_1_FindAllStackBits_m1394434451_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1394434451(__this, ___match0, method) ((  List_1_t3410367046 * (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_FindAllStackBits_m1394434451_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int16>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3410367046 * List_1_FindAllList_m3416247859_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m3416247859(__this, ___match0, method) ((  List_1_t3410367046 * (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_FindAllList_m3416247859_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m4265475237_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m4265475237(__this, ___match0, method) ((  int32_t (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_FindIndex_m4265475237_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1783184518_gshared (List_1_t3410367046 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2484216029 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1783184518(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3410367046 *, int32_t, int32_t, Predicate_1_t2484216029 *, const MethodInfo*))List_1_GetIndex_m1783184518_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2376692799_gshared (List_1_t3410367046 * __this, Action_1_t3843045296 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2376692799(__this, ___action0, method) ((  void (*) (List_1_t3410367046 *, Action_1_t3843045296 *, const MethodInfo*))List_1_ForEach_m2376692799_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int16>::GetEnumerator()
extern "C"  Enumerator_t2945096720  List_1_GetEnumerator_m2092564065_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2092564065(__this, method) ((  Enumerator_t2945096720  (*) (List_1_t3410367046 *, const MethodInfo*))List_1_GetEnumerator_m2092564065_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2155320956_gshared (List_1_t3410367046 * __this, int16_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2155320956(__this, ___item0, method) ((  int32_t (*) (List_1_t3410367046 *, int16_t, const MethodInfo*))List_1_IndexOf_m2155320956_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2046955921_gshared (List_1_t3410367046 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2046955921(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3410367046 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2046955921_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1981970686_gshared (List_1_t3410367046 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1981970686(__this, ___index0, method) ((  void (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1981970686_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m881891571_gshared (List_1_t3410367046 * __this, int32_t ___index0, int16_t ___item1, const MethodInfo* method);
#define List_1_Insert_m881891571(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3410367046 *, int32_t, int16_t, const MethodInfo*))List_1_Insert_m881891571_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3589742656_gshared (List_1_t3410367046 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3589742656(__this, ___collection0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3589742656_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int16>::Remove(T)
extern "C"  bool List_1_Remove_m2020107639_gshared (List_1_t3410367046 * __this, int16_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2020107639(__this, ___item0, method) ((  bool (*) (List_1_t3410367046 *, int16_t, const MethodInfo*))List_1_Remove_m2020107639_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m583327525_gshared (List_1_t3410367046 * __this, Predicate_1_t2484216029 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m583327525(__this, ___match0, method) ((  int32_t (*) (List_1_t3410367046 *, Predicate_1_t2484216029 *, const MethodInfo*))List_1_RemoveAll_m583327525_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4090589151_gshared (List_1_t3410367046 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4090589151(__this, ___index0, method) ((  void (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4090589151_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3423372466_gshared (List_1_t3410367046 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3423372466(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3410367046 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3423372466_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Reverse()
extern "C"  void List_1_Reverse_m648698033_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_Reverse_m648698033(__this, method) ((  void (*) (List_1_t3410367046 *, const MethodInfo*))List_1_Reverse_m648698033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Sort()
extern "C"  void List_1_Sort_m3815210615_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_Sort_m3815210615(__this, method) ((  void (*) (List_1_t3410367046 *, const MethodInfo*))List_1_Sort_m3815210615_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1368579619_gshared (List_1_t3410367046 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1368579619(__this, ___comparer0, method) ((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1368579619_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3661920938_gshared (List_1_t3410367046 * __this, Comparison_1_t1008017469 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3661920938(__this, ___comparison0, method) ((  void (*) (List_1_t3410367046 *, Comparison_1_t1008017469 *, const MethodInfo*))List_1_Sort_m3661920938_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Int16>::ToArray()
extern "C"  Int16U5BU5D_t3104283263* List_1_ToArray_m2234618448_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_ToArray_m2234618448(__this, method) ((  Int16U5BU5D_t3104283263* (*) (List_1_t3410367046 *, const MethodInfo*))List_1_ToArray_m2234618448_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3701581232_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3701581232(__this, method) ((  void (*) (List_1_t3410367046 *, const MethodInfo*))List_1_TrimExcess_m3701581232_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3724521294_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3724521294(__this, method) ((  int32_t (*) (List_1_t3410367046 *, const MethodInfo*))List_1_get_Capacity_m3724521294_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m733218077_gshared (List_1_t3410367046 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m733218077(__this, ___value0, method) ((  void (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1_set_Capacity_m733218077_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int16>::get_Count()
extern "C"  int32_t List_1_get_Count_m2190181829_gshared (List_1_t3410367046 * __this, const MethodInfo* method);
#define List_1_get_Count_m2190181829(__this, method) ((  int32_t (*) (List_1_t3410367046 *, const MethodInfo*))List_1_get_Count_m2190181829_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int16>::get_Item(System.Int32)
extern "C"  int16_t List_1_get_Item_m2092121059_gshared (List_1_t3410367046 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2092121059(__this, ___index0, method) ((  int16_t (*) (List_1_t3410367046 *, int32_t, const MethodInfo*))List_1_get_Item_m2092121059_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int16>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3059645872_gshared (List_1_t3410367046 * __this, int32_t ___index0, int16_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3059645872(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3410367046 *, int32_t, int16_t, const MethodInfo*))List_1_set_Item_m3059645872_gshared)(__this, ___index0, ___value1, method)
