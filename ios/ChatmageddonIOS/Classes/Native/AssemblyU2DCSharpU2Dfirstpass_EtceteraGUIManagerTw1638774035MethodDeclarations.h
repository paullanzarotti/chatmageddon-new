﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraGUIManagerTwo
struct EtceteraGUIManagerTwo_t1638774035;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.WWW
struct WWW_t2919945039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"

// System.Void EtceteraGUIManagerTwo::.ctor()
extern "C"  void EtceteraGUIManagerTwo__ctor_m896789918 (EtceteraGUIManagerTwo_t1638774035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::Start()
extern "C"  void EtceteraGUIManagerTwo_Start_m3364149062 (EtceteraGUIManagerTwo_t1638774035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::OnDisable()
extern "C"  void EtceteraGUIManagerTwo_OnDisable_m3690475005 (EtceteraGUIManagerTwo_t1638774035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::OnGUI()
extern "C"  void EtceteraGUIManagerTwo_OnGUI_m2114357214 (EtceteraGUIManagerTwo_t1638774035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::imagePickerChoseImage(System.String)
extern "C"  void EtceteraGUIManagerTwo_imagePickerChoseImage_m2913807638 (EtceteraGUIManagerTwo_t1638774035 * __this, String_t* ___imagePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraGUIManagerTwo::hideActivityView()
extern "C"  Il2CppObject * EtceteraGUIManagerTwo_hideActivityView_m1458939500 (EtceteraGUIManagerTwo_t1638774035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::textureLoaded(UnityEngine.Texture2D)
extern "C"  void EtceteraGUIManagerTwo_textureLoaded_m2678335738 (EtceteraGUIManagerTwo_t1638774035 * __this, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::textureLoadFailed(System.String)
extern "C"  void EtceteraGUIManagerTwo_textureLoadFailed_m3632663522 (EtceteraGUIManagerTwo_t1638774035 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo::<OnGUI>m__0(UnityEngine.WWW)
extern "C"  void EtceteraGUIManagerTwo_U3COnGUIU3Em__0_m261726005 (Il2CppObject * __this /* static, unused */, WWW_t2919945039 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
