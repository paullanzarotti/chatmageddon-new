﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_Pr143967646.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Colors_Adjust_PreFilters
struct  CameraFilterPack_Colors_Adjust_PreFilters_t3558640258  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Colors_Adjust_PreFilters::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Colors_Adjust_PreFilters::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// CameraFilterPack_Colors_Adjust_PreFilters/filters CameraFilterPack_Colors_Adjust_PreFilters::filterchoice
	int32_t ___filterchoice_4;
	// System.Single CameraFilterPack_Colors_Adjust_PreFilters::FadeFX
	float ___FadeFX_5;
	// System.Single CameraFilterPack_Colors_Adjust_PreFilters::TimeX
	float ___TimeX_6;
	// UnityEngine.Vector4 CameraFilterPack_Colors_Adjust_PreFilters::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_7;
	// UnityEngine.Material CameraFilterPack_Colors_Adjust_PreFilters::SCMaterial
	Material_t193706927 * ___SCMaterial_8;
	// System.Single[] CameraFilterPack_Colors_Adjust_PreFilters::Matrix9
	SingleU5BU5D_t577127397* ___Matrix9_9;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_filterchoice_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___filterchoice_4)); }
	inline int32_t get_filterchoice_4() const { return ___filterchoice_4; }
	inline int32_t* get_address_of_filterchoice_4() { return &___filterchoice_4; }
	inline void set_filterchoice_4(int32_t value)
	{
		___filterchoice_4 = value;
	}

	inline static int32_t get_offset_of_FadeFX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___FadeFX_5)); }
	inline float get_FadeFX_5() const { return ___FadeFX_5; }
	inline float* get_address_of_FadeFX_5() { return &___FadeFX_5; }
	inline void set_FadeFX_5(float value)
	{
		___FadeFX_5 = value;
	}

	inline static int32_t get_offset_of_TimeX_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___TimeX_6)); }
	inline float get_TimeX_6() const { return ___TimeX_6; }
	inline float* get_address_of_TimeX_6() { return &___TimeX_6; }
	inline void set_TimeX_6(float value)
	{
		___TimeX_6 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___ScreenResolution_7)); }
	inline Vector4_t2243707581  get_ScreenResolution_7() const { return ___ScreenResolution_7; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_7() { return &___ScreenResolution_7; }
	inline void set_ScreenResolution_7(Vector4_t2243707581  value)
	{
		___ScreenResolution_7 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___SCMaterial_8)); }
	inline Material_t193706927 * get_SCMaterial_8() const { return ___SCMaterial_8; }
	inline Material_t193706927 ** get_address_of_SCMaterial_8() { return &___SCMaterial_8; }
	inline void set_SCMaterial_8(Material_t193706927 * value)
	{
		___SCMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_8, value);
	}

	inline static int32_t get_offset_of_Matrix9_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Adjust_PreFilters_t3558640258, ___Matrix9_9)); }
	inline SingleU5BU5D_t577127397* get_Matrix9_9() const { return ___Matrix9_9; }
	inline SingleU5BU5D_t577127397** get_address_of_Matrix9_9() { return &___Matrix9_9; }
	inline void set_Matrix9_9(SingleU5BU5D_t577127397* value)
	{
		___Matrix9_9 = value;
		Il2CppCodeGenWriteBarrier(&___Matrix9_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
