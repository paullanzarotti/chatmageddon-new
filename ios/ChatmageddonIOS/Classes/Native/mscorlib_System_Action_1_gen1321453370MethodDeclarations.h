﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<Unibill.ProductDefinition>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1839769119(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1321453370 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2192865289_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<Unibill.ProductDefinition>::Invoke(T)
#define Action_1_Invoke_m1843435299(__this, ___obj0, method) ((  void (*) (Action_1_t1321453370 *, ProductDefinition_t1519653988 *, const MethodInfo*))Action_1_Invoke_m3960033342_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<Unibill.ProductDefinition>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m602659156(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1321453370 *, ProductDefinition_t1519653988 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<Unibill.ProductDefinition>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m2810124137(__this, ___result0, method) ((  void (*) (Action_1_t1321453370 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
