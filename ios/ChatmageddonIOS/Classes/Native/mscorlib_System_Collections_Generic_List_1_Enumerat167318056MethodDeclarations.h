﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PhoneNumberNavScreen>
struct List_1_t632588382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat167318056.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1169363221_gshared (Enumerator_t167318056 * __this, List_1_t632588382 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1169363221(__this, ___l0, method) ((  void (*) (Enumerator_t167318056 *, List_1_t632588382 *, const MethodInfo*))Enumerator__ctor_m1169363221_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m175879685_gshared (Enumerator_t167318056 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m175879685(__this, method) ((  void (*) (Enumerator_t167318056 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m175879685_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1431847801_gshared (Enumerator_t167318056 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1431847801(__this, method) ((  Il2CppObject * (*) (Enumerator_t167318056 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1431847801_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m2349068830_gshared (Enumerator_t167318056 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2349068830(__this, method) ((  void (*) (Enumerator_t167318056 *, const MethodInfo*))Enumerator_Dispose_m2349068830_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1505910543_gshared (Enumerator_t167318056 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1505910543(__this, method) ((  void (*) (Enumerator_t167318056 *, const MethodInfo*))Enumerator_VerifyState_m1505910543_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3695238349_gshared (Enumerator_t167318056 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3695238349(__this, method) ((  bool (*) (Enumerator_t167318056 *, const MethodInfo*))Enumerator_MoveNext_m3695238349_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PhoneNumberNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2427135550_gshared (Enumerator_t167318056 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2427135550(__this, method) ((  int32_t (*) (Enumerator_t167318056 *, const MethodInfo*))Enumerator_get_Current_m2427135550_gshared)(__this, method)
