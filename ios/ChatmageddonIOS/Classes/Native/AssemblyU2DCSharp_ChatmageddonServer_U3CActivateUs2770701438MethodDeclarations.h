﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<ActivateUserKnockout>c__AnonStorey3C
struct U3CActivateUserKnockoutU3Ec__AnonStorey3C_t2770701438;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<ActivateUserKnockout>c__AnonStorey3C::.ctor()
extern "C"  void U3CActivateUserKnockoutU3Ec__AnonStorey3C__ctor_m958050369 (U3CActivateUserKnockoutU3Ec__AnonStorey3C_t2770701438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<ActivateUserKnockout>c__AnonStorey3C::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CActivateUserKnockoutU3Ec__AnonStorey3C_U3CU3Em__0_m992510630 (U3CActivateUserKnockoutU3Ec__AnonStorey3C_t2770701438 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
