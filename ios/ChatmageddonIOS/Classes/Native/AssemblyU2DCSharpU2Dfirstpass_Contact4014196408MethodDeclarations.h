﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Contact
struct Contact_t4014196408;

#include "codegen/il2cpp-codegen.h"

// System.Void Contact::.ctor()
extern "C"  void Contact__ctor_m3413040793 (Contact_t4014196408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
