﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryManager/<CheckStandardMissileInventory>c__AnonStorey0
struct  U3CCheckStandardMissileInventoryU3Ec__AnonStorey0_t1452096246  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> InventoryManager/<CheckStandardMissileInventory>c__AnonStorey0::callBack
	Action_1_t3627374100 * ___callBack_0;

public:
	inline static int32_t get_offset_of_callBack_0() { return static_cast<int32_t>(offsetof(U3CCheckStandardMissileInventoryU3Ec__AnonStorey0_t1452096246, ___callBack_0)); }
	inline Action_1_t3627374100 * get_callBack_0() const { return ___callBack_0; }
	inline Action_1_t3627374100 ** get_address_of_callBack_0() { return &___callBack_0; }
	inline void set_callBack_0(Action_1_t3627374100 * value)
	{
		___callBack_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
