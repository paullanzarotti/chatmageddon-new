﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Edge_Edge_filter
struct  CameraFilterPack_Edge_Edge_filter_t2364446753  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Edge_Edge_filter::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Edge_Edge_filter::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Edge_Edge_filter::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Edge_Edge_filter::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Edge_Edge_filter::RedAmplifier
	float ___RedAmplifier_6;
	// System.Single CameraFilterPack_Edge_Edge_filter::GreenAmplifier
	float ___GreenAmplifier_7;
	// System.Single CameraFilterPack_Edge_Edge_filter::BlueAmplifier
	float ___BlueAmplifier_8;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_RedAmplifier_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___RedAmplifier_6)); }
	inline float get_RedAmplifier_6() const { return ___RedAmplifier_6; }
	inline float* get_address_of_RedAmplifier_6() { return &___RedAmplifier_6; }
	inline void set_RedAmplifier_6(float value)
	{
		___RedAmplifier_6 = value;
	}

	inline static int32_t get_offset_of_GreenAmplifier_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___GreenAmplifier_7)); }
	inline float get_GreenAmplifier_7() const { return ___GreenAmplifier_7; }
	inline float* get_address_of_GreenAmplifier_7() { return &___GreenAmplifier_7; }
	inline void set_GreenAmplifier_7(float value)
	{
		___GreenAmplifier_7 = value;
	}

	inline static int32_t get_offset_of_BlueAmplifier_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753, ___BlueAmplifier_8)); }
	inline float get_BlueAmplifier_8() const { return ___BlueAmplifier_8; }
	inline float* get_address_of_BlueAmplifier_8() { return &___BlueAmplifier_8; }
	inline void set_BlueAmplifier_8(float value)
	{
		___BlueAmplifier_8 = value;
	}
};

struct CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields
{
public:
	// System.Single CameraFilterPack_Edge_Edge_filter::ChangeRedAmplifier
	float ___ChangeRedAmplifier_9;
	// System.Single CameraFilterPack_Edge_Edge_filter::ChangeGreenAmplifier
	float ___ChangeGreenAmplifier_10;
	// System.Single CameraFilterPack_Edge_Edge_filter::ChangeBlueAmplifier
	float ___ChangeBlueAmplifier_11;

public:
	inline static int32_t get_offset_of_ChangeRedAmplifier_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields, ___ChangeRedAmplifier_9)); }
	inline float get_ChangeRedAmplifier_9() const { return ___ChangeRedAmplifier_9; }
	inline float* get_address_of_ChangeRedAmplifier_9() { return &___ChangeRedAmplifier_9; }
	inline void set_ChangeRedAmplifier_9(float value)
	{
		___ChangeRedAmplifier_9 = value;
	}

	inline static int32_t get_offset_of_ChangeGreenAmplifier_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields, ___ChangeGreenAmplifier_10)); }
	inline float get_ChangeGreenAmplifier_10() const { return ___ChangeGreenAmplifier_10; }
	inline float* get_address_of_ChangeGreenAmplifier_10() { return &___ChangeGreenAmplifier_10; }
	inline void set_ChangeGreenAmplifier_10(float value)
	{
		___ChangeGreenAmplifier_10 = value;
	}

	inline static int32_t get_offset_of_ChangeBlueAmplifier_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields, ___ChangeBlueAmplifier_11)); }
	inline float get_ChangeBlueAmplifier_11() const { return ___ChangeBlueAmplifier_11; }
	inline float* get_address_of_ChangeBlueAmplifier_11() { return &___ChangeBlueAmplifier_11; }
	inline void set_ChangeBlueAmplifier_11(float value)
	{
		___ChangeBlueAmplifier_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
