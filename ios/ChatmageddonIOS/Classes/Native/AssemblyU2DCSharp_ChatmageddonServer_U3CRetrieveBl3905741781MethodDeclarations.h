﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<RetrieveBlockedUsers>c__AnonStorey9
struct U3CRetrieveBlockedUsersU3Ec__AnonStorey9_t3905741781;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<RetrieveBlockedUsers>c__AnonStorey9::.ctor()
extern "C"  void U3CRetrieveBlockedUsersU3Ec__AnonStorey9__ctor_m1213200084 (U3CRetrieveBlockedUsersU3Ec__AnonStorey9_t3905741781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<RetrieveBlockedUsers>c__AnonStorey9::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveBlockedUsersU3Ec__AnonStorey9_U3CU3Em__0_m1175570989 (U3CRetrieveBlockedUsersU3Ec__AnonStorey9_t3905741781 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
