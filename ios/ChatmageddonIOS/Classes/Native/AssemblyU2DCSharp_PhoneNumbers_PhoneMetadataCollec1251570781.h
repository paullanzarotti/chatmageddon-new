﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneMetadataCollection
struct PhoneMetadataCollection_t4114095021;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneMetadataCollection/Builder
struct  Builder_t1251570781  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection/Builder::result
	PhoneMetadataCollection_t4114095021 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(Builder_t1251570781, ___result_0)); }
	inline PhoneMetadataCollection_t4114095021 * get_result_0() const { return ___result_0; }
	inline PhoneMetadataCollection_t4114095021 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(PhoneMetadataCollection_t4114095021 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
