﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditEmailNavScreen
struct EditEmailNavScreen_t3910863955;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void EditEmailNavScreen::.ctor()
extern "C"  void EditEmailNavScreen__ctor_m669678776 (EditEmailNavScreen_t3910863955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen::Start()
extern "C"  void EditEmailNavScreen_Start_m2832537780 (EditEmailNavScreen_t3910863955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen::UIClosing()
extern "C"  void EditEmailNavScreen_UIClosing_m3727086149 (EditEmailNavScreen_t3910863955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void EditEmailNavScreen_ScreenLoad_m1463176656 (EditEmailNavScreen_t3910863955 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void EditEmailNavScreen_ScreenUnload_m706194044 (EditEmailNavScreen_t3910863955 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EditEmailNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool EditEmailNavScreen_ValidateScreenNavigation_m4146994217 (EditEmailNavScreen_t3910863955 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EditEmailNavScreen::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * EditEmailNavScreen_SaveScreenContent_m2713924424 (EditEmailNavScreen_t3910863955 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
