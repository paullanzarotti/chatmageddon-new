﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusChatButton
struct StatusChatButton_t749649916;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusChatButton::.ctor()
extern "C"  void StatusChatButton__ctor_m448808055 (StatusChatButton_t749649916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusChatButton::OnClick()
extern "C"  void StatusChatButton_OnClick_m675670608 (StatusChatButton_t749649916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
