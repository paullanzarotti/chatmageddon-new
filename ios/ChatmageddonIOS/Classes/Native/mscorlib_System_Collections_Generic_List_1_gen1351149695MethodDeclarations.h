﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::.ctor()
#define List_1__ctor_m1045103920(__this, method) ((  void (*) (List_1_t1351149695 *, const MethodInfo*))List_1__ctor_m2944773907_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2646038554(__this, ___collection0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3642371895_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::.ctor(System.Int32)
#define List_1__ctor_m3303014840(__this, ___capacity0, method) ((  void (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::.ctor(T[],System.Int32)
#define List_1__ctor_m590581900(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1351149695 *, TilesetFadeExampleItemU5BU5D_t1953427266*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::.cctor()
#define List_1__cctor_m1344170312(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m915755275(__this, method) ((  Il2CppObject* (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3892616799(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1351149695 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1949562710(__this, method) ((  Il2CppObject * (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m666843335(__this, ___item0, method) ((  int32_t (*) (List_1_t1351149695 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1356154855(__this, ___item0, method) ((  bool (*) (List_1_t1351149695 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2009898457(__this, ___item0, method) ((  int32_t (*) (List_1_t1351149695 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m4190118460(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1351149695 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m367330602(__this, ___item0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m969079418(__this, method) ((  bool (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2280043679(__this, method) ((  bool (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1899084067(__this, method) ((  Il2CppObject * (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3654447008(__this, method) ((  bool (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3421682331(__this, method) ((  bool (*) (List_1_t1351149695 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3985101750(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3768897165(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1351149695 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Add(T)
#define List_1_Add_m1814883108(__this, ___item0, method) ((  void (*) (List_1_t1351149695 *, TilesetFadeExampleItem_t1982028563 *, const MethodInfo*))List_1_Add_m2016287831_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1006256595(__this, ___newCount0, method) ((  void (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m930982184(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1351149695 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m97531299(__this, ___collection0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m149958243(__this, ___enumerable0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3615990694(__this, ___collection0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1153521819_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<TilesetFadeExampleItem>::AsReadOnly()
#define List_1_AsReadOnly_m2028825379(__this, method) ((  ReadOnlyCollection_1_t2167814255 * (*) (List_1_t1351149695 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Clear()
#define List_1_Clear_m2941756940(__this, method) ((  void (*) (List_1_t1351149695 *, const MethodInfo*))List_1_Clear_m1130211784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::Contains(T)
#define List_1_Contains_m300193830(__this, ___item0, method) ((  bool (*) (List_1_t1351149695 *, TilesetFadeExampleItem_t1982028563 *, const MethodInfo*))List_1_Contains_m3004503139_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CopyTo(T[])
#define List_1_CopyTo_m3142924299(__this, ___array0, method) ((  void (*) (List_1_t1351149695 *, TilesetFadeExampleItemU5BU5D_t1953427266*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4141267556(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1351149695 *, TilesetFadeExampleItemU5BU5D_t1953427266*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m2973179206(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t1351149695 *, int32_t, TilesetFadeExampleItemU5BU5D_t1953427266*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3378929717_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m70386930(__this, ___match0, method) ((  bool (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<TilesetFadeExampleItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m3830290074(__this, ___match0, method) ((  TilesetFadeExampleItem_t1982028563 * (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2687247143(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t424998678 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TilesetFadeExampleItem>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1399003295(__this, ___match0, method) ((  List_1_t1351149695 * (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TilesetFadeExampleItem>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m3302045765(__this, ___match0, method) ((  List_1_t1351149695 * (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TilesetFadeExampleItem>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m409060069(__this, ___match0, method) ((  List_1_t1351149695 * (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m2368842663(__this, ___match0, method) ((  int32_t (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1285320676(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1351149695 *, int32_t, int32_t, Predicate_1_t424998678 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1641736005(__this, ___action0, method) ((  void (*) (List_1_t1351149695 *, Action_1_t1783827945 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<TilesetFadeExampleItem>::GetEnumerator()
#define List_1_GetEnumerator_m1332401417(__this, method) ((  Enumerator_t885879369  (*) (List_1_t1351149695 *, const MethodInfo*))List_1_GetEnumerator_m1278946119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::IndexOf(T)
#define List_1_IndexOf_m1531103498(__this, ___item0, method) ((  int32_t (*) (List_1_t1351149695 *, TilesetFadeExampleItem_t1982028563 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3502827899(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1351149695 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m772796452(__this, ___index0, method) ((  void (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Insert(System.Int32,T)
#define List_1_Insert_m2926944281(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1351149695 *, int32_t, TilesetFadeExampleItem_t1982028563 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m379481482(__this, ___collection0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<TilesetFadeExampleItem>::Remove(T)
#define List_1_Remove_m401696873(__this, ___item0, method) ((  bool (*) (List_1_t1351149695 *, TilesetFadeExampleItem_t1982028563 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2324207357(__this, ___match0, method) ((  int32_t (*) (List_1_t1351149695 *, Predicate_1_t424998678 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1703095637(__this, ___index0, method) ((  void (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m298376056(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1351149695 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Reverse()
#define List_1_Reverse_m2776449547(__this, method) ((  void (*) (List_1_t1351149695 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Sort()
#define List_1_Sort_m1965252001(__this, method) ((  void (*) (List_1_t1351149695 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2569569549(__this, ___comparer0, method) ((  void (*) (List_1_t1351149695 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m4225808000(__this, ___comparison0, method) ((  void (*) (List_1_t1351149695 *, Comparison_1_t3243767414 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<TilesetFadeExampleItem>::ToArray()
#define List_1_ToArray_m2379885926(__this, method) ((  TilesetFadeExampleItemU5BU5D_t1953427266* (*) (List_1_t1351149695 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::TrimExcess()
#define List_1_TrimExcess_m673688422(__this, method) ((  void (*) (List_1_t1351149695 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::get_Capacity()
#define List_1_get_Capacity_m599342992(__this, method) ((  int32_t (*) (List_1_t1351149695 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m51482211(__this, ___value0, method) ((  void (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<TilesetFadeExampleItem>::get_Count()
#define List_1_get_Count_m2455627175(__this, method) ((  int32_t (*) (List_1_t1351149695 *, const MethodInfo*))List_1_get_Count_m2021003857_gshared)(__this, method)
// T System.Collections.Generic.List`1<TilesetFadeExampleItem>::get_Item(System.Int32)
#define List_1_get_Item_m1499403737(__this, ___index0, method) ((  TilesetFadeExampleItem_t1982028563 * (*) (List_1_t1351149695 *, int32_t, const MethodInfo*))List_1_get_Item_m1607739884_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TilesetFadeExampleItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m3373229958(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1351149695 *, int32_t, TilesetFadeExampleItem_t1982028563 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
