﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GroupFriendUI
struct GroupFriendUI_t1634706847;
// Friend
struct Friend_t3555014108;
// MapGroupModal
struct MapGroupModal_t1530151518;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_MapGroupModal1530151518.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void GroupFriendUI::.ctor()
extern "C"  void GroupFriendUI__ctor_m2428744468 (GroupFriendUI_t1634706847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupFriendUI::PopulateGroupFriend(Friend,MapGroupModal)
extern "C"  void GroupFriendUI_PopulateGroupFriend_m2561975991 (GroupFriendUI_t1634706847 * __this, Friend_t3555014108 * ___groupFriend0, MapGroupModal_t1530151518 * ___groupModal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupFriendUI::UpdateFriendAvatar()
extern "C"  void GroupFriendUI_UpdateFriendAvatar_m1980072074 (GroupFriendUI_t1634706847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupFriendUI::SetPosTweenTo(UnityEngine.Vector3)
extern "C"  void GroupFriendUI_SetPosTweenTo_m1250377327 (GroupFriendUI_t1634706847 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupFriendUI::PlayTween(System.Boolean)
extern "C"  void GroupFriendUI_PlayTween_m1265198532 (GroupFriendUI_t1634706847 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupFriendUI::OpenFriendProfile()
extern "C"  void GroupFriendUI_OpenFriendProfile_m87184395 (GroupFriendUI_t1634706847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupFriendUI::SetCollisionActive(System.Boolean)
extern "C"  void GroupFriendUI_SetCollisionActive_m3856327501 (GroupFriendUI_t1634706847 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
