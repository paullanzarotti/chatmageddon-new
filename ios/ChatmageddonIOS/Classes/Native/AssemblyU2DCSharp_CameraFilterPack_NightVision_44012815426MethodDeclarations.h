﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_NightVision_4
struct CameraFilterPack_NightVision_4_t4012815426;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_NightVision_4::.ctor()
extern "C"  void CameraFilterPack_NightVision_4__ctor_m1064279507 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_NightVision_4::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_NightVision_4_get_material_m506904636 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::ChangeFilters()
extern "C"  void CameraFilterPack_NightVision_4_ChangeFilters_m4152750146 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::Start()
extern "C"  void CameraFilterPack_NightVision_4_Start_m3619723087 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_NightVision_4_OnRenderImage_m2546002303 (CameraFilterPack_NightVision_4_t4012815426 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::OnValidate()
extern "C"  void CameraFilterPack_NightVision_4_OnValidate_m3619816262 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::Update()
extern "C"  void CameraFilterPack_NightVision_4_Update_m2955563788 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::OnDisable()
extern "C"  void CameraFilterPack_NightVision_4_OnDisable_m2692617234 (CameraFilterPack_NightVision_4_t4012815426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
