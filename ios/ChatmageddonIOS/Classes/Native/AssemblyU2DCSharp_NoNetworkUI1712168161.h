﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenColor
struct TweenColor_t3390486518;
// TweenScale
struct TweenScale_t2697902175;

#include "AssemblyU2DCSharp_LockoutUI1337294023.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoNetworkUI
struct  NoNetworkUI_t1712168161  : public LockoutUI_t1337294023
{
public:
	// UISprite NoNetworkUI::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_3;
	// TweenAlpha NoNetworkUI::backgroundTween
	TweenAlpha_t2421518635 * ___backgroundTween_4;
	// TweenAlpha NoNetworkUI::backgroundFlashTween
	TweenAlpha_t2421518635 * ___backgroundFlashTween_5;
	// TweenColor NoNetworkUI::backgroundColourTween
	TweenColor_t3390486518 * ___backgroundColourTween_6;
	// TweenScale NoNetworkUI::contentTween
	TweenScale_t2697902175 * ___contentTween_7;
	// UnityEngine.Vector3 NoNetworkUI::uiPosition
	Vector3_t2243707580  ___uiPosition_8;

public:
	inline static int32_t get_offset_of_backgroundSprite_3() { return static_cast<int32_t>(offsetof(NoNetworkUI_t1712168161, ___backgroundSprite_3)); }
	inline UISprite_t603616735 * get_backgroundSprite_3() const { return ___backgroundSprite_3; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_3() { return &___backgroundSprite_3; }
	inline void set_backgroundSprite_3(UISprite_t603616735 * value)
	{
		___backgroundSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_3, value);
	}

	inline static int32_t get_offset_of_backgroundTween_4() { return static_cast<int32_t>(offsetof(NoNetworkUI_t1712168161, ___backgroundTween_4)); }
	inline TweenAlpha_t2421518635 * get_backgroundTween_4() const { return ___backgroundTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundTween_4() { return &___backgroundTween_4; }
	inline void set_backgroundTween_4(TweenAlpha_t2421518635 * value)
	{
		___backgroundTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTween_4, value);
	}

	inline static int32_t get_offset_of_backgroundFlashTween_5() { return static_cast<int32_t>(offsetof(NoNetworkUI_t1712168161, ___backgroundFlashTween_5)); }
	inline TweenAlpha_t2421518635 * get_backgroundFlashTween_5() const { return ___backgroundFlashTween_5; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundFlashTween_5() { return &___backgroundFlashTween_5; }
	inline void set_backgroundFlashTween_5(TweenAlpha_t2421518635 * value)
	{
		___backgroundFlashTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundFlashTween_5, value);
	}

	inline static int32_t get_offset_of_backgroundColourTween_6() { return static_cast<int32_t>(offsetof(NoNetworkUI_t1712168161, ___backgroundColourTween_6)); }
	inline TweenColor_t3390486518 * get_backgroundColourTween_6() const { return ___backgroundColourTween_6; }
	inline TweenColor_t3390486518 ** get_address_of_backgroundColourTween_6() { return &___backgroundColourTween_6; }
	inline void set_backgroundColourTween_6(TweenColor_t3390486518 * value)
	{
		___backgroundColourTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundColourTween_6, value);
	}

	inline static int32_t get_offset_of_contentTween_7() { return static_cast<int32_t>(offsetof(NoNetworkUI_t1712168161, ___contentTween_7)); }
	inline TweenScale_t2697902175 * get_contentTween_7() const { return ___contentTween_7; }
	inline TweenScale_t2697902175 ** get_address_of_contentTween_7() { return &___contentTween_7; }
	inline void set_contentTween_7(TweenScale_t2697902175 * value)
	{
		___contentTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___contentTween_7, value);
	}

	inline static int32_t get_offset_of_uiPosition_8() { return static_cast<int32_t>(offsetof(NoNetworkUI_t1712168161, ___uiPosition_8)); }
	inline Vector3_t2243707580  get_uiPosition_8() const { return ___uiPosition_8; }
	inline Vector3_t2243707580 * get_address_of_uiPosition_8() { return &___uiPosition_8; }
	inline void set_uiPosition_8(Vector3_t2243707580  value)
	{
		___uiPosition_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
