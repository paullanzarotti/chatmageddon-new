﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.SharingManager
struct SharingManager_t3325775873;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.SharingManager::.cctor()
extern "C"  void SharingManager__cctor_m3020320028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::.ctor()
extern "C"  void SharingManager__ctor_m953925353 (SharingManager_t3325775873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::add_sharingFinishedWithActivityTypeEvent(System.Action`1<System.String>)
extern "C"  void SharingManager_add_sharingFinishedWithActivityTypeEvent_m2689402590 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::remove_sharingFinishedWithActivityTypeEvent(System.Action`1<System.String>)
extern "C"  void SharingManager_remove_sharingFinishedWithActivityTypeEvent_m10057501 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::add_sharingCancelledEvent(System.Action)
extern "C"  void SharingManager_add_sharingCancelledEvent_m3133893273 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::remove_sharingCancelledEvent(System.Action)
extern "C"  void SharingManager_remove_sharingCancelledEvent_m3851981762 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::sharingFinishedWithActivityType(System.String)
extern "C"  void SharingManager_sharingFinishedWithActivityType_m373295736 (SharingManager_t3325775873 * __this, String_t* ___activityType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingManager::sharingCancelled(System.String)
extern "C"  void SharingManager_sharingCancelled_m4212084300 (SharingManager_t3325775873 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
