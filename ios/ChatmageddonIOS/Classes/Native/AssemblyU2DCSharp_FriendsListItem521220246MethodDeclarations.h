﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsListItem
struct FriendsListItem_t521220246;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void FriendsListItem::.ctor()
extern "C"  void FriendsListItem__ctor_m398760481 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnEnable()
extern "C"  void FriendsListItem_OnEnable_m2650823409 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnDisable()
extern "C"  void FriendsListItem_OnDisable_m352891802 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::CalculatorHitZero(System.Boolean)
extern "C"  void FriendsListItem_CalculatorHitZero_m2259835537 (FriendsListItem_t521220246 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetUpItem()
extern "C"  void FriendsListItem_SetUpItem_m2571388719 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::PopulateItem(Friend,System.Boolean)
extern "C"  void FriendsListItem_PopulateItem_m3473451759 (FriendsListItem_t521220246 * __this, Friend_t3555014108 * ___newFriend0, bool ___globalFriend1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::UpdateAvatar()
extern "C"  void FriendsListItem_UpdateAvatar_m2321989161 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetFriendRequestRecieved(System.Boolean)
extern "C"  void FriendsListItem_SetFriendRequestRecieved_m1108727290 (FriendsListItem_t521220246 * __this, bool ___requestRecieved0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetFriendRequestSent(System.Boolean)
extern "C"  void FriendsListItem_SetFriendRequestSent_m4202177147 (FriendsListItem_t521220246 * __this, bool ___requestSent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::UpdateLockoutStatus()
extern "C"  void FriendsListItem_UpdateLockoutStatus_m575339905 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetBackgroundColour(System.Boolean)
extern "C"  void FriendsListItem_SetBackgroundColour_m2462579604 (FriendsListItem_t521220246 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetBackgroundFriendRequest()
extern "C"  void FriendsListItem_SetBackgroundFriendRequest_m4218524896 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetBackgroundSpriteColour(UnityEngine.Color)
extern "C"  void FriendsListItem_SetBackgroundSpriteColour_m4254371560 (FriendsListItem_t521220246 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetProfileOutlineColour(UnityEngine.Color)
extern "C"  void FriendsListItem_SetProfileOutlineColour_m1003346860 (FriendsListItem_t521220246 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetVisible()
extern "C"  void FriendsListItem_SetVisible_m3707412645 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::SetInvisible()
extern "C"  void FriendsListItem_SetInvisible_m3815412654 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsListItem::verifyVisibility()
extern "C"  bool FriendsListItem_verifyVisibility_m3974286086 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::CheckVisibilty()
extern "C"  void FriendsListItem_CheckVisibilty_m3401409026 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::Update()
extern "C"  void FriendsListItem_Update_m3612430260 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::FindScrollView()
extern "C"  void FriendsListItem_FindScrollView_m1198017242 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnPress(System.Boolean)
extern "C"  void FriendsListItem_OnPress_m3521510460 (FriendsListItem_t521220246 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void FriendsListItem_OnDrag_m719159686 (FriendsListItem_t521220246 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnDragEnd()
extern "C"  void FriendsListItem_OnDragEnd_m2540044343 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnScroll(System.Single)
extern "C"  void FriendsListItem_OnScroll_m3380722970 (FriendsListItem_t521220246 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItem::OnClick()
extern "C"  void FriendsListItem_OnClick_m2284503674 (FriendsListItem_t521220246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
