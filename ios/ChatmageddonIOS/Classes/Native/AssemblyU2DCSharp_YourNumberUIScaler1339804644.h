﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YourNumberUIScaler
struct  YourNumberUIScaler_t1339804644  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite YourNumberUIScaler::topDivider
	UISprite_t603616735 * ___topDivider_14;
	// UISprite YourNumberUIScaler::middleDivider
	UISprite_t603616735 * ___middleDivider_15;
	// UISprite YourNumberUIScaler::bottomDivider
	UISprite_t603616735 * ___bottomDivider_16;
	// UnityEngine.BoxCollider YourNumberUIScaler::continueButtonCollider
	BoxCollider_t22920061 * ___continueButtonCollider_17;
	// UISprite YourNumberUIScaler::continueButton
	UISprite_t603616735 * ___continueButton_18;
	// UnityEngine.BoxCollider YourNumberUIScaler::countryPickerCollider
	BoxCollider_t22920061 * ___countryPickerCollider_19;
	// UISprite YourNumberUIScaler::countryPickerButton
	UISprite_t603616735 * ___countryPickerButton_20;
	// UILabel YourNumberUIScaler::countryPickerLabel
	UILabel_t1795115428 * ___countryPickerLabel_21;
	// UILabel YourNumberUIScaler::countryCodeLabel
	UILabel_t1795115428 * ___countryCodeLabel_22;
	// UISprite YourNumberUIScaler::countryCodeSeperator
	UISprite_t603616735 * ___countryCodeSeperator_23;
	// UnityEngine.BoxCollider YourNumberUIScaler::phoneNumberCollider
	BoxCollider_t22920061 * ___phoneNumberCollider_24;

public:
	inline static int32_t get_offset_of_topDivider_14() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___topDivider_14)); }
	inline UISprite_t603616735 * get_topDivider_14() const { return ___topDivider_14; }
	inline UISprite_t603616735 ** get_address_of_topDivider_14() { return &___topDivider_14; }
	inline void set_topDivider_14(UISprite_t603616735 * value)
	{
		___topDivider_14 = value;
		Il2CppCodeGenWriteBarrier(&___topDivider_14, value);
	}

	inline static int32_t get_offset_of_middleDivider_15() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___middleDivider_15)); }
	inline UISprite_t603616735 * get_middleDivider_15() const { return ___middleDivider_15; }
	inline UISprite_t603616735 ** get_address_of_middleDivider_15() { return &___middleDivider_15; }
	inline void set_middleDivider_15(UISprite_t603616735 * value)
	{
		___middleDivider_15 = value;
		Il2CppCodeGenWriteBarrier(&___middleDivider_15, value);
	}

	inline static int32_t get_offset_of_bottomDivider_16() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___bottomDivider_16)); }
	inline UISprite_t603616735 * get_bottomDivider_16() const { return ___bottomDivider_16; }
	inline UISprite_t603616735 ** get_address_of_bottomDivider_16() { return &___bottomDivider_16; }
	inline void set_bottomDivider_16(UISprite_t603616735 * value)
	{
		___bottomDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___bottomDivider_16, value);
	}

	inline static int32_t get_offset_of_continueButtonCollider_17() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___continueButtonCollider_17)); }
	inline BoxCollider_t22920061 * get_continueButtonCollider_17() const { return ___continueButtonCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_continueButtonCollider_17() { return &___continueButtonCollider_17; }
	inline void set_continueButtonCollider_17(BoxCollider_t22920061 * value)
	{
		___continueButtonCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___continueButtonCollider_17, value);
	}

	inline static int32_t get_offset_of_continueButton_18() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___continueButton_18)); }
	inline UISprite_t603616735 * get_continueButton_18() const { return ___continueButton_18; }
	inline UISprite_t603616735 ** get_address_of_continueButton_18() { return &___continueButton_18; }
	inline void set_continueButton_18(UISprite_t603616735 * value)
	{
		___continueButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___continueButton_18, value);
	}

	inline static int32_t get_offset_of_countryPickerCollider_19() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___countryPickerCollider_19)); }
	inline BoxCollider_t22920061 * get_countryPickerCollider_19() const { return ___countryPickerCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_countryPickerCollider_19() { return &___countryPickerCollider_19; }
	inline void set_countryPickerCollider_19(BoxCollider_t22920061 * value)
	{
		___countryPickerCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___countryPickerCollider_19, value);
	}

	inline static int32_t get_offset_of_countryPickerButton_20() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___countryPickerButton_20)); }
	inline UISprite_t603616735 * get_countryPickerButton_20() const { return ___countryPickerButton_20; }
	inline UISprite_t603616735 ** get_address_of_countryPickerButton_20() { return &___countryPickerButton_20; }
	inline void set_countryPickerButton_20(UISprite_t603616735 * value)
	{
		___countryPickerButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___countryPickerButton_20, value);
	}

	inline static int32_t get_offset_of_countryPickerLabel_21() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___countryPickerLabel_21)); }
	inline UILabel_t1795115428 * get_countryPickerLabel_21() const { return ___countryPickerLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_countryPickerLabel_21() { return &___countryPickerLabel_21; }
	inline void set_countryPickerLabel_21(UILabel_t1795115428 * value)
	{
		___countryPickerLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___countryPickerLabel_21, value);
	}

	inline static int32_t get_offset_of_countryCodeLabel_22() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___countryCodeLabel_22)); }
	inline UILabel_t1795115428 * get_countryCodeLabel_22() const { return ___countryCodeLabel_22; }
	inline UILabel_t1795115428 ** get_address_of_countryCodeLabel_22() { return &___countryCodeLabel_22; }
	inline void set_countryCodeLabel_22(UILabel_t1795115428 * value)
	{
		___countryCodeLabel_22 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeLabel_22, value);
	}

	inline static int32_t get_offset_of_countryCodeSeperator_23() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___countryCodeSeperator_23)); }
	inline UISprite_t603616735 * get_countryCodeSeperator_23() const { return ___countryCodeSeperator_23; }
	inline UISprite_t603616735 ** get_address_of_countryCodeSeperator_23() { return &___countryCodeSeperator_23; }
	inline void set_countryCodeSeperator_23(UISprite_t603616735 * value)
	{
		___countryCodeSeperator_23 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeSeperator_23, value);
	}

	inline static int32_t get_offset_of_phoneNumberCollider_24() { return static_cast<int32_t>(offsetof(YourNumberUIScaler_t1339804644, ___phoneNumberCollider_24)); }
	inline BoxCollider_t22920061 * get_phoneNumberCollider_24() const { return ___phoneNumberCollider_24; }
	inline BoxCollider_t22920061 ** get_address_of_phoneNumberCollider_24() { return &___phoneNumberCollider_24; }
	inline void set_phoneNumberCollider_24(BoxCollider_t22920061 * value)
	{
		___phoneNumberCollider_24 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberCollider_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
