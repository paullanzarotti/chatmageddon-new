﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0
struct U3CSaveScreenContentU3Ec__Iterator0_t2360964071;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0__ctor_m1684950230 (U3CSaveScreenContentU3Ec__Iterator0_t2360964071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0::MoveNext()
extern "C"  bool U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m327469014 (U3CSaveScreenContentU3Ec__Iterator0_t2360964071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3720549304 (U3CSaveScreenContentU3Ec__Iterator0_t2360964071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2049485136 (U3CSaveScreenContentU3Ec__Iterator0_t2360964071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0::Dispose()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Dispose_m124538873 (U3CSaveScreenContentU3Ec__Iterator0_t2360964071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceNavScreen/<SaveScreenContent>c__Iterator0::Reset()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Reset_m3228510827 (U3CSaveScreenContentU3Ec__Iterator0_t2360964071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
