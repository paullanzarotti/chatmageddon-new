﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoPointsBribeButton
struct  NoPointsBribeButton_t319339654  : public SFXButton_t792651341
{
public:
	// UILabel NoPointsBribeButton::bucksAmountLabel
	UILabel_t1795115428 * ___bucksAmountLabel_5;

public:
	inline static int32_t get_offset_of_bucksAmountLabel_5() { return static_cast<int32_t>(offsetof(NoPointsBribeButton_t319339654, ___bucksAmountLabel_5)); }
	inline UILabel_t1795115428 * get_bucksAmountLabel_5() const { return ___bucksAmountLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_bucksAmountLabel_5() { return &___bucksAmountLabel_5; }
	inline void set_bucksAmountLabel_5(UILabel_t1795115428 * value)
	{
		___bucksAmountLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___bucksAmountLabel_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
