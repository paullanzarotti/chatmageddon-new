﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StorePopUpController
struct StorePopUpController_t952004567;

#include "codegen/il2cpp-codegen.h"

// System.Void StorePopUpController::.ctor()
extern "C"  void StorePopUpController__ctor_m1323314100 (StorePopUpController_t952004567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StorePopUpController::Start()
extern "C"  void StorePopUpController_Start_m1284902008 (StorePopUpController_t952004567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StorePopUpController::PopUptweenFinish()
extern "C"  void StorePopUpController_PopUptweenFinish_m3780569380 (StorePopUpController_t952004567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StorePopUpController::ActivatePU(System.Boolean)
extern "C"  void StorePopUpController_ActivatePU_m4044694333 (StorePopUpController_t952004567 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StorePopUpController::OpenNotEnoughBucksPU(System.Int32)
extern "C"  void StorePopUpController_OpenNotEnoughBucksPU_m3394175433 (StorePopUpController_t952004567 * __this, int32_t ___bucksAmount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StorePopUpController::OpenPurchasePU()
extern "C"  void StorePopUpController_OpenPurchasePU_m1029459918 (StorePopUpController_t952004567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
