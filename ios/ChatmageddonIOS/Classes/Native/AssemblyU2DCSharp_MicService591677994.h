﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MicService
struct  MicService_t591677994  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MicService::sensitivity
	float ___sensitivity_2;
	// System.Single MicService::<loudness>k__BackingField
	float ___U3CloudnessU3Ek__BackingField_3;
	// System.Int32 MicService::timeLeft
	int32_t ___timeLeft_4;
	// System.Boolean MicService::testing
	bool ___testing_5;
	// System.Int32 MicService::samplerate
	int32_t ___samplerate_6;
	// System.Single MicService::sourceVolume
	float ___sourceVolume_7;
	// System.String MicService::deviceName
	String_t* ___deviceName_8;
	// System.Int32 MicService::amountSamples
	int32_t ___amountSamples_9;
	// System.Int32 MicService::minFreq
	int32_t ___minFreq_10;
	// System.Int32 MicService::maxFreq
	int32_t ___maxFreq_11;
	// UnityEngine.AudioSource MicService::goAudioSource
	AudioSource_t1135106623 * ___goAudioSource_12;

public:
	inline static int32_t get_offset_of_sensitivity_2() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___sensitivity_2)); }
	inline float get_sensitivity_2() const { return ___sensitivity_2; }
	inline float* get_address_of_sensitivity_2() { return &___sensitivity_2; }
	inline void set_sensitivity_2(float value)
	{
		___sensitivity_2 = value;
	}

	inline static int32_t get_offset_of_U3CloudnessU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___U3CloudnessU3Ek__BackingField_3)); }
	inline float get_U3CloudnessU3Ek__BackingField_3() const { return ___U3CloudnessU3Ek__BackingField_3; }
	inline float* get_address_of_U3CloudnessU3Ek__BackingField_3() { return &___U3CloudnessU3Ek__BackingField_3; }
	inline void set_U3CloudnessU3Ek__BackingField_3(float value)
	{
		___U3CloudnessU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_timeLeft_4() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___timeLeft_4)); }
	inline int32_t get_timeLeft_4() const { return ___timeLeft_4; }
	inline int32_t* get_address_of_timeLeft_4() { return &___timeLeft_4; }
	inline void set_timeLeft_4(int32_t value)
	{
		___timeLeft_4 = value;
	}

	inline static int32_t get_offset_of_testing_5() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___testing_5)); }
	inline bool get_testing_5() const { return ___testing_5; }
	inline bool* get_address_of_testing_5() { return &___testing_5; }
	inline void set_testing_5(bool value)
	{
		___testing_5 = value;
	}

	inline static int32_t get_offset_of_samplerate_6() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___samplerate_6)); }
	inline int32_t get_samplerate_6() const { return ___samplerate_6; }
	inline int32_t* get_address_of_samplerate_6() { return &___samplerate_6; }
	inline void set_samplerate_6(int32_t value)
	{
		___samplerate_6 = value;
	}

	inline static int32_t get_offset_of_sourceVolume_7() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___sourceVolume_7)); }
	inline float get_sourceVolume_7() const { return ___sourceVolume_7; }
	inline float* get_address_of_sourceVolume_7() { return &___sourceVolume_7; }
	inline void set_sourceVolume_7(float value)
	{
		___sourceVolume_7 = value;
	}

	inline static int32_t get_offset_of_deviceName_8() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___deviceName_8)); }
	inline String_t* get_deviceName_8() const { return ___deviceName_8; }
	inline String_t** get_address_of_deviceName_8() { return &___deviceName_8; }
	inline void set_deviceName_8(String_t* value)
	{
		___deviceName_8 = value;
		Il2CppCodeGenWriteBarrier(&___deviceName_8, value);
	}

	inline static int32_t get_offset_of_amountSamples_9() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___amountSamples_9)); }
	inline int32_t get_amountSamples_9() const { return ___amountSamples_9; }
	inline int32_t* get_address_of_amountSamples_9() { return &___amountSamples_9; }
	inline void set_amountSamples_9(int32_t value)
	{
		___amountSamples_9 = value;
	}

	inline static int32_t get_offset_of_minFreq_10() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___minFreq_10)); }
	inline int32_t get_minFreq_10() const { return ___minFreq_10; }
	inline int32_t* get_address_of_minFreq_10() { return &___minFreq_10; }
	inline void set_minFreq_10(int32_t value)
	{
		___minFreq_10 = value;
	}

	inline static int32_t get_offset_of_maxFreq_11() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___maxFreq_11)); }
	inline int32_t get_maxFreq_11() const { return ___maxFreq_11; }
	inline int32_t* get_address_of_maxFreq_11() { return &___maxFreq_11; }
	inline void set_maxFreq_11(int32_t value)
	{
		___maxFreq_11 = value;
	}

	inline static int32_t get_offset_of_goAudioSource_12() { return static_cast<int32_t>(offsetof(MicService_t591677994, ___goAudioSource_12)); }
	inline AudioSource_t1135106623 * get_goAudioSource_12() const { return ___goAudioSource_12; }
	inline AudioSource_t1135106623 ** get_address_of_goAudioSource_12() { return &___goAudioSource_12; }
	inline void set_goAudioSource_12(AudioSource_t1135106623 * value)
	{
		___goAudioSource_12 = value;
		Il2CppCodeGenWriteBarrier(&___goAudioSource_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
