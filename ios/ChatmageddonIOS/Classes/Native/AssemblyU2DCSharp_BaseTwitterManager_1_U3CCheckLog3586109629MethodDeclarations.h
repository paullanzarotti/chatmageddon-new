﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>
struct U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CCheckLogingSuccessU3Ec__Iterator0__ctor_m501644860_gshared (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 * __this, const MethodInfo* method);
#define U3CCheckLogingSuccessU3Ec__Iterator0__ctor_m501644860(__this, method) ((  void (*) (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 *, const MethodInfo*))U3CCheckLogingSuccessU3Ec__Iterator0__ctor_m501644860_gshared)(__this, method)
// System.Boolean BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CCheckLogingSuccessU3Ec__Iterator0_MoveNext_m788729036_gshared (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 * __this, const MethodInfo* method);
#define U3CCheckLogingSuccessU3Ec__Iterator0_MoveNext_m788729036(__this, method) ((  bool (*) (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 *, const MethodInfo*))U3CCheckLogingSuccessU3Ec__Iterator0_MoveNext_m788729036_gshared)(__this, method)
// System.Object BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckLogingSuccessU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3734415152_gshared (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 * __this, const MethodInfo* method);
#define U3CCheckLogingSuccessU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3734415152(__this, method) ((  Il2CppObject * (*) (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 *, const MethodInfo*))U3CCheckLogingSuccessU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3734415152_gshared)(__this, method)
// System.Object BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckLogingSuccessU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m899290040_gshared (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 * __this, const MethodInfo* method);
#define U3CCheckLogingSuccessU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m899290040(__this, method) ((  Il2CppObject * (*) (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 *, const MethodInfo*))U3CCheckLogingSuccessU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m899290040_gshared)(__this, method)
// System.Void BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CCheckLogingSuccessU3Ec__Iterator0_Dispose_m1703640231_gshared (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 * __this, const MethodInfo* method);
#define U3CCheckLogingSuccessU3Ec__Iterator0_Dispose_m1703640231(__this, method) ((  void (*) (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 *, const MethodInfo*))U3CCheckLogingSuccessU3Ec__Iterator0_Dispose_m1703640231_gshared)(__this, method)
// System.Void BaseTwitterManager`1/<CheckLogingSuccess>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CCheckLogingSuccessU3Ec__Iterator0_Reset_m3720764349_gshared (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 * __this, const MethodInfo* method);
#define U3CCheckLogingSuccessU3Ec__Iterator0_Reset_m3720764349(__this, method) ((  void (*) (U3CCheckLogingSuccessU3Ec__Iterator0_t3586109629 *, const MethodInfo*))U3CCheckLogingSuccessU3Ec__Iterator0_Reset_m3720764349_gshared)(__this, method)
