﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<OnlineMapsBuildingBase>
struct Action_1_t452526403;
// System.Action
struct Action_t3226471752;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// OnlineMapsBuildings
struct OnlineMapsBuildings_t3411229051;
// OnlineMapsRange
struct OnlineMapsRange_t3791609909;
// OnlineMapsBuildingMaterial[]
struct OnlineMapsBuildingMaterialU5BU5D_t1202819502;
// OnlineMapsVector2i
struct OnlineMapsVector2i_t2180897250;
// System.Collections.Generic.Dictionary`2<System.String,OnlineMapsBuildingBase>
struct Dictionary_2_t2565506283;
// System.Collections.Generic.List`1<OnlineMapsBuildingsNodeData>
struct List_1_t840985563;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuildings
struct  OnlineMapsBuildings_t3411229051  : public MonoBehaviour_t1158329972
{
public:
	// System.Action`1<OnlineMapsBuildingBase> OnlineMapsBuildings::OnBuildingCreated
	Action_1_t452526403 * ___OnBuildingCreated_2;
	// System.Action`1<OnlineMapsBuildingBase> OnlineMapsBuildings::OnBuildingDispose
	Action_1_t452526403 * ___OnBuildingDispose_3;
	// System.Action OnlineMapsBuildings::OnNewBuildingsReceived
	Action_t3226471752 * ___OnNewBuildingsReceived_4;
	// System.Action OnlineMapsBuildings::OnRequestSent
	Action_t3226471752 * ___OnRequestSent_5;
	// System.Action OnlineMapsBuildings::OnRequestComplete
	Action_t3226471752 * ___OnRequestComplete_6;
	// OnlineMapsRange OnlineMapsBuildings::zoomRange
	OnlineMapsRange_t3791609909 * ___zoomRange_9;
	// OnlineMapsRange OnlineMapsBuildings::levelsRange
	OnlineMapsRange_t3791609909 * ___levelsRange_10;
	// System.Single OnlineMapsBuildings::levelHeight
	float ___levelHeight_11;
	// System.Single OnlineMapsBuildings::minHeight
	float ___minHeight_12;
	// System.Single OnlineMapsBuildings::heightScale
	float ___heightScale_13;
	// OnlineMapsBuildingMaterial[] OnlineMapsBuildings::materials
	OnlineMapsBuildingMaterialU5BU5D_t1202819502* ___materials_14;
	// OnlineMapsVector2i OnlineMapsBuildings::topLeft
	OnlineMapsVector2i_t2180897250 * ___topLeft_15;
	// OnlineMapsVector2i OnlineMapsBuildings::bottomRight
	OnlineMapsVector2i_t2180897250 * ___bottomRight_16;
	// System.Collections.Generic.Dictionary`2<System.String,OnlineMapsBuildingBase> OnlineMapsBuildings::buildings
	Dictionary_2_t2565506283 * ___buildings_17;
	// System.Collections.Generic.Dictionary`2<System.String,OnlineMapsBuildingBase> OnlineMapsBuildings::unusedBuildings
	Dictionary_2_t2565506283 * ___unusedBuildings_18;
	// System.Collections.Generic.List`1<OnlineMapsBuildingsNodeData> OnlineMapsBuildings::newBuildingsData
	List_1_t840985563 * ___newBuildingsData_19;
	// System.Boolean OnlineMapsBuildings::sendBuildingsReceived
	bool ___sendBuildingsReceived_20;

public:
	inline static int32_t get_offset_of_OnBuildingCreated_2() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___OnBuildingCreated_2)); }
	inline Action_1_t452526403 * get_OnBuildingCreated_2() const { return ___OnBuildingCreated_2; }
	inline Action_1_t452526403 ** get_address_of_OnBuildingCreated_2() { return &___OnBuildingCreated_2; }
	inline void set_OnBuildingCreated_2(Action_1_t452526403 * value)
	{
		___OnBuildingCreated_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnBuildingCreated_2, value);
	}

	inline static int32_t get_offset_of_OnBuildingDispose_3() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___OnBuildingDispose_3)); }
	inline Action_1_t452526403 * get_OnBuildingDispose_3() const { return ___OnBuildingDispose_3; }
	inline Action_1_t452526403 ** get_address_of_OnBuildingDispose_3() { return &___OnBuildingDispose_3; }
	inline void set_OnBuildingDispose_3(Action_1_t452526403 * value)
	{
		___OnBuildingDispose_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnBuildingDispose_3, value);
	}

	inline static int32_t get_offset_of_OnNewBuildingsReceived_4() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___OnNewBuildingsReceived_4)); }
	inline Action_t3226471752 * get_OnNewBuildingsReceived_4() const { return ___OnNewBuildingsReceived_4; }
	inline Action_t3226471752 ** get_address_of_OnNewBuildingsReceived_4() { return &___OnNewBuildingsReceived_4; }
	inline void set_OnNewBuildingsReceived_4(Action_t3226471752 * value)
	{
		___OnNewBuildingsReceived_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnNewBuildingsReceived_4, value);
	}

	inline static int32_t get_offset_of_OnRequestSent_5() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___OnRequestSent_5)); }
	inline Action_t3226471752 * get_OnRequestSent_5() const { return ___OnRequestSent_5; }
	inline Action_t3226471752 ** get_address_of_OnRequestSent_5() { return &___OnRequestSent_5; }
	inline void set_OnRequestSent_5(Action_t3226471752 * value)
	{
		___OnRequestSent_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnRequestSent_5, value);
	}

	inline static int32_t get_offset_of_OnRequestComplete_6() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___OnRequestComplete_6)); }
	inline Action_t3226471752 * get_OnRequestComplete_6() const { return ___OnRequestComplete_6; }
	inline Action_t3226471752 ** get_address_of_OnRequestComplete_6() { return &___OnRequestComplete_6; }
	inline void set_OnRequestComplete_6(Action_t3226471752 * value)
	{
		___OnRequestComplete_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnRequestComplete_6, value);
	}

	inline static int32_t get_offset_of_zoomRange_9() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___zoomRange_9)); }
	inline OnlineMapsRange_t3791609909 * get_zoomRange_9() const { return ___zoomRange_9; }
	inline OnlineMapsRange_t3791609909 ** get_address_of_zoomRange_9() { return &___zoomRange_9; }
	inline void set_zoomRange_9(OnlineMapsRange_t3791609909 * value)
	{
		___zoomRange_9 = value;
		Il2CppCodeGenWriteBarrier(&___zoomRange_9, value);
	}

	inline static int32_t get_offset_of_levelsRange_10() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___levelsRange_10)); }
	inline OnlineMapsRange_t3791609909 * get_levelsRange_10() const { return ___levelsRange_10; }
	inline OnlineMapsRange_t3791609909 ** get_address_of_levelsRange_10() { return &___levelsRange_10; }
	inline void set_levelsRange_10(OnlineMapsRange_t3791609909 * value)
	{
		___levelsRange_10 = value;
		Il2CppCodeGenWriteBarrier(&___levelsRange_10, value);
	}

	inline static int32_t get_offset_of_levelHeight_11() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___levelHeight_11)); }
	inline float get_levelHeight_11() const { return ___levelHeight_11; }
	inline float* get_address_of_levelHeight_11() { return &___levelHeight_11; }
	inline void set_levelHeight_11(float value)
	{
		___levelHeight_11 = value;
	}

	inline static int32_t get_offset_of_minHeight_12() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___minHeight_12)); }
	inline float get_minHeight_12() const { return ___minHeight_12; }
	inline float* get_address_of_minHeight_12() { return &___minHeight_12; }
	inline void set_minHeight_12(float value)
	{
		___minHeight_12 = value;
	}

	inline static int32_t get_offset_of_heightScale_13() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___heightScale_13)); }
	inline float get_heightScale_13() const { return ___heightScale_13; }
	inline float* get_address_of_heightScale_13() { return &___heightScale_13; }
	inline void set_heightScale_13(float value)
	{
		___heightScale_13 = value;
	}

	inline static int32_t get_offset_of_materials_14() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___materials_14)); }
	inline OnlineMapsBuildingMaterialU5BU5D_t1202819502* get_materials_14() const { return ___materials_14; }
	inline OnlineMapsBuildingMaterialU5BU5D_t1202819502** get_address_of_materials_14() { return &___materials_14; }
	inline void set_materials_14(OnlineMapsBuildingMaterialU5BU5D_t1202819502* value)
	{
		___materials_14 = value;
		Il2CppCodeGenWriteBarrier(&___materials_14, value);
	}

	inline static int32_t get_offset_of_topLeft_15() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___topLeft_15)); }
	inline OnlineMapsVector2i_t2180897250 * get_topLeft_15() const { return ___topLeft_15; }
	inline OnlineMapsVector2i_t2180897250 ** get_address_of_topLeft_15() { return &___topLeft_15; }
	inline void set_topLeft_15(OnlineMapsVector2i_t2180897250 * value)
	{
		___topLeft_15 = value;
		Il2CppCodeGenWriteBarrier(&___topLeft_15, value);
	}

	inline static int32_t get_offset_of_bottomRight_16() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___bottomRight_16)); }
	inline OnlineMapsVector2i_t2180897250 * get_bottomRight_16() const { return ___bottomRight_16; }
	inline OnlineMapsVector2i_t2180897250 ** get_address_of_bottomRight_16() { return &___bottomRight_16; }
	inline void set_bottomRight_16(OnlineMapsVector2i_t2180897250 * value)
	{
		___bottomRight_16 = value;
		Il2CppCodeGenWriteBarrier(&___bottomRight_16, value);
	}

	inline static int32_t get_offset_of_buildings_17() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___buildings_17)); }
	inline Dictionary_2_t2565506283 * get_buildings_17() const { return ___buildings_17; }
	inline Dictionary_2_t2565506283 ** get_address_of_buildings_17() { return &___buildings_17; }
	inline void set_buildings_17(Dictionary_2_t2565506283 * value)
	{
		___buildings_17 = value;
		Il2CppCodeGenWriteBarrier(&___buildings_17, value);
	}

	inline static int32_t get_offset_of_unusedBuildings_18() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___unusedBuildings_18)); }
	inline Dictionary_2_t2565506283 * get_unusedBuildings_18() const { return ___unusedBuildings_18; }
	inline Dictionary_2_t2565506283 ** get_address_of_unusedBuildings_18() { return &___unusedBuildings_18; }
	inline void set_unusedBuildings_18(Dictionary_2_t2565506283 * value)
	{
		___unusedBuildings_18 = value;
		Il2CppCodeGenWriteBarrier(&___unusedBuildings_18, value);
	}

	inline static int32_t get_offset_of_newBuildingsData_19() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___newBuildingsData_19)); }
	inline List_1_t840985563 * get_newBuildingsData_19() const { return ___newBuildingsData_19; }
	inline List_1_t840985563 ** get_address_of_newBuildingsData_19() { return &___newBuildingsData_19; }
	inline void set_newBuildingsData_19(List_1_t840985563 * value)
	{
		___newBuildingsData_19 = value;
		Il2CppCodeGenWriteBarrier(&___newBuildingsData_19, value);
	}

	inline static int32_t get_offset_of_sendBuildingsReceived_20() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051, ___sendBuildingsReceived_20)); }
	inline bool get_sendBuildingsReceived_20() const { return ___sendBuildingsReceived_20; }
	inline bool* get_address_of_sendBuildingsReceived_20() { return &___sendBuildingsReceived_20; }
	inline void set_sendBuildingsReceived_20(bool value)
	{
		___sendBuildingsReceived_20 = value;
	}
};

struct OnlineMapsBuildings_t3411229051_StaticFields
{
public:
	// UnityEngine.GameObject OnlineMapsBuildings::buildingContainer
	GameObject_t1756533147 * ___buildingContainer_7;
	// OnlineMapsBuildings OnlineMapsBuildings::_instance
	OnlineMapsBuildings_t3411229051 * ____instance_8;

public:
	inline static int32_t get_offset_of_buildingContainer_7() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051_StaticFields, ___buildingContainer_7)); }
	inline GameObject_t1756533147 * get_buildingContainer_7() const { return ___buildingContainer_7; }
	inline GameObject_t1756533147 ** get_address_of_buildingContainer_7() { return &___buildingContainer_7; }
	inline void set_buildingContainer_7(GameObject_t1756533147 * value)
	{
		___buildingContainer_7 = value;
		Il2CppCodeGenWriteBarrier(&___buildingContainer_7, value);
	}

	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(OnlineMapsBuildings_t3411229051_StaticFields, ____instance_8)); }
	inline OnlineMapsBuildings_t3411229051 * get__instance_8() const { return ____instance_8; }
	inline OnlineMapsBuildings_t3411229051 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(OnlineMapsBuildings_t3411229051 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier(&____instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
