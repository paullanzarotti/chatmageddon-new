﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<LeaderboardSceneManager>::.ctor()
#define MonoSingleton_1__ctor_m2174543910(__this, method) ((  void (*) (MonoSingleton_1_t2633962756 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<LeaderboardSceneManager>::Awake()
#define MonoSingleton_1_Awake_m4057711225(__this, method) ((  void (*) (MonoSingleton_1_t2633962756 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<LeaderboardSceneManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2089743651(__this /* static, unused */, method) ((  LeaderboardSceneManager_t2883297036 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<LeaderboardSceneManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3504126787(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<LeaderboardSceneManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2944323020(__this, method) ((  void (*) (MonoSingleton_1_t2633962756 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<LeaderboardSceneManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1607819328(__this, method) ((  void (*) (MonoSingleton_1_t2633962756 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<LeaderboardSceneManager>::.cctor()
#define MonoSingleton_1__cctor_m456820339(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
