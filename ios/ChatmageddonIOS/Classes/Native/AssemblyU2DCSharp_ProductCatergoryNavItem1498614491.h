﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SwipeNavigationController
struct SwipeNavigationController_t3767682136;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductCatergoryNavItem
struct  ProductCatergoryNavItem_t1498614491  : public SFXButton_t792651341
{
public:
	// SwipeNavigationController ProductCatergoryNavItem::navController
	SwipeNavigationController_t3767682136 * ___navController_5;
	// PurchaseableCategory ProductCatergoryNavItem::category
	int32_t ___category_6;
	// System.Int32 ProductCatergoryNavItem::index
	int32_t ___index_7;

public:
	inline static int32_t get_offset_of_navController_5() { return static_cast<int32_t>(offsetof(ProductCatergoryNavItem_t1498614491, ___navController_5)); }
	inline SwipeNavigationController_t3767682136 * get_navController_5() const { return ___navController_5; }
	inline SwipeNavigationController_t3767682136 ** get_address_of_navController_5() { return &___navController_5; }
	inline void set_navController_5(SwipeNavigationController_t3767682136 * value)
	{
		___navController_5 = value;
		Il2CppCodeGenWriteBarrier(&___navController_5, value);
	}

	inline static int32_t get_offset_of_category_6() { return static_cast<int32_t>(offsetof(ProductCatergoryNavItem_t1498614491, ___category_6)); }
	inline int32_t get_category_6() const { return ___category_6; }
	inline int32_t* get_address_of_category_6() { return &___category_6; }
	inline void set_category_6(int32_t value)
	{
		___category_6 = value;
	}

	inline static int32_t get_offset_of_index_7() { return static_cast<int32_t>(offsetof(ProductCatergoryNavItem_t1498614491, ___index_7)); }
	inline int32_t get_index_7() const { return ___index_7; }
	inline int32_t* get_address_of_index_7() { return &___index_7; }
	inline void set_index_7(int32_t value)
	{
		___index_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
