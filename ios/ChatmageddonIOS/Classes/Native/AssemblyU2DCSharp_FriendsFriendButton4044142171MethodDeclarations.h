﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsFriendButton
struct FriendsFriendButton_t4044142171;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsFriendButton::.ctor()
extern "C"  void FriendsFriendButton__ctor_m3307180528 (FriendsFriendButton_t4044142171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsFriendButton::OnButtonClick()
extern "C"  void FriendsFriendButton_OnButtonClick_m1092298607 (FriendsFriendButton_t4044142171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
