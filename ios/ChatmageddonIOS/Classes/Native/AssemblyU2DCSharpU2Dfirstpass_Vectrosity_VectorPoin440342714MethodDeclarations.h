﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vectrosity.VectorPoints
struct VectorPoints_t440342714;
// System.String
struct String_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Material193706927.h"

// System.Void Vectrosity.VectorPoints::.ctor(System.String,UnityEngine.Vector2[],UnityEngine.Material,System.Single)
extern "C"  void VectorPoints__ctor_m3809556898 (VectorPoints_t440342714 * __this, String_t* ___name0, Vector2U5BU5D_t686124026* ___points1, Material_t193706927 * ___material2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorPoints::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Material,System.Single)
extern "C"  void VectorPoints__ctor_m1916596266 (VectorPoints_t440342714 * __this, String_t* ___name0, List_1_t1612828711 * ___points1, Material_t193706927 * ___material2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorPoints::.ctor(System.String,UnityEngine.Vector3[],UnityEngine.Material,System.Single)
extern "C"  void VectorPoints__ctor_m2994002151 (VectorPoints_t440342714 * __this, String_t* ___name0, Vector3U5BU5D_t1172311765* ___points1, Material_t193706927 * ___material2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorPoints::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Material,System.Single)
extern "C"  void VectorPoints__ctor_m796642959 (VectorPoints_t440342714 * __this, String_t* ___name0, List_1_t1612828712 * ___points1, Material_t193706927 * ___material2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
