﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_OldFilm_Cutting2
struct  CameraFilterPack_OldFilm_Cutting2_t1382199431  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_OldFilm_Cutting2::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_OldFilm_Cutting2::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_OldFilm_Cutting2::Speed
	float ___Speed_4;
	// System.Single CameraFilterPack_OldFilm_Cutting2::Luminosity
	float ___Luminosity_5;
	// System.Single CameraFilterPack_OldFilm_Cutting2::Vignette
	float ___Vignette_6;
	// System.Single CameraFilterPack_OldFilm_Cutting2::Negative
	float ___Negative_7;
	// UnityEngine.Material CameraFilterPack_OldFilm_Cutting2::SCMaterial
	Material_t193706927 * ___SCMaterial_8;
	// UnityEngine.Texture2D CameraFilterPack_OldFilm_Cutting2::Texture2
	Texture2D_t3542995729 * ___Texture2_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Speed_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___Speed_4)); }
	inline float get_Speed_4() const { return ___Speed_4; }
	inline float* get_address_of_Speed_4() { return &___Speed_4; }
	inline void set_Speed_4(float value)
	{
		___Speed_4 = value;
	}

	inline static int32_t get_offset_of_Luminosity_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___Luminosity_5)); }
	inline float get_Luminosity_5() const { return ___Luminosity_5; }
	inline float* get_address_of_Luminosity_5() { return &___Luminosity_5; }
	inline void set_Luminosity_5(float value)
	{
		___Luminosity_5 = value;
	}

	inline static int32_t get_offset_of_Vignette_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___Vignette_6)); }
	inline float get_Vignette_6() const { return ___Vignette_6; }
	inline float* get_address_of_Vignette_6() { return &___Vignette_6; }
	inline void set_Vignette_6(float value)
	{
		___Vignette_6 = value;
	}

	inline static int32_t get_offset_of_Negative_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___Negative_7)); }
	inline float get_Negative_7() const { return ___Negative_7; }
	inline float* get_address_of_Negative_7() { return &___Negative_7; }
	inline void set_Negative_7(float value)
	{
		___Negative_7 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___SCMaterial_8)); }
	inline Material_t193706927 * get_SCMaterial_8() const { return ___SCMaterial_8; }
	inline Material_t193706927 ** get_address_of_SCMaterial_8() { return &___SCMaterial_8; }
	inline void set_SCMaterial_8(Material_t193706927 * value)
	{
		___SCMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_8, value);
	}

	inline static int32_t get_offset_of_Texture2_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_OldFilm_Cutting2_t1382199431, ___Texture2_9)); }
	inline Texture2D_t3542995729 * get_Texture2_9() const { return ___Texture2_9; }
	inline Texture2D_t3542995729 ** get_address_of_Texture2_9() { return &___Texture2_9; }
	inline void set_Texture2_9(Texture2D_t3542995729 * value)
	{
		___Texture2_9 = value;
		Il2CppCodeGenWriteBarrier(&___Texture2_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
