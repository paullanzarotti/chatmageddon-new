﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsFindDirection
struct OnlineMapsFindDirection_t2666247748;
// System.String
struct String_t;
// OnlineMapsGoogleAPIQuery
struct OnlineMapsGoogleAPIQuery_t356009153;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void OnlineMapsFindDirection::.ctor(System.String,System.String,System.Boolean)
extern "C"  void OnlineMapsFindDirection__ctor_m2348155226 (OnlineMapsFindDirection_t2666247748 * __this, String_t* ___origin0, String_t* ___destination1, bool ___alternatives2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsFindDirection::get_type()
extern "C"  int32_t OnlineMapsFindDirection_get_type_m3264849631 (OnlineMapsFindDirection_t2666247748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindDirection::Find(System.String,System.String,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindDirection_Find_m1004063761 (Il2CppObject * __this /* static, unused */, String_t* ___origin0, String_t* ___destination1, bool ___alternatives2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindDirection::Find(System.String,UnityEngine.Vector2,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindDirection_Find_m1262850067 (Il2CppObject * __this /* static, unused */, String_t* ___origin0, Vector2_t2243707579  ___destination1, bool ___alternatives2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindDirection::Find(UnityEngine.Vector2,System.String,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindDirection_Find_m4282969299 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, String_t* ___destination1, bool ___alternatives2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindDirection::Find(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindDirection_Find_m1153551697 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___destination1, bool ___alternatives2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
