﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceCameraButton
struct DeviceCameraButton_t2936622169;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceCameraButton::.ctor()
extern "C"  void DeviceCameraButton__ctor_m3645789028 (DeviceCameraButton_t2936622169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraButton::OnButtonClick()
extern "C"  void DeviceCameraButton_OnButtonClick_m214528157 (DeviceCameraButton_t2936622169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
