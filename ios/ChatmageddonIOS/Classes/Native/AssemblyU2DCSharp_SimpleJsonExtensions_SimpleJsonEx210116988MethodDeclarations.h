﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_String2029220233.h"

// System.String SimpleJsonExtensions.SimpleJsonExtensions::XmlString(System.Collections.Hashtable)
extern "C"  String_t* SimpleJsonExtensions_XmlString_m1777150716 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___hash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::XmlString(System.Collections.ArrayList)
extern "C"  String_t* SimpleJsonExtensions_XmlString_m3970780947 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJsonExtensions.SimpleJsonExtensions::XmlString(System.Collections.Hashtable,System.String&,System.Int32)
extern "C"  void SimpleJsonExtensions_XmlString_m3154660142 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___hash0, String_t** ___str1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJsonExtensions.SimpleJsonExtensions::XmlString(System.Collections.ArrayList,System.String&,System.Int32)
extern "C"  void SimpleJsonExtensions_XmlString_m2643387007 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___arr0, String_t** ___str1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJsonExtensions.SimpleJsonExtensions::MoveToNewLineIfNeeded(System.String&,System.Int32)
extern "C"  void SimpleJsonExtensions_MoveToNewLineIfNeeded_m974765720 (Il2CppObject * __this /* static, unused */, String_t** ___str0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::JsonString(System.Collections.Hashtable)
extern "C"  String_t* SimpleJsonExtensions_JsonString_m727814785 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___hash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::JsonString(System.Collections.ArrayList)
extern "C"  String_t* SimpleJsonExtensions_JsonString_m1303027490 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::JsonString(System.Collections.Hashtable,System.Boolean)
extern "C"  String_t* SimpleJsonExtensions_JsonString_m4208395620 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___hash0, bool ___encode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::JsonString(System.Collections.ArrayList,System.Boolean)
extern "C"  String_t* SimpleJsonExtensions_JsonString_m2380232645 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___arr0, bool ___encode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJsonExtensions.SimpleJsonExtensions::JsonString(System.Collections.Hashtable,System.String&,System.Int32,System.Boolean)
extern "C"  void SimpleJsonExtensions_JsonString_m2764182156 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___hash0, String_t** ___str1, int32_t ___level2, bool ___encode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJsonExtensions.SimpleJsonExtensions::JsonString(System.Collections.ArrayList,System.String&,System.Int32,System.Boolean)
extern "C"  void SimpleJsonExtensions_JsonString_m1427642265 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___arr0, String_t** ___str1, int32_t ___level2, bool ___encode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJsonExtensions.SimpleJsonExtensions::GetNodeAtPath(System.Collections.ArrayList,System.String[])
extern "C"  Il2CppObject * SimpleJsonExtensions_GetNodeAtPath_m685215981 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___inNodeList0, StringU5BU5D_t1642385972* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJsonExtensions.SimpleJsonExtensions::GetNodeAtPath(System.Collections.Hashtable,System.String[])
extern "C"  Il2CppObject * SimpleJsonExtensions_GetNodeAtPath_m4293065952 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___inNodeHash0, StringU5BU5D_t1642385972* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJsonExtensions.SimpleJsonExtensions::GetNodeAtPath(System.Collections.ArrayList,System.String[],System.Int32)
extern "C"  Il2CppObject * SimpleJsonExtensions_GetNodeAtPath_m3696782102 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___inNodeList0, StringU5BU5D_t1642385972* ___path1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJsonExtensions.SimpleJsonExtensions::GetNodeAtPath(System.Collections.Hashtable,System.String[],System.Int32)
extern "C"  Il2CppObject * SimpleJsonExtensions_GetNodeAtPath_m403836163 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___inNodeHash0, StringU5BU5D_t1642385972* ___path1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SimpleJsonExtensions.SimpleJsonExtensions::GetNodeWithProperty(System.Collections.ArrayList,System.String,System.String)
extern "C"  Hashtable_t909839986 * SimpleJsonExtensions_GetNodeWithProperty_m2668345020 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___inNodeList0, String_t* ___aKey1, String_t* ___aValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SimpleJsonExtensions.SimpleJsonExtensions::GetNodeWithProperty(System.Collections.Hashtable,System.String,System.String)
extern "C"  Hashtable_t909839986 * SimpleJsonExtensions_GetNodeWithProperty_m2254996981 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___inNodeHash0, String_t* ___aKey1, String_t* ___aValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::XmlDecode(System.String)
extern "C"  String_t* SimpleJsonExtensions_XmlDecode_m4177784819 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::XmlEncode(System.String)
extern "C"  String_t* SimpleJsonExtensions_XmlEncode_m3480526973 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::JsonDecode(System.String)
extern "C"  String_t* SimpleJsonExtensions_JsonDecode_m618129932 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonExtensions.SimpleJsonExtensions::JsonEncode(System.String)
extern "C"  String_t* SimpleJsonExtensions_JsonEncode_m1098224636 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
