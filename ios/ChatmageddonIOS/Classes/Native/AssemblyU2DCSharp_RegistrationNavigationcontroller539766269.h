﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// NavigateForwardButton
struct NavigateForwardButton_t3075171100;
// NavigateBackwardButton
struct NavigateBackwardButton_t1567151050;

#include "AssemblyU2DCSharp_NavigationController_2_gen3798250245.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegistrationNavigationcontroller
struct  RegistrationNavigationcontroller_t539766269  : public NavigationController_2_t3798250245
{
public:
	// UnityEngine.Vector3 RegistrationNavigationcontroller::activePos
	Vector3_t2243707580  ___activePos_8;
	// UnityEngine.Vector3 RegistrationNavigationcontroller::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_9;
	// UnityEngine.Vector3 RegistrationNavigationcontroller::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_10;
	// UnityEngine.GameObject RegistrationNavigationcontroller::startPage
	GameObject_t1756533147 * ___startPage_11;
	// UnityEngine.GameObject RegistrationNavigationcontroller::contentObject
	GameObject_t1756533147 * ___contentObject_12;
	// UILabel RegistrationNavigationcontroller::titleLabel
	UILabel_t1795115428 * ___titleLabel_13;
	// UnityEngine.GameObject RegistrationNavigationcontroller::yourNumberObject
	GameObject_t1756533147 * ___yourNumberObject_14;
	// UnityEngine.GameObject RegistrationNavigationcontroller::verificationObject
	GameObject_t1756533147 * ___verificationObject_15;
	// UnityEngine.GameObject RegistrationNavigationcontroller::accountDetailsObject
	GameObject_t1756533147 * ___accountDetailsObject_16;
	// UnityEngine.GameObject RegistrationNavigationcontroller::registrationCompleteObject
	GameObject_t1756533147 * ___registrationCompleteObject_17;
	// NavigateForwardButton RegistrationNavigationcontroller::mainNavigateForwardButton
	NavigateForwardButton_t3075171100 * ___mainNavigateForwardButton_18;
	// NavigateBackwardButton RegistrationNavigationcontroller::mainNavigateBackwardButton
	NavigateBackwardButton_t1567151050 * ___mainNavigateBackwardButton_19;

public:
	inline static int32_t get_offset_of_activePos_8() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___activePos_8)); }
	inline Vector3_t2243707580  get_activePos_8() const { return ___activePos_8; }
	inline Vector3_t2243707580 * get_address_of_activePos_8() { return &___activePos_8; }
	inline void set_activePos_8(Vector3_t2243707580  value)
	{
		___activePos_8 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_9() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___leftInactivePos_9)); }
	inline Vector3_t2243707580  get_leftInactivePos_9() const { return ___leftInactivePos_9; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_9() { return &___leftInactivePos_9; }
	inline void set_leftInactivePos_9(Vector3_t2243707580  value)
	{
		___leftInactivePos_9 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_10() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___rightInactivePos_10)); }
	inline Vector3_t2243707580  get_rightInactivePos_10() const { return ___rightInactivePos_10; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_10() { return &___rightInactivePos_10; }
	inline void set_rightInactivePos_10(Vector3_t2243707580  value)
	{
		___rightInactivePos_10 = value;
	}

	inline static int32_t get_offset_of_startPage_11() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___startPage_11)); }
	inline GameObject_t1756533147 * get_startPage_11() const { return ___startPage_11; }
	inline GameObject_t1756533147 ** get_address_of_startPage_11() { return &___startPage_11; }
	inline void set_startPage_11(GameObject_t1756533147 * value)
	{
		___startPage_11 = value;
		Il2CppCodeGenWriteBarrier(&___startPage_11, value);
	}

	inline static int32_t get_offset_of_contentObject_12() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___contentObject_12)); }
	inline GameObject_t1756533147 * get_contentObject_12() const { return ___contentObject_12; }
	inline GameObject_t1756533147 ** get_address_of_contentObject_12() { return &___contentObject_12; }
	inline void set_contentObject_12(GameObject_t1756533147 * value)
	{
		___contentObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___contentObject_12, value);
	}

	inline static int32_t get_offset_of_titleLabel_13() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___titleLabel_13)); }
	inline UILabel_t1795115428 * get_titleLabel_13() const { return ___titleLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_13() { return &___titleLabel_13; }
	inline void set_titleLabel_13(UILabel_t1795115428 * value)
	{
		___titleLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_13, value);
	}

	inline static int32_t get_offset_of_yourNumberObject_14() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___yourNumberObject_14)); }
	inline GameObject_t1756533147 * get_yourNumberObject_14() const { return ___yourNumberObject_14; }
	inline GameObject_t1756533147 ** get_address_of_yourNumberObject_14() { return &___yourNumberObject_14; }
	inline void set_yourNumberObject_14(GameObject_t1756533147 * value)
	{
		___yourNumberObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___yourNumberObject_14, value);
	}

	inline static int32_t get_offset_of_verificationObject_15() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___verificationObject_15)); }
	inline GameObject_t1756533147 * get_verificationObject_15() const { return ___verificationObject_15; }
	inline GameObject_t1756533147 ** get_address_of_verificationObject_15() { return &___verificationObject_15; }
	inline void set_verificationObject_15(GameObject_t1756533147 * value)
	{
		___verificationObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___verificationObject_15, value);
	}

	inline static int32_t get_offset_of_accountDetailsObject_16() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___accountDetailsObject_16)); }
	inline GameObject_t1756533147 * get_accountDetailsObject_16() const { return ___accountDetailsObject_16; }
	inline GameObject_t1756533147 ** get_address_of_accountDetailsObject_16() { return &___accountDetailsObject_16; }
	inline void set_accountDetailsObject_16(GameObject_t1756533147 * value)
	{
		___accountDetailsObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___accountDetailsObject_16, value);
	}

	inline static int32_t get_offset_of_registrationCompleteObject_17() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___registrationCompleteObject_17)); }
	inline GameObject_t1756533147 * get_registrationCompleteObject_17() const { return ___registrationCompleteObject_17; }
	inline GameObject_t1756533147 ** get_address_of_registrationCompleteObject_17() { return &___registrationCompleteObject_17; }
	inline void set_registrationCompleteObject_17(GameObject_t1756533147 * value)
	{
		___registrationCompleteObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___registrationCompleteObject_17, value);
	}

	inline static int32_t get_offset_of_mainNavigateForwardButton_18() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___mainNavigateForwardButton_18)); }
	inline NavigateForwardButton_t3075171100 * get_mainNavigateForwardButton_18() const { return ___mainNavigateForwardButton_18; }
	inline NavigateForwardButton_t3075171100 ** get_address_of_mainNavigateForwardButton_18() { return &___mainNavigateForwardButton_18; }
	inline void set_mainNavigateForwardButton_18(NavigateForwardButton_t3075171100 * value)
	{
		___mainNavigateForwardButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateForwardButton_18, value);
	}

	inline static int32_t get_offset_of_mainNavigateBackwardButton_19() { return static_cast<int32_t>(offsetof(RegistrationNavigationcontroller_t539766269, ___mainNavigateBackwardButton_19)); }
	inline NavigateBackwardButton_t1567151050 * get_mainNavigateBackwardButton_19() const { return ___mainNavigateBackwardButton_19; }
	inline NavigateBackwardButton_t1567151050 ** get_address_of_mainNavigateBackwardButton_19() { return &___mainNavigateBackwardButton_19; }
	inline void set_mainNavigateBackwardButton_19(NavigateBackwardButton_t1567151050 * value)
	{
		___mainNavigateBackwardButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateBackwardButton_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
