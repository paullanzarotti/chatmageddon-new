﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.SettingsBindableAttribute
struct SettingsBindableAttribute_t2588031272;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.SettingsBindableAttribute::.ctor(System.Boolean)
extern "C"  void SettingsBindableAttribute__ctor_m3854332914 (SettingsBindableAttribute_t2588031272 * __this, bool ___bindable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.SettingsBindableAttribute::.cctor()
extern "C"  void SettingsBindableAttribute__cctor_m3175830806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.SettingsBindableAttribute::get_Bindable()
extern "C"  bool SettingsBindableAttribute_get_Bindable_m1194314699 (SettingsBindableAttribute_t2588031272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.SettingsBindableAttribute::GetHashCode()
extern "C"  int32_t SettingsBindableAttribute_GetHashCode_m1364968430 (SettingsBindableAttribute_t2588031272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.SettingsBindableAttribute::Equals(System.Object)
extern "C"  bool SettingsBindableAttribute_Equals_m750688338 (SettingsBindableAttribute_t2588031272 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
