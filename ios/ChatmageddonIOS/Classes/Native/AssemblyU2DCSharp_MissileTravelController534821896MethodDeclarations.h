﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileTravelController
struct MissileTravelController_t534821896;
// LaunchedMissile
struct LaunchedMissile_t1542515810;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"

// System.Void MissileTravelController::.ctor()
extern "C"  void MissileTravelController__ctor_m603021591 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::OnEnable()
extern "C"  void MissileTravelController_OnEnable_m1184852707 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::OnDisable()
extern "C"  void MissileTravelController_OnDisable_m2672356068 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::ResetContent()
extern "C"  void MissileTravelController_ResetContent_m1772682563 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::StartTrackingMissile(LaunchedMissile)
extern "C"  void MissileTravelController_StartTrackingMissile_m788215292 (MissileTravelController_t534821896 * __this, LaunchedMissile_t1542515810 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::StartTrack()
extern "C"  void MissileTravelController_StartTrack_m4160692008 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::CalculatorUpdated(System.TimeSpan)
extern "C"  void MissileTravelController_CalculatorUpdated_m1499217270 (MissileTravelController_t534821896 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::CalculatorHitZero()
extern "C"  void MissileTravelController_CalculatorHitZero_m3817769268 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::UpdateTravelState()
extern "C"  void MissileTravelController_UpdateTravelState_m1395921993 (MissileTravelController_t534821896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTravelController::SetColour(MissileTravelState)
extern "C"  void MissileTravelController_SetColour_m3693013688 (MissileTravelController_t534821896 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
