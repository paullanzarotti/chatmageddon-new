﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3346886819MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.ctor()
#define SortedDictionary_2__ctor_m3945712590(__this, method) ((  void (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2__ctor_m674714269_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define SortedDictionary_2__ctor_m205924584(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t1020118611 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m4130201156_gshared)(__this, ___comparer0, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m334798901(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1040024625_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1479525781(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3905150097_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m932118328(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t1020118611 *, KeyValuePair_2_t1422819240 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m166699524_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m440642884(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, KeyValuePair_2_t1422819240 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1516177424_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1442527239(__this, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1747061843_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2852380109(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, KeyValuePair_2_t1422819240 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2218591521_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Add_m3314336499(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1213244071_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.Contains(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m274252687(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m1437439451_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.GetEnumerator()
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2944720936(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m518365436_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.get_IsFixedSize()
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m574331936(__this, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1965119572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3973385699(__this, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1699106999_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.get_Keys()
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m1281508471(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2972533211_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.Remove(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m1722786178(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m283317966_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.get_Values()
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m3399077527(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m3009893083_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.get_Item(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m1800302605(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m3224431377_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m3007612846(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2135080018_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m599864353(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t1020118611 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m2962606917_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.ICollection.get_IsSynchronized()
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m2116519881(__this, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1251533845_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.ICollection.get_SyncRoot()
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2199144749(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m289765249_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IEnumerable.GetEnumerator()
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m399729758(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m356205410_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3601405403(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1313061119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_Count()
#define SortedDictionary_2_get_Count_m1668598480(__this, method) ((  int32_t (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_get_Count_m3476867889_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_Item(TKey)
#define SortedDictionary_2_get_Item_m222039922(__this, ___key0, method) ((  HashSet_1_t362681087 * (*) (SortedDictionary_2_t1020118611 *, int32_t, const MethodInfo*))SortedDictionary_2_get_Item_m3013028876_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::set_Item(TKey,TValue)
#define SortedDictionary_2_set_Item_m3839535969(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1020118611 *, int32_t, HashSet_1_t362681087 *, const MethodInfo*))SortedDictionary_2_set_Item_m3432483157_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_Keys()
#define SortedDictionary_2_get_Keys_m2116004613(__this, method) ((  KeyCollection_t1234264224 * (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_get_Keys_m2311581057_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::Add(TKey,TValue)
#define SortedDictionary_2_Add_m1294219748(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1020118611 *, int32_t, HashSet_1_t362681087 *, const MethodInfo*))SortedDictionary_2_Add_m3384498232_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::Clear()
#define SortedDictionary_2_Clear_m649027256(__this, method) ((  void (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_Clear_m2537365212_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::ContainsKey(TKey)
#define SortedDictionary_2_ContainsKey_m1501295101(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsKey_m2099208916_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::ContainsValue(TValue)
#define SortedDictionary_2_ContainsValue_m3498541624(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, HashSet_1_t362681087 *, const MethodInfo*))SortedDictionary_2_ContainsValue_m2660776548_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SortedDictionary_2_CopyTo_m1725284781(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t1020118611 *, KeyValuePair_2U5BU5D_t1132430521*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m1830145497_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m1723567759(__this, method) ((  Enumerator_t2388680111  (*) (SortedDictionary_2_t1020118611 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m369699555_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::Remove(TKey)
#define SortedDictionary_2_Remove_m2877828196(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, int32_t, const MethodInfo*))SortedDictionary_2_Remove_m2306315880_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::TryGetValue(TKey,TValue&)
#define SortedDictionary_2_TryGetValue_m807763237(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t1020118611 *, int32_t, HashSet_1_t362681087 **, const MethodInfo*))SortedDictionary_2_TryGetValue_m2911573889_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::ToKey(System.Object)
#define SortedDictionary_2_ToKey_m2845298471(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m4262124275_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::ToValue(System.Object)
#define SortedDictionary_2_ToValue_m1732655007(__this, ___value0, method) ((  HashSet_1_t362681087 * (*) (SortedDictionary_2_t1020118611 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m817036307_gshared)(__this, ___value0, method)
