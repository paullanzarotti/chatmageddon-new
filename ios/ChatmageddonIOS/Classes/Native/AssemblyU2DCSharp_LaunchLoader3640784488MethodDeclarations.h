﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchLoader
struct LaunchLoader_t3640784488;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchLoader::.ctor()
extern "C"  void LaunchLoader__ctor_m3845782999 (LaunchLoader_t3640784488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchLoader::Start()
extern "C"  void LaunchLoader_Start_m4131447379 (LaunchLoader_t3640784488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchLoader::Update()
extern "C"  void LaunchLoader_Update_m2972782854 (LaunchLoader_t3640784488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
