﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlockListItemUIScaler
struct  BlockListItemUIScaler_t2422342852  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite BlockListItemUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider BlockListItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UnityEngine.Transform BlockListItemUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_16;
	// UnityEngine.Transform BlockListItemUIScaler::nameLabel
	Transform_t3275118058 * ___nameLabel_17;
	// UnityEngine.Transform BlockListItemUIScaler::unblockButton
	Transform_t3275118058 * ___unblockButton_18;
	// UnityEngine.Transform BlockListItemUIScaler::actionLabel
	Transform_t3275118058 * ___actionLabel_19;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(BlockListItemUIScaler_t2422342852, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(BlockListItemUIScaler_t2422342852, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_profilePicture_16() { return static_cast<int32_t>(offsetof(BlockListItemUIScaler_t2422342852, ___profilePicture_16)); }
	inline Transform_t3275118058 * get_profilePicture_16() const { return ___profilePicture_16; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_16() { return &___profilePicture_16; }
	inline void set_profilePicture_16(Transform_t3275118058 * value)
	{
		___profilePicture_16 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_16, value);
	}

	inline static int32_t get_offset_of_nameLabel_17() { return static_cast<int32_t>(offsetof(BlockListItemUIScaler_t2422342852, ___nameLabel_17)); }
	inline Transform_t3275118058 * get_nameLabel_17() const { return ___nameLabel_17; }
	inline Transform_t3275118058 ** get_address_of_nameLabel_17() { return &___nameLabel_17; }
	inline void set_nameLabel_17(Transform_t3275118058 * value)
	{
		___nameLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_17, value);
	}

	inline static int32_t get_offset_of_unblockButton_18() { return static_cast<int32_t>(offsetof(BlockListItemUIScaler_t2422342852, ___unblockButton_18)); }
	inline Transform_t3275118058 * get_unblockButton_18() const { return ___unblockButton_18; }
	inline Transform_t3275118058 ** get_address_of_unblockButton_18() { return &___unblockButton_18; }
	inline void set_unblockButton_18(Transform_t3275118058 * value)
	{
		___unblockButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___unblockButton_18, value);
	}

	inline static int32_t get_offset_of_actionLabel_19() { return static_cast<int32_t>(offsetof(BlockListItemUIScaler_t2422342852, ___actionLabel_19)); }
	inline Transform_t3275118058 * get_actionLabel_19() const { return ___actionLabel_19; }
	inline Transform_t3275118058 ** get_address_of_actionLabel_19() { return &___actionLabel_19; }
	inline void set_actionLabel_19(Transform_t3275118058 * value)
	{
		___actionLabel_19 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
