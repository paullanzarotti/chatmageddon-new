﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3212009085MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3545975246(__this, ___dic0, method) ((  void (*) (Enumerator_t885240877 *, SortedDictionary_2_t1020118611 *, const MethodInfo*))Enumerator__ctor_m245074370_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1244986754(__this, method) ((  Il2CppObject * (*) (Enumerator_t885240877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m451777246_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3439861364(__this, method) ((  void (*) (Enumerator_t885240877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1175503104_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_Current()
#define Enumerator_get_Current_m2242671775(__this, method) ((  int32_t (*) (Enumerator_t885240877 *, const MethodInfo*))Enumerator_get_Current_m4004123576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::MoveNext()
#define Enumerator_MoveNext_m1213845635(__this, method) ((  bool (*) (Enumerator_t885240877 *, const MethodInfo*))Enumerator_MoveNext_m17822024_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::Dispose()
#define Enumerator_Dispose_m2139339183(__this, method) ((  void (*) (Enumerator_t885240877 *, const MethodInfo*))Enumerator_Dispose_m4250516907_gshared)(__this, method)
