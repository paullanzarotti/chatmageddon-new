﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SFXSource/<HideObjectCheck>c__Iterator0
struct U3CHideObjectCheckU3Ec__Iterator0_t3897528497;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SFXSource/<HideObjectCheck>c__Iterator0::.ctor()
extern "C"  void U3CHideObjectCheckU3Ec__Iterator0__ctor_m2063440258 (U3CHideObjectCheckU3Ec__Iterator0_t3897528497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SFXSource/<HideObjectCheck>c__Iterator0::MoveNext()
extern "C"  bool U3CHideObjectCheckU3Ec__Iterator0_MoveNext_m3042031298 (U3CHideObjectCheckU3Ec__Iterator0_t3897528497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SFXSource/<HideObjectCheck>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHideObjectCheckU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3313859020 (U3CHideObjectCheckU3Ec__Iterator0_t3897528497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SFXSource/<HideObjectCheck>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHideObjectCheckU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1164557684 (U3CHideObjectCheckU3Ec__Iterator0_t3897528497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXSource/<HideObjectCheck>c__Iterator0::Dispose()
extern "C"  void U3CHideObjectCheckU3Ec__Iterator0_Dispose_m1184787347 (U3CHideObjectCheckU3Ec__Iterator0_t3897528497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXSource/<HideObjectCheck>c__Iterator0::Reset()
extern "C"  void U3CHideObjectCheckU3Ec__Iterator0_Reset_m3665342901 (U3CHideObjectCheckU3Ec__Iterator0_t3897528497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
