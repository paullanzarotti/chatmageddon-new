﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContactsPhoneNumberManager
struct ContactsPhoneNumberManager_t4168190569;
// CountryCodeData
struct CountryCodeData_t1765037603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CountryCodeData1765037603.h"

// System.Void ContactsPhoneNumberManager::.ctor()
extern "C"  void ContactsPhoneNumberManager__ctor_m2838163608 (ContactsPhoneNumberManager_t4168190569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsPhoneNumberManager::Start()
extern "C"  void ContactsPhoneNumberManager_Start_m2869540648 (ContactsPhoneNumberManager_t4168190569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsPhoneNumberManager::SetSelectedCountryCode(CountryCodeData)
extern "C"  void ContactsPhoneNumberManager_SetSelectedCountryCode_m2845022935 (ContactsPhoneNumberManager_t4168190569 * __this, CountryCodeData_t1765037603 * ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
