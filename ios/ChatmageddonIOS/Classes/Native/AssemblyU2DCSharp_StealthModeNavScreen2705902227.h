﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TimeNumberPicker
struct TimeNumberPicker_t1112809442;
// SliderController
struct SliderController_t2812089311;
// UILabel
struct UILabel_t1795115428;
// StealthModeResetTimer
struct StealthModeResetTimer_t2296140228;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StealthModeNavScreen
struct  StealthModeNavScreen_t2705902227  : public NavigationScreen_t2333230110
{
public:
	// TimeNumberPicker StealthModeNavScreen::hourPicker
	TimeNumberPicker_t1112809442 * ___hourPicker_3;
	// TimeNumberPicker StealthModeNavScreen::minPicker
	TimeNumberPicker_t1112809442 * ___minPicker_4;
	// SliderController StealthModeNavScreen::durationSlider
	SliderController_t2812089311 * ___durationSlider_5;
	// UILabel StealthModeNavScreen::endTimeLabel
	UILabel_t1795115428 * ___endTimeLabel_6;
	// StealthModeResetTimer StealthModeNavScreen::resetTimer
	StealthModeResetTimer_t2296140228 * ___resetTimer_7;
	// System.Boolean StealthModeNavScreen::UIclosing
	bool ___UIclosing_8;

public:
	inline static int32_t get_offset_of_hourPicker_3() { return static_cast<int32_t>(offsetof(StealthModeNavScreen_t2705902227, ___hourPicker_3)); }
	inline TimeNumberPicker_t1112809442 * get_hourPicker_3() const { return ___hourPicker_3; }
	inline TimeNumberPicker_t1112809442 ** get_address_of_hourPicker_3() { return &___hourPicker_3; }
	inline void set_hourPicker_3(TimeNumberPicker_t1112809442 * value)
	{
		___hourPicker_3 = value;
		Il2CppCodeGenWriteBarrier(&___hourPicker_3, value);
	}

	inline static int32_t get_offset_of_minPicker_4() { return static_cast<int32_t>(offsetof(StealthModeNavScreen_t2705902227, ___minPicker_4)); }
	inline TimeNumberPicker_t1112809442 * get_minPicker_4() const { return ___minPicker_4; }
	inline TimeNumberPicker_t1112809442 ** get_address_of_minPicker_4() { return &___minPicker_4; }
	inline void set_minPicker_4(TimeNumberPicker_t1112809442 * value)
	{
		___minPicker_4 = value;
		Il2CppCodeGenWriteBarrier(&___minPicker_4, value);
	}

	inline static int32_t get_offset_of_durationSlider_5() { return static_cast<int32_t>(offsetof(StealthModeNavScreen_t2705902227, ___durationSlider_5)); }
	inline SliderController_t2812089311 * get_durationSlider_5() const { return ___durationSlider_5; }
	inline SliderController_t2812089311 ** get_address_of_durationSlider_5() { return &___durationSlider_5; }
	inline void set_durationSlider_5(SliderController_t2812089311 * value)
	{
		___durationSlider_5 = value;
		Il2CppCodeGenWriteBarrier(&___durationSlider_5, value);
	}

	inline static int32_t get_offset_of_endTimeLabel_6() { return static_cast<int32_t>(offsetof(StealthModeNavScreen_t2705902227, ___endTimeLabel_6)); }
	inline UILabel_t1795115428 * get_endTimeLabel_6() const { return ___endTimeLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_endTimeLabel_6() { return &___endTimeLabel_6; }
	inline void set_endTimeLabel_6(UILabel_t1795115428 * value)
	{
		___endTimeLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___endTimeLabel_6, value);
	}

	inline static int32_t get_offset_of_resetTimer_7() { return static_cast<int32_t>(offsetof(StealthModeNavScreen_t2705902227, ___resetTimer_7)); }
	inline StealthModeResetTimer_t2296140228 * get_resetTimer_7() const { return ___resetTimer_7; }
	inline StealthModeResetTimer_t2296140228 ** get_address_of_resetTimer_7() { return &___resetTimer_7; }
	inline void set_resetTimer_7(StealthModeResetTimer_t2296140228 * value)
	{
		___resetTimer_7 = value;
		Il2CppCodeGenWriteBarrier(&___resetTimer_7, value);
	}

	inline static int32_t get_offset_of_UIclosing_8() { return static_cast<int32_t>(offsetof(StealthModeNavScreen_t2705902227, ___UIclosing_8)); }
	inline bool get_UIclosing_8() const { return ___UIclosing_8; }
	inline bool* get_address_of_UIclosing_8() { return &___UIclosing_8; }
	inline void set_UIclosing_8(bool value)
	{
		___UIclosing_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
