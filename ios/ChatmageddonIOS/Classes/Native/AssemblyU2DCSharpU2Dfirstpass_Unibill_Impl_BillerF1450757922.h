﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Uniject.IResourceLoader
struct IResourceLoader_t2860850570;
// Uniject.ILogger
struct ILogger_t2858843691;
// Uniject.IStorage
struct IStorage_t1347868490;
// Unibill.Impl.IRawBillingPlatformProvider
struct IRawBillingPlatformProvider_t643465426;
// Uniject.IUtil
struct IUtil_t2188430191;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.CurrencyManager
struct CurrencyManager_t3009003778;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Unibill.Impl.HelpCentre
struct HelpCentre_t214342444;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// System.Func`2<PurchasableItem,System.Boolean>
struct Func_2_t485347032;
// System.Func`2<PurchasableItem,unibill.Dummy.Product>
struct Func_2_t2113113021;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.BillerFactory
struct  BillerFactory_t1450757922  : public Il2CppObject
{
public:
	// Uniject.IResourceLoader Unibill.Impl.BillerFactory::loader
	Il2CppObject * ___loader_0;
	// Uniject.ILogger Unibill.Impl.BillerFactory::logger
	Il2CppObject * ___logger_1;
	// Uniject.IStorage Unibill.Impl.BillerFactory::storage
	Il2CppObject * ___storage_2;
	// Unibill.Impl.IRawBillingPlatformProvider Unibill.Impl.BillerFactory::platformProvider
	Il2CppObject * ___platformProvider_3;
	// Uniject.IUtil Unibill.Impl.BillerFactory::util
	Il2CppObject * ___util_4;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.BillerFactory::config
	UnibillConfiguration_t2915611853 * ___config_5;
	// Unibill.Impl.CurrencyManager Unibill.Impl.BillerFactory::_currencyManager
	CurrencyManager_t3009003778 * ____currencyManager_6;
	// TransactionDatabase Unibill.Impl.BillerFactory::_tDb
	TransactionDatabase_t201476183 * ____tDb_7;
	// Unibill.Impl.HelpCentre Unibill.Impl.BillerFactory::_helpCentre
	HelpCentre_t214342444 * ____helpCentre_8;
	// Unibill.Impl.ProductIdRemapper Unibill.Impl.BillerFactory::_remapper
	ProductIdRemapper_t3313438456 * ____remapper_9;

public:
	inline static int32_t get_offset_of_loader_0() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ___loader_0)); }
	inline Il2CppObject * get_loader_0() const { return ___loader_0; }
	inline Il2CppObject ** get_address_of_loader_0() { return &___loader_0; }
	inline void set_loader_0(Il2CppObject * value)
	{
		___loader_0 = value;
		Il2CppCodeGenWriteBarrier(&___loader_0, value);
	}

	inline static int32_t get_offset_of_logger_1() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ___logger_1)); }
	inline Il2CppObject * get_logger_1() const { return ___logger_1; }
	inline Il2CppObject ** get_address_of_logger_1() { return &___logger_1; }
	inline void set_logger_1(Il2CppObject * value)
	{
		___logger_1 = value;
		Il2CppCodeGenWriteBarrier(&___logger_1, value);
	}

	inline static int32_t get_offset_of_storage_2() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ___storage_2)); }
	inline Il2CppObject * get_storage_2() const { return ___storage_2; }
	inline Il2CppObject ** get_address_of_storage_2() { return &___storage_2; }
	inline void set_storage_2(Il2CppObject * value)
	{
		___storage_2 = value;
		Il2CppCodeGenWriteBarrier(&___storage_2, value);
	}

	inline static int32_t get_offset_of_platformProvider_3() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ___platformProvider_3)); }
	inline Il2CppObject * get_platformProvider_3() const { return ___platformProvider_3; }
	inline Il2CppObject ** get_address_of_platformProvider_3() { return &___platformProvider_3; }
	inline void set_platformProvider_3(Il2CppObject * value)
	{
		___platformProvider_3 = value;
		Il2CppCodeGenWriteBarrier(&___platformProvider_3, value);
	}

	inline static int32_t get_offset_of_util_4() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ___util_4)); }
	inline Il2CppObject * get_util_4() const { return ___util_4; }
	inline Il2CppObject ** get_address_of_util_4() { return &___util_4; }
	inline void set_util_4(Il2CppObject * value)
	{
		___util_4 = value;
		Il2CppCodeGenWriteBarrier(&___util_4, value);
	}

	inline static int32_t get_offset_of_config_5() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ___config_5)); }
	inline UnibillConfiguration_t2915611853 * get_config_5() const { return ___config_5; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_config_5() { return &___config_5; }
	inline void set_config_5(UnibillConfiguration_t2915611853 * value)
	{
		___config_5 = value;
		Il2CppCodeGenWriteBarrier(&___config_5, value);
	}

	inline static int32_t get_offset_of__currencyManager_6() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ____currencyManager_6)); }
	inline CurrencyManager_t3009003778 * get__currencyManager_6() const { return ____currencyManager_6; }
	inline CurrencyManager_t3009003778 ** get_address_of__currencyManager_6() { return &____currencyManager_6; }
	inline void set__currencyManager_6(CurrencyManager_t3009003778 * value)
	{
		____currencyManager_6 = value;
		Il2CppCodeGenWriteBarrier(&____currencyManager_6, value);
	}

	inline static int32_t get_offset_of__tDb_7() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ____tDb_7)); }
	inline TransactionDatabase_t201476183 * get__tDb_7() const { return ____tDb_7; }
	inline TransactionDatabase_t201476183 ** get_address_of__tDb_7() { return &____tDb_7; }
	inline void set__tDb_7(TransactionDatabase_t201476183 * value)
	{
		____tDb_7 = value;
		Il2CppCodeGenWriteBarrier(&____tDb_7, value);
	}

	inline static int32_t get_offset_of__helpCentre_8() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ____helpCentre_8)); }
	inline HelpCentre_t214342444 * get__helpCentre_8() const { return ____helpCentre_8; }
	inline HelpCentre_t214342444 ** get_address_of__helpCentre_8() { return &____helpCentre_8; }
	inline void set__helpCentre_8(HelpCentre_t214342444 * value)
	{
		____helpCentre_8 = value;
		Il2CppCodeGenWriteBarrier(&____helpCentre_8, value);
	}

	inline static int32_t get_offset_of__remapper_9() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922, ____remapper_9)); }
	inline ProductIdRemapper_t3313438456 * get__remapper_9() const { return ____remapper_9; }
	inline ProductIdRemapper_t3313438456 ** get_address_of__remapper_9() { return &____remapper_9; }
	inline void set__remapper_9(ProductIdRemapper_t3313438456 * value)
	{
		____remapper_9 = value;
		Il2CppCodeGenWriteBarrier(&____remapper_9, value);
	}
};

struct BillerFactory_t1450757922_StaticFields
{
public:
	// System.Func`2<PurchasableItem,System.Boolean> Unibill.Impl.BillerFactory::<>f__am$cache0
	Func_2_t485347032 * ___U3CU3Ef__amU24cache0_10;
	// System.Func`2<PurchasableItem,unibill.Dummy.Product> Unibill.Impl.BillerFactory::<>f__am$cache1
	Func_2_t2113113021 * ___U3CU3Ef__amU24cache1_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Func_2_t485347032 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Func_2_t485347032 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Func_2_t485347032 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_11() { return static_cast<int32_t>(offsetof(BillerFactory_t1450757922_StaticFields, ___U3CU3Ef__amU24cache1_11)); }
	inline Func_2_t2113113021 * get_U3CU3Ef__amU24cache1_11() const { return ___U3CU3Ef__amU24cache1_11; }
	inline Func_2_t2113113021 ** get_address_of_U3CU3Ef__amU24cache1_11() { return &___U3CU3Ef__amU24cache1_11; }
	inline void set_U3CU3Ef__amU24cache1_11(Func_2_t2113113021 * value)
	{
		___U3CU3Ef__amU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
