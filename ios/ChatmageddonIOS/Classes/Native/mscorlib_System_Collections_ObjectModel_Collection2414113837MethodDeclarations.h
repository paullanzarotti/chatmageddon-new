﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<StatusNavScreen>
struct Collection_1_t2414113837;
// System.Collections.Generic.IList`1<StatusNavScreen>
struct IList_1_t3413309684;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// StatusNavScreen[]
struct StatusNavScreenU5BU5D_t3698178810;
// System.Collections.Generic.IEnumerator`1<StatusNavScreen>
struct IEnumerator_1_t347892910;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"

// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3001975000_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3001975000(__this, method) ((  void (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1__ctor_m3001975000_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m2804963375_gshared (Collection_1_t2414113837 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m2804963375(__this, ___list0, method) ((  void (*) (Collection_1_t2414113837 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m2804963375_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m350792705_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m350792705(__this, method) ((  bool (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m350792705_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1857168672_gshared (Collection_1_t2414113837 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1857168672(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2414113837 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1857168672_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m733338309_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m733338309(__this, method) ((  Il2CppObject * (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m733338309_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1096201608_gshared (Collection_1_t2414113837 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1096201608(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2414113837 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1096201608_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1276450262_gshared (Collection_1_t2414113837 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1276450262(__this, ___value0, method) ((  bool (*) (Collection_1_t2414113837 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1276450262_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1955776558_gshared (Collection_1_t2414113837 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1955776558(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2414113837 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1955776558_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3216331413_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3216331413(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3216331413_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2120429213_gshared (Collection_1_t2414113837 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2120429213(__this, ___value0, method) ((  void (*) (Collection_1_t2414113837 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2120429213_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m186323648_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m186323648(__this, method) ((  bool (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m186323648_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3885794528_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3885794528(__this, method) ((  Il2CppObject * (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3885794528_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m780693145_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m780693145(__this, method) ((  bool (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m780693145_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2738250188_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2738250188(__this, method) ((  bool (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2738250188_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m990113657_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m990113657(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m990113657_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m613406226_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m613406226(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m613406226_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m602317865_gshared (Collection_1_t2414113837 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m602317865(__this, ___item0, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_Add_m602317865_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m3927281285_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3927281285(__this, method) ((  void (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_Clear_m3927281285_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m303807483_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m303807483(__this, method) ((  void (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_ClearItems_m303807483_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m1671578135_gshared (Collection_1_t2414113837 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1671578135(__this, ___item0, method) ((  bool (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_Contains_m1671578135_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4117036705_gshared (Collection_1_t2414113837 * __this, StatusNavScreenU5BU5D_t3698178810* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4117036705(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2414113837 *, StatusNavScreenU5BU5D_t3698178810*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4117036705_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<StatusNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3684663940_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3684663940(__this, method) ((  Il2CppObject* (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_GetEnumerator_m3684663940_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<StatusNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m244931845_gshared (Collection_1_t2414113837 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m244931845(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m244931845_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2271280946_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2271280946(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m2271280946_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1778170487_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1778170487(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1778170487_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m1764574348_gshared (Collection_1_t2414113837 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1764574348(__this, ___item0, method) ((  bool (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_Remove_m1764574348_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3961550878_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3961550878(__this, ___index0, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3961550878_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m894465732_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m894465732(__this, ___index0, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m894465732_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<StatusNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3886882440_gshared (Collection_1_t2414113837 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3886882440(__this, method) ((  int32_t (*) (Collection_1_t2414113837 *, const MethodInfo*))Collection_1_get_Count_m3886882440_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<StatusNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3038480032_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3038480032(__this, ___index0, method) ((  int32_t (*) (Collection_1_t2414113837 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3038480032_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3746823561_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3746823561(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3746823561_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2063823362_gshared (Collection_1_t2414113837 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2063823362(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2414113837 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m2063823362_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1799501131_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1799501131(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1799501131_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<StatusNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3775743691_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3775743691(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3775743691_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<StatusNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m100119511_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m100119511(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m100119511_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2117284919_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2117284919(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2117284919_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<StatusNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1301608966_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1301608966(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1301608966_gshared)(__this /* static, unused */, ___list0, method)
