﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsBuffer/SortMarkersDelegate
struct SortMarkersDelegate_t3426682085;
// OnlineMaps
struct OnlineMaps_t1893290312;
// OnlineMapsVector2i
struct OnlineMapsVector2i_t2180897250;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Collections.Generic.List`1<OnlineMapsTile>
struct List_1_t3685418368;
// System.Collections.Generic.Dictionary`2<System.Int32,OnlineMapsBufferZoom>
struct Dictionary_2_t1080362012;
// System.Func`2<OnlineMapsMarker,System.Single>
struct Func_2_t2022702331;
// System.Predicate`1<OnlineMapsTile>
struct Predicate_1_t2759267351;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_OnlineMapsRedrawType1698729245.h"
#include "AssemblyU2DCSharp_OnlineMapsBufferStatus201731616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuffer
struct  OnlineMapsBuffer_t2643049474  : public Il2CppObject
{
public:
	// OnlineMaps OnlineMapsBuffer::api
	OnlineMaps_t1893290312 * ___api_1;
	// System.Int32 OnlineMapsBuffer::apiZoom
	int32_t ___apiZoom_2;
	// OnlineMapsVector2i OnlineMapsBuffer::bufferPosition
	OnlineMapsVector2i_t2180897250 * ___bufferPosition_3;
	// UnityEngine.Color[] OnlineMapsBuffer::frontBuffer
	ColorU5BU5D_t672350442* ___frontBuffer_4;
	// System.Boolean OnlineMapsBuffer::generateSmartBuffer
	bool ___generateSmartBuffer_5;
	// System.Int32 OnlineMapsBuffer::height
	int32_t ___height_6;
	// System.Collections.Generic.List`1<OnlineMapsTile> OnlineMapsBuffer::newTiles
	List_1_t3685418368 * ___newTiles_7;
	// OnlineMapsRedrawType OnlineMapsBuffer::redrawType
	int32_t ___redrawType_8;
	// OnlineMapsBufferStatus OnlineMapsBuffer::status
	int32_t ___status_9;
	// UnityEngine.Color[] OnlineMapsBuffer::smartBuffer
	ColorU5BU5D_t672350442* ___smartBuffer_10;
	// System.Boolean OnlineMapsBuffer::updateBackBuffer
	bool ___updateBackBuffer_11;
	// System.Int32 OnlineMapsBuffer::width
	int32_t ___width_12;
	// UnityEngine.Color[] OnlineMapsBuffer::backBuffer
	ColorU5BU5D_t672350442* ___backBuffer_13;
	// System.Int32 OnlineMapsBuffer::bufferZoom
	int32_t ___bufferZoom_14;
	// System.Boolean OnlineMapsBuffer::disposed
	bool ___disposed_15;
	// OnlineMapsVector2i OnlineMapsBuffer::frontBufferPosition
	OnlineMapsVector2i_t2180897250 * ___frontBufferPosition_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,OnlineMapsBufferZoom> OnlineMapsBuffer::zooms
	Dictionary_2_t1080362012 * ___zooms_17;
	// System.Boolean OnlineMapsBuffer::needUnloadTiles
	bool ___needUnloadTiles_18;
	// System.Double OnlineMapsBuffer::apiLongitude
	double ___apiLongitude_19;
	// System.Double OnlineMapsBuffer::apiLatitude
	double ___apiLatitude_20;

public:
	inline static int32_t get_offset_of_api_1() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___api_1)); }
	inline OnlineMaps_t1893290312 * get_api_1() const { return ___api_1; }
	inline OnlineMaps_t1893290312 ** get_address_of_api_1() { return &___api_1; }
	inline void set_api_1(OnlineMaps_t1893290312 * value)
	{
		___api_1 = value;
		Il2CppCodeGenWriteBarrier(&___api_1, value);
	}

	inline static int32_t get_offset_of_apiZoom_2() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___apiZoom_2)); }
	inline int32_t get_apiZoom_2() const { return ___apiZoom_2; }
	inline int32_t* get_address_of_apiZoom_2() { return &___apiZoom_2; }
	inline void set_apiZoom_2(int32_t value)
	{
		___apiZoom_2 = value;
	}

	inline static int32_t get_offset_of_bufferPosition_3() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___bufferPosition_3)); }
	inline OnlineMapsVector2i_t2180897250 * get_bufferPosition_3() const { return ___bufferPosition_3; }
	inline OnlineMapsVector2i_t2180897250 ** get_address_of_bufferPosition_3() { return &___bufferPosition_3; }
	inline void set_bufferPosition_3(OnlineMapsVector2i_t2180897250 * value)
	{
		___bufferPosition_3 = value;
		Il2CppCodeGenWriteBarrier(&___bufferPosition_3, value);
	}

	inline static int32_t get_offset_of_frontBuffer_4() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___frontBuffer_4)); }
	inline ColorU5BU5D_t672350442* get_frontBuffer_4() const { return ___frontBuffer_4; }
	inline ColorU5BU5D_t672350442** get_address_of_frontBuffer_4() { return &___frontBuffer_4; }
	inline void set_frontBuffer_4(ColorU5BU5D_t672350442* value)
	{
		___frontBuffer_4 = value;
		Il2CppCodeGenWriteBarrier(&___frontBuffer_4, value);
	}

	inline static int32_t get_offset_of_generateSmartBuffer_5() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___generateSmartBuffer_5)); }
	inline bool get_generateSmartBuffer_5() const { return ___generateSmartBuffer_5; }
	inline bool* get_address_of_generateSmartBuffer_5() { return &___generateSmartBuffer_5; }
	inline void set_generateSmartBuffer_5(bool value)
	{
		___generateSmartBuffer_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___height_6)); }
	inline int32_t get_height_6() const { return ___height_6; }
	inline int32_t* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(int32_t value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_newTiles_7() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___newTiles_7)); }
	inline List_1_t3685418368 * get_newTiles_7() const { return ___newTiles_7; }
	inline List_1_t3685418368 ** get_address_of_newTiles_7() { return &___newTiles_7; }
	inline void set_newTiles_7(List_1_t3685418368 * value)
	{
		___newTiles_7 = value;
		Il2CppCodeGenWriteBarrier(&___newTiles_7, value);
	}

	inline static int32_t get_offset_of_redrawType_8() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___redrawType_8)); }
	inline int32_t get_redrawType_8() const { return ___redrawType_8; }
	inline int32_t* get_address_of_redrawType_8() { return &___redrawType_8; }
	inline void set_redrawType_8(int32_t value)
	{
		___redrawType_8 = value;
	}

	inline static int32_t get_offset_of_status_9() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___status_9)); }
	inline int32_t get_status_9() const { return ___status_9; }
	inline int32_t* get_address_of_status_9() { return &___status_9; }
	inline void set_status_9(int32_t value)
	{
		___status_9 = value;
	}

	inline static int32_t get_offset_of_smartBuffer_10() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___smartBuffer_10)); }
	inline ColorU5BU5D_t672350442* get_smartBuffer_10() const { return ___smartBuffer_10; }
	inline ColorU5BU5D_t672350442** get_address_of_smartBuffer_10() { return &___smartBuffer_10; }
	inline void set_smartBuffer_10(ColorU5BU5D_t672350442* value)
	{
		___smartBuffer_10 = value;
		Il2CppCodeGenWriteBarrier(&___smartBuffer_10, value);
	}

	inline static int32_t get_offset_of_updateBackBuffer_11() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___updateBackBuffer_11)); }
	inline bool get_updateBackBuffer_11() const { return ___updateBackBuffer_11; }
	inline bool* get_address_of_updateBackBuffer_11() { return &___updateBackBuffer_11; }
	inline void set_updateBackBuffer_11(bool value)
	{
		___updateBackBuffer_11 = value;
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___width_12)); }
	inline int32_t get_width_12() const { return ___width_12; }
	inline int32_t* get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(int32_t value)
	{
		___width_12 = value;
	}

	inline static int32_t get_offset_of_backBuffer_13() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___backBuffer_13)); }
	inline ColorU5BU5D_t672350442* get_backBuffer_13() const { return ___backBuffer_13; }
	inline ColorU5BU5D_t672350442** get_address_of_backBuffer_13() { return &___backBuffer_13; }
	inline void set_backBuffer_13(ColorU5BU5D_t672350442* value)
	{
		___backBuffer_13 = value;
		Il2CppCodeGenWriteBarrier(&___backBuffer_13, value);
	}

	inline static int32_t get_offset_of_bufferZoom_14() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___bufferZoom_14)); }
	inline int32_t get_bufferZoom_14() const { return ___bufferZoom_14; }
	inline int32_t* get_address_of_bufferZoom_14() { return &___bufferZoom_14; }
	inline void set_bufferZoom_14(int32_t value)
	{
		___bufferZoom_14 = value;
	}

	inline static int32_t get_offset_of_disposed_15() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___disposed_15)); }
	inline bool get_disposed_15() const { return ___disposed_15; }
	inline bool* get_address_of_disposed_15() { return &___disposed_15; }
	inline void set_disposed_15(bool value)
	{
		___disposed_15 = value;
	}

	inline static int32_t get_offset_of_frontBufferPosition_16() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___frontBufferPosition_16)); }
	inline OnlineMapsVector2i_t2180897250 * get_frontBufferPosition_16() const { return ___frontBufferPosition_16; }
	inline OnlineMapsVector2i_t2180897250 ** get_address_of_frontBufferPosition_16() { return &___frontBufferPosition_16; }
	inline void set_frontBufferPosition_16(OnlineMapsVector2i_t2180897250 * value)
	{
		___frontBufferPosition_16 = value;
		Il2CppCodeGenWriteBarrier(&___frontBufferPosition_16, value);
	}

	inline static int32_t get_offset_of_zooms_17() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___zooms_17)); }
	inline Dictionary_2_t1080362012 * get_zooms_17() const { return ___zooms_17; }
	inline Dictionary_2_t1080362012 ** get_address_of_zooms_17() { return &___zooms_17; }
	inline void set_zooms_17(Dictionary_2_t1080362012 * value)
	{
		___zooms_17 = value;
		Il2CppCodeGenWriteBarrier(&___zooms_17, value);
	}

	inline static int32_t get_offset_of_needUnloadTiles_18() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___needUnloadTiles_18)); }
	inline bool get_needUnloadTiles_18() const { return ___needUnloadTiles_18; }
	inline bool* get_address_of_needUnloadTiles_18() { return &___needUnloadTiles_18; }
	inline void set_needUnloadTiles_18(bool value)
	{
		___needUnloadTiles_18 = value;
	}

	inline static int32_t get_offset_of_apiLongitude_19() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___apiLongitude_19)); }
	inline double get_apiLongitude_19() const { return ___apiLongitude_19; }
	inline double* get_address_of_apiLongitude_19() { return &___apiLongitude_19; }
	inline void set_apiLongitude_19(double value)
	{
		___apiLongitude_19 = value;
	}

	inline static int32_t get_offset_of_apiLatitude_20() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474, ___apiLatitude_20)); }
	inline double get_apiLatitude_20() const { return ___apiLatitude_20; }
	inline double* get_address_of_apiLatitude_20() { return &___apiLatitude_20; }
	inline void set_apiLatitude_20(double value)
	{
		___apiLatitude_20 = value;
	}
};

struct OnlineMapsBuffer_t2643049474_StaticFields
{
public:
	// OnlineMapsBuffer/SortMarkersDelegate OnlineMapsBuffer::OnSortMarker
	SortMarkersDelegate_t3426682085 * ___OnSortMarker_0;
	// System.Func`2<OnlineMapsMarker,System.Single> OnlineMapsBuffer::<>f__am$cache0
	Func_2_t2022702331 * ___U3CU3Ef__amU24cache0_21;
	// System.Predicate`1<OnlineMapsTile> OnlineMapsBuffer::<>f__am$cache1
	Predicate_1_t2759267351 * ___U3CU3Ef__amU24cache1_22;
	// System.Predicate`1<OnlineMapsTile> OnlineMapsBuffer::<>f__am$cache2
	Predicate_1_t2759267351 * ___U3CU3Ef__amU24cache2_23;

public:
	inline static int32_t get_offset_of_OnSortMarker_0() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474_StaticFields, ___OnSortMarker_0)); }
	inline SortMarkersDelegate_t3426682085 * get_OnSortMarker_0() const { return ___OnSortMarker_0; }
	inline SortMarkersDelegate_t3426682085 ** get_address_of_OnSortMarker_0() { return &___OnSortMarker_0; }
	inline void set_OnSortMarker_0(SortMarkersDelegate_t3426682085 * value)
	{
		___OnSortMarker_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnSortMarker_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_21() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474_StaticFields, ___U3CU3Ef__amU24cache0_21)); }
	inline Func_2_t2022702331 * get_U3CU3Ef__amU24cache0_21() const { return ___U3CU3Ef__amU24cache0_21; }
	inline Func_2_t2022702331 ** get_address_of_U3CU3Ef__amU24cache0_21() { return &___U3CU3Ef__amU24cache0_21; }
	inline void set_U3CU3Ef__amU24cache0_21(Func_2_t2022702331 * value)
	{
		___U3CU3Ef__amU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_22() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474_StaticFields, ___U3CU3Ef__amU24cache1_22)); }
	inline Predicate_1_t2759267351 * get_U3CU3Ef__amU24cache1_22() const { return ___U3CU3Ef__amU24cache1_22; }
	inline Predicate_1_t2759267351 ** get_address_of_U3CU3Ef__amU24cache1_22() { return &___U3CU3Ef__amU24cache1_22; }
	inline void set_U3CU3Ef__amU24cache1_22(Predicate_1_t2759267351 * value)
	{
		___U3CU3Ef__amU24cache1_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_23() { return static_cast<int32_t>(offsetof(OnlineMapsBuffer_t2643049474_StaticFields, ___U3CU3Ef__amU24cache2_23)); }
	inline Predicate_1_t2759267351 * get_U3CU3Ef__amU24cache2_23() const { return ___U3CU3Ef__amU24cache2_23; }
	inline Predicate_1_t2759267351 ** get_address_of_U3CU3Ef__amU24cache2_23() { return &___U3CU3Ef__amU24cache2_23; }
	inline void set_U3CU3Ef__amU24cache2_23(Predicate_1_t2759267351 * value)
	{
		___U3CU3Ef__amU24cache2_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
