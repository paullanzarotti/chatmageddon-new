﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIAnimation/OnAnimationUpdated
struct OnAnimationUpdated_t4231811366;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void NGUIAnimation/OnAnimationUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAnimationUpdated__ctor_m4286234729 (OnAnimationUpdated_t4231811366 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationUpdated::Invoke()
extern "C"  void OnAnimationUpdated_Invoke_m3439139721 (OnAnimationUpdated_t4231811366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NGUIAnimation/OnAnimationUpdated::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAnimationUpdated_BeginInvoke_m892342868 (OnAnimationUpdated_t4231811366 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void OnAnimationUpdated_EndInvoke_m4270043827 (OnAnimationUpdated_t4231811366 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
