﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsLocationService
struct OnlineMapsLocationService_t21331090;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2045506961;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// OnlineMaps
struct OnlineMaps_t1893290312;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsLocationServiceMarkerT2500453164.h"
#include "AssemblyU2DCSharp_OnlineMapsAlign3858887827.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsLocationService
struct  OnlineMapsLocationService_t21331090  : public MonoBehaviour_t1158329972
{
public:
	// System.Action`1<System.Single> OnlineMapsLocationService::OnCompassChanged
	Action_1_t1878309314 * ___OnCompassChanged_3;
	// System.Action`1<UnityEngine.Vector2> OnlineMapsLocationService::OnLocationChanged
	Action_1_t2045506961 * ___OnLocationChanged_4;
	// System.Boolean OnlineMapsLocationService::autoStopUpdateOnInput
	bool ___autoStopUpdateOnInput_5;
	// System.Boolean OnlineMapsLocationService::createMarkerInUserPosition
	bool ___createMarkerInUserPosition_6;
	// System.Single OnlineMapsLocationService::desiredAccuracy
	float ___desiredAccuracy_7;
	// System.Single OnlineMapsLocationService::emulatorCompass
	float ___emulatorCompass_8;
	// UnityEngine.Vector2 OnlineMapsLocationService::emulatorPosition
	Vector2_t2243707579  ___emulatorPosition_9;
	// System.String OnlineMapsLocationService::markerTooltip
	String_t* ___markerTooltip_10;
	// OnlineMapsLocationServiceMarkerType OnlineMapsLocationService::markerType
	int32_t ___markerType_11;
	// OnlineMapsAlign OnlineMapsLocationService::marker2DAlign
	int32_t ___marker2DAlign_12;
	// UnityEngine.Texture2D OnlineMapsLocationService::marker2DTexture
	Texture2D_t3542995729 * ___marker2DTexture_13;
	// UnityEngine.GameObject OnlineMapsLocationService::marker3DPrefab
	GameObject_t1756533147 * ___marker3DPrefab_14;
	// UnityEngine.Vector2 OnlineMapsLocationService::position
	Vector2_t2243707579  ___position_15;
	// System.Int32 OnlineMapsLocationService::restoreAfter
	int32_t ___restoreAfter_16;
	// System.Single OnlineMapsLocationService::trueHeading
	float ___trueHeading_17;
	// System.Single OnlineMapsLocationService::updateDistance
	float ___updateDistance_18;
	// System.Boolean OnlineMapsLocationService::updatePosition
	bool ___updatePosition_19;
	// System.Boolean OnlineMapsLocationService::useCompassForMarker
	bool ___useCompassForMarker_20;
	// System.Boolean OnlineMapsLocationService::useGPSEmulator
	bool ___useGPSEmulator_21;
	// OnlineMaps OnlineMapsLocationService::api
	OnlineMaps_t1893290312 * ___api_22;
	// System.Boolean OnlineMapsLocationService::allowUpdatePosition
	bool ___allowUpdatePosition_23;
	// System.Int64 OnlineMapsLocationService::lastPositionChangedTime
	int64_t ___lastPositionChangedTime_24;
	// System.Boolean OnlineMapsLocationService::lockDisable
	bool ___lockDisable_25;
	// OnlineMapsMarkerBase OnlineMapsLocationService::marker
	OnlineMapsMarkerBase_t3900955221 * ___marker_26;
	// System.Boolean OnlineMapsLocationService::started
	bool ___started_27;
	// System.String OnlineMapsLocationService::errorMessage
	String_t* ___errorMessage_28;

public:
	inline static int32_t get_offset_of_OnCompassChanged_3() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___OnCompassChanged_3)); }
	inline Action_1_t1878309314 * get_OnCompassChanged_3() const { return ___OnCompassChanged_3; }
	inline Action_1_t1878309314 ** get_address_of_OnCompassChanged_3() { return &___OnCompassChanged_3; }
	inline void set_OnCompassChanged_3(Action_1_t1878309314 * value)
	{
		___OnCompassChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnCompassChanged_3, value);
	}

	inline static int32_t get_offset_of_OnLocationChanged_4() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___OnLocationChanged_4)); }
	inline Action_1_t2045506961 * get_OnLocationChanged_4() const { return ___OnLocationChanged_4; }
	inline Action_1_t2045506961 ** get_address_of_OnLocationChanged_4() { return &___OnLocationChanged_4; }
	inline void set_OnLocationChanged_4(Action_1_t2045506961 * value)
	{
		___OnLocationChanged_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocationChanged_4, value);
	}

	inline static int32_t get_offset_of_autoStopUpdateOnInput_5() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___autoStopUpdateOnInput_5)); }
	inline bool get_autoStopUpdateOnInput_5() const { return ___autoStopUpdateOnInput_5; }
	inline bool* get_address_of_autoStopUpdateOnInput_5() { return &___autoStopUpdateOnInput_5; }
	inline void set_autoStopUpdateOnInput_5(bool value)
	{
		___autoStopUpdateOnInput_5 = value;
	}

	inline static int32_t get_offset_of_createMarkerInUserPosition_6() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___createMarkerInUserPosition_6)); }
	inline bool get_createMarkerInUserPosition_6() const { return ___createMarkerInUserPosition_6; }
	inline bool* get_address_of_createMarkerInUserPosition_6() { return &___createMarkerInUserPosition_6; }
	inline void set_createMarkerInUserPosition_6(bool value)
	{
		___createMarkerInUserPosition_6 = value;
	}

	inline static int32_t get_offset_of_desiredAccuracy_7() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___desiredAccuracy_7)); }
	inline float get_desiredAccuracy_7() const { return ___desiredAccuracy_7; }
	inline float* get_address_of_desiredAccuracy_7() { return &___desiredAccuracy_7; }
	inline void set_desiredAccuracy_7(float value)
	{
		___desiredAccuracy_7 = value;
	}

	inline static int32_t get_offset_of_emulatorCompass_8() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___emulatorCompass_8)); }
	inline float get_emulatorCompass_8() const { return ___emulatorCompass_8; }
	inline float* get_address_of_emulatorCompass_8() { return &___emulatorCompass_8; }
	inline void set_emulatorCompass_8(float value)
	{
		___emulatorCompass_8 = value;
	}

	inline static int32_t get_offset_of_emulatorPosition_9() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___emulatorPosition_9)); }
	inline Vector2_t2243707579  get_emulatorPosition_9() const { return ___emulatorPosition_9; }
	inline Vector2_t2243707579 * get_address_of_emulatorPosition_9() { return &___emulatorPosition_9; }
	inline void set_emulatorPosition_9(Vector2_t2243707579  value)
	{
		___emulatorPosition_9 = value;
	}

	inline static int32_t get_offset_of_markerTooltip_10() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___markerTooltip_10)); }
	inline String_t* get_markerTooltip_10() const { return ___markerTooltip_10; }
	inline String_t** get_address_of_markerTooltip_10() { return &___markerTooltip_10; }
	inline void set_markerTooltip_10(String_t* value)
	{
		___markerTooltip_10 = value;
		Il2CppCodeGenWriteBarrier(&___markerTooltip_10, value);
	}

	inline static int32_t get_offset_of_markerType_11() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___markerType_11)); }
	inline int32_t get_markerType_11() const { return ___markerType_11; }
	inline int32_t* get_address_of_markerType_11() { return &___markerType_11; }
	inline void set_markerType_11(int32_t value)
	{
		___markerType_11 = value;
	}

	inline static int32_t get_offset_of_marker2DAlign_12() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___marker2DAlign_12)); }
	inline int32_t get_marker2DAlign_12() const { return ___marker2DAlign_12; }
	inline int32_t* get_address_of_marker2DAlign_12() { return &___marker2DAlign_12; }
	inline void set_marker2DAlign_12(int32_t value)
	{
		___marker2DAlign_12 = value;
	}

	inline static int32_t get_offset_of_marker2DTexture_13() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___marker2DTexture_13)); }
	inline Texture2D_t3542995729 * get_marker2DTexture_13() const { return ___marker2DTexture_13; }
	inline Texture2D_t3542995729 ** get_address_of_marker2DTexture_13() { return &___marker2DTexture_13; }
	inline void set_marker2DTexture_13(Texture2D_t3542995729 * value)
	{
		___marker2DTexture_13 = value;
		Il2CppCodeGenWriteBarrier(&___marker2DTexture_13, value);
	}

	inline static int32_t get_offset_of_marker3DPrefab_14() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___marker3DPrefab_14)); }
	inline GameObject_t1756533147 * get_marker3DPrefab_14() const { return ___marker3DPrefab_14; }
	inline GameObject_t1756533147 ** get_address_of_marker3DPrefab_14() { return &___marker3DPrefab_14; }
	inline void set_marker3DPrefab_14(GameObject_t1756533147 * value)
	{
		___marker3DPrefab_14 = value;
		Il2CppCodeGenWriteBarrier(&___marker3DPrefab_14, value);
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___position_15)); }
	inline Vector2_t2243707579  get_position_15() const { return ___position_15; }
	inline Vector2_t2243707579 * get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(Vector2_t2243707579  value)
	{
		___position_15 = value;
	}

	inline static int32_t get_offset_of_restoreAfter_16() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___restoreAfter_16)); }
	inline int32_t get_restoreAfter_16() const { return ___restoreAfter_16; }
	inline int32_t* get_address_of_restoreAfter_16() { return &___restoreAfter_16; }
	inline void set_restoreAfter_16(int32_t value)
	{
		___restoreAfter_16 = value;
	}

	inline static int32_t get_offset_of_trueHeading_17() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___trueHeading_17)); }
	inline float get_trueHeading_17() const { return ___trueHeading_17; }
	inline float* get_address_of_trueHeading_17() { return &___trueHeading_17; }
	inline void set_trueHeading_17(float value)
	{
		___trueHeading_17 = value;
	}

	inline static int32_t get_offset_of_updateDistance_18() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___updateDistance_18)); }
	inline float get_updateDistance_18() const { return ___updateDistance_18; }
	inline float* get_address_of_updateDistance_18() { return &___updateDistance_18; }
	inline void set_updateDistance_18(float value)
	{
		___updateDistance_18 = value;
	}

	inline static int32_t get_offset_of_updatePosition_19() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___updatePosition_19)); }
	inline bool get_updatePosition_19() const { return ___updatePosition_19; }
	inline bool* get_address_of_updatePosition_19() { return &___updatePosition_19; }
	inline void set_updatePosition_19(bool value)
	{
		___updatePosition_19 = value;
	}

	inline static int32_t get_offset_of_useCompassForMarker_20() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___useCompassForMarker_20)); }
	inline bool get_useCompassForMarker_20() const { return ___useCompassForMarker_20; }
	inline bool* get_address_of_useCompassForMarker_20() { return &___useCompassForMarker_20; }
	inline void set_useCompassForMarker_20(bool value)
	{
		___useCompassForMarker_20 = value;
	}

	inline static int32_t get_offset_of_useGPSEmulator_21() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___useGPSEmulator_21)); }
	inline bool get_useGPSEmulator_21() const { return ___useGPSEmulator_21; }
	inline bool* get_address_of_useGPSEmulator_21() { return &___useGPSEmulator_21; }
	inline void set_useGPSEmulator_21(bool value)
	{
		___useGPSEmulator_21 = value;
	}

	inline static int32_t get_offset_of_api_22() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___api_22)); }
	inline OnlineMaps_t1893290312 * get_api_22() const { return ___api_22; }
	inline OnlineMaps_t1893290312 ** get_address_of_api_22() { return &___api_22; }
	inline void set_api_22(OnlineMaps_t1893290312 * value)
	{
		___api_22 = value;
		Il2CppCodeGenWriteBarrier(&___api_22, value);
	}

	inline static int32_t get_offset_of_allowUpdatePosition_23() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___allowUpdatePosition_23)); }
	inline bool get_allowUpdatePosition_23() const { return ___allowUpdatePosition_23; }
	inline bool* get_address_of_allowUpdatePosition_23() { return &___allowUpdatePosition_23; }
	inline void set_allowUpdatePosition_23(bool value)
	{
		___allowUpdatePosition_23 = value;
	}

	inline static int32_t get_offset_of_lastPositionChangedTime_24() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___lastPositionChangedTime_24)); }
	inline int64_t get_lastPositionChangedTime_24() const { return ___lastPositionChangedTime_24; }
	inline int64_t* get_address_of_lastPositionChangedTime_24() { return &___lastPositionChangedTime_24; }
	inline void set_lastPositionChangedTime_24(int64_t value)
	{
		___lastPositionChangedTime_24 = value;
	}

	inline static int32_t get_offset_of_lockDisable_25() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___lockDisable_25)); }
	inline bool get_lockDisable_25() const { return ___lockDisable_25; }
	inline bool* get_address_of_lockDisable_25() { return &___lockDisable_25; }
	inline void set_lockDisable_25(bool value)
	{
		___lockDisable_25 = value;
	}

	inline static int32_t get_offset_of_marker_26() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___marker_26)); }
	inline OnlineMapsMarkerBase_t3900955221 * get_marker_26() const { return ___marker_26; }
	inline OnlineMapsMarkerBase_t3900955221 ** get_address_of_marker_26() { return &___marker_26; }
	inline void set_marker_26(OnlineMapsMarkerBase_t3900955221 * value)
	{
		___marker_26 = value;
		Il2CppCodeGenWriteBarrier(&___marker_26, value);
	}

	inline static int32_t get_offset_of_started_27() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___started_27)); }
	inline bool get_started_27() const { return ___started_27; }
	inline bool* get_address_of_started_27() { return &___started_27; }
	inline void set_started_27(bool value)
	{
		___started_27 = value;
	}

	inline static int32_t get_offset_of_errorMessage_28() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090, ___errorMessage_28)); }
	inline String_t* get_errorMessage_28() const { return ___errorMessage_28; }
	inline String_t** get_address_of_errorMessage_28() { return &___errorMessage_28; }
	inline void set_errorMessage_28(String_t* value)
	{
		___errorMessage_28 = value;
		Il2CppCodeGenWriteBarrier(&___errorMessage_28, value);
	}
};

struct OnlineMapsLocationService_t21331090_StaticFields
{
public:
	// OnlineMapsLocationService OnlineMapsLocationService::_instance
	OnlineMapsLocationService_t21331090 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(OnlineMapsLocationService_t21331090_StaticFields, ____instance_2)); }
	inline OnlineMapsLocationService_t21331090 * get__instance_2() const { return ____instance_2; }
	inline OnlineMapsLocationService_t21331090 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(OnlineMapsLocationService_t21331090 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
