﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreamBarController/<CheckScream>c__Iterator0
struct U3CCheckScreamU3Ec__Iterator0_t1250448833;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreamBarController/<CheckScream>c__Iterator0::.ctor()
extern "C"  void U3CCheckScreamU3Ec__Iterator0__ctor_m1057714006 (U3CCheckScreamU3Ec__Iterator0_t1250448833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreamBarController/<CheckScream>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckScreamU3Ec__Iterator0_MoveNext_m1798927354 (U3CCheckScreamU3Ec__Iterator0_t1250448833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreamBarController/<CheckScream>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckScreamU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m42449724 (U3CCheckScreamU3Ec__Iterator0_t1250448833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreamBarController/<CheckScream>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckScreamU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1811404052 (U3CCheckScreamU3Ec__Iterator0_t1250448833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamBarController/<CheckScream>c__Iterator0::Dispose()
extern "C"  void U3CCheckScreamU3Ec__Iterator0_Dispose_m3754296251 (U3CCheckScreamU3Ec__Iterator0_t1250448833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamBarController/<CheckScream>c__Iterator0::Reset()
extern "C"  void U3CCheckScreamU3Ec__Iterator0_Reset_m4236080105 (U3CCheckScreamU3Ec__Iterator0_t1250448833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
