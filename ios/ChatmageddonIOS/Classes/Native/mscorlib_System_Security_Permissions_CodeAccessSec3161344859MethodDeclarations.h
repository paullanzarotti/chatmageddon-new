﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.CodeAccessSecurityAttribute
struct CodeAccessSecurityAttribute_t3161344859;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction446643378.h"

// System.Void System.Security.Permissions.CodeAccessSecurityAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C"  void CodeAccessSecurityAttribute__ctor_m2096829899 (CodeAccessSecurityAttribute_t3161344859 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
