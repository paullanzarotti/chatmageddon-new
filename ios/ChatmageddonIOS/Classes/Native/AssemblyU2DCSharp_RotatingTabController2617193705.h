﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RotatingTab
struct RotatingTab_t2077750253;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotatingTabController
struct  RotatingTabController_t2617193705  : public MonoBehaviour_t1158329972
{
public:
	// RotatingTab RotatingTabController::leftTab
	RotatingTab_t2077750253 * ___leftTab_2;
	// RotatingTab RotatingTabController::rightTab
	RotatingTab_t2077750253 * ___rightTab_3;
	// System.Single RotatingTabController::leftTabRPS
	float ___leftTabRPS_4;
	// System.Single RotatingTabController::rightTabRPS
	float ___rightTabRPS_5;
	// UnityEngine.Color RotatingTabController::tabCollidingColour
	Color_t2020392075  ___tabCollidingColour_6;
	// UnityEngine.Color RotatingTabController::tabNotCollidingColour
	Color_t2020392075  ___tabNotCollidingColour_7;
	// System.Boolean RotatingTabController::tabsColliding
	bool ___tabsColliding_8;

public:
	inline static int32_t get_offset_of_leftTab_2() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___leftTab_2)); }
	inline RotatingTab_t2077750253 * get_leftTab_2() const { return ___leftTab_2; }
	inline RotatingTab_t2077750253 ** get_address_of_leftTab_2() { return &___leftTab_2; }
	inline void set_leftTab_2(RotatingTab_t2077750253 * value)
	{
		___leftTab_2 = value;
		Il2CppCodeGenWriteBarrier(&___leftTab_2, value);
	}

	inline static int32_t get_offset_of_rightTab_3() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___rightTab_3)); }
	inline RotatingTab_t2077750253 * get_rightTab_3() const { return ___rightTab_3; }
	inline RotatingTab_t2077750253 ** get_address_of_rightTab_3() { return &___rightTab_3; }
	inline void set_rightTab_3(RotatingTab_t2077750253 * value)
	{
		___rightTab_3 = value;
		Il2CppCodeGenWriteBarrier(&___rightTab_3, value);
	}

	inline static int32_t get_offset_of_leftTabRPS_4() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___leftTabRPS_4)); }
	inline float get_leftTabRPS_4() const { return ___leftTabRPS_4; }
	inline float* get_address_of_leftTabRPS_4() { return &___leftTabRPS_4; }
	inline void set_leftTabRPS_4(float value)
	{
		___leftTabRPS_4 = value;
	}

	inline static int32_t get_offset_of_rightTabRPS_5() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___rightTabRPS_5)); }
	inline float get_rightTabRPS_5() const { return ___rightTabRPS_5; }
	inline float* get_address_of_rightTabRPS_5() { return &___rightTabRPS_5; }
	inline void set_rightTabRPS_5(float value)
	{
		___rightTabRPS_5 = value;
	}

	inline static int32_t get_offset_of_tabCollidingColour_6() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___tabCollidingColour_6)); }
	inline Color_t2020392075  get_tabCollidingColour_6() const { return ___tabCollidingColour_6; }
	inline Color_t2020392075 * get_address_of_tabCollidingColour_6() { return &___tabCollidingColour_6; }
	inline void set_tabCollidingColour_6(Color_t2020392075  value)
	{
		___tabCollidingColour_6 = value;
	}

	inline static int32_t get_offset_of_tabNotCollidingColour_7() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___tabNotCollidingColour_7)); }
	inline Color_t2020392075  get_tabNotCollidingColour_7() const { return ___tabNotCollidingColour_7; }
	inline Color_t2020392075 * get_address_of_tabNotCollidingColour_7() { return &___tabNotCollidingColour_7; }
	inline void set_tabNotCollidingColour_7(Color_t2020392075  value)
	{
		___tabNotCollidingColour_7 = value;
	}

	inline static int32_t get_offset_of_tabsColliding_8() { return static_cast<int32_t>(offsetof(RotatingTabController_t2617193705, ___tabsColliding_8)); }
	inline bool get_tabsColliding_8() const { return ___tabsColliding_8; }
	inline bool* get_address_of_tabsColliding_8() { return &___tabsColliding_8; }
	inline void set_tabsColliding_8(bool value)
	{
		___tabsColliding_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
