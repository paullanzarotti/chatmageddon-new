﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<DeleteSingleContact>c__AnonStorey19<System.Object>
struct U3CDeleteSingleContactU3Ec__AnonStorey19_t2560062602;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<DeleteSingleContact>c__AnonStorey19<System.Object>::.ctor()
extern "C"  void U3CDeleteSingleContactU3Ec__AnonStorey19__ctor_m1446723831_gshared (U3CDeleteSingleContactU3Ec__AnonStorey19_t2560062602 * __this, const MethodInfo* method);
#define U3CDeleteSingleContactU3Ec__AnonStorey19__ctor_m1446723831(__this, method) ((  void (*) (U3CDeleteSingleContactU3Ec__AnonStorey19_t2560062602 *, const MethodInfo*))U3CDeleteSingleContactU3Ec__AnonStorey19__ctor_m1446723831_gshared)(__this, method)
// System.Void BaseServer`1/<DeleteSingleContact>c__AnonStorey19<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeleteSingleContactU3Ec__AnonStorey19_U3CU3Em__0_m2219953778_gshared (U3CDeleteSingleContactU3Ec__AnonStorey19_t2560062602 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeleteSingleContactU3Ec__AnonStorey19_U3CU3Em__0_m2219953778(__this, ___request0, ___response1, method) ((  void (*) (U3CDeleteSingleContactU3Ec__AnonStorey19_t2560062602 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeleteSingleContactU3Ec__AnonStorey19_U3CU3Em__0_m2219953778_gshared)(__this, ___request0, ___response1, method)
