﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RevokeFriendRequest>c__AnonStorey16<System.Object>
struct U3CRevokeFriendRequestU3Ec__AnonStorey16_t3294443221;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RevokeFriendRequest>c__AnonStorey16<System.Object>::.ctor()
extern "C"  void U3CRevokeFriendRequestU3Ec__AnonStorey16__ctor_m611105022_gshared (U3CRevokeFriendRequestU3Ec__AnonStorey16_t3294443221 * __this, const MethodInfo* method);
#define U3CRevokeFriendRequestU3Ec__AnonStorey16__ctor_m611105022(__this, method) ((  void (*) (U3CRevokeFriendRequestU3Ec__AnonStorey16_t3294443221 *, const MethodInfo*))U3CRevokeFriendRequestU3Ec__AnonStorey16__ctor_m611105022_gshared)(__this, method)
// System.Void BaseServer`1/<RevokeFriendRequest>c__AnonStorey16<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRevokeFriendRequestU3Ec__AnonStorey16_U3CU3Em__0_m902430401_gshared (U3CRevokeFriendRequestU3Ec__AnonStorey16_t3294443221 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRevokeFriendRequestU3Ec__AnonStorey16_U3CU3Em__0_m902430401(__this, ___request0, ___response1, method) ((  void (*) (U3CRevokeFriendRequestU3Ec__AnonStorey16_t3294443221 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRevokeFriendRequestU3Ec__AnonStorey16_U3CU3Em__0_m902430401_gshared)(__this, ___request0, ___response1, method)
