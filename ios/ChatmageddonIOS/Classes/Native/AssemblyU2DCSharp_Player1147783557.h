﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// System.Collections.Generic.List`1<LaunchedItem>
struct List_1_t3039755559;
// System.Collections.Generic.List`1<ChatThread>
struct List_1_t1763444614;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// System.Func`2<Friend,System.Int32>
struct Func_2_t2787752525;
// System.Func`2<Friend,System.String>
struct Func_2_t2745095310;
// System.Comparison`1<LaunchedItem>
struct Comparison_1_t637405982;

#include "AssemblyU2DCSharp_User719925459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t1147783557  : public User_t719925459
{
public:
	// System.Collections.Generic.List`1<Friend> Player::innerFriendsList
	List_1_t2924135240 * ___innerFriendsList_41;
	// System.Collections.Generic.List`1<Friend> Player::outerFriendsList
	List_1_t2924135240 * ___outerFriendsList_42;
	// System.Collections.Generic.List`1<Friend> Player::incPendingFriendsList
	List_1_t2924135240 * ___incPendingFriendsList_43;
	// System.Collections.Generic.List`1<Friend> Player::outPendingFriendsList
	List_1_t2924135240 * ___outPendingFriendsList_44;
	// System.Collections.Generic.List`1<Friend> Player::blockList
	List_1_t2924135240 * ___blockList_45;
	// System.Collections.Generic.List`1<LaunchedItem> Player::incLaunchedItems
	List_1_t3039755559 * ___incLaunchedItems_46;
	// System.Collections.Generic.List`1<LaunchedItem> Player::outLaunchedItems
	List_1_t3039755559 * ___outLaunchedItems_47;
	// System.Collections.Generic.List`1<ChatThread> Player::chatThreadsList
	List_1_t1763444614 * ___chatThreadsList_48;
	// System.Boolean Player::allowIGNotifications
	bool ___allowIGNotifications_49;
	// System.Boolean Player::allowIGNSound
	bool ___allowIGNSound_50;
	// System.Boolean Player::allowIGNVibration
	bool ___allowIGNVibration_51;
	// System.Int32 Player::bucks
	int32_t ___bucks_52;
	// System.Int32 Player::currentFuel
	int32_t ___currentFuel_53;
	// System.Collections.Hashtable Player::latestInnerList
	Hashtable_t909839986 * ___latestInnerList_54;
	// System.String Player::mobileRegion
	String_t* ___mobileRegion_55;
	// System.String Player::nemesisID
	String_t* ___nemesisID_56;
	// System.Boolean Player::pointsReset
	bool ___pointsReset_57;

public:
	inline static int32_t get_offset_of_innerFriendsList_41() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___innerFriendsList_41)); }
	inline List_1_t2924135240 * get_innerFriendsList_41() const { return ___innerFriendsList_41; }
	inline List_1_t2924135240 ** get_address_of_innerFriendsList_41() { return &___innerFriendsList_41; }
	inline void set_innerFriendsList_41(List_1_t2924135240 * value)
	{
		___innerFriendsList_41 = value;
		Il2CppCodeGenWriteBarrier(&___innerFriendsList_41, value);
	}

	inline static int32_t get_offset_of_outerFriendsList_42() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___outerFriendsList_42)); }
	inline List_1_t2924135240 * get_outerFriendsList_42() const { return ___outerFriendsList_42; }
	inline List_1_t2924135240 ** get_address_of_outerFriendsList_42() { return &___outerFriendsList_42; }
	inline void set_outerFriendsList_42(List_1_t2924135240 * value)
	{
		___outerFriendsList_42 = value;
		Il2CppCodeGenWriteBarrier(&___outerFriendsList_42, value);
	}

	inline static int32_t get_offset_of_incPendingFriendsList_43() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___incPendingFriendsList_43)); }
	inline List_1_t2924135240 * get_incPendingFriendsList_43() const { return ___incPendingFriendsList_43; }
	inline List_1_t2924135240 ** get_address_of_incPendingFriendsList_43() { return &___incPendingFriendsList_43; }
	inline void set_incPendingFriendsList_43(List_1_t2924135240 * value)
	{
		___incPendingFriendsList_43 = value;
		Il2CppCodeGenWriteBarrier(&___incPendingFriendsList_43, value);
	}

	inline static int32_t get_offset_of_outPendingFriendsList_44() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___outPendingFriendsList_44)); }
	inline List_1_t2924135240 * get_outPendingFriendsList_44() const { return ___outPendingFriendsList_44; }
	inline List_1_t2924135240 ** get_address_of_outPendingFriendsList_44() { return &___outPendingFriendsList_44; }
	inline void set_outPendingFriendsList_44(List_1_t2924135240 * value)
	{
		___outPendingFriendsList_44 = value;
		Il2CppCodeGenWriteBarrier(&___outPendingFriendsList_44, value);
	}

	inline static int32_t get_offset_of_blockList_45() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___blockList_45)); }
	inline List_1_t2924135240 * get_blockList_45() const { return ___blockList_45; }
	inline List_1_t2924135240 ** get_address_of_blockList_45() { return &___blockList_45; }
	inline void set_blockList_45(List_1_t2924135240 * value)
	{
		___blockList_45 = value;
		Il2CppCodeGenWriteBarrier(&___blockList_45, value);
	}

	inline static int32_t get_offset_of_incLaunchedItems_46() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___incLaunchedItems_46)); }
	inline List_1_t3039755559 * get_incLaunchedItems_46() const { return ___incLaunchedItems_46; }
	inline List_1_t3039755559 ** get_address_of_incLaunchedItems_46() { return &___incLaunchedItems_46; }
	inline void set_incLaunchedItems_46(List_1_t3039755559 * value)
	{
		___incLaunchedItems_46 = value;
		Il2CppCodeGenWriteBarrier(&___incLaunchedItems_46, value);
	}

	inline static int32_t get_offset_of_outLaunchedItems_47() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___outLaunchedItems_47)); }
	inline List_1_t3039755559 * get_outLaunchedItems_47() const { return ___outLaunchedItems_47; }
	inline List_1_t3039755559 ** get_address_of_outLaunchedItems_47() { return &___outLaunchedItems_47; }
	inline void set_outLaunchedItems_47(List_1_t3039755559 * value)
	{
		___outLaunchedItems_47 = value;
		Il2CppCodeGenWriteBarrier(&___outLaunchedItems_47, value);
	}

	inline static int32_t get_offset_of_chatThreadsList_48() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___chatThreadsList_48)); }
	inline List_1_t1763444614 * get_chatThreadsList_48() const { return ___chatThreadsList_48; }
	inline List_1_t1763444614 ** get_address_of_chatThreadsList_48() { return &___chatThreadsList_48; }
	inline void set_chatThreadsList_48(List_1_t1763444614 * value)
	{
		___chatThreadsList_48 = value;
		Il2CppCodeGenWriteBarrier(&___chatThreadsList_48, value);
	}

	inline static int32_t get_offset_of_allowIGNotifications_49() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___allowIGNotifications_49)); }
	inline bool get_allowIGNotifications_49() const { return ___allowIGNotifications_49; }
	inline bool* get_address_of_allowIGNotifications_49() { return &___allowIGNotifications_49; }
	inline void set_allowIGNotifications_49(bool value)
	{
		___allowIGNotifications_49 = value;
	}

	inline static int32_t get_offset_of_allowIGNSound_50() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___allowIGNSound_50)); }
	inline bool get_allowIGNSound_50() const { return ___allowIGNSound_50; }
	inline bool* get_address_of_allowIGNSound_50() { return &___allowIGNSound_50; }
	inline void set_allowIGNSound_50(bool value)
	{
		___allowIGNSound_50 = value;
	}

	inline static int32_t get_offset_of_allowIGNVibration_51() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___allowIGNVibration_51)); }
	inline bool get_allowIGNVibration_51() const { return ___allowIGNVibration_51; }
	inline bool* get_address_of_allowIGNVibration_51() { return &___allowIGNVibration_51; }
	inline void set_allowIGNVibration_51(bool value)
	{
		___allowIGNVibration_51 = value;
	}

	inline static int32_t get_offset_of_bucks_52() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___bucks_52)); }
	inline int32_t get_bucks_52() const { return ___bucks_52; }
	inline int32_t* get_address_of_bucks_52() { return &___bucks_52; }
	inline void set_bucks_52(int32_t value)
	{
		___bucks_52 = value;
	}

	inline static int32_t get_offset_of_currentFuel_53() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___currentFuel_53)); }
	inline int32_t get_currentFuel_53() const { return ___currentFuel_53; }
	inline int32_t* get_address_of_currentFuel_53() { return &___currentFuel_53; }
	inline void set_currentFuel_53(int32_t value)
	{
		___currentFuel_53 = value;
	}

	inline static int32_t get_offset_of_latestInnerList_54() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___latestInnerList_54)); }
	inline Hashtable_t909839986 * get_latestInnerList_54() const { return ___latestInnerList_54; }
	inline Hashtable_t909839986 ** get_address_of_latestInnerList_54() { return &___latestInnerList_54; }
	inline void set_latestInnerList_54(Hashtable_t909839986 * value)
	{
		___latestInnerList_54 = value;
		Il2CppCodeGenWriteBarrier(&___latestInnerList_54, value);
	}

	inline static int32_t get_offset_of_mobileRegion_55() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___mobileRegion_55)); }
	inline String_t* get_mobileRegion_55() const { return ___mobileRegion_55; }
	inline String_t** get_address_of_mobileRegion_55() { return &___mobileRegion_55; }
	inline void set_mobileRegion_55(String_t* value)
	{
		___mobileRegion_55 = value;
		Il2CppCodeGenWriteBarrier(&___mobileRegion_55, value);
	}

	inline static int32_t get_offset_of_nemesisID_56() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___nemesisID_56)); }
	inline String_t* get_nemesisID_56() const { return ___nemesisID_56; }
	inline String_t** get_address_of_nemesisID_56() { return &___nemesisID_56; }
	inline void set_nemesisID_56(String_t* value)
	{
		___nemesisID_56 = value;
		Il2CppCodeGenWriteBarrier(&___nemesisID_56, value);
	}

	inline static int32_t get_offset_of_pointsReset_57() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___pointsReset_57)); }
	inline bool get_pointsReset_57() const { return ___pointsReset_57; }
	inline bool* get_address_of_pointsReset_57() { return &___pointsReset_57; }
	inline void set_pointsReset_57(bool value)
	{
		___pointsReset_57 = value;
	}
};

struct Player_t1147783557_StaticFields
{
public:
	// System.Func`2<Friend,System.Int32> Player::<>f__am$cache0
	Func_2_t2787752525 * ___U3CU3Ef__amU24cache0_58;
	// System.Func`2<Friend,System.String> Player::<>f__am$cache1
	Func_2_t2745095310 * ___U3CU3Ef__amU24cache1_59;
	// System.Comparison`1<LaunchedItem> Player::<>f__am$cache2
	Comparison_1_t637405982 * ___U3CU3Ef__amU24cache2_60;
	// System.Comparison`1<LaunchedItem> Player::<>f__am$cache3
	Comparison_1_t637405982 * ___U3CU3Ef__amU24cache3_61;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_58() { return static_cast<int32_t>(offsetof(Player_t1147783557_StaticFields, ___U3CU3Ef__amU24cache0_58)); }
	inline Func_2_t2787752525 * get_U3CU3Ef__amU24cache0_58() const { return ___U3CU3Ef__amU24cache0_58; }
	inline Func_2_t2787752525 ** get_address_of_U3CU3Ef__amU24cache0_58() { return &___U3CU3Ef__amU24cache0_58; }
	inline void set_U3CU3Ef__amU24cache0_58(Func_2_t2787752525 * value)
	{
		___U3CU3Ef__amU24cache0_58 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_58, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_59() { return static_cast<int32_t>(offsetof(Player_t1147783557_StaticFields, ___U3CU3Ef__amU24cache1_59)); }
	inline Func_2_t2745095310 * get_U3CU3Ef__amU24cache1_59() const { return ___U3CU3Ef__amU24cache1_59; }
	inline Func_2_t2745095310 ** get_address_of_U3CU3Ef__amU24cache1_59() { return &___U3CU3Ef__amU24cache1_59; }
	inline void set_U3CU3Ef__amU24cache1_59(Func_2_t2745095310 * value)
	{
		___U3CU3Ef__amU24cache1_59 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_59, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_60() { return static_cast<int32_t>(offsetof(Player_t1147783557_StaticFields, ___U3CU3Ef__amU24cache2_60)); }
	inline Comparison_1_t637405982 * get_U3CU3Ef__amU24cache2_60() const { return ___U3CU3Ef__amU24cache2_60; }
	inline Comparison_1_t637405982 ** get_address_of_U3CU3Ef__amU24cache2_60() { return &___U3CU3Ef__amU24cache2_60; }
	inline void set_U3CU3Ef__amU24cache2_60(Comparison_1_t637405982 * value)
	{
		___U3CU3Ef__amU24cache2_60 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_60, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_61() { return static_cast<int32_t>(offsetof(Player_t1147783557_StaticFields, ___U3CU3Ef__amU24cache3_61)); }
	inline Comparison_1_t637405982 * get_U3CU3Ef__amU24cache3_61() const { return ___U3CU3Ef__amU24cache3_61; }
	inline Comparison_1_t637405982 ** get_address_of_U3CU3Ef__amU24cache3_61() { return &___U3CU3Ef__amU24cache3_61; }
	inline void set_U3CU3Ef__amU24cache3_61(Comparison_1_t637405982 * value)
	{
		___U3CU3Ef__amU24cache3_61 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_61, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
