﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// TweenPosition
struct TweenPosition_t1144714832;
// System.Func`2<ChatThread,System.DateTime>
struct Func_2_t4273830580;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen863017027.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatThreadsILP
struct  ChatThreadsILP_t2341903446  : public BaseInfiniteListPopulator_1_t863017027
{
public:
	// System.Boolean ChatThreadsILP::listLoaded
	bool ___listLoaded_33;
	// System.Collections.ArrayList ChatThreadsILP::chatThreadArray
	ArrayList_t4252133567 * ___chatThreadArray_34;
	// TweenPosition ChatThreadsILP::tableTween
	TweenPosition_t1144714832 * ___tableTween_35;
	// UnityEngine.Color ChatThreadsILP::newMessageColour
	Color_t2020392075  ___newMessageColour_36;
	// UnityEngine.Color ChatThreadsILP::defaultColour
	Color_t2020392075  ___defaultColour_37;

public:
	inline static int32_t get_offset_of_listLoaded_33() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446, ___listLoaded_33)); }
	inline bool get_listLoaded_33() const { return ___listLoaded_33; }
	inline bool* get_address_of_listLoaded_33() { return &___listLoaded_33; }
	inline void set_listLoaded_33(bool value)
	{
		___listLoaded_33 = value;
	}

	inline static int32_t get_offset_of_chatThreadArray_34() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446, ___chatThreadArray_34)); }
	inline ArrayList_t4252133567 * get_chatThreadArray_34() const { return ___chatThreadArray_34; }
	inline ArrayList_t4252133567 ** get_address_of_chatThreadArray_34() { return &___chatThreadArray_34; }
	inline void set_chatThreadArray_34(ArrayList_t4252133567 * value)
	{
		___chatThreadArray_34 = value;
		Il2CppCodeGenWriteBarrier(&___chatThreadArray_34, value);
	}

	inline static int32_t get_offset_of_tableTween_35() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446, ___tableTween_35)); }
	inline TweenPosition_t1144714832 * get_tableTween_35() const { return ___tableTween_35; }
	inline TweenPosition_t1144714832 ** get_address_of_tableTween_35() { return &___tableTween_35; }
	inline void set_tableTween_35(TweenPosition_t1144714832 * value)
	{
		___tableTween_35 = value;
		Il2CppCodeGenWriteBarrier(&___tableTween_35, value);
	}

	inline static int32_t get_offset_of_newMessageColour_36() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446, ___newMessageColour_36)); }
	inline Color_t2020392075  get_newMessageColour_36() const { return ___newMessageColour_36; }
	inline Color_t2020392075 * get_address_of_newMessageColour_36() { return &___newMessageColour_36; }
	inline void set_newMessageColour_36(Color_t2020392075  value)
	{
		___newMessageColour_36 = value;
	}

	inline static int32_t get_offset_of_defaultColour_37() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446, ___defaultColour_37)); }
	inline Color_t2020392075  get_defaultColour_37() const { return ___defaultColour_37; }
	inline Color_t2020392075 * get_address_of_defaultColour_37() { return &___defaultColour_37; }
	inline void set_defaultColour_37(Color_t2020392075  value)
	{
		___defaultColour_37 = value;
	}
};

struct ChatThreadsILP_t2341903446_StaticFields
{
public:
	// System.Func`2<ChatThread,System.DateTime> ChatThreadsILP::<>f__am$cache0
	Func_2_t4273830580 * ___U3CU3Ef__amU24cache0_38;
	// System.Func`2<ChatThread,System.DateTime> ChatThreadsILP::<>f__am$cache1
	Func_2_t4273830580 * ___U3CU3Ef__amU24cache1_39;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_38() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446_StaticFields, ___U3CU3Ef__amU24cache0_38)); }
	inline Func_2_t4273830580 * get_U3CU3Ef__amU24cache0_38() const { return ___U3CU3Ef__amU24cache0_38; }
	inline Func_2_t4273830580 ** get_address_of_U3CU3Ef__amU24cache0_38() { return &___U3CU3Ef__amU24cache0_38; }
	inline void set_U3CU3Ef__amU24cache0_38(Func_2_t4273830580 * value)
	{
		___U3CU3Ef__amU24cache0_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_38, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_39() { return static_cast<int32_t>(offsetof(ChatThreadsILP_t2341903446_StaticFields, ___U3CU3Ef__amU24cache1_39)); }
	inline Func_2_t4273830580 * get_U3CU3Ef__amU24cache1_39() const { return ___U3CU3Ef__amU24cache1_39; }
	inline Func_2_t4273830580 ** get_address_of_U3CU3Ef__amU24cache1_39() { return &___U3CU3Ef__amU24cache1_39; }
	inline void set_U3CU3Ef__amU24cache1_39(Func_2_t4273830580 * value)
	{
		___U3CU3Ef__amU24cache1_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
