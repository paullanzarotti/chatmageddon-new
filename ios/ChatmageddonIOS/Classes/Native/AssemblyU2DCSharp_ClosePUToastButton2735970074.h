﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PUToastUI
struct PUToastUI_t4217418532;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClosePUToastButton
struct  ClosePUToastButton_t2735970074  : public SFXButton_t792651341
{
public:
	// PUToastUI ClosePUToastButton::toastUI
	PUToastUI_t4217418532 * ___toastUI_5;

public:
	inline static int32_t get_offset_of_toastUI_5() { return static_cast<int32_t>(offsetof(ClosePUToastButton_t2735970074, ___toastUI_5)); }
	inline PUToastUI_t4217418532 * get_toastUI_5() const { return ___toastUI_5; }
	inline PUToastUI_t4217418532 ** get_address_of_toastUI_5() { return &___toastUI_5; }
	inline void set_toastUI_5(PUToastUI_t4217418532 * value)
	{
		___toastUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___toastUI_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
