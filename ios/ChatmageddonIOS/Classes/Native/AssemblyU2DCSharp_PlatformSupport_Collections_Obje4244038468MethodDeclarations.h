﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>
struct ObservableDictionary_2_t4244038468;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t280592844;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler
struct NotifyCollectionChangedEventHandler_t1310059589;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.String
struct String_t;
// System.Collections.IList
struct IList_t3321498491;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje1310059589.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2640700633.h"

// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void ObservableDictionary_2__ctor_m3482229747_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2__ctor_m3482229747(__this, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2__ctor_m3482229747_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2__ctor_m2273999893_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define ObservableDictionary_2__ctor_m2273999893(__this, ___dictionary0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m2273999893_gshared)(__this, ___dictionary0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ObservableDictionary_2__ctor_m3424814776_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define ObservableDictionary_2__ctor_m3424814776(__this, ___comparer0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m3424814776_gshared)(__this, ___comparer0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Int32)
extern "C"  void ObservableDictionary_2__ctor_m4217517412_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___capacity0, const MethodInfo* method);
#define ObservableDictionary_2__ctor_m4217517412(__this, ___capacity0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, const MethodInfo*))ObservableDictionary_2__ctor_m4217517412_gshared)(__this, ___capacity0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ObservableDictionary_2__ctor_m1850798116_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define ObservableDictionary_2__ctor_m1850798116(__this, ___dictionary0, ___comparer1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m1850798116_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ObservableDictionary_2__ctor_m2924843205_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define ObservableDictionary_2__ctor_m2924843205(__this, ___capacity0, ___comparer1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m2924843205_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Collections.Generic.IDictionary`2<TKey,TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Dictionary()
extern "C"  Il2CppObject* ObservableDictionary_2_get_Dictionary_m267445679_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_get_Dictionary_m267445679(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_get_Dictionary_m267445679_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ObservableDictionary_2_Add_m809576144_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ObservableDictionary_2_Add_m809576144(__this, ___key0, ___value1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_Add_m809576144_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ObservableDictionary_2_ContainsKey_m3273400666_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ObservableDictionary_2_ContainsKey_m3273400666(__this, ___key0, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_ContainsKey_m3273400666_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ObservableDictionary_2_get_Keys_m4034744365_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_get_Keys_m4034744365(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_get_Keys_m4034744365_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ObservableDictionary_2_Remove_m1597064470_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ObservableDictionary_2_Remove_m1597064470(__this, ___key0, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_Remove_m1597064470_gshared)(__this, ___key0, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ObservableDictionary_2_TryGetValue_m2959695535_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define ObservableDictionary_2_TryGetValue_m2959695535(__this, ___key0, ___value1, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))ObservableDictionary_2_TryGetValue_m2959695535_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ObservableDictionary_2_get_Values_m2365880877_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_get_Values_m2365880877(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_get_Values_m2365880877_gshared)(__this, method)
// TValue PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ObservableDictionary_2_get_Item_m1340976434_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ObservableDictionary_2_get_Item_m1340976434(__this, ___key0, method) ((  Il2CppObject * (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_get_Item_m1340976434_gshared)(__this, ___key0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ObservableDictionary_2_set_Item_m1965010995_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ObservableDictionary_2_set_Item_m1965010995(__this, ___key0, ___value1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_set_Item_m1965010995_gshared)(__this, ___key0, ___value1, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2_Add_m219092131_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define ObservableDictionary_2_Add_m219092131(__this, ___item0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, KeyValuePair_2_t38854645 , const MethodInfo*))ObservableDictionary_2_Add_m219092131_gshared)(__this, ___item0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ObservableDictionary_2_Clear_m2078003600_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_Clear_m2078003600(__this, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_Clear_m2078003600_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ObservableDictionary_2_Contains_m156958993_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define ObservableDictionary_2_Contains_m156958993(__this, ___item0, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, KeyValuePair_2_t38854645 , const MethodInfo*))ObservableDictionary_2_Contains_m156958993_gshared)(__this, ___item0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ObservableDictionary_2_CopyTo_m3898517055_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ObservableDictionary_2_CopyTo_m3898517055(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, KeyValuePair_2U5BU5D_t2854920344*, int32_t, const MethodInfo*))ObservableDictionary_2_CopyTo_m3898517055_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ObservableDictionary_2_get_Count_m3837408227_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_get_Count_m3837408227(__this, method) ((  int32_t (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_get_Count_m3837408227_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ObservableDictionary_2_get_IsReadOnly_m2217790444_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_get_IsReadOnly_m2217790444(__this, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_get_IsReadOnly_m2217790444_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ObservableDictionary_2_Remove_m117162194_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define ObservableDictionary_2_Remove_m117162194(__this, ___item0, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, KeyValuePair_2_t38854645 , const MethodInfo*))ObservableDictionary_2_Remove_m117162194_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ObservableDictionary_2_GetEnumerator_m2468868070_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_GetEnumerator_m2468868070(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_GetEnumerator_m2468868070_gshared)(__this, method)
// System.Collections.IEnumerator PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304(__this, method) ((  Il2CppObject * (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::add_CollectionChanged(PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler)
extern "C"  void ObservableDictionary_2_add_CollectionChanged_m3033885855_gshared (ObservableDictionary_2_t4244038468 * __this, NotifyCollectionChangedEventHandler_t1310059589 * ___value0, const MethodInfo* method);
#define ObservableDictionary_2_add_CollectionChanged_m3033885855(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, NotifyCollectionChangedEventHandler_t1310059589 *, const MethodInfo*))ObservableDictionary_2_add_CollectionChanged_m3033885855_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::remove_CollectionChanged(PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler)
extern "C"  void ObservableDictionary_2_remove_CollectionChanged_m2705002958_gshared (ObservableDictionary_2_t4244038468 * __this, NotifyCollectionChangedEventHandler_t1310059589 * ___value0, const MethodInfo* method);
#define ObservableDictionary_2_remove_CollectionChanged_m2705002958(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, NotifyCollectionChangedEventHandler_t1310059589 *, const MethodInfo*))ObservableDictionary_2_remove_CollectionChanged_m2705002958_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void ObservableDictionary_2_add_PropertyChanged_m1468847672_gshared (ObservableDictionary_2_t4244038468 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method);
#define ObservableDictionary_2_add_PropertyChanged_m1468847672(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, PropertyChangedEventHandler_t3042952059 *, const MethodInfo*))ObservableDictionary_2_add_PropertyChanged_m1468847672_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void ObservableDictionary_2_remove_PropertyChanged_m26290967_gshared (ObservableDictionary_2_t4244038468 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method);
#define ObservableDictionary_2_remove_PropertyChanged_m26290967(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, PropertyChangedEventHandler_t3042952059 *, const MethodInfo*))ObservableDictionary_2_remove_PropertyChanged_m26290967_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::AddRange(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2_AddRange_m3987702119_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___items0, const MethodInfo* method);
#define ObservableDictionary_2_AddRange_m3987702119(__this, ___items0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject*, const MethodInfo*))ObservableDictionary_2_AddRange_m3987702119_gshared)(__this, ___items0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Insert(TKey,TValue,System.Boolean)
extern "C"  void ObservableDictionary_2_Insert_m2164254687_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, bool ___add2, const MethodInfo* method);
#define ObservableDictionary_2_Insert_m2164254687(__this, ___key0, ___value1, ___add2, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject *, bool, const MethodInfo*))ObservableDictionary_2_Insert_m2164254687_gshared)(__this, ___key0, ___value1, ___add2, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged()
extern "C"  void ObservableDictionary_2_OnPropertyChanged_m1628940995_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_OnPropertyChanged_m1628940995(__this, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_OnPropertyChanged_m1628940995_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged(System.String)
extern "C"  void ObservableDictionary_2_OnPropertyChanged_m643605273_gshared (ObservableDictionary_2_t4244038468 * __this, String_t* ___propertyName0, const MethodInfo* method);
#define ObservableDictionary_2_OnPropertyChanged_m643605273(__this, ___propertyName0, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, String_t*, const MethodInfo*))ObservableDictionary_2_OnPropertyChanged_m643605273_gshared)(__this, ___propertyName0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged()
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m1062698694_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method);
#define ObservableDictionary_2_OnCollectionChanged_m1062698694(__this, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m1062698694_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m78342394_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___action0, KeyValuePair_2_t38854645  ___changedItem1, const MethodInfo* method);
#define ObservableDictionary_2_OnCollectionChanged_m78342394(__this, ___action0, ___changedItem1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, KeyValuePair_2_t38854645 , const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m78342394_gshared)(__this, ___action0, ___changedItem1, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.Generic.KeyValuePair`2<TKey,TValue>,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m20718175_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___action0, KeyValuePair_2_t38854645  ___newItem1, KeyValuePair_2_t38854645  ___oldItem2, const MethodInfo* method);
#define ObservableDictionary_2_OnCollectionChanged_m20718175(__this, ___action0, ___newItem1, ___oldItem2, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, KeyValuePair_2_t38854645 , KeyValuePair_2_t38854645 , const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m20718175_gshared)(__this, ___action0, ___newItem1, ___oldItem2, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList)
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m908791682_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___action0, Il2CppObject * ___newItems1, const MethodInfo* method);
#define ObservableDictionary_2_OnCollectionChanged_m908791682(__this, ___action0, ___newItems1, method) ((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m908791682_gshared)(__this, ___action0, ___newItems1, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::<AddRange>m__0(TKey)
extern "C"  bool ObservableDictionary_2_U3CAddRangeU3Em__0_m3926150165_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___k0, const MethodInfo* method);
#define ObservableDictionary_2_U3CAddRangeU3Em__0_m3926150165(__this, ___k0, method) ((  bool (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_U3CAddRangeU3Em__0_m3926150165_gshared)(__this, ___k0, method)
