﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3248597474MethodDeclarations.h"

// System.Void BaseInfiniteListPopulator`1<StatusListItem>::.ctor()
#define BaseInfiniteListPopulator_1__ctor_m2349565118(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1__ctor_m2641845277_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::Awake()
#define BaseInfiniteListPopulator_1_Awake_m3836970205(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_Awake_m758246746_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::ResetPanel()
#define BaseInfiniteListPopulator_1_ResetPanel_m3172863723(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m3141932207(__this, ___item0, ___dataIndex1, ___carouselIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, Transform_t3275118058 *, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared)(__this, ___item0, ___dataIndex1, ___carouselIndex2, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetStartIndex(System.Int32)
#define BaseInfiniteListPopulator_1_SetStartIndex_m4075334841(__this, ___inStartIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared)(__this, ___inStartIndex0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetOriginalData(System.Collections.ArrayList)
#define BaseInfiniteListPopulator_1_SetOriginalData_m2466959238(__this, ___inDataList0, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, ArrayList_t4252133567 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared)(__this, ___inDataList0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetSectionIndices(System.Collections.Generic.List`1<System.Int32>)
#define BaseInfiniteListPopulator_1_SetSectionIndices_m4269303787(__this, ___inSectionsIndices0, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, List_1_t1440998580 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared)(__this, ___inSectionsIndices0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::RefreshTableView()
#define BaseInfiniteListPopulator_1_RefreshTableView_m3478492568(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::InitTableView(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableView_m1239820188(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::InitTableViewImp(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableViewImp_m312540334(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::RepositionList()
#define BaseInfiniteListPopulator_1_RepositionList_m977961710(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::UponPostRepositionReq()
#define BaseInfiniteListPopulator_1_UponPostRepositionReq_m948791870(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_InitListItemWithIndex_m2737718792(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, Transform_t3275118058 *, int32_t, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared)(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::PrepareListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m48052893(__this, ___item0, ___newIndex1, ___oldIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, Transform_t3275118058 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::PrepareCarouselListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m3351684144(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, Transform_t3275118058 *, int32_t, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<StatusListItem>::ItemIsInvisible(System.Int32)
#define BaseInfiniteListPopulator_1_ItemIsInvisible_m839865465(__this, ___itemNumber0, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetItemInvisible(Item,System.Boolean)
#define BaseInfiniteListPopulator_1_SetItemInvisible_m262980050(__this, ___item0, ___forward1, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, StatusListItem_t459202613 *, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared)(__this, ___item0, ___forward1, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::UpdateList(System.Int32)
#define BaseInfiniteListPopulator_1_UpdateList_m1247913588(__this, ___itemNumber0, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::UpdateListDirection(BaseInfiniteListPopulator`1/ListMovementDirection<Item>,System.Int32)
#define BaseInfiniteListPopulator_1_UpdateListDirection_m2733730445(__this, ___direction0, ___itemNumber1, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared)(__this, ___direction0, ___itemNumber1, method)
// System.Int32 BaseInfiniteListPopulator`1<StatusListItem>::GetJumpIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetJumpIndexForItem_m605085567(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared)(__this, ___itemDataIndex0, method)
// System.Int32 BaseInfiniteListPopulator`1<StatusListItem>::GetRealIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetRealIndexForItem_m2405277527(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::itemIsPressed(System.Int32,System.Boolean)
#define BaseInfiniteListPopulator_1_itemIsPressed_m3698196195(__this, ___itemDataIndex0, ___isDown1, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, bool, const MethodInfo*))BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared)(__this, ___itemDataIndex0, ___isDown1, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::itemClicked(System.Int32)
#define BaseInfiniteListPopulator_1_itemClicked_m3197054639(__this, ___itemDataIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::StopScrollViewMomentum()
#define BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1333139658(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared)(__this, method)
// UnityEngine.Transform BaseInfiniteListPopulator`1<StatusListItem>::GetItemFromPool(System.Int32)
#define BaseInfiniteListPopulator_1_GetItemFromPool_m1511923716(__this, ___i0, method) ((  Transform_t3275118058 * (*) (BaseInfiniteListPopulator_1_t1018350792 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared)(__this, ___i0, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::RefreshPool()
#define BaseInfiniteListPopulator_1_RefreshPool_m1910219653(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetScrollToBottom()
#define BaseInfiniteListPopulator_1_SetScrollToBottom_m3561978923(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetScrollPosition(System.Single,System.Boolean)
#define BaseInfiniteListPopulator_1_SetScrollPosition_m977424876(__this, ___exactPosition0, ___up1, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, float, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared)(__this, ___exactPosition0, ___up1, method)
// System.Void BaseInfiniteListPopulator`1<StatusListItem>::SetScrollPositionOverTime(System.Single)
#define BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m289920548(__this, ___exactPosition0, method) ((  void (*) (BaseInfiniteListPopulator_1_t1018350792 *, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared)(__this, ___exactPosition0, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<StatusListItem>::SetPanelPosOverTime(System.Single,System.Single)
#define BaseInfiniteListPopulator_1_SetPanelPosOverTime_m2941642805(__this, ___seconds0, ___position1, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t1018350792 *, float, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared)(__this, ___seconds0, ___position1, method)
