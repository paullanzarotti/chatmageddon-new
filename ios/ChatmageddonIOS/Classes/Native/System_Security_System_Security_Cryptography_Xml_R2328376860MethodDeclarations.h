﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.RSAKeyValue
struct RSAKeyValue_t2328376860;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.RSAKeyValue::.ctor()
extern "C"  void RSAKeyValue__ctor_m891161978 (RSAKeyValue_t2328376860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.RSAKeyValue::GetXml()
extern "C"  XmlElement_t2877111883 * RSAKeyValue_GetXml_m2231414525 (RSAKeyValue_t2328376860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.RSAKeyValue::LoadXml(System.Xml.XmlElement)
extern "C"  void RSAKeyValue_LoadXml_m3691336082 (RSAKeyValue_t2328376860 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
