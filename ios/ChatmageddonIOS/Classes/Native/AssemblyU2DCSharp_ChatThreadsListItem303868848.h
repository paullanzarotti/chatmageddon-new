﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatThreadsILP
struct ChatThreadsILP_t2341903446;
// ChatThread
struct ChatThread_t2394323482;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// UISprite
struct UISprite_t603616735;
// OpenPlayerProfileButton
struct OpenPlayerProfileButton_t3248004898;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen1079408212.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatThreadsListItem
struct  ChatThreadsListItem_t303868848  : public BaseInfiniteListItem_1_t1079408212
{
public:
	// ChatThreadsILP ChatThreadsListItem::listPopulator
	ChatThreadsILP_t2341903446 * ___listPopulator_10;
	// ChatThread ChatThreadsListItem::itemThread
	ChatThread_t2394323482 * ___itemThread_11;
	// UILabel ChatThreadsListItem::contatcsLabel
	UILabel_t1795115428 * ___contatcsLabel_12;
	// System.String ChatThreadsListItem::contactsString
	String_t* ___contactsString_13;
	// UILabel ChatThreadsListItem::messageDateLabel
	UILabel_t1795115428 * ___messageDateLabel_14;
	// UILabel ChatThreadsListItem::latestMessageLabel
	UILabel_t1795115428 * ___latestMessageLabel_15;
	// UILabel ChatThreadsListItem::rankLabel
	UILabel_t1795115428 * ___rankLabel_16;
	// UnityEngine.GameObject ChatThreadsListItem::newMessageObject
	GameObject_t1756533147 * ___newMessageObject_17;
	// AvatarUpdater ChatThreadsListItem::avatarUpdater
	AvatarUpdater_t2404165026 * ___avatarUpdater_18;
	// UISprite ChatThreadsListItem::groupChatProfilePic
	UISprite_t603616735 * ___groupChatProfilePic_19;
	// OpenPlayerProfileButton ChatThreadsListItem::playerProfile
	OpenPlayerProfileButton_t3248004898 * ___playerProfile_20;
	// UIScrollView ChatThreadsListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_21;
	// UIScrollView ChatThreadsListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_22;
	// UnityEngine.Transform ChatThreadsListItem::mTrans
	Transform_t3275118058 * ___mTrans_23;
	// UIScrollView ChatThreadsListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_24;
	// System.Boolean ChatThreadsListItem::mAutoFind
	bool ___mAutoFind_25;
	// System.Boolean ChatThreadsListItem::mStarted
	bool ___mStarted_26;

public:
	inline static int32_t get_offset_of_listPopulator_10() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___listPopulator_10)); }
	inline ChatThreadsILP_t2341903446 * get_listPopulator_10() const { return ___listPopulator_10; }
	inline ChatThreadsILP_t2341903446 ** get_address_of_listPopulator_10() { return &___listPopulator_10; }
	inline void set_listPopulator_10(ChatThreadsILP_t2341903446 * value)
	{
		___listPopulator_10 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_10, value);
	}

	inline static int32_t get_offset_of_itemThread_11() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___itemThread_11)); }
	inline ChatThread_t2394323482 * get_itemThread_11() const { return ___itemThread_11; }
	inline ChatThread_t2394323482 ** get_address_of_itemThread_11() { return &___itemThread_11; }
	inline void set_itemThread_11(ChatThread_t2394323482 * value)
	{
		___itemThread_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemThread_11, value);
	}

	inline static int32_t get_offset_of_contatcsLabel_12() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___contatcsLabel_12)); }
	inline UILabel_t1795115428 * get_contatcsLabel_12() const { return ___contatcsLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_contatcsLabel_12() { return &___contatcsLabel_12; }
	inline void set_contatcsLabel_12(UILabel_t1795115428 * value)
	{
		___contatcsLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___contatcsLabel_12, value);
	}

	inline static int32_t get_offset_of_contactsString_13() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___contactsString_13)); }
	inline String_t* get_contactsString_13() const { return ___contactsString_13; }
	inline String_t** get_address_of_contactsString_13() { return &___contactsString_13; }
	inline void set_contactsString_13(String_t* value)
	{
		___contactsString_13 = value;
		Il2CppCodeGenWriteBarrier(&___contactsString_13, value);
	}

	inline static int32_t get_offset_of_messageDateLabel_14() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___messageDateLabel_14)); }
	inline UILabel_t1795115428 * get_messageDateLabel_14() const { return ___messageDateLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_messageDateLabel_14() { return &___messageDateLabel_14; }
	inline void set_messageDateLabel_14(UILabel_t1795115428 * value)
	{
		___messageDateLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___messageDateLabel_14, value);
	}

	inline static int32_t get_offset_of_latestMessageLabel_15() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___latestMessageLabel_15)); }
	inline UILabel_t1795115428 * get_latestMessageLabel_15() const { return ___latestMessageLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_latestMessageLabel_15() { return &___latestMessageLabel_15; }
	inline void set_latestMessageLabel_15(UILabel_t1795115428 * value)
	{
		___latestMessageLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___latestMessageLabel_15, value);
	}

	inline static int32_t get_offset_of_rankLabel_16() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___rankLabel_16)); }
	inline UILabel_t1795115428 * get_rankLabel_16() const { return ___rankLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_16() { return &___rankLabel_16; }
	inline void set_rankLabel_16(UILabel_t1795115428 * value)
	{
		___rankLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_16, value);
	}

	inline static int32_t get_offset_of_newMessageObject_17() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___newMessageObject_17)); }
	inline GameObject_t1756533147 * get_newMessageObject_17() const { return ___newMessageObject_17; }
	inline GameObject_t1756533147 ** get_address_of_newMessageObject_17() { return &___newMessageObject_17; }
	inline void set_newMessageObject_17(GameObject_t1756533147 * value)
	{
		___newMessageObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___newMessageObject_17, value);
	}

	inline static int32_t get_offset_of_avatarUpdater_18() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___avatarUpdater_18)); }
	inline AvatarUpdater_t2404165026 * get_avatarUpdater_18() const { return ___avatarUpdater_18; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatarUpdater_18() { return &___avatarUpdater_18; }
	inline void set_avatarUpdater_18(AvatarUpdater_t2404165026 * value)
	{
		___avatarUpdater_18 = value;
		Il2CppCodeGenWriteBarrier(&___avatarUpdater_18, value);
	}

	inline static int32_t get_offset_of_groupChatProfilePic_19() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___groupChatProfilePic_19)); }
	inline UISprite_t603616735 * get_groupChatProfilePic_19() const { return ___groupChatProfilePic_19; }
	inline UISprite_t603616735 ** get_address_of_groupChatProfilePic_19() { return &___groupChatProfilePic_19; }
	inline void set_groupChatProfilePic_19(UISprite_t603616735 * value)
	{
		___groupChatProfilePic_19 = value;
		Il2CppCodeGenWriteBarrier(&___groupChatProfilePic_19, value);
	}

	inline static int32_t get_offset_of_playerProfile_20() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___playerProfile_20)); }
	inline OpenPlayerProfileButton_t3248004898 * get_playerProfile_20() const { return ___playerProfile_20; }
	inline OpenPlayerProfileButton_t3248004898 ** get_address_of_playerProfile_20() { return &___playerProfile_20; }
	inline void set_playerProfile_20(OpenPlayerProfileButton_t3248004898 * value)
	{
		___playerProfile_20 = value;
		Il2CppCodeGenWriteBarrier(&___playerProfile_20, value);
	}

	inline static int32_t get_offset_of_scrollView_21() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___scrollView_21)); }
	inline UIScrollView_t3033954930 * get_scrollView_21() const { return ___scrollView_21; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_21() { return &___scrollView_21; }
	inline void set_scrollView_21(UIScrollView_t3033954930 * value)
	{
		___scrollView_21 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_21, value);
	}

	inline static int32_t get_offset_of_draggablePanel_22() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___draggablePanel_22)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_22() const { return ___draggablePanel_22; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_22() { return &___draggablePanel_22; }
	inline void set_draggablePanel_22(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_22 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_22, value);
	}

	inline static int32_t get_offset_of_mTrans_23() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___mTrans_23)); }
	inline Transform_t3275118058 * get_mTrans_23() const { return ___mTrans_23; }
	inline Transform_t3275118058 ** get_address_of_mTrans_23() { return &___mTrans_23; }
	inline void set_mTrans_23(Transform_t3275118058 * value)
	{
		___mTrans_23 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_23, value);
	}

	inline static int32_t get_offset_of_mScroll_24() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___mScroll_24)); }
	inline UIScrollView_t3033954930 * get_mScroll_24() const { return ___mScroll_24; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_24() { return &___mScroll_24; }
	inline void set_mScroll_24(UIScrollView_t3033954930 * value)
	{
		___mScroll_24 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_24, value);
	}

	inline static int32_t get_offset_of_mAutoFind_25() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___mAutoFind_25)); }
	inline bool get_mAutoFind_25() const { return ___mAutoFind_25; }
	inline bool* get_address_of_mAutoFind_25() { return &___mAutoFind_25; }
	inline void set_mAutoFind_25(bool value)
	{
		___mAutoFind_25 = value;
	}

	inline static int32_t get_offset_of_mStarted_26() { return static_cast<int32_t>(offsetof(ChatThreadsListItem_t303868848, ___mStarted_26)); }
	inline bool get_mStarted_26() const { return ___mStarted_26; }
	inline bool* get_address_of_mStarted_26() { return &___mStarted_26; }
	inline void set_mStarted_26(bool value)
	{
		___mStarted_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
