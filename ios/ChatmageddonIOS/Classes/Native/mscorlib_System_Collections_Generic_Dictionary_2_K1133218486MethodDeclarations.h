﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2034921087MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2838337400(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1133218486 *, Dictionary_2_t2944688011 *, const MethodInfo*))KeyCollection__ctor_m3725338696_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3338958334(__this, ___item0, method) ((  void (*) (KeyCollection_t1133218486 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3774205578_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m955256759(__this, method) ((  void (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m856998823_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2430805558(__this, ___item0, method) ((  bool (*) (KeyCollection_t1133218486 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2636661350_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2727817463(__this, ___item0, method) ((  bool (*) (KeyCollection_t1133218486 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m874614855_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1405078985(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1391786117_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3151135949(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1133218486 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m103082201_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2889009158(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14508810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3501140323(__this, method) ((  bool (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m891212851_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2123929361(__this, method) ((  bool (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m931222373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1450214365(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1071395449_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3954630311(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1133218486 *, PanelTypeU5BU5D_t884548027*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3388582647_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2108732140(__this, method) ((  Enumerator_t1339224153  (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_GetEnumerator_m3504578684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,Panel>::get_Count()
#define KeyCollection_get_Count_m371254513(__this, method) ((  int32_t (*) (KeyCollection_t1133218486 *, const MethodInfo*))KeyCollection_get_Count_m3858243205_gshared)(__this, method)
