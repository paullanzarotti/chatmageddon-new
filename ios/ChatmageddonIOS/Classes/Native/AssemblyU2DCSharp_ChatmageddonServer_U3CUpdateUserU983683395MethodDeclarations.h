﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserUsername>c__AnonStorey10
struct U3CUpdateUserUsernameU3Ec__AnonStorey10_t983683395;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserUsername>c__AnonStorey10::.ctor()
extern "C"  void U3CUpdateUserUsernameU3Ec__AnonStorey10__ctor_m59539464 (U3CUpdateUserUsernameU3Ec__AnonStorey10_t983683395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserUsername>c__AnonStorey10::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserUsernameU3Ec__AnonStorey10_U3CU3Em__0_m901899339 (U3CUpdateUserUsernameU3Ec__AnonStorey10_t983683395 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
