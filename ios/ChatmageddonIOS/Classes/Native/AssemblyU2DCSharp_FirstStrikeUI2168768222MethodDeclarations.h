﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FirstStrikeUI
struct FirstStrikeUI_t2168768222;

#include "codegen/il2cpp-codegen.h"

// System.Void FirstStrikeUI::.ctor()
extern "C"  void FirstStrikeUI__ctor_m1576034751 (FirstStrikeUI_t2168768222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstStrikeUI::SetFirstStrikeActive(System.Int32)
extern "C"  void FirstStrikeUI_SetFirstStrikeActive_m691628466 (FirstStrikeUI_t2168768222 * __this, int32_t ___firstStrikePoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstStrikeUI::SetUIDepth(System.Int32)
extern "C"  void FirstStrikeUI_SetUIDepth_m2205123085 (FirstStrikeUI_t2168768222 * __this, int32_t ___baseDepth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
