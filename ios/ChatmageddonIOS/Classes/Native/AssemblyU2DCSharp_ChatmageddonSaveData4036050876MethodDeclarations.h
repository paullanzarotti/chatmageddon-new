﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonSaveData
struct ChatmageddonSaveData_t4036050876;
// System.String
struct String_t;
// System.Collections.Generic.List`1<ValidContact>
struct List_1_t849036066;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "mscorlib_System_Nullable_1_gen1447017461.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "mscorlib_System_Nullable_1_gen1881137068.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void ChatmageddonSaveData::.ctor()
extern "C"  void ChatmageddonSaveData__ctor_m1227521283 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::Init()
extern "C"  void ChatmageddonSaveData_Init_m791418481 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::ClearAllData()
extern "C"  void ChatmageddonSaveData_ClearAllData_m3786489899 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveUDID(System.String)
extern "C"  void ChatmageddonSaveData_SaveUDID_m1005654716 (ChatmageddonSaveData_t4036050876 * __this, String_t* ___udid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatmageddonSaveData::LoadUDID()
extern "C"  String_t* ChatmageddonSaveData_LoadUDID_m2359655542 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveMusicSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveMusicSetting_m2160558746 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadMusicSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadMusicSetting_m2347102483 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveSFXSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveSFXSetting_m1276102758 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadSFXSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadSFXSetting_m265605591 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveInGameNotificationSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveInGameNotificationSetting_m3384945507 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadInGameNotificationSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadInGameNotificationSetting_m543259318 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveInGameNotificationSoundSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveInGameNotificationSoundSetting_m2531558876 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadInGameNotificationSoundSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadInGameNotificationSoundSetting_m2738895453 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveInGameNotificationVibrationSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveInGameNotificationVibrationSetting_m2710775761 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadInGameNotificationVibrationSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadInGameNotificationVibrationSetting_m1219294738 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveDisplayLocationSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveDisplayLocationSetting_m2788216152 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadDisplayLocationSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadDisplayLocationSetting_m3618334265 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveAudienceSetting(Audience)
extern "C"  void ChatmageddonSaveData_SaveAudienceSetting_m388088788 (ChatmageddonSaveData_t4036050876 * __this, int32_t ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Audience> ChatmageddonSaveData::LoadAudienceSetting()
extern "C"  Nullable_1_t1447017461  ChatmageddonSaveData_LoadAudienceSetting_m3811864853 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveGenderSetting(Gender)
extern "C"  void ChatmageddonSaveData_SaveGenderSetting_m2672079732 (ChatmageddonSaveData_t4036050876 * __this, int32_t ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Gender> ChatmageddonSaveData::LoadGenderSetting()
extern "C"  Nullable_1_t1881137068  ChatmageddonSaveData_LoadGenderSetting_m571006197 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveRegistrationSetting(System.Boolean)
extern "C"  void ChatmageddonSaveData_SaveRegistrationSetting_m413524098 (ChatmageddonSaveData_t4036050876 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> ChatmageddonSaveData::LoadRegistrationSetting()
extern "C"  Nullable_1_t2088641033  ChatmageddonSaveData_LoadRegistrationSetting_m3550146495 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveValidContactsList(System.Collections.Generic.List`1<ValidContact>)
extern "C"  void ChatmageddonSaveData_SaveValidContactsList_m2579602157 (ChatmageddonSaveData_t4036050876 * __this, List_1_t849036066 * ___contactList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ValidContact> ChatmageddonSaveData::LoadValidContactsList()
extern "C"  List_1_t849036066 * ChatmageddonSaveData_LoadValidContactsList_m801128025 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveAddressBookChecksum(System.String)
extern "C"  void ChatmageddonSaveData_SaveAddressBookChecksum_m2505752740 (ChatmageddonSaveData_t4036050876 * __this, String_t* ___checksum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatmageddonSaveData::GetAddressBookChecksum()
extern "C"  String_t* ChatmageddonSaveData_GetAddressBookChecksum_m2757985068 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonSaveData::SaveFirstPlayDateSetting(System.DateTime)
extern "C"  void ChatmageddonSaveData_SaveFirstPlayDateSetting_m2642196176 (ChatmageddonSaveData_t4036050876 * __this, DateTime_t693205669  ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatmageddonSaveData::LoadFirstPlayDateSetting()
extern "C"  String_t* ChatmageddonSaveData_LoadFirstPlayDateSetting_m3368519874 (ChatmageddonSaveData_t4036050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
