﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Drunk2
struct CameraFilterPack_FX_Drunk2_t3160974908;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Drunk2::.ctor()
extern "C"  void CameraFilterPack_FX_Drunk2__ctor_m478094041 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Drunk2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Drunk2_get_material_m1578619058 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::Start()
extern "C"  void CameraFilterPack_FX_Drunk2_Start_m223264137 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Drunk2_OnRenderImage_m1854508417 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::OnValidate()
extern "C"  void CameraFilterPack_FX_Drunk2_OnValidate_m682542148 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::Update()
extern "C"  void CameraFilterPack_FX_Drunk2_Update_m2842734378 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::OnDisable()
extern "C"  void CameraFilterPack_FX_Drunk2_OnDisable_m3322378604 (CameraFilterPack_FX_Drunk2_t3160974908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
