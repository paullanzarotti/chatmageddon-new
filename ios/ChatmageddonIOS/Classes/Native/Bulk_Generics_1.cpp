﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Facebook.Unity.MethodCall`1<System.Object>
struct MethodCall_1_t2451378470;
// Facebook.Unity.FacebookBase
struct FacebookBase_t2463540723;
// System.String
struct String_t;
// Facebook.Unity.FacebookDelegate`1<System.Object>
struct FacebookDelegate_1_t1724794744;
// Facebook.Unity.MethodArguments
struct MethodArguments_t735664811;
// Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>
struct JavaMethodCall_1_t2462209892;
// Facebook.Unity.Mobile.Android.AndroidFacebook
struct AndroidFacebook_t3038444049;
// Facebook.Unity.Utilities/Callback`1<System.Object>
struct Callback_1_t1712464442;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>
struct U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283;
// FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>
struct U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;
// FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>
struct U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354;
// FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>
struct U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707;
// FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>
struct U3CPostHighScoreU3Ec__AnonStorey2_t429984188;
// Facebook.Unity.ILoginResult
struct ILoginResult_t403585443;
// FBManager`1/LoadPictureCallback<System.Object>
struct LoadPictureCallback_t2854975038;
// UnityEngine.Texture
struct Texture_t2243626319;
// FBManager`1<System.Object>
struct FBManager_1_t2091981452;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// FacebookPlayer
struct FacebookPlayer_t2645999341;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t278961118;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Facebook.Unity.IShareResult
struct IShareResult_t830127229;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// GameState`1<System.Object>
struct GameState_1_t846809420;
// LitJson.ExporterFunc`1<System.Object>
struct ExporterFunc_1_t3926562242;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// LitJson.ImporterFunc`2<System.Object,System.Object>
struct ImporterFunc_2_t2548729379;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey0`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey0_1_t3447138843;
// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey1_2_t1381013311;
// MonoSingleton`1<System.Object>
struct MonoSingleton_1_t2440115015;
// NavigationController`2<System.Object,AttackHomeNav>
struct NavigationController_2_t1832792260;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// NavigationController`2<System.Object,AttackNavScreen>
struct NavigationController_2_t2219628511;
// NavigationController`2<System.Object,AttackSearchNav>
struct NavigationController_2_t2145786393;
// NavigationController`2<System.Object,ChatNavScreen>
struct NavigationController_2_t2225246313;
// NavigationController`2<System.Object,DefendNavScreen>
struct NavigationController_2_t1726521459;
// NavigationController`2<System.Object,FriendHomeNavScreen>
struct NavigationController_2_t3781907348;
// NavigationController`2<System.Object,FriendNavScreen>
struct NavigationController_2_t63285209;
// NavigationController`2<System.Object,FriendSearchNavScreen>
struct NavigationController_2_t906290919;
// NavigationController`2<System.Object,LeaderboardNavScreen>
struct NavigationController_2_t243945468;
// NavigationController`2<System.Object,PhoneNumberNavScreen>
struct NavigationController_2_t3446336302;
// NavigationController`2<System.Object,PlayerProfileNavScreen>
struct NavigationController_2_t548570015;
// NavigationController`2<System.Object,ProfileNavScreen>
struct NavigationController_2_t3309526906;
// NavigationController`2<System.Object,RegNavScreen>
struct NavigationController_2_t2850042411;
// NavigationController`2<System.Object,SettingsNavScreen>
struct NavigationController_2_t155028990;
// NavigationController`2<System.Object,StatusNavScreen>
struct NavigationController_2_t760270839;
// NavigationController`2<System.Object,System.Object>
struct NavigationController_2_t577351051;
// PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>
struct U3CSwitchPanelsU3Ec__Iterator0_t2843932824;
// PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>
struct U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628;
// PanelContainer
struct PanelContainer_t1029301983;
// PanelTransitionManager`1<System.Object>
struct PanelTransitionManager_1_t3816465789;
// Panel
struct Panel_t1787746694;
// TweenPosition
struct TweenPosition_t1144714832;
// PhoneNumbers.EnumerableFromConstructor`1<System.Object>
struct EnumerableFromConstructor_1_t2022230559;
// System.Func`1<System.Collections.Generic.IEnumerator`1<System.Object>>
struct Func_1_t2119365804;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>
struct ObservableDictionary_2_t4244038468;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t280592844;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler
struct NotifyCollectionChangedEventHandler_t1310059589;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t330981690;
// System.Collections.IList
struct IList_t3321498491;
// Prime31.Reflection.SafeDictionary`2<System.Object,System.Object>
struct SafeDictionary_2_t1052943346;
// System.Action`1<AttackHomeNav>
struct Action_1_t3746689886;
// System.Action`1<AttackNavScreen>
struct Action_1_t4133526137;
// System.Action`1<AttackSearchNav>
struct Action_1_t4059684019;
// System.Action`1<ChatNavScreen>
struct Action_1_t4139143939;
// System.Action`1<DefendNavScreen>
struct Action_1_t3640419085;
// System.Action`1<FriendHomeNavScreen>
struct Action_1_t1400837678;
// System.Action`1<FriendNavScreen>
struct Action_1_t1977182835;
// System.Action`1<FriendSearchNavScreen>
struct Action_1_t2820188545;
// System.Action`1<LeaderboardNavScreen>
struct Action_1_t2157843094;
// System.Action`1<LitJson.PropertyMetadata>
struct Action_1_t3495625518;
// System.Action`1<PhoneNumberNavScreen>
struct Action_1_t1065266632;
// System.Action`1<PlayerProfileNavScreen>
struct Action_1_t2462467641;
// System.Action`1<ProfileNavScreen>
struct Action_1_t928457236;
// System.Action`1<RegNavScreen>
struct Action_1_t468972741;
// System.Action`1<SettingsNavScreen>
struct Action_1_t2068926616;
// System.Action`1<StatusNavScreen>
struct Action_1_t2674168465;
// System.Action`1<System.Byte>
struct Action_1_t3484903818;
// System.Action`1<System.Char>
struct Action_1_t3256280720;
// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t4135621323;
// System.Action`1<System.Int16>
struct Action_1_t3843045296;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Nullable`1<System.DateTime>>
struct Action_1_t3053038662;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Reflection.CustomAttributeNamedArgument>
struct Action_1_t4190924221;
// System.Action`1<System.Reflection.CustomAttributeTypedArgument>
struct Action_1_t1299997296;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<UnibillError>
struct Action_1_t1555659169;
// System.Action`1<UnibillState>
struct Action_1_t4073934390;
// System.Action`1<UnityEngine.Color>
struct Action_1_t1822191457;
// System.Action`1<UnityEngine.Color32>
struct Action_1_t676316900;
// System.Action`1<UnityEngine.EventSystems.RaycastResult>
struct Action_1_t4117953054;
// System.Action`1<UnityEngine.Experimental.Director.Playable>
struct Action_1_t3469344930;
// System.Action`1<UnityEngine.RuntimePlatform>
struct Action_1_t1671384349;
// System.Action`1<UnityEngine.UICharInfo>
struct Action_1_t2858436182;
// System.Action`1<UnityEngine.UILineInfo>
struct Action_1_t3423077256;
// System.Action`1<UnityEngine.UIVertex>
struct Action_1_t1006058200;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2045506961;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t2045506962;
// System.Action`1<UnityEngine.Vector4>
struct Action_1_t2045506963;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Object,System.Int32>
struct Action_2_t1954480006;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct Action_2_t3492767325;
// System.Action`3<System.Boolean,System.Object,System.Object>
struct Action_3_t2202283254;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t1115657183;
// System.Action`3<System.Object,System.UInt16,System.Object>
struct Action_3_t28289771;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Exception
struct Exception_t1927440687;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1279844890;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Facebook_Unity_Facebook_Unity_MethodCall_1_gen2451378470.h"
#include "Facebook_Unity_Facebook_Unity_MethodCall_1_gen2451378470MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookBase2463540723.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_MethodArguments735664811MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_MethodArguments735664811.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1724794744.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro2462209892.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro2462209892MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro3038444049.h"
#include "Facebook_Unity_Facebook_Unity_FacebookBase2463540723MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro3038444049MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_CallbackManager4242095184.h"
#include "Facebook_Unity_Facebook_Unity_CallbackManager4242095184MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_Utilities_Callback_11712464442.h"
#include "Facebook_Unity_Facebook_Unity_Utilities_Callback_11712464442MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CCheckScoreUpdateU2193426283.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CCheckScoreUpdateU2193426283MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_FBManager_1_gen2091981452.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CGetAppFriendsScore816147105.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CGetAppFriendsScore816147105MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSON3623987362MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen278961118MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_Generic_List_1_gen278961118.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "AssemblyU2DCSharp_FBManager_1_gen2091981452MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CLoadPictureAPIU3Ec_17207354.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CLoadPictureAPIU3Ec_17207354MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "AssemblyU2DCSharp_FBManager_1_LoadPictureCallback_2854975038.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CLoadPictureEnumer2129548707.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CLoadPictureEnumer2129548707MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_FBManager_1_LoadPictureCallback_2854975038MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CPostHighScoreU3Ec_429984188.h"
#include "AssemblyU2DCSharp_FBManager_1_U3CPostHighScoreU3Ec_429984188MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FB1612658294MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_AccessToken2518141643.h"
#include "AssemblyU2DCSharp_FacebookPlayer2645999341MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharp_FacebookPlayer2645999341.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"
#include "AssemblyU2DCSharp_InternetReachability3219127626MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_InitDelegate3410465555MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_HideUnityDelegate712804158MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_InitDelegate3410465555.h"
#include "Facebook_Unity_Facebook_Unity_HideUnityDelegate712804158.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginEntrance2243089009.h"
#include "Facebook_Unity_Facebook_Unity_AccessToken2518141643MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3733898188MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3733898188.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3020292135MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3020292135.h"
#include "Facebook_Unity_Facebook_Unity_HttpMethod3673888207.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen334943763MethodDeclarations.h"
#include "AssemblyU2DCSharp_FacebookUser244724091MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g4160439974MethodDeclarations.h"
#include "AssemblyU2DCSharp_FacebookStory448600801.h"
#include "System_System_Uri19570940.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g4160439974.h"
#include "AssemblyU2DCSharp_FacebookStory448600801MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "Facebook_Unity_Facebook_MiniJSON_Json151732106MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "AssemblyU2DCSharp_GameState_1_gen846809420.h"
#include "AssemblyU2DCSharp_GameState_1_gen846809420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "mscorlib_System_GC2902933594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3926562242.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3926562242MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2548729379.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2548729379MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3447138843.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3447138843MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm1381013311.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm1381013311MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen1832792260.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen1832792260MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3314011636.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3314011636MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2219628511.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2219628511MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3700847887.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3700847887MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2145786393.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2145786393MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3627005769.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3627005769MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2225246313.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2225246313MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3706465689.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3706465689MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen1726521459.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen1726521459MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3207740835.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3207740835MethodDeclarations.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3781907348.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3781907348MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen968159428.h"
#include "mscorlib_System_Collections_Generic_List_1_gen968159428MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen63285209.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen63285209MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1544504585.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1544504585MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen906290919.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen906290919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2387510295.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2387510295MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen243945468.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen243945468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1725164844.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1725164844MethodDeclarations.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3446336302.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3446336302MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen632588382.h"
#include "mscorlib_System_Collections_Generic_List_1_gen632588382MethodDeclarations.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen548570015.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen548570015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2029789391.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2029789391MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3309526906.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3309526906MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen495778986.h"
#include "mscorlib_System_Collections_Generic_List_1_gen495778986MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2850042411.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2850042411MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen36294491.h"
#include "mscorlib_System_Collections_Generic_List_1_gen36294491MethodDeclarations.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen155028990.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen155028990MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1636248366.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1636248366MethodDeclarations.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen760270839.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen760270839MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2241490215.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2241490215MethodDeclarations.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen577351051.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen577351051MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_U3CSwit2843932824.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_U3CSwit2843932824MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_gen3816465789.h"
#include "AssemblyU2DCSharp_Panel1787746694.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_gen3816465789MethodDeclarations.h"
#include "AssemblyU2DCSharp_Panel1787746694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_U3CTwee3352052628.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_U3CTwee3352052628MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener2986641582MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1156867826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_TweenPosition1144714832.h"
#include "AssemblyU2DCSharp_PanelContainer1029301983.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1156867826.h"
#include "AssemblyU2DCSharp_UITweener2986641582.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2944688011MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2944688011.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_702033233.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4264712713.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4264712713MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_702033233MethodDeclarations.h"
#include "AssemblyU2DCSharp_PhoneNumbers_EnumerableFromConst2022230559.h"
#include "AssemblyU2DCSharp_PhoneNumbers_EnumerableFromConst2022230559MethodDeclarations.h"
#include "System_Core_System_Func_1_gen2119365804.h"
#include "System_Core_System_Func_1_gen2119365804MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje4244038468.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje4244038468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje1310059589.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2640700633.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432MethodDeclarations.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059MethodDeclarations.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec3926133854MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje1310059589MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec3926133854.h"
#include "P31RestKit_Prime31_Reflection_SafeDictionary_2_gen1052943346.h"
#include "P31RestKit_Prime31_Reflection_SafeDictionary_2_gen1052943346MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3746689886.h"
#include "mscorlib_System_Action_1_gen3746689886MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4133526137.h"
#include "mscorlib_System_Action_1_gen4133526137MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4059684019.h"
#include "mscorlib_System_Action_1_gen4059684019MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4139143939.h"
#include "mscorlib_System_Action_1_gen4139143939MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3640419085.h"
#include "mscorlib_System_Action_1_gen3640419085MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1400837678.h"
#include "mscorlib_System_Action_1_gen1400837678MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1977182835.h"
#include "mscorlib_System_Action_1_gen1977182835MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2820188545.h"
#include "mscorlib_System_Action_1_gen2820188545MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2157843094.h"
#include "mscorlib_System_Action_1_gen2157843094MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3495625518.h"
#include "mscorlib_System_Action_1_gen3495625518MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_System_Action_1_gen1065266632.h"
#include "mscorlib_System_Action_1_gen1065266632MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2462467641.h"
#include "mscorlib_System_Action_1_gen2462467641MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen928457236.h"
#include "mscorlib_System_Action_1_gen928457236MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen468972741.h"
#include "mscorlib_System_Action_1_gen468972741MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2068926616.h"
#include "mscorlib_System_Action_1_gen2068926616MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2674168465.h"
#include "mscorlib_System_Action_1_gen2674168465MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3484903818.h"
#include "mscorlib_System_Action_1_gen3484903818MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Action_1_gen3256280720.h"
#include "mscorlib_System_Action_1_gen3256280720MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Action_1_gen4135621323.h"
#include "mscorlib_System_Action_1_gen4135621323MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3843045296.h"
#include "mscorlib_System_Action_1_gen3843045296MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Action_1_gen1873676830.h"
#include "mscorlib_System_Action_1_gen1873676830MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3053038662.h"
#include "mscorlib_System_Action_1_gen3053038662MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4190924221.h"
#include "mscorlib_System_Action_1_gen4190924221MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Action_1_gen1299997296.h"
#include "mscorlib_System_Action_1_gen1299997296MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Action_1_gen1878309314.h"
#include "mscorlib_System_Action_1_gen1878309314MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1555659169.h"
#include "mscorlib_System_Action_1_gen1555659169MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"
#include "mscorlib_System_Action_1_gen4073934390.h"
#include "mscorlib_System_Action_1_gen4073934390MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillState4272135008.h"
#include "mscorlib_System_Action_1_gen1822191457.h"
#include "mscorlib_System_Action_1_gen1822191457MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Action_1_gen676316900.h"
#include "mscorlib_System_Action_1_gen676316900MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Action_1_gen4117953054.h"
#include "mscorlib_System_Action_1_gen4117953054MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Action_1_gen3469344930.h"
#include "mscorlib_System_Action_1_gen3469344930MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Action_1_gen1671384349.h"
#include "mscorlib_System_Action_1_gen1671384349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Action_1_gen2858436182.h"
#include "mscorlib_System_Action_1_gen2858436182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Action_1_gen3423077256.h"
#include "mscorlib_System_Action_1_gen3423077256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Action_1_gen1006058200.h"
#include "mscorlib_System_Action_1_gen1006058200MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Action_1_gen2045506961.h"
#include "mscorlib_System_Action_1_gen2045506961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Action_1_gen2045506962.h"
#include "mscorlib_System_Action_1_gen2045506962MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2045506963.h"
#include "mscorlib_System_Action_1_gen2045506963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1954480006.h"
#include "System_Core_System_Action_2_gen1954480006MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"
#include "System_Core_System_Action_2_gen3492767325.h"
#include "System_Core_System_Action_2_gen3492767325MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2202283254.h"
#include "System_Core_System_Action_3_gen2202283254MethodDeclarations.h"
#include "System_Core_System_Action_3_gen1115657183.h"
#include "System_Core_System_Action_3_gen1115657183MethodDeclarations.h"
#include "System_Core_System_Action_3_gen28289771.h"
#include "System_Core_System_Action_3_gen28289771MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen508675470.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen508675470MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen895511721.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen895511721MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen821669603.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen821669603MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1483032230.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1483032230MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_ClientM624279968.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen901129523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen901129523MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen402404669.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen402404669MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2457790558.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2457790558MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3034135715.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3034135715MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3877141425.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3877141425MethodDeclarations.h"

// System.String Facebook.Unity.CallbackManager::AddFacebookDelegate<System.Object>(Facebook.Unity.FacebookDelegate`1<T>)
extern "C"  String_t* CallbackManager_AddFacebookDelegate_TisIl2CppObject_m2534176915_gshared (CallbackManager_t4242095184 * __this, FacebookDelegate_1_t1724794744 * ___callback0, const MethodInfo* method);
#define CallbackManager_AddFacebookDelegate_TisIl2CppObject_m2534176915(__this, ___callback0, method) ((  String_t* (*) (CallbackManager_t4242095184 *, FacebookDelegate_1_t1724794744 *, const MethodInfo*))CallbackManager_AddFacebookDelegate_TisIl2CppObject_m2534176915_gshared)(__this, ___callback0, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C"  bool Enumerable_Contains_TisIl2CppObject_m2438455970_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Enumerable_Contains_TisIl2CppObject_m2438455970(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2438455970_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Contains<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define Enumerable_Contains_TisString_t_m2925925544(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2438455970_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4011380260_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4011380260(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4011380260_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PanelContainer>()
#define GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472(__this, method) ((  PanelContainer_t1029301983 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisNotifyCollectionChangedEventHandler_t1310059589_m3604167515(__this /* static, unused */, p0, p1, p2, method) ((  NotifyCollectionChangedEventHandler_t1310059589 * (*) (Il2CppObject * /* static, unused */, NotifyCollectionChangedEventHandler_t1310059589 **, NotifyCollectionChangedEventHandler_t1310059589 *, NotifyCollectionChangedEventHandler_t1310059589 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.ComponentModel.PropertyChangedEventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisPropertyChangedEventHandler_t3042952059_m221271957(__this /* static, unused */, p0, p1, p2, method) ((  PropertyChangedEventHandler_t3042952059 * (*) (Il2CppObject * /* static, unused */, PropertyChangedEventHandler_t3042952059 **, PropertyChangedEventHandler_t3042952059 *, PropertyChangedEventHandler_t3042952059 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m2924442608_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m2924442608(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m2924442608_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  KeyValuePair_2U5BU5D_t2854920344* Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193(__this /* static, unused */, p0, method) ((  KeyValuePair_2U5BU5D_t2854920344* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2032877681_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2032877681(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2032877681_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<AttackHomeNav>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AttackNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AttackSearchNav>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<BestHTTP.SignalR.Messages.ClientMessage>(System.Int32)
extern "C"  ClientMessage_t624279968  Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756(__this, p0, method) ((  ClientMessage_t624279968  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ChatNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<DefendNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FriendHomeNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FriendNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FriendSearchNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.MethodCall`1<System.Object>::.ctor(Facebook.Unity.FacebookBase,System.String)
extern Il2CppClass* MethodArguments_t735664811_il2cpp_TypeInfo_var;
extern const uint32_t MethodCall_1__ctor_m66515861_MetadataUsageId;
extern "C"  void MethodCall_1__ctor_m66515861_gshared (MethodCall_1_t2451378470 * __this, FacebookBase_t2463540723 * ___facebookImpl0, String_t* ___methodName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodCall_1__ctor_m66515861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		MethodArguments_t735664811 * L_0 = (MethodArguments_t735664811 *)il2cpp_codegen_object_new(MethodArguments_t735664811_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m2626086966(L_0, /*hidden argument*/NULL);
		NullCheck((MethodCall_1_t2451378470 *)__this);
		((  void (*) (MethodCall_1_t2451378470 *, MethodArguments_t735664811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MethodCall_1_t2451378470 *)__this, (MethodArguments_t735664811 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		FacebookBase_t2463540723 * L_1 = ___facebookImpl0;
		NullCheck((MethodCall_1_t2451378470 *)__this);
		((  void (*) (MethodCall_1_t2451378470 *, FacebookBase_t2463540723 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((MethodCall_1_t2451378470 *)__this, (FacebookBase_t2463540723 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		String_t* L_2 = ___methodName1;
		NullCheck((MethodCall_1_t2451378470 *)__this);
		((  void (*) (MethodCall_1_t2451378470 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MethodCall_1_t2451378470 *)__this, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.String Facebook.Unity.MethodCall`1<System.Object>::get_MethodName()
extern "C"  String_t* MethodCall_1_get_MethodName_m3799188881_gshared (MethodCall_1_t2451378470 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CMethodNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_MethodName(System.String)
extern "C"  void MethodCall_1_set_MethodName_m3736187060_gshared (MethodCall_1_t2451378470 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMethodNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<System.Object>::get_Callback()
extern "C"  FacebookDelegate_1_t1724794744 * MethodCall_1_get_Callback_m822057729_gshared (MethodCall_1_t2451378470 * __this, const MethodInfo* method)
{
	{
		FacebookDelegate_1_t1724794744 * L_0 = (FacebookDelegate_1_t1724794744 *)__this->get_U3CCallbackU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
extern "C"  void MethodCall_1_set_Callback_m2868801814_gshared (MethodCall_1_t2451378470 * __this, FacebookDelegate_1_t1724794744 * ___value0, const MethodInfo* method)
{
	{
		FacebookDelegate_1_t1724794744 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_FacebookImpl(Facebook.Unity.FacebookBase)
extern "C"  void MethodCall_1_set_FacebookImpl_m2153249042_gshared (MethodCall_1_t2451378470 * __this, FacebookBase_t2463540723 * ___value0, const MethodInfo* method)
{
	{
		FacebookBase_t2463540723 * L_0 = ___value0;
		__this->set_U3CFacebookImplU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Parameters(Facebook.Unity.MethodArguments)
extern "C"  void MethodCall_1_set_Parameters_m2813365498_gshared (MethodCall_1_t2451378470 * __this, MethodArguments_t735664811 * ___value0, const MethodInfo* method)
{
	{
		MethodArguments_t735664811 * L_0 = ___value0;
		__this->set_U3CParametersU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
extern "C"  void JavaMethodCall_1__ctor_m346820143_gshared (JavaMethodCall_1_t2462209892 * __this, AndroidFacebook_t3038444049 * ___androidImpl0, String_t* ___methodName1, const MethodInfo* method)
{
	{
		AndroidFacebook_t3038444049 * L_0 = ___androidImpl0;
		String_t* L_1 = ___methodName1;
		NullCheck((MethodCall_1_t2451378470 *)__this);
		((  void (*) (MethodCall_1_t2451378470 *, FacebookBase_t2463540723 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MethodCall_1_t2451378470 *)__this, (FacebookBase_t2463540723 *)L_0, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		AndroidFacebook_t3038444049 * L_2 = ___androidImpl0;
		__this->set_androidImpl_4(L_2);
		return;
	}
}
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern Il2CppClass* MethodArguments_t735664811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2736671527;
extern const uint32_t JavaMethodCall_1_Call_m2069001670_MetadataUsageId;
extern "C"  void JavaMethodCall_1_Call_m2069001670_gshared (JavaMethodCall_1_t2462209892 * __this, MethodArguments_t735664811 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JavaMethodCall_1_Call_m2069001670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodArguments_t735664811 * V_0 = NULL;
	{
		MethodArguments_t735664811 * L_0 = ___args0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MethodArguments_t735664811 * L_1 = (MethodArguments_t735664811 *)il2cpp_codegen_object_new(MethodArguments_t735664811_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m2626086966(L_1, /*hidden argument*/NULL);
		V_0 = (MethodArguments_t735664811 *)L_1;
		goto IL_0018;
	}

IL_0011:
	{
		MethodArguments_t735664811 * L_2 = ___args0;
		MethodArguments_t735664811 * L_3 = (MethodArguments_t735664811 *)il2cpp_codegen_object_new(MethodArguments_t735664811_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m1402944452(L_3, (MethodArguments_t735664811 *)L_2, /*hidden argument*/NULL);
		V_0 = (MethodArguments_t735664811 *)L_3;
	}

IL_0018:
	{
		NullCheck((MethodCall_1_t2451378470 *)__this);
		FacebookDelegate_1_t1724794744 * L_4 = ((  FacebookDelegate_1_t1724794744 * (*) (MethodCall_1_t2451378470 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MethodCall_1_t2451378470 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		MethodArguments_t735664811 * L_5 = V_0;
		AndroidFacebook_t3038444049 * L_6 = (AndroidFacebook_t3038444049 *)__this->get_androidImpl_4();
		NullCheck((FacebookBase_t2463540723 *)L_6);
		CallbackManager_t4242095184 * L_7 = FacebookBase_get_CallbackManager_m3945259249((FacebookBase_t2463540723 *)L_6, /*hidden argument*/NULL);
		NullCheck((MethodCall_1_t2451378470 *)__this);
		FacebookDelegate_1_t1724794744 * L_8 = ((  FacebookDelegate_1_t1724794744 * (*) (MethodCall_1_t2451378470 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MethodCall_1_t2451378470 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((CallbackManager_t4242095184 *)L_7);
		String_t* L_9 = ((  String_t* (*) (CallbackManager_t4242095184 *, FacebookDelegate_1_t1724794744 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((CallbackManager_t4242095184 *)L_7, (FacebookDelegate_1_t1724794744 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		NullCheck((MethodArguments_t735664811 *)L_5);
		MethodArguments_AddString_m3767570050((MethodArguments_t735664811 *)L_5, (String_t*)_stringLiteral2736671527, (String_t*)L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		AndroidFacebook_t3038444049 * L_10 = (AndroidFacebook_t3038444049 *)__this->get_androidImpl_4();
		NullCheck((MethodCall_1_t2451378470 *)__this);
		String_t* L_11 = ((  String_t* (*) (MethodCall_1_t2451378470 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((MethodCall_1_t2451378470 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		MethodArguments_t735664811 * L_12 = V_0;
		NullCheck((MethodArguments_t735664811 *)L_12);
		String_t* L_13 = MethodArguments_ToJsonString_m1972480397((MethodArguments_t735664811 *)L_12, /*hidden argument*/NULL);
		NullCheck((AndroidFacebook_t3038444049 *)L_10);
		AndroidFacebook_CallFB_m445829063((AndroidFacebook_t3038444049 *)L_10, (String_t*)L_11, (String_t*)L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Utilities/Callback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Callback_1__ctor_m4046123018_gshared (Callback_1_t1712464442 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Facebook.Unity.Utilities/Callback`1<System.Object>::Invoke(T)
extern "C"  void Callback_1_Invoke_m1539990562_gshared (Callback_1_t1712464442 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Callback_1_Invoke_m1539990562((Callback_1_t1712464442 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Facebook.Unity.Utilities/Callback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Callback_1_BeginInvoke_m2342307435_gshared (Callback_1_t1712464442 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Facebook.Unity.Utilities/Callback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Callback_1_EndInvoke_m603559680_gshared (Callback_1_t1712464442 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CCheckScoreUpdateU3Ec__Iterator0__ctor_m4149393446_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckScoreUpdateU3Ec__Iterator0_MoveNext_m1213825570_MetadataUsageId;
extern "C"  bool U3CCheckScoreUpdateU3Ec__Iterator0_MoveNext_m1213825570_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckScoreUpdateU3Ec__Iterator0_MoveNext_m1213825570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0051;
			}
		}
	}
	{
		goto IL_0064;
	}

IL_0021:
	{
		FBManager_1_t2091981452 * L_2 = (FBManager_1_t2091981452 *)__this->get_U24this_0();
		NullCheck(L_2);
		L_2->set_scoreBeingChecked_10((bool)1);
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (float)(6.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		bool L_4 = (bool)__this->get_U24disposing_2();
		if (L_4)
		{
			goto IL_004c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_004c:
	{
		goto IL_0066;
	}

IL_0051:
	{
		FBManager_1_t2091981452 * L_5 = (FBManager_1_t2091981452 *)__this->get_U24this_0();
		NullCheck(L_5);
		L_5->set_scoreBeingChecked_10((bool)0);
		__this->set_U24PC_3((-1));
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}
}
// System.Object FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2850736412_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_1();
		return L_0;
	}
}
// System.Object FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2924679796_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_1();
		return L_0;
	}
}
// System.Void FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CCheckScoreUpdateU3Ec__Iterator0_Dispose_m2009072621_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckScoreUpdateU3Ec__Iterator0_Reset_m3007712715_MetadataUsageId;
extern "C"  void U3CCheckScoreUpdateU3Ec__Iterator0_Reset_m3007712715_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckScoreUpdateU3Ec__Iterator0_Reset_m3007712715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>::.ctor()
extern "C"  void U3CGetAppFriendsScoreListU3Ec__AnonStorey3__ctor_m184220960_gshared (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>::<>m__0(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t278961118_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2926404389_MethodInfo_var;
extern const MethodInfo* List_1_Add_m531140607_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m719161096_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2619694;
extern Il2CppCodeGenString* _stringLiteral2247021248;
extern Il2CppCodeGenString* _stringLiteral1448002985;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern const uint32_t U3CGetAppFriendsScoreListU3Ec__AnonStorey3_U3CU3Em__0_m472877894_MetadataUsageId;
extern "C"  void U3CGetAppFriendsScoreListU3Ec__AnonStorey3_U3CU3Em__0_m472877894_gshared (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_U3CU3Em__0_m472877894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	ArrayList_t4252133567 * V_1 = NULL;
	List_1_t278961118 * V_2 = NULL;
	int32_t V_3 = 0;
	Hashtable_t909839986 * V_4 = NULL;
	Hashtable_t909839986 * V_5 = NULL;
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck((Il2CppObject *)L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		Il2CppObject * L_2 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, (String_t*)L_1, /*hidden argument*/NULL);
		V_0 = (Hashtable_t909839986 *)((Hashtable_t909839986 *)Castclass(L_2, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_3 = V_0;
		NullCheck((Hashtable_t909839986 *)L_3);
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_3, (Il2CppObject *)_stringLiteral2619694);
		if (!L_4)
		{
			goto IL_0117;
		}
	}
	{
		Hashtable_t909839986 * L_5 = V_0;
		NullCheck((Hashtable_t909839986 *)L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_5, (Il2CppObject *)_stringLiteral2619694);
		V_1 = (ArrayList_t4252133567 *)((ArrayList_t4252133567 *)Castclass(L_6, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		List_1_t278961118 * L_7 = (List_1_t278961118 *)il2cpp_codegen_object_new(List_1_t278961118_il2cpp_TypeInfo_var);
		List_1__ctor_m2926404389(L_7, /*hidden argument*/List_1__ctor_m2926404389_MethodInfo_var);
		V_2 = (List_1_t278961118 *)L_7;
		V_3 = (int32_t)0;
		goto IL_010b;
	}

IL_003f:
	{
		Hashtable_t909839986 * L_8 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_8, /*hidden argument*/NULL);
		V_4 = (Hashtable_t909839986 *)L_8;
		List_1_t278961118 * L_9 = V_2;
		ArrayList_t4252133567 * L_10 = V_1;
		int32_t L_11 = V_3;
		NullCheck((ArrayList_t4252133567 *)L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, (ArrayList_t4252133567 *)L_10, (int32_t)L_11);
		NullCheck((List_1_t278961118 *)L_9);
		List_1_Add_m531140607((List_1_t278961118 *)L_9, (Hashtable_t909839986 *)((Hashtable_t909839986 *)Castclass(L_12, Hashtable_t909839986_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m531140607_MethodInfo_var);
		List_1_t278961118 * L_13 = V_2;
		int32_t L_14 = V_3;
		NullCheck((List_1_t278961118 *)L_13);
		Hashtable_t909839986 * L_15 = List_1_get_Item_m719161096((List_1_t278961118 *)L_13, (int32_t)L_14, /*hidden argument*/List_1_get_Item_m719161096_MethodInfo_var);
		NullCheck((Hashtable_t909839986 *)L_15);
		bool L_16 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_15, (Il2CppObject *)_stringLiteral2247021248);
		if (!L_16)
		{
			goto IL_0090;
		}
	}
	{
		Hashtable_t909839986 * L_17 = V_4;
		List_1_t278961118 * L_18 = V_2;
		int32_t L_19 = V_3;
		NullCheck((List_1_t278961118 *)L_18);
		Hashtable_t909839986 * L_20 = List_1_get_Item_m719161096((List_1_t278961118 *)L_18, (int32_t)L_19, /*hidden argument*/List_1_get_Item_m719161096_MethodInfo_var);
		NullCheck((Hashtable_t909839986 *)L_20);
		Il2CppObject * L_21 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_20, (Il2CppObject *)_stringLiteral2247021248);
		NullCheck((Il2CppObject *)L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_21);
		NullCheck((Hashtable_t909839986 *)L_17);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_17, (Il2CppObject *)_stringLiteral2247021248, (Il2CppObject *)L_22);
	}

IL_0090:
	{
		List_1_t278961118 * L_23 = V_2;
		int32_t L_24 = V_3;
		NullCheck((List_1_t278961118 *)L_23);
		Hashtable_t909839986 * L_25 = List_1_get_Item_m719161096((List_1_t278961118 *)L_23, (int32_t)L_24, /*hidden argument*/List_1_get_Item_m719161096_MethodInfo_var);
		NullCheck((Hashtable_t909839986 *)L_25);
		Il2CppObject * L_26 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_25, (Il2CppObject *)_stringLiteral1448002985);
		V_5 = (Hashtable_t909839986 *)((Hashtable_t909839986 *)Castclass(L_26, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_27 = V_5;
		NullCheck((Hashtable_t909839986 *)L_27);
		bool L_28 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_27, (Il2CppObject *)_stringLiteral2328218955);
		if (!L_28)
		{
			goto IL_00d1;
		}
	}
	{
		Hashtable_t909839986 * L_29 = V_4;
		Hashtable_t909839986 * L_30 = V_5;
		NullCheck((Hashtable_t909839986 *)L_30);
		Il2CppObject * L_31 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_30, (Il2CppObject *)_stringLiteral2328218955);
		NullCheck((Hashtable_t909839986 *)L_29);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_29, (Il2CppObject *)_stringLiteral2328218955, (Il2CppObject *)L_31);
	}

IL_00d1:
	{
		Hashtable_t909839986 * L_32 = V_5;
		NullCheck((Hashtable_t909839986 *)L_32);
		bool L_33 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_32, (Il2CppObject *)_stringLiteral287061489);
		if (!L_33)
		{
			goto IL_00fa;
		}
	}
	{
		Hashtable_t909839986 * L_34 = V_4;
		Hashtable_t909839986 * L_35 = V_5;
		NullCheck((Hashtable_t909839986 *)L_35);
		Il2CppObject * L_36 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_35, (Il2CppObject *)_stringLiteral287061489);
		NullCheck((Hashtable_t909839986 *)L_34);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_34, (Il2CppObject *)_stringLiteral287061489, (Il2CppObject *)L_36);
	}

IL_00fa:
	{
		List_1_t278961118 * L_37 = (List_1_t278961118 *)__this->get_UsersInfoList_0();
		Hashtable_t909839986 * L_38 = V_4;
		NullCheck((List_1_t278961118 *)L_37);
		List_1_Add_m531140607((List_1_t278961118 *)L_37, (Hashtable_t909839986 *)L_38, /*hidden argument*/List_1_Add_m531140607_MethodInfo_var);
		int32_t L_39 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_010b:
	{
		int32_t L_40 = V_3;
		ArrayList_t4252133567 * L_41 = V_1;
		NullCheck((ArrayList_t4252133567 *)L_41);
		int32_t L_42 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, (ArrayList_t4252133567 *)L_41);
		if ((((int32_t)L_40) < ((int32_t)L_42)))
		{
			goto IL_003f;
		}
	}

IL_0117:
	{
		List_1_t278961118 * L_43 = (List_1_t278961118 *)__this->get_UsersInfoList_0();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_scoreInfo_9(L_43);
		List_1_t278961118 * L_44 = ((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_scoreInfo_9();
		if (!L_44)
		{
			goto IL_012c;
		}
	}

IL_012c:
	{
		FBManager_1_t2091981452 * L_45 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		NullCheck(L_45);
		bool L_46 = (bool)L_45->get_scoreBeingChecked_10();
		if (L_46)
		{
			goto IL_0153;
		}
	}
	{
		FBManager_1_t2091981452 * L_47 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		FBManager_1_t2091981452 * L_48 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		NullCheck((FBManager_1_t2091981452 *)L_48);
		Il2CppObject * L_49 = ((  Il2CppObject * (*) (FBManager_1_t2091981452 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((FBManager_1_t2091981452 *)L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((MonoBehaviour_t1158329972 *)L_47);
		MonoBehaviour_StartCoroutine_m2470621050((MonoBehaviour_t1158329972 *)L_47, (Il2CppObject *)L_49, /*hidden argument*/NULL);
	}

IL_0153:
	{
		return;
	}
}
// System.Void FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>::.ctor()
extern "C"  void U3CLoadPictureAPIU3Ec__AnonStorey4__ctor_m2661967309_gshared (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>::<>m__0(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadPictureAPIU3Ec__AnonStorey4_U3CU3Em__0_m1328248257_MetadataUsageId;
extern "C"  void U3CLoadPictureAPIU3Ec__AnonStorey4_U3CU3Em__0_m1328248257_gshared (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadPictureAPIU3Ec__AnonStorey4_U3CU3Em__0_m1328248257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck((Il2CppObject *)L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_2 = ___result0;
		NullCheck((Il2CppObject *)L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FBManager_1_t2091981452 * L_4 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		Il2CppObject * L_5 = ___result0;
		NullCheck((Il2CppObject *)L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
		NullCheck((FBManager_1_t2091981452 *)L_4);
		String_t* L_7 = ((  String_t* (*) (FBManager_1_t2091981452 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((FBManager_1_t2091981452 *)L_4, (String_t*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (String_t*)L_7;
		FBManager_1_t2091981452 * L_8 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		FBManager_1_t2091981452 * L_9 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		String_t* L_10 = V_0;
		LoadPictureCallback_t2854975038 * L_11 = (LoadPictureCallback_t2854975038 *)__this->get_callback_0();
		NullCheck((FBManager_1_t2091981452 *)L_9);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (FBManager_1_t2091981452 *, String_t*, LoadPictureCallback_t2854975038 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((FBManager_1_t2091981452 *)L_9, (String_t*)L_10, (LoadPictureCallback_t2854975038 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		NullCheck((MonoBehaviour_t1158329972 *)L_8);
		MonoBehaviour_StartCoroutine_m2470621050((MonoBehaviour_t1158329972 *)L_8, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::.ctor()
extern "C"  void U3CLoadPictureEnumeratorU3Ec__Iterator1__ctor_m600969494_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadPictureEnumeratorU3Ec__Iterator1_MoveNext_m3366050890_MetadataUsageId;
extern "C"  bool U3CLoadPictureEnumeratorU3Ec__Iterator1_MoveNext_m3366050890_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadPictureEnumeratorU3Ec__Iterator1_MoveNext_m3366050890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0052;
			}
		}
	}
	{
		goto IL_006f;
	}

IL_0021:
	{
		String_t* L_2 = (String_t*)__this->get_url_0();
		WWW_t2919945039 * L_3 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_3, (String_t*)L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_3);
		WWW_t2919945039 * L_4 = (WWW_t2919945039 *)__this->get_U3CwwwU3E__0_1();
		__this->set_U24current_3(L_4);
		bool L_5 = (bool)__this->get_U24disposing_4();
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_004d:
	{
		goto IL_0071;
	}

IL_0052:
	{
		LoadPictureCallback_t2854975038 * L_6 = (LoadPictureCallback_t2854975038 *)__this->get_callback_2();
		WWW_t2919945039 * L_7 = (WWW_t2919945039 *)__this->get_U3CwwwU3E__0_1();
		NullCheck((WWW_t2919945039 *)L_7);
		Texture2D_t3542995729 * L_8 = WWW_get_texture_m1121178301((WWW_t2919945039 *)L_7, /*hidden argument*/NULL);
		NullCheck((LoadPictureCallback_t2854975038 *)L_6);
		((  void (*) (LoadPictureCallback_t2854975038 *, Texture_t2243626319 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LoadPictureCallback_t2854975038 *)L_6, (Texture_t2243626319 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_5((-1));
	}

IL_006f:
	{
		return (bool)0;
	}

IL_0071:
	{
		return (bool)1;
	}
}
// System.Object FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4250567546_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3131561906_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Void FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::Dispose()
extern "C"  void U3CLoadPictureEnumeratorU3Ec__Iterator1_Dispose_m1466434241_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadPictureEnumeratorU3Ec__Iterator1_Reset_m2048700007_MetadataUsageId;
extern "C"  void U3CLoadPictureEnumeratorU3Ec__Iterator1_Reset_m2048700007_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadPictureEnumeratorU3Ec__Iterator1_Reset_m2048700007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>::.ctor()
extern "C"  void U3CPostHighScoreU3Ec__AnonStorey2__ctor_m3859885075_gshared (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>::<>m__0(Facebook.Unity.ILoginResult)
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* ILoginResult_t403585443_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostHighScoreU3Ec__AnonStorey2_U3CU3Em__0_m1136012094_MetadataUsageId;
extern "C"  void U3CPostHighScoreU3Ec__AnonStorey2_U3CU3Em__0_m1136012094_gshared (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPostHighScoreU3Ec__AnonStorey2_U3CU3Em__0_m1136012094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject * L_1 = ___result0;
		NullCheck((Il2CppObject *)L_1);
		AccessToken_t2518141643 * L_2 = InterfaceFuncInvoker0< AccessToken_t2518141643 * >::Invoke(0 /* Facebook.Unity.AccessToken Facebook.Unity.ILoginResult::get_AccessToken() */, ILoginResult_t403585443_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		FBManager_1_t2091981452 * L_3 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		NullCheck((FBManager_1_t2091981452 *)L_3);
		bool L_4 = ((  bool (*) (FBManager_1_t2091981452 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((FBManager_1_t2091981452 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		FBManager_1_t2091981452 * L_5 = (FBManager_1_t2091981452 *)__this->get_U24this_1();
		int32_t L_6 = (int32_t)__this->get_score_0();
		NullCheck((FBManager_1_t2091981452 *)L_5);
		((  void (*) (FBManager_1_t2091981452 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((FBManager_1_t2091981452 *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0036:
	{
		return;
	}
}
// System.Void FBManager`1/LoadPictureCallback<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void LoadPictureCallback__ctor_m3122573841_gshared (LoadPictureCallback_t2854975038 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void FBManager`1/LoadPictureCallback<System.Object>::Invoke(UnityEngine.Texture)
extern "C"  void LoadPictureCallback_Invoke_m2615178341_gshared (LoadPictureCallback_t2854975038 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LoadPictureCallback_Invoke_m2615178341((LoadPictureCallback_t2854975038 *)__this->get_prev_9(),___texture0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Texture_t2243626319 * ___texture0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___texture0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Texture_t2243626319 * ___texture0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___texture0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___texture0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult FBManager`1/LoadPictureCallback<System.Object>::BeginInvoke(UnityEngine.Texture,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoadPictureCallback_BeginInvoke_m3362398610_gshared (LoadPictureCallback_t2854975038 * __this, Texture_t2243626319 * ___texture0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___texture0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void FBManager`1/LoadPictureCallback<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void LoadPictureCallback_EndInvoke_m2740201835_gshared (LoadPictureCallback_t2854975038 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void FBManager`1<System.Object>::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookPlayer_t2645999341_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1__ctor_m1614973529_MetadataUsageId;
extern "C"  void FBManager_1__ctor_m1614973529_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1__ctor_m1614973529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FacebookPlayer_t2645999341 * L_2 = (FacebookPlayer_t2645999341 *)il2cpp_codegen_object_new(FacebookPlayer_t2645999341_il2cpp_TypeInfo_var);
		FacebookPlayer__ctor_m3584626410(L_2, (String_t*)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		__this->set_loggedInUser_6(L_2);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void FBManager`1<System.Object>::InitFacebook()
extern Il2CppClass* InternetReachability_t3219127626_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral199695052;
extern const uint32_t FBManager_1_InitFacebook_m3847425487_MetadataUsageId;
extern "C"  void FBManager_1_InitFacebook_m3847425487_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_InitFacebook_m3847425487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternetReachability_t3219127626_il2cpp_TypeInfo_var);
		bool L_0 = InternetReachability_CheckForInternetConnection_m1437318007(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((FBManager_1_t2091981452 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		goto IL_002f;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_1 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral199695052, /*hidden argument*/NULL);
	}

IL_0029:
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void FBManager`1<System.Object>::InitFacebook() */, (FBManager_1_t2091981452 *)__this);
	}

IL_002f:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::CallInit()
extern Il2CppClass* InitDelegate_t3410465555_il2cpp_TypeInfo_var;
extern Il2CppClass* HideUnityDelegate_t712804158_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1_CallInit_m1371816793_MetadataUsageId;
extern "C"  void FBManager_1_CallInit_m1371816793_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_CallInit_m1371816793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		InitDelegate_t3410465555 * L_1 = (InitDelegate_t3410465555 *)il2cpp_codegen_object_new(InitDelegate_t3410465555_il2cpp_TypeInfo_var);
		InitDelegate__ctor_m1337962232(L_1, (Il2CppObject *)__this, (IntPtr_t)L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		HideUnityDelegate_t712804158 * L_3 = (HideUnityDelegate_t712804158 *)il2cpp_codegen_object_new(HideUnityDelegate_t712804158_il2cpp_TypeInfo_var);
		HideUnityDelegate__ctor_m4212726857(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_Init_m2482858118(NULL /*static, unused*/, (InitDelegate_t3410465555 *)L_1, (HideUnityDelegate_t712804158 *)L_3, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBManager`1<System.Object>::SetInit()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1354774870;
extern const uint32_t FBManager_1_SetInit_m3245493993_MetadataUsageId;
extern "C"  void FBManager_1_SetInit_m3245493993_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_SetInit_m3245493993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1354774870, /*hidden argument*/NULL);
	}

IL_0014:
	{
		__this->set_init_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_1 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void FBManager`1<System.Object>::OnLoggedIn() */, (FBManager_1_t2091981452 *)__this);
	}

IL_002b:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::OnHideUnity(System.Boolean)
extern "C"  void FBManager_1_OnHideUnity_m3813074720_gshared (FBManager_1_t2091981452 * __this, bool ___isGameShown0, const MethodInfo* method)
{
	{
		bool L_0 = ___isGameShown0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (float)(0.0f), /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0015:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (float)(1.0f), /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::LoginToFacebook(LoginEntrance)
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m1054243793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2157842303;
extern Il2CppCodeGenString* _stringLiteral1111044658;
extern Il2CppCodeGenString* _stringLiteral1552242599;
extern Il2CppCodeGenString* _stringLiteral4247710112;
extern const uint32_t FBManager_1_LoginToFacebook_m3247006910_MetadataUsageId;
extern "C"  void FBManager_1_LoginToFacebook_m3247006910_gshared (FBManager_1_t2091981452 * __this, int32_t ___entrance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_LoginToFacebook_m3247006910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1398341365 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsInitialized_m1783560910(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_1 = ___entrance0;
		__this->set_FBentrance_5(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_2 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_006e;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_3 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		NullCheck(L_3);
		String_t* L_4 = (String_t*)L_3->get_accessToken_5();
		if (L_4)
		{
			goto IL_0063;
		}
	}
	{
		AccessToken_t2518141643 * L_5 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004f;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_6 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		AccessToken_t2518141643 * L_7 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((AccessToken_t2518141643 *)L_7);
		String_t* L_8 = AccessToken_get_TokenString_m342443006((AccessToken_t2518141643 *)L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_accessToken_5(L_8);
		goto IL_0063;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_9 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2157842303, /*hidden argument*/NULL);
	}

IL_0063:
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void FBManager`1<System.Object>::OnLoggedIn() */, (FBManager_1_t2091981452 *)__this);
		goto IL_009c;
	}

IL_006e:
	{
		List_1_t1398341365 * L_10 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_10, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_0 = (List_1_t1398341365 *)L_10;
		List_1_t1398341365 * L_11 = V_0;
		NullCheck((List_1_t1398341365 *)L_11);
		List_1_Add_m4061286785((List_1_t1398341365 *)L_11, (String_t*)_stringLiteral1111044658, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_12 = V_0;
		NullCheck((List_1_t1398341365 *)L_12);
		List_1_Add_m4061286785((List_1_t1398341365 *)L_12, (String_t*)_stringLiteral1552242599, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		FacebookDelegate_1_t3733898188 * L_15 = (FacebookDelegate_1_t3733898188 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1054243793(L_15, (Il2CppObject *)__this, (IntPtr_t)L_14, /*hidden argument*/FacebookDelegate_1__ctor_m1054243793_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogInWithReadPermissions_m450336890(NULL /*static, unused*/, (Il2CppObject*)L_13, (FacebookDelegate_1_t3733898188 *)L_15, /*hidden argument*/NULL);
	}

IL_009c:
	{
		goto IL_00b5;
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_16 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4247710112, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::LoginCallback(Facebook.Unity.ILoginResult)
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* ILoginResult_t403585443_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1_LoginCallback_m3384117207_MetadataUsageId;
extern "C"  void FBManager_1_LoginCallback_m3384117207_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_LoginCallback_m3384117207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject * L_1 = ___result0;
		NullCheck((Il2CppObject *)L_1);
		AccessToken_t2518141643 * L_2 = InterfaceFuncInvoker0< AccessToken_t2518141643 * >::Invoke(0 /* Facebook.Unity.AccessToken Facebook.Unity.ILoginResult::get_AccessToken() */, ILoginResult_t403585443_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_3 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		Il2CppObject * L_4 = ___result0;
		NullCheck((Il2CppObject *)L_4);
		AccessToken_t2518141643 * L_5 = InterfaceFuncInvoker0< AccessToken_t2518141643 * >::Invoke(0 /* Facebook.Unity.AccessToken Facebook.Unity.ILoginResult::get_AccessToken() */, ILoginResult_t403585443_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		NullCheck((AccessToken_t2518141643 *)L_5);
		String_t* L_6 = AccessToken_get_TokenString_m342443006((AccessToken_t2518141643 *)L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_accessToken_5(L_6);
	}

IL_002b:
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void FBManager`1<System.Object>::OnLoggedIn() */, (FBManager_1_t2091981452 *)__this);
		goto IL_003c;
	}

IL_0036:
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker0::Invoke(8 /* System.Void FBManager`1<System.Object>::OnLoginCancelled() */, (FBManager_1_t2091981452 *)__this);
	}

IL_003c:
	{
		return;
	}
}
// System.Boolean FBManager`1<System.Object>::CurrentUserHasPublishPermission()
extern const MethodInfo* Enumerable_Contains_TisString_t_m2925925544_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2116175235;
extern const uint32_t FBManager_1_CurrentUserHasPublishPermission_m3669725089_MetadataUsageId;
extern "C"  bool FBManager_1_CurrentUserHasPublishPermission_m3669725089_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_CurrentUserHasPublishPermission_m3669725089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		AccessToken_t2518141643 * L_0 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		AccessToken_t2518141643 * L_1 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((AccessToken_t2518141643 *)L_1);
		Il2CppObject* L_2 = AccessToken_get_Permissions_m2321258725((AccessToken_t2518141643 *)L_1, /*hidden argument*/NULL);
		bool L_3 = Enumerable_Contains_TisString_t_m2925925544(NULL /*static, unused*/, (Il2CppObject*)L_2, (String_t*)_stringLiteral2116175235, /*hidden argument*/Enumerable_Contains_TisString_t_m2925925544_MethodInfo_var);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0021;
	}

IL_0020:
	{
		G_B3_0 = 0;
	}

IL_0021:
	{
		return (bool)G_B3_0;
	}
}
// System.Void FBManager`1<System.Object>::OnLoggedIn()
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m255206972_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3078427720;
extern Il2CppCodeGenString* _stringLiteral3015944840;
extern const uint32_t FBManager_1_OnLoggedIn_m1876336541_MetadataUsageId;
extern "C"  void FBManager_1_OnLoggedIn_m1876336541_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_OnLoggedIn_m1876336541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_loggedIn_4((bool)1);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		FacebookDelegate_1_t3020292135 * L_1 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_1, (Il2CppObject *)__this, (IntPtr_t)L_0, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, (String_t*)_stringLiteral3078427720, (int32_t)0, (FacebookDelegate_1_t3020292135 *)L_1, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		Nullable_1_t334943763  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m255206972(&L_2, (int32_t)((int32_t)128), /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		Nullable_1_t334943763  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m255206972(&L_3, (int32_t)((int32_t)128), /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		String_t* L_4 = ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, Nullable_1_t334943763 , Nullable_1_t334943763 , String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (String_t*)_stringLiteral3015944840, (Nullable_1_t334943763 )L_2, (Nullable_1_t334943763 )L_3, (String_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 13));
		LoadPictureCallback_t2854975038 * L_6 = (LoadPictureCallback_t2854975038 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13));
		((  void (*) (LoadPictureCallback_t2854975038 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		NullCheck((FBManager_1_t2091981452 *)__this);
		((  void (*) (FBManager_1_t2091981452 *, String_t*, LoadPictureCallback_t2854975038 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((FBManager_1_t2091981452 *)__this, (String_t*)L_4, (LoadPictureCallback_t2854975038 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}
}
// System.Void FBManager`1<System.Object>::APICallback(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3078427720;
extern Il2CppCodeGenString* _stringLiteral2132392826;
extern Il2CppCodeGenString* _stringLiteral2813401344;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral1111044658;
extern const uint32_t FBManager_1_APICallback_m2721173331_MetadataUsageId;
extern "C"  void FBManager_1_APICallback_m2721173331_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_APICallback_m2721173331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck((Il2CppObject *)L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		FacebookDelegate_1_t3020292135 * L_3 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, (String_t*)_stringLiteral3078427720, (int32_t)0, (FacebookDelegate_1_t3020292135 *)L_3, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		Il2CppObject * L_4 = ___result0;
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		Il2CppObject * L_6 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, (String_t*)L_5, /*hidden argument*/NULL);
		V_0 = (Hashtable_t909839986 *)((Hashtable_t909839986 *)Castclass(L_6, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_7 = V_0;
		NullCheck((Hashtable_t909839986 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_7, (Il2CppObject *)_stringLiteral2132392826);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_9 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		Hashtable_t909839986 * L_10 = V_0;
		NullCheck((Hashtable_t909839986 *)L_10);
		Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_10, (Il2CppObject *)_stringLiteral2132392826);
		NullCheck((Il2CppObject *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_11);
		NullCheck((FacebookUser_t244724091 *)L_9);
		FacebookUser_SetUserFirstName_m1631350954((FacebookUser_t244724091 *)L_9, (String_t*)L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		Hashtable_t909839986 * L_13 = V_0;
		NullCheck((Hashtable_t909839986 *)L_13);
		bool L_14 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_13, (Il2CppObject *)_stringLiteral2813401344);
		if (!L_14)
		{
			goto IL_008c;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_15 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		Hashtable_t909839986 * L_16 = V_0;
		NullCheck((Hashtable_t909839986 *)L_16);
		Il2CppObject * L_17 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_16, (Il2CppObject *)_stringLiteral2813401344);
		NullCheck((Il2CppObject *)L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_17);
		NullCheck((FacebookUser_t244724091 *)L_15);
		FacebookUser_SetUserLastName_m668642400((FacebookUser_t244724091 *)L_15, (String_t*)L_18, /*hidden argument*/NULL);
	}

IL_008c:
	{
		Hashtable_t909839986 * L_19 = V_0;
		NullCheck((Hashtable_t909839986 *)L_19);
		bool L_20 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_19, (Il2CppObject *)_stringLiteral287061489);
		if (!L_20)
		{
			goto IL_00b7;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_21 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		Hashtable_t909839986 * L_22 = V_0;
		NullCheck((Hashtable_t909839986 *)L_22);
		Il2CppObject * L_23 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_22, (Il2CppObject *)_stringLiteral287061489);
		NullCheck((Il2CppObject *)L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_23);
		NullCheck((FacebookUser_t244724091 *)L_21);
		FacebookUser_SetUserID_m2787758146((FacebookUser_t244724091 *)L_21, (String_t*)L_24, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		Hashtable_t909839986 * L_25 = V_0;
		NullCheck((Hashtable_t909839986 *)L_25);
		bool L_26 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_25, (Il2CppObject *)_stringLiteral1111044658);
		if (!L_26)
		{
			goto IL_00e2;
		}
	}
	{
		FacebookPlayer_t2645999341 * L_27 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		Hashtable_t909839986 * L_28 = V_0;
		NullCheck((Hashtable_t909839986 *)L_28);
		Il2CppObject * L_29 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_28, (Il2CppObject *)_stringLiteral1111044658);
		NullCheck((Il2CppObject *)L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_29);
		NullCheck((FacebookUser_t244724091 *)L_27);
		FacebookUser_SetUserEmail_m1442261011((FacebookUser_t244724091 *)L_27, (String_t*)L_30, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::OnLoginCancelled()
extern "C"  void FBManager_1_OnLoginCancelled_m3895251954_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::LogOutOfFacebook(System.Boolean)
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1_LogOutOfFacebook_m751438301_MetadataUsageId;
extern "C"  void FBManager_1_LogOutOfFacebook_m751438301_gshared (FBManager_1_t2091981452 * __this, bool ___online0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_LogOutOfFacebook_m751438301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___online0;
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void FBManager`1<System.Object>::OnLoggedOut(System.Boolean) */, (FBManager_1_t2091981452 *)__this, (bool)L_0);
		__this->set_loggedIn_4((bool)0);
		bool L_1 = ___online0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogOut_m3952695663(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::OnLoggedOut(System.Boolean)
extern "C"  void FBManager_1_OnLoggedOut_m1113730087_gshared (FBManager_1_t2091981452 * __this, bool ___online0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FBManager`1<System.Object>::GetUserLoginStatus()
extern "C"  bool FBManager_1_GetUserLoginStatus_m3132348805_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_loggedIn_4();
		return L_0;
	}
}
// System.Boolean FBManager`1<System.Object>::GetFacebookLoginStatus()
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1_GetFacebookLoginStatus_m1222898994_MetadataUsageId;
extern "C"  bool FBManager_1_GetFacebookLoginStatus_m1222898994_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_GetFacebookLoginStatus_m1222898994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// FacebookPlayer FBManager`1<System.Object>::GetLoggedInUser()
extern "C"  FacebookPlayer_t2645999341 * FBManager_1_GetLoggedInUser_m2334710691_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	{
		FacebookPlayer_t2645999341 * L_0 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		return L_0;
	}
}
// System.Void FBManager`1<System.Object>::PostHighScore(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m1054243793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1756683522;
extern Il2CppCodeGenString* _stringLiteral2116175235;
extern const uint32_t FBManager_1_PostHighScore_m3979228406_MetadataUsageId;
extern "C"  void FBManager_1_PostHighScore_m3979228406_gshared (FBManager_1_t2091981452 * __this, int32_t ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_PostHighScore_m3979228406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * V_0 = NULL;
	List_1_t1398341365 * V_1 = NULL;
	{
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_0 = (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		V_0 = (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 *)L_0;
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_1 = V_0;
		int32_t L_2 = ___score0;
		NullCheck(L_1);
		L_1->set_score_0(L_2);
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_score_0();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m56707527(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1756683522, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)L_9->get_score_0();
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		bool L_11 = ((  bool (*) (FBManager_1_t2091981452 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((FBManager_1_t2091981452 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_score_0();
		NullCheck((FBManager_1_t2091981452 *)__this);
		((  void (*) (FBManager_1_t2091981452 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((FBManager_1_t2091981452 *)__this, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0079;
	}

IL_0056:
	{
		List_1_t1398341365 * L_14 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_14, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_1 = (List_1_t1398341365 *)L_14;
		List_1_t1398341365 * L_15 = V_1;
		NullCheck((List_1_t1398341365 *)L_15);
		List_1_Add_m4061286785((List_1_t1398341365 *)L_15, (String_t*)_stringLiteral2116175235, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_16 = V_1;
		U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		FacebookDelegate_1_t3733898188 * L_19 = (FacebookDelegate_1_t3733898188 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1054243793(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/FacebookDelegate_1__ctor_m1054243793_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogInWithPublishPermissions_m619042695(NULL /*static, unused*/, (Il2CppObject*)L_16, (FacebookDelegate_1_t3733898188 *)L_19, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::_PostHighScore(System.Int32)
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1063737612_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2247021248;
extern Il2CppCodeGenString* _stringLiteral271152111;
extern const uint32_t FBManager_1__PostHighScore_m401835617_MetadataUsageId;
extern "C"  void FBManager_1__PostHighScore_m401835617_gshared (FBManager_1_t2091981452 * __this, int32_t ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1__PostHighScore_m401835617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	Dictionary_2_t3943999495 * V_1 = NULL;
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	{
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_0, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_1 = (Dictionary_2_t3943999495 *)L_0;
		Dictionary_2_t3943999495 * L_1 = V_1;
		String_t* L_2 = Int32_ToString_m2960866144((int32_t*)(&___score0), /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t3943999495 *)L_1);
		Dictionary_2_Add_m1063737612((Dictionary_2_t3943999495 *)L_1, (String_t*)_stringLiteral2247021248, (String_t*)L_2, /*hidden argument*/Dictionary_2_Add_m1063737612_MethodInfo_var);
		Dictionary_2_t3943999495 * L_3 = V_1;
		V_0 = (Dictionary_2_t3943999495 *)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		FacebookDelegate_1_t3020292135 * L_4 = ((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache0_11();
		G_B1_0 = 1;
		G_B1_1 = _stringLiteral271152111;
		if (L_4)
		{
			G_B2_0 = 1;
			G_B2_1 = _stringLiteral271152111;
			goto IL_003e;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		FacebookDelegate_1_t3020292135 * L_6 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)->static_fields)->set_U3CU3Ef__amU24cache0_11(L_6);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		FacebookDelegate_1_t3020292135 * L_7 = ((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache0_11();
		Dictionary_2_t3943999495 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, (String_t*)G_B2_1, (int32_t)G_B2_0, (FacebookDelegate_1_t3020292135 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.Collections.Hashtable> FBManager`1<System.Object>::GetAppFriendsScoreList()
extern Il2CppClass* List_1_t278961118_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2926404389_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2264133042_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral1524435028;
extern const uint32_t FBManager_1_GetAppFriendsScoreList_m238380784_MetadataUsageId;
extern "C"  List_1_t278961118 * FBManager_1_GetAppFriendsScoreList_m238380784_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_GetAppFriendsScoreList_m238380784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * V_0 = NULL;
	{
		U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * L_0 = (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 23));
		((  void (*) (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		V_0 = (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 *)L_0;
		U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		List_1_t278961118 * L_2 = ((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)->static_fields)->get_scoreInfo_9();
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		bool L_3 = (bool)__this->get_scoreBeingChecked_10();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		List_1_t278961118 * L_4 = ((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)->static_fields)->get_scoreInfo_9();
		return L_4;
	}

IL_0028:
	{
		U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * L_5 = V_0;
		List_1_t278961118 * L_6 = (List_1_t278961118 *)il2cpp_codegen_object_new(List_1_t278961118_il2cpp_TypeInfo_var);
		List_1__ctor_m2926404389(L_6, /*hidden argument*/List_1__ctor_m2926404389_MethodInfo_var);
		NullCheck(L_5);
		L_5->set_UsersInfoList_0(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_7 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003d;
		}
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		String_t* L_8 = FB_get_AppId_m4024040373(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral372029315, (String_t*)L_9, (String_t*)_stringLiteral1524435028, /*hidden argument*/NULL);
		U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		FacebookDelegate_1_t3020292135 * L_13 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_13, (Il2CppObject *)L_11, (IntPtr_t)L_12, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, (String_t*)L_10, (int32_t)0, (FacebookDelegate_1_t3020292135 *)L_13, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * L_14 = V_0;
		NullCheck(L_14);
		List_1_t278961118 * L_15 = (List_1_t278961118 *)L_14->get_UsersInfoList_0();
		NullCheck((List_1_t278961118 *)L_15);
		int32_t L_16 = List_1_get_Count_m2264133042((List_1_t278961118 *)L_15, /*hidden argument*/List_1_get_Count_m2264133042_MethodInfo_var);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0081;
		}
	}
	{
		U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * L_17 = V_0;
		NullCheck(L_17);
		List_1_t278961118 * L_18 = (List_1_t278961118 *)L_17->get_UsersInfoList_0();
		return L_18;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		List_1_t278961118 * L_19 = ((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)->static_fields)->get_scoreInfo_9();
		return L_19;
	}
}
// System.Collections.IEnumerator FBManager`1<System.Object>::CheckScoreUpdate()
extern "C"  Il2CppObject * FBManager_1_CheckScoreUpdate_m308373672_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * V_0 = NULL;
	{
		U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * L_0 = (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 26));
		((  void (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		V_0 = (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *)L_0;
		U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * L_2 = V_0;
		return L_2;
	}
}
// System.Void FBManager`1<System.Object>::PostStory(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1_PostStory_m3776293142_MetadataUsageId;
extern "C"  void FBManager_1_PostStory_m3776293142_gshared (FBManager_1_t2091981452 * __this, String_t* ___storyName0, String_t* ___storyCaption1, String_t* ___storyDescription2, String_t* ___imageURL3, String_t* ___linkURL4, String_t* ___userID5, String_t* ___mediaURL6, String_t* ___storyReference7, String_t* ___actionLink8, String_t* ___actionLinkName9, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_PostStory_m3776293142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_0, /*hidden argument*/NULL);
		V_0 = (Hashtable_t909839986 *)L_0;
		String_t* L_1 = ___storyName0;
		String_t* L_2 = ___storyCaption1;
		String_t* L_3 = ___storyDescription2;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_4 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, String_t*, String_t*, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)((FBManager_1_t2091981452 *)__this, (String_t*)L_1, (String_t*)L_2, (String_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		V_0 = (Hashtable_t909839986 *)L_4;
		String_t* L_5 = ___imageURL3;
		if (!L_5)
		{
			goto IL_0021;
		}
	}
	{
		Hashtable_t909839986 * L_6 = V_0;
		String_t* L_7 = ___imageURL3;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_8 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_6, (String_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		V_0 = (Hashtable_t909839986 *)L_8;
	}

IL_0021:
	{
		String_t* L_9 = ___linkURL4;
		if (!L_9)
		{
			goto IL_0032;
		}
	}
	{
		Hashtable_t909839986 * L_10 = V_0;
		String_t* L_11 = ___linkURL4;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_12 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_10, (String_t*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		V_0 = (Hashtable_t909839986 *)L_12;
	}

IL_0032:
	{
		String_t* L_13 = ___userID5;
		if (!L_13)
		{
			goto IL_0043;
		}
	}
	{
		Hashtable_t909839986 * L_14 = V_0;
		String_t* L_15 = ___userID5;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_16 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_14, (String_t*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		V_0 = (Hashtable_t909839986 *)L_16;
	}

IL_0043:
	{
		String_t* L_17 = ___mediaURL6;
		if (!L_17)
		{
			goto IL_0054;
		}
	}
	{
		Hashtable_t909839986 * L_18 = V_0;
		String_t* L_19 = ___mediaURL6;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_20 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_18, (String_t*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		V_0 = (Hashtable_t909839986 *)L_20;
	}

IL_0054:
	{
		String_t* L_21 = ___storyReference7;
		if (!L_21)
		{
			goto IL_0065;
		}
	}
	{
		Hashtable_t909839986 * L_22 = V_0;
		String_t* L_23 = ___storyReference7;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_24 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_22, (String_t*)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		V_0 = (Hashtable_t909839986 *)L_24;
	}

IL_0065:
	{
		String_t* L_25 = ___actionLink8;
		if (!L_25)
		{
			goto IL_0076;
		}
	}
	{
		Hashtable_t909839986 * L_26 = V_0;
		String_t* L_27 = ___actionLink8;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_28 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_26, (String_t*)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		V_0 = (Hashtable_t909839986 *)L_28;
	}

IL_0076:
	{
		String_t* L_29 = ___actionLinkName9;
		if (!L_29)
		{
			goto IL_0087;
		}
	}
	{
		Hashtable_t909839986 * L_30 = V_0;
		String_t* L_31 = ___actionLinkName9;
		NullCheck((FBManager_1_t2091981452 *)__this);
		Hashtable_t909839986 * L_32 = ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_30, (String_t*)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		V_0 = (Hashtable_t909839986 *)L_32;
	}

IL_0087:
	{
		Hashtable_t909839986 * L_33 = V_0;
		NullCheck((FBManager_1_t2091981452 *)__this);
		((  void (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)((FBManager_1_t2091981452 *)__this, (Hashtable_t909839986 *)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		bool L_34 = (bool)__this->get_loggedIn_4();
		if (!L_34)
		{
			goto IL_00a4;
		}
	}
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)((FBManager_1_t2091981452 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		goto IL_00ab;
	}

IL_00a4:
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void FBManager`1<System.Object>::LoginToFacebook(LoginEntrance) */, (FBManager_1_t2091981452 *)__this, (int32_t)2);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::PostStoryToFeed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m303745986_MethodInfo_var;
extern const uint32_t FBManager_1_PostStoryToFeed_m2787795195_MetadataUsageId;
extern "C"  void FBManager_1_PostStoryToFeed_m2787795195_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_PostStoryToFeed_m2787795195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FacebookStory_t448600801 * L_1 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		NullCheck(L_1);
		String_t* L_2 = (String_t*)L_1->get_linkURL_1();
		Uri_t19570940 * L_3 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m1248212436(L_3, (String_t*)L_2, /*hidden argument*/NULL);
		FacebookStory_t448600801 * L_4 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_linkName_2();
		FacebookStory_t448600801 * L_6 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		NullCheck(L_6);
		String_t* L_7 = (String_t*)L_6->get_linkCaption_3();
		FacebookStory_t448600801 * L_8 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		NullCheck(L_8);
		String_t* L_9 = (String_t*)L_8->get_linkDescription_4();
		FacebookStory_t448600801 * L_10 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		NullCheck(L_10);
		String_t* L_11 = (String_t*)L_10->get_linkImage_5();
		Uri_t19570940 * L_12 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m1248212436(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 39));
		FacebookDelegate_1_t4160439974 * L_15 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_15, (Il2CppObject *)__this, (IntPtr_t)L_14, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_FeedShare_m1853999185(NULL /*static, unused*/, (String_t*)L_0, (Uri_t19570940 *)L_3, (String_t*)L_5, (String_t*)L_7, (String_t*)L_9, (Uri_t19570940 *)L_12, (String_t*)L_13, (FacebookDelegate_1_t4160439974 *)L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBManager`1<System.Object>::StoryFeedCallback(Facebook.Unity.IShareResult)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3487517976;
extern Il2CppCodeGenString* _stringLiteral433537963;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral3323264318;
extern const uint32_t FBManager_1_StoryFeedCallback_m3748462935_MetadataUsageId;
extern "C"  void FBManager_1_StoryFeedCallback_m3748462935_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_StoryFeedCallback_m3748462935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___response0;
		NullCheck((Il2CppObject *)L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral3487517976, (String_t*)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___response0;
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		Il2CppObject * L_6 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, (String_t*)L_5, /*hidden argument*/NULL);
		V_0 = (Hashtable_t909839986 *)((Hashtable_t909839986 *)Castclass(L_6, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_7 = V_0;
		NullCheck((Hashtable_t909839986 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_7, (Il2CppObject *)_stringLiteral433537963);
		if (!L_8)
		{
			goto IL_0098;
		}
	}
	{
		Hashtable_t909839986 * L_9 = V_0;
		NullCheck((Hashtable_t909839986 *)L_9);
		Il2CppObject * L_10 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_9, (Il2CppObject *)_stringLiteral433537963);
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_11, (String_t*)_stringLiteral3323263070, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_007e;
		}
	}
	{
		Hashtable_t909839986 * L_13 = V_0;
		NullCheck((Hashtable_t909839986 *)L_13);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_13, (Il2CppObject *)_stringLiteral433537963);
		NullCheck((Il2CppObject *)L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_15, (String_t*)_stringLiteral3323264318, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_008b;
		}
	}

IL_007e:
	{
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker2< bool, Hashtable_t909839986 * >::Invoke(12 /* System.Void FBManager`1<System.Object>::StoryPosted(System.Boolean,System.Collections.Hashtable) */, (FBManager_1_t2091981452 *)__this, (bool)0, (Hashtable_t909839986 *)NULL);
		goto IL_0093;
	}

IL_008b:
	{
		Hashtable_t909839986 * L_17 = V_0;
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker2< bool, Hashtable_t909839986 * >::Invoke(12 /* System.Void FBManager`1<System.Object>::StoryPosted(System.Boolean,System.Collections.Hashtable) */, (FBManager_1_t2091981452 *)__this, (bool)1, (Hashtable_t909839986 *)L_17);
	}

IL_0093:
	{
		goto IL_00a0;
	}

IL_0098:
	{
		Hashtable_t909839986 * L_18 = V_0;
		NullCheck((FBManager_1_t2091981452 *)__this);
		VirtActionInvoker2< bool, Hashtable_t909839986 * >::Invoke(12 /* System.Void FBManager`1<System.Object>::StoryPosted(System.Boolean,System.Collections.Hashtable) */, (FBManager_1_t2091981452 *)__this, (bool)1, (Hashtable_t909839986 *)L_18);
	}

IL_00a0:
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::StoryPosted(System.Boolean,System.Collections.Hashtable)
extern "C"  void FBManager_1_StoryPosted_m1151905406_gshared (FBManager_1_t2091981452 * __this, bool ___success0, Hashtable_t909839986 * ___data1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FBManager`1<System.Object>::CreateStory(System.Collections.Hashtable)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookStory_t448600801_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1273082750;
extern Il2CppCodeGenString* _stringLiteral1641386439;
extern Il2CppCodeGenString* _stringLiteral817128595;
extern Il2CppCodeGenString* _stringLiteral334266244;
extern Il2CppCodeGenString* _stringLiteral3638239252;
extern Il2CppCodeGenString* _stringLiteral273947371;
extern Il2CppCodeGenString* _stringLiteral4246179526;
extern Il2CppCodeGenString* _stringLiteral3531870703;
extern Il2CppCodeGenString* _stringLiteral255567276;
extern Il2CppCodeGenString* _stringLiteral1902204853;
extern const uint32_t FBManager_1_CreateStory_m3878977026_MetadataUsageId;
extern "C"  void FBManager_1_CreateStory_m3878977026_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHashTable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_CreateStory_m3878977026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FacebookStory_t448600801 * L_10 = (FacebookStory_t448600801 *)il2cpp_codegen_object_new(FacebookStory_t448600801_il2cpp_TypeInfo_var);
		FacebookStory__ctor_m4104264714(L_10, (String_t*)L_0, (String_t*)L_1, (String_t*)L_2, (String_t*)L_3, (String_t*)L_4, (String_t*)L_5, (String_t*)L_6, (String_t*)L_7, (String_t*)L_8, (String_t*)L_9, /*hidden argument*/NULL);
		__this->set_userStory_8(L_10);
		Hashtable_t909839986 * L_11 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_11);
		bool L_12 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_11, (Il2CppObject *)_stringLiteral1273082750);
		if (!L_12)
		{
			goto IL_0068;
		}
	}
	{
		FacebookStory_t448600801 * L_13 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_14 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_14);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_14, (Il2CppObject *)_stringLiteral1273082750);
		NullCheck((Il2CppObject *)L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_15);
		NullCheck(L_13);
		L_13->set_toID_0(L_16);
	}

IL_0068:
	{
		Hashtable_t909839986 * L_17 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_17);
		bool L_18 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_17, (Il2CppObject *)_stringLiteral1641386439);
		if (!L_18)
		{
			goto IL_0093;
		}
	}
	{
		FacebookStory_t448600801 * L_19 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_20 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_20);
		Il2CppObject * L_21 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_20, (Il2CppObject *)_stringLiteral1641386439);
		NullCheck((Il2CppObject *)L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_21);
		NullCheck(L_19);
		L_19->set_linkURL_1(L_22);
	}

IL_0093:
	{
		Hashtable_t909839986 * L_23 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_23);
		bool L_24 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_23, (Il2CppObject *)_stringLiteral817128595);
		if (!L_24)
		{
			goto IL_00be;
		}
	}
	{
		FacebookStory_t448600801 * L_25 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_26 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_26);
		Il2CppObject * L_27 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_26, (Il2CppObject *)_stringLiteral817128595);
		NullCheck((Il2CppObject *)L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_27);
		NullCheck(L_25);
		L_25->set_linkName_2(L_28);
	}

IL_00be:
	{
		Hashtable_t909839986 * L_29 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_29);
		bool L_30 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_29, (Il2CppObject *)_stringLiteral334266244);
		if (!L_30)
		{
			goto IL_00e9;
		}
	}
	{
		FacebookStory_t448600801 * L_31 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_32 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_32);
		Il2CppObject * L_33 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_32, (Il2CppObject *)_stringLiteral334266244);
		NullCheck((Il2CppObject *)L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_33);
		NullCheck(L_31);
		L_31->set_linkCaption_3(L_34);
	}

IL_00e9:
	{
		Hashtable_t909839986 * L_35 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_35);
		bool L_36 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_35, (Il2CppObject *)_stringLiteral3638239252);
		if (!L_36)
		{
			goto IL_0114;
		}
	}
	{
		FacebookStory_t448600801 * L_37 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_38 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_38);
		Il2CppObject * L_39 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_38, (Il2CppObject *)_stringLiteral3638239252);
		NullCheck((Il2CppObject *)L_39);
		String_t* L_40 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_39);
		NullCheck(L_37);
		L_37->set_linkDescription_4(L_40);
	}

IL_0114:
	{
		Hashtable_t909839986 * L_41 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_41);
		bool L_42 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_41, (Il2CppObject *)_stringLiteral273947371);
		if (!L_42)
		{
			goto IL_013f;
		}
	}
	{
		FacebookStory_t448600801 * L_43 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_44 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_44);
		Il2CppObject * L_45 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_44, (Il2CppObject *)_stringLiteral273947371);
		NullCheck((Il2CppObject *)L_45);
		String_t* L_46 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_45);
		NullCheck(L_43);
		L_43->set_linkImage_5(L_46);
	}

IL_013f:
	{
		Hashtable_t909839986 * L_47 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_47);
		bool L_48 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_47, (Il2CppObject *)_stringLiteral4246179526);
		if (!L_48)
		{
			goto IL_016a;
		}
	}
	{
		FacebookStory_t448600801 * L_49 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_50 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_50);
		Il2CppObject * L_51 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_50, (Il2CppObject *)_stringLiteral4246179526);
		NullCheck((Il2CppObject *)L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_51);
		NullCheck(L_49);
		L_49->set_linkMedia_6(L_52);
	}

IL_016a:
	{
		Hashtable_t909839986 * L_53 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_53);
		bool L_54 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_53, (Il2CppObject *)_stringLiteral3531870703);
		if (!L_54)
		{
			goto IL_0195;
		}
	}
	{
		FacebookStory_t448600801 * L_55 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_56 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_56);
		Il2CppObject * L_57 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_56, (Il2CppObject *)_stringLiteral3531870703);
		NullCheck((Il2CppObject *)L_57);
		String_t* L_58 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_57);
		NullCheck(L_55);
		L_55->set_linkReference_9(L_58);
	}

IL_0195:
	{
		Hashtable_t909839986 * L_59 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_59);
		bool L_60 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_59, (Il2CppObject *)_stringLiteral255567276);
		if (!L_60)
		{
			goto IL_01c0;
		}
	}
	{
		FacebookStory_t448600801 * L_61 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_62 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_62);
		Il2CppObject * L_63 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_62, (Il2CppObject *)_stringLiteral255567276);
		NullCheck((Il2CppObject *)L_63);
		String_t* L_64 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_63);
		NullCheck(L_61);
		L_61->set_actionLink_7(L_64);
	}

IL_01c0:
	{
		Hashtable_t909839986 * L_65 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_65);
		bool L_66 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_65, (Il2CppObject *)_stringLiteral1902204853);
		if (!L_66)
		{
			goto IL_01eb;
		}
	}
	{
		FacebookStory_t448600801 * L_67 = (FacebookStory_t448600801 *)__this->get_userStory_8();
		Hashtable_t909839986 * L_68 = ___StoryHashTable0;
		NullCheck((Hashtable_t909839986 *)L_68);
		Il2CppObject * L_69 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_68, (Il2CppObject *)_stringLiteral1902204853);
		NullCheck((Il2CppObject *)L_69);
		String_t* L_70 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_69);
		NullCheck(L_67);
		L_67->set_actionName_8(L_70);
	}

IL_01eb:
	{
		return;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::BaseStoryHash(System.String,System.String,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral817128595;
extern Il2CppCodeGenString* _stringLiteral334266244;
extern Il2CppCodeGenString* _stringLiteral3638239252;
extern const uint32_t FBManager_1_BaseStoryHash_m3957017830_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_BaseStoryHash_m3957017830_gshared (FBManager_1_t2091981452 * __this, String_t* ___LinkName0, String_t* ___LinkCaption1, String_t* ___LinkDescription2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_BaseStoryHash_m3957017830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_0, /*hidden argument*/NULL);
		V_0 = (Hashtable_t909839986 *)L_0;
		Hashtable_t909839986 * L_1 = V_0;
		String_t* L_2 = ___LinkName0;
		NullCheck((Hashtable_t909839986 *)L_1);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_1, (Il2CppObject *)_stringLiteral817128595, (Il2CppObject *)L_2);
		Hashtable_t909839986 * L_3 = V_0;
		String_t* L_4 = ___LinkCaption1;
		NullCheck((Hashtable_t909839986 *)L_3);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_3, (Il2CppObject *)_stringLiteral334266244, (Il2CppObject *)L_4);
		Hashtable_t909839986 * L_5 = V_0;
		String_t* L_6 = ___LinkDescription2;
		NullCheck((Hashtable_t909839986 *)L_5);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_5, (Il2CppObject *)_stringLiteral3638239252, (Il2CppObject *)L_6);
		Hashtable_t909839986 * L_7 = V_0;
		return L_7;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddImageToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral273947371;
extern const uint32_t FBManager_1_AddImageToStory_m964751696_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddImageToStory_m964751696_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___ImageURL1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddImageToStory_m964751696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___ImageURL1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral273947371, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddLinkToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral1641386439;
extern const uint32_t FBManager_1_AddLinkToStory_m1070610637_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddLinkToStory_m1070610637_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___LinkURL1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddLinkToStory_m1070610637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___LinkURL1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral1641386439, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddUserIDToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral1273082750;
extern const uint32_t FBManager_1_AddUserIDToStory_m1119652137_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddUserIDToStory_m1119652137_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___UserID1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddUserIDToStory_m1119652137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___UserID1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral1273082750, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddMediaToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral4246179526;
extern const uint32_t FBManager_1_AddMediaToStory_m622260141_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddMediaToStory_m622260141_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___MediaURL1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddMediaToStory_m622260141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___MediaURL1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral4246179526, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddReferenceToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral3531870703;
extern const uint32_t FBManager_1_AddReferenceToStory_m4103099812_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddReferenceToStory_m4103099812_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___StoryReference1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddReferenceToStory_m4103099812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___StoryReference1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral3531870703, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddActionLinkToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral255567276;
extern const uint32_t FBManager_1_AddActionLinkToStory_m508191515_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddActionLinkToStory_m508191515_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___ActionLink1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddActionLinkToStory_m508191515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___ActionLink1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral255567276, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Collections.Hashtable FBManager`1<System.Object>::AddActionNameToStory(System.Collections.Hashtable,System.String)
extern Il2CppCodeGenString* _stringLiteral1902204853;
extern const uint32_t FBManager_1_AddActionNameToStory_m453440656_MetadataUsageId;
extern "C"  Hashtable_t909839986 * FBManager_1_AddActionNameToStory_m453440656_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___ActionLinkName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_AddActionNameToStory_m453440656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___StoryHash0;
		String_t* L_1 = ___ActionLinkName1;
		NullCheck((Hashtable_t909839986 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)_stringLiteral1902204853, (Il2CppObject *)L_1);
		Hashtable_t909839986 * L_2 = ___StoryHash0;
		return L_2;
	}
}
// System.Void FBManager`1<System.Object>::LoadPictureAPI(System.String,FBManager`1/LoadPictureCallback<ManagerType>)
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern const uint32_t FBManager_1_LoadPictureAPI_m1618865842_MetadataUsageId;
extern "C"  void FBManager_1_LoadPictureAPI_m1618865842_gshared (FBManager_1_t2091981452 * __this, String_t* ___url0, LoadPictureCallback_t2854975038 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_LoadPictureAPI_m1618865842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * V_0 = NULL;
	{
		U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * L_0 = (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 41));
		((  void (*) (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 42));
		V_0 = (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 *)L_0;
		U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * L_1 = V_0;
		LoadPictureCallback_t2854975038 * L_2 = ___callback1;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		String_t* L_4 = ___url0;
		U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 43));
		FacebookDelegate_1_t3020292135 * L_7 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, (String_t*)L_4, (int32_t)0, (FacebookDelegate_1_t3020292135 *)L_7, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator FBManager`1<System.Object>::LoadPictureEnumerator(System.String,FBManager`1/LoadPictureCallback<ManagerType>)
extern "C"  Il2CppObject * FBManager_1_LoadPictureEnumerator_m2946817620_gshared (FBManager_1_t2091981452 * __this, String_t* ___url0, LoadPictureCallback_t2854975038 * ___callback1, const MethodInfo* method)
{
	U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * V_0 = NULL;
	{
		U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * L_0 = (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 44));
		((  void (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 45));
		V_0 = (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *)L_0;
		U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * L_3 = V_0;
		LoadPictureCallback_t2854975038 * L_4 = ___callback1;
		NullCheck(L_3);
		L_3->set_callback_2(L_4);
		U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * L_5 = V_0;
		return L_5;
	}
}
// System.Void FBManager`1<System.Object>::MyPictureCallback(UnityEngine.Texture)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m255206972_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3015944840;
extern const uint32_t FBManager_1_MyPictureCallback_m4066877448_MetadataUsageId;
extern "C"  void FBManager_1_MyPictureCallback_m4066877448_gshared (FBManager_1_t2091981452 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_MyPictureCallback_m4066877448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t2243626319 * L_0 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		Nullable_1_t334943763  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m255206972(&L_2, (int32_t)((int32_t)128), /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		Nullable_1_t334943763  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m255206972(&L_3, (int32_t)((int32_t)128), /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		String_t* L_4 = ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, Nullable_1_t334943763 , Nullable_1_t334943763 , String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (String_t*)_stringLiteral3015944840, (Nullable_1_t334943763 )L_2, (Nullable_1_t334943763 )L_3, (String_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 13));
		LoadPictureCallback_t2854975038 * L_6 = (LoadPictureCallback_t2854975038 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13));
		((  void (*) (LoadPictureCallback_t2854975038 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		NullCheck((FBManager_1_t2091981452 *)__this);
		((  void (*) (FBManager_1_t2091981452 *, String_t*, LoadPictureCallback_t2854975038 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((FBManager_1_t2091981452 *)__this, (String_t*)L_4, (LoadPictureCallback_t2854975038 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}

IL_003f:
	{
		FacebookPlayer_t2645999341 * L_7 = (FacebookPlayer_t2645999341 *)__this->get_loggedInUser_6();
		Texture_t2243626319 * L_8 = ___texture0;
		NullCheck((FacebookUser_t244724091 *)L_7);
		FacebookUser_SetUserProfilePicture_m3527551772((FacebookUser_t244724091 *)L_7, (Texture_t2243626319 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.String FBManager`1<System.Object>::DeserializePictureURLString(System.String)
extern Il2CppClass* Json_t151732106_il2cpp_TypeInfo_var;
extern const uint32_t FBManager_1_DeserializePictureURLString_m1044315199_MetadataUsageId;
extern "C"  String_t* FBManager_1_DeserializePictureURLString_m1044315199_gshared (FBManager_1_t2091981452 * __this, String_t* ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_DeserializePictureURLString_m1044315199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___response0;
		IL2CPP_RUNTIME_CLASS_INIT(Json_t151732106_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = Json_Deserialize_m1129260701(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		NullCheck((FBManager_1_t2091981452 *)__this);
		String_t* L_2 = ((  String_t* (*) (FBManager_1_t2091981452 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46)->methodPointer)((FBManager_1_t2091981452 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 46));
		return L_2;
	}
}
// System.String FBManager`1<System.Object>::DeserializePictureURLObject(System.Object)
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m104737093_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2619694;
extern Il2CppCodeGenString* _stringLiteral386853519;
extern const uint32_t FBManager_1_DeserializePictureURLObject_m1113785023_MetadataUsageId;
extern "C"  String_t* FBManager_1_DeserializePictureURLObject_m1113785023_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___pictureObj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_DeserializePictureURLObject_m1113785023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___pictureObj0;
		NullCheck((Dictionary_2_t309261261 *)((Dictionary_2_t309261261 *)Castclass(L_0, Dictionary_2_t309261261_il2cpp_TypeInfo_var)));
		Il2CppObject * L_1 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)((Dictionary_2_t309261261 *)Castclass(L_0, Dictionary_2_t309261261_il2cpp_TypeInfo_var)), (String_t*)_stringLiteral2619694, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		V_0 = (Dictionary_2_t309261261 *)((Dictionary_2_t309261261 *)Castclass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_2 = V_0;
		NullCheck((Dictionary_2_t309261261 *)L_2);
		bool L_3 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_2, (String_t*)_stringLiteral386853519, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Dictionary_2_t309261261 * L_4 = V_0;
		NullCheck((Dictionary_2_t309261261 *)L_4);
		Il2CppObject * L_5 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_4, (String_t*)_stringLiteral386853519, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		NullCheck((Il2CppObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_5);
		__this->set_pictureURL_7(L_6);
	}

IL_003c:
	{
		V_1 = (Il2CppObject *)NULL;
		Dictionary_2_t309261261 * L_7 = V_0;
		NullCheck((Dictionary_2_t309261261 *)L_7);
		bool L_8 = Dictionary_2_TryGetValue_m104737093((Dictionary_2_t309261261 *)L_7, (String_t*)_stringLiteral386853519, (Il2CppObject **)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m104737093_MethodInfo_var);
		if (!L_8)
		{
			goto IL_0057;
		}
	}
	{
		Il2CppObject * L_9 = V_1;
		return ((String_t*)Castclass(L_9, String_t_il2cpp_TypeInfo_var));
	}

IL_0057:
	{
		return (String_t*)NULL;
	}
}
// System.String FBManager`1<System.Object>::GetPictureURL(System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t334943763_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2294837272_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1407553946;
extern Il2CppCodeGenString* _stringLiteral2302031961;
extern Il2CppCodeGenString* _stringLiteral449795626;
extern Il2CppCodeGenString* _stringLiteral2725807435;
extern Il2CppCodeGenString* _stringLiteral4076359698;
extern Il2CppCodeGenString* _stringLiteral1853145380;
extern const uint32_t FBManager_1_GetPictureURL_m2272155829_MetadataUsageId;
extern "C"  String_t* FBManager_1_GetPictureURL_m2272155829_gshared (Il2CppObject * __this /* static, unused */, String_t* ___facebookID0, Nullable_1_t334943763  ___width1, Nullable_1_t334943763  ___height2, String_t* ___type3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_GetPictureURL_m2272155829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	{
		String_t* L_0 = ___facebookID0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2024975688(NULL /*static, unused*/, (String_t*)_stringLiteral1407553946, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		V_0 = (String_t*)L_1;
		bool L_2 = Nullable_1_get_HasValue_m2294837272((Nullable_1_t334943763 *)(&___width1), /*hidden argument*/Nullable_1_get_HasValue_m2294837272_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		Il2CppClass* il2cpp_this_typeinfo_37 = Nullable_1_t334943763_il2cpp_TypeInfo_var;
		String_t* L_3 = ((  String_t* (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_37->vtable[3].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&___width1)), /*hidden argument*/il2cpp_this_typeinfo_37->vtable[3].method);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2302031961, (String_t*)L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0039;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
	}

IL_0039:
	{
		V_1 = (String_t*)G_B3_0;
		String_t* L_6 = V_1;
		bool L_7 = Nullable_1_get_HasValue_m2294837272((Nullable_1_t334943763 *)(&___height2), /*hidden argument*/Nullable_1_get_HasValue_m2294837272_MethodInfo_var);
		G_B4_0 = L_6;
		if (!L_7)
		{
			G_B5_0 = L_6;
			goto IL_0063;
		}
	}
	{
		Il2CppClass* il2cpp_this_typeinfo_84 = Nullable_1_t334943763_il2cpp_TypeInfo_var;
		String_t* L_8 = ((  String_t* (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_84->vtable[3].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&___height2)), /*hidden argument*/il2cpp_this_typeinfo_84->vtable[3].method);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral449795626, (String_t*)L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		goto IL_0068;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_10;
		G_B6_1 = G_B5_0;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)G_B6_1, (String_t*)G_B6_0, /*hidden argument*/NULL);
		V_1 = (String_t*)L_11;
		String_t* L_12 = V_1;
		String_t* L_13 = ___type3;
		G_B7_0 = L_12;
		if (!L_13)
		{
			G_B8_0 = L_12;
			goto IL_0085;
		}
	}
	{
		String_t* L_14 = ___type3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2725807435, (String_t*)L_14, /*hidden argument*/NULL);
		G_B9_0 = L_15;
		G_B9_1 = G_B7_0;
		goto IL_008a;
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B9_0 = L_16;
		G_B9_1 = G_B8_0;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)G_B9_1, (String_t*)G_B9_0, /*hidden argument*/NULL);
		V_1 = (String_t*)L_17;
		String_t* L_18 = V_1;
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)L_18, (String_t*)_stringLiteral4076359698, /*hidden argument*/NULL);
		V_1 = (String_t*)L_19;
		String_t* L_20 = V_1;
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_22 = String_op_Inequality_m304203149(NULL /*static, unused*/, (String_t*)L_20, (String_t*)L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b9;
		}
	}
	{
		String_t* L_23 = V_0;
		String_t* L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_23, (String_t*)_stringLiteral1853145380, (String_t*)L_24, /*hidden argument*/NULL);
		V_0 = (String_t*)L_25;
	}

IL_00b9:
	{
		String_t* L_26 = V_0;
		return L_26;
	}
}
// System.Void FBManager`1<System.Object>::.cctor()
extern Il2CppClass* List_1_t278961118_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2926404389_MethodInfo_var;
extern const uint32_t FBManager_1__cctor_m129197430_MetadataUsageId;
extern "C"  void FBManager_1__cctor_m129197430_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1__cctor_m129197430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t278961118 * L_0 = (List_1_t278961118 *)il2cpp_codegen_object_new(List_1_t278961118_il2cpp_TypeInfo_var);
		List_1__ctor_m2926404389(L_0, /*hidden argument*/List_1__ctor_m2926404389_MethodInfo_var);
		((FBManager_1_t2091981452_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->set_scoreInfo_9(L_0);
		return;
	}
}
// System.Void FBManager`1<System.Object>::<_PostHighScore>m__0(Facebook.Unity.IGraphResult)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1966417266;
extern const uint32_t FBManager_1_U3C_PostHighScoreU3Em__0_m235639230_MetadataUsageId;
extern "C"  void FBManager_1_U3C_PostHighScoreU3Em__0_m235639230_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FBManager_1_U3C_PostHighScoreU3Em__0_m235639230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___result0;
		NullCheck((Il2CppObject *)L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral1966417266, (String_t*)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void GameState`1<System.Object>::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GameState_1__ctor_m299346719_MetadataUsageId;
extern "C"  void GameState_1__ctor_m299346719_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameState_1__ctor_m299346719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_activeState_3(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_previousState_4(L_1);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GameState`1<System.Object>::Init()
extern "C"  void GameState_1_Init_m290996569_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Application_get_loadedLevelName_m1151756873(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_activeState_3(L_0);
		return;
	}
}
// System.Void GameState`1<System.Object>::DontDestroyOnLoad()
extern "C"  void GameState_1_DontDestroyOnLoad_m4221039203_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameState`1<System.Object>::LoadScene(System.String,System.Boolean,System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2261240688;
extern const uint32_t GameState_1_LoadScene_m1399394461_MetadataUsageId;
extern "C"  void GameState_1_LoadScene_m1399394461_gshared (GameState_1_t846809420 * __this, String_t* ___sceneName0, bool ___aSync1, bool ___garbageCollect2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameState_1_LoadScene_m1399394461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2261240688, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		String_t* L_3 = ___sceneName0;
		NullCheck((GameState_1_t846809420 *)__this);
		((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((GameState_1_t846809420 *)__this, (String_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		bool L_4 = ___aSync1;
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_5 = ___sceneName0;
		bool L_6 = ___garbageCollect2;
		NullCheck((GameState_1_t846809420 *)__this);
		VirtActionInvoker2< String_t*, bool >::Invoke(7 /* System.Void GameState`1<System.Object>::LoadLevelAsync(System.String,System.Boolean) */, (GameState_1_t846809420 *)__this, (String_t*)L_5, (bool)L_6);
		goto IL_003b;
	}

IL_0034:
	{
		String_t* L_7 = ___sceneName0;
		NullCheck((GameState_1_t846809420 *)__this);
		((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((GameState_1_t846809420 *)__this, (String_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003b:
	{
		return;
	}
}
// System.Void GameState`1<System.Object>::StartActiveScene()
extern "C"  void GameState_1_StartActiveScene_m2386984001_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_activeState_3();
		NullCheck((GameState_1_t846809420 *)__this);
		((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((GameState_1_t846809420 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void GameState`1<System.Object>::LoadLevel(System.String)
extern "C"  void GameState_1_LoadLevel_m1307252913_gshared (GameState_1_t846809420 * __this, String_t* ___level0, const MethodInfo* method)
{
	{
		NullCheck((GameState_1_t846809420 *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void GameState`1<System.Object>::PreLevelLoad() */, (GameState_1_t846809420 *)__this);
		NullCheck((GameState_1_t846809420 *)__this);
		VirtActionInvoker0::Invoke(5 /* System.Void GameState`1<System.Object>::DontDestroyOnLoad() */, (GameState_1_t846809420 *)__this);
		String_t* L_0 = ___level0;
		Application_LoadLevel_m393995325(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		Resources_UnloadUnusedAssets_m2770025609(NULL /*static, unused*/, /*hidden argument*/NULL);
		GC_Collect_m2249328497(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameState`1<System.Object>::PreLevelLoad()
extern "C"  void GameState_1_PreLevelLoad_m3232301818_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameState`1<System.Object>::LoadLevelAsync(System.String,System.Boolean)
extern "C"  void GameState_1_LoadLevelAsync_m731125564_gshared (GameState_1_t846809420 * __this, String_t* ___level0, bool ___garbageCollect1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameState`1<System.Object>::LoadState(System.String)
extern "C"  void GameState_1_LoadState_m3432771088_gshared (GameState_1_t846809420 * __this, String_t* ___nextState0, const MethodInfo* method)
{
	{
		NullCheck((GameState_1_t846809420 *)__this);
		((  void (*) (GameState_1_t846809420 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((GameState_1_t846809420 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		String_t* L_0 = ___nextState0;
		NullCheck((GameState_1_t846809420 *)__this);
		((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((GameState_1_t846809420 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Void GameState`1<System.Object>::SetPreviousState()
extern "C"  void GameState_1_SetPreviousState_m485008655_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_activeState_3();
		__this->set_previousState_4(L_0);
		return;
	}
}
// System.Void GameState`1<System.Object>::SetNextState(System.String)
extern "C"  void GameState_1_SetNextState_m1387511123_gshared (GameState_1_t846809420 * __this, String_t* ___nextState0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___nextState0;
		__this->set_activeState_3(L_0);
		return;
	}
}
// System.String GameState`1<System.Object>::getActiveScene()
extern "C"  String_t* GameState_1_getActiveScene_m4069071568_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_activeState_3();
		return L_0;
	}
}
// System.String GameState`1<System.Object>::getPreviousScene()
extern "C"  String_t* GameState_1_getPreviousScene_m1851301131_gshared (GameState_1_t846809420 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_previousState_4();
		return L_0;
	}
}
// System.Void LitJson.ExporterFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m104989128_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m1089798616_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m1089798616((ExporterFunc_1_t3926562242 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<System.Object>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m729158479_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2442068774_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ImporterFunc`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc_2__ctor_m2910045588_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::Invoke(TJson)
extern "C"  Il2CppObject * ImporterFunc_2_Invoke_m3373032134_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_2_Invoke_m3373032134((ImporterFunc_2_t2548729379 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ImporterFunc`2<System.Object,System.Object>::BeginInvoke(TJson,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_2_BeginInvoke_m2065437887_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_2_EndInvoke_m2375822358_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey0`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey0_1__ctor_m2178726344_gshared (U3CRegisterExporterU3Ec__AnonStorey0_1_t3447138843 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey0`1<System.Object>::<>m__0(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey0_1_U3CU3Em__0_m1160532603_gshared (U3CRegisterExporterU3Ec__AnonStorey0_1_t3447138843 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3926562242 * L_0 = (ExporterFunc_1_t3926562242 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3926562242 *)L_0);
		((  void (*) (ExporterFunc_1_t3926562242 *, Il2CppObject *, JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3926562242 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey1_2__ctor_m2787776139_gshared (U3CRegisterImporterU3Ec__AnonStorey1_2_t1381013311 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1`2<System.Object,System.Object>::<>m__0(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey1_2_U3CU3Em__0_m1308246711_gshared (U3CRegisterImporterU3Ec__AnonStorey1_2_t1381013311 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	{
		ImporterFunc_2_t2548729379 * L_0 = (ImporterFunc_2_t2548729379 *)__this->get_importer_0();
		Il2CppObject * L_1 = ___input0;
		NullCheck((ImporterFunc_2_t2548729379 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ImporterFunc_2_t2548729379 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ImporterFunc_2_t2548729379 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_2;
	}
}
// System.Void MonoSingleton`1<System.Object>::.ctor()
extern "C"  void MonoSingleton_1__ctor_m437210476_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MonoSingleton`1<System.Object>::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MonoSingleton_1_Awake_m2844725271_MetadataUsageId;
extern "C"  void MonoSingleton_1_Awake_m2844725271_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoSingleton_1_Awake_m2844725271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_0 = ((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_m_Instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_m_Instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
	}

IL_0025:
	{
		return;
	}
}
// T MonoSingleton`1<System.Object>::get_Instance()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4178851449;
extern Il2CppCodeGenString* _stringLiteral3862340868;
extern Il2CppCodeGenString* _stringLiteral1562035989;
extern const uint32_t MonoSingleton_1_get_Instance_m4076012193_MetadataUsageId;
extern "C"  Il2CppObject * MonoSingleton_1_get_Instance_m4076012193_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoSingleton_1_get_Instance_m4076012193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_Instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_m_Instance_2(((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		Il2CppObject * L_4 = ((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_Instance_2();
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_6 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral4178851449, (String_t*)L_8, (String_t*)_stringLiteral3862340868, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
	}

IL_0070:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		GameObject_t1756533147 * L_12 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_12);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((GameObject_t1756533147 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_m_Instance_2(L_13);
		Il2CppObject * L_14 = ((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_Instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_16 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral1562035989, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)L_19, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_20 = ((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_Instance_2();
		return L_20;
	}
}
// System.Boolean MonoSingleton`1<System.Object>::IsAvailable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MonoSingleton_1_IsAvailable_m527113581_MetadataUsageId;
extern "C"  bool MonoSingleton_1_IsAvailable_m527113581_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoSingleton_1_IsAvailable_m527113581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_Instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		return (bool)1;
	}
}
// System.Void MonoSingleton`1<System.Object>::ForceInstanceInit()
extern "C"  void MonoSingleton_1_ForceInstanceInit_m3222864930_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MonoSingleton`1<System.Object>::OnApplicationQuit()
extern "C"  void MonoSingleton_1_OnApplicationQuit_m750638130_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((MonoSingleton_1_t2440115015_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_m_Instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void MonoSingleton`1<System.Object>::.cctor()
extern "C"  void MonoSingleton_1__cctor_m590081433_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::.ctor()
extern "C"  void NavigationController_2__ctor_m890861153_gshared (NavigationController_2_t1832792260 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t3314011636 * L_0 = (List_1_t3314011636 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1452032531_gshared (NavigationController_2_t1832792260 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1579095628_gshared (NavigationController_2_t1832792260 * __this, const MethodInfo* method)
{
	{
		List_1_t3314011636 * L_0 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3314011636 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3314011636 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t3314011636 * L_2 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		List_1_t3314011636 * L_3 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3314011636 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3314011636 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3314011636 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3314011636 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t1832792260 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,AttackHomeNav>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t3314011636 * L_9 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		List_1_t3314011636 * L_10 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3314011636 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3314011636 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3314011636 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3314011636 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t3314011636 * L_13 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		List_1_t3314011636 * L_14 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3314011636 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3314011636 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3314011636 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3314011636 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t3314011636 * L_17 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		List_1_t3314011636 * L_18 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3314011636 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3314011636 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3314011636 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3314011636 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t3314011636 * L_21 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		List_1_t3314011636 * L_22 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3314011636 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t3314011636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3314011636 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3314011636 *)L_21);
		((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3314011636 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m2230020555_gshared (NavigationController_2_t1832792260 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t1832792260 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,AttackHomeNav>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t1832792260 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackHomeNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t1832792260 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3314011636 * L_15 = (List_1_t3314011636 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t3314011636 *)L_15);
		((  void (*) (List_1_t3314011636 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3314011636 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,AttackHomeNav>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m4172558399_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,AttackHomeNav>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1897244818_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3898453107_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m602541607_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackHomeNav>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2502305939_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2423512526_gshared (NavigationController_2_t2219628511 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t3700847887 * L_0 = (List_1_t3700847887 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1224047836_gshared (NavigationController_2_t2219628511 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m312182917_gshared (NavigationController_2_t2219628511 * __this, const MethodInfo* method)
{
	{
		List_1_t3700847887 * L_0 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3700847887 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3700847887 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t3700847887 * L_2 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		List_1_t3700847887 * L_3 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3700847887 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3700847887 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3700847887 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3700847887 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2219628511 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,AttackNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t3700847887 * L_9 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		List_1_t3700847887 * L_10 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3700847887 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3700847887 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3700847887 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3700847887 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t3700847887 * L_13 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		List_1_t3700847887 * L_14 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3700847887 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3700847887 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3700847887 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3700847887 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t3700847887 * L_17 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		List_1_t3700847887 * L_18 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3700847887 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3700847887 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3700847887 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3700847887 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t3700847887 * L_21 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		List_1_t3700847887 * L_22 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3700847887 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3700847887 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3700847887 *)L_21);
		((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3700847887 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1564361018_gshared (NavigationController_2_t2219628511 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t2219628511 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,AttackNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t2219628511 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2219628511 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3700847887 * L_15 = (List_1_t3700847887 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t3700847887 *)L_15);
		((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3700847887 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,AttackNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m3710279390_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,AttackNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m775835947_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m2565550832_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m1108255294_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m236222014_gshared (NavigationController_2_t2219628511 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::.ctor()
extern "C"  void NavigationController_2__ctor_m2534835378_gshared (NavigationController_2_t2145786393 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t3627005769 * L_0 = (List_1_t3627005769 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m3679967428_gshared (NavigationController_2_t2145786393 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3208631707_gshared (NavigationController_2_t2145786393 * __this, const MethodInfo* method)
{
	{
		List_1_t3627005769 * L_0 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3627005769 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3627005769 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t3627005769 * L_2 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		List_1_t3627005769 * L_3 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3627005769 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3627005769 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3627005769 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3627005769 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2145786393 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,AttackSearchNav>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t3627005769 * L_9 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		List_1_t3627005769 * L_10 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3627005769 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3627005769 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3627005769 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3627005769 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t3627005769 * L_13 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		List_1_t3627005769 * L_14 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3627005769 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3627005769 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3627005769 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3627005769 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t3627005769 * L_17 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		List_1_t3627005769 * L_18 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3627005769 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3627005769 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3627005769 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3627005769 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t3627005769 * L_21 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		List_1_t3627005769 * L_22 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3627005769 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3627005769 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3627005769 *)L_21);
		((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3627005769 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m346844242_gshared (NavigationController_2_t2145786393 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t2145786393 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,AttackSearchNav>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t2145786393 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,AttackSearchNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2145786393 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3627005769 * L_15 = (List_1_t3627005769 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t3627005769 *)L_15);
		((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3627005769 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,AttackSearchNav>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m1184525474_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,AttackSearchNav>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m579444669_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1577172356_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m1887059318_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,AttackSearchNav>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m898731946_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m1644053680_gshared (NavigationController_2_t2225246313 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t3706465689 * L_0 = (List_1_t3706465689 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1767840762_gshared (NavigationController_2_t2225246313 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3268801571_gshared (NavigationController_2_t2225246313 * __this, const MethodInfo* method)
{
	{
		List_1_t3706465689 * L_0 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3706465689 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3706465689 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t3706465689 * L_2 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		List_1_t3706465689 * L_3 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3706465689 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3706465689 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3706465689 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3706465689 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2225246313 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,ChatNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t3706465689 * L_9 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		List_1_t3706465689 * L_10 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3706465689 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3706465689 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3706465689 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3706465689 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t3706465689 * L_13 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		List_1_t3706465689 * L_14 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3706465689 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3706465689 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3706465689 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3706465689 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t3706465689 * L_17 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		List_1_t3706465689 * L_18 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3706465689 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3706465689 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3706465689 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3706465689 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t3706465689 * L_21 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		List_1_t3706465689 * L_22 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3706465689 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3706465689 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3706465689 *)L_21);
		((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3706465689 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m929386684_gshared (NavigationController_2_t2225246313 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t2225246313 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,ChatNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t2225246313 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,ChatNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2225246313 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3706465689 * L_15 = (List_1_t3706465689 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t3706465689 *)L_15);
		((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3706465689 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,ChatNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m599142940_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,ChatNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m33970125_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1163805226_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m605338048_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ChatNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2027155932_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m321013590_gshared (NavigationController_2_t1726521459 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t3207740835 * L_0 = (List_1_t3207740835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1859701416_gshared (NavigationController_2_t1726521459 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m534062593_gshared (NavigationController_2_t1726521459 * __this, const MethodInfo* method)
{
	{
		List_1_t3207740835 * L_0 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3207740835 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3207740835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t3207740835 * L_2 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		List_1_t3207740835 * L_3 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3207740835 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3207740835 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3207740835 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3207740835 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t1726521459 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,DefendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t3207740835 * L_9 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		List_1_t3207740835 * L_10 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3207740835 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3207740835 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3207740835 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3207740835 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t3207740835 * L_13 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		List_1_t3207740835 * L_14 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3207740835 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3207740835 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3207740835 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3207740835 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t3207740835 * L_17 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		List_1_t3207740835 * L_18 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3207740835 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3207740835 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3207740835 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3207740835 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t3207740835 * L_21 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		List_1_t3207740835 * L_22 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		NullCheck((List_1_t3207740835 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3207740835 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t3207740835 *)L_21);
		((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3207740835 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1021390126_gshared (NavigationController_2_t1726521459 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t1726521459 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,DefendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t1726521459 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,DefendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t1726521459 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3207740835 * L_15 = (List_1_t3207740835 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t3207740835 *)L_15);
		((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3207740835 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,DefendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m527669130_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,DefendNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1209895939_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m4012482644_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m4027734450_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,DefendNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1880414682_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m1781176995_gshared (NavigationController_2_t3781907348 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t968159428 * L_0 = (List_1_t968159428 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m3816322973_gshared (NavigationController_2_t3781907348 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m949364036_gshared (NavigationController_2_t3781907348 * __this, const MethodInfo* method)
{
	{
		List_1_t968159428 * L_0 = (List_1_t968159428 *)__this->get_previousScreens_5();
		NullCheck((List_1_t968159428 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t968159428 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t968159428 * L_2 = (List_1_t968159428 *)__this->get_previousScreens_5();
		List_1_t968159428 * L_3 = (List_1_t968159428 *)__this->get_previousScreens_5();
		NullCheck((List_1_t968159428 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t968159428 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t968159428 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t968159428 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t968159428 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3781907348 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,FriendHomeNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t968159428 * L_9 = (List_1_t968159428 *)__this->get_previousScreens_5();
		List_1_t968159428 * L_10 = (List_1_t968159428 *)__this->get_previousScreens_5();
		NullCheck((List_1_t968159428 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t968159428 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t968159428 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t968159428 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t968159428 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t968159428 * L_13 = (List_1_t968159428 *)__this->get_previousScreens_5();
		List_1_t968159428 * L_14 = (List_1_t968159428 *)__this->get_previousScreens_5();
		NullCheck((List_1_t968159428 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t968159428 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t968159428 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t968159428 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t968159428 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t968159428 * L_17 = (List_1_t968159428 *)__this->get_previousScreens_5();
		List_1_t968159428 * L_18 = (List_1_t968159428 *)__this->get_previousScreens_5();
		NullCheck((List_1_t968159428 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t968159428 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t968159428 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t968159428 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t968159428 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t968159428 * L_21 = (List_1_t968159428 *)__this->get_previousScreens_5();
		List_1_t968159428 * L_22 = (List_1_t968159428 *)__this->get_previousScreens_5();
		NullCheck((List_1_t968159428 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t968159428 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t968159428 *)L_21);
		((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t968159428 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m2240014625_gshared (NavigationController_2_t3781907348 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t3781907348 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,FriendHomeNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t3781907348 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3781907348 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t968159428 * L_15 = (List_1_t968159428 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t968159428 *)L_15);
		((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t968159428 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,FriendHomeNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m1138610217_gshared (NavigationController_2_t3781907348 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,FriendHomeNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m928326242_gshared (NavigationController_2_t3781907348 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3218964345_gshared (NavigationController_2_t3781907348 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m1612430877_gshared (NavigationController_2_t3781907348 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendHomeNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2788814773_gshared (NavigationController_2_t3781907348 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m254826646_gshared (NavigationController_2_t63285209 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t1544504585 * L_0 = (List_1_t1544504585 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m2217178148_gshared (NavigationController_2_t63285209 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m2984498779_gshared (NavigationController_2_t63285209 * __this, const MethodInfo* method)
{
	{
		List_1_t1544504585 * L_0 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1544504585 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1544504585 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t1544504585 * L_2 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		List_1_t1544504585 * L_3 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1544504585 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1544504585 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1544504585 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1544504585 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t63285209 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,FriendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t1544504585 * L_9 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		List_1_t1544504585 * L_10 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1544504585 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1544504585 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1544504585 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1544504585 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t1544504585 * L_13 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		List_1_t1544504585 * L_14 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1544504585 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1544504585 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1544504585 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1544504585 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t1544504585 * L_17 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		List_1_t1544504585 * L_18 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1544504585 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1544504585 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1544504585 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1544504585 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t1544504585 * L_21 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		List_1_t1544504585 * L_22 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1544504585 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1544504585 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1544504585 *)L_21);
		((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1544504585 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3329785178_gshared (NavigationController_2_t63285209 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t63285209 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,FriendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t63285209 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t63285209 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1544504585 * L_15 = (List_1_t1544504585 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t1544504585 *)L_15);
		((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1544504585 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,FriendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m710670682_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,FriendNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m635772033_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1954343124_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2758172006_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1720819378_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2342486618_gshared (NavigationController_2_t906290919 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t2387510295 * L_0 = (List_1_t2387510295 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1017759524_gshared (NavigationController_2_t906290919 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m182819309_gshared (NavigationController_2_t906290919 * __this, const MethodInfo* method)
{
	{
		List_1_t2387510295 * L_0 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2387510295 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2387510295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t2387510295 * L_2 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		List_1_t2387510295 * L_3 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2387510295 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2387510295 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2387510295 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2387510295 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t906290919 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,FriendSearchNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t2387510295 * L_9 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		List_1_t2387510295 * L_10 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2387510295 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2387510295 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2387510295 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2387510295 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t2387510295 * L_13 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		List_1_t2387510295 * L_14 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2387510295 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2387510295 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2387510295 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2387510295 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t2387510295 * L_17 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		List_1_t2387510295 * L_18 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2387510295 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2387510295 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2387510295 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2387510295 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t2387510295 * L_21 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		List_1_t2387510295 * L_22 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2387510295 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2387510295 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2387510295 *)L_21);
		((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2387510295 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1335549778_gshared (NavigationController_2_t906290919 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t906290919 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,FriendSearchNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t906290919 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t906290919 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t2387510295 * L_15 = (List_1_t2387510295 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t2387510295 *)L_15);
		((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2387510295 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,FriendSearchNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m670594270_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,FriendSearchNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m901421583_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1791692064_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m937344406_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,FriendSearchNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2780911014_gshared (NavigationController_2_t906290919 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2424262531_gshared (NavigationController_2_t243945468 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t1725164844 * L_0 = (List_1_t1725164844 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m3784836201_gshared (NavigationController_2_t243945468 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3704736956_gshared (NavigationController_2_t243945468 * __this, const MethodInfo* method)
{
	{
		List_1_t1725164844 * L_0 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1725164844 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1725164844 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t1725164844 * L_2 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		List_1_t1725164844 * L_3 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1725164844 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1725164844 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1725164844 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1725164844 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t243945468 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,LeaderboardNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t1725164844 * L_9 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		List_1_t1725164844 * L_10 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1725164844 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1725164844 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1725164844 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1725164844 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t1725164844 * L_13 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		List_1_t1725164844 * L_14 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1725164844 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1725164844 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1725164844 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1725164844 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t1725164844 * L_17 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		List_1_t1725164844 * L_18 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1725164844 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1725164844 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1725164844 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1725164844 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t1725164844 * L_21 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		List_1_t1725164844 * L_22 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1725164844 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1725164844 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1725164844 *)L_21);
		((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1725164844 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3528757533_gshared (NavigationController_2_t243945468 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t243945468 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,LeaderboardNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t243945468 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t243945468 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1725164844 * L_15 = (List_1_t1725164844 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t1725164844 *)L_15);
		((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1725164844 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,LeaderboardNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m1438642517_gshared (NavigationController_2_t243945468 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,LeaderboardNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1383451070_gshared (NavigationController_2_t243945468 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m4192486605_gshared (NavigationController_2_t243945468 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m3110878433_gshared (NavigationController_2_t243945468 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,LeaderboardNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1727917841_gshared (NavigationController_2_t243945468 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m1308199617_gshared (NavigationController_2_t3446336302 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t632588382 * L_0 = (List_1_t632588382 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m45824959_gshared (NavigationController_2_t3446336302 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1132032114_gshared (NavigationController_2_t3446336302 * __this, const MethodInfo* method)
{
	{
		List_1_t632588382 * L_0 = (List_1_t632588382 *)__this->get_previousScreens_5();
		NullCheck((List_1_t632588382 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t632588382 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t632588382 * L_2 = (List_1_t632588382 *)__this->get_previousScreens_5();
		List_1_t632588382 * L_3 = (List_1_t632588382 *)__this->get_previousScreens_5();
		NullCheck((List_1_t632588382 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t632588382 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t632588382 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t632588382 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t632588382 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3446336302 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,PhoneNumberNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t632588382 * L_9 = (List_1_t632588382 *)__this->get_previousScreens_5();
		List_1_t632588382 * L_10 = (List_1_t632588382 *)__this->get_previousScreens_5();
		NullCheck((List_1_t632588382 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t632588382 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t632588382 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t632588382 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t632588382 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t632588382 * L_13 = (List_1_t632588382 *)__this->get_previousScreens_5();
		List_1_t632588382 * L_14 = (List_1_t632588382 *)__this->get_previousScreens_5();
		NullCheck((List_1_t632588382 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t632588382 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t632588382 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t632588382 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t632588382 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t632588382 * L_17 = (List_1_t632588382 *)__this->get_previousScreens_5();
		List_1_t632588382 * L_18 = (List_1_t632588382 *)__this->get_previousScreens_5();
		NullCheck((List_1_t632588382 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t632588382 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t632588382 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t632588382 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t632588382 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t632588382 * L_21 = (List_1_t632588382 *)__this->get_previousScreens_5();
		List_1_t632588382 * L_22 = (List_1_t632588382 *)__this->get_previousScreens_5();
		NullCheck((List_1_t632588382 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t632588382 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t632588382 *)L_21);
		((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t632588382 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3705288999_gshared (NavigationController_2_t3446336302 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t3446336302 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,PhoneNumberNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t3446336302 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3446336302 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t632588382 * L_15 = (List_1_t632588382 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t632588382 *)L_15);
		((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t632588382 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,PhoneNumberNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m2067710955_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,PhoneNumberNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m3105950792_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3049343775_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2052677691_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m290611767_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m3648062236_gshared (NavigationController_2_t548570015 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t2029789391 * L_0 = (List_1_t2029789391 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1507831522_gshared (NavigationController_2_t548570015 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3154846949_gshared (NavigationController_2_t548570015 * __this, const MethodInfo* method)
{
	{
		List_1_t2029789391 * L_0 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2029789391 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2029789391 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t2029789391 * L_2 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		List_1_t2029789391 * L_3 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2029789391 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2029789391 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2029789391 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2029789391 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t548570015 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,PlayerProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t2029789391 * L_9 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		List_1_t2029789391 * L_10 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2029789391 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2029789391 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2029789391 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2029789391 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t2029789391 * L_13 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		List_1_t2029789391 * L_14 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2029789391 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2029789391 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2029789391 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2029789391 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t2029789391 * L_17 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		List_1_t2029789391 * L_18 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2029789391 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2029789391 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2029789391 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2029789391 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t2029789391 * L_21 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		List_1_t2029789391 * L_22 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2029789391 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2029789391 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2029789391 *)L_21);
		((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2029789391 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1086604804_gshared (NavigationController_2_t548570015 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t548570015 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,PlayerProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t548570015 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t548570015 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t2029789391 * L_15 = (List_1_t2029789391 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t2029789391 *)L_15);
		((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2029789391 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,PlayerProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m680059976_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,PlayerProfileNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m998082891_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3845100182_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2033329720_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m3772286536_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2166287613_gshared (NavigationController_2_t3309526906 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t495778986 * L_0 = (List_1_t495778986 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m2761873091_gshared (NavigationController_2_t3309526906 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m538863782_gshared (NavigationController_2_t3309526906 * __this, const MethodInfo* method)
{
	{
		List_1_t495778986 * L_0 = (List_1_t495778986 *)__this->get_previousScreens_5();
		NullCheck((List_1_t495778986 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t495778986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t495778986 * L_2 = (List_1_t495778986 *)__this->get_previousScreens_5();
		List_1_t495778986 * L_3 = (List_1_t495778986 *)__this->get_previousScreens_5();
		NullCheck((List_1_t495778986 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t495778986 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t495778986 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t495778986 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t495778986 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3309526906 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,ProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t495778986 * L_9 = (List_1_t495778986 *)__this->get_previousScreens_5();
		List_1_t495778986 * L_10 = (List_1_t495778986 *)__this->get_previousScreens_5();
		NullCheck((List_1_t495778986 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t495778986 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t495778986 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t495778986 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t495778986 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t495778986 * L_13 = (List_1_t495778986 *)__this->get_previousScreens_5();
		List_1_t495778986 * L_14 = (List_1_t495778986 *)__this->get_previousScreens_5();
		NullCheck((List_1_t495778986 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t495778986 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t495778986 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t495778986 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t495778986 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t495778986 * L_17 = (List_1_t495778986 *)__this->get_previousScreens_5();
		List_1_t495778986 * L_18 = (List_1_t495778986 *)__this->get_previousScreens_5();
		NullCheck((List_1_t495778986 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t495778986 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t495778986 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t495778986 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t495778986 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t495778986 * L_21 = (List_1_t495778986 *)__this->get_previousScreens_5();
		List_1_t495778986 * L_22 = (List_1_t495778986 *)__this->get_previousScreens_5();
		NullCheck((List_1_t495778986 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t495778986 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t495778986 *)L_21);
		((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t495778986 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m2294274539_gshared (NavigationController_2_t3309526906 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t3309526906 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,ProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t3309526906 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,ProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t3309526906 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t495778986 * L_15 = (List_1_t495778986 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t495778986 *)L_15);
		((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t495778986 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,ProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m4024368191_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,ProfileNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1522047260_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m286403891_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2954494359_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2444270883_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m356458940_gshared (NavigationController_2_t2850042411 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t36294491 * L_0 = (List_1_t36294491 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m550598550_gshared (NavigationController_2_t2850042411 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,RegNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,RegNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1751947769_gshared (NavigationController_2_t2850042411 * __this, const MethodInfo* method)
{
	{
		List_1_t36294491 * L_0 = (List_1_t36294491 *)__this->get_previousScreens_5();
		NullCheck((List_1_t36294491 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t36294491 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t36294491 * L_2 = (List_1_t36294491 *)__this->get_previousScreens_5();
		List_1_t36294491 * L_3 = (List_1_t36294491 *)__this->get_previousScreens_5();
		NullCheck((List_1_t36294491 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t36294491 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t36294491 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t36294491 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t36294491 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2850042411 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,RegNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,RegNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t36294491 * L_9 = (List_1_t36294491 *)__this->get_previousScreens_5();
		List_1_t36294491 * L_10 = (List_1_t36294491 *)__this->get_previousScreens_5();
		NullCheck((List_1_t36294491 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t36294491 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t36294491 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t36294491 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t36294491 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,RegNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t36294491 * L_13 = (List_1_t36294491 *)__this->get_previousScreens_5();
		List_1_t36294491 * L_14 = (List_1_t36294491 *)__this->get_previousScreens_5();
		NullCheck((List_1_t36294491 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t36294491 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t36294491 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t36294491 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t36294491 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,RegNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t36294491 * L_17 = (List_1_t36294491 *)__this->get_previousScreens_5();
		List_1_t36294491 * L_18 = (List_1_t36294491 *)__this->get_previousScreens_5();
		NullCheck((List_1_t36294491 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t36294491 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t36294491 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t36294491 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t36294491 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t36294491 * L_21 = (List_1_t36294491 *)__this->get_previousScreens_5();
		List_1_t36294491 * L_22 = (List_1_t36294491 *)__this->get_previousScreens_5();
		NullCheck((List_1_t36294491 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t36294491 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t36294491 *)L_21);
		((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t36294491 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,RegNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m639547216_gshared (NavigationController_2_t2850042411 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t2850042411 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,RegNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,RegNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,RegNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t2850042411 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,RegNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t2850042411 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t36294491 * L_15 = (List_1_t36294491 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t36294491 *)L_15);
		((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t36294491 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,RegNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m2164337852_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,RegNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m4143251419_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m2387324626_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2311448660_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,RegNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m366324300_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m3184207255_gshared (NavigationController_2_t155028990 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t1636248366 * L_0 = (List_1_t1636248366 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m402874753_gshared (NavigationController_2_t155028990 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3978899914_gshared (NavigationController_2_t155028990 * __this, const MethodInfo* method)
{
	{
		List_1_t1636248366 * L_0 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1636248366 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1636248366 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t1636248366 * L_2 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		List_1_t1636248366 * L_3 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1636248366 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1636248366 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1636248366 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1636248366 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t155028990 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,SettingsNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t1636248366 * L_9 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		List_1_t1636248366 * L_10 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1636248366 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1636248366 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1636248366 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1636248366 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t1636248366 * L_13 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		List_1_t1636248366 * L_14 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1636248366 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1636248366 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1636248366 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1636248366 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t1636248366 * L_17 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		List_1_t1636248366 * L_18 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1636248366 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1636248366 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1636248366 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1636248366 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t1636248366 * L_21 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		List_1_t1636248366 * L_22 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		NullCheck((List_1_t1636248366 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1636248366 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t1636248366 *)L_21);
		((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1636248366 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3977220309_gshared (NavigationController_2_t155028990 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t155028990 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,SettingsNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t155028990 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,SettingsNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t155028990 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1636248366 * L_15 = (List_1_t1636248366 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t1636248366 *)L_15);
		((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1636248366 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,SettingsNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m1643361941_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,SettingsNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m708187624_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m4096629085_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m126515209_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1326194473_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m887236312_gshared (NavigationController_2_t760270839 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t2241490215 * L_0 = (List_1_t2241490215 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m3463568014_gshared (NavigationController_2_t760270839 * __this, int32_t ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_2, (int32_t)0, (bool)1);
		int32_t L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m622743621_gshared (NavigationController_2_t760270839 * __this, const MethodInfo* method)
{
	{
		List_1_t2241490215 * L_0 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2241490215 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2241490215 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t2241490215 * L_2 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		List_1_t2241490215 * L_3 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2241490215 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2241490215 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2241490215 *)L_2);
		int32_t L_5 = ((  int32_t (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2241490215 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t760270839 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,StatusNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_7 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t2241490215 * L_9 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		List_1_t2241490215 * L_10 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2241490215 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2241490215 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2241490215 *)L_9);
		int32_t L_12 = ((  int32_t (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2241490215 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_12);
	}

IL_006b:
	{
		List_1_t2241490215 * L_13 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		List_1_t2241490215 * L_14 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2241490215 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2241490215 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2241490215 *)L_13);
		int32_t L_16 = ((  int32_t (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2241490215 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_16, (int32_t)1, (bool)0);
		List_1_t2241490215 * L_17 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		List_1_t2241490215 * L_18 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2241490215 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2241490215 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2241490215 *)L_17);
		int32_t L_20 = ((  int32_t (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2241490215 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t2241490215 * L_21 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		List_1_t2241490215 * L_22 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2241490215 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2241490215 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2241490215 *)L_21);
		((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2241490215 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		int32_t L_24 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1563548144_gshared (NavigationController_2_t760270839 * __this, int32_t ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_currentScreen_6();
		int32_t L_2 = ___nextScreen0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), &L_3);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		if (L_6)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_8 = ___nextScreen0;
		NullCheck((NavigationController_2_t760270839 *)__this);
		bool L_9 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,StatusNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_8, (int32_t)0);
		if (!L_9)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_10, (int32_t)0);
		bool L_11 = (bool)__this->get_titleBar_3();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = ___nextScreen0;
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_12);
	}

IL_0064:
	{
		int32_t L_13 = ___nextScreen0;
		NullCheck((NavigationController_2_t760270839 *)__this);
		VirtActionInvoker3< int32_t, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,StatusNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t760270839 *)__this, (int32_t)L_13, (int32_t)0, (bool)0);
		bool L_14 = (bool)__this->get_tabNavigation_4();
		if (L_14)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t2241490215 * L_15 = (List_1_t2241490215 *)__this->get_previousScreens_5();
		int32_t L_16 = (int32_t)__this->get_currentScreen_6();
		NullCheck((List_1_t2241490215 *)L_15);
		((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2241490215 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		int32_t L_17 = ___nextScreen0;
		__this->set_currentScreen_6(L_17);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,StatusNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m806311540_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,StatusNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m2919636131_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3436155602_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m680577980_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,StatusNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m3786128028_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::.ctor()
extern "C"  void NavigationController_2__ctor_m1930283657_gshared (NavigationController_2_t577351051 * __this, const MethodInfo* method)
{
	{
		__this->set_titleBar_3((bool)1);
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_previousScreens_5(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1631311415_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___startScreen0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_titleBar_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_1 = ___startScreen0;
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void NavigationController`2<System.Object,System.Object>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_1);
	}

IL_0012:
	{
		Il2CppObject * L_2 = ___startScreen0;
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker3< Il2CppObject *, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,System.Object>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_2, (int32_t)0, (bool)1);
		Il2CppObject * L_3 = ___startScreen0;
		__this->set_currentScreen_6(L_3);
		return;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1673772046_gshared (NavigationController_2_t577351051 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2058570427 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2058570427 *)L_3);
		int32_t L_4 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2058570427 *)L_2);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_2, (int32_t)((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t577351051 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,System.Object>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_5, (int32_t)1);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker2< Il2CppObject *, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,System.Object>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_7, (int32_t)1);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t2058570427 * L_9 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		List_1_t2058570427 * L_10 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2058570427 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2058570427 *)L_9);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void NavigationController`2<System.Object,System.Object>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_12);
	}

IL_006b:
	{
		List_1_t2058570427 * L_13 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		List_1_t2058570427 * L_14 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2058570427 *)L_14);
		int32_t L_15 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2058570427 *)L_13);
		Il2CppObject * L_16 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_13, (int32_t)((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker3< Il2CppObject *, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,System.Object>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_16, (int32_t)1, (bool)0);
		List_1_t2058570427 * L_17 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		List_1_t2058570427 * L_18 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2058570427 *)L_18);
		int32_t L_19 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2058570427 *)L_17);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_17, (int32_t)((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_currentScreen_6(L_20);
		List_1_t2058570427 * L_21 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		List_1_t2058570427 * L_22 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		NullCheck((List_1_t2058570427 *)L_22);
		int32_t L_23 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t2058570427 *)L_21);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2058570427 *)L_21, (int32_t)((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_00c1:
	{
		goto IL_00d3;
	}

IL_00c6:
	{
		Il2CppObject * L_24 = (Il2CppObject *)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker2< Il2CppObject *, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,System.Object>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_24, (int32_t)1);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m871857639_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___nextScreen0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = (bool)__this->get_overrideCurrentCheck_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0032;
	}

IL_0014:
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_currentScreen_6();
		Il2CppObject * L_2 = ___nextScreen0;
		NullCheck((Il2CppObject *)(*L_1));
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*L_1), (Il2CppObject *)L_2);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0032:
	{
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject * L_5 = ___nextScreen0;
		NullCheck((NavigationController_2_t577351051 *)__this);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, int32_t >::Invoke(7 /* System.Boolean NavigationController`2<System.Object,System.Object>::ValidateNavigation(NavScreenType,NavigationDirection) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_5, (int32_t)0);
		if (!L_6)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_currentScreen_6();
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker2< Il2CppObject *, int32_t >::Invoke(10 /* System.Void NavigationController`2<System.Object,System.Object>::UnloadNavScreen(NavScreenType,NavigationDirection) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_7, (int32_t)0);
		bool L_8 = (bool)__this->get_titleBar_3();
		if (!L_8)
		{
			goto IL_0064;
		}
	}
	{
		Il2CppObject * L_9 = ___nextScreen0;
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void NavigationController`2<System.Object,System.Object>::PopulateTitleBar(NavScreenType) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_9);
	}

IL_0064:
	{
		Il2CppObject * L_10 = ___nextScreen0;
		NullCheck((NavigationController_2_t577351051 *)__this);
		VirtActionInvoker3< Il2CppObject *, int32_t, bool >::Invoke(9 /* System.Void NavigationController`2<System.Object,System.Object>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean) */, (NavigationController_2_t577351051 *)__this, (Il2CppObject *)L_10, (int32_t)0, (bool)0);
		bool L_11 = (bool)__this->get_tabNavigation_4();
		if (L_11)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t2058570427 * L_12 = (List_1_t2058570427 *)__this->get_previousScreens_5();
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_currentScreen_6();
		NullCheck((List_1_t2058570427 *)L_12);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0089:
	{
		Il2CppObject * L_14 = ___nextScreen0;
		__this->set_currentScreen_6(L_14);
	}

IL_0090:
	{
		return;
	}
}
// System.Boolean NavigationController`2<System.Object,System.Object>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m284324339_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator NavigationController`2<System.Object,System.Object>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m189672568_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1853810855_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2840335907_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NavigationController`2<System.Object,System.Object>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m3757624735_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CSwitchPanelsU3Ec__Iterator0__ctor_m803570807_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::MoveNext()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral664871083;
extern const uint32_t U3CSwitchPanelsU3Ec__Iterator0_MoveNext_m2574030129_MetadataUsageId;
extern "C"  bool U3CSwitchPanelsU3Ec__Iterator0_MoveNext_m2574030129_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSwitchPanelsU3Ec__Iterator0_MoveNext_m2574030129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d2;
			}
		}
	}
	{
		goto IL_00fb;
	}

IL_0021:
	{
		PanelTransitionManager_1_t3816465789 * L_2 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_2);
		L_2->set_switchingPanels_6((bool)1);
		PanelTransitionManager_1_t3816465789 * L_3 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_3);
		Panel_t1787746694 * L_4 = (Panel_t1787746694 *)L_3->get_activePanel_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		PanelTransitionManager_1_t3816465789 * L_6 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		PanelTransitionManager_1_t3816465789 * L_7 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_7);
		Panel_t1787746694 * L_8 = (Panel_t1787746694 *)L_7->get_activePanel_4();
		NullCheck((PanelTransitionManager_1_t3816465789 *)L_6);
		((  void (*) (PanelTransitionManager_1_t3816465789 *, Panel_t1787746694 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((PanelTransitionManager_1_t3816465789 *)L_6, (Panel_t1787746694 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		goto IL_0072;
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_9 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral664871083, /*hidden argument*/NULL);
	}

IL_0072:
	{
		goto IL_00d2;
	}

IL_0077:
	{
		PanelTransitionManager_1_t3816465789 * L_10 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_10);
		Panel_t1787746694 * L_11 = (Panel_t1787746694 *)L_10->get_nextPanel_5();
		NullCheck((Panel_t1787746694 *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Panel::CheckIfTweening() */, (Panel_t1787746694 *)L_11);
		if (L_12)
		{
			goto IL_00b3;
		}
	}
	{
		PanelTransitionManager_1_t3816465789 * L_13 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		PanelTransitionManager_1_t3816465789 * L_14 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_14);
		Panel_t1787746694 * L_15 = (Panel_t1787746694 *)L_14->get_nextPanel_5();
		NullCheck(L_13);
		L_13->set_activePanel_4(L_15);
		PanelTransitionManager_1_t3816465789 * L_16 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_16);
		L_16->set_nextPanel_5((Panel_t1787746694 *)NULL);
		goto IL_00d2;
	}

IL_00b3:
	{
		WaitForEndOfFrame_t1785723201 * L_17 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_17, /*hidden argument*/NULL);
		__this->set_U24current_1(L_17);
		bool L_18 = (bool)__this->get_U24disposing_2();
		if (L_18)
		{
			goto IL_00cd;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_00cd:
	{
		goto IL_00fd;
	}

IL_00d2:
	{
		PanelTransitionManager_1_t3816465789 * L_19 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_19);
		Panel_t1787746694 * L_20 = (Panel_t1787746694 *)L_19->get_nextPanel_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_20, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0077;
		}
	}
	{
		PanelTransitionManager_1_t3816465789 * L_22 = (PanelTransitionManager_1_t3816465789 *)__this->get_U24this_0();
		NullCheck(L_22);
		L_22->set_switchingPanels_6((bool)0);
		__this->set_U24PC_3((-1));
	}

IL_00fb:
	{
		return (bool)0;
	}

IL_00fd:
	{
		return (bool)1;
	}
}
// System.Object PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSwitchPanelsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1377570573_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_1();
		return L_0;
	}
}
// System.Object PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSwitchPanelsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m6903685_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_1();
		return L_0;
	}
}
// System.Void PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CSwitchPanelsU3Ec__Iterator0_Dispose_m3518941590_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSwitchPanelsU3Ec__Iterator0_Reset_m3658082208_MetadataUsageId;
extern "C"  void U3CSwitchPanelsU3Ec__Iterator0_Reset_m3658082208_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSwitchPanelsU3Ec__Iterator0_Reset_m3658082208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::.ctor()
extern "C"  void U3CTweenObjectForwardsU3Ec__Iterator1__ctor_m710252673_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3691006402_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m582991306_MethodInfo_var;
extern const uint32_t U3CTweenObjectForwardsU3Ec__Iterator1_MoveNext_m1743400835_MetadataUsageId;
extern "C"  bool U3CTweenObjectForwardsU3Ec__Iterator1_MoveNext_m1743400835_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTweenObjectForwardsU3Ec__Iterator1_MoveNext_m1743400835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0058;
			}
			case 2:
			{
				goto IL_00a8;
			}
			case 3:
			{
				goto IL_013b;
			}
		}
	}
	{
		goto IL_0178;
	}

IL_0029:
	{
		TweenPosition_t1144714832 * L_2 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck((UITweener_t2986641582 *)L_2);
		UITweener_PlayForward_m3772341562((UITweener_t2986641582 *)L_2, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (float)(0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_3);
		bool L_4 = (bool)__this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0053;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0053:
	{
		goto IL_017a;
	}

IL_0058:
	{
		TweenPosition_t1144714832 * L_5 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck((Component_t3819376471 *)L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_5, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_6);
		PanelContainer_t1029301983 * L_7 = GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472((GameObject_t1756533147 *)L_6, /*hidden argument*/GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0105;
		}
	}
	{
		__this->set_U3CiU3E__0_1(0);
		goto IL_00e0;
	}

IL_007f:
	{
		goto IL_00a8;
	}

IL_0084:
	{
		WaitForSeconds_t3839502067 * L_9 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_9, (float)(0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_9);
		bool L_10 = (bool)__this->get_U24disposing_3();
		if (L_10)
		{
			goto IL_00a3;
		}
	}
	{
		__this->set_U24PC_4(2);
	}

IL_00a3:
	{
		goto IL_017a;
	}

IL_00a8:
	{
		TweenPosition_t1144714832 * L_11 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck((Component_t3819376471 *)L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_11, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_12);
		PanelContainer_t1029301983 * L_13 = GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472((GameObject_t1756533147 *)L_12, /*hidden argument*/GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472_MethodInfo_var);
		NullCheck(L_13);
		List_1_t1156867826 * L_14 = (List_1_t1156867826 *)L_13->get_panelsContained_2();
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__0_1();
		NullCheck((List_1_t1156867826 *)L_14);
		Panel_t1787746694 * L_16 = List_1_get_Item_m3691006402((List_1_t1156867826 *)L_14, (int32_t)L_15, /*hidden argument*/List_1_get_Item_m3691006402_MethodInfo_var);
		NullCheck((Panel_t1787746694 *)L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Panel::CheckIfTweening() */, (Panel_t1787746694 *)L_16);
		if (L_17)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_18 = (int32_t)__this->get_U3CiU3E__0_1();
		__this->set_U3CiU3E__0_1(((int32_t)((int32_t)L_18+(int32_t)1)));
	}

IL_00e0:
	{
		int32_t L_19 = (int32_t)__this->get_U3CiU3E__0_1();
		TweenPosition_t1144714832 * L_20 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck((Component_t3819376471 *)L_20);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_20, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_21);
		PanelContainer_t1029301983 * L_22 = GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472((GameObject_t1756533147 *)L_21, /*hidden argument*/GameObject_GetComponent_TisPanelContainer_t1029301983_m1826616472_MethodInfo_var);
		NullCheck(L_22);
		List_1_t1156867826 * L_23 = (List_1_t1156867826 *)L_22->get_panelsContained_2();
		NullCheck((List_1_t1156867826 *)L_23);
		int32_t L_24 = List_1_get_Count_m582991306((List_1_t1156867826 *)L_23, /*hidden argument*/List_1_get_Count_m582991306_MethodInfo_var);
		if ((((int32_t)L_19) < ((int32_t)L_24)))
		{
			goto IL_007f;
		}
	}

IL_0105:
	{
		goto IL_013b;
	}

IL_010a:
	{
		TweenPosition_t1144714832 * L_25 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck(L_25);
		float L_26 = (float)((UITweener_t2986641582 *)L_25)->get_delay_7();
		WaitForSeconds_t3839502067 * L_27 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_27, (float)((float)((float)(((float)((float)L_26)))/(float)(10.0f))), /*hidden argument*/NULL);
		__this->set_U24current_2(L_27);
		bool L_28 = (bool)__this->get_U24disposing_3();
		if (L_28)
		{
			goto IL_0136;
		}
	}
	{
		__this->set_U24PC_4(3);
	}

IL_0136:
	{
		goto IL_017a;
	}

IL_013b:
	{
		TweenPosition_t1144714832 * L_29 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck((Component_t3819376471 *)L_29);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695((Component_t3819376471 *)L_29, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_30);
		Vector3_t2243707580  L_31 = Transform_get_localPosition_m2533925116((Transform_t3275118058 *)L_30, /*hidden argument*/NULL);
		TweenPosition_t1144714832 * L_32 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = (Vector3_t2243707580 )L_32->get_to_21();
		bool L_34 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, (Vector3_t2243707580 )L_31, (Vector3_t2243707580 )L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_010a;
		}
	}
	{
		TweenPosition_t1144714832 * L_35 = (TweenPosition_t1144714832 *)__this->get_tweenPos_0();
		NullCheck((Component_t3819376471 *)L_35);
		GameObject_t1756533147 * L_36 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_35, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_36);
		GameObject_SetActive_m2887581199((GameObject_t1756533147 *)L_36, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0178:
	{
		return (bool)0;
	}

IL_017a:
	{
		return (bool)1;
	}
}
// System.Object PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1131874671_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2186467591_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Void PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::Dispose()
extern "C"  void U3CTweenObjectForwardsU3Ec__Iterator1_Dispose_m2699090910_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTweenObjectForwardsU3Ec__Iterator1_Reset_m3295146332_MetadataUsageId;
extern "C"  void U3CTweenObjectForwardsU3Ec__Iterator1_Reset_m3295146332_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTweenObjectForwardsU3Ec__Iterator1_Reset_m3295146332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PanelTransitionManager`1<System.Object>::.ctor()
extern Il2CppClass* Dictionary_2_t2944688011_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1654536071_MethodInfo_var;
extern const uint32_t PanelTransitionManager_1__ctor_m3470287956_MetadataUsageId;
extern "C"  void PanelTransitionManager_1__ctor_m3470287956_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1__ctor_m3470287956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2944688011 * L_0 = (Dictionary_2_t2944688011 *)il2cpp_codegen_object_new(Dictionary_2_t2944688011_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1654536071(L_0, /*hidden argument*/Dictionary_2__ctor_m1654536071_MethodInfo_var);
		__this->set_panelsList_3(L_0);
		NullCheck((MonoSingleton_1_t2440115015 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MonoSingleton_1_t2440115015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::Initialise()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1971091118;
extern const uint32_t PanelTransitionManager_1_Initialise_m3802373897_MetadataUsageId;
extern "C"  void PanelTransitionManager_1_Initialise_m3802373897_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_Initialise_m3802373897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1971091118, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::AddPanel(PanelType,Panel)
extern const MethodInfo* Dictionary_2_Add_m1476581900_MethodInfo_var;
extern const uint32_t PanelTransitionManager_1_AddPanel_m2443272611_MetadataUsageId;
extern "C"  void PanelTransitionManager_1_AddPanel_m2443272611_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panelType0, Panel_t1787746694 * ___panelToAdd1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_AddPanel_m2443272611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2944688011 * L_0 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_1 = ___panelType0;
		Panel_t1787746694 * L_2 = ___panelToAdd1;
		NullCheck((Dictionary_2_t2944688011 *)L_0);
		Dictionary_2_Add_m1476581900((Dictionary_2_t2944688011 *)L_0, (int32_t)L_1, (Panel_t1787746694 *)L_2, /*hidden argument*/Dictionary_2_Add_m1476581900_MethodInfo_var);
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::RemovePanel(PanelType)
extern const MethodInfo* Dictionary_2_ContainsKey_m1216092873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m655324487_MethodInfo_var;
extern const uint32_t PanelTransitionManager_1_RemovePanel_m2528126402_MetadataUsageId;
extern "C"  void PanelTransitionManager_1_RemovePanel_m2528126402_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panelType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_RemovePanel_m2528126402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2944688011 * L_0 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_1 = ___panelType0;
		NullCheck((Dictionary_2_t2944688011 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1216092873((Dictionary_2_t2944688011 *)L_0, (int32_t)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1216092873_MethodInfo_var);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t2944688011 * L_3 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_4 = ___panelType0;
		NullCheck((Dictionary_2_t2944688011 *)L_3);
		Dictionary_2_Remove_m655324487((Dictionary_2_t2944688011 *)L_3, (int32_t)L_4, /*hidden argument*/Dictionary_2_Remove_m655324487_MethodInfo_var);
	}

IL_001e:
	{
		return;
	}
}
// System.Boolean PanelTransitionManager`1<System.Object>::get_PanelActive()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t PanelTransitionManager_1_get_PanelActive_m3779807383_MetadataUsageId;
extern "C"  bool PanelTransitionManager_1_get_PanelActive_m3779807383_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_get_PanelActive_m3779807383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Panel_t1787746694 * L_0 = (Panel_t1787746694 *)__this->get_activePanel_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Panel_t1787746694 * L_2 = (Panel_t1787746694 *)__this->get_nextPanel_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}

IL_0022:
	{
		return (bool)1;
	}

IL_0024:
	{
		return (bool)0;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::TogglePanel(PanelType,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PanelType_t482769230_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1216092873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2503850408_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4089702136;
extern Il2CppCodeGenString* _stringLiteral1800975136;
extern Il2CppCodeGenString* _stringLiteral3298283466;
extern Il2CppCodeGenString* _stringLiteral1935310859;
extern Il2CppCodeGenString* _stringLiteral2351615181;
extern const uint32_t PanelTransitionManager_1_TogglePanel_m438349725_MetadataUsageId;
extern "C"  void PanelTransitionManager_1_TogglePanel_m438349725_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panel0, bool ___switching1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_TogglePanel_m438349725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	String_t* G_B9_2 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	String_t* G_B8_2 = NULL;
	String_t* G_B10_0 = NULL;
	String_t* G_B10_1 = NULL;
	String_t* G_B10_2 = NULL;
	String_t* G_B10_3 = NULL;
	{
		Dictionary_2_t2944688011 * L_0 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_1 = ___panel0;
		NullCheck((Dictionary_2_t2944688011 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1216092873((Dictionary_2_t2944688011 *)L_0, (int32_t)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1216092873_MethodInfo_var);
		if (!L_2)
		{
			goto IL_00b5;
		}
	}
	{
		bool L_3 = ___switching1;
		if (!L_3)
		{
			goto IL_004f;
		}
	}
	{
		Panel_t1787746694 * L_4 = (Panel_t1787746694 *)__this->get_activePanel_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_6 = ___panel0;
		__this->set_switchPanel_7(L_6);
		Dictionary_2_t2944688011 * L_7 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		Panel_t1787746694 * L_8 = (Panel_t1787746694 *)__this->get_activePanel_4();
		NullCheck(L_8);
		int32_t L_9 = (int32_t)L_8->get_panelName_2();
		NullCheck((Dictionary_2_t2944688011 *)L_7);
		Panel_t1787746694 * L_10 = Dictionary_2_get_Item_m2503850408((Dictionary_2_t2944688011 *)L_7, (int32_t)L_9, /*hidden argument*/Dictionary_2_get_Item_m2503850408_MethodInfo_var);
		NullCheck((Panel_t1787746694 *)L_10);
		VirtActionInvoker0::Invoke(4 /* System.Void Panel::TogglePanel() */, (Panel_t1787746694 *)L_10);
	}

IL_004a:
	{
		goto IL_0060;
	}

IL_004f:
	{
		Dictionary_2_t2944688011 * L_11 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_12 = ___panel0;
		NullCheck((Dictionary_2_t2944688011 *)L_11);
		Panel_t1787746694 * L_13 = Dictionary_2_get_Item_m2503850408((Dictionary_2_t2944688011 *)L_11, (int32_t)L_12, /*hidden argument*/Dictionary_2_get_Item_m2503850408_MethodInfo_var);
		NullCheck((Panel_t1787746694 *)L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void Panel::TogglePanel() */, (Panel_t1787746694 *)L_13);
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_14 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppObject * L_15 = Box(PanelType_t482769230_il2cpp_TypeInfo_var, (&___panel0));
		NullCheck((Il2CppObject *)L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_15);
		Dictionary_2_t2944688011 * L_17 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_18 = ___panel0;
		NullCheck((Dictionary_2_t2944688011 *)L_17);
		Panel_t1787746694 * L_19 = Dictionary_2_get_Item_m2503850408((Dictionary_2_t2944688011 *)L_17, (int32_t)L_18, /*hidden argument*/Dictionary_2_get_Item_m2503850408_MethodInfo_var);
		NullCheck(L_19);
		bool L_20 = (bool)L_19->get_panelOpening_7();
		G_B8_0 = _stringLiteral1800975136;
		G_B8_1 = L_16;
		G_B8_2 = _stringLiteral4089702136;
		if (!L_20)
		{
			G_B9_0 = _stringLiteral1800975136;
			G_B9_1 = L_16;
			G_B9_2 = _stringLiteral4089702136;
			goto IL_00a1;
		}
	}
	{
		G_B10_0 = _stringLiteral3298283466;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		G_B10_3 = G_B8_2;
		goto IL_00a6;
	}

IL_00a1:
	{
		G_B10_0 = _stringLiteral1935310859;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
		G_B10_3 = G_B9_2;
	}

IL_00a6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1561703559(NULL /*static, unused*/, (String_t*)G_B10_3, (String_t*)G_B10_2, (String_t*)G_B10_1, (String_t*)G_B10_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		goto IL_00c9;
	}

IL_00b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_22 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2351615181, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::ActivatePanel(Panel)
extern "C"  void PanelTransitionManager_1_ActivatePanel_m1159509373_gshared (PanelTransitionManager_1_t3816465789 * __this, Panel_t1787746694 * ___passed_Panel0, const MethodInfo* method)
{
	{
		Panel_t1787746694 * L_0 = ___passed_Panel0;
		__this->set_nextPanel_5(L_0);
		NullCheck((PanelTransitionManager_1_t3816465789 *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void PanelTransitionManager`1<System.Object>::ToggleUI() */, (PanelTransitionManager_1_t3816465789 *)__this);
		bool L_1 = (bool)__this->get_switchingPanels_6();
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		NullCheck((PanelTransitionManager_1_t3816465789 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator PanelTransitionManager`1<System.Object>::SwitchPanels() */, (PanelTransitionManager_1_t3816465789 *)__this);
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour_StartCoroutine_m2470621050((MonoBehaviour_t1158329972 *)__this, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::DeActivatePanel(Panel)
extern "C"  void PanelTransitionManager_1_DeActivatePanel_m413338124_gshared (PanelTransitionManager_1_t3816465789 * __this, Panel_t1787746694 * ___passed_Panel0, const MethodInfo* method)
{
	{
		Panel_t1787746694 * L_0 = ___passed_Panel0;
		NullCheck((Panel_t1787746694 *)L_0);
		VirtActionInvoker0::Invoke(6 /* System.Void Panel::ClosePanel() */, (Panel_t1787746694 *)L_0);
		NullCheck((PanelTransitionManager_1_t3816465789 *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void PanelTransitionManager`1<System.Object>::ToggleUI() */, (PanelTransitionManager_1_t3816465789 *)__this);
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::DeActivateAllPanels()
extern Il2CppClass* Enumerator_t4264712713_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3601838444_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m868540192_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1579408363_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2176699880_MethodInfo_var;
extern const uint32_t PanelTransitionManager_1_DeActivateAllPanels_m3808037922_MetadataUsageId;
extern "C"  void PanelTransitionManager_1_DeActivateAllPanels_m3808037922_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_DeActivateAllPanels_m3808037922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t702033233  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t4264712713  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2944688011 * L_0 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		NullCheck((Dictionary_2_t2944688011 *)L_0);
		Enumerator_t4264712713  L_1 = Dictionary_2_GetEnumerator_m3601838444((Dictionary_2_t2944688011 *)L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m3601838444_MethodInfo_var);
		V_1 = (Enumerator_t4264712713 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0011:
		{
			KeyValuePair_2_t702033233  L_2 = Enumerator_get_Current_m868540192((Enumerator_t4264712713 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m868540192_MethodInfo_var);
			V_0 = (KeyValuePair_2_t702033233 )L_2;
			Panel_t1787746694 * L_3 = KeyValuePair_2_get_Value_m1579408363((KeyValuePair_2_t702033233 *)(&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1579408363_MethodInfo_var);
			NullCheck((Panel_t1787746694 *)L_3);
			VirtActionInvoker0::Invoke(6 /* System.Void Panel::ClosePanel() */, (Panel_t1787746694 *)L_3);
		}

IL_0025:
		{
			bool L_4 = Enumerator_MoveNext_m2176699880((Enumerator_t4264712713 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m2176699880_MethodInfo_var);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppClass* il2cpp_this_typeinfo_62 = Enumerator_t4264712713_il2cpp_TypeInfo_var;
		int32_t il2cpp_interface_offset__62 = il2cpp_codegen_class_interface_offset(il2cpp_this_typeinfo_62, IDisposable_t2427283555_il2cpp_TypeInfo_var);
		((  void (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_62->vtable[0 + il2cpp_interface_offset__62].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&V_1)), /*hidden argument*/il2cpp_this_typeinfo_62->vtable[0 + il2cpp_interface_offset__62].method);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::ToggleUI()
extern "C"  void PanelTransitionManager_1_ToggleUI_m2580260392_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator PanelTransitionManager`1<System.Object>::SwitchPanels()
extern "C"  Il2CppObject * PanelTransitionManager_1_SwitchPanels_m1400862637_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * V_0 = NULL;
	{
		U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * L_0 = (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *)L_0;
		U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * L_2 = V_0;
		return L_2;
	}
}
// System.Void PanelTransitionManager`1<System.Object>::TransitionObject(TweenPosition,System.Boolean)
extern "C"  void PanelTransitionManager_1_TransitionObject_m1292715937_gshared (PanelTransitionManager_1_t3816465789 * __this, TweenPosition_t1144714832 * ___tweenPos0, bool ___isHiding1, const MethodInfo* method)
{
	{
		bool L_0 = ___isHiding1;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		TweenPosition_t1144714832 * L_1 = ___tweenPos0;
		NullCheck((PanelTransitionManager_1_t3816465789 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (PanelTransitionManager_1_t3816465789 *, TweenPosition_t1144714832 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((PanelTransitionManager_1_t3816465789 *)__this, (TweenPosition_t1144714832 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour_StartCoroutine_m2470621050((MonoBehaviour_t1158329972 *)__this, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0019:
	{
		TweenPosition_t1144714832 * L_3 = ___tweenPos0;
		NullCheck((Component_t3819376471 *)L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_3, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_4);
		GameObject_SetActive_m2887581199((GameObject_t1756533147 *)L_4, (bool)1, /*hidden argument*/NULL);
		TweenPosition_t1144714832 * L_5 = ___tweenPos0;
		NullCheck((UITweener_t2986641582 *)L_5);
		UITweener_PlayReverse_m4027828489((UITweener_t2986641582 *)L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Collections.IEnumerator PanelTransitionManager`1<System.Object>::TweenObjectForwards(TweenPosition)
extern "C"  Il2CppObject * PanelTransitionManager_1_TweenObjectForwards_m3624951864_gshared (PanelTransitionManager_1_t3816465789 * __this, TweenPosition_t1144714832 * ___tweenPos0, const MethodInfo* method)
{
	U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * V_0 = NULL;
	{
		U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * L_0 = (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *)L_0;
		U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * L_1 = V_0;
		TweenPosition_t1144714832 * L_2 = ___tweenPos0;
		NullCheck(L_1);
		L_1->set_tweenPos_0(L_2);
		U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * L_3 = V_0;
		return L_3;
	}
}
// Panel PanelTransitionManager`1<System.Object>::GetActivePanel()
extern "C"  Panel_t1787746694 * PanelTransitionManager_1_GetActivePanel_m1623511417_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method)
{
	{
		Panel_t1787746694 * L_0 = (Panel_t1787746694 *)__this->get_activePanel_4();
		return L_0;
	}
}
// Panel PanelTransitionManager`1<System.Object>::GetPanel(PanelType)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1216092873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2503850408_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2351615181;
extern const uint32_t PanelTransitionManager_1_GetPanel_m2500208523_MetadataUsageId;
extern "C"  Panel_t1787746694 * PanelTransitionManager_1_GetPanel_m2500208523_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panelType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelTransitionManager_1_GetPanel_m2500208523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2944688011 * L_0 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_1 = ___panelType0;
		NullCheck((Dictionary_2_t2944688011 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1216092873((Dictionary_2_t2944688011 *)L_0, (int32_t)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1216092873_MethodInfo_var);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t2944688011 * L_3 = (Dictionary_2_t2944688011 *)__this->get_panelsList_3();
		int32_t L_4 = ___panelType0;
		NullCheck((Dictionary_2_t2944688011 *)L_3);
		Panel_t1787746694 * L_5 = Dictionary_2_get_Item_m2503850408((Dictionary_2_t2944688011 *)L_3, (int32_t)L_4, /*hidden argument*/Dictionary_2_get_Item_m2503850408_MethodInfo_var);
		return L_5;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_6 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2351615181, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return (Panel_t1787746694 *)NULL;
	}
}
// System.Void PhoneNumbers.EnumerableFromConstructor`1<System.Object>::.ctor(System.Func`1<System.Collections.Generic.IEnumerator`1<T>>)
extern "C"  void EnumerableFromConstructor_1__ctor_m2151719767_gshared (EnumerableFromConstructor_1_t2022230559 * __this, Func_1_t2119365804 * ___fn0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_1_t2119365804 * L_0 = ___fn0;
		__this->set_fn__0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> PhoneNumbers.EnumerableFromConstructor`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* EnumerableFromConstructor_1_GetEnumerator_m1562695206_gshared (EnumerableFromConstructor_1_t2022230559 * __this, const MethodInfo* method)
{
	{
		Func_1_t2119365804 * L_0 = (Func_1_t2119365804 *)__this->get_fn__0();
		NullCheck((Func_1_t2119365804 *)L_0);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (Func_1_t2119365804 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_1_t2119365804 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Collections.IEnumerator PhoneNumbers.EnumerableFromConstructor`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * EnumerableFromConstructor_1_System_Collections_IEnumerable_GetEnumerator_m1507026405_gshared (EnumerableFromConstructor_1_t2022230559 * __this, const MethodInfo* method)
{
	{
		Func_1_t2119365804 * L_0 = (Func_1_t2119365804 *)__this->get_fn__0();
		NullCheck((Func_1_t2119365804 *)L_0);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (Func_1_t2119365804 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_1_t2119365804 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void ObservableDictionary_2__ctor_m3482229747_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__Dictionary_4(L_0);
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2__ctor_m2273999893_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		Dictionary_2_t2281509423 * L_1 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__Dictionary_4(L_1);
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ObservableDictionary_2__ctor_m3424814776_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		Dictionary_2_t2281509423 * L_1 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set__Dictionary_4(L_1);
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Int32)
extern "C"  void ObservableDictionary_2__ctor_m4217517412_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		Dictionary_2_t2281509423 * L_1 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set__Dictionary_4(L_1);
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ObservableDictionary_2__ctor_m1850798116_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		Il2CppObject* L_1 = ___comparer1;
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject*)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set__Dictionary_4(L_2);
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ObservableDictionary_2__ctor_m2924843205_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		Il2CppObject* L_1 = ___comparer1;
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_2, (int32_t)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set__Dictionary_4(L_2);
		return;
	}
}
// System.Collections.Generic.IDictionary`2<TKey,TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Dictionary()
extern "C"  Il2CppObject* ObservableDictionary_2_get_Dictionary_m267445679_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__Dictionary_4();
		return L_0;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ObservableDictionary_2_Add_m809576144_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ObservableDictionary_2_ContainsKey_m3273400666_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ObservableDictionary_2_get_Keys_m4034744365_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3021628599;
extern const uint32_t ObservableDictionary_2_Remove_m1597064470_MetadataUsageId;
extern "C"  bool ObservableDictionary_2_Remove_m1597064470_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_Remove_m1597064470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3021628599, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_2, (Il2CppObject *)L_3, (Il2CppObject **)(&V_0));
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject*)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_4, (Il2CppObject *)L_5);
		V_1 = (bool)L_6;
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
	}

IL_003e:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ObservableDictionary_2_TryGetValue_m2959695535_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_1 = ___key0;
		Il2CppObject ** L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// System.Collections.Generic.ICollection`1<TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ObservableDictionary_2_get_Values_m2365880877_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(7 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Values() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0);
		return L_1;
	}
}
// TValue PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ObservableDictionary_2_get_Item_m1340976434_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ObservableDictionary_2_set_Item_m1965010995_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ObservableDictionary_2_Add_m219092131_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ObservableDictionary_2_Clear_m2078003600_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_2);
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ObservableDictionary_2_Contains_m156958993_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		KeyValuePair_2_t38854645  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t38854645  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0, (KeyValuePair_2_t38854645 )L_1);
		return L_2;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ObservableDictionary_2_CopyTo_m3898517055_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		KeyValuePair_2U5BU5D_t2854920344* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2854920344*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0, (KeyValuePair_2U5BU5D_t2854920344*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Int32 PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ObservableDictionary_2_get_Count_m3837408227_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ObservableDictionary_2_get_IsReadOnly_m2217790444_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ObservableDictionary_2_Remove_m117162194_gshared (ObservableDictionary_2_t4244038468 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		bool L_1 = ((  bool (*) (ObservableDictionary_2_t4244038468 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ObservableDictionary_2_GetEnumerator_m2468868070_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304_MetadataUsageId;
extern "C"  Il2CppObject * ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::add_CollectionChanged(PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler)
extern Il2CppClass* NotifyCollectionChangedEventHandler_t1310059589_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_add_CollectionChanged_m3033885855_MetadataUsageId;
extern "C"  void ObservableDictionary_2_add_CollectionChanged_m3033885855_gshared (ObservableDictionary_2_t4244038468 * __this, NotifyCollectionChangedEventHandler_t1310059589 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_add_CollectionChanged_m3033885855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NotifyCollectionChangedEventHandler_t1310059589 * V_0 = NULL;
	NotifyCollectionChangedEventHandler_t1310059589 * V_1 = NULL;
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		V_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)L_0;
	}

IL_0007:
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_1 = V_0;
		V_1 = (NotifyCollectionChangedEventHandler_t1310059589 *)L_1;
		NotifyCollectionChangedEventHandler_t1310059589 ** L_2 = (NotifyCollectionChangedEventHandler_t1310059589 **)__this->get_address_of_CollectionChanged_5();
		NotifyCollectionChangedEventHandler_t1310059589 * L_3 = V_1;
		NotifyCollectionChangedEventHandler_t1310059589 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		NotifyCollectionChangedEventHandler_t1310059589 * L_6 = V_0;
		NotifyCollectionChangedEventHandler_t1310059589 * L_7 = InterlockedCompareExchangeImpl<NotifyCollectionChangedEventHandler_t1310059589 *>((NotifyCollectionChangedEventHandler_t1310059589 **)L_2, (NotifyCollectionChangedEventHandler_t1310059589 *)((NotifyCollectionChangedEventHandler_t1310059589 *)Castclass(L_5, NotifyCollectionChangedEventHandler_t1310059589_il2cpp_TypeInfo_var)), (NotifyCollectionChangedEventHandler_t1310059589 *)L_6);
		V_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)L_7;
		NotifyCollectionChangedEventHandler_t1310059589 * L_8 = V_0;
		NotifyCollectionChangedEventHandler_t1310059589 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NotifyCollectionChangedEventHandler_t1310059589 *)L_8) == ((Il2CppObject*)(NotifyCollectionChangedEventHandler_t1310059589 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::remove_CollectionChanged(PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler)
extern Il2CppClass* NotifyCollectionChangedEventHandler_t1310059589_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_remove_CollectionChanged_m2705002958_MetadataUsageId;
extern "C"  void ObservableDictionary_2_remove_CollectionChanged_m2705002958_gshared (ObservableDictionary_2_t4244038468 * __this, NotifyCollectionChangedEventHandler_t1310059589 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_remove_CollectionChanged_m2705002958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NotifyCollectionChangedEventHandler_t1310059589 * V_0 = NULL;
	NotifyCollectionChangedEventHandler_t1310059589 * V_1 = NULL;
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		V_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)L_0;
	}

IL_0007:
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_1 = V_0;
		V_1 = (NotifyCollectionChangedEventHandler_t1310059589 *)L_1;
		NotifyCollectionChangedEventHandler_t1310059589 ** L_2 = (NotifyCollectionChangedEventHandler_t1310059589 **)__this->get_address_of_CollectionChanged_5();
		NotifyCollectionChangedEventHandler_t1310059589 * L_3 = V_1;
		NotifyCollectionChangedEventHandler_t1310059589 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		NotifyCollectionChangedEventHandler_t1310059589 * L_6 = V_0;
		NotifyCollectionChangedEventHandler_t1310059589 * L_7 = InterlockedCompareExchangeImpl<NotifyCollectionChangedEventHandler_t1310059589 *>((NotifyCollectionChangedEventHandler_t1310059589 **)L_2, (NotifyCollectionChangedEventHandler_t1310059589 *)((NotifyCollectionChangedEventHandler_t1310059589 *)Castclass(L_5, NotifyCollectionChangedEventHandler_t1310059589_il2cpp_TypeInfo_var)), (NotifyCollectionChangedEventHandler_t1310059589 *)L_6);
		V_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)L_7;
		NotifyCollectionChangedEventHandler_t1310059589 * L_8 = V_0;
		NotifyCollectionChangedEventHandler_t1310059589 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NotifyCollectionChangedEventHandler_t1310059589 *)L_8) == ((Il2CppObject*)(NotifyCollectionChangedEventHandler_t1310059589 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern Il2CppClass* PropertyChangedEventHandler_t3042952059_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_add_PropertyChanged_m1468847672_MetadataUsageId;
extern "C"  void ObservableDictionary_2_add_PropertyChanged_m1468847672_gshared (ObservableDictionary_2_t4244038468 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_add_PropertyChanged_m1468847672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PropertyChangedEventHandler_t3042952059 * V_0 = NULL;
	PropertyChangedEventHandler_t3042952059 * V_1 = NULL;
	{
		PropertyChangedEventHandler_t3042952059 * L_0 = (PropertyChangedEventHandler_t3042952059 *)__this->get_PropertyChanged_6();
		V_0 = (PropertyChangedEventHandler_t3042952059 *)L_0;
	}

IL_0007:
	{
		PropertyChangedEventHandler_t3042952059 * L_1 = V_0;
		V_1 = (PropertyChangedEventHandler_t3042952059 *)L_1;
		PropertyChangedEventHandler_t3042952059 ** L_2 = (PropertyChangedEventHandler_t3042952059 **)__this->get_address_of_PropertyChanged_6();
		PropertyChangedEventHandler_t3042952059 * L_3 = V_1;
		PropertyChangedEventHandler_t3042952059 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		PropertyChangedEventHandler_t3042952059 * L_6 = V_0;
		PropertyChangedEventHandler_t3042952059 * L_7 = InterlockedCompareExchangeImpl<PropertyChangedEventHandler_t3042952059 *>((PropertyChangedEventHandler_t3042952059 **)L_2, (PropertyChangedEventHandler_t3042952059 *)((PropertyChangedEventHandler_t3042952059 *)Castclass(L_5, PropertyChangedEventHandler_t3042952059_il2cpp_TypeInfo_var)), (PropertyChangedEventHandler_t3042952059 *)L_6);
		V_0 = (PropertyChangedEventHandler_t3042952059 *)L_7;
		PropertyChangedEventHandler_t3042952059 * L_8 = V_0;
		PropertyChangedEventHandler_t3042952059 * L_9 = V_1;
		if ((!(((Il2CppObject*)(PropertyChangedEventHandler_t3042952059 *)L_8) == ((Il2CppObject*)(PropertyChangedEventHandler_t3042952059 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern Il2CppClass* PropertyChangedEventHandler_t3042952059_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_remove_PropertyChanged_m26290967_MetadataUsageId;
extern "C"  void ObservableDictionary_2_remove_PropertyChanged_m26290967_gshared (ObservableDictionary_2_t4244038468 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_remove_PropertyChanged_m26290967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PropertyChangedEventHandler_t3042952059 * V_0 = NULL;
	PropertyChangedEventHandler_t3042952059 * V_1 = NULL;
	{
		PropertyChangedEventHandler_t3042952059 * L_0 = (PropertyChangedEventHandler_t3042952059 *)__this->get_PropertyChanged_6();
		V_0 = (PropertyChangedEventHandler_t3042952059 *)L_0;
	}

IL_0007:
	{
		PropertyChangedEventHandler_t3042952059 * L_1 = V_0;
		V_1 = (PropertyChangedEventHandler_t3042952059 *)L_1;
		PropertyChangedEventHandler_t3042952059 ** L_2 = (PropertyChangedEventHandler_t3042952059 **)__this->get_address_of_PropertyChanged_6();
		PropertyChangedEventHandler_t3042952059 * L_3 = V_1;
		PropertyChangedEventHandler_t3042952059 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		PropertyChangedEventHandler_t3042952059 * L_6 = V_0;
		PropertyChangedEventHandler_t3042952059 * L_7 = InterlockedCompareExchangeImpl<PropertyChangedEventHandler_t3042952059 *>((PropertyChangedEventHandler_t3042952059 **)L_2, (PropertyChangedEventHandler_t3042952059 *)((PropertyChangedEventHandler_t3042952059 *)Castclass(L_5, PropertyChangedEventHandler_t3042952059_il2cpp_TypeInfo_var)), (PropertyChangedEventHandler_t3042952059 *)L_6);
		V_0 = (PropertyChangedEventHandler_t3042952059 *)L_7;
		PropertyChangedEventHandler_t3042952059 * L_8 = V_0;
		PropertyChangedEventHandler_t3042952059 * L_9 = V_1;
		if ((!(((Il2CppObject*)(PropertyChangedEventHandler_t3042952059 *)L_8) == ((Il2CppObject*)(PropertyChangedEventHandler_t3042952059 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::AddRange(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3356102854;
extern Il2CppCodeGenString* _stringLiteral621443882;
extern const uint32_t ObservableDictionary_2_AddRange_m3987702119_MetadataUsageId;
extern "C"  void ObservableDictionary_2_AddRange_m3987702119_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject* ___items0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_AddRange_m3987702119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___items0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3356102854, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject* L_2 = ___items0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_00af;
		}
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_4);
		int32_t L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_4);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0096;
		}
	}
	{
		Il2CppObject* L_6 = ___items0;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject* L_7 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_6);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Func_2_t3961629604 * L_9 = (Func_2_t3961629604 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (Func_2_t3961629604 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_9, (Il2CppObject *)__this, (IntPtr_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		bool L_10 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_7, (Func_2_t3961629604 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		if (!L_10)
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_11, (String_t*)_stringLiteral621443882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0055:
	{
		Il2CppObject* L_12 = ___items0;
		NullCheck((Il2CppObject*)L_12);
		Il2CppObject* L_13 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16), (Il2CppObject*)L_12);
		V_1 = (Il2CppObject*)L_13;
	}

IL_005c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0074;
		}

IL_0061:
		{
			Il2CppObject* L_14 = V_1;
			NullCheck((Il2CppObject*)L_14);
			KeyValuePair_2_t38854645  L_15 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_14);
			V_0 = (KeyValuePair_2_t38854645 )L_15;
			NullCheck((ObservableDictionary_2_t4244038468 *)__this);
			Il2CppObject* L_16 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			KeyValuePair_2_t38854645  L_17 = V_0;
			NullCheck((Il2CppObject*)L_16);
			InterfaceActionInvoker1< KeyValuePair_2_t38854645  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_16, (KeyValuePair_2_t38854645 )L_17);
		}

IL_0074:
		{
			Il2CppObject* L_18 = V_1;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_0061;
			}
		}

IL_007f:
		{
			IL2CPP_LEAVE(0x91, FINALLY_0084);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0084;
	}

FINALLY_0084:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_20 = V_1;
			if (!L_20)
			{
				goto IL_0090;
			}
		}

IL_008a:
		{
			Il2CppObject* L_21 = V_1;
			NullCheck((Il2CppObject *)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(132)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(132)
	{
		IL2CPP_JUMP_TBL(0x91, IL_0091)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0091:
	{
		goto IL_00a2;
	}

IL_0096:
	{
		Il2CppObject* L_22 = ___items0;
		Dictionary_2_t2281509423 * L_23 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_23, (Il2CppObject*)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__Dictionary_4(L_23);
	}

IL_00a2:
	{
		Il2CppObject* L_24 = ___items0;
		KeyValuePair_2U5BU5D_t2854920344* L_25 = ((  KeyValuePair_2U5BU5D_t2854920344* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (int32_t)0, (Il2CppObject *)(Il2CppObject *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
	}

IL_00af:
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::Insert(TKey,TValue,System.Boolean)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3021628599;
extern Il2CppCodeGenString* _stringLiteral621443882;
extern const uint32_t ObservableDictionary_2_Insert_m2164254687_MetadataUsageId;
extern "C"  void ObservableDictionary_2_Insert_m2164254687_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, bool ___add2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_Insert_m2164254687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3021628599, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_3 = ___key0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_2, (Il2CppObject *)L_3, (Il2CppObject **)(&V_0));
		if (!L_4)
		{
			goto IL_0078;
		}
	}
	{
		bool L_5 = ___add2;
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t3259014390 * L_6 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_6, (String_t*)_stringLiteral621443882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003a:
	{
		Il2CppObject * L_7 = V_0;
		Il2CppObject * L_8 = ___value1;
		bool L_9 = Object_Equals_m969736273(NULL /*static, unused*/, (Il2CppObject *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0051;
		}
	}
	{
		return;
	}

IL_0051:
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_10 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_11 = ___key0;
		Il2CppObject * L_12 = ___value1;
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12);
		Il2CppObject * L_13 = ___key0;
		Il2CppObject * L_14 = ___value1;
		KeyValuePair_2_t38854645  L_15;
		memset(&L_15, 0, sizeof(L_15));
		KeyValuePair_2__ctor_m1640124561(&L_15, (Il2CppObject *)L_13, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		Il2CppObject * L_16 = ___key0;
		Il2CppObject * L_17 = V_0;
		KeyValuePair_2_t38854645  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1640124561(&L_18, (Il2CppObject *)L_16, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, KeyValuePair_2_t38854645 , KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (int32_t)2, (KeyValuePair_2_t38854645 )L_15, (KeyValuePair_2_t38854645 )L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		goto IL_0093;
	}

IL_0078:
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_19 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_20 = ___key0;
		Il2CppObject * L_21 = ___value1;
		NullCheck((Il2CppObject*)L_19);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_19, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		Il2CppObject * L_22 = ___key0;
		Il2CppObject * L_23 = ___value1;
		KeyValuePair_2_t38854645  L_24;
		memset(&L_24, 0, sizeof(L_24));
		KeyValuePair_2__ctor_m1640124561(&L_24, (Il2CppObject *)L_22, (Il2CppObject *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, int32_t, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, (int32_t)0, (KeyValuePair_2_t38854645 )L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
	}

IL_0093:
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged()
extern Il2CppCodeGenString* _stringLiteral1554779067;
extern Il2CppCodeGenString* _stringLiteral3593357335;
extern Il2CppCodeGenString* _stringLiteral1857678454;
extern Il2CppCodeGenString* _stringLiteral1188955108;
extern const uint32_t ObservableDictionary_2_OnPropertyChanged_m1628940995_MetadataUsageId;
extern "C"  void ObservableDictionary_2_OnPropertyChanged_m1628940995_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_OnPropertyChanged_m1628940995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged(System.String) */, (ObservableDictionary_2_t4244038468 *)__this, (String_t*)_stringLiteral1554779067);
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged(System.String) */, (ObservableDictionary_2_t4244038468 *)__this, (String_t*)_stringLiteral3593357335);
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged(System.String) */, (ObservableDictionary_2_t4244038468 *)__this, (String_t*)_stringLiteral1857678454);
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged(System.String) */, (ObservableDictionary_2_t4244038468 *)__this, (String_t*)_stringLiteral1188955108);
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnPropertyChanged(System.String)
extern Il2CppClass* PropertyChangedEventArgs_t1689446432_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_OnPropertyChanged_m643605273_MetadataUsageId;
extern "C"  void ObservableDictionary_2_OnPropertyChanged_m643605273_gshared (ObservableDictionary_2_t4244038468 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_OnPropertyChanged_m643605273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyChangedEventHandler_t3042952059 * L_0 = (PropertyChangedEventHandler_t3042952059 *)__this->get_PropertyChanged_6();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		PropertyChangedEventHandler_t3042952059 * L_1 = (PropertyChangedEventHandler_t3042952059 *)__this->get_PropertyChanged_6();
		String_t* L_2 = ___propertyName0;
		PropertyChangedEventArgs_t1689446432 * L_3 = (PropertyChangedEventArgs_t1689446432 *)il2cpp_codegen_object_new(PropertyChangedEventArgs_t1689446432_il2cpp_TypeInfo_var);
		PropertyChangedEventArgs__ctor_m3472995223(L_3, (String_t*)L_2, /*hidden argument*/NULL);
		NullCheck((PropertyChangedEventHandler_t3042952059 *)L_1);
		PropertyChangedEventHandler_Invoke_m3733684827((PropertyChangedEventHandler_t3042952059 *)L_1, (Il2CppObject *)__this, (PropertyChangedEventArgs_t1689446432 *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged()
extern Il2CppClass* NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_OnCollectionChanged_m1062698694_MetadataUsageId;
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m1062698694_gshared (ObservableDictionary_2_t4244038468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_OnCollectionChanged_m1062698694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		NotifyCollectionChangedEventHandler_t1310059589 * L_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_1 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		NotifyCollectionChangedEventArgs_t3926133854 * L_2 = (NotifyCollectionChangedEventArgs_t3926133854 *)il2cpp_codegen_object_new(NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var);
		NotifyCollectionChangedEventArgs__ctor_m1724250992(L_2, (int32_t)4, /*hidden argument*/NULL);
		NullCheck((NotifyCollectionChangedEventHandler_t1310059589 *)L_1);
		NotifyCollectionChangedEventHandler_Invoke_m3701267723((NotifyCollectionChangedEventHandler_t1310059589 *)L_1, (Il2CppObject *)__this, (NotifyCollectionChangedEventArgs_t3926133854 *)L_2, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_OnCollectionChanged_m78342394_MetadataUsageId;
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m78342394_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___action0, KeyValuePair_2_t38854645  ___changedItem1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_OnCollectionChanged_m78342394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		NotifyCollectionChangedEventHandler_t1310059589 * L_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_1 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		int32_t L_2 = ___action0;
		KeyValuePair_2_t38854645  L_3 = ___changedItem1;
		KeyValuePair_2_t38854645  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_4);
		NotifyCollectionChangedEventArgs_t3926133854 * L_6 = (NotifyCollectionChangedEventArgs_t3926133854 *)il2cpp_codegen_object_new(NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var);
		NotifyCollectionChangedEventArgs__ctor_m862020742(L_6, (int32_t)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		NullCheck((NotifyCollectionChangedEventHandler_t1310059589 *)L_1);
		NotifyCollectionChangedEventHandler_Invoke_m3701267723((NotifyCollectionChangedEventHandler_t1310059589 *)L_1, (Il2CppObject *)__this, (NotifyCollectionChangedEventArgs_t3926133854 *)L_6, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.Generic.KeyValuePair`2<TKey,TValue>,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_OnCollectionChanged_m20718175_MetadataUsageId;
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m20718175_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___action0, KeyValuePair_2_t38854645  ___newItem1, KeyValuePair_2_t38854645  ___oldItem2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_OnCollectionChanged_m20718175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		NotifyCollectionChangedEventHandler_t1310059589 * L_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_1 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		int32_t L_2 = ___action0;
		KeyValuePair_2_t38854645  L_3 = ___newItem1;
		KeyValuePair_2_t38854645  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_4);
		KeyValuePair_2_t38854645  L_6 = ___oldItem2;
		KeyValuePair_2_t38854645  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 25), &L_7);
		NotifyCollectionChangedEventArgs_t3926133854 * L_9 = (NotifyCollectionChangedEventArgs_t3926133854 *)il2cpp_codegen_object_new(NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var);
		NotifyCollectionChangedEventArgs__ctor_m384819440(L_9, (int32_t)L_2, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		NullCheck((NotifyCollectionChangedEventHandler_t1310059589 *)L_1);
		NotifyCollectionChangedEventHandler_Invoke_m3701267723((NotifyCollectionChangedEventHandler_t1310059589 *)L_1, (Il2CppObject *)__this, (NotifyCollectionChangedEventArgs_t3926133854 *)L_9, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList)
extern Il2CppClass* NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var;
extern const uint32_t ObservableDictionary_2_OnCollectionChanged_m908791682_MetadataUsageId;
extern "C"  void ObservableDictionary_2_OnCollectionChanged_m908791682_gshared (ObservableDictionary_2_t4244038468 * __this, int32_t ___action0, Il2CppObject * ___newItems1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObservableDictionary_2_OnCollectionChanged_m908791682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		((  void (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		NotifyCollectionChangedEventHandler_t1310059589 * L_0 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		NotifyCollectionChangedEventHandler_t1310059589 * L_1 = (NotifyCollectionChangedEventHandler_t1310059589 *)__this->get_CollectionChanged_5();
		int32_t L_2 = ___action0;
		Il2CppObject * L_3 = ___newItems1;
		NotifyCollectionChangedEventArgs_t3926133854 * L_4 = (NotifyCollectionChangedEventArgs_t3926133854 *)il2cpp_codegen_object_new(NotifyCollectionChangedEventArgs_t3926133854_il2cpp_TypeInfo_var);
		NotifyCollectionChangedEventArgs__ctor_m3851201893(L_4, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck((NotifyCollectionChangedEventHandler_t1310059589 *)L_1);
		NotifyCollectionChangedEventHandler_Invoke_m3701267723((NotifyCollectionChangedEventHandler_t1310059589 *)L_1, (Il2CppObject *)__this, (NotifyCollectionChangedEventArgs_t3926133854 *)L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.Object,System.Object>::<AddRange>m__0(TKey)
extern "C"  bool ObservableDictionary_2_U3CAddRangeU3Em__0_m3926150165_gshared (ObservableDictionary_2_t4244038468 * __this, Il2CppObject * ___k0, const MethodInfo* method)
{
	{
		NullCheck((ObservableDictionary_2_t4244038468 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ObservableDictionary_2_t4244038468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ObservableDictionary_2_t4244038468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		Il2CppObject * L_1 = ___k0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void Prime31.Reflection.SafeDictionary`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2__ctor_m428099180_MetadataUsageId;
extern "C"  void SafeDictionary_2__ctor_m428099180_gshared (SafeDictionary_2_t1052943346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2__ctor_m428099180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__padlock_0(L_0);
		Dictionary_2_t2281509423 * L_1 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__dictionary_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Prime31.Reflection.SafeDictionary`2<System.Object,System.Object>::tryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_tryGetValue_m590177504_gshared (SafeDictionary_2_t1052943346 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_1();
		Il2CppObject * L_1 = ___key0;
		Il2CppObject ** L_2 = ___value1;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_3 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Prime31.Reflection.SafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m3729275973_gshared (SafeDictionary_2_t1052943346 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void Prime31.Reflection.SafeDictionary`2<System.Object,System.Object>::add(TKey,TValue)
extern "C"  void SafeDictionary_2_add_m1502702589_gshared (SafeDictionary_2_t1052943346 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__padlock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get__dictionary_1();
			Il2CppObject * L_3 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			if (L_4)
			{
				goto IL_002b;
			}
		}

IL_001e:
		{
			Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get__dictionary_1();
			Il2CppObject * L_6 = ___key0;
			Il2CppObject * L_7 = ___value1;
			NullCheck((Dictionary_2_t2281509423 *)L_5);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x37, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void System.Action`1<AttackHomeNav>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3305229710_gshared (Action_1_t3746689886 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<AttackHomeNav>::Invoke(T)
extern "C"  void Action_1_Invoke_m337991982_gshared (Action_1_t3746689886 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m337991982((Action_1_t3746689886 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<AttackHomeNav>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttackHomeNav_t3944890504_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m922733103_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m922733103_gshared (Action_1_t3746689886 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m922733103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttackHomeNav_t3944890504_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<AttackHomeNav>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3453949388_gshared (Action_1_t3746689886 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<AttackNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3309173175_gshared (Action_1_t4133526137 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<AttackNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m2969433059_gshared (Action_1_t4133526137 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2969433059((Action_1_t4133526137 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<AttackNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttackNavScreen_t36759459_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2686808482_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2686808482_gshared (Action_1_t4133526137 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2686808482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttackNavScreen_t36759459_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<AttackNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4129501157_gshared (Action_1_t4133526137 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<AttackSearchNav>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m4079321277_gshared (Action_1_t4059684019 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<AttackSearchNav>::Invoke(T)
extern "C"  void Action_1_Invoke_m3967991317_gshared (Action_1_t4059684019 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3967991317((Action_1_t4059684019 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<AttackSearchNav>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttackSearchNav_t4257884637_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m856081054_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m856081054_gshared (Action_1_t4059684019 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m856081054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttackSearchNav_t4257884637_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<AttackSearchNav>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m516602779_gshared (Action_1_t4059684019 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<ChatNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1216749949_gshared (Action_1_t4139143939 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<ChatNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m2705140965_gshared (Action_1_t4139143939 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2705140965((Action_1_t4139143939 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<ChatNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ChatNavScreen_t42377261_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1770129128_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1770129128_gshared (Action_1_t4139143939 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1770129128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ChatNavScreen_t42377261_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<ChatNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m537910531_gshared (Action_1_t4139143939 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<DefendNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2801945135_gshared (Action_1_t3640419085 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<DefendNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m948332923_gshared (Action_1_t3640419085 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m948332923((Action_1_t3640419085 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<DefendNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* DefendNavScreen_t3838619703_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3206883150_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3206883150_gshared (Action_1_t3640419085 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3206883150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DefendNavScreen_t3838619703_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<DefendNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2318622945_gshared (Action_1_t3640419085 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<FriendHomeNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2623790522_gshared (Action_1_t1400837678 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<FriendHomeNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m976245278_gshared (Action_1_t1400837678 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m976245278((Action_1_t1400837678 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<FriendHomeNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* FriendHomeNavScreen_t1599038296_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3101126489_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3101126489_gshared (Action_1_t1400837678 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3101126489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FriendHomeNavScreen_t1599038296_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<FriendHomeNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3197069828_gshared (Action_1_t1400837678 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<FriendNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3357461529_gshared (Action_1_t1977182835 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<FriendNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m1170078321_gshared (Action_1_t1977182835 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1170078321((Action_1_t1977182835 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<FriendNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* FriendNavScreen_t2175383453_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2892149870_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2892149870_gshared (Action_1_t1977182835 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2892149870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FriendNavScreen_t2175383453_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<FriendNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2271822763_gshared (Action_1_t1977182835 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<FriendSearchNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3441227475_gshared (Action_1_t2820188545 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<FriendSearchNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m3227163055_gshared (Action_1_t2820188545 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3227163055((Action_1_t2820188545 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<FriendSearchNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* FriendSearchNavScreen_t3018389163_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1334025714_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1334025714_gshared (Action_1_t2820188545 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1334025714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FriendSearchNavScreen_t3018389163_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<FriendSearchNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3117693293_gshared (Action_1_t2820188545 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<LeaderboardNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3070481036_gshared (Action_1_t2157843094 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<LeaderboardNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m1277607680_gshared (Action_1_t2157843094 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1277607680((Action_1_t2157843094 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<LeaderboardNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* LeaderboardNavScreen_t2356043712_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3979523237_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3979523237_gshared (Action_1_t2157843094 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3979523237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(LeaderboardNavScreen_t2356043712_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<LeaderboardNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3733752162_gshared (Action_1_t2157843094 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<LitJson.PropertyMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1373499491_gshared (Action_1_t3495625518 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<LitJson.PropertyMetadata>::Invoke(T)
extern "C"  void Action_1_Invoke_m2768078663_gshared (Action_1_t3495625518 * __this, PropertyMetadata_t3693826136  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2768078663((Action_1_t3495625518 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PropertyMetadata_t3693826136  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, PropertyMetadata_t3693826136  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<LitJson.PropertyMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t3693826136_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2912810398_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2912810398_gshared (Action_1_t3495625518 * __this, PropertyMetadata_t3693826136  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2912810398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PropertyMetadata_t3693826136_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<LitJson.PropertyMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3089357161_gshared (Action_1_t3495625518 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<PhoneNumberNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m544519198_gshared (Action_1_t1065266632 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<PhoneNumberNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m1621772150_gshared (Action_1_t1065266632 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1621772150((Action_1_t1065266632 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<PhoneNumberNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PhoneNumberNavScreen_t1263467250_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3106248055_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3106248055_gshared (Action_1_t1065266632 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3106248055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PhoneNumberNavScreen_t1263467250_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<PhoneNumberNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4170416320_gshared (Action_1_t1065266632 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<PlayerProfileNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1700589763_gshared (Action_1_t2462467641 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<PlayerProfileNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m736184615_gshared (Action_1_t2462467641 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m736184615((Action_1_t2462467641 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<PlayerProfileNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PlayerProfileNavScreen_t2660668259_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2188443582_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2188443582_gshared (Action_1_t2462467641 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2188443582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PlayerProfileNavScreen_t2660668259_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<PlayerProfileNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3288406601_gshared (Action_1_t2462467641 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<ProfileNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m4087593058_gshared (Action_1_t928457236 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<ProfileNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m1866935106_gshared (Action_1_t928457236 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1866935106((Action_1_t928457236 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<ProfileNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ProfileNavScreen_t1126657854_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3158501595_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3158501595_gshared (Action_1_t928457236 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3158501595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ProfileNavScreen_t1126657854_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<ProfileNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m379590156_gshared (Action_1_t928457236 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<RegNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m545641555_gshared (Action_1_t468972741 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<RegNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m1300412999_gshared (Action_1_t468972741 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1300412999((Action_1_t468972741 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<RegNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RegNavScreen_t667173359_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4011441818_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4011441818_gshared (Action_1_t468972741 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4011441818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RegNavScreen_t667173359_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<RegNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1222750749_gshared (Action_1_t468972741 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<SettingsNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3287699872_gshared (Action_1_t2068926616 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<SettingsNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m4191830716_gshared (Action_1_t2068926616 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4191830716((Action_1_t2068926616 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<SettingsNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* SettingsNavScreen_t2267127234_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1469076661_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1469076661_gshared (Action_1_t2068926616 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1469076661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SettingsNavScreen_t2267127234_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<SettingsNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2007440442_gshared (Action_1_t2068926616 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<StatusNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m744706543_gshared (Action_1_t2674168465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<StatusNavScreen>::Invoke(T)
extern "C"  void Action_1_Invoke_m2320977843_gshared (Action_1_t2674168465 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2320977843((Action_1_t2674168465 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<StatusNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* StatusNavScreen_t2872369083_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2959103408_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2959103408_gshared (Action_1_t2674168465 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2959103408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(StatusNavScreen_t2872369083_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<StatusNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2704301525_gshared (Action_1_t2674168465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3210289808_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2759293487_gshared (Action_1_t3484903818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Byte>::Invoke(T)
extern "C"  void Action_1_Invoke_m335335027_gshared (Action_1_t3484903818 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m335335027((Action_1_t3484903818 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m175262896_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m175262896_gshared (Action_1_t3484903818 * __this, uint8_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m175262896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1853114453_gshared (Action_1_t3484903818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m32077197_gshared (Action_1_t3256280720 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Char>::Invoke(T)
extern "C"  void Action_1_Invoke_m3702739261_gshared (Action_1_t3256280720 * __this, Il2CppChar ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3702739261((Action_1_t3256280720 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1592224130_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1592224130_gshared (Action_1_t3256280720 * __this, Il2CppChar ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1592224130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t3454481338_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3307217879_gshared (Action_1_t3256280720 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m684242267_gshared (Action_1_t4135621323 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m93096007_gshared (Action_1_t4135621323 * __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m93096007((Action_1_t4135621323 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t38854645_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2506333398_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2506333398_gshared (Action_1_t4135621323 * __this, KeyValuePair_2_t38854645  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2506333398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t38854645_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m596970257_gshared (Action_1_t4135621323 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1611651693_gshared (Action_1_t3843045296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int16>::Invoke(T)
extern "C"  void Action_1_Invoke_m480853045_gshared (Action_1_t3843045296 * __this, int16_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m480853045((Action_1_t3843045296 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int16_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int16_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int16>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1513573716_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1513573716_gshared (Action_1_t3843045296 * __this, int16_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1513573716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int16>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1520429331_gshared (Action_1_t3843045296 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m514039655_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m2055228803_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2055228803((Action_1_t1873676830 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1720726178_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1720726178_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1720726178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3128406917_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Nullable`1<System.DateTime>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3656815065_gshared (Action_1_t3053038662 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Nullable`1<System.DateTime>>::Invoke(T)
extern "C"  void Action_1_Invoke_m3439660561_gshared (Action_1_t3053038662 * __this, Nullable_1_t3251239280  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3439660561((Action_1_t3053038662 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Nullable_1_t3251239280  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Nullable_1_t3251239280  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Nullable`1<System.DateTime>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Nullable_1_t3251239280_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m753301554_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m753301554_gshared (Action_1_t3053038662 * __this, Nullable_1_t3251239280  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m753301554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Nullable_1_t3251239280_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Nullable`1<System.DateTime>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2650761147_gshared (Action_1_t3053038662 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2192865289_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m3960033342_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3960033342((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3609901643_gshared (Action_1_t4190924221 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m3377187479_gshared (Action_1_t4190924221 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3377187479((Action_1_t4190924221 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2361014002_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2361014002_gshared (Action_1_t4190924221 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2361014002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3555714869_gshared (Action_1_t4190924221 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m592790842_gshared (Action_1_t1299997296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m3721769778_gshared (Action_1_t1299997296 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3721769778((Action_1_t1299997296 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1386903007_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1386903007_gshared (Action_1_t1299997296 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1386903007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m350311208_gshared (Action_1_t1299997296 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m928763336_gshared (Action_1_t1878309314 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Single>::Invoke(T)
extern "C"  void Action_1_Invoke_m3661933365_gshared (Action_1_t1878309314 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3661933365((Action_1_t1878309314 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1617223338_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1617223338_gshared (Action_1_t1878309314 * __this, float ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1617223338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m249557173_gshared (Action_1_t1878309314 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnibillError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3576682535_gshared (Action_1_t1555659169 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnibillError>::Invoke(T)
extern "C"  void Action_1_Invoke_m979801491_gshared (Action_1_t1555659169 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m979801491((Action_1_t1555659169 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnibillError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UnibillError_t1753859787_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2299256718_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2299256718_gshared (Action_1_t1555659169 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2299256718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnibillError_t1753859787_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnibillError>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2927255369_gshared (Action_1_t1555659169 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnibillState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m4291250407_gshared (Action_1_t4073934390 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnibillState>::Invoke(T)
extern "C"  void Action_1_Invoke_m910945996_gshared (Action_1_t4073934390 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m910945996((Action_1_t4073934390 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnibillState>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UnibillState_t4272135008_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m50588463_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m50588463_gshared (Action_1_t4073934390 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m50588463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnibillState_t4272135008_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnibillState>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1747317214_gshared (Action_1_t4073934390 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m74371284_gshared (Action_1_t1822191457 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Color>::Invoke(T)
extern "C"  void Action_1_Invoke_m3422995740_gshared (Action_1_t1822191457 * __this, Color_t2020392075  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3422995740((Action_1_t1822191457 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1002727263_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1002727263_gshared (Action_1_t1822191457 * __this, Color_t2020392075  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1002727263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m5961818_gshared (Action_1_t1822191457 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m984921673_gshared (Action_1_t676316900 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Color32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3852022121_gshared (Action_1_t676316900 * __this, Color32_t874517518  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3852022121((Action_1_t676316900 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m670751528_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m670751528_gshared (Action_1_t676316900 * __this, Color32_t874517518  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m670751528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3798228067_gshared (Action_1_t676316900 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3415321671_gshared (Action_1_t4117953054 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m2664461947_gshared (Action_1_t4117953054 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2664461947((Action_1_t4117953054 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m910318220_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m910318220_gshared (Action_1_t4117953054 * __this, RaycastResult_t21186376  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m910318220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m857894033_gshared (Action_1_t4117953054 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2820836195_gshared (Action_1_t3469344930 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Experimental.Director.Playable>::Invoke(T)
extern "C"  void Action_1_Invoke_m4230181879_gshared (Action_1_t3469344930 * __this, Playable_t3667545548  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4230181879((Action_1_t3469344930 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Experimental.Director.Playable>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Playable_t3667545548_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3939923902_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3939923902_gshared (Action_1_t3469344930 * __this, Playable_t3667545548  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3939923902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Playable_t3667545548_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Experimental.Director.Playable>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2477520633_gshared (Action_1_t3469344930 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.RuntimePlatform>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m432574964_gshared (Action_1_t1671384349 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.RuntimePlatform>::Invoke(T)
extern "C"  void Action_1_Invoke_m1222519072_gshared (Action_1_t1671384349 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1222519072((Action_1_t1671384349 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.RuntimePlatform>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1732580905_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1732580905_gshared (Action_1_t1671384349 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1732580905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.RuntimePlatform>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1560669286_gshared (Action_1_t1671384349 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3865220215_gshared (Action_1_t2858436182 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m433096323_gshared (Action_1_t2858436182 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m433096323((Action_1_t2858436182 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3589829392_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3589829392_gshared (Action_1_t2858436182 * __this, UICharInfo_t3056636800  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3589829392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1787496237_gshared (Action_1_t2858436182 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m933377513_gshared (Action_1_t3423077256 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m1440002041_gshared (Action_1_t3423077256 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1440002041((Action_1_t3423077256 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1314092322_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1314092322_gshared (Action_1_t3423077256 * __this, UILineInfo_t3621277874  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1314092322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2124810843_gshared (Action_1_t3423077256 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m722292825_gshared (Action_1_t1006058200 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  void Action_1_Invoke_m2226099137_gshared (Action_1_t1006058200 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2226099137((Action_1_t1006058200 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3450966034_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3450966034_gshared (Action_1_t1006058200 * __this, UIVertex_t1204258818  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3450966034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1826856235_gshared (Action_1_t1006058200 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3102057377_gshared (Action_1_t2045506961 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void Action_1_Invoke_m2284682836_gshared (Action_1_t2045506961 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2284682836((Action_1_t2045506961 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m493747221_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m493747221_gshared (Action_1_t2045506961 * __this, Vector2_t2243707579  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m493747221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1586893730_gshared (Action_1_t2045506961 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1033856207_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void Action_1_Invoke_m212965499_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m212965499((Action_1_t2045506962 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m402802490_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m402802490_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m402802490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4206793277_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m858813886_gshared (Action_1_t2045506963 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void Action_1_Invoke_m3127338686_gshared (Action_1_t2045506963 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3127338686((Action_1_t2045506963 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1860947955_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1860947955_gshared (Action_1_t2045506963 * __this, Vector4_t2243707581  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1860947955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m11252568_gshared (Action_1_t2045506963 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3842146412_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3842146412((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m631378381_gshared (Action_2_t1954480006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1256339344_gshared (Action_2_t1954480006 * __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1256339344((Action_2_t1954480006 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m2449831909_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2449831909_gshared (Action_2_t1954480006 * __this, Il2CppObject * ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2449831909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m1881200271_gshared (Action_2_t1954480006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2967680750_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m715425719_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m715425719((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3652202720_gshared (Action_2_t3492767325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3217925451_gshared (Action_2_t3492767325 * __this, Vector2_t2243707579  ___arg10, Vector2_t2243707579  ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3217925451((Action_2_t3492767325 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___arg10, Vector2_t2243707579  ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___arg10, Vector2_t2243707579  ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m4292003632_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m4292003632_gshared (Action_2_t3492767325 * __this, Vector2_t2243707579  ___arg10, Vector2_t2243707579  ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m4292003632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2444925212_gshared (Action_2_t3492767325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2414365210_gshared (Action_3_t2202283254 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m1211332736_gshared (Action_3_t2202283254 * __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m1211332736((Action_3_t2202283254 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Boolean,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m4234685015_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m4234685015_gshared (Action_3_t2202283254 * __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m4234685015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m695059408_gshared (Action_3_t2202283254 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2182397233_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m759875865_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m759875865((Action_3_t1115657183 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m275697784_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m4265906515_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.UInt16,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2795754957_gshared (Action_3_t28289771 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.UInt16,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m2754766605_gshared (Action_3_t28289771 * __this, Il2CppObject * ___arg10, uint16_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m2754766605((Action_3_t28289771 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, uint16_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, uint16_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint16_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.UInt16,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1850453620_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1850453620_gshared (Action_3_t28289771 * __this, Il2CppObject * ___arg10, uint16_t ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1850453620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.UInt16,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m2830366695_gshared (Action_3_t28289771 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3778554727_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3194679940_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1279844890 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1279844890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1279844890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  ArrayReadOnlyList_1_get_Item_m2045253203_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1476592004_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2272682593_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m745254596_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m592463462_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638842154_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1984901664_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m3708038182_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3821693737_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1809425308_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m503707439_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m632503387_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2270349795_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2158247090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<AttackHomeNav>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2321939449_gshared (InternalEnumerator_1_t508675470 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2321939449_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t508675470 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t508675470 *>(__this + 1);
	InternalEnumerator_1__ctor_m2321939449(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackHomeNav>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4040966061_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4040966061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t508675470 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t508675470 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4040966061(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AttackHomeNav>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3095401865_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m4113124508((InternalEnumerator_1_t508675470 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3095401865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t508675470 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t508675470 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3095401865(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackHomeNav>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4240110220_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4240110220_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t508675470 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t508675470 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4240110220(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AttackHomeNav>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2803236661_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2803236661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t508675470 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t508675470 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2803236661(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AttackHomeNav>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4113124508_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m4113124508_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4113124508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m4113124508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t508675470 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t508675470 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4113124508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3796537098_gshared (InternalEnumerator_1_t895511721 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3796537098_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t895511721 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t895511721 *>(__this + 1);
	InternalEnumerator_1__ctor_m3796537098(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3883637026_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3883637026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t895511721 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t895511721 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3883637026(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AttackNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m285100348_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m875940467((InternalEnumerator_1_t895511721 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m285100348_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t895511721 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t895511721 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m285100348(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3830106263_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3830106263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t895511721 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t895511721 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3830106263(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AttackNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m762896482_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m762896482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t895511721 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t895511721 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m762896482(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AttackNavScreen>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m875940467_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m875940467_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m875940467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m875940467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t895511721 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t895511721 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m875940467(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackSearchNav>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3531111938_gshared (InternalEnumerator_1_t821669603 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3531111938_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t821669603 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t821669603 *>(__this + 1);
	InternalEnumerator_1__ctor_m3531111938(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackSearchNav>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1786218918_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1786218918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t821669603 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t821669603 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1786218918(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AttackSearchNav>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1808662040_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1184022829((InternalEnumerator_1_t821669603 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1808662040_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t821669603 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t821669603 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1808662040(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AttackSearchNav>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m171365285_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m171365285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t821669603 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t821669603 *>(__this + 1);
	InternalEnumerator_1_Dispose_m171365285(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AttackSearchNav>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1374684162_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1374684162_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t821669603 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t821669603 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1374684162(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AttackSearchNav>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1184022829_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1184022829_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1184022829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1184022829_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t821669603 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t821669603 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1184022829(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<BestHTTP.SignalR.Messages.ClientMessage>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1365310119_gshared (InternalEnumerator_1_t1483032230 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1365310119_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1483032230 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1483032230 *>(__this + 1);
	InternalEnumerator_1__ctor_m1365310119(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<BestHTTP.SignalR.Messages.ClientMessage>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1023521623_gshared (InternalEnumerator_1_t1483032230 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1023521623_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1483032230 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1483032230 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1023521623(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<BestHTTP.SignalR.Messages.ClientMessage>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3009718111_gshared (InternalEnumerator_1_t1483032230 * __this, const MethodInfo* method)
{
	{
		ClientMessage_t624279968  L_0 = InternalEnumerator_1_get_Current_m993106792((InternalEnumerator_1_t1483032230 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ClientMessage_t624279968  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3009718111_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1483032230 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1483032230 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3009718111(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<BestHTTP.SignalR.Messages.ClientMessage>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m81459612_gshared (InternalEnumerator_1_t1483032230 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m81459612_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1483032230 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1483032230 *>(__this + 1);
	InternalEnumerator_1_Dispose_m81459612(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<BestHTTP.SignalR.Messages.ClientMessage>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3084440387_gshared (InternalEnumerator_1_t1483032230 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3084440387_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1483032230 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1483032230 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3084440387(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<BestHTTP.SignalR.Messages.ClientMessage>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m993106792_MetadataUsageId;
extern "C"  ClientMessage_t624279968  InternalEnumerator_1_get_Current_m993106792_gshared (InternalEnumerator_1_t1483032230 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m993106792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ClientMessage_t624279968  L_8 = ((  ClientMessage_t624279968  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ClientMessage_t624279968  InternalEnumerator_1_get_Current_m993106792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1483032230 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1483032230 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m993106792(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ChatNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m154880432_gshared (InternalEnumerator_1_t901129523 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m154880432_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t901129523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t901129523 *>(__this + 1);
	InternalEnumerator_1__ctor_m154880432(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ChatNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1922998684_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1922998684_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t901129523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t901129523 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1922998684(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ChatNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3846825882_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2578937965((InternalEnumerator_1_t901129523 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3846825882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t901129523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t901129523 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3846825882(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ChatNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2998198293_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2998198293_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t901129523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t901129523 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2998198293(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ChatNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3954121856_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3954121856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t901129523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t901129523 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3954121856(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ChatNavScreen>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2578937965_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2578937965_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2578937965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2578937965_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t901129523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t901129523 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2578937965(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<DefendNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2280560146_gshared (InternalEnumerator_1_t402404669 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2280560146_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t402404669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t402404669 *>(__this + 1);
	InternalEnumerator_1__ctor_m2280560146(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<DefendNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m577257706_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m577257706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t402404669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t402404669 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m577257706(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<DefendNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2610194196_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2824285659((InternalEnumerator_1_t402404669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2610194196_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t402404669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t402404669 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2610194196(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<DefendNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m398501439_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m398501439_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t402404669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t402404669 *>(__this + 1);
	InternalEnumerator_1_Dispose_m398501439(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<DefendNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1431212986_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1431212986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t402404669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t402404669 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1431212986(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<DefendNavScreen>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2824285659_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2824285659_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2824285659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2824285659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t402404669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t402404669 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2824285659(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendHomeNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1886975895_gshared (InternalEnumerator_1_t2457790558 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1886975895_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2457790558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2457790558 *>(__this + 1);
	InternalEnumerator_1__ctor_m1886975895(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendHomeNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2015716919_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2015716919_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2457790558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2457790558 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2015716919(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FriendHomeNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2338409511_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2074619856((InternalEnumerator_1_t2457790558 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2338409511_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2457790558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2457790558 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2338409511(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendHomeNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4213171772_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4213171772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2457790558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2457790558 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4213171772(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FriendHomeNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2431540723_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2431540723_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2457790558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2457790558 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2431540723(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FriendHomeNavScreen>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2074619856_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2074619856_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2074619856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2074619856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2457790558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2457790558 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2074619856(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4150477014_gshared (InternalEnumerator_1_t3034135715 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4150477014_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3034135715 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3034135715 *>(__this + 1);
	InternalEnumerator_1__ctor_m4150477014(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3871634594_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3871634594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3034135715 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3034135715 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3871634594(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FriendNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1492489908_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m685573937((InternalEnumerator_1_t3034135715 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1492489908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3034135715 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3034135715 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1492489908(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3485547609_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3485547609_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3034135715 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3034135715 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3485547609(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FriendNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2243210150_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2243210150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3034135715 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3034135715 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2243210150(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FriendNavScreen>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m685573937_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m685573937_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m685573937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m685573937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3034135715 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3034135715 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m685573937(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendSearchNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3442344390_gshared (InternalEnumerator_1_t3877141425 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3442344390_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3877141425 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3877141425 *>(__this + 1);
	InternalEnumerator_1__ctor_m3442344390(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendSearchNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1294219654_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1294219654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3877141425 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3877141425 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1294219654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FriendSearchNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2472733776_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2417651319((InternalEnumerator_1_t3877141425 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2472733776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3877141425 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3877141425 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2472733776(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FriendSearchNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4101493547_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4101493547_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3877141425 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3877141425 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4101493547(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FriendSearchNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3772865278_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3772865278_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3877141425 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3877141425 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3772865278(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FriendSearchNavScreen>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2417651319_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2417651319_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2417651319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2417651319_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3877141425 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3877141425 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2417651319(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
