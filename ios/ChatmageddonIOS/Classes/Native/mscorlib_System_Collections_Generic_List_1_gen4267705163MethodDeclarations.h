﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UISprite>::.ctor()
#define List_1__ctor_m2346583436(__this, method) ((  void (*) (List_1_t4267705163 *, const MethodInfo*))List_1__ctor_m2944773907_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m413905582(__this, ___collection0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3642371895_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::.ctor(System.Int32)
#define List_1__ctor_m886824464(__this, ___capacity0, method) ((  void (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::.ctor(T[],System.Int32)
#define List_1__ctor_m29315296(__this, ___data0, ___size1, method) ((  void (*) (List_1_t4267705163 *, UISpriteU5BU5D_t3315594182*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::.cctor()
#define List_1__cctor_m3306587076(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UISprite>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m41349547(__this, method) ((  Il2CppObject* (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1387812879(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4267705163 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UISprite>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1338205158(__this, method) ((  Il2CppObject * (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1440356047(__this, ___item0, method) ((  int32_t (*) (List_1_t4267705163 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1001737863(__this, ___item0, method) ((  bool (*) (List_1_t4267705163 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1314043229(__this, ___item0, method) ((  int32_t (*) (List_1_t4267705163 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3743367580(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4267705163 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2374547762(__this, ___item0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3718886690(__this, method) ((  bool (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m322013927(__this, method) ((  bool (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UISprite>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4282804867(__this, method) ((  Il2CppObject * (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1794813256(__this, method) ((  bool (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1924864779(__this, method) ((  bool (*) (List_1_t4267705163 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UISprite>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m235090310(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3465801201(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4267705163 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Add(T)
#define List_1_Add_m68694088(__this, ___item0, method) ((  void (*) (List_1_t4267705163 *, UISprite_t603616735 *, const MethodInfo*))List_1_Add_m2016287831_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m576355683(__this, ___newCount0, method) ((  void (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m387347296(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4267705163 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1197935203(__this, ___collection0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2772251731(__this, ___enumerable0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2922493078(__this, ___collection0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1153521819_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UISprite>::AsReadOnly()
#define List_1_AsReadOnly_m1733624867(__this, method) ((  ReadOnlyCollection_1_t789402427 * (*) (List_1_t4267705163 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Clear()
#define List_1_Clear_m3619061132(__this, method) ((  void (*) (List_1_t4267705163 *, const MethodInfo*))List_1_Clear_m1130211784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::Contains(T)
#define List_1_Contains_m3604243458(__this, ___item0, method) ((  bool (*) (List_1_t4267705163 *, UISprite_t603616735 *, const MethodInfo*))List_1_Contains_m3004503139_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CopyTo(T[])
#define List_1_CopyTo_m3254048483(__this, ___array0, method) ((  void (*) (List_1_t4267705163 *, UISpriteU5BU5D_t3315594182*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1951367716(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4267705163 *, UISpriteU5BU5D_t3315594182*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m2938421702(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t4267705163 *, int32_t, UISpriteU5BU5D_t3315594182*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3378929717_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m3858186090(__this, ___match0, method) ((  bool (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<UISprite>::Find(System.Predicate`1<T>)
#define List_1_Find_m1665806398(__this, ___match0, method) ((  UISprite_t603616735 * (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3903126391(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3341554146 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UISprite>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3145738487(__this, ___match0, method) ((  List_1_t4267705163 * (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UISprite>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m3116989353(__this, ___match0, method) ((  List_1_t4267705163 * (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UISprite>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m2178375609(__this, ___match0, method) ((  List_1_t4267705163 * (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m904817095(__this, ___match0, method) ((  int32_t (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2648605544(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4267705163 *, int32_t, int32_t, Predicate_1_t3341554146 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UISprite>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m3576113265(__this, ___action0, method) ((  void (*) (List_1_t4267705163 *, Action_1_t405416117 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UISprite>::GetEnumerator()
#define List_1_GetEnumerator_m3304211901(__this, method) ((  Enumerator_t3802434837  (*) (List_1_t4267705163 *, const MethodInfo*))List_1_GetEnumerator_m1278946119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::IndexOf(T)
#define List_1_IndexOf_m609974338(__this, ___item0, method) ((  int32_t (*) (List_1_t4267705163 *, UISprite_t603616735 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2646154611(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4267705163 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1175858212(__this, ___index0, method) ((  void (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Insert(System.Int32,T)
#define List_1_Insert_m864376373(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4267705163 *, int32_t, UISprite_t603616735 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m161075422(__this, ___collection0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UISprite>::Remove(T)
#define List_1_Remove_m2755826117(__this, ___item0, method) ((  bool (*) (List_1_t4267705163 *, UISprite_t603616735 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m4081504015(__this, ___match0, method) ((  int32_t (*) (List_1_t4267705163 *, Predicate_1_t3341554146 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m164780153(__this, ___index0, method) ((  void (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m350186608(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4267705163 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Reverse()
#define List_1_Reverse_m821231403(__this, method) ((  void (*) (List_1_t4267705163 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Sort()
#define List_1_Sort_m310625229(__this, method) ((  void (*) (List_1_t4267705163 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3473370081(__this, ___comparer0, method) ((  void (*) (List_1_t4267705163 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m141992188(__this, ___comparison0, method) ((  void (*) (List_1_t4267705163 *, Comparison_1_t1865355586 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UISprite>::ToArray()
#define List_1_ToArray_m525562722(__this, method) ((  UISpriteU5BU5D_t3315594182* (*) (List_1_t4267705163 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::TrimExcess()
#define List_1_TrimExcess_m3673904694(__this, method) ((  void (*) (List_1_t4267705163 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::get_Capacity()
#define List_1_get_Capacity_m1172647468(__this, method) ((  int32_t (*) (List_1_t4267705163 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UISprite>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1978825219(__this, ___value0, method) ((  void (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UISprite>::get_Count()
#define List_1_get_Count_m3856361562(__this, method) ((  int32_t (*) (List_1_t4267705163 *, const MethodInfo*))List_1_get_Count_m2021003857_gshared)(__this, method)
// T System.Collections.Generic.List`1<UISprite>::get_Item(System.Int32)
#define List_1_get_Item_m3618430911(__this, ___index0, method) ((  UISprite_t603616735 * (*) (List_1_t4267705163 *, int32_t, const MethodInfo*))List_1_get_Item_m1607739884_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UISprite>::set_Item(System.Int32,T)
#define List_1_set_Item_m2245795558(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4267705163 *, int32_t, UISprite_t603616735 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
