﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "AssemblyU2DCSharp_Bread457172030.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PUBread
struct  PUBread_t493011471  : public Bread_t457172030
{
public:
	// System.String PUBread::toastTitle
	String_t* ___toastTitle_4;
	// UnityEngine.Texture PUBread::toastTexture
	Texture_t2243626319 * ___toastTexture_5;
	// System.String PUBread::toastButtonText
	String_t* ___toastButtonText_6;

public:
	inline static int32_t get_offset_of_toastTitle_4() { return static_cast<int32_t>(offsetof(PUBread_t493011471, ___toastTitle_4)); }
	inline String_t* get_toastTitle_4() const { return ___toastTitle_4; }
	inline String_t** get_address_of_toastTitle_4() { return &___toastTitle_4; }
	inline void set_toastTitle_4(String_t* value)
	{
		___toastTitle_4 = value;
		Il2CppCodeGenWriteBarrier(&___toastTitle_4, value);
	}

	inline static int32_t get_offset_of_toastTexture_5() { return static_cast<int32_t>(offsetof(PUBread_t493011471, ___toastTexture_5)); }
	inline Texture_t2243626319 * get_toastTexture_5() const { return ___toastTexture_5; }
	inline Texture_t2243626319 ** get_address_of_toastTexture_5() { return &___toastTexture_5; }
	inline void set_toastTexture_5(Texture_t2243626319 * value)
	{
		___toastTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___toastTexture_5, value);
	}

	inline static int32_t get_offset_of_toastButtonText_6() { return static_cast<int32_t>(offsetof(PUBread_t493011471, ___toastButtonText_6)); }
	inline String_t* get_toastButtonText_6() const { return ___toastButtonText_6; }
	inline String_t** get_address_of_toastButtonText_6() { return &___toastButtonText_6; }
	inline void set_toastButtonText_6(String_t* value)
	{
		___toastButtonText_6 = value;
		Il2CppCodeGenWriteBarrier(&___toastButtonText_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
