﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsNavigationController
struct FriendsNavigationController_t702141233;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsNavigationController::.ctor()
extern "C"  void FriendsNavigationController__ctor_m2970067686 (FriendsNavigationController_t702141233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigationController::NavigateBackwards()
extern "C"  void FriendsNavigationController_NavigateBackwards_m1627419367 (FriendsNavigationController_t702141233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigationController::NavigateForwards(FriendNavScreen)
extern "C"  void FriendsNavigationController_NavigateForwards_m1105393458 (FriendsNavigationController_t702141233 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsNavigationController::ValidateNavigation(FriendNavScreen,NavigationDirection)
extern "C"  bool FriendsNavigationController_ValidateNavigation_m529314942 (FriendsNavigationController_t702141233 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigationController::LoadNavScreen(FriendNavScreen,NavigationDirection,System.Boolean)
extern "C"  void FriendsNavigationController_LoadNavScreen_m3281927404 (FriendsNavigationController_t702141233 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigationController::UnloadNavScreen(FriendNavScreen,NavigationDirection)
extern "C"  void FriendsNavigationController_UnloadNavScreen_m1192857954 (FriendsNavigationController_t702141233 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigationController::PopulateTitleBar(FriendNavScreen)
extern "C"  void FriendsNavigationController_PopulateTitleBar_m1777624790 (FriendsNavigationController_t702141233 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
