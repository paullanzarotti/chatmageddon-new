﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2995460285.h"
#include "AssemblyU2DCSharp_TwitterLoginEntrance1132015978.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseTwitterManager`1<ChatmageddonTwitterManager>
struct  BaseTwitterManager_1_t2831110462  : public MonoSingleton_1_t2995460285
{
public:
	// System.Boolean BaseTwitterManager`1::canUseTweetSheet
	bool ___canUseTweetSheet_3;
	// System.String BaseTwitterManager`1::presetStatus
	String_t* ___presetStatus_4;
	// System.String BaseTwitterManager`1::imagePath
	String_t* ___imagePath_5;
	// TwitterLoginEntrance BaseTwitterManager`1::currentEntrance
	int32_t ___currentEntrance_6;
	// System.String BaseTwitterManager`1::loginStatus
	String_t* ___loginStatus_7;

public:
	inline static int32_t get_offset_of_canUseTweetSheet_3() { return static_cast<int32_t>(offsetof(BaseTwitterManager_1_t2831110462, ___canUseTweetSheet_3)); }
	inline bool get_canUseTweetSheet_3() const { return ___canUseTweetSheet_3; }
	inline bool* get_address_of_canUseTweetSheet_3() { return &___canUseTweetSheet_3; }
	inline void set_canUseTweetSheet_3(bool value)
	{
		___canUseTweetSheet_3 = value;
	}

	inline static int32_t get_offset_of_presetStatus_4() { return static_cast<int32_t>(offsetof(BaseTwitterManager_1_t2831110462, ___presetStatus_4)); }
	inline String_t* get_presetStatus_4() const { return ___presetStatus_4; }
	inline String_t** get_address_of_presetStatus_4() { return &___presetStatus_4; }
	inline void set_presetStatus_4(String_t* value)
	{
		___presetStatus_4 = value;
		Il2CppCodeGenWriteBarrier(&___presetStatus_4, value);
	}

	inline static int32_t get_offset_of_imagePath_5() { return static_cast<int32_t>(offsetof(BaseTwitterManager_1_t2831110462, ___imagePath_5)); }
	inline String_t* get_imagePath_5() const { return ___imagePath_5; }
	inline String_t** get_address_of_imagePath_5() { return &___imagePath_5; }
	inline void set_imagePath_5(String_t* value)
	{
		___imagePath_5 = value;
		Il2CppCodeGenWriteBarrier(&___imagePath_5, value);
	}

	inline static int32_t get_offset_of_currentEntrance_6() { return static_cast<int32_t>(offsetof(BaseTwitterManager_1_t2831110462, ___currentEntrance_6)); }
	inline int32_t get_currentEntrance_6() const { return ___currentEntrance_6; }
	inline int32_t* get_address_of_currentEntrance_6() { return &___currentEntrance_6; }
	inline void set_currentEntrance_6(int32_t value)
	{
		___currentEntrance_6 = value;
	}

	inline static int32_t get_offset_of_loginStatus_7() { return static_cast<int32_t>(offsetof(BaseTwitterManager_1_t2831110462, ___loginStatus_7)); }
	inline String_t* get_loginStatus_7() const { return ___loginStatus_7; }
	inline String_t** get_address_of_loginStatus_7() { return &___loginStatus_7; }
	inline void set_loginStatus_7(String_t* value)
	{
		___loginStatus_7 = value;
		Il2CppCodeGenWriteBarrier(&___loginStatus_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
