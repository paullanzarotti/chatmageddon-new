﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationSectionGroup
struct ConfigurationSectionGroup_t2230982736;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t2346323570;
// System.String
struct String_t;
// System.Configuration.ConfigurationSectionGroupCollection
struct ConfigurationSectionGroupCollection_t575145286;
// System.Configuration.ConfigurationSectionCollection
struct ConfigurationSectionCollection_t4261113299;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Configuration_System_Configuration_SectionG2346323570.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.ConfigurationSectionGroup::.ctor()
extern "C"  void ConfigurationSectionGroup__ctor_m2215148105 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroup::get_Config()
extern "C"  Configuration_t3335372970 * ConfigurationSectionGroup_get_Config_m3001742842 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSectionGroup::Initialize(System.Configuration.Configuration,System.Configuration.SectionGroupInfo)
extern "C"  void ConfigurationSectionGroup_Initialize_m2622037419 (ConfigurationSectionGroup_t2230982736 * __this, Configuration_t3335372970 * ___config0, SectionGroupInfo_t2346323570 * ___group1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSectionGroup::SetName(System.String)
extern "C"  void ConfigurationSectionGroup_SetName_m466328578 (ConfigurationSectionGroup_t2230982736 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSectionGroup::ForceDeclaration(System.Boolean)
extern "C"  void ConfigurationSectionGroup_ForceDeclaration_m2125287693 (ConfigurationSectionGroup_t2230982736 * __this, bool ___require0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSectionGroup::ForceDeclaration()
extern "C"  void ConfigurationSectionGroup_ForceDeclaration_m557321038 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationSectionGroup::get_IsDeclared()
extern "C"  bool ConfigurationSectionGroup_get_IsDeclared_m651971612 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationSectionGroup::get_IsDeclarationRequired()
extern "C"  bool ConfigurationSectionGroup_get_IsDeclarationRequired_m3712162445 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationSectionGroup::get_Name()
extern "C"  String_t* ConfigurationSectionGroup_get_Name_m72804892 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationSectionGroup::get_SectionGroupName()
extern "C"  String_t* ConfigurationSectionGroup_get_SectionGroupName_m499260658 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionGroupCollection System.Configuration.ConfigurationSectionGroup::get_SectionGroups()
extern "C"  ConfigurationSectionGroupCollection_t575145286 * ConfigurationSectionGroup_get_SectionGroups_m2522870927 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionGroup::get_Sections()
extern "C"  ConfigurationSectionCollection_t4261113299 * ConfigurationSectionGroup_get_Sections_m3081736119 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationSectionGroup::get_Type()
extern "C"  String_t* ConfigurationSectionGroup_get_Type_m1298375311 (ConfigurationSectionGroup_t2230982736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSectionGroup::set_Type(System.String)
extern "C"  void ConfigurationSectionGroup_set_Type_m2702895256 (ConfigurationSectionGroup_t2230982736 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
