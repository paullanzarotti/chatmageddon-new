﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1
struct U3CWaitForAnimationU3Ec__Iterator1_t3768918776;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1::.ctor()
extern "C"  void U3CWaitForAnimationU3Ec__Iterator1__ctor_m4201610425 (U3CWaitForAnimationU3Ec__Iterator1_t3768918776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitForAnimationU3Ec__Iterator1_MoveNext_m318667215 (U3CWaitForAnimationU3Ec__Iterator1_t3768918776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForAnimationU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1810438835 (U3CWaitForAnimationU3Ec__Iterator1_t3768918776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForAnimationU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m538964299 (U3CWaitForAnimationU3Ec__Iterator1_t3768918776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1::Dispose()
extern "C"  void U3CWaitForAnimationU3Ec__Iterator1_Dispose_m3897623562 (U3CWaitForAnimationU3Ec__Iterator1_t3768918776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager/<WaitForAnimation>c__Iterator1::Reset()
extern "C"  void U3CWaitForAnimationU3Ec__Iterator1_Reset_m483035740 (U3CWaitForAnimationU3Ec__Iterator1_t3768918776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
