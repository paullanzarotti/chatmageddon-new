﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseStoreButton
struct CloseStoreButton_t141935289;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseStoreButton::.ctor()
extern "C"  void CloseStoreButton__ctor_m2717794288 (CloseStoreButton_t141935289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseStoreButton::OnButtonClick()
extern "C"  void CloseStoreButton_OnButtonClick_m478632513 (CloseStoreButton_t141935289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
