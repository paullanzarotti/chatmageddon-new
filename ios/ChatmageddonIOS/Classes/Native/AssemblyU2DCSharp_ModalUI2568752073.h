﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalUI
struct  ModalUI_t2568752073  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ModalUI::reactivateMap
	bool ___reactivateMap_2;

public:
	inline static int32_t get_offset_of_reactivateMap_2() { return static_cast<int32_t>(offsetof(ModalUI_t2568752073, ___reactivateMap_2)); }
	inline bool get_reactivateMap_2() const { return ___reactivateMap_2; }
	inline bool* get_address_of_reactivateMap_2() { return &___reactivateMap_2; }
	inline void set_reactivateMap_2(bool value)
	{
		___reactivateMap_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
