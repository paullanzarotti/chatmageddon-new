﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<SubmitMobilePin>c__AnonStorey4
struct U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<SubmitMobilePin>c__AnonStorey4::.ctor()
extern "C"  void U3CSubmitMobilePinU3Ec__AnonStorey4__ctor_m1257641602 (U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<SubmitMobilePin>c__AnonStorey4::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CSubmitMobilePinU3Ec__AnonStorey4_U3CU3Em__0_m2669114079 (U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
