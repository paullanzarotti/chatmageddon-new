﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<ProfileNavScreen>
struct EqualityComparer_1_t3995260421;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<ProfileNavScreen>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1956068783_gshared (EqualityComparer_1_t3995260421 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1956068783(__this, method) ((  void (*) (EqualityComparer_1_t3995260421 *, const MethodInfo*))EqualityComparer_1__ctor_m1956068783_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<ProfileNavScreen>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m688444320_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m688444320(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m688444320_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<ProfileNavScreen>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4052432466_gshared (EqualityComparer_1_t3995260421 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4052432466(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3995260421 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4052432466_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<ProfileNavScreen>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4266391960_gshared (EqualityComparer_1_t3995260421 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4266391960(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3995260421 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4266391960_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<ProfileNavScreen>::get_Default()
extern "C"  EqualityComparer_1_t3995260421 * EqualityComparer_1_get_Default_m4016900031_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m4016900031(__this /* static, unused */, method) ((  EqualityComparer_1_t3995260421 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m4016900031_gshared)(__this /* static, unused */, method)
