﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ProtectedConfigurationSection
struct ProtectedConfigurationSection_t3541826375;
// System.String
struct String_t;
// System.Configuration.ProviderSettingsCollection
struct ProviderSettingsCollection_t585304908;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t3971982415;
// System.Configuration.ProtectedConfigurationProviderCollection
struct ProtectedConfigurationProviderCollection_t388338823;
// System.Configuration.ProviderSettings
struct ProviderSettings_t873049714;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Protecte3971982415.h"
#include "System_Configuration_System_Configuration_ProviderS873049714.h"

// System.Void System.Configuration.ProtectedConfigurationSection::.ctor()
extern "C"  void ProtectedConfigurationSection__ctor_m1127371210 (ProtectedConfigurationSection_t3541826375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProtectedConfigurationSection::.cctor()
extern "C"  void ProtectedConfigurationSection__cctor_m710069301 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ProtectedConfigurationSection::get_DefaultProvider()
extern "C"  String_t* ProtectedConfigurationSection_get_DefaultProvider_m1656993434 (ProtectedConfigurationSection_t3541826375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProtectedConfigurationSection::set_DefaultProvider(System.String)
extern "C"  void ProtectedConfigurationSection_set_DefaultProvider_m2577985551 (ProtectedConfigurationSection_t3541826375 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProviderSettingsCollection System.Configuration.ProtectedConfigurationSection::get_Providers()
extern "C"  ProviderSettingsCollection_t585304908 * ProtectedConfigurationSection_get_Providers_m3108810289 (ProtectedConfigurationSection_t3541826375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProtectedConfigurationSection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * ProtectedConfigurationSection_get_Properties_m3218406561 (ProtectedConfigurationSection_t3541826375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ProtectedConfigurationSection::EncryptSection(System.String,System.Configuration.ProtectedConfigurationProvider)
extern "C"  String_t* ProtectedConfigurationSection_EncryptSection_m568066933 (ProtectedConfigurationSection_t3541826375 * __this, String_t* ___clearXml0, ProtectedConfigurationProvider_t3971982415 * ___protectionProvider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ProtectedConfigurationSection::DecryptSection(System.String,System.Configuration.ProtectedConfigurationProvider)
extern "C"  String_t* ProtectedConfigurationSection_DecryptSection_m1576558531 (ProtectedConfigurationSection_t3541826375 * __this, String_t* ___encryptedXml0, ProtectedConfigurationProvider_t3971982415 * ___protectionProvider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProtectedConfigurationProviderCollection System.Configuration.ProtectedConfigurationSection::GetAllProviders()
extern "C"  ProtectedConfigurationProviderCollection_t388338823 * ProtectedConfigurationSection_GetAllProviders_m1380267610 (ProtectedConfigurationSection_t3541826375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProtectedConfigurationProvider System.Configuration.ProtectedConfigurationSection::InstantiateProvider(System.Configuration.ProviderSettings)
extern "C"  ProtectedConfigurationProvider_t3971982415 * ProtectedConfigurationSection_InstantiateProvider_m1916220169 (ProtectedConfigurationSection_t3541826375 * __this, ProviderSettings_t873049714 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
