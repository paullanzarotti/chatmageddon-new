﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_TV_Old_Movie
struct  CameraFilterPack_TV_Old_Movie_t1608414762  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_TV_Old_Movie::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Old_Movie::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Old_Movie::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_TV_Old_Movie::SCMaterial
	Material_t193706927 * ___SCMaterial_5;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Old_Movie_t1608414762, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Old_Movie_t1608414762, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Distortion_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Old_Movie_t1608414762, ___Distortion_4)); }
	inline float get_Distortion_4() const { return ___Distortion_4; }
	inline float* get_address_of_Distortion_4() { return &___Distortion_4; }
	inline void set_Distortion_4(float value)
	{
		___Distortion_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Old_Movie_t1608414762, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}
};

struct CameraFilterPack_TV_Old_Movie_t1608414762_StaticFields
{
public:
	// System.Single CameraFilterPack_TV_Old_Movie::ChangeDistortion
	float ___ChangeDistortion_6;

public:
	inline static int32_t get_offset_of_ChangeDistortion_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Old_Movie_t1608414762_StaticFields, ___ChangeDistortion_6)); }
	inline float get_ChangeDistortion_6() const { return ___ChangeDistortion_6; }
	inline float* get_address_of_ChangeDistortion_6() { return &___ChangeDistortion_6; }
	inline void set_ChangeDistortion_6(float value)
	{
		___ChangeDistortion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
