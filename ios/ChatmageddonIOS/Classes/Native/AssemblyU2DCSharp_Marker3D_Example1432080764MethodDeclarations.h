﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Marker3D_Example
struct Marker3D_Example_t1432080764;

#include "codegen/il2cpp-codegen.h"

// System.Void Marker3D_Example::.ctor()
extern "C"  void Marker3D_Example__ctor_m1285333317 (Marker3D_Example_t1432080764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker3D_Example::OnGUI()
extern "C"  void Marker3D_Example_OnGUI_m3979313311 (Marker3D_Example_t1432080764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker3D_Example::Start()
extern "C"  void Marker3D_Example_Start_m1618158429 (Marker3D_Example_t1432080764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
