﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PurchaseableItem>
struct List_1_t2720244128;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// MissileInfoButton
struct MissileInfoButton_t2682635900;
// ItemInfoController
struct ItemInfoController_t4231956141;
// ItemSwipeController
struct ItemSwipeController_t3011021799;
// UISprite
struct UISprite_t603616735;
// AttackInventoryBuyButton
struct AttackInventoryBuyButton_t1923642124;
// SelectMissileButton
struct SelectMissileButton_t3959346244;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackInventoryNavScreen
struct  AttackInventoryNavScreen_t33214553  : public NavigationScreen_t2333230110
{
public:
	// System.Boolean AttackInventoryNavScreen::inventoryUIClosing
	bool ___inventoryUIClosing_3;
	// System.Int32 AttackInventoryNavScreen::currentItemIndex
	int32_t ___currentItemIndex_4;
	// System.Collections.Generic.List`1<PurchaseableItem> AttackInventoryNavScreen::missileItems
	List_1_t2720244128 * ___missileItems_5;
	// UnityEngine.GameObject AttackInventoryNavScreen::infoObject
	GameObject_t1756533147 * ___infoObject_6;
	// UnityEngine.GameObject AttackInventoryNavScreen::itemObject
	GameObject_t1756533147 * ___itemObject_7;
	// UnityEngine.GameObject AttackInventoryNavScreen::lockedBackground
	GameObject_t1756533147 * ___lockedBackground_8;
	// UILabel AttackInventoryNavScreen::lockedLabel
	UILabel_t1795115428 * ___lockedLabel_9;
	// MissileInfoButton AttackInventoryNavScreen::infoButton
	MissileInfoButton_t2682635900 * ___infoButton_10;
	// ItemInfoController AttackInventoryNavScreen::infoController
	ItemInfoController_t4231956141 * ___infoController_11;
	// ItemSwipeController AttackInventoryNavScreen::swipeController
	ItemSwipeController_t3011021799 * ___swipeController_12;
	// UILabel AttackInventoryNavScreen::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_13;
	// UILabel AttackInventoryNavScreen::itemAmountLabel
	UILabel_t1795115428 * ___itemAmountLabel_14;
	// UILabel AttackInventoryNavScreen::itemDamageLabel
	UILabel_t1795115428 * ___itemDamageLabel_15;
	// UISprite AttackInventoryNavScreen::infinitySprite
	UISprite_t603616735 * ___infinitySprite_16;
	// UnityEngine.GameObject AttackInventoryNavScreen::itemLight
	GameObject_t1756533147 * ___itemLight_17;
	// AttackInventoryBuyButton AttackInventoryNavScreen::buyButton
	AttackInventoryBuyButton_t1923642124 * ___buyButton_18;
	// SelectMissileButton AttackInventoryNavScreen::selectMissileButton
	SelectMissileButton_t3959346244 * ___selectMissileButton_19;
	// System.Boolean AttackInventoryNavScreen::showingInfo
	bool ___showingInfo_20;
	// System.Boolean AttackInventoryNavScreen::firstTimeLoading
	bool ___firstTimeLoading_21;

public:
	inline static int32_t get_offset_of_inventoryUIClosing_3() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___inventoryUIClosing_3)); }
	inline bool get_inventoryUIClosing_3() const { return ___inventoryUIClosing_3; }
	inline bool* get_address_of_inventoryUIClosing_3() { return &___inventoryUIClosing_3; }
	inline void set_inventoryUIClosing_3(bool value)
	{
		___inventoryUIClosing_3 = value;
	}

	inline static int32_t get_offset_of_currentItemIndex_4() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___currentItemIndex_4)); }
	inline int32_t get_currentItemIndex_4() const { return ___currentItemIndex_4; }
	inline int32_t* get_address_of_currentItemIndex_4() { return &___currentItemIndex_4; }
	inline void set_currentItemIndex_4(int32_t value)
	{
		___currentItemIndex_4 = value;
	}

	inline static int32_t get_offset_of_missileItems_5() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___missileItems_5)); }
	inline List_1_t2720244128 * get_missileItems_5() const { return ___missileItems_5; }
	inline List_1_t2720244128 ** get_address_of_missileItems_5() { return &___missileItems_5; }
	inline void set_missileItems_5(List_1_t2720244128 * value)
	{
		___missileItems_5 = value;
		Il2CppCodeGenWriteBarrier(&___missileItems_5, value);
	}

	inline static int32_t get_offset_of_infoObject_6() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___infoObject_6)); }
	inline GameObject_t1756533147 * get_infoObject_6() const { return ___infoObject_6; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_6() { return &___infoObject_6; }
	inline void set_infoObject_6(GameObject_t1756533147 * value)
	{
		___infoObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_6, value);
	}

	inline static int32_t get_offset_of_itemObject_7() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___itemObject_7)); }
	inline GameObject_t1756533147 * get_itemObject_7() const { return ___itemObject_7; }
	inline GameObject_t1756533147 ** get_address_of_itemObject_7() { return &___itemObject_7; }
	inline void set_itemObject_7(GameObject_t1756533147 * value)
	{
		___itemObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___itemObject_7, value);
	}

	inline static int32_t get_offset_of_lockedBackground_8() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___lockedBackground_8)); }
	inline GameObject_t1756533147 * get_lockedBackground_8() const { return ___lockedBackground_8; }
	inline GameObject_t1756533147 ** get_address_of_lockedBackground_8() { return &___lockedBackground_8; }
	inline void set_lockedBackground_8(GameObject_t1756533147 * value)
	{
		___lockedBackground_8 = value;
		Il2CppCodeGenWriteBarrier(&___lockedBackground_8, value);
	}

	inline static int32_t get_offset_of_lockedLabel_9() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___lockedLabel_9)); }
	inline UILabel_t1795115428 * get_lockedLabel_9() const { return ___lockedLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_lockedLabel_9() { return &___lockedLabel_9; }
	inline void set_lockedLabel_9(UILabel_t1795115428 * value)
	{
		___lockedLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___lockedLabel_9, value);
	}

	inline static int32_t get_offset_of_infoButton_10() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___infoButton_10)); }
	inline MissileInfoButton_t2682635900 * get_infoButton_10() const { return ___infoButton_10; }
	inline MissileInfoButton_t2682635900 ** get_address_of_infoButton_10() { return &___infoButton_10; }
	inline void set_infoButton_10(MissileInfoButton_t2682635900 * value)
	{
		___infoButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___infoButton_10, value);
	}

	inline static int32_t get_offset_of_infoController_11() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___infoController_11)); }
	inline ItemInfoController_t4231956141 * get_infoController_11() const { return ___infoController_11; }
	inline ItemInfoController_t4231956141 ** get_address_of_infoController_11() { return &___infoController_11; }
	inline void set_infoController_11(ItemInfoController_t4231956141 * value)
	{
		___infoController_11 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_11, value);
	}

	inline static int32_t get_offset_of_swipeController_12() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___swipeController_12)); }
	inline ItemSwipeController_t3011021799 * get_swipeController_12() const { return ___swipeController_12; }
	inline ItemSwipeController_t3011021799 ** get_address_of_swipeController_12() { return &___swipeController_12; }
	inline void set_swipeController_12(ItemSwipeController_t3011021799 * value)
	{
		___swipeController_12 = value;
		Il2CppCodeGenWriteBarrier(&___swipeController_12, value);
	}

	inline static int32_t get_offset_of_itemNameLabel_13() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___itemNameLabel_13)); }
	inline UILabel_t1795115428 * get_itemNameLabel_13() const { return ___itemNameLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_13() { return &___itemNameLabel_13; }
	inline void set_itemNameLabel_13(UILabel_t1795115428 * value)
	{
		___itemNameLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_13, value);
	}

	inline static int32_t get_offset_of_itemAmountLabel_14() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___itemAmountLabel_14)); }
	inline UILabel_t1795115428 * get_itemAmountLabel_14() const { return ___itemAmountLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_itemAmountLabel_14() { return &___itemAmountLabel_14; }
	inline void set_itemAmountLabel_14(UILabel_t1795115428 * value)
	{
		___itemAmountLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemAmountLabel_14, value);
	}

	inline static int32_t get_offset_of_itemDamageLabel_15() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___itemDamageLabel_15)); }
	inline UILabel_t1795115428 * get_itemDamageLabel_15() const { return ___itemDamageLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_itemDamageLabel_15() { return &___itemDamageLabel_15; }
	inline void set_itemDamageLabel_15(UILabel_t1795115428 * value)
	{
		___itemDamageLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemDamageLabel_15, value);
	}

	inline static int32_t get_offset_of_infinitySprite_16() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___infinitySprite_16)); }
	inline UISprite_t603616735 * get_infinitySprite_16() const { return ___infinitySprite_16; }
	inline UISprite_t603616735 ** get_address_of_infinitySprite_16() { return &___infinitySprite_16; }
	inline void set_infinitySprite_16(UISprite_t603616735 * value)
	{
		___infinitySprite_16 = value;
		Il2CppCodeGenWriteBarrier(&___infinitySprite_16, value);
	}

	inline static int32_t get_offset_of_itemLight_17() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___itemLight_17)); }
	inline GameObject_t1756533147 * get_itemLight_17() const { return ___itemLight_17; }
	inline GameObject_t1756533147 ** get_address_of_itemLight_17() { return &___itemLight_17; }
	inline void set_itemLight_17(GameObject_t1756533147 * value)
	{
		___itemLight_17 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_17, value);
	}

	inline static int32_t get_offset_of_buyButton_18() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___buyButton_18)); }
	inline AttackInventoryBuyButton_t1923642124 * get_buyButton_18() const { return ___buyButton_18; }
	inline AttackInventoryBuyButton_t1923642124 ** get_address_of_buyButton_18() { return &___buyButton_18; }
	inline void set_buyButton_18(AttackInventoryBuyButton_t1923642124 * value)
	{
		___buyButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___buyButton_18, value);
	}

	inline static int32_t get_offset_of_selectMissileButton_19() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___selectMissileButton_19)); }
	inline SelectMissileButton_t3959346244 * get_selectMissileButton_19() const { return ___selectMissileButton_19; }
	inline SelectMissileButton_t3959346244 ** get_address_of_selectMissileButton_19() { return &___selectMissileButton_19; }
	inline void set_selectMissileButton_19(SelectMissileButton_t3959346244 * value)
	{
		___selectMissileButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___selectMissileButton_19, value);
	}

	inline static int32_t get_offset_of_showingInfo_20() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___showingInfo_20)); }
	inline bool get_showingInfo_20() const { return ___showingInfo_20; }
	inline bool* get_address_of_showingInfo_20() { return &___showingInfo_20; }
	inline void set_showingInfo_20(bool value)
	{
		___showingInfo_20 = value;
	}

	inline static int32_t get_offset_of_firstTimeLoading_21() { return static_cast<int32_t>(offsetof(AttackInventoryNavScreen_t33214553, ___firstTimeLoading_21)); }
	inline bool get_firstTimeLoading_21() const { return ___firstTimeLoading_21; }
	inline bool* get_address_of_firstTimeLoading_21() { return &___firstTimeLoading_21; }
	inline void set_firstTimeLoading_21(bool value)
	{
		___firstTimeLoading_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
