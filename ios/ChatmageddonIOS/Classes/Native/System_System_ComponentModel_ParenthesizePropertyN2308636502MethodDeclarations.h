﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ParenthesizePropertyNameAttribute
struct ParenthesizePropertyNameAttribute_t2308636502;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.ParenthesizePropertyNameAttribute::.ctor()
extern "C"  void ParenthesizePropertyNameAttribute__ctor_m3156775535 (ParenthesizePropertyNameAttribute_t2308636502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ParenthesizePropertyNameAttribute::.ctor(System.Boolean)
extern "C"  void ParenthesizePropertyNameAttribute__ctor_m1103677480 (ParenthesizePropertyNameAttribute_t2308636502 * __this, bool ___needParenthesis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ParenthesizePropertyNameAttribute::.cctor()
extern "C"  void ParenthesizePropertyNameAttribute__cctor_m1732991808 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ParenthesizePropertyNameAttribute::get_NeedParenthesis()
extern "C"  bool ParenthesizePropertyNameAttribute_get_NeedParenthesis_m4248912058 (ParenthesizePropertyNameAttribute_t2308636502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ParenthesizePropertyNameAttribute::Equals(System.Object)
extern "C"  bool ParenthesizePropertyNameAttribute_Equals_m1259427760 (ParenthesizePropertyNameAttribute_t2308636502 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ParenthesizePropertyNameAttribute::GetHashCode()
extern "C"  int32_t ParenthesizePropertyNameAttribute_GetHashCode_m4266593708 (ParenthesizePropertyNameAttribute_t2308636502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ParenthesizePropertyNameAttribute::IsDefaultAttribute()
extern "C"  bool ParenthesizePropertyNameAttribute_IsDefaultAttribute_m3229341358 (ParenthesizePropertyNameAttribute_t2308636502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
