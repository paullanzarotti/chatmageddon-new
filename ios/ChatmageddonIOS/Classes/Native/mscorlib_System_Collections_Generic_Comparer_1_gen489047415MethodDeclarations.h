﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<FriendHomeNavScreen>
struct Comparer_1_t489047415;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<FriendHomeNavScreen>::.ctor()
extern "C"  void Comparer_1__ctor_m3666615177_gshared (Comparer_1_t489047415 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3666615177(__this, method) ((  void (*) (Comparer_1_t489047415 *, const MethodInfo*))Comparer_1__ctor_m3666615177_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<FriendHomeNavScreen>::.cctor()
extern "C"  void Comparer_1__cctor_m3457246650_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m3457246650(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m3457246650_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<FriendHomeNavScreen>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3566530640_gshared (Comparer_1_t489047415 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m3566530640(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t489047415 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m3566530640_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<FriendHomeNavScreen>::get_Default()
extern "C"  Comparer_1_t489047415 * Comparer_1_get_Default_m56361609_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m56361609(__this /* static, unused */, method) ((  Comparer_1_t489047415 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m56361609_gshared)(__this /* static, unused */, method)
