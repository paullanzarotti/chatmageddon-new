﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.UnityHTTPRequest
struct UnityHTTPRequest_t3521437524;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"

// System.Void Unibill.Impl.UnityHTTPRequest::.ctor(UnityEngine.WWW)
extern "C"  void UnityHTTPRequest__ctor_m565024700 (UnityHTTPRequest_t3521437524 * __this, WWW_t2919945039 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Unibill.Impl.UnityHTTPRequest::get_responseHeaders()
extern "C"  Dictionary_2_t3943999495 * UnityHTTPRequest_get_responseHeaders_m2023278518 (UnityHTTPRequest_t3521437524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Unibill.Impl.UnityHTTPRequest::get_bytes()
extern "C"  ByteU5BU5D_t3397334013* UnityHTTPRequest_get_bytes_m4026605752 (UnityHTTPRequest_t3521437524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnityHTTPRequest::get_contentString()
extern "C"  String_t* UnityHTTPRequest_get_contentString_m401225062 (UnityHTTPRequest_t3521437524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnityHTTPRequest::get_error()
extern "C"  String_t* UnityHTTPRequest_get_error_m765459512 (UnityHTTPRequest_t3521437524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
