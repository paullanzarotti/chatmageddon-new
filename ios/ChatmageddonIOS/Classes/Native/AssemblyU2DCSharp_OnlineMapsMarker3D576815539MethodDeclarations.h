﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// UnityEngine.Transform
struct Transform_t3275118058;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"

// System.Void OnlineMapsMarker3D::.ctor()
extern "C"  void OnlineMapsMarker3D__ctor_m3659847096 (OnlineMapsMarker3D_t576815539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::set_enabled(System.Boolean)
extern "C"  void OnlineMapsMarker3D_set_enabled_m4252797393 (OnlineMapsMarker3D_t576815539 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 OnlineMapsMarker3D::get_relativePosition()
extern "C"  Vector3_t2243707580  OnlineMapsMarker3D_get_relativePosition_m4172240010 (OnlineMapsMarker3D_t576815539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion OnlineMapsMarker3D::get_rotation()
extern "C"  Quaternion_t4030073918  OnlineMapsMarker3D_get_rotation_m2615390023 (OnlineMapsMarker3D_t576815539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::set_rotation(UnityEngine.Quaternion)
extern "C"  void OnlineMapsMarker3D_set_rotation_m2409835304 (OnlineMapsMarker3D_t576815539 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform OnlineMapsMarker3D::get_transform()
extern "C"  Transform_t3275118058 * OnlineMapsMarker3D_get_transform_m3679921503 (OnlineMapsMarker3D_t576815539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::Init(UnityEngine.Transform)
extern "C"  void OnlineMapsMarker3D_Init_m3384351791 (OnlineMapsMarker3D_t576815539 * __this, Transform_t3275118058 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::LookToCoordinates(UnityEngine.Vector2)
extern "C"  void OnlineMapsMarker3D_LookToCoordinates_m1223730719 (OnlineMapsMarker3D_t576815539 * __this, Vector2_t2243707579  ___coordinates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::Reinit(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  void OnlineMapsMarker3D_Reinit_m1975385408 (OnlineMapsMarker3D_t576815539 * __this, Vector2_t2243707579  ___topLeft0, Vector2_t2243707579  ___bottomRight1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::Reinit(System.Double,System.Double,System.Double,System.Double,System.Int32)
extern "C"  void OnlineMapsMarker3D_Reinit_m1343274112 (OnlineMapsMarker3D_t576815539 * __this, double ___tlx0, double ___tly1, double ___brx2, double ___bry3, int32_t ___zoom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsXML OnlineMapsMarker3D::Save(OnlineMapsXML)
extern "C"  OnlineMapsXML_t3341520387 * OnlineMapsMarker3D_Save_m3914461362 (OnlineMapsMarker3D_t576815539 * __this, OnlineMapsXML_t3341520387 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::Update(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  void OnlineMapsMarker3D_Update_m4208805526 (OnlineMapsMarker3D_t576815539 * __this, Vector2_t2243707579  ___topLeft0, Vector2_t2243707579  ___bottomRight1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3D::Update(System.Double,System.Double,System.Double,System.Double,System.Int32)
extern "C"  void OnlineMapsMarker3D_Update_m524786006 (OnlineMapsMarker3D_t576815539 * __this, double ___tlx0, double ___tly1, double ___brx2, double ___bry3, int32_t ___zoom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
