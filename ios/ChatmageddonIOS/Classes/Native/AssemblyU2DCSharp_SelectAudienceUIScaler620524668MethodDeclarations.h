﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectAudienceUIScaler
struct SelectAudienceUIScaler_t620524668;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectAudienceUIScaler::.ctor()
extern "C"  void SelectAudienceUIScaler__ctor_m3880013209 (SelectAudienceUIScaler_t620524668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceUIScaler::GlobalUIScale()
extern "C"  void SelectAudienceUIScaler_GlobalUIScale_m117790062 (SelectAudienceUIScaler_t620524668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
