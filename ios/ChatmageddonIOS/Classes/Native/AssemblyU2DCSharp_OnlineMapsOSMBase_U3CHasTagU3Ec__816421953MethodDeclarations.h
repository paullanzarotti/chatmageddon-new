﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMBase/<HasTag>c__AnonStorey1
struct U3CHasTagU3Ec__AnonStorey1_t816421953;
// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"

// System.Void OnlineMapsOSMBase/<HasTag>c__AnonStorey1::.ctor()
extern "C"  void U3CHasTagU3Ec__AnonStorey1__ctor_m298626450 (U3CHasTagU3Ec__AnonStorey1_t816421953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsOSMBase/<HasTag>c__AnonStorey1::<>m__0(OnlineMapsOSMTag)
extern "C"  bool U3CHasTagU3Ec__AnonStorey1_U3CU3Em__0_m762440442 (U3CHasTagU3Ec__AnonStorey1_t816421953 * __this, OnlineMapsOSMTag_t3629071465 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
