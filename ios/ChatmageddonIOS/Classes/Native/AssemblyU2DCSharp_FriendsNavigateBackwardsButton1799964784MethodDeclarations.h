﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsNavigateBackwardsButton
struct FriendsNavigateBackwardsButton_t1799964784;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void FriendsNavigateBackwardsButton::.ctor()
extern "C"  void FriendsNavigateBackwardsButton__ctor_m1026546531 (FriendsNavigateBackwardsButton_t1799964784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigateBackwardsButton::OnClick()
extern "C"  void FriendsNavigateBackwardsButton_OnClick_m55993924 (FriendsNavigateBackwardsButton_t1799964784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsNavigateBackwardsButton::SetButtonDirection(ButtonDirection)
extern "C"  void FriendsNavigateBackwardsButton_SetButtonDirection_m2515043617 (FriendsNavigateBackwardsButton_t1799964784 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
