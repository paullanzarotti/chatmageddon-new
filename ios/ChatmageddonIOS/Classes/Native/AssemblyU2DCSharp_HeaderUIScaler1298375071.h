﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeaderUIScaler
struct  HeaderUIScaler_t1298375071  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite HeaderUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.Transform HeaderUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_15;
	// UnityEngine.Transform HeaderUIScaler::dailyPoints
	Transform_t3275118058 * ___dailyPoints_16;
	// UnityEngine.Transform HeaderUIScaler::bucks
	Transform_t3275118058 * ___bucks_17;
	// UnityEngine.Transform HeaderUIScaler::leaderboard
	Transform_t3275118058 * ___leaderboard_18;
	// UnityEngine.Transform HeaderUIScaler::fuel
	Transform_t3275118058 * ___fuel_19;
	// UnityEngine.Transform HeaderUIScaler::shield
	Transform_t3275118058 * ___shield_20;
	// UnityEngine.Transform HeaderUIScaler::chat
	Transform_t3275118058 * ___chat_21;
	// UISprite HeaderUIScaler::botDivider
	UISprite_t603616735 * ___botDivider_22;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_profilePicture_15() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___profilePicture_15)); }
	inline Transform_t3275118058 * get_profilePicture_15() const { return ___profilePicture_15; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_15() { return &___profilePicture_15; }
	inline void set_profilePicture_15(Transform_t3275118058 * value)
	{
		___profilePicture_15 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_15, value);
	}

	inline static int32_t get_offset_of_dailyPoints_16() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___dailyPoints_16)); }
	inline Transform_t3275118058 * get_dailyPoints_16() const { return ___dailyPoints_16; }
	inline Transform_t3275118058 ** get_address_of_dailyPoints_16() { return &___dailyPoints_16; }
	inline void set_dailyPoints_16(Transform_t3275118058 * value)
	{
		___dailyPoints_16 = value;
		Il2CppCodeGenWriteBarrier(&___dailyPoints_16, value);
	}

	inline static int32_t get_offset_of_bucks_17() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___bucks_17)); }
	inline Transform_t3275118058 * get_bucks_17() const { return ___bucks_17; }
	inline Transform_t3275118058 ** get_address_of_bucks_17() { return &___bucks_17; }
	inline void set_bucks_17(Transform_t3275118058 * value)
	{
		___bucks_17 = value;
		Il2CppCodeGenWriteBarrier(&___bucks_17, value);
	}

	inline static int32_t get_offset_of_leaderboard_18() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___leaderboard_18)); }
	inline Transform_t3275118058 * get_leaderboard_18() const { return ___leaderboard_18; }
	inline Transform_t3275118058 ** get_address_of_leaderboard_18() { return &___leaderboard_18; }
	inline void set_leaderboard_18(Transform_t3275118058 * value)
	{
		___leaderboard_18 = value;
		Il2CppCodeGenWriteBarrier(&___leaderboard_18, value);
	}

	inline static int32_t get_offset_of_fuel_19() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___fuel_19)); }
	inline Transform_t3275118058 * get_fuel_19() const { return ___fuel_19; }
	inline Transform_t3275118058 ** get_address_of_fuel_19() { return &___fuel_19; }
	inline void set_fuel_19(Transform_t3275118058 * value)
	{
		___fuel_19 = value;
		Il2CppCodeGenWriteBarrier(&___fuel_19, value);
	}

	inline static int32_t get_offset_of_shield_20() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___shield_20)); }
	inline Transform_t3275118058 * get_shield_20() const { return ___shield_20; }
	inline Transform_t3275118058 ** get_address_of_shield_20() { return &___shield_20; }
	inline void set_shield_20(Transform_t3275118058 * value)
	{
		___shield_20 = value;
		Il2CppCodeGenWriteBarrier(&___shield_20, value);
	}

	inline static int32_t get_offset_of_chat_21() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___chat_21)); }
	inline Transform_t3275118058 * get_chat_21() const { return ___chat_21; }
	inline Transform_t3275118058 ** get_address_of_chat_21() { return &___chat_21; }
	inline void set_chat_21(Transform_t3275118058 * value)
	{
		___chat_21 = value;
		Il2CppCodeGenWriteBarrier(&___chat_21, value);
	}

	inline static int32_t get_offset_of_botDivider_22() { return static_cast<int32_t>(offsetof(HeaderUIScaler_t1298375071, ___botDivider_22)); }
	inline UISprite_t603616735 * get_botDivider_22() const { return ___botDivider_22; }
	inline UISprite_t603616735 ** get_address_of_botDivider_22() { return &___botDivider_22; }
	inline void set_botDivider_22(UISprite_t603616735 * value)
	{
		___botDivider_22 = value;
		Il2CppCodeGenWriteBarrier(&___botDivider_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
