﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneNumber/Builder
struct Builder_t2361401461;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Types_C2831594294.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"

// System.Void PhoneNumbers.PhoneNumber::.cctor()
extern "C"  void PhoneNumber__cctor_m965607293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber::.ctor()
extern "C"  void PhoneNumber__ctor_m3585021568 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber::get_DefaultInstance()
extern "C"  PhoneNumber_t814071929 * PhoneNumber_get_DefaultInstance_m2700045699 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber::get_DefaultInstanceForType()
extern "C"  PhoneNumber_t814071929 * PhoneNumber_get_DefaultInstanceForType_m2914235338 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber::get_ThisMessage()
extern "C"  PhoneNumber_t814071929 * PhoneNumber_get_ThisMessage_m207302044 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasCountryCode()
extern "C"  bool PhoneNumber_get_HasCountryCode_m1754941846 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumber::get_CountryCode()
extern "C"  int32_t PhoneNumber_get_CountryCode_m2026536134 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasNationalNumber()
extern "C"  bool PhoneNumber_get_HasNationalNumber_m3747152954 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 PhoneNumbers.PhoneNumber::get_NationalNumber()
extern "C"  uint64_t PhoneNumber_get_NationalNumber_m3187227816 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasExtension()
extern "C"  bool PhoneNumber_get_HasExtension_m1081600708 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumber::get_Extension()
extern "C"  String_t* PhoneNumber_get_Extension_m2952740871 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasItalianLeadingZero()
extern "C"  bool PhoneNumber_get_HasItalianLeadingZero_m2032085605 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_ItalianLeadingZero()
extern "C"  bool PhoneNumber_get_ItalianLeadingZero_m4155429607 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasRawInput()
extern "C"  bool PhoneNumber_get_HasRawInput_m1445884959 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumber::get_RawInput()
extern "C"  String_t* PhoneNumber_get_RawInput_m881167840 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasCountryCodeSource()
extern "C"  bool PhoneNumber_get_HasCountryCodeSource_m1291719269 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Types/CountryCodeSource PhoneNumbers.PhoneNumber::get_CountryCodeSource()
extern "C"  int32_t PhoneNumber_get_CountryCodeSource_m1830349654 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_HasPreferredDomesticCarrierCode()
extern "C"  bool PhoneNumber_get_HasPreferredDomesticCarrierCode_m3899348647 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumber::get_PreferredDomesticCarrierCode()
extern "C"  String_t* PhoneNumber_get_PreferredDomesticCarrierCode_m769746824 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::get_IsInitialized()
extern "C"  bool PhoneNumber_get_IsInitialized_m3885003397 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumber::GetHashCode()
extern "C"  int32_t PhoneNumber_GetHashCode_m2476240949 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber::Equals(System.Object)
extern "C"  bool PhoneNumber_Equals_m306567223 (PhoneNumber_t814071929 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber::CreateBuilder()
extern "C"  Builder_t2361401461 * PhoneNumber_CreateBuilder_m1984358743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber::ToBuilder()
extern "C"  Builder_t2361401461 * PhoneNumber_ToBuilder_m3153659340 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber::CreateBuilderForType()
extern "C"  Builder_t2361401461 * PhoneNumber_CreateBuilderForType_m3300083600 (PhoneNumber_t814071929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber::CreateBuilder(PhoneNumbers.PhoneNumber)
extern "C"  Builder_t2361401461 * PhoneNumber_CreateBuilder_m2219870928 (Il2CppObject * __this /* static, unused */, PhoneNumber_t814071929 * ___prototype0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
