﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoPointsBribeButton
struct NoPointsBribeButton_t319339654;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NoPointsBribeButton::.ctor()
extern "C"  void NoPointsBribeButton__ctor_m4228672505 (NoPointsBribeButton_t319339654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsBribeButton::Awake()
extern "C"  void NoPointsBribeButton_Awake_m4014559186 (NoPointsBribeButton_t319339654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsBribeButton::OnButtonClick()
extern "C"  void NoPointsBribeButton_OnButtonClick_m671291710 (NoPointsBribeButton_t319339654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsBribeButton::DisplayMissileDefendedToast()
extern "C"  void NoPointsBribeButton_DisplayMissileDefendedToast_m3120819551 (NoPointsBribeButton_t319339654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsBribeButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void NoPointsBribeButton_U3COnButtonClickU3Em__0_m2759837134 (NoPointsBribeButton_t319339654 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
