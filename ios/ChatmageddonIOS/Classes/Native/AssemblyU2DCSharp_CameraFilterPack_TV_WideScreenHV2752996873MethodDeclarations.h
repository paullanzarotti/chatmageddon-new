﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_WideScreenHV
struct CameraFilterPack_TV_WideScreenHV_t2752996873;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_WideScreenHV::.ctor()
extern "C"  void CameraFilterPack_TV_WideScreenHV__ctor_m755350528 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenHV::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_WideScreenHV_get_material_m3282269493 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::Start()
extern "C"  void CameraFilterPack_TV_WideScreenHV_Start_m46647432 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_WideScreenHV_OnRenderImage_m3654660280 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::OnValidate()
extern "C"  void CameraFilterPack_TV_WideScreenHV_OnValidate_m497077847 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::Update()
extern "C"  void CameraFilterPack_TV_WideScreenHV_Update_m2407504337 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::OnDisable()
extern "C"  void CameraFilterPack_TV_WideScreenHV_OnDisable_m1292336133 (CameraFilterPack_TV_WideScreenHV_t2752996873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
