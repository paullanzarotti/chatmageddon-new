﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<InitSceneManager>::.ctor()
#define MonoSingleton_1__ctor_m2248392659(__this, method) ((  void (*) (MonoSingleton_1_t1441610665 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<InitSceneManager>::Awake()
#define MonoSingleton_1_Awake_m3131381818(__this, method) ((  void (*) (MonoSingleton_1_t1441610665 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<InitSceneManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3980252488(__this /* static, unused */, method) ((  InitSceneManager_t1690944945 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<InitSceneManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m470481176(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<InitSceneManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2471301173(__this, method) ((  void (*) (MonoSingleton_1_t1441610665 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<InitSceneManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m362009645(__this, method) ((  void (*) (MonoSingleton_1_t1441610665 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<InitSceneManager>::.cctor()
#define MonoSingleton_1__cctor_m1083712216(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
