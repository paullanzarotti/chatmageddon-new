﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tests.FakeBillingService
struct FakeBillingService_t881210075;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Tests.FakeBillingService::.ctor(Unibill.Impl.ProductIdRemapper)
extern "C"  void FakeBillingService__ctor_m3822182300 (FakeBillingService_t881210075 * __this, ProductIdRemapper_t3313438456 * ___remapper0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tests.FakeBillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void FakeBillingService_initialise_m2252162671 (FakeBillingService_t881210075 * __this, Il2CppObject * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tests.FakeBillingService::purchase(System.String,System.String)
extern "C"  void FakeBillingService_purchase_m2874333246 (FakeBillingService_t881210075 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tests.FakeBillingService::restoreTransactions()
extern "C"  void FakeBillingService_restoreTransactions_m4163757748 (FakeBillingService_t881210075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tests.FakeBillingService::hasReceipt(System.String)
extern "C"  bool FakeBillingService_hasReceipt_m4219842201 (FakeBillingService_t881210075 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Tests.FakeBillingService::getReceipt(System.String)
extern "C"  String_t* FakeBillingService_getReceipt_m1343295572 (FakeBillingService_t881210075 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
