﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FireGuage
struct  FireGuage_t4001636259  : public MonoBehaviour_t1158329972
{
public:
	// UIPanel FireGuage::firePanel
	UIPanel_t1795085332 * ___firePanel_2;
	// UITexture FireGuage::fireBackground
	UITexture_t2537039969 * ___fireBackground_3;
	// UnityEngine.Coroutine FireGuage::fireRoutine
	Coroutine_t2299508840 * ___fireRoutine_4;
	// System.Single FireGuage::guageSpeed
	float ___guageSpeed_5;
	// System.Boolean FireGuage::forward
	bool ___forward_6;

public:
	inline static int32_t get_offset_of_firePanel_2() { return static_cast<int32_t>(offsetof(FireGuage_t4001636259, ___firePanel_2)); }
	inline UIPanel_t1795085332 * get_firePanel_2() const { return ___firePanel_2; }
	inline UIPanel_t1795085332 ** get_address_of_firePanel_2() { return &___firePanel_2; }
	inline void set_firePanel_2(UIPanel_t1795085332 * value)
	{
		___firePanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___firePanel_2, value);
	}

	inline static int32_t get_offset_of_fireBackground_3() { return static_cast<int32_t>(offsetof(FireGuage_t4001636259, ___fireBackground_3)); }
	inline UITexture_t2537039969 * get_fireBackground_3() const { return ___fireBackground_3; }
	inline UITexture_t2537039969 ** get_address_of_fireBackground_3() { return &___fireBackground_3; }
	inline void set_fireBackground_3(UITexture_t2537039969 * value)
	{
		___fireBackground_3 = value;
		Il2CppCodeGenWriteBarrier(&___fireBackground_3, value);
	}

	inline static int32_t get_offset_of_fireRoutine_4() { return static_cast<int32_t>(offsetof(FireGuage_t4001636259, ___fireRoutine_4)); }
	inline Coroutine_t2299508840 * get_fireRoutine_4() const { return ___fireRoutine_4; }
	inline Coroutine_t2299508840 ** get_address_of_fireRoutine_4() { return &___fireRoutine_4; }
	inline void set_fireRoutine_4(Coroutine_t2299508840 * value)
	{
		___fireRoutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___fireRoutine_4, value);
	}

	inline static int32_t get_offset_of_guageSpeed_5() { return static_cast<int32_t>(offsetof(FireGuage_t4001636259, ___guageSpeed_5)); }
	inline float get_guageSpeed_5() const { return ___guageSpeed_5; }
	inline float* get_address_of_guageSpeed_5() { return &___guageSpeed_5; }
	inline void set_guageSpeed_5(float value)
	{
		___guageSpeed_5 = value;
	}

	inline static int32_t get_offset_of_forward_6() { return static_cast<int32_t>(offsetof(FireGuage_t4001636259, ___forward_6)); }
	inline bool get_forward_6() const { return ___forward_6; }
	inline bool* get_address_of_forward_6() { return &___forward_6; }
	inline void set_forward_6(bool value)
	{
		___forward_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
