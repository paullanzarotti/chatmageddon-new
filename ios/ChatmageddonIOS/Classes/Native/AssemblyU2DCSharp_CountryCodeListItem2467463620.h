﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CountryCodeItemUIScaler
struct CountryCodeItemUIScaler_t2043820862;
// CountryCodeILP
struct CountryCodeILP_t3216166666;
// CountryCodeData
struct CountryCodeData_t1765037603;
// UILabel
struct UILabel_t1795115428;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3243002984.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountryCodeListItem
struct  CountryCodeListItem_t2467463620  : public BaseInfiniteListItem_1_t3243002984
{
public:
	// CountryCodeItemUIScaler CountryCodeListItem::uiScaler
	CountryCodeItemUIScaler_t2043820862 * ___uiScaler_10;
	// CountryCodeILP CountryCodeListItem::listPopulator
	CountryCodeILP_t3216166666 * ___listPopulator_11;
	// CountryCodeData CountryCodeListItem::data
	CountryCodeData_t1765037603 * ___data_12;
	// UILabel CountryCodeListItem::codeLabel
	UILabel_t1795115428 * ___codeLabel_13;
	// UIScrollView CountryCodeListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_14;
	// UIScrollView CountryCodeListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_15;
	// UnityEngine.Transform CountryCodeListItem::mTrans
	Transform_t3275118058 * ___mTrans_16;
	// UIScrollView CountryCodeListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_17;
	// System.Boolean CountryCodeListItem::mAutoFind
	bool ___mAutoFind_18;
	// System.Boolean CountryCodeListItem::mStarted
	bool ___mStarted_19;

public:
	inline static int32_t get_offset_of_uiScaler_10() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___uiScaler_10)); }
	inline CountryCodeItemUIScaler_t2043820862 * get_uiScaler_10() const { return ___uiScaler_10; }
	inline CountryCodeItemUIScaler_t2043820862 ** get_address_of_uiScaler_10() { return &___uiScaler_10; }
	inline void set_uiScaler_10(CountryCodeItemUIScaler_t2043820862 * value)
	{
		___uiScaler_10 = value;
		Il2CppCodeGenWriteBarrier(&___uiScaler_10, value);
	}

	inline static int32_t get_offset_of_listPopulator_11() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___listPopulator_11)); }
	inline CountryCodeILP_t3216166666 * get_listPopulator_11() const { return ___listPopulator_11; }
	inline CountryCodeILP_t3216166666 ** get_address_of_listPopulator_11() { return &___listPopulator_11; }
	inline void set_listPopulator_11(CountryCodeILP_t3216166666 * value)
	{
		___listPopulator_11 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_11, value);
	}

	inline static int32_t get_offset_of_data_12() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___data_12)); }
	inline CountryCodeData_t1765037603 * get_data_12() const { return ___data_12; }
	inline CountryCodeData_t1765037603 ** get_address_of_data_12() { return &___data_12; }
	inline void set_data_12(CountryCodeData_t1765037603 * value)
	{
		___data_12 = value;
		Il2CppCodeGenWriteBarrier(&___data_12, value);
	}

	inline static int32_t get_offset_of_codeLabel_13() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___codeLabel_13)); }
	inline UILabel_t1795115428 * get_codeLabel_13() const { return ___codeLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_codeLabel_13() { return &___codeLabel_13; }
	inline void set_codeLabel_13(UILabel_t1795115428 * value)
	{
		___codeLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___codeLabel_13, value);
	}

	inline static int32_t get_offset_of_scrollView_14() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___scrollView_14)); }
	inline UIScrollView_t3033954930 * get_scrollView_14() const { return ___scrollView_14; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_14() { return &___scrollView_14; }
	inline void set_scrollView_14(UIScrollView_t3033954930 * value)
	{
		___scrollView_14 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_14, value);
	}

	inline static int32_t get_offset_of_draggablePanel_15() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___draggablePanel_15)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_15() const { return ___draggablePanel_15; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_15() { return &___draggablePanel_15; }
	inline void set_draggablePanel_15(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_15 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_15, value);
	}

	inline static int32_t get_offset_of_mTrans_16() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___mTrans_16)); }
	inline Transform_t3275118058 * get_mTrans_16() const { return ___mTrans_16; }
	inline Transform_t3275118058 ** get_address_of_mTrans_16() { return &___mTrans_16; }
	inline void set_mTrans_16(Transform_t3275118058 * value)
	{
		___mTrans_16 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_16, value);
	}

	inline static int32_t get_offset_of_mScroll_17() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___mScroll_17)); }
	inline UIScrollView_t3033954930 * get_mScroll_17() const { return ___mScroll_17; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_17() { return &___mScroll_17; }
	inline void set_mScroll_17(UIScrollView_t3033954930 * value)
	{
		___mScroll_17 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_17, value);
	}

	inline static int32_t get_offset_of_mAutoFind_18() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___mAutoFind_18)); }
	inline bool get_mAutoFind_18() const { return ___mAutoFind_18; }
	inline bool* get_address_of_mAutoFind_18() { return &___mAutoFind_18; }
	inline void set_mAutoFind_18(bool value)
	{
		___mAutoFind_18 = value;
	}

	inline static int32_t get_offset_of_mStarted_19() { return static_cast<int32_t>(offsetof(CountryCodeListItem_t2467463620, ___mStarted_19)); }
	inline bool get_mStarted_19() const { return ___mStarted_19; }
	inline bool* get_address_of_mStarted_19() { return &___mStarted_19; }
	inline void set_mStarted_19(bool value)
	{
		___mStarted_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
