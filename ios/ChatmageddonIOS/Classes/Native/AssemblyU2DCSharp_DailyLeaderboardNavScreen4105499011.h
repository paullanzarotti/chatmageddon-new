﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// LeaderboardILP
struct LeaderboardILP_t694823370;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DailyLeaderboardNavScreen
struct  DailyLeaderboardNavScreen_t4105499011  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject DailyLeaderboardNavScreen::hasResultsUI
	GameObject_t1756533147 * ___hasResultsUI_3;
	// UnityEngine.GameObject DailyLeaderboardNavScreen::hasNoResultsUI
	GameObject_t1756533147 * ___hasNoResultsUI_4;
	// LeaderboardILP DailyLeaderboardNavScreen::leaderboardList
	LeaderboardILP_t694823370 * ___leaderboardList_5;
	// System.Boolean DailyLeaderboardNavScreen::uiClosing
	bool ___uiClosing_6;

public:
	inline static int32_t get_offset_of_hasResultsUI_3() { return static_cast<int32_t>(offsetof(DailyLeaderboardNavScreen_t4105499011, ___hasResultsUI_3)); }
	inline GameObject_t1756533147 * get_hasResultsUI_3() const { return ___hasResultsUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasResultsUI_3() { return &___hasResultsUI_3; }
	inline void set_hasResultsUI_3(GameObject_t1756533147 * value)
	{
		___hasResultsUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasResultsUI_3, value);
	}

	inline static int32_t get_offset_of_hasNoResultsUI_4() { return static_cast<int32_t>(offsetof(DailyLeaderboardNavScreen_t4105499011, ___hasNoResultsUI_4)); }
	inline GameObject_t1756533147 * get_hasNoResultsUI_4() const { return ___hasNoResultsUI_4; }
	inline GameObject_t1756533147 ** get_address_of_hasNoResultsUI_4() { return &___hasNoResultsUI_4; }
	inline void set_hasNoResultsUI_4(GameObject_t1756533147 * value)
	{
		___hasNoResultsUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___hasNoResultsUI_4, value);
	}

	inline static int32_t get_offset_of_leaderboardList_5() { return static_cast<int32_t>(offsetof(DailyLeaderboardNavScreen_t4105499011, ___leaderboardList_5)); }
	inline LeaderboardILP_t694823370 * get_leaderboardList_5() const { return ___leaderboardList_5; }
	inline LeaderboardILP_t694823370 ** get_address_of_leaderboardList_5() { return &___leaderboardList_5; }
	inline void set_leaderboardList_5(LeaderboardILP_t694823370 * value)
	{
		___leaderboardList_5 = value;
		Il2CppCodeGenWriteBarrier(&___leaderboardList_5, value);
	}

	inline static int32_t get_offset_of_uiClosing_6() { return static_cast<int32_t>(offsetof(DailyLeaderboardNavScreen_t4105499011, ___uiClosing_6)); }
	inline bool get_uiClosing_6() const { return ___uiClosing_6; }
	inline bool* get_address_of_uiClosing_6() { return &___uiClosing_6; }
	inline void set_uiClosing_6(bool value)
	{
		___uiClosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
