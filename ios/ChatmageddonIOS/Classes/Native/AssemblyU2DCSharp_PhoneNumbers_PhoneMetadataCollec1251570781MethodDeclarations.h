﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneMetadataCollection/Builder
struct Builder_t1251570781;
// PhoneNumbers.PhoneMetadataCollection
struct PhoneMetadataCollection_t4114095021;
// System.Collections.Generic.IList`1<PhoneNumbers.PhoneMetadata>
struct IList_1_t907802004;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// PhoneNumbers.PhoneMetadata/Builder
struct Builder_t1569388123;
// System.Collections.Generic.IEnumerable`1<PhoneNumbers.PhoneMetadata>
struct IEnumerable_1_t658988448;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadataCollec4114095021.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata366861403.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata_Build1569388123.h"

// System.Void PhoneNumbers.PhoneMetadataCollection/Builder::.ctor()
extern "C"  void Builder__ctor_m3375527000 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::get_ThisBuilder()
extern "C"  Builder_t1251570781 * Builder_get_ThisBuilder_m814557624 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection/Builder::get_MessageBeingBuilt()
extern "C"  PhoneMetadataCollection_t4114095021 * Builder_get_MessageBeingBuilt_m3899324425 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::Clear()
extern "C"  Builder_t1251570781 * Builder_Clear_m3953569829 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::Clone()
extern "C"  Builder_t1251570781 * Builder_Clone_m852576091 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection/Builder::get_DefaultInstanceForType()
extern "C"  PhoneMetadataCollection_t4114095021 * Builder_get_DefaultInstanceForType_m3334871874 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection/Builder::Build()
extern "C"  PhoneMetadataCollection_t4114095021 * Builder_Build_m1507226880 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection/Builder::BuildPartial()
extern "C"  PhoneMetadataCollection_t4114095021 * Builder_BuildPartial_m2115384387 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::MergeFrom(PhoneNumbers.PhoneMetadataCollection)
extern "C"  Builder_t1251570781 * Builder_MergeFrom_m4174120703 (Builder_t1251570781 * __this, PhoneMetadataCollection_t4114095021 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<PhoneNumbers.PhoneMetadata> PhoneNumbers.PhoneMetadataCollection/Builder::get_MetadataList()
extern "C"  Il2CppObject* Builder_get_MetadataList_m3966935445 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadataCollection/Builder::get_MetadataCount()
extern "C"  int32_t Builder_get_MetadataCount_m2762801665 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadataCollection/Builder::GetMetadata(System.Int32)
extern "C"  PhoneMetadata_t366861403 * Builder_GetMetadata_m3976960884 (Builder_t1251570781 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::SetMetadata(System.Int32,PhoneNumbers.PhoneMetadata)
extern "C"  Builder_t1251570781 * Builder_SetMetadata_m1734987081 (Builder_t1251570781 * __this, int32_t ___index0, PhoneMetadata_t366861403 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::SetMetadata(System.Int32,PhoneNumbers.PhoneMetadata/Builder)
extern "C"  Builder_t1251570781 * Builder_SetMetadata_m3217733427 (Builder_t1251570781 * __this, int32_t ___index0, Builder_t1569388123 * ___builderForValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::AddMetadata(PhoneNumbers.PhoneMetadata)
extern "C"  Builder_t1251570781 * Builder_AddMetadata_m2992433695 (Builder_t1251570781 * __this, PhoneMetadata_t366861403 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::AddMetadata(PhoneNumbers.PhoneMetadata/Builder)
extern "C"  Builder_t1251570781 * Builder_AddMetadata_m781612013 (Builder_t1251570781 * __this, Builder_t1569388123 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::AddRangeMetadata(System.Collections.Generic.IEnumerable`1<PhoneNumbers.PhoneMetadata>)
extern "C"  Builder_t1251570781 * Builder_AddRangeMetadata_m3954527699 (Builder_t1251570781 * __this, Il2CppObject* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection/Builder::ClearMetadata()
extern "C"  Builder_t1251570781 * Builder_ClearMetadata_m4236290592 (Builder_t1251570781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
