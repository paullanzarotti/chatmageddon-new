﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<ProfileNavScreen>
struct List_1_t495778986;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato30508660.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3969584089_gshared (Enumerator_t30508660 * __this, List_1_t495778986 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3969584089(__this, ___l0, method) ((  void (*) (Enumerator_t30508660 *, List_1_t495778986 *, const MethodInfo*))Enumerator__ctor_m3969584089_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1647191497_gshared (Enumerator_t30508660 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1647191497(__this, method) ((  void (*) (Enumerator_t30508660 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1647191497_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3008140053_gshared (Enumerator_t30508660 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3008140053(__this, method) ((  Il2CppObject * (*) (Enumerator_t30508660 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3008140053_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m2142852498_gshared (Enumerator_t30508660 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2142852498(__this, method) ((  void (*) (Enumerator_t30508660 *, const MethodInfo*))Enumerator_Dispose_m2142852498_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3492220347_gshared (Enumerator_t30508660 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3492220347(__this, method) ((  void (*) (Enumerator_t30508660 *, const MethodInfo*))Enumerator_VerifyState_m3492220347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4016900265_gshared (Enumerator_t30508660 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4016900265(__this, method) ((  bool (*) (Enumerator_t30508660 *, const MethodInfo*))Enumerator_MoveNext_m4016900265_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ProfileNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2195376410_gshared (Enumerator_t30508660 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2195376410(__this, method) ((  int32_t (*) (Enumerator_t30508660 *, const MethodInfo*))Enumerator_get_Current_m2195376410_gshared)(__this, method)
