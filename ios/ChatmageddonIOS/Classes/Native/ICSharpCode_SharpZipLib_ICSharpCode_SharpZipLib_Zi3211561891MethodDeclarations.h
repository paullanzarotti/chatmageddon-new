﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t3211561891;
// System.IO.Stream
struct Stream_t3255436806;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t3958302971;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3958302971.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern "C"  void InflaterInputBuffer__ctor_m1983366716 (InflaterInputBuffer_t3211561891 * __this, Stream_t3255436806 * ___stream0, int32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern "C"  int32_t InflaterInputBuffer_get_RawLength_m1091203859 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern "C"  int32_t InflaterInputBuffer_get_Available_m2874517854 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern "C"  void InflaterInputBuffer_set_Available_m4062003677 (InflaterInputBuffer_t3211561891 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern "C"  void InflaterInputBuffer_SetInflaterInput_m1611710434 (InflaterInputBuffer_t3211561891 * __this, Inflater_t3958302971 * ___inflater0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern "C"  void InflaterInputBuffer_Fill_m1947457127 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[])
extern "C"  int32_t InflaterInputBuffer_ReadRawBuffer_m688284419 (InflaterInputBuffer_t3211561891 * __this, ByteU5BU5D_t3397334013* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t InflaterInputBuffer_ReadRawBuffer_m721002543 (InflaterInputBuffer_t3211561891 * __this, ByteU5BU5D_t3397334013* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t InflaterInputBuffer_ReadClearTextBuffer_m370674671 (InflaterInputBuffer_t3211561891 * __this, ByteU5BU5D_t3397334013* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern "C"  int32_t InflaterInputBuffer_ReadLeByte_m1338146083 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeShort()
extern "C"  int32_t InflaterInputBuffer_ReadLeShort_m3204983647 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeInt()
extern "C"  int32_t InflaterInputBuffer_ReadLeInt_m3478398094 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeLong()
extern "C"  int64_t InflaterInputBuffer_ReadLeLong_m1849502234 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_CryptoTransform(System.Security.Cryptography.ICryptoTransform)
extern "C"  void InflaterInputBuffer_set_CryptoTransform_m4126126743 (InflaterInputBuffer_t3211561891 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
