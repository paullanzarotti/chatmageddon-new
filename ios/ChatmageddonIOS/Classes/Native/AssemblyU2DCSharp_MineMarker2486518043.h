﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mine
struct Mine_t2729441277;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "AssemblyU2DCSharp_RadarMarkerBlip1130528275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineMarker
struct  MineMarker_t2486518043  : public RadarMarkerBlip_t1130528275
{
public:
	// Mine MineMarker::mine
	Mine_t2729441277 * ___mine_10;
	// TweenAlpha MineMarker::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_11;

public:
	inline static int32_t get_offset_of_mine_10() { return static_cast<int32_t>(offsetof(MineMarker_t2486518043, ___mine_10)); }
	inline Mine_t2729441277 * get_mine_10() const { return ___mine_10; }
	inline Mine_t2729441277 ** get_address_of_mine_10() { return &___mine_10; }
	inline void set_mine_10(Mine_t2729441277 * value)
	{
		___mine_10 = value;
		Il2CppCodeGenWriteBarrier(&___mine_10, value);
	}

	inline static int32_t get_offset_of_alphaTween_11() { return static_cast<int32_t>(offsetof(MineMarker_t2486518043, ___alphaTween_11)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_11() const { return ___alphaTween_11; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_11() { return &___alphaTween_11; }
	inline void set_alphaTween_11(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_11 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
