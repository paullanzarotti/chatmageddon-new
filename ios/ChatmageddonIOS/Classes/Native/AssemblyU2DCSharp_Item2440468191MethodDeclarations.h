﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Item
struct Item_t2440468191;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Item::.ctor(System.Collections.Hashtable)
extern "C"  void Item__ctor_m321255126 (Item_t2440468191 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Item::.ctor(Item)
extern "C"  void Item__ctor_m2522202507 (Item_t2440468191 * __this, Item_t2440468191 * ___existingItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Item::GetboolFromString(System.String)
extern "C"  bool Item_GetboolFromString_m229735667 (Item_t2440468191 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
