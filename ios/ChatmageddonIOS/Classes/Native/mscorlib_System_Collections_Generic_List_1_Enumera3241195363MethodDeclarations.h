﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<ChatNavScreen>
struct List_1_t3706465689;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3241195363.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1512314214_gshared (Enumerator_t3241195363 * __this, List_1_t3706465689 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1512314214(__this, ___l0, method) ((  void (*) (Enumerator_t3241195363 *, List_1_t3706465689 *, const MethodInfo*))Enumerator__ctor_m1512314214_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2401949780_gshared (Enumerator_t3241195363 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2401949780(__this, method) ((  void (*) (Enumerator_t3241195363 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2401949780_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2850476396_gshared (Enumerator_t3241195363 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2850476396(__this, method) ((  Il2CppObject * (*) (Enumerator_t3241195363 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2850476396_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m2394870137_gshared (Enumerator_t3241195363 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2394870137(__this, method) ((  void (*) (Enumerator_t3241195363 *, const MethodInfo*))Enumerator_Dispose_m2394870137_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1401662928_gshared (Enumerator_t3241195363 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1401662928(__this, method) ((  void (*) (Enumerator_t3241195363 *, const MethodInfo*))Enumerator_VerifyState_m1401662928_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m158502792_gshared (Enumerator_t3241195363 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m158502792(__this, method) ((  bool (*) (Enumerator_t3241195363 *, const MethodInfo*))Enumerator_MoveNext_m158502792_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ChatNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1361844849_gshared (Enumerator_t3241195363 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1361844849(__this, method) ((  int32_t (*) (Enumerator_t3241195363 *, const MethodInfo*))Enumerator_get_Current_m1361844849_gshared)(__this, method)
