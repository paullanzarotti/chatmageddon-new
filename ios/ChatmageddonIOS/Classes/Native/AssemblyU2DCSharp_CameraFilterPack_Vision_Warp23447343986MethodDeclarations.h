﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Vision_Warp2
struct CameraFilterPack_Vision_Warp2_t3447343986;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Vision_Warp2::.ctor()
extern "C"  void CameraFilterPack_Vision_Warp2__ctor_m112342821 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Warp2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Vision_Warp2_get_material_m1795850074 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::Start()
extern "C"  void CameraFilterPack_Vision_Warp2_Start_m1088840333 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Vision_Warp2_OnRenderImage_m366777317 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::OnValidate()
extern "C"  void CameraFilterPack_Vision_Warp2_OnValidate_m2064802278 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::Update()
extern "C"  void CameraFilterPack_Vision_Warp2_Update_m594592944 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::OnDisable()
extern "C"  void CameraFilterPack_Vision_Warp2_OnDisable_m3028469750 (CameraFilterPack_Vision_Warp2_t3447343986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
