﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StatusItemSwipe
struct StatusItemSwipe_t1669089155;

#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusSwipeComponent
struct  StatusSwipeComponent_t1744404785  : public NGUISwipe_t4135300327
{
public:
	// StatusItemSwipe StatusSwipeComponent::statusSwipe
	StatusItemSwipe_t1669089155 * ___statusSwipe_10;

public:
	inline static int32_t get_offset_of_statusSwipe_10() { return static_cast<int32_t>(offsetof(StatusSwipeComponent_t1744404785, ___statusSwipe_10)); }
	inline StatusItemSwipe_t1669089155 * get_statusSwipe_10() const { return ___statusSwipe_10; }
	inline StatusItemSwipe_t1669089155 ** get_address_of_statusSwipe_10() { return &___statusSwipe_10; }
	inline void set_statusSwipe_10(StatusItemSwipe_t1669089155 * value)
	{
		___statusSwipe_10 = value;
		Il2CppCodeGenWriteBarrier(&___statusSwipe_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
