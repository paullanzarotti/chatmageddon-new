﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform>
struct List_1_t1238706099;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t3046128587;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityUtil
struct  UnityUtil_t671748753  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Queue`1<System.Action> UnityUtil::mainThreadTasks
	Queue_1_t3046128587 * ___mainThreadTasks_3;

public:
	inline static int32_t get_offset_of_mainThreadTasks_3() { return static_cast<int32_t>(offsetof(UnityUtil_t671748753, ___mainThreadTasks_3)); }
	inline Queue_1_t3046128587 * get_mainThreadTasks_3() const { return ___mainThreadTasks_3; }
	inline Queue_1_t3046128587 ** get_address_of_mainThreadTasks_3() { return &___mainThreadTasks_3; }
	inline void set_mainThreadTasks_3(Queue_1_t3046128587 * value)
	{
		___mainThreadTasks_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainThreadTasks_3, value);
	}
};

struct UnityUtil_t671748753_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform> UnityUtil::PCControlledPlatforms
	List_1_t1238706099 * ___PCControlledPlatforms_2;

public:
	inline static int32_t get_offset_of_PCControlledPlatforms_2() { return static_cast<int32_t>(offsetof(UnityUtil_t671748753_StaticFields, ___PCControlledPlatforms_2)); }
	inline List_1_t1238706099 * get_PCControlledPlatforms_2() const { return ___PCControlledPlatforms_2; }
	inline List_1_t1238706099 ** get_address_of_PCControlledPlatforms_2() { return &___PCControlledPlatforms_2; }
	inline void set_PCControlledPlatforms_2(List_1_t1238706099 * value)
	{
		___PCControlledPlatforms_2 = value;
		Il2CppCodeGenWriteBarrier(&___PCControlledPlatforms_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
