﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blur_Blurry
struct  CameraFilterPack_Blur_Blurry_t3618975741  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Blur_Blurry::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Blurry::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Blurry::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Blurry::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Blurry::Amount
	float ___Amount_6;
	// System.Int32 CameraFilterPack_Blur_Blurry::FastFilter
	int32_t ___FastFilter_7;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}

	inline static int32_t get_offset_of_FastFilter_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741, ___FastFilter_7)); }
	inline int32_t get_FastFilter_7() const { return ___FastFilter_7; }
	inline int32_t* get_address_of_FastFilter_7() { return &___FastFilter_7; }
	inline void set_FastFilter_7(int32_t value)
	{
		___FastFilter_7 = value;
	}
};

struct CameraFilterPack_Blur_Blurry_t3618975741_StaticFields
{
public:
	// System.Single CameraFilterPack_Blur_Blurry::ChangeAmount
	float ___ChangeAmount_8;
	// System.Int32 CameraFilterPack_Blur_Blurry::ChangeFastFilter
	int32_t ___ChangeFastFilter_9;

public:
	inline static int32_t get_offset_of_ChangeAmount_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741_StaticFields, ___ChangeAmount_8)); }
	inline float get_ChangeAmount_8() const { return ___ChangeAmount_8; }
	inline float* get_address_of_ChangeAmount_8() { return &___ChangeAmount_8; }
	inline void set_ChangeAmount_8(float value)
	{
		___ChangeAmount_8 = value;
	}

	inline static int32_t get_offset_of_ChangeFastFilter_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Blurry_t3618975741_StaticFields, ___ChangeFastFilter_9)); }
	inline int32_t get_ChangeFastFilter_9() const { return ___ChangeFastFilter_9; }
	inline int32_t* get_address_of_ChangeFastFilter_9() { return &___ChangeFastFilter_9; }
	inline void set_ChangeFastFilter_9(int32_t value)
	{
		___ChangeFastFilter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
