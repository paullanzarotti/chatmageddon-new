﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfoRetrievalMethod
struct KeyInfoRetrievalMethod_t1087857306;
// System.String
struct String_t;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.KeyInfoRetrievalMethod::.ctor()
extern "C"  void KeyInfoRetrievalMethod__ctor_m4033506022 (KeyInfoRetrievalMethod_t1087857306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.KeyInfoRetrievalMethod::get_Type()
extern "C"  String_t* KeyInfoRetrievalMethod_get_Type_m271760058 (KeyInfoRetrievalMethod_t1087857306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoRetrievalMethod::set_Type(System.String)
extern "C"  void KeyInfoRetrievalMethod_set_Type_m4030279149 (KeyInfoRetrievalMethod_t1087857306 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfoRetrievalMethod::GetXml()
extern "C"  XmlElement_t2877111883 * KeyInfoRetrievalMethod_GetXml_m1511590373 (KeyInfoRetrievalMethod_t1087857306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoRetrievalMethod::LoadXml(System.Xml.XmlElement)
extern "C"  void KeyInfoRetrievalMethod_LoadXml_m144030998 (KeyInfoRetrievalMethod_t1087857306 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
