﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Answers.Answers
struct Answers_t1822067079;
// Fabric.Answers.Internal.IAnswers
struct IAnswers_t1475899032;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "mscorlib_System_Nullable_1_gen3282734688.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void Fabric.Answers.Answers::.ctor()
extern "C"  void Answers__ctor_m185471320 (Answers_t1822067079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Fabric.Answers.Internal.IAnswers Fabric.Answers.Answers::get_Implementation()
extern "C"  Il2CppObject * Answers_get_Implementation_m1520202015 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogSignUp(System.String,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogSignUp_m3657342775 (Il2CppObject * __this /* static, unused */, String_t* ___method0, Nullable_1_t2088641033  ___success1, Dictionary_2_t309261261 * ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogLogin(System.String,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogLogin_m1476463026 (Il2CppObject * __this /* static, unused */, String_t* ___method0, Nullable_1_t2088641033  ___success1, Dictionary_2_t309261261 * ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogShare(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogShare_m222295484 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, Dictionary_2_t309261261 * ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogInvite(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogInvite_m4104406016 (Il2CppObject * __this /* static, unused */, String_t* ___method0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogLevelStart(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogLevelStart_m3134925233 (Il2CppObject * __this /* static, unused */, String_t* ___level0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogLevelEnd(System.String,System.Nullable`1<System.Double>,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogLevelEnd_m1361644327 (Il2CppObject * __this /* static, unused */, String_t* ___level0, Nullable_1_t2341081996  ___score1, Nullable_1_t2088641033  ___success2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogAddToCart(System.Nullable`1<System.Decimal>,System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogAddToCart_m1614439418 (Il2CppObject * __this /* static, unused */, Nullable_1_t3282734688  ___itemPrice0, String_t* ___currency1, String_t* ___itemName2, String_t* ___itemType3, String_t* ___itemId4, Dictionary_2_t309261261 * ___customAttributes5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogPurchase(System.Nullable`1<System.Decimal>,System.String,System.Nullable`1<System.Boolean>,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogPurchase_m659160213 (Il2CppObject * __this /* static, unused */, Nullable_1_t3282734688  ___price0, String_t* ___currency1, Nullable_1_t2088641033  ___success2, String_t* ___itemName3, String_t* ___itemType4, String_t* ___itemId5, Dictionary_2_t309261261 * ___customAttributes6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogStartCheckout(System.Nullable`1<System.Decimal>,System.String,System.Nullable`1<System.Int32>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogStartCheckout_m2286973802 (Il2CppObject * __this /* static, unused */, Nullable_1_t3282734688  ___totalPrice0, String_t* ___currency1, Nullable_1_t334943763  ___itemCount2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogRating(System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogRating_m85979956 (Il2CppObject * __this /* static, unused */, Nullable_1_t334943763  ___rating0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, Dictionary_2_t309261261 * ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogContentView(System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogContentView_m2774384743 (Il2CppObject * __this /* static, unused */, String_t* ___contentName0, String_t* ___contentType1, String_t* ___contentId2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogSearch(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogSearch_m3923706029 (Il2CppObject * __this /* static, unused */, String_t* ___query0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Answers::LogCustom(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Answers_LogCustom_m1135571534 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
