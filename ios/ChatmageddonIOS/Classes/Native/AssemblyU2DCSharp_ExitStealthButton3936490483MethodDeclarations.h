﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitStealthButton
struct ExitStealthButton_t3936490483;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ExitStealthButton::.ctor()
extern "C"  void ExitStealthButton__ctor_m3280193514 (ExitStealthButton_t3936490483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitStealthButton::OnButtonClick()
extern "C"  void ExitStealthButton_OnButtonClick_m3085910251 (ExitStealthButton_t3936490483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitStealthButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ExitStealthButton_U3COnButtonClickU3Em__0_m1310480637 (ExitStealthButton_t3936490483 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
