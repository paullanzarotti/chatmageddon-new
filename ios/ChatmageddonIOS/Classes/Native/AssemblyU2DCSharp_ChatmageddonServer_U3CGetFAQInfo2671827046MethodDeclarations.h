﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetFAQInfo>c__AnonStorey3A
struct U3CGetFAQInfoU3Ec__AnonStorey3A_t2671827046;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetFAQInfo>c__AnonStorey3A::.ctor()
extern "C"  void U3CGetFAQInfoU3Ec__AnonStorey3A__ctor_m2852435161 (U3CGetFAQInfoU3Ec__AnonStorey3A_t2671827046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetFAQInfo>c__AnonStorey3A::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetFAQInfoU3Ec__AnonStorey3A_U3CU3Em__0_m2128697342 (U3CGetFAQInfoU3Ec__AnonStorey3A_t2671827046 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
