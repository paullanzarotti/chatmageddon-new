﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusListItemUIScaler
struct  StatusListItemUIScaler_t1403897979  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite StatusListItemUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider StatusListItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UnityEngine.Transform StatusListItemUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_16;
	// UnityEngine.Transform StatusListItemUIScaler::nameLabel
	Transform_t3275118058 * ___nameLabel_17;
	// UnityEngine.Transform StatusListItemUIScaler::actionLabel
	Transform_t3275118058 * ___actionLabel_18;
	// UnityEngine.Transform StatusListItemUIScaler::itemGraphic
	Transform_t3275118058 * ___itemGraphic_19;
	// UnityEngine.Transform StatusListItemUIScaler::flameSprite
	Transform_t3275118058 * ___flameSprite_20;
	// UnityEngine.Transform StatusListItemUIScaler::points
	Transform_t3275118058 * ___points_21;
	// UnityEngine.Transform StatusListItemUIScaler::activeShield
	Transform_t3275118058 * ___activeShield_22;
	// UnityEngine.Transform StatusListItemUIScaler::activeWarning
	Transform_t3275118058 * ___activeWarning_23;
	// UnityEngine.Transform StatusListItemUIScaler::activeUserLogo
	Transform_t3275118058 * ___activeUserLogo_24;
	// UnityEngine.Transform StatusListItemUIScaler::pendingInfoLabel
	Transform_t3275118058 * ___pendingInfoLabel_25;
	// UnityEngine.Transform StatusListItemUIScaler::inactiveStateLabel
	Transform_t3275118058 * ___inactiveStateLabel_26;
	// UnityEngine.Transform StatusListItemUIScaler::inactivePointsLabel
	Transform_t3275118058 * ___inactivePointsLabel_27;
	// UnityEngine.Transform StatusListItemUIScaler::finishTime
	Transform_t3275118058 * ___finishTime_28;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_profilePicture_16() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___profilePicture_16)); }
	inline Transform_t3275118058 * get_profilePicture_16() const { return ___profilePicture_16; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_16() { return &___profilePicture_16; }
	inline void set_profilePicture_16(Transform_t3275118058 * value)
	{
		___profilePicture_16 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_16, value);
	}

	inline static int32_t get_offset_of_nameLabel_17() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___nameLabel_17)); }
	inline Transform_t3275118058 * get_nameLabel_17() const { return ___nameLabel_17; }
	inline Transform_t3275118058 ** get_address_of_nameLabel_17() { return &___nameLabel_17; }
	inline void set_nameLabel_17(Transform_t3275118058 * value)
	{
		___nameLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_17, value);
	}

	inline static int32_t get_offset_of_actionLabel_18() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___actionLabel_18)); }
	inline Transform_t3275118058 * get_actionLabel_18() const { return ___actionLabel_18; }
	inline Transform_t3275118058 ** get_address_of_actionLabel_18() { return &___actionLabel_18; }
	inline void set_actionLabel_18(Transform_t3275118058 * value)
	{
		___actionLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_18, value);
	}

	inline static int32_t get_offset_of_itemGraphic_19() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___itemGraphic_19)); }
	inline Transform_t3275118058 * get_itemGraphic_19() const { return ___itemGraphic_19; }
	inline Transform_t3275118058 ** get_address_of_itemGraphic_19() { return &___itemGraphic_19; }
	inline void set_itemGraphic_19(Transform_t3275118058 * value)
	{
		___itemGraphic_19 = value;
		Il2CppCodeGenWriteBarrier(&___itemGraphic_19, value);
	}

	inline static int32_t get_offset_of_flameSprite_20() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___flameSprite_20)); }
	inline Transform_t3275118058 * get_flameSprite_20() const { return ___flameSprite_20; }
	inline Transform_t3275118058 ** get_address_of_flameSprite_20() { return &___flameSprite_20; }
	inline void set_flameSprite_20(Transform_t3275118058 * value)
	{
		___flameSprite_20 = value;
		Il2CppCodeGenWriteBarrier(&___flameSprite_20, value);
	}

	inline static int32_t get_offset_of_points_21() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___points_21)); }
	inline Transform_t3275118058 * get_points_21() const { return ___points_21; }
	inline Transform_t3275118058 ** get_address_of_points_21() { return &___points_21; }
	inline void set_points_21(Transform_t3275118058 * value)
	{
		___points_21 = value;
		Il2CppCodeGenWriteBarrier(&___points_21, value);
	}

	inline static int32_t get_offset_of_activeShield_22() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___activeShield_22)); }
	inline Transform_t3275118058 * get_activeShield_22() const { return ___activeShield_22; }
	inline Transform_t3275118058 ** get_address_of_activeShield_22() { return &___activeShield_22; }
	inline void set_activeShield_22(Transform_t3275118058 * value)
	{
		___activeShield_22 = value;
		Il2CppCodeGenWriteBarrier(&___activeShield_22, value);
	}

	inline static int32_t get_offset_of_activeWarning_23() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___activeWarning_23)); }
	inline Transform_t3275118058 * get_activeWarning_23() const { return ___activeWarning_23; }
	inline Transform_t3275118058 ** get_address_of_activeWarning_23() { return &___activeWarning_23; }
	inline void set_activeWarning_23(Transform_t3275118058 * value)
	{
		___activeWarning_23 = value;
		Il2CppCodeGenWriteBarrier(&___activeWarning_23, value);
	}

	inline static int32_t get_offset_of_activeUserLogo_24() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___activeUserLogo_24)); }
	inline Transform_t3275118058 * get_activeUserLogo_24() const { return ___activeUserLogo_24; }
	inline Transform_t3275118058 ** get_address_of_activeUserLogo_24() { return &___activeUserLogo_24; }
	inline void set_activeUserLogo_24(Transform_t3275118058 * value)
	{
		___activeUserLogo_24 = value;
		Il2CppCodeGenWriteBarrier(&___activeUserLogo_24, value);
	}

	inline static int32_t get_offset_of_pendingInfoLabel_25() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___pendingInfoLabel_25)); }
	inline Transform_t3275118058 * get_pendingInfoLabel_25() const { return ___pendingInfoLabel_25; }
	inline Transform_t3275118058 ** get_address_of_pendingInfoLabel_25() { return &___pendingInfoLabel_25; }
	inline void set_pendingInfoLabel_25(Transform_t3275118058 * value)
	{
		___pendingInfoLabel_25 = value;
		Il2CppCodeGenWriteBarrier(&___pendingInfoLabel_25, value);
	}

	inline static int32_t get_offset_of_inactiveStateLabel_26() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___inactiveStateLabel_26)); }
	inline Transform_t3275118058 * get_inactiveStateLabel_26() const { return ___inactiveStateLabel_26; }
	inline Transform_t3275118058 ** get_address_of_inactiveStateLabel_26() { return &___inactiveStateLabel_26; }
	inline void set_inactiveStateLabel_26(Transform_t3275118058 * value)
	{
		___inactiveStateLabel_26 = value;
		Il2CppCodeGenWriteBarrier(&___inactiveStateLabel_26, value);
	}

	inline static int32_t get_offset_of_inactivePointsLabel_27() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___inactivePointsLabel_27)); }
	inline Transform_t3275118058 * get_inactivePointsLabel_27() const { return ___inactivePointsLabel_27; }
	inline Transform_t3275118058 ** get_address_of_inactivePointsLabel_27() { return &___inactivePointsLabel_27; }
	inline void set_inactivePointsLabel_27(Transform_t3275118058 * value)
	{
		___inactivePointsLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___inactivePointsLabel_27, value);
	}

	inline static int32_t get_offset_of_finishTime_28() { return static_cast<int32_t>(offsetof(StatusListItemUIScaler_t1403897979, ___finishTime_28)); }
	inline Transform_t3275118058 * get_finishTime_28() const { return ___finishTime_28; }
	inline Transform_t3275118058 ** get_address_of_finishTime_28() { return &___finishTime_28; }
	inline void set_finishTime_28(Transform_t3275118058 * value)
	{
		___finishTime_28 = value;
		Il2CppCodeGenWriteBarrier(&___finishTime_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
