﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_EyesVision_2
struct CameraFilterPack_EyesVision_2_t1036579270;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_EyesVision_2::.ctor()
extern "C"  void CameraFilterPack_EyesVision_2__ctor_m2802944471 (CameraFilterPack_EyesVision_2_t1036579270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_EyesVision_2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_EyesVision_2_get_material_m3827810022 (CameraFilterPack_EyesVision_2_t1036579270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::Start()
extern "C"  void CameraFilterPack_EyesVision_2_Start_m1503965707 (CameraFilterPack_EyesVision_2_t1036579270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_EyesVision_2_OnRenderImage_m312062555 (CameraFilterPack_EyesVision_2_t1036579270 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::Update()
extern "C"  void CameraFilterPack_EyesVision_2_Update_m1935629396 (CameraFilterPack_EyesVision_2_t1036579270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::OnDisable()
extern "C"  void CameraFilterPack_EyesVision_2_OnDisable_m2017119410 (CameraFilterPack_EyesVision_2_t1036579270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
