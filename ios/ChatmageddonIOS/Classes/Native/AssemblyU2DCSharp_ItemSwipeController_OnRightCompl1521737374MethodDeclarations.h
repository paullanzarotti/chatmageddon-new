﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemSwipeController/OnRightComplete
struct OnRightComplete_t1521737374;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ItemSwipeController/OnRightComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRightComplete__ctor_m4003233325 (OnRightComplete_t1521737374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController/OnRightComplete::Invoke()
extern "C"  void OnRightComplete_Invoke_m3171755397 (OnRightComplete_t1521737374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ItemSwipeController/OnRightComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnRightComplete_BeginInvoke_m36760284 (OnRightComplete_t1521737374 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController/OnRightComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnRightComplete_EndInvoke_m4177963167 (OnRightComplete_t1521737374 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
