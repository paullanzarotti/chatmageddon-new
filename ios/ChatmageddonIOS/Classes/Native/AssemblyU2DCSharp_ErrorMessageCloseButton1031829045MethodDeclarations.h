﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessageCloseButton
struct ErrorMessageCloseButton_t1031829045;

#include "codegen/il2cpp-codegen.h"

// System.Void ErrorMessageCloseButton::.ctor()
extern "C"  void ErrorMessageCloseButton__ctor_m247404970 (ErrorMessageCloseButton_t1031829045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessageCloseButton::OnButtonClick()
extern "C"  void ErrorMessageCloseButton_OnButtonClick_m4111492141 (ErrorMessageCloseButton_t1031829045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
