﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.PositiveTimeSpanValidatorAttribute
struct PositiveTimeSpanValidatorAttribute_t2571126326;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Configuration.PositiveTimeSpanValidatorAttribute::.ctor()
extern "C"  void PositiveTimeSpanValidatorAttribute__ctor_m3440896745 (PositiveTimeSpanValidatorAttribute_t2571126326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.PositiveTimeSpanValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * PositiveTimeSpanValidatorAttribute_get_ValidatorInstance_m1648065176 (PositiveTimeSpanValidatorAttribute_t2571126326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
