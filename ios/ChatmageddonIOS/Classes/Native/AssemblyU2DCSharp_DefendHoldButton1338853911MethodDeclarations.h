﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendHoldButton
struct DefendHoldButton_t1338853911;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void DefendHoldButton::.ctor()
extern "C"  void DefendHoldButton__ctor_m3733310844 (DefendHoldButton_t1338853911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton::ResetHold()
extern "C"  void DefendHoldButton_ResetHold_m908946118 (DefendHoldButton_t1338853911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton::OnButtonClick()
extern "C"  void DefendHoldButton_OnButtonClick_m167914191 (DefendHoldButton_t1338853911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton::OnPress(System.Boolean)
extern "C"  void DefendHoldButton_OnPress_m2721727611 (DefendHoldButton_t1338853911 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DefendHoldButton::UpdateHoldProgress()
extern "C"  Il2CppObject * DefendHoldButton_UpdateHoldProgress_m4173549027 (DefendHoldButton_t1338853911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton::UpdateHoldBar(System.Single)
extern "C"  void DefendHoldButton_UpdateHoldBar_m1865766574 (DefendHoldButton_t1338853911 * __this, float ___totalTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton::DefendMissile()
extern "C"  void DefendHoldButton_DefendMissile_m1926961068 (DefendHoldButton_t1338853911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
