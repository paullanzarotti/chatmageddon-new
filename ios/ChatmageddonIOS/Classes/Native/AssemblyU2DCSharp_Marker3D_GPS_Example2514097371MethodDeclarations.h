﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Marker3D_GPS_Example
struct Marker3D_GPS_Example_t2514097371;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Marker3D_GPS_Example::.ctor()
extern "C"  void Marker3D_GPS_Example__ctor_m4014530574 (Marker3D_GPS_Example_t2514097371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker3D_GPS_Example::Start()
extern "C"  void Marker3D_GPS_Example_Start_m119483674 (Marker3D_GPS_Example_t2514097371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker3D_GPS_Example::OnChangeZoom()
extern "C"  void Marker3D_GPS_Example_OnChangeZoom_m3605035466 (Marker3D_GPS_Example_t2514097371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker3D_GPS_Example::OnCompassChanged(System.Single)
extern "C"  void Marker3D_GPS_Example_OnCompassChanged_m1799928010 (Marker3D_GPS_Example_t2514097371 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker3D_GPS_Example::OnLocationChanged(UnityEngine.Vector2)
extern "C"  void Marker3D_GPS_Example_OnLocationChanged_m1306681732 (Marker3D_GPS_Example_t2514097371 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
