﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CenterOnMapButton
struct CenterOnMapButton_t803353738;

#include "codegen/il2cpp-codegen.h"

// System.Void CenterOnMapButton::.ctor()
extern "C"  void CenterOnMapButton__ctor_m3298737259 (CenterOnMapButton_t803353738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CenterOnMapButton::OnButtonClick()
extern "C"  void CenterOnMapButton_OnButtonClick_m744901026 (CenterOnMapButton_t803353738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
