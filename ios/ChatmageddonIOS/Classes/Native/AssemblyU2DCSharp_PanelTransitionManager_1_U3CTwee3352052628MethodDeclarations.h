﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>
struct U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::.ctor()
extern "C"  void U3CTweenObjectForwardsU3Ec__Iterator1__ctor_m710252673_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method);
#define U3CTweenObjectForwardsU3Ec__Iterator1__ctor_m710252673(__this, method) ((  void (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))U3CTweenObjectForwardsU3Ec__Iterator1__ctor_m710252673_gshared)(__this, method)
// System.Boolean PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::MoveNext()
extern "C"  bool U3CTweenObjectForwardsU3Ec__Iterator1_MoveNext_m1743400835_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method);
#define U3CTweenObjectForwardsU3Ec__Iterator1_MoveNext_m1743400835(__this, method) ((  bool (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))U3CTweenObjectForwardsU3Ec__Iterator1_MoveNext_m1743400835_gshared)(__this, method)
// System.Object PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1131874671_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method);
#define U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1131874671(__this, method) ((  Il2CppObject * (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1131874671_gshared)(__this, method)
// System.Object PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2186467591_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method);
#define U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2186467591(__this, method) ((  Il2CppObject * (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))U3CTweenObjectForwardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2186467591_gshared)(__this, method)
// System.Void PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::Dispose()
extern "C"  void U3CTweenObjectForwardsU3Ec__Iterator1_Dispose_m2699090910_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method);
#define U3CTweenObjectForwardsU3Ec__Iterator1_Dispose_m2699090910(__this, method) ((  void (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))U3CTweenObjectForwardsU3Ec__Iterator1_Dispose_m2699090910_gshared)(__this, method)
// System.Void PanelTransitionManager`1/<TweenObjectForwards>c__Iterator1<System.Object>::Reset()
extern "C"  void U3CTweenObjectForwardsU3Ec__Iterator1_Reset_m3295146332_gshared (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 * __this, const MethodInfo* method);
#define U3CTweenObjectForwardsU3Ec__Iterator1_Reset_m3295146332(__this, method) ((  void (*) (U3CTweenObjectForwardsU3Ec__Iterator1_t3352052628 *, const MethodInfo*))U3CTweenObjectForwardsU3Ec__Iterator1_Reset_m3295146332_gshared)(__this, method)
