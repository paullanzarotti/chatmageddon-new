﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4074517125(__this, ___l0, method) ((  void (*) (Enumerator_t4021280246 *, List_1_t191583276 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1738214973(__this, method) ((  void (*) (Enumerator_t4021280246 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4016364205(__this, method) ((  Il2CppObject * (*) (Enumerator_t4021280246 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::Dispose()
#define Enumerator_Dispose_m962734402(__this, method) ((  void (*) (Enumerator_t4021280246 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::VerifyState()
#define Enumerator_VerifyState_m2475445799(__this, method) ((  void (*) (Enumerator_t4021280246 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::MoveNext()
#define Enumerator_MoveNext_m256915238(__this, method) ((  bool (*) (Enumerator_t4021280246 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<BestHTTP.Extensions.HeaderValue>::get_Current()
#define Enumerator_get_Current_m3300433340(__this, method) ((  HeaderValue_t822462144 * (*) (Enumerator_t4021280246 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
