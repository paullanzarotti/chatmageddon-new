﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t278961118;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// System.Collections.Generic.List`1<ChatMessage>
struct List_1_t1753349819;
// System.Func`2<ChatMessage,System.DateTime>
struct Func_2_t925466379;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatThread
struct  ChatThread_t2394323482  : public Il2CppObject
{
public:
	// System.String ChatThread::threadID
	String_t* ___threadID_0;
	// System.Collections.Generic.List`1<System.Collections.Hashtable> ChatThread::threadMessages
	List_1_t278961118 * ___threadMessages_1;
	// System.Collections.Generic.List`1<Friend> ChatThread::threadUsers
	List_1_t2924135240 * ___threadUsers_2;
	// System.Collections.Generic.List`1<ChatMessage> ChatThread::messageStringList
	List_1_t1753349819 * ___messageStringList_3;
	// System.String ChatThread::lastMessage
	String_t* ___lastMessage_4;
	// System.DateTime ChatThread::lastMessageDateTime
	DateTime_t693205669  ___lastMessageDateTime_5;
	// System.String ChatThread::friendName
	String_t* ___friendName_6;
	// System.Boolean ChatThread::firstTimeUpdating
	bool ___firstTimeUpdating_7;
	// System.Boolean ChatThread::multiUserChat
	bool ___multiUserChat_8;
	// System.Int32 ChatThread::unreadMessageCount
	int32_t ___unreadMessageCount_9;

public:
	inline static int32_t get_offset_of_threadID_0() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___threadID_0)); }
	inline String_t* get_threadID_0() const { return ___threadID_0; }
	inline String_t** get_address_of_threadID_0() { return &___threadID_0; }
	inline void set_threadID_0(String_t* value)
	{
		___threadID_0 = value;
		Il2CppCodeGenWriteBarrier(&___threadID_0, value);
	}

	inline static int32_t get_offset_of_threadMessages_1() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___threadMessages_1)); }
	inline List_1_t278961118 * get_threadMessages_1() const { return ___threadMessages_1; }
	inline List_1_t278961118 ** get_address_of_threadMessages_1() { return &___threadMessages_1; }
	inline void set_threadMessages_1(List_1_t278961118 * value)
	{
		___threadMessages_1 = value;
		Il2CppCodeGenWriteBarrier(&___threadMessages_1, value);
	}

	inline static int32_t get_offset_of_threadUsers_2() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___threadUsers_2)); }
	inline List_1_t2924135240 * get_threadUsers_2() const { return ___threadUsers_2; }
	inline List_1_t2924135240 ** get_address_of_threadUsers_2() { return &___threadUsers_2; }
	inline void set_threadUsers_2(List_1_t2924135240 * value)
	{
		___threadUsers_2 = value;
		Il2CppCodeGenWriteBarrier(&___threadUsers_2, value);
	}

	inline static int32_t get_offset_of_messageStringList_3() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___messageStringList_3)); }
	inline List_1_t1753349819 * get_messageStringList_3() const { return ___messageStringList_3; }
	inline List_1_t1753349819 ** get_address_of_messageStringList_3() { return &___messageStringList_3; }
	inline void set_messageStringList_3(List_1_t1753349819 * value)
	{
		___messageStringList_3 = value;
		Il2CppCodeGenWriteBarrier(&___messageStringList_3, value);
	}

	inline static int32_t get_offset_of_lastMessage_4() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___lastMessage_4)); }
	inline String_t* get_lastMessage_4() const { return ___lastMessage_4; }
	inline String_t** get_address_of_lastMessage_4() { return &___lastMessage_4; }
	inline void set_lastMessage_4(String_t* value)
	{
		___lastMessage_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastMessage_4, value);
	}

	inline static int32_t get_offset_of_lastMessageDateTime_5() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___lastMessageDateTime_5)); }
	inline DateTime_t693205669  get_lastMessageDateTime_5() const { return ___lastMessageDateTime_5; }
	inline DateTime_t693205669 * get_address_of_lastMessageDateTime_5() { return &___lastMessageDateTime_5; }
	inline void set_lastMessageDateTime_5(DateTime_t693205669  value)
	{
		___lastMessageDateTime_5 = value;
	}

	inline static int32_t get_offset_of_friendName_6() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___friendName_6)); }
	inline String_t* get_friendName_6() const { return ___friendName_6; }
	inline String_t** get_address_of_friendName_6() { return &___friendName_6; }
	inline void set_friendName_6(String_t* value)
	{
		___friendName_6 = value;
		Il2CppCodeGenWriteBarrier(&___friendName_6, value);
	}

	inline static int32_t get_offset_of_firstTimeUpdating_7() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___firstTimeUpdating_7)); }
	inline bool get_firstTimeUpdating_7() const { return ___firstTimeUpdating_7; }
	inline bool* get_address_of_firstTimeUpdating_7() { return &___firstTimeUpdating_7; }
	inline void set_firstTimeUpdating_7(bool value)
	{
		___firstTimeUpdating_7 = value;
	}

	inline static int32_t get_offset_of_multiUserChat_8() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___multiUserChat_8)); }
	inline bool get_multiUserChat_8() const { return ___multiUserChat_8; }
	inline bool* get_address_of_multiUserChat_8() { return &___multiUserChat_8; }
	inline void set_multiUserChat_8(bool value)
	{
		___multiUserChat_8 = value;
	}

	inline static int32_t get_offset_of_unreadMessageCount_9() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482, ___unreadMessageCount_9)); }
	inline int32_t get_unreadMessageCount_9() const { return ___unreadMessageCount_9; }
	inline int32_t* get_address_of_unreadMessageCount_9() { return &___unreadMessageCount_9; }
	inline void set_unreadMessageCount_9(int32_t value)
	{
		___unreadMessageCount_9 = value;
	}
};

struct ChatThread_t2394323482_StaticFields
{
public:
	// System.Func`2<ChatMessage,System.DateTime> ChatThread::<>f__am$cache0
	Func_2_t925466379 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(ChatThread_t2394323482_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Func_2_t925466379 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Func_2_t925466379 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Func_2_t925466379 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
