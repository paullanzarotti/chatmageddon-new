﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.TwitterCombo
struct  TwitterCombo_t1194619861  : public Il2CppObject
{
public:

public:
};

struct TwitterCombo_t1194619861_StaticFields
{
public:
	// System.String Prime31.TwitterCombo::_username
	String_t* ____username_0;

public:
	inline static int32_t get_offset_of__username_0() { return static_cast<int32_t>(offsetof(TwitterCombo_t1194619861_StaticFields, ____username_0)); }
	inline String_t* get__username_0() const { return ____username_0; }
	inline String_t** get_address_of__username_0() { return &____username_0; }
	inline void set__username_0(String_t* value)
	{
		____username_0 = value;
		Il2CppCodeGenWriteBarrier(&____username_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
