﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipInputStream
struct ZipInputStream_t1369792737;
// System.IO.Stream
struct Stream_t3255436806;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t1764014695;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::.ctor(System.IO.Stream)
extern "C"  void ZipInputStream__ctor_m2152262024 (ZipInputStream_t1369792737 * __this, Stream_t3255436806 * ___baseInputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_CanDecompressEntry()
extern "C"  bool ZipInputStream_get_CanDecompressEntry_m3676272695 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipInputStream::GetNextEntry()
extern "C"  ZipEntry_t1764014695 * ZipInputStream_GetNextEntry_m905824784 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadDataDescriptor()
extern "C"  void ZipInputStream_ReadDataDescriptor_m532537086 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CompleteCloseEntry(System.Boolean)
extern "C"  void ZipInputStream_CompleteCloseEntry_m1755215957 (ZipInputStream_t1369792737 * __this, bool ___testCrc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CloseEntry()
extern "C"  void ZipInputStream_CloseEntry_m3341626155 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Length()
extern "C"  int64_t ZipInputStream_get_Length_m4273692625 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadByte()
extern "C"  int32_t ZipInputStream_ReadByte_m3836589815 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotAvailable(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_ReadingNotAvailable_m3606461204 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotSupported(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_ReadingNotSupported_m1168240437 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::InitialRead(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_InitialRead_m3367700748 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_Read_m1441760760 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::BodyRead(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_BodyRead_m1704405032 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::Close()
extern "C"  void ZipInputStream_Close_m1006504851 (ZipInputStream_t1369792737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
