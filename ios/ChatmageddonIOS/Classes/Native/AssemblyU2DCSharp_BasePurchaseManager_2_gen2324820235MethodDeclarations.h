﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BasePurchaseManager_2_gen1658551487MethodDeclarations.h"

// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::.ctor()
#define BasePurchaseManager_2__ctor_m2989666985(__this, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, const MethodInfo*))BasePurchaseManager_2__ctor_m1131248398_gshared)(__this, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::Init()
#define BasePurchaseManager_2_Init_m3006726911(__this, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, const MethodInfo*))BasePurchaseManager_2_Init_m619327910_gshared)(__this, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::PurchaseUnibillItem(PurchasableItem)
#define BasePurchaseManager_2_PurchaseUnibillItem_m891734083(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, PurchasableItem_t3963353899 *, const MethodInfo*))BasePurchaseManager_2_PurchaseUnibillItem_m643615934_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::UnibillPurchaseSuccess(PurchasableItem,System.String)
#define BasePurchaseManager_2_UnibillPurchaseSuccess_m327807695(__this, ___item0, ___receipt1, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, PurchasableItem_t3963353899 *, String_t*, const MethodInfo*))BasePurchaseManager_2_UnibillPurchaseSuccess_m1466479594_gshared)(__this, ___item0, ___receipt1, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::UnibillPurchaseFailed(PurchasableItem)
#define BasePurchaseManager_2_UnibillPurchaseFailed_m4080829053(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, PurchasableItem_t3963353899 *, const MethodInfo*))BasePurchaseManager_2_UnibillPurchaseFailed_m3742805378_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::ProcessItem(System.String,System.String)
#define BasePurchaseManager_2_ProcessItem_m996757527(__this, ___id0, ___receipt1, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, String_t*, String_t*, const MethodInfo*))BasePurchaseManager_2_ProcessItem_m640918206_gshared)(__this, ___id0, ___receipt1, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::ProcessFailedItem(System.String)
#define BasePurchaseManager_2_ProcessFailedItem_m2017676646(__this, ___id0, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, String_t*, const MethodInfo*))BasePurchaseManager_2_ProcessFailedItem_m3360315917_gshared)(__this, ___id0, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::PurchaseItem(itemType)
#define BasePurchaseManager_2_PurchaseItem_m335073622(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, int32_t, const MethodInfo*))BasePurchaseManager_2_PurchaseItem_m926136925_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::PurchaseItemByID(System.String)
#define BasePurchaseManager_2_PurchaseItemByID_m3261849939(__this, ___itemID0, method) ((  void (*) (BasePurchaseManager_2_t2324820235 *, String_t*, const MethodInfo*))BasePurchaseManager_2_PurchaseItemByID_m4138142978_gshared)(__this, ___itemID0, method)
// System.String BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::GetPurchaseLocalizedString(itemType)
#define BasePurchaseManager_2_GetPurchaseLocalizedString_m214880340(__this, ___item0, method) ((  String_t* (*) (BasePurchaseManager_2_t2324820235 *, int32_t, const MethodInfo*))BasePurchaseManager_2_GetPurchaseLocalizedString_m1960358353_gshared)(__this, ___item0, method)
// System.String BasePurchaseManager`2<ChatmageddonPurchaseManager,ShopPurchasableItems>::GetPurchaseLocalizedStringByID(System.String)
#define BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m248370559(__this, ___itemID0, method) ((  String_t* (*) (BasePurchaseManager_2_t2324820235 *, String_t*, const MethodInfo*))BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m3465251108_gshared)(__this, ___itemID0, method)
