﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SmallBread
struct SmallBread_t121884373;
// TweenScale
struct TweenScale_t2697902175;
// System.Collections.Generic.List`1<TweenAlpha>
struct List_1_t1790639767;

#include "AssemblyU2DCSharp_ToastUI270783635.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmallToastUI
struct  SmallToastUI_t2064810232  : public ToastUI_t270783635
{
public:
	// System.Single SmallToastUI::positonOffset
	float ___positonOffset_6;
	// System.Int32 SmallToastUI::verticlePadding
	int32_t ___verticlePadding_7;
	// SmallBread SmallToastUI::smallBread
	SmallBread_t121884373 * ___smallBread_8;
	// TweenScale SmallToastUI::scaleTween
	TweenScale_t2697902175 * ___scaleTween_9;
	// System.Collections.Generic.List`1<TweenAlpha> SmallToastUI::alphaTweens
	List_1_t1790639767 * ___alphaTweens_10;
	// System.Boolean SmallToastUI::closing
	bool ___closing_11;

public:
	inline static int32_t get_offset_of_positonOffset_6() { return static_cast<int32_t>(offsetof(SmallToastUI_t2064810232, ___positonOffset_6)); }
	inline float get_positonOffset_6() const { return ___positonOffset_6; }
	inline float* get_address_of_positonOffset_6() { return &___positonOffset_6; }
	inline void set_positonOffset_6(float value)
	{
		___positonOffset_6 = value;
	}

	inline static int32_t get_offset_of_verticlePadding_7() { return static_cast<int32_t>(offsetof(SmallToastUI_t2064810232, ___verticlePadding_7)); }
	inline int32_t get_verticlePadding_7() const { return ___verticlePadding_7; }
	inline int32_t* get_address_of_verticlePadding_7() { return &___verticlePadding_7; }
	inline void set_verticlePadding_7(int32_t value)
	{
		___verticlePadding_7 = value;
	}

	inline static int32_t get_offset_of_smallBread_8() { return static_cast<int32_t>(offsetof(SmallToastUI_t2064810232, ___smallBread_8)); }
	inline SmallBread_t121884373 * get_smallBread_8() const { return ___smallBread_8; }
	inline SmallBread_t121884373 ** get_address_of_smallBread_8() { return &___smallBread_8; }
	inline void set_smallBread_8(SmallBread_t121884373 * value)
	{
		___smallBread_8 = value;
		Il2CppCodeGenWriteBarrier(&___smallBread_8, value);
	}

	inline static int32_t get_offset_of_scaleTween_9() { return static_cast<int32_t>(offsetof(SmallToastUI_t2064810232, ___scaleTween_9)); }
	inline TweenScale_t2697902175 * get_scaleTween_9() const { return ___scaleTween_9; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_9() { return &___scaleTween_9; }
	inline void set_scaleTween_9(TweenScale_t2697902175 * value)
	{
		___scaleTween_9 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_9, value);
	}

	inline static int32_t get_offset_of_alphaTweens_10() { return static_cast<int32_t>(offsetof(SmallToastUI_t2064810232, ___alphaTweens_10)); }
	inline List_1_t1790639767 * get_alphaTweens_10() const { return ___alphaTweens_10; }
	inline List_1_t1790639767 ** get_address_of_alphaTweens_10() { return &___alphaTweens_10; }
	inline void set_alphaTweens_10(List_1_t1790639767 * value)
	{
		___alphaTweens_10 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTweens_10, value);
	}

	inline static int32_t get_offset_of_closing_11() { return static_cast<int32_t>(offsetof(SmallToastUI_t2064810232, ___closing_11)); }
	inline bool get_closing_11() const { return ___closing_11; }
	inline bool* get_address_of_closing_11() { return &___closing_11; }
	inline void set_closing_11(bool value)
	{
		___closing_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
