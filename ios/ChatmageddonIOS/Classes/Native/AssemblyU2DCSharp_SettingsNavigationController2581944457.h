﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SettingsNavigateBackwardsButton
struct SettingsNavigateBackwardsButton_t350955144;
// SettingsNavigateForwardsButton
struct SettingsNavigateForwardsButton_t3659026136;
// LogOutTransition
struct LogOutTransition_t3259858399;

#include "AssemblyU2DCSharp_NavigationController_2_gen1825442972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsNavigationController
struct  SettingsNavigationController_t2581944457  : public NavigationController_2_t1825442972
{
public:
	// UILabel SettingsNavigationController::titleLabel
	UILabel_t1795115428 * ___titleLabel_8;
	// UnityEngine.GameObject SettingsNavigationController::editUI
	GameObject_t1756533147 * ___editUI_9;
	// UnityEngine.GameObject SettingsNavigationController::homeObject
	GameObject_t1756533147 * ___homeObject_10;
	// UnityEngine.GameObject SettingsNavigationController::profileObject
	GameObject_t1756533147 * ___profileObject_11;
	// UnityEngine.GameObject SettingsNavigationController::editNameObject
	GameObject_t1756533147 * ___editNameObject_12;
	// UnityEngine.GameObject SettingsNavigationController::selectAudienceObject
	GameObject_t1756533147 * ___selectAudienceObject_13;
	// UnityEngine.GameObject SettingsNavigationController::stealthModeObject
	GameObject_t1756533147 * ___stealthModeObject_14;
	// UnityEngine.GameObject SettingsNavigationController::aboutAndHelpObject
	GameObject_t1756533147 * ___aboutAndHelpObject_15;
	// UnityEngine.GameObject SettingsNavigationController::faqsObject
	GameObject_t1756533147 * ___faqsObject_16;
	// UnityEngine.GameObject SettingsNavigationController::textPageObject
	GameObject_t1756533147 * ___textPageObject_17;
	// UnityEngine.GameObject SettingsNavigationController::notificationsObject
	GameObject_t1756533147 * ___notificationsObject_18;
	// UnityEngine.GameObject SettingsNavigationController::blockListObject
	GameObject_t1756533147 * ___blockListObject_19;
	// UnityEngine.GameObject SettingsNavigationController::genderObject
	GameObject_t1756533147 * ___genderObject_20;
	// UnityEngine.GameObject SettingsNavigationController::editUsernameObject
	GameObject_t1756533147 * ___editUsernameObject_21;
	// UnityEngine.GameObject SettingsNavigationController::editEmailObject
	GameObject_t1756533147 * ___editEmailObject_22;
	// UnityEngine.GameObject SettingsNavigationController::updatePasswordObject
	GameObject_t1756533147 * ___updatePasswordObject_23;
	// UnityEngine.GameObject SettingsNavigationController::audioObject
	GameObject_t1756533147 * ___audioObject_24;
	// SettingsNavigateBackwardsButton SettingsNavigationController::mainNavigateBackwardButton
	SettingsNavigateBackwardsButton_t350955144 * ___mainNavigateBackwardButton_25;
	// SettingsNavigateForwardsButton SettingsNavigationController::mainNavigateForwardButton
	SettingsNavigateForwardsButton_t3659026136 * ___mainNavigateForwardButton_26;
	// UnityEngine.Vector3 SettingsNavigationController::activePos
	Vector3_t2243707580  ___activePos_27;
	// UnityEngine.Vector3 SettingsNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_28;
	// UnityEngine.Vector3 SettingsNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_29;
	// LogOutTransition SettingsNavigationController::logout
	LogOutTransition_t3259858399 * ___logout_30;

public:
	inline static int32_t get_offset_of_titleLabel_8() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___titleLabel_8)); }
	inline UILabel_t1795115428 * get_titleLabel_8() const { return ___titleLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_8() { return &___titleLabel_8; }
	inline void set_titleLabel_8(UILabel_t1795115428 * value)
	{
		___titleLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_8, value);
	}

	inline static int32_t get_offset_of_editUI_9() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___editUI_9)); }
	inline GameObject_t1756533147 * get_editUI_9() const { return ___editUI_9; }
	inline GameObject_t1756533147 ** get_address_of_editUI_9() { return &___editUI_9; }
	inline void set_editUI_9(GameObject_t1756533147 * value)
	{
		___editUI_9 = value;
		Il2CppCodeGenWriteBarrier(&___editUI_9, value);
	}

	inline static int32_t get_offset_of_homeObject_10() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___homeObject_10)); }
	inline GameObject_t1756533147 * get_homeObject_10() const { return ___homeObject_10; }
	inline GameObject_t1756533147 ** get_address_of_homeObject_10() { return &___homeObject_10; }
	inline void set_homeObject_10(GameObject_t1756533147 * value)
	{
		___homeObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___homeObject_10, value);
	}

	inline static int32_t get_offset_of_profileObject_11() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___profileObject_11)); }
	inline GameObject_t1756533147 * get_profileObject_11() const { return ___profileObject_11; }
	inline GameObject_t1756533147 ** get_address_of_profileObject_11() { return &___profileObject_11; }
	inline void set_profileObject_11(GameObject_t1756533147 * value)
	{
		___profileObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___profileObject_11, value);
	}

	inline static int32_t get_offset_of_editNameObject_12() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___editNameObject_12)); }
	inline GameObject_t1756533147 * get_editNameObject_12() const { return ___editNameObject_12; }
	inline GameObject_t1756533147 ** get_address_of_editNameObject_12() { return &___editNameObject_12; }
	inline void set_editNameObject_12(GameObject_t1756533147 * value)
	{
		___editNameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___editNameObject_12, value);
	}

	inline static int32_t get_offset_of_selectAudienceObject_13() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___selectAudienceObject_13)); }
	inline GameObject_t1756533147 * get_selectAudienceObject_13() const { return ___selectAudienceObject_13; }
	inline GameObject_t1756533147 ** get_address_of_selectAudienceObject_13() { return &___selectAudienceObject_13; }
	inline void set_selectAudienceObject_13(GameObject_t1756533147 * value)
	{
		___selectAudienceObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___selectAudienceObject_13, value);
	}

	inline static int32_t get_offset_of_stealthModeObject_14() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___stealthModeObject_14)); }
	inline GameObject_t1756533147 * get_stealthModeObject_14() const { return ___stealthModeObject_14; }
	inline GameObject_t1756533147 ** get_address_of_stealthModeObject_14() { return &___stealthModeObject_14; }
	inline void set_stealthModeObject_14(GameObject_t1756533147 * value)
	{
		___stealthModeObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___stealthModeObject_14, value);
	}

	inline static int32_t get_offset_of_aboutAndHelpObject_15() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___aboutAndHelpObject_15)); }
	inline GameObject_t1756533147 * get_aboutAndHelpObject_15() const { return ___aboutAndHelpObject_15; }
	inline GameObject_t1756533147 ** get_address_of_aboutAndHelpObject_15() { return &___aboutAndHelpObject_15; }
	inline void set_aboutAndHelpObject_15(GameObject_t1756533147 * value)
	{
		___aboutAndHelpObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___aboutAndHelpObject_15, value);
	}

	inline static int32_t get_offset_of_faqsObject_16() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___faqsObject_16)); }
	inline GameObject_t1756533147 * get_faqsObject_16() const { return ___faqsObject_16; }
	inline GameObject_t1756533147 ** get_address_of_faqsObject_16() { return &___faqsObject_16; }
	inline void set_faqsObject_16(GameObject_t1756533147 * value)
	{
		___faqsObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___faqsObject_16, value);
	}

	inline static int32_t get_offset_of_textPageObject_17() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___textPageObject_17)); }
	inline GameObject_t1756533147 * get_textPageObject_17() const { return ___textPageObject_17; }
	inline GameObject_t1756533147 ** get_address_of_textPageObject_17() { return &___textPageObject_17; }
	inline void set_textPageObject_17(GameObject_t1756533147 * value)
	{
		___textPageObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___textPageObject_17, value);
	}

	inline static int32_t get_offset_of_notificationsObject_18() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___notificationsObject_18)); }
	inline GameObject_t1756533147 * get_notificationsObject_18() const { return ___notificationsObject_18; }
	inline GameObject_t1756533147 ** get_address_of_notificationsObject_18() { return &___notificationsObject_18; }
	inline void set_notificationsObject_18(GameObject_t1756533147 * value)
	{
		___notificationsObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsObject_18, value);
	}

	inline static int32_t get_offset_of_blockListObject_19() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___blockListObject_19)); }
	inline GameObject_t1756533147 * get_blockListObject_19() const { return ___blockListObject_19; }
	inline GameObject_t1756533147 ** get_address_of_blockListObject_19() { return &___blockListObject_19; }
	inline void set_blockListObject_19(GameObject_t1756533147 * value)
	{
		___blockListObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___blockListObject_19, value);
	}

	inline static int32_t get_offset_of_genderObject_20() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___genderObject_20)); }
	inline GameObject_t1756533147 * get_genderObject_20() const { return ___genderObject_20; }
	inline GameObject_t1756533147 ** get_address_of_genderObject_20() { return &___genderObject_20; }
	inline void set_genderObject_20(GameObject_t1756533147 * value)
	{
		___genderObject_20 = value;
		Il2CppCodeGenWriteBarrier(&___genderObject_20, value);
	}

	inline static int32_t get_offset_of_editUsernameObject_21() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___editUsernameObject_21)); }
	inline GameObject_t1756533147 * get_editUsernameObject_21() const { return ___editUsernameObject_21; }
	inline GameObject_t1756533147 ** get_address_of_editUsernameObject_21() { return &___editUsernameObject_21; }
	inline void set_editUsernameObject_21(GameObject_t1756533147 * value)
	{
		___editUsernameObject_21 = value;
		Il2CppCodeGenWriteBarrier(&___editUsernameObject_21, value);
	}

	inline static int32_t get_offset_of_editEmailObject_22() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___editEmailObject_22)); }
	inline GameObject_t1756533147 * get_editEmailObject_22() const { return ___editEmailObject_22; }
	inline GameObject_t1756533147 ** get_address_of_editEmailObject_22() { return &___editEmailObject_22; }
	inline void set_editEmailObject_22(GameObject_t1756533147 * value)
	{
		___editEmailObject_22 = value;
		Il2CppCodeGenWriteBarrier(&___editEmailObject_22, value);
	}

	inline static int32_t get_offset_of_updatePasswordObject_23() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___updatePasswordObject_23)); }
	inline GameObject_t1756533147 * get_updatePasswordObject_23() const { return ___updatePasswordObject_23; }
	inline GameObject_t1756533147 ** get_address_of_updatePasswordObject_23() { return &___updatePasswordObject_23; }
	inline void set_updatePasswordObject_23(GameObject_t1756533147 * value)
	{
		___updatePasswordObject_23 = value;
		Il2CppCodeGenWriteBarrier(&___updatePasswordObject_23, value);
	}

	inline static int32_t get_offset_of_audioObject_24() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___audioObject_24)); }
	inline GameObject_t1756533147 * get_audioObject_24() const { return ___audioObject_24; }
	inline GameObject_t1756533147 ** get_address_of_audioObject_24() { return &___audioObject_24; }
	inline void set_audioObject_24(GameObject_t1756533147 * value)
	{
		___audioObject_24 = value;
		Il2CppCodeGenWriteBarrier(&___audioObject_24, value);
	}

	inline static int32_t get_offset_of_mainNavigateBackwardButton_25() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___mainNavigateBackwardButton_25)); }
	inline SettingsNavigateBackwardsButton_t350955144 * get_mainNavigateBackwardButton_25() const { return ___mainNavigateBackwardButton_25; }
	inline SettingsNavigateBackwardsButton_t350955144 ** get_address_of_mainNavigateBackwardButton_25() { return &___mainNavigateBackwardButton_25; }
	inline void set_mainNavigateBackwardButton_25(SettingsNavigateBackwardsButton_t350955144 * value)
	{
		___mainNavigateBackwardButton_25 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateBackwardButton_25, value);
	}

	inline static int32_t get_offset_of_mainNavigateForwardButton_26() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___mainNavigateForwardButton_26)); }
	inline SettingsNavigateForwardsButton_t3659026136 * get_mainNavigateForwardButton_26() const { return ___mainNavigateForwardButton_26; }
	inline SettingsNavigateForwardsButton_t3659026136 ** get_address_of_mainNavigateForwardButton_26() { return &___mainNavigateForwardButton_26; }
	inline void set_mainNavigateForwardButton_26(SettingsNavigateForwardsButton_t3659026136 * value)
	{
		___mainNavigateForwardButton_26 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateForwardButton_26, value);
	}

	inline static int32_t get_offset_of_activePos_27() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___activePos_27)); }
	inline Vector3_t2243707580  get_activePos_27() const { return ___activePos_27; }
	inline Vector3_t2243707580 * get_address_of_activePos_27() { return &___activePos_27; }
	inline void set_activePos_27(Vector3_t2243707580  value)
	{
		___activePos_27 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_28() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___leftInactivePos_28)); }
	inline Vector3_t2243707580  get_leftInactivePos_28() const { return ___leftInactivePos_28; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_28() { return &___leftInactivePos_28; }
	inline void set_leftInactivePos_28(Vector3_t2243707580  value)
	{
		___leftInactivePos_28 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_29() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___rightInactivePos_29)); }
	inline Vector3_t2243707580  get_rightInactivePos_29() const { return ___rightInactivePos_29; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_29() { return &___rightInactivePos_29; }
	inline void set_rightInactivePos_29(Vector3_t2243707580  value)
	{
		___rightInactivePos_29 = value;
	}

	inline static int32_t get_offset_of_logout_30() { return static_cast<int32_t>(offsetof(SettingsNavigationController_t2581944457, ___logout_30)); }
	inline LogOutTransition_t3259858399 * get_logout_30() const { return ___logout_30; }
	inline LogOutTransition_t3259858399 ** get_address_of_logout_30() { return &___logout_30; }
	inline void set_logout_30(LogOutTransition_t3259858399 * value)
	{
		___logout_30 = value;
		Il2CppCodeGenWriteBarrier(&___logout_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
