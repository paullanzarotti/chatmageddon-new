﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Char,System.Char,System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>
struct Transform_1_t4145265959;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22812303849.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Char,System.Char,System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3816043350_gshared (Transform_1_t4145265959 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3816043350(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t4145265959 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3816043350_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,System.Char,System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2812303849  Transform_1_Invoke_m1832561686_gshared (Transform_1_t4145265959 * __this, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1832561686(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2812303849  (*) (Transform_1_t4145265959 *, Il2CppChar, Il2CppChar, const MethodInfo*))Transform_1_Invoke_m1832561686_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Char,System.Char,System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2498194559_gshared (Transform_1_t4145265959 * __this, Il2CppChar ___key0, Il2CppChar ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2498194559(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t4145265959 *, Il2CppChar, Il2CppChar, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2498194559_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,System.Char,System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2812303849  Transform_1_EndInvoke_m632294828_gshared (Transform_1_t4145265959 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m632294828(__this, ___result0, method) ((  KeyValuePair_2_t2812303849  (*) (Transform_1_t4145265959 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m632294828_gshared)(__this, ___result0, method)
