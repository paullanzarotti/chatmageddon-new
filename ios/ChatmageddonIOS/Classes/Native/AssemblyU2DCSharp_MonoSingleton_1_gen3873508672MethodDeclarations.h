﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<LockoutManager>::.ctor()
#define MonoSingleton_1__ctor_m2455019916(__this, method) ((  void (*) (MonoSingleton_1_t3873508672 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<LockoutManager>::Awake()
#define MonoSingleton_1_Awake_m68532811(__this, method) ((  void (*) (MonoSingleton_1_t3873508672 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<LockoutManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3848760317(__this /* static, unused */, method) ((  LockoutManager_t4122842952 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<LockoutManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4259747297(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<LockoutManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m385178522(__this, method) ((  void (*) (MonoSingleton_1_t3873508672 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<LockoutManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2500837938(__this, method) ((  void (*) (MonoSingleton_1_t3873508672 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<LockoutManager>::.cctor()
#define MonoSingleton_1__cctor_m48806493(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
