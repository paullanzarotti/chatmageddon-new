﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "P31RestKit_Prime31_MonoBehaviourGUI1135617291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookGUIManager
struct  FacebookGUIManager_t3698881302  : public MonoBehaviourGUI_t1135617291
{
public:
	// UnityEngine.GameObject Prime31.FacebookGUIManager::cube
	GameObject_t1756533147 * ___cube_18;
	// System.String Prime31.FacebookGUIManager::_userId
	String_t* ____userId_19;
	// System.Boolean Prime31.FacebookGUIManager::_canUserUseFacebookComposer
	bool ____canUserUseFacebookComposer_20;
	// System.Boolean Prime31.FacebookGUIManager::_hasPublishActions
	bool ____hasPublishActions_21;

public:
	inline static int32_t get_offset_of_cube_18() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302, ___cube_18)); }
	inline GameObject_t1756533147 * get_cube_18() const { return ___cube_18; }
	inline GameObject_t1756533147 ** get_address_of_cube_18() { return &___cube_18; }
	inline void set_cube_18(GameObject_t1756533147 * value)
	{
		___cube_18 = value;
		Il2CppCodeGenWriteBarrier(&___cube_18, value);
	}

	inline static int32_t get_offset_of__userId_19() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302, ____userId_19)); }
	inline String_t* get__userId_19() const { return ____userId_19; }
	inline String_t** get_address_of__userId_19() { return &____userId_19; }
	inline void set__userId_19(String_t* value)
	{
		____userId_19 = value;
		Il2CppCodeGenWriteBarrier(&____userId_19, value);
	}

	inline static int32_t get_offset_of__canUserUseFacebookComposer_20() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302, ____canUserUseFacebookComposer_20)); }
	inline bool get__canUserUseFacebookComposer_20() const { return ____canUserUseFacebookComposer_20; }
	inline bool* get_address_of__canUserUseFacebookComposer_20() { return &____canUserUseFacebookComposer_20; }
	inline void set__canUserUseFacebookComposer_20(bool value)
	{
		____canUserUseFacebookComposer_20 = value;
	}

	inline static int32_t get_offset_of__hasPublishActions_21() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302, ____hasPublishActions_21)); }
	inline bool get__hasPublishActions_21() const { return ____hasPublishActions_21; }
	inline bool* get_address_of__hasPublishActions_21() { return &____hasPublishActions_21; }
	inline void set__hasPublishActions_21(bool value)
	{
		____hasPublishActions_21 = value;
	}
};

struct FacebookGUIManager_t3698881302_StaticFields
{
public:
	// System.String Prime31.FacebookGUIManager::screenshotFilename
	String_t* ___screenshotFilename_22;
	// System.Action`1<System.Object> Prime31.FacebookGUIManager::<>f__am$cache0
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache0_23;
	// System.Action`1<System.Boolean> Prime31.FacebookGUIManager::<>f__am$cache1
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache1_24;

public:
	inline static int32_t get_offset_of_screenshotFilename_22() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302_StaticFields, ___screenshotFilename_22)); }
	inline String_t* get_screenshotFilename_22() const { return ___screenshotFilename_22; }
	inline String_t** get_address_of_screenshotFilename_22() { return &___screenshotFilename_22; }
	inline void set_screenshotFilename_22(String_t* value)
	{
		___screenshotFilename_22 = value;
		Il2CppCodeGenWriteBarrier(&___screenshotFilename_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_23() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302_StaticFields, ___U3CU3Ef__amU24cache0_23)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache0_23() const { return ___U3CU3Ef__amU24cache0_23; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache0_23() { return &___U3CU3Ef__amU24cache0_23; }
	inline void set_U3CU3Ef__amU24cache0_23(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache0_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_24() { return static_cast<int32_t>(offsetof(FacebookGUIManager_t3698881302_StaticFields, ___U3CU3Ef__amU24cache1_24)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache1_24() const { return ___U3CU3Ef__amU24cache1_24; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache1_24() { return &___U3CU3Ef__amU24cache1_24; }
	inline void set_U3CU3Ef__amU24cache1_24(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache1_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
