﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LogOutTransition
struct LogOutTransition_t3259858399;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LogOutTransition::.ctor()
extern "C"  void LogOutTransition__ctor_m1359391424 (LogOutTransition_t3259858399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogOutTransition::Awake()
extern "C"  void LogOutTransition_Awake_m1988621931 (LogOutTransition_t3259858399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogOutTransition::UIClosing()
extern "C"  void LogOutTransition_UIClosing_m1071193681 (LogOutTransition_t3259858399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogOutTransition::OpenUI()
extern "C"  void LogOutTransition_OpenUI_m2799875988 (LogOutTransition_t3259858399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogOutTransition::CloseUI(System.Boolean)
extern "C"  void LogOutTransition_CloseUI_m178826239 (LogOutTransition_t3259858399 * __this, bool ___logout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogOutTransition::<CloseUI>m__0(System.Boolean,System.String)
extern "C"  void LogOutTransition_U3CCloseUIU3Em__0_m3965560342 (LogOutTransition_t3259858399 * __this, bool ___logoutSuccess0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
