﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>
struct Collection_1_t2560133917;
// System.Collections.Generic.IList`1<FriendSearchNavScreen>
struct IList_1_t3559329764;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FriendSearchNavScreen[]
struct FriendSearchNavScreenU5BU5D_t3949017674;
// System.Collections.Generic.IEnumerator`1<FriendSearchNavScreen>
struct IEnumerator_1_t493912990;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"

// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m2409926810_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2409926810(__this, method) ((  void (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1__ctor_m2409926810_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m3394816931_gshared (Collection_1_t2560133917 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m3394816931(__this, ___list0, method) ((  void (*) (Collection_1_t2560133917 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3394816931_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3091412849_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3091412849(__this, method) ((  bool (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3091412849_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3925889218_gshared (Collection_1_t2560133917 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3925889218(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2560133917 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3925889218_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1145768317_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1145768317(__this, method) ((  Il2CppObject * (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1145768317_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2790279770_gshared (Collection_1_t2560133917 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2790279770(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2560133917 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2790279770_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1288544364_gshared (Collection_1_t2560133917 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1288544364(__this, ___value0, method) ((  bool (*) (Collection_1_t2560133917 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1288544364_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1557768116_gshared (Collection_1_t2560133917 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1557768116(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2560133917 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1557768116_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3486340525_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3486340525(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3486340525_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m527875189_gshared (Collection_1_t2560133917 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m527875189(__this, ___value0, method) ((  void (*) (Collection_1_t2560133917 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m527875189_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1003286738_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1003286738(__this, method) ((  bool (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1003286738_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1706546162_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1706546162(__this, method) ((  Il2CppObject * (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1706546162_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1590904601_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1590904601(__this, method) ((  bool (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1590904601_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1952986694_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1952986694(__this, method) ((  bool (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1952986694_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1057407577_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1057407577(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1057407577_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m333310624_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m333310624(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m333310624_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m3705346601_gshared (Collection_1_t2560133917 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m3705346601(__this, ___item0, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_Add_m3705346601_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m2920642109_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2920642109(__this, method) ((  void (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_Clear_m2920642109_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3691851799_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3691851799(__this, method) ((  void (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_ClearItems_m3691851799_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m1130555_gshared (Collection_1_t2560133917 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1130555(__this, ___item0, method) ((  bool (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_Contains_m1130555_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1246306737_gshared (Collection_1_t2560133917 * __this, FriendSearchNavScreenU5BU5D_t3949017674* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1246306737(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2560133917 *, FriendSearchNavScreenU5BU5D_t3949017674*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1246306737_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2846069102_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2846069102(__this, method) ((  Il2CppObject* (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_GetEnumerator_m2846069102_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2198569149_gshared (Collection_1_t2560133917 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2198569149(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2198569149_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2924693040_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2924693040(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m2924693040_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1360548443_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1360548443(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1360548443_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m2335673814_gshared (Collection_1_t2560133917 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2335673814(__this, ___item0, method) ((  bool (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_Remove_m2335673814_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3450759940_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3450759940(__this, ___index0, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3450759940_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m914588046_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m914588046(__this, ___index0, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m914588046_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m822530570_gshared (Collection_1_t2560133917 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m822530570(__this, method) ((  int32_t (*) (Collection_1_t2560133917 *, const MethodInfo*))Collection_1_get_Count_m822530570_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m1782117922_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1782117922(__this, ___index0, method) ((  int32_t (*) (Collection_1_t2560133917 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1782117922_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2348684697_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2348684697(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m2348684697_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2022888448_gshared (Collection_1_t2560133917 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2022888448(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2560133917 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m2022888448_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3271395431_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3271395431(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3271395431_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m56335287_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m56335287(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m56335287_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1663272251_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1663272251(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1663272251_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m581201243_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m581201243(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m581201243_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendSearchNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3691675948_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3691675948(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3691675948_gshared)(__this /* static, unused */, ___list0, method)
