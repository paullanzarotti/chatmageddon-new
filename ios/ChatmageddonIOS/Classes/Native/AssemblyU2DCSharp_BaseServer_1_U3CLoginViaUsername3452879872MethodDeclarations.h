﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<LoginViaUsername>c__AnonStorey6<System.Object>
struct U3CLoginViaUsernameU3Ec__AnonStorey6_t3452879872;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<LoginViaUsername>c__AnonStorey6<System.Object>::.ctor()
extern "C"  void U3CLoginViaUsernameU3Ec__AnonStorey6__ctor_m2853249573_gshared (U3CLoginViaUsernameU3Ec__AnonStorey6_t3452879872 * __this, const MethodInfo* method);
#define U3CLoginViaUsernameU3Ec__AnonStorey6__ctor_m2853249573(__this, method) ((  void (*) (U3CLoginViaUsernameU3Ec__AnonStorey6_t3452879872 *, const MethodInfo*))U3CLoginViaUsernameU3Ec__AnonStorey6__ctor_m2853249573_gshared)(__this, method)
// System.Void BaseServer`1/<LoginViaUsername>c__AnonStorey6<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLoginViaUsernameU3Ec__AnonStorey6_U3CU3Em__0_m411375712_gshared (U3CLoginViaUsernameU3Ec__AnonStorey6_t3452879872 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CLoginViaUsernameU3Ec__AnonStorey6_U3CU3Em__0_m411375712(__this, ___request0, ___response1, method) ((  void (*) (U3CLoginViaUsernameU3Ec__AnonStorey6_t3452879872 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CLoginViaUsernameU3Ec__AnonStorey6_U3CU3Em__0_m411375712_gshared)(__this, ___request0, ___response1, method)
