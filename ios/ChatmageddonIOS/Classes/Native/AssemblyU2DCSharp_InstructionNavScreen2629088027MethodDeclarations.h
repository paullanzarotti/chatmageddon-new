﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InstructionNavScreen
struct InstructionNavScreen_t2629088027;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void InstructionNavScreen::.ctor()
extern "C"  void InstructionNavScreen__ctor_m2475356144 (InstructionNavScreen_t2629088027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionNavScreen::Start()
extern "C"  void InstructionNavScreen_Start_m1745226556 (InstructionNavScreen_t2629088027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionNavScreen::UIClosing()
extern "C"  void InstructionNavScreen_UIClosing_m681222989 (InstructionNavScreen_t2629088027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void InstructionNavScreen_ScreenLoad_m3069067464 (InstructionNavScreen_t2629088027 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void InstructionNavScreen_ScreenUnload_m210651572 (InstructionNavScreen_t2629088027 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InstructionNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool InstructionNavScreen_ValidateScreenNavigation_m1631917905 (InstructionNavScreen_t2629088027 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
