﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CountryCodeData
struct CountryCodeData_t1765037603;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;
// UILabel
struct UILabel_t1795115428;
// TweenPosition
struct TweenPosition_t1144714832;
// System.Collections.Generic.List`1<RegInput>
struct List_1_t1488281410;
// UIInput
struct UIInput_t860674234;
// UITexture
struct UITexture_t2537039969;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1284182836.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegistrationManager
struct  RegistrationManager_t1533517116  : public MonoSingleton_1_t1284182836
{
public:
	// System.Boolean RegistrationManager::facebookRegStart
	bool ___facebookRegStart_3;
	// System.Boolean RegistrationManager::numberVerified
	bool ___numberVerified_4;
	// System.Boolean RegistrationManager::registrationComplete
	bool ___registrationComplete_5;
	// CountryCodeData RegistrationManager::selectedCountryCode
	CountryCodeData_t1765037603 * ___selectedCountryCode_6;
	// System.String RegistrationManager::countryCode
	String_t* ___countryCode_7;
	// System.String RegistrationManager::phoneNumber
	String_t* ___phoneNumber_8;
	// System.String RegistrationManager::fullPhoneNumber
	String_t* ___fullPhoneNumber_9;
	// System.String RegistrationManager::mobileHash
	String_t* ___mobileHash_10;
	// System.String RegistrationManager::mobilePinID
	String_t* ___mobilePinID_11;
	// System.String RegistrationManager::mobilePin
	String_t* ___mobilePin_12;
	// UnityEngine.Texture RegistrationManager::profileImageSelected
	Texture_t2243626319 * ___profileImageSelected_13;
	// System.String RegistrationManager::profileImageURL
	String_t* ___profileImageURL_14;
	// UILabel RegistrationManager::countryNameLabel
	UILabel_t1795115428 * ___countryNameLabel_15;
	// UILabel RegistrationManager::countryCodeLabel
	UILabel_t1795115428 * ___countryCodeLabel_16;
	// System.String RegistrationManager::validEmail
	String_t* ___validEmail_17;
	// System.String RegistrationManager::validUsername
	String_t* ___validUsername_18;
	// System.String RegistrationManager::validPassword
	String_t* ___validPassword_19;
	// System.String RegistrationManager::validFirstName
	String_t* ___validFirstName_20;
	// System.String RegistrationManager::validLastName
	String_t* ___validLastName_21;
	// TweenPosition RegistrationManager::regContentTween
	TweenPosition_t1144714832 * ___regContentTween_22;
	// System.Collections.Generic.List`1<RegInput> RegistrationManager::reginputs
	List_1_t1488281410 * ___reginputs_23;
	// UnityEngine.Color RegistrationManager::regInputActive
	Color_t2020392075  ___regInputActive_24;
	// UnityEngine.Color RegistrationManager::regInputInactive
	Color_t2020392075  ___regInputInactive_25;
	// UIInput RegistrationManager::yourNumberLabel
	UIInput_t860674234 * ___yourNumberLabel_26;
	// UIInput RegistrationManager::verifyCodeLabel
	UIInput_t860674234 * ___verifyCodeLabel_27;
	// UILabel RegistrationManager::ADPhonenumberLabel
	UILabel_t1795115428 * ___ADPhonenumberLabel_28;
	// System.Boolean RegistrationManager::regComplete
	bool ___regComplete_29;
	// UIInput RegistrationManager::firstNameInput
	UIInput_t860674234 * ___firstNameInput_30;
	// UIInput RegistrationManager::lastNameInput
	UIInput_t860674234 * ___lastNameInput_31;
	// UIInput RegistrationManager::emailInput
	UIInput_t860674234 * ___emailInput_32;
	// UITexture RegistrationManager::profileTexture
	UITexture_t2537039969 * ___profileTexture_33;

public:
	inline static int32_t get_offset_of_facebookRegStart_3() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___facebookRegStart_3)); }
	inline bool get_facebookRegStart_3() const { return ___facebookRegStart_3; }
	inline bool* get_address_of_facebookRegStart_3() { return &___facebookRegStart_3; }
	inline void set_facebookRegStart_3(bool value)
	{
		___facebookRegStart_3 = value;
	}

	inline static int32_t get_offset_of_numberVerified_4() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___numberVerified_4)); }
	inline bool get_numberVerified_4() const { return ___numberVerified_4; }
	inline bool* get_address_of_numberVerified_4() { return &___numberVerified_4; }
	inline void set_numberVerified_4(bool value)
	{
		___numberVerified_4 = value;
	}

	inline static int32_t get_offset_of_registrationComplete_5() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___registrationComplete_5)); }
	inline bool get_registrationComplete_5() const { return ___registrationComplete_5; }
	inline bool* get_address_of_registrationComplete_5() { return &___registrationComplete_5; }
	inline void set_registrationComplete_5(bool value)
	{
		___registrationComplete_5 = value;
	}

	inline static int32_t get_offset_of_selectedCountryCode_6() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___selectedCountryCode_6)); }
	inline CountryCodeData_t1765037603 * get_selectedCountryCode_6() const { return ___selectedCountryCode_6; }
	inline CountryCodeData_t1765037603 ** get_address_of_selectedCountryCode_6() { return &___selectedCountryCode_6; }
	inline void set_selectedCountryCode_6(CountryCodeData_t1765037603 * value)
	{
		___selectedCountryCode_6 = value;
		Il2CppCodeGenWriteBarrier(&___selectedCountryCode_6, value);
	}

	inline static int32_t get_offset_of_countryCode_7() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___countryCode_7)); }
	inline String_t* get_countryCode_7() const { return ___countryCode_7; }
	inline String_t** get_address_of_countryCode_7() { return &___countryCode_7; }
	inline void set_countryCode_7(String_t* value)
	{
		___countryCode_7 = value;
		Il2CppCodeGenWriteBarrier(&___countryCode_7, value);
	}

	inline static int32_t get_offset_of_phoneNumber_8() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___phoneNumber_8)); }
	inline String_t* get_phoneNumber_8() const { return ___phoneNumber_8; }
	inline String_t** get_address_of_phoneNumber_8() { return &___phoneNumber_8; }
	inline void set_phoneNumber_8(String_t* value)
	{
		___phoneNumber_8 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumber_8, value);
	}

	inline static int32_t get_offset_of_fullPhoneNumber_9() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___fullPhoneNumber_9)); }
	inline String_t* get_fullPhoneNumber_9() const { return ___fullPhoneNumber_9; }
	inline String_t** get_address_of_fullPhoneNumber_9() { return &___fullPhoneNumber_9; }
	inline void set_fullPhoneNumber_9(String_t* value)
	{
		___fullPhoneNumber_9 = value;
		Il2CppCodeGenWriteBarrier(&___fullPhoneNumber_9, value);
	}

	inline static int32_t get_offset_of_mobileHash_10() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___mobileHash_10)); }
	inline String_t* get_mobileHash_10() const { return ___mobileHash_10; }
	inline String_t** get_address_of_mobileHash_10() { return &___mobileHash_10; }
	inline void set_mobileHash_10(String_t* value)
	{
		___mobileHash_10 = value;
		Il2CppCodeGenWriteBarrier(&___mobileHash_10, value);
	}

	inline static int32_t get_offset_of_mobilePinID_11() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___mobilePinID_11)); }
	inline String_t* get_mobilePinID_11() const { return ___mobilePinID_11; }
	inline String_t** get_address_of_mobilePinID_11() { return &___mobilePinID_11; }
	inline void set_mobilePinID_11(String_t* value)
	{
		___mobilePinID_11 = value;
		Il2CppCodeGenWriteBarrier(&___mobilePinID_11, value);
	}

	inline static int32_t get_offset_of_mobilePin_12() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___mobilePin_12)); }
	inline String_t* get_mobilePin_12() const { return ___mobilePin_12; }
	inline String_t** get_address_of_mobilePin_12() { return &___mobilePin_12; }
	inline void set_mobilePin_12(String_t* value)
	{
		___mobilePin_12 = value;
		Il2CppCodeGenWriteBarrier(&___mobilePin_12, value);
	}

	inline static int32_t get_offset_of_profileImageSelected_13() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___profileImageSelected_13)); }
	inline Texture_t2243626319 * get_profileImageSelected_13() const { return ___profileImageSelected_13; }
	inline Texture_t2243626319 ** get_address_of_profileImageSelected_13() { return &___profileImageSelected_13; }
	inline void set_profileImageSelected_13(Texture_t2243626319 * value)
	{
		___profileImageSelected_13 = value;
		Il2CppCodeGenWriteBarrier(&___profileImageSelected_13, value);
	}

	inline static int32_t get_offset_of_profileImageURL_14() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___profileImageURL_14)); }
	inline String_t* get_profileImageURL_14() const { return ___profileImageURL_14; }
	inline String_t** get_address_of_profileImageURL_14() { return &___profileImageURL_14; }
	inline void set_profileImageURL_14(String_t* value)
	{
		___profileImageURL_14 = value;
		Il2CppCodeGenWriteBarrier(&___profileImageURL_14, value);
	}

	inline static int32_t get_offset_of_countryNameLabel_15() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___countryNameLabel_15)); }
	inline UILabel_t1795115428 * get_countryNameLabel_15() const { return ___countryNameLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_countryNameLabel_15() { return &___countryNameLabel_15; }
	inline void set_countryNameLabel_15(UILabel_t1795115428 * value)
	{
		___countryNameLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___countryNameLabel_15, value);
	}

	inline static int32_t get_offset_of_countryCodeLabel_16() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___countryCodeLabel_16)); }
	inline UILabel_t1795115428 * get_countryCodeLabel_16() const { return ___countryCodeLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_countryCodeLabel_16() { return &___countryCodeLabel_16; }
	inline void set_countryCodeLabel_16(UILabel_t1795115428 * value)
	{
		___countryCodeLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeLabel_16, value);
	}

	inline static int32_t get_offset_of_validEmail_17() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___validEmail_17)); }
	inline String_t* get_validEmail_17() const { return ___validEmail_17; }
	inline String_t** get_address_of_validEmail_17() { return &___validEmail_17; }
	inline void set_validEmail_17(String_t* value)
	{
		___validEmail_17 = value;
		Il2CppCodeGenWriteBarrier(&___validEmail_17, value);
	}

	inline static int32_t get_offset_of_validUsername_18() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___validUsername_18)); }
	inline String_t* get_validUsername_18() const { return ___validUsername_18; }
	inline String_t** get_address_of_validUsername_18() { return &___validUsername_18; }
	inline void set_validUsername_18(String_t* value)
	{
		___validUsername_18 = value;
		Il2CppCodeGenWriteBarrier(&___validUsername_18, value);
	}

	inline static int32_t get_offset_of_validPassword_19() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___validPassword_19)); }
	inline String_t* get_validPassword_19() const { return ___validPassword_19; }
	inline String_t** get_address_of_validPassword_19() { return &___validPassword_19; }
	inline void set_validPassword_19(String_t* value)
	{
		___validPassword_19 = value;
		Il2CppCodeGenWriteBarrier(&___validPassword_19, value);
	}

	inline static int32_t get_offset_of_validFirstName_20() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___validFirstName_20)); }
	inline String_t* get_validFirstName_20() const { return ___validFirstName_20; }
	inline String_t** get_address_of_validFirstName_20() { return &___validFirstName_20; }
	inline void set_validFirstName_20(String_t* value)
	{
		___validFirstName_20 = value;
		Il2CppCodeGenWriteBarrier(&___validFirstName_20, value);
	}

	inline static int32_t get_offset_of_validLastName_21() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___validLastName_21)); }
	inline String_t* get_validLastName_21() const { return ___validLastName_21; }
	inline String_t** get_address_of_validLastName_21() { return &___validLastName_21; }
	inline void set_validLastName_21(String_t* value)
	{
		___validLastName_21 = value;
		Il2CppCodeGenWriteBarrier(&___validLastName_21, value);
	}

	inline static int32_t get_offset_of_regContentTween_22() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___regContentTween_22)); }
	inline TweenPosition_t1144714832 * get_regContentTween_22() const { return ___regContentTween_22; }
	inline TweenPosition_t1144714832 ** get_address_of_regContentTween_22() { return &___regContentTween_22; }
	inline void set_regContentTween_22(TweenPosition_t1144714832 * value)
	{
		___regContentTween_22 = value;
		Il2CppCodeGenWriteBarrier(&___regContentTween_22, value);
	}

	inline static int32_t get_offset_of_reginputs_23() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___reginputs_23)); }
	inline List_1_t1488281410 * get_reginputs_23() const { return ___reginputs_23; }
	inline List_1_t1488281410 ** get_address_of_reginputs_23() { return &___reginputs_23; }
	inline void set_reginputs_23(List_1_t1488281410 * value)
	{
		___reginputs_23 = value;
		Il2CppCodeGenWriteBarrier(&___reginputs_23, value);
	}

	inline static int32_t get_offset_of_regInputActive_24() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___regInputActive_24)); }
	inline Color_t2020392075  get_regInputActive_24() const { return ___regInputActive_24; }
	inline Color_t2020392075 * get_address_of_regInputActive_24() { return &___regInputActive_24; }
	inline void set_regInputActive_24(Color_t2020392075  value)
	{
		___regInputActive_24 = value;
	}

	inline static int32_t get_offset_of_regInputInactive_25() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___regInputInactive_25)); }
	inline Color_t2020392075  get_regInputInactive_25() const { return ___regInputInactive_25; }
	inline Color_t2020392075 * get_address_of_regInputInactive_25() { return &___regInputInactive_25; }
	inline void set_regInputInactive_25(Color_t2020392075  value)
	{
		___regInputInactive_25 = value;
	}

	inline static int32_t get_offset_of_yourNumberLabel_26() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___yourNumberLabel_26)); }
	inline UIInput_t860674234 * get_yourNumberLabel_26() const { return ___yourNumberLabel_26; }
	inline UIInput_t860674234 ** get_address_of_yourNumberLabel_26() { return &___yourNumberLabel_26; }
	inline void set_yourNumberLabel_26(UIInput_t860674234 * value)
	{
		___yourNumberLabel_26 = value;
		Il2CppCodeGenWriteBarrier(&___yourNumberLabel_26, value);
	}

	inline static int32_t get_offset_of_verifyCodeLabel_27() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___verifyCodeLabel_27)); }
	inline UIInput_t860674234 * get_verifyCodeLabel_27() const { return ___verifyCodeLabel_27; }
	inline UIInput_t860674234 ** get_address_of_verifyCodeLabel_27() { return &___verifyCodeLabel_27; }
	inline void set_verifyCodeLabel_27(UIInput_t860674234 * value)
	{
		___verifyCodeLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___verifyCodeLabel_27, value);
	}

	inline static int32_t get_offset_of_ADPhonenumberLabel_28() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___ADPhonenumberLabel_28)); }
	inline UILabel_t1795115428 * get_ADPhonenumberLabel_28() const { return ___ADPhonenumberLabel_28; }
	inline UILabel_t1795115428 ** get_address_of_ADPhonenumberLabel_28() { return &___ADPhonenumberLabel_28; }
	inline void set_ADPhonenumberLabel_28(UILabel_t1795115428 * value)
	{
		___ADPhonenumberLabel_28 = value;
		Il2CppCodeGenWriteBarrier(&___ADPhonenumberLabel_28, value);
	}

	inline static int32_t get_offset_of_regComplete_29() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___regComplete_29)); }
	inline bool get_regComplete_29() const { return ___regComplete_29; }
	inline bool* get_address_of_regComplete_29() { return &___regComplete_29; }
	inline void set_regComplete_29(bool value)
	{
		___regComplete_29 = value;
	}

	inline static int32_t get_offset_of_firstNameInput_30() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___firstNameInput_30)); }
	inline UIInput_t860674234 * get_firstNameInput_30() const { return ___firstNameInput_30; }
	inline UIInput_t860674234 ** get_address_of_firstNameInput_30() { return &___firstNameInput_30; }
	inline void set_firstNameInput_30(UIInput_t860674234 * value)
	{
		___firstNameInput_30 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameInput_30, value);
	}

	inline static int32_t get_offset_of_lastNameInput_31() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___lastNameInput_31)); }
	inline UIInput_t860674234 * get_lastNameInput_31() const { return ___lastNameInput_31; }
	inline UIInput_t860674234 ** get_address_of_lastNameInput_31() { return &___lastNameInput_31; }
	inline void set_lastNameInput_31(UIInput_t860674234 * value)
	{
		___lastNameInput_31 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameInput_31, value);
	}

	inline static int32_t get_offset_of_emailInput_32() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___emailInput_32)); }
	inline UIInput_t860674234 * get_emailInput_32() const { return ___emailInput_32; }
	inline UIInput_t860674234 ** get_address_of_emailInput_32() { return &___emailInput_32; }
	inline void set_emailInput_32(UIInput_t860674234 * value)
	{
		___emailInput_32 = value;
		Il2CppCodeGenWriteBarrier(&___emailInput_32, value);
	}

	inline static int32_t get_offset_of_profileTexture_33() { return static_cast<int32_t>(offsetof(RegistrationManager_t1533517116, ___profileTexture_33)); }
	inline UITexture_t2537039969 * get_profileTexture_33() const { return ___profileTexture_33; }
	inline UITexture_t2537039969 ** get_address_of_profileTexture_33() { return &___profileTexture_33; }
	inline void set_profileTexture_33(UITexture_t2537039969 * value)
	{
		___profileTexture_33 = value;
		Il2CppCodeGenWriteBarrier(&___profileTexture_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
