﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeOuterNavScreen
struct AttackHomeOuterNavScreen_t2515452089;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackHomeOuterNavScreen::.ctor()
extern "C"  void AttackHomeOuterNavScreen__ctor_m3929422738 (AttackHomeOuterNavScreen_t2515452089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeOuterNavScreen::Start()
extern "C"  void AttackHomeOuterNavScreen_Start_m3278989978 (AttackHomeOuterNavScreen_t2515452089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeOuterNavScreen::UIClosing()
extern "C"  void AttackHomeOuterNavScreen_UIClosing_m1627997675 (AttackHomeOuterNavScreen_t2515452089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeOuterNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackHomeOuterNavScreen_ScreenLoad_m2667384366 (AttackHomeOuterNavScreen_t2515452089 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeOuterNavScreen::UpdateUI()
extern "C"  void AttackHomeOuterNavScreen_UpdateUI_m3178459769 (AttackHomeOuterNavScreen_t2515452089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeOuterNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackHomeOuterNavScreen_ScreenUnload_m4251434606 (AttackHomeOuterNavScreen_t2515452089 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackHomeOuterNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackHomeOuterNavScreen_ValidateScreenNavigation_m1744370203 (AttackHomeOuterNavScreen_t2515452089 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
