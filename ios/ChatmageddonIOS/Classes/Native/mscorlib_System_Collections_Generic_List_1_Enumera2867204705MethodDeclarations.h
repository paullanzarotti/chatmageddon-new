﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PurchasableItem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1386828110(__this, ___l0, method) ((  void (*) (Enumerator_t2867204705 *, List_1_t3332475031 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PurchasableItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1299267068(__this, method) ((  void (*) (Enumerator_t2867204705 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PurchasableItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3903732568(__this, method) ((  Il2CppObject * (*) (Enumerator_t2867204705 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PurchasableItem>::Dispose()
#define Enumerator_Dispose_m672728491(__this, method) ((  void (*) (Enumerator_t2867204705 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PurchasableItem>::VerifyState()
#define Enumerator_VerifyState_m3887646404(__this, method) ((  void (*) (Enumerator_t2867204705 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PurchasableItem>::MoveNext()
#define Enumerator_MoveNext_m1572266815(__this, method) ((  bool (*) (Enumerator_t2867204705 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PurchasableItem>::get_Current()
#define Enumerator_get_Current_m2306968291(__this, method) ((  PurchasableItem_t3963353899 * (*) (Enumerator_t2867204705 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
