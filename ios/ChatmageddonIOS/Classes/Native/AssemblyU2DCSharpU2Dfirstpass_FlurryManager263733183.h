﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "P31RestKit_Prime31_AbstractManager1005944057.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlurryManager
struct  FlurryManager_t263733183  : public AbstractManager_t1005944057
{
public:

public:
};

struct FlurryManager_t263733183_StaticFields
{
public:
	// System.Action`1<System.String> FlurryManager::spaceDidDismissEvent
	Action_1_t1831019615 * ___spaceDidDismissEvent_5;
	// System.Action`1<System.String> FlurryManager::spaceWillLeaveApplicationEvent
	Action_1_t1831019615 * ___spaceWillLeaveApplicationEvent_6;
	// System.Action`1<System.String> FlurryManager::spaceDidFailToRenderEvent
	Action_1_t1831019615 * ___spaceDidFailToRenderEvent_7;
	// System.Action`1<System.String> FlurryManager::spaceDidFailToReceiveAdEvent
	Action_1_t1831019615 * ___spaceDidFailToReceiveAdEvent_8;
	// System.Action`1<System.String> FlurryManager::spaceDidReceiveAdEvent
	Action_1_t1831019615 * ___spaceDidReceiveAdEvent_9;
	// System.Action`1<System.String> FlurryManager::videoDidFinishEvent
	Action_1_t1831019615 * ___videoDidFinishEvent_10;

public:
	inline static int32_t get_offset_of_spaceDidDismissEvent_5() { return static_cast<int32_t>(offsetof(FlurryManager_t263733183_StaticFields, ___spaceDidDismissEvent_5)); }
	inline Action_1_t1831019615 * get_spaceDidDismissEvent_5() const { return ___spaceDidDismissEvent_5; }
	inline Action_1_t1831019615 ** get_address_of_spaceDidDismissEvent_5() { return &___spaceDidDismissEvent_5; }
	inline void set_spaceDidDismissEvent_5(Action_1_t1831019615 * value)
	{
		___spaceDidDismissEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___spaceDidDismissEvent_5, value);
	}

	inline static int32_t get_offset_of_spaceWillLeaveApplicationEvent_6() { return static_cast<int32_t>(offsetof(FlurryManager_t263733183_StaticFields, ___spaceWillLeaveApplicationEvent_6)); }
	inline Action_1_t1831019615 * get_spaceWillLeaveApplicationEvent_6() const { return ___spaceWillLeaveApplicationEvent_6; }
	inline Action_1_t1831019615 ** get_address_of_spaceWillLeaveApplicationEvent_6() { return &___spaceWillLeaveApplicationEvent_6; }
	inline void set_spaceWillLeaveApplicationEvent_6(Action_1_t1831019615 * value)
	{
		___spaceWillLeaveApplicationEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___spaceWillLeaveApplicationEvent_6, value);
	}

	inline static int32_t get_offset_of_spaceDidFailToRenderEvent_7() { return static_cast<int32_t>(offsetof(FlurryManager_t263733183_StaticFields, ___spaceDidFailToRenderEvent_7)); }
	inline Action_1_t1831019615 * get_spaceDidFailToRenderEvent_7() const { return ___spaceDidFailToRenderEvent_7; }
	inline Action_1_t1831019615 ** get_address_of_spaceDidFailToRenderEvent_7() { return &___spaceDidFailToRenderEvent_7; }
	inline void set_spaceDidFailToRenderEvent_7(Action_1_t1831019615 * value)
	{
		___spaceDidFailToRenderEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___spaceDidFailToRenderEvent_7, value);
	}

	inline static int32_t get_offset_of_spaceDidFailToReceiveAdEvent_8() { return static_cast<int32_t>(offsetof(FlurryManager_t263733183_StaticFields, ___spaceDidFailToReceiveAdEvent_8)); }
	inline Action_1_t1831019615 * get_spaceDidFailToReceiveAdEvent_8() const { return ___spaceDidFailToReceiveAdEvent_8; }
	inline Action_1_t1831019615 ** get_address_of_spaceDidFailToReceiveAdEvent_8() { return &___spaceDidFailToReceiveAdEvent_8; }
	inline void set_spaceDidFailToReceiveAdEvent_8(Action_1_t1831019615 * value)
	{
		___spaceDidFailToReceiveAdEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___spaceDidFailToReceiveAdEvent_8, value);
	}

	inline static int32_t get_offset_of_spaceDidReceiveAdEvent_9() { return static_cast<int32_t>(offsetof(FlurryManager_t263733183_StaticFields, ___spaceDidReceiveAdEvent_9)); }
	inline Action_1_t1831019615 * get_spaceDidReceiveAdEvent_9() const { return ___spaceDidReceiveAdEvent_9; }
	inline Action_1_t1831019615 ** get_address_of_spaceDidReceiveAdEvent_9() { return &___spaceDidReceiveAdEvent_9; }
	inline void set_spaceDidReceiveAdEvent_9(Action_1_t1831019615 * value)
	{
		___spaceDidReceiveAdEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___spaceDidReceiveAdEvent_9, value);
	}

	inline static int32_t get_offset_of_videoDidFinishEvent_10() { return static_cast<int32_t>(offsetof(FlurryManager_t263733183_StaticFields, ___videoDidFinishEvent_10)); }
	inline Action_1_t1831019615 * get_videoDidFinishEvent_10() const { return ___videoDidFinishEvent_10; }
	inline Action_1_t1831019615 ** get_address_of_videoDidFinishEvent_10() { return &___videoDidFinishEvent_10; }
	inline void set_videoDidFinishEvent_10(Action_1_t1831019615 * value)
	{
		___videoDidFinishEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___videoDidFinishEvent_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
