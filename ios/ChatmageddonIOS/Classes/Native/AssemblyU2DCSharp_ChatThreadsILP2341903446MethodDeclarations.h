﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatThreadsILP
struct ChatThreadsILP_t2341903446;
// ChatMessage
struct ChatMessage_t2384228687;
// ChatThreadsListItem
struct ChatThreadsListItem_t303868848;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// ChatThread
struct ChatThread_t2394323482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatMessage2384228687.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_ChatThread2394323482.h"

// System.Void ChatThreadsILP::.ctor()
extern "C"  void ChatThreadsILP__ctor_m3199016757 (ChatThreadsILP_t2341903446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::Start()
extern "C"  void ChatThreadsILP_Start_m3083613077 (ChatThreadsILP_t2341903446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::StartIL()
extern "C"  void ChatThreadsILP_StartIL_m986704860 (ChatThreadsILP_t2341903446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::SetDataArray()
extern "C"  void ChatThreadsILP_SetDataArray_m4207899724 (ChatThreadsILP_t2341903446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::UpdateItemData(ChatMessage)
extern "C"  void ChatThreadsILP_UpdateItemData_m2457523700 (ChatThreadsILP_t2341903446 * __this, ChatMessage_t2384228687 * ___newMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChatThreadsListItem ChatThreadsILP::GetNonGroupItemByUserID(System.String)
extern "C"  ChatThreadsListItem_t303868848 * ChatThreadsILP_GetNonGroupItemByUserID_m4040436278 (ChatThreadsILP_t2341903446 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::UpdatePopulation()
extern "C"  void ChatThreadsILP_UpdatePopulation_m4238037375 (ChatThreadsILP_t2341903446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::SetScrollToBottom()
extern "C"  void ChatThreadsILP_SetScrollToBottom_m1426004878 (ChatThreadsILP_t2341903446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void ChatThreadsILP_InitListItemWithIndex_m169818183 (ChatThreadsILP_t2341903446 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void ChatThreadsILP_PopulateListItemWithIndex_m4075033034 (ChatThreadsILP_t2341903446 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ChatThreadsILP::<SetDataArray>m__0(ChatThread)
extern "C"  DateTime_t693205669  ChatThreadsILP_U3CSetDataArrayU3Em__0_m1726496448 (Il2CppObject * __this /* static, unused */, ChatThread_t2394323482 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ChatThreadsILP::<SetDataArray>m__1(ChatThread)
extern "C"  DateTime_t693205669  ChatThreadsILP_U3CSetDataArrayU3Em__1_m2707971621 (Il2CppObject * __this /* static, unused */, ChatThread_t2394323482 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
