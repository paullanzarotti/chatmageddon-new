﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackInventoryNavScreen
struct AttackInventoryNavScreen_t33214553;

#include "AssemblyU2DCSharp_ItemSwipeController3011021799.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileSwipeController
struct  MissileSwipeController_t2686719534  : public ItemSwipeController_t3011021799
{
public:
	// AttackInventoryNavScreen MissileSwipeController::screen
	AttackInventoryNavScreen_t33214553 * ___screen_23;

public:
	inline static int32_t get_offset_of_screen_23() { return static_cast<int32_t>(offsetof(MissileSwipeController_t2686719534, ___screen_23)); }
	inline AttackInventoryNavScreen_t33214553 * get_screen_23() const { return ___screen_23; }
	inline AttackInventoryNavScreen_t33214553 ** get_address_of_screen_23() { return &___screen_23; }
	inline void set_screen_23(AttackInventoryNavScreen_t33214553 * value)
	{
		___screen_23 = value;
		Il2CppCodeGenWriteBarrier(&___screen_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
