﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.FlyweightMapStorage
struct FlyweightMapStorage_t1600303080;
// System.String
struct String_t;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>
struct SortedDictionary_2_t2686657757;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// PhoneNumbers.FlyweightMapStorage/ByteBuffer
struct ByteBuffer_t1505679999;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_FlyweightMapStorage1505679999.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.FlyweightMapStorage::.ctor()
extern "C"  void FlyweightMapStorage__ctor_m1119722435 (FlyweightMapStorage_t1600303080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage::getPrefix(System.Int32)
extern "C"  int32_t FlyweightMapStorage_getPrefix_m1219461542 (FlyweightMapStorage_t1600303080 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage::getStorageSize()
extern "C"  int32_t FlyweightMapStorage_getStorageSize_m3338759447 (FlyweightMapStorage_t1600303080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.FlyweightMapStorage::getDescription(System.Int32)
extern "C"  String_t* FlyweightMapStorage_getDescription_m6422535 (FlyweightMapStorage_t1600303080 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.FlyweightMapStorage::readFromSortedMap(System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>)
extern "C"  void FlyweightMapStorage_readFromSortedMap_m1131363663 (FlyweightMapStorage_t1600303080 * __this, SortedDictionary_2_t2686657757 * ___areaCodeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.FlyweightMapStorage::createDescriptionPool(System.Collections.Generic.HashSet`1<System.String>,System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>)
extern "C"  void FlyweightMapStorage_createDescriptionPool_m1073673484 (FlyweightMapStorage_t1600303080 * __this, HashSet_1_t362681087 * ___descriptionsSet0, SortedDictionary_2_t2686657757 * ___areaCodeMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage::getOptimalNumberOfBytesForValue(System.Int32)
extern "C"  int32_t FlyweightMapStorage_getOptimalNumberOfBytesForValue_m1794751831 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.FlyweightMapStorage::storeWordInBuffer(PhoneNumbers.FlyweightMapStorage/ByteBuffer,System.Int32,System.Int32,System.Int32)
extern "C"  void FlyweightMapStorage_storeWordInBuffer_m3490000133 (Il2CppObject * __this /* static, unused */, ByteBuffer_t1505679999 * ___buffer0, int32_t ___wordSize1, int32_t ___index2, int32_t ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage::readWordFromBuffer(PhoneNumbers.FlyweightMapStorage/ByteBuffer,System.Int32,System.Int32)
extern "C"  int32_t FlyweightMapStorage_readWordFromBuffer_m1078948522 (Il2CppObject * __this /* static, unused */, ByteBuffer_t1505679999 * ___buffer0, int32_t ___wordSize1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.FlyweightMapStorage::.cctor()
extern "C"  void FlyweightMapStorage__cctor_m1468174868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage::<getStorageSize>m__0(System.String)
extern "C"  int32_t FlyweightMapStorage_U3CgetStorageSizeU3Em__0_m1603818112 (Il2CppObject * __this /* static, unused */, String_t* ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
