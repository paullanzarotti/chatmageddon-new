﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FAQ
struct  FAQ_t2837997648  : public Il2CppObject
{
public:
	// System.String FAQ::question
	String_t* ___question_0;
	// System.String FAQ::anwser
	String_t* ___anwser_1;

public:
	inline static int32_t get_offset_of_question_0() { return static_cast<int32_t>(offsetof(FAQ_t2837997648, ___question_0)); }
	inline String_t* get_question_0() const { return ___question_0; }
	inline String_t** get_address_of_question_0() { return &___question_0; }
	inline void set_question_0(String_t* value)
	{
		___question_0 = value;
		Il2CppCodeGenWriteBarrier(&___question_0, value);
	}

	inline static int32_t get_offset_of_anwser_1() { return static_cast<int32_t>(offsetof(FAQ_t2837997648, ___anwser_1)); }
	inline String_t* get_anwser_1() const { return ___anwser_1; }
	inline String_t** get_address_of_anwser_1() { return &___anwser_1; }
	inline void set_anwser_1(String_t* value)
	{
		___anwser_1 = value;
		Il2CppCodeGenWriteBarrier(&___anwser_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
