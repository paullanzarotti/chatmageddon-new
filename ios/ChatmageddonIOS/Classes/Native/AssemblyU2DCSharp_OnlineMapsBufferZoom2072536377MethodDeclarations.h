﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBufferZoom
struct OnlineMapsBufferZoom_t2072536377;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsBufferZoom::.ctor(System.Int32)
extern "C"  void OnlineMapsBufferZoom__ctor_m4268297837 (OnlineMapsBufferZoom_t2072536377 * __this, int32_t ___zoom0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
