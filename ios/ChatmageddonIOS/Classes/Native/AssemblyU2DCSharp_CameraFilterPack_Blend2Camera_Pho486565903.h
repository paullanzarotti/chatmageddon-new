﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Ph3117785195.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blend2Camera_PhotoshopFilters
struct  CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Blend2Camera_PhotoshopFilters::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_PhotoshopFilters::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_PhotoshopFilters::Camera2
	Camera_t189460977 * ___Camera2_4;
	// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters CameraFilterPack_Blend2Camera_PhotoshopFilters::filterchoice
	int32_t ___filterchoice_5;
	// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters CameraFilterPack_Blend2Camera_PhotoshopFilters::filterchoicememo
	int32_t ___filterchoicememo_6;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::TimeX
	float ___TimeX_7;
	// UnityEngine.Vector4 CameraFilterPack_Blend2Camera_PhotoshopFilters::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_8;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_PhotoshopFilters::SCMaterial
	Material_t193706927 * ___SCMaterial_9;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_10;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::BlendFX
	float ___BlendFX_11;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_PhotoshopFilters::Camera2tex
	RenderTexture_t2666733923 * ___Camera2tex_14;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_Camera2_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___Camera2_4)); }
	inline Camera_t189460977 * get_Camera2_4() const { return ___Camera2_4; }
	inline Camera_t189460977 ** get_address_of_Camera2_4() { return &___Camera2_4; }
	inline void set_Camera2_4(Camera_t189460977 * value)
	{
		___Camera2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2_4, value);
	}

	inline static int32_t get_offset_of_filterchoice_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___filterchoice_5)); }
	inline int32_t get_filterchoice_5() const { return ___filterchoice_5; }
	inline int32_t* get_address_of_filterchoice_5() { return &___filterchoice_5; }
	inline void set_filterchoice_5(int32_t value)
	{
		___filterchoice_5 = value;
	}

	inline static int32_t get_offset_of_filterchoicememo_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___filterchoicememo_6)); }
	inline int32_t get_filterchoicememo_6() const { return ___filterchoicememo_6; }
	inline int32_t* get_address_of_filterchoicememo_6() { return &___filterchoicememo_6; }
	inline void set_filterchoicememo_6(int32_t value)
	{
		___filterchoicememo_6 = value;
	}

	inline static int32_t get_offset_of_TimeX_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___TimeX_7)); }
	inline float get_TimeX_7() const { return ___TimeX_7; }
	inline float* get_address_of_TimeX_7() { return &___TimeX_7; }
	inline void set_TimeX_7(float value)
	{
		___TimeX_7 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___ScreenResolution_8)); }
	inline Vector4_t2243707581  get_ScreenResolution_8() const { return ___ScreenResolution_8; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_8() { return &___ScreenResolution_8; }
	inline void set_ScreenResolution_8(Vector4_t2243707581  value)
	{
		___ScreenResolution_8 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___SCMaterial_9)); }
	inline Material_t193706927 * get_SCMaterial_9() const { return ___SCMaterial_9; }
	inline Material_t193706927 ** get_address_of_SCMaterial_9() { return &___SCMaterial_9; }
	inline void set_SCMaterial_9(Material_t193706927 * value)
	{
		___SCMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_9, value);
	}

	inline static int32_t get_offset_of_SwitchCameraToCamera2_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___SwitchCameraToCamera2_10)); }
	inline float get_SwitchCameraToCamera2_10() const { return ___SwitchCameraToCamera2_10; }
	inline float* get_address_of_SwitchCameraToCamera2_10() { return &___SwitchCameraToCamera2_10; }
	inline void set_SwitchCameraToCamera2_10(float value)
	{
		___SwitchCameraToCamera2_10 = value;
	}

	inline static int32_t get_offset_of_BlendFX_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___BlendFX_11)); }
	inline float get_BlendFX_11() const { return ___BlendFX_11; }
	inline float* get_address_of_BlendFX_11() { return &___BlendFX_11; }
	inline void set_BlendFX_11(float value)
	{
		___BlendFX_11 = value;
	}

	inline static int32_t get_offset_of_Camera2tex_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903, ___Camera2tex_14)); }
	inline RenderTexture_t2666733923 * get_Camera2tex_14() const { return ___Camera2tex_14; }
	inline RenderTexture_t2666733923 ** get_address_of_Camera2tex_14() { return &___Camera2tex_14; }
	inline void set_Camera2tex_14(RenderTexture_t2666733923 * value)
	{
		___Camera2tex_14 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2tex_14, value);
	}
};

struct CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903_StaticFields
{
public:
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::ChangeValue
	float ___ChangeValue_12;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::ChangeValue2
	float ___ChangeValue2_13;

public:
	inline static int32_t get_offset_of_ChangeValue_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903_StaticFields, ___ChangeValue_12)); }
	inline float get_ChangeValue_12() const { return ___ChangeValue_12; }
	inline float* get_address_of_ChangeValue_12() { return &___ChangeValue_12; }
	inline void set_ChangeValue_12(float value)
	{
		___ChangeValue_12 = value;
	}

	inline static int32_t get_offset_of_ChangeValue2_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903_StaticFields, ___ChangeValue2_13)); }
	inline float get_ChangeValue2_13() const { return ___ChangeValue2_13; }
	inline float* get_address_of_ChangeValue2_13() { return &___ChangeValue2_13; }
	inline void set_ChangeValue2_13(float value)
	{
		___ChangeValue2_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
