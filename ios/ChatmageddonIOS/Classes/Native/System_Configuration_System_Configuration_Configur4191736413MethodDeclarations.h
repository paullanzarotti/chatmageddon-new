﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationElementProperty
struct ConfigurationElementProperty_t4191736413;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Configura210547623.h"

// System.Void System.Configuration.ConfigurationElementProperty::.ctor(System.Configuration.ConfigurationValidatorBase)
extern "C"  void ConfigurationElementProperty__ctor_m3182969220 (ConfigurationElementProperty_t4191736413 * __this, ConfigurationValidatorBase_t210547623 * ___validator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationElementProperty::get_Validator()
extern "C"  ConfigurationValidatorBase_t210547623 * ConfigurationElementProperty_get_Validator_m138672906 (ConfigurationElementProperty_t4191736413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
