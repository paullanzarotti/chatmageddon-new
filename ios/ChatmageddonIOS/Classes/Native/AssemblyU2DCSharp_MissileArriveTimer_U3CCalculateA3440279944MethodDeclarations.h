﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileArriveTimer/<CalculateArriveTime>c__Iterator0
struct U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileArriveTimer/<CalculateArriveTime>c__Iterator0::.ctor()
extern "C"  void U3CCalculateArriveTimeU3Ec__Iterator0__ctor_m207912033 (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MissileArriveTimer/<CalculateArriveTime>c__Iterator0::MoveNext()
extern "C"  bool U3CCalculateArriveTimeU3Ec__Iterator0_MoveNext_m4140665235 (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MissileArriveTimer/<CalculateArriveTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculateArriveTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4099069707 (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MissileArriveTimer/<CalculateArriveTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculateArriveTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18146051 (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer/<CalculateArriveTime>c__Iterator0::Dispose()
extern "C"  void U3CCalculateArriveTimeU3Ec__Iterator0_Dispose_m2008677122 (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer/<CalculateArriveTime>c__Iterator0::Reset()
extern "C"  void U3CCalculateArriveTimeU3Ec__Iterator0_Reset_m2814204920 (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
