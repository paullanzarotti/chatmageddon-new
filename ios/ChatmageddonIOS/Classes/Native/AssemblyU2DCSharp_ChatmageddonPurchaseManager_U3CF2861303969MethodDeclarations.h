﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonPurchaseManager/<FinaliseBucksPurchase>c__AnonStorey0
struct U3CFinaliseBucksPurchaseU3Ec__AnonStorey0_t2861303969;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatmageddonPurchaseManager/<FinaliseBucksPurchase>c__AnonStorey0::.ctor()
extern "C"  void U3CFinaliseBucksPurchaseU3Ec__AnonStorey0__ctor_m3176278600 (U3CFinaliseBucksPurchaseU3Ec__AnonStorey0_t2861303969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPurchaseManager/<FinaliseBucksPurchase>c__AnonStorey0::<>m__0(System.Boolean,System.String)
extern "C"  void U3CFinaliseBucksPurchaseU3Ec__AnonStorey0_U3CU3Em__0_m2647107562 (U3CFinaliseBucksPurchaseU3Ec__AnonStorey0_t2861303969 * __this, bool ___success0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
