﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FAQListItem
struct FAQListItem_t1761156371;
// FAQ
struct FAQ_t2837997648;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FAQ2837997648.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void FAQListItem::.ctor()
extern "C"  void FAQListItem__ctor_m2547038238 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::SetUpItem()
extern "C"  void FAQListItem_SetUpItem_m3649138584 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::PopulateItem(FAQ)
extern "C"  void FAQListItem_PopulateItem_m1511172691 (FAQListItem_t1761156371 * __this, FAQ_t2837997648 * ___faq0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::SetVisible()
extern "C"  void FAQListItem_SetVisible_m966800494 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::SetInvisible()
extern "C"  void FAQListItem_SetInvisible_m2949331949 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FAQListItem::verifyVisibility()
extern "C"  bool FAQListItem_verifyVisibility_m3053766659 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::CheckVisibilty()
extern "C"  void FAQListItem_CheckVisibilty_m2300929143 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::Update()
extern "C"  void FAQListItem_Update_m680196627 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::OnEnable()
extern "C"  void FAQListItem_OnEnable_m3110354586 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::FindScrollView()
extern "C"  void FAQListItem_FindScrollView_m855118203 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::OnPress(System.Boolean)
extern "C"  void FAQListItem_OnPress_m4140621875 (FAQListItem_t1761156371 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void FAQListItem_OnDrag_m633290019 (FAQListItem_t1761156371 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::OnDragEnd()
extern "C"  void FAQListItem_OnDragEnd_m2726533602 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::OnScroll(System.Single)
extern "C"  void FAQListItem_OnScroll_m534308643 (FAQListItem_t1761156371 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItem::OnClick()
extern "C"  void FAQListItem_OnClick_m3445994621 (FAQListItem_t1761156371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
