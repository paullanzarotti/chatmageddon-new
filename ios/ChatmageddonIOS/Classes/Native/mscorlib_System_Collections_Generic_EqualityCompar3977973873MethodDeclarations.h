﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<LeaderboardNavScreen>
struct DefaultComparer_t3977973873;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<LeaderboardNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2207015496_gshared (DefaultComparer_t3977973873 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2207015496(__this, method) ((  void (*) (DefaultComparer_t3977973873 *, const MethodInfo*))DefaultComparer__ctor_m2207015496_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<LeaderboardNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m273847331_gshared (DefaultComparer_t3977973873 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m273847331(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3977973873 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m273847331_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<LeaderboardNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m509408003_gshared (DefaultComparer_t3977973873 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m509408003(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3977973873 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m509408003_gshared)(__this, ___x0, ___y1, method)
