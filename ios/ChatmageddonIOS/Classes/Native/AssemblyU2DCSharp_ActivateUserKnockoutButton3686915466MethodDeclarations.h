﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActivateUserKnockoutButton
struct ActivateUserKnockoutButton_t3686915466;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ActivateUserKnockoutButton::.ctor()
extern "C"  void ActivateUserKnockoutButton__ctor_m4032040725 (ActivateUserKnockoutButton_t3686915466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateUserKnockoutButton::Awake()
extern "C"  void ActivateUserKnockoutButton_Awake_m1625489370 (ActivateUserKnockoutButton_t3686915466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateUserKnockoutButton::OnButtonClick()
extern "C"  void ActivateUserKnockoutButton_OnButtonClick_m3056884078 (ActivateUserKnockoutButton_t3686915466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateUserKnockoutButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ActivateUserKnockoutButton_U3COnButtonClickU3Em__0_m436070666 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
