﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.GoogleCloudMessagingUI
struct GoogleCloudMessagingUI_t481364860;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.GoogleCloudMessagingUI::.ctor()
extern "C"  void GoogleCloudMessagingUI__ctor_m1229795430 (GoogleCloudMessagingUI_t481364860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
