﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.Transform
struct Transform_t729562346;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Security.Cryptography.Xml.Transform::.ctor()
extern "C"  void Transform__ctor_m3309061576 (Transform_t729562346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.Transform::set_Algorithm(System.String)
extern "C"  void Transform_set_Algorithm_m2946932632 (Transform_t729562346 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Security.Cryptography.Xml.Transform::get_PropagatedNamespaces()
extern "C"  Hashtable_t909839986 * Transform_get_PropagatedNamespaces_m2295686495 (Transform_t729562346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.Transform::GetXml()
extern "C"  XmlElement_t2877111883 * Transform_GetXml_m238924513 (Transform_t729562346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlResolver System.Security.Cryptography.Xml.Transform::GetResolver()
extern "C"  XmlResolver_t2024571559 * Transform_GetResolver_m2588053450 (Transform_t729562346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
