﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FindPlacesExample
struct FindPlacesExample_t604743347;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FindPlacesExample::.ctor()
extern "C"  void FindPlacesExample__ctor_m2359405730 (FindPlacesExample_t604743347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FindPlacesExample::Start()
extern "C"  void FindPlacesExample_Start_m4223065678 (FindPlacesExample_t604743347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FindPlacesExample::OnComplete(System.String)
extern "C"  void FindPlacesExample_OnComplete_m3215923252 (FindPlacesExample_t604743347 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
