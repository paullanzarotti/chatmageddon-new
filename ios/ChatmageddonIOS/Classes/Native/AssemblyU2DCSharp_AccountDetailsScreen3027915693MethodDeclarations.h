﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccountDetailsScreen
struct AccountDetailsScreen_t3027915693;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AccountDetailsScreen::.ctor()
extern "C"  void AccountDetailsScreen__ctor_m3556738502 (AccountDetailsScreen_t3027915693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccountDetailsScreen::Start()
extern "C"  void AccountDetailsScreen_Start_m2831495950 (AccountDetailsScreen_t3027915693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccountDetailsScreen::AccountDetailsUIClosing()
extern "C"  void AccountDetailsScreen_AccountDetailsUIClosing_m1356889318 (AccountDetailsScreen_t3027915693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccountDetailsScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AccountDetailsScreen_ScreenLoad_m3890117474 (AccountDetailsScreen_t3027915693 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccountDetailsScreen::ScreenUnload(NavigationDirection)
extern "C"  void AccountDetailsScreen_ScreenUnload_m2415228314 (AccountDetailsScreen_t3027915693 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AccountDetailsScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AccountDetailsScreen_ValidateScreenNavigation_m2356041759 (AccountDetailsScreen_t3027915693 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
