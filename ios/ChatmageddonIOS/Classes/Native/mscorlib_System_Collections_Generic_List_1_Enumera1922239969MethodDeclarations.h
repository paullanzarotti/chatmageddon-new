﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FriendSearchNavScreen>
struct List_1_t2387510295;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1922239969.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1600169796_gshared (Enumerator_t1922239969 * __this, List_1_t2387510295 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1600169796(__this, ___l0, method) ((  void (*) (Enumerator_t1922239969 *, List_1_t2387510295 *, const MethodInfo*))Enumerator__ctor_m1600169796_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2020615350_gshared (Enumerator_t1922239969 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2020615350(__this, method) ((  void (*) (Enumerator_t1922239969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2020615350_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1248153002_gshared (Enumerator_t1922239969 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1248153002(__this, method) ((  Il2CppObject * (*) (Enumerator_t1922239969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1248153002_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m4172531735_gshared (Enumerator_t1922239969 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4172531735(__this, method) ((  void (*) (Enumerator_t1922239969 *, const MethodInfo*))Enumerator_Dispose_m4172531735_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4236876174_gshared (Enumerator_t1922239969 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4236876174(__this, method) ((  void (*) (Enumerator_t1922239969 *, const MethodInfo*))Enumerator_VerifyState_m4236876174_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1444547198_gshared (Enumerator_t1922239969 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1444547198(__this, method) ((  bool (*) (Enumerator_t1922239969 *, const MethodInfo*))Enumerator_MoveNext_m1444547198_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FriendSearchNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3617490607_gshared (Enumerator_t1922239969 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3617490607(__this, method) ((  int32_t (*) (Enumerator_t1922239969 *, const MethodInfo*))Enumerator_get_Current_m3617490607_gshared)(__this, method)
