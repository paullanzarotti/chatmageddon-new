﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t3962498969;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Data
struct Data_t3569509720;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;
// LitJson.JsonReader
struct JsonReader_t1077921503;
// System.IO.TextReader
struct TextReader_t1561828458;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// PlayerPrefSaveData
struct PlayerPrefSaveData_t170835669;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Type
struct Type_t;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// IPCycler
struct IPCycler_t1336138445;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// UnityUtil
struct UnityUtil_t671748753;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t3035069757;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3369346583;
// System.Func`3<UnityEngine.Vector2,System.Object,UnityEngine.Vector2>
struct Func_3_t1022329359;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t2535834625;
// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_3_t246581160;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t106372939;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2364004493;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t3975231481;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t330981690;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Collections.Generic.IEnumerable`1<System.Int16>
struct IEnumerable_1_t38405663;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Func`3<System.Object,System.Single,System.Single>
struct Func_3_t4013867779;
// System.Func`3<UnityEngine.Vector3,System.Single,System.Single>
struct Func_3_t3779166440;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1186599945;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "AssemblyU2DCSharp_Data3569509720.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_Hashtable909839986MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory2270101009.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory2270101009MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory_IfNo643838650.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "Facebook_Unity_Facebook_Unity_Utilities2249569324.h"
#include "mscorlib_System_Int64909078037.h"
#include "Facebook_Unity_Facebook_Unity_FacebookLogger821309934MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_Utilities2249569324MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper800426905.h"
#include "AssemblyU2DCSharp_LitJson_JsonReader1077921503.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper800426905MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "AssemblyU2DCSharp_LitJson_JsonReader1077921503MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools2004302824.h"
#include "AssemblyU2DCSharp_NGUITools2004302824MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget1453041918MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_UIWidget1453041918.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Exception1927440687.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlElement2877111883MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode616554813MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387MethodDeclarations.h"
#include "AssemblyU2DCSharp_OnlineMapsUtils2843619045MethodDeclarations.h"
#include "AssemblyU2DCSharp_OnlineMapsRange3791609909MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_OnlineMapsRange3791609909.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerPrefSaveData170835669.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream743994179MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "P31RestKit_Prime31_Json1852637766.h"
#include "P31RestKit_Prime31_Json1852637766MethodDeclarations.h"
#include "P31RestKit_Prime31_SimpleJson4140114772MethodDeclarations.h"
#include "P31RestKit_Prime31_SimpleJson4140114772.h"
#include "P31RestKit_Prime31_Json_ObjectDecoder1026857897MethodDeclarations.h"
#include "P31RestKit_Prime31_Json_ObjectDecoder1026857897.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "P31RestKit_Prime31_Utils499752281MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "mscorlib_System_Activator1850728717.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_ClientM624279968.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_Mono_Security_Uri_UriScheme683497865.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "AssemblyU2DCSharp_OnlineMapsEvents850667009.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder_Code3472511518.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "P31RestKit_Prime31_JsonFormatter_JsonContextType3787516849.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li247561424.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21603735834.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22812303849.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653207108.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21345327748.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21043231486.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22369073723.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23888080226.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_322707255.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22387876582.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22335466038.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21020750381.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "mscorlib_System_Collections_Hashtable_Slot2022531261.h"
#include "mscorlib_System_Collections_SortedList_Slot2267560602.h"
#include "System_System_ComponentModel_PropertyTabScope2485003348.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3712112744.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelF4090909514.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo149559338.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "mscorlib_System_Reflection_Emit_MonoResource3127387157.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS2708608433.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCa333236149.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI3933049236.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi141209596.h"
#include "mscorlib_System_SByte454417549.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "mscorlib_System_TermInfoStrings1425267120.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_UInt642909196914.h"
#include "System_System_Uri_UriScheme1876590943.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XPath_XPathResultType1521569578.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry3041229383.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry974746545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1028629049.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "AssemblyU2DCSharp_UITweener2986641582.h"
#include "AssemblyU2DCSharp_UITweener2986641582MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener_Style4221671544.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_MiniJson615358805.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2745005863.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityUtil671748753.h"
#include "AssemblyU2DCSharp_IPCycler1336138445.h"
#include "AssemblyU2DCSharp_IPCycler1336138445MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData3093286891.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Func_3_gen3369346583.h"
#include "System_Core_System_Linq_Check578192424MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3369346583MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1022329359.h"
#include "System_Core_System_Func_3_gen1022329359MethodDeclarations.h"
#include "System_Core_System_Func_3_gen246581160.h"
#include "System_Core_System_Func_3_gen246581160MethodDeclarations.h"
#include "mscorlib_System_Converter_2_gen106372939.h"
#include "mscorlib_System_Converter_2_gen106372939MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "System_Core_System_Linq_Enumerable_Fallback2408918324.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3702943073.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3702943073MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3410367046.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3410367046MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712MethodDeclarations.h"
#include "System_Core_System_Func_3_gen4013867779.h"
#include "System_Core_System_Func_3_gen4013867779MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3779166440.h"
#include "System_Core_System_Func_3_gen3779166440MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1186599945.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190.h"

// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisCorner_t1077473318_m1354090789_gshared (LayoutGroup_t3962498969 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisCorner_t1077473318_m1354090789(__this, ___currentValue0, ___newValue1, method) ((  void (*) (LayoutGroup_t3962498969 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisCorner_t1077473318_m1354090789_gshared)(__this, ___currentValue0, ___newValue1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisVector2_t2243707579_m3010153489_gshared (LayoutGroup_t3962498969 * __this, Vector2_t2243707579 * ___currentValue0, Vector2_t2243707579  ___newValue1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisVector2_t2243707579_m3010153489(__this, ___currentValue0, ___newValue1, method) ((  void (*) (LayoutGroup_t3962498969 *, Vector2_t2243707579 *, Vector2_t2243707579 , const MethodInfo*))LayoutGroup_SetProperty_TisVector2_t2243707579_m3010153489_gshared)(__this, ___currentValue0, ___newValue1, method)
// T Data::Get<System.Object>(System.String)
extern "C"  Il2CppObject * Data_Get_TisIl2CppObject_m4245298088_gshared (Data_t3569509720 * __this, String_t* ___key0, const MethodInfo* method);
#define Data_Get_TisIl2CppObject_m4245298088(__this, ___key0, method) ((  Il2CppObject * (*) (Data_t3569509720 *, String_t*, const MethodInfo*))Data_Get_TisIl2CppObject_m4245298088_gshared)(__this, ___key0, method)
// T Facebook.Unity.ComponentFactory::AddComponent<System.Object>()
extern "C"  Il2CppObject * ComponentFactory_AddComponent_TisIl2CppObject_m2420316583_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ComponentFactory_AddComponent_TisIl2CppObject_m2420316583(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ComponentFactory_AddComponent_TisIl2CppObject_m2420316583_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4011380260_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4011380260(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4011380260_gshared)(__this, method)
// T Facebook.Unity.ComponentFactory::GetComponent<System.Object>(Facebook.Unity.ComponentFactory/IfNotExist)
extern "C"  Il2CppObject * ComponentFactory_GetComponent_TisIl2CppObject_m3641920544_gshared (Il2CppObject * __this /* static, unused */, int32_t ___ifNotExist0, const MethodInfo* method);
#define ComponentFactory_GetComponent_TisIl2CppObject_m3641920544(__this /* static, unused */, ___ifNotExist0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ComponentFactory_GetComponent_TisIl2CppObject_m3641920544_gshared)(__this /* static, unused */, ___ifNotExist0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// T Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic<System.Object>(System.String)
extern "C"  Il2CppObject * IAndroidWrapper_CallStatic_TisIl2CppObject_m1646114546_gshared (Il2CppObject * __this, String_t* ___methodName0, const MethodInfo* method);
#define IAndroidWrapper_CallStatic_TisIl2CppObject_m1646114546(__this, ___methodName0, method) ((  Il2CppObject * (*) (Il2CppObject *, String_t*, const MethodInfo*))IAndroidWrapper_CallStatic_TisIl2CppObject_m1646114546_gshared)(__this, ___methodName0, method)
// T Facebook.Unity.Utilities::GetValueOrDefault<System.Int64>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
extern "C"  int64_t Utilities_GetValueOrDefault_TisInt64_t909078037_m3442176644_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___dictionary0, String_t* ___key1, bool ___logWarning2, const MethodInfo* method);
#define Utilities_GetValueOrDefault_TisInt64_t909078037_m3442176644(__this /* static, unused */, ___dictionary0, ___key1, ___logWarning2, method) ((  int64_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, bool, const MethodInfo*))Utilities_GetValueOrDefault_TisInt64_t909078037_m3442176644_gshared)(__this /* static, unused */, ___dictionary0, ___key1, ___logWarning2, method)
// System.Boolean Facebook.Unity.Utilities::TryGetValue<System.Int64>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,T&)
extern "C"  bool Utilities_TryGetValue_TisInt64_t909078037_m4058319583_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___dictionary0, String_t* ___key1, int64_t* ___value2, const MethodInfo* method);
#define Utilities_TryGetValue_TisInt64_t909078037_m4058319583(__this /* static, unused */, ___dictionary0, ___key1, ___value2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, int64_t*, const MethodInfo*))Utilities_TryGetValue_TisInt64_t909078037_m4058319583_gshared)(__this /* static, unused */, ___dictionary0, ___key1, ___value2, method)
// T Facebook.Unity.Utilities::GetValueOrDefault<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
extern "C"  Il2CppObject * Utilities_GetValueOrDefault_TisIl2CppObject_m3695156018_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___dictionary0, String_t* ___key1, bool ___logWarning2, const MethodInfo* method);
#define Utilities_GetValueOrDefault_TisIl2CppObject_m3695156018(__this /* static, unused */, ___dictionary0, ___key1, ___logWarning2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, bool, const MethodInfo*))Utilities_GetValueOrDefault_TisIl2CppObject_m3695156018_gshared)(__this /* static, unused */, ___dictionary0, ___key1, ___logWarning2, method)
// System.Boolean Facebook.Unity.Utilities::TryGetValue<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,T&)
extern "C"  bool Utilities_TryGetValue_TisIl2CppObject_m586655899_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___dictionary0, String_t* ___key1, Il2CppObject ** ___value2, const MethodInfo* method);
#define Utilities_TryGetValue_TisIl2CppObject_m586655899(__this /* static, unused */, ___dictionary0, ___key1, ___value2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, Il2CppObject **, const MethodInfo*))Utilities_TryGetValue_TisIl2CppObject_m586655899_gshared)(__this /* static, unused */, ___dictionary0, ___key1, ___value2, method)
// T IData::Get<System.Object>(System.String)
extern "C"  Il2CppObject * IData_Get_TisIl2CppObject_m735631573_gshared (Il2CppObject * __this, String_t* ___key0, const MethodInfo* method);
#define IData_Get_TisIl2CppObject_m735631573(__this, ___key0, method) ((  Il2CppObject * (*) (Il2CppObject *, String_t*, const MethodInfo*))IData_Get_TisIl2CppObject_m735631573_gshared)(__this, ___key0, method)
// T LitJson.JsonMapper::ToObject<System.Object>(LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m2020447104_gshared (Il2CppObject * __this /* static, unused */, JsonReader_t1077921503 * ___reader0, const MethodInfo* method);
#define JsonMapper_ToObject_TisIl2CppObject_m2020447104(__this /* static, unused */, ___reader0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JsonReader_t1077921503 *, const MethodInfo*))JsonMapper_ToObject_TisIl2CppObject_m2020447104_gshared)(__this /* static, unused */, ___reader0, method)
// T LitJson.JsonMapper::ToObject<System.Object>(System.IO.TextReader)
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m2629474101_gshared (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, const MethodInfo* method);
#define JsonMapper_ToObject_TisIl2CppObject_m2629474101(__this /* static, unused */, ___reader0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, TextReader_t1561828458 *, const MethodInfo*))JsonMapper_ToObject_TisIl2CppObject_m2629474101_gshared)(__this /* static, unused */, ___reader0, method)
// T LitJson.JsonMapper::ToObject<System.Object>(System.String)
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m621049504_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method);
#define JsonMapper_ToObject_TisIl2CppObject_m621049504(__this /* static, unused */, ___json0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonMapper_ToObject_TisIl2CppObject_m621049504_gshared)(__this /* static, unused */, ___json0, method)
// T NGUITools::AddChild<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_AddChild_TisIl2CppObject_m3484082469_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, const MethodInfo* method);
#define NGUITools_AddChild_TisIl2CppObject_m3484082469(__this /* static, unused */, ___parent0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))NGUITools_AddChild_TisIl2CppObject_m3484082469_gshared)(__this /* static, unused */, ___parent0, method)
// System.String NGUITools::GetTypeName<System.Object>()
extern "C"  String_t* NGUITools_GetTypeName_TisIl2CppObject_m691711313_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NGUITools_GetTypeName_TisIl2CppObject_m691711313(__this /* static, unused */, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NGUITools_GetTypeName_TisIl2CppObject_m691711313_gshared)(__this /* static, unused */, method)
// T NGUITools::AddChild<System.Object>(UnityEngine.GameObject,System.Boolean)
extern "C"  Il2CppObject * NGUITools_AddChild_TisIl2CppObject_m2729428340_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, bool ___undo1, const MethodInfo* method);
#define NGUITools_AddChild_TisIl2CppObject_m2729428340(__this /* static, unused */, ___parent0, ___undo1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, bool, const MethodInfo*))NGUITools_AddChild_TisIl2CppObject_m2729428340_gshared)(__this /* static, unused */, ___parent0, ___undo1, method)
// T NGUITools::AddMissingComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_AddMissingComponent_TisIl2CppObject_m3399321306_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define NGUITools_AddMissingComponent_TisIl2CppObject_m3399321306(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))NGUITools_AddMissingComponent_TisIl2CppObject_m3399321306_gshared)(__this /* static, unused */, ___go0, method)
// T NGUITools::AddWidget<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_AddWidget_TisIl2CppObject_m1599480351_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define NGUITools_AddWidget_TisIl2CppObject_m1599480351(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))NGUITools_AddWidget_TisIl2CppObject_m1599480351_gshared)(__this /* static, unused */, ___go0, method)
// T NGUITools::AddWidget<System.Object>(UnityEngine.GameObject,System.Int32)
extern "C"  Il2CppObject * NGUITools_AddWidget_TisIl2CppObject_m861406406_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, int32_t ___depth1, const MethodInfo* method);
#define NGUITools_AddWidget_TisIl2CppObject_m861406406(__this /* static, unused */, ___go0, ___depth1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, int32_t, const MethodInfo*))NGUITools_AddWidget_TisIl2CppObject_m861406406_gshared)(__this /* static, unused */, ___go0, ___depth1, method)
// T NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m2434993371_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define NGUITools_FindInParents_TisIl2CppObject_m2434993371(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m2434993371_gshared)(__this /* static, unused */, ___go0, method)
// T NGUITools::FindInParents<System.Object>(UnityEngine.Transform)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m1167752884_gshared (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___trans0, const MethodInfo* method);
#define NGUITools_FindInParents_TisIl2CppObject_m1167752884(__this /* static, unused */, ___trans0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Transform_t3275118058 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m1167752884_gshared)(__this /* static, unused */, ___trans0, method)
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m2509612665(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// T OnlineMapsXML::A<System.Object>(System.String)
extern "C"  Il2CppObject * OnlineMapsXML_A_TisIl2CppObject_m1393778426_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___attributeName0, const MethodInfo* method);
#define OnlineMapsXML_A_TisIl2CppObject_m1393778426(__this, ___attributeName0, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_A_TisIl2CppObject_m1393778426_gshared)(__this, ___attributeName0, method)
// T OnlineMapsXML::A<System.Single>(System.String)
extern "C"  float OnlineMapsXML_A_TisSingle_t2076509932_m3191408141_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___attributeName0, const MethodInfo* method);
#define OnlineMapsXML_A_TisSingle_t2076509932_m3191408141(__this, ___attributeName0, method) ((  float (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_A_TisSingle_t2076509932_m3191408141_gshared)(__this, ___attributeName0, method)
// T OnlineMapsXML::Find<System.Int32>(System.String)
extern "C"  int32_t OnlineMapsXML_Find_TisInt32_t2071877448_m1886308485_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___xpath0, const MethodInfo* method);
#define OnlineMapsXML_Find_TisInt32_t2071877448_m1886308485(__this, ___xpath0, method) ((  int32_t (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_Find_TisInt32_t2071877448_m1886308485_gshared)(__this, ___xpath0, method)
// T OnlineMapsXML::Get<System.Int32>(System.Xml.XmlElement)
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m3769157143_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisInt32_t2071877448_m3769157143(__this, ___el0, method) ((  int32_t (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))OnlineMapsXML_Get_TisInt32_t2071877448_m3769157143_gshared)(__this, ___el0, method)
// T OnlineMapsXML::Find<System.Object>(System.String)
extern "C"  Il2CppObject * OnlineMapsXML_Find_TisIl2CppObject_m3977458542_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___xpath0, const MethodInfo* method);
#define OnlineMapsXML_Find_TisIl2CppObject_m3977458542(__this, ___xpath0, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_Find_TisIl2CppObject_m3977458542_gshared)(__this, ___xpath0, method)
// T OnlineMapsXML::Get<System.Object>(System.Xml.XmlElement)
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m704259572_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisIl2CppObject_m704259572(__this, ___el0, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))OnlineMapsXML_Get_TisIl2CppObject_m704259572_gshared)(__this, ___el0, method)
// T OnlineMapsXML::Get<System.Int32>(System.String)
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208(__this, ___childName0, method) ((  int32_t (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_gshared)(__this, ___childName0, method)
// T OnlineMapsXML::Get<System.Int32>(System.String,T)
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m837676824_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, int32_t ___defaultValue1, const MethodInfo* method);
#define OnlineMapsXML_Get_TisInt32_t2071877448_m837676824(__this, ___childName0, ___defaultValue1, method) ((  int32_t (*) (OnlineMapsXML_t3341520387 *, String_t*, int32_t, const MethodInfo*))OnlineMapsXML_Get_TisInt32_t2071877448_m837676824_gshared)(__this, ___childName0, ___defaultValue1, method)
// T OnlineMapsXML::Get<System.Int32>(System.Xml.XmlElement,T)
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m2886396799_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, int32_t ___defaultValue1, const MethodInfo* method);
#define OnlineMapsXML_Get_TisInt32_t2071877448_m2886396799(__this, ___el0, ___defaultValue1, method) ((  int32_t (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, int32_t, const MethodInfo*))OnlineMapsXML_Get_TisInt32_t2071877448_m2886396799_gshared)(__this, ___el0, ___defaultValue1, method)
// T OnlineMapsXML::Get<System.Single>(System.Xml.XmlElement)
extern "C"  float OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399(__this, ___el0, method) ((  float (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_gshared)(__this, ___el0, method)
// T OnlineMapsXML::Get<System.Object>(System.String)
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m3924433329_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisIl2CppObject_m3924433329(__this, ___childName0, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_Get_TisIl2CppObject_m3924433329_gshared)(__this, ___childName0, method)
// T OnlineMapsXML::Get<System.Object>(System.String,T)
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m448861289_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, Il2CppObject * ___defaultValue1, const MethodInfo* method);
#define OnlineMapsXML_Get_TisIl2CppObject_m448861289(__this, ___childName0, ___defaultValue1, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, String_t*, Il2CppObject *, const MethodInfo*))OnlineMapsXML_Get_TisIl2CppObject_m448861289_gshared)(__this, ___childName0, ___defaultValue1, method)
// T OnlineMapsXML::Get<System.Object>(System.Xml.XmlElement,T)
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m1557309828_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, Il2CppObject * ___defaultValue1, const MethodInfo* method);
#define OnlineMapsXML_Get_TisIl2CppObject_m1557309828(__this, ___el0, ___defaultValue1, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, Il2CppObject *, const MethodInfo*))OnlineMapsXML_Get_TisIl2CppObject_m1557309828_gshared)(__this, ___el0, ___defaultValue1, method)
// T OnlineMapsXML::Get<System.Single>(System.String)
extern "C"  float OnlineMapsXML_Get_TisSingle_t2076509932_m3546836458_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisSingle_t2076509932_m3546836458(__this, ___childName0, method) ((  float (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_Get_TisSingle_t2076509932_m3546836458_gshared)(__this, ___childName0, method)
// T OnlineMapsXML::Get<UnityEngine.Vector2>(System.String)
extern "C"  Vector2_t2243707579  OnlineMapsXML_Get_TisVector2_t2243707579_m2172396179_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisVector2_t2243707579_m2172396179(__this, ___childName0, method) ((  Vector2_t2243707579  (*) (OnlineMapsXML_t3341520387 *, String_t*, const MethodInfo*))OnlineMapsXML_Get_TisVector2_t2243707579_m2172396179_gshared)(__this, ___childName0, method)
// T OnlineMapsXML::Get<UnityEngine.Vector2>(System.Xml.XmlElement)
extern "C"  Vector2_t2243707579  OnlineMapsXML_Get_TisVector2_t2243707579_m3009823824_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method);
#define OnlineMapsXML_Get_TisVector2_t2243707579_m3009823824(__this, ___el0, method) ((  Vector2_t2243707579  (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))OnlineMapsXML_Get_TisVector2_t2243707579_m3009823824_gshared)(__this, ___el0, method)
// T OnlineMapsXML::Value<System.Int32>()
extern "C"  int32_t OnlineMapsXML_Value_TisInt32_t2071877448_m2298504637_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method);
#define OnlineMapsXML_Value_TisInt32_t2071877448_m2298504637(__this, method) ((  int32_t (*) (OnlineMapsXML_t3341520387 *, const MethodInfo*))OnlineMapsXML_Value_TisInt32_t2071877448_m2298504637_gshared)(__this, method)
// T OnlineMapsXML::Value<System.Object>()
extern "C"  Il2CppObject * OnlineMapsXML_Value_TisIl2CppObject_m33615830_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method);
#define OnlineMapsXML_Value_TisIl2CppObject_m33615830(__this, method) ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, const MethodInfo*))OnlineMapsXML_Value_TisIl2CppObject_m33615830_gshared)(__this, method)
// T OnlineMapsXML::Value<System.Single>()
extern "C"  float OnlineMapsXML_Value_TisSingle_t2076509932_m1578684385_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method);
#define OnlineMapsXML_Value_TisSingle_t2076509932_m1578684385(__this, method) ((  float (*) (OnlineMapsXML_t3341520387 *, const MethodInfo*))OnlineMapsXML_Value_TisSingle_t2076509932_m1578684385_gshared)(__this, method)
// T OnlineMapsXML::Value<UnityEngine.Vector2>()
extern "C"  Vector2_t2243707579  OnlineMapsXML_Value_TisVector2_t2243707579_m2370972126_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method);
#define OnlineMapsXML_Value_TisVector2_t2243707579_m2370972126(__this, method) ((  Vector2_t2243707579  (*) (OnlineMapsXML_t3341520387 *, const MethodInfo*))OnlineMapsXML_Value_TisVector2_t2243707579_m2370972126_gshared)(__this, method)
// T PlayerPrefSaveData::GetSavedObject<System.Object>(System.String)
extern "C"  Il2CppObject * PlayerPrefSaveData_GetSavedObject_TisIl2CppObject_m1035035119_gshared (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method);
#define PlayerPrefSaveData_GetSavedObject_TisIl2CppObject_m1035035119(__this, ___key0, method) ((  Il2CppObject * (*) (PlayerPrefSaveData_t170835669 *, String_t*, const MethodInfo*))PlayerPrefSaveData_GetSavedObject_TisIl2CppObject_m1035035119_gshared)(__this, ___key0, method)
// T Prime31.Json::decode<System.Object>(System.String,System.String)
extern "C"  Il2CppObject * Json_decode_TisIl2CppObject_m2605020229_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, String_t* ___rootElement1, const MethodInfo* method);
#define Json_decode_TisIl2CppObject_m2605020229(__this /* static, unused */, ___json0, ___rootElement1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, String_t*, const MethodInfo*))Json_decode_TisIl2CppObject_m2605020229_gshared)(__this /* static, unused */, ___json0, ___rootElement1, method)
// T Prime31.SimpleJson::decode<System.Object>(System.String,System.String)
extern "C"  Il2CppObject * SimpleJson_decode_TisIl2CppObject_m2631072897_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, String_t* ___rootElement1, const MethodInfo* method);
#define SimpleJson_decode_TisIl2CppObject_m2631072897(__this /* static, unused */, ___json0, ___rootElement1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, String_t*, const MethodInfo*))SimpleJson_decode_TisIl2CppObject_m2631072897_gshared)(__this /* static, unused */, ___json0, ___rootElement1, method)
// System.Object Prime31.Json/ObjectDecoder::decode<System.Object>(System.String,System.String)
extern "C"  Il2CppObject * ObjectDecoder_decode_TisIl2CppObject_m2926433253_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, String_t* ___rootElement1, const MethodInfo* method);
#define ObjectDecoder_decode_TisIl2CppObject_m2926433253(__this /* static, unused */, ___json0, ___rootElement1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, String_t*, const MethodInfo*))ObjectDecoder_decode_TisIl2CppObject_m2926433253_gshared)(__this /* static, unused */, ___json0, ___rootElement1, method)
// T Prime31.Json::decodeObject<System.Object>(System.Object,System.String)
extern "C"  Il2CppObject * Json_decodeObject_TisIl2CppObject_m4254816916_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonObject0, String_t* ___rootElement1, const MethodInfo* method);
#define Json_decodeObject_TisIl2CppObject_m4254816916(__this /* static, unused */, ___jsonObject0, ___rootElement1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, String_t*, const MethodInfo*))Json_decodeObject_TisIl2CppObject_m4254816916_gshared)(__this /* static, unused */, ___jsonObject0, ___rootElement1, method)
// T Prime31.SimpleJson::decodeObject<System.Object>(System.Object,System.String)
extern "C"  Il2CppObject * SimpleJson_decodeObject_TisIl2CppObject_m380181316_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonObject0, String_t* ___rootElement1, const MethodInfo* method);
#define SimpleJson_decodeObject_TisIl2CppObject_m380181316(__this /* static, unused */, ___jsonObject0, ___rootElement1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, String_t*, const MethodInfo*))SimpleJson_decodeObject_TisIl2CppObject_m380181316_gshared)(__this /* static, unused */, ___jsonObject0, ___rootElement1, method)
// T System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m580915191_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m580915191(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m580915191_gshared)(__this /* static, unused */, method)
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern "C"  Il2CppObject * Array_Find_TisIl2CppObject_m1654841559_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Predicate_1_t1132419410 * ___match1, const MethodInfo* method);
#define Array_Find_TisIl2CppObject_m1654841559(__this /* static, unused */, ___array0, ___match1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Predicate_1_t1132419410 *, const MethodInfo*))Array_Find_TisIl2CppObject_m1654841559_gshared)(__this /* static, unused */, ___array0, ___match1, method)
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern "C"  Il2CppObject * Array_FindLast_TisIl2CppObject_m1794562749_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Predicate_1_t1132419410 * ___match1, const MethodInfo* method);
#define Array_FindLast_TisIl2CppObject_m1794562749(__this /* static, unused */, ___array0, ___match1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Predicate_1_t1132419410 *, const MethodInfo*))Array_FindLast_TisIl2CppObject_m1794562749_gshared)(__this /* static, unused */, ___array0, ___match1, method)
// T System.Array::InternalArray__get_Item<AttackHomeNav>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<AttackHomeNav>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisAttackHomeNav_t3944890504_m501885785_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisAttackHomeNav_t3944890504_m501885785(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisAttackHomeNav_t3944890504_m501885785_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<AttackNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<AttackNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisAttackNavScreen_t36759459_m2767040246_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisAttackNavScreen_t36759459_m2767040246(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisAttackNavScreen_t36759459_m2767040246_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<AttackSearchNav>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<AttackSearchNav>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisAttackSearchNav_t4257884637_m681632714_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisAttackSearchNav_t4257884637_m681632714(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisAttackSearchNav_t4257884637_m681632714_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<BestHTTP.SignalR.Messages.ClientMessage>(System.Int32)
extern "C"  ClientMessage_t624279968  Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756(__this, ___index0, method) ((  ClientMessage_t624279968  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<BestHTTP.SignalR.Messages.ClientMessage>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisClientMessage_t624279968_m3458433939_gshared (Il2CppArray * __this, int32_t p0, ClientMessage_t624279968 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisClientMessage_t624279968_m3458433939(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ClientMessage_t624279968 *, const MethodInfo*))Array_GetGenericValueImpl_TisClientMessage_t624279968_m3458433939_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ChatNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ChatNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisChatNavScreen_t42377261_m3666423032_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisChatNavScreen_t42377261_m3666423032(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisChatNavScreen_t42377261_m3666423032_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<DefendNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<DefendNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDefendNavScreen_t3838619703_m1191705454_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDefendNavScreen_t3838619703_m1191705454(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisDefendNavScreen_t3838619703_m1191705454_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<FriendHomeNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<FriendHomeNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisFriendHomeNavScreen_t1599038296_m1317175403_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisFriendHomeNavScreen_t1599038296_m1317175403(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisFriendHomeNavScreen_t1599038296_m1317175403_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<FriendNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<FriendNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisFriendNavScreen_t2175383453_m2054038542_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisFriendNavScreen_t2175383453_m2054038542(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisFriendNavScreen_t2175383453_m2054038542_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<FriendSearchNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<FriendSearchNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisFriendSearchNavScreen_t3018389163_m4175902850_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisFriendSearchNavScreen_t3018389163_m4175902850(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisFriendSearchNavScreen_t3018389163_m4175902850_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<LeaderboardNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisLeaderboardNavScreen_t2356043712_m425675556_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLeaderboardNavScreen_t2356043712_m425675556(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLeaderboardNavScreen_t2356043712_m425675556_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<LeaderboardNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLeaderboardNavScreen_t2356043712_m3024443935_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLeaderboardNavScreen_t2356043712_m3024443935(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisLeaderboardNavScreen_t2356043712_m3024443935_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<LitJson.ArrayMetadata>(System.Int32)
extern "C"  ArrayMetadata_t2008834462  Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323(__this, ___index0, method) ((  ArrayMetadata_t2008834462  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<LitJson.ArrayMetadata>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisArrayMetadata_t2008834462_m2377945494_gshared (Il2CppArray * __this, int32_t p0, ArrayMetadata_t2008834462 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisArrayMetadata_t2008834462_m2377945494(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ArrayMetadata_t2008834462 *, const MethodInfo*))Array_GetGenericValueImpl_TisArrayMetadata_t2008834462_m2377945494_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<LitJson.ObjectMetadata>(System.Int32)
extern "C"  ObjectMetadata_t3995922398  Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155(__this, ___index0, method) ((  ObjectMetadata_t3995922398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<LitJson.ObjectMetadata>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisObjectMetadata_t3995922398_m1995218292_gshared (Il2CppArray * __this, int32_t p0, ObjectMetadata_t3995922398 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisObjectMetadata_t3995922398_m1995218292(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ObjectMetadata_t3995922398 *, const MethodInfo*))Array_GetGenericValueImpl_TisObjectMetadata_t3995922398_m1995218292_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<LitJson.PropertyMetadata>(System.Int32)
extern "C"  PropertyMetadata_t3693826136  Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445(__this, ___index0, method) ((  PropertyMetadata_t3693826136  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<LitJson.PropertyMetadata>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPropertyMetadata_t3693826136_m3953193078_gshared (Il2CppArray * __this, int32_t p0, PropertyMetadata_t3693826136 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPropertyMetadata_t3693826136_m3953193078(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, PropertyMetadata_t3693826136 *, const MethodInfo*))Array_GetGenericValueImpl_TisPropertyMetadata_t3693826136_m3953193078_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977(__this, ___index0, method) ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTableRange_t2011406615_m3090494674_gshared (Il2CppArray * __this, int32_t p0, TableRange_t2011406615 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTableRange_t2011406615_m3090494674(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TableRange_t2011406615 *, const MethodInfo*))Array_GetGenericValueImpl_TisTableRange_t2011406615_m3090494674_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisClientCertificateType_t4001384466_m2257558628_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisClientCertificateType_t4001384466_m2257558628(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisClientCertificateType_t4001384466_m2257558628_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Mono.Security.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t683497865  Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299(__this, ___index0, method) ((  UriScheme_t683497865  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Security.Uri/UriScheme>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUriScheme_t683497865_m524494900_gshared (Il2CppArray * __this, int32_t p0, UriScheme_t683497865 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUriScheme_t683497865_m524494900(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UriScheme_t683497865 *, const MethodInfo*))Array_GetGenericValueImpl_TisUriScheme_t683497865_m524494900_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2340974457  Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763(__this, ___index0, method) ((  TagName_t2340974457  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Xml2.XmlTextReader/TagName>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTagName_t2340974457_m1522944854_gshared (Il2CppArray * __this, int32_t p0, TagName_t2340974457 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTagName_t2340974457_m1522944854(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TagName_t2340974457 *, const MethodInfo*))Array_GetGenericValueImpl_TisTagName_t2340974457_m1522944854_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<OnlineMapsEvents>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisOnlineMapsEvents_t850667009_m511107595_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisOnlineMapsEvents_t850667009_m511107595(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisOnlineMapsEvents_t850667009_m511107595_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<OnlineMapsEvents>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisOnlineMapsEvents_t850667009_m36039040_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisOnlineMapsEvents_t850667009_m36039040(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisOnlineMapsEvents_t850667009_m36039040_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<OnlineMapsJPEGDecoder/Code>(System.Int32)
extern "C"  Code_t3472511518  Array_InternalArray__get_Item_TisCode_t3472511518_m2961437966_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCode_t3472511518_m2961437966(__this, ___index0, method) ((  Code_t3472511518  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCode_t3472511518_m2961437966_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<OnlineMapsJPEGDecoder/Code>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCode_t3472511518_m3059859547_gshared (Il2CppArray * __this, int32_t p0, Code_t3472511518 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCode_t3472511518_m3059859547(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Code_t3472511518 *, const MethodInfo*))Array_GetGenericValueImpl_TisCode_t3472511518_m3059859547_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<PanelType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPanelType_t482769230_m2025358834_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPanelType_t482769230_m2025358834(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPanelType_t482769230_m2025358834_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<PanelType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPanelType_t482769230_m4162280565_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPanelType_t482769230_m4162280565(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisPanelType_t482769230_m4162280565_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<PhoneNumberNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPhoneNumberNavScreen_t1263467250_m1726775518_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPhoneNumberNavScreen_t1263467250_m1726775518(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPhoneNumberNavScreen_t1263467250_m1726775518_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<PhoneNumberNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPhoneNumberNavScreen_t1263467250_m3534278401_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPhoneNumberNavScreen_t1263467250_m3534278401(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisPhoneNumberNavScreen_t1263467250_m3534278401_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<PlayerProfileNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPlayerProfileNavScreen_t2660668259_m4053680973_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPlayerProfileNavScreen_t2660668259_m4053680973(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPlayerProfileNavScreen_t2660668259_m4053680973_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<PlayerProfileNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPlayerProfileNavScreen_t2660668259_m2932866902_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPlayerProfileNavScreen_t2660668259_m2932866902(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisPlayerProfileNavScreen_t2660668259_m2932866902_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Prime31.JsonFormatter/JsonContextType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJsonContextType_t3787516849_m136453899_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJsonContextType_t3787516849_m136453899(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJsonContextType_t3787516849_m136453899_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Prime31.JsonFormatter/JsonContextType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisJsonContextType_t3787516849_m2032551948_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisJsonContextType_t3787516849_m2032551948(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisJsonContextType_t3787516849_m2032551948_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ProfileNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisProfileNavScreen_t1126657854_m2676274002_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisProfileNavScreen_t1126657854_m2676274002(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisProfileNavScreen_t1126657854_m2676274002_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ProfileNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisProfileNavScreen_t1126657854_m2014733141_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisProfileNavScreen_t1126657854_m2014733141(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisProfileNavScreen_t1126657854_m2014733141_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<RegNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisRegNavScreen_t667173359_m2994260105_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRegNavScreen_t667173359_m2994260105(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRegNavScreen_t667173359_m2994260105_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<RegNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRegNavScreen_t667173359_m3695191862_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRegNavScreen_t667173359_m3695191862(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisRegNavScreen_t667173359_m3695191862_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<SettingsNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisSettingsNavScreen_t2267127234_m1689028766_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSettingsNavScreen_t2267127234_m1689028766(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSettingsNavScreen_t2267127234_m1689028766_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<SettingsNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSettingsNavScreen_t2267127234_m2114654367_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSettingsNavScreen_t2267127234_m2114654367(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSettingsNavScreen_t2267127234_m2114654367_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<StatusNavScreen>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisStatusNavScreen_t2872369083_m2205417297_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisStatusNavScreen_t2872369083_m2205417297(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisStatusNavScreen_t2872369083_m2205417297_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<StatusNavScreen>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisStatusNavScreen_t2872369083_m4078586192_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisStatusNavScreen_t2872369083_m4078586192(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisStatusNavScreen_t2872369083_m4078586192_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2594217482  Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683(__this, ___index0, method) ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.ArraySegment`1<System.Byte>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisArraySegment_1_t2594217482_m1465024590_gshared (Il2CppArray * __this, int32_t p0, ArraySegment_1_t2594217482 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisArraySegment_1_t2594217482_m1465024590(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ArraySegment_1_t2594217482 *, const MethodInfo*))Array_GetGenericValueImpl_TisArraySegment_1_t2594217482_m1465024590_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639(__this, ___index0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Boolean>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisBoolean_t3825574718_m3733720694_gshared (Il2CppArray * __this, int32_t p0, bool* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisBoolean_t3825574718_m3733720694(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, bool*, const MethodInfo*))Array_GetGenericValueImpl_TisBoolean_t3825574718_m3733720694_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t3683104436_m635665873(__this, ___index0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Byte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisByte_t3683104436_m835007568_gshared (Il2CppArray * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisByte_t3683104436_m835007568(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisByte_t3683104436_m835007568_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547(__this, ___index0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Char>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisChar_t3454481338_m1407217314_gshared (Il2CppArray * __this, int32_t p0, Il2CppChar* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisChar_t3454481338_m1407217314(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Il2CppChar*, const MethodInfo*))Array_GetGenericValueImpl_TisChar_t3454481338_m1407217314_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320(__this, ___index0, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.DictionaryEntry>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDictionaryEntry_t3048875398_m3588961231_gshared (Il2CppArray * __this, int32_t p0, DictionaryEntry_t3048875398 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDictionaryEntry_t3048875398_m3588961231(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, DictionaryEntry_t3048875398 *, const MethodInfo*))Array_GetGenericValueImpl_TisDictionaryEntry_t3048875398_m3588961231_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Int32>>(System.Int32)
extern "C"  Link_t247561424  Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t247561424_m1513279294(__this, ___index0, method) ((  Link_t247561424  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.HashSet`1/Link<System.Int32>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLink_t247561424_m4166172253_gshared (Il2CppArray * __this, int32_t p0, Link_t247561424 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLink_t247561424_m4166172253(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Link_t247561424 *, const MethodInfo*))Array_GetGenericValueImpl_TisLink_t247561424_m4166172253_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t865133271_m2489845481(__this, ___index0, method) ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLink_t865133271_m1909607934_gshared (Il2CppArray * __this, int32_t p0, Link_t865133271 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLink_t865133271_m1909607934(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Link_t865133271 *, const MethodInfo*))Array_GetGenericValueImpl_TisLink_t865133271_m1909607934_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1603735834  Array_InternalArray__get_Item_TisKeyValuePair_2_t1603735834_m1792859529_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1603735834_m1792859529(__this, ___index0, method) ((  KeyValuePair_2_t1603735834  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1603735834_m1792859529_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1603735834_m1852014934_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1603735834 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1603735834_m1852014934(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1603735834 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1603735834_m1852014934_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>(System.Int32)
extern "C"  KeyValuePair_2_t2812303849  Array_InternalArray__get_Item_TisKeyValuePair_2_t2812303849_m1649835165_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2812303849_m1649835165(__this, ___index0, method) ((  KeyValuePair_2_t2812303849  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2812303849_m1649835165_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2812303849_m2524828558_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2812303849 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2812303849_m2524828558(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2812303849 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2812303849_m2524828558_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3132015601  Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841(__this, ___index0, method) ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3132015601_m2832359684_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3132015601 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3132015601_m2832359684(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3132015601 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3132015601_m2832359684_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118(__this, ___index0, method) ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3749587448_m1123124997_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3749587448 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3749587448_m1123124997(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3749587448 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3749587448_m1123124997_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern "C"  KeyValuePair_2_t1008373517  Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979(__this, ___index0, method) ((  KeyValuePair_2_t1008373517  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1008373517_m555256510_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1008373517 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1008373517_m555256510(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1008373517 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1008373517_m555256510_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>(System.Int32)
extern "C"  KeyValuePair_2_t3653207108  Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578(__this, ___index0, method) ((  KeyValuePair_2_t3653207108  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3653207108_m2810532887_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3653207108 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3653207108_m2810532887(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3653207108 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3653207108_m2810532887_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>(System.Int32)
extern "C"  KeyValuePair_2_t1345327748  Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408(__this, ___index0, method) ((  KeyValuePair_2_t1345327748  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1345327748_m4192901199_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1345327748 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1345327748_m4192901199(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1345327748 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1345327748_m4192901199_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>(System.Int32)
extern "C"  KeyValuePair_2_t1043231486  Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066(__this, ___index0, method) ((  KeyValuePair_2_t1043231486  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1043231486_m561657757_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1043231486 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1043231486_m561657757(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1043231486 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1043231486_m561657757_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t1174980068  Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642(__this, ___index0, method) ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1174980068_m239902827_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1174980068 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1174980068_m239902827(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1174980068 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1174980068_m239902827_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>(System.Int32)
extern "C"  KeyValuePair_2_t2369073723  Array_InternalArray__get_Item_TisKeyValuePair_2_t2369073723_m2312448055_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2369073723_m2312448055(__this, ___index0, method) ((  KeyValuePair_2_t2369073723  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2369073723_m2312448055_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2369073723_m1163782800_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2369073723 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2369073723_m1163782800(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2369073723 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2369073723_m1163782800_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630(__this, ___index0, method) ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3716250094_m515659545_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3716250094 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3716250094_m515659545(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3716250094 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3716250094_m515659545_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t38854645  Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821(__this, ___index0, method) ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t38854645_m3697171854_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t38854645 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t38854645_m3697171854(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t38854645 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t38854645_m3697171854_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern "C"  KeyValuePair_2_t488203048  Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965(__this, ___index0, method) ((  KeyValuePair_2_t488203048  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t488203048_m2194823594_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t488203048 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t488203048_m2194823594(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t488203048 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t488203048_m2194823594_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>(System.Int32)
extern "C"  KeyValuePair_2_t3888080226  Array_InternalArray__get_Item_TisKeyValuePair_2_t3888080226_m642757410_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3888080226_m642757410(__this, ___index0, method) ((  KeyValuePair_2_t3888080226  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3888080226_m642757410_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3888080226_m3569633953_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3888080226 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3888080226_m3569633953(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3888080226 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3888080226_m3569633953_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.UInt64,BestHTTP.SignalR.Messages.ClientMessage>>(System.Int32)
extern "C"  KeyValuePair_2_t322707255  Array_InternalArray__get_Item_TisKeyValuePair_2_t322707255_m2266875646_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t322707255_m2266875646(__this, ___index0, method) ((  KeyValuePair_2_t322707255  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t322707255_m2266875646_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.UInt64,BestHTTP.SignalR.Messages.ClientMessage>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t322707255_m3031054857_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t322707255 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t322707255_m3031054857(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t322707255 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t322707255_m3031054857_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2387876582  Array_InternalArray__get_Item_TisKeyValuePair_2_t2387876582_m4073223712_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2387876582_m4073223712(__this, ___index0, method) ((  KeyValuePair_2_t2387876582  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2387876582_m4073223712_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2387876582_m3464234887_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2387876582 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2387876582_m3464234887(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2387876582 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2387876582_m3464234887_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2335466038  Array_InternalArray__get_Item_TisKeyValuePair_2_t2335466038_m3011847674_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2335466038_m3011847674(__this, ___index0, method) ((  KeyValuePair_2_t2335466038  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2335466038_m3011847674_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2335466038_m3718118869_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2335466038 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2335466038_m3718118869(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2335466038 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2335466038_m3718118869_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t1020750381  Array_InternalArray__get_Item_TisKeyValuePair_2_t1020750381_m861546038_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1020750381_m861546038(__this, ___index0, method) ((  KeyValuePair_2_t1020750381  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1020750381_m861546038_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1020750381_m2471195149_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1020750381 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1020750381_m2471195149(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1020750381 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1020750381_m2471195149_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2723257478  Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655(__this, ___index0, method) ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.Link>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLink_t2723257478_m312085818_gshared (Il2CppArray * __this, int32_t p0, Link_t2723257478 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLink_t2723257478_m312085818(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Link_t2723257478 *, const MethodInfo*))Array_GetGenericValueImpl_TisLink_t2723257478_m312085818_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2022531261  Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551(__this, ___index0, method) ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSlot_t2022531261_m649447310_gshared (Il2CppArray * __this, int32_t p0, Slot_t2022531261 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t2022531261_m649447310(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Slot_t2022531261 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t2022531261_m649447310_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2267560602  Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430(__this, ___index0, method) ((  Slot_t2267560602  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSlot_t2267560602_m412219389_gshared (Il2CppArray * __this, int32_t p0, Slot_t2267560602 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t2267560602_m412219389(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Slot_t2267560602 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t2267560602_m412219389_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.ComponentModel.PropertyTabScope>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPropertyTabScope_t2485003348_m1882471435_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyTabScope_t2485003348_m1882471435(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyTabScope_t2485003348_m1882471435_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.ComponentModel.PropertyTabScope>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPropertyTabScope_t2485003348_m1977517556_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPropertyTabScope_t2485003348_m1977517556(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisPropertyTabScope_t2485003348_m1977517556_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t693205669  Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220(__this, ___index0, method) ((  DateTime_t693205669  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDateTime_t693205669_m1809708669_gshared (Il2CppArray * __this, int32_t p0, DateTime_t693205669 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDateTime_t693205669_m1809708669(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, DateTime_t693205669 *, const MethodInfo*))Array_GetGenericValueImpl_TisDateTime_t693205669_m1809708669_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t724701077  Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600(__this, ___index0, method) ((  Decimal_t724701077  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDecimal_t724701077_m1692453859_gshared (Il2CppArray * __this, int32_t p0, Decimal_t724701077 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDecimal_t724701077_m1692453859(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Decimal_t724701077 *, const MethodInfo*))Array_GetGenericValueImpl_TisDecimal_t724701077_m1692453859_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088(__this, ___index0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Double>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDouble_t4078015681_m4104650313_gshared (Il2CppArray * __this, int32_t p0, double* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDouble_t4078015681_m4104650313(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, double*, const MethodInfo*))Array_GetGenericValueImpl_TisDouble_t4078015681_m4104650313_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979(__this, ___index0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Int16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisInt16_t4041245914_m3808207008_gshared (Il2CppArray * __this, int32_t p0, int16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt16_t4041245914_m3808207008(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt16_t4041245914_m3808207008_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Int32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisInt32_t2071877448_m864220890_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt32_t2071877448_m864220890(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt32_t2071877448_m864220890_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204(__this, ___index0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Int64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisInt64_t909078037_m2189738725_gshared (Il2CppArray * __this, int32_t p0, int64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt64_t909078037_m2189738725(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt64_t909078037_m2189738725_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m210946760(__this, ___index0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.IntPtr>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisIntPtr_t_m3163030953_gshared (Il2CppArray * __this, int32_t p0, IntPtr_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisIntPtr_t_m3163030953(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, IntPtr_t*, const MethodInfo*))Array_GetGenericValueImpl_TisIntPtr_t_m3163030953_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m371871810(__this, ___index0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Object>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisIl2CppObject_m1383826529_gshared (Il2CppArray * __this, int32_t p0, Il2CppObject ** p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisIl2CppObject_m1383826529(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Il2CppObject **, const MethodInfo*))Array_GetGenericValueImpl_TisIl2CppObject_m1383826529_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t94157543  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745(__this, ___index0, method) ((  CustomAttributeNamedArgument_t94157543  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t94157543_m444953022_gshared (Il2CppArray * __this, int32_t p0, CustomAttributeNamedArgument_t94157543 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t94157543_m444953022(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, CustomAttributeNamedArgument_t94157543 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t94157543_m444953022_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t1498197914  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094(__this, ___index0, method) ((  CustomAttributeTypedArgument_t1498197914  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t1498197914_m2518526477_gshared (Il2CppArray * __this, int32_t p0, CustomAttributeTypedArgument_t1498197914 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t1498197914_m2518526477(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, CustomAttributeTypedArgument_t1498197914 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t1498197914_m2518526477_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t3712112744  Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768(__this, ___index0, method) ((  LabelData_t3712112744  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabelData_t3712112744_m3881182685_gshared (Il2CppArray * __this, int32_t p0, LabelData_t3712112744 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelData_t3712112744_m3881182685(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, LabelData_t3712112744 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelData_t3712112744_m3881182685_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t4090909514  Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142(__this, ___index0, method) ((  LabelFixup_t4090909514  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabelFixup_t4090909514_m1090849779_gshared (Il2CppArray * __this, int32_t p0, LabelFixup_t4090909514 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelFixup_t4090909514_m1090849779(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, LabelFixup_t4090909514 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelFixup_t4090909514_m1090849779_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t149559338  Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537(__this, ___index0, method) ((  ILTokenInfo_t149559338  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisILTokenInfo_t149559338_m1527183528_gshared (Il2CppArray * __this, int32_t p0, ILTokenInfo_t149559338 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisILTokenInfo_t149559338_m1527183528(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ILTokenInfo_t149559338 *, const MethodInfo*))Array_GetGenericValueImpl_TisILTokenInfo_t149559338_m1527183528_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern "C"  Label_t4243202660  Array_InternalArray__get_Item_TisLabel_t4243202660_m2000046803_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabel_t4243202660_m2000046803(__this, ___index0, method) ((  Label_t4243202660  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabel_t4243202660_m2000046803_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.Label>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabel_t4243202660_m185513360_gshared (Il2CppArray * __this, int32_t p0, Label_t4243202660 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabel_t4243202660_m185513360(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Label_t4243202660 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabel_t4243202660_m185513360_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern "C"  MonoResource_t3127387157  Array_InternalArray__get_Item_TisMonoResource_t3127387157_m310178846_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMonoResource_t3127387157_m310178846(__this, ___index0, method) ((  MonoResource_t3127387157  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMonoResource_t3127387157_m310178846_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.MonoResource>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMonoResource_t3127387157_m963948735_gshared (Il2CppArray * __this, int32_t p0, MonoResource_t3127387157 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMonoResource_t3127387157_m963948735(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, MonoResource_t3127387157 *, const MethodInfo*))Array_GetGenericValueImpl_TisMonoResource_t3127387157_m963948735_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern "C"  RefEmitPermissionSet_t2708608433  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t2708608433_m4227940714_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRefEmitPermissionSet_t2708608433_m4227940714(__this, ___index0, method) ((  RefEmitPermissionSet_t2708608433  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRefEmitPermissionSet_t2708608433_m4227940714_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRefEmitPermissionSet_t2708608433_m1258744523_gshared (Il2CppArray * __this, int32_t p0, RefEmitPermissionSet_t2708608433 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRefEmitPermissionSet_t2708608433_m1258744523(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RefEmitPermissionSet_t2708608433 *, const MethodInfo*))Array_GetGenericValueImpl_TisRefEmitPermissionSet_t2708608433_m1258744523_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t1820634920  Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304(__this, ___index0, method) ((  ParameterModifier_t1820634920  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.ParameterModifier>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisParameterModifier_t1820634920_m2107995815_gshared (Il2CppArray * __this, int32_t p0, ParameterModifier_t1820634920 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisParameterModifier_t1820634920_m2107995815(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ParameterModifier_t1820634920 *, const MethodInfo*))Array_GetGenericValueImpl_TisParameterModifier_t1820634920_m2107995815_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t333236149  Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631(__this, ___index0, method) ((  ResourceCacheItem_t333236149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResourceCacheItem_t333236149_m3398157766_gshared (Il2CppArray * __this, int32_t p0, ResourceCacheItem_t333236149 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceCacheItem_t333236149_m3398157766(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ResourceCacheItem_t333236149 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceCacheItem_t333236149_m3398157766_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t3933049236  Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352(__this, ___index0, method) ((  ResourceInfo_t3933049236  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResourceInfo_t3933049236_m1989129835_gshared (Il2CppArray * __this, int32_t p0, ResourceInfo_t3933049236 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceInfo_t3933049236_m1989129835(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ResourceInfo_t3933049236 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceInfo_t3933049236_m1989129835_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007(__this, ___index0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTypeTag_t141209596_m771175594_gshared (Il2CppArray * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTypeTag_t141209596_m771175594(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTypeTag_t141209596_m771175594_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452(__this, ___index0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSByte_t454417549_m193299233_gshared (Il2CppArray * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSByte_t454417549_m193299233(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSByte_t454417549_m193299233_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t4278378721  Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500(__this, ___index0, method) ((  X509ChainStatus_t4278378721  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisX509ChainStatus_t4278378721_m1031157553_gshared (Il2CppArray * __this, int32_t p0, X509ChainStatus_t4278378721 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisX509ChainStatus_t4278378721_m1031157553(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, X509ChainStatus_t4278378721 *, const MethodInfo*))Array_GetGenericValueImpl_TisX509ChainStatus_t4278378721_m1031157553_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753(__this, ___index0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Single>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSingle_t2076509932_m2068965306_gshared (Il2CppArray * __this, int32_t p0, float* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSingle_t2076509932_m2068965306(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, float*, const MethodInfo*))Array_GetGenericValueImpl_TisSingle_t2076509932_m2068965306_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t1425267120_m3923903537_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTermInfoStrings_t1425267120_m3923903537(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTermInfoStrings_t1425267120_m3923903537_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TermInfoStrings>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTermInfoStrings_t1425267120_m3813637998_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTermInfoStrings_t1425267120_m3813637998(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTermInfoStrings_t1425267120_m3813637998_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t2724874473  Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706(__this, ___index0, method) ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Text.RegularExpressions.Mark>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMark_t2724874473_m755456127_gshared (Il2CppArray * __this, int32_t p0, Mark_t2724874473 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMark_t2724874473_m755456127(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Mark_t2724874473 *, const MethodInfo*))Array_GetGenericValueImpl_TisMark_t2724874473_m755456127_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t3430258949  Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260(__this, ___index0, method) ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTimeSpan_t3430258949_m1230748161_gshared (Il2CppArray * __this, int32_t p0, TimeSpan_t3430258949 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t3430258949_m1230748161(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TimeSpan_t3430258949 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t3430258949_m1230748161_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710(__this, ___index0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt16_t986882611_m805867045_gshared (Il2CppArray * __this, int32_t p0, uint16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt16_t986882611_m805867045(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt16_t986882611_m805867045_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852(__this, ___index0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt32_t2149682021_m3317787023_gshared (Il2CppArray * __this, int32_t p0, uint32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt32_t2149682021_m3317787023(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt32_t2149682021_m3317787023_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875(__this, ___index0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt64_t2909196914_m2421453588_gshared (Il2CppArray * __this, int32_t p0, uint64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt64_t2909196914_m2421453588(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt64_t2909196914_m2421453588_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t1876590943  Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697(__this, ___index0, method) ((  UriScheme_t1876590943  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Uri/UriScheme>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUriScheme_t1876590943_m1746642778_gshared (Il2CppArray * __this, int32_t p0, UriScheme_t1876590943 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUriScheme_t1876590943_m1746642778(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UriScheme_t1876590943 *, const MethodInfo*))Array_GetGenericValueImpl_TisUriScheme_t1876590943_m1746642778_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t3210081295  Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909(__this, ___index0, method) ((  NsDecl_t3210081295  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNsDecl_t3210081295_m1380674728_gshared (Il2CppArray * __this, int32_t p0, NsDecl_t3210081295 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNsDecl_t3210081295_m1380674728(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NsDecl_t3210081295 *, const MethodInfo*))Array_GetGenericValueImpl_TisNsDecl_t3210081295_m1380674728_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t2513625351  Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529(__this, ___index0, method) ((  NsScope_t2513625351  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XmlNamespaceManager/NsScope>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNsScope_t2513625351_m3472217580_gshared (Il2CppArray * __this, int32_t p0, NsScope_t2513625351 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNsScope_t2513625351_m3472217580(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NsScope_t2513625351 *, const MethodInfo*))Array_GetGenericValueImpl_TisNsScope_t2513625351_m3472217580_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XPath.XPathResultType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisXPathResultType_t1521569578_m3711281163_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisXPathResultType_t1521569578_m3711281163(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisXPathResultType_t1521569578_m3711281163_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XPath.XPathResultType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisXPathResultType_t1521569578_m895620358_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisXPathResultType_t1521569578_m895620358(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisXPathResultType_t1521569578_m895620358_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern "C"  FadeEntry_t3041229383  Array_InternalArray__get_Item_TisFadeEntry_t3041229383_m946129697_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFadeEntry_t3041229383_m946129697(__this, ___index0, method) ((  FadeEntry_t3041229383  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFadeEntry_t3041229383_m946129697_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<TypewriterEffect/FadeEntry>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisFadeEntry_t3041229383_m440753886_gshared (Il2CppArray * __this, int32_t p0, FadeEntry_t3041229383 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisFadeEntry_t3041229383_m440753886(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, FadeEntry_t3041229383 *, const MethodInfo*))Array_GetGenericValueImpl_TisFadeEntry_t3041229383_m440753886_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern "C"  DepthEntry_t974746545  Array_InternalArray__get_Item_TisDepthEntry_t974746545_m2114350883_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDepthEntry_t974746545_m2114350883(__this, ___index0, method) ((  DepthEntry_t974746545  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDepthEntry_t974746545_m2114350883_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UICamera/DepthEntry>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDepthEntry_t974746545_m2898160382_gshared (Il2CppArray * __this, int32_t p0, DepthEntry_t974746545 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDepthEntry_t974746545_m2898160382(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, DepthEntry_t974746545 *, const MethodInfo*))Array_GetGenericValueImpl_TisDepthEntry_t974746545_m2898160382_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Unibill.Impl.BillingPlatform>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisBillingPlatform_t552059234_m1273653209_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBillingPlatform_t552059234_m1273653209(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBillingPlatform_t552059234_m1273653209_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Unibill.Impl.BillingPlatform>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisBillingPlatform_t552059234_m945329542_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisBillingPlatform_t552059234_m945329542(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisBillingPlatform_t552059234_m945329542_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnibillError>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisUnibillError_t1753859787_m702214397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUnibillError_t1753859787_m702214397(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUnibillError_t1753859787_m702214397_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnibillError>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUnibillError_t1753859787_m604379370_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUnibillError_t1753859787_m604379370(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUnibillError_t1753859787_m604379370_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern "C"  Bounds_t3033363703  Array_InternalArray__get_Item_TisBounds_t3033363703_m816654186_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBounds_t3033363703_m816654186(__this, ___index0, method) ((  Bounds_t3033363703  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBounds_t3033363703_m816654186_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Bounds>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisBounds_t3033363703_m3026284577_gshared (Il2CppArray * __this, int32_t p0, Bounds_t3033363703 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisBounds_t3033363703_m3026284577(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Bounds_t3033363703 *, const MethodInfo*))Array_GetGenericValueImpl_TisBounds_t3033363703_m3026284577_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t2020392075  Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t2020392075_m996560062(__this, ___index0, method) ((  Color_t2020392075  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor_t2020392075_m137158401_gshared (Il2CppArray * __this, int32_t p0, Color_t2020392075 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor_t2020392075_m137158401(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color_t2020392075 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor_t2020392075_m137158401_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t874517518  Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687(__this, ___index0, method) ((  Color32_t874517518  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor32_t874517518_m268407884_gshared (Il2CppArray * __this, int32_t p0, Color32_t874517518 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor32_t874517518_m268407884(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color32_t874517518 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor32_t874517518_m268407884_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t1376425630  Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783(__this, ___index0, method) ((  ContactPoint_t1376425630  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint_t1376425630_m1109516784_gshared (Il2CppArray * __this, int32_t p0, ContactPoint_t1376425630 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint_t1376425630_m1109516784(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint_t1376425630 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint_t1376425630_m1109516784_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3659330976  Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777(__this, ___index0, method) ((  ContactPoint2D_t3659330976  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint2D_t3659330976_m1180959526_gshared (Il2CppArray * __this, int32_t p0, ContactPoint2D_t3659330976 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint2D_t3659330976_m1180959526(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint2D_t3659330976 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint2D_t3659330976_m1180959526_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t21186376  Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765(__this, ___index0, method) ((  RaycastResult_t21186376  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.EventSystems.RaycastResult>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastResult_t21186376_m597658884_gshared (Il2CppArray * __this, int32_t p0, RaycastResult_t21186376 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastResult_t21186376_m597658884(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastResult_t21186376 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastResult_t21186376_m597658884_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Experimental.Director.Playable>(System.Int32)
extern "C"  Playable_t3667545548  Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877(__this, ___index0, method) ((  Playable_t3667545548  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Experimental.Director.Playable>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPlayable_t3667545548_m530332998_gshared (Il2CppArray * __this, int32_t p0, Playable_t3667545548 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPlayable_t3667545548_m530332998(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Playable_t3667545548 *, const MethodInfo*))Array_GetGenericValueImpl_TisPlayable_t3667545548_m530332998_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisKeyCode_t2283395152_m3267050797_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyCode_t2283395152_m3267050797(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyCode_t2283395152_m3267050797_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.KeyCode>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyCode_t2283395152_m3144061556_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyCode_t2283395152_m3144061556(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisKeyCode_t2283395152_m3144061556_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t1449471340  Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933(__this, ___index0, method) ((  Keyframe_t1449471340  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Keyframe>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyframe_t1449471340_m4055881886_gshared (Il2CppArray * __this, int32_t p0, Keyframe_t1449471340 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyframe_t1449471340_m4055881886(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Keyframe_t1449471340 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyframe_t1449471340_m4055881886_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Plane>(System.Int32)
extern "C"  Plane_t3727654732  Array_InternalArray__get_Item_TisPlane_t3727654732_m3853269401_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPlane_t3727654732_m3853269401(__this, ___index0, method) ((  Plane_t3727654732  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPlane_t3727654732_m3853269401_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Plane>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPlane_t3727654732_m987325568_gshared (Il2CppArray * __this, int32_t p0, Plane_t3727654732 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPlane_t3727654732_m987325568(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Plane_t3727654732 *, const MethodInfo*))Array_GetGenericValueImpl_TisPlane_t3727654732_m987325568_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t87180320  Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569(__this, ___index0, method) ((  RaycastHit_t87180320  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit_t87180320_m4205930102_gshared (Il2CppArray * __this, int32_t p0, RaycastHit_t87180320 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit_t87180320_m4205930102(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit_t87180320 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit_t87180320_m4205930102_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t4063908774  Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655(__this, ___index0, method) ((  RaycastHit2D_t4063908774  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit2D_t4063908774_m822940056_gshared (Il2CppArray * __this, int32_t p0, RaycastHit2D_t4063908774 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit2D_t4063908774_m822940056(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit2D_t4063908774 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit2D_t4063908774_m822940056_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Rect>(System.Int32)
extern "C"  Rect_t3681755626  Array_InternalArray__get_Item_TisRect_t3681755626_m1107043059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRect_t3681755626_m1107043059(__this, ___index0, method) ((  Rect_t3681755626  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRect_t3681755626_m1107043059_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Rect>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRect_t3681755626_m2828010452_gshared (Il2CppArray * __this, int32_t p0, Rect_t3681755626 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRect_t3681755626_m2828010452(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Rect_t3681755626 *, const MethodInfo*))Array_GetGenericValueImpl_TisRect_t3681755626_m2828010452_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RuntimePlatform>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RuntimePlatform>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRuntimePlatform_t1869584967_m98279955_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRuntimePlatform_t1869584967_m98279955(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisRuntimePlatform_t1869584967_m98279955_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t1761367055  Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901(__this, ___index0, method) ((  HitInfo_t1761367055  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SendMouseEvents/HitInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisHitInfo_t1761367055_m254462600_gshared (Il2CppArray * __this, int32_t p0, HitInfo_t1761367055 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisHitInfo_t1761367055_m254462600(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, HitInfo_t1761367055 *, const MethodInfo*))Array_GetGenericValueImpl_TisHitInfo_t1761367055_m254462600_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1754866149  Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810(__this, ___index0, method) ((  GcAchievementData_t1754866149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcAchievementData_t1754866149_m3738568593_gshared (Il2CppArray * __this, int32_t p0, GcAchievementData_t1754866149 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcAchievementData_t1754866149_m3738568593(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcAchievementData_t1754866149 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcAchievementData_t1754866149_m3738568593_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t3676783238  Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313(__this, ___index0, method) ((  GcScoreData_t3676783238  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcScoreData_t3676783238_m3457843794_gshared (Il2CppArray * __this, int32_t p0, GcScoreData_t3676783238 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcScoreData_t3676783238_m3457843794(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcScoreData_t3676783238 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcScoreData_t3676783238_m3457843794_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.TextEditor/TextEditOp>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTextEditOp_t3138797698_m4047452505_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTextEditOp_t3138797698_m4047452505(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTextEditOp_t3138797698_m4047452505_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Touch>(System.Int32)
extern "C"  Touch_t407273883  Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510(__this, ___index0, method) ((  Touch_t407273883  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Touch>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTouch_t407273883_m3156021057_gshared (Il2CppArray * __this, int32_t p0, Touch_t407273883 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTouch_t407273883_m3156021057(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Touch_t407273883 *, const MethodInfo*))Array_GetGenericValueImpl_TisTouch_t407273883_m3156021057_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UI.InputField/ContentType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContentType_t1028629049_m3267712264_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContentType_t1028629049_m3267712264(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisContentType_t1028629049_m3267712264_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t3056636800  Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785(__this, ___index0, method) ((  UICharInfo_t3056636800  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UICharInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUICharInfo_t3056636800_m1425638036_gshared (Il2CppArray * __this, int32_t p0, UICharInfo_t3056636800 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUICharInfo_t3056636800_m1425638036(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UICharInfo_t3056636800 *, const MethodInfo*))Array_GetGenericValueImpl_TisUICharInfo_t3056636800_m1425638036_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t3621277874  Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059(__this, ___index0, method) ((  UILineInfo_t3621277874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UILineInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUILineInfo_t3621277874_m431994246_gshared (Il2CppArray * __this, int32_t p0, UILineInfo_t3621277874 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUILineInfo_t3621277874_m431994246(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UILineInfo_t3621277874 *, const MethodInfo*))Array_GetGenericValueImpl_TisUILineInfo_t3621277874_m431994246_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t1204258818  Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955(__this, ___index0, method) ((  UIVertex_t1204258818  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UIVertex>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUIVertex_t1204258818_m4244482102_gshared (Il2CppArray * __this, int32_t p0, UIVertex_t1204258818 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUIVertex_t1204258818_m4244482102(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UIVertex_t1204258818 *, const MethodInfo*))Array_GetGenericValueImpl_TisUIVertex_t1204258818_m4244482102_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t2243707579  Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294(__this, ___index0, method) ((  Vector2_t2243707579  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector2>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector2_t2243707579_m601998659_gshared (Il2CppArray * __this, int32_t p0, Vector2_t2243707579 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector2_t2243707579_m601998659(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector2_t2243707579 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector2_t2243707579_m601998659_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t2243707580  Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745(__this, ___index0, method) ((  Vector3_t2243707580  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector3>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector3_t2243707580_m966839774_gshared (Il2CppArray * __this, int32_t p0, Vector3_t2243707580 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector3_t2243707580_m966839774(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector3_t2243707580 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector3_t2243707580_m966839774_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t2243707581  Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892(__this, ___index0, method) ((  Vector4_t2243707581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector4>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector4_t2243707581_m870130081_gshared (Il2CppArray * __this, int32_t p0, Vector4_t2243707581 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector4_t2243707581_m870130081(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector4_t2243707581 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector4_t2243707581_m870130081_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vectrosity.Vector3Pair>(System.Int32)
extern "C"  Vector3Pair_t2859078138  Array_InternalArray__get_Item_TisVector3Pair_t2859078138_m2447468668_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3Pair_t2859078138_m2447468668(__this, ___index0, method) ((  Vector3Pair_t2859078138  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3Pair_t2859078138_m2447468668_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vectrosity.Vector3Pair>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector3Pair_t2859078138_m399274027_gshared (Il2CppArray * __this, int32_t p0, Vector3Pair_t2859078138 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector3Pair_t2859078138_m399274027(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector3Pair_t2859078138 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector3Pair_t2859078138_m399274027_gshared)(__this, p0, p1, method)
// T System.Threading.Interlocked::CompareExchange<System.Object>(T&,T,T)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___location10, Il2CppObject * ___value1, Il2CppObject * ___comparand2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, ___location10, ___value1, ___comparand2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, ___location10, ___value1, ___comparand2, method)
// T UITweener::Begin<System.Object>(UnityEngine.GameObject,System.Single)
extern "C"  Il2CppObject * UITweener_Begin_TisIl2CppObject_m3878506588_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, float ___duration1, const MethodInfo* method);
#define UITweener_Begin_TisIl2CppObject_m3878506588(__this /* static, unused */, ___go0, ___duration1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, float, const MethodInfo*))UITweener_Begin_TisIl2CppObject_m3878506588_gshared)(__this /* static, unused */, ___go0, ___duration1, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponents_TisIl2CppObject_m890532490_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m890532490(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m890532490_gshared)(__this, method)
// T Unibill.Impl.MiniJsonExtensions::get<System.Object>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  Il2CppObject * MiniJsonExtensions_get_TisIl2CppObject_m1169879225_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method);
#define MiniJsonExtensions_get_TisIl2CppObject_m1169879225(__this /* static, unused */, ___dic0, ___key1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, const MethodInfo*))MiniJsonExtensions_get_TisIl2CppObject_m1169879225_gshared)(__this /* static, unused */, ___dic0, ___key1, method)
// T Unibill.Impl.MiniJsonExtensions::getEnum<PurchaseType>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  int32_t MiniJsonExtensions_getEnum_TisPurchaseType_t1639241305_m1386270067_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method);
#define MiniJsonExtensions_getEnum_TisPurchaseType_t1639241305_m1386270067(__this /* static, unused */, ___dic0, ___key1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, const MethodInfo*))MiniJsonExtensions_getEnum_TisPurchaseType_t1639241305_m1386270067_gshared)(__this /* static, unused */, ___dic0, ___key1, method)
// T Unibill.Impl.MiniJsonExtensions::getEnum<System.Object>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  Il2CppObject * MiniJsonExtensions_getEnum_TisIl2CppObject_m1497042404_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method);
#define MiniJsonExtensions_getEnum_TisIl2CppObject_m1497042404(__this /* static, unused */, ___dic0, ___key1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, const MethodInfo*))MiniJsonExtensions_getEnum_TisIl2CppObject_m1497042404_gshared)(__this /* static, unused */, ___dic0, ___key1, method)
// T Unibill.Impl.MiniJsonExtensions::getEnum<Unibill.Impl.BillingPlatform>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  int32_t MiniJsonExtensions_getEnum_TisBillingPlatform_t552059234_m90687349_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method);
#define MiniJsonExtensions_getEnum_TisBillingPlatform_t552059234_m90687349(__this /* static, unused */, ___dic0, ___key1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, const MethodInfo*))MiniJsonExtensions_getEnum_TisBillingPlatform_t552059234_m90687349_gshared)(__this /* static, unused */, ___dic0, ___key1, method)
// T Unibill.Impl.MiniJsonExtensions::getEnum<Unibill.Impl.SamsungAppsMode>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  int32_t MiniJsonExtensions_getEnum_TisSamsungAppsMode_t2745005863_m422064390_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method);
#define MiniJsonExtensions_getEnum_TisSamsungAppsMode_t2745005863_m422064390(__this /* static, unused */, ___dic0, ___key1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, const MethodInfo*))MiniJsonExtensions_getEnum_TisSamsungAppsMode_t2745005863_m422064390_gshared)(__this /* static, unused */, ___dic0, ___key1, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  Il2CppObject * AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method);
#define AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473(__this /* static, unused */, ___klass0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_gshared)(__this /* static, unused */, ___klass0, method)
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m2461586036(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m1823576579_gshared (Component_t3819376471 * __this, bool ___includeInactive0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m1823576579(__this, ___includeInactive0, method) ((  Il2CppObject * (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m1823576579_gshared)(__this, ___includeInactive0, method)
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m3838331218_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t2681005625 * ___data0, const MethodInfo* method);
#define ExecuteEvents_ValidateEventData_TisIl2CppObject_m3838331218(__this /* static, unused */, ___data0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, BaseEventData_t2681005625 *, const MethodInfo*))ExecuteEvents_ValidateEventData_TisIl2CppObject_m3838331218_gshared)(__this /* static, unused */, ___data0, method)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m327292296(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m1362037227_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m1362037227(__this, ___includeInactive0, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, bool, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m1362037227_gshared)(__this, ___includeInactive0, method)
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m2967490724_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m2967490724(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m2967490724_gshared)(__this /* static, unused */, method)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, ___original0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, ___original0, method)
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m1767088036_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Transform_t3275118058 * ___parent1, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m1767088036(__this /* static, unused */, ___original0, ___parent1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m1767088036_gshared)(__this /* static, unused */, ___original0, ___parent1, method)
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Transform,System.Boolean)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m1736742113_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m1736742113(__this /* static, unused */, ___original0, ___parent1, ___worldPositionStays2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, bool, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m1736742113_gshared)(__this /* static, unused */, ___original0, ___parent1, ___worldPositionStays2, method)
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m653480707_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m653480707(__this /* static, unused */, ___original0, ___position1, ___rotation2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m653480707_gshared)(__this /* static, unused */, ___original0, ___position1, ___rotation2, method)
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m4219963824_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, Transform_t3275118058 * ___parent3, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m4219963824(__this /* static, unused */, ___original0, ___position1, ___rotation2, ___parent3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m4219963824_gshared)(__this /* static, unused */, ___original0, ___position1, ___rotation2, ___parent3, method)
// T UnityEngine.Resources::GetBuiltinResource<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_GetBuiltinResource_TisIl2CppObject_m1023501484_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method);
#define Resources_GetBuiltinResource_TisIl2CppObject_m1023501484(__this /* static, unused */, ___path0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_GetBuiltinResource_TisIl2CppObject_m1023501484_gshared)(__this /* static, unused */, ___path0, method)
// T UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2884518678_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2884518678(__this /* static, unused */, ___path0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2884518678_gshared)(__this /* static, unused */, ___path0, method)
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m2022535220_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m2022535220(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m2022535220_gshared)(__this /* static, unused */, method)
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m2875934266_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define Dropdown_GetOrAddComponent_TisIl2CppObject_m2875934266(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Dropdown_GetOrAddComponent_TisIl2CppObject_m2875934266_gshared)(__this /* static, unused */, ___go0, method)
// T UnityUtil::findInstanceOfType<System.Object>()
extern "C"  Il2CppObject * UnityUtil_findInstanceOfType_TisIl2CppObject_m2306951808_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityUtil_findInstanceOfType_TisIl2CppObject_m2306951808(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityUtil_findInstanceOfType_TisIl2CppObject_m2306951808_gshared)(__this /* static, unused */, method)
// T UnityUtil::loadResourceInstanceOfType<System.Object>()
extern "C"  Il2CppObject * UnityUtil_loadResourceInstanceOfType_TisIl2CppObject_m927293821_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityUtil_loadResourceInstanceOfType_TisIl2CppObject_m927293821(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityUtil_loadResourceInstanceOfType_TisIl2CppObject_m927293821_gshared)(__this /* static, unused */, method)
// T[] IPCycler::MakeWidgets<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* IPCycler_MakeWidgets_TisIl2CppObject_m3829340906_gshared (IPCycler_t1336138445 * __this, const MethodInfo* method);
#define IPCycler_MakeWidgets_TisIl2CppObject_m3829340906(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (IPCycler_t1336138445 *, const MethodInfo*))IPCycler_MakeWidgets_TisIl2CppObject_m3829340906_gshared)(__this, method)
// T[] NGUITools::FindActive<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* NGUITools_FindActive_TisIl2CppObject_m2645751425_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NGUITools_FindActive_TisIl2CppObject_m2645751425(__this /* static, unused */, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NGUITools_FindActive_TisIl2CppObject_m2645751425_gshared)(__this /* static, unused */, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C"  ObjectU5BU5D_t3614634134* Array_FindAll_TisIl2CppObject_m2420286284_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Predicate_1_t1132419410 * ___match1, const MethodInfo* method);
#define Array_FindAll_TisIl2CppObject_m2420286284(__this /* static, unused */, ___array0, ___match1, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Predicate_1_t1132419410 *, const MethodInfo*))Array_FindAll_TisIl2CppObject_m2420286284_gshared)(__this /* static, unused */, ___array0, ___match1, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m3798820896_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m3798820896(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m3798820896_gshared)(__this /* static, unused */, p0, p1, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t3614634134* CustomAttributeData_UnboxValues_TisIl2CppObject_m1499708102_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisIl2CppObject_m1499708102(__this /* static, unused */, ___values0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, const MethodInfo*))CustomAttributeData_UnboxValues_TisIl2CppObject_m1499708102_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3304067486* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t94157543_m2789115353_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t94157543_m2789115353(__this /* static, unused */, ___values0, method) ((  CustomAttributeNamedArgumentU5BU5D_t3304067486* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t94157543_m2789115353_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t1075686591* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1498197914_m2561215702_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1498197914_m2561215702(__this /* static, unused */, ___values0, method) ((  CustomAttributeTypedArgumentU5BU5D_t1075686591* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1498197914_m2561215702_gshared)(__this /* static, unused */, ___values0, method)
// T[] Uniject.IUtil::getAnyComponentsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* IUtil_getAnyComponentsOfType_TisIl2CppObject_m3661619866_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IUtil_getAnyComponentsOfType_TisIl2CppObject_m3661619866(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject *, const MethodInfo*))IUtil_getAnyComponentsOfType_TisIl2CppObject_m3661619866_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponents_TisIl2CppObject_m3998315035_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m3998315035(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m3998315035_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3978412804(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared (Component_t3819376471 * __this, bool ___includeInactive0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3229891463(__this, ___includeInactive0, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared)(__this, ___includeInactive0, method)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m541433219_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m541433219(__this, ___includeInactive0, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m541433219_gshared)(__this, ___includeInactive0, method)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInParent_TisIl2CppObject_m1112546512_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1112546512(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1112546512_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInParent_TisIl2CppObject_m2092455797_gshared (Component_t3819376471 * __this, bool ___includeInactive0, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m2092455797(__this, ___includeInactive0, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m2092455797_gshared)(__this, ___includeInactive0, method)
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method);
#define GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873(__this, ___includeInactive0, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_gshared)(__this, ___includeInactive0, method)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482_gshared)(__this, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588(__this, ___channel0, method) ((  ObjectU5BU5D_t3614634134* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222(__this, ___channel0, ___format1, ___dim2, method) ((  ObjectU5BU5D_t3614634134* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t30278651* Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417(__this, ___channel0, ___format1, ___dim2, method) ((  Color32U5BU5D_t30278651* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716(__this, ___channel0, method) ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426(__this, ___channel0, ___format1, ___dim2, method) ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537(__this, ___channel0, method) ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415(__this, ___channel0, ___format1, ___dim2, method) ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442(__this, ___channel0, method) ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692(__this, ___channel0, ___format1, ___dim2, method) ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisIl2CppObject_m1343658011_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m1343658011(__this /* static, unused */, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1343658011_gshared)(__this /* static, unused */, method)
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t3614634134* Resources_ConvertObjects_TisIl2CppObject_m2571720668_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t4217747464* ___rawObjects0, const MethodInfo* method);
#define Resources_ConvertObjects_TisIl2CppObject_m2571720668(__this /* static, unused */, ___rawObjects0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t4217747464*, const MethodInfo*))Resources_ConvertObjects_TisIl2CppObject_m2571720668_gshared)(__this /* static, unused */, ___rawObjects0, method)
// T[] UnityUtil::getAnyComponentsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* UnityUtil_getAnyComponentsOfType_TisIl2CppObject_m30029786_gshared (UnityUtil_t671748753 * __this, const MethodInfo* method);
#define UnityUtil_getAnyComponentsOfType_TisIl2CppObject_m30029786(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (UnityUtil_t671748753 *, const MethodInfo*))UnityUtil_getAnyComponentsOfType_TisIl2CppObject_m30029786_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<UnityEngine.MonoBehaviour>()
#define GameObject_GetComponents_TisMonoBehaviour_t1158329972_m1288443065(__this, method) ((  MonoBehaviourU5BU5D_t3035069757* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m890532490_gshared)(__this, method)
// TAccumulate System.Linq.Enumerable::Aggregate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern "C"  Il2CppObject * Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m2460971397_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___seed1, Func_3_t3369346583 * ___func2, const MethodInfo* method);
#define Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m2460971397(__this /* static, unused */, ___source0, ___seed1, ___func2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, Func_3_t3369346583 *, const MethodInfo*))Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m2460971397_gshared)(__this /* static, unused */, ___source0, ___seed1, ___func2, method)
// TAccumulate System.Linq.Enumerable::Aggregate<System.Object,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern "C"  Vector2_t2243707579  Enumerable_Aggregate_TisIl2CppObject_TisVector2_t2243707579_m519767975_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Vector2_t2243707579  ___seed1, Func_3_t1022329359 * ___func2, const MethodInfo* method);
#define Enumerable_Aggregate_TisIl2CppObject_TisVector2_t2243707579_m519767975(__this /* static, unused */, ___source0, ___seed1, ___func2, method) ((  Vector2_t2243707579  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Vector2_t2243707579 , Func_3_t1022329359 *, const MethodInfo*))Enumerable_Aggregate_TisIl2CppObject_TisVector2_t2243707579_m519767975_gshared)(__this /* static, unused */, ___source0, ___seed1, ___func2, method)
// TAccumulate System.Linq.Enumerable::Aggregate<UnityEngine.Vector3,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern "C"  Vector3_t2243707580  Enumerable_Aggregate_TisVector3_t2243707580_TisVector3_t2243707580_m2439534529_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Vector3_t2243707580  ___seed1, Func_3_t246581160 * ___func2, const MethodInfo* method);
#define Enumerable_Aggregate_TisVector3_t2243707580_TisVector3_t2243707580_m2439534529(__this /* static, unused */, ___source0, ___seed1, ___func2, method) ((  Vector3_t2243707580  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Vector3_t2243707580 , Func_3_t246581160 *, const MethodInfo*))Enumerable_Aggregate_TisVector3_t2243707580_TisVector3_t2243707580_m2439534529_gshared)(__this /* static, unused */, ___source0, ___seed1, ___func2, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t3614634134* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Converter_2_t106372939 * ___converter1, const MethodInfo* method);
#define Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546(__this /* static, unused */, ___array0, ___converter1, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Converter_2_t106372939 *, const MethodInfo*))Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_gshared)(__this /* static, unused */, ___array0, ___converter1, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3158850377_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m3158850377(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m3158850377_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m1382289173_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3961629604 * ___predicate1, int32_t ___fallback2, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m1382289173(__this /* static, unused */, ___source0, ___predicate1, ___fallback2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, int32_t, const MethodInfo*))Enumerable_First_TisIl2CppObject_m1382289173_gshared)(__this /* static, unused */, ___source0, ___predicate1, ___fallback2, method)
// TSource System.Linq.Enumerable::First<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Vector3_t2243707580  Enumerable_First_TisVector3_t2243707580_m2727413067_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_First_TisVector3_t2243707580_m2727413067(__this /* static, unused */, ___source0, method) ((  Vector3_t2243707580  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisVector3_t2243707580_m2727413067_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3961629604 * ___predicate1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748(__this /* static, unused */, ___source0, ___predicate1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748_gshared)(__this /* static, unused */, ___source0, ___predicate1, method)
// TSource System.Linq.Enumerable::Last<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  int32_t Enumerable_Last_TisInt32_t2071877448_m2438477215_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_Last_TisInt32_t2071877448_m2438477215(__this /* static, unused */, ___source0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Last_TisInt32_t2071877448_m2438477215_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m518628776_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_Last_TisIl2CppObject_m518628776(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Last_TisIl2CppObject_m518628776_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::Last<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Vector3_t2243707580  Enumerable_Last_TisVector3_t2243707580_m2164312195_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_Last_TisVector3_t2243707580_m2164312195(__this /* static, unused */, ___source0, method) ((  Vector3_t2243707580  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Last_TisVector3_t2243707580_m2164312195_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Byte>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ByteU5BU5D_t3397334013* Enumerable_ToArray_TisByte_t3683104436_m975316257_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisByte_t3683104436_m975316257(__this /* static, unused */, ___source0, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisByte_t3683104436_m975316257_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  KeyValuePair_2U5BU5D_t2854920344* Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193(__this /* static, unused */, ___source0, method) ((  KeyValuePair_2U5BU5D_t2854920344* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Int16>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int16U5BU5D_t3104283263* Enumerable_ToArray_TisInt16_t4041245914_m2624416067_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisInt16_t4041245914_m2624416067(__this /* static, unused */, ___source0, method) ((  Int16U5BU5D_t3104283263* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisInt16_t4041245914_m2624416067_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t3030399641* Enumerable_ToArray_TisInt32_t2071877448_m513246933_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisInt32_t2071877448_m513246933(__this /* static, unused */, ___source0, method) ((  Int32U5BU5D_t3030399641* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisInt32_t2071877448_m513246933_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m349912619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m349912619(__this /* static, unused */, ___source0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m349912619_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Vector3U5BU5D_t1172311765* Enumerable_ToArray_TisVector3_t2243707580_m1930596601_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisVector3_t2243707580_m1930596601(__this /* static, unused */, ___source0, method) ((  Vector3U5BU5D_t1172311765* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisVector3_t2243707580_m1930596601_gshared)(__this /* static, unused */, ___source0, method)
// U System.Linq.Enumerable::Iterate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern "C"  Il2CppObject * Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3644922910_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___initValue1, Func_3_t3369346583 * ___selector2, const MethodInfo* method);
#define Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3644922910(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, Func_3_t3369346583 *, const MethodInfo*))Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3644922910_gshared)(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method)
// U System.Linq.Enumerable::Iterate<System.Object,System.Single>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern "C"  float Enumerable_Iterate_TisIl2CppObject_TisSingle_t2076509932_m641357313_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, float ___initValue1, Func_3_t4013867779 * ___selector2, const MethodInfo* method);
#define Enumerable_Iterate_TisIl2CppObject_TisSingle_t2076509932_m641357313(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, float, Func_3_t4013867779 *, const MethodInfo*))Enumerable_Iterate_TisIl2CppObject_TisSingle_t2076509932_m641357313_gshared)(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method)
// U System.Linq.Enumerable::Iterate<UnityEngine.Vector3,System.Single>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern "C"  float Enumerable_Iterate_TisVector3_t2243707580_TisSingle_t2076509932_m1684800810_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, float ___initValue1, Func_3_t3779166440 * ___selector2, const MethodInfo* method);
#define Enumerable_Iterate_TisVector3_t2243707580_TisSingle_t2076509932_m1684800810(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, float, Func_3_t3779166440 *, const MethodInfo*))Enumerable_Iterate_TisVector3_t2243707580_TisSingle_t2076509932_m1684800810_gshared)(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t1756533147 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, BaseEventData_t2681005625 * ___eventData1, EventFunction_1_t1186599945 * ___callbackFunction2, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  bool ExecuteEvents_Execute_TisIl2CppObject_m4168308247_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, BaseEventData_t2681005625 * ___eventData1, EventFunction_1_t1186599945 * ___functor2, const MethodInfo* method);
#define ExecuteEvents_Execute_TisIl2CppObject_m4168308247(__this /* static, unused */, ___target0, ___eventData1, ___functor2, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))ExecuteEvents_Execute_TisIl2CppObject_m4168308247_gshared)(__this /* static, unused */, ___target0, ___eventData1, ___functor2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, const MethodInfo* method);
#define ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576(__this /* static, unused */, ___root0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_gshared)(__this /* static, unused */, ___root0, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::CanHandleEvent<System.Object>(UnityEngine.GameObject)
extern "C"  bool ExecuteEvents_CanHandleEvent_TisIl2CppObject_m1201779629_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define ExecuteEvents_CanHandleEvent_TisIl2CppObject_m1201779629(__this /* static, unused */, ___go0, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))ExecuteEvents_CanHandleEvent_TisIl2CppObject_m1201779629_gshared)(__this /* static, unused */, ___go0, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisCorner_t1077473318_m1354090789_gshared (LayoutGroup_t3962498969 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		int32_t* L_3 = ___currentValue0;
		int32_t L_4 = ___newValue1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		if (!L_8)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		int32_t* L_9 = ___currentValue0;
		int32_t L_10 = ___newValue1;
		(*(int32_t*)L_9) = L_10;
		NullCheck((LayoutGroup_t3962498969 *)__this);
		LayoutGroup_SetDirty_m3600378958((LayoutGroup_t3962498969 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisVector2_t2243707579_m3010153489_gshared (LayoutGroup_t3962498969 * __this, Vector2_t2243707579 * ___currentValue0, Vector2_t2243707579  ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		Vector2_t2243707579 * L_3 = ___currentValue0;
		Vector2_t2243707579  L_4 = ___newValue1;
		Vector2_t2243707579  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		bool L_7 = Vector2_Equals_m1405920279((Vector2_t2243707579 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		Vector2_t2243707579 * L_8 = ___currentValue0;
		Vector2_t2243707579  L_9 = ___newValue1;
		(*(Vector2_t2243707579 *)L_8) = L_9;
		NullCheck((LayoutGroup_t3962498969 *)__this);
		LayoutGroup_SetDirty_m3600378958((LayoutGroup_t3962498969 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// T Data::Get<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1000087476;
extern const uint32_t Data_Get_TisIl2CppObject_m4245298088_MetadataUsageId;
extern "C"  Il2CppObject * Data_Get_TisIl2CppObject_m4245298088_gshared (Data_t3569509720 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Data_Get_TisIl2CppObject_m4245298088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = (Hashtable_t909839986 *)__this->get_data_0();
		String_t* L_1 = ___key0;
		NullCheck((Hashtable_t909839986 *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, (Hashtable_t909839986 *)L_0, (Il2CppObject *)L_1);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Hashtable_t909839986 * L_3 = (Hashtable_t909839986 *)__this->get_data_0();
		String_t* L_4 = ___key0;
		NullCheck((Hashtable_t909839986 *)L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t909839986 *)L_3, (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_5, (Type_t *)L_6, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0032:
	{
		String_t* L_8 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)L_8, (String_t*)_stringLiteral1000087476, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_10 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_10, (String_t*)L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}
}
// T Facebook.Unity.ComponentFactory::AddComponent<System.Object>()
extern "C"  Il2CppObject * ComponentFactory_AddComponent_TisIl2CppObject_m2420316583_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ComponentFactory_get_FacebookGameObject_m4175809231(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T Facebook.Unity.ComponentFactory::GetComponent<System.Object>(Facebook.Unity.ComponentFactory/IfNotExist)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ComponentFactory_GetComponent_TisIl2CppObject_m3641920544_MetadataUsageId;
extern "C"  Il2CppObject * ComponentFactory_GetComponent_TisIl2CppObject_m3641920544_gshared (Il2CppObject * __this /* static, unused */, int32_t ___ifNotExist0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactory_GetComponent_TisIl2CppObject_m3641920544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ComponentFactory_get_FacebookGameObject_m4175809231(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (GameObject_t1756533147 *)L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck((GameObject_t1756533147 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = ___ifNotExist0;
		if (L_5)
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck((GameObject_t1756533147 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1756533147 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_1 = (Il2CppObject *)L_7;
	}

IL_002b:
	{
		Il2CppObject * L_8 = V_1;
		return L_8;
	}
}
// T Facebook.Unity.Utilities::GetValueOrDefault<System.Int64>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookLogger_t821309934_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3774792693;
extern const uint32_t Utilities_GetValueOrDefault_TisInt64_t909078037_m3442176644_MetadataUsageId;
extern "C"  int64_t Utilities_GetValueOrDefault_TisInt64_t909078037_m3442176644_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___dictionary0, String_t* ___key1, bool ___logWarning2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utilities_GetValueOrDefault_TisInt64_t909078037_m3442176644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___dictionary0;
		String_t* L_1 = ___key1;
		bool L_2 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, int64_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (String_t*)L_1, (int64_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		bool L_3 = ___logWarning2;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		StringU5BU5D_t1642385972* L_4 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_5 = ___key1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookLogger_t821309934_il2cpp_TypeInfo_var);
		FacebookLogger_Warn_m3395822361(NULL /*static, unused*/, (String_t*)_stringLiteral3774792693, (StringU5BU5D_t1642385972*)L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		int64_t L_6 = V_0;
		return L_6;
	}
}
// T Facebook.Unity.Utilities::GetValueOrDefault<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookLogger_t821309934_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3774792693;
extern const uint32_t Utilities_GetValueOrDefault_TisIl2CppObject_m3695156018_MetadataUsageId;
extern "C"  Il2CppObject * Utilities_GetValueOrDefault_TisIl2CppObject_m3695156018_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___dictionary0, String_t* ___key1, bool ___logWarning2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utilities_GetValueOrDefault_TisIl2CppObject_m3695156018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject* L_0 = ___dictionary0;
		String_t* L_1 = ___key1;
		bool L_2 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (String_t*)L_1, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		bool L_3 = ___logWarning2;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		StringU5BU5D_t1642385972* L_4 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_5 = ___key1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookLogger_t821309934_il2cpp_TypeInfo_var);
		FacebookLogger_Warn_m3395822361(NULL /*static, unused*/, (String_t*)_stringLiteral3774792693, (StringU5BU5D_t1642385972*)L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T LitJson.JsonMapper::ToObject<System.Object>(LitJson.JsonReader)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t800426905_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToObject_TisIl2CppObject_m2020447104_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m2020447104_gshared (Il2CppObject * __this /* static, unused */, JsonReader_t1077921503 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToObject_TisIl2CppObject_m2020447104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		JsonReader_t1077921503 * L_1 = ___reader0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t800426905_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = JsonMapper_ReadValue_m387333528(NULL /*static, unused*/, (Type_t *)L_0, (JsonReader_t1077921503 *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T LitJson.JsonMapper::ToObject<System.Object>(System.IO.TextReader)
extern Il2CppClass* JsonReader_t1077921503_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t800426905_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToObject_TisIl2CppObject_m2629474101_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m2629474101_gshared (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToObject_TisIl2CppObject_m2629474101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonReader_t1077921503 * V_0 = NULL;
	{
		TextReader_t1561828458 * L_0 = ___reader0;
		JsonReader_t1077921503 * L_1 = (JsonReader_t1077921503 *)il2cpp_codegen_object_new(JsonReader_t1077921503_il2cpp_TypeInfo_var);
		JsonReader__ctor_m1288497814(L_1, (TextReader_t1561828458 *)L_0, /*hidden argument*/NULL);
		V_0 = (JsonReader_t1077921503 *)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		JsonReader_t1077921503 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t800426905_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = JsonMapper_ReadValue_m387333528(NULL /*static, unused*/, (Type_t *)L_2, (JsonReader_t1077921503 *)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T LitJson.JsonMapper::ToObject<System.Object>(System.String)
extern Il2CppClass* JsonReader_t1077921503_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t800426905_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToObject_TisIl2CppObject_m621049504_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m621049504_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToObject_TisIl2CppObject_m621049504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonReader_t1077921503 * V_0 = NULL;
	{
		String_t* L_0 = ___json0;
		JsonReader_t1077921503 * L_1 = (JsonReader_t1077921503 *)il2cpp_codegen_object_new(JsonReader_t1077921503_il2cpp_TypeInfo_var);
		JsonReader__ctor_m3043878087(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		V_0 = (JsonReader_t1077921503 *)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		JsonReader_t1077921503 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t800426905_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = JsonMapper_ReadValue_m387333528(NULL /*static, unused*/, (Type_t *)L_2, (JsonReader_t1077921503 *)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T NGUITools::AddChild<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_AddChild_TisIl2CppObject_m3484082469_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_AddChild_TisIl2CppObject_m3484082469_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddChild_TisIl2CppObject_m3484082469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_1 = NGUITools_AddChild_m3914341557(NULL /*static, unused*/, (GameObject_t1756533147 *)L_0, /*hidden argument*/NULL);
		V_0 = (GameObject_t1756533147 *)L_1;
		GameObject_t1756533147 * L_2 = V_0;
		String_t* L_3 = ((  String_t* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Object_t1021602117 *)L_2);
		Object_set_name_m4157836998((Object_t1021602117 *)L_2, (String_t*)L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck((GameObject_t1756533147 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1756533147 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// T NGUITools::AddChild<System.Object>(UnityEngine.GameObject,System.Boolean)
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_AddChild_TisIl2CppObject_m2729428340_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_AddChild_TisIl2CppObject_m2729428340_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, bool ___undo1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddChild_TisIl2CppObject_m2729428340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___parent0;
		bool L_1 = ___undo1;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_2 = NGUITools_AddChild_m3119301680(NULL /*static, unused*/, (GameObject_t1756533147 *)L_0, (bool)L_1, /*hidden argument*/NULL);
		V_0 = (GameObject_t1756533147 *)L_2;
		GameObject_t1756533147 * L_3 = V_0;
		String_t* L_4 = ((  String_t* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Object_t1021602117 *)L_3);
		Object_set_name_m4157836998((Object_t1021602117 *)L_3, (String_t*)L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck((GameObject_t1756533147 *)L_5);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1756533147 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// T NGUITools::AddMissingComponent<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_AddMissingComponent_TisIl2CppObject_m3399321306_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_AddMissingComponent_TisIl2CppObject_m3399321306_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddMissingComponent_TisIl2CppObject_m3399321306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		GameObject_t1756533147 * L_4 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1756533147 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001f:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T NGUITools::AddWidget<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_AddWidget_TisIl2CppObject_m1599480351_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_AddWidget_TisIl2CppObject_m1599480351_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddWidget_TisIl2CppObject_m1599480351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		int32_t L_1 = NGUITools_CalculateNextDepth_m3123726241(NULL /*static, unused*/, (GameObject_t1756533147 *)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		GameObject_t1756533147 * L_2 = ___go0;
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_3;
		NullCheck((UIWidget_t1453041918 *)(*(&V_1)));
		UIWidget_set_width_m1785998957((UIWidget_t1453041918 *)(*(&V_1)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		NullCheck((UIWidget_t1453041918 *)(*(&V_1)));
		UIWidget_set_height_m3993111822((UIWidget_t1453041918 *)(*(&V_1)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		NullCheck((UIWidget_t1453041918 *)(*(&V_1)));
		UIWidget_set_depth_m2276777530((UIWidget_t1453041918 *)(*(&V_1)), (int32_t)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// T NGUITools::AddWidget<System.Object>(UnityEngine.GameObject,System.Int32)
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_AddWidget_TisIl2CppObject_m861406406_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_AddWidget_TisIl2CppObject_m861406406_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, int32_t ___depth1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddWidget_TisIl2CppObject_m861406406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		NullCheck((UIWidget_t1453041918 *)(*(&V_0)));
		UIWidget_set_width_m1785998957((UIWidget_t1453041918 *)(*(&V_0)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		NullCheck((UIWidget_t1453041918 *)(*(&V_0)));
		UIWidget_set_height_m3993111822((UIWidget_t1453041918 *)(*(&V_0)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		int32_t L_2 = ___depth1;
		NullCheck((UIWidget_t1453041918 *)(*(&V_0)));
		UIWidget_set_depth_m2276777530((UIWidget_t1453041918 *)(*(&V_0)), (int32_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_FindInParents_TisIl2CppObject_m2434993371_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m2434993371_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindInParents_TisIl2CppObject_m2434993371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0013:
	{
		GameObject_t1756533147 * L_2 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1756533147 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006c;
		}
	}
	{
		GameObject_t1756533147 * L_6 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_6, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_7);
		Transform_t3275118058 * L_8 = Transform_get_parent_m147407266((Transform_t3275118058 *)L_7, /*hidden argument*/NULL);
		V_1 = (Transform_t3275118058 *)L_8;
		goto IL_004f;
	}

IL_003c:
	{
		Transform_t3275118058 * L_9 = V_1;
		NullCheck((Component_t3819376471 *)L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_9, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_10);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1756533147 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (Il2CppObject *)L_11;
		Transform_t3275118058 * L_12 = V_1;
		NullCheck((Transform_t3275118058 *)L_12);
		Transform_t3275118058 * L_13 = Transform_get_parent_m147407266((Transform_t3275118058 *)L_12, /*hidden argument*/NULL);
		V_1 = (Transform_t3275118058 *)L_13;
	}

IL_004f:
	{
		Transform_t3275118058 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		Il2CppObject * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_003c;
		}
	}

IL_006c:
	{
		Il2CppObject * L_18 = V_0;
		return L_18;
	}
}
// T NGUITools::FindInParents<System.Object>(UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_FindInParents_TisIl2CppObject_m1167752884_MetadataUsageId;
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m1167752884_gshared (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___trans0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindInParents_TisIl2CppObject_m1167752884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___trans0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0013:
	{
		Transform_t3275118058 * L_2 = ___trans0;
		NullCheck((Component_t3819376471 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Component_t3819376471 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// T OnlineMapsXML::A<System.Object>(System.String)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern const uint32_t OnlineMapsXML_A_TisIl2CppObject_m1393778426_MetadataUsageId;
extern "C"  Il2CppObject * OnlineMapsXML_A_TisIl2CppObject_m1393778426_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___attributeName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_A_TisIl2CppObject_m1393778426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XmlAttribute_t175731005 * V_1 = NULL;
	String_t* V_2 = NULL;
	Type_t * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_5 = NULL;
	Type_t * V_6 = NULL;
	MethodInfo_t * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlElement_t2877111883 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(49 /* System.Boolean System.Xml.XmlElement::get_HasAttributes() */, (XmlElement_t2877111883 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlAttributeCollection_t3359885287 * L_5 = VirtFuncInvoker0< XmlAttributeCollection_t3359885287 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, (XmlNode_t616554813 *)L_4);
		String_t* L_6 = ___attributeName0;
		NullCheck((XmlAttributeCollection_t3359885287 *)L_5);
		XmlAttribute_t175731005 * L_7 = XmlAttributeCollection_get_ItemOf_m2084104187((XmlAttributeCollection_t3359885287 *)L_5, (String_t*)L_6, /*hidden argument*/NULL);
		V_1 = (XmlAttribute_t175731005 *)L_7;
		XmlAttribute_t175731005 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_9 = V_0;
		return L_9;
	}

IL_0047:
	{
		XmlAttribute_t175731005 * L_10 = V_1;
		NullCheck((XmlNode_t616554813 *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(30 /* System.String System.Xml.XmlNode::get_Value() */, (XmlNode_t616554813 *)L_10);
		V_2 = (String_t*)L_11;
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0063;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_14 = V_0;
		return L_14;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_3 = (Type_t *)L_15;
		Type_t * L_16 = V_3;
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_16) == ((Il2CppObject*)(Type_t *)L_17))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_18 = V_2;
		Type_t * L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_20 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_18, (Type_t *)L_19, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_20, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_008b:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_21 = V_0;
		V_4 = (Il2CppObject *)L_21;
		Type_t * L_22 = V_3;
		NullCheck((Type_t *)L_22);
		PropertyInfoU5BU5D_t1736152084* L_23 = Type_GetProperties_m2803026104((Type_t *)L_22, /*hidden argument*/NULL);
		V_5 = (PropertyInfoU5BU5D_t1736152084*)L_23;
		Type_t * L_24 = V_3;
		V_6 = (Type_t *)L_24;
		PropertyInfoU5BU5D_t1736152084* L_25 = V_5;
		NullCheck(L_25);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_00cf;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_26 = V_5;
		NullCheck(L_26);
		int32_t L_27 = 0;
		PropertyInfo_t * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((MemberInfo_t *)L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_29, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00cf;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_31 = V_5;
		NullCheck(L_31);
		int32_t L_32 = 1;
		PropertyInfo_t * L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck((PropertyInfo_t *)L_33);
		Type_t * L_34 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_33);
		V_6 = (Type_t *)L_34;
	}

IL_00cf:
	try
	{ // begin try (depth: 1)
		Type_t * L_35 = V_6;
		TypeU5BU5D_t1664964607* L_36 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_37);
		NullCheck((Type_t *)L_35);
		MethodInfo_t * L_38 = Type_GetMethod_m2079823229((Type_t *)L_35, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_36, /*hidden argument*/NULL);
		V_7 = (MethodInfo_t *)L_38;
		MethodInfo_t * L_39 = V_7;
		StringU5BU5D_t1642385972* L_40 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_41 = V_2;
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_41);
		NullCheck((MethodBase_t904190842 *)L_39);
		Il2CppObject * L_42 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_39, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_40, /*hidden argument*/NULL);
		V_4 = (Il2CppObject *)((Il2CppObject *)Castclass(L_42, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0111;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_010e;
		throw e;
	}

CATCH_010e:
	{ // begin catch(System.Exception)
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0111:
	{
		Il2CppObject * L_43 = V_4;
		return L_43;
	}
}
// T OnlineMapsXML::A<System.Single>(System.String)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern const uint32_t OnlineMapsXML_A_TisSingle_t2076509932_m3191408141_MetadataUsageId;
extern "C"  float OnlineMapsXML_A_TisSingle_t2076509932_m3191408141_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___attributeName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_A_TisSingle_t2076509932_m3191408141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	XmlAttribute_t175731005 * V_1 = NULL;
	String_t* V_2 = NULL;
	Type_t * V_3 = NULL;
	float V_4 = 0.0f;
	PropertyInfoU5BU5D_t1736152084* V_5 = NULL;
	Type_t * V_6 = NULL;
	MethodInfo_t * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlElement_t2877111883 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(49 /* System.Boolean System.Xml.XmlElement::get_HasAttributes() */, (XmlElement_t2877111883 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlAttributeCollection_t3359885287 * L_5 = VirtFuncInvoker0< XmlAttributeCollection_t3359885287 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, (XmlNode_t616554813 *)L_4);
		String_t* L_6 = ___attributeName0;
		NullCheck((XmlAttributeCollection_t3359885287 *)L_5);
		XmlAttribute_t175731005 * L_7 = XmlAttributeCollection_get_ItemOf_m2084104187((XmlAttributeCollection_t3359885287 *)L_5, (String_t*)L_6, /*hidden argument*/NULL);
		V_1 = (XmlAttribute_t175731005 *)L_7;
		XmlAttribute_t175731005 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_9 = V_0;
		return L_9;
	}

IL_0047:
	{
		XmlAttribute_t175731005 * L_10 = V_1;
		NullCheck((XmlNode_t616554813 *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(30 /* System.String System.Xml.XmlNode::get_Value() */, (XmlNode_t616554813 *)L_10);
		V_2 = (String_t*)L_11;
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0063;
		}
	}
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_14 = V_0;
		return L_14;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_3 = (Type_t *)L_15;
		Type_t * L_16 = V_3;
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_16) == ((Il2CppObject*)(Type_t *)L_17))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_18 = V_2;
		Type_t * L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_20 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_18, (Type_t *)L_19, /*hidden argument*/NULL);
		return ((*(float*)((float*)UnBox (L_20, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_008b:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_21 = V_0;
		V_4 = (float)L_21;
		Type_t * L_22 = V_3;
		NullCheck((Type_t *)L_22);
		PropertyInfoU5BU5D_t1736152084* L_23 = Type_GetProperties_m2803026104((Type_t *)L_22, /*hidden argument*/NULL);
		V_5 = (PropertyInfoU5BU5D_t1736152084*)L_23;
		Type_t * L_24 = V_3;
		V_6 = (Type_t *)L_24;
		PropertyInfoU5BU5D_t1736152084* L_25 = V_5;
		NullCheck(L_25);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_00cf;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_26 = V_5;
		NullCheck(L_26);
		int32_t L_27 = 0;
		PropertyInfo_t * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((MemberInfo_t *)L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_29, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00cf;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_31 = V_5;
		NullCheck(L_31);
		int32_t L_32 = 1;
		PropertyInfo_t * L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck((PropertyInfo_t *)L_33);
		Type_t * L_34 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_33);
		V_6 = (Type_t *)L_34;
	}

IL_00cf:
	try
	{ // begin try (depth: 1)
		Type_t * L_35 = V_6;
		TypeU5BU5D_t1664964607* L_36 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_37);
		NullCheck((Type_t *)L_35);
		MethodInfo_t * L_38 = Type_GetMethod_m2079823229((Type_t *)L_35, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_36, /*hidden argument*/NULL);
		V_7 = (MethodInfo_t *)L_38;
		MethodInfo_t * L_39 = V_7;
		StringU5BU5D_t1642385972* L_40 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_41 = V_2;
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_41);
		NullCheck((MethodBase_t904190842 *)L_39);
		Il2CppObject * L_42 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_39, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_40, /*hidden argument*/NULL);
		V_4 = (float)((*(float*)((float*)UnBox (L_42, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		goto IL_0111;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_010e;
		throw e;
	}

CATCH_010e:
	{ // begin catch(System.Exception)
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0111:
	{
		float L_43 = V_4;
		return L_43;
	}
}
// T OnlineMapsXML::Find<System.Int32>(System.String)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t2877111883_il2cpp_TypeInfo_var;
extern const uint32_t OnlineMapsXML_Find_TisInt32_t2071877448_m1886308485_MetadataUsageId;
extern "C"  int32_t OnlineMapsXML_Find_TisInt32_t2071877448_m1886308485_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Find_TisInt32_t2071877448_m1886308485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___xpath0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlNode_t616554813 * L_6 = XmlNode_SelectSingleNode_m1354503634((XmlNode_t616554813 *)L_4, (String_t*)L_5, /*hidden argument*/NULL);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_7 = ((  int32_t (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)((XmlElement_t2877111883 *)IsInst(L_6, XmlElement_t2877111883_il2cpp_TypeInfo_var)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// T OnlineMapsXML::Find<System.Object>(System.String)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t2877111883_il2cpp_TypeInfo_var;
extern const uint32_t OnlineMapsXML_Find_TisIl2CppObject_m3977458542_MetadataUsageId;
extern "C"  Il2CppObject * OnlineMapsXML_Find_TisIl2CppObject_m3977458542_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Find_TisIl2CppObject_m3977458542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___xpath0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlNode_t616554813 * L_6 = XmlNode_SelectSingleNode_m1354503634((XmlNode_t616554813 *)L_4, (String_t*)L_5, /*hidden argument*/NULL);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)((XmlElement_t2877111883 *)IsInst(L_6, XmlElement_t2877111883_il2cpp_TypeInfo_var)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// T OnlineMapsXML::Get<System.Int32>(System.String)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MetadataUsageId;
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___childName0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlElement_t2877111883 * L_6 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_4, (String_t*)L_5);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_7 = ((  int32_t (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// T OnlineMapsXML::Get<System.Int32>(System.String,T)
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m837676824_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, int32_t ___defaultValue1, const MethodInfo* method)
{
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		int32_t L_3 = ___defaultValue1;
		return L_3;
	}

IL_001d:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___childName0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlElement_t2877111883 * L_6 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_4, (String_t*)L_5);
		int32_t L_7 = ___defaultValue1;
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_8 = ((  int32_t (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_8;
	}
}
// T OnlineMapsXML::Get<System.Int32>(System.Xml.XmlElement)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern const Il2CppType* Color_t2020392075_0_0_0_var;
extern const Il2CppType* Color32_t874517518_0_0_0_var;
extern const Il2CppType* OnlineMapsRange_t3791609909_0_0_0_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral372029429;
extern Il2CppCodeGenString* _stringLiteral372029432;
extern Il2CppCodeGenString* _stringLiteral339800066;
extern Il2CppCodeGenString* _stringLiteral696030436;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t OnlineMapsXML_Get_TisInt32_t2071877448_m3769157143_MetadataUsageId;
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m3769157143_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisInt32_t2071877448_m3769157143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	Type_t * V_2 = NULL;
	int32_t V_3 = 0;
	PropertyInfoU5BU5D_t1736152084* V_4 = NULL;
	Type_t * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	Exception_t1927440687 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		XmlElement_t2877111883 * L_2 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Xml.XmlNode::get_InnerXml() */, (XmlNode_t616554813 *)L_2);
		V_1 = (String_t*)L_3;
		String_t* L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_6 = V_0;
		return L_6;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_2 = (Type_t *)L_7;
		Type_t * L_8 = V_2;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_10 = V_1;
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0054:
	{
		Type_t * L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_009c;
		}
	}
	{
		XmlElement_t2877111883 * L_15 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_15);
		XmlElement_t2877111883 * L_16 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_15, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_17 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_16, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_18 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_18);
		XmlElement_t2877111883 * L_19 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_18, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_20 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_19, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, (float)L_17, (float)L_20, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_22);
		Type_t * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_25 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_23, (Type_t *)L_24, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_009c:
	{
		Type_t * L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_00f5;
		}
	}
	{
		XmlElement_t2877111883 * L_28 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_28);
		XmlElement_t2877111883 * L_29 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_28, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_30 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_29, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_31 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_31);
		XmlElement_t2877111883 * L_32 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_31, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_33 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_32, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_34 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_34);
		XmlElement_t2877111883 * L_35 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_34, (String_t*)_stringLiteral372029432);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_36 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_35, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, (float)L_30, (float)L_33, (float)L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_39, (Type_t *)L_40, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_00f5:
	{
		Type_t * L_42 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color_t2020392075_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_42) == ((Il2CppObject*)(Type_t *)L_43)))
		{
			goto IL_0115;
		}
	}
	{
		Type_t * L_44 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color32_t874517518_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_44) == ((Il2CppObject*)(Type_t *)L_45))))
		{
			goto IL_012c;
		}
	}

IL_0115:
	{
		String_t* L_46 = V_1;
		Color_t2020392075  L_47 = OnlineMapsUtils_HexToColor_m3899320166(NULL /*static, unused*/, (String_t*)L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = L_47;
		Il2CppObject * L_49 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_48);
		Type_t * L_50 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_49, (Type_t *)L_50, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_51, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_012c:
	{
		Type_t * L_52 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(OnlineMapsRange_t3791609909_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_52) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_0163;
		}
	}
	{
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_54 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral339800066, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_55 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral696030436, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		OnlineMapsRange_t3791609909 * L_56 = (OnlineMapsRange_t3791609909 *)il2cpp_codegen_object_new(OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var);
		OnlineMapsRange__ctor_m2214011296(L_56, (int32_t)L_54, (int32_t)L_55, /*hidden argument*/NULL);
		Type_t * L_57 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_58, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0163:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_59 = V_0;
		V_3 = (int32_t)L_59;
		Type_t * L_60 = V_2;
		NullCheck((Type_t *)L_60);
		PropertyInfoU5BU5D_t1736152084* L_61 = Type_GetProperties_m2803026104((Type_t *)L_60, /*hidden argument*/NULL);
		V_4 = (PropertyInfoU5BU5D_t1736152084*)L_61;
		Type_t * L_62 = V_2;
		V_5 = (Type_t *)L_62;
		PropertyInfoU5BU5D_t1736152084* L_63 = V_4;
		NullCheck(L_63);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_64 = V_4;
		NullCheck(L_64);
		int32_t L_65 = 0;
		PropertyInfo_t * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck((MemberInfo_t *)L_66);
		String_t* L_67 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_67, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_69 = V_4;
		NullCheck(L_69);
		int32_t L_70 = 1;
		PropertyInfo_t * L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck((PropertyInfo_t *)L_71);
		Type_t * L_72 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_71);
		V_5 = (Type_t *)L_72;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		Type_t * L_73 = V_5;
		TypeU5BU5D_t1664964607* L_74 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		NullCheck((Type_t *)L_73);
		MethodInfo_t * L_76 = Type_GetMethod_m2079823229((Type_t *)L_73, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_74, /*hidden argument*/NULL);
		V_6 = (MethodInfo_t *)L_76;
		MethodInfo_t * L_77 = V_6;
		StringU5BU5D_t1642385972* L_78 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_79 = V_1;
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_79);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_79);
		NullCheck((MethodBase_t904190842 *)L_77);
		Il2CppObject * L_80 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_77, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_78, /*hidden argument*/NULL);
		V_3 = (int32_t)((*(int32_t*)((int32_t*)UnBox (L_80, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		goto IL_0205;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01e4;
		throw e;
	}

CATCH_01e4:
	{ // begin catch(System.Exception)
		V_7 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_81 = V_7;
		NullCheck((Exception_t1927440687 *)L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_81);
		Exception_t1927440687 * L_83 = V_7;
		NullCheck((Exception_t1927440687 *)L_83);
		String_t* L_84 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, (Exception_t1927440687 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_82, (String_t*)_stringLiteral372029352, (String_t*)L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0205:
	{
		int32_t L_86 = V_3;
		return L_86;
	}
}
// T OnlineMapsXML::Get<System.Int32>(System.Xml.XmlElement,T)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern const Il2CppType* Color_t2020392075_0_0_0_var;
extern const Il2CppType* Color32_t874517518_0_0_0_var;
extern const Il2CppType* OnlineMapsRange_t3791609909_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral372029429;
extern Il2CppCodeGenString* _stringLiteral372029432;
extern Il2CppCodeGenString* _stringLiteral339800066;
extern Il2CppCodeGenString* _stringLiteral696030436;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t OnlineMapsXML_Get_TisInt32_t2071877448_m2886396799_MetadataUsageId;
extern "C"  int32_t OnlineMapsXML_Get_TisInt32_t2071877448_m2886396799_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, int32_t ___defaultValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisInt32_t2071877448_m2886396799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Type_t * V_1 = NULL;
	int32_t V_2 = 0;
	PropertyInfoU5BU5D_t1736152084* V_3 = NULL;
	Type_t * V_4 = NULL;
	MethodInfo_t * V_5 = NULL;
	Exception_t1927440687 * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___defaultValue1;
		return L_1;
	}

IL_0008:
	{
		XmlElement_t2877111883 * L_2 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Xml.XmlNode::get_InnerXml() */, (XmlNode_t616554813 *)L_2);
		V_0 = (String_t*)L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_6 = ___defaultValue1;
		return L_6;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_7;
		Type_t * L_8 = V_1;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_10 = V_0;
		Type_t * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0044:
	{
		Type_t * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_008c;
		}
	}
	{
		XmlElement_t2877111883 * L_15 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_15);
		XmlElement_t2877111883 * L_16 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_15, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_17 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_16, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_18 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_18);
		XmlElement_t2877111883 * L_19 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_18, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_20 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_19, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, (float)L_17, (float)L_20, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_22);
		Type_t * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_25 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_23, (Type_t *)L_24, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_008c:
	{
		Type_t * L_26 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_00e5;
		}
	}
	{
		XmlElement_t2877111883 * L_28 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_28);
		XmlElement_t2877111883 * L_29 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_28, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_30 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_29, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_31 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_31);
		XmlElement_t2877111883 * L_32 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_31, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_33 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_32, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_34 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_34);
		XmlElement_t2877111883 * L_35 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_34, (String_t*)_stringLiteral372029432);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_36 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_35, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, (float)L_30, (float)L_33, (float)L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_39, (Type_t *)L_40, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_00e5:
	{
		Type_t * L_42 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color_t2020392075_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_42) == ((Il2CppObject*)(Type_t *)L_43)))
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color32_t874517518_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_44) == ((Il2CppObject*)(Type_t *)L_45))))
		{
			goto IL_011c;
		}
	}

IL_0105:
	{
		String_t* L_46 = V_0;
		Color_t2020392075  L_47 = OnlineMapsUtils_HexToColor_m3899320166(NULL /*static, unused*/, (String_t*)L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = L_47;
		Il2CppObject * L_49 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_48);
		Type_t * L_50 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_49, (Type_t *)L_50, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_51, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_011c:
	{
		Type_t * L_52 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(OnlineMapsRange_t3791609909_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_52) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_0153;
		}
	}
	{
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_54 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral339800066, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_55 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral696030436, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		OnlineMapsRange_t3791609909 * L_56 = (OnlineMapsRange_t3791609909 *)il2cpp_codegen_object_new(OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var);
		OnlineMapsRange__ctor_m2214011296(L_56, (int32_t)L_54, (int32_t)L_55, /*hidden argument*/NULL);
		Type_t * L_57 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_58, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0153:
	{
		int32_t L_59 = ___defaultValue1;
		V_2 = (int32_t)L_59;
		Type_t * L_60 = V_1;
		NullCheck((Type_t *)L_60);
		PropertyInfoU5BU5D_t1736152084* L_61 = Type_GetProperties_m2803026104((Type_t *)L_60, /*hidden argument*/NULL);
		V_3 = (PropertyInfoU5BU5D_t1736152084*)L_61;
		Type_t * L_62 = V_1;
		V_4 = (Type_t *)L_62;
		PropertyInfoU5BU5D_t1736152084* L_63 = V_3;
		NullCheck(L_63);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_018a;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_64 = V_3;
		NullCheck(L_64);
		int32_t L_65 = 0;
		PropertyInfo_t * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck((MemberInfo_t *)L_66);
		String_t* L_67 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_67, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_018a;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_69 = V_3;
		NullCheck(L_69);
		int32_t L_70 = 1;
		PropertyInfo_t * L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck((PropertyInfo_t *)L_71);
		Type_t * L_72 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_71);
		V_4 = (Type_t *)L_72;
	}

IL_018a:
	try
	{ // begin try (depth: 1)
		Type_t * L_73 = V_4;
		TypeU5BU5D_t1664964607* L_74 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		NullCheck((Type_t *)L_73);
		MethodInfo_t * L_76 = Type_GetMethod_m2079823229((Type_t *)L_73, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_74, /*hidden argument*/NULL);
		V_5 = (MethodInfo_t *)L_76;
		MethodInfo_t * L_77 = V_5;
		StringU5BU5D_t1642385972* L_78 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_79 = V_0;
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_79);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_79);
		NullCheck((MethodBase_t904190842 *)L_77);
		Il2CppObject * L_80 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_77, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_78, /*hidden argument*/NULL);
		V_2 = (int32_t)((*(int32_t*)((int32_t*)UnBox (L_80, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		goto IL_01e9;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01c8;
		throw e;
	}

CATCH_01c8:
	{ // begin catch(System.Exception)
		V_6 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_81 = V_6;
		NullCheck((Exception_t1927440687 *)L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_81);
		Exception_t1927440687 * L_83 = V_6;
		NullCheck((Exception_t1927440687 *)L_83);
		String_t* L_84 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, (Exception_t1927440687 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_82, (String_t*)_stringLiteral372029352, (String_t*)L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_01e9:
	{
		int32_t L_86 = V_2;
		return L_86;
	}
}
// T OnlineMapsXML::Get<System.Object>(System.String)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t OnlineMapsXML_Get_TisIl2CppObject_m3924433329_MetadataUsageId;
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m3924433329_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisIl2CppObject_m3924433329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___childName0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlElement_t2877111883 * L_6 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_4, (String_t*)L_5);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// T OnlineMapsXML::Get<System.Object>(System.String,T)
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m448861289_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, Il2CppObject * ___defaultValue1, const MethodInfo* method)
{
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		Il2CppObject * L_3 = ___defaultValue1;
		return L_3;
	}

IL_001d:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___childName0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlElement_t2877111883 * L_6 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_4, (String_t*)L_5);
		Il2CppObject * L_7 = ___defaultValue1;
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_8;
	}
}
// T OnlineMapsXML::Get<System.Object>(System.Xml.XmlElement)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern const Il2CppType* Color_t2020392075_0_0_0_var;
extern const Il2CppType* Color32_t874517518_0_0_0_var;
extern const Il2CppType* OnlineMapsRange_t3791609909_0_0_0_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral372029429;
extern Il2CppCodeGenString* _stringLiteral372029432;
extern Il2CppCodeGenString* _stringLiteral339800066;
extern Il2CppCodeGenString* _stringLiteral696030436;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t OnlineMapsXML_Get_TisIl2CppObject_m704259572_MetadataUsageId;
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m704259572_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisIl2CppObject_m704259572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Type_t * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_4 = NULL;
	Type_t * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	Exception_t1927440687 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		XmlElement_t2877111883 * L_2 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Xml.XmlNode::get_InnerXml() */, (XmlNode_t616554813 *)L_2);
		V_1 = (String_t*)L_3;
		String_t* L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_6 = V_0;
		return L_6;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_2 = (Type_t *)L_7;
		Type_t * L_8 = V_2;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_10 = V_1;
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0054:
	{
		Type_t * L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_009c;
		}
	}
	{
		XmlElement_t2877111883 * L_15 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_15);
		XmlElement_t2877111883 * L_16 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_15, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_17 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_16, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_18 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_18);
		XmlElement_t2877111883 * L_19 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_18, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_20 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_19, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, (float)L_17, (float)L_20, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_22);
		Type_t * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_25 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_23, (Type_t *)L_24, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_009c:
	{
		Type_t * L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_00f5;
		}
	}
	{
		XmlElement_t2877111883 * L_28 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_28);
		XmlElement_t2877111883 * L_29 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_28, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_30 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_29, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_31 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_31);
		XmlElement_t2877111883 * L_32 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_31, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_33 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_32, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_34 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_34);
		XmlElement_t2877111883 * L_35 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_34, (String_t*)_stringLiteral372029432);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_36 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_35, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, (float)L_30, (float)L_33, (float)L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_39, (Type_t *)L_40, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00f5:
	{
		Type_t * L_42 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color_t2020392075_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_42) == ((Il2CppObject*)(Type_t *)L_43)))
		{
			goto IL_0115;
		}
	}
	{
		Type_t * L_44 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color32_t874517518_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_44) == ((Il2CppObject*)(Type_t *)L_45))))
		{
			goto IL_012c;
		}
	}

IL_0115:
	{
		String_t* L_46 = V_1;
		Color_t2020392075  L_47 = OnlineMapsUtils_HexToColor_m3899320166(NULL /*static, unused*/, (String_t*)L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = L_47;
		Il2CppObject * L_49 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_48);
		Type_t * L_50 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_49, (Type_t *)L_50, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_51, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_012c:
	{
		Type_t * L_52 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(OnlineMapsRange_t3791609909_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_52) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_0163;
		}
	}
	{
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_54 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral339800066, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_55 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral696030436, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		OnlineMapsRange_t3791609909 * L_56 = (OnlineMapsRange_t3791609909 *)il2cpp_codegen_object_new(OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var);
		OnlineMapsRange__ctor_m2214011296(L_56, (int32_t)L_54, (int32_t)L_55, /*hidden argument*/NULL);
		Type_t * L_57 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_58, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0163:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_59 = V_0;
		V_3 = (Il2CppObject *)L_59;
		Type_t * L_60 = V_2;
		NullCheck((Type_t *)L_60);
		PropertyInfoU5BU5D_t1736152084* L_61 = Type_GetProperties_m2803026104((Type_t *)L_60, /*hidden argument*/NULL);
		V_4 = (PropertyInfoU5BU5D_t1736152084*)L_61;
		Type_t * L_62 = V_2;
		V_5 = (Type_t *)L_62;
		PropertyInfoU5BU5D_t1736152084* L_63 = V_4;
		NullCheck(L_63);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_64 = V_4;
		NullCheck(L_64);
		int32_t L_65 = 0;
		PropertyInfo_t * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck((MemberInfo_t *)L_66);
		String_t* L_67 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_67, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_69 = V_4;
		NullCheck(L_69);
		int32_t L_70 = 1;
		PropertyInfo_t * L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck((PropertyInfo_t *)L_71);
		Type_t * L_72 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_71);
		V_5 = (Type_t *)L_72;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		Type_t * L_73 = V_5;
		TypeU5BU5D_t1664964607* L_74 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		NullCheck((Type_t *)L_73);
		MethodInfo_t * L_76 = Type_GetMethod_m2079823229((Type_t *)L_73, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_74, /*hidden argument*/NULL);
		V_6 = (MethodInfo_t *)L_76;
		MethodInfo_t * L_77 = V_6;
		StringU5BU5D_t1642385972* L_78 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_79 = V_1;
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_79);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_79);
		NullCheck((MethodBase_t904190842 *)L_77);
		Il2CppObject * L_80 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_77, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_78, /*hidden argument*/NULL);
		V_3 = (Il2CppObject *)((Il2CppObject *)Castclass(L_80, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0205;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01e4;
		throw e;
	}

CATCH_01e4:
	{ // begin catch(System.Exception)
		V_7 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_81 = V_7;
		NullCheck((Exception_t1927440687 *)L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_81);
		Exception_t1927440687 * L_83 = V_7;
		NullCheck((Exception_t1927440687 *)L_83);
		String_t* L_84 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, (Exception_t1927440687 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_82, (String_t*)_stringLiteral372029352, (String_t*)L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0205:
	{
		Il2CppObject * L_86 = V_3;
		return L_86;
	}
}
// T OnlineMapsXML::Get<System.Object>(System.Xml.XmlElement,T)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern const Il2CppType* Color_t2020392075_0_0_0_var;
extern const Il2CppType* Color32_t874517518_0_0_0_var;
extern const Il2CppType* OnlineMapsRange_t3791609909_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral372029429;
extern Il2CppCodeGenString* _stringLiteral372029432;
extern Il2CppCodeGenString* _stringLiteral339800066;
extern Il2CppCodeGenString* _stringLiteral696030436;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t OnlineMapsXML_Get_TisIl2CppObject_m1557309828_MetadataUsageId;
extern "C"  Il2CppObject * OnlineMapsXML_Get_TisIl2CppObject_m1557309828_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, Il2CppObject * ___defaultValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisIl2CppObject_m1557309828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Type_t * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_3 = NULL;
	Type_t * V_4 = NULL;
	MethodInfo_t * V_5 = NULL;
	Exception_t1927440687 * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		Il2CppObject * L_1 = ___defaultValue1;
		return L_1;
	}

IL_0008:
	{
		XmlElement_t2877111883 * L_2 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Xml.XmlNode::get_InnerXml() */, (XmlNode_t616554813 *)L_2);
		V_0 = (String_t*)L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_6 = ___defaultValue1;
		return L_6;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_7;
		Type_t * L_8 = V_1;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_10 = V_0;
		Type_t * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0044:
	{
		Type_t * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_008c;
		}
	}
	{
		XmlElement_t2877111883 * L_15 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_15);
		XmlElement_t2877111883 * L_16 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_15, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_17 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_16, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_18 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_18);
		XmlElement_t2877111883 * L_19 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_18, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_20 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_19, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, (float)L_17, (float)L_20, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_22);
		Type_t * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_25 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_23, (Type_t *)L_24, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_008c:
	{
		Type_t * L_26 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_00e5;
		}
	}
	{
		XmlElement_t2877111883 * L_28 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_28);
		XmlElement_t2877111883 * L_29 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_28, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_30 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_29, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_31 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_31);
		XmlElement_t2877111883 * L_32 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_31, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_33 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_32, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_34 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_34);
		XmlElement_t2877111883 * L_35 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_34, (String_t*)_stringLiteral372029432);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_36 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_35, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, (float)L_30, (float)L_33, (float)L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_39, (Type_t *)L_40, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00e5:
	{
		Type_t * L_42 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color_t2020392075_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_42) == ((Il2CppObject*)(Type_t *)L_43)))
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color32_t874517518_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_44) == ((Il2CppObject*)(Type_t *)L_45))))
		{
			goto IL_011c;
		}
	}

IL_0105:
	{
		String_t* L_46 = V_0;
		Color_t2020392075  L_47 = OnlineMapsUtils_HexToColor_m3899320166(NULL /*static, unused*/, (String_t*)L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = L_47;
		Il2CppObject * L_49 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_48);
		Type_t * L_50 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_49, (Type_t *)L_50, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_51, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_011c:
	{
		Type_t * L_52 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(OnlineMapsRange_t3791609909_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_52) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_0153;
		}
	}
	{
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_54 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral339800066, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_55 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral696030436, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		OnlineMapsRange_t3791609909 * L_56 = (OnlineMapsRange_t3791609909 *)il2cpp_codegen_object_new(OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var);
		OnlineMapsRange__ctor_m2214011296(L_56, (int32_t)L_54, (int32_t)L_55, /*hidden argument*/NULL);
		Type_t * L_57 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_58, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0153:
	{
		Il2CppObject * L_59 = ___defaultValue1;
		V_2 = (Il2CppObject *)L_59;
		Type_t * L_60 = V_1;
		NullCheck((Type_t *)L_60);
		PropertyInfoU5BU5D_t1736152084* L_61 = Type_GetProperties_m2803026104((Type_t *)L_60, /*hidden argument*/NULL);
		V_3 = (PropertyInfoU5BU5D_t1736152084*)L_61;
		Type_t * L_62 = V_1;
		V_4 = (Type_t *)L_62;
		PropertyInfoU5BU5D_t1736152084* L_63 = V_3;
		NullCheck(L_63);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_018a;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_64 = V_3;
		NullCheck(L_64);
		int32_t L_65 = 0;
		PropertyInfo_t * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck((MemberInfo_t *)L_66);
		String_t* L_67 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_67, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_018a;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_69 = V_3;
		NullCheck(L_69);
		int32_t L_70 = 1;
		PropertyInfo_t * L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck((PropertyInfo_t *)L_71);
		Type_t * L_72 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_71);
		V_4 = (Type_t *)L_72;
	}

IL_018a:
	try
	{ // begin try (depth: 1)
		Type_t * L_73 = V_4;
		TypeU5BU5D_t1664964607* L_74 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		NullCheck((Type_t *)L_73);
		MethodInfo_t * L_76 = Type_GetMethod_m2079823229((Type_t *)L_73, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_74, /*hidden argument*/NULL);
		V_5 = (MethodInfo_t *)L_76;
		MethodInfo_t * L_77 = V_5;
		StringU5BU5D_t1642385972* L_78 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_79 = V_0;
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_79);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_79);
		NullCheck((MethodBase_t904190842 *)L_77);
		Il2CppObject * L_80 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_77, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_78, /*hidden argument*/NULL);
		V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(L_80, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_01e9;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01c8;
		throw e;
	}

CATCH_01c8:
	{ // begin catch(System.Exception)
		V_6 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_81 = V_6;
		NullCheck((Exception_t1927440687 *)L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_81);
		Exception_t1927440687 * L_83 = V_6;
		NullCheck((Exception_t1927440687 *)L_83);
		String_t* L_84 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, (Exception_t1927440687 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_82, (String_t*)_stringLiteral372029352, (String_t*)L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_01e9:
	{
		Il2CppObject * L_86 = V_2;
		return L_86;
	}
}
// T OnlineMapsXML::Get<System.Single>(System.String)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t OnlineMapsXML_Get_TisSingle_t2076509932_m3546836458_MetadataUsageId;
extern "C"  float OnlineMapsXML_Get_TisSingle_t2076509932_m3546836458_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisSingle_t2076509932_m3546836458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___childName0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlElement_t2877111883 * L_6 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_4, (String_t*)L_5);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_7 = ((  float (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// T OnlineMapsXML::Get<System.Single>(System.Xml.XmlElement)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern const Il2CppType* Color_t2020392075_0_0_0_var;
extern const Il2CppType* Color32_t874517518_0_0_0_var;
extern const Il2CppType* OnlineMapsRange_t3791609909_0_0_0_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral372029429;
extern Il2CppCodeGenString* _stringLiteral372029432;
extern Il2CppCodeGenString* _stringLiteral339800066;
extern Il2CppCodeGenString* _stringLiteral696030436;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MetadataUsageId;
extern "C"  float OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	String_t* V_1 = NULL;
	Type_t * V_2 = NULL;
	float V_3 = 0.0f;
	PropertyInfoU5BU5D_t1736152084* V_4 = NULL;
	Type_t * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	Exception_t1927440687 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		XmlElement_t2877111883 * L_2 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Xml.XmlNode::get_InnerXml() */, (XmlNode_t616554813 *)L_2);
		V_1 = (String_t*)L_3;
		String_t* L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_6 = V_0;
		return L_6;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_2 = (Type_t *)L_7;
		Type_t * L_8 = V_2;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_10 = V_1;
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		return ((*(float*)((float*)UnBox (L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0054:
	{
		Type_t * L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_009c;
		}
	}
	{
		XmlElement_t2877111883 * L_15 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_15);
		XmlElement_t2877111883 * L_16 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_15, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_17 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_16, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_18 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_18);
		XmlElement_t2877111883 * L_19 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_18, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_20 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_19, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, (float)L_17, (float)L_20, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_22);
		Type_t * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_25 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_23, (Type_t *)L_24, /*hidden argument*/NULL);
		return ((*(float*)((float*)UnBox (L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_009c:
	{
		Type_t * L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_00f5;
		}
	}
	{
		XmlElement_t2877111883 * L_28 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_28);
		XmlElement_t2877111883 * L_29 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_28, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_30 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_29, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_31 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_31);
		XmlElement_t2877111883 * L_32 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_31, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_33 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_32, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_34 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_34);
		XmlElement_t2877111883 * L_35 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_34, (String_t*)_stringLiteral372029432);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_36 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_35, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, (float)L_30, (float)L_33, (float)L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_39, (Type_t *)L_40, /*hidden argument*/NULL);
		return ((*(float*)((float*)UnBox (L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_00f5:
	{
		Type_t * L_42 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color_t2020392075_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_42) == ((Il2CppObject*)(Type_t *)L_43)))
		{
			goto IL_0115;
		}
	}
	{
		Type_t * L_44 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color32_t874517518_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_44) == ((Il2CppObject*)(Type_t *)L_45))))
		{
			goto IL_012c;
		}
	}

IL_0115:
	{
		String_t* L_46 = V_1;
		Color_t2020392075  L_47 = OnlineMapsUtils_HexToColor_m3899320166(NULL /*static, unused*/, (String_t*)L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = L_47;
		Il2CppObject * L_49 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_48);
		Type_t * L_50 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_49, (Type_t *)L_50, /*hidden argument*/NULL);
		return ((*(float*)((float*)UnBox (L_51, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_012c:
	{
		Type_t * L_52 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(OnlineMapsRange_t3791609909_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_52) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_0163;
		}
	}
	{
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_54 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral339800066, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_55 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral696030436, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		OnlineMapsRange_t3791609909 * L_56 = (OnlineMapsRange_t3791609909 *)il2cpp_codegen_object_new(OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var);
		OnlineMapsRange__ctor_m2214011296(L_56, (int32_t)L_54, (int32_t)L_55, /*hidden argument*/NULL);
		Type_t * L_57 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		return ((*(float*)((float*)UnBox (L_58, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0163:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_59 = V_0;
		V_3 = (float)L_59;
		Type_t * L_60 = V_2;
		NullCheck((Type_t *)L_60);
		PropertyInfoU5BU5D_t1736152084* L_61 = Type_GetProperties_m2803026104((Type_t *)L_60, /*hidden argument*/NULL);
		V_4 = (PropertyInfoU5BU5D_t1736152084*)L_61;
		Type_t * L_62 = V_2;
		V_5 = (Type_t *)L_62;
		PropertyInfoU5BU5D_t1736152084* L_63 = V_4;
		NullCheck(L_63);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_64 = V_4;
		NullCheck(L_64);
		int32_t L_65 = 0;
		PropertyInfo_t * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck((MemberInfo_t *)L_66);
		String_t* L_67 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_67, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_69 = V_4;
		NullCheck(L_69);
		int32_t L_70 = 1;
		PropertyInfo_t * L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck((PropertyInfo_t *)L_71);
		Type_t * L_72 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_71);
		V_5 = (Type_t *)L_72;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		Type_t * L_73 = V_5;
		TypeU5BU5D_t1664964607* L_74 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		NullCheck((Type_t *)L_73);
		MethodInfo_t * L_76 = Type_GetMethod_m2079823229((Type_t *)L_73, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_74, /*hidden argument*/NULL);
		V_6 = (MethodInfo_t *)L_76;
		MethodInfo_t * L_77 = V_6;
		StringU5BU5D_t1642385972* L_78 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_79 = V_1;
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_79);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_79);
		NullCheck((MethodBase_t904190842 *)L_77);
		Il2CppObject * L_80 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_77, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_78, /*hidden argument*/NULL);
		V_3 = (float)((*(float*)((float*)UnBox (L_80, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		goto IL_0205;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01e4;
		throw e;
	}

CATCH_01e4:
	{ // begin catch(System.Exception)
		V_7 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_81 = V_7;
		NullCheck((Exception_t1927440687 *)L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_81);
		Exception_t1927440687 * L_83 = V_7;
		NullCheck((Exception_t1927440687 *)L_83);
		String_t* L_84 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, (Exception_t1927440687 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_82, (String_t*)_stringLiteral372029352, (String_t*)L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0205:
	{
		float L_86 = V_3;
		return L_86;
	}
}
// T OnlineMapsXML::Get<UnityEngine.Vector2>(System.String)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t OnlineMapsXML_Get_TisVector2_t2243707579_m2172396179_MetadataUsageId;
extern "C"  Vector2_t2243707579  OnlineMapsXML_Get_TisVector2_t2243707579_m2172396179_gshared (OnlineMapsXML_t3341520387 * __this, String_t* ___childName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisVector2_t2243707579_m2172396179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlElement_t2877111883 * L_1 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((XmlNode_t616554813 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, (XmlNode_t616554813 *)L_1);
		if (L_2)
		{
			goto IL_0025;
		}
	}

IL_001b:
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}

IL_0025:
	{
		XmlElement_t2877111883 * L_4 = (XmlElement_t2877111883 *)__this->get__element_1();
		String_t* L_5 = ___childName0;
		NullCheck((XmlNode_t616554813 *)L_4);
		XmlElement_t2877111883 * L_6 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_4, (String_t*)L_5);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		Vector2_t2243707579  L_7 = ((  Vector2_t2243707579  (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// T OnlineMapsXML::Get<UnityEngine.Vector2>(System.Xml.XmlElement)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern const Il2CppType* Color_t2020392075_0_0_0_var;
extern const Il2CppType* Color32_t874517518_0_0_0_var;
extern const Il2CppType* OnlineMapsRange_t3791609909_0_0_0_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var;
extern const MethodInfo* OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral372029429;
extern Il2CppCodeGenString* _stringLiteral372029432;
extern Il2CppCodeGenString* _stringLiteral339800066;
extern Il2CppCodeGenString* _stringLiteral696030436;
extern Il2CppCodeGenString* _stringLiteral1293322481;
extern Il2CppCodeGenString* _stringLiteral2609932885;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t OnlineMapsXML_Get_TisVector2_t2243707579_m3009823824_MetadataUsageId;
extern "C"  Vector2_t2243707579  OnlineMapsXML_Get_TisVector2_t2243707579_m3009823824_gshared (OnlineMapsXML_t3341520387 * __this, XmlElement_t2877111883 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnlineMapsXML_Get_TisVector2_t2243707579_m3009823824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	Type_t * V_2 = NULL;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	PropertyInfoU5BU5D_t1736152084* V_4 = NULL;
	Type_t * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	Exception_t1927440687 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t2877111883 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		XmlElement_t2877111883 * L_2 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Xml.XmlNode::get_InnerXml() */, (XmlNode_t616554813 *)L_2);
		V_1 = (String_t*)L_3;
		String_t* L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_6 = V_0;
		return L_6;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_2 = (Type_t *)L_7;
		Type_t * L_8 = V_2;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_10 = V_1;
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		return ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0054:
	{
		Type_t * L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_009c;
		}
	}
	{
		XmlElement_t2877111883 * L_15 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_15);
		XmlElement_t2877111883 * L_16 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_15, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_17 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_16, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_18 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_18);
		XmlElement_t2877111883 * L_19 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_18, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_20 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_19, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector2_t2243707579  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3067419446(&L_21, (float)L_17, (float)L_20, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_22);
		Type_t * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_25 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_23, (Type_t *)L_24, /*hidden argument*/NULL);
		return ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_009c:
	{
		Type_t * L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_00f5;
		}
	}
	{
		XmlElement_t2877111883 * L_28 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_28);
		XmlElement_t2877111883 * L_29 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_28, (String_t*)_stringLiteral372029430);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_30 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_29, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_31 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_31);
		XmlElement_t2877111883 * L_32 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_31, (String_t*)_stringLiteral372029429);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_33 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_32, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		XmlElement_t2877111883 * L_34 = ___el0;
		NullCheck((XmlNode_t616554813 *)L_34);
		XmlElement_t2877111883 * L_35 = VirtFuncInvoker1< XmlElement_t2877111883 *, String_t* >::Invoke(17 /* System.Xml.XmlElement System.Xml.XmlNode::get_Item(System.String) */, (XmlNode_t616554813 *)L_34, (String_t*)_stringLiteral372029432);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_36 = OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_35, /*hidden argument*/OnlineMapsXML_Get_TisSingle_t2076509932_m2150559399_MethodInfo_var);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, (float)L_30, (float)L_33, (float)L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_38);
		Type_t * L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_39, (Type_t *)L_40, /*hidden argument*/NULL);
		return ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_00f5:
	{
		Type_t * L_42 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color_t2020392075_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_42) == ((Il2CppObject*)(Type_t *)L_43)))
		{
			goto IL_0115;
		}
	}
	{
		Type_t * L_44 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Color32_t874517518_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_44) == ((Il2CppObject*)(Type_t *)L_45))))
		{
			goto IL_012c;
		}
	}

IL_0115:
	{
		String_t* L_46 = V_1;
		Color_t2020392075  L_47 = OnlineMapsUtils_HexToColor_m3899320166(NULL /*static, unused*/, (String_t*)L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = L_47;
		Il2CppObject * L_49 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_48);
		Type_t * L_50 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_49, (Type_t *)L_50, /*hidden argument*/NULL);
		return ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_51, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_012c:
	{
		Type_t * L_52 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(OnlineMapsRange_t3791609909_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_52) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_0163;
		}
	}
	{
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_54 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral339800066, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_55 = OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208((OnlineMapsXML_t3341520387 *)__this, (String_t*)_stringLiteral696030436, /*hidden argument*/OnlineMapsXML_Get_TisInt32_t2071877448_m2894399208_MethodInfo_var);
		OnlineMapsRange_t3791609909 * L_56 = (OnlineMapsRange_t3791609909 *)il2cpp_codegen_object_new(OnlineMapsRange_t3791609909_il2cpp_TypeInfo_var);
		OnlineMapsRange__ctor_m2214011296(L_56, (int32_t)L_54, (int32_t)L_55, /*hidden argument*/NULL);
		Type_t * L_57 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		return ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_58, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_0163:
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_59 = V_0;
		V_3 = (Vector2_t2243707579 )L_59;
		Type_t * L_60 = V_2;
		NullCheck((Type_t *)L_60);
		PropertyInfoU5BU5D_t1736152084* L_61 = Type_GetProperties_m2803026104((Type_t *)L_60, /*hidden argument*/NULL);
		V_4 = (PropertyInfoU5BU5D_t1736152084*)L_61;
		Type_t * L_62 = V_2;
		V_5 = (Type_t *)L_62;
		PropertyInfoU5BU5D_t1736152084* L_63 = V_4;
		NullCheck(L_63);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_64 = V_4;
		NullCheck(L_64);
		int32_t L_65 = 0;
		PropertyInfo_t * L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck((MemberInfo_t *)L_66);
		String_t* L_67 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_Equals_m2950069882(NULL /*static, unused*/, (String_t*)L_67, (String_t*)_stringLiteral1293322481, (int32_t)3, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfoU5BU5D_t1736152084* L_69 = V_4;
		NullCheck(L_69);
		int32_t L_70 = 1;
		PropertyInfo_t * L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck((PropertyInfo_t *)L_71);
		Type_t * L_72 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, (PropertyInfo_t *)L_71);
		V_5 = (Type_t *)L_72;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		Type_t * L_73 = V_5;
		TypeU5BU5D_t1664964607* L_74 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		NullCheck((Type_t *)L_73);
		MethodInfo_t * L_76 = Type_GetMethod_m2079823229((Type_t *)L_73, (String_t*)_stringLiteral2609932885, (TypeU5BU5D_t1664964607*)L_74, /*hidden argument*/NULL);
		V_6 = (MethodInfo_t *)L_76;
		MethodInfo_t * L_77 = V_6;
		StringU5BU5D_t1642385972* L_78 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_79 = V_1;
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_79);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_79);
		NullCheck((MethodBase_t904190842 *)L_77);
		Il2CppObject * L_80 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_77, (Il2CppObject *)NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)L_78, /*hidden argument*/NULL);
		V_3 = (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_80, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		goto IL_0205;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01e4;
		throw e;
	}

CATCH_01e4:
	{ // begin catch(System.Exception)
		V_7 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_81 = V_7;
		NullCheck((Exception_t1927440687 *)L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_81);
		Exception_t1927440687 * L_83 = V_7;
		NullCheck((Exception_t1927440687 *)L_83);
		String_t* L_84 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, (Exception_t1927440687 *)L_83);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_82, (String_t*)_stringLiteral372029352, (String_t*)L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0205:
	{
		Vector2_t2243707579  L_86 = V_3;
		return L_86;
	}
}
// T OnlineMapsXML::Value<System.Int32>()
extern "C"  int32_t OnlineMapsXML_Value_TisInt32_t2071877448_m2298504637_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method)
{
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		int32_t L_1 = ((  int32_t (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T OnlineMapsXML::Value<System.Object>()
extern "C"  Il2CppObject * OnlineMapsXML_Value_TisIl2CppObject_m33615830_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method)
{
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T OnlineMapsXML::Value<System.Single>()
extern "C"  float OnlineMapsXML_Value_TisSingle_t2076509932_m1578684385_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method)
{
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		float L_1 = ((  float (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T OnlineMapsXML::Value<UnityEngine.Vector2>()
extern "C"  Vector2_t2243707579  OnlineMapsXML_Value_TisVector2_t2243707579_m2370972126_gshared (OnlineMapsXML_t3341520387 * __this, const MethodInfo* method)
{
	{
		XmlElement_t2877111883 * L_0 = (XmlElement_t2877111883 *)__this->get__element_1();
		NullCheck((OnlineMapsXML_t3341520387 *)__this);
		Vector2_t2243707579  L_1 = ((  Vector2_t2243707579  (*) (OnlineMapsXML_t3341520387 *, XmlElement_t2877111883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((OnlineMapsXML_t3341520387 *)__this, (XmlElement_t2877111883 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T PlayerPrefSaveData::GetSavedObject<System.Object>(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1216969609;
extern const uint32_t PlayerPrefSaveData_GetSavedObject_TisIl2CppObject_m1035035119_MetadataUsageId;
extern "C"  Il2CppObject * PlayerPrefSaveData_GetSavedObject_TisIl2CppObject_m1035035119_gshared (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefSaveData_GetSavedObject_TisIl2CppObject_m1035035119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	BinaryFormatter_t1866979105 * V_1 = NULL;
	MemoryStream_t743994179 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		V_0 = (String_t*)L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0059;
		}
	}
	{
		BinaryFormatter_t1866979105 * L_4 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_4, /*hidden argument*/NULL);
		V_1 = (BinaryFormatter_t1866979105 *)L_4;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_6 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, (String_t*)L_5, /*hidden argument*/NULL);
		MemoryStream_t743994179 * L_7 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_7, (ByteU5BU5D_t3397334013*)L_6, /*hidden argument*/NULL);
		V_2 = (MemoryStream_t743994179 *)L_7;
		BinaryFormatter_t1866979105 * L_8 = V_1;
		MemoryStream_t743994179 * L_9 = V_2;
		NullCheck((BinaryFormatter_t1866979105 *)L_8);
		Il2CppObject * L_10 = BinaryFormatter_Deserialize_m2771853471((BinaryFormatter_t1866979105 *)L_8, (Stream_t3255436806 *)L_9, /*hidden argument*/NULL);
		V_3 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_10, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject * L_11 = V_3;
		V_4 = (Il2CppObject *)L_11;
		goto IL_0074;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003e;
		throw e;
	}

CATCH_003e:
	{ // begin catch(System.Exception)
		V_5 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_12 = V_5;
		NullCheck((Exception_t1927440687 *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		V_4 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0074;
	} // end catch (depth: 1)

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_14 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1216969609, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0074:
	{
		Il2CppObject * L_15 = V_4;
		return L_15;
	}
}
// T Prime31.Json::decode<System.Object>(System.String,System.String)
extern Il2CppClass* Json_t1852637766_il2cpp_TypeInfo_var;
extern const uint32_t Json_decode_TisIl2CppObject_m2605020229_MetadataUsageId;
extern "C"  Il2CppObject * Json_decode_TisIl2CppObject_m2605020229_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, String_t* ___rootElement1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Json_decode_TisIl2CppObject_m2605020229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Json_t1852637766_il2cpp_TypeInfo_var);
		bool L_0 = ((Json_t1852637766_StaticFields*)Json_t1852637766_il2cpp_TypeInfo_var->static_fields)->get_useSimpleJson_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___json0;
		String_t* L_2 = ___rootElement1;
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (String_t*)L_1, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}

IL_0012:
	{
		String_t* L_4 = ___json0;
		String_t* L_5 = ___rootElement1;
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (String_t*)L_4, (String_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return ((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 2)));
	}
}
// T Prime31.Json::decodeObject<System.Object>(System.Object,System.String)
extern "C"  Il2CppObject * Json_decodeObject_TisIl2CppObject_m4254816916_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonObject0, String_t* ___rootElement1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___jsonObject0;
		String_t* L_1 = ___rootElement1;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T Prime31.SimpleJson::decode<System.Object>(System.String,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_decode_TisIl2CppObject_m2631072897_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_decode_TisIl2CppObject_m2631072897_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, String_t* ___rootElement1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_decode_TisIl2CppObject_m2631072897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_2 = ___rootElement1;
		Il2CppObject * L_3 = SimpleJson_decode_m2822751612(NULL /*static, unused*/, (String_t*)L_0, (Type_t *)L_1, (String_t*)L_2, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T Prime31.SimpleJson::decodeObject<System.Object>(System.Object,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IJsonSerializerStrategy_t1541341984_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral19161393;
extern Il2CppCodeGenString* _stringLiteral4126974361;
extern const uint32_t SimpleJson_decodeObject_TisIl2CppObject_m380181316_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_decodeObject_TisIl2CppObject_m380181316_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonObject0, String_t* ___rootElement1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_decodeObject_TisIl2CppObject_m380181316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Dictionary_2_t309261261 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Type_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_2 = ___jsonObject0;
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_3 = ___jsonObject0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)L_3, /*hidden argument*/NULL);
		Type_t * L_5 = V_0;
		NullCheck((Type_t *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Type_t * >::Invoke(45 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_4, (Type_t *)L_5);
		if (!L_6)
		{
			goto IL_002f;
		}
	}

IL_0028:
	{
		Il2CppObject * L_7 = ___jsonObject0;
		return ((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_002f:
	{
		String_t* L_8 = ___rootElement1;
		if (!L_8)
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_9 = ___jsonObject0;
		if (!((Dictionary_2_t309261261 *)IsInst(L_9, Dictionary_2_t309261261_il2cpp_TypeInfo_var)))
		{
			goto IL_0076;
		}
	}
	{
		Il2CppObject * L_10 = ___jsonObject0;
		V_1 = (Dictionary_2_t309261261 *)((Dictionary_2_t309261261 *)IsInst(L_10, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_11 = V_1;
		String_t* L_12 = ___rootElement1;
		NullCheck((Dictionary_2_t309261261 *)L_11);
		bool L_13 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_11, (String_t*)L_12, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_13)
		{
			goto IL_0061;
		}
	}
	{
		Dictionary_2_t309261261 * L_14 = V_1;
		String_t* L_15 = ___rootElement1;
		NullCheck((Dictionary_2_t309261261 *)L_14);
		Il2CppObject * L_16 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_14, (String_t*)L_15, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		___jsonObject0 = (Il2CppObject *)L_16;
		goto IL_0071;
	}

IL_0061:
	{
		String_t* L_17 = ___rootElement1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m2024975688(NULL /*static, unused*/, (String_t*)_stringLiteral19161393, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		Utils_logObject_m1779708428(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_0071:
	{
		goto IL_008c;
	}

IL_0076:
	{
		String_t* L_19 = ___rootElement1;
		Il2CppObject * L_20 = ___jsonObject0;
		NullCheck((Il2CppObject *)L_20);
		Type_t * L_21 = Object_GetType_m191970594((Il2CppObject *)L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m1811873526(NULL /*static, unused*/, (String_t*)_stringLiteral4126974361, (Il2CppObject *)L_19, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		Utils_logObject_m1779708428(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
	}

IL_008c:
	{
		Il2CppObject * L_23 = SimpleJson_get_currentJsonSerializerStrategy_m2549509207(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_24 = ___jsonObject0;
		Type_t * L_25 = V_0;
		NullCheck((Il2CppObject *)L_23);
		Il2CppObject * L_26 = InterfaceFuncInvoker2< Il2CppObject *, Il2CppObject *, Type_t * >::Invoke(1 /* System.Object Prime31.IJsonSerializerStrategy::deserializeObject(System.Object,System.Type) */, IJsonSerializerStrategy_t1541341984_il2cpp_TypeInfo_var, (Il2CppObject *)L_23, (Il2CppObject *)L_24, (Type_t *)L_25);
		return ((Il2CppObject *)Castclass(L_26, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T System.Activator::CreateInstance<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Activator_CreateInstance_TisIl2CppObject_m580915191_MetadataUsageId;
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m580915191_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Activator_CreateInstance_TisIl2CppObject_m580915191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_1 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t Array_Find_TisIl2CppObject_m1654841559_MetadataUsageId;
extern "C"  Il2CppObject * Array_Find_TisIl2CppObject_m1654841559_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Predicate_1_t1132419410 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_Find_TisIl2CppObject_m1654841559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t1132419410 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		V_1 = (ObjectU5BU5D_t3614634134*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t3614634134* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = (Il2CppObject *)L_8;
		Predicate_1_t1132419410 * L_9 = ___match1;
		Il2CppObject * L_10 = V_0;
		NullCheck((Predicate_1_t1132419410 *)L_9);
		bool L_11 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Predicate_1_t1132419410 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_11)
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_12 = V_0;
		return L_12;
	}

IL_0041:
	{
		int32_t L_13 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_14 = V_2;
		ObjectU5BU5D_t3614634134* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_16 = V_3;
		return L_16;
	}
}
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t Array_FindLast_TisIl2CppObject_m1794562749_MetadataUsageId;
extern "C"  Il2CppObject * Array_FindLast_TisIl2CppObject_m1794562749_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Predicate_1_t1132419410 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_FindLast_TisIl2CppObject_m1794562749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t1132419410 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)1));
		goto IL_004b;
	}

IL_002d:
	{
		Predicate_1_t1132419410 * L_5 = ___match1;
		ObjectU5BU5D_t3614634134* L_6 = ___array0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck((Predicate_1_t1132419410 *)L_5);
		bool L_10 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Predicate_1_t1132419410 *)L_5, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_10)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_11 = ___array0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		return L_14;
	}

IL_0047:
	{
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15-(int32_t)1));
	}

IL_004b:
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_17 = V_1;
		return L_17;
	}
}
// T System.Array::InternalArray__get_Item<AttackHomeNav>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAttackHomeNav_t3944890504_m963999648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<AttackNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAttackNavScreen_t36759459_m3481746225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<AttackSearchNav>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAttackSearchNav_t4257884637_m783162855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<BestHTTP.SignalR.Messages.ClientMessage>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_MetadataUsageId;
extern "C"  ClientMessage_t624279968  Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisClientMessage_t624279968_m2227391756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ClientMessage_t624279968  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ClientMessage_t624279968 *)(&V_0));
		ClientMessage_t624279968  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ChatNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisChatNavScreen_t42377261_m1669121527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<DefendNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDefendNavScreen_t3838619703_m3272711021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<FriendHomeNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFriendHomeNavScreen_t1599038296_m1939116176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<FriendNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFriendNavScreen_t2175383453_m3642060815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<FriendSearchNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFriendSearchNavScreen_t3018389163_m923068673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<LeaderboardNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLeaderboardNavScreen_t2356043712_m425675556_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisLeaderboardNavScreen_t2356043712_m425675556_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLeaderboardNavScreen_t2356043712_m425675556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<LitJson.ArrayMetadata>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_MetadataUsageId;
extern "C"  ArrayMetadata_t2008834462  Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayMetadata_t2008834462  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ArrayMetadata_t2008834462 *)(&V_0));
		ArrayMetadata_t2008834462  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<LitJson.ObjectMetadata>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_MetadataUsageId;
extern "C"  ObjectMetadata_t3995922398  Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectMetadata_t3995922398  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ObjectMetadata_t3995922398 *)(&V_0));
		ObjectMetadata_t3995922398  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<LitJson.PropertyMetadata>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_MetadataUsageId;
extern "C"  PropertyMetadata_t3693826136  Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PropertyMetadata_t3693826136  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (PropertyMetadata_t3693826136 *)(&V_0));
		PropertyMetadata_t3693826136  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_MetadataUsageId;
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TableRange_t2011406615  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TableRange_t2011406615 *)(&V_0));
		TableRange_t2011406615  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Security.Uri/UriScheme>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_MetadataUsageId;
extern "C"  UriScheme_t683497865  Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UriScheme_t683497865  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t683497865 *)(&V_0));
		UriScheme_t683497865  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_MetadataUsageId;
extern "C"  TagName_t2340974457  Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TagName_t2340974457  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TagName_t2340974457 *)(&V_0));
		TagName_t2340974457  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<OnlineMapsEvents>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisOnlineMapsEvents_t850667009_m511107595_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisOnlineMapsEvents_t850667009_m511107595_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisOnlineMapsEvents_t850667009_m511107595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<OnlineMapsJPEGDecoder/Code>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisCode_t3472511518_m2961437966_MetadataUsageId;
extern "C"  Code_t3472511518  Array_InternalArray__get_Item_TisCode_t3472511518_m2961437966_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCode_t3472511518_m2961437966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Code_t3472511518  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Code_t3472511518 *)(&V_0));
		Code_t3472511518  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<PanelType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPanelType_t482769230_m2025358834_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisPanelType_t482769230_m2025358834_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPanelType_t482769230_m2025358834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<PhoneNumberNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPhoneNumberNavScreen_t1263467250_m1726775518_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisPhoneNumberNavScreen_t1263467250_m1726775518_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPhoneNumberNavScreen_t1263467250_m1726775518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<PlayerProfileNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPlayerProfileNavScreen_t2660668259_m4053680973_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisPlayerProfileNavScreen_t2660668259_m4053680973_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPlayerProfileNavScreen_t2660668259_m4053680973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Prime31.JsonFormatter/JsonContextType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisJsonContextType_t3787516849_m136453899_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisJsonContextType_t3787516849_m136453899_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisJsonContextType_t3787516849_m136453899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ProfileNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisProfileNavScreen_t1126657854_m2676274002_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisProfileNavScreen_t1126657854_m2676274002_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisProfileNavScreen_t1126657854_m2676274002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<RegNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRegNavScreen_t667173359_m2994260105_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisRegNavScreen_t667173359_m2994260105_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRegNavScreen_t667173359_m2994260105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<SettingsNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisSettingsNavScreen_t2267127234_m1689028766_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisSettingsNavScreen_t2267127234_m1689028766_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSettingsNavScreen_t2267127234_m1689028766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<StatusNavScreen>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisStatusNavScreen_t2872369083_m2205417297_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisStatusNavScreen_t2872369083_m2205417297_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisStatusNavScreen_t2872369083_m2205417297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_MetadataUsageId;
extern "C"  ArraySegment_1_t2594217482  Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArraySegment_1_t2594217482  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ArraySegment_1_t2594217482 *)(&V_0));
		ArraySegment_1_t2594217482  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_MetadataUsageId;
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (bool*)(&V_0));
		bool L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_MetadataUsageId;
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_MetadataUsageId;
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Il2CppChar*)(&V_0));
		Il2CppChar L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryEntry_t3048875398  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DictionaryEntry_t3048875398 *)(&V_0));
		DictionaryEntry_t3048875398  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Int32>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_MetadataUsageId;
extern "C"  Link_t247561424  Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Link_t247561424  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t247561424 *)(&V_0));
		Link_t247561424  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_MetadataUsageId;
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Link_t865133271  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t865133271 *)(&V_0));
		Link_t865133271  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1603735834_m1792859529_MetadataUsageId;
extern "C"  KeyValuePair_2_t1603735834  Array_InternalArray__get_Item_TisKeyValuePair_2_t1603735834_m1792859529_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1603735834_m1792859529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1603735834  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1603735834 *)(&V_0));
		KeyValuePair_2_t1603735834  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2812303849_m1649835165_MetadataUsageId;
extern "C"  KeyValuePair_2_t2812303849  Array_InternalArray__get_Item_TisKeyValuePair_2_t2812303849_m1649835165_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2812303849_m1649835165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2812303849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2812303849 *)(&V_0));
		KeyValuePair_2_t2812303849  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_MetadataUsageId;
extern "C"  KeyValuePair_2_t3132015601  Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3132015601 *)(&V_0));
		KeyValuePair_2_t3132015601  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_MetadataUsageId;
extern "C"  KeyValuePair_2_t3749587448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3749587448 *)(&V_0));
		KeyValuePair_2_t3749587448  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_MetadataUsageId;
extern "C"  KeyValuePair_2_t1008373517  Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1008373517  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1008373517 *)(&V_0));
		KeyValuePair_2_t1008373517  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_MetadataUsageId;
extern "C"  KeyValuePair_2_t3653207108  Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3653207108  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3653207108 *)(&V_0));
		KeyValuePair_2_t3653207108  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_MetadataUsageId;
extern "C"  KeyValuePair_2_t1345327748  Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1345327748  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1345327748 *)(&V_0));
		KeyValuePair_2_t1345327748  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_MetadataUsageId;
extern "C"  KeyValuePair_2_t1043231486  Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1043231486  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1043231486 *)(&V_0));
		KeyValuePair_2_t1043231486  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_MetadataUsageId;
extern "C"  KeyValuePair_2_t1174980068  Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1174980068 *)(&V_0));
		KeyValuePair_2_t1174980068  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2369073723_m2312448055_MetadataUsageId;
extern "C"  KeyValuePair_2_t2369073723  Array_InternalArray__get_Item_TisKeyValuePair_2_t2369073723_m2312448055_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2369073723_m2312448055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2369073723  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2369073723 *)(&V_0));
		KeyValuePair_2_t2369073723  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_MetadataUsageId;
extern "C"  KeyValuePair_2_t3716250094  Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3716250094 *)(&V_0));
		KeyValuePair_2_t3716250094  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_MetadataUsageId;
extern "C"  KeyValuePair_2_t38854645  Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t38854645 *)(&V_0));
		KeyValuePair_2_t38854645  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_MetadataUsageId;
extern "C"  KeyValuePair_2_t488203048  Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t488203048  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t488203048 *)(&V_0));
		KeyValuePair_2_t488203048  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3888080226_m642757410_MetadataUsageId;
extern "C"  KeyValuePair_2_t3888080226  Array_InternalArray__get_Item_TisKeyValuePair_2_t3888080226_m642757410_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3888080226_m642757410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3888080226  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3888080226 *)(&V_0));
		KeyValuePair_2_t3888080226  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.UInt64,BestHTTP.SignalR.Messages.ClientMessage>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t322707255_m2266875646_MetadataUsageId;
extern "C"  KeyValuePair_2_t322707255  Array_InternalArray__get_Item_TisKeyValuePair_2_t322707255_m2266875646_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t322707255_m2266875646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t322707255  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t322707255 *)(&V_0));
		KeyValuePair_2_t322707255  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2387876582_m4073223712_MetadataUsageId;
extern "C"  KeyValuePair_2_t2387876582  Array_InternalArray__get_Item_TisKeyValuePair_2_t2387876582_m4073223712_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2387876582_m4073223712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2387876582  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2387876582 *)(&V_0));
		KeyValuePair_2_t2387876582  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2335466038_m3011847674_MetadataUsageId;
extern "C"  KeyValuePair_2_t2335466038  Array_InternalArray__get_Item_TisKeyValuePair_2_t2335466038_m3011847674_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2335466038_m3011847674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2335466038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2335466038 *)(&V_0));
		KeyValuePair_2_t2335466038  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1020750381_m861546038_MetadataUsageId;
extern "C"  KeyValuePair_2_t1020750381  Array_InternalArray__get_Item_TisKeyValuePair_2_t1020750381_m861546038_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1020750381_m861546038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1020750381  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1020750381 *)(&V_0));
		KeyValuePair_2_t1020750381  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_MetadataUsageId;
extern "C"  Link_t2723257478  Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Link_t2723257478  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t2723257478 *)(&V_0));
		Link_t2723257478  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_MetadataUsageId;
extern "C"  Slot_t2022531261  Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Slot_t2022531261  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Slot_t2022531261 *)(&V_0));
		Slot_t2022531261  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_MetadataUsageId;
extern "C"  Slot_t2267560602  Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Slot_t2267560602  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Slot_t2267560602 *)(&V_0));
		Slot_t2267560602  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.ComponentModel.PropertyTabScope>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPropertyTabScope_t2485003348_m1882471435_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisPropertyTabScope_t2485003348_m1882471435_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPropertyTabScope_t2485003348_m1882471435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_MetadataUsageId;
extern "C"  DateTime_t693205669  Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DateTime_t693205669 *)(&V_0));
		DateTime_t693205669  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_MetadataUsageId;
extern "C"  Decimal_t724701077  Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Decimal_t724701077  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Decimal_t724701077 *)(&V_0));
		Decimal_t724701077  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_MetadataUsageId;
extern "C"  double Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (double*)(&V_0));
		double L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_MetadataUsageId;
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int16_t*)(&V_0));
		int16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_MetadataUsageId;
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int64_t*)(&V_0));
		int64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisIntPtr_t_m210946760_MetadataUsageId;
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIntPtr_t_m210946760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (IntPtr_t*)(&V_0));
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisIl2CppObject_m371871810_MetadataUsageId;
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIl2CppObject_m371871810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Il2CppObject **)(&V_0));
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeNamedArgument_t94157543  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeNamedArgument_t94157543 *)(&V_0));
		CustomAttributeNamedArgument_t94157543  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeTypedArgument_t1498197914  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeTypedArgument_t1498197914 *)(&V_0));
		CustomAttributeTypedArgument_t1498197914  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_MetadataUsageId;
extern "C"  LabelData_t3712112744  Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LabelData_t3712112744  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelData_t3712112744 *)(&V_0));
		LabelData_t3712112744  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_MetadataUsageId;
extern "C"  LabelFixup_t4090909514  Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LabelFixup_t4090909514  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelFixup_t4090909514 *)(&V_0));
		LabelFixup_t4090909514  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_MetadataUsageId;
extern "C"  ILTokenInfo_t149559338  Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ILTokenInfo_t149559338  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ILTokenInfo_t149559338 *)(&V_0));
		ILTokenInfo_t149559338  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisLabel_t4243202660_m2000046803_MetadataUsageId;
extern "C"  Label_t4243202660  Array_InternalArray__get_Item_TisLabel_t4243202660_m2000046803_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabel_t4243202660_m2000046803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Label_t4243202660  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Label_t4243202660 *)(&V_0));
		Label_t4243202660  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisMonoResource_t3127387157_m310178846_MetadataUsageId;
extern "C"  MonoResource_t3127387157  Array_InternalArray__get_Item_TisMonoResource_t3127387157_m310178846_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMonoResource_t3127387157_m310178846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MonoResource_t3127387157  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (MonoResource_t3127387157 *)(&V_0));
		MonoResource_t3127387157  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRefEmitPermissionSet_t2708608433_m4227940714_MetadataUsageId;
extern "C"  RefEmitPermissionSet_t2708608433  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t2708608433_m4227940714_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRefEmitPermissionSet_t2708608433_m4227940714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RefEmitPermissionSet_t2708608433  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RefEmitPermissionSet_t2708608433 *)(&V_0));
		RefEmitPermissionSet_t2708608433  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_MetadataUsageId;
extern "C"  ParameterModifier_t1820634920  Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParameterModifier_t1820634920  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ParameterModifier_t1820634920 *)(&V_0));
		ParameterModifier_t1820634920  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_MetadataUsageId;
extern "C"  ResourceCacheItem_t333236149  Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResourceCacheItem_t333236149  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceCacheItem_t333236149 *)(&V_0));
		ResourceCacheItem_t333236149  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_MetadataUsageId;
extern "C"  ResourceInfo_t3933049236  Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResourceInfo_t3933049236  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceInfo_t3933049236 *)(&V_0));
		ResourceInfo_t3933049236  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_MetadataUsageId;
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_MetadataUsageId;
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_MetadataUsageId;
extern "C"  X509ChainStatus_t4278378721  Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509ChainStatus_t4278378721  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (X509ChainStatus_t4278378721 *)(&V_0));
		X509ChainStatus_t4278378721  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_MetadataUsageId;
extern "C"  float Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (float*)(&V_0));
		float L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTermInfoStrings_t1425267120_m3923903537_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t1425267120_m3923903537_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTermInfoStrings_t1425267120_m3923903537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_MetadataUsageId;
extern "C"  Mark_t2724874473  Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mark_t2724874473  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Mark_t2724874473 *)(&V_0));
		Mark_t2724874473  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t3430258949  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeSpan_t3430258949 *)(&V_0));
		TimeSpan_t3430258949  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_MetadataUsageId;
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_MetadataUsageId;
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint32_t*)(&V_0));
		uint32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_MetadataUsageId;
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint64_t*)(&V_0));
		uint64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_MetadataUsageId;
extern "C"  UriScheme_t1876590943  Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UriScheme_t1876590943  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t1876590943 *)(&V_0));
		UriScheme_t1876590943  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_MetadataUsageId;
extern "C"  NsDecl_t3210081295  Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsDecl_t3210081295_m2296051909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NsDecl_t3210081295  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsDecl_t3210081295 *)(&V_0));
		NsDecl_t3210081295  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_MetadataUsageId;
extern "C"  NsScope_t2513625351  Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsScope_t2513625351_m2981250529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NsScope_t2513625351  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsScope_t2513625351 *)(&V_0));
		NsScope_t2513625351  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XPath.XPathResultType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisXPathResultType_t1521569578_m3711281163_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisXPathResultType_t1521569578_m3711281163_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisXPathResultType_t1521569578_m3711281163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisFadeEntry_t3041229383_m946129697_MetadataUsageId;
extern "C"  FadeEntry_t3041229383  Array_InternalArray__get_Item_TisFadeEntry_t3041229383_m946129697_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFadeEntry_t3041229383_m946129697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FadeEntry_t3041229383  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (FadeEntry_t3041229383 *)(&V_0));
		FadeEntry_t3041229383  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisDepthEntry_t974746545_m2114350883_MetadataUsageId;
extern "C"  DepthEntry_t974746545  Array_InternalArray__get_Item_TisDepthEntry_t974746545_m2114350883_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDepthEntry_t974746545_m2114350883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DepthEntry_t974746545  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DepthEntry_t974746545 *)(&V_0));
		DepthEntry_t974746545  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Unibill.Impl.BillingPlatform>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisBillingPlatform_t552059234_m1273653209_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisBillingPlatform_t552059234_m1273653209_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBillingPlatform_t552059234_m1273653209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnibillError>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUnibillError_t1753859787_m702214397_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisUnibillError_t1753859787_m702214397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUnibillError_t1753859787_m702214397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisBounds_t3033363703_m816654186_MetadataUsageId;
extern "C"  Bounds_t3033363703  Array_InternalArray__get_Item_TisBounds_t3033363703_m816654186_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBounds_t3033363703_m816654186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Bounds_t3033363703 *)(&V_0));
		Bounds_t3033363703  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_MetadataUsageId;
extern "C"  Color_t2020392075  Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color_t2020392075 *)(&V_0));
		Color_t2020392075  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_MetadataUsageId;
extern "C"  Color32_t874517518  Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t874517518  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color32_t874517518 *)(&V_0));
		Color32_t874517518  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_MetadataUsageId;
extern "C"  ContactPoint_t1376425630  Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactPoint_t1376425630  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint_t1376425630 *)(&V_0));
		ContactPoint_t1376425630  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_MetadataUsageId;
extern "C"  ContactPoint2D_t3659330976  Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactPoint2D_t3659330976  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint2D_t3659330976 *)(&V_0));
		ContactPoint2D_t3659330976  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_MetadataUsageId;
extern "C"  RaycastResult_t21186376  Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t21186376  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastResult_t21186376 *)(&V_0));
		RaycastResult_t21186376  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Experimental.Director.Playable>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_MetadataUsageId;
extern "C"  Playable_t3667545548  Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t3667545548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Playable_t3667545548 *)(&V_0));
		Playable_t3667545548  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyCode_t2283395152_m3267050797_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisKeyCode_t2283395152_m3267050797_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyCode_t2283395152_m3267050797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_MetadataUsageId;
extern "C"  Keyframe_t1449471340  Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Keyframe_t1449471340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Keyframe_t1449471340 *)(&V_0));
		Keyframe_t1449471340  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Plane>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisPlane_t3727654732_m3853269401_MetadataUsageId;
extern "C"  Plane_t3727654732  Array_InternalArray__get_Item_TisPlane_t3727654732_m3853269401_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPlane_t3727654732_m3853269401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Plane_t3727654732  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Plane_t3727654732 *)(&V_0));
		Plane_t3727654732  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_MetadataUsageId;
extern "C"  RaycastHit_t87180320  Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t87180320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit_t87180320 *)(&V_0));
		RaycastHit_t87180320  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t4063908774  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit2D_t4063908774 *)(&V_0));
		RaycastHit2D_t4063908774  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Rect>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRect_t3681755626_m1107043059_MetadataUsageId;
extern "C"  Rect_t3681755626  Array_InternalArray__get_Item_TisRect_t3681755626_m1107043059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRect_t3681755626_m1107043059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Rect_t3681755626 *)(&V_0));
		Rect_t3681755626  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RuntimePlatform>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_MetadataUsageId;
extern "C"  HitInfo_t1761367055  Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t1761367055  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (HitInfo_t1761367055 *)(&V_0));
		HitInfo_t1761367055  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_MetadataUsageId;
extern "C"  GcAchievementData_t1754866149  Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GcAchievementData_t1754866149  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcAchievementData_t1754866149 *)(&V_0));
		GcAchievementData_t1754866149  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_MetadataUsageId;
extern "C"  GcScoreData_t3676783238  Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GcScoreData_t3676783238  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcScoreData_t3676783238 *)(&V_0));
		GcScoreData_t3676783238  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTextEditOp_t3138797698_m2832701950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Touch>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_MetadataUsageId;
extern "C"  Touch_t407273883  Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Touch_t407273883 *)(&V_0));
		Touch_t407273883  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_MetadataUsageId;
extern "C"  UICharInfo_t3056636800  Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UICharInfo_t3056636800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UICharInfo_t3056636800 *)(&V_0));
		UICharInfo_t3056636800  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_MetadataUsageId;
extern "C"  UILineInfo_t3621277874  Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UILineInfo_t3621277874  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UILineInfo_t3621277874 *)(&V_0));
		UILineInfo_t3621277874  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_MetadataUsageId;
extern "C"  UIVertex_t1204258818  Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIVertex_t1204258818  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UIVertex_t1204258818 *)(&V_0));
		UIVertex_t1204258818  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_MetadataUsageId;
extern "C"  Vector2_t2243707579  Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector2_t2243707579 *)(&V_0));
		Vector2_t2243707579  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_MetadataUsageId;
extern "C"  Vector3_t2243707580  Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3_t2243707580 *)(&V_0));
		Vector3_t2243707580  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_MetadataUsageId;
extern "C"  Vector4_t2243707581  Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector4_t2243707581 *)(&V_0));
		Vector4_t2243707581  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vectrosity.Vector3Pair>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Array_InternalArray__get_Item_TisVector3Pair_t2859078138_m2447468668_MetadataUsageId;
extern "C"  Vector3Pair_t2859078138  Array_InternalArray__get_Item_TisVector3Pair_t2859078138_m2447468668_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3Pair_t2859078138_m2447468668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3Pair_t2859078138  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1498215565((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3Pair_t2859078138 *)(&V_0));
		Vector3Pair_t2859078138  L_4 = V_0;
		return L_4;
	}
}
// T UITweener::Begin<System.Object>(UnityEngine.GameObject,System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3185208195;
extern Il2CppCodeGenString* _stringLiteral271646399;
extern const uint32_t UITweener_Begin_TisIl2CppObject_m3878506588_MetadataUsageId;
extern "C"  Il2CppObject * UITweener_Begin_TisIl2CppObject_m3878506588_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, float ___duration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Begin_TisIl2CppObject_m3878506588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0081;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)((UITweener_t2986641582 *)L_4)->get_tweenGroup_10();
		if (!L_5)
		{
			goto IL_0081;
		}
	}
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		GameObject_t1756533147 * L_6 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1756533147 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_1 = (ObjectU5BU5D_t3614634134*)L_7;
		V_2 = (int32_t)0;
		ObjectU5BU5D_t3614634134* L_8 = V_1;
		NullCheck(L_8);
		V_3 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))));
		goto IL_007a;
	}

IL_0041:
	{
		ObjectU5BU5D_t3614634134* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_0 = (Il2CppObject *)L_12;
		Il2CppObject * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006f;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)((UITweener_t2986641582 *)L_15)->get_tweenGroup_10();
		if (L_16)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0081;
	}

IL_006f:
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		int32_t L_17 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_18 = V_2;
		int32_t L_19 = V_3;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0041;
		}
	}

IL_0081:
	{
		Il2CppObject * L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_20, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00e8;
		}
	}
	{
		GameObject_t1756533147 * L_22 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_22);
		Il2CppObject * L_23 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((GameObject_t1756533147 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		V_0 = (Il2CppObject *)L_23;
		Il2CppObject * L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_24, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral3185208195);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3185208195);
		ObjectU5BU5D_t3614634134* L_27 = (ObjectU5BU5D_t3614634134*)L_26;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_28);
		ObjectU5BU5D_t3614634134* L_29 = (ObjectU5BU5D_t3614634134*)L_27;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral271646399);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral271646399);
		ObjectU5BU5D_t3614634134* L_30 = (ObjectU5BU5D_t3614634134*)L_29;
		GameObject_t1756533147 * L_31 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		String_t* L_32 = NGUITools_GetHierarchy_m743996189(NULL /*static, unused*/, (GameObject_t1756533147 *)L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m865553560(NULL /*static, unused*/, (Il2CppObject *)L_33, (Object_t1021602117 *)L_34, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00e8:
	{
		Il2CppObject * L_35 = V_0;
		NullCheck(L_35);
		((UITweener_t2986641582 *)L_35)->set_mStarted_14((bool)0);
		Il2CppObject * L_36 = V_0;
		float L_37 = ___duration1;
		NullCheck(L_36);
		((UITweener_t2986641582 *)L_36)->set_duration_8(L_37);
		Il2CppObject * L_38 = V_0;
		NullCheck(L_38);
		((UITweener_t2986641582 *)L_38)->set_mFactor_18((0.0f));
		Il2CppObject * L_39 = V_0;
		NullCheck((UITweener_t2986641582 *)(*(&V_0)));
		float L_40 = UITweener_get_amountPerDelta_m3796492245((UITweener_t2986641582 *)(*(&V_0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_41 = fabsf((float)L_40);
		NullCheck(L_39);
		((UITweener_t2986641582 *)L_39)->set_mAmountPerDelta_17(L_41);
		Il2CppObject * L_42 = V_0;
		NullCheck(L_42);
		((UITweener_t2986641582 *)L_42)->set_style_4(0);
		Il2CppObject * L_43 = V_0;
		KeyframeU5BU5D_t449065829* L_44 = (KeyframeU5BU5D_t449065829*)((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_44);
		Keyframe_t1449471340  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Keyframe__ctor_m140082843(&L_45, (float)(0.0f), (float)(0.0f), (float)(0.0f), (float)(1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_45;
		KeyframeU5BU5D_t449065829* L_46 = (KeyframeU5BU5D_t449065829*)L_44;
		NullCheck(L_46);
		Keyframe_t1449471340  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Keyframe__ctor_m140082843(&L_47, (float)(1.0f), (float)(1.0f), (float)(1.0f), (float)(0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_47;
		AnimationCurve_t3306541151 * L_48 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_48, (KeyframeU5BU5D_t449065829*)L_46, /*hidden argument*/NULL);
		NullCheck(L_43);
		((UITweener_t2986641582 *)L_43)->set_animationCurve_5(L_48);
		Il2CppObject * L_49 = V_0;
		NullCheck(L_49);
		((UITweener_t2986641582 *)L_49)->set_eventReceiver_12((GameObject_t1756533147 *)NULL);
		Il2CppObject * L_50 = V_0;
		NullCheck(L_50);
		((UITweener_t2986641582 *)L_50)->set_callWhenFinished_13((String_t*)NULL);
		NullCheck((Behaviour_t955675639 *)(*(&V_0)));
		Behaviour_set_enabled_m1796096907((Behaviour_t955675639 *)(*(&V_0)), (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_51 = V_0;
		return L_51;
	}
}
// T Unibill.Impl.MiniJsonExtensions::get<System.Object>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern const uint32_t MiniJsonExtensions_get_TisIl2CppObject_m1169879225_MetadataUsageId;
extern "C"  Il2CppObject * MiniJsonExtensions_get_TisIl2CppObject_m1169879225_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_get_TisIl2CppObject_m1169879225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_0, (String_t*)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t309261261 * L_3 = ___dic0;
		String_t* L_4 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_3);
		Il2CppObject * L_5 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_3, (String_t*)L_4, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0019:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T Unibill.Impl.MiniJsonExtensions::getEnum<PurchaseType>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseType_t1639241305_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern const uint32_t MiniJsonExtensions_getEnum_TisPurchaseType_t1639241305_m1386270067_MetadataUsageId;
extern "C"  int32_t MiniJsonExtensions_getEnum_TisPurchaseType_t1639241305_m1386270067_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_getEnum_TisPurchaseType_t1639241305_m1386270067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_0, (String_t*)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_4 = ___dic0;
		String_t* L_5 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_4);
		Il2CppObject * L_6 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_4, (String_t*)L_5, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		NullCheck((Il2CppObject *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = Enum_Parse_m982704874(NULL /*static, unused*/, (Type_t *)L_3, (String_t*)L_7, (bool)1, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_002e:
	{
		Initobj (PurchaseType_t1639241305_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_9 = V_0;
		return L_9;
	}
}
// T Unibill.Impl.MiniJsonExtensions::getEnum<System.Object>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern const uint32_t MiniJsonExtensions_getEnum_TisIl2CppObject_m1497042404_MetadataUsageId;
extern "C"  Il2CppObject * MiniJsonExtensions_getEnum_TisIl2CppObject_m1497042404_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_getEnum_TisIl2CppObject_m1497042404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_0, (String_t*)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_4 = ___dic0;
		String_t* L_5 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_4);
		Il2CppObject * L_6 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_4, (String_t*)L_5, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		NullCheck((Il2CppObject *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = Enum_Parse_m982704874(NULL /*static, unused*/, (Type_t *)L_3, (String_t*)L_7, (bool)1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_002e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// T Unibill.Impl.MiniJsonExtensions::getEnum<Unibill.Impl.BillingPlatform>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* BillingPlatform_t552059234_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern const uint32_t MiniJsonExtensions_getEnum_TisBillingPlatform_t552059234_m90687349_MetadataUsageId;
extern "C"  int32_t MiniJsonExtensions_getEnum_TisBillingPlatform_t552059234_m90687349_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_getEnum_TisBillingPlatform_t552059234_m90687349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_0, (String_t*)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_4 = ___dic0;
		String_t* L_5 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_4);
		Il2CppObject * L_6 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_4, (String_t*)L_5, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		NullCheck((Il2CppObject *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = Enum_Parse_m982704874(NULL /*static, unused*/, (Type_t *)L_3, (String_t*)L_7, (bool)1, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_002e:
	{
		Initobj (BillingPlatform_t552059234_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_9 = V_0;
		return L_9;
	}
}
// T Unibill.Impl.MiniJsonExtensions::getEnum<Unibill.Impl.SamsungAppsMode>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* SamsungAppsMode_t2745005863_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2363051217_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2798132724_MethodInfo_var;
extern const uint32_t MiniJsonExtensions_getEnum_TisSamsungAppsMode_t2745005863_m422064390_MetadataUsageId;
extern "C"  int32_t MiniJsonExtensions_getEnum_TisSamsungAppsMode_t2745005863_m422064390_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_getEnum_TisSamsungAppsMode_t2745005863_m422064390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_0);
		bool L_2 = Dictionary_2_ContainsKey_m2363051217((Dictionary_2_t309261261 *)L_0, (String_t*)L_1, /*hidden argument*/Dictionary_2_ContainsKey_m2363051217_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_4 = ___dic0;
		String_t* L_5 = ___key1;
		NullCheck((Dictionary_2_t309261261 *)L_4);
		Il2CppObject * L_6 = Dictionary_2_get_Item_m2798132724((Dictionary_2_t309261261 *)L_4, (String_t*)L_5, /*hidden argument*/Dictionary_2_get_Item_m2798132724_MethodInfo_var);
		NullCheck((Il2CppObject *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = Enum_Parse_m982704874(NULL /*static, unused*/, (Type_t *)L_3, (String_t*)L_7, (bool)1, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox (L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
	}

IL_002e:
	{
		Initobj (SamsungAppsMode_t2745005863_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_9 = V_0;
		return L_9;
	}
}
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_MetadataUsageId;
extern "C"  Il2CppObject * AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m581732473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Type_t * L_1 = ___klass0;
		Type_t * L_2 = V_0;
		NullCheck((MemberInfo_t *)L_1);
		ObjectU5BU5D_t3614634134* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, (MemberInfo_t *)L_1, (Type_t *)L_2, (bool)1);
		V_1 = (ObjectU5BU5D_t3614634134*)L_3;
		ObjectU5BU5D_t3614634134* L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = V_1;
		NullCheck(L_5);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = 0;
		Il2CppObject * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_003d;
	}

IL_0031:
	{
		V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_003d;
	}

IL_003d:
	{
		Il2CppObject * L_9 = V_2;
		return L_9;
	}
}
// T UnityEngine.Component::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t3207297272_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponent_TisIl2CppObject_m4109961936_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponent_TisIl2CppObject_m4109961936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CastHelper_1_t3207297272  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	{
		Initobj (CastHelper_1_t3207297272_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m3033286303(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((Component_t3819376471 *)__this);
		Component_GetComponentFastPath_m2700814707((Component_t3819376471 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		V_1 = (Il2CppObject *)L_3;
		goto IL_0032;
	}

IL_0032:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((Component_t3819376471 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Component_t3819376471 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t3819376471 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Il2CppObject * L_2 = V_1;
		return L_2;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInChildren_TisIl2CppObject_m1823576579_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m1823576579_gshared (Component_t3819376471 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInChildren_TisIl2CppObject_m1823576579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((Component_t3819376471 *)__this);
		Component_t3819376471 * L_2 = Component_GetComponentInChildren_m3925629424((Component_t3819376471 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001d;
	}

IL_001d:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInParent_TisIl2CppObject_m2509612665_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared (Component_t3819376471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInParent_TisIl2CppObject_m2509612665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Component_t3819376471 *)__this);
		Component_t3819376471 * L_1 = Component_GetComponentInParent_m2799402500((Component_t3819376471 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001c;
	}

IL_001c:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral55013368;
extern const uint32_t ExecuteEvents_ValidateEventData_TisIl2CppObject_m3838331218_MetadataUsageId;
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m3838331218_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t2681005625 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ValidateEventData_TisIl2CppObject_m3838331218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		BaseEventData_t2681005625 * L_0 = ___data0;
		if (((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003b;
		}
	}
	{
		BaseEventData_t2681005625 * L_1 = ___data0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m191970594((Il2CppObject *)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1811873526(NULL /*static, unused*/, (String_t*)_stringLiteral55013368, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_5 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003b:
	{
		BaseEventData_t2681005625 * L_6 = ___data0;
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0051;
	}

IL_0051:
	{
		Il2CppObject * L_7 = V_0;
		return L_7;
	}
}
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_AddComponent_TisIl2CppObject_m4011380260_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4011380260_gshared (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_AddComponent_TisIl2CppObject_m4011380260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)__this);
		Component_t3819376471 * L_1 = GameObject_AddComponent_m3757565614((GameObject_t1756533147 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t3207297272_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponent_TisIl2CppObject_m2812611596_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponent_TisIl2CppObject_m2812611596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CastHelper_1_t3207297272  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	{
		Initobj (CastHelper_1_t3207297272_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m3033286303(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)__this);
		GameObject_GetComponentFastPath_m481070871((GameObject_t1756533147 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		V_1 = (Il2CppObject *)L_3;
		goto IL_0032;
	}

IL_0032:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Il2CppObject * L_2 = V_1;
		return L_2;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentInChildren_TisIl2CppObject_m1362037227_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m1362037227_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInChildren_TisIl2CppObject_m1362037227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)__this);
		Component_t3819376471 * L_2 = GameObject_GetComponentInChildren_m4263325740((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001d;
	}

IL_001d:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_TisIl2CppObject_m2967490724_MetadataUsageId;
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m2967490724_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_TisIl2CppObject_m2967490724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m447919519_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m447919519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral444318565, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___original0;
		Object_t1021602117 * L_2 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, (Object_t1021602117 *)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0027;
	}

IL_0027:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m1767088036_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m1767088036_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m1767088036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Transform_t3275118058 *)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Transform,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m1736742113_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m1736742113_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m1736742113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Transform_t3275118058 * L_1 = ___parent1;
		bool L_2 = ___worldPositionStays2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Instantiate_m2489341053(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Transform_t3275118058 *)L_1, (bool)L_2, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0019;
	}

IL_0019:
	{
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m653480707_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m653480707_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m653480707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Vector3_t2243707580  L_1 = ___position1;
		Quaternion_t4030073918  L_2 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Instantiate_m938141395(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Vector3_t2243707580 )L_1, (Quaternion_t4030073918 )L_2, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0019;
	}

IL_0019:
	{
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m4219963824_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m4219963824_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m4219963824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Vector3_t2243707580  L_1 = ___position1;
		Quaternion_t4030073918  L_2 = ___rotation2;
		Transform_t3275118058 * L_3 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_4 = Object_Instantiate_m2160322936(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Vector3_t2243707580 )L_1, (Quaternion_t4030073918 )L_2, (Transform_t3275118058 *)L_3, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_001a;
	}

IL_001a:
	{
		Il2CppObject * L_5 = V_0;
		return L_5;
	}
}
// T UnityEngine.Resources::GetBuiltinResource<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_GetBuiltinResource_TisIl2CppObject_m1023501484_MetadataUsageId;
extern "C"  Il2CppObject * Resources_GetBuiltinResource_TisIl2CppObject_m1023501484_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_GetBuiltinResource_TisIl2CppObject_m1023501484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_1 = ___path0;
		Object_t1021602117 * L_2 = Resources_GetBuiltinResource_m582410469(NULL /*static, unused*/, (Type_t *)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001c;
	}

IL_001c:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Resources::Load<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_TisIl2CppObject_m2884518678_MetadataUsageId;
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2884518678_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_TisIl2CppObject_m2884518678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, (String_t*)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001c;
	}

IL_001c:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject_CreateInstance_TisIl2CppObject_m2022535220_MetadataUsageId;
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m2022535220_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject_CreateInstance_TisIl2CppObject_m2022535220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ScriptableObject_t1975622470 * L_1 = ScriptableObject_CreateInstance_m3271154163(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Dropdown_GetOrAddComponent_TisIl2CppObject_m2875934266_MetadataUsageId;
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m2875934266_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dropdown_GetOrAddComponent_TisIl2CppObject_m2875934266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, (Object_t1021602117 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		GameObject_t1756533147 * L_4 = ___go0;
		NullCheck((GameObject_t1756533147 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1756533147 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001f:
	{
		Il2CppObject * L_6 = V_0;
		V_1 = (Il2CppObject *)L_6;
		goto IL_0026;
	}

IL_0026:
	{
		Il2CppObject * L_7 = V_1;
		return L_7;
	}
}
// T UnityUtil::findInstanceOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_findInstanceOfType_TisIl2CppObject_m2306951808_MetadataUsageId;
extern "C"  Il2CppObject * UnityUtil_findInstanceOfType_TisIl2CppObject_m2306951808_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_findInstanceOfType_TisIl2CppObject_m2306951808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityUtil::loadResourceInstanceOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_loadResourceInstanceOfType_TisIl2CppObject_m927293821_MetadataUsageId;
extern "C"  Il2CppObject * UnityUtil_loadResourceInstanceOfType_TisIl2CppObject_m927293821_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_loadResourceInstanceOfType_TisIl2CppObject_m927293821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_0);
		Object_t1021602117 * L_2 = Resources_Load_m2041782325(NULL /*static, unused*/, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Instantiate_m2439155489(NULL /*static, unused*/, (Object_t1021602117 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)((GameObject_t1756533147 *)Castclass(L_3, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1756533147 *)((GameObject_t1756533147 *)Castclass(L_3, GameObject_t1756533147_il2cpp_TypeInfo_var)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// T[] IPCycler::MakeWidgets<System.Object>()
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t IPCycler_MakeWidgets_TisIl2CppObject_m3829340906_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* IPCycler_MakeWidgets_TisIl2CppObject_m3829340906_gshared (IPCycler_t1336138445 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IPCycler_MakeWidgets_TisIl2CppObject_m3829340906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	int32_t V_1 = 0;
	{
		NullCheck((IPCycler_t1336138445 *)__this);
		int32_t L_0 = IPCycler_get_NbOfTransforms_m2926184893((IPCycler_t1336138445 *)__this, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_0030;
	}

IL_0013:
	{
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		int32_t L_2 = V_1;
		TransformU5BU5D_t3764228911* L_3 = (TransformU5BU5D_t3764228911*)__this->get__cycledTransforms_17();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Transform_t3275118058 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck((Component_t3819376471 *)L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_8);
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_10 = V_1;
		NullCheck((IPCycler_t1336138445 *)__this);
		int32_t L_11 = IPCycler_get_NbOfTransforms_m2926184893((IPCycler_t1336138445 *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_12 = V_0;
		return L_12;
	}
}
// T[] NGUITools::FindActive<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_FindActive_TisIl2CppObject_m2645751425_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* NGUITools_FindActive_TisIl2CppObject_m2645751425_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindActive_TisIl2CppObject_m2645751425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t3614634134*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t Array_FindAll_TisIl2CppObject_m2420286284_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* Array_FindAll_TisIl2CppObject_m2420286284_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Predicate_1_t1132419410 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_FindAll_TisIl2CppObject_m2420286284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t3614634134* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t1132419410 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ObjectU5BU5D_t3614634134* L_5 = ___array0;
		V_3 = (ObjectU5BU5D_t3614634134*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t3614634134* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_2 = (Il2CppObject *)L_9;
		Predicate_1_t1132419410 * L_10 = ___match1;
		Il2CppObject * L_11 = V_2;
		NullCheck((Predicate_1_t1132419410 *)L_10);
		bool L_12 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Predicate_1_t1132419410 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_13 = V_1;
		int32_t L_14 = V_0;
		int32_t L_15 = (int32_t)L_14;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		Il2CppObject * L_16 = V_2;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_16);
	}

IL_0058:
	{
		int32_t L_17 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_18 = V_4;
		ObjectU5BU5D_t3614634134* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_20 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134**)(&V_1), (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_21 = V_1;
		return L_21;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t3614634134* CustomAttributeData_UnboxValues_TisIl2CppObject_m1499708102_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t3614634134* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t3614634134* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_10 = V_0;
		return L_10;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3304067486* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t94157543_m2789115353_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3304067486* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)((CustomAttributeNamedArgumentU5BU5D_t3304067486*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t3614634134* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t3614634134* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = V_0;
		return L_10;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t1075686591* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1498197914_m2561215702_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___values0, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t1075686591* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)((CustomAttributeTypedArgumentU5BU5D_t1075686591*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t3614634134* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t3614634134* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = V_0;
		return L_10;
	}
}
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponents_TisIl2CppObject_m3998315035_gshared (Component_t3819376471 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_0);
		ObjectU5BU5D_t3614634134* L_1 = ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		return L_2;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared (Component_t3819376471 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Component_t3819376471 *)__this);
		ObjectU5BU5D_t3614634134* L_0 = ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t3819376471 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_0;
		goto IL_000e;
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared (Component_t3819376471 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)L_0);
		ObjectU5BU5D_t3614634134* L_2 = ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInParent_TisIl2CppObject_m1112546512_gshared (Component_t3819376471 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Component_t3819376471 *)__this);
		ObjectU5BU5D_t3614634134* L_0 = ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t3819376471 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_0;
		goto IL_000e;
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInParent_TisIl2CppObject_m2092455797_gshared (Component_t3819376471 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)L_0);
		ObjectU5BU5D_t3614634134* L_2 = ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m890532490_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponents_TisIl2CppObject_m890532490_gshared (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m890532490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m3486524399((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482_gshared (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((GameObject_t1756533147 *)__this);
		ObjectU5BU5D_t3614634134* L_0 = ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1756533147 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_0;
		goto IL_000e;
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		return L_1;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m541433219_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m541433219_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m541433219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m3486524399((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m3486524399((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		ObjectU5BU5D_t3614634134* L_3 = ((  ObjectU5BU5D_t3614634134* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		ObjectU5BU5D_t3614634134* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t30278651* Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Color32U5BU5D_t30278651* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Color32U5BU5D_t30278651*)((Color32U5BU5D_t30278651*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Color32U5BU5D_t30278651*)((Color32U5BU5D_t30278651*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Color32U5BU5D_t30278651* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector2U5BU5D_t686124026* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		Vector2U5BU5D_t686124026* L_3 = ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector2U5BU5D_t686124026*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2U5BU5D_t686124026* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector2U5BU5D_t686124026* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector2U5BU5D_t686124026*)((Vector2U5BU5D_t686124026*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector2U5BU5D_t686124026*)((Vector2U5BU5D_t686124026*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector2U5BU5D_t686124026* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		Vector3U5BU5D_t1172311765* L_3 = ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector3U5BU5D_t1172311765*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector3U5BU5D_t1172311765* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector3U5BU5D_t1172311765*)((Vector3U5BU5D_t1172311765*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector3U5BU5D_t1172311765*)((Vector3U5BU5D_t1172311765*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector3U5BU5D_t1172311765* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector4U5BU5D_t1658499504* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		Vector4U5BU5D_t1658499504* L_3 = ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector4U5BU5D_t1658499504*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector4U5BU5D_t1658499504* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector4U5BU5D_t1658499504* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector4U5BU5D_t1658499504*)((Vector4U5BU5D_t1658499504*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector4U5BU5D_t1658499504*)((Vector4U5BU5D_t1658499504*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector4U5BU5D_t1658499504* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m1343658011_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisIl2CppObject_m1343658011_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m1343658011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_2 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t4217747464*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t4217747464*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (ObjectU5BU5D_t3614634134*)L_2;
		goto IL_001b;
	}

IL_001b:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t3614634134* Resources_ConvertObjects_TisIl2CppObject_m2571720668_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t4217747464* ___rawObjects0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t4217747464* L_0 = ___rawObjects0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = (ObjectU5BU5D_t3614634134*)NULL;
		goto IL_0041;
	}

IL_000e:
	{
		ObjectU5BU5D_t4217747464* L_1 = ___rawObjects0;
		NullCheck(L_1);
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_2 = (int32_t)0;
		goto IL_0031;
	}

IL_001e:
	{
		ObjectU5BU5D_t3614634134* L_2 = V_1;
		int32_t L_3 = V_2;
		ObjectU5BU5D_t4217747464* L_4 = ___rawObjects0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Object_t1021602117 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_8 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_2;
		ObjectU5BU5D_t3614634134* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_11 = V_1;
		V_0 = (ObjectU5BU5D_t3614634134*)L_11;
		goto IL_0041;
	}

IL_0041:
	{
		ObjectU5BU5D_t3614634134* L_12 = V_0;
		return L_12;
	}
}
// T[] UnityUtil::getAnyComponentsOfType<System.Object>()
extern const Il2CppType* GameObject_t1756533147_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponents_TisMonoBehaviour_t1158329972_m1288443065_MethodInfo_var;
extern const uint32_t UnityUtil_getAnyComponentsOfType_TisIl2CppObject_m30029786_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* UnityUtil_getAnyComponentsOfType_TisIl2CppObject_m30029786_gshared (UnityUtil_t671748753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_getAnyComponentsOfType_TisIl2CppObject_m30029786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	List_1_t2058570427 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObjectU5BU5D_t3057952154* V_3 = NULL;
	int32_t V_4 = 0;
	MonoBehaviour_t1158329972 * V_5 = NULL;
	MonoBehaviourU5BU5D_t3035069757* V_6 = NULL;
	int32_t V_7 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GameObject_t1756533147_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (GameObjectU5BU5D_t3057952154*)((GameObjectU5BU5D_t3057952154*)Castclass(L_1, GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var));
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_1 = (List_1_t2058570427 *)L_2;
		GameObjectU5BU5D_t3057952154* L_3 = V_0;
		V_3 = (GameObjectU5BU5D_t3057952154*)L_3;
		V_4 = (int32_t)0;
		goto IL_007b;
	}

IL_0025:
	{
		GameObjectU5BU5D_t3057952154* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_t1756533147 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = (GameObject_t1756533147 *)L_7;
		GameObject_t1756533147 * L_8 = V_2;
		NullCheck((GameObject_t1756533147 *)L_8);
		MonoBehaviourU5BU5D_t3035069757* L_9 = GameObject_GetComponents_TisMonoBehaviour_t1158329972_m1288443065((GameObject_t1756533147 *)L_8, /*hidden argument*/GameObject_GetComponents_TisMonoBehaviour_t1158329972_m1288443065_MethodInfo_var);
		V_6 = (MonoBehaviourU5BU5D_t3035069757*)L_9;
		V_7 = (int32_t)0;
		goto IL_006a;
	}

IL_003a:
	{
		MonoBehaviourU5BU5D_t3035069757* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		MonoBehaviour_t1158329972 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = (MonoBehaviour_t1158329972 *)L_13;
		MonoBehaviour_t1158329972 * L_14 = V_5;
		if (!((Il2CppObject *)IsInst(L_14, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))
		{
			goto IL_0064;
		}
	}
	{
		List_1_t2058570427 * L_15 = V_1;
		MonoBehaviour_t1158329972 * L_16 = V_5;
		NullCheck((List_1_t2058570427 *)L_15);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((List_1_t2058570427 *)L_15, (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_16, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))), IL2CPP_RGCTX_DATA(method->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
	}

IL_0064:
	{
		int32_t L_17 = V_7;
		V_7 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006a:
	{
		int32_t L_18 = V_7;
		MonoBehaviourU5BU5D_t3035069757* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_20 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_007b:
	{
		int32_t L_21 = V_4;
		GameObjectU5BU5D_t3057952154* L_22 = V_3;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0025;
		}
	}
	{
		List_1_t2058570427 * L_23 = V_1;
		NullCheck((List_1_t2058570427 *)L_23);
		ObjectU5BU5D_t3614634134* L_24 = ((  ObjectU5BU5D_t3614634134* (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t2058570427 *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_24;
	}
}
// TAccumulate System.Linq.Enumerable::Aggregate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m2460971397_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m2460971397_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___seed1, Func_3_t3369346583 * ___func2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m2460971397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Func_3_t3369346583 * L_1 = ___func2;
		Check_SourceAndFunc_m1901269228(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___seed1;
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject* L_3 = ___source0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_2 = (Il2CppObject*)L_4;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0015:
		{
			Il2CppObject* L_5 = V_2;
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_5);
			V_1 = (Il2CppObject *)L_6;
			Func_3_t3369346583 * L_7 = ___func2;
			Il2CppObject * L_8 = V_0;
			Il2CppObject * L_9 = V_1;
			NullCheck((Func_3_t3369346583 *)L_7);
			Il2CppObject * L_10 = ((  Il2CppObject * (*) (Func_3_t3369346583 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t3369346583 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			V_0 = (Il2CppObject *)L_10;
		}

IL_0025:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0015;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_13 = V_2;
			if (L_13)
			{
				goto IL_0039;
			}
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(53)
		}

IL_0039:
		{
			Il2CppObject* L_14 = V_2;
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(53)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		Il2CppObject * L_15 = V_0;
		return L_15;
	}
}
// TAccumulate System.Linq.Enumerable::Aggregate<System.Object,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Aggregate_TisIl2CppObject_TisVector2_t2243707579_m519767975_MetadataUsageId;
extern "C"  Vector2_t2243707579  Enumerable_Aggregate_TisIl2CppObject_TisVector2_t2243707579_m519767975_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Vector2_t2243707579  ___seed1, Func_3_t1022329359 * ___func2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Aggregate_TisIl2CppObject_TisVector2_t2243707579_m519767975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Func_3_t1022329359 * L_1 = ___func2;
		Check_SourceAndFunc_m1901269228(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = ___seed1;
		V_0 = (Vector2_t2243707579 )L_2;
		Il2CppObject* L_3 = ___source0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_2 = (Il2CppObject*)L_4;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0015:
		{
			Il2CppObject* L_5 = V_2;
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_5);
			V_1 = (Il2CppObject *)L_6;
			Func_3_t1022329359 * L_7 = ___func2;
			Vector2_t2243707579  L_8 = V_0;
			Il2CppObject * L_9 = V_1;
			NullCheck((Func_3_t1022329359 *)L_7);
			Vector2_t2243707579  L_10 = ((  Vector2_t2243707579  (*) (Func_3_t1022329359 *, Vector2_t2243707579 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t1022329359 *)L_7, (Vector2_t2243707579 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			V_0 = (Vector2_t2243707579 )L_10;
		}

IL_0025:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0015;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_13 = V_2;
			if (L_13)
			{
				goto IL_0039;
			}
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(53)
		}

IL_0039:
		{
			Il2CppObject* L_14 = V_2;
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(53)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		Vector2_t2243707579  L_15 = V_0;
		return L_15;
	}
}
// TAccumulate System.Linq.Enumerable::Aggregate<UnityEngine.Vector3,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Aggregate_TisVector3_t2243707580_TisVector3_t2243707580_m2439534529_MetadataUsageId;
extern "C"  Vector3_t2243707580  Enumerable_Aggregate_TisVector3_t2243707580_TisVector3_t2243707580_m2439534529_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Vector3_t2243707580  ___seed1, Func_3_t246581160 * ___func2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Aggregate_TisVector3_t2243707580_TisVector3_t2243707580_m2439534529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Func_3_t246581160 * L_1 = ___func2;
		Check_SourceAndFunc_m1901269228(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___seed1;
		V_0 = (Vector3_t2243707580 )L_2;
		Il2CppObject* L_3 = ___source0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_2 = (Il2CppObject*)L_4;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0015:
		{
			Il2CppObject* L_5 = V_2;
			NullCheck((Il2CppObject*)L_5);
			Vector3_t2243707580  L_6 = InterfaceFuncInvoker0< Vector3_t2243707580  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_5);
			V_1 = (Vector3_t2243707580 )L_6;
			Func_3_t246581160 * L_7 = ___func2;
			Vector3_t2243707580  L_8 = V_0;
			Vector3_t2243707580  L_9 = V_1;
			NullCheck((Func_3_t246581160 *)L_7);
			Vector3_t2243707580  L_10 = ((  Vector3_t2243707580  (*) (Func_3_t246581160 *, Vector3_t2243707580 , Vector3_t2243707580 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t246581160 *)L_7, (Vector3_t2243707580 )L_8, (Vector3_t2243707580 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			V_0 = (Vector3_t2243707580 )L_10;
		}

IL_0025:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0015;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_13 = V_2;
			if (L_13)
			{
				goto IL_0039;
			}
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(53)
		}

IL_0039:
		{
			Il2CppObject* L_14 = V_2;
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(53)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		Vector3_t2243707580  L_15 = V_0;
		return L_15;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral3706307074;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Converter_2_t106372939 * ___converter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t106372939 * L_2 = ___converter1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3706307074, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t106372939 * L_7 = ___converter1;
		ObjectU5BU5D_t3614634134* L_8 = ___array0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck((Converter_2_t106372939 *)L_7);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (Converter_2_t106372939 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Converter_2_t106372939 *)L_7, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_1;
		ObjectU5BU5D_t3614634134* L_15 = ___array0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_16 = V_0;
		return L_16;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m3158850377_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3158850377_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m3158850377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source0;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005c:
	{
		InvalidOperationException_t721527559 * L_16 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		Il2CppObject * L_17 = V_2;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m1382289173_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m1382289173_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3961629604 * ___predicate1, int32_t ___fallback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m1382289173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Func_2_t3961629604 * L_4 = ___predicate1;
			Il2CppObject * L_5 = V_0;
			NullCheck((Func_2_t3961629604 *)L_4);
			bool L_6 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_2_t3961629604 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			if (!L_6)
			{
				goto IL_0026;
			}
		}

IL_001f:
		{
			Il2CppObject * L_7 = V_0;
			V_2 = (Il2CppObject *)L_7;
			IL2CPP_LEAVE(0x58, FINALLY_0036);
		}

IL_0026:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000c;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			if (L_10)
			{
				goto IL_003a;
			}
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(54)
		}

IL_003a:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(54)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0041:
	{
		int32_t L_12 = ___fallback2;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		InvalidOperationException_t721527559 * L_13 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_004e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_14 = V_3;
		return L_14;
	}

IL_0058:
	{
		Il2CppObject * L_15 = V_2;
		return L_15;
	}
}
// TSource System.Linq.Enumerable::First<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisVector3_t2243707580_m2727413067_MetadataUsageId;
extern "C"  Vector3_t2243707580  Enumerable_First_TisVector3_t2243707580_m2727413067_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisVector3_t2243707580_m2727413067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		Vector3_t2243707580  L_6 = InterfaceFuncInvoker1< Vector3_t2243707580 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.Vector3>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source0;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			Vector3_t2243707580  L_13 = InterfaceFuncInvoker0< Vector3_t2243707580  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (Vector3_t2243707580 )L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005c:
	{
		InvalidOperationException_t721527559 * L_16 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		Vector3_t2243707580  L_17 = V_2;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_1);
		V_1 = (Il2CppObject*)L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_0012:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck((Il2CppObject*)L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
			V_0 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_0;
			V_2 = (Il2CppObject *)L_5;
			IL2CPP_LEAVE(0x45, FINALLY_0030);
		}

IL_0020:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0012;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3B, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0034;
			}
		}

IL_0033:
		{
			IL2CPP_END_FINALLY(48)
		}

IL_0034:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_10 = V_3;
		return L_10;
	}

IL_0045:
	{
		Il2CppObject * L_11 = V_2;
		return L_11;
	}
}
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3961629604 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		Func_2_t3961629604 * L_1 = ___predicate1;
		Check_SourceAndPredicate_m2063478409(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source0;
		Func_2_t3961629604 * L_3 = ___predicate1;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t3961629604 *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource System.Linq.Enumerable::Last<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Last_TisInt32_t2071877448_m2438477215_MetadataUsageId;
extern "C"  int32_t Enumerable_Last_TisInt32_t2071877448_m2438477215_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Last_TisInt32_t2071877448_m2438477215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppObject* V_5 = NULL;
	int32_t V_6 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0024:
	{
		Il2CppObject* L_6 = ___source0;
		V_1 = (Il2CppObject*)((Il2CppObject*)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		Il2CppObject* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject* L_8 = V_1;
		Il2CppObject* L_9 = V_1;
		NullCheck((Il2CppObject*)L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_9);
		NullCheck((Il2CppObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Int32>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)));
		return L_11;
	}

IL_0040:
	{
		V_2 = (bool)1;
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_6));
		int32_t L_12 = V_6;
		V_3 = (int32_t)L_12;
		Il2CppObject* L_13 = ___source0;
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_13);
		V_5 = (Il2CppObject*)L_14;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_005a:
		{
			Il2CppObject* L_15 = V_5;
			NullCheck((Il2CppObject*)L_15);
			int32_t L_16 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_15);
			V_4 = (int32_t)L_16;
			int32_t L_17 = V_4;
			V_3 = (int32_t)L_17;
			V_2 = (bool)0;
		}

IL_0068:
		{
			Il2CppObject* L_18 = V_5;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_005a;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_20 = V_5;
			if (L_20)
			{
				goto IL_007e;
			}
		}

IL_007d:
		{
			IL2CPP_END_FINALLY(121)
		}

IL_007e:
		{
			Il2CppObject* L_21 = V_5;
			NullCheck((Il2CppObject *)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0086:
	{
		bool L_22 = V_2;
		if (L_22)
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_23 = V_3;
		return L_23;
	}

IL_008e:
	{
		InvalidOperationException_t721527559 * L_24 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}
}
// TSource System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Last_TisIl2CppObject_m518628776_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m518628776_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Last_TisIl2CppObject_m518628776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0024:
	{
		Il2CppObject* L_6 = ___source0;
		V_1 = (Il2CppObject*)((Il2CppObject*)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		Il2CppObject* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject* L_8 = V_1;
		Il2CppObject* L_9 = V_1;
		NullCheck((Il2CppObject*)L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_9);
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)));
		return L_11;
	}

IL_0040:
	{
		V_2 = (bool)1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_6));
		Il2CppObject * L_12 = V_6;
		V_3 = (Il2CppObject *)L_12;
		Il2CppObject* L_13 = ___source0;
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_13);
		V_5 = (Il2CppObject*)L_14;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_005a:
		{
			Il2CppObject* L_15 = V_5;
			NullCheck((Il2CppObject*)L_15);
			Il2CppObject * L_16 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_15);
			V_4 = (Il2CppObject *)L_16;
			Il2CppObject * L_17 = V_4;
			V_3 = (Il2CppObject *)L_17;
			V_2 = (bool)0;
		}

IL_0068:
		{
			Il2CppObject* L_18 = V_5;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_005a;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_20 = V_5;
			if (L_20)
			{
				goto IL_007e;
			}
		}

IL_007d:
		{
			IL2CPP_END_FINALLY(121)
		}

IL_007e:
		{
			Il2CppObject* L_21 = V_5;
			NullCheck((Il2CppObject *)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0086:
	{
		bool L_22 = V_2;
		if (L_22)
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_23 = V_3;
		return L_23;
	}

IL_008e:
	{
		InvalidOperationException_t721527559 * L_24 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}
}
// TSource System.Linq.Enumerable::Last<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Last_TisVector3_t2243707580_m2164312195_MetadataUsageId;
extern "C"  Vector3_t2243707580  Enumerable_Last_TisVector3_t2243707580_m2164312195_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Last_TisVector3_t2243707580_m2164312195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Il2CppObject* V_5 = NULL;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0024:
	{
		Il2CppObject* L_6 = ___source0;
		V_1 = (Il2CppObject*)((Il2CppObject*)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		Il2CppObject* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject* L_8 = V_1;
		Il2CppObject* L_9 = V_1;
		NullCheck((Il2CppObject*)L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_9);
		NullCheck((Il2CppObject*)L_8);
		Vector3_t2243707580  L_11 = InterfaceFuncInvoker1< Vector3_t2243707580 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.Vector3>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)));
		return L_11;
	}

IL_0040:
	{
		V_2 = (bool)1;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_6));
		Vector3_t2243707580  L_12 = V_6;
		V_3 = (Vector3_t2243707580 )L_12;
		Il2CppObject* L_13 = ___source0;
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_13);
		V_5 = (Il2CppObject*)L_14;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_005a:
		{
			Il2CppObject* L_15 = V_5;
			NullCheck((Il2CppObject*)L_15);
			Vector3_t2243707580  L_16 = InterfaceFuncInvoker0< Vector3_t2243707580  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_15);
			V_4 = (Vector3_t2243707580 )L_16;
			Vector3_t2243707580  L_17 = V_4;
			V_3 = (Vector3_t2243707580 )L_17;
			V_2 = (bool)0;
		}

IL_0068:
		{
			Il2CppObject* L_18 = V_5;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_005a;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_20 = V_5;
			if (L_20)
			{
				goto IL_007e;
			}
		}

IL_007d:
		{
			IL2CPP_END_FINALLY(121)
		}

IL_007e:
		{
			Il2CppObject* L_21 = V_5;
			NullCheck((Il2CppObject *)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0086:
	{
		bool L_22 = V_2;
		if (L_22)
		{
			goto IL_008e;
		}
	}
	{
		Vector3_t2243707580  L_23 = V_3;
		return L_23;
	}

IL_008e:
	{
		InvalidOperationException_t721527559 * L_24 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Byte>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ByteU5BU5D_t3397334013* Enumerable_ToArray_TisByte_t3683104436_m975316257_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Byte>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (ByteU5BU5D_t3397334013*)((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		ByteU5BU5D_t3397334013* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< ByteU5BU5D_t3397334013*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Byte>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (ByteU5BU5D_t3397334013*)L_6, (int32_t)0);
		ByteU5BU5D_t3397334013* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t3052225568 * L_9 = (List_1_t3052225568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t3052225568 *)L_9);
		ByteU5BU5D_t3397334013* L_10 = ((  ByteU5BU5D_t3397334013* (*) (List_1_t3052225568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t3052225568 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  KeyValuePair_2U5BU5D_t2854920344* Enumerable_ToArray_TisKeyValuePair_2_t38854645_m825136193_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	KeyValuePair_2U5BU5D_t2854920344* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (KeyValuePair_2U5BU5D_t2854920344*)((KeyValuePair_2U5BU5D_t2854920344*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		KeyValuePair_2U5BU5D_t2854920344* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2854920344*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (KeyValuePair_2U5BU5D_t2854920344*)L_6, (int32_t)0);
		KeyValuePair_2U5BU5D_t2854920344* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t3702943073 * L_9 = (List_1_t3702943073 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t3702943073 *)L_9);
		KeyValuePair_2U5BU5D_t2854920344* L_10 = ((  KeyValuePair_2U5BU5D_t2854920344* (*) (List_1_t3702943073 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t3702943073 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Int16>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int16U5BU5D_t3104283263* Enumerable_ToArray_TisInt16_t4041245914_m2624416067_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Int16U5BU5D_t3104283263* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int16>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Int16U5BU5D_t3104283263*)((Int16U5BU5D_t3104283263*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Int16U5BU5D_t3104283263* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Int16U5BU5D_t3104283263*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int16>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Int16U5BU5D_t3104283263*)L_6, (int32_t)0);
		Int16U5BU5D_t3104283263* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t3410367046 * L_9 = (List_1_t3410367046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t3410367046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t3410367046 *)L_9);
		Int16U5BU5D_t3104283263* L_10 = ((  Int16U5BU5D_t3104283263* (*) (List_1_t3410367046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t3410367046 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t3030399641* Enumerable_ToArray_TisInt32_t2071877448_m513246933_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Int32U5BU5D_t3030399641* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Int32U5BU5D_t3030399641* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Int32U5BU5D_t3030399641*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Int32U5BU5D_t3030399641*)L_6, (int32_t)0);
		Int32U5BU5D_t3030399641* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t1440998580 * L_9 = (List_1_t1440998580 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t1440998580 *)L_9);
		Int32U5BU5D_t3030399641* L_10 = ((  Int32U5BU5D_t3030399641* (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t1440998580 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m349912619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t3614634134* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< ObjectU5BU5D_t3614634134*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (ObjectU5BU5D_t3614634134*)L_6, (int32_t)0);
		ObjectU5BU5D_t3614634134* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t2058570427 * L_9 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t2058570427 *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = ((  ObjectU5BU5D_t3614634134* (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t2058570427 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Vector3U5BU5D_t1172311765* Enumerable_ToArray_TisVector3_t2243707580_m1930596601_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Vector3U5BU5D_t1172311765* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m3385315029(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Vector3U5BU5D_t1172311765*)((Vector3U5BU5D_t1172311765*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Vector3U5BU5D_t1172311765* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Vector3U5BU5D_t1172311765*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Vector3U5BU5D_t1172311765*)L_6, (int32_t)0);
		Vector3U5BU5D_t1172311765* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t1612828712 * L_9 = (List_1_t1612828712 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t1612828712 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t1612828712 *)L_9);
		Vector3U5BU5D_t1172311765* L_10 = ((  Vector3U5BU5D_t1172311765* (*) (List_1_t1612828712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t1612828712 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// U System.Linq.Enumerable::Iterate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3644922910_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3644922910_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___initValue1, Func_3_t3369346583 * ___selector2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3644922910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_2 = (Il2CppObject*)L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0021;
		}

IL_000e:
		{
			Il2CppObject* L_2 = V_2;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_1 = (Il2CppObject *)L_3;
			Func_3_t3369346583 * L_4 = ___selector2;
			Il2CppObject * L_5 = V_1;
			Il2CppObject * L_6 = ___initValue1;
			NullCheck((Func_3_t3369346583 *)L_4);
			Il2CppObject * L_7 = ((  Il2CppObject * (*) (Func_3_t3369346583 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t3369346583 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			___initValue1 = (Il2CppObject *)L_7;
			V_0 = (bool)0;
		}

IL_0021:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000e;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (L_10)
			{
				goto IL_0035;
			}
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(49)
		}

IL_0035:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003c:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0048;
		}
	}
	{
		InvalidOperationException_t721527559 * L_13 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0048:
	{
		Il2CppObject * L_14 = ___initValue1;
		return L_14;
	}
}
// U System.Linq.Enumerable::Iterate<System.Object,System.Single>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Iterate_TisIl2CppObject_TisSingle_t2076509932_m641357313_MetadataUsageId;
extern "C"  float Enumerable_Iterate_TisIl2CppObject_TisSingle_t2076509932_m641357313_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, float ___initValue1, Func_3_t4013867779 * ___selector2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Iterate_TisIl2CppObject_TisSingle_t2076509932_m641357313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_2 = (Il2CppObject*)L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0021;
		}

IL_000e:
		{
			Il2CppObject* L_2 = V_2;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_1 = (Il2CppObject *)L_3;
			Func_3_t4013867779 * L_4 = ___selector2;
			Il2CppObject * L_5 = V_1;
			float L_6 = ___initValue1;
			NullCheck((Func_3_t4013867779 *)L_4);
			float L_7 = ((  float (*) (Func_3_t4013867779 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t4013867779 *)L_4, (Il2CppObject *)L_5, (float)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			___initValue1 = (float)L_7;
			V_0 = (bool)0;
		}

IL_0021:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000e;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (L_10)
			{
				goto IL_0035;
			}
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(49)
		}

IL_0035:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003c:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0048;
		}
	}
	{
		InvalidOperationException_t721527559 * L_13 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0048:
	{
		float L_14 = ___initValue1;
		return L_14;
	}
}
// U System.Linq.Enumerable::Iterate<UnityEngine.Vector3,System.Single>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Iterate_TisVector3_t2243707580_TisSingle_t2076509932_m1684800810_MetadataUsageId;
extern "C"  float Enumerable_Iterate_TisVector3_t2243707580_TisSingle_t2076509932_m1684800810_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, float ___initValue1, Func_3_t3779166440 * ___selector2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Iterate_TisVector3_t2243707580_TisSingle_t2076509932_m1684800810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_2 = (Il2CppObject*)L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0021;
		}

IL_000e:
		{
			Il2CppObject* L_2 = V_2;
			NullCheck((Il2CppObject*)L_2);
			Vector3_t2243707580  L_3 = InterfaceFuncInvoker0< Vector3_t2243707580  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_1 = (Vector3_t2243707580 )L_3;
			Func_3_t3779166440 * L_4 = ___selector2;
			Vector3_t2243707580  L_5 = V_1;
			float L_6 = ___initValue1;
			NullCheck((Func_3_t3779166440 *)L_4);
			float L_7 = ((  float (*) (Func_3_t3779166440 *, Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t3779166440 *)L_4, (Vector3_t2243707580 )L_5, (float)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			___initValue1 = (float)L_7;
			V_0 = (bool)0;
		}

IL_0021:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000e;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (L_10)
			{
				goto IL_0035;
			}
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(49)
		}

IL_0035:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003c:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0048;
		}
	}
	{
		InvalidOperationException_t721527559 * L_13 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0048:
	{
		float L_14 = ___initValue1;
		return L_14;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m563292115_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1995107056_MethodInfo_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, BaseEventData_t2681005625 * ___eventData1, EventFunction_1_t1186599945 * ___callbackFunction2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3275118058 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		List_1_t2644239190 * L_1 = ((ExecuteEvents_t1693084770_StaticFields*)ExecuteEvents_t1693084770_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m713190182(NULL /*static, unused*/, (GameObject_t1756533147 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_0043;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		List_1_t2644239190 * L_2 = ((ExecuteEvents_t1693084770_StaticFields*)ExecuteEvents_t1693084770_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t2644239190 *)L_2);
		Transform_t3275118058 * L_4 = List_1_get_Item_m563292115((List_1_t2644239190 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_1 = (Transform_t3275118058 *)L_4;
		Transform_t3275118058 * L_5 = V_1;
		NullCheck((Component_t3819376471 *)L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t2681005625 * L_7 = ___eventData1;
		EventFunction_1_t1186599945 * L_8 = ___callbackFunction2;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_6, (BaseEventData_t2681005625 *)L_7, (EventFunction_1_t1186599945 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		Transform_t3275118058 * L_10 = V_1;
		NullCheck((Component_t3819376471 *)L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_10, /*hidden argument*/NULL);
		V_2 = (GameObject_t1756533147 *)L_11;
		goto IL_005a;
	}

IL_003e:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		List_1_t2644239190 * L_14 = ((ExecuteEvents_t1693084770_StaticFields*)ExecuteEvents_t1693084770_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		NullCheck((List_1_t2644239190 *)L_14);
		int32_t L_15 = List_1_get_Count_m1995107056((List_1_t2644239190 *)L_14, /*hidden argument*/List_1_get_Count_m1995107056_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		V_2 = (GameObject_t1756533147 *)NULL;
		goto IL_005a;
	}

IL_005a:
	{
		GameObject_t1756533147 * L_16 = V_2;
		return L_16;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		goto IL_0058;
	}

IL_0014:
	{
		GameObject_t1756533147 * L_2 = ___root0;
		NullCheck((GameObject_t1756533147 *)L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_2, /*hidden argument*/NULL);
		V_1 = (Transform_t3275118058 *)L_3;
		goto IL_0045;
	}

IL_0020:
	{
		Transform_t3275118058 * L_4 = V_1;
		NullCheck((Component_t3819376471 *)L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3275118058 * L_7 = V_1;
		NullCheck((Component_t3819376471 *)L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_7, /*hidden argument*/NULL);
		V_0 = (GameObject_t1756533147 *)L_8;
		goto IL_0058;
	}

IL_003d:
	{
		Transform_t3275118058 * L_9 = V_1;
		NullCheck((Transform_t3275118058 *)L_9);
		Transform_t3275118058 * L_10 = Transform_get_parent_m147407266((Transform_t3275118058 *)L_9, /*hidden argument*/NULL);
		V_1 = (Transform_t3275118058 *)L_10;
	}

IL_0045:
	{
		Transform_t3275118058 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		goto IL_0058;
	}

IL_0058:
	{
		GameObject_t1756533147 * L_13 = V_0;
		return L_13;
	}
}
