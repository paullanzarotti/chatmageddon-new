﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnClosedDelegate587495364.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnErrorDelegate3605384424.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnStateChanged3950199048.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnPrepareReques3434123680.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_ProtocolVersion4229923159.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Connection2915781690.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_TransportTypes614909816.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_MessageTypes615080634.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_ConnectionStates420400692.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_RequestTypes1771348820.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_TransportStates2802087367.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodCa1474974379.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodCa3483117754.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodRe3666295392.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodFa3007711612.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodPr4205605290.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_Hub272719679.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_JsonEncoders_Defa78830025.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_ClientM624279968.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_KeepAl3806624343.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_MultiMe207384742.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_DataMes333379451.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Method1119839878.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Result3036240250.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Failur2793948643.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Progre1081190384.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_NegotiationData3059020807.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Poll1976236230.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Post4137891006.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Serv1441151459.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_OnTr1888872800.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Trans148904526.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_WebS3045819522.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_TransportEvent2410498534.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketIOEventT2290781438.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketIOErrors567512470.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Error1148500814.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_SocketIOC88619200.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_SocketIOA53599143.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventDe4057040835.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventNam236102534.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventTa3321213846.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventTab356852809.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_HandshakeData1703965475.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_JsonEncoders_D1361426165.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Packet1309324146.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Packet_Payload2318637743.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Socket2716624701.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketManager3470268644.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketManager_3533834126.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketOptions3023631931.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Tra1004880669.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Pol2868132702.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Web1308710816.h"
#include "AssemblyU2DCSharp_BestHTTP_Statistics_StatisticsQu1999199910.h"
#include "AssemblyU2DCSharp_BestHTTP_Statistics_GeneralStati1610391941.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Extensions_Pe1814721980.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSoc4163283394.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSock549273869.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSoc3499449257.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketOp4018547189.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketMes730459590.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketBi3919072826.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketCl2679686585.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketEr1328789641.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketEr2599777013.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketIn2035490966.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocket71448861.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocketResp3376763264.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocketStau2332119393.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_AssetBundleSam4142830892.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_AssetBundleSam1982816999.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_AssetBundleSam3058815025.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_LargeFileDownl3427736475.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_TextureDownload853801945.h"
#include "AssemblyU2DCSharp_UploadStream108391441.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_CodeBlocks753507477.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_GUIHelper1081137527.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_GUIMessageList2719866248.h"
#include "AssemblyU2DCSharp_LitJson_JsonType3145703806.h"
#include "AssemblyU2DCSharp_LitJson_JsonData269267574.h"
#include "AssemblyU2DCSharp_LitJson_OrderedDictionaryEnumera3437478891.h"
#include "AssemblyU2DCSharp_LitJson_JsonException613047007.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc408878057.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (OnClosedDelegate_t587495364), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (OnErrorDelegate_t3605384424), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (OnStateChanged_t3950199048), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (OnPrepareRequestDelegate_t3434123680), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { sizeof (ProtocolVersions_t4229923159)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4505[4] = 
{
	ProtocolVersions_t4229923159::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (Connection_t2915781690), -1, sizeof(Connection_t2915781690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4506[37] = 
{
	Connection_t2915781690_StaticFields::get_offset_of_DefaultEncoder_0(),
	Connection_t2915781690::get_offset_of_U3CUriU3Ek__BackingField_1(),
	Connection_t2915781690::get_offset_of__state_2(),
	Connection_t2915781690::get_offset_of_U3CNegotiationResultU3Ek__BackingField_3(),
	Connection_t2915781690::get_offset_of_U3CHubsU3Ek__BackingField_4(),
	Connection_t2915781690::get_offset_of_U3CTransportU3Ek__BackingField_5(),
	Connection_t2915781690::get_offset_of_additionalQueryParams_6(),
	Connection_t2915781690::get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_7(),
	Connection_t2915781690::get_offset_of_U3CJsonEncoderU3Ek__BackingField_8(),
	Connection_t2915781690::get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_9(),
	Connection_t2915781690::get_offset_of_OnConnected_10(),
	Connection_t2915781690::get_offset_of_OnClosed_11(),
	Connection_t2915781690::get_offset_of_OnError_12(),
	Connection_t2915781690::get_offset_of_OnReconnecting_13(),
	Connection_t2915781690::get_offset_of_OnReconnected_14(),
	Connection_t2915781690::get_offset_of_OnStateChanged_15(),
	Connection_t2915781690::get_offset_of_OnNonHubMessage_16(),
	Connection_t2915781690::get_offset_of_U3CRequestPreparatorU3Ek__BackingField_17(),
	Connection_t2915781690::get_offset_of_U3CProtocolU3Ek__BackingField_18(),
	Connection_t2915781690::get_offset_of_SyncRoot_19(),
	Connection_t2915781690::get_offset_of_U3CClientMessageCounterU3Ek__BackingField_20(),
	Connection_t2915781690::get_offset_of_ClientProtocols_21(),
	Connection_t2915781690::get_offset_of_RequestCounter_22(),
	Connection_t2915781690::get_offset_of_LastReceivedMessage_23(),
	Connection_t2915781690::get_offset_of_GroupsToken_24(),
	Connection_t2915781690::get_offset_of_BufferedMessages_25(),
	Connection_t2915781690::get_offset_of_LastMessageReceivedAt_26(),
	Connection_t2915781690::get_offset_of_ReconnectStartedAt_27(),
	Connection_t2915781690::get_offset_of_ReconnectStarted_28(),
	Connection_t2915781690::get_offset_of_LastPingSentAt_29(),
	Connection_t2915781690::get_offset_of_PingInterval_30(),
	Connection_t2915781690::get_offset_of_PingRequest_31(),
	Connection_t2915781690::get_offset_of_TransportConnectionStartedAt_32(),
	Connection_t2915781690::get_offset_of_queryBuilder_33(),
	Connection_t2915781690::get_offset_of_BuiltConnectionData_34(),
	Connection_t2915781690::get_offset_of_BuiltQueryParams_35(),
	Connection_t2915781690::get_offset_of_NextProtocolToTry_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (TransportTypes_t614909816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4507[4] = 
{
	TransportTypes_t614909816::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (MessageTypes_t615080634)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4508[8] = 
{
	MessageTypes_t615080634::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (ConnectionStates_t420400692)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4509[8] = 
{
	ConnectionStates_t420400692::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (RequestTypes_t1771348820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4510[9] = 
{
	RequestTypes_t1771348820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (TransportStates_t2802087367)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4511[8] = 
{
	TransportStates_t2802087367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (OnMethodCallDelegate_t1474974379), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { sizeof (OnMethodCallCallbackDelegate_t3483117754), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (OnMethodResultDelegate_t3666295392), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (OnMethodFailedDelegate_t3007711612), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (OnMethodProgressDelegate_t4205605290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (Hub_t272719679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4517[7] = 
{
	Hub_t272719679::get_offset_of_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_0(),
	Hub_t272719679::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Hub_t272719679::get_offset_of_state_2(),
	Hub_t272719679::get_offset_of_OnMethodCall_3(),
	Hub_t272719679::get_offset_of_SentMessages_4(),
	Hub_t272719679::get_offset_of_MethodTable_5(),
	Hub_t272719679::get_offset_of_builder_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (DefaultJsonEncoder_t78830025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (ClientMessage_t624279968)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4521[7] = 
{
	ClientMessage_t624279968::get_offset_of_Hub_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_Method_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_Args_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_CallIdx_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_ResultCallback_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_ResultErrorCallback_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_ProgressCallback_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { sizeof (KeepAliveMessage_t3806624343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { sizeof (MultiMessage_t207384742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4525[6] = 
{
	MultiMessage_t207384742::get_offset_of_U3CMessageIdU3Ek__BackingField_0(),
	MultiMessage_t207384742::get_offset_of_U3CIsInitializationU3Ek__BackingField_1(),
	MultiMessage_t207384742::get_offset_of_U3CGroupsTokenU3Ek__BackingField_2(),
	MultiMessage_t207384742::get_offset_of_U3CShouldReconnectU3Ek__BackingField_3(),
	MultiMessage_t207384742::get_offset_of_U3CPollDelayU3Ek__BackingField_4(),
	MultiMessage_t207384742::get_offset_of_U3CDataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (DataMessage_t333379451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4526[1] = 
{
	DataMessage_t333379451::get_offset_of_U3CDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (MethodCallMessage_t1119839878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4527[4] = 
{
	MethodCallMessage_t1119839878::get_offset_of_U3CHubU3Ek__BackingField_0(),
	MethodCallMessage_t1119839878::get_offset_of_U3CMethodU3Ek__BackingField_1(),
	MethodCallMessage_t1119839878::get_offset_of_U3CArgumentsU3Ek__BackingField_2(),
	MethodCallMessage_t1119839878::get_offset_of_U3CStateU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (ResultMessage_t3036240250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4528[3] = 
{
	ResultMessage_t3036240250::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	ResultMessage_t3036240250::get_offset_of_U3CReturnValueU3Ek__BackingField_1(),
	ResultMessage_t3036240250::get_offset_of_U3CStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (FailureMessage_t2793948643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4529[6] = 
{
	FailureMessage_t2793948643::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	FailureMessage_t2793948643::get_offset_of_U3CIsHubErrorU3Ek__BackingField_1(),
	FailureMessage_t2793948643::get_offset_of_U3CErrorMessageU3Ek__BackingField_2(),
	FailureMessage_t2793948643::get_offset_of_U3CAdditionalDataU3Ek__BackingField_3(),
	FailureMessage_t2793948643::get_offset_of_U3CStackTraceU3Ek__BackingField_4(),
	FailureMessage_t2793948643::get_offset_of_U3CStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (ProgressMessage_t1081190384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4530[2] = 
{
	ProgressMessage_t1081190384::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	ProgressMessage_t1081190384::get_offset_of_U3CProgressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (NegotiationData_t3059020807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4531[15] = 
{
	NegotiationData_t3059020807::get_offset_of_U3CUrlU3Ek__BackingField_0(),
	NegotiationData_t3059020807::get_offset_of_U3CWebSocketServerUrlU3Ek__BackingField_1(),
	NegotiationData_t3059020807::get_offset_of_U3CConnectionTokenU3Ek__BackingField_2(),
	NegotiationData_t3059020807::get_offset_of_U3CConnectionIdU3Ek__BackingField_3(),
	NegotiationData_t3059020807::get_offset_of_U3CKeepAliveTimeoutU3Ek__BackingField_4(),
	NegotiationData_t3059020807::get_offset_of_U3CDisconnectTimeoutU3Ek__BackingField_5(),
	NegotiationData_t3059020807::get_offset_of_U3CConnectionTimeoutU3Ek__BackingField_6(),
	NegotiationData_t3059020807::get_offset_of_U3CTryWebSocketsU3Ek__BackingField_7(),
	NegotiationData_t3059020807::get_offset_of_U3CProtocolVersionU3Ek__BackingField_8(),
	NegotiationData_t3059020807::get_offset_of_U3CTransportConnectTimeoutU3Ek__BackingField_9(),
	NegotiationData_t3059020807::get_offset_of_U3CLongPollDelayU3Ek__BackingField_10(),
	NegotiationData_t3059020807::get_offset_of_OnReceived_11(),
	NegotiationData_t3059020807::get_offset_of_OnError_12(),
	NegotiationData_t3059020807::get_offset_of_NegotiationRequest_13(),
	NegotiationData_t3059020807::get_offset_of_Connection_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (PollingTransport_t1976236230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4532[4] = 
{
	PollingTransport_t1976236230::get_offset_of_LastPoll_6(),
	PollingTransport_t1976236230::get_offset_of_PollDelay_7(),
	PollingTransport_t1976236230::get_offset_of_PollTimeout_8(),
	PollingTransport_t1976236230::get_offset_of_pollRequest_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (PostSendTransportBase_t4137891006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[1] = 
{
	PostSendTransportBase_t4137891006::get_offset_of_sendRequestQueue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (ServerSentEventsTransport_t1441151459), -1, sizeof(ServerSentEventsTransport_t1441151459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4534[2] = 
{
	ServerSentEventsTransport_t1441151459::get_offset_of_EventSource_6(),
	ServerSentEventsTransport_t1441151459_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (OnTransportStateChangedDelegate_t1888872800), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (TransportBase_t148904526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4536[5] = 
{
	0,
	TransportBase_t148904526::get_offset_of_U3CNameU3Ek__BackingField_1(),
	TransportBase_t148904526::get_offset_of_U3CConnectionU3Ek__BackingField_2(),
	TransportBase_t148904526::get_offset_of__state_3(),
	TransportBase_t148904526::get_offset_of_OnStateChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (WebSocketTransport_t3045819522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4537[1] = 
{
	WebSocketTransport_t3045819522::get_offset_of_wSocket_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (TransportEventTypes_t2410498534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4538[9] = 
{
	TransportEventTypes_t2410498534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (SocketIOEventTypes_t2290781438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4539[9] = 
{
	SocketIOEventTypes_t2290781438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (SocketIOErrors_t567512470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4540[8] = 
{
	SocketIOErrors_t567512470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (Error_t1148500814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4541[2] = 
{
	Error_t1148500814::get_offset_of_U3CCodeU3Ek__BackingField_0(),
	Error_t1148500814::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (SocketIOCallback_t88619200), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (SocketIOAckCallback_t53599143), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (EventDescriptor_t4057040835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4544[4] = 
{
	EventDescriptor_t4057040835::get_offset_of_U3CCallbacksU3Ek__BackingField_0(),
	EventDescriptor_t4057040835::get_offset_of_U3COnlyOnceU3Ek__BackingField_1(),
	EventDescriptor_t4057040835::get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_2(),
	EventDescriptor_t4057040835::get_offset_of_CallbackArray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (EventNames_t236102534), -1, sizeof(EventNames_t236102534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4545[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	EventNames_t236102534_StaticFields::get_offset_of_SocketIONames_7(),
	EventNames_t236102534_StaticFields::get_offset_of_TransportNames_8(),
	EventNames_t236102534_StaticFields::get_offset_of_BlacklistedEvents_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (EventTable_t3321213846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4546[2] = 
{
	EventTable_t3321213846::get_offset_of_U3CSocketU3Ek__BackingField_0(),
	EventTable_t3321213846::get_offset_of_Table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (U3CRegisterU3Ec__AnonStorey0_t356852809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4547[2] = 
{
	U3CRegisterU3Ec__AnonStorey0_t356852809::get_offset_of_onlyOnce_0(),
	U3CRegisterU3Ec__AnonStorey0_t356852809::get_offset_of_autoDecodePayload_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (HandshakeData_t1703965475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4548[8] = 
{
	HandshakeData_t1703965475::get_offset_of_U3CSidU3Ek__BackingField_0(),
	HandshakeData_t1703965475::get_offset_of_U3CUpgradesU3Ek__BackingField_1(),
	HandshakeData_t1703965475::get_offset_of_U3CPingIntervalU3Ek__BackingField_2(),
	HandshakeData_t1703965475::get_offset_of_U3CPingTimeoutU3Ek__BackingField_3(),
	HandshakeData_t1703965475::get_offset_of_U3CManagerU3Ek__BackingField_4(),
	HandshakeData_t1703965475::get_offset_of_OnReceived_5(),
	HandshakeData_t1703965475::get_offset_of_OnError_6(),
	HandshakeData_t1703965475::get_offset_of_HandshakeRequest_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (DefaultJSonEncoder_t1361426165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (Packet_t1309324146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4553[11] = 
{
	0,
	Packet_t1309324146::get_offset_of_U3CTransportEventU3Ek__BackingField_1(),
	Packet_t1309324146::get_offset_of_U3CSocketIOEventU3Ek__BackingField_2(),
	Packet_t1309324146::get_offset_of_U3CAttachmentCountU3Ek__BackingField_3(),
	Packet_t1309324146::get_offset_of_U3CIdU3Ek__BackingField_4(),
	Packet_t1309324146::get_offset_of_U3CNamespaceU3Ek__BackingField_5(),
	Packet_t1309324146::get_offset_of_U3CPayloadU3Ek__BackingField_6(),
	Packet_t1309324146::get_offset_of_U3CEventNameU3Ek__BackingField_7(),
	Packet_t1309324146::get_offset_of_attachments_8(),
	Packet_t1309324146::get_offset_of_U3CIsDecodedU3Ek__BackingField_9(),
	Packet_t1309324146::get_offset_of_U3CDecodedArgsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (PayloadTypes_t2318637743)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4554[3] = 
{
	PayloadTypes_t2318637743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (Socket_t2716624701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4555[7] = 
{
	Socket_t2716624701::get_offset_of_U3CManagerU3Ek__BackingField_0(),
	Socket_t2716624701::get_offset_of_U3CNamespaceU3Ek__BackingField_1(),
	Socket_t2716624701::get_offset_of_U3CIsOpenU3Ek__BackingField_2(),
	Socket_t2716624701::get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_3(),
	Socket_t2716624701::get_offset_of_AckCallbacks_4(),
	Socket_t2716624701::get_offset_of_EventCallbacks_5(),
	Socket_t2716624701::get_offset_of_arguments_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (SocketManager_t3470268644), -1, sizeof(SocketManager_t3470268644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4556[20] = 
{
	SocketManager_t3470268644_StaticFields::get_offset_of_DefaultEncoder_0(),
	0,
	SocketManager_t3470268644::get_offset_of_state_2(),
	SocketManager_t3470268644::get_offset_of_U3COptionsU3Ek__BackingField_3(),
	SocketManager_t3470268644::get_offset_of_U3CUriU3Ek__BackingField_4(),
	SocketManager_t3470268644::get_offset_of_U3CHandshakeU3Ek__BackingField_5(),
	SocketManager_t3470268644::get_offset_of_U3CTransportU3Ek__BackingField_6(),
	SocketManager_t3470268644::get_offset_of_U3CRequestCounterU3Ek__BackingField_7(),
	SocketManager_t3470268644::get_offset_of_U3CReconnectAttemptsU3Ek__BackingField_8(),
	SocketManager_t3470268644::get_offset_of_U3CEncoderU3Ek__BackingField_9(),
	SocketManager_t3470268644::get_offset_of_nextAckId_10(),
	SocketManager_t3470268644::get_offset_of_U3CPreviousStateU3Ek__BackingField_11(),
	SocketManager_t3470268644::get_offset_of_Namespaces_12(),
	SocketManager_t3470268644::get_offset_of_Sockets_13(),
	SocketManager_t3470268644::get_offset_of_OfflinePackets_14(),
	SocketManager_t3470268644::get_offset_of_LastHeartbeat_15(),
	SocketManager_t3470268644::get_offset_of_LastPongReceived_16(),
	SocketManager_t3470268644::get_offset_of_ReconnectAt_17(),
	SocketManager_t3470268644::get_offset_of_ConnectionStarted_18(),
	SocketManager_t3470268644::get_offset_of_closing_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (States_t3533834126)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4557[6] = 
{
	States_t3533834126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (SocketOptions_t3023631931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4558[10] = 
{
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionU3Ek__BackingField_0(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_1(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionDelayU3Ek__BackingField_2(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionDelayMaxU3Ek__BackingField_3(),
	SocketOptions_t3023631931::get_offset_of_randomizationFactor_4(),
	SocketOptions_t3023631931::get_offset_of_U3CTimeoutU3Ek__BackingField_5(),
	SocketOptions_t3023631931::get_offset_of_U3CAutoConnectU3Ek__BackingField_6(),
	SocketOptions_t3023631931::get_offset_of_additionalQueryParams_7(),
	SocketOptions_t3023631931::get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8(),
	SocketOptions_t3023631931::get_offset_of_BuiltQueryParams_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (TransportStates_t1004880669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4559[6] = 
{
	TransportStates_t1004880669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (PollingTransport_t2868132702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4561[5] = 
{
	PollingTransport_t2868132702::get_offset_of_U3CStateU3Ek__BackingField_0(),
	PollingTransport_t2868132702::get_offset_of_U3CManagerU3Ek__BackingField_1(),
	PollingTransport_t2868132702::get_offset_of_LastRequest_2(),
	PollingTransport_t2868132702::get_offset_of_PollRequest_3(),
	PollingTransport_t2868132702::get_offset_of_PacketWithAttachment_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (WebSocketTransport_t1308710816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4562[6] = 
{
	WebSocketTransport_t1308710816::get_offset_of_U3CStateU3Ek__BackingField_0(),
	WebSocketTransport_t1308710816::get_offset_of_U3CManagerU3Ek__BackingField_1(),
	WebSocketTransport_t1308710816::get_offset_of_U3CIsRequestInProgressU3Ek__BackingField_2(),
	WebSocketTransport_t1308710816::get_offset_of_U3CImplementationU3Ek__BackingField_3(),
	WebSocketTransport_t1308710816::get_offset_of_PacketWithAttachment_4(),
	WebSocketTransport_t1308710816::get_offset_of_Buffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (StatisticsQueryFlags_t1999199910)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4563[5] = 
{
	StatisticsQueryFlags_t1999199910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (GeneralStatistics_t1610391941)+ sizeof (Il2CppObject), sizeof(GeneralStatistics_t1610391941 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4564[10] = 
{
	GeneralStatistics_t1610391941::get_offset_of_QueryFlags_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_Connections_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_ActiveConnections_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_FreeConnections_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_RecycledConnections_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_RequestsInQueue_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CacheEntityCount_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CacheSize_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CookieCount_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CookieJarSize_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (PerMessageCompression_t1814721980), -1, sizeof(PerMessageCompression_t1814721980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4566[13] = 
{
	PerMessageCompression_t1814721980_StaticFields::get_offset_of_Trailer_0(),
	PerMessageCompression_t1814721980::get_offset_of_U3CClientNoContextTakeoverU3Ek__BackingField_1(),
	PerMessageCompression_t1814721980::get_offset_of_U3CServerNoContextTakeoverU3Ek__BackingField_2(),
	PerMessageCompression_t1814721980::get_offset_of_U3CClientMaxWindowBitsU3Ek__BackingField_3(),
	PerMessageCompression_t1814721980::get_offset_of_U3CServerMaxWindowBitsU3Ek__BackingField_4(),
	PerMessageCompression_t1814721980::get_offset_of_U3CLevelU3Ek__BackingField_5(),
	PerMessageCompression_t1814721980::get_offset_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_6(),
	PerMessageCompression_t1814721980::get_offset_of_compressorOutputStream_7(),
	PerMessageCompression_t1814721980::get_offset_of_compressorDeflateStream_8(),
	PerMessageCompression_t1814721980::get_offset_of_decompressorInputStream_9(),
	PerMessageCompression_t1814721980::get_offset_of_decompressorOutputStream_10(),
	PerMessageCompression_t1814721980::get_offset_of_decompressorDeflateStream_11(),
	PerMessageCompression_t1814721980::get_offset_of_copyBuffer_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (WebSocketFrame_t4163283394), -1, sizeof(WebSocketFrame_t4163283394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4567[6] = 
{
	WebSocketFrame_t4163283394_StaticFields::get_offset_of_NoData_0(),
	WebSocketFrame_t4163283394::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	WebSocketFrame_t4163283394::get_offset_of_U3CIsFinalU3Ek__BackingField_2(),
	WebSocketFrame_t4163283394::get_offset_of_U3CHeaderU3Ek__BackingField_3(),
	WebSocketFrame_t4163283394::get_offset_of_U3CDataU3Ek__BackingField_4(),
	WebSocketFrame_t4163283394::get_offset_of_U3CUseExtensionsU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (WebSocketFrameReader_t549273869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4568[7] = 
{
	WebSocketFrameReader_t549273869::get_offset_of_U3CHeaderU3Ek__BackingField_0(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CIsFinalU3Ek__BackingField_1(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CHasMaskU3Ek__BackingField_3(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CLengthU3Ek__BackingField_4(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CMaskU3Ek__BackingField_5(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CDataU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (WebSocketFrameTypes_t3499449257)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4569[7] = 
{
	WebSocketFrameTypes_t3499449257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (OnWebSocketOpenDelegate_t4018547189), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (OnWebSocketMessageDelegate_t730459590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (OnWebSocketBinaryDelegate_t3919072826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (OnWebSocketClosedDelegate_t2679686585), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (OnWebSocketErrorDelegate_t1328789641), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (OnWebSocketErrorDescriptionDelegate_t2599777013), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (OnWebSocketIncompleteFrameDelegate_t2035490966), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (WebSocket_t71448861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4577[13] = 
{
	WebSocket_t71448861::get_offset_of_U3CStartPingThreadU3Ek__BackingField_0(),
	WebSocket_t71448861::get_offset_of_U3CPingFrequencyU3Ek__BackingField_1(),
	WebSocket_t71448861::get_offset_of_U3CInternalRequestU3Ek__BackingField_2(),
	WebSocket_t71448861::get_offset_of_U3CExtensionsU3Ek__BackingField_3(),
	WebSocket_t71448861::get_offset_of_OnOpen_4(),
	WebSocket_t71448861::get_offset_of_OnMessage_5(),
	WebSocket_t71448861::get_offset_of_OnBinary_6(),
	WebSocket_t71448861::get_offset_of_OnClosed_7(),
	WebSocket_t71448861::get_offset_of_OnError_8(),
	WebSocket_t71448861::get_offset_of_OnErrorDesc_9(),
	WebSocket_t71448861::get_offset_of_OnIncompleteFrame_10(),
	WebSocket_t71448861::get_offset_of_requestSent_11(),
	WebSocket_t71448861::get_offset_of_webSocket_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (WebSocketResponse_t3376763264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4578[15] = 
{
	WebSocketResponse_t3376763264::get_offset_of_U3CWebSocketU3Ek__BackingField_25(),
	WebSocketResponse_t3376763264::get_offset_of_OnText_26(),
	WebSocketResponse_t3376763264::get_offset_of_OnBinary_27(),
	WebSocketResponse_t3376763264::get_offset_of_OnIncompleteFrame_28(),
	WebSocketResponse_t3376763264::get_offset_of_OnClosed_29(),
	WebSocketResponse_t3376763264::get_offset_of_U3CPingFrequnecyU3Ek__BackingField_30(),
	WebSocketResponse_t3376763264::get_offset_of_U3CMaxFragmentSizeU3Ek__BackingField_31(),
	WebSocketResponse_t3376763264::get_offset_of_IncompleteFrames_32(),
	WebSocketResponse_t3376763264::get_offset_of_CompletedFrames_33(),
	WebSocketResponse_t3376763264::get_offset_of_CloseFrame_34(),
	WebSocketResponse_t3376763264::get_offset_of_FrameLock_35(),
	WebSocketResponse_t3376763264::get_offset_of_SendLock_36(),
	WebSocketResponse_t3376763264::get_offset_of_closeSent_37(),
	WebSocketResponse_t3376763264::get_offset_of_closed_38(),
	WebSocketResponse_t3376763264::get_offset_of_lastPing_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (WebSocketStausCodes_t2332119393)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4579[14] = 
{
	WebSocketStausCodes_t2332119393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (AssetBundleSample_t4142830892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4580[5] = 
{
	0,
	AssetBundleSample_t4142830892::get_offset_of_status_3(),
	AssetBundleSample_t4142830892::get_offset_of_cachedBundle_4(),
	AssetBundleSample_t4142830892::get_offset_of_texture_5(),
	AssetBundleSample_t4142830892::get_offset_of_downloading_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4581[7] = 
{
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U3CrequestU3E__0_0(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24locvar0_1(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U3CasyncU3E__1_2(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24this_3(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24current_4(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24disposing_5(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (U3CProcessAssetBundleU3Ec__Iterator1_t3058815025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4582[6] = 
{
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_bundle_0(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U3CasyncAssetU3E__0_1(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24this_2(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24current_3(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24disposing_4(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (LargeFileDownloadSample_t3427736475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4583[5] = 
{
	0,
	LargeFileDownloadSample_t3427736475::get_offset_of_request_3(),
	LargeFileDownloadSample_t3427736475::get_offset_of_status_4(),
	LargeFileDownloadSample_t3427736475::get_offset_of_progress_5(),
	LargeFileDownloadSample_t3427736475::get_offset_of_fragmentSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (TextureDownloadSample_t853801945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4584[6] = 
{
	0,
	TextureDownloadSample_t853801945::get_offset_of_Images_3(),
	TextureDownloadSample_t853801945::get_offset_of_Textures_4(),
	TextureDownloadSample_t853801945::get_offset_of_allDownloadedFromLocalCache_5(),
	TextureDownloadSample_t853801945::get_offset_of_finishedCount_6(),
	TextureDownloadSample_t853801945::get_offset_of_scrollPos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (UploadStream_t108391441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4585[6] = 
{
	UploadStream_t108391441::get_offset_of_ReadBuffer_2(),
	UploadStream_t108391441::get_offset_of_WriteBuffer_3(),
	UploadStream_t108391441::get_offset_of_noMoreData_4(),
	UploadStream_t108391441::get_offset_of_ARE_5(),
	UploadStream_t108391441::get_offset_of_locker_6(),
	UploadStream_t108391441::get_offset_of_U3CNameU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (CodeBlocks_t753507477), -1, sizeof(CodeBlocks_t753507477_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4586[12] = 
{
	CodeBlocks_t753507477_StaticFields::get_offset_of_TextureDownloadSample_0(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_WebSocketSample_1(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_AssetBundleSample_2(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_LargeFileDownloadSample_3(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SocketIOChatSample_4(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SocketIOWePlaySample_5(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_SimpleStreamingSample_6(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_ConnectionAPISample_7(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_ConnectionStatusSample_8(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_DemoHubSample_9(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_AuthenticationSample_10(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_CacheMaintenanceSample_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (GUIHelper_t1081137527), -1, sizeof(GUIHelper_t1081137527_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4587[3] = 
{
	GUIHelper_t1081137527_StaticFields::get_offset_of_centerAlignedLabel_0(),
	GUIHelper_t1081137527_StaticFields::get_offset_of_rightAlignedLabel_1(),
	GUIHelper_t1081137527_StaticFields::get_offset_of_ClientArea_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (GUIMessageList_t2719866248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4588[2] = 
{
	GUIMessageList_t2719866248::get_offset_of_messages_0(),
	GUIMessageList_t2719866248::get_offset_of_scrollPos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (JsonType_t3145703806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4589[9] = 
{
	JsonType_t3145703806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (JsonData_t269267574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4592[10] = 
{
	JsonData_t269267574::get_offset_of_inst_array_0(),
	JsonData_t269267574::get_offset_of_inst_boolean_1(),
	JsonData_t269267574::get_offset_of_inst_double_2(),
	JsonData_t269267574::get_offset_of_inst_int_3(),
	JsonData_t269267574::get_offset_of_inst_long_4(),
	JsonData_t269267574::get_offset_of_inst_object_5(),
	JsonData_t269267574::get_offset_of_inst_string_6(),
	JsonData_t269267574::get_offset_of_json_7(),
	JsonData_t269267574::get_offset_of_type_8(),
	JsonData_t269267574::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (OrderedDictionaryEnumerator_t3437478891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4593[1] = 
{
	OrderedDictionaryEnumerator_t3437478891::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (JsonException_t613047007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (PropertyMetadata_t3693826136)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4595[3] = 
{
	PropertyMetadata_t3693826136::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (ArrayMetadata_t2008834462)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4596[3] = 
{
	ArrayMetadata_t2008834462::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t2008834462::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t2008834462::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (ObjectMetadata_t3995922398)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4597[3] = 
{
	ObjectMetadata_t3995922398::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3995922398::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3995922398::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (ExporterFunc_t408878057), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
