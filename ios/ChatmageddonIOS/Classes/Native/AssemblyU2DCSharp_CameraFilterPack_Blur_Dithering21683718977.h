﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blur_Dithering2x2
struct  CameraFilterPack_Blur_Dithering2x2_t1683718977  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Blur_Dithering2x2::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Dithering2x2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Dithering2x2::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Dithering2x2::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Int32 CameraFilterPack_Blur_Dithering2x2::Level
	int32_t ___Level_6;
	// UnityEngine.Vector2 CameraFilterPack_Blur_Dithering2x2::Distance
	Vector2_t2243707579  ___Distance_7;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Level_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977, ___Level_6)); }
	inline int32_t get_Level_6() const { return ___Level_6; }
	inline int32_t* get_address_of_Level_6() { return &___Level_6; }
	inline void set_Level_6(int32_t value)
	{
		___Level_6 = value;
	}

	inline static int32_t get_offset_of_Distance_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977, ___Distance_7)); }
	inline Vector2_t2243707579  get_Distance_7() const { return ___Distance_7; }
	inline Vector2_t2243707579 * get_address_of_Distance_7() { return &___Distance_7; }
	inline void set_Distance_7(Vector2_t2243707579  value)
	{
		___Distance_7 = value;
	}
};

struct CameraFilterPack_Blur_Dithering2x2_t1683718977_StaticFields
{
public:
	// System.Int32 CameraFilterPack_Blur_Dithering2x2::ChangeLevel
	int32_t ___ChangeLevel_8;
	// UnityEngine.Vector2 CameraFilterPack_Blur_Dithering2x2::ChangeDistance
	Vector2_t2243707579  ___ChangeDistance_9;

public:
	inline static int32_t get_offset_of_ChangeLevel_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977_StaticFields, ___ChangeLevel_8)); }
	inline int32_t get_ChangeLevel_8() const { return ___ChangeLevel_8; }
	inline int32_t* get_address_of_ChangeLevel_8() { return &___ChangeLevel_8; }
	inline void set_ChangeLevel_8(int32_t value)
	{
		___ChangeLevel_8 = value;
	}

	inline static int32_t get_offset_of_ChangeDistance_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Dithering2x2_t1683718977_StaticFields, ___ChangeDistance_9)); }
	inline Vector2_t2243707579  get_ChangeDistance_9() const { return ___ChangeDistance_9; }
	inline Vector2_t2243707579 * get_address_of_ChangeDistance_9() { return &___ChangeDistance_9; }
	inline void set_ChangeDistance_9(Vector2_t2243707579  value)
	{
		___ChangeDistance_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
