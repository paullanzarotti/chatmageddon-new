﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// SettingsHomeScreenList
struct SettingsHomeScreenList_t2265775114;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeNavScreen
struct  HomeNavScreen_t3588214316  : public NavigationScreen_t2333230110
{
public:
	// TweenPosition HomeNavScreen::tableTween
	TweenPosition_t1144714832 * ___tableTween_3;
	// SettingsHomeScreenList HomeNavScreen::profileList
	SettingsHomeScreenList_t2265775114 * ___profileList_4;
	// System.Boolean HomeNavScreen::UIclosing
	bool ___UIclosing_5;
	// System.Boolean HomeNavScreen::settingsUIclosing
	bool ___settingsUIclosing_6;

public:
	inline static int32_t get_offset_of_tableTween_3() { return static_cast<int32_t>(offsetof(HomeNavScreen_t3588214316, ___tableTween_3)); }
	inline TweenPosition_t1144714832 * get_tableTween_3() const { return ___tableTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_tableTween_3() { return &___tableTween_3; }
	inline void set_tableTween_3(TweenPosition_t1144714832 * value)
	{
		___tableTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___tableTween_3, value);
	}

	inline static int32_t get_offset_of_profileList_4() { return static_cast<int32_t>(offsetof(HomeNavScreen_t3588214316, ___profileList_4)); }
	inline SettingsHomeScreenList_t2265775114 * get_profileList_4() const { return ___profileList_4; }
	inline SettingsHomeScreenList_t2265775114 ** get_address_of_profileList_4() { return &___profileList_4; }
	inline void set_profileList_4(SettingsHomeScreenList_t2265775114 * value)
	{
		___profileList_4 = value;
		Il2CppCodeGenWriteBarrier(&___profileList_4, value);
	}

	inline static int32_t get_offset_of_UIclosing_5() { return static_cast<int32_t>(offsetof(HomeNavScreen_t3588214316, ___UIclosing_5)); }
	inline bool get_UIclosing_5() const { return ___UIclosing_5; }
	inline bool* get_address_of_UIclosing_5() { return &___UIclosing_5; }
	inline void set_UIclosing_5(bool value)
	{
		___UIclosing_5 = value;
	}

	inline static int32_t get_offset_of_settingsUIclosing_6() { return static_cast<int32_t>(offsetof(HomeNavScreen_t3588214316, ___settingsUIclosing_6)); }
	inline bool get_settingsUIclosing_6() const { return ___settingsUIclosing_6; }
	inline bool* get_address_of_settingsUIclosing_6() { return &___settingsUIclosing_6; }
	inline void set_settingsUIclosing_6(bool value)
	{
		___settingsUIclosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
