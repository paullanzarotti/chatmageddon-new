﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoNetworkUIScaler
struct NoNetworkUIScaler_t2051622931;

#include "codegen/il2cpp-codegen.h"

// System.Void NoNetworkUIScaler::.ctor()
extern "C"  void NoNetworkUIScaler__ctor_m6837156 (NoNetworkUIScaler_t2051622931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUIScaler::GlobalUIScale()
extern "C"  void NoNetworkUIScaler_GlobalUIScale_m2917909249 (NoNetworkUIScaler_t2051622931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
