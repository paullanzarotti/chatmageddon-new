﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumberOfflineGeocoder
struct PhoneNumberOfflineGeocoder_t3035616292;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// PhoneNumbers.MappingFileProvider
struct MappingFileProvider_t2052216305;
// System.Collections.Generic.Dictionary`2<System.String,PhoneNumbers.AreaCodeMap>
struct Dictionary_2_t850571338;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberOfflineGeocoder
struct  PhoneNumberOfflineGeocoder_t3035616292  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.PhoneNumberOfflineGeocoder::phoneUtil
	PhoneNumberUtil_t4155573397 * ___phoneUtil_3;
	// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::phonePrefixDataDirectory
	String_t* ___phonePrefixDataDirectory_4;
	// PhoneNumbers.MappingFileProvider PhoneNumbers.PhoneNumberOfflineGeocoder::mappingFileProvider
	MappingFileProvider_t2052216305 * ___mappingFileProvider_5;
	// System.Collections.Generic.Dictionary`2<System.String,PhoneNumbers.AreaCodeMap> PhoneNumbers.PhoneNumberOfflineGeocoder::availablePhonePrefixMaps
	Dictionary_2_t850571338 * ___availablePhonePrefixMaps_6;

public:
	inline static int32_t get_offset_of_phoneUtil_3() { return static_cast<int32_t>(offsetof(PhoneNumberOfflineGeocoder_t3035616292, ___phoneUtil_3)); }
	inline PhoneNumberUtil_t4155573397 * get_phoneUtil_3() const { return ___phoneUtil_3; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_phoneUtil_3() { return &___phoneUtil_3; }
	inline void set_phoneUtil_3(PhoneNumberUtil_t4155573397 * value)
	{
		___phoneUtil_3 = value;
		Il2CppCodeGenWriteBarrier(&___phoneUtil_3, value);
	}

	inline static int32_t get_offset_of_phonePrefixDataDirectory_4() { return static_cast<int32_t>(offsetof(PhoneNumberOfflineGeocoder_t3035616292, ___phonePrefixDataDirectory_4)); }
	inline String_t* get_phonePrefixDataDirectory_4() const { return ___phonePrefixDataDirectory_4; }
	inline String_t** get_address_of_phonePrefixDataDirectory_4() { return &___phonePrefixDataDirectory_4; }
	inline void set_phonePrefixDataDirectory_4(String_t* value)
	{
		___phonePrefixDataDirectory_4 = value;
		Il2CppCodeGenWriteBarrier(&___phonePrefixDataDirectory_4, value);
	}

	inline static int32_t get_offset_of_mappingFileProvider_5() { return static_cast<int32_t>(offsetof(PhoneNumberOfflineGeocoder_t3035616292, ___mappingFileProvider_5)); }
	inline MappingFileProvider_t2052216305 * get_mappingFileProvider_5() const { return ___mappingFileProvider_5; }
	inline MappingFileProvider_t2052216305 ** get_address_of_mappingFileProvider_5() { return &___mappingFileProvider_5; }
	inline void set_mappingFileProvider_5(MappingFileProvider_t2052216305 * value)
	{
		___mappingFileProvider_5 = value;
		Il2CppCodeGenWriteBarrier(&___mappingFileProvider_5, value);
	}

	inline static int32_t get_offset_of_availablePhonePrefixMaps_6() { return static_cast<int32_t>(offsetof(PhoneNumberOfflineGeocoder_t3035616292, ___availablePhonePrefixMaps_6)); }
	inline Dictionary_2_t850571338 * get_availablePhonePrefixMaps_6() const { return ___availablePhonePrefixMaps_6; }
	inline Dictionary_2_t850571338 ** get_address_of_availablePhonePrefixMaps_6() { return &___availablePhonePrefixMaps_6; }
	inline void set_availablePhonePrefixMaps_6(Dictionary_2_t850571338 * value)
	{
		___availablePhonePrefixMaps_6 = value;
		Il2CppCodeGenWriteBarrier(&___availablePhonePrefixMaps_6, value);
	}
};

struct PhoneNumberOfflineGeocoder_t3035616292_StaticFields
{
public:
	// PhoneNumbers.PhoneNumberOfflineGeocoder PhoneNumbers.PhoneNumberOfflineGeocoder::instance
	PhoneNumberOfflineGeocoder_t3035616292 * ___instance_0;
	// System.Object PhoneNumbers.PhoneNumberOfflineGeocoder::thisLock
	Il2CppObject * ___thisLock_2;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(PhoneNumberOfflineGeocoder_t3035616292_StaticFields, ___instance_0)); }
	inline PhoneNumberOfflineGeocoder_t3035616292 * get_instance_0() const { return ___instance_0; }
	inline PhoneNumberOfflineGeocoder_t3035616292 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(PhoneNumberOfflineGeocoder_t3035616292 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}

	inline static int32_t get_offset_of_thisLock_2() { return static_cast<int32_t>(offsetof(PhoneNumberOfflineGeocoder_t3035616292_StaticFields, ___thisLock_2)); }
	inline Il2CppObject * get_thisLock_2() const { return ___thisLock_2; }
	inline Il2CppObject ** get_address_of_thisLock_2() { return &___thisLock_2; }
	inline void set_thisLock_2(Il2CppObject * value)
	{
		___thisLock_2 = value;
		Il2CppCodeGenWriteBarrier(&___thisLock_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
