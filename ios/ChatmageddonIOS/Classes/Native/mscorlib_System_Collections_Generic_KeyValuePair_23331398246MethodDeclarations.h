﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23888080226MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,UnityEngine.Vector3>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m959738872(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3331398246 *, GameObject_t1756533147 *, Vector3_t2243707580 , const MethodInfo*))KeyValuePair_2__ctor_m1541431460_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,UnityEngine.Vector3>::get_Key()
#define KeyValuePair_2_get_Key_m3327601698(__this, method) ((  GameObject_t1756533147 * (*) (KeyValuePair_2_t3331398246 *, const MethodInfo*))KeyValuePair_2_get_Key_m11844622_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,UnityEngine.Vector3>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m945393641(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3331398246 *, GameObject_t1756533147 *, const MethodInfo*))KeyValuePair_2_set_Key_m1751767387_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,UnityEngine.Vector3>::get_Value()
#define KeyValuePair_2_get_Value_m887599658(__this, method) ((  Vector3_t2243707580  (*) (KeyValuePair_2_t3331398246 *, const MethodInfo*))KeyValuePair_2_get_Value_m3480897294_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,UnityEngine.Vector3>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3647987201(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3331398246 *, Vector3_t2243707580 , const MethodInfo*))KeyValuePair_2_set_Value_m1557602963_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,UnityEngine.Vector3>::ToString()
#define KeyValuePair_2_ToString_m3682788695(__this, method) ((  String_t* (*) (KeyValuePair_2_t3331398246 *, const MethodInfo*))KeyValuePair_2_ToString_m690127133_gshared)(__this, method)
