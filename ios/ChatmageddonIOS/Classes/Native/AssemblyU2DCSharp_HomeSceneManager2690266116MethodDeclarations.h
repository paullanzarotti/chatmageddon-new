﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeSceneManager
struct HomeSceneManager_t2690266116;
// Friend
struct Friend_t3555014108;
// Missile
struct Missile_t813944928;
// Shield
struct Shield_t3327121081;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Scene4200804392.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"

// System.Void HomeSceneManager::.ctor()
extern "C"  void HomeSceneManager__ctor_m1840864997 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::InitScene()
extern "C"  void HomeSceneManager_InitScene_m4190097605 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeSceneManager::CheckLockoutState(System.Boolean)
extern "C"  bool HomeSceneManager_CheckLockoutState_m2761069386 (HomeSceneManager_t2690266116 * __this, bool ___instantLoad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::LoadSceneLoader(System.Boolean)
extern "C"  void HomeSceneManager_LoadSceneLoader_m344372247 (HomeSceneManager_t2690266116 * __this, bool ___unload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::StartSceneUnLoader()
extern "C"  void HomeSceneManager_StartSceneUnLoader_m4120669093 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::StartSceneLoader(Scene)
extern "C"  void HomeSceneManager_StartSceneLoader_m1905765146 (HomeSceneManager_t2690266116 * __this, int32_t ___sceneToLoad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::StartChatLoader(ChatNavScreen,Friend)
extern "C"  void HomeSceneManager_StartChatLoader_m3897767363 (HomeSceneManager_t2690266116 * __this, int32_t ___startScreen0, Friend_t3555014108 * ___friend1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ResetAttckStarts()
extern "C"  void HomeSceneManager_ResetAttckStarts_m2748889142 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeSceneManager::CheckAttackStarts()
extern "C"  bool HomeSceneManager_CheckAttackStarts_m2436449116 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ActivateAttackPanel(AttackNavScreen,Missile,Friend)
extern "C"  void HomeSceneManager_ActivateAttackPanel_m2452672505 (HomeSceneManager_t2690266116 * __this, int32_t ___startScreen0, Missile_t813944928 * ___weapon1, Friend_t3555014108 * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ActivateMissileInventoryPanel(Missile)
extern "C"  void HomeSceneManager_ActivateMissileInventoryPanel_m3662444692 (HomeSceneManager_t2690266116 * __this, Missile_t813944928 * ___missileToOpen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ActivateShieldItemInventoryPanel(Shield)
extern "C"  void HomeSceneManager_ActivateShieldItemInventoryPanel_m2962348437 (HomeSceneManager_t2690266116 * __this, Shield_t3327121081 * ___shieldToOpen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ActivateShieldInventoryPanel()
extern "C"  void HomeSceneManager_ActivateShieldInventoryPanel_m3942949375 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ActivateMineInventoryPanel()
extern "C"  void HomeSceneManager_ActivateMineInventoryPanel_m290035715 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::ActivateFuelInventoryPanel()
extern "C"  void HomeSceneManager_ActivateFuelInventoryPanel_m3724612420 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::OpenInventoryPanel()
extern "C"  void HomeSceneManager_OpenInventoryPanel_m3575985673 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeSceneManager::UnloadScene()
extern "C"  void HomeSceneManager_UnloadScene_m2337716786 (HomeSceneManager_t2690266116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
