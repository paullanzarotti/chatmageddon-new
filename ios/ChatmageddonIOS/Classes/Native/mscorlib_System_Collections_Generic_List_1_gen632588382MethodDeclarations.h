﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PhoneNumberNavScreen>
struct List_1_t632588382;
// System.Collections.Generic.IEnumerable`1<PhoneNumberNavScreen>
struct IEnumerable_1_t1555594295;
// PhoneNumberNavScreen[]
struct PhoneNumberNavScreenU5BU5D_t4021408007;
// System.Collections.Generic.IEnumerator`1<PhoneNumberNavScreen>
struct IEnumerator_1_t3033958373;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<PhoneNumberNavScreen>
struct ICollection_1_t2215542555;
// System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>
struct ReadOnlyCollection_1_t1449252942;
// System.Predicate`1<PhoneNumberNavScreen>
struct Predicate_1_t4001404661;
// System.Action`1<PhoneNumberNavScreen>
struct Action_1_t1065266632;
// System.Collections.Generic.IComparer`1<PhoneNumberNavScreen>
struct IComparer_1_t3512897668;
// System.Comparison`1<PhoneNumberNavScreen>
struct Comparison_1_t2525206101;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat167318056.h"

// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::.ctor()
extern "C"  void List_1__ctor_m1663504188_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1__ctor_m1663504188(__this, method) ((  void (*) (List_1_t632588382 *, const MethodInfo*))List_1__ctor_m1663504188_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2823753071_gshared (List_1_t632588382 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2823753071(__this, ___collection0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2823753071_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m273821509_gshared (List_1_t632588382 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m273821509(__this, ___capacity0, method) ((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1__ctor_m273821509_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2986755299_gshared (List_1_t632588382 * __this, PhoneNumberNavScreenU5BU5D_t4021408007* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2986755299(__this, ___data0, ___size1, method) ((  void (*) (List_1_t632588382 *, PhoneNumberNavScreenU5BU5D_t4021408007*, int32_t, const MethodInfo*))List_1__ctor_m2986755299_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::.cctor()
extern "C"  void List_1__cctor_m818426207_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m818426207(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m818426207_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2320271774_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2320271774(__this, method) ((  Il2CppObject* (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2320271774_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2227799604_gshared (List_1_t632588382 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2227799604(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t632588382 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2227799604_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3490608041_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3490608041(__this, method) ((  Il2CppObject * (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3490608041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m29428304_gshared (List_1_t632588382 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m29428304(__this, ___item0, method) ((  int32_t (*) (List_1_t632588382 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m29428304_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m652839066_gshared (List_1_t632588382 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m652839066(__this, ___item0, method) ((  bool (*) (List_1_t632588382 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m652839066_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2914749562_gshared (List_1_t632588382 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2914749562(__this, ___item0, method) ((  int32_t (*) (List_1_t632588382 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2914749562_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3066228805_gshared (List_1_t632588382 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3066228805(__this, ___index0, ___item1, method) ((  void (*) (List_1_t632588382 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3066228805_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m607648789_gshared (List_1_t632588382 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m607648789(__this, ___item0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m607648789_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2406855513_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2406855513(__this, method) ((  bool (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2406855513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3990184532_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3990184532(__this, method) ((  bool (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3990184532_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3502375130_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3502375130(__this, method) ((  Il2CppObject * (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3502375130_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m359026185_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m359026185(__this, method) ((  bool (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m359026185_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2040912072_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2040912072(__this, method) ((  bool (*) (List_1_t632588382 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2040912072_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1153791213_gshared (List_1_t632588382 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1153791213(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1153791213_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1385008278_gshared (List_1_t632588382 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1385008278(__this, ___index0, ___value1, method) ((  void (*) (List_1_t632588382 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1385008278_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Add(T)
extern "C"  void List_1_Add_m3949620113_gshared (List_1_t632588382 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3949620113(__this, ___item0, method) ((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_Add_m3949620113_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m205732414_gshared (List_1_t632588382 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m205732414(__this, ___newCount0, method) ((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m205732414_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2436731005_gshared (List_1_t632588382 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2436731005(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t632588382 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2436731005_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m465819638_gshared (List_1_t632588382 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m465819638(__this, ___collection0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m465819638_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m869190646_gshared (List_1_t632588382 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m869190646(__this, ___enumerable0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m869190646_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1320807157_gshared (List_1_t632588382 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1320807157(__this, ___collection0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1320807157_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PhoneNumberNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1449252942 * List_1_AsReadOnly_m2717969208_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2717969208(__this, method) ((  ReadOnlyCollection_1_t1449252942 * (*) (List_1_t632588382 *, const MethodInfo*))List_1_AsReadOnly_m2717969208_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Clear()
extern "C"  void List_1_Clear_m4095615677_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_Clear_m4095615677(__this, method) ((  void (*) (List_1_t632588382 *, const MethodInfo*))List_1_Clear_m4095615677_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m3979285255_gshared (List_1_t632588382 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3979285255(__this, ___item0, method) ((  bool (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_Contains_m3979285255_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m4017818868_gshared (List_1_t632588382 * __this, PhoneNumberNavScreenU5BU5D_t4021408007* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m4017818868(__this, ___array0, method) ((  void (*) (List_1_t632588382 *, PhoneNumberNavScreenU5BU5D_t4021408007*, const MethodInfo*))List_1_CopyTo_m4017818868_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1953863961_gshared (List_1_t632588382 * __this, PhoneNumberNavScreenU5BU5D_t4021408007* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1953863961(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t632588382 *, PhoneNumberNavScreenU5BU5D_t4021408007*, int32_t, const MethodInfo*))List_1_CopyTo_m1953863961_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m2999824665_gshared (List_1_t632588382 * __this, int32_t ___index0, PhoneNumberNavScreenU5BU5D_t4021408007* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m2999824665(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t632588382 *, int32_t, PhoneNumberNavScreenU5BU5D_t4021408007*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m2999824665_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m2369475761_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_Exists_m2369475761(__this, ___match0, method) ((  bool (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_Exists_m2369475761_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<PhoneNumberNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m789202695_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_Find_m789202695(__this, ___match0, method) ((  int32_t (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_Find_m789202695_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1427490164_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1427490164(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4001404661 *, const MethodInfo*))List_1_CheckMatch_m1427490164_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<PhoneNumberNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t632588382 * List_1_FindAll_m493566604_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m493566604(__this, ___match0, method) ((  List_1_t632588382 * (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_FindAll_m493566604_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<PhoneNumberNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t632588382 * List_1_FindAllStackBits_m2404891300_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m2404891300(__this, ___match0, method) ((  List_1_t632588382 * (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_FindAllStackBits_m2404891300_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<PhoneNumberNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t632588382 * List_1_FindAllList_m1655845220_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m1655845220(__this, ___match0, method) ((  List_1_t632588382 * (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_FindAllList_m1655845220_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m2880841562_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m2880841562(__this, ___match0, method) ((  int32_t (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_FindIndex_m2880841562_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m624809295_gshared (List_1_t632588382 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4001404661 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m624809295(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t632588382 *, int32_t, int32_t, Predicate_1_t4001404661 *, const MethodInfo*))List_1_GetIndex_m624809295_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m487763252_gshared (List_1_t632588382 * __this, Action_1_t1065266632 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m487763252(__this, ___action0, method) ((  void (*) (List_1_t632588382 *, Action_1_t1065266632 *, const MethodInfo*))List_1_ForEach_m487763252_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PhoneNumberNavScreen>::GetEnumerator()
extern "C"  Enumerator_t167318056  List_1_GetEnumerator_m892716320_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m892716320(__this, method) ((  Enumerator_t167318056  (*) (List_1_t632588382 *, const MethodInfo*))List_1_GetEnumerator_m892716320_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m859757377_gshared (List_1_t632588382 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m859757377(__this, ___item0, method) ((  int32_t (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_IndexOf_m859757377_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3802779984_gshared (List_1_t632588382 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3802779984(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t632588382 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3802779984_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m970170445_gshared (List_1_t632588382 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m970170445(__this, ___index0, method) ((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_CheckIndex_m970170445_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1607690286_gshared (List_1_t632588382 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1607690286(__this, ___index0, ___item1, method) ((  void (*) (List_1_t632588382 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1607690286_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1887673523_gshared (List_1_t632588382 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1887673523(__this, ___collection0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1887673523_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PhoneNumberNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3829179688_gshared (List_1_t632588382 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3829179688(__this, ___item0, method) ((  bool (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_Remove_m3829179688_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1120629336_gshared (List_1_t632588382 * __this, Predicate_1_t4001404661 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1120629336(__this, ___match0, method) ((  int32_t (*) (List_1_t632588382 *, Predicate_1_t4001404661 *, const MethodInfo*))List_1_RemoveAll_m1120629336_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3863597770_gshared (List_1_t632588382 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3863597770(__this, ___index0, method) ((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3863597770_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m4133842145_gshared (List_1_t632588382 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m4133842145(__this, ___index0, ___count1, method) ((  void (*) (List_1_t632588382 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m4133842145_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m3843345434_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_Reverse_m3843345434(__this, method) ((  void (*) (List_1_t632588382 *, const MethodInfo*))List_1_Reverse_m3843345434_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Sort()
extern "C"  void List_1_Sort_m1823740746_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_Sort_m1823740746(__this, method) ((  void (*) (List_1_t632588382 *, const MethodInfo*))List_1_Sort_m1823740746_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2580988030_gshared (List_1_t632588382 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2580988030(__this, ___comparer0, method) ((  void (*) (List_1_t632588382 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2580988030_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m236251095_gshared (List_1_t632588382 * __this, Comparison_1_t2525206101 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m236251095(__this, ___comparison0, method) ((  void (*) (List_1_t632588382 *, Comparison_1_t2525206101 *, const MethodInfo*))List_1_Sort_m236251095_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PhoneNumberNavScreen>::ToArray()
extern "C"  PhoneNumberNavScreenU5BU5D_t4021408007* List_1_ToArray_m1353072443_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_ToArray_m1353072443(__this, method) ((  PhoneNumberNavScreenU5BU5D_t4021408007* (*) (List_1_t632588382 *, const MethodInfo*))List_1_ToArray_m1353072443_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2721901105_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2721901105(__this, method) ((  void (*) (List_1_t632588382 *, const MethodInfo*))List_1_TrimExcess_m2721901105_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2120643951_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2120643951(__this, method) ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))List_1_get_Capacity_m2120643951_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2114955446_gshared (List_1_t632588382 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2114955446(__this, ___value0, method) ((  void (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2114955446_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PhoneNumberNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m490809424_gshared (List_1_t632588382 * __this, const MethodInfo* method);
#define List_1_get_Count_m490809424(__this, method) ((  int32_t (*) (List_1_t632588382 *, const MethodInfo*))List_1_get_Count_m490809424_gshared)(__this, method)
// T System.Collections.Generic.List`1<PhoneNumberNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2414238214_gshared (List_1_t632588382 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2414238214(__this, ___index0, method) ((  int32_t (*) (List_1_t632588382 *, int32_t, const MethodInfo*))List_1_get_Item_m2414238214_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PhoneNumberNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1579922801_gshared (List_1_t632588382 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1579922801(__this, ___index0, ___value1, method) ((  void (*) (List_1_t632588382 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1579922801_gshared)(__this, ___index0, ___value1, method)
