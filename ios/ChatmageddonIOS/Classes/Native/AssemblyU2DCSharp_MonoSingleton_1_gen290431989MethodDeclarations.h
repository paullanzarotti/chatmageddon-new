﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<RegistrationNavigationcontroller>::.ctor()
#define MonoSingleton_1__ctor_m621762279(__this, method) ((  void (*) (MonoSingleton_1_t290431989 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<RegistrationNavigationcontroller>::Awake()
#define MonoSingleton_1_Awake_m1799514568(__this, method) ((  void (*) (MonoSingleton_1_t290431989 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<RegistrationNavigationcontroller>::get_Instance()
#define MonoSingleton_1_get_Instance_m2660067994(__this /* static, unused */, method) ((  RegistrationNavigationcontroller_t539766269 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<RegistrationNavigationcontroller>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m817330426(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<RegistrationNavigationcontroller>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3611851813(__this, method) ((  void (*) (MonoSingleton_1_t290431989 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<RegistrationNavigationcontroller>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1162822501(__this, method) ((  void (*) (MonoSingleton_1_t290431989 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<RegistrationNavigationcontroller>::.cctor()
#define MonoSingleton_1__cctor_m1106542146(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
