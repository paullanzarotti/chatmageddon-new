﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_p1426196141.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_NightVisionFX
struct  CameraFilterPack_NightVisionFX_t1351189223  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_NightVisionFX::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// CameraFilterPack_NightVisionFX/preset CameraFilterPack_NightVisionFX::Preset
	int32_t ___Preset_3;
	// CameraFilterPack_NightVisionFX/preset CameraFilterPack_NightVisionFX::PresetMemo
	int32_t ___PresetMemo_4;
	// System.Single CameraFilterPack_NightVisionFX::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_NightVisionFX::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_NightVisionFX::SCMaterial
	Material_t193706927 * ___SCMaterial_7;
	// System.Single CameraFilterPack_NightVisionFX::Greenness
	float ___Greenness_8;
	// System.Single CameraFilterPack_NightVisionFX::Vignette
	float ___Vignette_9;
	// System.Single CameraFilterPack_NightVisionFX::Vignette_Alpha
	float ___Vignette_Alpha_10;
	// System.Single CameraFilterPack_NightVisionFX::Distortion
	float ___Distortion_11;
	// System.Single CameraFilterPack_NightVisionFX::Noise
	float ___Noise_12;
	// System.Single CameraFilterPack_NightVisionFX::Intensity
	float ___Intensity_13;
	// System.Single CameraFilterPack_NightVisionFX::Light
	float ___Light_14;
	// System.Single CameraFilterPack_NightVisionFX::Light2
	float ___Light2_15;
	// System.Single CameraFilterPack_NightVisionFX::Line
	float ___Line_16;
	// System.Single CameraFilterPack_NightVisionFX::Color_R
	float ___Color_R_17;
	// System.Single CameraFilterPack_NightVisionFX::Color_G
	float ___Color_G_18;
	// System.Single CameraFilterPack_NightVisionFX::Color_B
	float ___Color_B_19;
	// System.Single CameraFilterPack_NightVisionFX::_Binocular_Size
	float ____Binocular_Size_20;
	// System.Single CameraFilterPack_NightVisionFX::_Binocular_Smooth
	float ____Binocular_Smooth_21;
	// System.Single CameraFilterPack_NightVisionFX::_Binocular_Dist
	float ____Binocular_Dist_22;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_Preset_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Preset_3)); }
	inline int32_t get_Preset_3() const { return ___Preset_3; }
	inline int32_t* get_address_of_Preset_3() { return &___Preset_3; }
	inline void set_Preset_3(int32_t value)
	{
		___Preset_3 = value;
	}

	inline static int32_t get_offset_of_PresetMemo_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___PresetMemo_4)); }
	inline int32_t get_PresetMemo_4() const { return ___PresetMemo_4; }
	inline int32_t* get_address_of_PresetMemo_4() { return &___PresetMemo_4; }
	inline void set_PresetMemo_4(int32_t value)
	{
		___PresetMemo_4 = value;
	}

	inline static int32_t get_offset_of_TimeX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___TimeX_5)); }
	inline float get_TimeX_5() const { return ___TimeX_5; }
	inline float* get_address_of_TimeX_5() { return &___TimeX_5; }
	inline void set_TimeX_5(float value)
	{
		___TimeX_5 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___ScreenResolution_6)); }
	inline Vector4_t2243707581  get_ScreenResolution_6() const { return ___ScreenResolution_6; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_6() { return &___ScreenResolution_6; }
	inline void set_ScreenResolution_6(Vector4_t2243707581  value)
	{
		___ScreenResolution_6 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___SCMaterial_7)); }
	inline Material_t193706927 * get_SCMaterial_7() const { return ___SCMaterial_7; }
	inline Material_t193706927 ** get_address_of_SCMaterial_7() { return &___SCMaterial_7; }
	inline void set_SCMaterial_7(Material_t193706927 * value)
	{
		___SCMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_7, value);
	}

	inline static int32_t get_offset_of_Greenness_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Greenness_8)); }
	inline float get_Greenness_8() const { return ___Greenness_8; }
	inline float* get_address_of_Greenness_8() { return &___Greenness_8; }
	inline void set_Greenness_8(float value)
	{
		___Greenness_8 = value;
	}

	inline static int32_t get_offset_of_Vignette_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Vignette_9)); }
	inline float get_Vignette_9() const { return ___Vignette_9; }
	inline float* get_address_of_Vignette_9() { return &___Vignette_9; }
	inline void set_Vignette_9(float value)
	{
		___Vignette_9 = value;
	}

	inline static int32_t get_offset_of_Vignette_Alpha_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Vignette_Alpha_10)); }
	inline float get_Vignette_Alpha_10() const { return ___Vignette_Alpha_10; }
	inline float* get_address_of_Vignette_Alpha_10() { return &___Vignette_Alpha_10; }
	inline void set_Vignette_Alpha_10(float value)
	{
		___Vignette_Alpha_10 = value;
	}

	inline static int32_t get_offset_of_Distortion_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Distortion_11)); }
	inline float get_Distortion_11() const { return ___Distortion_11; }
	inline float* get_address_of_Distortion_11() { return &___Distortion_11; }
	inline void set_Distortion_11(float value)
	{
		___Distortion_11 = value;
	}

	inline static int32_t get_offset_of_Noise_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Noise_12)); }
	inline float get_Noise_12() const { return ___Noise_12; }
	inline float* get_address_of_Noise_12() { return &___Noise_12; }
	inline void set_Noise_12(float value)
	{
		___Noise_12 = value;
	}

	inline static int32_t get_offset_of_Intensity_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Intensity_13)); }
	inline float get_Intensity_13() const { return ___Intensity_13; }
	inline float* get_address_of_Intensity_13() { return &___Intensity_13; }
	inline void set_Intensity_13(float value)
	{
		___Intensity_13 = value;
	}

	inline static int32_t get_offset_of_Light_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Light_14)); }
	inline float get_Light_14() const { return ___Light_14; }
	inline float* get_address_of_Light_14() { return &___Light_14; }
	inline void set_Light_14(float value)
	{
		___Light_14 = value;
	}

	inline static int32_t get_offset_of_Light2_15() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Light2_15)); }
	inline float get_Light2_15() const { return ___Light2_15; }
	inline float* get_address_of_Light2_15() { return &___Light2_15; }
	inline void set_Light2_15(float value)
	{
		___Light2_15 = value;
	}

	inline static int32_t get_offset_of_Line_16() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Line_16)); }
	inline float get_Line_16() const { return ___Line_16; }
	inline float* get_address_of_Line_16() { return &___Line_16; }
	inline void set_Line_16(float value)
	{
		___Line_16 = value;
	}

	inline static int32_t get_offset_of_Color_R_17() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Color_R_17)); }
	inline float get_Color_R_17() const { return ___Color_R_17; }
	inline float* get_address_of_Color_R_17() { return &___Color_R_17; }
	inline void set_Color_R_17(float value)
	{
		___Color_R_17 = value;
	}

	inline static int32_t get_offset_of_Color_G_18() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Color_G_18)); }
	inline float get_Color_G_18() const { return ___Color_G_18; }
	inline float* get_address_of_Color_G_18() { return &___Color_G_18; }
	inline void set_Color_G_18(float value)
	{
		___Color_G_18 = value;
	}

	inline static int32_t get_offset_of_Color_B_19() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ___Color_B_19)); }
	inline float get_Color_B_19() const { return ___Color_B_19; }
	inline float* get_address_of_Color_B_19() { return &___Color_B_19; }
	inline void set_Color_B_19(float value)
	{
		___Color_B_19 = value;
	}

	inline static int32_t get_offset_of__Binocular_Size_20() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ____Binocular_Size_20)); }
	inline float get__Binocular_Size_20() const { return ____Binocular_Size_20; }
	inline float* get_address_of__Binocular_Size_20() { return &____Binocular_Size_20; }
	inline void set__Binocular_Size_20(float value)
	{
		____Binocular_Size_20 = value;
	}

	inline static int32_t get_offset_of__Binocular_Smooth_21() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ____Binocular_Smooth_21)); }
	inline float get__Binocular_Smooth_21() const { return ____Binocular_Smooth_21; }
	inline float* get_address_of__Binocular_Smooth_21() { return &____Binocular_Smooth_21; }
	inline void set__Binocular_Smooth_21(float value)
	{
		____Binocular_Smooth_21 = value;
	}

	inline static int32_t get_offset_of__Binocular_Dist_22() { return static_cast<int32_t>(offsetof(CameraFilterPack_NightVisionFX_t1351189223, ____Binocular_Dist_22)); }
	inline float get__Binocular_Dist_22() const { return ____Binocular_Dist_22; }
	inline float* get_address_of__Binocular_Dist_22() { return &____Binocular_Dist_22; }
	inline void set__Binocular_Dist_22(float value)
	{
		____Binocular_Dist_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
