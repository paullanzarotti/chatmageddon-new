﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AttackNavScreen>
struct DefaultComparer_t1658689620;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AttackNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2777759753_gshared (DefaultComparer_t1658689620 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2777759753(__this, method) ((  void (*) (DefaultComparer_t1658689620 *, const MethodInfo*))DefaultComparer__ctor_m2777759753_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AttackNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m293846416_gshared (DefaultComparer_t1658689620 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m293846416(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1658689620 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m293846416_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AttackNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1848590752_gshared (DefaultComparer_t1658689620 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1848590752(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1658689620 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1848590752_gshared)(__this, ___x0, ___y1, method)
