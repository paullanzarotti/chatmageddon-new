﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PanelType>
struct DefaultComparer_t2104699391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PanelType>::.ctor()
extern "C"  void DefaultComparer__ctor_m484214028_gshared (DefaultComparer_t2104699391 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m484214028(__this, method) ((  void (*) (DefaultComparer_t2104699391 *, const MethodInfo*))DefaultComparer__ctor_m484214028_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PanelType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m852729937_gshared (DefaultComparer_t2104699391 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m852729937(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2104699391 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m852729937_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PanelType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4029327665_gshared (DefaultComparer_t2104699391 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4029327665(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2104699391 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m4029327665_gshared)(__this, ___x0, ___y1, method)
