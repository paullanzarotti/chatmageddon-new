﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserAudioMusic>c__AnonStorey18
struct U3CUpdateUserAudioMusicU3Ec__AnonStorey18_t3809964204;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserAudioMusic>c__AnonStorey18::.ctor()
extern "C"  void U3CUpdateUserAudioMusicU3Ec__AnonStorey18__ctor_m3284623911 (U3CUpdateUserAudioMusicU3Ec__AnonStorey18_t3809964204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserAudioMusic>c__AnonStorey18::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserAudioMusicU3Ec__AnonStorey18_U3CU3Em__0_m2290795972 (U3CUpdateUserAudioMusicU3Ec__AnonStorey18_t3809964204 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
