﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3019169210MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<Unibill.Impl.HTTPClient/PostRequest>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3533785127(__this, ___q0, method) ((  void (*) (Enumerator_t167621649 *, Queue_1_t3952525865 *, const MethodInfo*))Enumerator__ctor_m677001007_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3073437690(__this, method) ((  void (*) (Enumerator_t167621649 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m373072478_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2627999260(__this, method) ((  Il2CppObject * (*) (Enumerator_t167621649 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2167685344_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Unibill.Impl.HTTPClient/PostRequest>::Dispose()
#define Enumerator_Dispose_m1100775665(__this, method) ((  void (*) (Enumerator_t167621649 *, const MethodInfo*))Enumerator_Dispose_m575349149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<Unibill.Impl.HTTPClient/PostRequest>::MoveNext()
#define Enumerator_MoveNext_m1551604170(__this, method) ((  bool (*) (Enumerator_t167621649 *, const MethodInfo*))Enumerator_MoveNext_m742418190_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<Unibill.Impl.HTTPClient/PostRequest>::get_Current()
#define Enumerator_get_Current_m1789359881(__this, method) ((  PostRequest_t4132869030 * (*) (Enumerator_t167621649 *, const MethodInfo*))Enumerator_get_Current_m1613610405_gshared)(__this, method)
