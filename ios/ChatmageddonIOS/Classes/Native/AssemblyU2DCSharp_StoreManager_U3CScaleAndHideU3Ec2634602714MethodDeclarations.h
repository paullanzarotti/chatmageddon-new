﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreManager/<ScaleAndHide>c__Iterator1
struct U3CScaleAndHideU3Ec__Iterator1_t2634602714;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StoreManager/<ScaleAndHide>c__Iterator1::.ctor()
extern "C"  void U3CScaleAndHideU3Ec__Iterator1__ctor_m3123453969 (U3CScaleAndHideU3Ec__Iterator1_t2634602714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StoreManager/<ScaleAndHide>c__Iterator1::MoveNext()
extern "C"  bool U3CScaleAndHideU3Ec__Iterator1_MoveNext_m4147890303 (U3CScaleAndHideU3Ec__Iterator1_t2634602714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<ScaleAndHide>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CScaleAndHideU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3200525547 (U3CScaleAndHideU3Ec__Iterator1_t2634602714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<ScaleAndHide>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CScaleAndHideU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1447449299 (U3CScaleAndHideU3Ec__Iterator1_t2634602714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<ScaleAndHide>c__Iterator1::Dispose()
extern "C"  void U3CScaleAndHideU3Ec__Iterator1_Dispose_m3159674196 (U3CScaleAndHideU3Ec__Iterator1_t2634602714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<ScaleAndHide>c__Iterator1::Reset()
extern "C"  void U3CScaleAndHideU3Ec__Iterator1_Reset_m1094595654 (U3CScaleAndHideU3Ec__Iterator1_t2634602714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
