﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blend2Camera_Blend
struct  CameraFilterPack_Blend2Camera_Blend_t3468651737  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Blend2Camera_Blend::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_Blend::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_Blend::Camera2
	Camera_t189460977 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_Blend::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_Blend2Camera_Blend::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_Blend::SCMaterial
	Material_t193706927 * ___SCMaterial_7;
	// System.Single CameraFilterPack_Blend2Camera_Blend::BlendFX
	float ___BlendFX_8;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_Blend::Camera2tex
	RenderTexture_t2666733923 * ___Camera2tex_10;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_Camera2_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___Camera2_4)); }
	inline Camera_t189460977 * get_Camera2_4() const { return ___Camera2_4; }
	inline Camera_t189460977 ** get_address_of_Camera2_4() { return &___Camera2_4; }
	inline void set_Camera2_4(Camera_t189460977 * value)
	{
		___Camera2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2_4, value);
	}

	inline static int32_t get_offset_of_TimeX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___TimeX_5)); }
	inline float get_TimeX_5() const { return ___TimeX_5; }
	inline float* get_address_of_TimeX_5() { return &___TimeX_5; }
	inline void set_TimeX_5(float value)
	{
		___TimeX_5 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___ScreenResolution_6)); }
	inline Vector4_t2243707581  get_ScreenResolution_6() const { return ___ScreenResolution_6; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_6() { return &___ScreenResolution_6; }
	inline void set_ScreenResolution_6(Vector4_t2243707581  value)
	{
		___ScreenResolution_6 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___SCMaterial_7)); }
	inline Material_t193706927 * get_SCMaterial_7() const { return ___SCMaterial_7; }
	inline Material_t193706927 ** get_address_of_SCMaterial_7() { return &___SCMaterial_7; }
	inline void set_SCMaterial_7(Material_t193706927 * value)
	{
		___SCMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_7, value);
	}

	inline static int32_t get_offset_of_BlendFX_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___BlendFX_8)); }
	inline float get_BlendFX_8() const { return ___BlendFX_8; }
	inline float* get_address_of_BlendFX_8() { return &___BlendFX_8; }
	inline void set_BlendFX_8(float value)
	{
		___BlendFX_8 = value;
	}

	inline static int32_t get_offset_of_Camera2tex_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737, ___Camera2tex_10)); }
	inline RenderTexture_t2666733923 * get_Camera2tex_10() const { return ___Camera2tex_10; }
	inline RenderTexture_t2666733923 ** get_address_of_Camera2tex_10() { return &___Camera2tex_10; }
	inline void set_Camera2tex_10(RenderTexture_t2666733923 * value)
	{
		___Camera2tex_10 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2tex_10, value);
	}
};

struct CameraFilterPack_Blend2Camera_Blend_t3468651737_StaticFields
{
public:
	// System.Single CameraFilterPack_Blend2Camera_Blend::ChangeValue
	float ___ChangeValue_9;

public:
	inline static int32_t get_offset_of_ChangeValue_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_Blend_t3468651737_StaticFields, ___ChangeValue_9)); }
	inline float get_ChangeValue_9() const { return ___ChangeValue_9; }
	inline float* get_address_of_ChangeValue_9() { return &___ChangeValue_9; }
	inline void set_ChangeValue_9(float value)
	{
		___ChangeValue_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
