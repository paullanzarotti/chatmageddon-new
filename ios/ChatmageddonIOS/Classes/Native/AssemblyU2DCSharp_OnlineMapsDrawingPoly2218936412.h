﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;

#include "AssemblyU2DCSharp_OnlineMapsDrawingElement539447654.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsDrawingPoly
struct  OnlineMapsDrawingPoly_t2218936412  : public OnlineMapsDrawingElement_t539447654
{
public:
	// UnityEngine.Color OnlineMapsDrawingPoly::backgroundColor
	Color_t2020392075  ___backgroundColor_21;
	// UnityEngine.Color OnlineMapsDrawingPoly::borderColor
	Color_t2020392075  ___borderColor_22;
	// System.Single OnlineMapsDrawingPoly::borderWeight
	float ___borderWeight_23;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> OnlineMapsDrawingPoly::points
	List_1_t1612828711 * ___points_24;

public:
	inline static int32_t get_offset_of_backgroundColor_21() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingPoly_t2218936412, ___backgroundColor_21)); }
	inline Color_t2020392075  get_backgroundColor_21() const { return ___backgroundColor_21; }
	inline Color_t2020392075 * get_address_of_backgroundColor_21() { return &___backgroundColor_21; }
	inline void set_backgroundColor_21(Color_t2020392075  value)
	{
		___backgroundColor_21 = value;
	}

	inline static int32_t get_offset_of_borderColor_22() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingPoly_t2218936412, ___borderColor_22)); }
	inline Color_t2020392075  get_borderColor_22() const { return ___borderColor_22; }
	inline Color_t2020392075 * get_address_of_borderColor_22() { return &___borderColor_22; }
	inline void set_borderColor_22(Color_t2020392075  value)
	{
		___borderColor_22 = value;
	}

	inline static int32_t get_offset_of_borderWeight_23() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingPoly_t2218936412, ___borderWeight_23)); }
	inline float get_borderWeight_23() const { return ___borderWeight_23; }
	inline float* get_address_of_borderWeight_23() { return &___borderWeight_23; }
	inline void set_borderWeight_23(float value)
	{
		___borderWeight_23 = value;
	}

	inline static int32_t get_offset_of_points_24() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingPoly_t2218936412, ___points_24)); }
	inline List_1_t1612828711 * get_points_24() const { return ___points_24; }
	inline List_1_t1612828711 ** get_address_of_points_24() { return &___points_24; }
	inline void set_points_24(List_1_t1612828711 * value)
	{
		___points_24 = value;
		Il2CppCodeGenWriteBarrier(&___points_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
