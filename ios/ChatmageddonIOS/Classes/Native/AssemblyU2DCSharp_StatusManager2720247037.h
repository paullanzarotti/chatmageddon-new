﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_MonoSingleton_1_gen2470912757.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusManager
struct  StatusManager_t2720247037  : public MonoSingleton_1_t2470912757
{
public:
	// System.Boolean StatusManager::incomingListUpdated
	bool ___incomingListUpdated_3;
	// System.Boolean StatusManager::outgoingListUpdated
	bool ___outgoingListUpdated_4;
	// UnityEngine.Color StatusManager::darkGreenColour
	Color_t2020392075  ___darkGreenColour_5;
	// UnityEngine.Color StatusManager::greenColour
	Color_t2020392075  ___greenColour_6;
	// UnityEngine.Color StatusManager::darkOrangeColour
	Color_t2020392075  ___darkOrangeColour_7;
	// UnityEngine.Color StatusManager::orangeColour
	Color_t2020392075  ___orangeColour_8;
	// UnityEngine.Color StatusManager::darkRedColour
	Color_t2020392075  ___darkRedColour_9;
	// UnityEngine.Color StatusManager::redColour
	Color_t2020392075  ___redColour_10;
	// UnityEngine.Color StatusManager::greyColour
	Color_t2020392075  ___greyColour_11;

public:
	inline static int32_t get_offset_of_incomingListUpdated_3() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___incomingListUpdated_3)); }
	inline bool get_incomingListUpdated_3() const { return ___incomingListUpdated_3; }
	inline bool* get_address_of_incomingListUpdated_3() { return &___incomingListUpdated_3; }
	inline void set_incomingListUpdated_3(bool value)
	{
		___incomingListUpdated_3 = value;
	}

	inline static int32_t get_offset_of_outgoingListUpdated_4() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___outgoingListUpdated_4)); }
	inline bool get_outgoingListUpdated_4() const { return ___outgoingListUpdated_4; }
	inline bool* get_address_of_outgoingListUpdated_4() { return &___outgoingListUpdated_4; }
	inline void set_outgoingListUpdated_4(bool value)
	{
		___outgoingListUpdated_4 = value;
	}

	inline static int32_t get_offset_of_darkGreenColour_5() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___darkGreenColour_5)); }
	inline Color_t2020392075  get_darkGreenColour_5() const { return ___darkGreenColour_5; }
	inline Color_t2020392075 * get_address_of_darkGreenColour_5() { return &___darkGreenColour_5; }
	inline void set_darkGreenColour_5(Color_t2020392075  value)
	{
		___darkGreenColour_5 = value;
	}

	inline static int32_t get_offset_of_greenColour_6() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___greenColour_6)); }
	inline Color_t2020392075  get_greenColour_6() const { return ___greenColour_6; }
	inline Color_t2020392075 * get_address_of_greenColour_6() { return &___greenColour_6; }
	inline void set_greenColour_6(Color_t2020392075  value)
	{
		___greenColour_6 = value;
	}

	inline static int32_t get_offset_of_darkOrangeColour_7() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___darkOrangeColour_7)); }
	inline Color_t2020392075  get_darkOrangeColour_7() const { return ___darkOrangeColour_7; }
	inline Color_t2020392075 * get_address_of_darkOrangeColour_7() { return &___darkOrangeColour_7; }
	inline void set_darkOrangeColour_7(Color_t2020392075  value)
	{
		___darkOrangeColour_7 = value;
	}

	inline static int32_t get_offset_of_orangeColour_8() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___orangeColour_8)); }
	inline Color_t2020392075  get_orangeColour_8() const { return ___orangeColour_8; }
	inline Color_t2020392075 * get_address_of_orangeColour_8() { return &___orangeColour_8; }
	inline void set_orangeColour_8(Color_t2020392075  value)
	{
		___orangeColour_8 = value;
	}

	inline static int32_t get_offset_of_darkRedColour_9() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___darkRedColour_9)); }
	inline Color_t2020392075  get_darkRedColour_9() const { return ___darkRedColour_9; }
	inline Color_t2020392075 * get_address_of_darkRedColour_9() { return &___darkRedColour_9; }
	inline void set_darkRedColour_9(Color_t2020392075  value)
	{
		___darkRedColour_9 = value;
	}

	inline static int32_t get_offset_of_redColour_10() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___redColour_10)); }
	inline Color_t2020392075  get_redColour_10() const { return ___redColour_10; }
	inline Color_t2020392075 * get_address_of_redColour_10() { return &___redColour_10; }
	inline void set_redColour_10(Color_t2020392075  value)
	{
		___redColour_10 = value;
	}

	inline static int32_t get_offset_of_greyColour_11() { return static_cast<int32_t>(offsetof(StatusManager_t2720247037, ___greyColour_11)); }
	inline Color_t2020392075  get_greyColour_11() const { return ___greyColour_11; }
	inline Color_t2020392075 * get_address_of_greyColour_11() { return &___greyColour_11; }
	inline void set_greyColour_11(Color_t2020392075  value)
	{
		___greyColour_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
