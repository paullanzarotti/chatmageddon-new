﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.AppleAppStoreBillingService
struct AppleAppStoreBillingService_t956296338;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.OSXStoreKitPluginImpl
struct  OSXStoreKitPluginImpl_t3020431434  : public Il2CppObject
{
public:

public:
};

struct OSXStoreKitPluginImpl_t3020431434_StaticFields
{
public:
	// Unibill.Impl.AppleAppStoreBillingService Unibill.Impl.OSXStoreKitPluginImpl::callback
	AppleAppStoreBillingService_t956296338 * ___callback_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Unibill.Impl.OSXStoreKitPluginImpl::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(OSXStoreKitPluginImpl_t3020431434_StaticFields, ___callback_0)); }
	inline AppleAppStoreBillingService_t956296338 * get_callback_0() const { return ___callback_0; }
	inline AppleAppStoreBillingService_t956296338 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AppleAppStoreBillingService_t956296338 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_1() { return static_cast<int32_t>(offsetof(OSXStoreKitPluginImpl_t3020431434_StaticFields, ___U3CU3Ef__switchU24map0_1)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_1() const { return ___U3CU3Ef__switchU24map0_1; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_1() { return &___U3CU3Ef__switchU24map0_1; }
	inline void set_U3CU3Ef__switchU24map0_1(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
