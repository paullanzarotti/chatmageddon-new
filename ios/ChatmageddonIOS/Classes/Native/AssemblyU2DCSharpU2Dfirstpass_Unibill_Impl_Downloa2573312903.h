﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Unibill.Impl.DownloadManager/<download>c__Iterator2
struct U3CdownloadU3Ec__Iterator2_t3409902454;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3
struct  U3CdownloadU3Ec__AnonStorey3_t2573312903  : public Il2CppObject
{
public:
	// System.String Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3::bundleId
	String_t* ___bundleId_0;
	// Unibill.Impl.DownloadManager/<download>c__Iterator2 Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3::<>f__ref$2
	U3CdownloadU3Ec__Iterator2_t3409902454 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_bundleId_0() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__AnonStorey3_t2573312903, ___bundleId_0)); }
	inline String_t* get_bundleId_0() const { return ___bundleId_0; }
	inline String_t** get_address_of_bundleId_0() { return &___bundleId_0; }
	inline void set_bundleId_0(String_t* value)
	{
		___bundleId_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleId_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__AnonStorey3_t2573312903, ___U3CU3Ef__refU242_1)); }
	inline U3CdownloadU3Ec__Iterator2_t3409902454 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CdownloadU3Ec__Iterator2_t3409902454 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CdownloadU3Ec__Iterator2_t3409902454 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU242_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
