﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blur_Movie
struct  CameraFilterPack_Blur_Movie_t2949542203  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Blur_Movie::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Movie::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Movie::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Movie::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Movie::Radius
	float ___Radius_6;
	// System.Single CameraFilterPack_Blur_Movie::Factor
	float ___Factor_7;
	// System.Int32 CameraFilterPack_Blur_Movie::FastFilter
	int32_t ___FastFilter_8;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Radius_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___Radius_6)); }
	inline float get_Radius_6() const { return ___Radius_6; }
	inline float* get_address_of_Radius_6() { return &___Radius_6; }
	inline void set_Radius_6(float value)
	{
		___Radius_6 = value;
	}

	inline static int32_t get_offset_of_Factor_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___Factor_7)); }
	inline float get_Factor_7() const { return ___Factor_7; }
	inline float* get_address_of_Factor_7() { return &___Factor_7; }
	inline void set_Factor_7(float value)
	{
		___Factor_7 = value;
	}

	inline static int32_t get_offset_of_FastFilter_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203, ___FastFilter_8)); }
	inline int32_t get_FastFilter_8() const { return ___FastFilter_8; }
	inline int32_t* get_address_of_FastFilter_8() { return &___FastFilter_8; }
	inline void set_FastFilter_8(int32_t value)
	{
		___FastFilter_8 = value;
	}
};

struct CameraFilterPack_Blur_Movie_t2949542203_StaticFields
{
public:
	// System.Single CameraFilterPack_Blur_Movie::ChangeRadius
	float ___ChangeRadius_9;
	// System.Single CameraFilterPack_Blur_Movie::ChangeFactor
	float ___ChangeFactor_10;
	// System.Int32 CameraFilterPack_Blur_Movie::ChangeFastFilter
	int32_t ___ChangeFastFilter_11;

public:
	inline static int32_t get_offset_of_ChangeRadius_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203_StaticFields, ___ChangeRadius_9)); }
	inline float get_ChangeRadius_9() const { return ___ChangeRadius_9; }
	inline float* get_address_of_ChangeRadius_9() { return &___ChangeRadius_9; }
	inline void set_ChangeRadius_9(float value)
	{
		___ChangeRadius_9 = value;
	}

	inline static int32_t get_offset_of_ChangeFactor_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203_StaticFields, ___ChangeFactor_10)); }
	inline float get_ChangeFactor_10() const { return ___ChangeFactor_10; }
	inline float* get_address_of_ChangeFactor_10() { return &___ChangeFactor_10; }
	inline void set_ChangeFactor_10(float value)
	{
		___ChangeFactor_10 = value;
	}

	inline static int32_t get_offset_of_ChangeFastFilter_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Movie_t2949542203_StaticFields, ___ChangeFastFilter_11)); }
	inline int32_t get_ChangeFastFilter_11() const { return ___ChangeFastFilter_11; }
	inline int32_t* get_address_of_ChangeFastFilter_11() { return &___ChangeFastFilter_11; }
	inline void set_ChangeFastFilter_11(int32_t value)
	{
		___ChangeFastFilter_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
