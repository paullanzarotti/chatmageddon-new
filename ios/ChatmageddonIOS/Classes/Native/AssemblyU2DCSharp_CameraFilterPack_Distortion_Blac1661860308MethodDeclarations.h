﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_BlackHole
struct CameraFilterPack_Distortion_BlackHole_t1661860308;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_BlackHole::.ctor()
extern "C"  void CameraFilterPack_Distortion_BlackHole__ctor_m3352243253 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_BlackHole::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_BlackHole_get_material_m3371343592 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::Start()
extern "C"  void CameraFilterPack_Distortion_BlackHole_Start_m1488572741 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_BlackHole_OnRenderImage_m2938570941 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::OnValidate()
extern "C"  void CameraFilterPack_Distortion_BlackHole_OnValidate_m2860823572 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::Update()
extern "C"  void CameraFilterPack_Distortion_BlackHole_Update_m2045714562 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::OnDisable()
extern "C"  void CameraFilterPack_Distortion_BlackHole_OnDisable_m2443400836 (CameraFilterPack_Distortion_BlackHole_t1661860308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
