﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneMetadata/Builder
struct Builder_t1569388123;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// PhoneNumbers.PhoneNumberDesc/Builder
struct Builder_t474280692;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat>
struct IList_1_t982679825;
// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// PhoneNumbers.NumberFormat/Builder
struct Builder_t3378404562;
// System.Collections.Generic.IEnumerable`1<PhoneNumbers.NumberFormat>
struct IEnumerable_1_t733866269;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata366861403.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc922391174.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc_Buil474280692.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat_Builde3378404562.h"

// System.Void PhoneNumbers.PhoneMetadata/Builder::.ctor()
extern "C"  void Builder__ctor_m4292857784 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::get_ThisBuilder()
extern "C"  Builder_t1569388123 * Builder_get_ThisBuilder_m2069355032 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata/Builder::get_MessageBeingBuilt()
extern "C"  PhoneMetadata_t366861403 * Builder_get_MessageBeingBuilt_m166359613 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::Clear()
extern "C"  Builder_t1569388123 * Builder_Clear_m4051638753 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::Clone()
extern "C"  Builder_t1569388123 * Builder_Clone_m1600430163 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata/Builder::get_DefaultInstanceForType()
extern "C"  PhoneMetadata_t366861403 * Builder_get_DefaultInstanceForType_m1461373650 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata/Builder::Build()
extern "C"  PhoneMetadata_t366861403 * Builder_Build_m2143006660 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata/Builder::BuildPartial()
extern "C"  PhoneMetadata_t366861403 * Builder_BuildPartial_m3660059979 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeFrom(PhoneNumbers.PhoneMetadata)
extern "C"  Builder_t1569388123 * Builder_MergeFrom_m3362743349 (Builder_t1569388123 * __this, PhoneMetadata_t366861403 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasGeneralDesc()
extern "C"  bool Builder_get_HasGeneralDesc_m4144476456 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_GeneralDesc()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_GeneralDesc_m327833787 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_GeneralDesc(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_GeneralDesc_m1240019378 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetGeneralDesc(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetGeneralDesc_m476056581 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetGeneralDesc(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetGeneralDesc_m3137950071 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeGeneralDesc(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeGeneralDesc_m1298256707 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearGeneralDesc()
extern "C"  Builder_t1569388123 * Builder_ClearGeneralDesc_m3944639240 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasFixedLine()
extern "C"  bool Builder_get_HasFixedLine_m1090710037 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_FixedLine()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_FixedLine_m2694715762 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_FixedLine(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_FixedLine_m3619894979 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetFixedLine(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetFixedLine_m2674876458 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetFixedLine(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetFixedLine_m3853044170 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeFixedLine(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeFixedLine_m2951725982 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearFixedLine()
extern "C"  Builder_t1569388123 * Builder_ClearFixedLine_m635283969 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasMobile()
extern "C"  bool Builder_get_HasMobile_m2703009149 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_Mobile()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_Mobile_m3895042714 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Mobile(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_Mobile_m2194109739 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetMobile(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetMobile_m1261443410 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetMobile(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetMobile_m4118808238 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeMobile(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeMobile_m817815206 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearMobile()
extern "C"  Builder_t1569388123 * Builder_ClearMobile_m3591038329 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasTollFree()
extern "C"  bool Builder_get_HasTollFree_m706470914 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_TollFree()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_TollFree_m3060456175 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_TollFree(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_TollFree_m2175506960 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetTollFree(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetTollFree_m2846816317 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetTollFree(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetTollFree_m822621035 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeTollFree(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeTollFree_m2595555007 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearTollFree()
extern "C"  Builder_t1569388123 * Builder_ClearTollFree_m2864264306 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasPremiumRate()
extern "C"  bool Builder_get_HasPremiumRate_m3473850954 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_PremiumRate()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_PremiumRate_m1539190265 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_PremiumRate(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_PremiumRate_m2924404220 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPremiumRate(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetPremiumRate_m152779283 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPremiumRate(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetPremiumRate_m3084028789 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergePremiumRate(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergePremiumRate_m91144905 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearPremiumRate()
extern "C"  Builder_t1569388123 * Builder_ClearPremiumRate_m122623758 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasSharedCost()
extern "C"  bool Builder_get_HasSharedCost_m757969987 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_SharedCost()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_SharedCost_m3211809292 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_SharedCost(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_SharedCost_m1413196145 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetSharedCost(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetSharedCost_m4113629524 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetSharedCost(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetSharedCost_m3723807316 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeSharedCost(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeSharedCost_m1270351736 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearSharedCost()
extern "C"  Builder_t1569388123 * Builder_ClearSharedCost_m72806803 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasPersonalNumber()
extern "C"  bool Builder_get_HasPersonalNumber_m3144423404 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_PersonalNumber()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_PersonalNumber_m2548946911 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_PersonalNumber(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_PersonalNumber_m2330463866 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPersonalNumber(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetPersonalNumber_m313193653 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPersonalNumber(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetPersonalNumber_m1485277579 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergePersonalNumber(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergePersonalNumber_m3718279015 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearPersonalNumber()
extern "C"  Builder_t1569388123 * Builder_ClearPersonalNumber_m1100812476 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasVoip()
extern "C"  bool Builder_get_HasVoip_m3737792973 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_Voip()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_Voip_m2046273978 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Voip(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_Voip_m2033684315 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetVoip(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetVoip_m3780782138 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetVoip(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetVoip_m138192254 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeVoip(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeVoip_m2511372846 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearVoip()
extern "C"  Builder_t1569388123 * Builder_ClearVoip_m1059243449 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasPager()
extern "C"  bool Builder_get_HasPager_m446107490 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_Pager()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_Pager_m378745745 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Pager(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_Pager_m1624684100 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPager(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetPager_m2775521771 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPager(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetPager_m2777100765 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergePager(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergePager_m3326113745 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearPager()
extern "C"  Builder_t1569388123 * Builder_ClearPager_m626856374 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasUan()
extern "C"  bool Builder_get_HasUan_m2964386215 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_Uan()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_Uan_m714438526 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Uan(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_Uan_m1904533589 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetUan(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetUan_m2905823666 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetUan(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetUan_m644136014 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeUan(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeUan_m1040406554 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearUan()
extern "C"  Builder_t1569388123 * Builder_ClearUan_m1448183959 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasEmergency()
extern "C"  bool Builder_get_HasEmergency_m1007765124 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_Emergency()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_Emergency_m3058013 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Emergency(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_Emergency_m182655606 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetEmergency(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetEmergency_m2607596959 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetEmergency(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetEmergency_m3896227761 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeEmergency(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeEmergency_m2558637821 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearEmergency()
extern "C"  Builder_t1569388123 * Builder_ClearEmergency_m2195297928 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasVoicemail()
extern "C"  bool Builder_get_HasVoicemail_m1742843488 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_Voicemail()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_Voicemail_m1270819027 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Voicemail(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_Voicemail_m566183974 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetVoicemail(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetVoicemail_m2650669641 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetVoicemail(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetVoicemail_m3371369407 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeVoicemail(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeVoicemail_m2735951443 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearVoicemail()
extern "C"  Builder_t1569388123 * Builder_ClearVoicemail_m1803865568 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasNoInternationalDialling()
extern "C"  bool Builder_get_HasNoInternationalDialling_m2084983858 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata/Builder::get_NoInternationalDialling()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_NoInternationalDialling_m2409116943 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_NoInternationalDialling(PhoneNumbers.PhoneNumberDesc)
extern "C"  void Builder_set_NoInternationalDialling_m3226160636 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNoInternationalDialling(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_SetNoInternationalDialling_m49716729 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNoInternationalDialling(PhoneNumbers.PhoneNumberDesc/Builder)
extern "C"  Builder_t1569388123 * Builder_SetNoInternationalDialling_m2558218755 (Builder_t1569388123 * __this, Builder_t474280692 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::MergeNoInternationalDialling(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t1569388123 * Builder_MergeNoInternationalDialling_m4246793775 (Builder_t1569388123 * __this, PhoneNumberDesc_t922391174 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearNoInternationalDialling()
extern "C"  Builder_t1569388123 * Builder_ClearNoInternationalDialling_m1018256242 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasId()
extern "C"  bool Builder_get_HasId_m11112256 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_Id()
extern "C"  String_t* Builder_get_Id_m2145056987 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_Id(System.String)
extern "C"  void Builder_set_Id_m3875067316 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetId(System.String)
extern "C"  Builder_t1569388123 * Builder_SetId_m3584775025 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearId()
extern "C"  Builder_t1569388123 * Builder_ClearId_m1925439572 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasCountryCode()
extern "C"  bool Builder_get_HasCountryCode_m4167516638 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata/Builder::get_CountryCode()
extern "C"  int32_t Builder_get_CountryCode_m1770086406 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_CountryCode(System.Int32)
extern "C"  void Builder_set_CountryCode_m3492131103 (Builder_t1569388123 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetCountryCode(System.Int32)
extern "C"  Builder_t1569388123 * Builder_SetCountryCode_m3207464920 (Builder_t1569388123 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearCountryCode()
extern "C"  Builder_t1569388123 * Builder_ClearCountryCode_m3683762386 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasInternationalPrefix()
extern "C"  bool Builder_get_HasInternationalPrefix_m1565883775 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_InternationalPrefix()
extern "C"  String_t* Builder_get_InternationalPrefix_m3204650620 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_InternationalPrefix(System.String)
extern "C"  void Builder_set_InternationalPrefix_m226810367 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetInternationalPrefix(System.String)
extern "C"  Builder_t1569388123 * Builder_SetInternationalPrefix_m4142924016 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearInternationalPrefix()
extern "C"  Builder_t1569388123 * Builder_ClearInternationalPrefix_m2963781503 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasPreferredInternationalPrefix()
extern "C"  bool Builder_get_HasPreferredInternationalPrefix_m626995442 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_PreferredInternationalPrefix()
extern "C"  String_t* Builder_get_PreferredInternationalPrefix_m2480165689 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_PreferredInternationalPrefix(System.String)
extern "C"  void Builder_set_PreferredInternationalPrefix_m229754882 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPreferredInternationalPrefix(System.String)
extern "C"  Builder_t1569388123 * Builder_SetPreferredInternationalPrefix_m3826984395 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearPreferredInternationalPrefix()
extern "C"  Builder_t1569388123 * Builder_ClearPreferredInternationalPrefix_m2534166210 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasNationalPrefix()
extern "C"  bool Builder_get_HasNationalPrefix_m1981170923 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_NationalPrefix()
extern "C"  String_t* Builder_get_NationalPrefix_m2916975152 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_NationalPrefix(System.String)
extern "C"  void Builder_set_NationalPrefix_m3603049003 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNationalPrefix(System.String)
extern "C"  Builder_t1569388123 * Builder_SetNationalPrefix_m2101352252 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearNationalPrefix()
extern "C"  Builder_t1569388123 * Builder_ClearNationalPrefix_m1848054059 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasPreferredExtnPrefix()
extern "C"  bool Builder_get_HasPreferredExtnPrefix_m1305872795 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_PreferredExtnPrefix()
extern "C"  String_t* Builder_get_PreferredExtnPrefix_m1143013242 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_PreferredExtnPrefix(System.String)
extern "C"  void Builder_set_PreferredExtnPrefix_m603934875 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetPreferredExtnPrefix(System.String)
extern "C"  Builder_t1569388123 * Builder_SetPreferredExtnPrefix_m1662518234 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearPreferredExtnPrefix()
extern "C"  Builder_t1569388123 * Builder_ClearPreferredExtnPrefix_m1610946891 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasNationalPrefixForParsing()
extern "C"  bool Builder_get_HasNationalPrefixForParsing_m327608094 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_NationalPrefixForParsing()
extern "C"  String_t* Builder_get_NationalPrefixForParsing_m3812408999 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_NationalPrefixForParsing(System.String)
extern "C"  void Builder_set_NationalPrefixForParsing_m3016708098 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNationalPrefixForParsing(System.String)
extern "C"  Builder_t1569388123 * Builder_SetNationalPrefixForParsing_m4280944725 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearNationalPrefixForParsing()
extern "C"  Builder_t1569388123 * Builder_ClearNationalPrefixForParsing_m1642918514 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasNationalPrefixTransformRule()
extern "C"  bool Builder_get_HasNationalPrefixTransformRule_m2570277693 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_NationalPrefixTransformRule()
extern "C"  String_t* Builder_get_NationalPrefixTransformRule_m3838297352 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_NationalPrefixTransformRule(System.String)
extern "C"  void Builder_set_NationalPrefixTransformRule_m2763730077 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNationalPrefixTransformRule(System.String)
extern "C"  Builder_t1569388123 * Builder_SetNationalPrefixTransformRule_m1172375736 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearNationalPrefixTransformRule()
extern "C"  Builder_t1569388123 * Builder_ClearNationalPrefixTransformRule_m2297414833 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasSameMobileAndFixedLinePattern()
extern "C"  bool Builder_get_HasSameMobileAndFixedLinePattern_m1895520626 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_SameMobileAndFixedLinePattern()
extern "C"  bool Builder_get_SameMobileAndFixedLinePattern_m191534254 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_SameMobileAndFixedLinePattern(System.Boolean)
extern "C"  void Builder_set_SameMobileAndFixedLinePattern_m649960513 (Builder_t1569388123 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetSameMobileAndFixedLinePattern(System.Boolean)
extern "C"  Builder_t1569388123 * Builder_SetSameMobileAndFixedLinePattern_m3407025516 (Builder_t1569388123 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearSameMobileAndFixedLinePattern()
extern "C"  Builder_t1569388123 * Builder_ClearSameMobileAndFixedLinePattern_m3198981494 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat> PhoneNumbers.PhoneMetadata/Builder::get_NumberFormatList()
extern "C"  Il2CppObject* Builder_get_NumberFormatList_m579704679 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata/Builder::get_NumberFormatCount()
extern "C"  int32_t Builder_get_NumberFormatCount_m1409307912 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.PhoneMetadata/Builder::GetNumberFormat(System.Int32)
extern "C"  NumberFormat_t441739224 * Builder_GetNumberFormat_m1998979758 (Builder_t1569388123 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNumberFormat(System.Int32,PhoneNumbers.NumberFormat)
extern "C"  Builder_t1569388123 * Builder_SetNumberFormat_m3452122753 (Builder_t1569388123 * __this, int32_t ___index0, NumberFormat_t441739224 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetNumberFormat(System.Int32,PhoneNumbers.NumberFormat/Builder)
extern "C"  Builder_t1569388123 * Builder_SetNumberFormat_m2545697651 (Builder_t1569388123 * __this, int32_t ___index0, Builder_t3378404562 * ___builderForValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::AddNumberFormat(PhoneNumbers.NumberFormat)
extern "C"  Builder_t1569388123 * Builder_AddNumberFormat_m1505247387 (Builder_t1569388123 * __this, NumberFormat_t441739224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::AddNumberFormat(PhoneNumbers.NumberFormat/Builder)
extern "C"  Builder_t1569388123 * Builder_AddNumberFormat_m732142685 (Builder_t1569388123 * __this, Builder_t3378404562 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::AddRangeNumberFormat(System.Collections.Generic.IEnumerable`1<PhoneNumbers.NumberFormat>)
extern "C"  Builder_t1569388123 * Builder_AddRangeNumberFormat_m892305475 (Builder_t1569388123 * __this, Il2CppObject* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearNumberFormat()
extern "C"  Builder_t1569388123 * Builder_ClearNumberFormat_m2093126521 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat> PhoneNumbers.PhoneMetadata/Builder::get_IntlNumberFormatList()
extern "C"  Il2CppObject* Builder_get_IntlNumberFormatList_m4186656584 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata/Builder::get_IntlNumberFormatCount()
extern "C"  int32_t Builder_get_IntlNumberFormatCount_m2979462489 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.PhoneMetadata/Builder::GetIntlNumberFormat(System.Int32)
extern "C"  NumberFormat_t441739224 * Builder_GetIntlNumberFormat_m1112679019 (Builder_t1569388123 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetIntlNumberFormat(System.Int32,PhoneNumbers.NumberFormat)
extern "C"  Builder_t1569388123 * Builder_SetIntlNumberFormat_m2275066142 (Builder_t1569388123 * __this, int32_t ___index0, NumberFormat_t441739224 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetIntlNumberFormat(System.Int32,PhoneNumbers.NumberFormat/Builder)
extern "C"  Builder_t1569388123 * Builder_SetIntlNumberFormat_m3451672342 (Builder_t1569388123 * __this, int32_t ___index0, Builder_t3378404562 * ___builderForValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::AddIntlNumberFormat(PhoneNumbers.NumberFormat)
extern "C"  Builder_t1569388123 * Builder_AddIntlNumberFormat_m2209036846 (Builder_t1569388123 * __this, NumberFormat_t441739224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::AddIntlNumberFormat(PhoneNumbers.NumberFormat/Builder)
extern "C"  Builder_t1569388123 * Builder_AddIntlNumberFormat_m3196173774 (Builder_t1569388123 * __this, Builder_t3378404562 * ___builderForValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::AddRangeIntlNumberFormat(System.Collections.Generic.IEnumerable`1<PhoneNumbers.NumberFormat>)
extern "C"  Builder_t1569388123 * Builder_AddRangeIntlNumberFormat_m2162781018 (Builder_t1569388123 * __this, Il2CppObject* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearIntlNumberFormat()
extern "C"  Builder_t1569388123 * Builder_ClearIntlNumberFormat_m1629831552 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasMainCountryForCode()
extern "C"  bool Builder_get_HasMainCountryForCode_m190432940 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_MainCountryForCode()
extern "C"  bool Builder_get_MainCountryForCode_m303565560 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_MainCountryForCode(System.Boolean)
extern "C"  void Builder_set_MainCountryForCode_m3758116157 (Builder_t1569388123 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetMainCountryForCode(System.Boolean)
extern "C"  Builder_t1569388123 * Builder_SetMainCountryForCode_m2761522288 (Builder_t1569388123 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearMainCountryForCode()
extern "C"  Builder_t1569388123 * Builder_ClearMainCountryForCode_m4160926984 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasLeadingDigits()
extern "C"  bool Builder_get_HasLeadingDigits_m1181921513 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata/Builder::get_LeadingDigits()
extern "C"  String_t* Builder_get_LeadingDigits_m4141248044 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_LeadingDigits(System.String)
extern "C"  void Builder_set_LeadingDigits_m3475398153 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetLeadingDigits(System.String)
extern "C"  Builder_t1569388123 * Builder_SetLeadingDigits_m1421262036 (Builder_t1569388123 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearLeadingDigits()
extern "C"  Builder_t1569388123 * Builder_ClearLeadingDigits_m2174535661 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_HasLeadingZeroPossible()
extern "C"  bool Builder_get_HasLeadingZeroPossible_m568428702 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata/Builder::get_LeadingZeroPossible()
extern "C"  bool Builder_get_LeadingZeroPossible_m1450759466 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata/Builder::set_LeadingZeroPossible(System.Boolean)
extern "C"  void Builder_set_LeadingZeroPossible_m1969625871 (Builder_t1569388123 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::SetLeadingZeroPossible(System.Boolean)
extern "C"  Builder_t1569388123 * Builder_SetLeadingZeroPossible_m3402512016 (Builder_t1569388123 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata/Builder::ClearLeadingZeroPossible()
extern "C"  Builder_t1569388123 * Builder_ClearLeadingZeroPossible_m2732027022 (Builder_t1569388123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
