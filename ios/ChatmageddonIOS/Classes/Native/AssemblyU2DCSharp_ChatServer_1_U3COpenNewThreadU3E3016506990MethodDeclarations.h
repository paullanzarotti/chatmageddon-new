﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<OpenNewThread>c__AnonStorey0<System.Object>
struct U3COpenNewThreadU3Ec__AnonStorey0_t3016506990;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<OpenNewThread>c__AnonStorey0<System.Object>::.ctor()
extern "C"  void U3COpenNewThreadU3Ec__AnonStorey0__ctor_m69007069_gshared (U3COpenNewThreadU3Ec__AnonStorey0_t3016506990 * __this, const MethodInfo* method);
#define U3COpenNewThreadU3Ec__AnonStorey0__ctor_m69007069(__this, method) ((  void (*) (U3COpenNewThreadU3Ec__AnonStorey0_t3016506990 *, const MethodInfo*))U3COpenNewThreadU3Ec__AnonStorey0__ctor_m69007069_gshared)(__this, method)
// System.Void ChatServer`1/<OpenNewThread>c__AnonStorey0<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3COpenNewThreadU3Ec__AnonStorey0_U3CU3Em__0_m3813209206_gshared (U3COpenNewThreadU3Ec__AnonStorey0_t3016506990 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3COpenNewThreadU3Ec__AnonStorey0_U3CU3Em__0_m3813209206(__this, ___request0, ___response1, method) ((  void (*) (U3COpenNewThreadU3Ec__AnonStorey0_t3016506990 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3COpenNewThreadU3Ec__AnonStorey0_U3CU3Em__0_m3813209206_gshared)(__this, ___request0, ___response1, method)
