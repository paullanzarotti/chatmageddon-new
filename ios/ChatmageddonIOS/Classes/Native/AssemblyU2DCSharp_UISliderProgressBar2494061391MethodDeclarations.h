﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISliderProgressBar
struct UISliderProgressBar_t2494061391;

#include "codegen/il2cpp-codegen.h"

// System.Void UISliderProgressBar::.ctor()
extern "C"  void UISliderProgressBar__ctor_m776695048 (UISliderProgressBar_t2494061391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISliderProgressBar::ProgressUpdated()
extern "C"  void UISliderProgressBar_ProgressUpdated_m3967953782 (UISliderProgressBar_t2494061391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISliderProgressBar::ProgressFinished()
extern "C"  void UISliderProgressBar_ProgressFinished_m2052252345 (UISliderProgressBar_t2494061391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
