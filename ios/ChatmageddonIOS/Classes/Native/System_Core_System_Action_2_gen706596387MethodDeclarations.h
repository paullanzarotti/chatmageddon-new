﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<BestHTTP.SocketIO.HandshakeData,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1548860894(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t706596387 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2967680750_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<BestHTTP.SocketIO.HandshakeData,System.String>::Invoke(T1,T2)
#define Action_2_Invoke_m3923447029(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t706596387 *, HandshakeData_t1703965475 *, String_t*, const MethodInfo*))Action_2_Invoke_m715425719_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<BestHTTP.SocketIO.HandshakeData,System.String>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3934640526(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t706596387 *, HandshakeData_t1703965475 *, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<BestHTTP.SocketIO.HandshakeData,System.String>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3524855002(__this, ___result0, method) ((  void (*) (Action_2_t706596387 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
