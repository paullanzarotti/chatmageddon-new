﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusUIScaler
struct  StatusUIScaler_t1399255572  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite StatusUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UISprite StatusUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_15;
	// UnityEngine.Transform StatusUIScaler::navigateBackButton
	Transform_t3275118058 * ___navigateBackButton_16;
	// UnityEngine.Transform StatusUIScaler::clearButton
	Transform_t3275118058 * ___clearButton_17;
	// UISprite StatusUIScaler::tabBackground
	UISprite_t603616735 * ___tabBackground_18;
	// UISprite StatusUIScaler::bottomSeperator
	UISprite_t603616735 * ___bottomSeperator_19;
	// UnityEngine.Transform StatusUIScaler::incomingTab
	Transform_t3275118058 * ___incomingTab_20;
	// UnityEngine.BoxCollider StatusUIScaler::incomingTabCollider
	BoxCollider_t22920061 * ___incomingTabCollider_21;
	// UnityEngine.Transform StatusUIScaler::outgoingTab
	Transform_t3275118058 * ___outgoingTab_22;
	// UnityEngine.BoxCollider StatusUIScaler::outgoingTabCollider
	BoxCollider_t22920061 * ___outgoingTabCollider_23;
	// UIPanel StatusUIScaler::topPanel
	UIPanel_t1795085332 * ___topPanel_24;
	// UIPanel StatusUIScaler::incomingPanel
	UIPanel_t1795085332 * ___incomingPanel_25;
	// UIPanel StatusUIScaler::outgoingPanel
	UIPanel_t1795085332 * ___outgoingPanel_26;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_titleSeperator_15() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___titleSeperator_15)); }
	inline UISprite_t603616735 * get_titleSeperator_15() const { return ___titleSeperator_15; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_15() { return &___titleSeperator_15; }
	inline void set_titleSeperator_15(UISprite_t603616735 * value)
	{
		___titleSeperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_15, value);
	}

	inline static int32_t get_offset_of_navigateBackButton_16() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___navigateBackButton_16)); }
	inline Transform_t3275118058 * get_navigateBackButton_16() const { return ___navigateBackButton_16; }
	inline Transform_t3275118058 ** get_address_of_navigateBackButton_16() { return &___navigateBackButton_16; }
	inline void set_navigateBackButton_16(Transform_t3275118058 * value)
	{
		___navigateBackButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___navigateBackButton_16, value);
	}

	inline static int32_t get_offset_of_clearButton_17() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___clearButton_17)); }
	inline Transform_t3275118058 * get_clearButton_17() const { return ___clearButton_17; }
	inline Transform_t3275118058 ** get_address_of_clearButton_17() { return &___clearButton_17; }
	inline void set_clearButton_17(Transform_t3275118058 * value)
	{
		___clearButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___clearButton_17, value);
	}

	inline static int32_t get_offset_of_tabBackground_18() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___tabBackground_18)); }
	inline UISprite_t603616735 * get_tabBackground_18() const { return ___tabBackground_18; }
	inline UISprite_t603616735 ** get_address_of_tabBackground_18() { return &___tabBackground_18; }
	inline void set_tabBackground_18(UISprite_t603616735 * value)
	{
		___tabBackground_18 = value;
		Il2CppCodeGenWriteBarrier(&___tabBackground_18, value);
	}

	inline static int32_t get_offset_of_bottomSeperator_19() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___bottomSeperator_19)); }
	inline UISprite_t603616735 * get_bottomSeperator_19() const { return ___bottomSeperator_19; }
	inline UISprite_t603616735 ** get_address_of_bottomSeperator_19() { return &___bottomSeperator_19; }
	inline void set_bottomSeperator_19(UISprite_t603616735 * value)
	{
		___bottomSeperator_19 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSeperator_19, value);
	}

	inline static int32_t get_offset_of_incomingTab_20() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___incomingTab_20)); }
	inline Transform_t3275118058 * get_incomingTab_20() const { return ___incomingTab_20; }
	inline Transform_t3275118058 ** get_address_of_incomingTab_20() { return &___incomingTab_20; }
	inline void set_incomingTab_20(Transform_t3275118058 * value)
	{
		___incomingTab_20 = value;
		Il2CppCodeGenWriteBarrier(&___incomingTab_20, value);
	}

	inline static int32_t get_offset_of_incomingTabCollider_21() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___incomingTabCollider_21)); }
	inline BoxCollider_t22920061 * get_incomingTabCollider_21() const { return ___incomingTabCollider_21; }
	inline BoxCollider_t22920061 ** get_address_of_incomingTabCollider_21() { return &___incomingTabCollider_21; }
	inline void set_incomingTabCollider_21(BoxCollider_t22920061 * value)
	{
		___incomingTabCollider_21 = value;
		Il2CppCodeGenWriteBarrier(&___incomingTabCollider_21, value);
	}

	inline static int32_t get_offset_of_outgoingTab_22() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___outgoingTab_22)); }
	inline Transform_t3275118058 * get_outgoingTab_22() const { return ___outgoingTab_22; }
	inline Transform_t3275118058 ** get_address_of_outgoingTab_22() { return &___outgoingTab_22; }
	inline void set_outgoingTab_22(Transform_t3275118058 * value)
	{
		___outgoingTab_22 = value;
		Il2CppCodeGenWriteBarrier(&___outgoingTab_22, value);
	}

	inline static int32_t get_offset_of_outgoingTabCollider_23() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___outgoingTabCollider_23)); }
	inline BoxCollider_t22920061 * get_outgoingTabCollider_23() const { return ___outgoingTabCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of_outgoingTabCollider_23() { return &___outgoingTabCollider_23; }
	inline void set_outgoingTabCollider_23(BoxCollider_t22920061 * value)
	{
		___outgoingTabCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&___outgoingTabCollider_23, value);
	}

	inline static int32_t get_offset_of_topPanel_24() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___topPanel_24)); }
	inline UIPanel_t1795085332 * get_topPanel_24() const { return ___topPanel_24; }
	inline UIPanel_t1795085332 ** get_address_of_topPanel_24() { return &___topPanel_24; }
	inline void set_topPanel_24(UIPanel_t1795085332 * value)
	{
		___topPanel_24 = value;
		Il2CppCodeGenWriteBarrier(&___topPanel_24, value);
	}

	inline static int32_t get_offset_of_incomingPanel_25() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___incomingPanel_25)); }
	inline UIPanel_t1795085332 * get_incomingPanel_25() const { return ___incomingPanel_25; }
	inline UIPanel_t1795085332 ** get_address_of_incomingPanel_25() { return &___incomingPanel_25; }
	inline void set_incomingPanel_25(UIPanel_t1795085332 * value)
	{
		___incomingPanel_25 = value;
		Il2CppCodeGenWriteBarrier(&___incomingPanel_25, value);
	}

	inline static int32_t get_offset_of_outgoingPanel_26() { return static_cast<int32_t>(offsetof(StatusUIScaler_t1399255572, ___outgoingPanel_26)); }
	inline UIPanel_t1795085332 * get_outgoingPanel_26() const { return ___outgoingPanel_26; }
	inline UIPanel_t1795085332 ** get_address_of_outgoingPanel_26() { return &___outgoingPanel_26; }
	inline void set_outgoingPanel_26(UIPanel_t1795085332 * value)
	{
		___outgoingPanel_26 = value;
		Il2CppCodeGenWriteBarrier(&___outgoingPanel_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
