﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToastUI
struct ToastUI_t270783635;
// Bread
struct Bread_t457172030;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Bread457172030.h"

// System.Void ToastUI::.ctor()
extern "C"  void ToastUI__ctor_m2244124972 (ToastUI_t270783635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastUI::Awake()
extern "C"  void ToastUI_Awake_m2244019187 (ToastUI_t270783635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastUI::OnAwake()
extern "C"  void ToastUI_OnAwake_m553496488 (ToastUI_t270783635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastUI::SetToastID(System.Int32)
extern "C"  void ToastUI_SetToastID_m1723784101 (ToastUI_t270783635 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastUI::LoadToastUI(Bread)
extern "C"  void ToastUI_LoadToastUI_m3456217169 (ToastUI_t270783635 * __this, Bread_t457172030 * ___bread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastUI::UnloadToastUI()
extern "C"  void ToastUI_UnloadToastUI_m3741981092 (ToastUI_t270783635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToastUI::FinishToasting()
extern "C"  void ToastUI_FinishToasting_m1810779996 (ToastUI_t270783635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
