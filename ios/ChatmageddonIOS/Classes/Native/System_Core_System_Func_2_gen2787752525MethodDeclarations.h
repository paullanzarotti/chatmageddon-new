﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2207932334MethodDeclarations.h"

// System.Void System.Func`2<Friend,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3856220697(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2787752525 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2656334681_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Friend,System.Int32>::Invoke(T)
#define Func_2_Invoke_m1496941539(__this, ___arg10, method) ((  int32_t (*) (Func_2_t2787752525 *, Friend_t3555014108 *, const MethodInfo*))Func_2_Invoke_m1969117355_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Friend,System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m246382496(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2787752525 *, Friend_t3555014108 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m230101644_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Friend,System.Int32>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3287189945(__this, ___result0, method) ((  int32_t (*) (Func_2_t2787752525 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1401450769_gshared)(__this, ___result0, method)
