﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseModalButton
struct CloseModalButton_t3895210965;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseModalButton::.ctor()
extern "C"  void CloseModalButton__ctor_m166387724 (CloseModalButton_t3895210965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseModalButton::OnButtonClick()
extern "C"  void CloseModalButton_OnButtonClick_m371426605 (CloseModalButton_t3895210965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
