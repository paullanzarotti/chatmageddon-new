﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1
struct U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1::.ctor()
extern "C"  void U3CWaitToHideConnectingUIU3Ec__Iterator1__ctor_m2052438720 (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitToHideConnectingUIU3Ec__Iterator1_MoveNext_m389444716 (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToHideConnectingUIU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m232773296 (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToHideConnectingUIU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2860770424 (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1::Dispose()
extern "C"  void U3CWaitToHideConnectingUIU3Ec__Iterator1_Dispose_m778663991 (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp/<WaitToHideConnectingUI>c__Iterator1::Reset()
extern "C"  void U3CWaitToHideConnectingUIU3Ec__Iterator1_Reset_m1260041489 (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
