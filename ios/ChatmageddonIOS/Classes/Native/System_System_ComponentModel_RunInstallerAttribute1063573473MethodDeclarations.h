﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.RunInstallerAttribute
struct RunInstallerAttribute_t1063573473;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.RunInstallerAttribute::.ctor(System.Boolean)
extern "C"  void RunInstallerAttribute__ctor_m839328135 (RunInstallerAttribute_t1063573473 * __this, bool ___runInstaller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.RunInstallerAttribute::.cctor()
extern "C"  void RunInstallerAttribute__cctor_m598830259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.RunInstallerAttribute::Equals(System.Object)
extern "C"  bool RunInstallerAttribute_Equals_m864983901 (RunInstallerAttribute_t1063573473 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.RunInstallerAttribute::GetHashCode()
extern "C"  int32_t RunInstallerAttribute_GetHashCode_m1085330823 (RunInstallerAttribute_t1063573473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.RunInstallerAttribute::IsDefaultAttribute()
extern "C"  bool RunInstallerAttribute_IsDefaultAttribute_m1115369189 (RunInstallerAttribute_t1063573473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.RunInstallerAttribute::get_RunInstaller()
extern "C"  bool RunInstallerAttribute_get_RunInstaller_m4128654118 (RunInstallerAttribute_t1063573473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
