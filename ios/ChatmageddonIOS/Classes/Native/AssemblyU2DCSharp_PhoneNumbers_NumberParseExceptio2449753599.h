﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Exception1927440687.h"
#include "AssemblyU2DCSharp_PhoneNumbers_ErrorType3615858992.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.NumberParseException
struct  NumberParseException_t2449753599  : public Exception_t1927440687
{
public:
	// PhoneNumbers.ErrorType PhoneNumbers.NumberParseException::ErrorType
	int32_t ___ErrorType_11;

public:
	inline static int32_t get_offset_of_ErrorType_11() { return static_cast<int32_t>(offsetof(NumberParseException_t2449753599, ___ErrorType_11)); }
	inline int32_t get_ErrorType_11() const { return ___ErrorType_11; }
	inline int32_t* get_address_of_ErrorType_11() { return &___ErrorType_11; }
	inline void set_ErrorType_11(int32_t value)
	{
		___ErrorType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
