﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En871448018MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2592367395(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4264712713 *, Dictionary_2_t2944688011 *, const MethodInfo*))Enumerator__ctor_m2916059171_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2721333668(__this, method) ((  Il2CppObject * (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3022940580_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1499344232(__this, method) ((  void (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3754034472_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1679308277(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3367077649_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1147019226(__this, method) ((  Il2CppObject * (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2653554246_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2518389832(__this, method) ((  Il2CppObject * (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2154534068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::MoveNext()
#define Enumerator_MoveNext_m2176699880(__this, method) ((  bool (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_MoveNext_m2336684632_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::get_Current()
#define Enumerator_get_Current_m868540192(__this, method) ((  KeyValuePair_2_t702033233  (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_get_Current_m3770553840_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2768450143(__this, method) ((  int32_t (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_get_CurrentKey_m3233614783_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1933588991(__this, method) ((  Panel_t1787746694 * (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_get_CurrentValue_m3518794015_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::Reset()
#define Enumerator_Reset_m1113632061(__this, method) ((  void (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_Reset_m3156758209_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::VerifyState()
#define Enumerator_VerifyState_m696025756(__this, method) ((  void (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_VerifyState_m19120032_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3274917184(__this, method) ((  void (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_VerifyCurrent_m1522794816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,Panel>::Dispose()
#define Enumerator_Dispose_m3171747111(__this, method) ((  void (*) (Enumerator_t4264712713 *, const MethodInfo*))Enumerator_Dispose_m426295975_gshared)(__this, method)
