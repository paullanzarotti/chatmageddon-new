﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<DeleteThread>c__AnonStorey8<System.Object>
struct U3CDeleteThreadU3Ec__AnonStorey8_t2797832465;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<DeleteThread>c__AnonStorey8<System.Object>::.ctor()
extern "C"  void U3CDeleteThreadU3Ec__AnonStorey8__ctor_m343674236_gshared (U3CDeleteThreadU3Ec__AnonStorey8_t2797832465 * __this, const MethodInfo* method);
#define U3CDeleteThreadU3Ec__AnonStorey8__ctor_m343674236(__this, method) ((  void (*) (U3CDeleteThreadU3Ec__AnonStorey8_t2797832465 *, const MethodInfo*))U3CDeleteThreadU3Ec__AnonStorey8__ctor_m343674236_gshared)(__this, method)
// System.Void ChatServer`1/<DeleteThread>c__AnonStorey8<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeleteThreadU3Ec__AnonStorey8_U3CU3Em__0_m3554209241_gshared (U3CDeleteThreadU3Ec__AnonStorey8_t2797832465 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeleteThreadU3Ec__AnonStorey8_U3CU3Em__0_m3554209241(__this, ___request0, ___response1, method) ((  void (*) (U3CDeleteThreadU3Ec__AnonStorey8_t2797832465 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeleteThreadU3Ec__AnonStorey8_U3CU3Em__0_m3554209241_gshared)(__this, ___request0, ___response1, method)
