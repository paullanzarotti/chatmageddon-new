﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.StoreKitPluginImpl
struct StoreKitPluginImpl_t529087534;
// System.String
struct String_t;
// Unibill.Impl.AppleAppStoreBillingService
struct AppleAppStoreBillingService_t956296338;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_AppleApp956296338.h"

// System.Void Unibill.Impl.StoreKitPluginImpl::.ctor()
extern "C"  void StoreKitPluginImpl__ctor_m1305639484 (StoreKitPluginImpl_t529087534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.StoreKitPluginImpl::_storeKitPaymentsAvailable()
extern "C"  bool StoreKitPluginImpl__storeKitPaymentsAvailable_m3299978504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::_storeKitRequestProductData(System.String)
extern "C"  void StoreKitPluginImpl__storeKitRequestProductData_m42406420 (Il2CppObject * __this /* static, unused */, String_t* ___productIdentifiers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::_storeKitPurchaseProduct(System.String)
extern "C"  void StoreKitPluginImpl__storeKitPurchaseProduct_m23469220 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::_storeKitRestoreTransactions()
extern "C"  void StoreKitPluginImpl__storeKitRestoreTransactions_m852864197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::_storeKitAddTransactionObserver()
extern "C"  void StoreKitPluginImpl__storeKitAddTransactionObserver_m4108276171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::initialise(Unibill.Impl.AppleAppStoreBillingService)
extern "C"  void StoreKitPluginImpl_initialise_m1980421106 (StoreKitPluginImpl_t529087534 * __this, AppleAppStoreBillingService_t956296338 * ___svc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.StoreKitPluginImpl::storeKitPaymentsAvailable()
extern "C"  bool StoreKitPluginImpl_storeKitPaymentsAvailable_m3767864713 (StoreKitPluginImpl_t529087534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::storeKitRequestProductData(System.String,System.String[])
extern "C"  void StoreKitPluginImpl_storeKitRequestProductData_m1427253649 (StoreKitPluginImpl_t529087534 * __this, String_t* ___productIdentifiers0, StringU5BU5D_t1642385972* ___productIds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::storeKitPurchaseProduct(System.String)
extern "C"  void StoreKitPluginImpl_storeKitPurchaseProduct_m3443568879 (StoreKitPluginImpl_t529087534 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::storeKitRestoreTransactions()
extern "C"  void StoreKitPluginImpl_storeKitRestoreTransactions_m395176298 (StoreKitPluginImpl_t529087534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.StoreKitPluginImpl::addTransactionObserver()
extern "C"  void StoreKitPluginImpl_addTransactionObserver_m2992943129 (StoreKitPluginImpl_t529087534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
