﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterShareButton
struct TwitterShareButton_t638004942;

#include "codegen/il2cpp-codegen.h"

// System.Void TwitterShareButton::.ctor()
extern "C"  void TwitterShareButton__ctor_m3694522609 (TwitterShareButton_t638004942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterShareButton::OnButtonClick()
extern "C"  void TwitterShareButton_OnButtonClick_m1982606530 (TwitterShareButton_t638004942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
