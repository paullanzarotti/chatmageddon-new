﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BlockILP
struct BlockILP_t879176280;
// Friend
struct Friend_t3555014108;
// UILabel
struct UILabel_t1795115428;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// OpenPlayerProfileButton
struct OpenPlayerProfileButton_t3248004898;
// UISprite
struct UISprite_t603616735;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen2290043410.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlockListItem
struct  BlockListItem_t1514504046  : public BaseInfiniteListItem_1_t2290043410
{
public:
	// BlockILP BlockListItem::listPopulator
	BlockILP_t879176280 * ___listPopulator_10;
	// Friend BlockListItem::blockedFriend
	Friend_t3555014108 * ___blockedFriend_11;
	// UILabel BlockListItem::nameLabel
	UILabel_t1795115428 * ___nameLabel_12;
	// AvatarUpdater BlockListItem::avatar
	AvatarUpdater_t2404165026 * ___avatar_13;
	// OpenPlayerProfileButton BlockListItem::playerProfile
	OpenPlayerProfileButton_t3248004898 * ___playerProfile_14;
	// UISprite BlockListItem::blockSprite
	UISprite_t603616735 * ___blockSprite_15;
	// UILabel BlockListItem::blockLabel
	UILabel_t1795115428 * ___blockLabel_16;
	// UILabel BlockListItem::rankLabel
	UILabel_t1795115428 * ___rankLabel_17;
	// UIScrollView BlockListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_18;
	// UIScrollView BlockListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_19;
	// UnityEngine.Transform BlockListItem::mTrans
	Transform_t3275118058 * ___mTrans_20;
	// UIScrollView BlockListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_21;
	// System.Boolean BlockListItem::mAutoFind
	bool ___mAutoFind_22;
	// System.Boolean BlockListItem::mStarted
	bool ___mStarted_23;

public:
	inline static int32_t get_offset_of_listPopulator_10() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___listPopulator_10)); }
	inline BlockILP_t879176280 * get_listPopulator_10() const { return ___listPopulator_10; }
	inline BlockILP_t879176280 ** get_address_of_listPopulator_10() { return &___listPopulator_10; }
	inline void set_listPopulator_10(BlockILP_t879176280 * value)
	{
		___listPopulator_10 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_10, value);
	}

	inline static int32_t get_offset_of_blockedFriend_11() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___blockedFriend_11)); }
	inline Friend_t3555014108 * get_blockedFriend_11() const { return ___blockedFriend_11; }
	inline Friend_t3555014108 ** get_address_of_blockedFriend_11() { return &___blockedFriend_11; }
	inline void set_blockedFriend_11(Friend_t3555014108 * value)
	{
		___blockedFriend_11 = value;
		Il2CppCodeGenWriteBarrier(&___blockedFriend_11, value);
	}

	inline static int32_t get_offset_of_nameLabel_12() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___nameLabel_12)); }
	inline UILabel_t1795115428 * get_nameLabel_12() const { return ___nameLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_12() { return &___nameLabel_12; }
	inline void set_nameLabel_12(UILabel_t1795115428 * value)
	{
		___nameLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_12, value);
	}

	inline static int32_t get_offset_of_avatar_13() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___avatar_13)); }
	inline AvatarUpdater_t2404165026 * get_avatar_13() const { return ___avatar_13; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_13() { return &___avatar_13; }
	inline void set_avatar_13(AvatarUpdater_t2404165026 * value)
	{
		___avatar_13 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_13, value);
	}

	inline static int32_t get_offset_of_playerProfile_14() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___playerProfile_14)); }
	inline OpenPlayerProfileButton_t3248004898 * get_playerProfile_14() const { return ___playerProfile_14; }
	inline OpenPlayerProfileButton_t3248004898 ** get_address_of_playerProfile_14() { return &___playerProfile_14; }
	inline void set_playerProfile_14(OpenPlayerProfileButton_t3248004898 * value)
	{
		___playerProfile_14 = value;
		Il2CppCodeGenWriteBarrier(&___playerProfile_14, value);
	}

	inline static int32_t get_offset_of_blockSprite_15() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___blockSprite_15)); }
	inline UISprite_t603616735 * get_blockSprite_15() const { return ___blockSprite_15; }
	inline UISprite_t603616735 ** get_address_of_blockSprite_15() { return &___blockSprite_15; }
	inline void set_blockSprite_15(UISprite_t603616735 * value)
	{
		___blockSprite_15 = value;
		Il2CppCodeGenWriteBarrier(&___blockSprite_15, value);
	}

	inline static int32_t get_offset_of_blockLabel_16() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___blockLabel_16)); }
	inline UILabel_t1795115428 * get_blockLabel_16() const { return ___blockLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_blockLabel_16() { return &___blockLabel_16; }
	inline void set_blockLabel_16(UILabel_t1795115428 * value)
	{
		___blockLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___blockLabel_16, value);
	}

	inline static int32_t get_offset_of_rankLabel_17() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___rankLabel_17)); }
	inline UILabel_t1795115428 * get_rankLabel_17() const { return ___rankLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_17() { return &___rankLabel_17; }
	inline void set_rankLabel_17(UILabel_t1795115428 * value)
	{
		___rankLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_17, value);
	}

	inline static int32_t get_offset_of_scrollView_18() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___scrollView_18)); }
	inline UIScrollView_t3033954930 * get_scrollView_18() const { return ___scrollView_18; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_18() { return &___scrollView_18; }
	inline void set_scrollView_18(UIScrollView_t3033954930 * value)
	{
		___scrollView_18 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_18, value);
	}

	inline static int32_t get_offset_of_draggablePanel_19() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___draggablePanel_19)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_19() const { return ___draggablePanel_19; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_19() { return &___draggablePanel_19; }
	inline void set_draggablePanel_19(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_19, value);
	}

	inline static int32_t get_offset_of_mTrans_20() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___mTrans_20)); }
	inline Transform_t3275118058 * get_mTrans_20() const { return ___mTrans_20; }
	inline Transform_t3275118058 ** get_address_of_mTrans_20() { return &___mTrans_20; }
	inline void set_mTrans_20(Transform_t3275118058 * value)
	{
		___mTrans_20 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_20, value);
	}

	inline static int32_t get_offset_of_mScroll_21() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___mScroll_21)); }
	inline UIScrollView_t3033954930 * get_mScroll_21() const { return ___mScroll_21; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_21() { return &___mScroll_21; }
	inline void set_mScroll_21(UIScrollView_t3033954930 * value)
	{
		___mScroll_21 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_21, value);
	}

	inline static int32_t get_offset_of_mAutoFind_22() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___mAutoFind_22)); }
	inline bool get_mAutoFind_22() const { return ___mAutoFind_22; }
	inline bool* get_address_of_mAutoFind_22() { return &___mAutoFind_22; }
	inline void set_mAutoFind_22(bool value)
	{
		___mAutoFind_22 = value;
	}

	inline static int32_t get_offset_of_mStarted_23() { return static_cast<int32_t>(offsetof(BlockListItem_t1514504046, ___mStarted_23)); }
	inline bool get_mStarted_23() const { return ___mStarted_23; }
	inline bool* get_address_of_mStarted_23() { return &___mStarted_23; }
	inline void set_mStarted_23(bool value)
	{
		___mStarted_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
