﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SFXButton
struct  SFXButton_t792651341  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SFXButton::playSFX
	bool ___playSFX_2;
	// SFX SFXButton::sfxToPlay
	int32_t ___sfxToPlay_3;
	// UnityEngine.BoxCollider SFXButton::collision
	BoxCollider_t22920061 * ___collision_4;

public:
	inline static int32_t get_offset_of_playSFX_2() { return static_cast<int32_t>(offsetof(SFXButton_t792651341, ___playSFX_2)); }
	inline bool get_playSFX_2() const { return ___playSFX_2; }
	inline bool* get_address_of_playSFX_2() { return &___playSFX_2; }
	inline void set_playSFX_2(bool value)
	{
		___playSFX_2 = value;
	}

	inline static int32_t get_offset_of_sfxToPlay_3() { return static_cast<int32_t>(offsetof(SFXButton_t792651341, ___sfxToPlay_3)); }
	inline int32_t get_sfxToPlay_3() const { return ___sfxToPlay_3; }
	inline int32_t* get_address_of_sfxToPlay_3() { return &___sfxToPlay_3; }
	inline void set_sfxToPlay_3(int32_t value)
	{
		___sfxToPlay_3 = value;
	}

	inline static int32_t get_offset_of_collision_4() { return static_cast<int32_t>(offsetof(SFXButton_t792651341, ___collision_4)); }
	inline BoxCollider_t22920061 * get_collision_4() const { return ___collision_4; }
	inline BoxCollider_t22920061 ** get_address_of_collision_4() { return &___collision_4; }
	inline void set_collision_4(BoxCollider_t22920061 * value)
	{
		___collision_4 = value;
		Il2CppCodeGenWriteBarrier(&___collision_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
