﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendAvatarUpdater
struct  FriendAvatarUpdater_t4187090262  : public MonoBehaviour_t1158329972
{
public:
	// UITexture FriendAvatarUpdater::avatarTexture
	UITexture_t2537039969 * ___avatarTexture_2;
	// UISprite FriendAvatarUpdater::avatarOutline
	UISprite_t603616735 * ___avatarOutline_3;
	// UILabel FriendAvatarUpdater::initialsLabel
	UILabel_t1795115428 * ___initialsLabel_4;

public:
	inline static int32_t get_offset_of_avatarTexture_2() { return static_cast<int32_t>(offsetof(FriendAvatarUpdater_t4187090262, ___avatarTexture_2)); }
	inline UITexture_t2537039969 * get_avatarTexture_2() const { return ___avatarTexture_2; }
	inline UITexture_t2537039969 ** get_address_of_avatarTexture_2() { return &___avatarTexture_2; }
	inline void set_avatarTexture_2(UITexture_t2537039969 * value)
	{
		___avatarTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___avatarTexture_2, value);
	}

	inline static int32_t get_offset_of_avatarOutline_3() { return static_cast<int32_t>(offsetof(FriendAvatarUpdater_t4187090262, ___avatarOutline_3)); }
	inline UISprite_t603616735 * get_avatarOutline_3() const { return ___avatarOutline_3; }
	inline UISprite_t603616735 ** get_address_of_avatarOutline_3() { return &___avatarOutline_3; }
	inline void set_avatarOutline_3(UISprite_t603616735 * value)
	{
		___avatarOutline_3 = value;
		Il2CppCodeGenWriteBarrier(&___avatarOutline_3, value);
	}

	inline static int32_t get_offset_of_initialsLabel_4() { return static_cast<int32_t>(offsetof(FriendAvatarUpdater_t4187090262, ___initialsLabel_4)); }
	inline UILabel_t1795115428 * get_initialsLabel_4() const { return ___initialsLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_initialsLabel_4() { return &___initialsLabel_4; }
	inline void set_initialsLabel_4(UILabel_t1795115428 * value)
	{
		___initialsLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___initialsLabel_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
