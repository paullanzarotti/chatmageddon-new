﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Vision_Crystal
struct CameraFilterPack_Vision_Crystal_t3702178000;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Vision_Crystal::.ctor()
extern "C"  void CameraFilterPack_Vision_Crystal__ctor_m4019124269 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Crystal::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Vision_Crystal_get_material_m1310218408 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::Start()
extern "C"  void CameraFilterPack_Vision_Crystal_Start_m2720108117 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Vision_Crystal_OnRenderImage_m1496363269 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::OnValidate()
extern "C"  void CameraFilterPack_Vision_Crystal_OnValidate_m314887368 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::Update()
extern "C"  void CameraFilterPack_Vision_Crystal_Update_m1532807994 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::OnDisable()
extern "C"  void CameraFilterPack_Vision_Crystal_OnDisable_m3569432716 (CameraFilterPack_Vision_Crystal_t3702178000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
