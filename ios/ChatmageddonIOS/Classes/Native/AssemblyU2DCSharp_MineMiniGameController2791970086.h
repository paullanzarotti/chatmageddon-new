﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// MineInfoController
struct MineInfoController_t2990208339;
// LaunchCountdownTimer
struct LaunchCountdownTimer_t1840221825;
// TweenAlpha
struct TweenAlpha_t2421518635;
// System.Collections.Generic.List`1<MineDefuseSwipe>
struct List_1_t2324820293;
// MultiImageNGUIAnimation
struct MultiImageNGUIAnimation_t45560503;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineMiniGameController
struct  MineMiniGameController_t2791970086  : public MonoBehaviour_t1158329972
{
public:
	// TweenPosition MineMiniGameController::posTween
	TweenPosition_t1144714832 * ___posTween_2;
	// MineInfoController MineMiniGameController::infoController
	MineInfoController_t2990208339 * ___infoController_3;
	// LaunchCountdownTimer MineMiniGameController::gameCountdownTimer
	LaunchCountdownTimer_t1840221825 * ___gameCountdownTimer_4;
	// TweenAlpha MineMiniGameController::timerAlphaTween
	TweenAlpha_t2421518635 * ___timerAlphaTween_5;
	// System.Collections.Generic.List`1<MineDefuseSwipe> MineMiniGameController::swipes
	List_1_t2324820293 * ___swipes_6;
	// MultiImageNGUIAnimation MineMiniGameController::mineAnimation
	MultiImageNGUIAnimation_t45560503 * ___mineAnimation_7;
	// System.Boolean MineMiniGameController::closingUI
	bool ___closingUI_8;

public:
	inline static int32_t get_offset_of_posTween_2() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___posTween_2)); }
	inline TweenPosition_t1144714832 * get_posTween_2() const { return ___posTween_2; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_2() { return &___posTween_2; }
	inline void set_posTween_2(TweenPosition_t1144714832 * value)
	{
		___posTween_2 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_2, value);
	}

	inline static int32_t get_offset_of_infoController_3() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___infoController_3)); }
	inline MineInfoController_t2990208339 * get_infoController_3() const { return ___infoController_3; }
	inline MineInfoController_t2990208339 ** get_address_of_infoController_3() { return &___infoController_3; }
	inline void set_infoController_3(MineInfoController_t2990208339 * value)
	{
		___infoController_3 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_3, value);
	}

	inline static int32_t get_offset_of_gameCountdownTimer_4() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___gameCountdownTimer_4)); }
	inline LaunchCountdownTimer_t1840221825 * get_gameCountdownTimer_4() const { return ___gameCountdownTimer_4; }
	inline LaunchCountdownTimer_t1840221825 ** get_address_of_gameCountdownTimer_4() { return &___gameCountdownTimer_4; }
	inline void set_gameCountdownTimer_4(LaunchCountdownTimer_t1840221825 * value)
	{
		___gameCountdownTimer_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameCountdownTimer_4, value);
	}

	inline static int32_t get_offset_of_timerAlphaTween_5() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___timerAlphaTween_5)); }
	inline TweenAlpha_t2421518635 * get_timerAlphaTween_5() const { return ___timerAlphaTween_5; }
	inline TweenAlpha_t2421518635 ** get_address_of_timerAlphaTween_5() { return &___timerAlphaTween_5; }
	inline void set_timerAlphaTween_5(TweenAlpha_t2421518635 * value)
	{
		___timerAlphaTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___timerAlphaTween_5, value);
	}

	inline static int32_t get_offset_of_swipes_6() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___swipes_6)); }
	inline List_1_t2324820293 * get_swipes_6() const { return ___swipes_6; }
	inline List_1_t2324820293 ** get_address_of_swipes_6() { return &___swipes_6; }
	inline void set_swipes_6(List_1_t2324820293 * value)
	{
		___swipes_6 = value;
		Il2CppCodeGenWriteBarrier(&___swipes_6, value);
	}

	inline static int32_t get_offset_of_mineAnimation_7() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___mineAnimation_7)); }
	inline MultiImageNGUIAnimation_t45560503 * get_mineAnimation_7() const { return ___mineAnimation_7; }
	inline MultiImageNGUIAnimation_t45560503 ** get_address_of_mineAnimation_7() { return &___mineAnimation_7; }
	inline void set_mineAnimation_7(MultiImageNGUIAnimation_t45560503 * value)
	{
		___mineAnimation_7 = value;
		Il2CppCodeGenWriteBarrier(&___mineAnimation_7, value);
	}

	inline static int32_t get_offset_of_closingUI_8() { return static_cast<int32_t>(offsetof(MineMiniGameController_t2791970086, ___closingUI_8)); }
	inline bool get_closingUI_8() const { return ___closingUI_8; }
	inline bool* get_address_of_closingUI_8() { return &___closingUI_8; }
	inline void set_closingUI_8(bool value)
	{
		___closingUI_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
