﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<DeactivateUserKnockout>c__AnonStorey20
struct U3CDeactivateUserKnockoutU3Ec__AnonStorey20_t2295932339;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<DeactivateUserKnockout>c__AnonStorey20::.ctor()
extern "C"  void U3CDeactivateUserKnockoutU3Ec__AnonStorey20__ctor_m4004906592 (U3CDeactivateUserKnockoutU3Ec__AnonStorey20_t2295932339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<DeactivateUserKnockout>c__AnonStorey20::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeactivateUserKnockoutU3Ec__AnonStorey20_U3CU3Em__0_m1629172507 (U3CDeactivateUserKnockoutU3Ec__AnonStorey20_t2295932339 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
