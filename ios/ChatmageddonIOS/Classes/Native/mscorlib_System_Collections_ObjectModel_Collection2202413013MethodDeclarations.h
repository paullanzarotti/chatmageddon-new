﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>
struct Collection_1_t2202413013;
// System.Collections.Generic.IList`1<PlayerProfileNavScreen>
struct IList_1_t3201608860;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PlayerProfileNavScreen[]
struct PlayerProfileNavScreenU5BU5D_t3066215346;
// System.Collections.Generic.IEnumerator`1<PlayerProfileNavScreen>
struct IEnumerator_1_t136192086;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"

// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3782481280_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3782481280(__this, method) ((  void (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1__ctor_m3782481280_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m36587975_gshared (Collection_1_t2202413013 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m36587975(__this, ___list0, method) ((  void (*) (Collection_1_t2202413013 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m36587975_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3120275837_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3120275837(__this, method) ((  bool (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3120275837_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2275632176_gshared (Collection_1_t2202413013 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2275632176(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2202413013 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2275632176_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2097080661_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2097080661(__this, method) ((  Il2CppObject * (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2097080661_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m96653228_gshared (Collection_1_t2202413013 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m96653228(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2202413013 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m96653228_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1821872470_gshared (Collection_1_t2202413013 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1821872470(__this, ___value0, method) ((  bool (*) (Collection_1_t2202413013 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1821872470_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3380953722_gshared (Collection_1_t2202413013 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3380953722(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2202413013 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3380953722_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m983221561_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m983221561(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m983221561_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m259683833_gshared (Collection_1_t2202413013 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m259683833(__this, ___value0, method) ((  void (*) (Collection_1_t2202413013 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m259683833_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m966281560_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m966281560(__this, method) ((  bool (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m966281560_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m17511750_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17511750(__this, method) ((  Il2CppObject * (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17511750_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1640188629_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1640188629(__this, method) ((  bool (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1640188629_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3915843308_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3915843308(__this, method) ((  bool (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3915843308_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m45715449_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m45715449(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m45715449_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3637335238_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3637335238(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3637335238_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m4192854933_gshared (Collection_1_t2202413013 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m4192854933(__this, ___item0, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_Add_m4192854933_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m2813731313_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2813731313(__this, method) ((  void (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_Clear_m2813731313_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2606652051_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2606652051(__this, method) ((  void (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_ClearItems_m2606652051_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m1527092567_gshared (Collection_1_t2202413013 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1527092567(__this, ___item0, method) ((  bool (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_Contains_m1527092567_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3365153669_gshared (Collection_1_t2202413013 * __this, PlayerProfileNavScreenU5BU5D_t3066215346* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3365153669(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2202413013 *, PlayerProfileNavScreenU5BU5D_t3066215346*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3365153669_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2636276168_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2636276168(__this, method) ((  Il2CppObject* (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_GetEnumerator_m2636276168_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2515906997_gshared (Collection_1_t2202413013 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2515906997(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2515906997_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m919628894_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m919628894(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m919628894_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2414269607_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2414269607(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2414269607_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m125536760_gshared (Collection_1_t2202413013 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m125536760(__this, ___item0, method) ((  bool (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_Remove_m125536760_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2735707642_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2735707642(__this, ___index0, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2735707642_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4266882536_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4266882536(__this, ___index0, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4266882536_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m60783708_gshared (Collection_1_t2202413013 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m60783708(__this, method) ((  int32_t (*) (Collection_1_t2202413013 *, const MethodInfo*))Collection_1_get_Count_m60783708_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m114758790_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m114758790(__this, ___index0, method) ((  int32_t (*) (Collection_1_t2202413013 *, int32_t, const MethodInfo*))Collection_1_get_Item_m114758790_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1324487557_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1324487557(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1324487557_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3311936774_gshared (Collection_1_t2202413013 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3311936774(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2202413013 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m3311936774_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m4046262299_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m4046262299(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m4046262299_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m2933751183_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2933751183(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2933751183_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m61445415_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m61445415(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m61445415_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1457277375_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1457277375(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1457277375_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayerProfileNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3708606714_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3708606714(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3708606714_gshared)(__this /* static, unused */, ___list0, method)
