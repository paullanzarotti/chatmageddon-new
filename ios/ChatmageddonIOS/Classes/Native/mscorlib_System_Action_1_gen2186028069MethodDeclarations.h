﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<ChatMessage>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3850570551(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2186028069 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2192865289_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<ChatMessage>::Invoke(T)
#define Action_1_Invoke_m2741120923(__this, ___obj0, method) ((  void (*) (Action_1_t2186028069 *, ChatMessage_t2384228687 *, const MethodInfo*))Action_1_Invoke_m3960033342_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<ChatMessage>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m872173206(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2186028069 *, ChatMessage_t2384228687 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<ChatMessage>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m936379993(__this, ___result0, method) ((  void (*) (Action_1_t2186028069 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
