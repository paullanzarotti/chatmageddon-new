﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemInfoController
struct ItemInfoController_t4231956141;
// ItemInfoController/OnPopulationComplete
struct OnPopulationComplete_t1301655353;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ItemInfoController_OnPopulationC1301655353.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ItemInfoController::.ctor()
extern "C"  void ItemInfoController__ctor_m460587034 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::add_onPopulationComplete(ItemInfoController/OnPopulationComplete)
extern "C"  void ItemInfoController_add_onPopulationComplete_m607505002 (ItemInfoController_t4231956141 * __this, OnPopulationComplete_t1301655353 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::remove_onPopulationComplete(ItemInfoController/OnPopulationComplete)
extern "C"  void ItemInfoController_remove_onPopulationComplete_m2857469909 (ItemInfoController_t4231956141 * __this, OnPopulationComplete_t1301655353 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::Awake()
extern "C"  void ItemInfoController_Awake_m4128989597 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::OnEnable()
extern "C"  void ItemInfoController_OnEnable_m2610806490 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::OnDisable()
extern "C"  void ItemInfoController_OnDisable_m69607429 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::SetTitle(System.Int32,System.String)
extern "C"  void ItemInfoController_SetTitle_m2546138765 (ItemInfoController_t4231956141 * __this, int32_t ___index0, String_t* ___titleString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::SetDescription(System.Int32,System.String)
extern "C"  void ItemInfoController_SetDescription_m718462827 (ItemInfoController_t4231956141 * __this, int32_t ___index0, String_t* ___descriptionString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::ResetStrings()
extern "C"  void ItemInfoController_ResetStrings_m1679259103 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::ResetLabels()
extern "C"  void ItemInfoController_ResetLabels_m1300064650 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::PopulateLabels(System.Single)
extern "C"  void ItemInfoController_PopulateLabels_m2878159630 (ItemInfoController_t4231956141 * __this, float ___startWaitTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ItemInfoController::WaitForSeconds(System.Single,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * ItemInfoController_WaitForSeconds_m384362465 (ItemInfoController_t4231956141 * __this, float ___waitTime0, Action_1_t3627374100 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::UpdateNextLabel()
extern "C"  void ItemInfoController_UpdateNextLabel_m3049092160 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::StopLabelPopulation()
extern "C"  void ItemInfoController_StopLabelPopulation_m1445478313 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::UpdateLabel()
extern "C"  void ItemInfoController_UpdateLabel_m3235012735 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::SendPopulationCompleteEvent()
extern "C"  void ItemInfoController_SendPopulationCompleteEvent_m3349572076 (ItemInfoController_t4231956141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::<PopulateLabels>m__0(System.Boolean)
extern "C"  void ItemInfoController_U3CPopulateLabelsU3Em__0_m1821228999 (Il2CppObject * __this /* static, unused */, bool ___waited0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController::<UpdateNextLabel>m__1(System.Boolean)
extern "C"  void ItemInfoController_U3CUpdateNextLabelU3Em__1_m1902398305 (ItemInfoController_t4231956141 * __this, bool ___waited0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
