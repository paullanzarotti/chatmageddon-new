﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookComboUI
struct FacebookComboUI_t2562467258;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Prime31.FacebookFriendsResult
struct FacebookFriendsResult_t459691178;
// Prime31.FacebookMeResult
struct FacebookMeResult_t187696501;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookFrien459691178.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookMeRes187696501.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void Prime31.FacebookComboUI::.ctor()
extern "C"  void FacebookComboUI__ctor_m126457262 (FacebookComboUI_t2562467258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::completionHandler(System.String,System.Object)
extern "C"  void FacebookComboUI_completionHandler_m3051661912 (FacebookComboUI_t2562467258 * __this, String_t* ___error0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::Start()
extern "C"  void FacebookComboUI_Start_m3130996642 (FacebookComboUI_t2562467258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::OnGUI()
extern "C"  void FacebookComboUI_OnGUI_m3119690738 (FacebookComboUI_t2562467258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::secondColumnButtonsGUI()
extern "C"  void FacebookComboUI_secondColumnButtonsGUI_m3888229890 (FacebookComboUI_t2562467258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::.cctor()
extern "C"  void FacebookComboUI__cctor_m1208961081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::<Start>m__0(System.Object)
extern "C"  void FacebookComboUI_U3CStartU3Em__0_m2924505323 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::<OnGUI>m__1(System.Boolean)
extern "C"  void FacebookComboUI_U3COnGUIU3Em__1_m603097817 (Il2CppObject * __this /* static, unused */, bool ___isValid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::<secondColumnButtonsGUI>m__2(System.Boolean)
extern "C"  void FacebookComboUI_U3CsecondColumnButtonsGUIU3Em__2_m705011088 (Il2CppObject * __this /* static, unused */, bool ___didPost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::<secondColumnButtonsGUI>m__3(System.String,Prime31.FacebookFriendsResult)
extern "C"  void FacebookComboUI_U3CsecondColumnButtonsGUIU3Em__3_m617656067 (Il2CppObject * __this /* static, unused */, String_t* ___error0, FacebookFriendsResult_t459691178 * ___friends1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::<secondColumnButtonsGUI>m__4(System.String,Prime31.FacebookMeResult)
extern "C"  void FacebookComboUI_U3CsecondColumnButtonsGUIU3Em__4_m2367741985 (FacebookComboUI_t2562467258 * __this, String_t* ___error0, FacebookMeResult_t187696501 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookComboUI::<secondColumnButtonsGUI>m__5(UnityEngine.Texture2D)
extern "C"  void FacebookComboUI_U3CsecondColumnButtonsGUIU3Em__5_m2685009598 (FacebookComboUI_t2562467258 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
