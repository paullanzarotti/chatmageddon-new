﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.Hook
struct Hook_t3980190809;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_HookAction2799868295.h"

// System.String System.Xml.Serialization.Hook::GetCode(System.Xml.Serialization.HookAction)
extern "C"  String_t* Hook_GetCode_m3398723085 (Hook_t3980190809 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
