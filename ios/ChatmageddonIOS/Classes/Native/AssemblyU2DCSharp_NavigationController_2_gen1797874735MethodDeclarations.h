﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen63285209MethodDeclarations.h"

// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::.ctor()
#define NavigationController_2__ctor_m1768800897(__this, method) ((  void (*) (NavigationController_2_t1797874735 *, const MethodInfo*))NavigationController_2__ctor_m254826646_gshared)(__this, method)
// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m1174511667(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t1797874735 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m2217178148_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m3680676204(__this, method) ((  void (*) (NavigationController_2_t1797874735 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m2984498779_gshared)(__this, method)
// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3751010435(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t1797874735 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3329785178_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<FriendsNavigationController,FriendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m3025427767(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t1797874735 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m710670682_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<FriendsNavigationController,FriendNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m3162765266(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t1797874735 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m635772033_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m3087224243(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t1797874735 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1954343124_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m1564037991(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t1797874735 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2758172006_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<FriendsNavigationController,FriendNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m3464466107(__this, ___screen0, method) ((  void (*) (NavigationController_2_t1797874735 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1720819378_gshared)(__this, ___screen0, method)
