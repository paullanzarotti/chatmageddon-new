﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoPointsFacebookButton
struct NoPointsFacebookButton_t2783514292;

#include "codegen/il2cpp-codegen.h"

// System.Void NoPointsFacebookButton::.ctor()
extern "C"  void NoPointsFacebookButton__ctor_m2813826355 (NoPointsFacebookButton_t2783514292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsFacebookButton::OnButtonClick()
extern "C"  void NoPointsFacebookButton_OnButtonClick_m499163576 (NoPointsFacebookButton_t2783514292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
