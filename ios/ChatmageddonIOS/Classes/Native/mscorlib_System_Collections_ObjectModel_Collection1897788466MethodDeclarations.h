﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>
struct Collection_1_t1897788466;
// System.Collections.Generic.IList`1<LeaderboardNavScreen>
struct IList_1_t2896984313;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// LeaderboardNavScreen[]
struct LeaderboardNavScreenU5BU5D_t2088986945;
// System.Collections.Generic.IEnumerator`1<LeaderboardNavScreen>
struct IEnumerator_1_t4126534835;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"

// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m2119581609_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2119581609(__this, method) ((  void (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1__ctor_m2119581609_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1471101262_gshared (Collection_1_t1897788466 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1471101262(__this, ___list0, method) ((  void (*) (Collection_1_t1897788466 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1471101262_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1488973168_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1488973168(__this, method) ((  bool (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1488973168_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2925737593_gshared (Collection_1_t1897788466 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2925737593(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1897788466 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2925737593_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m331597256_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m331597256(__this, method) ((  Il2CppObject * (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m331597256_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m678868633_gshared (Collection_1_t1897788466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m678868633(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1897788466 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m678868633_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3875515385_gshared (Collection_1_t1897788466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3875515385(__this, ___value0, method) ((  bool (*) (Collection_1_t1897788466 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3875515385_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3524963687_gshared (Collection_1_t1897788466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3524963687(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1897788466 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3524963687_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2007185410_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2007185410(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2007185410_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1340272912_gshared (Collection_1_t1897788466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1340272912(__this, ___value0, method) ((  void (*) (Collection_1_t1897788466 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1340272912_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4082096789_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4082096789(__this, method) ((  bool (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4082096789_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2961550493_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2961550493(__this, method) ((  Il2CppObject * (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2961550493_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3726469250_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3726469250(__this, method) ((  bool (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3726469250_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2302018553_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2302018553(__this, method) ((  bool (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2302018553_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1542625120_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1542625120(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1542625120_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m4281608527_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m4281608527(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m4281608527_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m1846119358_gshared (Collection_1_t1897788466 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1846119358(__this, ___item0, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_Add_m1846119358_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m413990522_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_Clear_m413990522(__this, method) ((  void (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_Clear_m413990522_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1712821708_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1712821708(__this, method) ((  void (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_ClearItems_m1712821708_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m1369130436_gshared (Collection_1_t1897788466 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1369130436(__this, ___item0, method) ((  bool (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_Contains_m1369130436_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2856223262_gshared (Collection_1_t1897788466 * __this, LeaderboardNavScreenU5BU5D_t2088986945* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2856223262(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1897788466 *, LeaderboardNavScreenU5BU5D_t2088986945*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2856223262_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m992239585_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m992239585(__this, method) ((  Il2CppObject* (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_GetEnumerator_m992239585_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2655308456_gshared (Collection_1_t1897788466 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2655308456(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2655308456_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1079593559_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1079593559(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1079593559_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3605653264_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3605653264(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m3605653264_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m3403415947_gshared (Collection_1_t1897788466 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3403415947(__this, ___item0, method) ((  bool (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_Remove_m3403415947_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3076496579_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3076496579(__this, ___index0, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3076496579_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3552059743_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3552059743(__this, ___index0, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3552059743_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2513156249_gshared (Collection_1_t1897788466 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2513156249(__this, method) ((  int32_t (*) (Collection_1_t1897788466 *, const MethodInfo*))Collection_1_get_Count_m2513156249_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m110166143_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m110166143(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1897788466 *, int32_t, const MethodInfo*))Collection_1_get_Item_m110166143_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m999739084_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m999739084(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m999739084_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2687271871_gshared (Collection_1_t1897788466 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2687271871(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1897788466 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m2687271871_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1766854974_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1766854974(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1766854974_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m4012362934_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4012362934(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4012362934_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2587432750_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2587432750(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2587432750_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3451494636_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3451494636(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3451494636_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<LeaderboardNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2418671447_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2418671447(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2418671447_gshared)(__this /* static, unused */, ___list0, method)
