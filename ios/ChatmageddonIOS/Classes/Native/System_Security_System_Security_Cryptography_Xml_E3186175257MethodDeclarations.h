﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptedKey
struct EncryptedKey_t3186175257;
// System.String
struct String_t;
// System.Security.Cryptography.Xml.ReferenceList
struct ReferenceList_t1582251325;
// System.Security.Cryptography.Xml.DataReference
struct DataReference_t2617499975;
// System.Security.Cryptography.Xml.KeyReference
struct KeyReference_t1620770554;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Security_System_Security_Cryptography_Xml_D2617499975.h"
#include "System_Security_System_Security_Cryptography_Xml_K1620770554.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.EncryptedKey::.ctor()
extern "C"  void EncryptedKey__ctor_m859917415 (EncryptedKey_t3186175257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedKey::get_CarriedKeyName()
extern "C"  String_t* EncryptedKey_get_CarriedKeyName_m551872021 (EncryptedKey_t3186175257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedKey::set_CarriedKeyName(System.String)
extern "C"  void EncryptedKey_set_CarriedKeyName_m3227857202 (EncryptedKey_t3186175257 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedKey::get_Recipient()
extern "C"  String_t* EncryptedKey_get_Recipient_m257743374 (EncryptedKey_t3186175257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedKey::set_Recipient(System.String)
extern "C"  void EncryptedKey_set_Recipient_m139952751 (EncryptedKey_t3186175257 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.ReferenceList System.Security.Cryptography.Xml.EncryptedKey::get_ReferenceList()
extern "C"  ReferenceList_t1582251325 * EncryptedKey_get_ReferenceList_m4135608257 (EncryptedKey_t3186175257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedKey::AddReference(System.Security.Cryptography.Xml.DataReference)
extern "C"  void EncryptedKey_AddReference_m415684308 (EncryptedKey_t3186175257 * __this, DataReference_t2617499975 * ___dataReference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedKey::AddReference(System.Security.Cryptography.Xml.KeyReference)
extern "C"  void EncryptedKey_AddReference_m3735201433 (EncryptedKey_t3186175257 * __this, KeyReference_t1620770554 * ___keyReference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptedKey::GetXml()
extern "C"  XmlElement_t2877111883 * EncryptedKey_GetXml_m2689895358 (EncryptedKey_t3186175257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptedKey::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * EncryptedKey_GetXml_m324369850 (EncryptedKey_t3186175257 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedKey::LoadXml(System.Xml.XmlElement)
extern "C"  void EncryptedKey_LoadXml_m1413458743 (EncryptedKey_t3186175257 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
