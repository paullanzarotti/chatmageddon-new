﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>
struct ValueCollection_t3281180659;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct Dictionary_2_t283153520;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1969686284.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1130550637_gshared (ValueCollection_t3281180659 * __this, Dictionary_2_t283153520 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1130550637(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3281180659 *, Dictionary_2_t283153520 *, const MethodInfo*))ValueCollection__ctor_m1130550637_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m217425835_gshared (ValueCollection_t3281180659 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m217425835(__this, ___item0, method) ((  void (*) (ValueCollection_t3281180659 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m217425835_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2289377654_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2289377654(__this, method) ((  void (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2289377654_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m366745989_gshared (ValueCollection_t3281180659 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m366745989(__this, ___item0, method) ((  bool (*) (ValueCollection_t3281180659 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m366745989_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m851237364_gshared (ValueCollection_t3281180659 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m851237364(__this, ___item0, method) ((  bool (*) (ValueCollection_t3281180659 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m851237364_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3711002716_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3711002716(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3711002716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3834982446_gshared (ValueCollection_t3281180659 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3834982446(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3281180659 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3834982446_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m462519307_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m462519307(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m462519307_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1697003576_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1697003576(__this, method) ((  bool (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1697003576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1852695594_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1852695594(__this, method) ((  bool (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1852695594_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1586717140_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1586717140(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1586717140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1062615890_gshared (ValueCollection_t3281180659 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1062615890(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3281180659 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1062615890_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1969686284  ValueCollection_GetEnumerator_m978813709_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m978813709(__this, method) ((  Enumerator_t1969686284  (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_GetEnumerator_m978813709_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2397638386_gshared (ValueCollection_t3281180659 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2397638386(__this, method) ((  int32_t (*) (ValueCollection_t3281180659 *, const MethodInfo*))ValueCollection_get_Count_m2397638386_gshared)(__this, method)
