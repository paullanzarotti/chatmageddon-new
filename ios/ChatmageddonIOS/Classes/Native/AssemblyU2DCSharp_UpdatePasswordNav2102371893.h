﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdatePasswordNav
struct  UpdatePasswordNav_t2102371893  : public NavigationScreen_t2333230110
{
public:
	// UIInput UpdatePasswordNav::currentPassword
	UIInput_t860674234 * ___currentPassword_3;
	// UIInput UpdatePasswordNav::newPassword
	UIInput_t860674234 * ___newPassword_4;
	// UIInput UpdatePasswordNav::confirmPassword
	UIInput_t860674234 * ___confirmPassword_5;
	// System.Boolean UpdatePasswordNav::UIclosing
	bool ___UIclosing_6;

public:
	inline static int32_t get_offset_of_currentPassword_3() { return static_cast<int32_t>(offsetof(UpdatePasswordNav_t2102371893, ___currentPassword_3)); }
	inline UIInput_t860674234 * get_currentPassword_3() const { return ___currentPassword_3; }
	inline UIInput_t860674234 ** get_address_of_currentPassword_3() { return &___currentPassword_3; }
	inline void set_currentPassword_3(UIInput_t860674234 * value)
	{
		___currentPassword_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentPassword_3, value);
	}

	inline static int32_t get_offset_of_newPassword_4() { return static_cast<int32_t>(offsetof(UpdatePasswordNav_t2102371893, ___newPassword_4)); }
	inline UIInput_t860674234 * get_newPassword_4() const { return ___newPassword_4; }
	inline UIInput_t860674234 ** get_address_of_newPassword_4() { return &___newPassword_4; }
	inline void set_newPassword_4(UIInput_t860674234 * value)
	{
		___newPassword_4 = value;
		Il2CppCodeGenWriteBarrier(&___newPassword_4, value);
	}

	inline static int32_t get_offset_of_confirmPassword_5() { return static_cast<int32_t>(offsetof(UpdatePasswordNav_t2102371893, ___confirmPassword_5)); }
	inline UIInput_t860674234 * get_confirmPassword_5() const { return ___confirmPassword_5; }
	inline UIInput_t860674234 ** get_address_of_confirmPassword_5() { return &___confirmPassword_5; }
	inline void set_confirmPassword_5(UIInput_t860674234 * value)
	{
		___confirmPassword_5 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPassword_5, value);
	}

	inline static int32_t get_offset_of_UIclosing_6() { return static_cast<int32_t>(offsetof(UpdatePasswordNav_t2102371893, ___UIclosing_6)); }
	inline bool get_UIclosing_6() const { return ___UIclosing_6; }
	inline bool* get_address_of_UIclosing_6() { return &___UIclosing_6; }
	inline void set_UIclosing_6(bool value)
	{
		___UIclosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
