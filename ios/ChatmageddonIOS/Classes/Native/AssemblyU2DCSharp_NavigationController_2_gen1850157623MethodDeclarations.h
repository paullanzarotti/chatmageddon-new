﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen906290919MethodDeclarations.h"

// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::.ctor()
#define NavigationController_2__ctor_m736117789(__this, method) ((  void (*) (NavigationController_2_t1850157623 *, const MethodInfo*))NavigationController_2__ctor_m2342486618_gshared)(__this, method)
// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m3991330219(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t1850157623 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1017759524_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m1727429716(__this, method) ((  void (*) (NavigationController_2_t1850157623 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m182819309_gshared)(__this, method)
// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3108722003(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t1850157623 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1335549778_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m1921353671(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t1850157623 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m670594270_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m590538926(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t1850157623 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m901421583_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m586552331(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t1850157623 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1791692064_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m2477947951(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t1850157623 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m937344406_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<FriendsSearchNavigationController,FriendSearchNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m983918843(__this, ___screen0, method) ((  void (*) (NavigationController_2_t1850157623 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2780911014_gshared)(__this, ___screen0, method)
