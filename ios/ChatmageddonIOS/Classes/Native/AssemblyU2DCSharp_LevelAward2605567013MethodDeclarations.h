﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LevelAward
struct LevelAward_t2605567013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LevelAward::.ctor(System.Int32,System.String,System.Int32)
extern "C"  void LevelAward__ctor_m3976876206 (LevelAward_t2605567013 * __this, int32_t ___level0, String_t* ___name1, int32_t ___bonus2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
