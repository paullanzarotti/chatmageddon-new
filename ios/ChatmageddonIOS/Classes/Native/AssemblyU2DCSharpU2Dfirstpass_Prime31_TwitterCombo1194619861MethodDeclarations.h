﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.TwitterCombo::.cctor()
extern "C"  void TwitterCombo__cctor_m1053670156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::init(System.String,System.String)
extern "C"  void TwitterCombo_init_m731007801 (Il2CppObject * __this /* static, unused */, String_t* ___consumerKey0, String_t* ___consumerSecret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.TwitterCombo::isLoggedIn()
extern "C"  bool TwitterCombo_isLoggedIn_m792835754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.TwitterCombo::loggedInUsername()
extern "C"  String_t* TwitterCombo_loggedInUsername_m298692365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::showLoginDialog()
extern "C"  void TwitterCombo_showLoginDialog_m2120791039 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::logout()
extern "C"  void TwitterCombo_logout_m4074114605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::postStatusUpdate(System.String)
extern "C"  void TwitterCombo_postStatusUpdate_m3784665366 (Il2CppObject * __this /* static, unused */, String_t* ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::postStatusUpdate(System.String,System.String)
extern "C"  void TwitterCombo_postStatusUpdate_m179367458 (Il2CppObject * __this /* static, unused */, String_t* ___status0, String_t* ___pathToImage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::performRequest(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void TwitterCombo_performRequest_m3191366842 (Il2CppObject * __this /* static, unused */, String_t* ___methodType0, String_t* ___path1, Dictionary_2_t3943999495 * ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterCombo::<TwitterCombo>m__0(System.String)
extern "C"  void TwitterCombo_U3CTwitterComboU3Em__0_m146003595 (Il2CppObject * __this /* static, unused */, String_t* ___username0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
