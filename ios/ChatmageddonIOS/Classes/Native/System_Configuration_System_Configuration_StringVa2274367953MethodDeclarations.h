﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.StringValidatorAttribute
struct StringValidatorAttribute_t2274367953;
// System.String
struct String_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.StringValidatorAttribute::.ctor()
extern "C"  void StringValidatorAttribute__ctor_m3369096334 (StringValidatorAttribute_t2274367953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.StringValidatorAttribute::get_InvalidCharacters()
extern "C"  String_t* StringValidatorAttribute_get_InvalidCharacters_m2917099341 (StringValidatorAttribute_t2274367953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.StringValidatorAttribute::set_InvalidCharacters(System.String)
extern "C"  void StringValidatorAttribute_set_InvalidCharacters_m1969255240 (StringValidatorAttribute_t2274367953 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Configuration.StringValidatorAttribute::get_MaxLength()
extern "C"  int32_t StringValidatorAttribute_get_MaxLength_m2718417733 (StringValidatorAttribute_t2274367953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.StringValidatorAttribute::set_MaxLength(System.Int32)
extern "C"  void StringValidatorAttribute_set_MaxLength_m3091828830 (StringValidatorAttribute_t2274367953 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Configuration.StringValidatorAttribute::get_MinLength()
extern "C"  int32_t StringValidatorAttribute_get_MinLength_m3829611367 (StringValidatorAttribute_t2274367953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.StringValidatorAttribute::set_MinLength(System.Int32)
extern "C"  void StringValidatorAttribute_set_MinLength_m3952287424 (StringValidatorAttribute_t2274367953 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.StringValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * StringValidatorAttribute_get_ValidatorInstance_m3553236573 (StringValidatorAttribute_t2274367953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
