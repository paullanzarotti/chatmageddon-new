﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemSwipeController/OnLeftComplete
struct OnLeftComplete_t1529246307;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ItemSwipeController/OnLeftComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLeftComplete__ctor_m1618379286 (OnLeftComplete_t1529246307 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController/OnLeftComplete::Invoke()
extern "C"  void OnLeftComplete_Invoke_m4225175788 (OnLeftComplete_t1529246307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ItemSwipeController/OnLeftComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLeftComplete_BeginInvoke_m888672333 (OnLeftComplete_t1529246307 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController/OnLeftComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnLeftComplete_EndInvoke_m352917036 (OnLeftComplete_t1529246307 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
