﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// Uniject.IStorage
struct IStorage_t1347868490;
// RemoteConfigFetcher
struct RemoteConfigFetcher_t2652256879;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteConfigFetcher/<fetch>c__Iterator0
struct  U3CfetchU3Ec__Iterator0_t2771239707  : public Il2CppObject
{
public:
	// System.String RemoteConfigFetcher/<fetch>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW RemoteConfigFetcher/<fetch>c__Iterator0::<request>__0
	WWW_t2919945039 * ___U3CrequestU3E__0_1;
	// Uniject.IStorage RemoteConfigFetcher/<fetch>c__Iterator0::storage
	Il2CppObject * ___storage_2;
	// System.String RemoteConfigFetcher/<fetch>c__Iterator0::key
	String_t* ___key_3;
	// RemoteConfigFetcher RemoteConfigFetcher/<fetch>c__Iterator0::$this
	RemoteConfigFetcher_t2652256879 * ___U24this_4;
	// System.Object RemoteConfigFetcher/<fetch>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean RemoteConfigFetcher/<fetch>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 RemoteConfigFetcher/<fetch>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___U3CrequestU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_1, value);
	}

	inline static int32_t get_offset_of_storage_2() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___storage_2)); }
	inline Il2CppObject * get_storage_2() const { return ___storage_2; }
	inline Il2CppObject ** get_address_of_storage_2() { return &___storage_2; }
	inline void set_storage_2(Il2CppObject * value)
	{
		___storage_2 = value;
		Il2CppCodeGenWriteBarrier(&___storage_2, value);
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___key_3)); }
	inline String_t* get_key_3() const { return ___key_3; }
	inline String_t** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(String_t* value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier(&___key_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___U24this_4)); }
	inline RemoteConfigFetcher_t2652256879 * get_U24this_4() const { return ___U24this_4; }
	inline RemoteConfigFetcher_t2652256879 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(RemoteConfigFetcher_t2652256879 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CfetchU3Ec__Iterator0_t2771239707, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
