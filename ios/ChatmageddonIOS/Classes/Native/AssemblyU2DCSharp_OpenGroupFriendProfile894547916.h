﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GroupFriendUI
struct GroupFriendUI_t1634706847;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenGroupFriendProfile
struct  OpenGroupFriendProfile_t894547916  : public SFXButton_t792651341
{
public:
	// GroupFriendUI OpenGroupFriendProfile::ui
	GroupFriendUI_t1634706847 * ___ui_5;

public:
	inline static int32_t get_offset_of_ui_5() { return static_cast<int32_t>(offsetof(OpenGroupFriendProfile_t894547916, ___ui_5)); }
	inline GroupFriendUI_t1634706847 * get_ui_5() const { return ___ui_5; }
	inline GroupFriendUI_t1634706847 ** get_address_of_ui_5() { return &___ui_5; }
	inline void set_ui_5(GroupFriendUI_t1634706847 * value)
	{
		___ui_5 = value;
		Il2CppCodeGenWriteBarrier(&___ui_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
