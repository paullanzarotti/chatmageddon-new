﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UIViewport
struct UIViewport_t1541362616;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Object
struct Il2CppObject;
// UIWidget
struct UIWidget_t1453041918;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2690968101;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t948534547;
// UnityEngine.Transform
struct Transform_t3275118058;
// UIPanel
struct UIPanel_t1795085332;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t2464096222;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t2464096221;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t1094906160;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t2464096223;
// UIWidget/HitCheck
struct HitCheck_t3590501724;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t3620741577;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t836279582;
// UIWidgetContainer
struct UIWidgetContainer_t701016325;
// UIWrapContent
struct UIWrapContent_t1931723019;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIScrollView
struct UIScrollView_t3033954930;
// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t2948699028;
// UnibillManager
struct UnibillManager_t3725383138;
// PurchaseEvent
struct PurchaseEvent_t743554429;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;
// UniversalFeed
struct UniversalFeed_t2144776909;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// UnloadLogoutUIButton
struct UnloadLogoutUIButton_t2627629715;
// UpdateActiveLeaderboardButton
struct UpdateActiveLeaderboardButton_t3273818106;
// UpdatePasswordNav
struct UpdatePasswordNav_t2102371893;
// TweenPosition
struct TweenPosition_t1144714832;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// UpdatePasswordNav/<SaveScreenContent>c__Iterator0
struct U3CSaveScreenContentU3Ec__Iterator0_t2254993109;
// UpdatePasswordNav/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1
struct U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170;
// UpdatePasswordUIScaler
struct UpdatePasswordUIScaler_t4097640474;
// UploadStream
struct UploadStream_t108391441;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// User
struct User_t719925459;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// Shield
struct Shield_t3327121081;
// UserLoginButton
struct UserLoginButton_t3500827568;
// TweenScale
struct TweenScale_t2697902175;
// UsernameLoginButton
struct UsernameLoginButton_t2274508449;
// UserPayload
struct UserPayload_t146188169;
// UserRegistrationButton
struct UserRegistrationButton_t1399913110;
// ValidContact
struct ValidContact_t1479914934;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// VerificationCodeInput
struct VerificationCodeInput_t924401746;
// VerificationScreen
struct VerificationScreen_t1800753931;
// VerificationUIScaler
struct VerificationUIScaler_t217609153;
// VerifyCodeButton
struct VerifyCodeButton_t1959864684;
// VibrationController
struct VibrationController_t2976556198;
// VibrationController/<ContinuousVibrate>c__Iterator0
struct U3CContinuousVibrateU3Ec__Iterator0_t1905345410;
// VibrationController/<PulseVibrate>c__Iterator1
struct U3CPulseVibrateU3Ec__Iterator1_t3172871213;
// VibrationController/<SingleVibrate>c__Iterator2
struct U3CSingleVibrateU3Ec__Iterator2_t2980562147;
// VibrationController/<SwitchVibrate>c__Iterator3
struct U3CSwitchVibrateU3Ec__Iterator3_t2167946060;
// WebSocketSample
struct WebSocketSample_t3966400161;
// BestHTTP.WebSocket.WebSocket
struct WebSocket_t71448861;
// System.Exception
struct Exception_t1927440687;
// YNContinueButton
struct YNContinueButton_t318956450;
// YNContinueButton/<OnButtonClick>c__AnonStorey0
struct U3COnButtonClickU3Ec__AnonStorey0_t1978033145;
// YourNumberScreen
struct YourNumberScreen_t2760251358;
// LoginCenterUI
struct LoginCenterUI_t890505802;
// YourNumberUIScaler
struct YourNumberUIScaler_t1339804644;
// ZeroPointsUI
struct ZeroPointsUI_t1285366563;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_UIViewport1541362616.h"
#include "AssemblyU2DCSharp_UIViewport1541362616MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_UIWidget1453041918.h"
#include "AssemblyU2DCSharp_UIWidget1453041918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIGeometry1005900006MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIRect4127168124MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot752586349.h"
#include "AssemblyU2DCSharp_UIGeometry1005900006.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2690968101.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "AssemblyU2DCSharp_UIDrawCall3291843512.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_NGUIMath221371675MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource2759843449.h"
#include "AssemblyU2DCSharp_UIRect4127168124.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint4057058294.h"
#include "AssemblyU2DCSharp_NGUITools2004302824MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPanel1795085332MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPanel1795085332.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_BoxCollider22920061.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_BoxCollider2D948534547.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint4057058294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Bounds3033363703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3620741577MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3620741577.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2464096222.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2464096221.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1094906160.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2464096223.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource2759843449MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3590501724.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3590501724MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback836279582.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback836279582MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot752586349MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidgetContainer701016325.h"
#include "AssemblyU2DCSharp_UIWidgetContainer701016325MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent1931723019.h"
#include "AssemblyU2DCSharp_UIWrapContent1931723019MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved4045505957MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIScrollView3033954930.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved4045505957.h"
#include "mscorlib_System_Comparison_1_gen241889613MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen241889613.h"
#include "AssemblyU2DCSharp_UIGrid2420180906.h"
#include "AssemblyU2DCSharp_UIGrid2420180906MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools2004302824.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement1682624982.h"
#include "AssemblyU2DCSharp_UICamera1496819779MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2948699028MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2948699028.h"
#include "AssemblyU2DCSharp_UnibillManager3725383138.h"
#include "AssemblyU2DCSharp_UnibillManager3725383138MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3476048858MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen345140658MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1865222972MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonServer594474938MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonServer594474938.h"
#include "System_Core_System_Action_2_gen1865222972.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillState4272135008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseEvent743554429.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen971871211MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseEvent743554429MethodDeclarations.h"
#include "AssemblyU2DCSharp_BasePurchaseManager_2_gen2324820235MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonPurchaseManager1221205491.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibiller1392804696MethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4073934390MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen545353811MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3765153281MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4073934390.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen545353811.h"
#include "mscorlib_System_Action_1_gen3765153281.h"
#include "mscorlib_System_Collections_Generic_List_1_gen888775120.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"
#include "AssemblyU2DCSharp_UniversalFeed2144776909.h"
#include "AssemblyU2DCSharp_UniversalFeed2144776909MethodDeclarations.h"
#include "AssemblyU2DCSharp_Feed3408144164MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen1990368447MethodDeclarations.h"
#include "AssemblyU2DCSharp_ConfigManager2239702727.h"
#include "AssemblyU2DCSharp_Feed3408144164.h"
#include "System_Core_System_Action_3_gen3681841185MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "System_Core_System_Action_3_gen3681841185.h"
#include "AssemblyU2DCSharp_JSON3623987362MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_Hashtable909839986MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_UniversalUpdateType1629501440.h"
#include "AssemblyU2DCSharp_AutoFriendType103565391.h"
#include "AssemblyU2DCSharp_Friend3555014108MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen1347319308MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2924135240MethodDeclarations.h"
#include "AssemblyU2DCSharp_User719925459MethodDeclarations.h"
#include "AssemblyU2DCSharp_Player1147783557MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen4081773843MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendManager36140827MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3887179032MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen752959214MethodDeclarations.h"
#include "AssemblyU2DCSharp_LanguageModel2428340347MethodDeclarations.h"
#include "AssemblyU2DCSharp_LanguageConfig1338327304MethodDeclarations.h"
#include "AssemblyU2DCSharp_ToastManager1002293494MethodDeclarations.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_LanguageModel2428340347.h"
#include "AssemblyU2DCSharp_PlayerManager1596653588.h"
#include "AssemblyU2DCSharp_Player1147783557.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2924135240.h"
#include "AssemblyU2DCSharp_FriendManager36140827.h"
#include "AssemblyU2DCSharp_ModalManager4136513312.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_ToastManager1002293494.h"
#include "AssemblyU2DCSharp_LanguageConfig1338327304.h"
#include "AssemblyU2DCSharp_PlayerManager1596653588MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3226218845MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackManager3475553125MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackManager3475553125.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2543256415MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatManager2792590695MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatManager2792590695.h"
#include "AssemblyU2DCSharp_ChatListType844669534.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharp_BaseServer_1_gen2790028406MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_ModalType1526568337.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3873508672MethodDeclarations.h"
#include "AssemblyU2DCSharp_LockoutManager4122842952MethodDeclarations.h"
#include "AssemblyU2DCSharp_LockoutManager4122842952.h"
#include "AssemblyU2DCSharp_LockoutState173023818.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen1149681085MethodDeclarations.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_gen2526031859MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModalManager4136513312MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440931836MethodDeclarations.h"
#include "AssemblyU2DCSharp_HomeSceneManager2690266116MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonPanelTransitionManag1399015365.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_gen2526031859.h"
#include "AssemblyU2DCSharp_Panel1787746694.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "AssemblyU2DCSharp_HomeSceneManager2690266116.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen4094818176MethodDeclarations.h"
#include "AssemblyU2DCSharp_HeaderManager49185160MethodDeclarations.h"
#include "AssemblyU2DCSharp_HeaderManager49185160.h"
#include "AssemblyU2DCSharp_LevelAward2605567013MethodDeclarations.h"
#include "AssemblyU2DCSharp_LevelAward2605567013.h"
#include "AssemblyU2DCSharp_Shield3327121081MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2139140742MethodDeclarations.h"
#include "AssemblyU2DCSharp_NotificationManager2388475022MethodDeclarations.h"
#include "AssemblyU2DCSharp_NotificationManager2388475022.h"
#include "AssemblyU2DCSharp_Mine2729441277MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3344362265MethodDeclarations.h"
#include "AssemblyU2DCSharp_MapManager3593696545MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_MapManager3593696545.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen4083902911MethodDeclarations.h"
#include "AssemblyU2DCSharp_InventoryManager38269895MethodDeclarations.h"
#include "AssemblyU2DCSharp_InventoryManager38269895.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2572512622MethodDeclarations.h"
#include "AssemblyU2DCSharp_MineManager2821846902MethodDeclarations.h"
#include "AssemblyU2DCSharp_MineManager2821846902.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810MethodDeclarations.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen1861297139MethodDeclarations.h"
#include "AssemblyU2DCSharp_ApplicationManager2110631419MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2470912757MethodDeclarations.h"
#include "AssemblyU2DCSharp_StatusManager2720247037MethodDeclarations.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"
#include "AssemblyU2DCSharp_ApplicationManager2110631419.h"
#include "AssemblyU2DCSharp_StatusManager2720247037.h"
#include "AssemblyU2DCSharp_StatusListType2488921270.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3039755559MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3039755559.h"
#include "AssemblyU2DCSharp_LaunchedMine3677282773MethodDeclarations.h"
#include "AssemblyU2DCSharp_LaunchedMine3677282773.h"
#include "AssemblyU2DCSharp_UniversalUpdateType1629501440MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnloadLogoutUIButton2627629715.h"
#include "AssemblyU2DCSharp_UnloadLogoutUIButton2627629715MethodDeclarations.h"
#include "AssemblyU2DCSharp_SFXButton792651341MethodDeclarations.h"
#include "AssemblyU2DCSharp_LogOutTransition3259858399MethodDeclarations.h"
#include "AssemblyU2DCSharp_LogOutTransition3259858399.h"
#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_UpdateActiveLeaderboardButton3273818106.h"
#include "AssemblyU2DCSharp_UpdateActiveLeaderboardButton3273818106MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen1431643053MethodDeclarations.h"
#include "AssemblyU2DCSharp_LeaderboardNavigationController1680977333MethodDeclarations.h"
#include "AssemblyU2DCSharp_LeaderboardNavigationController1680977333.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav2102371893.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav2102371893MethodDeclarations.h"
#include "AssemblyU2DCSharp_NavigationScreen2333230110MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback2100910411MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener2986641582MethodDeclarations.h"
#include "AssemblyU2DCSharp_TweenPosition1144714832.h"
#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback2100910411.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "AssemblyU2DCSharp_UIInput860674234MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIInput860674234.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2332610177MethodDeclarations.h"
#include "AssemblyU2DCSharp_SettingsNavigationController2581944457.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav_U3CSaveScreenC2254993109MethodDeclarations.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav_U3CSaveScreenC2254993109.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav_U3CSaveScreenC1305955170MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen962131611MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorMessagePopUp1211465891MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav_U3CSaveScreenC1305955170.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_ErrorMessagePopUp1211465891.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"
#include "mscorlib_System_Nullable_1_gen339576247MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_UpdatePasswordUIScaler4097640474.h"
#include "AssemblyU2DCSharp_UpdatePasswordUIScaler4097640474MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider22920061MethodDeclarations.h"
#include "AssemblyU2DCSharp_BaseUIScaler568866473MethodDeclarations.h"
#include "AssemblyU2DCSharp_BaseUIScaler568866473.h"
#include "AssemblyU2DCSharp_UILabel1795115428.h"
#include "AssemblyU2DCSharp_UISprite603616735.h"
#include "AssemblyU2DCSharp_UploadStream108391441.h"
#include "AssemblyU2DCSharp_UploadStream108391441MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream743994179MethodDeclarations.h"
#include "mscorlib_System_Threading_AutoResetEvent15112628MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_Threading_AutoResetEvent15112628.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Byte3683104436.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPManager2983460817MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "AssemblyU2DCSharp_UserPayload146188169MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AssemblyU2DCSharp_BaseServer_1_gen2790028406.h"
#include "AssemblyU2DCSharp_UserPayload146188169.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2136529692MethodDeclarations.h"
#include "AssemblyU2DCSharp_FlurryController2385863972MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "AssemblyU2DCSharp_FlurryController2385863972.h"
#include "AssemblyU2DCSharp_ShieldType96263953.h"
#include "AssemblyU2DCSharp_UserLoginButton3500827568.h"
#include "AssemblyU2DCSharp_UserLoginButton3500827568MethodDeclarations.h"
#include "AssemblyU2DCSharp_TweenScale2697902175.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3488760729MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen559355580MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameState_1_gen3261017281MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginBottomUIManager3738095009.h"
#include "AssemblyU2DCSharp_UsernameLoginButton2274508449.h"
#include "AssemblyU2DCSharp_ChatmageddonGamestate808689860.h"
#include "AssemblyU2DCSharp_UsernameLoginButton2274508449MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserRegistrationButton1399913110.h"
#include "AssemblyU2DCSharp_UserRegistrationButton1399913110MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen1284182836MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen290431989MethodDeclarations.h"
#include "AssemblyU2DCSharp_RegistrationManager1533517116.h"
#include "AssemblyU2DCSharp_RegistrationNavigationcontroller539766269.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3798250245MethodDeclarations.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3798250245.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "AssemblyU2DCSharp_ValidContact1479914934.h"
#include "AssemblyU2DCSharp_ValidContact1479914934MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA512Manage3949709369MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA5122908163326.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Security_Cryptography_SHA512Manage3949709369.h"
#include "mscorlib_System_BitConverter3195628829MethodDeclarations.h"
#include "AssemblyU2DCSharp_AddressBook411411037MethodDeclarations.h"
#include "AssemblyU2DCSharp_VerificationCodeInput924401746.h"
#include "AssemblyU2DCSharp_VerificationCodeInput924401746MethodDeclarations.h"
#include "AssemblyU2DCSharp_VerificationScreen1800753931.h"
#include "AssemblyU2DCSharp_VerificationScreen1800753931MethodDeclarations.h"
#include "AssemblyU2DCSharp_UILabel1795115428MethodDeclarations.h"
#include "AssemblyU2DCSharp_VerificationUIScaler217609153.h"
#include "AssemblyU2DCSharp_VerificationUIScaler217609153MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen4068614118MethodDeclarations.h"
#include "AssemblyU2DCSharp_ContactsNaviagationController22981102.h"
#include "AssemblyU2DCSharp_VerifyCodeButton1959864684.h"
#include "AssemblyU2DCSharp_VerifyCodeButton1959864684MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate3496309181MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2865430313MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2865430313.h"
#include "AssemblyU2DCSharp_EventDelegate3496309181.h"
#include "AssemblyU2DCSharp_VibrationController2976556198.h"
#include "AssemblyU2DCSharp_VibrationController2976556198MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2727221918MethodDeclarations.h"
#include "AssemblyU2DCSharp_VibrationType1951301914.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_VibrationController_U3CContinuou1905345410MethodDeclarations.h"
#include "AssemblyU2DCSharp_VibrationController_U3CContinuou1905345410.h"
#include "AssemblyU2DCSharp_VibrationController_U3CPulseVibr3172871213MethodDeclarations.h"
#include "AssemblyU2DCSharp_VibrationController_U3CPulseVibr3172871213.h"
#include "AssemblyU2DCSharp_VibrationController_U3CSingleVib2980562147MethodDeclarations.h"
#include "AssemblyU2DCSharp_VibrationController_U3CSingleVib2980562147.h"
#include "AssemblyU2DCSharp_VibrationController_U3CSwitchVib2167946060MethodDeclarations.h"
#include "AssemblyU2DCSharp_VibrationController_U3CSwitchVib2167946060.h"
#include "UnityEngine_UnityEngine_Handheld4075775256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "AssemblyU2DCSharp_VibrationType1951301914MethodDeclarations.h"
#include "AssemblyU2DCSharp_WarefareState701596006.h"
#include "AssemblyU2DCSharp_WarefareState701596006MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSample3966400161.h"
#include "AssemblyU2DCSharp_WebSocketSample3966400161MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocket71448861MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocket71448861.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_GUIHelper1081137527MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_GUIHelper1081137527.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_UInt16986882611.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPProxy2644053826MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketOp4018547189MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketMes730459590MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketCl2679686585MethodDeclarations.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketEr1328789641MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "System_System_Uri19570940.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPProxy2644053826.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Credenti3762395084.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketOp4018547189.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketMes730459590.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketCl2679686585.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketEr1328789641.h"
#include "AssemblyU2DCSharp_YNContinueButton318956450.h"
#include "AssemblyU2DCSharp_YNContinueButton318956450MethodDeclarations.h"
#include "AssemblyU2DCSharp_NavigateForwardButton3075171100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "AssemblyU2DCSharp_YNContinueButton_U3COnButtonClic1978033145MethodDeclarations.h"
#include "AssemblyU2DCSharp_YNContinueButton_U3COnButtonClic1978033145.h"
#include "AssemblyU2DCSharp_NavigateForwardButton3075171100.h"
#include "System_System_Text_RegularExpressions_Regex1803876613MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_MatchCollect3718216671MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Text_RegularExpressions_MatchCollect3718216671.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "AssemblyU2DCSharp_FlurryEvent3996959420.h"
#include "AssemblyU2DCSharp_YourNumberScreen2760251358.h"
#include "AssemblyU2DCSharp_YourNumberScreen2760251358MethodDeclarations.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2078169705MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginCenterUI890505802MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginRegistrationSceneManager2327503985.h"
#include "AssemblyU2DCSharp_LoginCenterUI890505802.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen3786716596MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonSaveData4036050876MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatmageddonSaveData4036050876.h"
#include "AssemblyU2DCSharp_YourNumberUIScaler1339804644.h"
#include "AssemblyU2DCSharp_YourNumberUIScaler1339804644MethodDeclarations.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2069092468.h"
#include "AssemblyU2DCSharp_ZeroPointsUI1285366563.h"
#include "AssemblyU2DCSharp_ZeroPointsUI1285366563MethodDeclarations.h"
#include "AssemblyU2DCSharp_LockoutUI1337294023MethodDeclarations.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton_OnZeroHit3971658986MethodDeclarations.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton1249750581MethodDeclarations.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton1249750581.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton_OnZeroHit3971658986.h"
#include "AssemblyU2DCSharp_TweenAlpha2421518635.h"
#include "AssemblyU2DCSharp_ChatmageddonPanelTransitionManag1399015365MethodDeclarations.h"
#include "AssemblyU2DCSharp_LockoutUI1337294023.h"
#include "AssemblyU2DCSharp_UITweener2986641582.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t3497673348_m2974738468(__this, method) ((  Collider_t3497673348 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t948534547_m324820273(__this, method) ((  BoxCollider2D_t948534547 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPanel>()
#define Component_GetComponent_TisUIPanel_t1795085332_m1654704505(__this, method) ((  UIPanel_t1795085332 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// T NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m2434993371_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define NGUITools_FindInParents_TisIl2CppObject_m2434993371(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m2434993371_gshared)(__this /* static, unused */, ___go0, method)
// T NGUITools::FindInParents<UIPanel>(UnityEngine.GameObject)
#define NGUITools_FindInParents_TisUIPanel_t1795085332_m297231755(__this /* static, unused */, ___go0, method) ((  UIPanel_t1795085332 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m2434993371_gshared)(__this /* static, unused */, ___go0, method)
// !!0 UnityEngine.Component::GetComponent<UIScrollView>()
#define Component_GetComponent_TisUIScrollView_t3033954930_m3668191775(__this, method) ((  UIScrollView_t3033954930 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TweenPosition>()
#define GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(__this, method) ((  TweenPosition_t1144714832 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<TweenScale>()
#define Component_GetComponent_TisTweenScale_t2697902175_m3059496736(__this, method) ((  TweenScale_t2697902175 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<TweenPosition>()
#define Component_GetComponent_TisTweenPosition_t1144714832_m4195983071(__this, method) ((  TweenPosition_t1144714832 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Skip_TisIl2CppObject_m353235285_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Skip_TisIl2CppObject_m353235285(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Skip_TisIl2CppObject_m353235285_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Skip_TisString_t_m3256592265(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Skip_TisIl2CppObject_m353235285_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m349912619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m349912619(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m349912619_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m1652456295(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m349912619_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<LoginCenterUI>()
#define GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895(__this, method) ((  LoginCenterUI_t890505802 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIViewport::.ctor()
extern "C"  void UIViewport__ctor_m4190691379 (UIViewport_t1541362616 * __this, const MethodInfo* method)
{
	{
		__this->set_fullSize_5((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIViewport::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const uint32_t UIViewport_Start_m2670372919_MetadataUsageId;
extern "C"  void UIViewport_Start_m2670372919 (UIViewport_t1541362616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_Start_m2670372919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		__this->set_mCam_6(L_0);
		Camera_t189460977 * L_1 = __this->get_sourceCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Camera_t189460977 * L_3 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sourceCamera_2(L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UIViewport::LateUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIViewport_LateUpdate_m1995412902_MetadataUsageId;
extern "C"  void UIViewport_LateUpdate_m1995412902 (UIViewport_t1541362616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_LateUpdate_m1995412902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Transform_t3275118058 * L_0 = __this->get_topLeft_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_bottomRight_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t3275118058 * L_4 = __this->get_topLeft_3();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeInHierarchy_m4242915935(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0113;
		}
	}
	{
		Camera_t189460977 * L_7 = __this->get_sourceCamera_2();
		Transform_t3275118058 * L_8 = __this->get_topLeft_3();
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_10 = Camera_WorldToScreenPoint_m638747266(L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Camera_t189460977 * L_11 = __this->get_sourceCamera_2();
		Transform_t3275118058 * L_12 = __this->get_bottomRight_4();
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_14 = Camera_WorldToScreenPoint_m638747266(L_11, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_0)->get_x_1();
		int32_t L_16 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = (&V_1)->get_y_2();
		int32_t L_18 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_x_1();
		float L_20 = (&V_0)->get_x_1();
		int32_t L_21 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = (&V_0)->get_y_2();
		float L_23 = (&V_1)->get_y_2();
		int32_t L_24 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m1220545469((&V_2), ((float)((float)L_15/(float)(((float)((float)L_16))))), ((float)((float)L_17/(float)(((float)((float)L_18))))), ((float)((float)((float)((float)L_19-(float)L_20))/(float)(((float)((float)L_21))))), ((float)((float)((float)((float)L_22-(float)L_23))/(float)(((float)((float)L_24))))), /*hidden argument*/NULL);
		float L_25 = __this->get_fullSize_5();
		float L_26 = Rect_get_height_m3128694305((&V_2), /*hidden argument*/NULL);
		V_3 = ((float)((float)L_25*(float)L_26));
		Rect_t3681755626  L_27 = V_2;
		Camera_t189460977 * L_28 = __this->get_mCam_6();
		NullCheck(L_28);
		Rect_t3681755626  L_29 = Camera_get_rect_m2873096661(L_28, /*hidden argument*/NULL);
		bool L_30 = Rect_op_Inequality_m3595915756(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e5;
		}
	}
	{
		Camera_t189460977 * L_31 = __this->get_mCam_6();
		Rect_t3681755626  L_32 = V_2;
		NullCheck(L_31);
		Camera_set_rect_m1838810502(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		Camera_t189460977 * L_33 = __this->get_mCam_6();
		NullCheck(L_33);
		float L_34 = Camera_get_orthographicSize_m1801974358(L_33, /*hidden argument*/NULL);
		float L_35 = V_3;
		if ((((float)L_34) == ((float)L_35)))
		{
			goto IL_0102;
		}
	}
	{
		Camera_t189460977 * L_36 = __this->get_mCam_6();
		float L_37 = V_3;
		NullCheck(L_36);
		Camera_set_orthographicSize_m2708824189(L_36, L_37, /*hidden argument*/NULL);
	}

IL_0102:
	{
		Camera_t189460977 * L_38 = __this->get_mCam_6();
		NullCheck(L_38);
		Behaviour_set_enabled_m1796096907(L_38, (bool)1, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_0113:
	{
		Camera_t189460977 * L_39 = __this->get_mCam_6();
		NullCheck(L_39);
		Behaviour_set_enabled_m1796096907(L_39, (bool)0, /*hidden argument*/NULL);
	}

IL_011f:
	{
		return;
	}
}
// System.Void UIWidget::.ctor()
extern Il2CppClass* UIGeometry_t1005900006_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern Il2CppClass* UIRect_t4127168124_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget__ctor_m1747155039_MetadataUsageId;
extern "C"  void UIWidget__ctor_m1747155039 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget__ctor_m1747155039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mColor_22(L_0);
		__this->set_mPivot_23(4);
		__this->set_mWidth_24(((int32_t)100));
		__this->set_mHeight_25(((int32_t)100));
		__this->set_aspectRatio_33((1.0f));
		UIGeometry_t1005900006 * L_1 = (UIGeometry_t1005900006 *)il2cpp_codegen_object_new(UIGeometry_t1005900006_il2cpp_TypeInfo_var);
		UIGeometry__ctor_m4281277957(L_1, /*hidden argument*/NULL);
		__this->set_geometry_36(L_1);
		__this->set_fillGeometry_37((bool)1);
		__this->set_mPlayMode_38((bool)1);
		Vector4_t2243707581  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m1222289168(&L_2, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mDrawRegion_39(L_2);
		__this->set_mIsVisibleByAlpha_41((bool)1);
		__this->set_mIsVisibleByPanel_42((bool)1);
		__this->set_mIsInFront_43((bool)1);
		__this->set_mCorners_47(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_mAlphaFrameID_48((-1));
		__this->set_mMatrixFrame_49((-1));
		IL2CPP_RUNTIME_CLASS_INIT(UIRect_t4127168124_il2cpp_TypeInfo_var);
		UIRect__ctor_m1688524151(__this, /*hidden argument*/NULL);
		return;
	}
}
// UIDrawCall/OnRenderCallback UIWidget::get_onRender()
extern "C"  OnRenderCallback_t2690968101 * UIWidget_get_onRender_m4232393577 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		OnRenderCallback_t2690968101 * L_0 = __this->get_mOnRender_29();
		return L_0;
	}
}
// System.Void UIWidget::set_onRender(UIDrawCall/OnRenderCallback)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* OnRenderCallback_t2690968101_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_onRender_m2573789460_MetadataUsageId;
extern "C"  void UIWidget_set_onRender_m2573789460 (UIWidget_t1453041918 * __this, OnRenderCallback_t2690968101 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_onRender_m2573789460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnRenderCallback_t2690968101 * L_0 = __this->get_mOnRender_29();
		OnRenderCallback_t2690968101 * L_1 = ___value0;
		bool L_2 = Delegate_op_Inequality_m1907501135(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t3291843512 * L_3 = __this->get_drawCall_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t3291843512 * L_5 = __this->get_drawCall_46();
		NullCheck(L_5);
		OnRenderCallback_t2690968101 * L_6 = L_5->get_onRender_32();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		OnRenderCallback_t2690968101 * L_7 = __this->get_mOnRender_29();
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t3291843512 * L_8 = __this->get_drawCall_46();
		UIDrawCall_t3291843512 * L_9 = L_8;
		NullCheck(L_9);
		OnRenderCallback_t2690968101 * L_10 = L_9->get_onRender_32();
		OnRenderCallback_t2690968101 * L_11 = __this->get_mOnRender_29();
		Delegate_t3022476291 * L_12 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_onRender_32(((OnRenderCallback_t2690968101 *)CastclassSealed(L_12, OnRenderCallback_t2690968101_il2cpp_TypeInfo_var)));
	}

IL_005e:
	{
		OnRenderCallback_t2690968101 * L_13 = ___value0;
		__this->set_mOnRender_29(L_13);
		UIDrawCall_t3291843512 * L_14 = __this->get_drawCall_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t3291843512 * L_16 = __this->get_drawCall_46();
		UIDrawCall_t3291843512 * L_17 = L_16;
		NullCheck(L_17);
		OnRenderCallback_t2690968101 * L_18 = L_17->get_onRender_32();
		OnRenderCallback_t2690968101 * L_19 = ___value0;
		Delegate_t3022476291 * L_20 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_onRender_32(((OnRenderCallback_t2690968101 *)CastclassSealed(L_20, OnRenderCallback_t2690968101_il2cpp_TypeInfo_var)));
	}

IL_0092:
	{
		return;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawRegion()
extern "C"  Vector4_t2243707581  UIWidget_get_drawRegion_m3587096183 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = __this->get_mDrawRegion_39();
		return L_0;
	}
}
// System.Void UIWidget::set_drawRegion(UnityEngine.Vector4)
extern "C"  void UIWidget_set_drawRegion_m454391600 (UIWidget_t1453041918 * __this, Vector4_t2243707581  ___value0, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = __this->get_mDrawRegion_39();
		Vector4_t2243707581  L_1 = ___value0;
		bool L_2 = Vector4_op_Inequality_m1143290655(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector4_t2243707581  L_3 = ___value0;
		__this->set_mDrawRegion_39(L_3);
		bool L_4 = __this->get_autoResizeBoxCollider_30();
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		UIWidget_ResizeCollider_m4072963299(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002f:
	{
		return;
	}
}
// UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern "C"  Vector2_t2243707579  UIWidget_get_pivotOffset_m943693420 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = UIWidget_get_pivot_m678429010(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = NGUIMath_GetPivotOffset_m479984401(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UIWidget::get_width()
extern "C"  int32_t UIWidget_get_width_m1323147272 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mWidth_24();
		return L_0;
	}
}
// System.Void UIWidget::set_width(System.Int32)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_width_m1785998957_MetadataUsageId;
extern "C"  void UIWidget_set_width_m1785998957 (UIWidget_t1453041918 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_width_m1785998957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mWidth_24();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UIRect::get_isAnchoredHorizontally() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t4057058294 * L_8 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t4057058294 * L_11 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = L_11->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)3)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if (L_16)
		{
			goto IL_00a6;
		}
	}

IL_0083:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)L_18))))), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a6:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if ((((int32_t)L_19) == ((int32_t)8)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)5)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mWidth_24();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (((float)((float)((int32_t)((int32_t)L_22-(int32_t)L_23))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mWidth_24();
		V_1 = ((int32_t)((int32_t)L_24-(int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, ((float)((float)(((float)((float)((-L_29)))))*(float)(0.5f))), (0.0f), ((float)((float)(((float)((float)L_30)))*(float)(0.5f))), (0.0f), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t4057058294 * L_31 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = L_31->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_32, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_34-(int32_t)L_35))))), (0.0f), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mWidth_24();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (((float)((float)((int32_t)((int32_t)L_36-(int32_t)L_37))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = ___value0;
		int32_t L_39 = __this->get_mHeight_25();
		UIWidget_SetDimensions_m1379791388(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// System.Int32 UIWidget::get_height()
extern "C"  int32_t UIWidget_get_height_m613271667 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mHeight_25();
		return L_0;
	}
}
// System.Void UIWidget::set_height(System.Int32)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_height_m3993111822_MetadataUsageId;
extern "C"  void UIWidget_set_height_m3993111822 (UIWidget_t1453041918 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_height_m3993111822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mHeight_25();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UIRect::get_isAnchoredVertically() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t4057058294 * L_8 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t4057058294 * L_11 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = L_11->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)7)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_16) == ((uint32_t)8))))
		{
			goto IL_00a7;
		}
	}

IL_0084:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)L_18))))), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a7:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if (!L_19)
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)1)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mHeight_25();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)((int32_t)L_22-(int32_t)L_23))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mHeight_25();
		V_1 = ((int32_t)((int32_t)L_24-(int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), ((float)((float)(((float)((float)((-L_29)))))*(float)(0.5f))), (0.0f), ((float)((float)(((float)((float)L_30)))*(float)(0.5f))), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t4057058294 * L_31 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = L_31->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_32, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_34-(int32_t)L_35))))), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mHeight_25();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m356449699(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)((int32_t)L_36-(int32_t)L_37))))), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = __this->get_mWidth_24();
		int32_t L_39 = ___value0;
		UIWidget_SetDimensions_m1379791388(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// UnityEngine.Color UIWidget::get_color()
extern "C"  Color_t2020392075  UIWidget_get_color_m338614656 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get_mColor_22();
		return L_0;
	}
}
// System.Void UIWidget::set_color(UnityEngine.Color)
extern "C"  void UIWidget_set_color_m1406575269 (UIWidget_t1453041918 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Color_t2020392075  L_0 = __this->get_mColor_22();
		Color_t2020392075  L_1 = ___value0;
		bool L_2 = Color_op_Inequality_m3949383683(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Color_t2020392075 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = L_3->get_a_3();
		float L_5 = (&___value0)->get_a_3();
		V_0 = (bool)((((int32_t)((((float)L_4) == ((float)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Color_t2020392075  L_6 = ___value0;
		__this->set_mColor_22(L_6);
		bool L_7 = V_0;
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, L_7);
	}

IL_0037:
	{
		return;
	}
}
// System.Single UIWidget::get_alpha()
extern "C"  float UIWidget_get_alpha_m1577200396 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		return L_1;
	}
}
// System.Void UIWidget::set_alpha(System.Single)
extern "C"  void UIWidget_set_alpha_m4102714563 (UIWidget_t1453041918 * __this, float ___value0, const MethodInfo* method)
{
	{
		Color_t2020392075 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		float L_2 = ___value0;
		if ((((float)L_1) == ((float)L_2)))
		{
			goto IL_0024;
		}
	}
	{
		Color_t2020392075 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = ___value0;
		L_3->set_a_3(L_4);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean UIWidget::get_isVisible()
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_isVisible_m1555008678_MetadataUsageId;
extern "C"  bool UIWidget_get_isVisible_m1555008678 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_isVisible_m1555008678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		bool L_0 = __this->get_mIsVisibleByPanel_42();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		bool L_1 = __this->get_mIsVisibleByAlpha_41();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		bool L_2 = __this->get_mIsInFront_43();
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = ((UIRect_t4127168124 *)__this)->get_finalAlpha_20();
		if ((!(((float)L_3) > ((float)(0.001f)))))
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		bool L_4 = NGUITools_GetActive_m2411194121(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_4));
		goto IL_003a;
	}

IL_0039:
	{
		G_B6_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean UIWidget::get_hasVertices()
extern "C"  bool UIWidget_get_hasVertices_m455088681 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UIGeometry_t1005900006 * L_0 = __this->get_geometry_36();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UIGeometry_t1005900006 * L_1 = __this->get_geometry_36();
		NullCheck(L_1);
		bool L_2 = UIGeometry_get_hasVertices_m2650326039(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// UIWidget/Pivot UIWidget::get_rawPivot()
extern "C"  int32_t UIWidget_get_rawPivot_m701429390 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_rawPivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_rawPivot_m2483066965 (UIWidget_t1453041918 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_mPivot_23(L_2);
		bool L_3 = __this->get_autoResizeBoxCollider_30();
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_ResizeCollider_m4072963299(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002a:
	{
		return;
	}
}
// UIWidget/Pivot UIWidget::get_pivot()
extern "C"  int32_t UIWidget_get_pivot_m678429010 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_pivot(UIWidget/Pivot)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_pivot_m2500521417_MetadataUsageId;
extern "C"  void UIWidget_set_pivot_m2500521417 (UIWidget_t1453041918 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_pivot_m2500521417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t3275118058 * V_2 = NULL;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_00ea;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, __this);
		NullCheck(L_2);
		V_0 = (*(Vector3_t2243707580 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t L_3 = ___value0;
		__this->set_mPivot_23(L_3);
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
		Vector3U5BU5D_t1172311765* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, __this);
		NullCheck(L_4);
		V_1 = (*(Vector3_t2243707580 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Transform_t3275118058 * L_5 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		Transform_t3275118058 * L_6 = V_2;
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Transform_t3275118058 * L_8 = V_2;
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_localPosition_m2533925116(L_8, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_z_3();
		V_4 = L_10;
		Vector3_t2243707580 * L_11 = (&V_3);
		float L_12 = L_11->get_x_1();
		float L_13 = (&V_0)->get_x_1();
		float L_14 = (&V_1)->get_x_1();
		L_11->set_x_1(((float)((float)L_12+(float)((float)((float)L_13-(float)L_14)))));
		Vector3_t2243707580 * L_15 = (&V_3);
		float L_16 = L_15->get_y_2();
		float L_17 = (&V_0)->get_y_2();
		float L_18 = (&V_1)->get_y_2();
		L_15->set_y_2(((float)((float)L_16+(float)((float)((float)L_17-(float)L_18)))));
		Transform_t3275118058 * L_19 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = V_3;
		NullCheck(L_19);
		Transform_set_position_m2469242620(L_19, L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_localPosition_m2533925116(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = bankers_roundf(L_23);
		(&V_3)->set_x_1(L_24);
		float L_25 = (&V_3)->get_y_2();
		float L_26 = bankers_roundf(L_25);
		(&V_3)->set_y_2(L_26);
		float L_27 = V_4;
		(&V_3)->set_z_3(L_27);
		Transform_t3275118058 * L_28 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_29 = V_3;
		NullCheck(L_28);
		Transform_set_localPosition_m1026930133(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		return;
	}
}
// System.Int32 UIWidget::get_depth()
extern "C"  int32_t UIWidget_get_depth_m3794940569 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mDepth_26();
		return L_0;
	}
}
// System.Void UIWidget::set_depth(System.Int32)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_depth_m2276777530_MetadataUsageId;
extern "C"  void UIWidget_set_depth_m2276777530 (UIWidget_t1453041918 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_depth_m2276777530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_mDepth_26();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t1795085332 * L_2 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		UIPanel_t1795085332 * L_4 = __this->get_panel_35();
		NullCheck(L_4);
		UIPanel_RemoveWidget_m2792282035(L_4, __this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = ___value0;
		__this->set_mDepth_26(L_5);
		UIPanel_t1795085332 * L_6 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t1795085332 * L_8 = __this->get_panel_35();
		NullCheck(L_8);
		UIPanel_AddWidget_m318008474(L_8, __this, /*hidden argument*/NULL);
		bool L_9 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t1795085332 * L_10 = __this->get_panel_35();
		NullCheck(L_10);
		UIPanel_SortWidgets_m3912063282(L_10, /*hidden argument*/NULL);
		UIPanel_t1795085332 * L_11 = __this->get_panel_35();
		NullCheck(L_11);
		UIPanel_RebuildAllDrawCalls_m2421877910(L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Int32 UIWidget::get_raycastDepth()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_raycastDepth_m2960564342_MetadataUsageId;
extern "C"  int32_t UIWidget_get_raycastDepth_m2960564342 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_raycastDepth_m2960564342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		UIPanel_t1795085332 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m3689313888(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		UIPanel_t1795085332 * L_2 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = __this->get_mDepth_26();
		UIPanel_t1795085332 * L_5 = __this->get_panel_35();
		NullCheck(L_5);
		int32_t L_6 = UIPanel_get_depth_m1315426983(L_5, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)1000)))));
		goto IL_004c;
	}

IL_0046:
	{
		int32_t L_7 = __this->get_mDepth_26();
		G_B5_0 = L_7;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3[] UIWidget::get_localCorners()
extern "C"  Vector3U5BU5D_t1172311765* UIWidget_get_localCorners_m320454507 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector2_t2243707579  L_0 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Vector3U5BU5D_t1172311765* L_9 = __this->get_mCorners_47();
		NullCheck(L_9);
		float L_10 = V_1;
		float L_11 = V_2;
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2720820983(&L_12, L_10, L_11, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_12;
		Vector3U5BU5D_t1172311765* L_13 = __this->get_mCorners_47();
		NullCheck(L_13);
		float L_14 = V_1;
		float L_15 = V_4;
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2720820983(&L_16, L_14, L_15, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_16;
		Vector3U5BU5D_t1172311765* L_17 = __this->get_mCorners_47();
		NullCheck(L_17);
		float L_18 = V_3;
		float L_19 = V_4;
		Vector3_t2243707580  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2720820983(&L_20, L_18, L_19, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_20;
		Vector3U5BU5D_t1172311765* L_21 = __this->get_mCorners_47();
		NullCheck(L_21);
		float L_22 = V_3;
		float L_23 = V_2;
		Vector3_t2243707580  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2720820983(&L_24, L_22, L_23, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_24;
		Vector3U5BU5D_t1172311765* L_25 = __this->get_mCorners_47();
		return L_25;
	}
}
// UnityEngine.Vector2 UIWidget::get_localSize()
extern "C"  Vector2_t2243707579  UIWidget_get_localSize_m368013321 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		Vector3U5BU5D_t1172311765* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(10 /* UnityEngine.Vector3[] UIRect::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t1172311765* L_1 = V_0;
		NullCheck(L_1);
		Vector3U5BU5D_t1172311765* L_2 = V_0;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, (*(Vector3_t2243707580 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (*(Vector3_t2243707580 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 UIWidget::get_localCenter()
extern "C"  Vector3_t2243707580  UIWidget_get_localCenter_m1865171662 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		Vector3U5BU5D_t1172311765* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(10 /* UnityEngine.Vector3[] UIRect::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t1172311765* L_1 = V_0;
		NullCheck(L_1);
		Vector3U5BU5D_t1172311765* L_2 = V_0;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, (*(Vector3_t2243707580 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t2243707580 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3[] UIWidget::get_worldCorners()
extern "C"  Vector3U5BU5D_t1172311765* UIWidget_get_worldCorners_m3247184012 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Transform_t3275118058 * V_5 = NULL;
	{
		Vector2_t2243707579  L_0 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Transform_t3275118058 * L_9 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		V_5 = L_9;
		Vector3U5BU5D_t1172311765* L_10 = __this->get_mCorners_47();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = V_5;
		float L_12 = V_1;
		float L_13 = V_2;
		NullCheck(L_11);
		Vector3_t2243707580  L_14 = Transform_TransformPoint_m3936139914(L_11, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_14;
		Vector3U5BU5D_t1172311765* L_15 = __this->get_mCorners_47();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = V_5;
		float L_17 = V_1;
		float L_18 = V_4;
		NullCheck(L_16);
		Vector3_t2243707580  L_19 = Transform_TransformPoint_m3936139914(L_16, L_17, L_18, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_19;
		Vector3U5BU5D_t1172311765* L_20 = __this->get_mCorners_47();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = V_5;
		float L_22 = V_3;
		float L_23 = V_4;
		NullCheck(L_21);
		Vector3_t2243707580  L_24 = Transform_TransformPoint_m3936139914(L_21, L_22, L_23, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_24;
		Vector3U5BU5D_t1172311765* L_25 = __this->get_mCorners_47();
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = V_5;
		float L_27 = V_3;
		float L_28 = V_2;
		NullCheck(L_26);
		Vector3_t2243707580  L_29 = Transform_TransformPoint_m3936139914(L_26, L_27, L_28, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_29;
		Vector3U5BU5D_t1172311765* L_30 = __this->get_mCorners_47();
		return L_30;
	}
}
// UnityEngine.Vector3 UIWidget::get_worldCenter()
extern "C"  Vector3_t2243707580  UIWidget_get_worldCenter_m1326689473 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = UIWidget_get_localCenter_m1865171662(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_2 = Transform_TransformPoint_m3272254198(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawingDimensions()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_drawingDimensions_m1954568534_MetadataUsageId;
extern "C"  Vector4_t2243707581  UIWidget_get_drawingDimensions_m1954568534 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_drawingDimensions_m1954568534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B11_2 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B10_1 = 0.0f;
	float G_B10_2 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B12_2 = 0.0f;
	float G_B12_3 = 0.0f;
	{
		Vector2_t2243707579  L_0 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Vector4_t2243707581 * L_9 = __this->get_address_of_mDrawRegion_39();
		float L_10 = L_9->get_x_1();
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_11 = V_1;
		G_B3_0 = L_11;
		goto IL_006b;
	}

IL_0059:
	{
		float L_12 = V_1;
		float L_13 = V_3;
		Vector4_t2243707581 * L_14 = __this->get_address_of_mDrawRegion_39();
		float L_15 = L_14->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		G_B3_0 = L_16;
	}

IL_006b:
	{
		Vector4_t2243707581 * L_17 = __this->get_address_of_mDrawRegion_39();
		float L_18 = L_17->get_y_2();
		G_B4_0 = G_B3_0;
		if ((!(((float)L_18) == ((float)(0.0f)))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0086;
		}
	}
	{
		float L_19 = V_2;
		G_B6_0 = L_19;
		G_B6_1 = G_B4_0;
		goto IL_0099;
	}

IL_0086:
	{
		float L_20 = V_2;
		float L_21 = V_4;
		Vector4_t2243707581 * L_22 = __this->get_address_of_mDrawRegion_39();
		float L_23 = L_22->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_20, L_21, L_23, /*hidden argument*/NULL);
		G_B6_0 = L_24;
		G_B6_1 = G_B5_0;
	}

IL_0099:
	{
		Vector4_t2243707581 * L_25 = __this->get_address_of_mDrawRegion_39();
		float L_26 = L_25->get_z_3();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if ((!(((float)L_26) == ((float)(1.0f)))))
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			goto IL_00b4;
		}
	}
	{
		float L_27 = V_3;
		G_B9_0 = L_27;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c6;
	}

IL_00b4:
	{
		float L_28 = V_1;
		float L_29 = V_3;
		Vector4_t2243707581 * L_30 = __this->get_address_of_mDrawRegion_39();
		float L_31 = L_30->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_28, L_29, L_31, /*hidden argument*/NULL);
		G_B9_0 = L_32;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c6:
	{
		Vector4_t2243707581 * L_33 = __this->get_address_of_mDrawRegion_39();
		float L_34 = L_33->get_w_4();
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		if ((!(((float)L_34) == ((float)(1.0f)))))
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			G_B11_2 = G_B9_2;
			goto IL_00e2;
		}
	}
	{
		float L_35 = V_4;
		G_B12_0 = L_35;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_00f5;
	}

IL_00e2:
	{
		float L_36 = V_2;
		float L_37 = V_4;
		Vector4_t2243707581 * L_38 = __this->get_address_of_mDrawRegion_39();
		float L_39 = L_38->get_w_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_40 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_36, L_37, L_39, /*hidden argument*/NULL);
		G_B12_0 = L_40;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_00f5:
	{
		Vector4_t2243707581  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector4__ctor_m1222289168(&L_41, G_B12_3, G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		return L_41;
	}
}
// UnityEngine.Material UIWidget::get_material()
extern "C"  Material_t193706927 * UIWidget_get_material_m570210584 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		return (Material_t193706927 *)NULL;
	}
}
// System.Void UIWidget::set_material(UnityEngine.Material)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3647878049;
extern const uint32_t UIWidget_set_material_m1102044077_MetadataUsageId;
extern "C"  void UIWidget_set_material_m1102044077 (UIWidget_t1453041918 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_material_m1102044077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m56707527(NULL /*static, unused*/, L_0, _stringLiteral3647878049, /*hidden argument*/NULL);
		NotImplementedException_t2785117854 * L_2 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Texture UIWidget::get_mainTexture()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_mainTexture_m582147825_MetadataUsageId;
extern "C"  Texture_t2243626319 * UIWidget_get_mainTexture_m582147825 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_mainTexture_m582147825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	Texture_t2243626319 * G_B3_0 = NULL;
	{
		Material_t193706927 * L_0 = VirtFuncInvoker0< Material_t193706927 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t193706927 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t193706927 * L_3 = V_0;
		NullCheck(L_3);
		Texture_t2243626319 * L_4 = Material_get_mainTexture_m432794412(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Texture_t2243626319 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_mainTexture(UnityEngine.Texture)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2412910540;
extern const uint32_t UIWidget_set_mainTexture_m448685722_MetadataUsageId;
extern "C"  void UIWidget_set_mainTexture_m448685722 (UIWidget_t1453041918 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_mainTexture_m448685722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m56707527(NULL /*static, unused*/, L_0, _stringLiteral2412910540, /*hidden argument*/NULL);
		NotImplementedException_t2785117854 * L_2 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Shader UIWidget::get_shader()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_shader_m1961722432_MetadataUsageId;
extern "C"  Shader_t2430389951 * UIWidget_get_shader_m1961722432 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_shader_m1961722432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	Shader_t2430389951 * G_B3_0 = NULL;
	{
		Material_t193706927 * L_0 = VirtFuncInvoker0< Material_t193706927 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t193706927 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t193706927 * L_3 = V_0;
		NullCheck(L_3);
		Shader_t2430389951 * L_4 = Material_get_shader_m2320486867(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Shader_t2430389951 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_shader(UnityEngine.Shader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral40541627;
extern const uint32_t UIWidget_set_shader_m3834103641_MetadataUsageId;
extern "C"  void UIWidget_set_shader_m3834103641 (UIWidget_t1453041918 * __this, Shader_t2430389951 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_shader_m3834103641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m56707527(NULL /*static, unused*/, L_0, _stringLiteral40541627, /*hidden argument*/NULL);
		NotImplementedException_t2785117854 * L_2 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Vector2 UIWidget::get_relativeSize()
extern "C"  Vector2_t2243707579  UIWidget_get_relativeSize_m2102430774 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UIWidget::get_hasBoxCollider()
extern Il2CppClass* BoxCollider_t22920061_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t3497673348_m2974738468_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t948534547_m324820273_MethodInfo_var;
extern const uint32_t UIWidget_get_hasBoxCollider_m2614473607_MetadataUsageId;
extern "C"  bool UIWidget_get_hasBoxCollider_m2614473607 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_hasBoxCollider_m2614473607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BoxCollider_t22920061 * V_0 = NULL;
	{
		Collider_t3497673348 * L_0 = Component_GetComponent_TisCollider_t3497673348_m2974738468(__this, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m2974738468_MethodInfo_var);
		V_0 = ((BoxCollider_t22920061 *)IsInstSealed(L_0, BoxCollider_t22920061_il2cpp_TypeInfo_var));
		BoxCollider_t22920061 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		BoxCollider2D_t948534547 * L_3 = Component_GetComponent_TisBoxCollider2D_t948534547_m324820273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t948534547_m324820273_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetDimensions_m1379791388_MetadataUsageId;
extern "C"  void UIWidget_SetDimensions_m1379791388 (UIWidget_t1453041918 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDimensions_m1379791388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_mWidth_24();
		int32_t L_1 = ___w0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_mHeight_25();
		int32_t L_3 = ___h1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_00b8;
		}
	}

IL_0018:
	{
		int32_t L_4 = ___w0;
		__this->set_mWidth_24(L_4);
		int32_t L_5 = ___h1;
		__this->set_mHeight_25(L_5);
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_7 = __this->get_mWidth_24();
		float L_8 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_7)))/(float)L_8)), /*hidden argument*/NULL);
		__this->set_mHeight_25(L_9);
		goto IL_009a;
	}

IL_0050:
	{
		int32_t L_10 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_11 = __this->get_mHeight_25();
		float L_12 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_11)))*(float)L_12)), /*hidden argument*/NULL);
		__this->set_mWidth_24(L_13);
		goto IL_009a;
	}

IL_007a:
	{
		int32_t L_14 = __this->get_keepAspectRatio_32();
		if (L_14)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_15 = __this->get_mWidth_24();
		int32_t L_16 = __this->get_mHeight_25();
		__this->set_aspectRatio_33(((float)((float)(((float)((float)L_15)))/(float)(((float)((float)L_16))))));
	}

IL_009a:
	{
		__this->set_mMoved_45((bool)1);
		bool L_17 = __this->get_autoResizeBoxCollider_30();
		if (!L_17)
		{
			goto IL_00b2;
		}
	}
	{
		UIWidget_ResizeCollider_m4072963299(__this, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_00b8:
	{
		return;
	}
}
// UnityEngine.Vector3[] UIWidget::GetSides(UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_GetSides_m2442461784_MetadataUsageId;
extern "C"  Vector3U5BU5D_t1172311765* UIWidget_GetSides_m2442461784 (UIWidget_t1453041918 * __this, Transform_t3275118058 * ___relativeTo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_GetSides_m2442461784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Transform_t3275118058 * V_7 = NULL;
	int32_t V_8 = 0;
	{
		Vector2_t2243707579  L_0 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_0();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_1();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		float L_9 = V_1;
		float L_10 = V_3;
		V_5 = ((float)((float)((float)((float)L_9+(float)L_10))*(float)(0.5f)));
		float L_11 = V_2;
		float L_12 = V_4;
		V_6 = ((float)((float)((float)((float)L_11+(float)L_12))*(float)(0.5f)));
		Transform_t3275118058 * L_13 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		V_7 = L_13;
		Vector3U5BU5D_t1172311765* L_14 = __this->get_mCorners_47();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = V_7;
		float L_16 = V_1;
		float L_17 = V_6;
		NullCheck(L_15);
		Vector3_t2243707580  L_18 = Transform_TransformPoint_m3936139914(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_18;
		Vector3U5BU5D_t1172311765* L_19 = __this->get_mCorners_47();
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = V_7;
		float L_21 = V_5;
		float L_22 = V_4;
		NullCheck(L_20);
		Vector3_t2243707580  L_23 = Transform_TransformPoint_m3936139914(L_20, L_21, L_22, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_23;
		Vector3U5BU5D_t1172311765* L_24 = __this->get_mCorners_47();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = V_7;
		float L_26 = V_3;
		float L_27 = V_6;
		NullCheck(L_25);
		Vector3_t2243707580  L_28 = Transform_TransformPoint_m3936139914(L_25, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_28;
		Vector3U5BU5D_t1172311765* L_29 = __this->get_mCorners_47();
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = V_7;
		float L_31 = V_5;
		float L_32 = V_2;
		NullCheck(L_30);
		Vector3_t2243707580  L_33 = Transform_TransformPoint_m3936139914(L_30, L_31, L_32, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_33;
		Transform_t3275118058 * L_34 = ___relativeTo0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_34, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012a;
		}
	}
	{
		V_8 = 0;
		goto IL_0122;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_36 = __this->get_mCorners_47();
		int32_t L_37 = V_8;
		NullCheck(L_36);
		Transform_t3275118058 * L_38 = ___relativeTo0;
		Vector3U5BU5D_t1172311765* L_39 = __this->get_mCorners_47();
		int32_t L_40 = V_8;
		NullCheck(L_39);
		NullCheck(L_38);
		Vector3_t2243707580  L_41 = Transform_InverseTransformPoint_m2648491174(L_38, (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))) = L_41;
		int32_t L_42 = V_8;
		V_8 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_0122:
	{
		int32_t L_43 = V_8;
		if ((((int32_t)L_43) < ((int32_t)4)))
		{
			goto IL_00f2;
		}
	}

IL_012a:
	{
		Vector3U5BU5D_t1172311765* L_44 = __this->get_mCorners_47();
		return L_44;
	}
}
// System.Single UIWidget::CalculateFinalAlpha(System.Int32)
extern "C"  float UIWidget_CalculateFinalAlpha_m1063953086 (UIWidget_t1453041918 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mAlphaFrameID_48();
		int32_t L_1 = ___frameID0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___frameID0;
		__this->set_mAlphaFrameID_48(L_2);
		int32_t L_3 = ___frameID0;
		UIWidget_UpdateFinalAlpha_m4015810103(__this, L_3, /*hidden argument*/NULL);
	}

IL_001a:
	{
		float L_4 = ((UIRect_t4127168124 *)__this)->get_finalAlpha_20();
		return L_4;
	}
}
// System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpdateFinalAlpha_m4015810103_MetadataUsageId;
extern "C"  void UIWidget_UpdateFinalAlpha_m4015810103 (UIWidget_t1453041918 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateFinalAlpha_m4015810103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIRect_t4127168124 * V_0 = NULL;
	UIWidget_t1453041918 * G_B5_0 = NULL;
	UIWidget_t1453041918 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	UIWidget_t1453041918 * G_B6_1 = NULL;
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_41();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_mIsInFront_43();
		if (L_1)
		{
			goto IL_0026;
		}
	}

IL_0016:
	{
		((UIRect_t4127168124 *)__this)->set_finalAlpha_20((0.0f));
		goto IL_0062;
	}

IL_0026:
	{
		UIRect_t4127168124 * L_2 = UIRect_get_parent_m2721070757(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		UIRect_t4127168124 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if (!L_4)
		{
			G_B5_0 = __this;
			goto IL_0052;
		}
	}
	{
		UIRect_t4127168124 * L_5 = V_0;
		int32_t L_6 = ___frameID0;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_5, L_6);
		Color_t2020392075 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B6_0 = ((float)((float)L_7*(float)L_9));
		G_B6_1 = G_B4_0;
		goto IL_005d;
	}

IL_0052:
	{
		Color_t2020392075 * L_10 = __this->get_address_of_mColor_22();
		float L_11 = L_10->get_a_3();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
	}

IL_005d:
	{
		NullCheck(G_B6_1);
		((UIRect_t4127168124 *)G_B6_1)->set_finalAlpha_20(G_B6_0);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UIWidget::Invalidate(System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_Invalidate_m657250173_MetadataUsageId;
extern "C"  void UIWidget_Invalidate_m657250173 (UIWidget_t1453041918 * __this, bool ___includeChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_Invalidate_m657250173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
		__this->set_mAlphaFrameID_48((-1));
		UIPanel_t1795085332 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007f;
		}
	}
	{
		bool L_2 = __this->get_hideIfOffScreen_31();
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		UIPanel_t1795085332 * L_3 = __this->get_panel_35();
		NullCheck(L_3);
		bool L_4 = UIPanel_get_hasCumulativeClipping_m1668757633(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}

IL_003a:
	{
		UIPanel_t1795085332 * L_5 = __this->get_panel_35();
		NullCheck(L_5);
		bool L_6 = UIPanel_IsVisible_m2056051053(L_5, __this, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 1;
	}

IL_004c:
	{
		V_0 = (bool)G_B5_0;
		int32_t L_7 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = UIWidget_CalculateCumulativeAlpha_m2522743069(__this, L_7, /*hidden argument*/NULL);
		bool L_9 = V_0;
		UIWidget_UpdateVisibility_m2482889154(__this, (bool)((((float)L_8) > ((float)(0.001f)))? 1 : 0), L_9, /*hidden argument*/NULL);
		int32_t L_10 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		UIWidget_UpdateFinalAlpha_m4015810103(__this, L_10, /*hidden argument*/NULL);
		bool L_11 = ___includeChildren0;
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		UIRect_Invalidate_m3248007297(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CalculateCumulativeAlpha_m2522743069_MetadataUsageId;
extern "C"  float UIWidget_CalculateCumulativeAlpha_m2522743069 (UIWidget_t1453041918 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CalculateCumulativeAlpha_m2522743069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIRect_t4127168124 * V_0 = NULL;
	float G_B3_0 = 0.0f;
	{
		UIRect_t4127168124 * L_0 = UIRect_get_parent_m2721070757(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		UIRect_t4127168124 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		UIRect_t4127168124 * L_3 = V_0;
		int32_t L_4 = ___frameID0;
		NullCheck(L_3);
		float L_5 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_3, L_4);
		Color_t2020392075 * L_6 = __this->get_address_of_mColor_22();
		float L_7 = L_6->get_a_3();
		G_B3_0 = ((float)((float)L_5*(float)L_7));
		goto IL_0036;
	}

IL_002b:
	{
		Color_t2020392075 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetRect_m1687821069_MetadataUsageId;
extern "C"  void UIWidget_SetRect_m1687821069 (UIWidget_t1453041918 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetRect_m1687821069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Transform_t3275118058 * V_5 = NULL;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Vector2_t2243707579  L_0 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___x0;
		float L_2 = ___x0;
		float L_3 = ___width2;
		float L_4 = (&V_0)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_1, ((float)((float)L_2+(float)L_3)), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = ___y1;
		float L_7 = ___y1;
		float L_8 = ___height3;
		float L_9 = (&V_0)->get_y_1();
		float L_10 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_6, ((float)((float)L_7+(float)L_8)), L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = ___width2;
		int32_t L_12 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, ((float)((float)L_11+(float)(0.5f))), /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = ___height3;
		int32_t L_14 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, ((float)((float)L_13+(float)(0.5f))), /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = (&V_0)->get_x_0();
		if ((!(((float)L_15) == ((float)(0.5f)))))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16>>(int32_t)1))<<(int32_t)1));
	}

IL_005d:
	{
		float L_17 = (&V_0)->get_y_1();
		if ((!(((float)L_17) == ((float)(0.5f)))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18>>(int32_t)1))<<(int32_t)1));
	}

IL_0076:
	{
		Transform_t3275118058 * L_19 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		V_5 = L_19;
		Transform_t3275118058 * L_20 = V_5;
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_localPosition_m2533925116(L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		float L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_23 = floorf(((float)((float)L_22+(float)(0.5f))));
		(&V_6)->set_x_1(L_23);
		float L_24 = V_2;
		float L_25 = floorf(((float)((float)L_24+(float)(0.5f))));
		(&V_6)->set_y_2(L_25);
		int32_t L_26 = V_3;
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_3 = L_28;
	}

IL_00c0:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_29) >= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_4 = L_31;
	}

IL_00d5:
	{
		Transform_t3275118058 * L_32 = V_5;
		Vector3_t2243707580  L_33 = V_6;
		NullCheck(L_32);
		Transform_set_localPosition_m1026930133(L_32, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		UIWidget_set_width_m1785998957(__this, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_4;
		UIWidget_set_height_m3993111822(__this, L_35, /*hidden argument*/NULL);
		bool L_36 = UIRect_get_isAnchored_m4030260496(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0192;
		}
	}
	{
		Transform_t3275118058 * L_37 = V_5;
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Transform_get_parent_m147407266(L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		AnchorPoint_t4057058294 * L_39 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = L_39->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		AnchorPoint_t4057058294 * L_42 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		Transform_t3275118058 * L_43 = V_5;
		float L_44 = ___x0;
		NullCheck(L_42);
		AnchorPoint_SetHorizontal_m3575214515(L_42, L_43, L_44, /*hidden argument*/NULL);
	}

IL_0124:
	{
		AnchorPoint_t4057058294 * L_45 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = L_45->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0149;
		}
	}
	{
		AnchorPoint_t4057058294 * L_48 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		Transform_t3275118058 * L_49 = V_5;
		float L_50 = ___x0;
		float L_51 = ___width2;
		NullCheck(L_48);
		AnchorPoint_SetHorizontal_m3575214515(L_48, L_49, ((float)((float)L_50+(float)L_51)), /*hidden argument*/NULL);
	}

IL_0149:
	{
		AnchorPoint_t4057058294 * L_52 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = L_52->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		AnchorPoint_t4057058294 * L_55 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		Transform_t3275118058 * L_56 = V_5;
		float L_57 = ___y1;
		NullCheck(L_55);
		AnchorPoint_SetVertical_m3286489459(L_55, L_56, L_57, /*hidden argument*/NULL);
	}

IL_016c:
	{
		AnchorPoint_t4057058294 * L_58 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_58);
		Transform_t3275118058 * L_59 = L_58->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0192;
		}
	}
	{
		AnchorPoint_t4057058294 * L_61 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		Transform_t3275118058 * L_62 = V_5;
		float L_63 = ___y1;
		float L_64 = ___height3;
		NullCheck(L_61);
		AnchorPoint_SetVertical_m3286489459(L_61, L_62, ((float)((float)L_63+(float)L_64)), /*hidden argument*/NULL);
	}

IL_0192:
	{
		return;
	}
}
// System.Void UIWidget::ResizeCollider()
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_ResizeCollider_m4072963299_MetadataUsageId;
extern "C"  void UIWidget_ResizeCollider_m4072963299 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ResizeCollider_m4072963299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m2411194121(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m3681670752(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 UIWidget::FullCompareFunc(UIWidget,UIWidget)
extern Il2CppClass* UIPanel_t1795085332_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_FullCompareFunc_m1697745011_MetadataUsageId;
extern "C"  int32_t UIWidget_FullCompareFunc_m1697745011 (Il2CppObject * __this /* static, unused */, UIWidget_t1453041918 * ___left0, UIWidget_t1453041918 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_FullCompareFunc_m1697745011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		UIWidget_t1453041918 * L_0 = ___left0;
		NullCheck(L_0);
		UIPanel_t1795085332 * L_1 = L_0->get_panel_35();
		UIWidget_t1453041918 * L_2 = ___right1;
		NullCheck(L_2);
		UIPanel_t1795085332 * L_3 = L_2->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t1795085332_il2cpp_TypeInfo_var);
		int32_t L_4 = UIPanel_CompareFunc_m2010974746(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_t1453041918 * L_6 = ___left0;
		UIWidget_t1453041918 * L_7 = ___right1;
		int32_t L_8 = UIWidget_PanelCompareFunc_m4031376758(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		goto IL_0025;
	}

IL_0024:
	{
		int32_t L_9 = V_0;
		G_B3_0 = L_9;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_PanelCompareFunc_m4031376758_MetadataUsageId;
extern "C"  int32_t UIWidget_PanelCompareFunc_m4031376758 (Il2CppObject * __this /* static, unused */, UIWidget_t1453041918 * ___left0, UIWidget_t1453041918 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_PanelCompareFunc_m4031376758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	Material_t193706927 * V_1 = NULL;
	int32_t G_B13_0 = 0;
	{
		UIWidget_t1453041918 * L_0 = ___left0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_mDepth_26();
		UIWidget_t1453041918 * L_2 = ___right1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_mDepth_26();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		UIWidget_t1453041918 * L_4 = ___left0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_mDepth_26();
		UIWidget_t1453041918 * L_6 = ___right1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_mDepth_26();
		if ((((int32_t)L_5) <= ((int32_t)L_7)))
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		UIWidget_t1453041918 * L_8 = ___left0;
		NullCheck(L_8);
		Material_t193706927 * L_9 = VirtFuncInvoker0< Material_t193706927 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, L_8);
		V_0 = L_9;
		UIWidget_t1453041918 * L_10 = ___right1;
		NullCheck(L_10);
		Material_t193706927 * L_11 = VirtFuncInvoker0< Material_t193706927 * >::Invoke(24 /* UnityEngine.Material UIWidget::get_material() */, L_10);
		V_1 = L_11;
		Material_t193706927 * L_12 = V_0;
		Material_t193706927 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}

IL_0042:
	{
		Material_t193706927 * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0050;
		}
	}
	{
		return (-1);
	}

IL_0050:
	{
		Material_t193706927 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_005e;
		}
	}
	{
		return 1;
	}

IL_005e:
	{
		Material_t193706927 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = Object_GetInstanceID_m1920497914(L_19, /*hidden argument*/NULL);
		Material_t193706927 * L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = Object_GetInstanceID_m1920497914(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_20) >= ((int32_t)L_22)))
		{
			goto IL_0075;
		}
	}
	{
		G_B13_0 = (-1);
		goto IL_0076;
	}

IL_0075:
	{
		G_B13_0 = 1;
	}

IL_0076:
	{
		return G_B13_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds()
extern "C"  Bounds_t3033363703  UIWidget_CalculateBounds_m2654683567 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Bounds_t3033363703  L_0 = UIWidget_CalculateBounds_m3265295620(__this, (Transform_t3275118058 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CalculateBounds_m3265295620_MetadataUsageId;
extern "C"  Bounds_t3033363703  UIWidget_CalculateBounds_m3265295620 (UIWidget_t1453041918 * __this, Transform_t3275118058 * ___relativeParent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CalculateBounds_m3265295620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Matrix4x4_t2933234003  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3U5BU5D_t1172311765* V_4 = NULL;
	Bounds_t3033363703  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	{
		Transform_t3275118058 * L_0 = ___relativeParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(10 /* UnityEngine.Vector3[] UIRect::get_localCorners() */, __this);
		V_0 = L_2;
		Vector3U5BU5D_t1172311765* L_3 = V_0;
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m1202659404((&V_1), (*(Vector3_t2243707580 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), L_4, /*hidden argument*/NULL);
		V_2 = 1;
		goto IL_0049;
	}

IL_0032:
	{
		Vector3U5BU5D_t1172311765* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		Bounds_Encapsulate_m3688171368((&V_1), (*(Vector3_t2243707580 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) < ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		Bounds_t3033363703  L_9 = V_1;
		return L_9;
	}

IL_0052:
	{
		Transform_t3275118058 * L_10 = ___relativeParent0;
		NullCheck(L_10);
		Matrix4x4_t2933234003  L_11 = Transform_get_worldToLocalMatrix_m3299477436(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Vector3U5BU5D_t1172311765* L_12 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, __this);
		V_4 = L_12;
		Vector3U5BU5D_t1172311765* L_13 = V_4;
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Matrix4x4_MultiplyPoint3x4_m1007952212((&V_3), (*(Vector3_t2243707580 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m1202659404((&V_5), L_14, L_15, /*hidden argument*/NULL);
		V_6 = 1;
		goto IL_00ab;
	}

IL_0089:
	{
		Vector3U5BU5D_t1172311765* L_16 = V_4;
		int32_t L_17 = V_6;
		NullCheck(L_16);
		Vector3_t2243707580  L_18 = Matrix4x4_MultiplyPoint3x4_m1007952212((&V_3), (*(Vector3_t2243707580 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))), /*hidden argument*/NULL);
		Bounds_Encapsulate_m3688171368((&V_5), L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00ab:
	{
		int32_t L_20 = V_6;
		if ((((int32_t)L_20) < ((int32_t)4)))
		{
			goto IL_0089;
		}
	}
	{
		Bounds_t3033363703  L_21 = V_5;
		return L_21;
	}
}
// System.Void UIWidget::SetDirty()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetDirty_m3367984983_MetadataUsageId;
extern "C"  void UIWidget_SetDirty_m3367984983 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDirty_m3367984983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIDrawCall_t3291843512 * L_0 = __this->get_drawCall_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		UIDrawCall_t3291843512 * L_2 = __this->get_drawCall_46();
		NullCheck(L_2);
		L_2->set_isDirty_30((bool)1);
		goto IL_003f;
	}

IL_0022:
	{
		bool L_3 = UIWidget_get_isVisible_m1555008678(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = UIWidget_get_hasVertices_m455088681(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		UIWidget_CreatePanel_m3689313888(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void UIWidget::RemoveFromPanel()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_RemoveFromPanel_m2657016785_MetadataUsageId;
extern "C"  void UIWidget_RemoveFromPanel_m2657016785 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_RemoveFromPanel_m2657016785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIPanel_t1795085332 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		UIPanel_t1795085332 * L_2 = __this->get_panel_35();
		NullCheck(L_2);
		UIPanel_RemoveWidget_m2792282035(L_2, __this, /*hidden argument*/NULL);
		__this->set_panel_35((UIPanel_t1795085332 *)NULL);
	}

IL_0024:
	{
		__this->set_drawCall_46((UIDrawCall_t3291843512 *)NULL);
		return;
	}
}
// System.Void UIWidget::MarkAsChanged()
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_MarkAsChanged_m3276847846_MetadataUsageId;
extern "C"  void UIWidget_MarkAsChanged_m3276847846 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MarkAsChanged_m3276847846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m2411194121(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0055;
		}
	}
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
		UIPanel_t1795085332 * L_1 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m96773627(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		bool L_6 = __this->get_mPlayMode_38();
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		UIWidget_SetDirty_m3367984983(__this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m2160812004(__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// UIPanel UIWidget::CreatePanel()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern Il2CppClass* UIPanel_t1795085332_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CreatePanel_m3689313888_MetadataUsageId;
extern "C"  UIPanel_t1795085332 * UIWidget_CreatePanel_m3689313888 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CreatePanel_m3689313888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((UIRect_t4127168124 *)__this)->get_mStarted_11();
		if (!L_0)
		{
			goto IL_0085;
		}
	}
	{
		UIPanel_t1795085332 * L_1 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0085;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m96773627(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		Transform_t3275118058 * L_6 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = UIRect_get_cachedGameObject_m4223691068(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = GameObject_get_layer_m725607808(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t1795085332_il2cpp_TypeInfo_var);
		UIPanel_t1795085332 * L_9 = UIPanel_Find_m3221384014(NULL /*static, unused*/, L_6, (bool)1, L_8, /*hidden argument*/NULL);
		__this->set_panel_35(L_9);
		UIPanel_t1795085332 * L_10 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0085;
		}
	}
	{
		((UIRect_t4127168124 *)__this)->set_mParentFound_12((bool)0);
		UIPanel_t1795085332 * L_12 = __this->get_panel_35();
		NullCheck(L_12);
		UIPanel_AddWidget_m318008474(L_12, __this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m2160812004(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIRect::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0085:
	{
		UIPanel_t1795085332 * L_13 = __this->get_panel_35();
		return L_13;
	}
}
// System.Void UIWidget::CheckLayer()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2775886444;
extern const uint32_t UIWidget_CheckLayer_m2160812004_MetadataUsageId;
extern "C"  void UIWidget_CheckLayer_m2160812004 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CheckLayer_m2160812004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIPanel_t1795085332 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		UIPanel_t1795085332 * L_2 = __this->get_panel_35();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m725607808(L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = GameObject_get_layer_m725607808(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_6)))
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, _stringLiteral2775886444, __this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		UIPanel_t1795085332 * L_8 = __this->get_panel_35();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = GameObject_get_layer_m725607808(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_set_layer_m2712461877(L_7, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UIWidget::ParentHasChanged()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UIPanel_t1795085332_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_ParentHasChanged_m3532498979_MetadataUsageId;
extern "C"  void UIWidget_ParentHasChanged_m3532498979 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ParentHasChanged_m3532498979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIPanel_t1795085332 * V_0 = NULL;
	{
		UIRect_ParentHasChanged_m2147319067(__this, /*hidden argument*/NULL);
		UIPanel_t1795085332 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t3275118058 * L_2 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = UIRect_get_cachedGameObject_m4223691068(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m725607808(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t1795085332_il2cpp_TypeInfo_var);
		UIPanel_t1795085332 * L_5 = UIPanel_Find_m3221384014(NULL /*static, unused*/, L_2, (bool)1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		UIPanel_t1795085332 * L_6 = __this->get_panel_35();
		UIPanel_t1795085332 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		UIWidget_RemoveFromPanel_m2657016785(__this, /*hidden argument*/NULL);
		UIWidget_CreatePanel_m3689313888(__this, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UIWidget::Awake()
extern "C"  void UIWidget_Awake_m1113369770 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		((UIRect_t4127168124 *)__this)->set_mGo_7(L_0);
		bool L_1 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_38(L_1);
		return;
	}
}
// System.Void UIWidget::OnInit()
extern "C"  void UIWidget_OnInit_m2528938130 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		UIRect_OnInit_m2134624388(__this, /*hidden argument*/NULL);
		UIWidget_RemoveFromPanel_m2657016785(__this, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)1);
		int32_t L_0 = __this->get_mWidth_24();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_1 = __this->get_mHeight_25();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_0060;
		}
	}
	{
		Transform_t3275118058 * L_2 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_localScale_m3074381503(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)(8.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		VirtActionInvoker0::Invoke(32 /* System.Void UIWidget::UpgradeFrom265() */, __this);
		Transform_t3275118058 * L_5 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m2325460848(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0060:
	{
		UIRect_Update_m898912158(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::UpgradeFrom265()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpgradeFrom265_m1253861766_MetadataUsageId;
extern "C"  void UIWidget_UpgradeFrom265_m1253861766 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpgradeFrom265_m1253861766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_localScale_m3074381503(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = Mathf_Abs_m1192949464(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_mWidth_24(L_4);
		float L_5 = (&V_0)->get_y_2();
		int32_t L_6 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = Mathf_Abs_m1192949464(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mHeight_25(L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m98388961(NULL /*static, unused*/, L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnStart()
extern "C"  void UIWidget_OnStart_m4254763914 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		UIWidget_CreatePanel_m3689313888(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnAnchor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_OnAnchor_m1980491791_MetadataUsageId;
extern "C"  void UIWidget_OnAnchor_m1980491791 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnAnchor_m1980491791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Transform_t3275118058 * V_4 = NULL;
	Transform_t3275118058 * V_5 = NULL;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3U5BU5D_t1172311765* V_10 = NULL;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3U5BU5D_t1172311765* V_12 = NULL;
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3U5BU5D_t1172311765* V_14 = NULL;
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3U5BU5D_t1172311765* V_16 = NULL;
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	UIWidget_t1453041918 * G_B7_0 = NULL;
	UIWidget_t1453041918 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	UIWidget_t1453041918 * G_B8_1 = NULL;
	{
		Transform_t3275118058 * L_0 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		V_4 = L_0;
		Transform_t3275118058 * L_1 = V_4;
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_get_parent_m147407266(L_1, /*hidden argument*/NULL);
		V_5 = L_2;
		Transform_t3275118058 * L_3 = V_4;
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_localPosition_m2533925116(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		Vector2_t2243707579  L_5 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_7 = L_5;
		AnchorPoint_t4057058294 * L_6 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = L_6->get_target_0();
		AnchorPoint_t4057058294 * L_8 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t4057058294 * L_11 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = L_11->get_target_0();
		AnchorPoint_t4057058294 * L_13 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = L_13->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t4057058294 * L_16 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = L_16->get_target_0();
		AnchorPoint_t4057058294 * L_18 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = L_18->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t4057058294 * L_21 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		Transform_t3275118058 * L_22 = V_5;
		NullCheck(L_21);
		Vector3U5BU5D_t1172311765* L_23 = AnchorPoint_GetSides_m2242848644(L_21, L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		Vector3U5BU5D_t1172311765* L_24 = V_8;
		if (!L_24)
		{
			goto IL_0184;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_25 = V_8;
		NullCheck(L_25);
		float L_26 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1172311765* L_27 = V_8;
		NullCheck(L_27);
		float L_28 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t4057058294 * L_29 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_29);
		float L_30 = L_29->get_relative_1();
		float L_31 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_26, L_28, L_30, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_32 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_32);
		int32_t L_33 = L_32->get_absolute_2();
		V_0 = ((float)((float)L_31+(float)(((float)((float)L_33)))));
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		NullCheck(L_34);
		float L_35 = ((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1172311765* L_36 = V_8;
		NullCheck(L_36);
		float L_37 = ((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t4057058294 * L_38 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_38);
		float L_39 = L_38->get_relative_1();
		float L_40 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_41 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_absolute_2();
		V_2 = ((float)((float)L_40+(float)(((float)((float)L_42)))));
		Vector3U5BU5D_t1172311765* L_43 = V_8;
		NullCheck(L_43);
		float L_44 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1172311765* L_45 = V_8;
		NullCheck(L_45);
		float L_46 = ((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t4057058294 * L_47 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_47);
		float L_48 = L_47->get_relative_1();
		float L_49 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_44, L_46, L_48, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_50 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_50);
		int32_t L_51 = L_50->get_absolute_2();
		V_1 = ((float)((float)L_49+(float)(((float)((float)L_51)))));
		Vector3U5BU5D_t1172311765* L_52 = V_8;
		NullCheck(L_52);
		float L_53 = ((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1172311765* L_54 = V_8;
		NullCheck(L_54);
		float L_55 = ((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t4057058294 * L_56 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_56);
		float L_57 = L_56->get_relative_1();
		float L_58 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_53, L_55, L_57, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_59 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_59);
		int32_t L_60 = L_59->get_absolute_2();
		V_3 = ((float)((float)L_58+(float)(((float)((float)L_60)))));
		__this->set_mIsInFront_43((bool)1);
		goto IL_020d;
	}

IL_0184:
	{
		AnchorPoint_t4057058294 * L_61 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		Transform_t3275118058 * L_62 = V_5;
		Vector3_t2243707580  L_63 = UIRect_GetLocalPos_m1346399807(__this, L_61, L_62, /*hidden argument*/NULL);
		V_9 = L_63;
		float L_64 = (&V_9)->get_x_1();
		AnchorPoint_t4057058294 * L_65 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_65);
		int32_t L_66 = L_65->get_absolute_2();
		V_0 = ((float)((float)L_64+(float)(((float)((float)L_66)))));
		float L_67 = (&V_9)->get_y_2();
		AnchorPoint_t4057058294 * L_68 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_68);
		int32_t L_69 = L_68->get_absolute_2();
		V_1 = ((float)((float)L_67+(float)(((float)((float)L_69)))));
		float L_70 = (&V_9)->get_x_1();
		AnchorPoint_t4057058294 * L_71 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_absolute_2();
		V_2 = ((float)((float)L_70+(float)(((float)((float)L_72)))));
		float L_73 = (&V_9)->get_y_2();
		AnchorPoint_t4057058294 * L_74 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_74);
		int32_t L_75 = L_74->get_absolute_2();
		V_3 = ((float)((float)L_73+(float)(((float)((float)L_75)))));
		bool L_76 = __this->get_hideIfOffScreen_31();
		G_B6_0 = __this;
		if (!L_76)
		{
			G_B7_0 = __this;
			goto IL_0207;
		}
	}
	{
		float L_77 = (&V_9)->get_z_3();
		G_B8_0 = ((((int32_t)((!(((float)L_77) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B8_1 = G_B6_0;
		goto IL_0208;
	}

IL_0207:
	{
		G_B8_0 = 1;
		G_B8_1 = G_B7_0;
	}

IL_0208:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mIsInFront_43((bool)G_B8_0);
	}

IL_020d:
	{
		goto IL_04d1;
	}

IL_0212:
	{
		__this->set_mIsInFront_43((bool)1);
		AnchorPoint_t4057058294 * L_78 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_78);
		Transform_t3275118058 * L_79 = L_78->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_80 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02ab;
		}
	}
	{
		AnchorPoint_t4057058294 * L_81 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		Transform_t3275118058 * L_82 = V_5;
		NullCheck(L_81);
		Vector3U5BU5D_t1172311765* L_83 = AnchorPoint_GetSides_m2242848644(L_81, L_82, /*hidden argument*/NULL);
		V_10 = L_83;
		Vector3U5BU5D_t1172311765* L_84 = V_10;
		if (!L_84)
		{
			goto IL_0281;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_85 = V_10;
		NullCheck(L_85);
		float L_86 = ((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1172311765* L_87 = V_10;
		NullCheck(L_87);
		float L_88 = ((L_87)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t4057058294 * L_89 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_89);
		float L_90 = L_89->get_relative_1();
		float L_91 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_86, L_88, L_90, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_92 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_92);
		int32_t L_93 = L_92->get_absolute_2();
		V_0 = ((float)((float)L_91+(float)(((float)((float)L_93)))));
		goto IL_02a6;
	}

IL_0281:
	{
		AnchorPoint_t4057058294 * L_94 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		Transform_t3275118058 * L_95 = V_5;
		Vector3_t2243707580  L_96 = UIRect_GetLocalPos_m1346399807(__this, L_94, L_95, /*hidden argument*/NULL);
		V_11 = L_96;
		float L_97 = (&V_11)->get_x_1();
		AnchorPoint_t4057058294 * L_98 = ((UIRect_t4127168124 *)__this)->get_leftAnchor_2();
		NullCheck(L_98);
		int32_t L_99 = L_98->get_absolute_2();
		V_0 = ((float)((float)L_97+(float)(((float)((float)L_99)))));
	}

IL_02a6:
	{
		goto IL_02c3;
	}

IL_02ab:
	{
		float L_100 = (&V_6)->get_x_1();
		float L_101 = (&V_7)->get_x_0();
		int32_t L_102 = __this->get_mWidth_24();
		V_0 = ((float)((float)L_100-(float)((float)((float)L_101*(float)(((float)((float)L_102)))))));
	}

IL_02c3:
	{
		AnchorPoint_t4057058294 * L_103 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_103);
		Transform_t3275118058 * L_104 = L_103->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_105 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_0355;
		}
	}
	{
		AnchorPoint_t4057058294 * L_106 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		Transform_t3275118058 * L_107 = V_5;
		NullCheck(L_106);
		Vector3U5BU5D_t1172311765* L_108 = AnchorPoint_GetSides_m2242848644(L_106, L_107, /*hidden argument*/NULL);
		V_12 = L_108;
		Vector3U5BU5D_t1172311765* L_109 = V_12;
		if (!L_109)
		{
			goto IL_032b;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_110 = V_12;
		NullCheck(L_110);
		float L_111 = ((L_110)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t1172311765* L_112 = V_12;
		NullCheck(L_112);
		float L_113 = ((L_112)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t4057058294 * L_114 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_114);
		float L_115 = L_114->get_relative_1();
		float L_116 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_111, L_113, L_115, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_117 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_117);
		int32_t L_118 = L_117->get_absolute_2();
		V_2 = ((float)((float)L_116+(float)(((float)((float)L_118)))));
		goto IL_0350;
	}

IL_032b:
	{
		AnchorPoint_t4057058294 * L_119 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		Transform_t3275118058 * L_120 = V_5;
		Vector3_t2243707580  L_121 = UIRect_GetLocalPos_m1346399807(__this, L_119, L_120, /*hidden argument*/NULL);
		V_13 = L_121;
		float L_122 = (&V_13)->get_x_1();
		AnchorPoint_t4057058294 * L_123 = ((UIRect_t4127168124 *)__this)->get_rightAnchor_3();
		NullCheck(L_123);
		int32_t L_124 = L_123->get_absolute_2();
		V_2 = ((float)((float)L_122+(float)(((float)((float)L_124)))));
	}

IL_0350:
	{
		goto IL_0375;
	}

IL_0355:
	{
		float L_125 = (&V_6)->get_x_1();
		float L_126 = (&V_7)->get_x_0();
		int32_t L_127 = __this->get_mWidth_24();
		int32_t L_128 = __this->get_mWidth_24();
		V_2 = ((float)((float)((float)((float)L_125-(float)((float)((float)L_126*(float)(((float)((float)L_127)))))))+(float)(((float)((float)L_128)))));
	}

IL_0375:
	{
		AnchorPoint_t4057058294 * L_129 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_129);
		Transform_t3275118058 * L_130 = L_129->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_131 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_130, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0407;
		}
	}
	{
		AnchorPoint_t4057058294 * L_132 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		Transform_t3275118058 * L_133 = V_5;
		NullCheck(L_132);
		Vector3U5BU5D_t1172311765* L_134 = AnchorPoint_GetSides_m2242848644(L_132, L_133, /*hidden argument*/NULL);
		V_14 = L_134;
		Vector3U5BU5D_t1172311765* L_135 = V_14;
		if (!L_135)
		{
			goto IL_03dd;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_136 = V_14;
		NullCheck(L_136);
		float L_137 = ((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1172311765* L_138 = V_14;
		NullCheck(L_138);
		float L_139 = ((L_138)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t4057058294 * L_140 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_140);
		float L_141 = L_140->get_relative_1();
		float L_142 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_137, L_139, L_141, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_143 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_143);
		int32_t L_144 = L_143->get_absolute_2();
		V_1 = ((float)((float)L_142+(float)(((float)((float)L_144)))));
		goto IL_0402;
	}

IL_03dd:
	{
		AnchorPoint_t4057058294 * L_145 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		Transform_t3275118058 * L_146 = V_5;
		Vector3_t2243707580  L_147 = UIRect_GetLocalPos_m1346399807(__this, L_145, L_146, /*hidden argument*/NULL);
		V_15 = L_147;
		float L_148 = (&V_15)->get_y_2();
		AnchorPoint_t4057058294 * L_149 = ((UIRect_t4127168124 *)__this)->get_bottomAnchor_4();
		NullCheck(L_149);
		int32_t L_150 = L_149->get_absolute_2();
		V_1 = ((float)((float)L_148+(float)(((float)((float)L_150)))));
	}

IL_0402:
	{
		goto IL_041f;
	}

IL_0407:
	{
		float L_151 = (&V_6)->get_y_2();
		float L_152 = (&V_7)->get_y_1();
		int32_t L_153 = __this->get_mHeight_25();
		V_1 = ((float)((float)L_151-(float)((float)((float)L_152*(float)(((float)((float)L_153)))))));
	}

IL_041f:
	{
		AnchorPoint_t4057058294 * L_154 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_154);
		Transform_t3275118058 * L_155 = L_154->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_156 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_155, /*hidden argument*/NULL);
		if (!L_156)
		{
			goto IL_04b1;
		}
	}
	{
		AnchorPoint_t4057058294 * L_157 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		Transform_t3275118058 * L_158 = V_5;
		NullCheck(L_157);
		Vector3U5BU5D_t1172311765* L_159 = AnchorPoint_GetSides_m2242848644(L_157, L_158, /*hidden argument*/NULL);
		V_16 = L_159;
		Vector3U5BU5D_t1172311765* L_160 = V_16;
		if (!L_160)
		{
			goto IL_0487;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_161 = V_16;
		NullCheck(L_161);
		float L_162 = ((L_161)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t1172311765* L_163 = V_16;
		NullCheck(L_163);
		float L_164 = ((L_163)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t4057058294 * L_165 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_165);
		float L_166 = L_165->get_relative_1();
		float L_167 = NGUIMath_Lerp_m4160563120(NULL /*static, unused*/, L_162, L_164, L_166, /*hidden argument*/NULL);
		AnchorPoint_t4057058294 * L_168 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_168);
		int32_t L_169 = L_168->get_absolute_2();
		V_3 = ((float)((float)L_167+(float)(((float)((float)L_169)))));
		goto IL_04ac;
	}

IL_0487:
	{
		AnchorPoint_t4057058294 * L_170 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		Transform_t3275118058 * L_171 = V_5;
		Vector3_t2243707580  L_172 = UIRect_GetLocalPos_m1346399807(__this, L_170, L_171, /*hidden argument*/NULL);
		V_17 = L_172;
		float L_173 = (&V_17)->get_y_2();
		AnchorPoint_t4057058294 * L_174 = ((UIRect_t4127168124 *)__this)->get_topAnchor_5();
		NullCheck(L_174);
		int32_t L_175 = L_174->get_absolute_2();
		V_3 = ((float)((float)L_173+(float)(((float)((float)L_175)))));
	}

IL_04ac:
	{
		goto IL_04d1;
	}

IL_04b1:
	{
		float L_176 = (&V_6)->get_y_2();
		float L_177 = (&V_7)->get_y_1();
		int32_t L_178 = __this->get_mHeight_25();
		int32_t L_179 = __this->get_mHeight_25();
		V_3 = ((float)((float)((float)((float)L_176-(float)((float)((float)L_177*(float)(((float)((float)L_178)))))))+(float)(((float)((float)L_179)))));
	}

IL_04d1:
	{
		float L_180 = V_0;
		float L_181 = V_2;
		float L_182 = (&V_7)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_183 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_180, L_181, L_182, /*hidden argument*/NULL);
		float L_184 = V_1;
		float L_185 = V_3;
		float L_186 = (&V_7)->get_y_1();
		float L_187 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_184, L_185, L_186, /*hidden argument*/NULL);
		float L_188 = (&V_6)->get_z_3();
		Vector3__ctor_m2638739322((&V_18), L_183, L_187, L_188, /*hidden argument*/NULL);
		float L_189 = (&V_18)->get_x_1();
		float L_190 = bankers_roundf(L_189);
		(&V_18)->set_x_1(L_190);
		float L_191 = (&V_18)->get_y_2();
		float L_192 = bankers_roundf(L_191);
		(&V_18)->set_y_2(L_192);
		float L_193 = V_2;
		float L_194 = V_0;
		int32_t L_195 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, ((float)((float)((float)((float)L_193-(float)L_194))+(float)(0.5f))), /*hidden argument*/NULL);
		V_19 = L_195;
		float L_196 = V_3;
		float L_197 = V_1;
		int32_t L_198 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, ((float)((float)((float)((float)L_196-(float)L_197))+(float)(0.5f))), /*hidden argument*/NULL);
		V_20 = L_198;
		int32_t L_199 = __this->get_keepAspectRatio_32();
		if (!L_199)
		{
			goto IL_058f;
		}
	}
	{
		float L_200 = __this->get_aspectRatio_33();
		if ((((float)L_200) == ((float)(0.0f))))
		{
			goto IL_058f;
		}
	}
	{
		int32_t L_201 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_201) == ((uint32_t)2))))
		{
			goto IL_057e;
		}
	}
	{
		int32_t L_202 = V_20;
		float L_203 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_204 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_202)))*(float)L_203)), /*hidden argument*/NULL);
		V_19 = L_204;
		goto IL_058f;
	}

IL_057e:
	{
		int32_t L_205 = V_19;
		float L_206 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_207 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_205)))/(float)L_206)), /*hidden argument*/NULL);
		V_20 = L_207;
	}

IL_058f:
	{
		int32_t L_208 = V_19;
		int32_t L_209 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_208) >= ((int32_t)L_209)))
		{
			goto IL_05a4;
		}
	}
	{
		int32_t L_210 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_19 = L_210;
	}

IL_05a4:
	{
		int32_t L_211 = V_20;
		int32_t L_212 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_211) >= ((int32_t)L_212)))
		{
			goto IL_05b9;
		}
	}
	{
		int32_t L_213 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_20 = L_213;
	}

IL_05b9:
	{
		Vector3_t2243707580  L_214 = V_6;
		Vector3_t2243707580  L_215 = V_18;
		Vector3_t2243707580  L_216 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_214, L_215, /*hidden argument*/NULL);
		float L_217 = Vector3_SqrMagnitude_m3759098164(NULL /*static, unused*/, L_216, /*hidden argument*/NULL);
		if ((!(((float)L_217) > ((float)(0.001f)))))
		{
			goto IL_05f0;
		}
	}
	{
		Transform_t3275118058 * L_218 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_219 = V_18;
		NullCheck(L_218);
		Transform_set_localPosition_m1026930133(L_218, L_219, /*hidden argument*/NULL);
		bool L_220 = __this->get_mIsInFront_43();
		if (!L_220)
		{
			goto IL_05f0;
		}
	}
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
	}

IL_05f0:
	{
		int32_t L_221 = __this->get_mWidth_24();
		int32_t L_222 = V_19;
		if ((!(((uint32_t)L_221) == ((uint32_t)L_222))))
		{
			goto IL_060a;
		}
	}
	{
		int32_t L_223 = __this->get_mHeight_25();
		int32_t L_224 = V_20;
		if ((((int32_t)L_223) == ((int32_t)L_224)))
		{
			goto IL_063d;
		}
	}

IL_060a:
	{
		int32_t L_225 = V_19;
		__this->set_mWidth_24(L_225);
		int32_t L_226 = V_20;
		__this->set_mHeight_25(L_226);
		bool L_227 = __this->get_mIsInFront_43();
		if (!L_227)
		{
			goto IL_062c;
		}
	}
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
	}

IL_062c:
	{
		bool L_228 = __this->get_autoResizeBoxCollider_30();
		if (!L_228)
		{
			goto IL_063d;
		}
	}
	{
		UIWidget_ResizeCollider_m4072963299(__this, /*hidden argument*/NULL);
	}

IL_063d:
	{
		return;
	}
}
// System.Void UIWidget::OnUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_OnUpdate_m4095118167_MetadataUsageId;
extern "C"  void UIWidget_OnUpdate_m4095118167 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnUpdate_m4095118167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIPanel_t1795085332 * L_0 = __this->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m3689313888(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UIWidget::OnApplicationPause(System.Boolean)
extern "C"  void UIWidget_OnApplicationPause_m1203259445 (UIWidget_t1453041918 * __this, bool ___paused0, const MethodInfo* method)
{
	{
		bool L_0 = ___paused0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_000c:
	{
		return;
	}
}
// System.Void UIWidget::OnDisable()
extern "C"  void UIWidget_OnDisable_m1865079414 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		UIWidget_RemoveFromPanel_m2657016785(__this, /*hidden argument*/NULL);
		UIRect_OnDisable_m665651736(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnDestroy()
extern "C"  void UIWidget_OnDestroy_m4104971420 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		UIWidget_RemoveFromPanel_m2657016785(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern "C"  bool UIWidget_UpdateVisibility_m2482889154 (UIWidget_t1453041918 * __this, bool ___visibleByAlpha0, bool ___visibleByPanel1, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_41();
		bool L_1 = ___visibleByAlpha0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		bool L_2 = __this->get_mIsVisibleByPanel_42();
		bool L_3 = ___visibleByPanel1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002f;
		}
	}

IL_0018:
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
		bool L_4 = ___visibleByAlpha0;
		__this->set_mIsVisibleByAlpha_41(L_4);
		bool L_5 = ___visibleByPanel1;
		__this->set_mIsVisibleByPanel_42(L_5);
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean UIWidget::UpdateTransform(System.Int32)
extern "C"  bool UIWidget_UpdateTransform_m1671498507 (UIWidget_t1453041918 * __this, int32_t ___frame0, const MethodInfo* method)
{
	Transform_t3275118058 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t G_B13_0 = 0;
	{
		Transform_t3275118058 * L_0 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_38(L_1);
		bool L_2 = __this->get_mMoved_45();
		if (!L_2)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_mMoved_45((bool)1);
		__this->set_mMatrixFrame_49((-1));
		Transform_t3275118058 * L_3 = V_0;
		NullCheck(L_3);
		Transform_set_hasChanged_m2322582363(L_3, (bool)0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_0();
		int32_t L_6 = __this->get_mWidth_24();
		V_2 = ((float)((float)((-L_5))*(float)(((float)((float)L_6)))));
		float L_7 = (&V_1)->get_y_1();
		int32_t L_8 = __this->get_mHeight_25();
		V_3 = ((float)((float)((-L_7))*(float)(((float)((float)L_8)))));
		float L_9 = V_2;
		int32_t L_10 = __this->get_mWidth_24();
		V_4 = ((float)((float)L_9+(float)(((float)((float)L_10)))));
		float L_11 = V_3;
		int32_t L_12 = __this->get_mHeight_25();
		V_5 = ((float)((float)L_11+(float)(((float)((float)L_12)))));
		UIPanel_t1795085332 * L_13 = __this->get_panel_35();
		NullCheck(L_13);
		Matrix4x4_t2933234003 * L_14 = L_13->get_address_of_worldToLocal_35();
		Transform_t3275118058 * L_15 = V_0;
		float L_16 = V_2;
		float L_17 = V_3;
		NullCheck(L_15);
		Vector3_t2243707580  L_18 = Transform_TransformPoint_m3936139914(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Matrix4x4_MultiplyPoint3x4_m1007952212(L_14, L_18, /*hidden argument*/NULL);
		__this->set_mOldV0_50(L_19);
		UIPanel_t1795085332 * L_20 = __this->get_panel_35();
		NullCheck(L_20);
		Matrix4x4_t2933234003 * L_21 = L_20->get_address_of_worldToLocal_35();
		Transform_t3275118058 * L_22 = V_0;
		float L_23 = V_4;
		float L_24 = V_5;
		NullCheck(L_22);
		Vector3_t2243707580  L_25 = Transform_TransformPoint_m3936139914(L_22, L_23, L_24, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Matrix4x4_MultiplyPoint3x4_m1007952212(L_21, L_25, /*hidden argument*/NULL);
		__this->set_mOldV1_51(L_26);
		goto IL_01c3;
	}

IL_00be:
	{
		UIPanel_t1795085332 * L_27 = __this->get_panel_35();
		NullCheck(L_27);
		bool L_28 = L_27->get_widgetsAreStatic_26();
		if (L_28)
		{
			goto IL_01c3;
		}
	}
	{
		Transform_t3275118058 * L_29 = V_0;
		NullCheck(L_29);
		bool L_30 = Transform_get_hasChanged_m4220930228(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_01c3;
		}
	}
	{
		__this->set_mMoved_45((bool)1);
		__this->set_mMatrixFrame_49((-1));
		Transform_t3275118058 * L_31 = V_0;
		NullCheck(L_31);
		Transform_set_hasChanged_m2322582363(L_31, (bool)0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_32 = UIWidget_get_pivotOffset_m943693420(__this, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = (&V_6)->get_x_0();
		int32_t L_34 = __this->get_mWidth_24();
		V_7 = ((float)((float)((-L_33))*(float)(((float)((float)L_34)))));
		float L_35 = (&V_6)->get_y_1();
		int32_t L_36 = __this->get_mHeight_25();
		V_8 = ((float)((float)((-L_35))*(float)(((float)((float)L_36)))));
		float L_37 = V_7;
		int32_t L_38 = __this->get_mWidth_24();
		V_9 = ((float)((float)L_37+(float)(((float)((float)L_38)))));
		float L_39 = V_8;
		int32_t L_40 = __this->get_mHeight_25();
		V_10 = ((float)((float)L_39+(float)(((float)((float)L_40)))));
		UIPanel_t1795085332 * L_41 = __this->get_panel_35();
		NullCheck(L_41);
		Matrix4x4_t2933234003 * L_42 = L_41->get_address_of_worldToLocal_35();
		Transform_t3275118058 * L_43 = V_0;
		float L_44 = V_7;
		float L_45 = V_8;
		NullCheck(L_43);
		Vector3_t2243707580  L_46 = Transform_TransformPoint_m3936139914(L_43, L_44, L_45, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = Matrix4x4_MultiplyPoint3x4_m1007952212(L_42, L_46, /*hidden argument*/NULL);
		V_11 = L_47;
		UIPanel_t1795085332 * L_48 = __this->get_panel_35();
		NullCheck(L_48);
		Matrix4x4_t2933234003 * L_49 = L_48->get_address_of_worldToLocal_35();
		Transform_t3275118058 * L_50 = V_0;
		float L_51 = V_9;
		float L_52 = V_10;
		NullCheck(L_50);
		Vector3_t2243707580  L_53 = Transform_TransformPoint_m3936139914(L_50, L_51, L_52, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = Matrix4x4_MultiplyPoint3x4_m1007952212(L_49, L_53, /*hidden argument*/NULL);
		V_12 = L_54;
		Vector3_t2243707580  L_55 = __this->get_mOldV0_50();
		Vector3_t2243707580  L_56 = V_11;
		Vector3_t2243707580  L_57 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Vector3_SqrMagnitude_m3759098164(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if ((((float)L_58) > ((float)(1.0E-06f))))
		{
			goto IL_01ac;
		}
	}
	{
		Vector3_t2243707580  L_59 = __this->get_mOldV1_51();
		Vector3_t2243707580  L_60 = V_12;
		Vector3_t2243707580  L_61 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		float L_62 = Vector3_SqrMagnitude_m3759098164(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if ((!(((float)L_62) > ((float)(1.0E-06f)))))
		{
			goto IL_01c3;
		}
	}

IL_01ac:
	{
		__this->set_mMoved_45((bool)1);
		Vector3_t2243707580  L_63 = V_11;
		__this->set_mOldV0_50(L_63);
		Vector3_t2243707580  L_64 = V_12;
		__this->set_mOldV1_51(L_64);
	}

IL_01c3:
	{
		bool L_65 = __this->get_mMoved_45();
		if (!L_65)
		{
			goto IL_01e4;
		}
	}
	{
		OnDimensionsChanged_t3620741577 * L_66 = __this->get_onChange_27();
		if (!L_66)
		{
			goto IL_01e4;
		}
	}
	{
		OnDimensionsChanged_t3620741577 * L_67 = __this->get_onChange_27();
		NullCheck(L_67);
		OnDimensionsChanged_Invoke_m3518608402(L_67, /*hidden argument*/NULL);
	}

IL_01e4:
	{
		bool L_68 = __this->get_mMoved_45();
		if (L_68)
		{
			goto IL_01f7;
		}
	}
	{
		bool L_69 = ((UIRect_t4127168124 *)__this)->get_mChanged_10();
		G_B13_0 = ((int32_t)(L_69));
		goto IL_01f8;
	}

IL_01f7:
	{
		G_B13_0 = 1;
	}

IL_01f8:
	{
		return (bool)G_B13_0;
	}
}
// System.Boolean UIWidget::UpdateGeometry(System.Int32)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpdateGeometry_m387959493_MetadataUsageId;
extern "C"  bool UIWidget_UpdateGeometry_m387959493 (UIWidget_t1453041918 * __this, int32_t ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpdateGeometry_m387959493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		int32_t L_0 = ___frame0;
		float L_1 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, __this, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_mIsVisibleByAlpha_41();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		float L_3 = __this->get_mLastAlpha_44();
		float L_4 = V_0;
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)1);
	}

IL_0026:
	{
		float L_5 = V_0;
		__this->set_mLastAlpha_44(L_5);
		bool L_6 = ((UIRect_t4127168124 *)__this)->get_mChanged_10();
		if (!L_6)
		{
			goto IL_014e;
		}
	}
	{
		((UIRect_t4127168124 *)__this)->set_mChanged_10((bool)0);
		bool L_7 = __this->get_mIsVisibleByAlpha_41();
		if (!L_7)
		{
			goto IL_011a;
		}
	}
	{
		float L_8 = V_0;
		if ((!(((float)L_8) > ((float)(0.001f)))))
		{
			goto IL_011a;
		}
	}
	{
		Shader_t2430389951 * L_9 = VirtFuncInvoker0< Shader_t2430389951 * >::Invoke(28 /* UnityEngine.Shader UIWidget::get_shader() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_011a;
		}
	}
	{
		UIGeometry_t1005900006 * L_11 = __this->get_geometry_36();
		NullCheck(L_11);
		bool L_12 = UIGeometry_get_hasVertices_m2650326039(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		bool L_13 = __this->get_fillGeometry_37();
		if (!L_13)
		{
			goto IL_00af;
		}
	}
	{
		UIGeometry_t1005900006 * L_14 = __this->get_geometry_36();
		NullCheck(L_14);
		UIGeometry_Clear_m3476537084(L_14, /*hidden argument*/NULL);
		UIGeometry_t1005900006 * L_15 = __this->get_geometry_36();
		NullCheck(L_15);
		BetterList_1_t2464096222 * L_16 = L_15->get_verts_0();
		UIGeometry_t1005900006 * L_17 = __this->get_geometry_36();
		NullCheck(L_17);
		BetterList_1_t2464096221 * L_18 = L_17->get_uvs_1();
		UIGeometry_t1005900006 * L_19 = __this->get_geometry_36();
		NullCheck(L_19);
		BetterList_1_t1094906160 * L_20 = L_19->get_cols_2();
		VirtActionInvoker3< BetterList_1_t2464096222 *, BetterList_1_t2464096221 *, BetterList_1_t1094906160 * >::Invoke(38 /* System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>) */, __this, L_16, L_18, L_20);
	}

IL_00af:
	{
		UIGeometry_t1005900006 * L_21 = __this->get_geometry_36();
		NullCheck(L_21);
		bool L_22 = UIGeometry_get_hasVertices_m2650326039(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_23 = __this->get_mMatrixFrame_49();
		int32_t L_24 = ___frame0;
		if ((((int32_t)L_23) == ((int32_t)L_24)))
		{
			goto IL_00f3;
		}
	}
	{
		UIPanel_t1795085332 * L_25 = __this->get_panel_35();
		NullCheck(L_25);
		Matrix4x4_t2933234003  L_26 = L_25->get_worldToLocal_35();
		Transform_t3275118058 * L_27 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Matrix4x4_t2933234003  L_28 = Transform_get_localToWorldMatrix_m2868579006(L_27, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_29 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_40(L_29);
		int32_t L_30 = ___frame0;
		__this->set_mMatrixFrame_49(L_30);
	}

IL_00f3:
	{
		UIGeometry_t1005900006 * L_31 = __this->get_geometry_36();
		Matrix4x4_t2933234003  L_32 = __this->get_mLocalToPanel_40();
		UIPanel_t1795085332 * L_33 = __this->get_panel_35();
		NullCheck(L_33);
		bool L_34 = L_33->get_generateNormals_25();
		NullCheck(L_31);
		UIGeometry_ApplyTransform_m2467642938(L_31, L_32, L_34, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)0);
		return (bool)1;
	}

IL_0118:
	{
		bool L_35 = V_1;
		return L_35;
	}

IL_011a:
	{
		UIGeometry_t1005900006 * L_36 = __this->get_geometry_36();
		NullCheck(L_36);
		bool L_37 = UIGeometry_get_hasVertices_m2650326039(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0149;
		}
	}
	{
		bool L_38 = __this->get_fillGeometry_37();
		if (!L_38)
		{
			goto IL_0140;
		}
	}
	{
		UIGeometry_t1005900006 * L_39 = __this->get_geometry_36();
		NullCheck(L_39);
		UIGeometry_Clear_m3476537084(L_39, /*hidden argument*/NULL);
	}

IL_0140:
	{
		__this->set_mMoved_45((bool)0);
		return (bool)1;
	}

IL_0149:
	{
		goto IL_01c2;
	}

IL_014e:
	{
		bool L_40 = __this->get_mMoved_45();
		if (!L_40)
		{
			goto IL_01c2;
		}
	}
	{
		UIGeometry_t1005900006 * L_41 = __this->get_geometry_36();
		NullCheck(L_41);
		bool L_42 = UIGeometry_get_hasVertices_m2650326039(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_43 = __this->get_mMatrixFrame_49();
		int32_t L_44 = ___frame0;
		if ((((int32_t)L_43) == ((int32_t)L_44)))
		{
			goto IL_019d;
		}
	}
	{
		UIPanel_t1795085332 * L_45 = __this->get_panel_35();
		NullCheck(L_45);
		Matrix4x4_t2933234003  L_46 = L_45->get_worldToLocal_35();
		Transform_t3275118058 * L_47 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Matrix4x4_t2933234003  L_48 = Transform_get_localToWorldMatrix_m2868579006(L_47, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_49 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_40(L_49);
		int32_t L_50 = ___frame0;
		__this->set_mMatrixFrame_49(L_50);
	}

IL_019d:
	{
		UIGeometry_t1005900006 * L_51 = __this->get_geometry_36();
		Matrix4x4_t2933234003  L_52 = __this->get_mLocalToPanel_40();
		UIPanel_t1795085332 * L_53 = __this->get_panel_35();
		NullCheck(L_53);
		bool L_54 = L_53->get_generateNormals_25();
		NullCheck(L_51);
		UIGeometry_ApplyTransform_m2467642938(L_51, L_52, L_54, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)0);
		return (bool)1;
	}

IL_01c2:
	{
		__this->set_mMoved_45((bool)0);
		return (bool)0;
	}
}
// System.Void UIWidget::WriteToBuffers(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector4>)
extern "C"  void UIWidget_WriteToBuffers_m3134340184 (UIWidget_t1453041918 * __this, BetterList_1_t2464096222 * ___v0, BetterList_1_t2464096221 * ___u1, BetterList_1_t1094906160 * ___c2, BetterList_1_t2464096222 * ___n3, BetterList_1_t2464096223 * ___t4, const MethodInfo* method)
{
	{
		UIGeometry_t1005900006 * L_0 = __this->get_geometry_36();
		BetterList_1_t2464096222 * L_1 = ___v0;
		BetterList_1_t2464096221 * L_2 = ___u1;
		BetterList_1_t1094906160 * L_3 = ___c2;
		BetterList_1_t2464096222 * L_4 = ___n3;
		BetterList_1_t2464096223 * L_5 = ___t4;
		NullCheck(L_0);
		UIGeometry_WriteToBuffers_m4147440500(L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::MakePixelPerfect()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_MakePixelPerfect_m1134133674_MetadataUsageId;
extern "C"  void UIWidget_MakePixelPerfect_m1134133674 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MakePixelPerfect_m1134133674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_localPosition_m2533925116(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(L_2);
		(&V_0)->set_z_3(L_3);
		float L_4 = (&V_0)->get_x_1();
		float L_5 = bankers_roundf(L_4);
		(&V_0)->set_x_1(L_5);
		float L_6 = (&V_0)->get_y_2();
		float L_7 = bankers_roundf(L_6);
		(&V_0)->set_y_2(L_7);
		Transform_t3275118058 * L_8 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_localPosition_m1026930133(L_8, L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_localScale_m3074381503(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		Transform_t3275118058 * L_12 = UIRect_get_cachedTransform_m4093729156(__this, /*hidden argument*/NULL);
		float L_13 = (&V_1)->get_x_1();
		float L_14 = Mathf_Sign_m2039143327(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = (&V_1)->get_y_2();
		float L_16 = Mathf_Sign_m2039143327(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2638739322(&L_17, L_14, L_16, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m2325460848(L_12, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UIWidget::get_minWidth()
extern "C"  int32_t UIWidget_get_minWidth_m1221315338 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		return 2;
	}
}
// System.Int32 UIWidget::get_minHeight()
extern "C"  int32_t UIWidget_get_minHeight_m2377742789 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		return 2;
	}
}
// UnityEngine.Vector4 UIWidget::get_border()
extern "C"  Vector4_t2243707581  UIWidget_get_border_m2075395219 (UIWidget_t1453041918 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = Vector4_get_zero_m3810945132(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UIWidget::set_border(UnityEngine.Vector4)
extern "C"  void UIWidget_set_border_m3688808592 (UIWidget_t1453041918 * __this, Vector4_t2243707581  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIWidget_OnFill_m1558183438 (UIWidget_t1453041918 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIWidget/HitCheck::.ctor(System.Object,System.IntPtr)
extern "C"  void HitCheck__ctor_m3643541551 (HitCheck_t3590501724 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UIWidget/HitCheck::Invoke(UnityEngine.Vector3)
extern "C"  bool HitCheck_Invoke_m2489327010 (HitCheck_t3590501724 * __this, Vector3_t2243707580  ___worldPos0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		HitCheck_Invoke_m2489327010((HitCheck_t3590501724 *)__this->get_prev_9(),___worldPos0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___worldPos0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___worldPos0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___worldPos0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___worldPos0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  bool DelegatePInvokeWrapper_HitCheck_t3590501724 (HitCheck_t3590501724 * __this, Vector3_t2243707580  ___worldPos0, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(Vector3_t2243707580 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___worldPos0);

	return static_cast<bool>(returnValue);
}
// System.IAsyncResult UIWidget/HitCheck::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t HitCheck_BeginInvoke_m2348926761_MetadataUsageId;
extern "C"  Il2CppObject * HitCheck_BeginInvoke_m2348926761 (HitCheck_t3590501724 * __this, Vector3_t2243707580  ___worldPos0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitCheck_BeginInvoke_m2348926761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___worldPos0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UIWidget/HitCheck::EndInvoke(System.IAsyncResult)
extern "C"  bool HitCheck_EndInvoke_m738858617 (HitCheck_t3590501724 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void UIWidget/OnDimensionsChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDimensionsChanged__ctor_m701176096 (OnDimensionsChanged_t3620741577 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnDimensionsChanged::Invoke()
extern "C"  void OnDimensionsChanged_Invoke_m3518608402 (OnDimensionsChanged_t3620741577 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDimensionsChanged_Invoke_m3518608402((OnDimensionsChanged_t3620741577 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnDimensionsChanged_t3620741577 (OnDimensionsChanged_t3620741577 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UIWidget/OnDimensionsChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDimensionsChanged_BeginInvoke_m3542901479 (OnDimensionsChanged_t3620741577 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UIWidget/OnDimensionsChanged::EndInvoke(System.IAsyncResult)
extern "C"  void OnDimensionsChanged_EndInvoke_m3075486630 (OnDimensionsChanged_t3620741577 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIWidget/OnPostFillCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostFillCallback__ctor_m1735764407 (OnPostFillCallback_t836279582 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnPostFillCallback::Invoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void OnPostFillCallback_Invoke_m2953004057 (OnPostFillCallback_t836279582 * __this, UIWidget_t1453041918 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnPostFillCallback_Invoke_m2953004057((OnPostFillCallback_t836279582 *)__this->get_prev_9(),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UIWidget_t1453041918 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, UIWidget_t1453041918 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UIWidget/OnPostFillCallback::BeginInvoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t OnPostFillCallback_BeginInvoke_m3876064332_MetadataUsageId;
extern "C"  Il2CppObject * OnPostFillCallback_BeginInvoke_m3876064332 (OnPostFillCallback_t836279582 * __this, UIWidget_t1453041918 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPostFillCallback_BeginInvoke_m3876064332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = ___widget0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___bufferOffset1);
	__d_args[2] = ___verts2;
	__d_args[3] = ___uvs3;
	__d_args[4] = ___cols4;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Void UIWidget/OnPostFillCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostFillCallback_EndInvoke_m1375087617 (OnPostFillCallback_t836279582 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIWidgetContainer::.ctor()
extern "C"  void UIWidgetContainer__ctor_m1800355576 (UIWidgetContainer_t701016325 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::.ctor()
extern Il2CppClass* List_1_t2644239190_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2292054616_MethodInfo_var;
extern const uint32_t UIWrapContent__ctor_m1536675230_MetadataUsageId;
extern "C"  void UIWrapContent__ctor_m1536675230 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent__ctor_m1536675230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_itemSize_2(((int32_t)100));
		__this->set_cullContent_3((bool)1);
		__this->set_mFirstTime_11((bool)1);
		List_1_t2644239190 * L_0 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_0, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set_mChildren_12(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* OnClippingMoved_t4045505957_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIPanel_t1795085332_m1654704505_MethodInfo_var;
extern const uint32_t UIWrapContent_Start_m3780419666_MetadataUsageId;
extern "C"  void UIWrapContent_Start_m3780419666 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_Start_m3780419666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIWrapContent_SortBasedOnScrollMovement_m996357908(__this, /*hidden argument*/NULL);
		UIWrapContent_WrapContent_m3270887951(__this, /*hidden argument*/NULL);
		UIScrollView_t3033954930 * L_0 = __this->get_mScroll_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		UIScrollView_t3033954930 * L_2 = __this->get_mScroll_9();
		NullCheck(L_2);
		UIPanel_t1795085332 * L_3 = Component_GetComponent_TisUIPanel_t1795085332_m1654704505(L_2, /*hidden argument*/Component_GetComponent_TisUIPanel_t1795085332_m1654704505_MethodInfo_var);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 5));
		OnClippingMoved_t4045505957 * L_5 = (OnClippingMoved_t4045505957 *)il2cpp_codegen_object_new(OnClippingMoved_t4045505957_il2cpp_TypeInfo_var);
		OnClippingMoved__ctor_m3705441944(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_onClipMove_37(L_5);
	}

IL_003a:
	{
		__this->set_mFirstTime_11((bool)0);
		return;
	}
}
// System.Void UIWrapContent::OnMove(UIPanel)
extern "C"  void UIWrapContent_OnMove_m664260418 (UIWrapContent_t1931723019 * __this, UIPanel_t1795085332 * ___panel0, const MethodInfo* method)
{
	{
		UIWrapContent_WrapContent_m3270887951(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::SortBasedOnScrollMovement()
extern Il2CppClass* UIWrapContent_t1931723019_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t241889613_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2376290435_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3606265932_MethodInfo_var;
extern const MethodInfo* UIGrid_SortHorizontal_m4102275981_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1954584929_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2663355476_MethodInfo_var;
extern const MethodInfo* UIGrid_SortVertical_m3913489617_MethodInfo_var;
extern const uint32_t UIWrapContent_SortBasedOnScrollMovement_m996357908_MetadataUsageId;
extern "C"  void UIWrapContent_SortBasedOnScrollMovement_m996357908 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortBasedOnScrollMovement_m996357908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t2644239190 * G_B8_0 = NULL;
	List_1_t2644239190 * G_B7_0 = NULL;
	List_1_t2644239190 * G_B11_0 = NULL;
	List_1_t2644239190 * G_B10_0 = NULL;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m31863134(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t2644239190 * L_1 = __this->get_mChildren_12();
		NullCheck(L_1);
		List_1_Clear_m2376290435(L_1, /*hidden argument*/List_1_Clear_m2376290435_MethodInfo_var);
		V_0 = 0;
		goto IL_0039;
	}

IL_001e:
	{
		List_1_t2644239190 * L_2 = __this->get_mChildren_12();
		Transform_t3275118058 * L_3 = __this->get_mTrans_7();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Add_m3606265932(L_2, L_5, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_7 = V_0;
		Transform_t3275118058 * L_8 = __this->get_mTrans_7();
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m881385315(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_001e;
		}
	}
	{
		bool L_10 = __this->get_mHorizontal_10();
		if (!L_10)
		{
			goto IL_0082;
		}
	}
	{
		List_1_t2644239190 * L_11 = __this->get_mChildren_12();
		Comparison_1_t241889613 * L_12 = ((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_13();
		G_B7_0 = L_11;
		if (L_12)
		{
			G_B8_0 = L_11;
			goto IL_0073;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)UIGrid_SortHorizontal_m4102275981_MethodInfo_var);
		Comparison_1_t241889613 * L_14 = (Comparison_1_t241889613 *)il2cpp_codegen_object_new(Comparison_1_t241889613_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1954584929(L_14, NULL, L_13, /*hidden argument*/Comparison_1__ctor_m1954584929_MethodInfo_var);
		((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_13(L_14);
		G_B8_0 = G_B7_0;
	}

IL_0073:
	{
		Comparison_1_t241889613 * L_15 = ((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_13();
		NullCheck(G_B8_0);
		List_1_Sort_m2663355476(G_B8_0, L_15, /*hidden argument*/List_1_Sort_m2663355476_MethodInfo_var);
		goto IL_00aa;
	}

IL_0082:
	{
		List_1_t2644239190 * L_16 = __this->get_mChildren_12();
		Comparison_1_t241889613 * L_17 = ((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_14();
		G_B10_0 = L_16;
		if (L_17)
		{
			G_B11_0 = L_16;
			goto IL_00a0;
		}
	}
	{
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)UIGrid_SortVertical_m3913489617_MethodInfo_var);
		Comparison_1_t241889613 * L_19 = (Comparison_1_t241889613 *)il2cpp_codegen_object_new(Comparison_1_t241889613_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1954584929(L_19, NULL, L_18, /*hidden argument*/Comparison_1__ctor_m1954584929_MethodInfo_var);
		((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_14(L_19);
		G_B11_0 = G_B10_0;
	}

IL_00a0:
	{
		Comparison_1_t241889613 * L_20 = ((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_14();
		NullCheck(G_B11_0);
		List_1_Sort_m2663355476(G_B11_0, L_20, /*hidden argument*/List_1_Sort_m2663355476_MethodInfo_var);
	}

IL_00aa:
	{
		UIWrapContent_ResetChildPositions_m2659735431(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::SortAlphabetically()
extern Il2CppClass* UIWrapContent_t1931723019_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t241889613_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2376290435_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3606265932_MethodInfo_var;
extern const MethodInfo* UIGrid_SortByName_m2117232175_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1954584929_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2663355476_MethodInfo_var;
extern const uint32_t UIWrapContent_SortAlphabetically_m747851437_MetadataUsageId;
extern "C"  void UIWrapContent_SortAlphabetically_m747851437 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortAlphabetically_m747851437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t2644239190 * G_B7_0 = NULL;
	List_1_t2644239190 * G_B6_0 = NULL;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m31863134(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t2644239190 * L_1 = __this->get_mChildren_12();
		NullCheck(L_1);
		List_1_Clear_m2376290435(L_1, /*hidden argument*/List_1_Clear_m2376290435_MethodInfo_var);
		V_0 = 0;
		goto IL_0039;
	}

IL_001e:
	{
		List_1_t2644239190 * L_2 = __this->get_mChildren_12();
		Transform_t3275118058 * L_3 = __this->get_mTrans_7();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Add_m3606265932(L_2, L_5, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_7 = V_0;
		Transform_t3275118058 * L_8 = __this->get_mTrans_7();
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m881385315(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t2644239190 * L_10 = __this->get_mChildren_12();
		Comparison_1_t241889613 * L_11 = ((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_15();
		G_B6_0 = L_10;
		if (L_11)
		{
			G_B7_0 = L_10;
			goto IL_0068;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)UIGrid_SortByName_m2117232175_MethodInfo_var);
		Comparison_1_t241889613 * L_13 = (Comparison_1_t241889613 *)il2cpp_codegen_object_new(Comparison_1_t241889613_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1954584929(L_13, NULL, L_12, /*hidden argument*/Comparison_1__ctor_m1954584929_MethodInfo_var);
		((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache2_15(L_13);
		G_B7_0 = G_B6_0;
	}

IL_0068:
	{
		Comparison_1_t241889613 * L_14 = ((UIWrapContent_t1931723019_StaticFields*)UIWrapContent_t1931723019_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_15();
		NullCheck(G_B7_0);
		List_1_Sort_m2663355476(G_B7_0, L_14, /*hidden argument*/List_1_Sort_m2663355476_MethodInfo_var);
		UIWrapContent_ResetChildPositions_m2659735431(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIWrapContent::CacheScrollView()
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* NGUITools_FindInParents_TisUIPanel_t1795085332_m297231755_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIScrollView_t3033954930_m3668191775_MethodInfo_var;
extern const uint32_t UIWrapContent_CacheScrollView_m31863134_MetadataUsageId;
extern "C"  bool UIWrapContent_CacheScrollView_m31863134 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_CacheScrollView_m31863134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_mTrans_7(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		UIPanel_t1795085332 * L_2 = NGUITools_FindInParents_TisUIPanel_t1795085332_m297231755(NULL /*static, unused*/, L_1, /*hidden argument*/NGUITools_FindInParents_TisUIPanel_t1795085332_m297231755_MethodInfo_var);
		__this->set_mPanel_8(L_2);
		UIPanel_t1795085332 * L_3 = __this->get_mPanel_8();
		NullCheck(L_3);
		UIScrollView_t3033954930 * L_4 = Component_GetComponent_TisUIScrollView_t3033954930_m3668191775(L_3, /*hidden argument*/Component_GetComponent_TisUIScrollView_t3033954930_m3668191775_MethodInfo_var);
		__this->set_mScroll_9(L_4);
		UIScrollView_t3033954930 * L_5 = __this->get_mScroll_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		UIScrollView_t3033954930 * L_7 = __this->get_mScroll_9();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_movement_3();
		if (L_8)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_mHorizontal_10((bool)1);
		goto IL_007c;
	}

IL_005d:
	{
		UIScrollView_t3033954930 * L_9 = __this->get_mScroll_9();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_movement_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		__this->set_mHorizontal_10((bool)0);
		goto IL_007c;
	}

IL_007a:
	{
		return (bool)0;
	}

IL_007c:
	{
		return (bool)1;
	}
}
// System.Void UIWrapContent::ResetChildPositions()
extern const MethodInfo* List_1_get_Count_m1995107056_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m563292115_MethodInfo_var;
extern const uint32_t UIWrapContent_ResetChildPositions_m2659735431_MetadataUsageId;
extern "C"  void UIWrapContent_ResetChildPositions_m2659735431 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_ResetChildPositions_m2659735431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Transform_t3275118058 * V_2 = NULL;
	Transform_t3275118058 * G_B3_0 = NULL;
	Transform_t3275118058 * G_B2_0 = NULL;
	Vector3_t2243707580  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Transform_t3275118058 * G_B4_1 = NULL;
	{
		V_0 = 0;
		List_1_t2644239190 * L_0 = __this->get_mChildren_12();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1995107056(L_0, /*hidden argument*/List_1_get_Count_m1995107056_MethodInfo_var);
		V_1 = L_1;
		goto IL_0073;
	}

IL_0013:
	{
		List_1_t2644239190 * L_2 = __this->get_mChildren_12();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t3275118058 * L_4 = List_1_get_Item_m563292115(L_2, L_3, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_2 = L_4;
		Transform_t3275118058 * L_5 = V_2;
		bool L_6 = __this->get_mHorizontal_10();
		G_B2_0 = L_5;
		if (!L_6)
		{
			G_B3_0 = L_5;
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_itemSize_2();
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (((float)((float)((int32_t)((int32_t)L_7*(int32_t)L_8))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_9;
		G_B4_1 = G_B2_0;
		goto IL_0062;
	}

IL_0049:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_itemSize_2();
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2638739322(&L_12, (0.0f), (((float)((float)((int32_t)((int32_t)((-L_10))*(int32_t)L_11))))), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0062:
	{
		NullCheck(G_B4_1);
		Transform_set_localPosition_m1026930133(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = V_2;
		int32_t L_14 = V_0;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_13, L_14);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UIWrapContent::WrapContent()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t1496819779_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t2004302824_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1995107056_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m563292115_MethodInfo_var;
extern const uint32_t UIWrapContent_WrapContent_m3270887951_MetadataUsageId;
extern "C"  void UIWrapContent_WrapContent_m3270887951 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_WrapContent_m3270887951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3U5BU5D_t1172311765* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	bool V_5 = false;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Transform_t3275118058 * V_11 = NULL;
	float V_12 = 0.0f;
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	int32_t V_17 = 0;
	Vector2_t2243707579  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	Transform_t3275118058 * V_24 = NULL;
	float V_25 = 0.0f;
	Vector3_t2243707580  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t2243707580  V_27;
	memset(&V_27, 0, sizeof(V_27));
	int32_t V_28 = 0;
	Vector3_t2243707580  V_29;
	memset(&V_29, 0, sizeof(V_29));
	int32_t V_30 = 0;
	Vector2_t2243707579  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t2243707580  V_32;
	memset(&V_32, 0, sizeof(V_32));
	GameObject_t1756533147 * G_B25_0 = NULL;
	GameObject_t1756533147 * G_B24_0 = NULL;
	int32_t G_B26_0 = 0;
	GameObject_t1756533147 * G_B26_1 = NULL;
	GameObject_t1756533147 * G_B51_0 = NULL;
	GameObject_t1756533147 * G_B50_0 = NULL;
	int32_t G_B52_0 = 0;
	GameObject_t1756533147 * G_B52_1 = NULL;
	{
		int32_t L_0 = __this->get_itemSize_2();
		List_1_t2644239190 * L_1 = __this->get_mChildren_12();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m1995107056(L_1, /*hidden argument*/List_1_get_Count_m1995107056_MethodInfo_var);
		V_0 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_0*(int32_t)L_2)))))*(float)(0.5f)));
		UIPanel_t1795085332 * L_3 = __this->get_mPanel_8();
		NullCheck(L_3);
		Vector3U5BU5D_t1172311765* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t1172311765* >::Invoke(11 /* UnityEngine.Vector3[] UIRect::get_worldCorners() */, L_3);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0058;
	}

IL_002d:
	{
		Vector3U5BU5D_t1172311765* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		V_3 = (*(Vector3_t2243707580 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		Transform_t3275118058 * L_7 = __this->get_mTrans_7();
		Vector3_t2243707580  L_8 = V_3;
		NullCheck(L_7);
		Vector3_t2243707580  L_9 = Transform_InverseTransformPoint_m2648491174(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3U5BU5D_t1172311765* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Vector3_t2243707580  L_12 = V_3;
		(*(Vector3_t2243707580 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))) = L_12;
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) < ((int32_t)4)))
		{
			goto IL_002d;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_15 = V_1;
		NullCheck(L_15);
		Vector3U5BU5D_t1172311765* L_16 = V_1;
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, (*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t2243707580 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		V_4 = L_17;
		V_5 = (bool)1;
		float L_18 = V_0;
		V_6 = ((float)((float)L_18*(float)(2.0f)));
		bool L_19 = __this->get_mHorizontal_10();
		if (!L_19)
		{
			goto IL_02bd;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_20 = V_1;
		NullCheck(L_20);
		float L_21 = ((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		int32_t L_22 = __this->get_itemSize_2();
		V_7 = ((float)((float)L_21-(float)(((float)((float)L_22)))));
		Vector3U5BU5D_t1172311765* L_23 = V_1;
		NullCheck(L_23);
		float L_24 = ((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		int32_t L_25 = __this->get_itemSize_2();
		V_8 = ((float)((float)L_24+(float)(((float)((float)L_25)))));
		V_9 = 0;
		List_1_t2644239190 * L_26 = __this->get_mChildren_12();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m1995107056(L_26, /*hidden argument*/List_1_get_Count_m1995107056_MethodInfo_var);
		V_10 = L_27;
		goto IL_02af;
	}

IL_00db:
	{
		List_1_t2644239190 * L_28 = __this->get_mChildren_12();
		int32_t L_29 = V_9;
		NullCheck(L_28);
		Transform_t3275118058 * L_30 = List_1_get_Item_m563292115(L_28, L_29, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_11 = L_30;
		Transform_t3275118058 * L_31 = V_11;
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_localPosition_m2533925116(L_31, /*hidden argument*/NULL);
		V_13 = L_32;
		float L_33 = (&V_13)->get_x_1();
		float L_34 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_33-(float)L_34));
		float L_35 = V_12;
		float L_36 = V_0;
		if ((!(((float)L_35) < ((float)((-L_36))))))
		{
			goto IL_0198;
		}
	}
	{
		Transform_t3275118058 * L_37 = V_11;
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_localPosition_m2533925116(L_37, /*hidden argument*/NULL);
		V_14 = L_38;
		Vector3_t2243707580 * L_39 = (&V_14);
		float L_40 = L_39->get_x_1();
		float L_41 = V_6;
		L_39->set_x_1(((float)((float)L_40+(float)L_41)));
		float L_42 = (&V_14)->get_x_1();
		float L_43 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_42-(float)L_43));
		float L_44 = (&V_14)->get_x_1();
		int32_t L_45 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_46 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_44/(float)(((float)((float)L_45))))), /*hidden argument*/NULL);
		V_15 = L_46;
		int32_t L_47 = __this->get_minIndex_4();
		int32_t L_48 = __this->get_maxIndex_5();
		if ((((int32_t)L_47) == ((int32_t)L_48)))
		{
			goto IL_0178;
		}
	}
	{
		int32_t L_49 = __this->get_minIndex_4();
		int32_t L_50 = V_15;
		if ((((int32_t)L_49) > ((int32_t)L_50)))
		{
			goto IL_0190;
		}
	}
	{
		int32_t L_51 = V_15;
		int32_t L_52 = __this->get_maxIndex_5();
		if ((((int32_t)L_51) > ((int32_t)L_52)))
		{
			goto IL_0190;
		}
	}

IL_0178:
	{
		Transform_t3275118058 * L_53 = V_11;
		Vector3_t2243707580  L_54 = V_14;
		NullCheck(L_53);
		Transform_set_localPosition_m1026930133(L_53, L_54, /*hidden argument*/NULL);
		Transform_t3275118058 * L_55 = V_11;
		int32_t L_56 = V_9;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_55, L_56);
		goto IL_0193;
	}

IL_0190:
	{
		V_5 = (bool)0;
	}

IL_0193:
	{
		goto IL_0240;
	}

IL_0198:
	{
		float L_57 = V_12;
		float L_58 = V_0;
		if ((!(((float)L_57) > ((float)L_58))))
		{
			goto IL_022b;
		}
	}
	{
		Transform_t3275118058 * L_59 = V_11;
		NullCheck(L_59);
		Vector3_t2243707580  L_60 = Transform_get_localPosition_m2533925116(L_59, /*hidden argument*/NULL);
		V_16 = L_60;
		Vector3_t2243707580 * L_61 = (&V_16);
		float L_62 = L_61->get_x_1();
		float L_63 = V_6;
		L_61->set_x_1(((float)((float)L_62-(float)L_63)));
		float L_64 = (&V_16)->get_x_1();
		float L_65 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_64-(float)L_65));
		float L_66 = (&V_16)->get_x_1();
		int32_t L_67 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_68 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_66/(float)(((float)((float)L_67))))), /*hidden argument*/NULL);
		V_17 = L_68;
		int32_t L_69 = __this->get_minIndex_4();
		int32_t L_70 = __this->get_maxIndex_5();
		if ((((int32_t)L_69) == ((int32_t)L_70)))
		{
			goto IL_020b;
		}
	}
	{
		int32_t L_71 = __this->get_minIndex_4();
		int32_t L_72 = V_17;
		if ((((int32_t)L_71) > ((int32_t)L_72)))
		{
			goto IL_0223;
		}
	}
	{
		int32_t L_73 = V_17;
		int32_t L_74 = __this->get_maxIndex_5();
		if ((((int32_t)L_73) > ((int32_t)L_74)))
		{
			goto IL_0223;
		}
	}

IL_020b:
	{
		Transform_t3275118058 * L_75 = V_11;
		Vector3_t2243707580  L_76 = V_16;
		NullCheck(L_75);
		Transform_set_localPosition_m1026930133(L_75, L_76, /*hidden argument*/NULL);
		Transform_t3275118058 * L_77 = V_11;
		int32_t L_78 = V_9;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_77, L_78);
		goto IL_0226;
	}

IL_0223:
	{
		V_5 = (bool)0;
	}

IL_0226:
	{
		goto IL_0240;
	}

IL_022b:
	{
		bool L_79 = __this->get_mFirstTime_11();
		if (!L_79)
		{
			goto IL_0240;
		}
	}
	{
		Transform_t3275118058 * L_80 = V_11;
		int32_t L_81 = V_9;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_80, L_81);
	}

IL_0240:
	{
		bool L_82 = __this->get_cullContent_3();
		if (!L_82)
		{
			goto IL_02a9;
		}
	}
	{
		float L_83 = V_12;
		UIPanel_t1795085332 * L_84 = __this->get_mPanel_8();
		NullCheck(L_84);
		Vector2_t2243707579  L_85 = UIPanel_get_clipOffset_m1165225790(L_84, /*hidden argument*/NULL);
		V_18 = L_85;
		float L_86 = (&V_18)->get_x_0();
		Transform_t3275118058 * L_87 = __this->get_mTrans_7();
		NullCheck(L_87);
		Vector3_t2243707580  L_88 = Transform_get_localPosition_m2533925116(L_87, /*hidden argument*/NULL);
		V_19 = L_88;
		float L_89 = (&V_19)->get_x_1();
		V_12 = ((float)((float)L_83+(float)((float)((float)L_86-(float)L_89))));
		Transform_t3275118058 * L_90 = V_11;
		NullCheck(L_90);
		GameObject_t1756533147 * L_91 = Component_get_gameObject_m3105766835(L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1496819779_il2cpp_TypeInfo_var);
		bool L_92 = UICamera_IsPressed_m545745070(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (L_92)
		{
			goto IL_02a9;
		}
	}
	{
		Transform_t3275118058 * L_93 = V_11;
		NullCheck(L_93);
		GameObject_t1756533147 * L_94 = Component_get_gameObject_m3105766835(L_93, /*hidden argument*/NULL);
		float L_95 = V_12;
		float L_96 = V_7;
		G_B24_0 = L_94;
		if ((!(((float)L_95) > ((float)L_96))))
		{
			G_B25_0 = L_94;
			goto IL_02a2;
		}
	}
	{
		float L_97 = V_12;
		float L_98 = V_8;
		G_B26_0 = ((((float)L_97) < ((float)L_98))? 1 : 0);
		G_B26_1 = G_B24_0;
		goto IL_02a3;
	}

IL_02a2:
	{
		G_B26_0 = 0;
		G_B26_1 = G_B25_0;
	}

IL_02a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3545134331(NULL /*static, unused*/, G_B26_1, (bool)G_B26_0, (bool)0, /*hidden argument*/NULL);
	}

IL_02a9:
	{
		int32_t L_99 = V_9;
		V_9 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_02af:
	{
		int32_t L_100 = V_9;
		int32_t L_101 = V_10;
		if ((((int32_t)L_100) < ((int32_t)L_101)))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_04db;
	}

IL_02bd:
	{
		Vector3U5BU5D_t1172311765* L_102 = V_1;
		NullCheck(L_102);
		float L_103 = ((L_102)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_y_2();
		int32_t L_104 = __this->get_itemSize_2();
		V_20 = ((float)((float)L_103-(float)(((float)((float)L_104)))));
		Vector3U5BU5D_t1172311765* L_105 = V_1;
		NullCheck(L_105);
		float L_106 = ((L_105)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_y_2();
		int32_t L_107 = __this->get_itemSize_2();
		V_21 = ((float)((float)L_106+(float)(((float)((float)L_107)))));
		V_22 = 0;
		List_1_t2644239190 * L_108 = __this->get_mChildren_12();
		NullCheck(L_108);
		int32_t L_109 = List_1_get_Count_m1995107056(L_108, /*hidden argument*/List_1_get_Count_m1995107056_MethodInfo_var);
		V_23 = L_109;
		goto IL_04d2;
	}

IL_02fe:
	{
		List_1_t2644239190 * L_110 = __this->get_mChildren_12();
		int32_t L_111 = V_22;
		NullCheck(L_110);
		Transform_t3275118058 * L_112 = List_1_get_Item_m563292115(L_110, L_111, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_24 = L_112;
		Transform_t3275118058 * L_113 = V_24;
		NullCheck(L_113);
		Vector3_t2243707580  L_114 = Transform_get_localPosition_m2533925116(L_113, /*hidden argument*/NULL);
		V_26 = L_114;
		float L_115 = (&V_26)->get_y_2();
		float L_116 = (&V_4)->get_y_2();
		V_25 = ((float)((float)L_115-(float)L_116));
		float L_117 = V_25;
		float L_118 = V_0;
		if ((!(((float)L_117) < ((float)((-L_118))))))
		{
			goto IL_03bb;
		}
	}
	{
		Transform_t3275118058 * L_119 = V_24;
		NullCheck(L_119);
		Vector3_t2243707580  L_120 = Transform_get_localPosition_m2533925116(L_119, /*hidden argument*/NULL);
		V_27 = L_120;
		Vector3_t2243707580 * L_121 = (&V_27);
		float L_122 = L_121->get_y_2();
		float L_123 = V_6;
		L_121->set_y_2(((float)((float)L_122+(float)L_123)));
		float L_124 = (&V_27)->get_y_2();
		float L_125 = (&V_4)->get_y_2();
		V_25 = ((float)((float)L_124-(float)L_125));
		float L_126 = (&V_27)->get_y_2();
		int32_t L_127 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_128 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_126/(float)(((float)((float)L_127))))), /*hidden argument*/NULL);
		V_28 = L_128;
		int32_t L_129 = __this->get_minIndex_4();
		int32_t L_130 = __this->get_maxIndex_5();
		if ((((int32_t)L_129) == ((int32_t)L_130)))
		{
			goto IL_039b;
		}
	}
	{
		int32_t L_131 = __this->get_minIndex_4();
		int32_t L_132 = V_28;
		if ((((int32_t)L_131) > ((int32_t)L_132)))
		{
			goto IL_03b3;
		}
	}
	{
		int32_t L_133 = V_28;
		int32_t L_134 = __this->get_maxIndex_5();
		if ((((int32_t)L_133) > ((int32_t)L_134)))
		{
			goto IL_03b3;
		}
	}

IL_039b:
	{
		Transform_t3275118058 * L_135 = V_24;
		Vector3_t2243707580  L_136 = V_27;
		NullCheck(L_135);
		Transform_set_localPosition_m1026930133(L_135, L_136, /*hidden argument*/NULL);
		Transform_t3275118058 * L_137 = V_24;
		int32_t L_138 = V_22;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_137, L_138);
		goto IL_03b6;
	}

IL_03b3:
	{
		V_5 = (bool)0;
	}

IL_03b6:
	{
		goto IL_0463;
	}

IL_03bb:
	{
		float L_139 = V_25;
		float L_140 = V_0;
		if ((!(((float)L_139) > ((float)L_140))))
		{
			goto IL_044e;
		}
	}
	{
		Transform_t3275118058 * L_141 = V_24;
		NullCheck(L_141);
		Vector3_t2243707580  L_142 = Transform_get_localPosition_m2533925116(L_141, /*hidden argument*/NULL);
		V_29 = L_142;
		Vector3_t2243707580 * L_143 = (&V_29);
		float L_144 = L_143->get_y_2();
		float L_145 = V_6;
		L_143->set_y_2(((float)((float)L_144-(float)L_145)));
		float L_146 = (&V_29)->get_y_2();
		float L_147 = (&V_4)->get_y_2();
		V_25 = ((float)((float)L_146-(float)L_147));
		float L_148 = (&V_29)->get_y_2();
		int32_t L_149 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_150 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_148/(float)(((float)((float)L_149))))), /*hidden argument*/NULL);
		V_30 = L_150;
		int32_t L_151 = __this->get_minIndex_4();
		int32_t L_152 = __this->get_maxIndex_5();
		if ((((int32_t)L_151) == ((int32_t)L_152)))
		{
			goto IL_042e;
		}
	}
	{
		int32_t L_153 = __this->get_minIndex_4();
		int32_t L_154 = V_30;
		if ((((int32_t)L_153) > ((int32_t)L_154)))
		{
			goto IL_0446;
		}
	}
	{
		int32_t L_155 = V_30;
		int32_t L_156 = __this->get_maxIndex_5();
		if ((((int32_t)L_155) > ((int32_t)L_156)))
		{
			goto IL_0446;
		}
	}

IL_042e:
	{
		Transform_t3275118058 * L_157 = V_24;
		Vector3_t2243707580  L_158 = V_29;
		NullCheck(L_157);
		Transform_set_localPosition_m1026930133(L_157, L_158, /*hidden argument*/NULL);
		Transform_t3275118058 * L_159 = V_24;
		int32_t L_160 = V_22;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_159, L_160);
		goto IL_0449;
	}

IL_0446:
	{
		V_5 = (bool)0;
	}

IL_0449:
	{
		goto IL_0463;
	}

IL_044e:
	{
		bool L_161 = __this->get_mFirstTime_11();
		if (!L_161)
		{
			goto IL_0463;
		}
	}
	{
		Transform_t3275118058 * L_162 = V_24;
		int32_t L_163 = V_22;
		VirtActionInvoker2< Transform_t3275118058 *, int32_t >::Invoke(6 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_162, L_163);
	}

IL_0463:
	{
		bool L_164 = __this->get_cullContent_3();
		if (!L_164)
		{
			goto IL_04cc;
		}
	}
	{
		float L_165 = V_25;
		UIPanel_t1795085332 * L_166 = __this->get_mPanel_8();
		NullCheck(L_166);
		Vector2_t2243707579  L_167 = UIPanel_get_clipOffset_m1165225790(L_166, /*hidden argument*/NULL);
		V_31 = L_167;
		float L_168 = (&V_31)->get_y_1();
		Transform_t3275118058 * L_169 = __this->get_mTrans_7();
		NullCheck(L_169);
		Vector3_t2243707580  L_170 = Transform_get_localPosition_m2533925116(L_169, /*hidden argument*/NULL);
		V_32 = L_170;
		float L_171 = (&V_32)->get_y_2();
		V_25 = ((float)((float)L_165+(float)((float)((float)L_168-(float)L_171))));
		Transform_t3275118058 * L_172 = V_24;
		NullCheck(L_172);
		GameObject_t1756533147 * L_173 = Component_get_gameObject_m3105766835(L_172, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t1496819779_il2cpp_TypeInfo_var);
		bool L_174 = UICamera_IsPressed_m545745070(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		if (L_174)
		{
			goto IL_04cc;
		}
	}
	{
		Transform_t3275118058 * L_175 = V_24;
		NullCheck(L_175);
		GameObject_t1756533147 * L_176 = Component_get_gameObject_m3105766835(L_175, /*hidden argument*/NULL);
		float L_177 = V_25;
		float L_178 = V_20;
		G_B50_0 = L_176;
		if ((!(((float)L_177) > ((float)L_178))))
		{
			G_B51_0 = L_176;
			goto IL_04c5;
		}
	}
	{
		float L_179 = V_25;
		float L_180 = V_21;
		G_B52_0 = ((((float)L_179) < ((float)L_180))? 1 : 0);
		G_B52_1 = G_B50_0;
		goto IL_04c6;
	}

IL_04c5:
	{
		G_B52_0 = 0;
		G_B52_1 = G_B51_0;
	}

IL_04c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t2004302824_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3545134331(NULL /*static, unused*/, G_B52_1, (bool)G_B52_0, (bool)0, /*hidden argument*/NULL);
	}

IL_04cc:
	{
		int32_t L_181 = V_22;
		V_22 = ((int32_t)((int32_t)L_181+(int32_t)1));
	}

IL_04d2:
	{
		int32_t L_182 = V_22;
		int32_t L_183 = V_23;
		if ((((int32_t)L_182) < ((int32_t)L_183)))
		{
			goto IL_02fe;
		}
	}

IL_04db:
	{
		UIScrollView_t3033954930 * L_184 = __this->get_mScroll_9();
		bool L_185 = V_5;
		NullCheck(L_184);
		L_184->set_restrictWithinPanel_5((bool)((((int32_t)L_185) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void UIWrapContent::OnValidate()
extern "C"  void UIWrapContent_OnValidate_m2065446881 (UIWrapContent_t1931723019 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_maxIndex_5();
		int32_t L_1 = __this->get_minIndex_4();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_2);
	}

IL_001d:
	{
		int32_t L_3 = __this->get_minIndex_4();
		int32_t L_4 = __this->get_maxIndex_5();
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_5);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UIWrapContent_UpdateItem_m2112905438_MetadataUsageId;
extern "C"  void UIWrapContent_UpdateItem_m2112905438 (UIWrapContent_t1931723019 * __this, Transform_t3275118058 * ___item0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_UpdateItem_m2112905438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B4_0 = 0;
	{
		OnInitializeItem_t2948699028 * L_0 = __this->get_onInitializeItem_6();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		UIScrollView_t3033954930 * L_1 = __this->get_mScroll_9();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_movement_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_003c;
		}
	}
	{
		Transform_t3275118058 * L_3 = ___item0;
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_localPosition_m2533925116(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		int32_t L_6 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_5/(float)(((float)((float)L_6))))), /*hidden argument*/NULL);
		G_B4_0 = L_7;
		goto IL_0057;
	}

IL_003c:
	{
		Transform_t3275118058 * L_8 = ___item0;
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_localPosition_m2533925116(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		int32_t L_11 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_10/(float)(((float)((float)L_11))))), /*hidden argument*/NULL);
		G_B4_0 = L_12;
	}

IL_0057:
	{
		V_0 = G_B4_0;
		OnInitializeItem_t2948699028 * L_13 = __this->get_onInitializeItem_6();
		Transform_t3275118058 * L_14 = ___item0;
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(L_14, /*hidden argument*/NULL);
		int32_t L_16 = ___index1;
		int32_t L_17 = V_0;
		NullCheck(L_13);
		OnInitializeItem_Invoke_m286694789(L_13, L_15, L_16, L_17, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void UIWrapContent/OnInitializeItem::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitializeItem__ctor_m2088558765 (OnInitializeItem_t2948699028 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWrapContent/OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void OnInitializeItem_Invoke_m286694789 (OnInitializeItem_t2948699028 * __this, GameObject_t1756533147 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnInitializeItem_Invoke_m286694789((OnInitializeItem_t2948699028 *)__this->get_prev_9(),___go0, ___wrapIndex1, ___realIndex2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t1756533147 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, GameObject_t1756533147 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UIWrapContent/OnInitializeItem::BeginInvoke(UnityEngine.GameObject,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t OnInitializeItem_BeginInvoke_m3324777424_MetadataUsageId;
extern "C"  Il2CppObject * OnInitializeItem_BeginInvoke_m3324777424 (OnInitializeItem_t2948699028 * __this, GameObject_t1756533147 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnInitializeItem_BeginInvoke_m3324777424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___go0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___wrapIndex1);
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___realIndex2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UIWrapContent/OnInitializeItem::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitializeItem_EndInvoke_m1518335515 (OnInitializeItem_t2948699028 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnibillManager::.ctor()
extern Il2CppClass* MonoSingleton_1_t3476048858_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1__ctor_m3795133234_MethodInfo_var;
extern const uint32_t UnibillManager__ctor_m2544794205_MetadataUsageId;
extern "C"  void UnibillManager__ctor_m2544794205 (UnibillManager_t3725383138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnibillManager__ctor_m2544794205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3476048858_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m3795133234(__this, /*hidden argument*/MonoSingleton_1__ctor_m3795133234_MethodInfo_var);
		return;
	}
}
// System.Void UnibillManager::InitUniBill()
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1865222972_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UnibillManager_U3CInitUniBillU3Em__0_m1754596986_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m759102168_MethodInfo_var;
extern const uint32_t UnibillManager_InitUniBill_m3090035400_MetadataUsageId;
extern "C"  void UnibillManager_InitUniBill_m3090035400 (UnibillManager_t3725383138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnibillManager_InitUniBill_m3090035400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_unibillInitialised_10();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_1 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UnibillManager_U3CInitUniBillU3Em__0_m1754596986_MethodInfo_var);
		Action_2_t1865222972 * L_3 = (Action_2_t1865222972 *)il2cpp_codegen_object_new(Action_2_t1865222972_il2cpp_TypeInfo_var);
		Action_2__ctor_m759102168(L_3, __this, L_2, /*hidden argument*/Action_2__ctor_m759102168_MethodInfo_var);
		NullCheck(L_1);
		ChatmageddonServer_PingServer_m1337531634(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnibillManager::onBillerReady(UnibillState)
extern "C"  void UnibillManager_onBillerReady_m4156283233 (UnibillManager_t3725383138 * __this, int32_t ___state0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnibillManager::onTransactionsRestored(System.Boolean)
extern "C"  void UnibillManager_onTransactionsRestored_m143372654 (UnibillManager_t3725383138 * __this, bool ___success0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnibillManager::onPurchased(PurchaseEvent)
extern Il2CppClass* MonoSingleton_1_t971871211_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4047537980_MethodInfo_var;
extern const MethodInfo* BasePurchaseManager_2_UnibillPurchaseSuccess_m327807695_MethodInfo_var;
extern const uint32_t UnibillManager_onPurchased_m3583371024_MetadataUsageId;
extern "C"  void UnibillManager_onPurchased_m3583371024 (UnibillManager_t3725383138 * __this, PurchaseEvent_t743554429 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnibillManager_onPurchased_m3583371024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t971871211_il2cpp_TypeInfo_var);
		ChatmageddonPurchaseManager_t1221205491 * L_0 = MonoSingleton_1_get_Instance_m4047537980(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4047537980_MethodInfo_var);
		PurchaseEvent_t743554429 * L_1 = ___e0;
		NullCheck(L_1);
		PurchasableItem_t3963353899 * L_2 = PurchaseEvent_get_PurchasedItem_m2741253279(L_1, /*hidden argument*/NULL);
		PurchaseEvent_t743554429 * L_3 = ___e0;
		NullCheck(L_3);
		String_t* L_4 = PurchaseEvent_get_Receipt_m2167638012(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		BasePurchaseManager_2_UnibillPurchaseSuccess_m327807695(L_0, L_2, L_4, /*hidden argument*/BasePurchaseManager_2_UnibillPurchaseSuccess_m327807695_MethodInfo_var);
		return;
	}
}
// System.Void UnibillManager::onDeferred(PurchasableItem)
extern "C"  void UnibillManager_onDeferred_m2871327162 (UnibillManager_t3725383138 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnibillManager::MakePurchase(PurchasableItem)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1925078513;
extern const uint32_t UnibillManager_MakePurchase_m1304399525_MetadataUsageId;
extern "C"  void UnibillManager_MakePurchase_m1304399525 (UnibillManager_t3725383138 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnibillManager_MakePurchase_m1304399525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1925078513, /*hidden argument*/NULL);
	}

IL_0014:
	{
		PurchasableItem_t3963353899 * L_1 = ___item0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Unibiller_initiatePurchase_m216438704(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnibillManager::RestorePurchases()
extern "C"  void UnibillManager_RestorePurchases_m2050649057 (UnibillManager_t3725383138 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0007:
	{
		PurchasableItemU5BU5D_t138351562* L_0 = Unibiller_get_AllNonConsumablePurchasableItems_m1390806025(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PurchasableItem_t3963353899 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		int32_t L_4 = Unibiller_GetPurchaseCount_m3738977176(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if ((((int32_t)0) >= ((int32_t)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		PurchasableItemU5BU5D_t138351562* L_5 = Unibiller_get_AllNonConsumablePurchasableItems_m1390806025(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		PurchasableItem_t3963353899 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		UnibillManager_RestorePurchaseItem_m1695208614(__this, L_8, /*hidden argument*/NULL);
	}

IL_0026:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		PurchasableItemU5BU5D_t138351562* L_11 = Unibiller_get_AllNonConsumablePurchasableItems_m1390806025(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnibillManager::RestorePurchaseItem(PurchasableItem)
extern "C"  void UnibillManager_RestorePurchaseItem_m1695208614 (UnibillManager_t3725383138 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String UnibillManager::GetPurchaseLocalizedString(System.String)
extern "C"  String_t* UnibillManager_GetPurchaseLocalizedString_m2890061501 (UnibillManager_t3725383138 * __this, String_t* ___purchaseItemID0, const MethodInfo* method)
{
	PurchasableItem_t3963353899 * V_0 = NULL;
	{
		String_t* L_0 = ___purchaseItemID0;
		PurchasableItem_t3963353899 * L_1 = Unibiller_GetPurchasableItemById_m1089428912(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PurchasableItem_t3963353899 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		PurchasableItem_t3963353899 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = PurchasableItem_get_localizedPriceString_m807780071(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0014:
	{
		return (String_t*)NULL;
	}
}
// System.Void UnibillManager::<InitUniBill>m__0(System.Boolean,System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t4073934390_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t545353811_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3765153281_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* UnibillError_t1753859787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* UnibillManager_onBillerReady_m4156283233_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4291250407_MethodInfo_var;
extern const MethodInfo* UnibillManager_onTransactionsRestored_m143372654_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3210289808_MethodInfo_var;
extern const MethodInfo* UnibillManager_onPurchased_m3583371024_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1074119010_MethodInfo_var;
extern const MethodInfo* UnibillManager_onDeferred_m2871327162_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1668058440_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2006030885;
extern Il2CppCodeGenString* _stringLiteral2871013507;
extern const uint32_t UnibillManager_U3CInitUniBillU3Em__0_m1754596986_MetadataUsageId;
extern "C"  void UnibillManager_U3CInitUniBillU3Em__0_m1754596986 (UnibillManager_t3725383138 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnibillManager_U3CInitUniBillU3Em__0_m1754596986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = ___isConnected0;
		if (!L_0)
		{
			goto IL_00d7;
		}
	}
	{
		Object_t1021602117 * L_1 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral2006030885, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)UnibillManager_onBillerReady_m4156283233_MethodInfo_var);
		Action_1_t4073934390 * L_5 = (Action_1_t4073934390 *)il2cpp_codegen_object_new(Action_1_t4073934390_il2cpp_TypeInfo_var);
		Action_1__ctor_m4291250407(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m4291250407_MethodInfo_var);
		Unibiller_add_onBillerReady_m2427151891(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UnibillManager_onTransactionsRestored_m143372654_MethodInfo_var);
		Action_1_t3627374100 * L_7 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m3210289808(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m3210289808_MethodInfo_var);
		Unibiller_add_onTransactionsRestored_m2411787806(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)UnibillManager_onPurchased_m3583371024_MethodInfo_var);
		Action_1_t545353811 * L_9 = (Action_1_t545353811 *)il2cpp_codegen_object_new(Action_1_t545353811_il2cpp_TypeInfo_var);
		Action_1__ctor_m1074119010(L_9, __this, L_8, /*hidden argument*/Action_1__ctor_m1074119010_MethodInfo_var);
		Unibiller_add_onPurchaseCompleteEvent_m2452521383(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)UnibillManager_onDeferred_m2871327162_MethodInfo_var);
		Action_1_t3765153281 * L_11 = (Action_1_t3765153281 *)il2cpp_codegen_object_new(Action_1_t3765153281_il2cpp_TypeInfo_var);
		Action_1__ctor_m1668058440(L_11, __this, L_10, /*hidden argument*/Action_1__ctor_m1668058440_MethodInfo_var);
		Unibiller_add_onPurchaseDeferred_m1938384491(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Unibiller_Initialise_m1050417409(NULL /*static, unused*/, (List_1_t888775120 *)NULL, /*hidden argument*/NULL);
		UnibillErrorU5BU5D_t4092036522* L_12 = Unibiller_get_Errors_m3622792281(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_13 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c6;
		}
	}
	{
		V_0 = 0;
		goto IL_00b9;
	}

IL_0090:
	{
		UnibillErrorU5BU5D_t4092036522* L_14 = Unibiller_get_Errors_m3622792281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Il2CppObject * L_16 = Box(UnibillError_t1753859787_il2cpp_TypeInfo_var, ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15))));
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2871013507, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_20 = V_0;
		UnibillErrorU5BU5D_t4092036522* L_21 = Unibiller_get_Errors_m3622792281(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0090;
		}
	}

IL_00c6:
	{
		goto IL_00d2;
	}

IL_00cb:
	{
		__this->set_unibillInitialised_10((bool)1);
	}

IL_00d2:
	{
		goto IL_00d7;
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::.ctor()
extern "C"  void UniversalFeed__ctor_m739608520 (UniversalFeed_t2144776909 * __this, const MethodInfo* method)
{
	{
		Feed__ctor_m3203805317(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::InitFeed()
extern Il2CppClass* MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var;
extern const uint32_t UniversalFeed_InitFeed_m2219532144_MetadataUsageId;
extern "C"  void UniversalFeed_InitFeed_m2219532144 (UniversalFeed_t2144776909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_InitFeed_m2219532144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_0 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_0);
		float L_1 = L_0->get_universalFeedActivePoll_7();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_2 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_2);
		float L_3 = L_2->get_universalFeedActivePoll_7();
		Feed_SetFeedPollTime_m3938617064(__this, L_3, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_4 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_4);
		float L_5 = L_4->get_universalFeedDefaultPoll_6();
		Feed_SetFeedPollTime_m3938617064(__this, L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		Feed_InitFeed_m3446165509(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::GetFeedInfo()
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CGetFeedInfoU3Em__0_m1170692438_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern const uint32_t UniversalFeed_GetFeedInfo_m3167845748_MetadataUsageId;
extern "C"  void UniversalFeed_GetFeedInfo_m3167845748 (UniversalFeed_t2144776909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_GetFeedInfo_m3167845748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_0 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_1 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_1);
		String_t* L_2 = L_1->get_universalFeedURL_5();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)UniversalFeed_U3CGetFeedInfoU3Em__0_m1170692438_MethodInfo_var);
		Action_3_t3681841185 * L_4 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_4, __this, L_3, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		NullCheck(L_0);
		ChatmageddonServer_GetUniversalFeed_m1393471569(L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::ProcessFeedInfo(System.Collections.Hashtable)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2753913416;
extern Il2CppCodeGenString* _stringLiteral2754995797;
extern Il2CppCodeGenString* _stringLiteral811305496;
extern Il2CppCodeGenString* _stringLiteral1421151742;
extern Il2CppCodeGenString* _stringLiteral2619694;
extern const uint32_t UniversalFeed_ProcessFeedInfo_m3533661859_MetadataUsageId;
extern "C"  void UniversalFeed_ProcessFeedInfo_m3533661859 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___infoTable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_ProcessFeedInfo_m3533661859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	int32_t V_1 = 0;
	Hashtable_t909839986 * V_2 = NULL;
	String_t* V_3 = NULL;
	Hashtable_t909839986 * V_4 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___infoTable0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_0, _stringLiteral2753913416);
		if (!L_1)
		{
			goto IL_00cf;
		}
	}
	{
		Hashtable_t909839986 * L_2 = ___infoTable0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral2753913416);
		V_0 = ((ArrayList_t4252133567 *)CastclassClass(L_3, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		ArrayList_t4252133567 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_4);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_00cf;
		}
	}
	{
		V_1 = 0;
		goto IL_00c3;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_6 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0076;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2754995797);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2754995797);
		ObjectU5BU5D_t3614634134* L_8 = L_7;
		int32_t L_9 = V_1;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral811305496);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral811305496);
		ObjectU5BU5D_t3614634134* L_13 = L_12;
		ArrayList_t4252133567 * L_14 = V_0;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		Il2CppObject * L_16 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_14, L_15);
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3881798623(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0076:
	{
		ArrayList_t4252133567 * L_19 = V_0;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		Il2CppObject * L_21 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_19, L_20);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		Il2CppObject * L_23 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		V_2 = ((Hashtable_t909839986 *)CastclassClass(L_23, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_24 = V_2;
		NullCheck(L_24);
		Il2CppObject * L_25 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_24, _stringLiteral1421151742);
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_3 = L_26;
		Hashtable_t909839986 * L_27 = V_2;
		NullCheck(L_27);
		Il2CppObject * L_28 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_27, _stringLiteral2619694);
		V_4 = ((Hashtable_t909839986 *)CastclassClass(L_28, Hashtable_t909839986_il2cpp_TypeInfo_var));
		String_t* L_29 = V_3;
		int32_t L_30 = UniversalFeed_GetTypeFromString_m4051903190(__this, L_29, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_31 = V_4;
		UniversalFeed_ProcessUpdate_m1841288512(__this, L_30, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_1;
		V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00c3:
	{
		int32_t L_33 = V_1;
		ArrayList_t4252133567 * L_34 = V_0;
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_34);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_0034;
		}
	}

IL_00cf:
	{
		Hashtable_t909839986 * L_36 = ___infoTable0;
		Feed_ProcessFeedInfo_m1800979426(__this, L_36, /*hidden argument*/NULL);
		return;
	}
}
// UniversalUpdateType UniversalFeed::GetTypeFromString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral500164998;
extern Il2CppCodeGenString* _stringLiteral582042821;
extern Il2CppCodeGenString* _stringLiteral2852677269;
extern Il2CppCodeGenString* _stringLiteral2361546749;
extern Il2CppCodeGenString* _stringLiteral3837533918;
extern Il2CppCodeGenString* _stringLiteral2788335717;
extern Il2CppCodeGenString* _stringLiteral2518743725;
extern Il2CppCodeGenString* _stringLiteral2970143030;
extern Il2CppCodeGenString* _stringLiteral765449538;
extern Il2CppCodeGenString* _stringLiteral1788176356;
extern Il2CppCodeGenString* _stringLiteral2380946739;
extern Il2CppCodeGenString* _stringLiteral106443081;
extern Il2CppCodeGenString* _stringLiteral2176705310;
extern Il2CppCodeGenString* _stringLiteral1716804601;
extern Il2CppCodeGenString* _stringLiteral383841057;
extern Il2CppCodeGenString* _stringLiteral668489572;
extern Il2CppCodeGenString* _stringLiteral3648356432;
extern Il2CppCodeGenString* _stringLiteral1050072652;
extern Il2CppCodeGenString* _stringLiteral4256843322;
extern Il2CppCodeGenString* _stringLiteral3480539265;
extern Il2CppCodeGenString* _stringLiteral3259010553;
extern Il2CppCodeGenString* _stringLiteral775373522;
extern Il2CppCodeGenString* _stringLiteral930243958;
extern Il2CppCodeGenString* _stringLiteral751748003;
extern Il2CppCodeGenString* _stringLiteral1069000360;
extern Il2CppCodeGenString* _stringLiteral2560815127;
extern Il2CppCodeGenString* _stringLiteral2876078369;
extern Il2CppCodeGenString* _stringLiteral1082572698;
extern Il2CppCodeGenString* _stringLiteral1032881247;
extern Il2CppCodeGenString* _stringLiteral3736552370;
extern Il2CppCodeGenString* _stringLiteral1292866206;
extern Il2CppCodeGenString* _stringLiteral837657026;
extern Il2CppCodeGenString* _stringLiteral3765283968;
extern Il2CppCodeGenString* _stringLiteral618630782;
extern const uint32_t UniversalFeed_GetTypeFromString_m4051903190_MetadataUsageId;
extern "C"  int32_t UniversalFeed_GetTypeFromString_m4051903190 (UniversalFeed_t2144776909 * __this, String_t* ___typeString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_GetTypeFromString_m4051903190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral500164998, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0012:
	{
		String_t* L_2 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral582042821, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0024:
	{
		String_t* L_4 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral2852677269, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0036:
	{
		String_t* L_6 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral2361546749, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		return (int32_t)(((int32_t)31));
	}

IL_0049:
	{
		String_t* L_8 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3837533918, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_10 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral2788335717, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006b;
		}
	}

IL_0069:
	{
		return (int32_t)(5);
	}

IL_006b:
	{
		String_t* L_12 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral2518743725, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_14 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral2970143030, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008d;
		}
	}

IL_008b:
	{
		return (int32_t)(6);
	}

IL_008d:
	{
		String_t* L_16 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral765449538, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009f;
		}
	}
	{
		return (int32_t)(7);
	}

IL_009f:
	{
		String_t* L_18 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral1788176356, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00b2;
		}
	}
	{
		return (int32_t)(((int32_t)9));
	}

IL_00b2:
	{
		String_t* L_20 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, _stringLiteral2380946739, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c5;
		}
	}
	{
		return (int32_t)(((int32_t)10));
	}

IL_00c5:
	{
		String_t* L_22 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_22, _stringLiteral106443081, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00d8;
		}
	}
	{
		return (int32_t)(((int32_t)11));
	}

IL_00d8:
	{
		String_t* L_24 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral2176705310, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00eb;
		}
	}
	{
		return (int32_t)(((int32_t)12));
	}

IL_00eb:
	{
		String_t* L_26 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_26, _stringLiteral1716804601, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fe;
		}
	}
	{
		return (int32_t)(((int32_t)14));
	}

IL_00fe:
	{
		String_t* L_28 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_28, _stringLiteral383841057, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0111;
		}
	}
	{
		return (int32_t)(((int32_t)13));
	}

IL_0111:
	{
		String_t* L_30 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_30, _stringLiteral668489572, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0124;
		}
	}
	{
		return (int32_t)(((int32_t)15));
	}

IL_0124:
	{
		String_t* L_32 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_32, _stringLiteral3648356432, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0137;
		}
	}
	{
		return (int32_t)(((int32_t)16));
	}

IL_0137:
	{
		String_t* L_34 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_34, _stringLiteral1050072652, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_014a;
		}
	}
	{
		return (int32_t)(((int32_t)17));
	}

IL_014a:
	{
		String_t* L_36 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_36, _stringLiteral4256843322, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_015d;
		}
	}
	{
		return (int32_t)(((int32_t)18));
	}

IL_015d:
	{
		String_t* L_38 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_38, _stringLiteral3480539265, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0170;
		}
	}
	{
		return (int32_t)(((int32_t)19));
	}

IL_0170:
	{
		String_t* L_40 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_40, _stringLiteral3259010553, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0183;
		}
	}
	{
		return (int32_t)(((int32_t)20));
	}

IL_0183:
	{
		String_t* L_42 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_43 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_42, _stringLiteral775373522, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0196;
		}
	}
	{
		return (int32_t)(((int32_t)21));
	}

IL_0196:
	{
		String_t* L_44 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_44, _stringLiteral930243958, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_01a8;
		}
	}
	{
		return (int32_t)(8);
	}

IL_01a8:
	{
		String_t* L_46 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_47 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_46, _stringLiteral751748003, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01ba;
		}
	}
	{
		return (int32_t)(4);
	}

IL_01ba:
	{
		String_t* L_48 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_48, _stringLiteral1069000360, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_01cd;
		}
	}
	{
		return (int32_t)(((int32_t)22));
	}

IL_01cd:
	{
		String_t* L_50 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_51 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_50, _stringLiteral2560815127, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01e0;
		}
	}
	{
		return (int32_t)(((int32_t)23));
	}

IL_01e0:
	{
		String_t* L_52 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_53 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_52, _stringLiteral2876078369, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01f3;
		}
	}
	{
		return (int32_t)(((int32_t)24));
	}

IL_01f3:
	{
		String_t* L_54 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_54, _stringLiteral1082572698, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0206;
		}
	}
	{
		return (int32_t)(((int32_t)25));
	}

IL_0206:
	{
		String_t* L_56 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_57 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_56, _stringLiteral1032881247, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0219;
		}
	}
	{
		return (int32_t)(((int32_t)26));
	}

IL_0219:
	{
		String_t* L_58 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_59 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_58, _stringLiteral3736552370, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_022c;
		}
	}
	{
		return (int32_t)(((int32_t)27));
	}

IL_022c:
	{
		String_t* L_60 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_60, _stringLiteral1292866206, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_024c;
		}
	}
	{
		String_t* L_62 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_63 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_62, _stringLiteral837657026, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_024f;
		}
	}

IL_024c:
	{
		return (int32_t)(((int32_t)28));
	}

IL_024f:
	{
		String_t* L_64 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_65 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_64, _stringLiteral3765283968, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_0262;
		}
	}
	{
		return (int32_t)(((int32_t)29));
	}

IL_0262:
	{
		String_t* L_66 = ___typeString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_66, _stringLiteral618630782, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0275;
		}
	}
	{
		return (int32_t)(((int32_t)30));
	}

IL_0275:
	{
		return (int32_t)(0);
	}
}
// System.Void UniversalFeed::ProcessUpdate(UniversalUpdateType,System.Collections.Hashtable)
extern "C"  void UniversalFeed_ProcessUpdate_m1841288512 (UniversalFeed_t2144776909 * __this, int32_t ___type0, Hashtable_t909839986 * ___data1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___data1;
		UniversalFeed_IncomingFriendRequest_m3177195349(__this, L_1, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0013:
	{
		int32_t L_2 = ___type0;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0026;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___data1;
		UniversalFeed_FriendRequestReplied_m143828200(__this, L_3, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0026:
	{
		int32_t L_4 = ___type0;
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_003a;
		}
	}
	{
		Hashtable_t909839986 * L_5 = ___data1;
		UniversalFeed_AutoFriendAdded_m1171627830(__this, L_5, 0, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_003a:
	{
		int32_t L_6 = ___type0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)31)))))
		{
			goto IL_004f;
		}
	}
	{
		Hashtable_t909839986 * L_7 = ___data1;
		UniversalFeed_AutoFriendAdded_m1171627830(__this, L_7, 1, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_004f:
	{
		int32_t L_8 = ___type0;
		if ((!(((uint32_t)L_8) == ((uint32_t)5))))
		{
			goto IL_0062;
		}
	}
	{
		Hashtable_t909839986 * L_9 = ___data1;
		UniversalFeed_ActivateUserStealthMode_m807757662(__this, L_9, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0062:
	{
		int32_t L_10 = ___type0;
		if ((!(((uint32_t)L_10) == ((uint32_t)6))))
		{
			goto IL_0075;
		}
	}
	{
		Hashtable_t909839986 * L_11 = ___data1;
		UniversalFeed_DeactivateUserStealthMode_m2475276757(__this, L_11, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0075:
	{
		int32_t L_12 = ___type0;
		if ((!(((uint32_t)L_12) == ((uint32_t)7))))
		{
			goto IL_0088;
		}
	}
	{
		Hashtable_t909839986 * L_13 = ___data1;
		UniversalFeed_FuelRegeneration_m4051909779(__this, L_13, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0088:
	{
		int32_t L_14 = ___type0;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_009c;
		}
	}
	{
		Hashtable_t909839986 * L_15 = ___data1;
		UniversalFeed_IncomingMissileLaunched_m3413217878(__this, L_15, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_009c:
	{
		int32_t L_16 = ___type0;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00b0;
		}
	}
	{
		Hashtable_t909839986 * L_17 = ___data1;
		UniversalFeed_IncomingMissileHit_m3377027419(__this, L_17, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_00b0:
	{
		int32_t L_18 = ___type0;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00c4;
		}
	}
	{
		Hashtable_t909839986 * L_19 = ___data1;
		UniversalFeed_IncomingMissileBackfired_m276608961(__this, L_19, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_00c4:
	{
		int32_t L_20 = ___type0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_00d8;
		}
	}
	{
		Hashtable_t909839986 * L_21 = ___data1;
		UniversalFeed_IncomingMissileBlocked_m1124794290(__this, L_21, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_00d8:
	{
		int32_t L_22 = ___type0;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00ec;
		}
	}
	{
		Hashtable_t909839986 * L_23 = ___data1;
		UniversalFeed_OutgoingMissileHit_m2487313759(__this, L_23, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_00ec:
	{
		int32_t L_24 = ___type0;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0100;
		}
	}
	{
		Hashtable_t909839986 * L_25 = ___data1;
		UniversalFeed_OutgoingMissileDefended_m3398775051(__this, L_25, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0100:
	{
		int32_t L_26 = ___type0;
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0114;
		}
	}
	{
		Hashtable_t909839986 * L_27 = ___data1;
		UniversalFeed_OutgoingMissileBlocked_m1989842192(__this, L_27, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0114:
	{
		int32_t L_28 = ___type0;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0128;
		}
	}
	{
		Hashtable_t909839986 * L_29 = ___data1;
		UniversalFeed_PointsReset_m1552661162(__this, L_29, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0128:
	{
		int32_t L_30 = ___type0;
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_013c;
		}
	}
	{
		Hashtable_t909839986 * L_31 = ___data1;
		UniversalFeed_KnockoutUser_m2430757729(__this, L_31, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_013c:
	{
		int32_t L_32 = ___type0;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)19)))))
		{
			goto IL_0150;
		}
	}
	{
		Hashtable_t909839986 * L_33 = ___data1;
		UniversalFeed_RemoveUserKnockout_m1715649883(__this, L_33, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0150:
	{
		int32_t L_34 = ___type0;
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0164;
		}
	}
	{
		Hashtable_t909839986 * L_35 = ___data1;
		UniversalFeed_KnockoutFriend_m4181166270(__this, L_35, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0164:
	{
		int32_t L_36 = ___type0;
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_0178;
		}
	}
	{
		Hashtable_t909839986 * L_37 = ___data1;
		UniversalFeed_RemoveFriendKnockout_m1930223550(__this, L_37, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0178:
	{
		int32_t L_38 = ___type0;
		if ((!(((uint32_t)L_38) == ((uint32_t)8))))
		{
			goto IL_018b;
		}
	}
	{
		Hashtable_t909839986 * L_39 = ___data1;
		UniversalFeed_LevelAwarded_m1898210124(__this, L_39, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_018b:
	{
		int32_t L_40 = ___type0;
		if ((!(((uint32_t)L_40) == ((uint32_t)4))))
		{
			goto IL_019e;
		}
	}
	{
		Hashtable_t909839986 * L_41 = ___data1;
		UniversalFeed_InnerFriendsUpdate_m854973838(__this, L_41, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_019e:
	{
		int32_t L_42 = ___type0;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)22)))))
		{
			goto IL_01b2;
		}
	}
	{
		Hashtable_t909839986 * L_43 = ___data1;
		UniversalFeed_PointsUpdated_m4061199730(__this, L_43, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_01b2:
	{
		int32_t L_44 = ___type0;
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_01c6;
		}
	}
	{
		Hashtable_t909839986 * L_45 = ___data1;
		UniversalFeed_LongNotificationSent_m4056106827(__this, L_45, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_01c6:
	{
		int32_t L_46 = ___type0;
		if ((!(((uint32_t)L_46) == ((uint32_t)((int32_t)24)))))
		{
			goto IL_01da;
		}
	}
	{
		Hashtable_t909839986 * L_47 = ___data1;
		UniversalFeed_PlayerBlocked_m256374011(__this, L_47, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_01da:
	{
		int32_t L_48 = ___type0;
		if ((!(((uint32_t)L_48) == ((uint32_t)((int32_t)25)))))
		{
			goto IL_01ee;
		}
	}
	{
		Hashtable_t909839986 * L_49 = ___data1;
		UniversalFeed_PlayerUnblocked_m2608247246(__this, L_49, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_01ee:
	{
		int32_t L_50 = ___type0;
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)26)))))
		{
			goto IL_0202;
		}
	}
	{
		Hashtable_t909839986 * L_51 = ___data1;
		UniversalFeed_IncomingMineLaid_m677346689(__this, L_51, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0202:
	{
		int32_t L_52 = ___type0;
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0216;
		}
	}
	{
		Hashtable_t909839986 * L_53 = ___data1;
		UniversalFeed_IncomingMineExpired_m1386121628(__this, L_53, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_0216:
	{
		int32_t L_54 = ___type0;
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)28)))))
		{
			goto IL_022a;
		}
	}
	{
		Hashtable_t909839986 * L_55 = ___data1;
		UniversalFeed_IncomingMineComplete_m816087098(__this, L_55, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_022a:
	{
		int32_t L_56 = ___type0;
		if ((!(((uint32_t)L_56) == ((uint32_t)((int32_t)29)))))
		{
			goto IL_023e;
		}
	}
	{
		Hashtable_t909839986 * L_57 = ___data1;
		UniversalFeed_IncomingMineTimedOut_m528756124(__this, L_57, /*hidden argument*/NULL);
		goto IL_024d;
	}

IL_023e:
	{
		int32_t L_58 = ___type0;
		if ((!(((uint32_t)L_58) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_024d;
		}
	}
	{
		Hashtable_t909839986 * L_59 = ___data1;
		UniversalFeed_NemesisUpdated_m2406805583(__this, L_59, /*hidden argument*/NULL);
	}

IL_024d:
	{
		return;
	}
}
// System.Void UniversalFeed::IncomingFriendRequest(System.Collections.Hashtable)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* Friend_t3555014108_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t752959214_il2cpp_TypeInfo_var;
extern Il2CppClass* LanguageModel_t2428340347_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4183030420_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3172543035_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral51170699;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern Il2CppCodeGenString* _stringLiteral1015666662;
extern Il2CppCodeGenString* _stringLiteral1448002985;
extern Il2CppCodeGenString* _stringLiteral1984894250;
extern const uint32_t UniversalFeed_IncomingFriendRequest_m3177195349_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingFriendRequest_m3177195349 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingFriendRequest_m3177195349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	Friend_t3555014108 * V_1 = NULL;
	String_t* V_2 = NULL;
	Hashtable_t909839986 * V_3 = NULL;
	bool V_4 = false;
	int32_t V_5 = 0;
	LanguageModel_t2428340347 * V_6 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral51170699);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_1, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_2 = V_0;
		Friend_t3555014108 * L_3 = (Friend_t3555014108 *)il2cpp_codegen_object_new(Friend_t3555014108_il2cpp_TypeInfo_var);
		Friend__ctor_m1640256369(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Hashtable_t909839986 * L_4 = ___data0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, _stringLiteral287061489);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_2 = L_6;
		Hashtable_t909839986 * L_7 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_7, /*hidden argument*/NULL);
		V_3 = L_7;
		Hashtable_t909839986 * L_8 = V_3;
		String_t* L_9 = V_2;
		NullCheck(L_8);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_8, _stringLiteral339800794, L_9);
		V_4 = (bool)0;
		V_5 = 0;
		goto IL_00d2;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_10 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_10);
		Player_t1147783557 * L_11 = L_10->get_user_3();
		NullCheck(L_11);
		List_1_t2924135240 * L_12 = L_11->get_innerFriendsList_41();
		int32_t L_13 = V_5;
		NullCheck(L_12);
		Friend_t3555014108 * L_14 = List_1_get_Item_m4183030420(L_12, L_13, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		if (!L_14)
		{
			goto IL_00cc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_15 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_15);
		Player_t1147783557 * L_16 = L_15->get_user_3();
		NullCheck(L_16);
		List_1_t2924135240 * L_17 = L_16->get_innerFriendsList_41();
		int32_t L_18 = V_5;
		NullCheck(L_17);
		Friend_t3555014108 * L_19 = List_1_get_Item_m4183030420(L_17, L_18, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		NullCheck(L_19);
		String_t* L_20 = User_GetID_m1606899520(L_19, /*hidden argument*/NULL);
		Friend_t3555014108 * L_21 = V_1;
		NullCheck(L_21);
		String_t* L_22 = User_GetID_m1606899520(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00cc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_24 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_24);
		Player_t1147783557 * L_25 = L_24->get_user_3();
		NullCheck(L_25);
		List_1_t2924135240 * L_26 = L_25->get_innerFriendsList_41();
		int32_t L_27 = V_5;
		NullCheck(L_26);
		Friend_t3555014108 * L_28 = List_1_get_Item_m4183030420(L_26, L_27, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		Hashtable_t909839986 * L_29 = V_3;
		NullCheck(L_28);
		Friend_AddFriendRequest_m210257863(L_28, L_29, /*hidden argument*/NULL);
		PlayerManager_t1596653588 * L_30 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_30);
		Player_t1147783557 * L_31 = L_30->get_user_3();
		NullCheck(L_31);
		List_1_t2924135240 * L_32 = L_31->get_innerFriendsList_41();
		int32_t L_33 = V_5;
		NullCheck(L_32);
		Friend_t3555014108 * L_34 = List_1_get_Item_m4183030420(L_32, L_33, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		NullCheck(L_34);
		L_34->set_incomingFriendRequest_42((bool)1);
		V_4 = (bool)1;
		goto IL_00ed;
	}

IL_00cc:
	{
		int32_t L_35 = V_5;
		V_5 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00d2:
	{
		int32_t L_36 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_37 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_37);
		Player_t1147783557 * L_38 = L_37->get_user_3();
		NullCheck(L_38);
		List_1_t2924135240 * L_39 = L_38->get_innerFriendsList_41();
		NullCheck(L_39);
		int32_t L_40 = List_1_get_Count_m3172543035(L_39, /*hidden argument*/List_1_get_Count_m3172543035_MethodInfo_var);
		if ((((int32_t)L_36) < ((int32_t)L_40)))
		{
			goto IL_0046;
		}
	}

IL_00ed:
	{
		bool L_41 = V_4;
		if (L_41)
		{
			goto IL_0112;
		}
	}
	{
		Friend_t3555014108 * L_42 = V_1;
		Hashtable_t909839986 * L_43 = V_3;
		NullCheck(L_42);
		Friend_AddFriendRequest_m210257863(L_42, L_43, /*hidden argument*/NULL);
		Friend_t3555014108 * L_44 = V_1;
		NullCheck(L_44);
		Friend_SetGlobalFriend_m3265428825(L_44, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_45 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_45);
		Player_t1147783557 * L_46 = L_45->get_user_3();
		Friend_t3555014108 * L_47 = V_1;
		NullCheck(L_46);
		Player_AddIncPendingFriend_m3433020360(L_46, L_47, /*hidden argument*/NULL);
	}

IL_0112:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_48 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_48)
		{
			goto IL_0126;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_49 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_49);
		FriendManager_IncomingFriendRequestUpdate_m56842502(L_49, /*hidden argument*/NULL);
	}

IL_0126:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_50 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_50)
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_51 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_51);
		GameObject_t1756533147 * L_52 = L_51->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_53 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_52, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_54 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_54);
		GameObject_t1756533147 * L_55 = L_54->get_currentModalUI_7();
		NullCheck(L_55);
		GameObject_SendMessage_m1177535567(L_55, _stringLiteral1015666662, /*hidden argument*/NULL);
	}

IL_0159:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		bool L_56 = MonoSingleton_1_IsAvailable_m3383580221(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var);
		if (!L_56)
		{
			goto IL_0197;
		}
	}
	{
		LanguageModel_t2428340347 * L_57 = (LanguageModel_t2428340347 *)il2cpp_codegen_object_new(LanguageModel_t2428340347_il2cpp_TypeInfo_var);
		LanguageModel__ctor_m1803072634(L_57, /*hidden argument*/NULL);
		V_6 = L_57;
		LanguageModel_t2428340347 * L_58 = V_6;
		Friend_t3555014108 * L_59 = V_1;
		NullCheck(L_58);
		LanguageModel_AddObject_m3911119665(L_58, _stringLiteral1448002985, L_59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		ToastManager_t1002293494 * L_60 = MonoSingleton_1_get_Instance_m3921578081(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_61 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_61);
		LanguageConfig_t1338327304 * L_62 = L_61->get_langConfig_29();
		LanguageModel_t2428340347 * L_63 = V_6;
		NullCheck(L_62);
		String_t* L_64 = LanguageConfig_GetLanguageStringUsingModel_m2336875389(L_62, _stringLiteral1984894250, L_63, /*hidden argument*/NULL);
		NullCheck(L_60);
		ToastManager_OpenSmallToast_m805283205(L_60, L_64, /*hidden argument*/NULL);
	}

IL_0197:
	{
		return;
	}
}
// System.Void UniversalFeed::FriendRequestReplied(System.Collections.Hashtable)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t752959214_il2cpp_TypeInfo_var;
extern Il2CppClass* LanguageModel_t2428340347_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4183030420_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3172543035_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral1055450070;
extern Il2CppCodeGenString* _stringLiteral1959740304;
extern Il2CppCodeGenString* _stringLiteral867047697;
extern Il2CppCodeGenString* _stringLiteral2501681323;
extern Il2CppCodeGenString* _stringLiteral3830479505;
extern Il2CppCodeGenString* _stringLiteral1448002985;
extern Il2CppCodeGenString* _stringLiteral1894819181;
extern Il2CppCodeGenString* _stringLiteral3589880116;
extern const uint32_t UniversalFeed_FriendRequestReplied_m143828200_MetadataUsageId;
extern "C"  void UniversalFeed_FriendRequestReplied_m143828200 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_FriendRequestReplied_m143828200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	String_t* V_3 = NULL;
	bool V_4 = false;
	int32_t V_5 = 0;
	LanguageModel_t2428340347 * V_6 = NULL;
	Friend_t3555014108 * V_7 = NULL;
	LanguageModel_t2428340347 * V_8 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral287061489);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_3 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1055450070, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		Hashtable_t909839986 * L_6 = ___data0;
		NullCheck(L_6);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_6, _stringLiteral1959740304);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		V_1 = L_8;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_9 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0056;
		}
	}
	{
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral867047697, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0056:
	{
		V_2 = (bool)0;
		Hashtable_t909839986 * L_12 = ___data0;
		NullCheck(L_12);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_12, _stringLiteral2501681323);
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		V_3 = L_14;
		V_4 = (bool)0;
		V_5 = 0;
		goto IL_015f;
	}

IL_0074:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_15 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_15);
		Player_t1147783557 * L_16 = L_15->get_user_3();
		NullCheck(L_16);
		List_1_t2924135240 * L_17 = L_16->get_innerFriendsList_41();
		int32_t L_18 = V_5;
		NullCheck(L_17);
		Friend_t3555014108 * L_19 = List_1_get_Item_m4183030420(L_17, L_18, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		if (!L_19)
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_20 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_20);
		Player_t1147783557 * L_21 = L_20->get_user_3();
		NullCheck(L_21);
		List_1_t2924135240 * L_22 = L_21->get_innerFriendsList_41();
		int32_t L_23 = V_5;
		NullCheck(L_22);
		Friend_t3555014108 * L_24 = List_1_get_Item_m4183030420(L_22, L_23, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		NullCheck(L_24);
		String_t* L_25 = User_GetID_m1606899520(L_24, /*hidden argument*/NULL);
		String_t* L_26 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_28 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_28);
		Player_t1147783557 * L_29 = L_28->get_user_3();
		NullCheck(L_29);
		List_1_t2924135240 * L_30 = L_29->get_innerFriendsList_41();
		int32_t L_31 = V_5;
		NullCheck(L_30);
		Friend_t3555014108 * L_32 = List_1_get_Item_m4183030420(L_30, L_31, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		NullCheck(L_32);
		Friend_DeleteFriendRequest_m2571495599(L_32, /*hidden argument*/NULL);
		String_t* L_33 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_33, _stringLiteral3830479505, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0151;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_35 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_35);
		Player_t1147783557 * L_36 = L_35->get_user_3();
		NullCheck(L_36);
		List_1_t2924135240 * L_37 = L_36->get_innerFriendsList_41();
		int32_t L_38 = V_5;
		NullCheck(L_37);
		Friend_t3555014108 * L_39 = List_1_get_Item_m4183030420(L_37, L_38, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		NullCheck(L_39);
		Friend_SetGlobalFriend_m3265428825(L_39, (bool)0, /*hidden argument*/NULL);
		V_2 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		bool L_40 = MonoSingleton_1_IsAvailable_m3383580221(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var);
		if (!L_40)
		{
			goto IL_0151;
		}
	}
	{
		LanguageModel_t2428340347 * L_41 = (LanguageModel_t2428340347 *)il2cpp_codegen_object_new(LanguageModel_t2428340347_il2cpp_TypeInfo_var);
		LanguageModel__ctor_m1803072634(L_41, /*hidden argument*/NULL);
		V_6 = L_41;
		LanguageModel_t2428340347 * L_42 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_43 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_43);
		Player_t1147783557 * L_44 = L_43->get_user_3();
		NullCheck(L_44);
		List_1_t2924135240 * L_45 = L_44->get_innerFriendsList_41();
		int32_t L_46 = V_5;
		NullCheck(L_45);
		Friend_t3555014108 * L_47 = List_1_get_Item_m4183030420(L_45, L_46, /*hidden argument*/List_1_get_Item_m4183030420_MethodInfo_var);
		NullCheck(L_42);
		LanguageModel_AddObject_m3911119665(L_42, _stringLiteral1448002985, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		ToastManager_t1002293494 * L_48 = MonoSingleton_1_get_Instance_m3921578081(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_49 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_49);
		LanguageConfig_t1338327304 * L_50 = L_49->get_langConfig_29();
		LanguageModel_t2428340347 * L_51 = V_6;
		NullCheck(L_50);
		String_t* L_52 = LanguageConfig_GetLanguageStringUsingModel_m2336875389(L_50, _stringLiteral1894819181, L_51, /*hidden argument*/NULL);
		NullCheck(L_48);
		ToastManager_OpenSmallToast_m805283205(L_48, L_52, /*hidden argument*/NULL);
	}

IL_0151:
	{
		V_4 = (bool)1;
		goto IL_017a;
	}

IL_0159:
	{
		int32_t L_53 = V_5;
		V_5 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_54 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_55 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_55);
		Player_t1147783557 * L_56 = L_55->get_user_3();
		NullCheck(L_56);
		List_1_t2924135240 * L_57 = L_56->get_innerFriendsList_41();
		NullCheck(L_57);
		int32_t L_58 = List_1_get_Count_m3172543035(L_57, /*hidden argument*/List_1_get_Count_m3172543035_MethodInfo_var);
		if ((((int32_t)L_54) < ((int32_t)L_58)))
		{
			goto IL_0074;
		}
	}

IL_017a:
	{
		bool L_59 = V_4;
		if (L_59)
		{
			goto IL_0217;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_60 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		String_t* L_61 = V_3;
		NullCheck(L_60);
		Friend_t3555014108 * L_62 = PlayerManager_CheckOutgoingPendingList_m2926296793(L_60, L_61, /*hidden argument*/NULL);
		V_7 = L_62;
		Friend_t3555014108 * L_63 = V_7;
		if (!L_63)
		{
			goto IL_0217;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_64 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_64);
		Player_t1147783557 * L_65 = L_64->get_user_3();
		Friend_t3555014108 * L_66 = V_7;
		NullCheck(L_65);
		Player_RemoveOutPendingFriend_m1308657843(L_65, L_66, /*hidden argument*/NULL);
		Friend_t3555014108 * L_67 = V_7;
		NullCheck(L_67);
		Friend_DeleteFriendRequest_m2571495599(L_67, /*hidden argument*/NULL);
		String_t* L_68 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_69 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_68, _stringLiteral3830479505, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0217;
		}
	}
	{
		V_2 = (bool)1;
		Friend_t3555014108 * L_70 = V_7;
		NullCheck(L_70);
		Friend_SetGlobalFriend_m3265428825(L_70, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_71 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_71);
		Player_t1147783557 * L_72 = L_71->get_user_3();
		Friend_t3555014108 * L_73 = V_7;
		NullCheck(L_72);
		Player_AddOuterFriend_m3393695944(L_72, L_73, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		bool L_74 = MonoSingleton_1_IsAvailable_m3383580221(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var);
		if (!L_74)
		{
			goto IL_0217;
		}
	}
	{
		LanguageModel_t2428340347 * L_75 = (LanguageModel_t2428340347 *)il2cpp_codegen_object_new(LanguageModel_t2428340347_il2cpp_TypeInfo_var);
		LanguageModel__ctor_m1803072634(L_75, /*hidden argument*/NULL);
		V_8 = L_75;
		LanguageModel_t2428340347 * L_76 = V_8;
		Friend_t3555014108 * L_77 = V_7;
		NullCheck(L_76);
		LanguageModel_AddObject_m3911119665(L_76, _stringLiteral1448002985, L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		ToastManager_t1002293494 * L_78 = MonoSingleton_1_get_Instance_m3921578081(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_79 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_79);
		LanguageConfig_t1338327304 * L_80 = L_79->get_langConfig_29();
		LanguageModel_t2428340347 * L_81 = V_8;
		NullCheck(L_80);
		String_t* L_82 = LanguageConfig_GetLanguageStringUsingModel_m2336875389(L_80, _stringLiteral1894819181, L_81, /*hidden argument*/NULL);
		NullCheck(L_78);
		ToastManager_OpenSmallToast_m805283205(L_78, L_82, /*hidden argument*/NULL);
	}

IL_0217:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_83 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_83)
		{
			goto IL_022d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_84 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		bool L_85 = V_2;
		String_t* L_86 = V_3;
		NullCheck(L_84);
		FriendManager_OutgoingFriendRequestUpdate_m2474854607(L_84, L_85, L_86, /*hidden argument*/NULL);
	}

IL_022d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_87 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_87)
		{
			goto IL_0243;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_88 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		bool L_89 = V_2;
		String_t* L_90 = V_3;
		NullCheck(L_88);
		AttackManager_OutgoingFriendRequestUpdate_m2823455769(L_88, L_89, L_90, /*hidden argument*/NULL);
	}

IL_0243:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_91 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_91)
		{
			goto IL_027c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_92 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_92);
		GameObject_t1756533147 * L_93 = L_92->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_94 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_93, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_027c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_95 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_95);
		GameObject_t1756533147 * L_96 = L_95->get_currentModalUI_7();
		bool L_97 = V_2;
		bool L_98 = L_97;
		Il2CppObject * L_99 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_98);
		NullCheck(L_96);
		GameObject_SendMessage_m2115020133(L_96, _stringLiteral3589880116, L_99, /*hidden argument*/NULL);
	}

IL_027c:
	{
		return;
	}
}
// System.Void UniversalFeed::AutoFriendAdded(System.Collections.Hashtable,AutoFriendType)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* Friend_t3555014108_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t752959214_il2cpp_TypeInfo_var;
extern Il2CppClass* LanguageModel_t2428340347_il2cpp_TypeInfo_var;
extern Il2CppClass* AutoFriendType_t103565391_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4283060082;
extern Il2CppCodeGenString* _stringLiteral1615844055;
extern Il2CppCodeGenString* _stringLiteral3939549399;
extern Il2CppCodeGenString* _stringLiteral1958614947;
extern Il2CppCodeGenString* _stringLiteral1448002985;
extern Il2CppCodeGenString* _stringLiteral2348570912;
extern const uint32_t UniversalFeed_AutoFriendAdded_m1171627830_MetadataUsageId;
extern "C"  void UniversalFeed_AutoFriendAdded_m1171627830 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, int32_t ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_AutoFriendAdded_m1171627830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	Friend_t3555014108 * V_1 = NULL;
	Friend_t3555014108 * V_2 = NULL;
	LanguageModel_t2428340347 * V_3 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral4283060082);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_1, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_2 = V_0;
		Friend_t3555014108 * L_3 = (Friend_t3555014108 *)il2cpp_codegen_object_new(Friend_t3555014108_il2cpp_TypeInfo_var);
		Friend__ctor_m1640256369(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_4 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_4);
		Player_t1147783557 * L_5 = L_4->get_user_3();
		Friend_t3555014108 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = User_GetID_m1606899520(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Friend_t3555014108 * L_8 = Player_CheckIfPlayerFriend_m3839000879(L_5, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Friend_t3555014108 * L_9 = V_2;
		if (L_9)
		{
			goto IL_00f0;
		}
	}
	{
		Friend_t3555014108 * L_10 = V_1;
		NullCheck(L_10);
		Friend_SetGlobalFriend_m3265428825(L_10, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_11 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_11);
		Player_t1147783557 * L_12 = L_11->get_user_3();
		Friend_t3555014108 * L_13 = V_1;
		NullCheck(L_12);
		Player_AddOuterFriend_m3393695944(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_14 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0075;
		}
	}
	{
		Friend_t3555014108 * L_15 = V_1;
		NullCheck(L_15);
		String_t* L_16 = User_GetID_m1606899520(L_15, /*hidden argument*/NULL);
		Friend_t3555014108 * L_17 = V_1;
		NullCheck(L_17);
		String_t* L_18 = User_GetFullName_m1642387097(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral1615844055, L_16, _stringLiteral3939549399, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_20 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_21 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_21);
		FriendManager_ContactFriendAddition_m1693357766(L_21, /*hidden argument*/NULL);
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_22 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_22)
		{
			goto IL_009d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_23 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_23);
		AttackManager_ContactFriendAddition_m1355861958(L_23, /*hidden argument*/NULL);
	}

IL_009d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		bool L_24 = MonoSingleton_1_IsAvailable_m3383580221(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m3383580221_MethodInfo_var);
		if (!L_24)
		{
			goto IL_00f0;
		}
	}
	{
		LanguageModel_t2428340347 * L_25 = (LanguageModel_t2428340347 *)il2cpp_codegen_object_new(LanguageModel_t2428340347_il2cpp_TypeInfo_var);
		LanguageModel__ctor_m1803072634(L_25, /*hidden argument*/NULL);
		V_3 = L_25;
		LanguageModel_t2428340347 * L_26 = V_3;
		Il2CppObject * L_27 = Box(AutoFriendType_t103565391_il2cpp_TypeInfo_var, (&___type1));
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_27);
		NullCheck(L_26);
		LanguageModel_AddObject_m3467800580(L_26, _stringLiteral1958614947, L_28, /*hidden argument*/NULL);
		LanguageModel_t2428340347 * L_29 = V_3;
		Friend_t3555014108 * L_30 = V_1;
		NullCheck(L_29);
		LanguageModel_AddObject_m3911119665(L_29, _stringLiteral1448002985, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		ToastManager_t1002293494 * L_31 = MonoSingleton_1_get_Instance_m3921578081(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1990368447_il2cpp_TypeInfo_var);
		ConfigManager_t2239702727 * L_32 = MonoSingleton_1_get_Instance_m4274868308(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4274868308_MethodInfo_var);
		NullCheck(L_32);
		LanguageConfig_t1338327304 * L_33 = L_32->get_langConfig_29();
		LanguageModel_t2428340347 * L_34 = V_3;
		NullCheck(L_33);
		String_t* L_35 = LanguageConfig_GetLanguageStringUsingModel_m2336875389(L_33, _stringLiteral2348570912, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		ToastManager_OpenSmallToast_m805283205(L_31, L_35, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		return;
	}
}
// System.Void UniversalFeed::InnerFriendsUpdate(System.Collections.Hashtable)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1635564570;
extern Il2CppCodeGenString* _stringLiteral907054555;
extern Il2CppCodeGenString* _stringLiteral3640134458;
extern const uint32_t UniversalFeed_InnerFriendsUpdate_m854973838_MetadataUsageId;
extern "C"  void UniversalFeed_InnerFriendsUpdate_m854973838 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_InnerFriendsUpdate_m854973838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral1635564570);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_2, _stringLiteral907054555, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004d;
		}
	}
	{
		Hashtable_t909839986 * L_4 = ___data0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, _stringLiteral1635564570);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_5, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_6);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		Hashtable_t909839986 * L_10 = V_0;
		NullCheck(L_9);
		Player_UpdateInnerFriendsList_m2357477435(L_9, L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_11 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_11)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_12 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_12);
		FriendManager_InnerFriendUpdate_m3799450959(L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_13 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_13)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_14 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_14);
		AttackManager_InnerFriendUpdate_m1031136153(L_14, /*hidden argument*/NULL);
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_15 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_15)
		{
			goto IL_00a8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_16 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_19 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = L_19->get_currentModalUI_7();
		NullCheck(L_20);
		GameObject_SendMessage_m1177535567(L_20, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		return;
	}
}
// System.Void UniversalFeed::PlayerBlocked(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2543256415_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1702771522_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2070012008_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2080789023;
extern Il2CppCodeGenString* _stringLiteral3640134458;
extern const uint32_t UniversalFeed_PlayerBlocked_m256374011_MetadataUsageId;
extern "C"  void UniversalFeed_PlayerBlocked_m256374011 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_PlayerBlocked_m256374011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Friend_t3555014108 * V_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2080789023);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_3 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		String_t* L_4 = V_0;
		NullCheck(L_3);
		Friend_t3555014108 * L_5 = PlayerManager_GetExistingPlayer_m4237454126(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Friend_t3555014108 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_00a5;
		}
	}
	{
		Friend_t3555014108 * L_7 = V_1;
		NullCheck(L_7);
		L_7->set_incomingBlocked_44((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_8 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_9 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_9);
		FriendManager_InnerFriendUpdate_m3799450959(L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_10 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_10)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_11 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_11);
		AttackManager_InnerFriendUpdate_m1031136153(L_11, /*hidden argument*/NULL);
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2543256415_il2cpp_TypeInfo_var);
		bool L_12 = MonoSingleton_1_IsAvailable_m1702771522(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1702771522_MethodInfo_var);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2543256415_il2cpp_TypeInfo_var);
		ChatManager_t2792590695 * L_13 = MonoSingleton_1_get_Instance_m2070012008(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2070012008_MethodInfo_var);
		NullCheck(L_13);
		L_13->set_threadsListUpdated_12((bool)1);
		ChatManager_t2792590695 * L_14 = MonoSingleton_1_get_Instance_m2070012008(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2070012008_MethodInfo_var);
		NullCheck(L_14);
		ChatManager_StartList_m1650398138(L_14, 0, /*hidden argument*/NULL);
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_15 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_15)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_16 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_19 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = L_19->get_currentModalUI_7();
		NullCheck(L_20);
		GameObject_SendMessage_m1177535567(L_20, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UniversalFeed::PlayerUnblocked(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2543256415_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1702771522_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2070012008_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025070970;
extern Il2CppCodeGenString* _stringLiteral3640134458;
extern const uint32_t UniversalFeed_PlayerUnblocked_m2608247246_MetadataUsageId;
extern "C"  void UniversalFeed_PlayerUnblocked_m2608247246 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_PlayerUnblocked_m2608247246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Friend_t3555014108 * V_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3025070970);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_3 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		String_t* L_4 = V_0;
		NullCheck(L_3);
		Friend_t3555014108 * L_5 = PlayerManager_GetExistingPlayer_m4237454126(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Friend_t3555014108 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_00a5;
		}
	}
	{
		Friend_t3555014108 * L_7 = V_1;
		NullCheck(L_7);
		L_7->set_incomingBlocked_44((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_8 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_9 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_9);
		FriendManager_InnerFriendUpdate_m3799450959(L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_10 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_10)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_11 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_11);
		AttackManager_InnerFriendUpdate_m1031136153(L_11, /*hidden argument*/NULL);
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2543256415_il2cpp_TypeInfo_var);
		bool L_12 = MonoSingleton_1_IsAvailable_m1702771522(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1702771522_MethodInfo_var);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2543256415_il2cpp_TypeInfo_var);
		ChatManager_t2792590695 * L_13 = MonoSingleton_1_get_Instance_m2070012008(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2070012008_MethodInfo_var);
		NullCheck(L_13);
		L_13->set_threadsListUpdated_12((bool)1);
		ChatManager_t2792590695 * L_14 = MonoSingleton_1_get_Instance_m2070012008(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2070012008_MethodInfo_var);
		NullCheck(L_14);
		ChatManager_StartList_m1650398138(L_14, 0, /*hidden argument*/NULL);
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_15 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_15)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_16 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_19 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = L_19->get_currentModalUI_7();
		NullCheck(L_20);
		GameObject_SendMessage_m1177535567(L_20, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UniversalFeed::ActivateUserStealthMode(System.Collections.Hashtable)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3623672911;
extern Il2CppCodeGenString* _stringLiteral3749952829;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral3640134458;
extern const uint32_t UniversalFeed_ActivateUserStealthMode_m807757662_MetadataUsageId;
extern "C"  void UniversalFeed_ActivateUserStealthMode_m807757662 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_ActivateUserStealthMode_m807757662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Friend_t3555014108 * V_2 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3623672911);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_3 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		Hashtable_t909839986 * L_4 = ___data0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, _stringLiteral3749952829);
		if (!L_5)
		{
			goto IL_0070;
		}
	}
	{
		Hashtable_t909839986 * L_6 = ___data0;
		NullCheck(L_6);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_6, _stringLiteral3749952829);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_8, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0070;
		}
	}
	{
		Hashtable_t909839986 * L_10 = ___data0;
		NullCheck(L_10);
		Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_10, _stringLiteral3749952829);
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_13 = Convert_ToDateTime_m2228400765(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_14 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		DateTime_t693205669  L_15 = V_1;
		NullCheck(L_14);
		DateTime_t693205669  L_16 = BaseServer_1_ApplyServerTimeOffset_m2903528104(L_14, L_15, /*hidden argument*/BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var);
		V_1 = L_16;
		DateTime_t693205669  L_17 = DateTime_ToLocalTime_m1957689902((&V_1), /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_0070:
	{
		String_t* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_19 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_19);
		Player_t1147783557 * L_20 = L_19->get_user_3();
		NullCheck(L_20);
		String_t* L_21 = User_GetID_m1606899520(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_23 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_23);
		Player_t1147783557 * L_24 = L_23->get_user_3();
		DateTime_t693205669  L_25 = V_1;
		NullCheck(L_24);
		User_EnterStealthMode_m2512144816(L_24, L_25, /*hidden argument*/NULL);
		UniversalFeed_CheckLockoutUIActivation_m3357259255(__this, /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_26 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_26);
		Player_t1147783557 * L_27 = L_26->get_user_3();
		String_t* L_28 = V_0;
		NullCheck(L_27);
		Friend_t3555014108 * L_29 = Player_CheckIfPlayerFriend_m3839000879(L_27, L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		Friend_t3555014108 * L_30 = V_2;
		if (!L_30)
		{
			goto IL_00c3;
		}
	}
	{
		Friend_t3555014108 * L_31 = V_2;
		DateTime_t693205669  L_32 = V_1;
		NullCheck(L_31);
		User_EnterStealthMode_m2512144816(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_33 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_33)
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_34 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_34);
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m3105766835(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		GameObject_SendMessage_m1177535567(L_35, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_36 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_36)
		{
			goto IL_00ff;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_37 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_37);
		GameObject_t1756533147 * L_38 = Component_get_gameObject_m3105766835(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		GameObject_SendMessage_m1177535567(L_38, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_39 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_39)
		{
			goto IL_0142;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_40 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_40);
		GameObject_t1756533147 * L_41 = L_40->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_41, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0142;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_43 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_43);
		int32_t L_44 = L_43->get_activeModal_3();
		if ((!(((uint32_t)L_44) == ((uint32_t)1))))
		{
			goto IL_0142;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_45 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_45);
		GameObject_t1756533147 * L_46 = L_45->get_currentModalUI_7();
		NullCheck(L_46);
		GameObject_SendMessage_m1177535567(L_46, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_0142:
	{
		return;
	}
}
// System.Void UniversalFeed::DeactivateUserStealthMode(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3873508672_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3848760317_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3623672911;
extern Il2CppCodeGenString* _stringLiteral3640134458;
extern const uint32_t UniversalFeed_DeactivateUserStealthMode_m2475276757_MetadataUsageId;
extern "C"  void UniversalFeed_DeactivateUserStealthMode_m2475276757 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_DeactivateUserStealthMode_m2475276757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Friend_t3555014108 * V_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3623672911);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_4 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_4);
		Player_t1147783557 * L_5 = L_4->get_user_3();
		NullCheck(L_5);
		String_t* L_6 = User_GetID_m1606899520(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		NullCheck(L_9);
		User_ExitStealthMode_m4032515930(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3873508672_il2cpp_TypeInfo_var);
		LockoutManager_t4122842952 * L_10 = MonoSingleton_1_get_Instance_m3848760317(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3848760317_MethodInfo_var);
		NullCheck(L_10);
		LockoutManager_UnloadLockoutState_m1438471479(L_10, 2, (bool)0, /*hidden argument*/NULL);
		goto IL_00a4;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_11 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_11);
		Player_t1147783557 * L_12 = L_11->get_user_3();
		String_t* L_13 = V_0;
		NullCheck(L_12);
		Friend_t3555014108 * L_14 = Player_CheckIfPlayerFriend_m3839000879(L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		Friend_t3555014108 * L_15 = V_1;
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		Friend_t3555014108 * L_16 = V_1;
		NullCheck(L_16);
		User_ExitStealthMode_m4032515930(L_16, /*hidden argument*/NULL);
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_17 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_17)
		{
			goto IL_0086;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_18 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_18);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_SendMessage_m1177535567(L_19, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_0086:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_20 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_20)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_21 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_SendMessage_m1177535567(L_22, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_23 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_23)
		{
			goto IL_00d7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_24 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = L_24->get_currentModalUI_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_25, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00d7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_27 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_27);
		GameObject_t1756533147 * L_28 = L_27->get_currentModalUI_7();
		NullCheck(L_28);
		GameObject_SendMessage_m1177535567(L_28, _stringLiteral3640134458, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::CheckLockoutUIActivation()
extern Il2CppClass* MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m395896004_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var;
extern const MethodInfo* PanelTransitionManager_1_GetActivePanel_m173370810_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m4119218369_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var;
extern const uint32_t UniversalFeed_CheckLockoutUIActivation_m3357259255_MetadataUsageId;
extern "C"  void UniversalFeed_CheckLockoutUIActivation_m3357259255 (UniversalFeed_t2144776909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_CheckLockoutUIActivation_m3357259255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		bool L_0 = MonoSingleton_1_IsAvailable_m395896004(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m395896004_MethodInfo_var);
		if (!L_0)
		{
			goto IL_00f7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_1 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_1);
		Panel_t1787746694 * L_2 = ((PanelTransitionManager_1_t2526031859 *)L_1)->get_activePanel_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_4 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_4);
		Panel_t1787746694 * L_5 = PanelTransitionManager_1_GetActivePanel_m173370810(L_4, /*hidden argument*/PanelTransitionManager_1_GetActivePanel_m173370810_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_panelName_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_7 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, bool >::Invoke(5 /* System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::TogglePanel(PanelType,System.Boolean) */, L_7, 4, (bool)0);
		goto IL_00a9;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_8 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_8);
		Panel_t1787746694 * L_9 = PanelTransitionManager_1_GetActivePanel_m173370810(L_8, /*hidden argument*/PanelTransitionManager_1_GetActivePanel_m173370810_MethodInfo_var);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_panelName_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_00a9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_11 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_11);
		VirtActionInvoker2< int32_t, bool >::Invoke(5 /* System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::TogglePanel(PanelType,System.Boolean) */, L_11, 2, (bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_12 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_12)
		{
			goto IL_00a9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_13 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_activeModal_3();
		if (!L_14)
		{
			goto IL_00a9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_15 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_15);
		int32_t L_16 = L_15->get_activeModal_3();
		if ((((int32_t)L_16) == ((int32_t)2)))
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_17 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_17);
		int32_t L_18 = L_17->get_activeModal_3();
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_00a9;
		}
	}

IL_009f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_19 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_19);
		ModalManager_UnloadModal_m581591513(L_19, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		goto IL_00f7;
	}

IL_00ae:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		bool L_20 = MonoSingleton_1_IsAvailable_m1749275871(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1749275871_MethodInfo_var);
		if (!L_20)
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_21 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_21);
		int32_t L_22 = L_21->get_activeModal_3();
		if (!L_22)
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_23 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_23);
		int32_t L_24 = L_23->get_activeModal_3();
		if ((!(((uint32_t)L_24) == ((uint32_t)4))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_25 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_25);
		ModalManager_UnloadModal_m581591513(L_25, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var);
		bool L_26 = MonoSingleton_1_IsAvailable_m4119218369(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m4119218369_MethodInfo_var);
		if (!L_26)
		{
			goto IL_00f7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var);
		HomeSceneManager_t2690266116 * L_27 = MonoSingleton_1_get_Instance_m2687909429(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var);
		NullCheck(L_27);
		HomeSceneManager_CheckLockoutState_m2761069386(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		return;
	}
}
// System.Void UniversalFeed::KnockoutUser(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const uint32_t UniversalFeed_KnockoutUser_m2430757729_MetadataUsageId;
extern "C"  void UniversalFeed_KnockoutUser_m2430757729 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_KnockoutUser_m2430757729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_0 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_0);
		Player_t1147783557 * L_1 = L_0->get_user_3();
		Hashtable_t909839986 * L_2 = ___data0;
		NullCheck(L_1);
		User_UpdateKnockoutData_m3519987727(L_1, L_2, /*hidden argument*/NULL);
		UniversalFeed_CheckLockoutUIActivation_m3357259255(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::RemoveUserKnockout(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3873508672_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3848760317_MethodInfo_var;
extern const uint32_t UniversalFeed_RemoveUserKnockout_m1715649883_MetadataUsageId;
extern "C"  void UniversalFeed_RemoveUserKnockout_m1715649883 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_RemoveUserKnockout_m1715649883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_0 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_0);
		Player_t1147783557 * L_1 = L_0->get_user_3();
		NullCheck(L_1);
		User_ExitKnockOut_m815129388(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3873508672_il2cpp_TypeInfo_var);
		LockoutManager_t4122842952 * L_2 = MonoSingleton_1_get_Instance_m3848760317(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3848760317_MethodInfo_var);
		NullCheck(L_2);
		LockoutManager_UnloadLockoutState_m1438471479(L_2, 1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::KnockoutFriend(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3623672911;
extern const uint32_t UniversalFeed_KnockoutFriend_m4181166270_MetadataUsageId;
extern "C"  void UniversalFeed_KnockoutFriend_m4181166270 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_KnockoutFriend_m4181166270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Friend_t3555014108 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_0 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		Hashtable_t909839986 * L_1 = ___data0;
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, _stringLiteral3623672911);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_0);
		Friend_t3555014108 * L_4 = PlayerManager_GetExistingPlayer_m4237454126(L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Friend_t3555014108 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		Friend_t3555014108 * L_6 = V_0;
		Hashtable_t909839986 * L_7 = ___data0;
		NullCheck(L_6);
		User_UpdateKnockoutData_m3519987727(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_8 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_8)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_9 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_9);
		FriendManager_InnerFriendUpdate_m3799450959(L_9, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_10 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_10)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_11 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_11);
		AttackManager_InnerFriendUpdate_m1031136153(L_11, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UniversalFeed::RemoveFriendKnockout(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3623672911;
extern const uint32_t UniversalFeed_RemoveFriendKnockout_m1930223550_MetadataUsageId;
extern "C"  void UniversalFeed_RemoveFriendKnockout_m1930223550 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_RemoveFriendKnockout_m1930223550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Friend_t3555014108 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_0 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		Hashtable_t909839986 * L_1 = ___data0;
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, _stringLiteral3623672911);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_0);
		Friend_t3555014108 * L_4 = PlayerManager_GetExistingPlayer_m4237454126(L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Friend_t3555014108 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		Friend_t3555014108 * L_6 = V_0;
		NullCheck(L_6);
		User_ExitKnockOut_m815129388(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_7 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_8 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_8);
		AttackManager_InnerFriendUpdate_m1031136153(L_8, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UniversalFeed::FuelRegeneration(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624050;
extern const uint32_t UniversalFeed_FuelRegeneration_m4051909779_MetadataUsageId;
extern "C"  void UniversalFeed_FuelRegeneration_m4051909779 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_FuelRegeneration_m4051909779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_0 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_0);
		Player_t1147783557 * L_1 = L_0->get_user_3();
		Hashtable_t909839986 * L_2 = ___data0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral1743624050);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		int32_t L_5 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_currentFuel_53(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		bool L_6 = MonoSingleton_1_IsAvailable_m1265861587(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		HeaderManager_t49185160 * L_7 = MonoSingleton_1_get_Instance_m1428073779(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var);
		NullCheck(L_7);
		HeaderManager_SetFuelBar_m3291002710(L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void UniversalFeed::LevelAwarded(System.Collections.Hashtable)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var;
extern Il2CppClass* LevelAward_t2605567013_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral586649447;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern Il2CppCodeGenString* _stringLiteral1594891052;
extern Il2CppCodeGenString* _stringLiteral889338293;
extern Il2CppCodeGenString* _stringLiteral2778558052;
extern const uint32_t UniversalFeed_LevelAwarded_m1898210124_MetadataUsageId;
extern "C"  void UniversalFeed_LevelAwarded_m1898210124 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_LevelAwarded_m1898210124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	LevelAward_t2605567013 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral586649447, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Hashtable_t909839986 * L_1 = ___data0;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral339800794);
		if (!L_2)
		{
			goto IL_00b9;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___data0;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1594891052);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		int32_t L_6 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Hashtable_t909839986 * L_7 = ___data0;
		NullCheck(L_7);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_7, _stringLiteral889338293);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		int32_t L_10 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Hashtable_t909839986 * L_11 = ___data0;
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_11, _stringLiteral2778558052);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		V_2 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_14 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_14);
		Player_t1147783557 * L_15 = L_14->get_user_3();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		User_SetRankLevel_m784945417(L_15, L_16, /*hidden argument*/NULL);
		PlayerManager_t1596653588 * L_17 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_17);
		Player_t1147783557 * L_18 = L_17->get_user_3();
		String_t* L_19 = V_2;
		NullCheck(L_18);
		User_SetRankName_m1393779823(L_18, L_19, /*hidden argument*/NULL);
		PlayerManager_t1596653588 * L_20 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_20);
		Player_t1147783557 * L_21 = L_20->get_user_3();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		Player_AddBucks_m1842278886(L_21, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		bool L_23 = MonoSingleton_1_IsAvailable_m1265861587(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var);
		if (!L_23)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		HeaderManager_t49185160 * L_24 = MonoSingleton_1_get_Instance_m1428073779(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var);
		NullCheck(L_24);
		HeaderManager_SetBucksLabel_m4159429163(L_24, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		int32_t L_25 = V_0;
		String_t* L_26 = V_2;
		int32_t L_27 = V_1;
		LevelAward_t2605567013 * L_28 = (LevelAward_t2605567013 *)il2cpp_codegen_object_new(LevelAward_t2605567013_il2cpp_TypeInfo_var);
		LevelAward__ctor_m3976876206(L_28, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_3 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_29 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		LevelAward_t2605567013 * L_30 = V_3;
		NullCheck(L_29);
		ModalManager_LoadAwardModal_m1283904096(L_29, L_30, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		return;
	}
}
// System.Void UniversalFeed::IncomingMissileLaunched(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CIncomingMissileLaunchedU3Em__1_m958946509_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern const uint32_t UniversalFeed_IncomingMissileLaunched_m3413217878_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMissileLaunched_m3413217878 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMissileLaunched_m3413217878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3CIncomingMissileLaunchedU3Em__1_m958946509_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_6(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::IncomingMissileBackfired(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CIncomingMissileBackfiredU3Em__2_m1578107957_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern const uint32_t UniversalFeed_IncomingMissileBackfired_m276608961_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMissileBackfired_m276608961 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMissileBackfired_m276608961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_7();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3CIncomingMissileBackfiredU3Em__2_m1578107957_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_7(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_7();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::IncomingMissileHit(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CIncomingMissileHitU3Em__3_m3571877754_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern const uint32_t UniversalFeed_IncomingMissileHit_m3377027419_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMissileHit_m3377027419 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMissileHit_m3377027419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_8();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3CIncomingMissileHitU3Em__3_m3571877754_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_8(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_8();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::IncomingMissileBlocked(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CIncomingMissileBlockedU3Em__4_m2082762212_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern Il2CppCodeGenString* _stringLiteral686271777;
extern Il2CppCodeGenString* _stringLiteral326007245;
extern const uint32_t UniversalFeed_IncomingMissileBlocked_m1124794290_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMissileBlocked_m1124794290 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMissileBlocked_m1124794290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_9();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3CIncomingMissileBlockedU3Em__4_m2082762212_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_9(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_9();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_9 = ___data0;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_9, _stringLiteral686271777);
		if (!L_10)
		{
			goto IL_00b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_11 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_11);
		Player_t1147783557 * L_12 = L_11->get_user_3();
		NullCheck(L_12);
		bool L_13 = User_get_missileShielded_m1115021069(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00b3;
		}
	}
	{
		Hashtable_t909839986 * L_14 = ___data0;
		NullCheck(L_14);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_14, _stringLiteral686271777);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_1 = L_16;
		Hashtable_t909839986 * L_17 = ___data0;
		NullCheck(L_17);
		Il2CppObject * L_18 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_17, _stringLiteral326007245);
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		V_2 = L_19;
		String_t* L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_21 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_21);
		Player_t1147783557 * L_22 = L_21->get_user_3();
		NullCheck(L_22);
		Shield_t3327121081 * L_23 = ((User_t719925459 *)L_22)->get_currentMissileShield_16();
		NullCheck(L_23);
		String_t* L_24 = ((Item_t2440468191 *)L_23)->get_id_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_26 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_26);
		Player_t1147783557 * L_27 = L_26->get_user_3();
		NullCheck(L_27);
		Shield_t3327121081 * L_28 = ((User_t719925459 *)L_27)->get_currentMissileShield_16();
		String_t* L_29 = V_2;
		NullCheck(L_28);
		Shield_SetDeactivateTime_m1019816785(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		return;
	}
}
// System.Void UniversalFeed::OutgoingMissileHit(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3COutgoingMissileHitU3Em__5_m2875933902_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern const uint32_t UniversalFeed_OutgoingMissileHit_m2487313759_MetadataUsageId;
extern "C"  void UniversalFeed_OutgoingMissileHit_m2487313759 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_OutgoingMissileHit_m2487313759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_10();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3COutgoingMissileHitU3Em__5_m2875933902_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_10(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_10();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::OutgoingMissileDefended(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3COutgoingMissileDefendedU3Em__6_m1535567463_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern const uint32_t UniversalFeed_OutgoingMissileDefended_m3398775051_MetadataUsageId;
extern "C"  void UniversalFeed_OutgoingMissileDefended_m3398775051 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_OutgoingMissileDefended_m3398775051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_11();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3COutgoingMissileDefendedU3Em__6_m1535567463_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_11(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_11();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::OutgoingMissileBlocked(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3COutgoingMissileBlockedU3Em__7_m2383077407_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2655765478;
extern const uint32_t UniversalFeed_OutgoingMissileBlocked_m1989842192_MetadataUsageId;
extern "C"  void UniversalFeed_OutgoingMissileBlocked_m1989842192 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_OutgoingMissileBlocked_m1989842192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2655765478);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_12();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3COutgoingMissileBlockedU3Em__7_m2383077407_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_12(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_12();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMissileWarefareStatus_m3758223651(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::PointsReset(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CPointsResetU3Em__8_m191711564_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1838257019;
extern Il2CppCodeGenString* _stringLiteral3519098914;
extern const uint32_t UniversalFeed_PointsReset_m1552661162_MetadataUsageId;
extern "C"  void UniversalFeed_PointsReset_m1552661162 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_PointsReset_m1552661162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ChatmageddonServer_t594474938 * G_B8_0 = NULL;
	ChatmageddonServer_t594474938 * G_B7_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, _stringLiteral1838257019);
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		Hashtable_t909839986 * L_2 = ___data0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral1838257019);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		int32_t L_5 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_6 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_6);
		Player_t1147783557 * L_7 = L_6->get_user_3();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		User_UpdateDailyPoints_m3824119854(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		bool L_9 = MonoSingleton_1_IsAvailable_m1265861587(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		HeaderManager_t49185160 * L_10 = MonoSingleton_1_get_Instance_m1428073779(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var);
		NullCheck(L_10);
		HeaderManager_SetDailyPointsLabel_m4255293245(L_10, /*hidden argument*/NULL);
	}

IL_004a:
	{
		Hashtable_t909839986 * L_11 = ___data0;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_11, _stringLiteral3519098914);
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		Hashtable_t909839986 * L_13 = ___data0;
		NullCheck(L_13);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_13, _stringLiteral3519098914);
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		int32_t L_16 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_17 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_17);
		Player_t1147783557 * L_18 = L_17->get_user_3();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		((User_t719925459 *)L_18)->set_totalPoints_26(L_19);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		bool L_20 = MonoSingleton_1_IsAvailable_m1265861587(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var);
		if (!L_20)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		HeaderManager_t49185160 * L_21 = MonoSingleton_1_get_Instance_m1428073779(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var);
		NullCheck(L_21);
		HeaderManager_SetDailyPointsLabel_m4255293245(L_21, /*hidden argument*/NULL);
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_22 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		Action_3_t3681841185 * L_23 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_13();
		G_B7_0 = L_22;
		if (L_23)
		{
			G_B8_0 = L_22;
			goto IL_00b1;
		}
	}
	{
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)UniversalFeed_U3CPointsResetU3Em__8_m191711564_MethodInfo_var);
		Action_3_t3681841185 * L_25 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_25, NULL, L_24, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_13(L_25);
		G_B8_0 = G_B7_0;
	}

IL_00b1:
	{
		Action_3_t3681841185 * L_26 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_13();
		NullCheck(G_B8_0);
		ChatmageddonServer_ResetPoints_m4188559892(G_B8_0, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::PointsUpdated(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1838257019;
extern const uint32_t UniversalFeed_PointsUpdated_m4061199730_MetadataUsageId;
extern "C"  void UniversalFeed_PointsUpdated_m4061199730 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_PointsUpdated_m4061199730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, _stringLiteral1838257019);
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		Hashtable_t909839986 * L_2 = ___data0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral1838257019);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		int32_t L_5 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_6 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_6);
		Player_t1147783557 * L_7 = L_6->get_user_3();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		User_UpdateDailyPoints_m3824119854(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		bool L_9 = MonoSingleton_1_IsAvailable_m1265861587(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		HeaderManager_t49185160 * L_10 = MonoSingleton_1_get_Instance_m1428073779(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var);
		NullCheck(L_10);
		HeaderManager_SetDailyPointsLabel_m4255293245(L_10, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void UniversalFeed::LongNotificationSent(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2139140742_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2515493933_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2411302897_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3423761293;
extern const uint32_t UniversalFeed_LongNotificationSent_m4056106827_MetadataUsageId;
extern "C"  void UniversalFeed_LongNotificationSent_m4056106827 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_LongNotificationSent_m4056106827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, _stringLiteral3423761293);
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		Hashtable_t909839986 * L_2 = ___data0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral3423761293);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_5 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_5);
		Player_t1147783557 * L_6 = L_5->get_user_3();
		NullCheck(L_6);
		bool L_7 = Player_GetAllowIGN_m2838283383(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2139140742_il2cpp_TypeInfo_var);
		NotificationManager_t2388475022 * L_8 = MonoSingleton_1_get_Instance_m2515493933(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2515493933_MethodInfo_var);
		String_t* L_9 = V_0;
		NullCheck(L_8);
		NotificationManager_AddUnreadNotification_m147346916(L_8, L_9, /*hidden argument*/NULL);
		goto IL_0059;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2139140742_il2cpp_TypeInfo_var);
		bool L_10 = MonoSingleton_1_IsAvailable_m2411302897(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2411302897_MethodInfo_var);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2139140742_il2cpp_TypeInfo_var);
		NotificationManager_t2388475022 * L_11 = MonoSingleton_1_get_Instance_m2515493933(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2515493933_MethodInfo_var);
		NullCheck(L_11);
		NotificationManager_ClearNotifications_m541151290(L_11, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Void UniversalFeed::IncomingMineLaid(System.Collections.Hashtable)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* Mine_t2729441277_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457518803;
extern const uint32_t UniversalFeed_IncomingMineLaid_m677346689_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMineLaid_m677346689 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMineLaid_m677346689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mine_t2729441277 * V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3457518803);
		Mine_t2729441277 * L_2 = (Mine_t2729441277 *)il2cpp_codegen_object_new(Mine_t2729441277_il2cpp_TypeInfo_var);
		Mine__ctor_m414532838(L_2, ((Hashtable_t909839986 *)CastclassClass(L_1, Hashtable_t909839986_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		bool L_3 = MonoSingleton_1_IsAvailable_m2547723652(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		MapManager_t3593696545 * L_4 = MonoSingleton_1_get_Instance_m3809739556(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var);
		Mine_t2729441277 * L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = MapManager_CheckMineInTriggerRange_m279616690(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		MapManager_t3593696545 * L_7 = MonoSingleton_1_get_Instance_m3809739556(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var);
		Mine_t2729441277 * L_8 = V_0;
		NullCheck(L_7);
		MapManager_TriggerMine_m45962698(L_7, L_8, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UniversalFeed::IncomingMineExpired(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t4083902911_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m641947622_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral243350237;
extern Il2CppCodeGenString* _stringLiteral2677918659;
extern const uint32_t UniversalFeed_IncomingMineExpired_m1386121628_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMineExpired_m1386121628 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMineExpired_m1386121628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral243350237);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4083902911_il2cpp_TypeInfo_var);
		InventoryManager_t38269895 * L_3 = MonoSingleton_1_get_Instance_m641947622(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m641947622_MethodInfo_var);
		String_t* L_4 = V_0;
		NullCheck(L_3);
		InventoryManager_RemoveMineFromInventory_m3683215027(L_3, _stringLiteral2677918659, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		bool L_5 = MonoSingleton_1_IsAvailable_m2547723652(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		MapManager_t3593696545 * L_6 = MonoSingleton_1_get_Instance_m3809739556(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_currentState_8();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		MapManager_t3593696545 * L_8 = MonoSingleton_1_get_Instance_m3809739556(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var);
		NullCheck(L_8);
		MapManager_ForceCurrentMapState_m4268007775(L_8, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniversalFeed::IncomingMineComplete(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CIncomingMineCompleteU3Em__9_m2365915627_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral243350237;
extern const uint32_t UniversalFeed_IncomingMineComplete_m816087098_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMineComplete_m816087098 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMineComplete_m816087098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral243350237);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_14();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3CIncomingMineCompleteU3Em__9_m2365915627_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache8_14(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_14();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMineWarefareStatus_m2729867900(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::IncomingMineTimedOut(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* UniversalFeed_t2144776909_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2572512622_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UniversalFeed_U3CIncomingMineTimedOutU3Em__A_m4053126829_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2885404279_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1574472535_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral243350237;
extern const uint32_t UniversalFeed_IncomingMineTimedOut_m528756124_MetadataUsageId;
extern "C"  void UniversalFeed_IncomingMineTimedOut_m528756124 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_IncomingMineTimedOut_m528756124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	ChatmageddonServer_t594474938 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	ChatmageddonServer_t594474938 * G_B1_1 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral243350237);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_3 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_4 = V_0;
		Action_3_t3681841185 * L_5 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_15();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UniversalFeed_U3CIncomingMineTimedOutU3Em__A_m4053126829_MethodInfo_var);
		Action_3_t3681841185 * L_7 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_7, NULL, L_6, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache9_15(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		Action_3_t3681841185 * L_8 = ((UniversalFeed_t2144776909_StaticFields*)UniversalFeed_t2144776909_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_15();
		NullCheck(G_B2_1);
		ChatmageddonServer_GetMineWarefareStatus_m2729867900(G_B2_1, G_B2_0, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2572512622_il2cpp_TypeInfo_var);
		bool L_9 = MonoSingleton_1_IsAvailable_m2885404279(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2885404279_MethodInfo_var);
		if (!L_9)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2572512622_il2cpp_TypeInfo_var);
		MineManager_t2821846902 * L_10 = MonoSingleton_1_get_Instance_m1574472535(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1574472535_MethodInfo_var);
		NullCheck(L_10);
		MineManager_ForceRemoveMineUI_m1788555135(L_10, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UniversalFeed::NemesisUpdated(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2324137218;
extern const uint32_t UniversalFeed_NemesisUpdated_m2406805583_MetadataUsageId;
extern "C"  void UniversalFeed_NemesisUpdated_m2406805583 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_NemesisUpdated_m2406805583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, _stringLiteral2324137218);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_2 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_2);
		Player_t1147783557 * L_3 = L_2->get_user_3();
		Hashtable_t909839986 * L_4 = ___data0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, _stringLiteral2324137218);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		NullCheck(L_3);
		L_3->set_nemesisID_56(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		bool L_7 = MonoSingleton_1_IsAvailable_m1423037380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1423037380_MethodInfo_var);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4081773843_il2cpp_TypeInfo_var);
		FriendManager_t36140827 * L_8 = MonoSingleton_1_get_Instance_m2787360518(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2787360518_MethodInfo_var);
		NullCheck(L_8);
		FriendManager_InnerFriendUpdate_m3799450959(L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		bool L_9 = MonoSingleton_1_IsAvailable_m2440759360(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2440759360_MethodInfo_var);
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3226218845_il2cpp_TypeInfo_var);
		AttackManager_t3475553125 * L_10 = MonoSingleton_1_get_Instance_m2989894674(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2989894674_MethodInfo_var);
		NullCheck(L_10);
		AttackManager_InnerFriendUpdate_m1031136153(L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UniversalFeed::<GetFeedInfo>__BaseCallProxy0()
extern "C"  void UniversalFeed_U3CGetFeedInfoU3E__BaseCallProxy0_m3082176731 (UniversalFeed_t2144776909 * __this, const MethodInfo* method)
{
	{
		Feed_GetFeedInfo_m2681812927(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniversalFeed::<GetFeedInfo>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t UniversalFeed_U3CGetFeedInfoU3Em__0_m1170692438_MetadataUsageId;
extern "C"  void UniversalFeed_U3CGetFeedInfoU3Em__0_m1170692438 (UniversalFeed_t2144776909 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CGetFeedInfoU3Em__0_m1170692438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		VirtActionInvoker1< Hashtable_t909839986 * >::Invoke(6 /* System.Void Feed::ProcessFeedInfo(System.Collections.Hashtable) */, __this, L_1);
		UniversalFeed_U3CGetFeedInfoU3E__BaseCallProxy0_m3082176731(__this, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_2 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0028:
	{
		UniversalFeed_U3CGetFeedInfoU3E__BaseCallProxy0_m3082176731(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UniversalFeed::<IncomingMissileLaunched>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedMissile_t1542515810_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedItem_t3670634427_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1861297139_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1331888220_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern const uint32_t UniversalFeed_U3CIncomingMissileLaunchedU3Em__1_m958946509_MetadataUsageId;
extern "C"  void UniversalFeed_U3CIncomingMissileLaunchedU3Em__1_m958946509 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CIncomingMissileLaunchedU3Em__1_m958946509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LaunchedMissile_t1542515810 * V_0 = NULL;
	LaunchedItem_t3670634427 * V_1 = NULL;
	Friend_t3555014108 * V_2 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_00bc;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_00b7;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		LaunchedMissile_t1542515810 * L_5 = (LaunchedMissile_t1542515810 *)il2cpp_codegen_object_new(LaunchedMissile_t1542515810_il2cpp_TypeInfo_var);
		LaunchedMissile__ctor_m1140740047(L_5, ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_5;
		LaunchedMissile_t1542515810 * L_6 = V_0;
		LaunchedItem_t3670634427 * L_7 = (LaunchedItem_t3670634427 *)il2cpp_codegen_object_new(LaunchedItem_t3670634427_il2cpp_TypeInfo_var);
		LaunchedItem__ctor_m526981076(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		LaunchedItem_t3670634427 * L_10 = V_1;
		NullCheck(L_9);
		Player_AddLaunchedItem_m233124113(L_9, L_10, /*hidden argument*/NULL);
		LaunchedItem_t3670634427 * L_11 = V_1;
		NullCheck(L_11);
		LaunchedMissile_t1542515810 * L_12 = L_11->get_missileItem_1();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_firstStrikePoints_13();
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0097;
		}
	}
	{
		LaunchedItem_t3670634427 * L_14 = V_1;
		NullCheck(L_14);
		LaunchedMissile_t1542515810 * L_15 = L_14->get_missileItem_1();
		NullCheck(L_15);
		Friend_t3555014108 * L_16 = L_15->get_friend_22();
		if (!L_16)
		{
			goto IL_0097;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_17 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		LaunchedItem_t3670634427 * L_18 = V_1;
		NullCheck(L_18);
		LaunchedMissile_t1542515810 * L_19 = L_18->get_missileItem_1();
		NullCheck(L_19);
		Friend_t3555014108 * L_20 = L_19->get_friend_22();
		NullCheck(L_20);
		String_t* L_21 = User_GetID_m1606899520(L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		Friend_t3555014108 * L_22 = PlayerManager_GetExistingFriend_m4019571299(L_17, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		Friend_t3555014108 * L_23 = V_2;
		if (!L_23)
		{
			goto IL_0097;
		}
	}
	{
		Friend_t3555014108 * L_24 = V_2;
		NullCheck(L_24);
		L_24->set_strikeFirstPoints_49(0);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1861297139_il2cpp_TypeInfo_var);
		ApplicationManager_t2110631419 * L_25 = MonoSingleton_1_get_Instance_m1331888220(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1331888220_MethodInfo_var);
		Friend_t3555014108 * L_26 = V_2;
		NullCheck(L_25);
		ApplicationManager_UpdateAvatars_m3701950467(L_25, L_26, /*hidden argument*/NULL);
	}

IL_0097:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_27 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_27)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_28 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_28);
		L_28->set_incomingListUpdated_3((bool)1);
		StatusManager_t2720247037 * L_29 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_29);
		StatusManager_StartList_m904555342(L_29, 0, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		goto IL_00cc;
	}

IL_00bc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_30 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00cc;
		}
	}
	{
		String_t* L_31 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		return;
	}
}
// System.Void UniversalFeed::<IncomingMissileBackfired>m__2(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedMissile_t1542515810_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedItem_t3670634427_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern const uint32_t UniversalFeed_U3CIncomingMissileBackfiredU3Em__2_m1578107957_MetadataUsageId;
extern "C"  void UniversalFeed_U3CIncomingMissileBackfiredU3Em__2_m1578107957 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CIncomingMissileBackfiredU3Em__2_m1578107957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LaunchedMissile_t1542515810 * V_0 = NULL;
	LaunchedItem_t3670634427 * V_1 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_0068;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_0063;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		LaunchedMissile_t1542515810 * L_5 = (LaunchedMissile_t1542515810 *)il2cpp_codegen_object_new(LaunchedMissile_t1542515810_il2cpp_TypeInfo_var);
		LaunchedMissile__ctor_m1140740047(L_5, ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_5;
		LaunchedMissile_t1542515810 * L_6 = V_0;
		LaunchedItem_t3670634427 * L_7 = (LaunchedItem_t3670634427 *)il2cpp_codegen_object_new(LaunchedItem_t3670634427_il2cpp_TypeInfo_var);
		LaunchedItem__ctor_m526981076(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		LaunchedItem_t3670634427 * L_10 = V_1;
		NullCheck(L_9);
		Player_AddLaunchedItem_m233124113(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_11 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_12 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_12);
		L_12->set_incomingListUpdated_3((bool)1);
		StatusManager_t2720247037 * L_13 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_13);
		StatusManager_StartList_m904555342(L_13, 0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_0078;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_14 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_15 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void UniversalFeed::<IncomingMissileHit>m__3(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m387146023_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3589382540_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern const uint32_t UniversalFeed_U3CIncomingMissileHitU3Em__3_m3571877754_MetadataUsageId;
extern "C"  void UniversalFeed_U3CIncomingMissileHitU3Em__3_m3571877754 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CIncomingMissileHitU3Em__3_m3571877754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_00c7;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_00c2;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, _stringLiteral339800794);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0088;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		NullCheck(L_9);
		List_1_t3039755559 * L_10 = L_9->get_incLaunchedItems_46();
		int32_t L_11 = V_2;
		NullCheck(L_10);
		LaunchedItem_t3670634427 * L_12 = List_1_get_Item_m387146023(L_10, L_11, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_id_5();
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_16 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_16);
		Player_t1147783557 * L_17 = L_16->get_user_3();
		NullCheck(L_17);
		List_1_t3039755559 * L_18 = L_17->get_incLaunchedItems_46();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		LaunchedItem_t3670634427 * L_20 = List_1_get_Item_m387146023(L_18, L_19, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_20);
		LaunchedMissile_t1542515810 * L_21 = L_20->get_missileItem_1();
		Hashtable_t909839986 * L_22 = V_0;
		NullCheck(L_21);
		LaunchedMissile_UpdateMissile_m1188196666(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0084:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_25 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_25);
		Player_t1147783557 * L_26 = L_25->get_user_3();
		NullCheck(L_26);
		List_1_t3039755559 * L_27 = L_26->get_incLaunchedItems_46();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m3589382540(L_27, /*hidden argument*/List_1_get_Count_m3589382540_MethodInfo_var);
		if ((((int32_t)L_24) < ((int32_t)L_28)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_29 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_29)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_30 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_30);
		L_30->set_incomingListUpdated_3((bool)1);
		StatusManager_t2720247037 * L_31 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_31);
		StatusManager_StartList_m904555342(L_31, 0, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		goto IL_00d7;
	}

IL_00c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_32 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d7;
		}
	}
	{
		String_t* L_33 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::<IncomingMissileBlocked>m__4(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m387146023_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3589382540_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern const uint32_t UniversalFeed_U3CIncomingMissileBlockedU3Em__4_m2082762212_MetadataUsageId;
extern "C"  void UniversalFeed_U3CIncomingMissileBlockedU3Em__4_m2082762212 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CIncomingMissileBlockedU3Em__4_m2082762212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_00c7;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_00c2;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, _stringLiteral339800794);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0088;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		NullCheck(L_9);
		List_1_t3039755559 * L_10 = L_9->get_incLaunchedItems_46();
		int32_t L_11 = V_2;
		NullCheck(L_10);
		LaunchedItem_t3670634427 * L_12 = List_1_get_Item_m387146023(L_10, L_11, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_id_5();
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_16 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_16);
		Player_t1147783557 * L_17 = L_16->get_user_3();
		NullCheck(L_17);
		List_1_t3039755559 * L_18 = L_17->get_incLaunchedItems_46();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		LaunchedItem_t3670634427 * L_20 = List_1_get_Item_m387146023(L_18, L_19, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_20);
		LaunchedMissile_t1542515810 * L_21 = L_20->get_missileItem_1();
		Hashtable_t909839986 * L_22 = V_0;
		NullCheck(L_21);
		LaunchedMissile_UpdateMissile_m1188196666(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0084:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_25 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_25);
		Player_t1147783557 * L_26 = L_25->get_user_3();
		NullCheck(L_26);
		List_1_t3039755559 * L_27 = L_26->get_incLaunchedItems_46();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m3589382540(L_27, /*hidden argument*/List_1_get_Count_m3589382540_MethodInfo_var);
		if ((((int32_t)L_24) < ((int32_t)L_28)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_29 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_29)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_30 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_30);
		L_30->set_incomingListUpdated_3((bool)1);
		StatusManager_t2720247037 * L_31 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_31);
		StatusManager_StartList_m904555342(L_31, 0, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		goto IL_00d7;
	}

IL_00c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_32 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d7;
		}
	}
	{
		String_t* L_33 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::<OutgoingMissileHit>m__5(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m387146023_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3589382540_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern const uint32_t UniversalFeed_U3COutgoingMissileHitU3Em__5_m2875933902_MetadataUsageId;
extern "C"  void UniversalFeed_U3COutgoingMissileHitU3Em__5_m2875933902 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3COutgoingMissileHitU3Em__5_m2875933902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_00c7;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_00c2;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, _stringLiteral339800794);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0088;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		NullCheck(L_9);
		List_1_t3039755559 * L_10 = L_9->get_outLaunchedItems_47();
		int32_t L_11 = V_2;
		NullCheck(L_10);
		LaunchedItem_t3670634427 * L_12 = List_1_get_Item_m387146023(L_10, L_11, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_id_5();
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_16 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_16);
		Player_t1147783557 * L_17 = L_16->get_user_3();
		NullCheck(L_17);
		List_1_t3039755559 * L_18 = L_17->get_outLaunchedItems_47();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		LaunchedItem_t3670634427 * L_20 = List_1_get_Item_m387146023(L_18, L_19, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_20);
		LaunchedMissile_t1542515810 * L_21 = L_20->get_missileItem_1();
		Hashtable_t909839986 * L_22 = V_0;
		NullCheck(L_21);
		LaunchedMissile_UpdateMissile_m1188196666(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0084:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_25 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_25);
		Player_t1147783557 * L_26 = L_25->get_user_3();
		NullCheck(L_26);
		List_1_t3039755559 * L_27 = L_26->get_outLaunchedItems_47();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m3589382540(L_27, /*hidden argument*/List_1_get_Count_m3589382540_MethodInfo_var);
		if ((((int32_t)L_24) < ((int32_t)L_28)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_29 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_29)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_30 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_30);
		L_30->set_outgoingListUpdated_4((bool)1);
		StatusManager_t2720247037 * L_31 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_31);
		StatusManager_StartList_m904555342(L_31, 1, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		goto IL_00d7;
	}

IL_00c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_32 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d7;
		}
	}
	{
		String_t* L_33 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::<OutgoingMissileDefended>m__6(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m387146023_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3589382540_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern const uint32_t UniversalFeed_U3COutgoingMissileDefendedU3Em__6_m1535567463_MetadataUsageId;
extern "C"  void UniversalFeed_U3COutgoingMissileDefendedU3Em__6_m1535567463 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3COutgoingMissileDefendedU3Em__6_m1535567463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_00c7;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_00c2;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, _stringLiteral339800794);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0088;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		NullCheck(L_9);
		List_1_t3039755559 * L_10 = L_9->get_outLaunchedItems_47();
		int32_t L_11 = V_2;
		NullCheck(L_10);
		LaunchedItem_t3670634427 * L_12 = List_1_get_Item_m387146023(L_10, L_11, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_id_5();
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_16 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_16);
		Player_t1147783557 * L_17 = L_16->get_user_3();
		NullCheck(L_17);
		List_1_t3039755559 * L_18 = L_17->get_outLaunchedItems_47();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		LaunchedItem_t3670634427 * L_20 = List_1_get_Item_m387146023(L_18, L_19, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_20);
		LaunchedMissile_t1542515810 * L_21 = L_20->get_missileItem_1();
		Hashtable_t909839986 * L_22 = V_0;
		NullCheck(L_21);
		LaunchedMissile_UpdateMissile_m1188196666(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0084:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_25 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_25);
		Player_t1147783557 * L_26 = L_25->get_user_3();
		NullCheck(L_26);
		List_1_t3039755559 * L_27 = L_26->get_outLaunchedItems_47();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m3589382540(L_27, /*hidden argument*/List_1_get_Count_m3589382540_MethodInfo_var);
		if ((((int32_t)L_24) < ((int32_t)L_28)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_29 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_29)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_30 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_30);
		L_30->set_outgoingListUpdated_4((bool)1);
		StatusManager_t2720247037 * L_31 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_31);
		StatusManager_StartList_m904555342(L_31, 1, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		goto IL_00d7;
	}

IL_00c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_32 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d7;
		}
	}
	{
		String_t* L_33 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::<OutgoingMissileBlocked>m__7(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m387146023_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3589382540_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1540949558;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern const uint32_t UniversalFeed_U3COutgoingMissileBlockedU3Em__7_m2383077407_MetadataUsageId;
extern "C"  void UniversalFeed_U3COutgoingMissileBlockedU3Em__7_m2383077407 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3COutgoingMissileBlockedU3Em__7_m2383077407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_00c7;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1540949558);
		if (!L_2)
		{
			goto IL_00c2;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1540949558);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, _stringLiteral339800794);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0088;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		NullCheck(L_9);
		List_1_t3039755559 * L_10 = L_9->get_outLaunchedItems_47();
		int32_t L_11 = V_2;
		NullCheck(L_10);
		LaunchedItem_t3670634427 * L_12 = List_1_get_Item_m387146023(L_10, L_11, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_id_5();
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_16 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_16);
		Player_t1147783557 * L_17 = L_16->get_user_3();
		NullCheck(L_17);
		List_1_t3039755559 * L_18 = L_17->get_outLaunchedItems_47();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		LaunchedItem_t3670634427 * L_20 = List_1_get_Item_m387146023(L_18, L_19, /*hidden argument*/List_1_get_Item_m387146023_MethodInfo_var);
		NullCheck(L_20);
		LaunchedMissile_t1542515810 * L_21 = L_20->get_missileItem_1();
		Hashtable_t909839986 * L_22 = V_0;
		NullCheck(L_21);
		LaunchedMissile_UpdateMissile_m1188196666(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0084:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_25 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_25);
		Player_t1147783557 * L_26 = L_25->get_user_3();
		NullCheck(L_26);
		List_1_t3039755559 * L_27 = L_26->get_outLaunchedItems_47();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m3589382540(L_27, /*hidden argument*/List_1_get_Count_m3589382540_MethodInfo_var);
		if ((((int32_t)L_24) < ((int32_t)L_28)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_29 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_29)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_30 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_30);
		L_30->set_outgoingListUpdated_4((bool)1);
		StatusManager_t2720247037 * L_31 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_31);
		StatusManager_StartList_m904555342(L_31, 1, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		goto IL_00d7;
	}

IL_00c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_32 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d7;
		}
	}
	{
		String_t* L_33 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void UniversalFeed::<PointsReset>m__8(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1861297139_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m1331888220_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1838257019;
extern const uint32_t UniversalFeed_U3CPointsResetU3Em__8_m191711564_MetadataUsageId;
extern "C"  void UniversalFeed_U3CPointsResetU3Em__8_m191711564 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CPointsResetU3Em__8_m191711564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_006f;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral1838257019);
		if (!L_2)
		{
			goto IL_006a;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral1838257019);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		int32_t L_6 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_7 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_7);
		Player_t1147783557 * L_8 = L_7->get_user_3();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		User_UpdateDailyPoints_m3824119854(L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		bool L_10 = MonoSingleton_1_IsAvailable_m1265861587(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1265861587_MethodInfo_var);
		if (!L_10)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4094818176_il2cpp_TypeInfo_var);
		HeaderManager_t49185160 * L_11 = MonoSingleton_1_get_Instance_m1428073779(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1428073779_MethodInfo_var);
		NullCheck(L_11);
		HeaderManager_SetDailyPointsLabel_m4255293245(L_11, /*hidden argument*/NULL);
	}

IL_0050:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_12 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_12);
		Player_t1147783557 * L_13 = L_12->get_user_3();
		NullCheck(L_13);
		L_13->set_pointsReset_57((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1861297139_il2cpp_TypeInfo_var);
		ApplicationManager_t2110631419 * L_14 = MonoSingleton_1_get_Instance_m1331888220(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m1331888220_MethodInfo_var);
		NullCheck(L_14);
		ApplicationManager_CheckFirstPlayOfDay_m1041101535(L_14, /*hidden argument*/NULL);
	}

IL_006a:
	{
		goto IL_007f;
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_15 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_16 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UniversalFeed::<IncomingMineComplete>m__9(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedMine_t3677282773_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedItem_t3670634427_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457518803;
extern const uint32_t UniversalFeed_U3CIncomingMineCompleteU3Em__9_m2365915627_MetadataUsageId;
extern "C"  void UniversalFeed_U3CIncomingMineCompleteU3Em__9_m2365915627 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CIncomingMineCompleteU3Em__9_m2365915627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LaunchedMine_t3677282773 * V_0 = NULL;
	LaunchedItem_t3670634427 * V_1 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_0068;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral3457518803);
		if (!L_2)
		{
			goto IL_0063;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral3457518803);
		LaunchedMine_t3677282773 * L_5 = (LaunchedMine_t3677282773 *)il2cpp_codegen_object_new(LaunchedMine_t3677282773_il2cpp_TypeInfo_var);
		LaunchedMine__ctor_m1515173176(L_5, ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_5;
		LaunchedMine_t3677282773 * L_6 = V_0;
		LaunchedItem_t3670634427 * L_7 = (LaunchedItem_t3670634427 *)il2cpp_codegen_object_new(LaunchedItem_t3670634427_il2cpp_TypeInfo_var);
		LaunchedItem__ctor_m2612281629(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		LaunchedItem_t3670634427 * L_10 = V_1;
		NullCheck(L_9);
		Player_AddLaunchedItem_m233124113(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_11 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_12 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_12);
		L_12->set_incomingListUpdated_3((bool)1);
		StatusManager_t2720247037 * L_13 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_13);
		StatusManager_StartList_m904555342(L_13, 0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_0078;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_14 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_15 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void UniversalFeed::<IncomingMineTimedOut>m__A(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedMine_t3677282773_il2cpp_TypeInfo_var;
extern Il2CppClass* LaunchedItem_t3670634427_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m821239464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457518803;
extern const uint32_t UniversalFeed_U3CIncomingMineTimedOutU3Em__A_m4053126829_MetadataUsageId;
extern "C"  void UniversalFeed_U3CIncomingMineTimedOutU3Em__A_m4053126829 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniversalFeed_U3CIncomingMineTimedOutU3Em__A_m4053126829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LaunchedMine_t3677282773 * V_0 = NULL;
	LaunchedItem_t3670634427 * V_1 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_0068;
		}
	}
	{
		Hashtable_t909839986 * L_1 = ___info1;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_1, _stringLiteral3457518803);
		if (!L_2)
		{
			goto IL_0063;
		}
	}
	{
		Hashtable_t909839986 * L_3 = ___info1;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral3457518803);
		LaunchedMine_t3677282773 * L_5 = (LaunchedMine_t3677282773 *)il2cpp_codegen_object_new(LaunchedMine_t3677282773_il2cpp_TypeInfo_var);
		LaunchedMine__ctor_m1515173176(L_5, ((Hashtable_t909839986 *)CastclassClass(L_4, Hashtable_t909839986_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_5;
		LaunchedMine_t3677282773 * L_6 = V_0;
		LaunchedItem_t3670634427 * L_7 = (LaunchedItem_t3670634427 *)il2cpp_codegen_object_new(LaunchedItem_t3670634427_il2cpp_TypeInfo_var);
		LaunchedItem__ctor_m2612281629(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_8 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_8);
		Player_t1147783557 * L_9 = L_8->get_user_3();
		LaunchedItem_t3670634427 * L_10 = V_1;
		NullCheck(L_9);
		Player_AddLaunchedItem_m233124113(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		bool L_11 = MonoSingleton_1_IsAvailable_m1135699226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m1135699226_MethodInfo_var);
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2470912757_il2cpp_TypeInfo_var);
		StatusManager_t2720247037 * L_12 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_12);
		L_12->set_incomingListUpdated_3((bool)1);
		StatusManager_t2720247037 * L_13 = MonoSingleton_1_get_Instance_m821239464(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m821239464_MethodInfo_var);
		NullCheck(L_13);
		StatusManager_StartList_m904555342(L_13, 0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_0078;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_14 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_15 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void UnloadLogoutUIButton::.ctor()
extern "C"  void UnloadLogoutUIButton__ctor_m493960578 (UnloadLogoutUIButton_t2627629715 * __this, const MethodInfo* method)
{
	{
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnloadLogoutUIButton::OnButtonClick()
extern "C"  void UnloadLogoutUIButton_OnButtonClick_m396652151 (UnloadLogoutUIButton_t2627629715 * __this, const MethodInfo* method)
{
	{
		LogOutTransition_t3259858399 * L_0 = __this->get_logoutTransition_5();
		bool L_1 = __this->get_logout_6();
		NullCheck(L_0);
		LogOutTransition_CloseUI_m178826239(L_0, L_1, /*hidden argument*/NULL);
		SFXButton_OnButtonClick_m3012217745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdateActiveLeaderboardButton::.ctor()
extern "C"  void UpdateActiveLeaderboardButton__ctor_m1293716381 (UpdateActiveLeaderboardButton_t3273818106 * __this, const MethodInfo* method)
{
	{
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdateActiveLeaderboardButton::OnButtonClick()
extern Il2CppClass* MonoSingleton_1_t1431643053_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m459929268_MethodInfo_var;
extern const uint32_t UpdateActiveLeaderboardButton_OnButtonClick_m1183314706_MetadataUsageId;
extern "C"  void UpdateActiveLeaderboardButton_OnButtonClick_m1183314706 (UpdateActiveLeaderboardButton_t3273818106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdateActiveLeaderboardButton_OnButtonClick_m1183314706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1431643053_il2cpp_TypeInfo_var);
		LeaderboardNavigationController_t1680977333 * L_0 = MonoSingleton_1_get_Instance_m459929268(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m459929268_MethodInfo_var);
		int32_t L_1 = __this->get_type_5();
		NullCheck(L_0);
		LeaderboardNavigationController_UpdateActiveLeaderboardType_m566824345(L_0, L_1, /*hidden argument*/NULL);
		SFXButton_OnButtonClick_m3012217745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdatePasswordNav::.ctor()
extern "C"  void UpdatePasswordNav__ctor_m4256045576 (UpdatePasswordNav_t2102371893 * __this, const MethodInfo* method)
{
	{
		NavigationScreen__ctor_m2982757537(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdatePasswordNav::Start()
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const uint32_t UpdatePasswordNav_Start_m4058908112_MetadataUsageId;
extern "C"  void UpdatePasswordNav_Start_m4058908112 (UpdatePasswordNav_t2102371893 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdatePasswordNav_Start_m4058908112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TweenPosition_t1144714832 * L_1 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_0, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 4));
		Callback_t2100910411 * L_3 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		UITweener_AddOnFinished_m3855136488(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdatePasswordNav::UIClosing()
extern "C"  void UpdatePasswordNav_UIClosing_m1338946087 (UpdatePasswordNav_t2102371893 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_UIclosing_6();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_UIclosing_6((bool)0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void UpdatePasswordNav::ScreenLoad(NavigationDirection,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const uint32_t UpdatePasswordNav_ScreenLoad_m4033340344_MetadataUsageId;
extern "C"  void UpdatePasswordNav_ScreenLoad_m4033340344 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdatePasswordNav_ScreenLoad_m4033340344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___direction0;
		if (L_0)
		{
			goto IL_006f;
		}
	}
	{
		UIInput_t860674234 * L_1 = __this->get_currentPassword_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_1);
		UIInput_set_value_m430904237(L_1, L_2, /*hidden argument*/NULL);
		UIInput_t860674234 * L_3 = __this->get_newPassword_4();
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_3);
		UIInput_set_value_m430904237(L_3, L_4, /*hidden argument*/NULL);
		UIInput_t860674234 * L_5 = __this->get_confirmPassword_5();
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_5);
		UIInput_set_value_m430904237(L_5, L_6, /*hidden argument*/NULL);
		__this->set_UIclosing_6((bool)0);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		TweenPosition_t1144714832 * L_9 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_8, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_9);
		Behaviour_set_enabled_m1796096907(L_9, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		TweenPosition_t1144714832 * L_11 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_10, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_11);
		UITweener_PlayForward_m3772341562(L_11, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_006f:
	{
		int32_t L_12 = ___direction0;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_00aa;
		}
	}
	{
		__this->set_UIclosing_6((bool)0);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		TweenPosition_t1144714832 * L_15 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_14, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_15);
		Behaviour_set_enabled_m1796096907(L_15, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		TweenPosition_t1144714832 * L_17 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_16, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_17);
		UITweener_PlayReverse_m4027828489(L_17, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UpdatePasswordNav::ScreenUnload(NavigationDirection)
extern Il2CppClass* MonoSingleton_1_t2332610177_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3872251226_MethodInfo_var;
extern const uint32_t UpdatePasswordNav_ScreenUnload_m2330175024_MetadataUsageId;
extern "C"  void UpdatePasswordNav_ScreenUnload_m2330175024 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdatePasswordNav_ScreenUnload_m2330175024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___direction0;
		if (L_0)
		{
			goto IL_0067;
		}
	}
	{
		__this->set_UIclosing_6((bool)1);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		TweenPosition_t1144714832 * L_2 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_1, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2332610177_il2cpp_TypeInfo_var);
		SettingsNavigationController_t2581944457 * L_3 = MonoSingleton_1_get_Instance_m3872251226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3872251226_MethodInfo_var);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = L_3->get_activePos_27();
		NullCheck(L_2);
		L_2->set_from_20(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		TweenPosition_t1144714832 * L_6 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_5, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		SettingsNavigationController_t2581944457 * L_7 = MonoSingleton_1_get_Instance_m3872251226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3872251226_MethodInfo_var);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = L_7->get_leftInactivePos_28();
		NullCheck(L_6);
		L_6->set_to_21(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		TweenPosition_t1144714832 * L_10 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_9, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_10);
		Behaviour_set_enabled_m1796096907(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		TweenPosition_t1144714832 * L_12 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_11, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_12);
		UITweener_PlayForward_m3772341562(L_12, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_0067:
	{
		int32_t L_13 = ___direction0;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_00f3;
		}
	}
	{
		__this->set_UIclosing_6((bool)1);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_localPosition_m2533925116(L_15, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		TweenPosition_t1144714832 * L_18 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_17, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = L_18->get_from_20();
		bool L_20 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e3;
		}
	}
	{
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		TweenPosition_t1144714832 * L_22 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_21, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2332610177_il2cpp_TypeInfo_var);
		SettingsNavigationController_t2581944457 * L_23 = MonoSingleton_1_get_Instance_m3872251226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3872251226_MethodInfo_var);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = L_23->get_rightInactivePos_29();
		NullCheck(L_22);
		L_22->set_from_20(L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		TweenPosition_t1144714832 * L_26 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_25, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		SettingsNavigationController_t2581944457 * L_27 = MonoSingleton_1_get_Instance_m3872251226(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3872251226_MethodInfo_var);
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = L_27->get_activePos_27();
		NullCheck(L_26);
		L_26->set_to_21(L_28);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		TweenPosition_t1144714832 * L_30 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_29, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_30);
		UITweener_ResetToBeginning_m1313982072(L_30, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		TweenPosition_t1144714832 * L_32 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_31, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_32);
		UITweener_PlayReverse_m4027828489(L_32, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Boolean UpdatePasswordNav::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool UpdatePasswordNav_ValidateScreenNavigation_m1804539695 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator UpdatePasswordNav::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern Il2CppClass* U3CSaveScreenContentU3Ec__Iterator0_t2254993109_il2cpp_TypeInfo_var;
extern const uint32_t UpdatePasswordNav_SaveScreenContent_m1455114760_MetadataUsageId;
extern "C"  Il2CppObject * UpdatePasswordNav_SaveScreenContent_m1455114760 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdatePasswordNav_SaveScreenContent_m1455114760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * V_0 = NULL;
	{
		U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * L_0 = (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 *)il2cpp_codegen_object_new(U3CSaveScreenContentU3Ec__Iterator0_t2254993109_il2cpp_TypeInfo_var);
		U3CSaveScreenContentU3Ec__Iterator0__ctor_m2584483732(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * L_1 = V_0;
		Action_1_t3627374100 * L_2 = ___savedAction1;
		NullCheck(L_1);
		L_1->set_savedAction_0(L_2);
		U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * L_4 = V_0;
		return L_4;
	}
}
// System.Void UpdatePasswordNav/<SaveScreenContent>c__Iterator0::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0__ctor_m2584483732 (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UpdatePasswordNav/<SaveScreenContent>c__Iterator0::MoveNext()
extern Il2CppClass* U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m2689160153_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1687053610;
extern Il2CppCodeGenString* _stringLiteral2257274910;
extern Il2CppCodeGenString* _stringLiteral1772211161;
extern Il2CppCodeGenString* _stringLiteral1559763682;
extern Il2CppCodeGenString* _stringLiteral111569229;
extern const uint32_t U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m4281538224_MetadataUsageId;
extern "C"  bool U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m4281538224 (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m4281538224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0264;
			}
		}
	}
	{
		goto IL_026b;
	}

IL_0021:
	{
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_2 = (U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 *)il2cpp_codegen_object_new(U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170_il2cpp_TypeInfo_var);
		U3CSaveScreenContentU3Ec__AnonStorey1__ctor_m1521641543(L_2, /*hidden argument*/NULL);
		__this->set_U24locvar0_5(L_2);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_3 = __this->get_U24locvar0_5();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__refU240_1(__this);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_4 = __this->get_U24locvar0_5();
		Action_1_t3627374100 * L_5 = __this->get_savedAction_0();
		NullCheck(L_4);
		L_4->set_savedAction_0(L_5);
		UpdatePasswordNav_t2102371893 * L_6 = __this->get_U24this_1();
		NullCheck(L_6);
		UIInput_t860674234 * L_7 = L_6->get_currentPassword_3();
		NullCheck(L_7);
		String_t* L_8 = UIInput_get_value_m2016753770(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_10 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_021a;
		}
	}
	{
		UpdatePasswordNav_t2102371893 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		UIInput_t860674234 * L_12 = L_11->get_newPassword_4();
		NullCheck(L_12);
		String_t* L_13 = UIInput_get_value_m2016753770(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_15 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_01ea;
		}
	}
	{
		UpdatePasswordNav_t2102371893 * L_16 = __this->get_U24this_1();
		NullCheck(L_16);
		UIInput_t860674234 * L_17 = L_16->get_newPassword_4();
		NullCheck(L_17);
		String_t* L_18 = UIInput_get_value_m2016753770(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		CharU5BU5D_t1328083999* L_19 = String_ToCharArray_m870309954(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length))))) < ((int32_t)8)))
		{
			goto IL_01ba;
		}
	}
	{
		UpdatePasswordNav_t2102371893 * L_20 = __this->get_U24this_1();
		NullCheck(L_20);
		UIInput_t860674234 * L_21 = L_20->get_currentPassword_3();
		NullCheck(L_21);
		String_t* L_22 = UIInput_get_value_m2016753770(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		CharU5BU5D_t1328083999* L_23 = String_ToCharArray_m870309954(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))))) < ((int32_t)8)))
		{
			goto IL_01ba;
		}
	}
	{
		UpdatePasswordNav_t2102371893 * L_24 = __this->get_U24this_1();
		NullCheck(L_24);
		UIInput_t860674234 * L_25 = L_24->get_confirmPassword_5();
		NullCheck(L_25);
		String_t* L_26 = UIInput_get_value_m2016753770(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_28 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_018a;
		}
	}
	{
		UpdatePasswordNav_t2102371893 * L_29 = __this->get_U24this_1();
		NullCheck(L_29);
		UIInput_t860674234 * L_30 = L_29->get_confirmPassword_5();
		NullCheck(L_30);
		String_t* L_31 = UIInput_get_value_m2016753770(L_30, /*hidden argument*/NULL);
		UpdatePasswordNav_t2102371893 * L_32 = __this->get_U24this_1();
		NullCheck(L_32);
		UIInput_t860674234 * L_33 = L_32->get_newPassword_4();
		NullCheck(L_33);
		String_t* L_34 = UIInput_get_value_m2016753770(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_31, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_015a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_36 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		UpdatePasswordNav_t2102371893 * L_37 = __this->get_U24this_1();
		NullCheck(L_37);
		UIInput_t860674234 * L_38 = L_37->get_currentPassword_3();
		NullCheck(L_38);
		String_t* L_39 = UIInput_get_value_m2016753770(L_38, /*hidden argument*/NULL);
		UpdatePasswordNav_t2102371893 * L_40 = __this->get_U24this_1();
		NullCheck(L_40);
		UIInput_t860674234 * L_41 = L_40->get_newPassword_4();
		NullCheck(L_41);
		String_t* L_42 = UIInput_get_value_m2016753770(L_41, /*hidden argument*/NULL);
		UpdatePasswordNav_t2102371893 * L_43 = __this->get_U24this_1();
		NullCheck(L_43);
		UIInput_t860674234 * L_44 = L_43->get_confirmPassword_5();
		NullCheck(L_44);
		String_t* L_45 = UIInput_get_value_m2016753770(L_44, /*hidden argument*/NULL);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_46 = __this->get_U24locvar0_5();
		IntPtr_t L_47;
		L_47.set_m_value_0((void*)(void*)U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m2689160153_MethodInfo_var);
		Action_3_t3681841185 * L_48 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_48, L_46, L_47, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		NullCheck(L_36);
		ChatmageddonServer_UpdateUserPassword_m3075323563(L_36, L_39, L_42, L_45, L_48, /*hidden argument*/NULL);
		goto IL_0185;
	}

IL_015a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_49 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Nullable_1__ctor_m1978009879(&L_50, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_49);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_49, _stringLiteral1687053610, L_50, (bool)0, /*hidden argument*/NULL);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_51 = __this->get_U24locvar0_5();
		NullCheck(L_51);
		Action_1_t3627374100 * L_52 = L_51->get_savedAction_0();
		NullCheck(L_52);
		Action_1_Invoke_m3662000152(L_52, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0185:
	{
		goto IL_01b5;
	}

IL_018a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_53 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Nullable_1__ctor_m1978009879(&L_54, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_53);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_53, _stringLiteral2257274910, L_54, (bool)0, /*hidden argument*/NULL);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_55 = __this->get_U24locvar0_5();
		NullCheck(L_55);
		Action_1_t3627374100 * L_56 = L_55->get_savedAction_0();
		NullCheck(L_56);
		Action_1_Invoke_m3662000152(L_56, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_01b5:
	{
		goto IL_01e5;
	}

IL_01ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_57 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Nullable_1__ctor_m1978009879(&L_58, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_57);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_57, _stringLiteral1772211161, L_58, (bool)0, /*hidden argument*/NULL);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_59 = __this->get_U24locvar0_5();
		NullCheck(L_59);
		Action_1_t3627374100 * L_60 = L_59->get_savedAction_0();
		NullCheck(L_60);
		Action_1_Invoke_m3662000152(L_60, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_01e5:
	{
		goto IL_0215;
	}

IL_01ea:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_61 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Nullable_1__ctor_m1978009879(&L_62, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_61);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_61, _stringLiteral1559763682, L_62, (bool)0, /*hidden argument*/NULL);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_63 = __this->get_U24locvar0_5();
		NullCheck(L_63);
		Action_1_t3627374100 * L_64 = L_63->get_savedAction_0();
		NullCheck(L_64);
		Action_1_Invoke_m3662000152(L_64, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0215:
	{
		goto IL_0245;
	}

IL_021a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_65 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Nullable_1__ctor_m1978009879(&L_66, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_65);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_65, _stringLiteral111569229, L_66, (bool)0, /*hidden argument*/NULL);
		U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * L_67 = __this->get_U24locvar0_5();
		NullCheck(L_67);
		Action_1_t3627374100 * L_68 = L_67->get_savedAction_0();
		NullCheck(L_68);
		Action_1_Invoke_m3662000152(L_68, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0245:
	{
		WaitForFixedUpdate_t3968615785 * L_69 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_69, /*hidden argument*/NULL);
		__this->set_U24current_2(L_69);
		bool L_70 = __this->get_U24disposing_3();
		if (L_70)
		{
			goto IL_025f;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_025f:
	{
		goto IL_026d;
	}

IL_0264:
	{
		__this->set_U24PC_4((-1));
	}

IL_026b:
	{
		return (bool)0;
	}

IL_026d:
	{
		return (bool)1;
	}
}
// System.Object UpdatePasswordNav/<SaveScreenContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m697485606 (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object UpdatePasswordNav/<SaveScreenContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1744268494 (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void UpdatePasswordNav/<SaveScreenContent>c__Iterator0::Dispose()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Dispose_m4159167551 (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void UpdatePasswordNav/<SaveScreenContent>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSaveScreenContentU3Ec__Iterator0_Reset_m1585018593_MetadataUsageId;
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Reset_m1585018593 (U3CSaveScreenContentU3Ec__Iterator0_t2254993109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSaveScreenContentU3Ec__Iterator0_Reset_m1585018593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UpdatePasswordNav/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__AnonStorey1__ctor_m1521641543 (U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdatePasswordNav/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t752959214_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral708252994;
extern Il2CppCodeGenString* _stringLiteral1797641543;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern Il2CppCodeGenString* _stringLiteral707468138;
extern Il2CppCodeGenString* _stringLiteral919483285;
extern const uint32_t U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m2689160153_MetadataUsageId;
extern "C"  void U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m2689160153 (U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m2689160153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Hashtable_t909839986 * V_1 = NULL;
	Hashtable_t909839986 * V_2 = NULL;
	ArrayList_t4252133567 * V_3 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_1 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral708252994, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t752959214_il2cpp_TypeInfo_var);
		ToastManager_t1002293494 * L_2 = MonoSingleton_1_get_Instance_m3921578081(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3921578081_MethodInfo_var);
		NullCheck(L_2);
		ToastManager_OpenSmallToast_m805283205(L_2, _stringLiteral1797641543, /*hidden argument*/NULL);
		Action_1_t3627374100 * L_3 = __this->get_savedAction_0();
		NullCheck(L_3);
		Action_1_Invoke_m3662000152(L_3, (bool)1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		goto IL_00dd;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_4 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_5 = ___errorCode2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_6;
		String_t* L_7 = ___errorCode2;
		Il2CppObject * L_8 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = ((Hashtable_t909839986 *)CastclassClass(L_8, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_9 = V_1;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_9, _stringLiteral359429027);
		if (!L_10)
		{
			goto IL_00ab;
		}
	}
	{
		Hashtable_t909839986 * L_11 = V_1;
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_11, _stringLiteral707468138);
		V_2 = ((Hashtable_t909839986 *)CastclassClass(L_12, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_13 = V_2;
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_13, _stringLiteral919483285);
		if (!L_14)
		{
			goto IL_00ab;
		}
	}
	{
		Hashtable_t909839986 * L_15 = V_2;
		NullCheck(L_15);
		Il2CppObject * L_16 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_15, _stringLiteral919483285);
		V_3 = ((ArrayList_t4252133567 *)CastclassClass(L_16, ArrayList_t4252133567_il2cpp_TypeInfo_var));
		ArrayList_t4252133567 * L_17 = V_3;
		NullCheck(L_17);
		Il2CppObject * L_18 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_17, 0);
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		V_0 = L_19;
	}

IL_00ab:
	{
		String_t* L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_22 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00d1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_23 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		String_t* L_24 = V_0;
		Nullable_1_t339576247  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Nullable_1__ctor_m1978009879(&L_25, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_23);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_23, L_24, L_25, (bool)0, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		Action_1_t3627374100 * L_26 = __this->get_savedAction_0();
		NullCheck(L_26);
		Action_1_Invoke_m3662000152(L_26, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_00dd:
	{
		return;
	}
}
// System.Void UpdatePasswordUIScaler::.ctor()
extern "C"  void UpdatePasswordUIScaler__ctor_m3075515183 (UpdatePasswordUIScaler_t4097640474 * __this, const MethodInfo* method)
{
	{
		ChatmageddonUIScaler__ctor_m1032172578(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdatePasswordUIScaler::GlobalUIScale()
extern "C"  void UpdatePasswordUIScaler_GlobalUIScale_m540768084 (UpdatePasswordUIScaler_t4097640474 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		float L_0 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		V_0 = ((-((float)((float)L_0*(float)(0.4375f)))));
		UILabel_t1795115428 * L_1 = __this->get_currentPasswordLabel_14();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(L_1, /*hidden argument*/NULL);
		float L_3 = V_0;
		UILabel_t1795115428 * L_4 = __this->get_currentPasswordLabel_14();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localPosition_m2533925116(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		UILabel_t1795115428 * L_8 = __this->get_currentPasswordLabel_14();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_localPosition_m2533925116(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_z_3();
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2638739322(&L_12, L_3, L_7, L_11, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localPosition_m1026930133(L_2, L_12, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_13 = __this->get_currentPasswordCollider_15();
		float L_14 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_15 = __this->get_currentPasswordCollider_15();
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = BoxCollider_get_size_m201254358(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = (&V_3)->get_y_2();
		BoxCollider_t22920061 * L_18 = __this->get_currentPasswordCollider_15();
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = BoxCollider_get_size_m201254358(L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		float L_20 = (&V_4)->get_z_3();
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, L_14, L_17, L_20, /*hidden argument*/NULL);
		NullCheck(L_13);
		BoxCollider_set_size_m4101048759(L_13, L_21, /*hidden argument*/NULL);
		UISprite_t603616735 * L_22 = __this->get_currentPasswordDivider_16();
		float L_23 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_22);
		UIWidget_set_width_m1785998957(L_22, (((int32_t)((int32_t)L_23))), /*hidden argument*/NULL);
		UILabel_t1795115428 * L_24 = __this->get_newPasswordLabel_17();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(L_24, /*hidden argument*/NULL);
		float L_26 = V_0;
		UILabel_t1795115428 * L_27 = __this->get_newPasswordLabel_17();
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = Transform_get_localPosition_m2533925116(L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		float L_30 = (&V_5)->get_y_2();
		UILabel_t1795115428 * L_31 = __this->get_newPasswordLabel_17();
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_localPosition_m2533925116(L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		float L_34 = (&V_6)->get_z_3();
		Vector3_t2243707580  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m2638739322(&L_35, L_26, L_30, L_34, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localPosition_m1026930133(L_25, L_35, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_36 = __this->get_newPasswordCollider_18();
		float L_37 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_38 = __this->get_newPasswordCollider_18();
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = BoxCollider_get_size_m201254358(L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		float L_40 = (&V_7)->get_y_2();
		BoxCollider_t22920061 * L_41 = __this->get_newPasswordCollider_18();
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = BoxCollider_get_size_m201254358(L_41, /*hidden argument*/NULL);
		V_8 = L_42;
		float L_43 = (&V_8)->get_z_3();
		Vector3_t2243707580  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m2638739322(&L_44, L_37, L_40, L_43, /*hidden argument*/NULL);
		NullCheck(L_36);
		BoxCollider_set_size_m4101048759(L_36, L_44, /*hidden argument*/NULL);
		UISprite_t603616735 * L_45 = __this->get_newPasswordDivider_19();
		float L_46 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_45);
		UIWidget_set_width_m1785998957(L_45, (((int32_t)((int32_t)L_46))), /*hidden argument*/NULL);
		UILabel_t1795115428 * L_47 = __this->get_confirmPasswordLabel_20();
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = Component_get_transform_m2697483695(L_47, /*hidden argument*/NULL);
		float L_49 = V_0;
		UILabel_t1795115428 * L_50 = __this->get_confirmPasswordLabel_20();
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = Component_get_transform_m2697483695(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t2243707580  L_52 = Transform_get_localPosition_m2533925116(L_51, /*hidden argument*/NULL);
		V_9 = L_52;
		float L_53 = (&V_9)->get_y_2();
		UILabel_t1795115428 * L_54 = __this->get_confirmPasswordLabel_20();
		NullCheck(L_54);
		Transform_t3275118058 * L_55 = Component_get_transform_m2697483695(L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t2243707580  L_56 = Transform_get_localPosition_m2533925116(L_55, /*hidden argument*/NULL);
		V_10 = L_56;
		float L_57 = (&V_10)->get_z_3();
		Vector3_t2243707580  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Vector3__ctor_m2638739322(&L_58, L_49, L_53, L_57, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_localPosition_m1026930133(L_48, L_58, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_59 = __this->get_confirmPasswordCollider_21();
		float L_60 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_61 = __this->get_confirmPasswordCollider_21();
		NullCheck(L_61);
		Vector3_t2243707580  L_62 = BoxCollider_get_size_m201254358(L_61, /*hidden argument*/NULL);
		V_11 = L_62;
		float L_63 = (&V_11)->get_y_2();
		BoxCollider_t22920061 * L_64 = __this->get_confirmPasswordCollider_21();
		NullCheck(L_64);
		Vector3_t2243707580  L_65 = BoxCollider_get_size_m201254358(L_64, /*hidden argument*/NULL);
		V_12 = L_65;
		float L_66 = (&V_12)->get_z_3();
		Vector3_t2243707580  L_67;
		memset(&L_67, 0, sizeof(L_67));
		Vector3__ctor_m2638739322(&L_67, L_60, L_63, L_66, /*hidden argument*/NULL);
		NullCheck(L_59);
		BoxCollider_set_size_m4101048759(L_59, L_67, /*hidden argument*/NULL);
		UISprite_t603616735 * L_68 = __this->get_confirmPasswordDivider_22();
		float L_69 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_68);
		UIWidget_set_width_m1785998957(L_68, (((int32_t)((int32_t)L_69))), /*hidden argument*/NULL);
		BaseUIScaler_GlobalUIScale_m2469894115(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UploadStream::.ctor(System.String)
extern "C"  void UploadStream__ctor_m2255988742 (UploadStream_t108391441 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		UploadStream__ctor_m595060804(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		UploadStream_set_Name_m408726878(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UploadStream::.ctor()
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* AutoResetEvent_t15112628_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream__ctor_m595060804_MetadataUsageId;
extern "C"  void UploadStream__ctor_m595060804 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream__ctor_m595060804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		__this->set_ReadBuffer_2(L_0);
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_1, /*hidden argument*/NULL);
		__this->set_WriteBuffer_3(L_1);
		AutoResetEvent_t15112628 * L_2 = (AutoResetEvent_t15112628 *)il2cpp_codegen_object_new(AutoResetEvent_t15112628_il2cpp_TypeInfo_var);
		AutoResetEvent__ctor_m2634317352(L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_ARE_5(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_3, /*hidden argument*/NULL);
		__this->set_locker_6(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023(__this, /*hidden argument*/NULL);
		MemoryStream_t743994179 * L_4 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_4, /*hidden argument*/NULL);
		__this->set_ReadBuffer_2(L_4);
		MemoryStream_t743994179 * L_5 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_5, /*hidden argument*/NULL);
		__this->set_WriteBuffer_3(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		UploadStream_set_Name_m408726878(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.String UploadStream::get_Name()
extern "C"  String_t* UploadStream_get_Name_m1613933223 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UploadStream::set_Name(System.String)
extern "C"  void UploadStream_set_Name_m408726878 (UploadStream_t108391441 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Boolean UploadStream::get_IsReadBufferEmpty()
extern "C"  bool UploadStream_get_IsReadBufferEmpty_m4231920118 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_locker_6();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		MemoryStream_t743994179 * L_2 = __this->get_ReadBuffer_2();
		NullCheck(L_2);
		int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_2);
		MemoryStream_t743994179 * L_4 = __this->get_ReadBuffer_2();
		NullCheck(L_4);
		int64_t L_5 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_4);
		V_1 = (bool)((((int64_t)L_3) == ((int64_t)L_5))? 1 : 0);
		IL2CPP_LEAVE(0x32, FINALLY_002b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0032:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Int32 UploadStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* HTTPManager_t2983460817_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t2372156491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral836469959;
extern Il2CppCodeGenString* _stringLiteral4073970604;
extern const uint32_t UploadStream_Read_m3665688795_MetadataUsageId;
extern "C"  int32_t UploadStream_Read_m3665688795 (UploadStream_t108391441 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_Read_m3665688795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_noMoreData_4();
		if (!L_0)
		{
			goto IL_0079;
		}
	}
	{
		MemoryStream_t743994179 * L_1 = __this->get_ReadBuffer_2();
		NullCheck(L_1);
		int64_t L_2 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_1);
		MemoryStream_t743994179 * L_3 = __this->get_ReadBuffer_2();
		NullCheck(L_3);
		int64_t L_4 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_3);
		if ((!(((uint64_t)L_2) == ((uint64_t)L_4))))
		{
			goto IL_006a;
		}
	}
	{
		MemoryStream_t743994179 * L_5 = __this->get_WriteBuffer_3();
		NullCheck(L_5);
		int64_t L_6 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_5);
		if ((((int64_t)L_6) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0044;
		}
	}
	{
		UploadStream_SwitchBuffers_m3503612345(__this, /*hidden argument*/NULL);
		goto IL_0065;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HTTPManager_t2983460817_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = HTTPManager_get_Logger_m2103818269(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = UploadStream_get_Name_m1613933223(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4073970604, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(13 /* System.Void BestHTTP.Logger.ILogger::Information(System.String,System.String) */, ILogger_t2372156491_il2cpp_TypeInfo_var, L_7, _stringLiteral836469959, L_9);
		return (-1);
	}

IL_0065:
	{
		goto IL_0079;
	}

IL_006a:
	{
		MemoryStream_t743994179 * L_10 = __this->get_ReadBuffer_2();
		ByteU5BU5D_t3397334013* L_11 = ___buffer0;
		int32_t L_12 = ___offset1;
		int32_t L_13 = ___count2;
		NullCheck(L_10);
		int32_t L_14 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, L_12, L_13);
		return L_14;
	}

IL_0079:
	{
		bool L_15 = UploadStream_get_IsReadBufferEmpty_m4231920118(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00cd;
		}
	}
	{
		AutoResetEvent_t15112628 * L_16 = __this->get_ARE_5();
		NullCheck(L_16);
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne() */, L_16);
		Il2CppObject * L_17 = __this->get_locker_6();
		V_0 = L_17;
		Il2CppObject * L_18 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_009d:
	try
	{ // begin try (depth: 1)
		{
			bool L_19 = UploadStream_get_IsReadBufferEmpty_m4231920118(__this, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_00c1;
			}
		}

IL_00a8:
		{
			MemoryStream_t743994179 * L_20 = __this->get_WriteBuffer_3();
			NullCheck(L_20);
			int64_t L_21 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_20);
			if ((((int64_t)L_21) <= ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_00c1;
			}
		}

IL_00ba:
		{
			UploadStream_SwitchBuffers_m3503612345(__this, /*hidden argument*/NULL);
		}

IL_00c1:
		{
			IL2CPP_LEAVE(0xCD, FINALLY_00c6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c6;
	}

FINALLY_00c6:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(198)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(198)
	{
		IL2CPP_JUMP_TBL(0xCD, IL_00cd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00cd:
	{
		V_1 = (-1);
		Il2CppObject * L_23 = __this->get_locker_6();
		V_2 = L_23;
		Il2CppObject * L_24 = V_2;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_00dc:
	try
	{ // begin try (depth: 1)
		MemoryStream_t743994179 * L_25 = __this->get_ReadBuffer_2();
		ByteU5BU5D_t3397334013* L_26 = ___buffer0;
		int32_t L_27 = ___offset1;
		int32_t L_28 = ___count2;
		NullCheck(L_25);
		int32_t L_29 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_25, L_26, L_27, L_28);
		V_1 = L_29;
		IL2CPP_LEAVE(0xF7, FINALLY_00f0);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00f0;
	}

FINALLY_00f0:
	{ // begin finally (depth: 1)
		Il2CppObject * L_30 = V_2;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(240)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(240)
	{
		IL2CPP_JUMP_TBL(0xF7, IL_00f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00f7:
	{
		int32_t L_31 = V_1;
		return L_31;
	}
}
// System.Void UploadStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2145767625;
extern const uint32_t UploadStream_Write_m1775372748_MetadataUsageId;
extern "C"  void UploadStream_Write_m1775372748 (UploadStream_t108391441 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_Write_m1775372748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_noMoreData_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral2145767625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = __this->get_locker_6();
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		MemoryStream_t743994179 * L_4 = __this->get_WriteBuffer_3();
		ByteU5BU5D_t3397334013* L_5 = ___buffer0;
		int32_t L_6 = ___offset1;
		int32_t L_7 = ___count2;
		NullCheck(L_4);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(22 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, L_6, L_7);
		UploadStream_SwitchBuffers_m3503612345(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x44, FINALLY_003d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0044:
	{
		AutoResetEvent_t15112628 * L_9 = __this->get_ARE_5();
		NullCheck(L_9);
		EventWaitHandle_Set_m2975776670(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UploadStream::Flush()
extern "C"  void UploadStream_Flush_m4117070910 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	{
		UploadStream_Finish_m2476831819(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UploadStream::Dispose(System.Boolean)
extern Il2CppClass* HTTPManager_t2983460817_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t2372156491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral836469959;
extern Il2CppCodeGenString* _stringLiteral3794159534;
extern const uint32_t UploadStream_Dispose_m3600771840_MetadataUsageId;
extern "C"  void UploadStream_Dispose_m3600771840 (UploadStream_t108391441 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_Dispose_m3600771840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HTTPManager_t2983460817_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = HTTPManager_get_Logger_m2103818269(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = UploadStream_get_Name_m1613933223(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3794159534, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(13 /* System.Void BestHTTP.Logger.ILogger::Information(System.String,System.String) */, ILogger_t2372156491_il2cpp_TypeInfo_var, L_1, _stringLiteral836469959, L_3);
		MemoryStream_t743994179 * L_4 = __this->get_ReadBuffer_2();
		NullCheck(L_4);
		Stream_Dispose_m2417657914(L_4, /*hidden argument*/NULL);
		__this->set_ReadBuffer_2((MemoryStream_t743994179 *)NULL);
		MemoryStream_t743994179 * L_5 = __this->get_WriteBuffer_3();
		NullCheck(L_5);
		Stream_Dispose_m2417657914(L_5, /*hidden argument*/NULL);
		__this->set_WriteBuffer_3((MemoryStream_t743994179 *)NULL);
		AutoResetEvent_t15112628 * L_6 = __this->get_ARE_5();
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(7 /* System.Void System.Threading.WaitHandle::Close() */, L_6);
		__this->set_ARE_5((AutoResetEvent_t15112628 *)NULL);
	}

IL_005b:
	{
		bool L_7 = ___disposing0;
		Stream_Dispose_m440254723(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UploadStream::Finish()
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* HTTPManager_t2983460817_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t2372156491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2145767625;
extern Il2CppCodeGenString* _stringLiteral836469959;
extern Il2CppCodeGenString* _stringLiteral2602990534;
extern const uint32_t UploadStream_Finish_m2476831819_MetadataUsageId;
extern "C"  void UploadStream_Finish_m2476831819 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_Finish_m2476831819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_noMoreData_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral2145767625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HTTPManager_t2983460817_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = HTTPManager_get_Logger_m2103818269(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = UploadStream_get_Name_m1613933223(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2602990534, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(13 /* System.Void BestHTTP.Logger.ILogger::Information(System.String,System.String) */, ILogger_t2372156491_il2cpp_TypeInfo_var, L_2, _stringLiteral836469959, L_4);
		__this->set_noMoreData_4((bool)1);
		AutoResetEvent_t15112628 * L_5 = __this->get_ARE_5();
		NullCheck(L_5);
		EventWaitHandle_Set_m2975776670(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UploadStream::SwitchBuffers()
extern "C"  bool UploadStream_SwitchBuffers_m3503612345 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	MemoryStream_t743994179 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_locker_6();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			MemoryStream_t743994179 * L_2 = __this->get_ReadBuffer_2();
			NullCheck(L_2);
			int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_2);
			MemoryStream_t743994179 * L_4 = __this->get_ReadBuffer_2();
			NullCheck(L_4);
			int64_t L_5 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_4);
			if ((!(((uint64_t)L_3) == ((uint64_t)L_5))))
			{
				goto IL_0065;
			}
		}

IL_0028:
		{
			MemoryStream_t743994179 * L_6 = __this->get_WriteBuffer_3();
			NullCheck(L_6);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_6, (((int64_t)((int64_t)0))), 0);
			MemoryStream_t743994179 * L_7 = __this->get_ReadBuffer_2();
			NullCheck(L_7);
			VirtActionInvoker1< int64_t >::Invoke(21 /* System.Void System.IO.Stream::SetLength(System.Int64) */, L_7, (((int64_t)((int64_t)0))));
			MemoryStream_t743994179 * L_8 = __this->get_WriteBuffer_3();
			V_1 = L_8;
			MemoryStream_t743994179 * L_9 = __this->get_ReadBuffer_2();
			__this->set_WriteBuffer_3(L_9);
			MemoryStream_t743994179 * L_10 = V_1;
			__this->set_ReadBuffer_2(L_10);
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x73, FINALLY_006a);
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0071:
	{
		return (bool)0;
	}

IL_0073:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
// System.Boolean UploadStream::get_CanRead()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_get_CanRead_m1779176443_MetadataUsageId;
extern "C"  bool UploadStream_get_CanRead_m1779176443 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_get_CanRead_m1779176443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UploadStream::get_CanSeek()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_get_CanSeek_m2915123445_MetadataUsageId;
extern "C"  bool UploadStream_get_CanSeek_m2915123445 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_get_CanSeek_m2915123445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UploadStream::get_CanWrite()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_get_CanWrite_m1262831716_MetadataUsageId;
extern "C"  bool UploadStream_get_CanWrite_m1262831716 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_get_CanWrite_m1262831716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 UploadStream::get_Length()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_get_Length_m142445272_MetadataUsageId;
extern "C"  int64_t UploadStream_get_Length_m142445272 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_get_Length_m142445272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 UploadStream::get_Position()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_get_Position_m2248893473_MetadataUsageId;
extern "C"  int64_t UploadStream_get_Position_m2248893473 (UploadStream_t108391441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_get_Position_m2248893473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UploadStream::set_Position(System.Int64)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_set_Position_m177322616_MetadataUsageId;
extern "C"  void UploadStream_set_Position_m177322616 (UploadStream_t108391441 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_set_Position_m177322616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 UploadStream::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_Seek_m3090062000_MetadataUsageId;
extern "C"  int64_t UploadStream_Seek_m3090062000 (UploadStream_t108391441 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_Seek_m3090062000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UploadStream::SetLength(System.Int64)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UploadStream_SetLength_m2237829008_MetadataUsageId;
extern "C"  void UploadStream_SetLength_m2237829008 (UploadStream_t108391441 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadStream_SetLength_m2237829008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void User::.ctor(System.Collections.Hashtable)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t User__ctor_m2613408474_MetadataUsageId;
extern "C"  void User__ctor_m2613408474 (User_t719925459 * __this, Hashtable_t909839986 * ___userHash0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User__ctor_m2613408474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_userName_0(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_fullName_1(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_firstName_2(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_secondName_3(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_email_4(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_ID_5(L_5);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_profileImageURL_6(L_6);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_rankName_7(L_7);
		__this->set_userAudience_12(2);
		__this->set_allowLocation_13((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_8 = ___userHash0;
		User_AddNewUserData_m1463459072(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void User::AddNewUserData(System.Collections.Hashtable)
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3833952254;
extern Il2CppCodeGenString* _stringLiteral1009384125;
extern Il2CppCodeGenString* _stringLiteral3334300974;
extern Il2CppCodeGenString* _stringLiteral3749952829;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral441031451;
extern Il2CppCodeGenString* _stringLiteral1838257019;
extern Il2CppCodeGenString* _stringLiteral3519098914;
extern Il2CppCodeGenString* _stringLiteral2446688449;
extern Il2CppCodeGenString* _stringLiteral2323915341;
extern Il2CppCodeGenString* _stringLiteral974613453;
extern const uint32_t User_AddNewUserData_m1463459072_MetadataUsageId;
extern "C"  void User_AddNewUserData_m1463459072 (User_t719925459 * __this, Hashtable_t909839986 * ___userHash0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_AddNewUserData_m1463459072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	Hashtable_t909839986 * V_1 = NULL;
	TimeSpan_t3430258949  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t693205669  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Hashtable_t909839986 * V_4 = NULL;
	ArrayList_t4252133567 * V_5 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_1 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_1);
		UserPayload_t146188169 * L_2 = ((BaseServer_1_t2790028406 *)L_1)->get_userPayload_5();
		NullCheck(L_2);
		String_t* L_3 = UserPayload_GetProfileThumbnailTag_m4004667390(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, L_3);
		if (!L_4)
		{
			goto IL_009c;
		}
	}
	{
		Hashtable_t909839986 * L_5 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_6 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_6);
		UserPayload_t146188169 * L_7 = ((BaseServer_1_t2790028406 *)L_6)->get_userPayload_5();
		NullCheck(L_7);
		String_t* L_8 = UserPayload_GetProfileThumbnailTag_m4004667390(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, L_8);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		__this->set_profileImageURL_6(L_10);
		String_t* L_11 = __this->get_profileImageURL_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_13 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_009c;
		}
	}
	{
		__this->set_loadingAvatar_11((bool)1);
		String_t* L_14 = __this->get_profileImageURL_6();
		NullCheck(L_14);
		bool L_15 = String_Contains_m4017059963(L_14, _stringLiteral3833952254, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0081;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_16 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_17 = __this->get_profileImageURL_6();
		NullCheck(L_16);
		VirtActionInvoker2< String_t*, User_t719925459 * >::Invoke(24 /* System.Void BaseServer`1<ChatmageddonServer>::LoadImageFromURL(System.String,User) */, L_16, L_17, __this);
		goto IL_009c;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_18 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		String_t* L_19 = __this->get_profileImageURL_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1009384125, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker2< String_t*, User_t719925459 * >::Invoke(24 /* System.Void BaseServer`1<ChatmageddonServer>::LoadImageFromURL(System.String,User) */, L_18, L_20, __this);
	}

IL_009c:
	{
		Hashtable_t909839986 * L_21 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_22 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_22);
		UserPayload_t146188169 * L_23 = ((BaseServer_1_t2790028406 *)L_22)->get_userPayload_5();
		NullCheck(L_23);
		String_t* L_24 = UserPayload_GetUserNameTag_m153107099(L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		Il2CppObject * L_25 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_21, L_24);
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		__this->set_userName_0(L_26);
		Hashtable_t909839986 * L_27 = ___userHash0;
		ChatmageddonServer_t594474938 * L_28 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_28);
		UserPayload_t146188169 * L_29 = ((BaseServer_1_t2790028406 *)L_28)->get_userPayload_5();
		NullCheck(L_29);
		String_t* L_30 = UserPayload_GetFullNameTag_m679247165(L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		Il2CppObject * L_31 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_27, L_30);
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_31);
		__this->set_fullName_1(L_32);
		Hashtable_t909839986 * L_33 = ___userHash0;
		ChatmageddonServer_t594474938 * L_34 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_34);
		UserPayload_t146188169 * L_35 = ((BaseServer_1_t2790028406 *)L_34)->get_userPayload_5();
		NullCheck(L_35);
		String_t* L_36 = UserPayload_GetFirstNameTag_m4133148532(L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		Il2CppObject * L_37 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_33, L_36);
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		__this->set_firstName_2(L_38);
		Hashtable_t909839986 * L_39 = ___userHash0;
		ChatmageddonServer_t594474938 * L_40 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_40);
		UserPayload_t146188169 * L_41 = ((BaseServer_1_t2790028406 *)L_40)->get_userPayload_5();
		NullCheck(L_41);
		String_t* L_42 = UserPayload_GetSecondNameTag_m3730413342(L_41, /*hidden argument*/NULL);
		NullCheck(L_39);
		Il2CppObject * L_43 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_39, L_42);
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_43);
		__this->set_secondName_3(L_44);
		Hashtable_t909839986 * L_45 = ___userHash0;
		ChatmageddonServer_t594474938 * L_46 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_46);
		UserPayload_t146188169 * L_47 = ((BaseServer_1_t2790028406 *)L_46)->get_userPayload_5();
		NullCheck(L_47);
		String_t* L_48 = UserPayload_GetEmailTag_m1715712457(L_47, /*hidden argument*/NULL);
		NullCheck(L_45);
		bool L_49 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_45, L_48);
		if (!L_49)
		{
			goto IL_0156;
		}
	}
	{
		Hashtable_t909839986 * L_50 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_51 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_51);
		UserPayload_t146188169 * L_52 = ((BaseServer_1_t2790028406 *)L_51)->get_userPayload_5();
		NullCheck(L_52);
		String_t* L_53 = UserPayload_GetEmailTag_m1715712457(L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		Il2CppObject * L_54 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_50, L_53);
		NullCheck(L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_54);
		__this->set_email_4(L_55);
	}

IL_0156:
	{
		Hashtable_t909839986 * L_56 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_57 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_57);
		UserPayload_t146188169 * L_58 = ((BaseServer_1_t2790028406 *)L_57)->get_userPayload_5();
		NullCheck(L_58);
		String_t* L_59 = UserPayload_GetIDTag_m2447284976(L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		Il2CppObject * L_60 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_56, L_59);
		NullCheck(L_60);
		String_t* L_61 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		__this->set_ID_5(L_61);
		Hashtable_t909839986 * L_62 = ___userHash0;
		NullCheck(L_62);
		bool L_63 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_62, _stringLiteral3334300974);
		if (!L_63)
		{
			goto IL_01a2;
		}
	}
	{
		Hashtable_t909839986 * L_64 = ___userHash0;
		NullCheck(L_64);
		Il2CppObject * L_65 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_64, _stringLiteral3334300974);
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		int32_t L_67 = User_GetAudienceFromString_m2135198796(__this, L_66, /*hidden argument*/NULL);
		__this->set_userAudience_12(L_67);
	}

IL_01a2:
	{
		Hashtable_t909839986 * L_68 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_69 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_69);
		UserPayload_t146188169 * L_70 = ((BaseServer_1_t2790028406 *)L_69)->get_userPayload_5();
		NullCheck(L_70);
		String_t* L_71 = UserPayload_GetGenderTag_m3290465964(L_70, /*hidden argument*/NULL);
		NullCheck(L_68);
		bool L_72 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_68, L_71);
		if (!L_72)
		{
			goto IL_01e2;
		}
	}
	{
		Hashtable_t909839986 * L_73 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_74 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_74);
		UserPayload_t146188169 * L_75 = ((BaseServer_1_t2790028406 *)L_74)->get_userPayload_5();
		NullCheck(L_75);
		String_t* L_76 = UserPayload_GetGenderTag_m3290465964(L_75, /*hidden argument*/NULL);
		NullCheck(L_73);
		Il2CppObject * L_77 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_73, L_76);
		NullCheck(L_77);
		String_t* L_78 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_77);
		int32_t L_79 = User_GetGenderFromString_m3873467884(__this, L_78, /*hidden argument*/NULL);
		__this->set_userGender_9(L_79);
	}

IL_01e2:
	{
		Hashtable_t909839986 * L_80 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_81 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_81);
		UserPayload_t146188169 * L_82 = ((BaseServer_1_t2790028406 *)L_81)->get_userPayload_5();
		NullCheck(L_82);
		String_t* L_83 = UserPayload_GetLevelTag_m1590126275(L_82, /*hidden argument*/NULL);
		NullCheck(L_80);
		bool L_84 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_80, L_83);
		if (!L_84)
		{
			goto IL_0295;
		}
	}
	{
		Hashtable_t909839986 * L_85 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_86 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_86);
		UserPayload_t146188169 * L_87 = ((BaseServer_1_t2790028406 *)L_86)->get_userPayload_5();
		NullCheck(L_87);
		String_t* L_88 = UserPayload_GetLevelTag_m1590126275(L_87, /*hidden argument*/NULL);
		NullCheck(L_85);
		Il2CppObject * L_89 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_85, L_88);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_89, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_90 = V_0;
		ChatmageddonServer_t594474938 * L_91 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_91);
		UserPayload_t146188169 * L_92 = ((BaseServer_1_t2790028406 *)L_91)->get_userPayload_5();
		NullCheck(L_92);
		String_t* L_93 = UserPayload_GetLevelTag_m1590126275(L_92, /*hidden argument*/NULL);
		NullCheck(L_90);
		bool L_94 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_90, L_93);
		if (!L_94)
		{
			goto IL_0256;
		}
	}
	{
		Hashtable_t909839986 * L_95 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_96 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_96);
		UserPayload_t146188169 * L_97 = ((BaseServer_1_t2790028406 *)L_96)->get_userPayload_5();
		NullCheck(L_97);
		String_t* L_98 = UserPayload_GetLevelTag_m1590126275(L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		Il2CppObject * L_99 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_95, L_98);
		NullCheck(L_99);
		String_t* L_100 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_99);
		int32_t L_101 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_100, /*hidden argument*/NULL);
		__this->set_rankLevel_8(L_101);
	}

IL_0256:
	{
		Hashtable_t909839986 * L_102 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_103 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_103);
		UserPayload_t146188169 * L_104 = ((BaseServer_1_t2790028406 *)L_103)->get_userPayload_5();
		NullCheck(L_104);
		String_t* L_105 = UserPayload_GetRankTag_m2881288159(L_104, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_106 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_102, L_105);
		if (!L_106)
		{
			goto IL_0295;
		}
	}
	{
		Hashtable_t909839986 * L_107 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_108 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_108);
		UserPayload_t146188169 * L_109 = ((BaseServer_1_t2790028406 *)L_108)->get_userPayload_5();
		NullCheck(L_109);
		String_t* L_110 = UserPayload_GetRankTag_m2881288159(L_109, /*hidden argument*/NULL);
		NullCheck(L_107);
		Il2CppObject * L_111 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_107, L_110);
		NullCheck(L_111);
		String_t* L_112 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_111);
		NullCheck(L_112);
		String_t* L_113 = String_ToUpper_m3715743312(L_112, /*hidden argument*/NULL);
		__this->set_rankName_7(L_113);
	}

IL_0295:
	{
		Hashtable_t909839986 * L_114 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_115 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_115);
		UserPayload_t146188169 * L_116 = ((BaseServer_1_t2790028406 *)L_115)->get_userPayload_5();
		NullCheck(L_116);
		String_t* L_117 = UserPayload_GetStealthModeTag_m3747900581(L_116, /*hidden argument*/NULL);
		NullCheck(L_114);
		bool L_118 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_114, L_117);
		if (!L_118)
		{
			goto IL_04b1;
		}
	}
	{
		Hashtable_t909839986 * L_119 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_120 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_120);
		UserPayload_t146188169 * L_121 = ((BaseServer_1_t2790028406 *)L_120)->get_userPayload_5();
		NullCheck(L_121);
		String_t* L_122 = UserPayload_GetStealthModeTag_m3747900581(L_121, /*hidden argument*/NULL);
		NullCheck(L_119);
		Il2CppObject * L_123 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_119, L_122);
		V_1 = ((Hashtable_t909839986 *)CastclassClass(L_123, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_124 = V_1;
		ChatmageddonServer_t594474938 * L_125 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_125);
		UserPayload_t146188169 * L_126 = ((BaseServer_1_t2790028406 *)L_125)->get_userPayload_5();
		NullCheck(L_126);
		String_t* L_127 = UserPayload_GetStealthModeActiveTag_m3885858145(L_126, /*hidden argument*/NULL);
		NullCheck(L_124);
		Il2CppObject * L_128 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_124, L_127);
		NullCheck(L_128);
		String_t* L_129 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_128);
		bool L_130 = User_GetboolFromString_m3228516247(__this, L_129, /*hidden argument*/NULL);
		__this->set_inStealth_18(L_130);
		Hashtable_t909839986 * L_131 = V_1;
		ChatmageddonServer_t594474938 * L_132 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_132);
		UserPayload_t146188169 * L_133 = ((BaseServer_1_t2790028406 *)L_132)->get_userPayload_5();
		NullCheck(L_133);
		String_t* L_134 = UserPayload_GetStealthModeHourTag_m3913217993(L_133, /*hidden argument*/NULL);
		NullCheck(L_131);
		Il2CppObject * L_135 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_131, L_134);
		NullCheck(L_135);
		String_t* L_136 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_135);
		int32_t* L_137 = __this->get_address_of_stealthStartHour_20();
		Int32_TryParse_m656840904(NULL /*static, unused*/, L_136, L_137, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_138 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t693205669  L_139 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_139;
		DateTime_t693205669  L_140 = DateTime_ToLocalTime_m1957689902((&V_3), /*hidden argument*/NULL);
		TimeSpan_t3430258949  L_141 = DateTime_op_Subtraction_m3246456251(NULL /*static, unused*/, L_138, L_140, /*hidden argument*/NULL);
		V_2 = L_141;
		int32_t L_142 = __this->get_stealthStartHour_20();
		double L_143 = TimeSpan_get_TotalHours_m2301166279((&V_2), /*hidden argument*/NULL);
		double L_144 = bankers_round(L_143);
		__this->set_stealthStartHour_20(((int32_t)((int32_t)L_142+(int32_t)((-(((int32_t)((int32_t)L_144))))))));
		int32_t L_145 = __this->get_stealthStartHour_20();
		if ((!(((uint32_t)L_145) == ((uint32_t)((int32_t)24)))))
		{
			goto IL_035d;
		}
	}
	{
		__this->set_stealthStartHour_20(0);
	}

IL_035d:
	{
		Hashtable_t909839986 * L_146 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_147 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_147);
		UserPayload_t146188169 * L_148 = ((BaseServer_1_t2790028406 *)L_147)->get_userPayload_5();
		NullCheck(L_148);
		String_t* L_149 = UserPayload_GetStealthModeMinTag_m682840799(L_148, /*hidden argument*/NULL);
		NullCheck(L_146);
		Il2CppObject * L_150 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_146, L_149);
		NullCheck(L_150);
		String_t* L_151 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_150);
		int32_t* L_152 = __this->get_address_of_stealthStartMin_21();
		Int32_TryParse_m656840904(NULL /*static, unused*/, L_151, L_152, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_153 = V_1;
		ChatmageddonServer_t594474938 * L_154 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_154);
		UserPayload_t146188169 * L_155 = ((BaseServer_1_t2790028406 *)L_154)->get_userPayload_5();
		NullCheck(L_155);
		String_t* L_156 = UserPayload_GetStealthModeDurationTag_m4026287879(L_155, /*hidden argument*/NULL);
		NullCheck(L_153);
		Il2CppObject * L_157 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_153, L_156);
		NullCheck(L_157);
		String_t* L_158 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_157);
		int32_t* L_159 = __this->get_address_of_stealthDuration_19();
		Int32_TryParse_m656840904(NULL /*static, unused*/, L_158, L_159, /*hidden argument*/NULL);
		User_GenerateStealthActivateTime_m934795268(__this, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_160 = V_1;
		NullCheck(L_160);
		bool L_161 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_160, _stringLiteral3749952829);
		if (!L_161)
		{
			goto IL_0430;
		}
	}
	{
		Hashtable_t909839986 * L_162 = V_1;
		NullCheck(L_162);
		Il2CppObject * L_163 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_162, _stringLiteral3749952829);
		if (!L_163)
		{
			goto IL_0430;
		}
	}
	{
		Hashtable_t909839986 * L_164 = V_1;
		NullCheck(L_164);
		Il2CppObject * L_165 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_164, _stringLiteral3749952829);
		NullCheck(L_165);
		String_t* L_166 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_165);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_167 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_166, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (!L_167)
		{
			goto IL_0430;
		}
	}
	{
		Hashtable_t909839986 * L_168 = V_1;
		NullCheck(L_168);
		Il2CppObject * L_169 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_168, _stringLiteral3749952829);
		NullCheck(L_169);
		String_t* L_170 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_169);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_171 = Convert_ToDateTime_m2228400765(NULL /*static, unused*/, L_170, /*hidden argument*/NULL);
		__this->set_deactivatesStealthAt_22(L_171);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_172 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		DateTime_t693205669  L_173 = __this->get_deactivatesStealthAt_22();
		NullCheck(L_172);
		DateTime_t693205669  L_174 = BaseServer_1_ApplyServerTimeOffset_m2903528104(L_172, L_173, /*hidden argument*/BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var);
		__this->set_deactivatesStealthAt_22(L_174);
		DateTime_t693205669 * L_175 = __this->get_address_of_deactivatesStealthAt_22();
		DateTime_t693205669  L_176 = DateTime_ToLocalTime_m1957689902(L_175, /*hidden argument*/NULL);
		__this->set_deactivatesStealthAt_22(L_176);
	}

IL_0430:
	{
		Hashtable_t909839986 * L_177 = V_1;
		NullCheck(L_177);
		bool L_178 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_177, _stringLiteral441031451);
		if (!L_178)
		{
			goto IL_04b1;
		}
	}
	{
		Hashtable_t909839986 * L_179 = V_1;
		NullCheck(L_179);
		Il2CppObject * L_180 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_179, _stringLiteral441031451);
		if (!L_180)
		{
			goto IL_04b1;
		}
	}
	{
		Hashtable_t909839986 * L_181 = V_1;
		NullCheck(L_181);
		Il2CppObject * L_182 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_181, _stringLiteral441031451);
		NullCheck(L_182);
		String_t* L_183 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_182);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_184 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_183, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (!L_184)
		{
			goto IL_04b1;
		}
	}
	{
		Hashtable_t909839986 * L_185 = V_1;
		NullCheck(L_185);
		Il2CppObject * L_186 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_185, _stringLiteral441031451);
		NullCheck(L_186);
		String_t* L_187 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_186);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_188 = Convert_ToDateTime_m2228400765(NULL /*static, unused*/, L_187, /*hidden argument*/NULL);
		__this->set_lastStealthReset_24(L_188);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_189 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		DateTime_t693205669  L_190 = __this->get_lastStealthReset_24();
		NullCheck(L_189);
		DateTime_t693205669  L_191 = BaseServer_1_ApplyServerTimeOffset_m2903528104(L_189, L_190, /*hidden argument*/BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var);
		__this->set_lastStealthReset_24(L_191);
		DateTime_t693205669 * L_192 = __this->get_address_of_lastStealthReset_24();
		DateTime_t693205669  L_193 = DateTime_ToLocalTime_m1957689902(L_192, /*hidden argument*/NULL);
		__this->set_lastStealthReset_24(L_193);
	}

IL_04b1:
	{
		Hashtable_t909839986 * L_194 = ___userHash0;
		User_UpdateKnockoutData_m3519987727(__this, L_194, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_195 = ___userHash0;
		NullCheck(L_195);
		bool L_196 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_195, _stringLiteral1838257019);
		if (!L_196)
		{
			goto IL_04e3;
		}
	}
	{
		Hashtable_t909839986 * L_197 = ___userHash0;
		NullCheck(L_197);
		Il2CppObject * L_198 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_197, _stringLiteral1838257019);
		NullCheck(L_198);
		String_t* L_199 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_198);
		int32_t L_200 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_199, /*hidden argument*/NULL);
		__this->set_dailyPoints_25(L_200);
	}

IL_04e3:
	{
		Hashtable_t909839986 * L_201 = ___userHash0;
		NullCheck(L_201);
		bool L_202 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_201, _stringLiteral3519098914);
		if (!L_202)
		{
			goto IL_050e;
		}
	}
	{
		Hashtable_t909839986 * L_203 = ___userHash0;
		NullCheck(L_203);
		Il2CppObject * L_204 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_203, _stringLiteral3519098914);
		NullCheck(L_204);
		String_t* L_205 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_204);
		int32_t L_206 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_205, /*hidden argument*/NULL);
		__this->set_totalPoints_26(L_206);
	}

IL_050e:
	{
		Hashtable_t909839986 * L_207 = ___userHash0;
		NullCheck(L_207);
		bool L_208 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_207, _stringLiteral2446688449);
		if (!L_208)
		{
			goto IL_0534;
		}
	}
	{
		Hashtable_t909839986 * L_209 = ___userHash0;
		NullCheck(L_209);
		Il2CppObject * L_210 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_209, _stringLiteral2446688449);
		NullCheck(L_210);
		String_t* L_211 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_210);
		__this->set_mobileHash_31(L_211);
	}

IL_0534:
	{
		Hashtable_t909839986 * L_212 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_213 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_213);
		UserPayload_t146188169 * L_214 = ((BaseServer_1_t2790028406 *)L_213)->get_userPayload_5();
		NullCheck(L_214);
		String_t* L_215 = UserPayload_GetDisplayLocationTag_m559639940(L_214, /*hidden argument*/NULL);
		NullCheck(L_212);
		bool L_216 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_212, L_215);
		if (!L_216)
		{
			goto IL_0574;
		}
	}
	{
		Hashtable_t909839986 * L_217 = ___userHash0;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_218 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		NullCheck(L_218);
		UserPayload_t146188169 * L_219 = ((BaseServer_1_t2790028406 *)L_218)->get_userPayload_5();
		NullCheck(L_219);
		String_t* L_220 = UserPayload_GetDisplayLocationTag_m559639940(L_219, /*hidden argument*/NULL);
		NullCheck(L_217);
		Il2CppObject * L_221 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_217, L_220);
		NullCheck(L_221);
		String_t* L_222 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_221);
		bool L_223 = User_GetboolFromString_m3228516247(__this, L_222, /*hidden argument*/NULL);
		__this->set_allowLocation_13(L_223);
	}

IL_0574:
	{
		Hashtable_t909839986 * L_224 = ___userHash0;
		NullCheck(L_224);
		bool L_225 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_224, _stringLiteral2323915341);
		if (!L_225)
		{
			goto IL_05fd;
		}
	}
	{
		Hashtable_t909839986 * L_226 = ___userHash0;
		NullCheck(L_226);
		Il2CppObject * L_227 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_226, _stringLiteral2323915341);
		NullCheck(L_227);
		String_t* L_228 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_227);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_229 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_228, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (!L_229)
		{
			goto IL_05fd;
		}
	}
	{
		Hashtable_t909839986 * L_230 = ___userHash0;
		NullCheck(L_230);
		Il2CppObject * L_231 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_230, _stringLiteral2323915341);
		V_4 = ((Hashtable_t909839986 *)CastclassClass(L_231, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_232 = V_4;
		NullCheck(L_232);
		Il2CppObject * L_233 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_232, _stringLiteral974613453);
		ArrayList_t4252133567 * L_234 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m194094802(L_234, ((ArrayList_t4252133567 *)CastclassClass(L_233, ArrayList_t4252133567_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_5 = L_234;
		ArrayList_t4252133567 * L_235 = V_5;
		NullCheck(L_235);
		Il2CppObject * L_236 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_235, 0);
		NullCheck(L_236);
		String_t* L_237 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_236);
		float L_238 = Single_Parse_m1861732734(NULL /*static, unused*/, L_237, /*hidden argument*/NULL);
		__this->set_latitude_14(L_238);
		ArrayList_t4252133567 * L_239 = V_5;
		NullCheck(L_239);
		Il2CppObject * L_240 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_239, 1);
		NullCheck(L_240);
		String_t* L_241 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_240);
		float L_242 = Single_Parse_m1861732734(NULL /*static, unused*/, L_241, /*hidden argument*/NULL);
		__this->set_longitude_15(L_242);
	}

IL_05fd:
	{
		return;
	}
}
// System.Void User::AddNewStatData(System.Collections.Hashtable)
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4151059802;
extern Il2CppCodeGenString* _stringLiteral3443206164;
extern Il2CppCodeGenString* _stringLiteral3452442362;
extern Il2CppCodeGenString* _stringLiteral525807276;
extern Il2CppCodeGenString* _stringLiteral2948038670;
extern Il2CppCodeGenString* _stringLiteral1357918456;
extern Il2CppCodeGenString* _stringLiteral2112322107;
extern Il2CppCodeGenString* _stringLiteral3645071682;
extern Il2CppCodeGenString* _stringLiteral762134816;
extern Il2CppCodeGenString* _stringLiteral2707554329;
extern Il2CppCodeGenString* _stringLiteral1239562208;
extern const uint32_t User_AddNewStatData_m3644275485_MetadataUsageId;
extern "C"  void User_AddNewStatData_m3644275485 (User_t719925459 * __this, Hashtable_t909839986 * ___statHash0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_AddNewStatData_m3644275485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	Hashtable_t909839986 * V_1 = NULL;
	Hashtable_t909839986 * V_2 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___statHash0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, _stringLiteral4151059802);
		if (!L_1)
		{
			goto IL_0072;
		}
	}
	{
		Hashtable_t909839986 * L_2 = ___statHash0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral4151059802);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_3, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, _stringLiteral3443206164);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		int32_t L_7 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_missilesFired_32(L_7);
		Hashtable_t909839986 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_8, _stringLiteral3452442362);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		int32_t L_11 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_attackDirectHits_34(L_11);
		Hashtable_t909839986 * L_12 = V_0;
		NullCheck(L_12);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_12, _stringLiteral525807276);
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		float L_15 = Single_Parse_m1861732734(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set_missilesSuccessRate_33(L_15);
	}

IL_0072:
	{
		Hashtable_t909839986 * L_16 = ___statHash0;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_16, _stringLiteral2948038670);
		if (!L_17)
		{
			goto IL_00e4;
		}
	}
	{
		Hashtable_t909839986 * L_18 = ___statHash0;
		NullCheck(L_18);
		Il2CppObject * L_19 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_18, _stringLiteral2948038670);
		V_1 = ((Hashtable_t909839986 *)CastclassClass(L_19, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_20 = V_1;
		NullCheck(L_20);
		Il2CppObject * L_21 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_20, _stringLiteral1357918456);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		int32_t L_23 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		__this->set_incomingAttacks_35(L_23);
		Hashtable_t909839986 * L_24 = V_1;
		NullCheck(L_24);
		Il2CppObject * L_25 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_24, _stringLiteral3452442362);
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		int32_t L_27 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		__this->set_hitBy_36(L_27);
		Hashtable_t909839986 * L_28 = V_1;
		NullCheck(L_28);
		Il2CppObject * L_29 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_28, _stringLiteral2112322107);
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		float L_31 = Single_Parse_m1861732734(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		__this->set_missilesDefenceRate_37(L_31);
	}

IL_00e4:
	{
		Hashtable_t909839986 * L_32 = ___statHash0;
		NullCheck(L_32);
		bool L_33 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_32, _stringLiteral3645071682);
		if (!L_33)
		{
			goto IL_0156;
		}
	}
	{
		Hashtable_t909839986 * L_34 = ___statHash0;
		NullCheck(L_34);
		Il2CppObject * L_35 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_34, _stringLiteral3645071682);
		V_2 = ((Hashtable_t909839986 *)CastclassClass(L_35, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_36 = V_2;
		NullCheck(L_36);
		Il2CppObject * L_37 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_36, _stringLiteral762134816);
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		int32_t L_39 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		__this->set_minesLaid_38(L_39);
		Hashtable_t909839986 * L_40 = V_2;
		NullCheck(L_40);
		Il2CppObject * L_41 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_40, _stringLiteral2707554329);
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_41);
		int32_t L_43 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		__this->set_minesTriggerred_39(L_43);
		Hashtable_t909839986 * L_44 = V_2;
		NullCheck(L_44);
		Il2CppObject * L_45 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_44, _stringLiteral1239562208);
		NullCheck(L_45);
		String_t* L_46 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		int32_t L_47 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		__this->set_minesDefused_40(L_47);
	}

IL_0156:
	{
		return;
	}
}
// System.Void User::UpdateKnockoutData(System.Collections.Hashtable)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2918998460;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral4149376527;
extern Il2CppCodeGenString* _stringLiteral2845190196;
extern Il2CppCodeGenString* _stringLiteral524867311;
extern Il2CppCodeGenString* _stringLiteral339800794;
extern const uint32_t User_UpdateKnockoutData_m3519987727_MetadataUsageId;
extern "C"  void User_UpdateKnockoutData_m3519987727 (User_t719925459 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_UpdateKnockoutData_m3519987727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	{
		Hashtable_t909839986 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_0, _stringLiteral2918998460);
		if (!L_1)
		{
			goto IL_0088;
		}
	}
	{
		Hashtable_t909839986 * L_2 = ___data0;
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral2918998460);
		if (!L_3)
		{
			goto IL_0088;
		}
	}
	{
		Hashtable_t909839986 * L_4 = ___data0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, _stringLiteral2918998460);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_6, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0088;
		}
	}
	{
		Hashtable_t909839986 * L_8 = ___data0;
		NullCheck(L_8);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_8, _stringLiteral2918998460);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_11 = Convert_ToDateTime_m2228400765(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_knockedOutUntil_28(L_11);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_12 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		DateTime_t693205669  L_13 = __this->get_knockedOutUntil_28();
		NullCheck(L_12);
		DateTime_t693205669  L_14 = BaseServer_1_ApplyServerTimeOffset_m2903528104(L_12, L_13, /*hidden argument*/BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var);
		__this->set_knockedOutUntil_28(L_14);
		DateTime_t693205669 * L_15 = __this->get_address_of_knockedOutUntil_28();
		DateTime_t693205669  L_16 = DateTime_ToLocalTime_m1957689902(L_15, /*hidden argument*/NULL);
		__this->set_knockedOutUntil_28(L_16);
		User_SetUserKnockoutStatus_m2409352638(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0088:
	{
		Hashtable_t909839986 * L_17 = ___data0;
		NullCheck(L_17);
		bool L_18 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_17, _stringLiteral4149376527);
		if (!L_18)
		{
			goto IL_0124;
		}
	}
	{
		Hashtable_t909839986 * L_19 = ___data0;
		NullCheck(L_19);
		Il2CppObject * L_20 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_19, _stringLiteral4149376527);
		if (!L_20)
		{
			goto IL_0124;
		}
	}
	{
		Hashtable_t909839986 * L_21 = ___data0;
		NullCheck(L_21);
		Il2CppObject * L_22 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_21, _stringLiteral4149376527);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_23, _stringLiteral2845190196, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0124;
		}
	}
	{
		Hashtable_t909839986 * L_25 = ___data0;
		NullCheck(L_25);
		Il2CppObject * L_26 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_25, _stringLiteral4149376527);
		V_0 = ((Hashtable_t909839986 *)CastclassClass(L_26, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_27 = V_0;
		NullCheck(L_27);
		bool L_28 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_27, _stringLiteral524867311);
		if (!L_28)
		{
			goto IL_00fe;
		}
	}
	{
		Hashtable_t909839986 * L_29 = V_0;
		NullCheck(L_29);
		Il2CppObject * L_30 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_29, _stringLiteral524867311);
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		__this->set_knockedOutByName_30(L_31);
	}

IL_00fe:
	{
		Hashtable_t909839986 * L_32 = V_0;
		NullCheck(L_32);
		bool L_33 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_32, _stringLiteral339800794);
		if (!L_33)
		{
			goto IL_0124;
		}
	}
	{
		Hashtable_t909839986 * L_34 = V_0;
		NullCheck(L_34);
		Il2CppObject * L_35 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_34, _stringLiteral339800794);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		__this->set_knockedOutByID_29(L_36);
	}

IL_0124:
	{
		return;
	}
}
// System.Boolean User::GetboolFromString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029325;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral3323264318;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral2609910045;
extern const uint32_t User_GetboolFromString_m3228516247_MetadataUsageId;
extern "C"  bool User_GetboolFromString_m3228516247 (User_t719925459 * __this, String_t* ___String0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_GetboolFromString_m3228516247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral372029325, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_2 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral3323263070, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_4 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral3323264318, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0032;
		}
	}

IL_0030:
	{
		return (bool)1;
	}

IL_0032:
	{
		String_t* L_6 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral372029326, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_8 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral2609877245, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_10 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral2609910045, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0064;
		}
	}

IL_0062:
	{
		return (bool)0;
	}

IL_0064:
	{
		return (bool)0;
	}
}
// Gender User::GetGenderFromString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2328218793;
extern Il2CppCodeGenString* _stringLiteral183590532;
extern Il2CppCodeGenString* _stringLiteral461081110;
extern const uint32_t User_GetGenderFromString_m3873467884_MetadataUsageId;
extern "C"  int32_t User_GetGenderFromString_m3873467884 (User_t719925459 * __this, String_t* ___String0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_GetGenderFromString_m3873467884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral2328218793, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral183590532, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (int32_t)(0);
	}

IL_0022:
	{
		String_t* L_4 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral461081110, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0034:
	{
		return (int32_t)(0);
	}
}
// Audience User::GetAudienceFromString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3707847497;
extern Il2CppCodeGenString* _stringLiteral1714962659;
extern Il2CppCodeGenString* _stringLiteral3989186809;
extern const uint32_t User_GetAudienceFromString_m2135198796_MetadataUsageId;
extern "C"  int32_t User_GetAudienceFromString_m2135198796 (User_t719925459 * __this, String_t* ___String0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_GetAudienceFromString_m2135198796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral3707847497, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0012:
	{
		String_t* L_2 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1714962659, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0024:
	{
		String_t* L_4 = ___String0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral3989186809, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0036:
	{
		return (int32_t)(2);
	}
}
// System.Void User::SetUsername(System.String)
extern "C"  void User_SetUsername_m3499064600 (User_t719925459 * __this, String_t* ___usernameToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___usernameToSet0;
		__this->set_userName_0(L_0);
		return;
	}
}
// System.String User::GetUsername()
extern "C"  String_t* User_GetUsername_m3962550579 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_userName_0();
		return L_0;
	}
}
// System.Void User::SetFirstName(System.String)
extern "C"  void User_SetFirstName_m1167106307 (User_t719925459 * __this, String_t* ___firstNameToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___firstNameToSet0;
		__this->set_firstName_2(L_0);
		return;
	}
}
// System.String User::GetFirstName()
extern "C"  String_t* User_GetFirstName_m2758212472 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_firstName_2();
		return L_0;
	}
}
// System.Void User::SetSecondName(System.String)
extern "C"  void User_SetSecondName_m3164596413 (User_t719925459 * __this, String_t* ___secondNameToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___secondNameToSet0;
		__this->set_secondName_3(L_0);
		return;
	}
}
// System.String User::GetSecondName()
extern "C"  String_t* User_GetSecondName_m2355537218 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_secondName_3();
		return L_0;
	}
}
// System.Void User::SetFullName(System.String)
extern "C"  void User_SetFullName_m1692152578 (User_t719925459 * __this, String_t* ___fullNameToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___fullNameToSet0;
		__this->set_fullName_1(L_0);
		return;
	}
}
// System.String User::GetFullName()
extern "C"  String_t* User_GetFullName_m1642387097 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_fullName_1();
		return L_0;
	}
}
// System.Void User::SetEmail(System.String)
extern "C"  void User_SetEmail_m2011861658 (User_t719925459 * __this, String_t* ___emailToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___emailToSet0;
		__this->set_email_4(L_0);
		return;
	}
}
// System.String User::GetEmail()
extern "C"  String_t* User_GetEmail_m4206096257 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_email_4();
		return L_0;
	}
}
// System.Void User::SetID(System.String)
extern "C"  void User_SetID_m1856835131 (User_t719925459 * __this, String_t* ___IDToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___IDToSet0;
		__this->set_ID_5(L_0);
		return;
	}
}
// System.String User::GetID()
extern "C"  String_t* User_GetID_m1606899520 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_ID_5();
		return L_0;
	}
}
// System.Void User::SetGender(Gender)
extern "C"  void User_SetGender_m3410000268 (User_t719925459 * __this, int32_t ___genderToSet0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___genderToSet0;
		__this->set_userGender_9(L_0);
		return;
	}
}
// Gender User::GetGender()
extern "C"  int32_t User_GetGender_m2767708915 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_userGender_9();
		return L_0;
	}
}
// System.Void User::SetRankName(System.String)
extern "C"  void User_SetRankName_m1393779823 (User_t719925459 * __this, String_t* ___rankToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___rankToSet0;
		__this->set_rankName_7(L_0);
		return;
	}
}
// System.String User::GetRankName()
extern "C"  String_t* User_GetRankName_m1586895408 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_rankName_7();
		return L_0;
	}
}
// System.Void User::SetRankLevel(System.Int32)
extern "C"  void User_SetRankLevel_m784945417 (User_t719925459 * __this, int32_t ___rankToSet0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___rankToSet0;
		__this->set_rankLevel_8(L_0);
		return;
	}
}
// System.Int32 User::GetRankLevel()
extern "C"  int32_t User_GetRankLevel_m1380276626 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_rankLevel_8();
		return L_0;
	}
}
// UnityEngine.Texture2D User::CalculateTexture(System.Int32,System.Int32,System.Single,System.Single,System.Single,UnityEngine.Texture2D)
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t User_CalculateTexture_m4208086963_MetadataUsageId;
extern "C"  Texture2D_t3542995729 * User_CalculateTexture_m4208086963 (User_t719925459 * __this, int32_t ___h0, int32_t ___w1, float ___r2, float ___cx3, float ___cy4, Texture2D_t3542995729 * ___sourceTex5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_CalculateTexture_m4208086963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColorU5BU5D_t672350442* V_0 = NULL;
	Texture2D_t3542995729 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		Texture2D_t3542995729 * L_0 = ___sourceTex5;
		Texture2D_t3542995729 * L_1 = ___sourceTex5;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_1);
		Texture2D_t3542995729 * L_3 = ___sourceTex5;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_3);
		NullCheck(L_0);
		ColorU5BU5D_t672350442* L_5 = Texture2D_GetPixels_m3077752766(L_0, 0, 0, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = ___h0;
		int32_t L_7 = ___w1;
		Texture2D_t3542995729 * L_8 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_8, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = ___cx3;
		float L_10 = ___r2;
		V_2 = (((int32_t)((int32_t)((float)((float)L_9-(float)L_10)))));
		goto IL_00ac;
	}

IL_002b:
	{
		float L_11 = ___cy4;
		float L_12 = ___r2;
		V_3 = (((int32_t)((int32_t)((float)((float)L_11-(float)L_12)))));
		goto IL_009d;
	}

IL_0036:
	{
		int32_t L_13 = V_2;
		float L_14 = ___cx3;
		V_4 = ((float)((float)(((float)((float)L_13)))-(float)L_14));
		int32_t L_15 = V_3;
		float L_16 = ___cy4;
		V_5 = ((float)((float)(((float)((float)L_15)))-(float)L_16));
		float L_17 = V_4;
		float L_18 = V_4;
		float L_19 = V_5;
		float L_20 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_21 = sqrtf(((float)((float)((float)((float)L_17*(float)L_18))+(float)((float)((float)L_19*(float)L_20)))));
		V_6 = L_21;
		float L_22 = V_6;
		float L_23 = ___r2;
		if ((!(((float)L_22) <= ((float)L_23))))
		{
			goto IL_0080;
		}
	}
	{
		Texture2D_t3542995729 * L_24 = V_1;
		int32_t L_25 = V_2;
		float L_26 = ___cx3;
		float L_27 = ___r2;
		int32_t L_28 = V_3;
		float L_29 = ___cy4;
		float L_30 = ___r2;
		Texture2D_t3542995729 * L_31 = ___sourceTex5;
		int32_t L_32 = V_2;
		int32_t L_33 = V_3;
		NullCheck(L_31);
		Color_t2020392075  L_34 = Texture2D_GetPixel_m1570987051(L_31, L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_24);
		Texture2D_SetPixel_m609991514(L_24, ((int32_t)((int32_t)L_25-(int32_t)(((int32_t)((int32_t)((float)((float)L_26-(float)L_27))))))), ((int32_t)((int32_t)L_28-(int32_t)(((int32_t)((int32_t)((float)((float)L_29-(float)L_30))))))), L_34, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0080:
	{
		Texture2D_t3542995729 * L_35 = V_1;
		int32_t L_36 = V_2;
		float L_37 = ___cx3;
		float L_38 = ___r2;
		int32_t L_39 = V_3;
		float L_40 = ___cy4;
		float L_41 = ___r2;
		Color_t2020392075  L_42 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		Texture2D_SetPixel_m609991514(L_35, ((int32_t)((int32_t)L_36-(int32_t)(((int32_t)((int32_t)((float)((float)L_37-(float)L_38))))))), ((int32_t)((int32_t)L_39-(int32_t)(((int32_t)((int32_t)((float)((float)L_40-(float)L_41))))))), L_42, /*hidden argument*/NULL);
	}

IL_0099:
	{
		int32_t L_43 = V_3;
		V_3 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_009d:
	{
		int32_t L_44 = V_3;
		float L_45 = ___cy4;
		float L_46 = ___r2;
		if ((((float)(((float)((float)L_44)))) < ((float)((float)((float)L_45+(float)L_46)))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_47 = V_2;
		V_2 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_00ac:
	{
		int32_t L_48 = V_2;
		float L_49 = ___cx3;
		float L_50 = ___r2;
		if ((((float)(((float)((float)L_48)))) < ((float)((float)((float)L_49+(float)L_50)))))
		{
			goto IL_002b;
		}
	}
	{
		Texture2D_t3542995729 * L_51 = V_1;
		NullCheck(L_51);
		Texture2D_Apply_m3543341930(L_51, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_52 = V_1;
		return L_52;
	}
}
// System.Void User::SetUserAvatar(UnityEngine.Texture)
extern "C"  void User_SetUserAvatar_m4006846452 (User_t719925459 * __this, Texture_t2243626319 * ___textureToSet0, const MethodInfo* method)
{
	{
		Texture_t2243626319 * L_0 = ___textureToSet0;
		__this->set_userAvatar_10(L_0);
		return;
	}
}
// UnityEngine.Texture User::GetUserAvatar()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2517326139;
extern const uint32_t User_GetUserAvatar_m1568865019_MetadataUsageId;
extern "C"  Texture_t2243626319 * User_GetUserAvatar_m1568865019 (User_t719925459 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_GetUserAvatar_m1568865019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		Texture_t2243626319 * L_0 = __this->get_userAvatar_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a2;
		}
	}
	{
		Texture_t2243626319 * L_2 = __this->get_userAvatar_10();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture_t2243626319 * L_4 = __this->get_userAvatar_10();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		if ((((int32_t)L_3) <= ((int32_t)L_5)))
		{
			goto IL_003c;
		}
	}
	{
		Texture_t2243626319 * L_6 = __this->get_userAvatar_10();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		G_B4_0 = L_7;
		goto IL_0047;
	}

IL_003c:
	{
		Texture_t2243626319 * L_8 = __this->get_userAvatar_10();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		G_B4_0 = L_9;
	}

IL_0047:
	{
		V_0 = G_B4_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_10 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2517326139, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0067:
	{
		V_0 = ((int32_t)192);
		int32_t L_15 = V_0;
		int32_t L_16 = V_0;
		Texture_t2243626319 * L_17 = __this->get_userAvatar_10();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		Texture_t2243626319 * L_19 = __this->get_userAvatar_10();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_19);
		Texture_t2243626319 * L_21 = __this->get_userAvatar_10();
		Texture2D_t3542995729 * L_22 = User_CalculateTexture_m4208086963(__this, L_15, L_16, (96.0f), (((float)((float)((int32_t)((int32_t)L_18/(int32_t)2))))), (((float)((float)((int32_t)((int32_t)L_20/(int32_t)2))))), ((Texture2D_t3542995729 *)IsInstSealed(L_21, Texture2D_t3542995729_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_22;
	}

IL_00a2:
	{
		Texture_t2243626319 * L_23 = __this->get_userAvatar_10();
		return L_23;
	}
}
// System.Void User::SetProfileImageURL(System.String)
extern "C"  void User_SetProfileImageURL_m2251375873 (User_t719925459 * __this, String_t* ___imageURLToSet0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___imageURLToSet0;
		__this->set_profileImageURL_6(L_0);
		return;
	}
}
// System.String User::GetProfileImageUrl()
extern "C"  String_t* User_GetProfileImageUrl_m1524992026 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_profileImageURL_6();
		return L_0;
	}
}
// System.Void User::SetUserAudience(Audience)
extern "C"  void User_SetUserAudience_m1274780805 (User_t719925459 * __this, int32_t ___audienceToSet0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___audienceToSet0;
		__this->set_userAudience_12(L_0);
		return;
	}
}
// Audience User::GetUserAudience()
extern "C"  int32_t User_GetUserAudience_m2064638984 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_userAudience_12();
		return L_0;
	}
}
// System.Void User::UpdateDailyPoints(System.Int32)
extern "C"  void User_UpdateDailyPoints_m3824119854 (User_t719925459 * __this, int32_t ___amount0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___amount0;
		__this->set_dailyPoints_25(L_0);
		return;
	}
}
// System.Void User::EnterStealthMode(System.DateTime)
extern "C"  void User_EnterStealthMode_m2512144816 (User_t719925459 * __this, DateTime_t693205669  ___deativateAt0, const MethodInfo* method)
{
	{
		User_SetUserStealthStatus_m1346937537(__this, (bool)1, /*hidden argument*/NULL);
		DateTime_t693205669  L_0 = ___deativateAt0;
		__this->set_deactivatesStealthAt_22(L_0);
		return;
	}
}
// System.Void User::ExitStealthMode()
extern "C"  void User_ExitStealthMode_m4032515930 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		User_SetUserStealthStatus_m1346937537(__this, (bool)0, /*hidden argument*/NULL);
		User_GenerateStealthActivateTime_m934795268(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void User::SetUserStealthStatus(System.Boolean)
extern "C"  void User_SetUserStealthStatus_m1346937537 (User_t719925459 * __this, bool ___statusToSet0, const MethodInfo* method)
{
	{
		bool L_0 = ___statusToSet0;
		__this->set_inStealth_18(L_0);
		return;
	}
}
// System.Boolean User::GetUserStealthStatus()
extern "C"  bool User_GetUserStealthStatus_m2174563722 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_inStealth_18();
		return L_0;
	}
}
// System.Void User::GenerateStealthActivateTime()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2136529692_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1063737612_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2166063373_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2902814631;
extern Il2CppCodeGenString* _stringLiteral2782082665;
extern Il2CppCodeGenString* _stringLiteral866653105;
extern Il2CppCodeGenString* _stringLiteral3115736605;
extern Il2CppCodeGenString* _stringLiteral1865351123;
extern const uint32_t User_GenerateStealthActivateTime_m934795268_MetadataUsageId;
extern "C"  void User_GenerateStealthActivateTime_m934795268 (User_t719925459 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (User_GenerateStealthActivateTime_m934795268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t693205669  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t693205669  V_3;
	memset(&V_3, 0, sizeof(V_3));
	TimeSpan_t3430258949  V_4;
	memset(&V_4, 0, sizeof(V_4));
	DateTime_t693205669  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1927440687 * V_6 = NULL;
	Dictionary_2_t3943999495 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
			DateTime_t693205669  L_1 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_1;
			int32_t L_2 = DateTime_get_Year_m1985210972((&V_1), /*hidden argument*/NULL);
			DateTime_t693205669  L_3 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_2 = L_3;
			int32_t L_4 = DateTime_get_Month_m1464831817((&V_2), /*hidden argument*/NULL);
			DateTime_t693205669  L_5 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_3 = L_5;
			int32_t L_6 = DateTime_get_Day_m2066530041((&V_3), /*hidden argument*/NULL);
			int32_t L_7 = __this->get_stealthStartHour_20();
			int32_t L_8 = __this->get_stealthStartMin_21();
			DateTime__ctor_m3153923094((&V_0), L_2, L_4, L_6, L_7, L_8, 0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
			ChatmageddonServer_t594474938 * L_9 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
			DateTime_t693205669  L_10 = V_0;
			NullCheck(L_9);
			DateTime_t693205669  L_11 = BaseServer_1_ApplyServerTimeOffset_m2903528104(L_9, L_10, /*hidden argument*/BaseServer_1_ApplyServerTimeOffset_m2903528104_MethodInfo_var);
			V_0 = L_11;
			DateTime_t693205669  L_12 = V_0;
			DateTime_t693205669  L_13 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_5 = L_13;
			DateTime_t693205669  L_14 = DateTime_ToLocalTime_m1957689902((&V_5), /*hidden argument*/NULL);
			TimeSpan_t3430258949  L_15 = DateTime_op_Subtraction_m3246456251(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
			V_4 = L_15;
			int32_t L_16 = TimeSpan_get_Hours_m3804628138((&V_4), /*hidden argument*/NULL);
			if ((((int32_t)L_16) <= ((int32_t)0)))
			{
				goto IL_008a;
			}
		}

IL_0070:
		{
			int32_t L_17 = TimeSpan_get_Minutes_m2575030536((&V_4), /*hidden argument*/NULL);
			if ((((int32_t)L_17) <= ((int32_t)0)))
			{
				goto IL_008a;
			}
		}

IL_007d:
		{
			int32_t L_18 = TimeSpan_get_Seconds_m3108432552((&V_4), /*hidden argument*/NULL);
			if ((((int32_t)L_18) > ((int32_t)0)))
			{
				goto IL_00a5;
			}
		}

IL_008a:
		{
			DateTime_t693205669  L_19 = DateTime_AddHours_m422526479((&V_0), (24.0), /*hidden argument*/NULL);
			__this->set_activatesStealthAt_23(L_19);
			goto IL_00ac;
		}

IL_00a5:
		{
			DateTime_t693205669  L_20 = V_0;
			__this->set_activatesStealthAt_23(L_20);
		}

IL_00ac:
		{
			goto IL_0156;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00b1;
		throw e;
	}

CATCH_00b1:
	{ // begin catch(System.Exception)
		V_6 = ((Exception_t1927440687 *)__exception_local);
		Dictionary_2_t3943999495 * L_21 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_21, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_7 = L_21;
		Dictionary_2_t3943999495 * L_22 = V_7;
		int32_t L_23 = __this->get_stealthStartHour_20();
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_27 = String_Concat_m56707527(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_22);
		Dictionary_2_Add_m1063737612(L_22, _stringLiteral2902814631, L_27, /*hidden argument*/Dictionary_2_Add_m1063737612_MethodInfo_var);
		Dictionary_2_t3943999495 * L_28 = V_7;
		int32_t L_29 = __this->get_stealthStartMin_21();
		int32_t L_30 = L_29;
		Il2CppObject * L_31 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_30);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_33 = String_Concat_m56707527(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_28);
		Dictionary_2_Add_m1063737612(L_28, _stringLiteral2782082665, L_33, /*hidden argument*/Dictionary_2_Add_m1063737612_MethodInfo_var);
		Dictionary_2_t3943999495 * L_34 = V_7;
		String_t* L_35 = DateTime_ToString_m1117481977((&V_0), /*hidden argument*/NULL);
		String_t* L_36 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_37 = String_Concat_m2596409543(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		Dictionary_2_Add_m1063737612(L_34, _stringLiteral866653105, L_37, /*hidden argument*/Dictionary_2_Add_m1063737612_MethodInfo_var);
		Dictionary_2_t3943999495 * L_38 = V_7;
		Exception_t1927440687 * L_39 = V_6;
		NullCheck(L_39);
		String_t* L_40 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_39);
		NullCheck(L_38);
		Dictionary_2_Add_m1063737612(L_38, _stringLiteral3115736605, L_40, /*hidden argument*/Dictionary_2_Add_m1063737612_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2136529692_il2cpp_TypeInfo_var);
		FlurryController_t2385863972 * L_41 = MonoSingleton_1_get_Instance_m2166063373(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2166063373_MethodInfo_var);
		Dictionary_2_t3943999495 * L_42 = V_7;
		NullCheck(L_41);
		FlurryController_SendEvent_m1111498304(L_41, _stringLiteral1865351123, L_42, /*hidden argument*/NULL);
		DateTime_t693205669  L_43 = V_0;
		__this->set_activatesStealthAt_23(L_43);
		Exception_t1927440687 * L_44 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogException_m1861430175(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		goto IL_0156;
	} // end catch (depth: 1)

IL_0156:
	{
		return;
	}
}
// System.Void User::SetStealthDuration(System.Int32)
extern "C"  void User_SetStealthDuration_m1563407126 (User_t719925459 * __this, int32_t ___durationToSet0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___durationToSet0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000a;
		}
	}
	{
		___durationToSet0 = 0;
	}

IL_000a:
	{
		int32_t L_1 = ___durationToSet0;
		__this->set_stealthDuration_19(L_1);
		return;
	}
}
// System.Int32 User::GetStealthDuration()
extern "C"  int32_t User_GetStealthDuration_m1358343207 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_stealthDuration_19();
		return L_0;
	}
}
// System.Void User::SetStealthStartHour(System.Int32)
extern "C"  void User_SetStealthStartHour_m152216616 (User_t719925459 * __this, int32_t ___hourToSet0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___hourToSet0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000a;
		}
	}
	{
		___hourToSet0 = 0;
	}

IL_000a:
	{
		int32_t L_1 = ___hourToSet0;
		__this->set_stealthStartHour_20(L_1);
		return;
	}
}
// System.Int32 User::GetStealthStartHour()
extern "C"  int32_t User_GetStealthStartHour_m3740009413 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_stealthStartHour_20();
		return L_0;
	}
}
// System.Void User::SetStealthStartMin(System.Int32)
extern "C"  void User_SetStealthStartMin_m3978436462 (User_t719925459 * __this, int32_t ___minToSet0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___minToSet0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000a;
		}
	}
	{
		___minToSet0 = 0;
	}

IL_000a:
	{
		int32_t L_1 = ___minToSet0;
		__this->set_stealthStartMin_21(L_1);
		return;
	}
}
// System.Int32 User::GetStealthStartMin()
extern "C"  int32_t User_GetStealthStartMin_m452950579 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_stealthStartMin_21();
		return L_0;
	}
}
// System.Void User::KnockOutUser(System.DateTime)
extern "C"  void User_KnockOutUser_m3949102117 (User_t719925459 * __this, DateTime_t693205669  ___deativateAt0, const MethodInfo* method)
{
	{
		User_SetUserKnockoutStatus_m2409352638(__this, (bool)1, /*hidden argument*/NULL);
		DateTime_t693205669  L_0 = ___deativateAt0;
		__this->set_knockedOutUntil_28(L_0);
		return;
	}
}
// System.Void User::ExitKnockOut()
extern "C"  void User_ExitKnockOut_m815129388 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		User_SetUserKnockoutStatus_m2409352638(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void User::SetUserKnockoutStatus(System.Boolean)
extern "C"  void User_SetUserKnockoutStatus_m2409352638 (User_t719925459 * __this, bool ___statusToSet0, const MethodInfo* method)
{
	{
		bool L_0 = ___statusToSet0;
		__this->set_knockedOut_27(L_0);
		return;
	}
}
// System.Boolean User::GetUserKnockOutStatus()
extern "C"  bool User_GetUserKnockOutStatus_m785227929 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_knockedOut_27();
		return L_0;
	}
}
// System.Void User::SetActiveShield(Shield)
extern "C"  void User_SetActiveShield_m4195006786 (User_t719925459 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method)
{
	{
		Shield_t3327121081 * L_0 = ___shield0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_shieldType_4();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		Shield_t3327121081 * L_2 = ___shield0;
		__this->set_currentMissileShield_16(L_2);
		goto IL_002b;
	}

IL_0018:
	{
		Shield_t3327121081 * L_3 = ___shield0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shieldType_4();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_002b;
		}
	}
	{
		Shield_t3327121081 * L_5 = ___shield0;
		__this->set_currentMineShield_17(L_5);
	}

IL_002b:
	{
		return;
	}
}
// System.Boolean User::get_mineShielded()
extern "C"  bool User_get_mineShielded_m4077626408 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		Shield_t3327121081 * L_0 = __this->get_currentMineShield_17();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		return (bool)1;
	}
}
// System.Boolean User::get_missileShielded()
extern "C"  bool User_get_missileShielded_m1115021069 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		Shield_t3327121081 * L_0 = __this->get_currentMissileShield_16();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		return (bool)1;
	}
}
// System.Void User::SetAllowLocation(System.Boolean)
extern "C"  void User_SetAllowLocation_m2584520667 (User_t719925459 * __this, bool ___locationToSet0, const MethodInfo* method)
{
	{
		bool L_0 = ___locationToSet0;
		__this->set_allowLocation_13(L_0);
		return;
	}
}
// System.Boolean User::GetAllowLocation()
extern "C"  bool User_GetAllowLocation_m705717784 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_allowLocation_13();
		return L_0;
	}
}
// System.Void User::SetLongitude(System.Single)
extern "C"  void User_SetLongitude_m2611637698 (User_t719925459 * __this, float ___longitudeToSet0, const MethodInfo* method)
{
	{
		float L_0 = ___longitudeToSet0;
		__this->set_longitude_15(L_0);
		return;
	}
}
// System.Single User::GetLongitude()
extern "C"  float User_GetLongitude_m2128835213 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_longitude_15();
		return L_0;
	}
}
// System.Void User::SetLatitude(System.Single)
extern "C"  void User_SetLatitude_m1219378189 (User_t719925459 * __this, float ___latitudeToSet0, const MethodInfo* method)
{
	{
		float L_0 = ___latitudeToSet0;
		__this->set_latitude_14(L_0);
		return;
	}
}
// System.Single User::GetLatitude()
extern "C"  float User_GetLatitude_m1048514152 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_latitude_14();
		return L_0;
	}
}
// System.Void UserLoginButton::.ctor()
extern "C"  void UserLoginButton__ctor_m2784767299 (UserLoginButton_t3500827568 * __this, const MethodInfo* method)
{
	{
		__this->set_loaded_6((bool)1);
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserLoginButton::Start()
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var;
extern const MethodInfo* UserLoginButton_LoadRemainingUI_m1942796173_MethodInfo_var;
extern const uint32_t UserLoginButton_Start_m112400055_MetadataUsageId;
extern "C"  void UserLoginButton_Start_m112400055 (UserLoginButton_t3500827568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLoginButton_Start_m112400055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_startPage_5();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		TweenScale_t2697902175 * L_2 = Component_GetComponent_TisTweenScale_t2697902175_m3059496736(L_1, /*hidden argument*/Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)UserLoginButton_LoadRemainingUI_m1942796173_MethodInfo_var);
		Callback_t2100910411 * L_4 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		UITweener_AddOnFinished_m3855136488(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserLoginButton::OnButtonClick()
extern "C"  void UserLoginButton_OnButtonClick_m568155012 (UserLoginButton_t3500827568 * __this, const MethodInfo* method)
{
	{
		__this->set_loaded_6((bool)0);
		UserLoginButton_ActivateStartPage_m3499998890(__this, (bool)0, /*hidden argument*/NULL);
		SFXButton_OnButtonClick_m3012217745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserLoginButton::ActivateStartPage(System.Boolean)
extern const MethodInfo* Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var;
extern const uint32_t UserLoginButton_ActivateStartPage_m3499998890_MetadataUsageId;
extern "C"  void UserLoginButton_ActivateStartPage_m3499998890 (UserLoginButton_t3500827568 * __this, bool ___active0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLoginButton_ActivateStartPage_m3499998890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___active0;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_startPage_5();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		TweenScale_t2697902175 * L_3 = Component_GetComponent_TisTweenScale_t2697902175_m3059496736(L_2, /*hidden argument*/Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var);
		NullCheck(L_3);
		UITweener_PlayForward_m3772341562(L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_0020:
	{
		GameObject_t1756533147 * L_4 = __this->get_startPage_5();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		TweenScale_t2697902175 * L_6 = Component_GetComponent_TisTweenScale_t2697902175_m3059496736(L_5, /*hidden argument*/Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var);
		NullCheck(L_6);
		UITweener_PlayReverse_m4027828489(L_6, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UserLoginButton::LoadRemainingUI()
extern Il2CppClass* MonoSingleton_1_t3488760729_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t559355580_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3268274223_MethodInfo_var;
extern const MethodInfo* GameState_1_LoadScene_m3018880911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral251395756;
extern const uint32_t UserLoginButton_LoadRemainingUI_m1942796173_MetadataUsageId;
extern "C"  void UserLoginButton_LoadRemainingUI_m1942796173 (UserLoginButton_t3500827568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLoginButton_LoadRemainingUI_m1942796173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_loaded_6();
		if (L_0)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3488760729_il2cpp_TypeInfo_var);
		LoginBottomUIManager_t3738095009 * L_1 = MonoSingleton_1_get_Instance_m3398713466(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var);
		NullCheck(L_1);
		UsernameLoginButton_t2274508449 * L_2 = L_1->get_loginButton_5();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_closeLoginButton_6();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		LoginBottomUIManager_t3738095009 * L_4 = MonoSingleton_1_get_Instance_m3398713466(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var);
		NullCheck(L_4);
		UsernameLoginButton_t2274508449 * L_5 = L_4->get_loginButton_5();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_loginUI_5();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
		LoginBottomUIManager_t3738095009 * L_7 = MonoSingleton_1_get_Instance_m3398713466(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var);
		NullCheck(L_7);
		UsernameLoginButton_t2274508449 * L_8 = L_7->get_loginButton_5();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_loginUI_5();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		TweenPosition_t1144714832 * L_11 = Component_GetComponent_TisTweenPosition_t1144714832_m4195983071(L_10, /*hidden argument*/Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var);
		NullCheck(L_11);
		Behaviour_set_enabled_m1796096907(L_11, (bool)1, /*hidden argument*/NULL);
		LoginBottomUIManager_t3738095009 * L_12 = MonoSingleton_1_get_Instance_m3398713466(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var);
		NullCheck(L_12);
		UsernameLoginButton_t2274508449 * L_13 = L_12->get_loginButton_5();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = L_13->get_loginUI_5();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		TweenPosition_t1144714832 * L_16 = Component_GetComponent_TisTweenPosition_t1144714832_m4195983071(L_15, /*hidden argument*/Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var);
		NullCheck(L_16);
		UITweener_PlayForward_m3772341562(L_16, /*hidden argument*/NULL);
		__this->set_loaded_6((bool)1);
		goto IL_009a;
	}

IL_007e:
	{
		bool L_17 = __this->get_facebookLogin_7();
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t559355580_il2cpp_TypeInfo_var);
		ChatmageddonGamestate_t808689860 * L_18 = MonoSingleton_1_get_Instance_m3268274223(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3268274223_MethodInfo_var);
		NullCheck(L_18);
		GameState_1_LoadScene_m3018880911(L_18, _stringLiteral251395756, (bool)0, (bool)0, /*hidden argument*/GameState_1_LoadScene_m3018880911_MethodInfo_var);
	}

IL_009a:
	{
		return;
	}
}
// System.Void UsernameLoginButton::.ctor()
extern "C"  void UsernameLoginButton__ctor_m1114319632 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	{
		__this->set_unloaded_9((bool)1);
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UsernameLoginButton::Start()
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var;
extern const MethodInfo* UsernameLoginButton_UnloadRemainingUI_m3560200141_MethodInfo_var;
extern const uint32_t UsernameLoginButton_Start_m1341925416_MetadataUsageId;
extern "C"  void UsernameLoginButton_Start_m1341925416 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UsernameLoginButton_Start_m1341925416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_loginUI_5();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		TweenPosition_t1144714832 * L_2 = Component_GetComponent_TisTweenPosition_t1144714832_m4195983071(L_1, /*hidden argument*/Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)UsernameLoginButton_UnloadRemainingUI_m3560200141_MethodInfo_var);
		Callback_t2100910411 * L_4 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		UITweener_AddOnFinished_m3855136488(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UsernameLoginButton::OnButtonClick()
extern "C"  void UsernameLoginButton_OnButtonClick_m2993763317 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	{
		UsernameLoginButton_UserLogin_m1143078920(__this, /*hidden argument*/NULL);
		SFXButton_OnButtonClick_m3012217745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UsernameLoginButton::UserLogin()
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1865222972_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* UsernameLoginButton_U3CUserLoginU3Em__0_m535833292_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m759102168_MethodInfo_var;
extern const uint32_t UsernameLoginButton_UserLogin_m1143078920_MetadataUsageId;
extern "C"  void UsernameLoginButton_UserLogin_m1143078920 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UsernameLoginButton_UserLogin_m1143078920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = UsernameLoginButton_ValidateInput_m567769604(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_1 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		UIInput_t860674234 * L_2 = __this->get_usernameInput_7();
		NullCheck(L_2);
		String_t* L_3 = UIInput_get_value_m2016753770(L_2, /*hidden argument*/NULL);
		UIInput_t860674234 * L_4 = __this->get_passwordInput_8();
		NullCheck(L_4);
		String_t* L_5 = UIInput_get_value_m2016753770(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UsernameLoginButton_U3CUserLoginU3Em__0_m535833292_MethodInfo_var);
		Action_2_t1865222972 * L_7 = (Action_2_t1865222972 *)il2cpp_codegen_object_new(Action_2_t1865222972_il2cpp_TypeInfo_var);
		Action_2__ctor_m759102168(L_7, __this, L_6, /*hidden argument*/Action_2__ctor_m759102168_MethodInfo_var);
		NullCheck(L_1);
		VirtActionInvoker3< String_t*, String_t*, Action_2_t1865222972 * >::Invoke(6 /* System.Void BaseServer`1<ChatmageddonServer>::LoginViaUsername(System.String,System.String,System.Action`2<System.Boolean,System.String>) */, L_1, L_3, L_5, L_7);
	}

IL_0037:
	{
		return;
	}
}
// System.Boolean UsernameLoginButton::ValidateInput()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2190604083;
extern Il2CppCodeGenString* _stringLiteral1567291887;
extern Il2CppCodeGenString* _stringLiteral2650033037;
extern Il2CppCodeGenString* _stringLiteral1337746668;
extern Il2CppCodeGenString* _stringLiteral1523449804;
extern const uint32_t UsernameLoginButton_ValidateInput_m567769604_MetadataUsageId;
extern "C"  bool UsernameLoginButton_ValidateInput_m567769604 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UsernameLoginButton_ValidateInput_m567769604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = (bool)0;
		UIInput_t860674234 * L_1 = __this->get_usernameInput_7();
		NullCheck(L_1);
		String_t* L_2 = UIInput_get_value_m2016753770(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_4 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		UIInput_t860674234 * L_5 = __this->get_usernameInput_7();
		NullCheck(L_5);
		String_t* L_6 = UIInput_get_value_m2016753770(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		CharU5BU5D_t1328083999* L_7 = String_ToCharArray_m870309954(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))) < ((int32_t)3)))
		{
			goto IL_0041;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_0047;
	}

IL_0041:
	{
		V_0 = _stringLiteral2190604083;
	}

IL_0047:
	{
		goto IL_0052;
	}

IL_004c:
	{
		V_0 = _stringLiteral1567291887;
	}

IL_0052:
	{
		V_2 = (bool)0;
		UIInput_t860674234 * L_8 = __this->get_passwordInput_8();
		NullCheck(L_8);
		String_t* L_9 = UIInput_get_value_m2016753770(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_11 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_009e;
		}
	}
	{
		bool L_12 = V_1;
		if (!L_12)
		{
			goto IL_0099;
		}
	}
	{
		UIInput_t860674234 * L_13 = __this->get_passwordInput_8();
		NullCheck(L_13);
		String_t* L_14 = UIInput_get_value_m2016753770(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		CharU5BU5D_t1328083999* L_15 = String_ToCharArray_m870309954(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))) < ((int32_t)8)))
		{
			goto IL_0093;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0099;
	}

IL_0093:
	{
		V_0 = _stringLiteral2650033037;
	}

IL_0099:
	{
		goto IL_00c9;
	}

IL_009e:
	{
		UIInput_t860674234 * L_16 = __this->get_usernameInput_7();
		NullCheck(L_16);
		String_t* L_17 = UIInput_get_value_m2016753770(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00c3;
		}
	}
	{
		V_0 = _stringLiteral1337746668;
		goto IL_00c9;
	}

IL_00c3:
	{
		V_0 = _stringLiteral1523449804;
	}

IL_00c9:
	{
		bool L_20 = V_1;
		if (!L_20)
		{
			goto IL_00d7;
		}
	}
	{
		bool L_21 = V_2;
		if (!L_21)
		{
			goto IL_00d7;
		}
	}
	{
		return (bool)1;
	}

IL_00d7:
	{
		String_t* L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_24 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00fd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_25 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		String_t* L_26 = V_0;
		Nullable_1_t339576247  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Nullable_1__ctor_m1978009879(&L_27, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_25);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_25, L_26, L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		return (bool)0;
	}
}
// System.Void UsernameLoginButton::UnloadUserLoginUI()
extern const MethodInfo* Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var;
extern const uint32_t UsernameLoginButton_UnloadUserLoginUI_m1160369251_MetadataUsageId;
extern "C"  void UsernameLoginButton_UnloadUserLoginUI_m1160369251 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UsernameLoginButton_UnloadUserLoginUI_m1160369251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_unloaded_9((bool)0);
		GameObject_t1756533147 * L_0 = __this->get_loginUI_5();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		TweenPosition_t1144714832 * L_2 = Component_GetComponent_TisTweenPosition_t1144714832_m4195983071(L_1, /*hidden argument*/Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var);
		NullCheck(L_2);
		Behaviour_set_enabled_m1796096907(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_loginUI_5();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		TweenPosition_t1144714832 * L_5 = Component_GetComponent_TisTweenPosition_t1144714832_m4195983071(L_4, /*hidden argument*/Component_GetComponent_TisTweenPosition_t1144714832_m4195983071_MethodInfo_var);
		NullCheck(L_5);
		UITweener_PlayReverse_m4027828489(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UsernameLoginButton::UnloadRemainingUI()
extern Il2CppClass* MonoSingleton_1_t559355580_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3488760729_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3268274223_MethodInfo_var;
extern const MethodInfo* GameState_1_LoadScene_m3018880911_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral251395756;
extern const uint32_t UsernameLoginButton_UnloadRemainingUI_m3560200141_MetadataUsageId;
extern "C"  void UsernameLoginButton_UnloadRemainingUI_m3560200141 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UsernameLoginButton_UnloadRemainingUI_m3560200141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_unloaded_9();
		if (L_0)
		{
			goto IL_0069;
		}
	}
	{
		bool L_1 = __this->get_userLoginSuccess_10();
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t559355580_il2cpp_TypeInfo_var);
		ChatmageddonGamestate_t808689860 * L_2 = MonoSingleton_1_get_Instance_m3268274223(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3268274223_MethodInfo_var);
		NullCheck(L_2);
		GameState_1_LoadScene_m3018880911(L_2, _stringLiteral251395756, (bool)0, (bool)0, /*hidden argument*/GameState_1_LoadScene_m3018880911_MethodInfo_var);
		goto IL_0069;
	}

IL_002c:
	{
		GameObject_t1756533147 * L_3 = __this->get_closeLoginButton_6();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_loginUI_5();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3488760729_il2cpp_TypeInfo_var);
		LoginBottomUIManager_t3738095009 * L_5 = MonoSingleton_1_get_Instance_m3398713466(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3398713466_MethodInfo_var);
		NullCheck(L_5);
		UserLoginButton_t3500827568 * L_6 = L_5->get_menuLoginButton_6();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_startPage_5();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		TweenScale_t2697902175 * L_9 = Component_GetComponent_TisTweenScale_t2697902175_m3059496736(L_8, /*hidden argument*/Component_GetComponent_TisTweenScale_t2697902175_m3059496736_MethodInfo_var);
		NullCheck(L_9);
		UITweener_PlayForward_m3772341562(L_9, /*hidden argument*/NULL);
		__this->set_unloaded_9((bool)1);
	}

IL_0069:
	{
		return;
	}
}
// System.Void UsernameLoginButton::<UserLogin>m__0(System.Boolean,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t UsernameLoginButton_U3CUserLoginU3Em__0_m535833292_MetadataUsageId;
extern "C"  void UsernameLoginButton_U3CUserLoginU3Em__0_m535833292 (UsernameLoginButton_t2274508449 * __this, bool ___loginSuccess0, String_t* ___errorCode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UsernameLoginButton_U3CUserLoginU3Em__0_m535833292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Hashtable_t909839986 * V_1 = NULL;
	{
		bool L_0 = ___loginSuccess0;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		__this->set_userLoginSuccess_10((bool)1);
		UsernameLoginButton_UnloadUserLoginUI_m1160369251(__this, /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_1;
		String_t* L_2 = ___errorCode1;
		Il2CppObject * L_3 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = ((Hashtable_t909839986 *)CastclassClass(L_3, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_4, _stringLiteral359429027);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Hashtable_t909839986 * L_6 = V_1;
		NullCheck(L_6);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_6, _stringLiteral359429027);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		V_0 = L_8;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_9 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		String_t* L_10 = V_0;
		Nullable_1_t339576247  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Nullable_1__ctor_m1978009879(&L_11, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_9);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_9, L_10, L_11, (bool)0, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void UserPayload::.ctor()
extern Il2CppCodeGenString* _stringLiteral339800794;
extern Il2CppCodeGenString* _stringLiteral2132392826;
extern Il2CppCodeGenString* _stringLiteral2813401344;
extern Il2CppCodeGenString* _stringLiteral1111044658;
extern Il2CppCodeGenString* _stringLiteral524867311;
extern Il2CppCodeGenString* _stringLiteral3726277302;
extern Il2CppCodeGenString* _stringLiteral2143767523;
extern Il2CppCodeGenString* _stringLiteral2676687986;
extern Il2CppCodeGenString* _stringLiteral51149175;
extern Il2CppCodeGenString* _stringLiteral4103790218;
extern Il2CppCodeGenString* _stringLiteral708089278;
extern Il2CppCodeGenString* _stringLiteral720945496;
extern Il2CppCodeGenString* _stringLiteral2624098672;
extern Il2CppCodeGenString* _stringLiteral2220809406;
extern Il2CppCodeGenString* _stringLiteral1232249462;
extern Il2CppCodeGenString* _stringLiteral1594891052;
extern Il2CppCodeGenString* _stringLiteral2778558052;
extern const uint32_t UserPayload__ctor_m1877607510_MetadataUsageId;
extern "C"  void UserPayload__ctor_m1877607510 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserPayload__ctor_m1877607510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_tID_17(_stringLiteral339800794);
		__this->set_tFirstName_18(_stringLiteral2132392826);
		__this->set_tSecondName_19(_stringLiteral2813401344);
		__this->set_tEmail_20(_stringLiteral1111044658);
		__this->set_tFullName_21(_stringLiteral524867311);
		__this->set_tUserName_22(_stringLiteral3726277302);
		__this->set_tProfileImage_23(_stringLiteral2143767523);
		__this->set_tProfileImageThumbnail_24(_stringLiteral2676687986);
		__this->set_tGender_25(_stringLiteral51149175);
		__this->set_tDisplayLocation_26(_stringLiteral4103790218);
		__this->set_tStealthMode_27(_stringLiteral708089278);
		__this->set_tStealthModeActive_28(_stringLiteral720945496);
		__this->set_tStealthModeHour_29(_stringLiteral2624098672);
		__this->set_tStealthModeMin_30(_stringLiteral2220809406);
		__this->set_tStealthModeDuration_31(_stringLiteral1232249462);
		__this->set_tLevel_32(_stringLiteral1594891052);
		__this->set_tRank_33(_stringLiteral2778558052);
		return;
	}
}
// System.String UserPayload::GetLevelTag()
extern "C"  String_t* UserPayload_GetLevelTag_m1590126275 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tLevel_32();
		return L_0;
	}
}
// System.String UserPayload::GetRankTag()
extern "C"  String_t* UserPayload_GetRankTag_m2881288159 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tRank_33();
		return L_0;
	}
}
// System.String UserPayload::GetStealthModeActiveTag()
extern "C"  String_t* UserPayload_GetStealthModeActiveTag_m3885858145 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tStealthModeActive_28();
		return L_0;
	}
}
// System.String UserPayload::GetStealthModeDurationTag()
extern "C"  String_t* UserPayload_GetStealthModeDurationTag_m4026287879 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tStealthModeDuration_31();
		return L_0;
	}
}
// System.String UserPayload::GetStealthModeMinTag()
extern "C"  String_t* UserPayload_GetStealthModeMinTag_m682840799 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tStealthModeMin_30();
		return L_0;
	}
}
// System.String UserPayload::GetStealthModeHourTag()
extern "C"  String_t* UserPayload_GetStealthModeHourTag_m3913217993 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tStealthModeHour_29();
		return L_0;
	}
}
// System.String UserPayload::GetStealthModeTag()
extern "C"  String_t* UserPayload_GetStealthModeTag_m3747900581 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tStealthMode_27();
		return L_0;
	}
}
// System.String UserPayload::GetDisplayLocationTag()
extern "C"  String_t* UserPayload_GetDisplayLocationTag_m559639940 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tDisplayLocation_26();
		return L_0;
	}
}
// System.String UserPayload::GetGenderTag()
extern "C"  String_t* UserPayload_GetGenderTag_m3290465964 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tGender_25();
		return L_0;
	}
}
// System.String UserPayload::GetUserNameTag()
extern "C"  String_t* UserPayload_GetUserNameTag_m153107099 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tUserName_22();
		return L_0;
	}
}
// System.String UserPayload::GetFirstNameTag()
extern "C"  String_t* UserPayload_GetFirstNameTag_m4133148532 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tFirstName_18();
		return L_0;
	}
}
// System.String UserPayload::GetSecondNameTag()
extern "C"  String_t* UserPayload_GetSecondNameTag_m3730413342 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tSecondName_19();
		return L_0;
	}
}
// System.String UserPayload::GetEmailTag()
extern "C"  String_t* UserPayload_GetEmailTag_m1715712457 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tEmail_20();
		return L_0;
	}
}
// System.String UserPayload::GetFullNameTag()
extern "C"  String_t* UserPayload_GetFullNameTag_m679247165 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tFullName_21();
		return L_0;
	}
}
// System.String UserPayload::GetIDTag()
extern "C"  String_t* UserPayload_GetIDTag_m2447284976 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tID_17();
		return L_0;
	}
}
// System.String UserPayload::GetProfileImageTag()
extern "C"  String_t* UserPayload_GetProfileImageTag_m4172151285 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tProfileImage_23();
		return L_0;
	}
}
// System.String UserPayload::GetProfileThumbnailTag()
extern "C"  String_t* UserPayload_GetProfileThumbnailTag_m4004667390 (UserPayload_t146188169 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tProfileImageThumbnail_24();
		return L_0;
	}
}
// System.Void UserRegistrationButton::.ctor()
extern "C"  void UserRegistrationButton__ctor_m2212632477 (UserRegistrationButton_t1399913110 * __this, const MethodInfo* method)
{
	{
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserRegistrationButton::OnButtonClick()
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const uint32_t UserRegistrationButton_OnButtonClick_m4271833294_MetadataUsageId;
extern "C"  void UserRegistrationButton_OnButtonClick_m4271833294 (UserRegistrationButton_t1399913110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserRegistrationButton_OnButtonClick_m4271833294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_0 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_0);
		L_0->set_facebookRegStart_3((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_1 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(4 /* System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::SetupStartScreen(NavScreenType) */, L_1, 1);
		SFXButton_OnButtonClick_m3012217745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ValidContact::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t ValidContact__ctor_m3865902013_MetadataUsageId;
extern "C"  void ValidContact__ctor_m3865902013 (ValidContact_t1479914934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidContact__ctor_m3865902013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_firstName_0(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_lastName_1(L_1);
		List_1_t1398341365 * L_2 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_2, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_phoneNumbers_2(L_2);
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_phoneNumberHashes_3(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_rawChecksum_4(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_validatedChecksum_5(L_5);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ValidContact::AddContactName(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Skip_TisString_t_m3256592265_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1652456295_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral2846264340;
extern Il2CppCodeGenString* _stringLiteral564878756;
extern const uint32_t ValidContact_AddContactName_m2146732781_MetadataUsageId;
extern "C"  void ValidContact_AddContactName_m2146732781 (ValidContact_t1479914934 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidContact_AddContactName_m2146732781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	ValidContact_t1479914934 * G_B2_0 = NULL;
	ValidContact_t1479914934 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	ValidContact_t1479914934 * G_B3_1 = NULL;
	{
		String_t* L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = String_ToUpper_m3715743312(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = String_Trim_m2668767713(L_1, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_2);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3326265864(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		StringU5BU5D_t1642385972* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = 0;
		String_t* L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_firstName_0(L_7);
		StringU5BU5D_t1642385972* L_8 = V_0;
		NullCheck(L_8);
		G_B1_0 = __this;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))) <= ((int32_t)1)))
		{
			G_B2_0 = __this;
			goto IL_004a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_9 = V_0;
		Il2CppObject* L_10 = Enumerable_Skip_TisString_t_m3256592265(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_9, 1, /*hidden argument*/Enumerable_Skip_TisString_t_m3256592265_MethodInfo_var);
		StringU5BU5D_t1642385972* L_11 = Enumerable_ToArray_TisString_t_m1652456295(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_ToArray_TisString_t_m1652456295_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral372029310, L_11, /*hidden argument*/NULL);
		G_B3_0 = L_12;
		G_B3_1 = G_B1_0;
		goto IL_004f;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_13;
		G_B3_1 = G_B2_0;
	}

IL_004f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_lastName_1(G_B3_0);
		String_t* L_14 = __this->get_firstName_0();
		NullCheck(L_14);
		String_t* L_15 = String_Trim_m2668767713(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008d;
		}
	}
	{
		__this->set_firstName_0(_stringLiteral2846264340);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_18 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_008d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral564878756, /*hidden argument*/NULL);
	}

IL_008d:
	{
		return;
	}
}
// System.Void ValidContact::AddContactPhoneNumber(System.String)
extern const MethodInfo* List_1_Contains_m3447151919_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m972781753_MethodInfo_var;
extern const uint32_t ValidContact_AddContactPhoneNumber_m2410896373_MetadataUsageId;
extern "C"  void ValidContact_AddContactPhoneNumber_m2410896373 (ValidContact_t1479914934 * __this, String_t* ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidContact_AddContactPhoneNumber_m2410896373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___number0;
		String_t* L_1 = ValidContact_numberToHash_m4186448226(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		List_1_t1398341365 * L_3 = __this->get_phoneNumberHashes_3();
		String_t* L_4 = V_0;
		NullCheck(L_3);
		bool L_5 = List_1_Contains_m3447151919(L_3, L_4, /*hidden argument*/List_1_Contains_m3447151919_MethodInfo_var);
		if (L_5)
		{
			goto IL_0048;
		}
	}
	{
		__this->set_hasChanged_7((bool)1);
		List_1_t1398341365 * L_6 = __this->get_phoneNumbers_2();
		String_t* L_7 = ___number0;
		NullCheck(L_6);
		List_1_Add_m4061286785(L_6, L_7, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_8 = __this->get_phoneNumberHashes_3();
		String_t* L_9 = V_0;
		NullCheck(L_8);
		List_1_Add_m4061286785(L_8, L_9, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_10 = __this->get_phoneNumberHashes_3();
		NullCheck(L_10);
		List_1_Sort_m972781753(L_10, /*hidden argument*/List_1_Sort_m972781753_MethodInfo_var);
	}

IL_0048:
	{
		return;
	}
}
// System.String ValidContact::numberToHash(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* SHA512Managed_t3949709369_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t ValidContact_numberToHash_m4186448226_MetadataUsageId;
extern "C"  String_t* ValidContact_numberToHash_m4186448226 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidContact_numberToHash_m4186448226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	SHA512_t2908163326 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___number0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_1 = L_2;
		SHA512Managed_t3949709369 * L_3 = (SHA512Managed_t3949709369 *)il2cpp_codegen_object_new(SHA512Managed_t3949709369_il2cpp_TypeInfo_var);
		SHA512Managed__ctor_m2316753784(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		SHA512_t2908163326 * L_4 = V_2;
		ByteU5BU5D_t3397334013* L_5 = V_1;
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_6 = HashAlgorithm_ComputeHash_m3637856778(L_4, L_5, /*hidden argument*/NULL);
		String_t* L_7 = ValidContact_ByteArrayToString_m3394199796(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		IL2CPP_LEAVE(0x33, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			SHA512_t2908163326 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0032;
			}
		}

IL_002c:
		{
			SHA512_t2908163326 * L_9 = V_2;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0033:
	{
		String_t* L_10 = V_0;
		if (L_10)
		{
			goto IL_003b;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_003b:
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = String_ToLower_m2994460523(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.String ValidContact::ByteArrayToString(System.Byte[])
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t ValidContact_ByteArrayToString_m3394199796_MetadataUsageId;
extern "C"  String_t* ValidContact_ByteArrayToString_m3394199796 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___ba0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidContact_ByteArrayToString_m3394199796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___ba0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		String_t* L_1 = BitConverter_ToString_m927173850(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_2);
		String_t* L_4 = String_Replace_m1941156251(L_2, _stringLiteral372029313, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String ValidContact::getValidatedChecksum()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AddressBook_t411411037_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Sort_m972781753_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m948919263_MethodInfo_var;
extern const uint32_t ValidContact_getValidatedChecksum_m1050687113_MetadataUsageId;
extern "C"  String_t* ValidContact_getValidatedChecksum_m1050687113 (ValidContact_t1479914934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidContact_getValidatedChecksum_m1050687113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_validatedChecksum_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = __this->get_hasChanged_7();
		if (!L_3)
		{
			goto IL_004b;
		}
	}

IL_0020:
	{
		List_1_t1398341365 * L_4 = __this->get_phoneNumberHashes_3();
		NullCheck(L_4);
		List_1_Sort_m972781753(L_4, /*hidden argument*/List_1_Sort_m972781753_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		List_1_t1398341365 * L_6 = __this->get_phoneNumberHashes_3();
		NullCheck(L_6);
		StringU5BU5D_t1642385972* L_7 = List_1_ToArray_m948919263(L_6, /*hidden argument*/List_1_ToArray_m948919263_MethodInfo_var);
		String_t* L_8 = String_Join_m1966872927(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AddressBook_t411411037_il2cpp_TypeInfo_var);
		String_t* L_9 = AddressBook_Md5Sum_m958752352(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_validatedChecksum_5(L_9);
	}

IL_004b:
	{
		String_t* L_10 = __this->get_validatedChecksum_5();
		return L_10;
	}
}
// System.Void VerificationCodeInput::.ctor()
extern "C"  void VerificationCodeInput__ctor_m1686097505 (VerificationCodeInput_t924401746 * __this, const MethodInfo* method)
{
	{
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerificationCodeInput::InputSubmit()
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern Il2CppClass* VerificationCodeInput_t924401746_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* VerificationCodeInput_U3CInputSubmitU3Em__0_m4212791937_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern const uint32_t VerificationCodeInput_InputSubmit_m8653775_MetadataUsageId;
extern "C"  void VerificationCodeInput_InputSubmit_m8653775 (VerificationCodeInput_t924401746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationCodeInput_InputSubmit_m8653775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	ChatmageddonServer_t594474938 * G_B3_3 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	ChatmageddonServer_t594474938 * G_B2_3 = NULL;
	{
		bool L_0 = VerificationCodeInput_ValidateInput_m2922263993(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_1 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_2 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_mobileHash_10();
		RegistrationManager_t1533517116 * L_4 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_4);
		String_t* L_5 = L_4->get_mobilePinID_11();
		UIInput_t860674234 * L_6 = __this->get_codeinput_5();
		NullCheck(L_6);
		String_t* L_7 = UIInput_get_value_m2016753770(L_6, /*hidden argument*/NULL);
		Action_3_t3681841185 * L_8 = ((VerificationCodeInput_t924401746_StaticFields*)VerificationCodeInput_t924401746_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		G_B2_0 = L_7;
		G_B2_1 = L_5;
		G_B2_2 = L_3;
		G_B2_3 = L_1;
		if (L_8)
		{
			G_B3_0 = L_7;
			G_B3_1 = L_5;
			G_B3_2 = L_3;
			G_B3_3 = L_1;
			goto IL_0047;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)VerificationCodeInput_U3CInputSubmitU3Em__0_m4212791937_MethodInfo_var);
		Action_3_t3681841185 * L_10 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_10, NULL, L_9, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		((VerificationCodeInput_t924401746_StaticFields*)VerificationCodeInput_t924401746_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_6(L_10);
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
		G_B3_2 = G_B2_2;
		G_B3_3 = G_B2_3;
	}

IL_0047:
	{
		Action_3_t3681841185 * L_11 = ((VerificationCodeInput_t924401746_StaticFields*)VerificationCodeInput_t924401746_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		NullCheck(G_B3_3);
		ChatmageddonServer_VerifyMobileNumber_m2287088762(G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_11, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Boolean VerificationCodeInput::ValidateInput()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857191402;
extern const uint32_t VerificationCodeInput_ValidateInput_m2922263993_MetadataUsageId;
extern "C"  bool VerificationCodeInput_ValidateInput_m2922263993 (VerificationCodeInput_t924401746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationCodeInput_ValidateInput_m2922263993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIInput_t860674234 * L_0 = __this->get_codeinput_5();
		NullCheck(L_0);
		String_t* L_1 = UIInput_get_value_m2016753770(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_4 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Nullable_1__ctor_m1978009879(&L_5, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_4);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_4, _stringLiteral1857191402, L_5, (bool)0, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void VerificationCodeInput::<InputSubmit>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t VerificationCodeInput_U3CInputSubmitU3Em__0_m4212791937_MetadataUsageId;
extern "C"  void VerificationCodeInput_U3CInputSubmitU3Em__0_m4212791937 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationCodeInput_U3CInputSubmitU3Em__0_m4212791937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Hashtable_t909839986 * V_1 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_1 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_1);
		L_1->set_numberVerified_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_2 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::NavigateForwards(NavScreenType) */, L_2, 3);
		goto IL_006a;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_3;
		String_t* L_4 = ___errorCode2;
		Il2CppObject * L_5 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = ((Hashtable_t909839986 *)CastclassClass(L_5, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_6, _stringLiteral359429027);
		if (!L_7)
		{
			goto IL_0054;
		}
	}
	{
		Hashtable_t909839986 * L_8 = V_1;
		NullCheck(L_8);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_8, _stringLiteral359429027);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		V_0 = L_10;
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_11 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		String_t* L_12 = V_0;
		Nullable_1_t339576247  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Nullable_1__ctor_m1978009879(&L_13, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_11);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_11, L_12, L_13, (bool)0, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void VerificationScreen::.ctor()
extern "C"  void VerificationScreen__ctor_m4173238258 (VerificationScreen_t1800753931 * __this, const MethodInfo* method)
{
	{
		NavigationScreen__ctor_m2982757537(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerificationScreen::Start()
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const MethodInfo* VerificationScreen_VerificationUIClosing_m2202126214_MethodInfo_var;
extern const uint32_t VerificationScreen_Start_m3424584630_MetadataUsageId;
extern "C"  void VerificationScreen_Start_m3424584630 (VerificationScreen_t1800753931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationScreen_Start_m3424584630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TweenPosition_t1144714832 * L_1 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_0, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)VerificationScreen_VerificationUIClosing_m2202126214_MethodInfo_var);
		Callback_t2100910411 * L_3 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		UITweener_AddOnFinished_m3855136488(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerificationScreen::VerificationUIClosing()
extern "C"  void VerificationScreen_VerificationUIClosing_m2202126214 (VerificationScreen_t1800753931 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_verificationUIclosing_6();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_verificationUIclosing_6((bool)0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void VerificationScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const uint32_t VerificationScreen_ScreenLoad_m3599340954_MetadataUsageId;
extern "C"  void VerificationScreen_ScreenLoad_m3599340954 (VerificationScreen_t1800753931 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationScreen_ScreenLoad_m3599340954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___direction0;
		if (L_0)
		{
			goto IL_00a4;
		}
	}
	{
		UILabel_t1795115428 * L_1 = __this->get_phonenumLabel_3();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_2 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_fullPhoneNumber_9();
		NullCheck(L_1);
		UILabel_set_text_m451064939(L_1, L_3, /*hidden argument*/NULL);
		RegistrationManager_t1533517116 * L_4 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_4);
		String_t* L_5 = L_4->get_mobilePin_12();
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		UIInput_t860674234 * L_6 = __this->get_codeinput_4();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_7 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_7);
		String_t* L_8 = L_7->get_mobilePin_12();
		NullCheck(L_6);
		UIInput_set_value_m430904237(L_6, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_9 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_9);
		bool L_10 = L_9->get_numberVerified_4();
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t1756533147 * L_11 = __this->get_reSendButton_5();
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)0, /*hidden argument*/NULL);
		goto IL_006b;
	}

IL_005f:
	{
		GameObject_t1756533147 * L_12 = __this->get_reSendButton_5();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_006b:
	{
		__this->set_verificationUIclosing_6((bool)0);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		TweenPosition_t1144714832 * L_15 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_14, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_15);
		Behaviour_set_enabled_m1796096907(L_15, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		TweenPosition_t1144714832 * L_17 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_16, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_17);
		UITweener_PlayForward_m3772341562(L_17, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00a4:
	{
		int32_t L_18 = ___direction0;
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_00df;
		}
	}
	{
		__this->set_verificationUIclosing_6((bool)0);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		TweenPosition_t1144714832 * L_21 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_20, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_21);
		Behaviour_set_enabled_m1796096907(L_21, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		TweenPosition_t1144714832 * L_23 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_22, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_23);
		UITweener_PlayReverse_m4027828489(L_23, /*hidden argument*/NULL);
	}

IL_00df:
	{
		return;
	}
}
// System.Void VerificationScreen::ScreenUnload(NavigationDirection)
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const uint32_t VerificationScreen_ScreenUnload_m1706785742_MetadataUsageId;
extern "C"  void VerificationScreen_ScreenUnload_m1706785742 (VerificationScreen_t1800753931 * __this, int32_t ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationScreen_ScreenUnload_m1706785742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___direction0;
		if (L_0)
		{
			goto IL_0090;
		}
	}
	{
		__this->set_verificationUIclosing_6((bool)1);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_localPosition_m2533925116(L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		TweenPosition_t1144714832 * L_5 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_4, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = L_5->get_to_21();
		bool L_7 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		TweenPosition_t1144714832 * L_9 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_8, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_10 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = L_10->get_activePos_8();
		NullCheck(L_9);
		L_9->set_from_20(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		TweenPosition_t1144714832 * L_13 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_12, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_14 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = L_14->get_leftInactivePos_9();
		NullCheck(L_13);
		L_13->set_to_21(L_15);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		TweenPosition_t1144714832 * L_17 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_16, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_17);
		UITweener_ResetToBeginning_m1313982072(L_17, /*hidden argument*/NULL);
	}

IL_007b:
	{
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		TweenPosition_t1144714832 * L_19 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_18, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_19);
		UITweener_PlayForward_m3772341562(L_19, /*hidden argument*/NULL);
		goto IL_011c;
	}

IL_0090:
	{
		int32_t L_20 = ___direction0;
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_011c;
		}
	}
	{
		__this->set_verificationUIclosing_6((bool)1);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = GameObject_get_transform_m909382139(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_localPosition_m2533925116(L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		TweenPosition_t1144714832 * L_25 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_24, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = L_25->get_from_20();
		bool L_27 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_010c;
		}
	}
	{
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		TweenPosition_t1144714832 * L_29 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_28, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_30 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = L_30->get_rightInactivePos_10();
		NullCheck(L_29);
		L_29->set_from_20(L_31);
		GameObject_t1756533147 * L_32 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		TweenPosition_t1144714832 * L_33 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_32, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_34 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = L_34->get_activePos_8();
		NullCheck(L_33);
		L_33->set_to_21(L_35);
		GameObject_t1756533147 * L_36 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		TweenPosition_t1144714832 * L_37 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_36, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_37);
		UITweener_ResetToBeginning_m1313982072(L_37, /*hidden argument*/NULL);
	}

IL_010c:
	{
		GameObject_t1756533147 * L_38 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		TweenPosition_t1144714832 * L_39 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_38, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_39);
		UITweener_PlayReverse_m4027828489(L_39, /*hidden argument*/NULL);
	}

IL_011c:
	{
		return;
	}
}
// System.Boolean VerificationScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool VerificationScreen_ValidateScreenNavigation_m2515162657 (VerificationScreen_t1800753931 * __this, int32_t ___direction0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void VerificationUIScaler::.ctor()
extern "C"  void VerificationUIScaler__ctor_m283385268 (VerificationUIScaler_t217609153 * __this, const MethodInfo* method)
{
	{
		ChatmageddonUIScaler__ctor_m1032172578(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerificationUIScaler::GlobalUIScale()
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t4068614118_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m817330426_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m387681903_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m17319439_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const uint32_t VerificationUIScaler_GlobalUIScale_m1431232971_MetadataUsageId;
extern "C"  void VerificationUIScaler_GlobalUIScale_m1431232971 (VerificationUIScaler_t217609153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerificationUIScaler_GlobalUIScale_m1431232971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		bool L_0 = MonoSingleton_1_IsAvailable_m817330426(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m817330426_MethodInfo_var);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_2 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_3 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_3);
		Vector3_t2243707580 * L_4 = L_3->get_address_of_rightInactivePos_10();
		float L_5 = L_4->get_y_2();
		RegistrationNavigationcontroller_t539766269 * L_6 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_6);
		Vector3_t2243707580 * L_7 = L_6->get_address_of_rightInactivePos_10();
		float L_8 = L_7->get_z_3();
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_9, /*hidden argument*/NULL);
		goto IL_0081;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4068614118_il2cpp_TypeInfo_var);
		bool L_10 = MonoSingleton_1_IsAvailable_m387681903(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m387681903_MethodInfo_var);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_12 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t4068614118_il2cpp_TypeInfo_var);
		ContactsNaviagationController_t22981102 * L_13 = MonoSingleton_1_get_Instance_m17319439(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m17319439_MethodInfo_var);
		NullCheck(L_13);
		Vector3_t2243707580 * L_14 = L_13->get_address_of_rightInactivePos_10();
		float L_15 = L_14->get_y_2();
		ContactsNaviagationController_t22981102 * L_16 = MonoSingleton_1_get_Instance_m17319439(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m17319439_MethodInfo_var);
		NullCheck(L_16);
		Vector3_t2243707580 * L_17 = L_16->get_address_of_rightInactivePos_10();
		float L_18 = L_17->get_z_3();
		Vector3_t2243707580  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2638739322(&L_19, L_12, L_15, L_18, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localPosition_m1026930133(L_11, L_19, /*hidden argument*/NULL);
	}

IL_0081:
	{
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		TweenPosition_t1144714832 * L_21 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_20, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_localPosition_m2533925116(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_from_20(L_23);
		UISprite_t603616735 * L_24 = __this->get_topSeperator_14();
		float L_25 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_24);
		UIWidget_set_width_m1785998957(L_24, (((int32_t)((int32_t)L_25))), /*hidden argument*/NULL);
		UISprite_t603616735 * L_26 = __this->get_bottomSeperator_15();
		float L_27 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_26);
		UIWidget_set_width_m1785998957(L_26, (((int32_t)((int32_t)L_27))), /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_28 = __this->get_codeCollider_16();
		float L_29 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_30 = __this->get_codeCollider_16();
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = BoxCollider_get_size_m201254358(L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		float L_32 = (&V_0)->get_y_2();
		BoxCollider_t22920061 * L_33 = __this->get_codeCollider_16();
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = BoxCollider_get_size_m201254358(L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		float L_35 = (&V_1)->get_z_3();
		Vector3_t2243707580  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m2638739322(&L_36, L_29, L_32, L_35, /*hidden argument*/NULL);
		NullCheck(L_28);
		BoxCollider_set_size_m4101048759(L_28, L_36, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_37 = __this->get_resendCollider_17();
		float L_38 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_39 = __this->get_resendCollider_17();
		NullCheck(L_39);
		Vector3_t2243707580  L_40 = BoxCollider_get_size_m201254358(L_39, /*hidden argument*/NULL);
		V_2 = L_40;
		float L_41 = (&V_2)->get_y_2();
		BoxCollider_t22920061 * L_42 = __this->get_resendCollider_17();
		NullCheck(L_42);
		Vector3_t2243707580  L_43 = BoxCollider_get_size_m201254358(L_42, /*hidden argument*/NULL);
		V_3 = L_43;
		float L_44 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector3__ctor_m2638739322(&L_45, L_38, L_41, L_44, /*hidden argument*/NULL);
		NullCheck(L_37);
		BoxCollider_set_size_m4101048759(L_37, L_45, /*hidden argument*/NULL);
		UISprite_t603616735 * L_46 = __this->get_verifyButton_18();
		float L_47 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_46);
		UIWidget_set_width_m1785998957(L_46, (((int32_t)((int32_t)L_47))), /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_48 = __this->get_verifyCollider_19();
		float L_49 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_50 = __this->get_verifyCollider_19();
		NullCheck(L_50);
		Vector3_t2243707580  L_51 = BoxCollider_get_size_m201254358(L_50, /*hidden argument*/NULL);
		V_4 = L_51;
		float L_52 = (&V_4)->get_y_2();
		BoxCollider_t22920061 * L_53 = __this->get_verifyCollider_19();
		NullCheck(L_53);
		Vector3_t2243707580  L_54 = BoxCollider_get_size_m201254358(L_53, /*hidden argument*/NULL);
		V_5 = L_54;
		float L_55 = (&V_5)->get_z_3();
		Vector3_t2243707580  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector3__ctor_m2638739322(&L_56, L_49, L_52, L_55, /*hidden argument*/NULL);
		NullCheck(L_48);
		BoxCollider_set_size_m4101048759(L_48, L_56, /*hidden argument*/NULL);
		BaseUIScaler_GlobalUIScale_m2469894115(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerifyCodeButton::.ctor()
extern "C"  void VerifyCodeButton__ctor_m3255555879 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method)
{
	{
		SFXButton__ctor_m538305182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerifyCodeButton::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t3496309181_il2cpp_TypeInfo_var;
extern const MethodInfo* VerifyCodeButton_U3CStartU3Em__0_m682529774_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3183853268_MethodInfo_var;
extern const uint32_t VerifyCodeButton_Start_m85647915_MetadataUsageId;
extern "C"  void VerifyCodeButton_Start_m85647915 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerifyCodeButton_Start_m85647915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VerificationCodeInput_t924401746 * L_0 = __this->get_input_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		VerificationCodeInput_t924401746 * L_2 = __this->get_input_5();
		NullCheck(L_2);
		UIInput_t860674234 * L_3 = L_2->get_codeinput_5();
		NullCheck(L_3);
		List_1_t2865430313 * L_4 = L_3->get_onChange_19();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)VerifyCodeButton_U3CStartU3Em__0_m682529774_MethodInfo_var);
		Callback_t2100910411 * L_6 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_6, __this, L_5, /*hidden argument*/NULL);
		EventDelegate_t3496309181 * L_7 = (EventDelegate_t3496309181 *)il2cpp_codegen_object_new(EventDelegate_t3496309181_il2cpp_TypeInfo_var);
		EventDelegate__ctor_m411819485(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_Add_m3183853268(L_4, L_7, /*hidden argument*/List_1_Add_m3183853268_MethodInfo_var);
	}

IL_0037:
	{
		return;
	}
}
// System.Void VerifyCodeButton::CheckCount()
extern "C"  void VerifyCodeButton_CheckCount_m2811372368 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method)
{
	{
		VerificationCodeInput_t924401746 * L_0 = __this->get_input_5();
		NullCheck(L_0);
		UIInput_t860674234 * L_1 = L_0->get_codeinput_5();
		NullCheck(L_1);
		String_t* L_2 = UIInput_get_value_m2016753770(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)4)))
		{
			goto IL_003f;
		}
	}
	{
		UISprite_t603616735 * L_4 = __this->get_buttonBackground_6();
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m3811852957(&L_5, (1.0f), (0.6313726f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		UIWidget_set_color_m1406575269(L_4, L_5, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_003f:
	{
		UISprite_t603616735 * L_6 = __this->get_buttonBackground_6();
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m3811852957(&L_7, (0.745098054f), (0.745098054f), (0.745098054f), /*hidden argument*/NULL);
		NullCheck(L_6);
		UIWidget_set_color_m1406575269(L_6, L_7, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void VerifyCodeButton::OnButtonClick()
extern "C"  void VerifyCodeButton_OnButtonClick_m3263143044 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method)
{
	{
		VerificationCodeInput_t924401746 * L_0 = __this->get_input_5();
		NullCheck(L_0);
		VerificationCodeInput_InputSubmit_m8653775(L_0, /*hidden argument*/NULL);
		SFXButton_OnButtonClick_m3012217745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VerifyCodeButton::<Start>m__0()
extern "C"  void VerifyCodeButton_U3CStartU3Em__0_m682529774 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method)
{
	{
		VerifyCodeButton_CheckCount_m2811372368(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VibrationController::.ctor()
extern Il2CppClass* MonoSingleton_1_t2727221918_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1__ctor_m573910172_MethodInfo_var;
extern const uint32_t VibrationController__ctor_m3021961997_MetadataUsageId;
extern "C"  void VibrationController__ctor_m3021961997 (VibrationController_t2976556198 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VibrationController__ctor_m3021961997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2727221918_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m573910172(__this, /*hidden argument*/MonoSingleton_1__ctor_m573910172_MethodInfo_var);
		return;
	}
}
// System.Void VibrationController::VibrateDevice(VibrationType,System.Single)
extern "C"  void VibrationController_VibrateDevice_m432977373 (VibrationController_t2976556198 * __this, int32_t ___vibration0, float ___durationInSecs1, const MethodInfo* method)
{
	{
		__this->set_vibrateActive_3((bool)1);
		int32_t L_0 = ___vibration0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0048;
			}
			case 1:
			{
				goto IL_0035;
			}
			case 2:
			{
				goto IL_005b;
			}
			case 3:
			{
				goto IL_0022;
			}
		}
	}
	{
		goto IL_006e;
	}

IL_0022:
	{
		float L_1 = ___durationInSecs1;
		Il2CppObject * L_2 = VibrationController_ContinuousVibrate_m2533401476(__this, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0035:
	{
		float L_3 = ___durationInSecs1;
		Il2CppObject * L_4 = VibrationController_PulseVibrate_m2606443574(__this, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0048:
	{
		float L_5 = ___durationInSecs1;
		Il2CppObject * L_6 = VibrationController_SingleVibrate_m597131523(__this, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_6, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_005b:
	{
		float L_7 = ___durationInSecs1;
		Il2CppObject * L_8 = VibrationController_SwitchVibrate_m1470384715(__this, L_7, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_8, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_006e:
	{
		goto IL_0073;
	}

IL_0073:
	{
		return;
	}
}
// System.Collections.IEnumerator VibrationController::ContinuousVibrate(System.Single)
extern Il2CppClass* U3CContinuousVibrateU3Ec__Iterator0_t1905345410_il2cpp_TypeInfo_var;
extern const uint32_t VibrationController_ContinuousVibrate_m2533401476_MetadataUsageId;
extern "C"  Il2CppObject * VibrationController_ContinuousVibrate_m2533401476 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VibrationController_ContinuousVibrate_m2533401476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * V_0 = NULL;
	{
		U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * L_0 = (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 *)il2cpp_codegen_object_new(U3CContinuousVibrateU3Ec__Iterator0_t1905345410_il2cpp_TypeInfo_var);
		U3CContinuousVibrateU3Ec__Iterator0__ctor_m2225703931(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * L_1 = V_0;
		float L_2 = ___duration0;
		NullCheck(L_1);
		L_1->set_duration_1(L_2);
		U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_2(__this);
		U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator VibrationController::PulseVibrate(System.Single)
extern Il2CppClass* U3CPulseVibrateU3Ec__Iterator1_t3172871213_il2cpp_TypeInfo_var;
extern const uint32_t VibrationController_PulseVibrate_m2606443574_MetadataUsageId;
extern "C"  Il2CppObject * VibrationController_PulseVibrate_m2606443574 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VibrationController_PulseVibrate_m2606443574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPulseVibrateU3Ec__Iterator1_t3172871213 * V_0 = NULL;
	{
		U3CPulseVibrateU3Ec__Iterator1_t3172871213 * L_0 = (U3CPulseVibrateU3Ec__Iterator1_t3172871213 *)il2cpp_codegen_object_new(U3CPulseVibrateU3Ec__Iterator1_t3172871213_il2cpp_TypeInfo_var);
		U3CPulseVibrateU3Ec__Iterator1__ctor_m3213733588(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPulseVibrateU3Ec__Iterator1_t3172871213 * L_1 = V_0;
		float L_2 = ___duration0;
		NullCheck(L_1);
		L_1->set_duration_2(L_2);
		U3CPulseVibrateU3Ec__Iterator1_t3172871213 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CPulseVibrateU3Ec__Iterator1_t3172871213 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator VibrationController::SingleVibrate(System.Single)
extern Il2CppClass* U3CSingleVibrateU3Ec__Iterator2_t2980562147_il2cpp_TypeInfo_var;
extern const uint32_t VibrationController_SingleVibrate_m597131523_MetadataUsageId;
extern "C"  Il2CppObject * VibrationController_SingleVibrate_m597131523 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VibrationController_SingleVibrate_m597131523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSingleVibrateU3Ec__Iterator2_t2980562147 * V_0 = NULL;
	{
		U3CSingleVibrateU3Ec__Iterator2_t2980562147 * L_0 = (U3CSingleVibrateU3Ec__Iterator2_t2980562147 *)il2cpp_codegen_object_new(U3CSingleVibrateU3Ec__Iterator2_t2980562147_il2cpp_TypeInfo_var);
		U3CSingleVibrateU3Ec__Iterator2__ctor_m2341378010(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSingleVibrateU3Ec__Iterator2_t2980562147 * L_1 = V_0;
		float L_2 = ___duration0;
		NullCheck(L_1);
		L_1->set_duration_1(L_2);
		U3CSingleVibrateU3Ec__Iterator2_t2980562147 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_2(__this);
		U3CSingleVibrateU3Ec__Iterator2_t2980562147 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator VibrationController::SwitchVibrate(System.Single)
extern Il2CppClass* U3CSwitchVibrateU3Ec__Iterator3_t2167946060_il2cpp_TypeInfo_var;
extern const uint32_t VibrationController_SwitchVibrate_m1470384715_MetadataUsageId;
extern "C"  Il2CppObject * VibrationController_SwitchVibrate_m1470384715 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VibrationController_SwitchVibrate_m1470384715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * V_0 = NULL;
	{
		U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * L_0 = (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 *)il2cpp_codegen_object_new(U3CSwitchVibrateU3Ec__Iterator3_t2167946060_il2cpp_TypeInfo_var);
		U3CSwitchVibrateU3Ec__Iterator3__ctor_m573071091(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * L_1 = V_0;
		float L_2 = ___duration0;
		NullCheck(L_1);
		L_1->set_duration_2(L_2);
		U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * L_4 = V_0;
		return L_4;
	}
}
// System.Void VibrationController/<ContinuousVibrate>c__Iterator0::.ctor()
extern "C"  void U3CContinuousVibrateU3Ec__Iterator0__ctor_m2225703931 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VibrationController/<ContinuousVibrate>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var;
extern const uint32_t U3CContinuousVibrateU3Ec__Iterator0_MoveNext_m1803085589_MetadataUsageId;
extern "C"  bool U3CContinuousVibrateU3Ec__Iterator0_MoveNext_m1803085589 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinuousVibrateU3Ec__Iterator0_MoveNext_m1803085589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0065;
			}
		}
	}
	{
		goto IL_009f;
	}

IL_0021:
	{
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeStartedU3E__0_0(L_2);
		goto IL_0075;
	}

IL_0031:
	{
		VibrationController_t2976556198 * L_3 = __this->get_U24this_2();
		NullCheck(L_3);
		bool L_4 = L_3->get_vibrateActive_3();
		if (!L_4)
		{
			goto IL_006a;
		}
	}
	{
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaitForFixedUpdate_t3968615785 * L_5 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_5, /*hidden argument*/NULL);
		__this->set_U24current_3(L_5);
		bool L_6 = __this->get_U24disposing_4();
		if (L_6)
		{
			goto IL_0060;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0060:
	{
		goto IL_00a1;
	}

IL_0065:
	{
		goto IL_0075;
	}

IL_006a:
	{
		__this->set_duration_1((0.0f));
	}

IL_0075:
	{
		float L_7 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_U3CtimeStartedU3E__0_0();
		float L_9 = __this->get_duration_1();
		if ((((float)L_7) < ((float)((float)((float)L_8+(float)L_9)))))
		{
			goto IL_0031;
		}
	}
	{
		VibrationController_t2976556198 * L_10 = __this->get_U24this_2();
		NullCheck(L_10);
		L_10->set_vibrateActive_3((bool)0);
		__this->set_U24PC_5((-1));
	}

IL_009f:
	{
		return (bool)0;
	}

IL_00a1:
	{
		return (bool)1;
	}
}
// System.Object VibrationController/<ContinuousVibrate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CContinuousVibrateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3649436573 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object VibrationController/<ContinuousVibrate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CContinuousVibrateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2385699845 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void VibrationController/<ContinuousVibrate>c__Iterator0::Dispose()
extern "C"  void U3CContinuousVibrateU3Ec__Iterator0_Dispose_m1425763732 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void VibrationController/<ContinuousVibrate>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CContinuousVibrateU3Ec__Iterator0_Reset_m977914630_MetadataUsageId;
extern "C"  void U3CContinuousVibrateU3Ec__Iterator0_Reset_m977914630 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinuousVibrateU3Ec__Iterator0_Reset_m977914630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VibrationController/<PulseVibrate>c__Iterator1::.ctor()
extern "C"  void U3CPulseVibrateU3Ec__Iterator1__ctor_m3213733588 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VibrationController/<PulseVibrate>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var;
extern const uint32_t U3CPulseVibrateU3Ec__Iterator1_MoveNext_m1976877108_MetadataUsageId;
extern "C"  bool U3CPulseVibrateU3Ec__Iterator1_MoveNext_m1976877108 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPulseVibrateU3Ec__Iterator1_MoveNext_m1976877108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00ac;
			}
		}
	}
	{
		goto IL_00e6;
	}

IL_0021:
	{
		__this->set_U3CcountU3E__0_0(0);
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeStartedU3E__1_1(L_2);
		goto IL_00bc;
	}

IL_0038:
	{
		VibrationController_t2976556198 * L_3 = __this->get_U24this_3();
		NullCheck(L_3);
		bool L_4 = L_3->get_vibrateActive_3();
		if (!L_4)
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_5 = __this->get_U3CcountU3E__0_0();
		if ((((int32_t)L_5) >= ((int32_t)((int32_t)30))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_6 = __this->get_U3CcountU3E__0_0();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0066;
		}
	}
	{
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0066:
	{
		int32_t L_7 = __this->get_U3CcountU3E__0_0();
		if ((((int32_t)L_7) < ((int32_t)((int32_t)40))))
		{
			goto IL_007f;
		}
	}
	{
		__this->set_U3CcountU3E__0_0(0);
		goto IL_008d;
	}

IL_007f:
	{
		int32_t L_8 = __this->get_U3CcountU3E__0_0();
		__this->set_U3CcountU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_008d:
	{
		WaitForFixedUpdate_t3968615785 * L_9 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_9, /*hidden argument*/NULL);
		__this->set_U24current_4(L_9);
		bool L_10 = __this->get_U24disposing_5();
		if (L_10)
		{
			goto IL_00a7;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_00a7:
	{
		goto IL_00e8;
	}

IL_00ac:
	{
		goto IL_00bc;
	}

IL_00b1:
	{
		__this->set_duration_2((0.0f));
	}

IL_00bc:
	{
		float L_11 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = __this->get_U3CtimeStartedU3E__1_1();
		float L_13 = __this->get_duration_2();
		if ((((float)L_11) < ((float)((float)((float)L_12+(float)L_13)))))
		{
			goto IL_0038;
		}
	}
	{
		VibrationController_t2976556198 * L_14 = __this->get_U24this_3();
		NullCheck(L_14);
		L_14->set_vibrateActive_3((bool)0);
		__this->set_U24PC_6((-1));
	}

IL_00e6:
	{
		return (bool)0;
	}

IL_00e8:
	{
		return (bool)1;
	}
}
// System.Object VibrationController/<PulseVibrate>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPulseVibrateU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1585096720 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object VibrationController/<PulseVibrate>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPulseVibrateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4195196936 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void VibrationController/<PulseVibrate>c__Iterator1::Dispose()
extern "C"  void U3CPulseVibrateU3Ec__Iterator1_Dispose_m3095994143 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void VibrationController/<PulseVibrate>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPulseVibrateU3Ec__Iterator1_Reset_m4006132869_MetadataUsageId;
extern "C"  void U3CPulseVibrateU3Ec__Iterator1_Reset_m4006132869 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPulseVibrateU3Ec__Iterator1_Reset_m4006132869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VibrationController/<SingleVibrate>c__Iterator2::.ctor()
extern "C"  void U3CSingleVibrateU3Ec__Iterator2__ctor_m2341378010 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VibrationController/<SingleVibrate>c__Iterator2::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CSingleVibrateU3Ec__Iterator2_MoveNext_m2824126370_MetadataUsageId;
extern "C"  bool U3CSingleVibrateU3Ec__Iterator2_MoveNext_m2824126370 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSingleVibrateU3Ec__Iterator2_MoveNext_m2824126370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_006a;
			}
		}
	}
	{
		goto IL_00a4;
	}

IL_0021:
	{
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeStartedU3E__0_0(L_2);
		goto IL_007a;
	}

IL_0031:
	{
		VibrationController_t2976556198 * L_3 = __this->get_U24this_2();
		NullCheck(L_3);
		bool L_4 = L_3->get_vibrateActive_3();
		if (!L_4)
		{
			goto IL_006f;
		}
	}
	{
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_5 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_5, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_5);
		bool L_6 = __this->get_U24disposing_4();
		if (L_6)
		{
			goto IL_0065;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0065:
	{
		goto IL_00a6;
	}

IL_006a:
	{
		goto IL_007a;
	}

IL_006f:
	{
		__this->set_duration_1((0.0f));
	}

IL_007a:
	{
		float L_7 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_U3CtimeStartedU3E__0_0();
		float L_9 = __this->get_duration_1();
		if ((((float)L_7) < ((float)((float)((float)L_8+(float)L_9)))))
		{
			goto IL_0031;
		}
	}
	{
		VibrationController_t2976556198 * L_10 = __this->get_U24this_2();
		NullCheck(L_10);
		L_10->set_vibrateActive_3((bool)0);
		__this->set_U24PC_5((-1));
	}

IL_00a4:
	{
		return (bool)0;
	}

IL_00a6:
	{
		return (bool)1;
	}
}
// System.Object VibrationController/<SingleVibrate>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSingleVibrateU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1475061172 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object VibrationController/<SingleVibrate>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSingleVibrateU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2060965276 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void VibrationController/<SingleVibrate>c__Iterator2::Dispose()
extern "C"  void U3CSingleVibrateU3Ec__Iterator2_Dispose_m911557205 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void VibrationController/<SingleVibrate>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSingleVibrateU3Ec__Iterator2_Reset_m2158566503_MetadataUsageId;
extern "C"  void U3CSingleVibrateU3Ec__Iterator2_Reset_m2158566503 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSingleVibrateU3Ec__Iterator2_Reset_m2158566503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VibrationController/<SwitchVibrate>c__Iterator3::.ctor()
extern "C"  void U3CSwitchVibrateU3Ec__Iterator3__ctor_m573071091 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VibrationController/<SwitchVibrate>c__Iterator3::MoveNext()
extern Il2CppClass* WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var;
extern const uint32_t U3CSwitchVibrateU3Ec__Iterator3_MoveNext_m2197227717_MetadataUsageId;
extern "C"  bool U3CSwitchVibrateU3Ec__Iterator3_MoveNext_m2197227717 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSwitchVibrateU3Ec__Iterator3_MoveNext_m2197227717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00c8;
			}
		}
	}
	{
		goto IL_0102;
	}

IL_0021:
	{
		__this->set_U3CcountU3E__0_0(0);
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeStartedU3E__1_1(L_2);
		goto IL_00d8;
	}

IL_0038:
	{
		VibrationController_t2976556198 * L_3 = __this->get_U24this_3();
		NullCheck(L_3);
		bool L_4 = L_3->get_vibrateActive_3();
		if (!L_4)
		{
			goto IL_00cd;
		}
	}
	{
		int32_t L_5 = __this->get_U3CcountU3E__0_0();
		if ((((int32_t)L_5) >= ((int32_t)((int32_t)60))))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = __this->get_U3CcountU3E__0_0();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_006b;
		}
	}
	{
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_006b:
	{
		int32_t L_7 = __this->get_U3CcountU3E__0_0();
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)90)))))
		{
			goto IL_0082;
		}
	}
	{
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_0082:
	{
		int32_t L_8 = __this->get_U3CcountU3E__0_0();
		if ((((int32_t)L_8) < ((int32_t)((int32_t)120))))
		{
			goto IL_009b;
		}
	}
	{
		__this->set_U3CcountU3E__0_0(0);
		goto IL_00a9;
	}

IL_009b:
	{
		int32_t L_9 = __this->get_U3CcountU3E__0_0();
		__this->set_U3CcountU3E__0_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_00a9:
	{
		WaitForFixedUpdate_t3968615785 * L_10 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_10, /*hidden argument*/NULL);
		__this->set_U24current_4(L_10);
		bool L_11 = __this->get_U24disposing_5();
		if (L_11)
		{
			goto IL_00c3;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_00c3:
	{
		goto IL_0104;
	}

IL_00c8:
	{
		goto IL_00d8;
	}

IL_00cd:
	{
		__this->set_duration_2((0.0f));
	}

IL_00d8:
	{
		float L_12 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = __this->get_U3CtimeStartedU3E__1_1();
		float L_14 = __this->get_duration_2();
		if ((((float)L_12) < ((float)((float)((float)L_13+(float)L_14)))))
		{
			goto IL_0038;
		}
	}
	{
		VibrationController_t2976556198 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		L_15->set_vibrateActive_3((bool)0);
		__this->set_U24PC_6((-1));
	}

IL_0102:
	{
		return (bool)0;
	}

IL_0104:
	{
		return (bool)1;
	}
}
// System.Object VibrationController/<SwitchVibrate>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSwitchVibrateU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1698350205 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object VibrationController/<SwitchVibrate>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSwitchVibrateU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1779214549 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void VibrationController/<SwitchVibrate>c__Iterator3::Dispose()
extern "C"  void U3CSwitchVibrateU3Ec__Iterator3_Dispose_m1190319694 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void VibrationController/<SwitchVibrate>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSwitchVibrateU3Ec__Iterator3_Reset_m2081466768_MetadataUsageId;
extern "C"  void U3CSwitchVibrateU3Ec__Iterator3_Reset_m2081466768 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSwitchVibrateU3Ec__Iterator3_Reset_m2081466768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSample::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral144945899;
extern Il2CppCodeGenString* _stringLiteral2305923669;
extern const uint32_t WebSocketSample__ctor_m2014750320_MetadataUsageId;
extern "C"  void WebSocketSample__ctor_m2014750320 (WebSocketSample_t3966400161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample__ctor_m2014750320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_address_2(_stringLiteral144945899);
		__this->set_msgToSend_3(_stringLiteral2305923669);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_Text_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSample::OnDestroy()
extern "C"  void WebSocketSample_OnDestroy_m1642033623 (WebSocketSample_t3966400161 * __this, const MethodInfo* method)
{
	{
		WebSocket_t71448861 * L_0 = __this->get_webSocket_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		WebSocket_t71448861 * L_1 = __this->get_webSocket_5();
		NullCheck(L_1);
		WebSocket_Close_m812654301(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void WebSocketSample::OnGUI()
extern Il2CppClass* GUIHelper_t1081137527_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocketSample_U3COnGUIU3Em__0_m1212533797_MethodInfo_var;
extern const uint32_t WebSocketSample_OnGUI_m101555608_MetadataUsageId;
extern "C"  void WebSocketSample_OnGUI_m101555608 (WebSocketSample_t3966400161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample_OnGUI_m101555608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rect_t3681755626  L_0 = ((GUIHelper_t1081137527_StaticFields*)GUIHelper_t1081137527_il2cpp_TypeInfo_var->static_fields)->get_ClientArea_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)WebSocketSample_U3COnGUIU3Em__0_m1212533797_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		GUIHelper_DrawArea_m7986099(NULL /*static, unused*/, L_0, (bool)1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSample::OnOpen(BestHTTP.WebSocket.WebSocket)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4279159427;
extern const uint32_t WebSocketSample_OnOpen_m1733835373_MetadataUsageId;
extern "C"  void WebSocketSample_OnOpen_m1733835373 (WebSocketSample_t3966400161 * __this, WebSocket_t71448861 * ___ws0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample_OnOpen_m1733835373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Text_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral4279159427, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Text_4(L_2);
		return;
	}
}
// System.Void WebSocketSample::OnMessageReceived(BestHTTP.WebSocket.WebSocket,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1055461543;
extern const uint32_t WebSocketSample_OnMessageReceived_m1722491755_MetadataUsageId;
extern "C"  void WebSocketSample_OnMessageReceived_m1722491755 (WebSocketSample_t3966400161 * __this, WebSocket_t71448861 * ___ws0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample_OnMessageReceived_m1722491755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Text_4();
		String_t* L_1 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1055461543, L_1, /*hidden argument*/NULL);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		__this->set_Text_4(L_3);
		return;
	}
}
// System.Void WebSocketSample::OnClosed(BestHTTP.WebSocket.WebSocket,System.UInt16,System.String)
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral812351022;
extern const uint32_t WebSocketSample_OnClosed_m2641889363_MetadataUsageId;
extern "C"  void WebSocketSample_OnClosed_m2641889363 (WebSocketSample_t3966400161 * __this, WebSocket_t71448861 * ___ws0, uint16_t ___code1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample_OnClosed_m2641889363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Text_4();
		uint16_t L_1 = ___code1;
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4 = ___message2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral812351022, L_3, L_4, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		__this->set_Text_4(L_6);
		__this->set_webSocket_5((WebSocket_t71448861 *)NULL);
		return;
	}
}
// System.Void WebSocketSample::OnError(BestHTTP.WebSocket.WebSocket,System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2177915787;
extern Il2CppCodeGenString* _stringLiteral1839997521;
extern Il2CppCodeGenString* _stringLiteral547283914;
extern const uint32_t WebSocketSample_OnError_m643242149_MetadataUsageId;
extern "C"  void WebSocketSample_OnError_m643242149 (WebSocketSample_t3966400161 * __this, WebSocket_t71448861 * ___ws0, Exception_t1927440687 * ___ex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample_OnError_m643242149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	WebSocketSample_t3966400161 * G_B4_2 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	WebSocketSample_t3966400161 * G_B3_2 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	String_t* G_B5_2 = NULL;
	WebSocketSample_t3966400161 * G_B5_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		WebSocket_t71448861 * L_1 = ___ws0;
		NullCheck(L_1);
		HTTPRequest_t138485887 * L_2 = WebSocket_get_InternalRequest_m3005945664(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		HTTPResponse_t62748825 * L_3 = HTTPRequest_get_Response_m1740439732(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		WebSocket_t71448861 * L_4 = ___ws0;
		NullCheck(L_4);
		HTTPRequest_t138485887 * L_5 = WebSocket_get_InternalRequest_m3005945664(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		HTTPResponse_t62748825 * L_6 = HTTPRequest_get_Response_m1740439732(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = HTTPResponse_get_StatusCode_m1879961860(L_6, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		WebSocket_t71448861 * L_10 = ___ws0;
		NullCheck(L_10);
		HTTPRequest_t138485887 * L_11 = WebSocket_get_InternalRequest_m3005945664(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		HTTPResponse_t62748825 * L_12 = HTTPRequest_get_Response_m1740439732(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = HTTPResponse_get_Message_m1456893857(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2177915787, L_9, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0046:
	{
		String_t* L_15 = __this->get_Text_4();
		Exception_t1927440687 * L_16 = ___ex1;
		G_B3_0 = _stringLiteral1839997521;
		G_B3_1 = L_15;
		G_B3_2 = __this;
		if (!L_16)
		{
			G_B4_0 = _stringLiteral1839997521;
			G_B4_1 = L_15;
			G_B4_2 = __this;
			goto IL_0063;
		}
	}
	{
		Exception_t1927440687 * L_17 = ___ex1;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_17);
		G_B5_0 = L_18;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		G_B5_3 = G_B3_2;
		goto IL_006e;
	}

IL_0063:
	{
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral547283914, L_19, /*hidden argument*/NULL);
		G_B5_0 = L_20;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		G_B5_3 = G_B4_2;
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Format_m2024975688(NULL /*static, unused*/, G_B5_1, G_B5_0, /*hidden argument*/NULL);
		String_t* L_22 = String_Concat_m2596409543(NULL /*static, unused*/, G_B5_2, L_21, /*hidden argument*/NULL);
		NullCheck(G_B5_3);
		G_B5_3->set_Text_4(L_22);
		__this->set_webSocket_5((WebSocket_t71448861 *)NULL);
		return;
	}
}
// System.Void WebSocketSample::<OnGUI>m__0()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t71448861_il2cpp_TypeInfo_var;
extern Il2CppClass* HTTPManager_t2983460817_il2cpp_TypeInfo_var;
extern Il2CppClass* HTTPProxy_t2644053826_il2cpp_TypeInfo_var;
extern Il2CppClass* OnWebSocketOpenDelegate_t4018547189_il2cpp_TypeInfo_var;
extern Il2CppClass* OnWebSocketMessageDelegate_t730459590_il2cpp_TypeInfo_var;
extern Il2CppClass* OnWebSocketClosedDelegate_t2679686585_il2cpp_TypeInfo_var;
extern Il2CppClass* OnWebSocketErrorDelegate_t1328789641_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocketSample_OnOpen_m1733835373_MethodInfo_var;
extern const MethodInfo* WebSocketSample_OnMessageReceived_m1722491755_MethodInfo_var;
extern const MethodInfo* WebSocketSample_OnClosed_m2641889363_MethodInfo_var;
extern const MethodInfo* WebSocketSample_OnError_m643242149_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral961687297;
extern Il2CppCodeGenString* _stringLiteral565528789;
extern Il2CppCodeGenString* _stringLiteral4136223372;
extern Il2CppCodeGenString* _stringLiteral12462031;
extern Il2CppCodeGenString* _stringLiteral3033402446;
extern Il2CppCodeGenString* _stringLiteral540678131;
extern const uint32_t WebSocketSample_U3COnGUIU3Em__0_m1212533797_MetadataUsageId;
extern "C"  void WebSocketSample_U3COnGUIU3Em__0_m1212533797 (WebSocketSample_t3966400161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketSample_U3COnGUIU3Em__0_m1212533797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t2243707579  L_0 = __this->get_scrollPos_6();
		Vector2_t2243707579  L_1 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_scrollPos_6(L_1);
		String_t* L_2 = __this->get_Text_4();
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_2, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_Space_m1087863221(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2019304577(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_address_2();
		String_t* L_4 = GUILayout_TextField_m1976649007(NULL /*static, unused*/, L_3, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_address_2(L_4);
		WebSocket_t71448861 * L_5 = __this->get_webSocket_5();
		if (L_5)
		{
			goto IL_017a;
		}
	}
	{
		bool L_6 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral961687297, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_017a;
		}
	}
	{
		String_t* L_7 = __this->get_address_2();
		Uri_t19570940 * L_8 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m1248212436(L_8, L_7, /*hidden argument*/NULL);
		WebSocket_t71448861 * L_9 = (WebSocket_t71448861 *)il2cpp_codegen_object_new(WebSocket_t71448861_il2cpp_TypeInfo_var);
		WebSocket__ctor_m342256434(L_9, L_8, /*hidden argument*/NULL);
		__this->set_webSocket_5(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(HTTPManager_t2983460817_il2cpp_TypeInfo_var);
		HTTPProxy_t2644053826 * L_10 = HTTPManager_get_Proxy_m3066598116(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00bd;
		}
	}
	{
		WebSocket_t71448861 * L_11 = __this->get_webSocket_5();
		NullCheck(L_11);
		HTTPRequest_t138485887 * L_12 = WebSocket_get_InternalRequest_m3005945664(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HTTPManager_t2983460817_il2cpp_TypeInfo_var);
		HTTPProxy_t2644053826 * L_13 = HTTPManager_get_Proxy_m3066598116(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Uri_t19570940 * L_14 = HTTPProxy_get_Address_m1994177776(L_13, /*hidden argument*/NULL);
		HTTPProxy_t2644053826 * L_15 = HTTPManager_get_Proxy_m3066598116(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Credentials_t3762395084 * L_16 = HTTPProxy_get_Credentials_m1367413791(L_15, /*hidden argument*/NULL);
		HTTPProxy_t2644053826 * L_17 = (HTTPProxy_t2644053826 *)il2cpp_codegen_object_new(HTTPProxy_t2644053826_il2cpp_TypeInfo_var);
		HTTPProxy__ctor_m1999728127(L_17, L_14, L_16, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_12);
		HTTPRequest_set_Proxy_m2961271395(L_12, L_17, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		WebSocket_t71448861 * L_18 = __this->get_webSocket_5();
		WebSocket_t71448861 * L_19 = L_18;
		NullCheck(L_19);
		OnWebSocketOpenDelegate_t4018547189 * L_20 = L_19->get_OnOpen_4();
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)WebSocketSample_OnOpen_m1733835373_MethodInfo_var);
		OnWebSocketOpenDelegate_t4018547189 * L_22 = (OnWebSocketOpenDelegate_t4018547189 *)il2cpp_codegen_object_new(OnWebSocketOpenDelegate_t4018547189_il2cpp_TypeInfo_var);
		OnWebSocketOpenDelegate__ctor_m3403249169(L_22, __this, L_21, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_23 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_OnOpen_4(((OnWebSocketOpenDelegate_t4018547189 *)CastclassSealed(L_23, OnWebSocketOpenDelegate_t4018547189_il2cpp_TypeInfo_var)));
		WebSocket_t71448861 * L_24 = __this->get_webSocket_5();
		WebSocket_t71448861 * L_25 = L_24;
		NullCheck(L_25);
		OnWebSocketMessageDelegate_t730459590 * L_26 = L_25->get_OnMessage_5();
		IntPtr_t L_27;
		L_27.set_m_value_0((void*)(void*)WebSocketSample_OnMessageReceived_m1722491755_MethodInfo_var);
		OnWebSocketMessageDelegate_t730459590 * L_28 = (OnWebSocketMessageDelegate_t730459590 *)il2cpp_codegen_object_new(OnWebSocketMessageDelegate_t730459590_il2cpp_TypeInfo_var);
		OnWebSocketMessageDelegate__ctor_m1347557150(L_28, __this, L_27, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_29 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		L_25->set_OnMessage_5(((OnWebSocketMessageDelegate_t730459590 *)CastclassSealed(L_29, OnWebSocketMessageDelegate_t730459590_il2cpp_TypeInfo_var)));
		WebSocket_t71448861 * L_30 = __this->get_webSocket_5();
		WebSocket_t71448861 * L_31 = L_30;
		NullCheck(L_31);
		OnWebSocketClosedDelegate_t2679686585 * L_32 = L_31->get_OnClosed_7();
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)WebSocketSample_OnClosed_m2641889363_MethodInfo_var);
		OnWebSocketClosedDelegate_t2679686585 * L_34 = (OnWebSocketClosedDelegate_t2679686585 *)il2cpp_codegen_object_new(OnWebSocketClosedDelegate_t2679686585_il2cpp_TypeInfo_var);
		OnWebSocketClosedDelegate__ctor_m857571287(L_34, __this, L_33, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_35 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_OnClosed_7(((OnWebSocketClosedDelegate_t2679686585 *)CastclassSealed(L_35, OnWebSocketClosedDelegate_t2679686585_il2cpp_TypeInfo_var)));
		WebSocket_t71448861 * L_36 = __this->get_webSocket_5();
		WebSocket_t71448861 * L_37 = L_36;
		NullCheck(L_37);
		OnWebSocketErrorDelegate_t1328789641 * L_38 = L_37->get_OnError_8();
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)WebSocketSample_OnError_m643242149_MethodInfo_var);
		OnWebSocketErrorDelegate_t1328789641 * L_40 = (OnWebSocketErrorDelegate_t1328789641 *)il2cpp_codegen_object_new(OnWebSocketErrorDelegate_t1328789641_il2cpp_TypeInfo_var);
		OnWebSocketErrorDelegate__ctor_m3137164369(L_40, __this, L_39, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_41 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_37);
		L_37->set_OnError_8(((OnWebSocketErrorDelegate_t1328789641 *)CastclassSealed(L_41, OnWebSocketErrorDelegate_t1328789641_il2cpp_TypeInfo_var)));
		WebSocket_t71448861 * L_42 = __this->get_webSocket_5();
		NullCheck(L_42);
		WebSocket_Open_m2517733755(L_42, /*hidden argument*/NULL);
		String_t* L_43 = __this->get_Text_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2596409543(NULL /*static, unused*/, L_43, _stringLiteral565528789, /*hidden argument*/NULL);
		__this->set_Text_4(L_44);
	}

IL_017a:
	{
		WebSocket_t71448861 * L_45 = __this->get_webSocket_5();
		if (!L_45)
		{
			goto IL_0243;
		}
	}
	{
		WebSocket_t71448861 * L_46 = __this->get_webSocket_5();
		NullCheck(L_46);
		bool L_47 = WebSocket_get_IsOpen_m3184359266(L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0243;
		}
	}
	{
		GUILayout_Space_m1087863221(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		String_t* L_48 = __this->get_msgToSend_3();
		String_t* L_49 = GUILayout_TextField_m1976649007(NULL /*static, unused*/, L_48, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_msgToSend_3(L_49);
		GUILayoutOptionU5BU5D_t2108882777* L_50 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t4183744904 * L_51 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, (70.0f), /*hidden argument*/NULL);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_51);
		bool L_52 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral4136223372, L_50, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_020a;
		}
	}
	{
		String_t* L_53 = __this->get_Text_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m2596409543(NULL /*static, unused*/, L_53, _stringLiteral12462031, /*hidden argument*/NULL);
		__this->set_Text_4(L_54);
		WebSocket_t71448861 * L_55 = __this->get_webSocket_5();
		String_t* L_56 = __this->get_msgToSend_3();
		NullCheck(L_55);
		WebSocket_Send_m2745806833(L_55, L_56, /*hidden argument*/NULL);
	}

IL_020a:
	{
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_Space_m1087863221(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		bool L_57 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral3033402446, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0243;
		}
	}
	{
		WebSocket_t71448861 * L_58 = __this->get_webSocket_5();
		NullCheck(L_58);
		WebSocket_Close_m2415643861(L_58, ((int32_t)1000), _stringLiteral540678131, /*hidden argument*/NULL);
	}

IL_0243:
	{
		return;
	}
}
// System.Void YNContinueButton::.ctor()
extern "C"  void YNContinueButton__ctor_m2412689991 (YNContinueButton_t318956450 * __this, const MethodInfo* method)
{
	{
		NavigateForwardButton__ctor_m95191469(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YNContinueButton::OnButtonClick()
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3COnButtonClickU3Ec__AnonStorey0_t1978033145_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t345140658_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3681841185_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var;
extern const MethodInfo* U3COnButtonClickU3Ec__AnonStorey0_U3CU3Em__0_m1573217692_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1380539338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029319;
extern const uint32_t YNContinueButton_OnButtonClick_m1054378970_MetadataUsageId;
extern "C"  void YNContinueButton_OnButtonClick_m1054378970 (YNContinueButton_t318956450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YNContinueButton_OnButtonClick_m1054378970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * V_0 = NULL;
	{
		bool L_0 = YNContinueButton_ValidateInput_m2344323863(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a1;
		}
	}
	{
		BoxCollider_t22920061 * L_1 = ((SFXButton_t792651341 *)__this)->get_collision_4();
		NullCheck(L_1);
		Collider_set_enabled_m3489100454(L_1, (bool)0, /*hidden argument*/NULL);
		UIInput_t860674234 * L_2 = __this->get_phoneNumberLabel_8();
		NullCheck(L_2);
		String_t* L_3 = UIInput_get_value_m2016753770(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_4 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_4);
		String_t* L_5 = L_4->get_phoneNumber_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_009b;
		}
	}
	{
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_7 = (U3COnButtonClickU3Ec__AnonStorey0_t1978033145 *)il2cpp_codegen_object_new(U3COnButtonClickU3Ec__AnonStorey0_t1978033145_il2cpp_TypeInfo_var);
		U3COnButtonClickU3Ec__AnonStorey0__ctor_m2673723432(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_8 = V_0;
		NullCheck(L_8);
		L_8->set_U24this_1(__this);
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_9 = V_0;
		UILabel_t1795115428 * L_10 = __this->get_countryCodeLabel_7();
		NullCheck(L_10);
		String_t* L_11 = UILabel_get_text_m222864222(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_countyCode_0(L_11);
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_12 = V_0;
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = L_13->get_countyCode_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_14);
		String_t* L_16 = String_Replace_m1941156251(L_14, _stringLiteral372029319, L_15, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_countyCode_0(L_16);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t345140658_il2cpp_TypeInfo_var);
		ChatmageddonServer_t594474938 * L_17 = MonoSingleton_1_get_Instance_m4284870611(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m4284870611_MethodInfo_var);
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_countyCode_0();
		UIInput_t860674234 * L_20 = __this->get_phoneNumberLabel_8();
		NullCheck(L_20);
		String_t* L_21 = UIInput_get_value_m2016753770(L_20, /*hidden argument*/NULL);
		U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * L_22 = V_0;
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)U3COnButtonClickU3Ec__AnonStorey0_U3CU3Em__0_m1573217692_MethodInfo_var);
		Action_3_t3681841185 * L_24 = (Action_3_t3681841185 *)il2cpp_codegen_object_new(Action_3_t3681841185_il2cpp_TypeInfo_var);
		Action_3__ctor_m1380539338(L_24, L_22, L_23, /*hidden argument*/Action_3__ctor_m1380539338_MethodInfo_var);
		NullCheck(L_17);
		ChatmageddonServer_SubmitMobileNumber_m249974985(L_17, L_19, L_21, L_24, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_009b:
	{
		NavigateForwardButton_OnButtonClick_m263286112(__this, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		return;
	}
}
// System.Boolean YNContinueButton::ValidateInput()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t1803876613_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1572632425;
extern Il2CppCodeGenString* _stringLiteral987970346;
extern Il2CppCodeGenString* _stringLiteral1515674420;
extern const uint32_t YNContinueButton_ValidateInput_m2344323863_MetadataUsageId;
extern "C"  bool YNContinueButton_ValidateInput_m2344323863 (YNContinueButton_t318956450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YNContinueButton_ValidateInput_m2344323863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Regex_t1803876613 * V_1 = NULL;
	MatchCollection_t3718216671 * V_2 = NULL;
	{
		UIInput_t860674234 * L_0 = __this->get_phoneNumberLabel_8();
		NullCheck(L_0);
		String_t* L_1 = UIInput_get_value_m2016753770(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0074;
		}
	}
	{
		V_0 = _stringLiteral1572632425;
		String_t* L_4 = V_0;
		Regex_t1803876613 * L_5 = (Regex_t1803876613 *)il2cpp_codegen_object_new(Regex_t1803876613_il2cpp_TypeInfo_var);
		Regex__ctor_m2521903438(L_5, L_4, 1, /*hidden argument*/NULL);
		V_1 = L_5;
		Regex_t1803876613 * L_6 = V_1;
		UILabel_t1795115428 * L_7 = __this->get_countryCodeLabel_7();
		NullCheck(L_7);
		String_t* L_8 = UILabel_get_text_m222864222(L_7, /*hidden argument*/NULL);
		UIInput_t860674234 * L_9 = __this->get_phoneNumberLabel_8();
		NullCheck(L_9);
		String_t* L_10 = UIInput_get_value_m2016753770(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		MatchCollection_t3718216671 * L_12 = Regex_Matches_m3834294444(L_6, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		MatchCollection_t3718216671 * L_13 = V_2;
		NullCheck(L_13);
		int32_t L_14 = MatchCollection_get_Count_m3236470266(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		return (bool)1;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_15 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Nullable_1__ctor_m1978009879(&L_16, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_15);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_15, _stringLiteral987970346, L_16, (bool)0, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0074:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_17 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		Nullable_1_t339576247  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Nullable_1__ctor_m1978009879(&L_18, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_17);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_17, _stringLiteral1515674420, L_18, (bool)0, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void YNContinueButton::<OnButtonClick>__BaseCallProxy0()
extern "C"  void YNContinueButton_U3COnButtonClickU3E__BaseCallProxy0_m3592997299 (YNContinueButton_t318956450 * __this, const MethodInfo* method)
{
	{
		NavigateForwardButton_OnButtonClick_m263286112(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YNContinueButton/<OnButtonClick>c__AnonStorey0::.ctor()
extern "C"  void U3COnButtonClickU3Ec__AnonStorey0__ctor_m2673723432 (U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YNContinueButton/<OnButtonClick>c__AnonStorey0::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern Il2CppClass* MonoSingleton_1_t2136529692_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t962131611_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2166063373_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m8922380_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral2446688449;
extern Il2CppCodeGenString* _stringLiteral1272573477;
extern Il2CppCodeGenString* _stringLiteral339799447;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t U3COnButtonClickU3Ec__AnonStorey0_U3CU3Em__0_m1573217692_MetadataUsageId;
extern "C"  void U3COnButtonClickU3Ec__AnonStorey0_U3CU3Em__0_m1573217692 (U3COnButtonClickU3Ec__AnonStorey0_t1978033145 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3COnButtonClickU3Ec__AnonStorey0_U3CU3Em__0_m1573217692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	String_t* V_1 = NULL;
	Hashtable_t909839986 * V_2 = NULL;
	{
		bool L_0 = ___success0;
		if (!L_0)
		{
			goto IL_010b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2136529692_il2cpp_TypeInfo_var);
		FlurryController_t2385863972 * L_1 = MonoSingleton_1_get_Instance_m2166063373(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2166063373_MethodInfo_var);
		NullCheck(L_1);
		FlurryController_SendEvent_m2773676096(L_1, 1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_2 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		String_t* L_3 = __this->get_countyCode_0();
		NullCheck(L_2);
		L_2->set_countryCode_7(L_3);
		RegistrationManager_t1533517116 * L_4 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		YNContinueButton_t318956450 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		UIInput_t860674234 * L_6 = L_5->get_phoneNumberLabel_8();
		NullCheck(L_6);
		String_t* L_7 = UIInput_get_value_m2016753770(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_phoneNumber_8(L_7);
		RegistrationManager_t1533517116 * L_8 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		YNContinueButton_t318956450 * L_9 = __this->get_U24this_1();
		NullCheck(L_9);
		UILabel_t1795115428 * L_10 = L_9->get_countryCodeLabel_7();
		NullCheck(L_10);
		String_t* L_11 = UILabel_get_text_m222864222(L_10, /*hidden argument*/NULL);
		YNContinueButton_t318956450 * L_12 = __this->get_U24this_1();
		NullCheck(L_12);
		UIInput_t860674234 * L_13 = L_12->get_phoneNumberLabel_8();
		NullCheck(L_13);
		String_t* L_14 = UIInput_get_value_m2016753770(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m612901809(NULL /*static, unused*/, L_11, _stringLiteral372029310, L_14, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_fullPhoneNumber_9(L_15);
		RegistrationManager_t1533517116 * L_16 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_16);
		L_16->set_numberVerified_4((bool)0);
		Hashtable_t909839986 * L_17 = ___info1;
		V_0 = L_17;
		Hashtable_t909839986 * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_18, _stringLiteral2446688449);
		if (!L_19)
		{
			goto IL_00a7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_20 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		Hashtable_t909839986 * L_21 = V_0;
		NullCheck(L_21);
		Il2CppObject * L_22 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_21, _stringLiteral2446688449);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_20);
		L_20->set_mobileHash_10(L_23);
	}

IL_00a7:
	{
		Hashtable_t909839986 * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_24, _stringLiteral1272573477);
		if (!L_25)
		{
			goto IL_00d1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_26 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		Hashtable_t909839986 * L_27 = V_0;
		NullCheck(L_27);
		Il2CppObject * L_28 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_27, _stringLiteral1272573477);
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_28);
		NullCheck(L_26);
		L_26->set_mobilePinID_11(L_29);
	}

IL_00d1:
	{
		Hashtable_t909839986 * L_30 = V_0;
		NullCheck(L_30);
		bool L_31 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_30, _stringLiteral339799447);
		if (!L_31)
		{
			goto IL_00fb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_32 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		Hashtable_t909839986 * L_33 = V_0;
		NullCheck(L_33);
		Il2CppObject * L_34 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_33, _stringLiteral339799447);
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		NullCheck(L_32);
		L_32->set_mobilePin_12(L_35);
	}

IL_00fb:
	{
		YNContinueButton_t318956450 * L_36 = __this->get_U24this_1();
		NullCheck(L_36);
		YNContinueButton_U3COnButtonClickU3E__BaseCallProxy0_m3592997299(L_36, /*hidden argument*/NULL);
		goto IL_0165;
	}

IL_010b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_37;
		String_t* L_38 = ___errorCode2;
		Il2CppObject * L_39 = JSON_JsonDecode_m1043082852(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		V_2 = ((Hashtable_t909839986 *)CastclassClass(L_39, Hashtable_t909839986_il2cpp_TypeInfo_var));
		Hashtable_t909839986 * L_40 = V_2;
		NullCheck(L_40);
		bool L_41 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_40, _stringLiteral359429027);
		if (!L_41)
		{
			goto IL_013e;
		}
	}
	{
		Hashtable_t909839986 * L_42 = V_2;
		NullCheck(L_42);
		Il2CppObject * L_43 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_42, _stringLiteral359429027);
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_43);
		V_1 = L_44;
	}

IL_013e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t962131611_il2cpp_TypeInfo_var);
		ErrorMessagePopUp_t1211465891 * L_45 = MonoSingleton_1_get_Instance_m8922380(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m8922380_MethodInfo_var);
		String_t* L_46 = V_1;
		Nullable_1_t339576247  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Nullable_1__ctor_m1978009879(&L_47, (2.0f), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		NullCheck(L_45);
		ErrorMessagePopUp_LoadErrorMessage_m3418110718(L_45, L_46, L_47, (bool)0, /*hidden argument*/NULL);
		YNContinueButton_t318956450 * L_48 = __this->get_U24this_1();
		NullCheck(L_48);
		BoxCollider_t22920061 * L_49 = ((SFXButton_t792651341 *)L_48)->get_collision_4();
		NullCheck(L_49);
		Collider_set_enabled_m3489100454(L_49, (bool)1, /*hidden argument*/NULL);
	}

IL_0165:
	{
		return;
	}
}
// System.Void YourNumberScreen::.ctor()
extern "C"  void YourNumberScreen__ctor_m1100232789 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method)
{
	{
		NavigationScreen__ctor_m2982757537(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YourNumberScreen::Start()
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const MethodInfo* YourNumberScreen_RegisterUIClose_m763565588_MethodInfo_var;
extern const MethodInfo* YourNumberScreen_YourNumberUIClosing_m184456198_MethodInfo_var;
extern const uint32_t YourNumberScreen_Start_m2226407389_MetadataUsageId;
extern "C"  void YourNumberScreen_Start_m2226407389 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YourNumberScreen_Start_m2226407389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_contentObject_3();
		NullCheck(L_0);
		TweenPosition_t1144714832 * L_1 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_0, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)YourNumberScreen_RegisterUIClose_m763565588_MethodInfo_var);
		Callback_t2100910411 * L_3 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		UITweener_AddOnFinished_m3855136488(L_1, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		TweenPosition_t1144714832 * L_5 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_4, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)YourNumberScreen_YourNumberUIClosing_m184456198_MethodInfo_var);
		Callback_t2100910411 * L_7 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UITweener_AddOnFinished_m3855136488(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YourNumberScreen::RegisterUIClose()
extern Il2CppClass* MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2078169705_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m597098886_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895_MethodInfo_var;
extern const uint32_t YourNumberScreen_RegisterUIClose_m763565588_MetadataUsageId;
extern "C"  void YourNumberScreen_RegisterUIClose_m763565588 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YourNumberScreen_RegisterUIClose_m763565588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_registerUIclosing_6();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_contentObject_3();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		__this->set_registerUIclosing_6((bool)0);
		goto IL_0057;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1284182836_il2cpp_TypeInfo_var);
		RegistrationManager_t1533517116 * L_2 = MonoSingleton_1_get_Instance_m3164489833(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3164489833_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = L_2->get_regComplete_29();
		if (L_3)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_4 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_startPage_11();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2078169705_il2cpp_TypeInfo_var);
		LoginRegistrationSceneManager_t2327503985 * L_6 = MonoSingleton_1_get_Instance_m597098886(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m597098886_MethodInfo_var);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_LoginCenterUI_19();
		NullCheck(L_7);
		LoginCenterUI_t890505802 * L_8 = GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895(L_7, /*hidden argument*/GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895_MethodInfo_var);
		NullCheck(L_8);
		LoginCenterUI_SetBackgroundActive_m3399614538(L_8, (bool)0, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void YourNumberScreen::YourNumberUIClosing()
extern "C"  void YourNumberScreen_YourNumberUIClosing_m184456198 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_yourNumUIclosing_7();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_yourNumUIclosing_7((bool)0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void YourNumberScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const uint32_t YourNumberScreen_ScreenLoad_m1414592691_MetadataUsageId;
extern "C"  void YourNumberScreen_ScreenLoad_m1414592691 (YourNumberScreen_t2760251358 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YourNumberScreen_ScreenLoad_m1414592691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		YNContinueButton_t318956450 * L_0 = __this->get_continueButton_5();
		NullCheck(L_0);
		BoxCollider_t22920061 * L_1 = ((SFXButton_t792651341 *)L_0)->get_collision_4();
		NullCheck(L_1);
		Collider_set_enabled_m3489100454(L_1, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_2 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_2);
		NavigateForwardButton_t3075171100 * L_3 = L_2->get_mainNavigateForwardButton_18();
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void SFXButton::SetButtonActive(System.Boolean) */, L_3, (bool)1);
		int32_t L_4 = ___direction0;
		if (L_4)
		{
			goto IL_006c;
		}
	}
	{
		__this->set_yourNumUIclosing_7((bool)0);
		GameObject_t1756533147 * L_5 = __this->get_contentObject_3();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_contentObject_3();
		NullCheck(L_6);
		TweenPosition_t1144714832 * L_7 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_6, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_7);
		Behaviour_set_enabled_m1796096907(L_7, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_contentObject_3();
		NullCheck(L_8);
		TweenPosition_t1144714832 * L_9 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_8, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_9);
		UITweener_PlayForward_m3772341562(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_00a7;
	}

IL_006c:
	{
		int32_t L_11 = ___direction0;
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_00a7;
		}
	}
	{
		__this->set_yourNumUIclosing_7((bool)0);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		TweenPosition_t1144714832 * L_14 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_13, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_14);
		Behaviour_set_enabled_m1796096907(L_14, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		TweenPosition_t1144714832 * L_16 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_15, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_16);
		UITweener_PlayReverse_m4027828489(L_16, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		return;
	}
}
// System.Void YourNumberScreen::ScreenUnload(NavigationDirection)
extern Il2CppClass* MonoSingleton_1_t290431989_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3786716596_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2078169705_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3183586707_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m597098886_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895_MethodInfo_var;
extern const uint32_t YourNumberScreen_ScreenUnload_m2285231787_MetadataUsageId;
extern "C"  void YourNumberScreen_ScreenUnload_m2285231787 (YourNumberScreen_t2760251358 * __this, int32_t ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YourNumberScreen_ScreenUnload_m2285231787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___direction0;
		if (L_0)
		{
			goto IL_0067;
		}
	}
	{
		__this->set_yourNumUIclosing_7((bool)1);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		TweenPosition_t1144714832 * L_2 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_1, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_3 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = L_3->get_activePos_8();
		NullCheck(L_2);
		L_2->set_from_20(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		TweenPosition_t1144714832 * L_6 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_5, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_7 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = L_7->get_leftInactivePos_9();
		NullCheck(L_6);
		L_6->set_to_21(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		TweenPosition_t1144714832 * L_10 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_9, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_10);
		Behaviour_set_enabled_m1796096907(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		TweenPosition_t1144714832 * L_12 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_11, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_12);
		UITweener_PlayForward_m3772341562(L_12, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_0067:
	{
		int32_t L_13 = ___direction0;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_00c6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3786716596_il2cpp_TypeInfo_var);
		ChatmageddonSaveData_t4036050876 * L_14 = MonoSingleton_1_get_Instance_m3183586707(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3183586707_MethodInfo_var);
		NullCheck(L_14);
		ChatmageddonSaveData_SaveRegistrationSetting_m413524098(L_14, (bool)0, /*hidden argument*/NULL);
		__this->set_registerUIclosing_6((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t290431989_il2cpp_TypeInfo_var);
		RegistrationNavigationcontroller_t539766269 * L_15 = MonoSingleton_1_get_Instance_m2660067994(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2660067994_MethodInfo_var);
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = L_15->get_startPage_11();
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2078169705_il2cpp_TypeInfo_var);
		LoginRegistrationSceneManager_t2327503985 * L_17 = MonoSingleton_1_get_Instance_m597098886(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m597098886_MethodInfo_var);
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = L_17->get_LoginCenterUI_19();
		NullCheck(L_18);
		LoginCenterUI_t890505802 * L_19 = GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895(L_18, /*hidden argument*/GameObject_GetComponent_TisLoginCenterUI_t890505802_m1644260895_MethodInfo_var);
		NullCheck(L_19);
		LoginCenterUI_SetBackgroundActive_m3399614538(L_19, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = __this->get_contentObject_3();
		NullCheck(L_20);
		TweenPosition_t1144714832 * L_21 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_20, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_21);
		Behaviour_set_enabled_m1796096907(L_21, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = __this->get_contentObject_3();
		NullCheck(L_22);
		TweenPosition_t1144714832 * L_23 = GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171(L_22, /*hidden argument*/GameObject_GetComponent_TisTweenPosition_t1144714832_m1577603171_MethodInfo_var);
		NullCheck(L_23);
		UITweener_PlayReverse_m4027828489(L_23, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Boolean YourNumberScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool YourNumberScreen_ValidateScreenNavigation_m4236459530 (YourNumberScreen_t2760251358 * __this, int32_t ___direction0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void YourNumberUIScaler::.ctor()
extern "C"  void YourNumberUIScaler__ctor_m2610499143 (YourNumberUIScaler_t1339804644 * __this, const MethodInfo* method)
{
	{
		ChatmageddonUIScaler__ctor_m1032172578(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YourNumberUIScaler::GlobalUIScale()
extern Il2CppClass* MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var;
extern const uint32_t YourNumberUIScaler_GlobalUIScale_m3177150830_MetadataUsageId;
extern "C"  void YourNumberUIScaler_GlobalUIScale_m3177150830 (YourNumberUIScaler_t1339804644 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YourNumberUIScaler_GlobalUIScale_m3177150830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	{
		UISprite_t603616735 * L_0 = __this->get_topDivider_14();
		float L_1 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_0);
		UIWidget_set_width_m1785998957(L_0, (((int32_t)((int32_t)L_1))), /*hidden argument*/NULL);
		UISprite_t603616735 * L_2 = __this->get_middleDivider_15();
		float L_3 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_2);
		UIWidget_set_width_m1785998957(L_2, (((int32_t)((int32_t)L_3))), /*hidden argument*/NULL);
		UISprite_t603616735 * L_4 = __this->get_bottomDivider_16();
		float L_5 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_4);
		UIWidget_set_width_m1785998957(L_4, (((int32_t)((int32_t)L_5))), /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_6 = __this->get_continueButtonCollider_17();
		float L_7 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_8 = __this->get_continueButtonCollider_17();
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = BoxCollider_get_size_m201254358(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = (&V_0)->get_y_2();
		BoxCollider_t22920061 * L_11 = __this->get_continueButtonCollider_17();
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = BoxCollider_get_size_m201254358(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = (&V_1)->get_z_3();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_7, L_10, L_13, /*hidden argument*/NULL);
		NullCheck(L_6);
		BoxCollider_set_size_m4101048759(L_6, L_14, /*hidden argument*/NULL);
		UISprite_t603616735 * L_15 = __this->get_continueButton_18();
		float L_16 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_15);
		UIWidget_set_width_m1785998957(L_15, (((int32_t)((int32_t)L_16))), /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_17 = __this->get_countryPickerCollider_19();
		float L_18 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		BoxCollider_t22920061 * L_19 = __this->get_countryPickerCollider_19();
		NullCheck(L_19);
		Vector3_t2243707580  L_20 = BoxCollider_get_size_m201254358(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = (&V_2)->get_y_2();
		BoxCollider_t22920061 * L_22 = __this->get_countryPickerCollider_19();
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = BoxCollider_get_size_m201254358(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		float L_24 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322(&L_25, L_18, L_21, L_24, /*hidden argument*/NULL);
		NullCheck(L_17);
		BoxCollider_set_size_m4101048759(L_17, L_25, /*hidden argument*/NULL);
		UISprite_t603616735 * L_26 = __this->get_countryPickerButton_20();
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var);
		HomeSceneManager_t2690266116 * L_28 = MonoSingleton_1_get_Instance_m2687909429(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var);
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = ((BaseSceneManager_1_t2069092468 *)L_28)->get_rightAnchor_13();
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = GameObject_get_transform_m909382139(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_localPosition_m2533925116(L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		float L_32 = (&V_4)->get_x_1();
		UISprite_t603616735 * L_33 = __this->get_countryPickerButton_20();
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_localPosition_m2533925116(L_34, /*hidden argument*/NULL);
		V_5 = L_35;
		float L_36 = (&V_5)->get_y_2();
		UISprite_t603616735 * L_37 = __this->get_countryPickerButton_20();
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_localPosition_m2533925116(L_38, /*hidden argument*/NULL);
		V_6 = L_39;
		float L_40 = (&V_6)->get_z_3();
		Vector3_t2243707580  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector3__ctor_m2638739322(&L_41, ((float)((float)L_32-(float)(46.0f))), L_36, L_40, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localPosition_m1026930133(L_27, L_41, /*hidden argument*/NULL);
		UILabel_t1795115428 * L_42 = __this->get_countryPickerLabel_21();
		float L_43 = ((BaseUIScaler_t568866473 *)__this)->get_calcScaleWidth_9();
		NullCheck(L_42);
		UIWidget_set_width_m1785998957(L_42, (((int32_t)((int32_t)((double)((double)(((double)((double)L_43)))*(double)(0.93)))))), /*hidden argument*/NULL);
		UILabel_t1795115428 * L_44 = __this->get_countryCodeLabel_22();
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = Component_get_transform_m2697483695(L_44, /*hidden argument*/NULL);
		HomeSceneManager_t2690266116 * L_46 = MonoSingleton_1_get_Instance_m2687909429(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var);
		NullCheck(L_46);
		GameObject_t1756533147 * L_47 = ((BaseSceneManager_1_t2069092468 *)L_46)->get_leftAnchor_11();
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = GameObject_get_transform_m909382139(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t2243707580  L_49 = Transform_get_localPosition_m2533925116(L_48, /*hidden argument*/NULL);
		V_7 = L_49;
		float L_50 = (&V_7)->get_x_1();
		UILabel_t1795115428 * L_51 = __this->get_countryCodeLabel_22();
		NullCheck(L_51);
		int32_t L_52 = UIWidget_get_width_m1323147272(L_51, /*hidden argument*/NULL);
		UILabel_t1795115428 * L_53 = __this->get_countryCodeLabel_22();
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = Component_get_transform_m2697483695(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		Vector3_t2243707580  L_55 = Transform_get_localPosition_m2533925116(L_54, /*hidden argument*/NULL);
		V_8 = L_55;
		float L_56 = (&V_8)->get_y_2();
		UILabel_t1795115428 * L_57 = __this->get_countryCodeLabel_22();
		NullCheck(L_57);
		Transform_t3275118058 * L_58 = Component_get_transform_m2697483695(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t2243707580  L_59 = Transform_get_localPosition_m2533925116(L_58, /*hidden argument*/NULL);
		V_9 = L_59;
		float L_60 = (&V_9)->get_z_3();
		Vector3_t2243707580  L_61;
		memset(&L_61, 0, sizeof(L_61));
		Vector3__ctor_m2638739322(&L_61, ((float)((float)((float)((float)L_50+(float)(((float)((float)((int32_t)((int32_t)L_52/(int32_t)2)))))))+(float)(2.0f))), L_56, L_60, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_localPosition_m1026930133(L_45, L_61, /*hidden argument*/NULL);
		UISprite_t603616735 * L_62 = __this->get_countryCodeSeperator_23();
		NullCheck(L_62);
		Transform_t3275118058 * L_63 = Component_get_transform_m2697483695(L_62, /*hidden argument*/NULL);
		UILabel_t1795115428 * L_64 = __this->get_countryCodeLabel_22();
		NullCheck(L_64);
		Transform_t3275118058 * L_65 = Component_get_transform_m2697483695(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		Vector3_t2243707580  L_66 = Transform_get_localPosition_m2533925116(L_65, /*hidden argument*/NULL);
		V_10 = L_66;
		float L_67 = (&V_10)->get_x_1();
		UILabel_t1795115428 * L_68 = __this->get_countryCodeLabel_22();
		NullCheck(L_68);
		int32_t L_69 = UIWidget_get_width_m1323147272(L_68, /*hidden argument*/NULL);
		UISprite_t603616735 * L_70 = __this->get_countryCodeSeperator_23();
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = Component_get_transform_m2697483695(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		Vector3_t2243707580  L_72 = Transform_get_localPosition_m2533925116(L_71, /*hidden argument*/NULL);
		V_11 = L_72;
		float L_73 = (&V_11)->get_y_2();
		UISprite_t603616735 * L_74 = __this->get_countryCodeSeperator_23();
		NullCheck(L_74);
		Transform_t3275118058 * L_75 = Component_get_transform_m2697483695(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		Vector3_t2243707580  L_76 = Transform_get_localPosition_m2533925116(L_75, /*hidden argument*/NULL);
		V_12 = L_76;
		float L_77 = (&V_12)->get_z_3();
		Vector3_t2243707580  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m2638739322(&L_78, ((float)((float)L_67+(float)(((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_69/(int32_t)2))+(int32_t)6))))))), L_73, L_77, /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_set_localPosition_m1026930133(L_63, L_78, /*hidden argument*/NULL);
		BoxCollider_t22920061 * L_79 = __this->get_phoneNumberCollider_24();
		NullCheck(L_79);
		Transform_t3275118058 * L_80 = Component_get_transform_m2697483695(L_79, /*hidden argument*/NULL);
		UISprite_t603616735 * L_81 = __this->get_countryCodeSeperator_23();
		NullCheck(L_81);
		Transform_t3275118058 * L_82 = Component_get_transform_m2697483695(L_81, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t2243707580  L_83 = Transform_get_localPosition_m2533925116(L_82, /*hidden argument*/NULL);
		V_13 = L_83;
		float L_84 = (&V_13)->get_x_1();
		BoxCollider_t22920061 * L_85 = __this->get_phoneNumberCollider_24();
		NullCheck(L_85);
		Vector3_t2243707580  L_86 = BoxCollider_get_size_m201254358(L_85, /*hidden argument*/NULL);
		V_14 = L_86;
		float L_87 = (&V_14)->get_x_1();
		BoxCollider_t22920061 * L_88 = __this->get_phoneNumberCollider_24();
		NullCheck(L_88);
		Vector3_t2243707580  L_89 = BoxCollider_get_center_m2103454062(L_88, /*hidden argument*/NULL);
		V_15 = L_89;
		float L_90 = (&V_15)->get_x_1();
		BoxCollider_t22920061 * L_91 = __this->get_phoneNumberCollider_24();
		NullCheck(L_91);
		Transform_t3275118058 * L_92 = Component_get_transform_m2697483695(L_91, /*hidden argument*/NULL);
		NullCheck(L_92);
		Vector3_t2243707580  L_93 = Transform_get_localPosition_m2533925116(L_92, /*hidden argument*/NULL);
		V_16 = L_93;
		float L_94 = (&V_16)->get_y_2();
		BoxCollider_t22920061 * L_95 = __this->get_phoneNumberCollider_24();
		NullCheck(L_95);
		Transform_t3275118058 * L_96 = Component_get_transform_m2697483695(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		Vector3_t2243707580  L_97 = Transform_get_localPosition_m2533925116(L_96, /*hidden argument*/NULL);
		V_17 = L_97;
		float L_98 = (&V_17)->get_z_3();
		Vector3_t2243707580  L_99;
		memset(&L_99, 0, sizeof(L_99));
		Vector3__ctor_m2638739322(&L_99, ((float)((float)L_84+(float)((float)((float)((float)((float)L_87/(float)(2.0f)))-(float)L_90)))), L_94, L_98, /*hidden argument*/NULL);
		NullCheck(L_80);
		Transform_set_localPosition_m1026930133(L_80, L_99, /*hidden argument*/NULL);
		BaseUIScaler_GlobalUIScale_m2469894115(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::.ctor()
extern "C"  void ZeroPointsUI__ctor_m2510471974 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	{
		LockoutUI__ctor_m3681405870(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::OnEnable()
extern Il2CppClass* OnZeroHit_t3971658986_il2cpp_TypeInfo_var;
extern const MethodInfo* ZeroPointsUI_CalculatorHitZero_m1398515437_MethodInfo_var;
extern const uint32_t ZeroPointsUI_OnEnable_m1289514338_MetadataUsageId;
extern "C"  void ZeroPointsUI_OnEnable_m1289514338 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZeroPointsUI_OnEnable_m1289514338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NoPointsWaitButton_t1249750581 * L_0 = __this->get_waitTimer_6();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)ZeroPointsUI_CalculatorHitZero_m1398515437_MethodInfo_var);
		OnZeroHit_t3971658986 * L_2 = (OnZeroHit_t3971658986 *)il2cpp_codegen_object_new(OnZeroHit_t3971658986_il2cpp_TypeInfo_var);
		OnZeroHit__ctor_m994361025(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		NoPointsWaitButton_add_onZeroHit_m1102814586(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::OnDisable()
extern Il2CppClass* OnZeroHit_t3971658986_il2cpp_TypeInfo_var;
extern const MethodInfo* ZeroPointsUI_CalculatorHitZero_m1398515437_MethodInfo_var;
extern const uint32_t ZeroPointsUI_OnDisable_m1481135407_MetadataUsageId;
extern "C"  void ZeroPointsUI_OnDisable_m1481135407 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZeroPointsUI_OnDisable_m1481135407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NoPointsWaitButton_t1249750581 * L_0 = __this->get_waitTimer_6();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)ZeroPointsUI_CalculatorHitZero_m1398515437_MethodInfo_var);
		OnZeroHit_t3971658986 * L_2 = (OnZeroHit_t3971658986 *)il2cpp_codegen_object_new(OnZeroHit_t3971658986_il2cpp_TypeInfo_var);
		OnZeroHit__ctor_m994361025(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		NoPointsWaitButton_remove_onZeroHit_m3018186249(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::Awake()
extern Il2CppClass* Callback_t2100910411_il2cpp_TypeInfo_var;
extern const MethodInfo* ZeroPointsUI_BackgroundTweenFinished_m2129898799_MethodInfo_var;
extern const MethodInfo* ZeroPointsUI_ContentTweenFinished_m997736054_MethodInfo_var;
extern const uint32_t ZeroPointsUI_Awake_m1881537779_MetadataUsageId;
extern "C"  void ZeroPointsUI_Awake_m1881537779 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZeroPointsUI_Awake_m1881537779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TweenAlpha_t2421518635 * L_0 = __this->get_backgroundTween_3();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)ZeroPointsUI_BackgroundTweenFinished_m2129898799_MethodInfo_var);
		Callback_t2100910411 * L_2 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UITweener_AddOnFinished_m3855136488(L_0, L_2, /*hidden argument*/NULL);
		TweenScale_t2697902175 * L_3 = __this->get_contentTween_4();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)ZeroPointsUI_ContentTweenFinished_m997736054_MethodInfo_var);
		Callback_t2100910411 * L_5 = (Callback_t2100910411 *)il2cpp_codegen_object_new(Callback_t2100910411_il2cpp_TypeInfo_var);
		Callback__ctor_m2698878814(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		UITweener_AddOnFinished_m3855136488(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::CalculatorHitZero()
extern "C"  void ZeroPointsUI_CalculatorHitZero_m1398515437 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ZeroPointsUI::SetKnockedOutByLabel()
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2018909202;
extern const uint32_t ZeroPointsUI_SetKnockedOutByLabel_m4226317228_MetadataUsageId;
extern "C"  void ZeroPointsUI_SetKnockedOutByLabel_m4226317228 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZeroPointsUI_SetKnockedOutByLabel_m4226317228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_0 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_0);
		Player_t1147783557 * L_1 = L_0->get_user_3();
		NullCheck(L_1);
		String_t* L_2 = ((User_t719925459 *)L_1)->get_knockedOutByName_30();
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		UILabel_t1795115428 * L_3 = __this->get_attackerNameLabel_5();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_4 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_4);
		Player_t1147783557 * L_5 = L_4->get_user_3();
		NullCheck(L_5);
		String_t* L_6 = ((User_t719925459 *)L_5)->get_knockedOutByName_30();
		NullCheck(L_6);
		String_t* L_7 = String_ToUpper_m3715743312(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, L_7, _stringLiteral2018909202, /*hidden argument*/NULL);
		NullCheck(L_3);
		UILabel_set_text_m451064939(L_3, L_8, /*hidden argument*/NULL);
		UILabel_t1795115428 * L_9 = __this->get_attackerNameLabel_5();
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_0053:
	{
		UILabel_t1795115428 * L_11 = __this->get_attackerNameLabel_5();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)0, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void ZeroPointsUI::LoadUI(System.Boolean)
extern Il2CppClass* MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var;
extern const uint32_t ZeroPointsUI_LoadUI_m1572827343_MetadataUsageId;
extern "C"  void ZeroPointsUI_LoadUI_m1572827343 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZeroPointsUI_LoadUI_m1572827343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t3430258949  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_0 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_0);
		ChatmageddonPanelTransitionManager_HideBottomUI_m1609030635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		bool L_1 = MonoSingleton_1_IsAvailable_m2547723652(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		MapManager_t3593696545 * L_2 = MonoSingleton_1_get_Instance_m3809739556(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var);
		NullCheck(L_2);
		MapManager_SetMapUIActive_m4077652589(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001f:
	{
		((LockoutUI_t1337294023 *)__this)->set_unloading_2((bool)0);
		ZeroPointsUI_SetKnockedOutByLabel_m4226317228(__this, /*hidden argument*/NULL);
		bool L_3 = ___instant0;
		VirtActionInvoker2< bool, bool >::Invoke(6 /* System.Void LockoutUI::ShowUI(System.Boolean,System.Boolean) */, __this, L_3, (bool)1);
		NoPointsWaitButton_t1249750581 * L_4 = __this->get_waitTimer_6();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_5 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_5);
		Player_t1147783557 * L_6 = L_5->get_user_3();
		NullCheck(L_4);
		TimeSpan_t3430258949  L_7 = NoPointsWaitButton_GetKnockoutTimeSpan_m2052793211(L_4, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = TimeSpan_get_Hours_m3804628138((&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_9 = TimeSpan_get_Minutes_m2575030536((&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_10 = TimeSpan_get_Seconds_m3108432552((&V_0), /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10+(int32_t)1))) > ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		NoPointsWaitButton_t1249750581 * L_11 = __this->get_waitTimer_6();
		NullCheck(L_11);
		NoPointsWaitButton_ShowExitButton_m957422178(L_11, (bool)1, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0084:
	{
		NoPointsWaitButton_t1249750581 * L_12 = __this->get_waitTimer_6();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1347319308_il2cpp_TypeInfo_var);
		PlayerManager_t1596653588 * L_13 = MonoSingleton_1_get_Instance_m3629440119(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3629440119_MethodInfo_var);
		NullCheck(L_13);
		Player_t1147783557 * L_14 = L_13->get_user_3();
		NullCheck(L_12);
		NoPointsWaitButton_StartCalculator_m1858778849(L_12, L_14, /*hidden argument*/NULL);
	}

IL_0099:
	{
		bool L_15 = ___instant0;
		LockoutUI_LoadUI_m2697867739(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::UnloadUI(System.Boolean)
extern "C"  void ZeroPointsUI_UnloadUI_m2092964220 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, const MethodInfo* method)
{
	{
		((LockoutUI_t1337294023 *)__this)->set_unloading_2((bool)1);
		bool L_0 = ___instant0;
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void LockoutUI::HideUI(System.Boolean) */, __this, L_0);
		bool L_1 = ___instant0;
		LockoutUI_UnloadUI_m4060370304(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::ShowUI(System.Boolean,System.Boolean)
extern "C"  void ZeroPointsUI_ShowUI_m4104334781 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, bool ___reset1, const MethodInfo* method)
{
	{
		bool L_0 = ___instant0;
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		TweenAlpha_t2421518635 * L_1 = __this->get_backgroundTween_3();
		NullCheck(L_1);
		UITweener_set_tweenFactor_m1495142921(L_1, (1.0f), /*hidden argument*/NULL);
		TweenScale_t2697902175 * L_2 = __this->get_contentTween_4();
		NullCheck(L_2);
		UITweener_set_tweenFactor_m1495142921(L_2, (1.0f), /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_002b:
	{
		bool L_3 = ___reset1;
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		TweenAlpha_t2421518635 * L_4 = __this->get_backgroundTween_3();
		NullCheck(L_4);
		UITweener_ResetToBeginning_m1313982072(L_4, /*hidden argument*/NULL);
		TweenScale_t2697902175 * L_5 = __this->get_contentTween_4();
		NullCheck(L_5);
		UITweener_ResetToBeginning_m1313982072(L_5, /*hidden argument*/NULL);
	}

IL_0047:
	{
		TweenAlpha_t2421518635 * L_6 = __this->get_backgroundTween_3();
		NullCheck(L_6);
		((UITweener_t2986641582 *)L_6)->set_delay_7((0.0f));
		TweenScale_t2697902175 * L_7 = __this->get_contentTween_4();
		TweenAlpha_t2421518635 * L_8 = __this->get_backgroundTween_3();
		NullCheck(L_8);
		float L_9 = ((UITweener_t2986641582 *)L_8)->get_duration_8();
		NullCheck(L_7);
		((UITweener_t2986641582 *)L_7)->set_delay_7(L_9);
	}

IL_006d:
	{
		TweenAlpha_t2421518635 * L_10 = __this->get_backgroundTween_3();
		NullCheck(L_10);
		UITweener_PlayForward_m3772341562(L_10, /*hidden argument*/NULL);
		TweenScale_t2697902175 * L_11 = __this->get_contentTween_4();
		NullCheck(L_11);
		UITweener_PlayForward_m3772341562(L_11, /*hidden argument*/NULL);
		bool L_12 = ___instant0;
		LockoutUI_ShowUI_m815079381(__this, L_12, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::HideUI(System.Boolean)
extern "C"  void ZeroPointsUI_HideUI_m2304286909 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, const MethodInfo* method)
{
	{
		TweenScale_t2697902175 * L_0 = __this->get_contentTween_4();
		NullCheck(L_0);
		((UITweener_t2986641582 *)L_0)->set_delay_7((0.0f));
		TweenScale_t2697902175 * L_1 = __this->get_contentTween_4();
		NullCheck(L_1);
		UITweener_PlayReverse_m4027828489(L_1, /*hidden argument*/NULL);
		TweenAlpha_t2421518635 * L_2 = __this->get_backgroundTween_3();
		TweenScale_t2697902175 * L_3 = __this->get_contentTween_4();
		NullCheck(L_3);
		float L_4 = ((UITweener_t2986641582 *)L_3)->get_duration_8();
		NullCheck(L_2);
		((UITweener_t2986641582 *)L_2)->set_delay_7(L_4);
		TweenAlpha_t2421518635 * L_5 = __this->get_backgroundTween_3();
		NullCheck(L_5);
		UITweener_PlayReverse_m4027828489(L_5, /*hidden argument*/NULL);
		bool L_6 = ___instant0;
		LockoutUI_HideUI_m2397945269(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZeroPointsUI::BackgroundTweenFinished()
extern Il2CppClass* MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3873508672_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3848760317_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var;
extern const MethodInfo* PanelTransitionManager_1_get_PanelActive_m2412409104_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var;
extern const MethodInfo* MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var;
extern const uint32_t ZeroPointsUI_BackgroundTweenFinished_m2129898799_MetadataUsageId;
extern "C"  void ZeroPointsUI_BackgroundTweenFinished_m2129898799 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZeroPointsUI_BackgroundTweenFinished_m2129898799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((LockoutUI_t1337294023 *)__this)->get_unloading_2();
		if (!L_0)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		bool L_1 = MonoSingleton_1_IsAvailable_m2547723652(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_IsAvailable_m2547723652_MethodInfo_var);
		if (!L_1)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3873508672_il2cpp_TypeInfo_var);
		LockoutManager_t4122842952 * L_2 = MonoSingleton_1_get_Instance_m3848760317(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3848760317_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = LockoutManager_LockoutUIActive_m2911161970(L_2, 3, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3887179032_il2cpp_TypeInfo_var);
		ModalManager_t4136513312 * L_4 = MonoSingleton_1_get_Instance_m3651464771(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3651464771_MethodInfo_var);
		NullCheck(L_4);
		bool L_5 = L_4->get_modalActive_4();
		if (L_5)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_6 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_6);
		bool L_7 = PanelTransitionManager_1_get_PanelActive_m2412409104(L_6, /*hidden argument*/PanelTransitionManager_1_get_PanelActive_m2412409104_MethodInfo_var);
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t3344362265_il2cpp_TypeInfo_var);
		MapManager_t3593696545 * L_8 = MonoSingleton_1_get_Instance_m3809739556(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m3809739556_MethodInfo_var);
		NullCheck(L_8);
		MapManager_SetMapUIActive_m4077652589(L_8, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t1149681085_il2cpp_TypeInfo_var);
		ChatmageddonPanelTransitionManager_t1399015365 * L_9 = MonoSingleton_1_get_Instance_m2797109228(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2797109228_MethodInfo_var);
		NullCheck(L_9);
		ChatmageddonPanelTransitionManager_ShowBottomUI_m1084300817(L_9, (0.0f), /*hidden argument*/NULL);
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t2440931836_il2cpp_TypeInfo_var);
		HomeSceneManager_t2690266116 * L_10 = MonoSingleton_1_get_Instance_m2687909429(NULL /*static, unused*/, /*hidden argument*/MonoSingleton_1_get_Instance_m2687909429_MethodInfo_var);
		NullCheck(L_10);
		HomeSceneManager_CheckLockoutState_m2761069386(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0079:
	{
		return;
	}
}
// System.Void ZeroPointsUI::ContentTweenFinished()
extern "C"  void ZeroPointsUI_ContentTweenFinished_m997736054 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((LockoutUI_t1337294023 *)__this)->get_unloading_2();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0010;
	}

IL_0010:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
