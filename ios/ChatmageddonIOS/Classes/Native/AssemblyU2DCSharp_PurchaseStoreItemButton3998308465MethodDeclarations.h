﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchaseStoreItemButton
struct PurchaseStoreItemButton_t3998308465;

#include "codegen/il2cpp-codegen.h"

// System.Void PurchaseStoreItemButton::.ctor()
extern "C"  void PurchaseStoreItemButton__ctor_m3116473382 (PurchaseStoreItemButton_t3998308465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
