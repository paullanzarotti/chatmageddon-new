﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendSearchButton
struct FriendSearchButton_t1070893876;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendSearchButton::.ctor()
extern "C"  void FriendSearchButton__ctor_m343191625 (FriendSearchButton_t1070893876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendSearchButton::OnButtonClick()
extern "C"  void FriendSearchButton_OnButtonClick_m1505308472 (FriendSearchButton_t1070893876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
