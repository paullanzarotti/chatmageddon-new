﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// ICSharpCode.SharpZipLib.Encryption.PkzipClassic
struct PkzipClassic_t1612326202;

#include "codegen/il2cpp-codegen.h"

// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassic::GenerateKeys(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* PkzipClassic_GenerateKeys_m4071511090 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___seed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassic::.ctor()
extern "C"  void PkzipClassic__ctor_m1153871058 (PkzipClassic_t1612326202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
