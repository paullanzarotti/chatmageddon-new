﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveMarkersExample
struct SaveMarkersExample_t1902651330;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveMarkersExample::.ctor()
extern "C"  void SaveMarkersExample__ctor_m534369055 (SaveMarkersExample_t1902651330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarkersExample::SaveMarkers()
extern "C"  void SaveMarkersExample_SaveMarkers_m3946748011 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarkersExample::Start()
extern "C"  void SaveMarkersExample_Start_m3379159331 (SaveMarkersExample_t1902651330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarkersExample::TryLoadMarkers()
extern "C"  void SaveMarkersExample_TryLoadMarkers_m2063968591 (SaveMarkersExample_t1902651330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarkersExample::.cctor()
extern "C"  void SaveMarkersExample__cctor_m801187772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
