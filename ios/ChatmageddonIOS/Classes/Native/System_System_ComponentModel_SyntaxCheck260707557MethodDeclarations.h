﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Boolean System.ComponentModel.SyntaxCheck::CheckMachineName(System.String)
extern "C"  bool SyntaxCheck_CheckMachineName_m3507606364 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.SyntaxCheck::CheckPath(System.String)
extern "C"  bool SyntaxCheck_CheckPath_m2835614917 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.SyntaxCheck::CheckRootedPath(System.String)
extern "C"  bool SyntaxCheck_CheckRootedPath_m2145813262 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
