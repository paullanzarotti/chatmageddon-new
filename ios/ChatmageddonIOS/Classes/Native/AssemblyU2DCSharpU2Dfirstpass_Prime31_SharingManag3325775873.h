﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action
struct Action_t3226471752;

#include "P31RestKit_Prime31_AbstractManager1005944057.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.SharingManager
struct  SharingManager_t3325775873  : public AbstractManager_t1005944057
{
public:

public:
};

struct SharingManager_t3325775873_StaticFields
{
public:
	// System.Action`1<System.String> Prime31.SharingManager::sharingFinishedWithActivityTypeEvent
	Action_1_t1831019615 * ___sharingFinishedWithActivityTypeEvent_5;
	// System.Action Prime31.SharingManager::sharingCancelledEvent
	Action_t3226471752 * ___sharingCancelledEvent_6;

public:
	inline static int32_t get_offset_of_sharingFinishedWithActivityTypeEvent_5() { return static_cast<int32_t>(offsetof(SharingManager_t3325775873_StaticFields, ___sharingFinishedWithActivityTypeEvent_5)); }
	inline Action_1_t1831019615 * get_sharingFinishedWithActivityTypeEvent_5() const { return ___sharingFinishedWithActivityTypeEvent_5; }
	inline Action_1_t1831019615 ** get_address_of_sharingFinishedWithActivityTypeEvent_5() { return &___sharingFinishedWithActivityTypeEvent_5; }
	inline void set_sharingFinishedWithActivityTypeEvent_5(Action_1_t1831019615 * value)
	{
		___sharingFinishedWithActivityTypeEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___sharingFinishedWithActivityTypeEvent_5, value);
	}

	inline static int32_t get_offset_of_sharingCancelledEvent_6() { return static_cast<int32_t>(offsetof(SharingManager_t3325775873_StaticFields, ___sharingCancelledEvent_6)); }
	inline Action_t3226471752 * get_sharingCancelledEvent_6() const { return ___sharingCancelledEvent_6; }
	inline Action_t3226471752 ** get_address_of_sharingCancelledEvent_6() { return &___sharingCancelledEvent_6; }
	inline void set_sharingCancelledEvent_6(Action_t3226471752 * value)
	{
		___sharingCancelledEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___sharingCancelledEvent_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
