﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_VHS_Tracking
struct CameraFilterPack_VHS_Tracking_t1539512780;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_VHS_Tracking::.ctor()
extern "C"  void CameraFilterPack_VHS_Tracking__ctor_m1531327401 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_VHS_Tracking::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_VHS_Tracking_get_material_m313423220 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::Start()
extern "C"  void CameraFilterPack_VHS_Tracking_Start_m1699597585 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_VHS_Tracking_OnRenderImage_m2585129353 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::OnValidate()
extern "C"  void CameraFilterPack_VHS_Tracking_OnValidate_m4178885900 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::Update()
extern "C"  void CameraFilterPack_VHS_Tracking_Update_m3815638454 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::OnDisable()
extern "C"  void CameraFilterPack_VHS_Tracking_OnDisable_m309690384 (CameraFilterPack_VHS_Tracking_t1539512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
