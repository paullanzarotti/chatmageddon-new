﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioManager`1<System.Object>
struct AudioManager_1_t3699525966;
// System.String
struct String_t;
// SFXSource
struct SFXSource_t383013662;
// MusicSource
struct MusicSource_t904181206;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"
#include "AssemblyU2DCSharp_Music799847391.h"

// System.Void AudioManager`1<System.Object>::.ctor()
extern "C"  void AudioManager_1__ctor_m3811624603_gshared (AudioManager_1_t3699525966 * __this, const MethodInfo* method);
#define AudioManager_1__ctor_m3811624603(__this, method) ((  void (*) (AudioManager_1_t3699525966 *, const MethodInfo*))AudioManager_1__ctor_m3811624603_gshared)(__this, method)
// System.Void AudioManager`1<System.Object>::InitAudioObjects(System.String)
extern "C"  void AudioManager_1_InitAudioObjects_m3363084627_gshared (AudioManager_1_t3699525966 * __this, String_t* ___ResourcePath0, const MethodInfo* method);
#define AudioManager_1_InitAudioObjects_m3363084627(__this, ___ResourcePath0, method) ((  void (*) (AudioManager_1_t3699525966 *, String_t*, const MethodInfo*))AudioManager_1_InitAudioObjects_m3363084627_gshared)(__this, ___ResourcePath0, method)
// System.Void AudioManager`1<System.Object>::LoadAudioSettings()
extern "C"  void AudioManager_1_LoadAudioSettings_m77945118_gshared (AudioManager_1_t3699525966 * __this, const MethodInfo* method);
#define AudioManager_1_LoadAudioSettings_m77945118(__this, method) ((  void (*) (AudioManager_1_t3699525966 *, const MethodInfo*))AudioManager_1_LoadAudioSettings_m77945118_gshared)(__this, method)
// System.Boolean AudioManager`1<System.Object>::CheckSFXToggle()
extern "C"  bool AudioManager_1_CheckSFXToggle_m4066422776_gshared (AudioManager_1_t3699525966 * __this, const MethodInfo* method);
#define AudioManager_1_CheckSFXToggle_m4066422776(__this, method) ((  bool (*) (AudioManager_1_t3699525966 *, const MethodInfo*))AudioManager_1_CheckSFXToggle_m4066422776_gshared)(__this, method)
// System.Void AudioManager`1<System.Object>::SetSFXActive(System.Boolean)
extern "C"  void AudioManager_1_SetSFXActive_m4010709183_gshared (AudioManager_1_t3699525966 * __this, bool ___active0, const MethodInfo* method);
#define AudioManager_1_SetSFXActive_m4010709183(__this, ___active0, method) ((  void (*) (AudioManager_1_t3699525966 *, bool, const MethodInfo*))AudioManager_1_SetSFXActive_m4010709183_gshared)(__this, ___active0, method)
// SFXSource AudioManager`1<System.Object>::InitSFXObject(SFX)
extern "C"  SFXSource_t383013662 * AudioManager_1_InitSFXObject_m2253683943_gshared (AudioManager_1_t3699525966 * __this, int32_t ___sfxToPlay0, const MethodInfo* method);
#define AudioManager_1_InitSFXObject_m2253683943(__this, ___sfxToPlay0, method) ((  SFXSource_t383013662 * (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_InitSFXObject_m2253683943_gshared)(__this, ___sfxToPlay0, method)
// System.Void AudioManager`1<System.Object>::PlaySFX(SFX,System.Boolean)
extern "C"  void AudioManager_1_PlaySFX_m1516199870_gshared (AudioManager_1_t3699525966 * __this, int32_t ___sfxToPlay0, bool ___loop1, const MethodInfo* method);
#define AudioManager_1_PlaySFX_m1516199870(__this, ___sfxToPlay0, ___loop1, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, bool, const MethodInfo*))AudioManager_1_PlaySFX_m1516199870_gshared)(__this, ___sfxToPlay0, ___loop1, method)
// System.Void AudioManager`1<System.Object>::FadeInSFX(SFX,System.Single)
extern "C"  void AudioManager_1_FadeInSFX_m2831456923_gshared (AudioManager_1_t3699525966 * __this, int32_t ___sfxToFade0, float ___fadeDuration1, const MethodInfo* method);
#define AudioManager_1_FadeInSFX_m2831456923(__this, ___sfxToFade0, ___fadeDuration1, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, float, const MethodInfo*))AudioManager_1_FadeInSFX_m2831456923_gshared)(__this, ___sfxToFade0, ___fadeDuration1, method)
// System.Void AudioManager`1<System.Object>::FadeOutSFX(SFX,System.Single,System.Boolean)
extern "C"  void AudioManager_1_FadeOutSFX_m2898713101_gshared (AudioManager_1_t3699525966 * __this, int32_t ___sfxToFade0, float ___fadeDuration1, bool ___pause2, const MethodInfo* method);
#define AudioManager_1_FadeOutSFX_m2898713101(__this, ___sfxToFade0, ___fadeDuration1, ___pause2, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, float, bool, const MethodInfo*))AudioManager_1_FadeOutSFX_m2898713101_gshared)(__this, ___sfxToFade0, ___fadeDuration1, ___pause2, method)
// System.Void AudioManager`1<System.Object>::StopSFX(SFX)
extern "C"  void AudioManager_1_StopSFX_m3596533875_gshared (AudioManager_1_t3699525966 * __this, int32_t ___sfxToStop0, const MethodInfo* method);
#define AudioManager_1_StopSFX_m3596533875(__this, ___sfxToStop0, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_StopSFX_m3596533875_gshared)(__this, ___sfxToStop0, method)
// System.Void AudioManager`1<System.Object>::PauseSFX(SFX)
extern "C"  void AudioManager_1_PauseSFX_m2124942313_gshared (AudioManager_1_t3699525966 * __this, int32_t ___sfxToPause0, const MethodInfo* method);
#define AudioManager_1_PauseSFX_m2124942313(__this, ___sfxToPause0, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_PauseSFX_m2124942313_gshared)(__this, ___sfxToPause0, method)
// System.Boolean AudioManager`1<System.Object>::CheckMusicToggle()
extern "C"  bool AudioManager_1_CheckMusicToggle_m326464212_gshared (AudioManager_1_t3699525966 * __this, const MethodInfo* method);
#define AudioManager_1_CheckMusicToggle_m326464212(__this, method) ((  bool (*) (AudioManager_1_t3699525966 *, const MethodInfo*))AudioManager_1_CheckMusicToggle_m326464212_gshared)(__this, method)
// System.Void AudioManager`1<System.Object>::SetMusicActive(System.Boolean)
extern "C"  void AudioManager_1_SetMusicActive_m1244833635_gshared (AudioManager_1_t3699525966 * __this, bool ___active0, const MethodInfo* method);
#define AudioManager_1_SetMusicActive_m1244833635(__this, ___active0, method) ((  void (*) (AudioManager_1_t3699525966 *, bool, const MethodInfo*))AudioManager_1_SetMusicActive_m1244833635_gshared)(__this, ___active0, method)
// MusicSource AudioManager`1<System.Object>::InitMusicObject(Music)
extern "C"  MusicSource_t904181206 * AudioManager_1_InitMusicObject_m1835014451_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToPlay0, const MethodInfo* method);
#define AudioManager_1_InitMusicObject_m1835014451(__this, ___musicToPlay0, method) ((  MusicSource_t904181206 * (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_InitMusicObject_m1835014451_gshared)(__this, ___musicToPlay0, method)
// System.Void AudioManager`1<System.Object>::PlayMusic(Music,System.Boolean)
extern "C"  void AudioManager_1_PlayMusic_m3461114206_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToPlay0, bool ___loop1, const MethodInfo* method);
#define AudioManager_1_PlayMusic_m3461114206(__this, ___musicToPlay0, ___loop1, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, bool, const MethodInfo*))AudioManager_1_PlayMusic_m3461114206_gshared)(__this, ___musicToPlay0, ___loop1, method)
// System.Void AudioManager`1<System.Object>::StopMusic(Music)
extern "C"  void AudioManager_1_StopMusic_m2497180243_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToStop0, const MethodInfo* method);
#define AudioManager_1_StopMusic_m2497180243(__this, ___musicToStop0, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_StopMusic_m2497180243_gshared)(__this, ___musicToStop0, method)
// System.Void AudioManager`1<System.Object>::PauseMusic(Music)
extern "C"  void AudioManager_1_PauseMusic_m600535241_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToPause0, const MethodInfo* method);
#define AudioManager_1_PauseMusic_m600535241(__this, ___musicToPause0, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_PauseMusic_m600535241_gshared)(__this, ___musicToPause0, method)
// System.Void AudioManager`1<System.Object>::FadeInMusic(Music,System.Single)
extern "C"  void AudioManager_1_FadeInMusic_m1751265275_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToFade0, float ___fadeDuration1, const MethodInfo* method);
#define AudioManager_1_FadeInMusic_m1751265275(__this, ___musicToFade0, ___fadeDuration1, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, float, const MethodInfo*))AudioManager_1_FadeInMusic_m1751265275_gshared)(__this, ___musicToFade0, ___fadeDuration1, method)
// System.Void AudioManager`1<System.Object>::FadeOutMusic(Music,System.Single,System.Boolean)
extern "C"  void AudioManager_1_FadeOutMusic_m977247021_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToFade0, float ___fadeDuration1, bool ___pause2, const MethodInfo* method);
#define AudioManager_1_FadeOutMusic_m977247021(__this, ___musicToFade0, ___fadeDuration1, ___pause2, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, float, bool, const MethodInfo*))AudioManager_1_FadeOutMusic_m977247021_gshared)(__this, ___musicToFade0, ___fadeDuration1, ___pause2, method)
// System.Boolean AudioManager`1<System.Object>::CheckMusicPlaying(Music)
extern "C"  bool AudioManager_1_CheckMusicPlaying_m237763017_gshared (AudioManager_1_t3699525966 * __this, int32_t ___musicToCheck0, const MethodInfo* method);
#define AudioManager_1_CheckMusicPlaying_m237763017(__this, ___musicToCheck0, method) ((  bool (*) (AudioManager_1_t3699525966 *, int32_t, const MethodInfo*))AudioManager_1_CheckMusicPlaying_m237763017_gshared)(__this, ___musicToCheck0, method)
// System.Void AudioManager`1<System.Object>::SetMusicVolume(Music,System.Single)
extern "C"  void AudioManager_1_SetMusicVolume_m2460378328_gshared (AudioManager_1_t3699525966 * __this, int32_t ___music0, float ___newVolume1, const MethodInfo* method);
#define AudioManager_1_SetMusicVolume_m2460378328(__this, ___music0, ___newVolume1, method) ((  void (*) (AudioManager_1_t3699525966 *, int32_t, float, const MethodInfo*))AudioManager_1_SetMusicVolume_m2460378328_gshared)(__this, ___music0, ___newVolume1, method)
