﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.Internal.InternalConfigEventHandler
struct InternalConfigEventHandler_t2492130711;
// System.Object
struct Il2CppObject;
// System.Configuration.Internal.InternalConfigEventArgs
struct InternalConfigEventArgs_t2642881318;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Configuration_System_Configuration_Internal2642881318.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Configuration.Internal.InternalConfigEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void InternalConfigEventHandler__ctor_m1506040761 (InternalConfigEventHandler_t2492130711 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Internal.InternalConfigEventHandler::Invoke(System.Object,System.Configuration.Internal.InternalConfigEventArgs)
extern "C"  void InternalConfigEventHandler_Invoke_m3855072253 (InternalConfigEventHandler_t2492130711 * __this, Il2CppObject * ___sender0, InternalConfigEventArgs_t2642881318 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Configuration.Internal.InternalConfigEventHandler::BeginInvoke(System.Object,System.Configuration.Internal.InternalConfigEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InternalConfigEventHandler_BeginInvoke_m1972195032 (InternalConfigEventHandler_t2492130711 * __this, Il2CppObject * ___sender0, InternalConfigEventArgs_t2642881318 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Internal.InternalConfigEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void InternalConfigEventHandler_EndInvoke_m612501655 (InternalConfigEventHandler_t2492130711 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
