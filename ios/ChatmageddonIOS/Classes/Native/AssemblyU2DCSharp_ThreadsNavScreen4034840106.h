﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThreadsNavScreen
struct  ThreadsNavScreen_t4034840106  : public NavigationScreen_t2333230110
{
public:
	// TweenPosition ThreadsNavScreen::tableTween
	TweenPosition_t1144714832 * ___tableTween_3;
	// System.Boolean ThreadsNavScreen::threadsUIclosing
	bool ___threadsUIclosing_4;
	// System.Boolean ThreadsNavScreen::ChatUIclosing
	bool ___ChatUIclosing_5;

public:
	inline static int32_t get_offset_of_tableTween_3() { return static_cast<int32_t>(offsetof(ThreadsNavScreen_t4034840106, ___tableTween_3)); }
	inline TweenPosition_t1144714832 * get_tableTween_3() const { return ___tableTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_tableTween_3() { return &___tableTween_3; }
	inline void set_tableTween_3(TweenPosition_t1144714832 * value)
	{
		___tableTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___tableTween_3, value);
	}

	inline static int32_t get_offset_of_threadsUIclosing_4() { return static_cast<int32_t>(offsetof(ThreadsNavScreen_t4034840106, ___threadsUIclosing_4)); }
	inline bool get_threadsUIclosing_4() const { return ___threadsUIclosing_4; }
	inline bool* get_address_of_threadsUIclosing_4() { return &___threadsUIclosing_4; }
	inline void set_threadsUIclosing_4(bool value)
	{
		___threadsUIclosing_4 = value;
	}

	inline static int32_t get_offset_of_ChatUIclosing_5() { return static_cast<int32_t>(offsetof(ThreadsNavScreen_t4034840106, ___ChatUIclosing_5)); }
	inline bool get_ChatUIclosing_5() const { return ___ChatUIclosing_5; }
	inline bool* get_address_of_ChatUIclosing_5() { return &___ChatUIclosing_5; }
	inline void set_ChatUIclosing_5(bool value)
	{
		___ChatUIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
