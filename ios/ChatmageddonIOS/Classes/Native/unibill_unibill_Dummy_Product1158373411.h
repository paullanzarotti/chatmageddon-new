﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Decimal724701077.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// unibill.Dummy.Product
struct  Product_t1158373411  : public Il2CppObject
{
public:
	// System.String unibill.Dummy.Product::Id
	String_t* ___Id_0;
	// System.String unibill.Dummy.Product::Title
	String_t* ___Title_1;
	// System.String unibill.Dummy.Product::Description
	String_t* ___Description_2;
	// System.String unibill.Dummy.Product::Price
	String_t* ___Price_3;
	// System.Boolean unibill.Dummy.Product::Consumable
	bool ___Consumable_4;
	// System.String unibill.Dummy.Product::IsoCurrencyCode
	String_t* ___IsoCurrencyCode_5;
	// System.Decimal unibill.Dummy.Product::PriceDecimal
	Decimal_t724701077  ___PriceDecimal_6;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier(&___Id_0, value);
	}

	inline static int32_t get_offset_of_Title_1() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___Title_1)); }
	inline String_t* get_Title_1() const { return ___Title_1; }
	inline String_t** get_address_of_Title_1() { return &___Title_1; }
	inline void set_Title_1(String_t* value)
	{
		___Title_1 = value;
		Il2CppCodeGenWriteBarrier(&___Title_1, value);
	}

	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___Description_2)); }
	inline String_t* get_Description_2() const { return ___Description_2; }
	inline String_t** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(String_t* value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier(&___Description_2, value);
	}

	inline static int32_t get_offset_of_Price_3() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___Price_3)); }
	inline String_t* get_Price_3() const { return ___Price_3; }
	inline String_t** get_address_of_Price_3() { return &___Price_3; }
	inline void set_Price_3(String_t* value)
	{
		___Price_3 = value;
		Il2CppCodeGenWriteBarrier(&___Price_3, value);
	}

	inline static int32_t get_offset_of_Consumable_4() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___Consumable_4)); }
	inline bool get_Consumable_4() const { return ___Consumable_4; }
	inline bool* get_address_of_Consumable_4() { return &___Consumable_4; }
	inline void set_Consumable_4(bool value)
	{
		___Consumable_4 = value;
	}

	inline static int32_t get_offset_of_IsoCurrencyCode_5() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___IsoCurrencyCode_5)); }
	inline String_t* get_IsoCurrencyCode_5() const { return ___IsoCurrencyCode_5; }
	inline String_t** get_address_of_IsoCurrencyCode_5() { return &___IsoCurrencyCode_5; }
	inline void set_IsoCurrencyCode_5(String_t* value)
	{
		___IsoCurrencyCode_5 = value;
		Il2CppCodeGenWriteBarrier(&___IsoCurrencyCode_5, value);
	}

	inline static int32_t get_offset_of_PriceDecimal_6() { return static_cast<int32_t>(offsetof(Product_t1158373411, ___PriceDecimal_6)); }
	inline Decimal_t724701077  get_PriceDecimal_6() const { return ___PriceDecimal_6; }
	inline Decimal_t724701077 * get_address_of_PriceDecimal_6() { return &___PriceDecimal_6; }
	inline void set_PriceDecimal_6(Decimal_t724701077  value)
	{
		___PriceDecimal_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
