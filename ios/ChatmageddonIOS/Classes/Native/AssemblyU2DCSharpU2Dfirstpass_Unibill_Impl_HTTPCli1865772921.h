﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>
struct Queue_1_t3952525865;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.HTTPClient
struct  HTTPClient_t1865772921  : public Il2CppObject
{
public:
	// System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest> Unibill.Impl.HTTPClient::events
	Queue_1_t3952525865 * ___events_0;
	// UnityEngine.WaitForSeconds Unibill.Impl.HTTPClient::wait
	WaitForSeconds_t3839502067 * ___wait_1;

public:
	inline static int32_t get_offset_of_events_0() { return static_cast<int32_t>(offsetof(HTTPClient_t1865772921, ___events_0)); }
	inline Queue_1_t3952525865 * get_events_0() const { return ___events_0; }
	inline Queue_1_t3952525865 ** get_address_of_events_0() { return &___events_0; }
	inline void set_events_0(Queue_1_t3952525865 * value)
	{
		___events_0 = value;
		Il2CppCodeGenWriteBarrier(&___events_0, value);
	}

	inline static int32_t get_offset_of_wait_1() { return static_cast<int32_t>(offsetof(HTTPClient_t1865772921, ___wait_1)); }
	inline WaitForSeconds_t3839502067 * get_wait_1() const { return ___wait_1; }
	inline WaitForSeconds_t3839502067 ** get_address_of_wait_1() { return &___wait_1; }
	inline void set_wait_1(WaitForSeconds_t3839502067 * value)
	{
		___wait_1 = value;
		Il2CppCodeGenWriteBarrier(&___wait_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
