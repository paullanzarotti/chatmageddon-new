﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<LeaderboardListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m4152569220(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3887848756 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<LeaderboardListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m2255227799(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t3887848756 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<LeaderboardListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m693241749(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3887848756 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<LeaderboardListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m3115814450(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3887848756 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<LeaderboardListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m2610187819(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t3887848756 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<LeaderboardListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m1746214356(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3887848756 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<LeaderboardListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m2319694573(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3887848756 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
