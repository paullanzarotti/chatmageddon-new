﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchGlobalNavScreen
struct AttackSearchGlobalNavScreen_t581792468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackSearchGlobalNavScreen::.ctor()
extern "C"  void AttackSearchGlobalNavScreen__ctor_m1739674935 (AttackSearchGlobalNavScreen_t581792468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchGlobalNavScreen::Start()
extern "C"  void AttackSearchGlobalNavScreen_Start_m2462063451 (AttackSearchGlobalNavScreen_t581792468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchGlobalNavScreen::UIClosing()
extern "C"  void AttackSearchGlobalNavScreen_UIClosing_m758599738 (AttackSearchGlobalNavScreen_t581792468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchGlobalNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackSearchGlobalNavScreen_ScreenLoad_m2621450513 (AttackSearchGlobalNavScreen_t581792468 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchGlobalNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackSearchGlobalNavScreen_ScreenUnload_m1073468485 (AttackSearchGlobalNavScreen_t581792468 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackSearchGlobalNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackSearchGlobalNavScreen_ValidateScreenNavigation_m4272686288 (AttackSearchGlobalNavScreen_t581792468 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
