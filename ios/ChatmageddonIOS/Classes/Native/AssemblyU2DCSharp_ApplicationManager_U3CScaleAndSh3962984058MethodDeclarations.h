﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApplicationManager/<ScaleAndShow>c__Iterator1
struct U3CScaleAndShowU3Ec__Iterator1_t3962984058;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ApplicationManager/<ScaleAndShow>c__Iterator1::.ctor()
extern "C"  void U3CScaleAndShowU3Ec__Iterator1__ctor_m943341527 (U3CScaleAndShowU3Ec__Iterator1_t3962984058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ApplicationManager/<ScaleAndShow>c__Iterator1::MoveNext()
extern "C"  bool U3CScaleAndShowU3Ec__Iterator1_MoveNext_m780529797 (U3CScaleAndShowU3Ec__Iterator1_t3962984058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ApplicationManager/<ScaleAndShow>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CScaleAndShowU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2771020597 (U3CScaleAndShowU3Ec__Iterator1_t3962984058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ApplicationManager/<ScaleAndShow>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CScaleAndShowU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2596578461 (U3CScaleAndShowU3Ec__Iterator1_t3962984058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager/<ScaleAndShow>c__Iterator1::Dispose()
extern "C"  void U3CScaleAndShowU3Ec__Iterator1_Dispose_m1619006572 (U3CScaleAndShowU3Ec__Iterator1_t3962984058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager/<ScaleAndShow>c__Iterator1::Reset()
extern "C"  void U3CScaleAndShowU3Ec__Iterator1_Reset_m2700519706 (U3CScaleAndShowU3Ec__Iterator1_t3962984058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
