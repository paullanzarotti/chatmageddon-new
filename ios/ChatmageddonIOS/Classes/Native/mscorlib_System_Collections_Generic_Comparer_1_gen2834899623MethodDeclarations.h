﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<AttackHomeNav>
struct Comparer_1_t2834899623;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<AttackHomeNav>::.ctor()
extern "C"  void Comparer_1__ctor_m2407958911_gshared (Comparer_1_t2834899623 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2407958911(__this, method) ((  void (*) (Comparer_1_t2834899623 *, const MethodInfo*))Comparer_1__ctor_m2407958911_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<AttackHomeNav>::.cctor()
extern "C"  void Comparer_1__cctor_m3751207534_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m3751207534(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m3751207534_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<AttackHomeNav>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m45950000_gshared (Comparer_1_t2834899623 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m45950000(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2834899623 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m45950000_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<AttackHomeNav>::get_Default()
extern "C"  Comparer_1_t2834899623 * Comparer_1_get_Default_m3659511199_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m3659511199(__this /* static, unused */, method) ((  Comparer_1_t2834899623 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m3659511199_gshared)(__this /* static, unused */, method)
