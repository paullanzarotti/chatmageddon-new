﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SettingsNavScreen>
struct List_1_t1636248366;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1170978040.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1528521323_gshared (Enumerator_t1170978040 * __this, List_1_t1636248366 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1528521323(__this, ___l0, method) ((  void (*) (Enumerator_t1170978040 *, List_1_t1636248366 *, const MethodInfo*))Enumerator__ctor_m1528521323_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2884438847_gshared (Enumerator_t1170978040 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2884438847(__this, method) ((  void (*) (Enumerator_t1170978040 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2884438847_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1626380259_gshared (Enumerator_t1170978040 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1626380259(__this, method) ((  Il2CppObject * (*) (Enumerator_t1170978040 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1626380259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m4237939118_gshared (Enumerator_t1170978040 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4237939118(__this, method) ((  void (*) (Enumerator_t1170978040 *, const MethodInfo*))Enumerator_Dispose_m4237939118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3600803477_gshared (Enumerator_t1170978040 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3600803477(__this, method) ((  void (*) (Enumerator_t1170978040 *, const MethodInfo*))Enumerator_VerifyState_m3600803477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3757804283_gshared (Enumerator_t1170978040 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3757804283(__this, method) ((  bool (*) (Enumerator_t1170978040 *, const MethodInfo*))Enumerator_MoveNext_m3757804283_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SettingsNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1981354856_gshared (Enumerator_t1170978040 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1981354856(__this, method) ((  int32_t (*) (Enumerator_t1170978040 *, const MethodInfo*))Enumerator_get_Current_m1981354856_gshared)(__this, method)
