﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Drawing_Manga
struct  CameraFilterPack_Drawing_Manga_t2205931982  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Drawing_Manga::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_Manga::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_Drawing_Manga::SCMaterial
	Material_t193706927 * ___SCMaterial_4;
	// System.Single CameraFilterPack_Drawing_Manga::DotSize
	float ___DotSize_5;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_Manga_t2205931982, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_Manga_t2205931982, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_Manga_t2205931982, ___SCMaterial_4)); }
	inline Material_t193706927 * get_SCMaterial_4() const { return ___SCMaterial_4; }
	inline Material_t193706927 ** get_address_of_SCMaterial_4() { return &___SCMaterial_4; }
	inline void set_SCMaterial_4(Material_t193706927 * value)
	{
		___SCMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_4, value);
	}

	inline static int32_t get_offset_of_DotSize_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_Manga_t2205931982, ___DotSize_5)); }
	inline float get_DotSize_5() const { return ___DotSize_5; }
	inline float* get_address_of_DotSize_5() { return &___DotSize_5; }
	inline void set_DotSize_5(float value)
	{
		___DotSize_5 = value;
	}
};

struct CameraFilterPack_Drawing_Manga_t2205931982_StaticFields
{
public:
	// System.Single CameraFilterPack_Drawing_Manga::ChangeDotSize
	float ___ChangeDotSize_6;

public:
	inline static int32_t get_offset_of_ChangeDotSize_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_Manga_t2205931982_StaticFields, ___ChangeDotSize_6)); }
	inline float get_ChangeDotSize_6() const { return ___ChangeDotSize_6; }
	inline float* get_address_of_ChangeDotSize_6() { return &___ChangeDotSize_6; }
	inline void set_ChangeDotSize_6(float value)
	{
		___ChangeDotSize_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
