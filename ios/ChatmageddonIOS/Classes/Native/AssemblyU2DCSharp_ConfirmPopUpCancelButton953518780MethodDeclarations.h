﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfirmPopUpCancelButton
struct ConfirmPopUpCancelButton_t953518780;

#include "codegen/il2cpp-codegen.h"

// System.Void ConfirmPopUpCancelButton::.ctor()
extern "C"  void ConfirmPopUpCancelButton__ctor_m2540991979 (ConfirmPopUpCancelButton_t953518780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfirmPopUpCancelButton::OnButtonClick()
extern "C"  void ConfirmPopUpCancelButton_OnButtonClick_m2113813584 (ConfirmPopUpCancelButton_t953518780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
