﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountryPickerButton
struct CountryPickerButton_t1162517224;

#include "codegen/il2cpp-codegen.h"

// System.Void CountryPickerButton::.ctor()
extern "C"  void CountryPickerButton__ctor_m3162632591 (CountryPickerButton_t1162517224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryPickerButton::OnButtonClick()
extern "C"  void CountryPickerButton_OnButtonClick_m1540092704 (CountryPickerButton_t1162517224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
