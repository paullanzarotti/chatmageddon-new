﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VibrationController/<SingleVibrate>c__Iterator2
struct U3CSingleVibrateU3Ec__Iterator2_t2980562147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VibrationController/<SingleVibrate>c__Iterator2::.ctor()
extern "C"  void U3CSingleVibrateU3Ec__Iterator2__ctor_m2341378010 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VibrationController/<SingleVibrate>c__Iterator2::MoveNext()
extern "C"  bool U3CSingleVibrateU3Ec__Iterator2_MoveNext_m2824126370 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<SingleVibrate>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSingleVibrateU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1475061172 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<SingleVibrate>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSingleVibrateU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2060965276 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<SingleVibrate>c__Iterator2::Dispose()
extern "C"  void U3CSingleVibrateU3Ec__Iterator2_Dispose_m911557205 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<SingleVibrate>c__Iterator2::Reset()
extern "C"  void U3CSingleVibrateU3Ec__Iterator2_Reset_m2158566503 (U3CSingleVibrateU3Ec__Iterator2_t2980562147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
