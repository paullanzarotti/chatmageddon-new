﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPCycler
struct IPCycler_t1336138445;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void IPCycler::.ctor()
extern "C"  void IPCycler__ctor_m625586824 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPCycler::get_IsMoving()
extern "C"  bool IPCycler_get_IsMoving_m1238759163 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::set_IsMoving(System.Boolean)
extern "C"  void IPCycler_set_IsMoving_m2911549280 (IPCycler_t1336138445 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPCycler::get_CenterWidgetIndex()
extern "C"  int32_t IPCycler_get_CenterWidgetIndex_m760101406 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::set_CenterWidgetIndex(System.Int32)
extern "C"  void IPCycler_set_CenterWidgetIndex_m1138812809 (IPCycler_t1336138445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPCycler::get_RecenterTargetWidgetIndex()
extern "C"  int32_t IPCycler_get_RecenterTargetWidgetIndex_m550586278 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::set_RecenterTargetWidgetIndex(System.Int32)
extern "C"  void IPCycler_set_RecenterTargetWidgetIndex_m3776813475 (IPCycler_t1336138445 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPCycler::get_NbOfTransforms()
extern "C"  int32_t IPCycler_get_NbOfTransforms_m2926184893 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPCycler::get_ClampIncrement()
extern "C"  bool IPCycler_get_ClampIncrement_m2456249867 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::set_ClampIncrement(System.Boolean)
extern "C"  void IPCycler_set_ClampIncrement_m2152456970 (IPCycler_t1336138445 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPCycler::get_ClampDecrement()
extern "C"  bool IPCycler_get_ClampDecrement_m1149887833 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::set_ClampDecrement(System.Boolean)
extern "C"  void IPCycler_set_ClampDecrement_m428297334 (IPCycler_t1336138445 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::Init()
extern "C"  void IPCycler_Init_m3382685648 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::ResetCycler()
extern "C"  void IPCycler_ResetCycler_m3297035515 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::Update()
extern "C"  void IPCycler_Update_m536757209 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::PickerStopped()
extern "C"  void IPCycler_PickerStopped_m3527343539 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::Scroll(System.Single)
extern "C"  void IPCycler_Scroll_m2732256720 (IPCycler_t1336138445 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::Recenter()
extern "C"  void IPCycler_Recenter_m3208512042 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::AutoScrollToNextElement()
extern "C"  void IPCycler_AutoScrollToNextElement_m793154646 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::AutoScrollToPreviousElement()
extern "C"  void IPCycler_AutoScrollToPreviousElement_m1368858078 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::AutoScrollBy(System.Int32)
extern "C"  void IPCycler_AutoScrollBy_m463261892 (IPCycler_t1336138445 * __this, int32_t ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPCycler::GetDeltaIndexFromScreenPos(UnityEngine.Vector2)
extern "C"  int32_t IPCycler_GetDeltaIndexFromScreenPos_m365621228 (IPCycler_t1336138445 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPCycler::GetIndexFromScreenPos(UnityEngine.Vector2)
extern "C"  int32_t IPCycler_GetIndexFromScreenPos_m3545206324 (IPCycler_t1336138445 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::OnPress(System.Boolean)
extern "C"  void IPCycler_OnPress_m3051402441 (IPCycler_t1336138445 * __this, bool ___press0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::ScrollWheelStartOrStop(System.Boolean)
extern "C"  void IPCycler_ScrollWheelStartOrStop_m1332838814 (IPCycler_t1336138445 * __this, bool ___start0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IPCycler::RecenterOnThreshold()
extern "C"  Il2CppObject * IPCycler_RecenterOnThreshold_m3445991520 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::CenterOnTransformIndex(System.Int32)
extern "C"  void IPCycler_CenterOnTransformIndex_m2925377245 (IPCycler_t1336138445 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single IPCycler::SignificantPosVector(UnityEngine.Transform)
extern "C"  float IPCycler_SignificantPosVector_m3124865565 (IPCycler_t1336138445 * __this, Transform_t3275118058 * ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::SetWidgetSignificantPos(UnityEngine.Transform,System.Single)
extern "C"  void IPCycler_SetWidgetSignificantPos_m3219145665 (IPCycler_t1336138445 * __this, Transform_t3275118058 * ___trans0, float ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPCycler::TryIncrement()
extern "C"  bool IPCycler_TryIncrement_m2464087150 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPCycler::TryDecrement()
extern "C"  bool IPCycler_TryDecrement_m837231582 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform IPCycler::FirstWidget(System.Int32&)
extern "C"  Transform_t3275118058 * IPCycler_FirstWidget_m1593962861 (IPCycler_t1336138445 * __this, int32_t* ___firstWidgetIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform IPCycler::LastWidget(System.Int32&)
extern "C"  Transform_t3275118058 * IPCycler_LastWidget_m738771209 (IPCycler_t1336138445 * __this, int32_t* ___lastWidgetIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::EditorInit()
extern "C"  void IPCycler_EditorInit_m1543866135 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::UpdateTrueSpacing()
extern "C"  void IPCycler_UpdateTrueSpacing_m1735273438 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::PlaceTransforms()
extern "C"  void IPCycler_PlaceTransforms_m3255725042 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::RebuildTransforms(System.Int32)
extern "C"  void IPCycler_RebuildTransforms_m24975555 (IPCycler_t1336138445 * __this, int32_t ___newNb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler::DestroyChildrenOfChildren()
extern "C"  void IPCycler_DestroyChildrenOfChildren_m2550464183 (IPCycler_t1336138445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
