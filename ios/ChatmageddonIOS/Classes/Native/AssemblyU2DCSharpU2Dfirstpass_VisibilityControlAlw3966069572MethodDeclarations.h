﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VisibilityControlAlways
struct VisibilityControlAlways_t3966069572;
// RefInt
struct RefInt_t2938871354;
// Vectrosity.VectorLine
struct VectorLine_t3390220087;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"

// System.Void VisibilityControlAlways::.ctor()
extern "C"  void VisibilityControlAlways__ctor_m1732851813 (VisibilityControlAlways_t3966069572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RefInt VisibilityControlAlways::get_objectNumber()
extern "C"  RefInt_t2938871354 * VisibilityControlAlways_get_objectNumber_m1445195483 (VisibilityControlAlways_t3966069572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlAlways::Setup(Vectrosity.VectorLine)
extern "C"  void VisibilityControlAlways_Setup_m4119118361 (VisibilityControlAlways_t3966069572 * __this, VectorLine_t3390220087 * ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlAlways::OnDestroy()
extern "C"  void VisibilityControlAlways_OnDestroy_m2521214732 (VisibilityControlAlways_t3966069572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
