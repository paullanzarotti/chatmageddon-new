﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.UnityURLFetcher
struct UnityURLFetcher_t313076257;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Uniject.IHTTPRequest
struct IHTTPRequest_t54590966;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.UnityURLFetcher::.ctor()
extern "C"  void UnityURLFetcher__ctor_m2188367875 (UnityURLFetcher_t313076257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.UnityURLFetcher::doGet(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Il2CppObject * UnityURLFetcher_doGet_m220258204 (UnityURLFetcher_t313076257 * __this, String_t* ___url0, Dictionary_2_t3943999495 * ___headers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.UnityURLFetcher::doPost(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Il2CppObject * UnityURLFetcher_doPost_m2895090366 (UnityURLFetcher_t313076257 * __this, String_t* ___url0, Dictionary_2_t3943999495 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Uniject.IHTTPRequest Unibill.Impl.UnityURLFetcher::getResponse()
extern "C"  Il2CppObject * UnityURLFetcher_getResponse_m1077517061 (UnityURLFetcher_t313076257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
