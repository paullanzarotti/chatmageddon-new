﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Old_Movie
struct CameraFilterPack_TV_Old_Movie_t1608414762;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Old_Movie::.ctor()
extern "C"  void CameraFilterPack_TV_Old_Movie__ctor_m3191386561 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Old_Movie::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Old_Movie_get_material_m34732078 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::Start()
extern "C"  void CameraFilterPack_TV_Old_Movie_Start_m3446401137 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Old_Movie_OnRenderImage_m475633097 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::OnValidate()
extern "C"  void CameraFilterPack_TV_Old_Movie_OnValidate_m1692227038 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::Update()
extern "C"  void CameraFilterPack_TV_Old_Movie_Update_m496048396 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::OnDisable()
extern "C"  void CameraFilterPack_TV_Old_Movie_OnDisable_m1247799642 (CameraFilterPack_TV_Old_Movie_t1608414762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
