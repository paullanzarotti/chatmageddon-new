﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blend2Camera_LinearBurn
struct  CameraFilterPack_Blend2Camera_LinearBurn_t1186093094  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Blend2Camera_LinearBurn::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_LinearBurn::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_LinearBurn::Camera2
	Camera_t189460977 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_LinearBurn::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_Blend2Camera_LinearBurn::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_LinearBurn::SCMaterial
	Material_t193706927 * ___SCMaterial_7;
	// System.Single CameraFilterPack_Blend2Camera_LinearBurn::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_8;
	// System.Single CameraFilterPack_Blend2Camera_LinearBurn::BlendFX
	float ___BlendFX_9;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_LinearBurn::Camera2tex
	RenderTexture_t2666733923 * ___Camera2tex_12;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_Camera2_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___Camera2_4)); }
	inline Camera_t189460977 * get_Camera2_4() const { return ___Camera2_4; }
	inline Camera_t189460977 ** get_address_of_Camera2_4() { return &___Camera2_4; }
	inline void set_Camera2_4(Camera_t189460977 * value)
	{
		___Camera2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2_4, value);
	}

	inline static int32_t get_offset_of_TimeX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___TimeX_5)); }
	inline float get_TimeX_5() const { return ___TimeX_5; }
	inline float* get_address_of_TimeX_5() { return &___TimeX_5; }
	inline void set_TimeX_5(float value)
	{
		___TimeX_5 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___ScreenResolution_6)); }
	inline Vector4_t2243707581  get_ScreenResolution_6() const { return ___ScreenResolution_6; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_6() { return &___ScreenResolution_6; }
	inline void set_ScreenResolution_6(Vector4_t2243707581  value)
	{
		___ScreenResolution_6 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___SCMaterial_7)); }
	inline Material_t193706927 * get_SCMaterial_7() const { return ___SCMaterial_7; }
	inline Material_t193706927 ** get_address_of_SCMaterial_7() { return &___SCMaterial_7; }
	inline void set_SCMaterial_7(Material_t193706927 * value)
	{
		___SCMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_7, value);
	}

	inline static int32_t get_offset_of_SwitchCameraToCamera2_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___SwitchCameraToCamera2_8)); }
	inline float get_SwitchCameraToCamera2_8() const { return ___SwitchCameraToCamera2_8; }
	inline float* get_address_of_SwitchCameraToCamera2_8() { return &___SwitchCameraToCamera2_8; }
	inline void set_SwitchCameraToCamera2_8(float value)
	{
		___SwitchCameraToCamera2_8 = value;
	}

	inline static int32_t get_offset_of_BlendFX_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___BlendFX_9)); }
	inline float get_BlendFX_9() const { return ___BlendFX_9; }
	inline float* get_address_of_BlendFX_9() { return &___BlendFX_9; }
	inline void set_BlendFX_9(float value)
	{
		___BlendFX_9 = value;
	}

	inline static int32_t get_offset_of_Camera2tex_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094, ___Camera2tex_12)); }
	inline RenderTexture_t2666733923 * get_Camera2tex_12() const { return ___Camera2tex_12; }
	inline RenderTexture_t2666733923 ** get_address_of_Camera2tex_12() { return &___Camera2tex_12; }
	inline void set_Camera2tex_12(RenderTexture_t2666733923 * value)
	{
		___Camera2tex_12 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2tex_12, value);
	}
};

struct CameraFilterPack_Blend2Camera_LinearBurn_t1186093094_StaticFields
{
public:
	// System.Single CameraFilterPack_Blend2Camera_LinearBurn::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Blend2Camera_LinearBurn::ChangeValue2
	float ___ChangeValue2_11;

public:
	inline static int32_t get_offset_of_ChangeValue_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094_StaticFields, ___ChangeValue_10)); }
	inline float get_ChangeValue_10() const { return ___ChangeValue_10; }
	inline float* get_address_of_ChangeValue_10() { return &___ChangeValue_10; }
	inline void set_ChangeValue_10(float value)
	{
		___ChangeValue_10 = value;
	}

	inline static int32_t get_offset_of_ChangeValue2_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_LinearBurn_t1186093094_StaticFields, ___ChangeValue2_11)); }
	inline float get_ChangeValue2_11() const { return ___ChangeValue2_11; }
	inline float* get_address_of_ChangeValue2_11() { return &___ChangeValue2_11; }
	inline void set_ChangeValue2_11(float value)
	{
		___ChangeValue2_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
