﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<TriggerMine>c__AnonStorey2B
struct U3CTriggerMineU3Ec__AnonStorey2B_t2536103459;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<TriggerMine>c__AnonStorey2B::.ctor()
extern "C"  void U3CTriggerMineU3Ec__AnonStorey2B__ctor_m2915735186 (U3CTriggerMineU3Ec__AnonStorey2B_t2536103459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<TriggerMine>c__AnonStorey2B::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CTriggerMineU3Ec__AnonStorey2B_U3CU3Em__0_m3731226539 (U3CTriggerMineU3Ec__AnonStorey2B_t2536103459 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
