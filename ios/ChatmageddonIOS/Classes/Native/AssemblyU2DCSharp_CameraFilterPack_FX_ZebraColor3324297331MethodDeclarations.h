﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_ZebraColor
struct CameraFilterPack_FX_ZebraColor_t3324297331;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_ZebraColor::.ctor()
extern "C"  void CameraFilterPack_FX_ZebraColor__ctor_m2523935018 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_ZebraColor::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_ZebraColor_get_material_m1418893095 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::Start()
extern "C"  void CameraFilterPack_FX_ZebraColor_Start_m2637435422 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_ZebraColor_OnRenderImage_m3311398542 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::OnValidate()
extern "C"  void CameraFilterPack_FX_ZebraColor_OnValidate_m3689762221 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::Update()
extern "C"  void CameraFilterPack_FX_ZebraColor_Update_m3996886395 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::OnDisable()
extern "C"  void CameraFilterPack_FX_ZebraColor_OnDisable_m3447269403 (CameraFilterPack_FX_ZebraColor_t3324297331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
