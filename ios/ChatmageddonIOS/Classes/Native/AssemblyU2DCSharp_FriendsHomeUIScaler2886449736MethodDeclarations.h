﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsHomeUIScaler
struct FriendsHomeUIScaler_t2886449736;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsHomeUIScaler::.ctor()
extern "C"  void FriendsHomeUIScaler__ctor_m4237060335 (FriendsHomeUIScaler_t2886449736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsHomeUIScaler::GlobalUIScale()
extern "C"  void FriendsHomeUIScaler_GlobalUIScale_m2120389798 (FriendsHomeUIScaler_t2886449736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
