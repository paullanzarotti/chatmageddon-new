﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackInventoryNavScreen
struct AttackInventoryNavScreen_t33214553;

#include "AssemblyU2DCSharp_ItemSelectionSwipe2104393431.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileSelectionSwipe
struct  MissileSelectionSwipe_t4045384104  : public ItemSelectionSwipe_t2104393431
{
public:
	// AttackInventoryNavScreen MissileSelectionSwipe::screen
	AttackInventoryNavScreen_t33214553 * ___screen_11;

public:
	inline static int32_t get_offset_of_screen_11() { return static_cast<int32_t>(offsetof(MissileSelectionSwipe_t4045384104, ___screen_11)); }
	inline AttackInventoryNavScreen_t33214553 * get_screen_11() const { return ___screen_11; }
	inline AttackInventoryNavScreen_t33214553 ** get_address_of_screen_11() { return &___screen_11; }
	inline void set_screen_11(AttackInventoryNavScreen_t33214553 * value)
	{
		___screen_11 = value;
		Il2CppCodeGenWriteBarrier(&___screen_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
