﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ConnectionPopUp>::.ctor()
#define MonoSingleton_1__ctor_m4265218644(__this, method) ((  void (*) (MonoSingleton_1_t221309578 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ConnectionPopUp>::Awake()
#define MonoSingleton_1_Awake_m3635287089(__this, method) ((  void (*) (MonoSingleton_1_t221309578 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ConnectionPopUp>::get_Instance()
#define MonoSingleton_1_get_Instance_m1657144359(__this /* static, unused */, method) ((  ConnectionPopUp_t470643858 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ConnectionPopUp>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4146417719(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ConnectionPopUp>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3013730334(__this, method) ((  void (*) (MonoSingleton_1_t221309578 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ConnectionPopUp>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2717488058(__this, method) ((  void (*) (MonoSingleton_1_t221309578 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ConnectionPopUp>::.cctor()
#define MonoSingleton_1__cctor_m2616436103(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
