﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4014198703;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3744437497_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3744437497(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3744437497_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Vector3_t2243707580  U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3598759710_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3598759710(__this, method) ((  Vector3_t2243707580  (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3598759710_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1434257643_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1434257643(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1434257643_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2204398158_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2204398158(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2204398158_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1915838749_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1915838749(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1915838749_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2537538495_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2537538495(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2537538495_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1758250_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1758250(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1758250_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1027294588_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1027294588(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1027294588_gshared)(__this, method)
