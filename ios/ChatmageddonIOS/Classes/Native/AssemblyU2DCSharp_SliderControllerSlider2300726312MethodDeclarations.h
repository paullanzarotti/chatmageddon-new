﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SliderControllerSlider
struct SliderControllerSlider_t2300726312;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void SliderControllerSlider::.ctor()
extern "C"  void SliderControllerSlider__ctor_m3209009311 (SliderControllerSlider_t2300726312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderControllerSlider::Start()
extern "C"  void SliderControllerSlider_Start_m3418740051 (SliderControllerSlider_t2300726312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderControllerSlider::OnDragStart()
extern "C"  void SliderControllerSlider_OnDragStart_m2791484550 (SliderControllerSlider_t2300726312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderControllerSlider::OnDrag(UnityEngine.Vector2)
extern "C"  void SliderControllerSlider_OnDrag_m1311983712 (SliderControllerSlider_t2300726312 * __this, Vector2_t2243707579  ___dragPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SliderControllerSlider::OnDragEnd()
extern "C"  void SliderControllerSlider_OnDragEnd_m2773871093 (SliderControllerSlider_t2300726312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
