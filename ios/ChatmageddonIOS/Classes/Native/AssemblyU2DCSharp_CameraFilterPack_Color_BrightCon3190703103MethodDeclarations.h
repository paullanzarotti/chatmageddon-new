﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_BrightContrastSaturation
struct CameraFilterPack_Color_BrightContrastSaturation_t3190703103;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_BrightContrastSaturation::.ctor()
extern "C"  void CameraFilterPack_Color_BrightContrastSaturation__ctor_m1089901974 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_BrightContrastSaturation::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_BrightContrastSaturation_get_material_m1635423563 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::Start()
extern "C"  void CameraFilterPack_Color_BrightContrastSaturation_Start_m2438192218 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_BrightContrastSaturation_OnRenderImage_m3921992946 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::OnValidate()
extern "C"  void CameraFilterPack_Color_BrightContrastSaturation_OnValidate_m902910101 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::Update()
extern "C"  void CameraFilterPack_Color_BrightContrastSaturation_Update_m2311138499 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::OnDisable()
extern "C"  void CameraFilterPack_Color_BrightContrastSaturation_OnDisable_m1556488259 (CameraFilterPack_Color_BrightContrastSaturation_t3190703103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
