﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TermsOfServiceUI
struct TermsOfServiceUI_t3223215579;

#include "codegen/il2cpp-codegen.h"

// System.Void TermsOfServiceUI::.ctor()
extern "C"  void TermsOfServiceUI__ctor_m408437922 (TermsOfServiceUI_t3223215579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TermsOfServiceUI::LoadUI()
extern "C"  void TermsOfServiceUI_LoadUI_m736700850 (TermsOfServiceUI_t3223215579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
