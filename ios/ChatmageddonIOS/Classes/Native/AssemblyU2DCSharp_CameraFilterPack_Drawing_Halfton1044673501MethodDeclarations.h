﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Halftone
struct CameraFilterPack_Drawing_Halftone_t1044673501;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Halftone::.ctor()
extern "C"  void CameraFilterPack_Drawing_Halftone__ctor_m448135482 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Halftone::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Halftone_get_material_m3357801673 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::Start()
extern "C"  void CameraFilterPack_Drawing_Halftone_Start_m1765961762 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Halftone_OnRenderImage_m3414134330 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Halftone_OnValidate_m2732052027 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::Update()
extern "C"  void CameraFilterPack_Drawing_Halftone_Update_m3286238853 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Halftone_OnDisable_m3681188001 (CameraFilterPack_Drawing_Halftone_t1044673501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
