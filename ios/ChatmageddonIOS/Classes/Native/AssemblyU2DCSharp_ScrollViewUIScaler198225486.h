﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollViewUIScaler
struct  ScrollViewUIScaler_t198225486  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UIPanel ScrollViewUIScaler::scrollView
	UIPanel_t1795085332 * ___scrollView_14;

public:
	inline static int32_t get_offset_of_scrollView_14() { return static_cast<int32_t>(offsetof(ScrollViewUIScaler_t198225486, ___scrollView_14)); }
	inline UIPanel_t1795085332 * get_scrollView_14() const { return ___scrollView_14; }
	inline UIPanel_t1795085332 ** get_address_of_scrollView_14() { return &___scrollView_14; }
	inline void set_scrollView_14(UIPanel_t1795085332 * value)
	{
		___scrollView_14 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
