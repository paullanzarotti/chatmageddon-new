﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackOuterPhoneNumberButton
struct AttackOuterPhoneNumberButton_t2666821364;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackOuterPhoneNumberButton::.ctor()
extern "C"  void AttackOuterPhoneNumberButton__ctor_m4203251499 (AttackOuterPhoneNumberButton_t2666821364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackOuterPhoneNumberButton::SetupButton()
extern "C"  void AttackOuterPhoneNumberButton_SetupButton_m1296898666 (AttackOuterPhoneNumberButton_t2666821364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
