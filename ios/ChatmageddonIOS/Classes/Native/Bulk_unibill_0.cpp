﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// unibill.Dummy.IWindowsIAP
struct IWindowsIAP_t2654797964;
// unibill.Dummy.Product[]
struct ProductU5BU5D_t4190069746;
// unibill.Dummy.Product
struct Product_t1158373411;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "unibill_U3CModuleU3E3783534214.h"
#include "unibill_U3CModuleU3E3783534214MethodDeclarations.h"
#include "unibill_unibill_Dummy_Factory1706693194.h"
#include "unibill_unibill_Dummy_Factory1706693194MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "unibill_ArrayTypes.h"
#include "unibill_unibill_Dummy_Product1158373411.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Void1841601450.h"
#include "unibill_unibill_Dummy_Product1158373411MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// unibill.Dummy.IWindowsIAP unibill.Dummy.Factory::Create(System.Boolean,unibill.Dummy.Product[])
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t Factory_Create_m4265076366_MetadataUsageId;
extern "C"  Il2CppObject * Factory_Create_m4265076366 (Il2CppObject * __this /* static, unused */, bool ___mocked0, ProductU5BU5D_t4190069746* ___products1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Factory_Create_m4265076366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void unibill.Dummy.Product::.ctor()
extern "C"  void Product__ctor_m1876249810 (Product_t1158373411 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
