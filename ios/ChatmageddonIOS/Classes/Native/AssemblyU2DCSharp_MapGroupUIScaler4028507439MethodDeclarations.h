﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapGroupUIScaler
struct MapGroupUIScaler_t4028507439;

#include "codegen/il2cpp-codegen.h"

// System.Void MapGroupUIScaler::.ctor()
extern "C"  void MapGroupUIScaler__ctor_m3165827090 (MapGroupUIScaler_t4028507439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupUIScaler::GlobalUIScale()
extern "C"  void MapGroupUIScaler_GlobalUIScale_m1457899961 (MapGroupUIScaler_t4028507439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
