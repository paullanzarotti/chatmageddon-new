﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeInnerNavScreen
struct AttackHomeInnerNavScreen_t1310676258;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackHomeInnerNavScreen::.ctor()
extern "C"  void AttackHomeInnerNavScreen__ctor_m3968157427 (AttackHomeInnerNavScreen_t1310676258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeInnerNavScreen::Start()
extern "C"  void AttackHomeInnerNavScreen_Start_m1438379631 (AttackHomeInnerNavScreen_t1310676258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeInnerNavScreen::UIClosing()
extern "C"  void AttackHomeInnerNavScreen_UIClosing_m694929348 (AttackHomeInnerNavScreen_t1310676258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeInnerNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackHomeInnerNavScreen_ScreenLoad_m1582095257 (AttackHomeInnerNavScreen_t1310676258 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeInnerNavScreen::UpdateUI()
extern "C"  void AttackHomeInnerNavScreen_UpdateUI_m1981328138 (AttackHomeInnerNavScreen_t1310676258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeInnerNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackHomeInnerNavScreen_ScreenUnload_m3067733917 (AttackHomeInnerNavScreen_t1310676258 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackHomeInnerNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackHomeInnerNavScreen_ValidateScreenNavigation_m1260532918 (AttackHomeInnerNavScreen_t1310676258 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
