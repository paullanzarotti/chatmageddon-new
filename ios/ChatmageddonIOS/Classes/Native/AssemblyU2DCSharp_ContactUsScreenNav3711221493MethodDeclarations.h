﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContactUsScreenNav
struct ContactUsScreenNav_t3711221493;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void ContactUsScreenNav::.ctor()
extern "C"  void ContactUsScreenNav__ctor_m3781372980 (ContactUsScreenNav_t3711221493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactUsScreenNav::Start()
extern "C"  void ContactUsScreenNav_Start_m681755188 (ContactUsScreenNav_t3711221493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactUsScreenNav::UIClosing()
extern "C"  void ContactUsScreenNav_UIClosing_m641798527 (ContactUsScreenNav_t3711221493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactUsScreenNav::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void ContactUsScreenNav_ScreenLoad_m2173927324 (ContactUsScreenNav_t3711221493 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactUsScreenNav::ScreenUnload(NavigationDirection)
extern "C"  void ContactUsScreenNav_ScreenUnload_m1958860348 (ContactUsScreenNav_t3711221493 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ContactUsScreenNav::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool ContactUsScreenNav_ValidateScreenNavigation_m4180795375 (ContactUsScreenNav_t3711221493 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
