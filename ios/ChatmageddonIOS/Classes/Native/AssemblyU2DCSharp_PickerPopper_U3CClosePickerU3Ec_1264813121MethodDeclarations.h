﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickerPopper/<ClosePicker>c__Iterator1
struct U3CClosePickerU3Ec__Iterator1_t1264813121;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PickerPopper/<ClosePicker>c__Iterator1::.ctor()
extern "C"  void U3CClosePickerU3Ec__Iterator1__ctor_m2962977990 (U3CClosePickerU3Ec__Iterator1_t1264813121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickerPopper/<ClosePicker>c__Iterator1::MoveNext()
extern "C"  bool U3CClosePickerU3Ec__Iterator1_MoveNext_m4203101254 (U3CClosePickerU3Ec__Iterator1_t1264813121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<ClosePicker>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CClosePickerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2689607402 (U3CClosePickerU3Ec__Iterator1_t1264813121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<ClosePicker>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CClosePickerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m18050594 (U3CClosePickerU3Ec__Iterator1_t1264813121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<ClosePicker>c__Iterator1::Dispose()
extern "C"  void U3CClosePickerU3Ec__Iterator1_Dispose_m2716471675 (U3CClosePickerU3Ec__Iterator1_t1264813121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<ClosePicker>c__Iterator1::Reset()
extern "C"  void U3CClosePickerU3Ec__Iterator1_Reset_m2781170677 (U3CClosePickerU3Ec__Iterator1_t1264813121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
