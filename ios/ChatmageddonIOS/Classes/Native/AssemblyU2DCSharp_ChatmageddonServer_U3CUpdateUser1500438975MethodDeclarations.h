﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserGender>c__AnonStorey15
struct U3CUpdateUserGenderU3Ec__AnonStorey15_t1500438975;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserGender>c__AnonStorey15::.ctor()
extern "C"  void U3CUpdateUserGenderU3Ec__AnonStorey15__ctor_m3805688118 (U3CUpdateUserGenderU3Ec__AnonStorey15_t1500438975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserGender>c__AnonStorey15::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserGenderU3Ec__AnonStorey15_U3CU3Em__0_m3911056275 (U3CUpdateUserGenderU3Ec__AnonStorey15_t1500438975 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
