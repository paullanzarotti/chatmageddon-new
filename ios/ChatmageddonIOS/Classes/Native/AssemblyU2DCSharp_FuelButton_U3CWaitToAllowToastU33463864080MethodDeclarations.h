﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuelButton/<WaitToAllowToast>c__Iterator0
struct U3CWaitToAllowToastU3Ec__Iterator0_t3463864080;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FuelButton/<WaitToAllowToast>c__Iterator0::.ctor()
extern "C"  void U3CWaitToAllowToastU3Ec__Iterator0__ctor_m440696355 (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuelButton/<WaitToAllowToast>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToAllowToastU3Ec__Iterator0_MoveNext_m1399680145 (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FuelButton/<WaitToAllowToast>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToAllowToastU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2491101313 (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FuelButton/<WaitToAllowToast>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToAllowToastU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3320673241 (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelButton/<WaitToAllowToast>c__Iterator0::Dispose()
extern "C"  void U3CWaitToAllowToastU3Ec__Iterator0_Dispose_m3251221634 (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelButton/<WaitToAllowToast>c__Iterator0::Reset()
extern "C"  void U3CWaitToAllowToastU3Ec__Iterator0_Reset_m1159870288 (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
