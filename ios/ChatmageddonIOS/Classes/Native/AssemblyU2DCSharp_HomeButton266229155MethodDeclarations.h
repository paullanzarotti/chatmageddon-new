﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeButton
struct HomeButton_t266229155;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeButton::.ctor()
extern "C"  void HomeButton__ctor_m2483448798 (HomeButton_t266229155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeButton::OnButtonClick()
extern "C"  void HomeButton_OnButtonClick_m4160925947 (HomeButton_t266229155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeButton::OnScaleTweenFinished()
extern "C"  void HomeButton_OnScaleTweenFinished_m1355211230 (HomeButton_t266229155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
