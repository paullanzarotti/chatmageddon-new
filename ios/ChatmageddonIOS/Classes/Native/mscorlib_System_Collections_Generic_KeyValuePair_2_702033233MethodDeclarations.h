﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21603735834MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PanelType,Panel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3216206073(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t702033233 *, int32_t, Panel_t1787746694 *, const MethodInfo*))KeyValuePair_2__ctor_m314027741_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PanelType,Panel>::get_Key()
#define KeyValuePair_2_get_Key_m3935982027(__this, method) ((  int32_t (*) (KeyValuePair_2_t702033233 *, const MethodInfo*))KeyValuePair_2_get_Key_m1725944715_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PanelType,Panel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3925008884(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t702033233 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m618491520_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PanelType,Panel>::get_Value()
#define KeyValuePair_2_get_Value_m1579408363(__this, method) ((  Panel_t1787746694 * (*) (KeyValuePair_2_t702033233 *, const MethodInfo*))KeyValuePair_2_get_Value_m2780871275_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PanelType,Panel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m224609036(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t702033233 *, Panel_t1787746694 *, const MethodInfo*))KeyValuePair_2_set_Value_m207419112_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PanelType,Panel>::ToString()
#define KeyValuePair_2_ToString_m3177506340(__this, method) ((  String_t* (*) (KeyValuePair_2_t702033233 *, const MethodInfo*))KeyValuePair_2_ToString_m1695002580_gshared)(__this, method)
