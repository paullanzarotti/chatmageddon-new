﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<NotificationManager>::.ctor()
#define MonoSingleton_1__ctor_m3284229316(__this, method) ((  void (*) (MonoSingleton_1_t2139140742 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<NotificationManager>::Awake()
#define MonoSingleton_1_Awake_m1395779455(__this, method) ((  void (*) (MonoSingleton_1_t2139140742 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<NotificationManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2515493933(__this /* static, unused */, method) ((  NotificationManager_t2388475022 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<NotificationManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2411302897(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<NotificationManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m384269134(__this, method) ((  void (*) (MonoSingleton_1_t2139140742 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<NotificationManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2844593366(__this, method) ((  void (*) (MonoSingleton_1_t2139140742 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<NotificationManager>::.cctor()
#define MonoSingleton_1__cctor_m206690581(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
