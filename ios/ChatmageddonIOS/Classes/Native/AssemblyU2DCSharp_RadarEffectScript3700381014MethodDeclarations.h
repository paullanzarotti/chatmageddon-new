﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RadarEffectScript
struct RadarEffectScript_t3700381014;

#include "codegen/il2cpp-codegen.h"

// System.Void RadarEffectScript::.ctor()
extern "C"  void RadarEffectScript__ctor_m2860218111 (RadarEffectScript_t3700381014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarEffectScript::Start()
extern "C"  void RadarEffectScript_Start_m3622007067 (RadarEffectScript_t3700381014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
