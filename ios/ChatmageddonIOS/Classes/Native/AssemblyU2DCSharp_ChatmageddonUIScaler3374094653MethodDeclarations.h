﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonUIScaler
struct ChatmageddonUIScaler_t3374094653;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatmageddonUIScaler::.ctor()
extern "C"  void ChatmageddonUIScaler__ctor_m1032172578 (ChatmageddonUIScaler_t3374094653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonUIScaler::SetUICalculation()
extern "C"  void ChatmageddonUIScaler_SetUICalculation_m3721091723 (ChatmageddonUIScaler_t3374094653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
