﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.CSharp.CSharpCodeGenerator
struct CSharpCodeGenerator_t1166552479;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.CSharp.CSharpCodeGenerator::.ctor()
extern "C"  void CSharpCodeGenerator__ctor_m1527921379 (CSharpCodeGenerator_t1166552479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.CSharp.CSharpCodeGenerator::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void CSharpCodeGenerator__ctor_m4237627593 (CSharpCodeGenerator_t1166552479 * __this, Il2CppObject* ___providerOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.CSharp.CSharpCodeGenerator::.cctor()
extern "C"  void CSharpCodeGenerator__cctor_m3233125654 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Mono.CSharp.CSharpCodeGenerator::get_ProviderOptions()
extern "C"  Il2CppObject* CSharpCodeGenerator_get_ProviderOptions_m4117671164 (CSharpCodeGenerator_t1166552479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
