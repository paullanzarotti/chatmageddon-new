﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberDesc/Builder
struct Builder_t474280692;
// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc922391174.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.PhoneNumberDesc/Builder::.ctor()
extern "C"  void Builder__ctor_m3032793313 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::get_ThisBuilder()
extern "C"  Builder_t474280692 * Builder_get_ThisBuilder_m1906728834 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc/Builder::get_MessageBeingBuilt()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_MessageBeingBuilt_m987482397 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::Clear()
extern "C"  Builder_t474280692 * Builder_Clear_m1686089849 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::Clone()
extern "C"  Builder_t474280692 * Builder_Clone_m2359436551 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc/Builder::get_DefaultInstanceForType()
extern "C"  PhoneNumberDesc_t922391174 * Builder_get_DefaultInstanceForType_m1656258984 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc/Builder::Build()
extern "C"  PhoneNumberDesc_t922391174 * Builder_Build_m1358962582 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc/Builder::BuildPartial()
extern "C"  PhoneNumberDesc_t922391174 * Builder_BuildPartial_m3042406319 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::MergeFrom(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t474280692 * Builder_MergeFrom_m3160686390 (Builder_t474280692 * __this, PhoneNumberDesc_t922391174 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc/Builder::get_HasNationalNumberPattern()
extern "C"  bool Builder_get_HasNationalNumberPattern_m2081464667 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberDesc/Builder::get_NationalNumberPattern()
extern "C"  String_t* Builder_get_NationalNumberPattern_m802086508 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberDesc/Builder::set_NationalNumberPattern(System.String)
extern "C"  void Builder_set_NationalNumberPattern_m2525405631 (Builder_t474280692 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::SetNationalNumberPattern(System.String)
extern "C"  Builder_t474280692 * Builder_SetNationalNumberPattern_m3127722377 (Builder_t474280692 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::ClearNationalNumberPattern()
extern "C"  Builder_t474280692 * Builder_ClearNationalNumberPattern_m2738899120 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc/Builder::get_HasPossibleNumberPattern()
extern "C"  bool Builder_get_HasPossibleNumberPattern_m4268761748 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberDesc/Builder::get_PossibleNumberPattern()
extern "C"  String_t* Builder_get_PossibleNumberPattern_m2971878579 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberDesc/Builder::set_PossibleNumberPattern(System.String)
extern "C"  void Builder_set_PossibleNumberPattern_m210751014 (Builder_t474280692 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::SetPossibleNumberPattern(System.String)
extern "C"  Builder_t474280692 * Builder_SetPossibleNumberPattern_m840104486 (Builder_t474280692 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::ClearPossibleNumberPattern()
extern "C"  Builder_t474280692 * Builder_ClearPossibleNumberPattern_m3937485687 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc/Builder::get_HasExampleNumber()
extern "C"  bool Builder_get_HasExampleNumber_m1347070933 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberDesc/Builder::get_ExampleNumber()
extern "C"  String_t* Builder_get_ExampleNumber_m282982736 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberDesc/Builder::set_ExampleNumber(System.String)
extern "C"  void Builder_set_ExampleNumber_m2298609221 (Builder_t474280692 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::SetExampleNumber(System.String)
extern "C"  Builder_t474280692 * Builder_SetExampleNumber_m706028179 (Builder_t474280692 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc/Builder::ClearExampleNumber()
extern "C"  Builder_t474280692 * Builder_ClearExampleNumber_m941593836 (Builder_t474280692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
