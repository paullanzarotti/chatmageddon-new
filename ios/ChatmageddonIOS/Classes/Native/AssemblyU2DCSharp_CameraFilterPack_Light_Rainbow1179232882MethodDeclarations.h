﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Light_Rainbow
struct CameraFilterPack_Light_Rainbow_t1179232882;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Light_Rainbow::.ctor()
extern "C"  void CameraFilterPack_Light_Rainbow__ctor_m2153993879 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Rainbow::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Light_Rainbow_get_material_m2044479296 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::Start()
extern "C"  void CameraFilterPack_Light_Rainbow_Start_m837051091 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Light_Rainbow_OnRenderImage_m3854928571 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::OnValidate()
extern "C"  void CameraFilterPack_Light_Rainbow_OnValidate_m2123105190 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::Update()
extern "C"  void CameraFilterPack_Light_Rainbow_Update_m3430233248 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::OnDisable()
extern "C"  void CameraFilterPack_Light_Rainbow_OnDisable_m2525042238 (CameraFilterPack_Light_Rainbow_t1179232882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
