﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.SharingGUIManager
struct SharingGUIManager_t369570694;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.SharingGUIManager::.ctor()
extern "C"  void SharingGUIManager__ctor_m3550221908 (SharingGUIManager_t369570694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingGUIManager::Start()
extern "C"  void SharingGUIManager_Start_m1592983088 (SharingGUIManager_t369570694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingGUIManager::OnGUI()
extern "C"  void SharingGUIManager_OnGUI_m3997058912 (SharingGUIManager_t369570694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingGUIManager::.cctor()
extern "C"  void SharingGUIManager__cctor_m2374076729 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingGUIManager::<Start>m__0(System.String)
extern "C"  void SharingGUIManager_U3CStartU3Em__0_m1222206771 (Il2CppObject * __this /* static, unused */, String_t* ___activityType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingGUIManager::<Start>m__1()
extern "C"  void SharingGUIManager_U3CStartU3Em__1_m1765766374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
