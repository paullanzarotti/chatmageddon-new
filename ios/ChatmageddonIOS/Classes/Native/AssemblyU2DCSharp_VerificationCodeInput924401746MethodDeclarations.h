﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VerificationCodeInput
struct VerificationCodeInput_t924401746;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VerificationCodeInput::.ctor()
extern "C"  void VerificationCodeInput__ctor_m1686097505 (VerificationCodeInput_t924401746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationCodeInput::InputSubmit()
extern "C"  void VerificationCodeInput_InputSubmit_m8653775 (VerificationCodeInput_t924401746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VerificationCodeInput::ValidateInput()
extern "C"  bool VerificationCodeInput_ValidateInput_m2922263993 (VerificationCodeInput_t924401746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationCodeInput::<InputSubmit>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void VerificationCodeInput_U3CInputSubmitU3Em__0_m4212791937 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
