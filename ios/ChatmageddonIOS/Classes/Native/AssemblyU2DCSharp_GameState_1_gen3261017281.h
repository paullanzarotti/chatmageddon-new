﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen559355580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameState`1<ChatmageddonGamestate>
struct  GameState_1_t3261017281  : public MonoSingleton_1_t559355580
{
public:
	// System.String GameState`1::activeState
	String_t* ___activeState_3;
	// System.String GameState`1::previousState
	String_t* ___previousState_4;

public:
	inline static int32_t get_offset_of_activeState_3() { return static_cast<int32_t>(offsetof(GameState_1_t3261017281, ___activeState_3)); }
	inline String_t* get_activeState_3() const { return ___activeState_3; }
	inline String_t** get_address_of_activeState_3() { return &___activeState_3; }
	inline void set_activeState_3(String_t* value)
	{
		___activeState_3 = value;
		Il2CppCodeGenWriteBarrier(&___activeState_3, value);
	}

	inline static int32_t get_offset_of_previousState_4() { return static_cast<int32_t>(offsetof(GameState_1_t3261017281, ___previousState_4)); }
	inline String_t* get_previousState_4() const { return ___previousState_4; }
	inline String_t** get_address_of_previousState_4() { return &___previousState_4; }
	inline void set_previousState_4(String_t* value)
	{
		___previousState_4 = value;
		Il2CppCodeGenWriteBarrier(&___previousState_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
