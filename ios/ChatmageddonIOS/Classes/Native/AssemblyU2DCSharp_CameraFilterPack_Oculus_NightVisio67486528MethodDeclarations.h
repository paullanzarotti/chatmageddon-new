﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Oculus_NightVision3
struct CameraFilterPack_Oculus_NightVision3_t67486528;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Oculus_NightVision3::.ctor()
extern "C"  void CameraFilterPack_Oculus_NightVision3__ctor_m885935987 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision3::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Oculus_NightVision3_get_material_m1057277250 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::Start()
extern "C"  void CameraFilterPack_Oculus_NightVision3_Start_m2546804223 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Oculus_NightVision3_OnRenderImage_m77500039 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::OnValidate()
extern "C"  void CameraFilterPack_Oculus_NightVision3_OnValidate_m3801705512 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::Update()
extern "C"  void CameraFilterPack_Oculus_NightVision3_Update_m2558996418 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::OnDisable()
extern "C"  void CameraFilterPack_Oculus_NightVision3_OnDisable_m180132860 (CameraFilterPack_Oculus_NightVision3_t67486528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
