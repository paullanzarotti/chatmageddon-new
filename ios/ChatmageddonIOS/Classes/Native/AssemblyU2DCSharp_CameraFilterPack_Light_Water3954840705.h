﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Light_Water
struct  CameraFilterPack_Light_Water_t3954840705  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Light_Water::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Light_Water::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Light_Water::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Light_Water::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Light_Water::Size
	float ___Size_6;
	// System.Single CameraFilterPack_Light_Water::Alpha
	float ___Alpha_7;
	// System.Single CameraFilterPack_Light_Water::Distance
	float ___Distance_8;
	// System.Single CameraFilterPack_Light_Water::Speed
	float ___Speed_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Size_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___Size_6)); }
	inline float get_Size_6() const { return ___Size_6; }
	inline float* get_address_of_Size_6() { return &___Size_6; }
	inline void set_Size_6(float value)
	{
		___Size_6 = value;
	}

	inline static int32_t get_offset_of_Alpha_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___Alpha_7)); }
	inline float get_Alpha_7() const { return ___Alpha_7; }
	inline float* get_address_of_Alpha_7() { return &___Alpha_7; }
	inline void set_Alpha_7(float value)
	{
		___Alpha_7 = value;
	}

	inline static int32_t get_offset_of_Distance_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___Distance_8)); }
	inline float get_Distance_8() const { return ___Distance_8; }
	inline float* get_address_of_Distance_8() { return &___Distance_8; }
	inline void set_Distance_8(float value)
	{
		___Distance_8 = value;
	}

	inline static int32_t get_offset_of_Speed_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705, ___Speed_9)); }
	inline float get_Speed_9() const { return ___Speed_9; }
	inline float* get_address_of_Speed_9() { return &___Speed_9; }
	inline void set_Speed_9(float value)
	{
		___Speed_9 = value;
	}
};

struct CameraFilterPack_Light_Water_t3954840705_StaticFields
{
public:
	// System.Single CameraFilterPack_Light_Water::ChangeAlpha
	float ___ChangeAlpha_10;
	// System.Single CameraFilterPack_Light_Water::ChangeDistance
	float ___ChangeDistance_11;
	// System.Single CameraFilterPack_Light_Water::ChangeSize
	float ___ChangeSize_12;

public:
	inline static int32_t get_offset_of_ChangeAlpha_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705_StaticFields, ___ChangeAlpha_10)); }
	inline float get_ChangeAlpha_10() const { return ___ChangeAlpha_10; }
	inline float* get_address_of_ChangeAlpha_10() { return &___ChangeAlpha_10; }
	inline void set_ChangeAlpha_10(float value)
	{
		___ChangeAlpha_10 = value;
	}

	inline static int32_t get_offset_of_ChangeDistance_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705_StaticFields, ___ChangeDistance_11)); }
	inline float get_ChangeDistance_11() const { return ___ChangeDistance_11; }
	inline float* get_address_of_ChangeDistance_11() { return &___ChangeDistance_11; }
	inline void set_ChangeDistance_11(float value)
	{
		___ChangeDistance_11 = value;
	}

	inline static int32_t get_offset_of_ChangeSize_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Light_Water_t3954840705_StaticFields, ___ChangeSize_12)); }
	inline float get_ChangeSize_12() const { return ___ChangeSize_12; }
	inline float* get_address_of_ChangeSize_12() { return &___ChangeSize_12; }
	inline void set_ChangeSize_12(float value)
	{
		___ChangeSize_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
