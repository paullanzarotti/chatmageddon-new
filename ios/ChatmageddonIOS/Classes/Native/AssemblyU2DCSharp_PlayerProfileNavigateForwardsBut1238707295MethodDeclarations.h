﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileNavigateForwardsButton
struct PlayerProfileNavigateForwardsButton_t1238707295;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerProfileNavigateForwardsButton::.ctor()
extern "C"  void PlayerProfileNavigateForwardsButton__ctor_m3634526326 (PlayerProfileNavigateForwardsButton_t1238707295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileNavigateForwardsButton::OnButtonClick()
extern "C"  void PlayerProfileNavigateForwardsButton_OnButtonClick_m3035047303 (PlayerProfileNavigateForwardsButton_t1238707295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
