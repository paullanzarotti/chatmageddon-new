﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// User
struct User_t719925459;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Texture
struct Texture_t2243626319;
// Shield
struct Shield_t3327121081;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"

// System.Void User::.ctor(System.Collections.Hashtable)
extern "C"  void User__ctor_m2613408474 (User_t719925459 * __this, Hashtable_t909839986 * ___userHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::AddNewUserData(System.Collections.Hashtable)
extern "C"  void User_AddNewUserData_m1463459072 (User_t719925459 * __this, Hashtable_t909839986 * ___userHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::AddNewStatData(System.Collections.Hashtable)
extern "C"  void User_AddNewStatData_m3644275485 (User_t719925459 * __this, Hashtable_t909839986 * ___statHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::UpdateKnockoutData(System.Collections.Hashtable)
extern "C"  void User_UpdateKnockoutData_m3519987727 (User_t719925459 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean User::GetboolFromString(System.String)
extern "C"  bool User_GetboolFromString_m3228516247 (User_t719925459 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Gender User::GetGenderFromString(System.String)
extern "C"  int32_t User_GetGenderFromString_m3873467884 (User_t719925459 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Audience User::GetAudienceFromString(System.String)
extern "C"  int32_t User_GetAudienceFromString_m2135198796 (User_t719925459 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetUsername(System.String)
extern "C"  void User_SetUsername_m3499064600 (User_t719925459 * __this, String_t* ___usernameToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetUsername()
extern "C"  String_t* User_GetUsername_m3962550579 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetFirstName(System.String)
extern "C"  void User_SetFirstName_m1167106307 (User_t719925459 * __this, String_t* ___firstNameToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetFirstName()
extern "C"  String_t* User_GetFirstName_m2758212472 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetSecondName(System.String)
extern "C"  void User_SetSecondName_m3164596413 (User_t719925459 * __this, String_t* ___secondNameToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetSecondName()
extern "C"  String_t* User_GetSecondName_m2355537218 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetFullName(System.String)
extern "C"  void User_SetFullName_m1692152578 (User_t719925459 * __this, String_t* ___fullNameToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetFullName()
extern "C"  String_t* User_GetFullName_m1642387097 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetEmail(System.String)
extern "C"  void User_SetEmail_m2011861658 (User_t719925459 * __this, String_t* ___emailToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetEmail()
extern "C"  String_t* User_GetEmail_m4206096257 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetID(System.String)
extern "C"  void User_SetID_m1856835131 (User_t719925459 * __this, String_t* ___IDToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetID()
extern "C"  String_t* User_GetID_m1606899520 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetGender(Gender)
extern "C"  void User_SetGender_m3410000268 (User_t719925459 * __this, int32_t ___genderToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Gender User::GetGender()
extern "C"  int32_t User_GetGender_m2767708915 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetRankName(System.String)
extern "C"  void User_SetRankName_m1393779823 (User_t719925459 * __this, String_t* ___rankToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetRankName()
extern "C"  String_t* User_GetRankName_m1586895408 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetRankLevel(System.Int32)
extern "C"  void User_SetRankLevel_m784945417 (User_t719925459 * __this, int32_t ___rankToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 User::GetRankLevel()
extern "C"  int32_t User_GetRankLevel_m1380276626 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D User::CalculateTexture(System.Int32,System.Int32,System.Single,System.Single,System.Single,UnityEngine.Texture2D)
extern "C"  Texture2D_t3542995729 * User_CalculateTexture_m4208086963 (User_t719925459 * __this, int32_t ___h0, int32_t ___w1, float ___r2, float ___cx3, float ___cy4, Texture2D_t3542995729 * ___sourceTex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetUserAvatar(UnityEngine.Texture)
extern "C"  void User_SetUserAvatar_m4006846452 (User_t719925459 * __this, Texture_t2243626319 * ___textureToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture User::GetUserAvatar()
extern "C"  Texture_t2243626319 * User_GetUserAvatar_m1568865019 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetProfileImageURL(System.String)
extern "C"  void User_SetProfileImageURL_m2251375873 (User_t719925459 * __this, String_t* ___imageURLToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::GetProfileImageUrl()
extern "C"  String_t* User_GetProfileImageUrl_m1524992026 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetUserAudience(Audience)
extern "C"  void User_SetUserAudience_m1274780805 (User_t719925459 * __this, int32_t ___audienceToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Audience User::GetUserAudience()
extern "C"  int32_t User_GetUserAudience_m2064638984 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::UpdateDailyPoints(System.Int32)
extern "C"  void User_UpdateDailyPoints_m3824119854 (User_t719925459 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::EnterStealthMode(System.DateTime)
extern "C"  void User_EnterStealthMode_m2512144816 (User_t719925459 * __this, DateTime_t693205669  ___deativateAt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::ExitStealthMode()
extern "C"  void User_ExitStealthMode_m4032515930 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetUserStealthStatus(System.Boolean)
extern "C"  void User_SetUserStealthStatus_m1346937537 (User_t719925459 * __this, bool ___statusToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean User::GetUserStealthStatus()
extern "C"  bool User_GetUserStealthStatus_m2174563722 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::GenerateStealthActivateTime()
extern "C"  void User_GenerateStealthActivateTime_m934795268 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetStealthDuration(System.Int32)
extern "C"  void User_SetStealthDuration_m1563407126 (User_t719925459 * __this, int32_t ___durationToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 User::GetStealthDuration()
extern "C"  int32_t User_GetStealthDuration_m1358343207 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetStealthStartHour(System.Int32)
extern "C"  void User_SetStealthStartHour_m152216616 (User_t719925459 * __this, int32_t ___hourToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 User::GetStealthStartHour()
extern "C"  int32_t User_GetStealthStartHour_m3740009413 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetStealthStartMin(System.Int32)
extern "C"  void User_SetStealthStartMin_m3978436462 (User_t719925459 * __this, int32_t ___minToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 User::GetStealthStartMin()
extern "C"  int32_t User_GetStealthStartMin_m452950579 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::KnockOutUser(System.DateTime)
extern "C"  void User_KnockOutUser_m3949102117 (User_t719925459 * __this, DateTime_t693205669  ___deativateAt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::ExitKnockOut()
extern "C"  void User_ExitKnockOut_m815129388 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetUserKnockoutStatus(System.Boolean)
extern "C"  void User_SetUserKnockoutStatus_m2409352638 (User_t719925459 * __this, bool ___statusToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean User::GetUserKnockOutStatus()
extern "C"  bool User_GetUserKnockOutStatus_m785227929 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetActiveShield(Shield)
extern "C"  void User_SetActiveShield_m4195006786 (User_t719925459 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean User::get_mineShielded()
extern "C"  bool User_get_mineShielded_m4077626408 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean User::get_missileShielded()
extern "C"  bool User_get_missileShielded_m1115021069 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetAllowLocation(System.Boolean)
extern "C"  void User_SetAllowLocation_m2584520667 (User_t719925459 * __this, bool ___locationToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean User::GetAllowLocation()
extern "C"  bool User_GetAllowLocation_m705717784 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetLongitude(System.Single)
extern "C"  void User_SetLongitude_m2611637698 (User_t719925459 * __this, float ___longitudeToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single User::GetLongitude()
extern "C"  float User_GetLongitude_m2128835213 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::SetLatitude(System.Single)
extern "C"  void User_SetLatitude_m1219378189 (User_t719925459 * __this, float ___latitudeToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single User::GetLatitude()
extern "C"  float User_GetLatitude_m1048514152 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
