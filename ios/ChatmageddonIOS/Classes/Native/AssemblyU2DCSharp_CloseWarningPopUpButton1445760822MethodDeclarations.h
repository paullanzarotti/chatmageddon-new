﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseWarningPopUpButton
struct CloseWarningPopUpButton_t1445760822;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseWarningPopUpButton::.ctor()
extern "C"  void CloseWarningPopUpButton__ctor_m2444640595 (CloseWarningPopUpButton_t1445760822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseWarningPopUpButton::OnButtonClick()
extern "C"  void CloseWarningPopUpButton_OnButtonClick_m1683915322 (CloseWarningPopUpButton_t1445760822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
