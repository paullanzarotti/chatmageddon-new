﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<ProfileNavScreen>
struct DefaultComparer_t3910895383;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<ProfileNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m3349555958_gshared (DefaultComparer_t3910895383 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3349555958(__this, method) ((  void (*) (DefaultComparer_t3910895383 *, const MethodInfo*))DefaultComparer__ctor_m3349555958_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<ProfileNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1549626903_gshared (DefaultComparer_t3910895383 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1549626903(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3910895383 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m1549626903_gshared)(__this, ___x0, ___y1, method)
