﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeleteThreadButton
struct DeleteThreadButton_t3881568975;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DeleteThreadButton::.ctor()
extern "C"  void DeleteThreadButton__ctor_m2120176910 (DeleteThreadButton_t3881568975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteThreadButton::OnButtonClick()
extern "C"  void DeleteThreadButton_OnButtonClick_m2052540019 (DeleteThreadButton_t3881568975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteThreadButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void DeleteThreadButton_U3COnButtonClickU3Em__0_m2341614869 (DeleteThreadButton_t3881568975 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
