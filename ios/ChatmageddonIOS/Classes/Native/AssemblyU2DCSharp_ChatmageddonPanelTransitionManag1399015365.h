﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_PanelTransitionManager_1_gen2526031859.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonPanelTransitionManager
struct  ChatmageddonPanelTransitionManager_t1399015365  : public PanelTransitionManager_1_t2526031859
{
public:
	// UnityEngine.GameObject ChatmageddonPanelTransitionManager::bottomUI
	GameObject_t1756533147 * ___bottomUI_8;
	// UnityEngine.GameObject ChatmageddonPanelTransitionManager::topTabs
	GameObject_t1756533147 * ___topTabs_9;
	// UIPanel ChatmageddonPanelTransitionManager::clipPanel
	UIPanel_t1795085332 * ___clipPanel_10;
	// System.Boolean ChatmageddonPanelTransitionManager::showMapUI
	bool ___showMapUI_11;

public:
	inline static int32_t get_offset_of_bottomUI_8() { return static_cast<int32_t>(offsetof(ChatmageddonPanelTransitionManager_t1399015365, ___bottomUI_8)); }
	inline GameObject_t1756533147 * get_bottomUI_8() const { return ___bottomUI_8; }
	inline GameObject_t1756533147 ** get_address_of_bottomUI_8() { return &___bottomUI_8; }
	inline void set_bottomUI_8(GameObject_t1756533147 * value)
	{
		___bottomUI_8 = value;
		Il2CppCodeGenWriteBarrier(&___bottomUI_8, value);
	}

	inline static int32_t get_offset_of_topTabs_9() { return static_cast<int32_t>(offsetof(ChatmageddonPanelTransitionManager_t1399015365, ___topTabs_9)); }
	inline GameObject_t1756533147 * get_topTabs_9() const { return ___topTabs_9; }
	inline GameObject_t1756533147 ** get_address_of_topTabs_9() { return &___topTabs_9; }
	inline void set_topTabs_9(GameObject_t1756533147 * value)
	{
		___topTabs_9 = value;
		Il2CppCodeGenWriteBarrier(&___topTabs_9, value);
	}

	inline static int32_t get_offset_of_clipPanel_10() { return static_cast<int32_t>(offsetof(ChatmageddonPanelTransitionManager_t1399015365, ___clipPanel_10)); }
	inline UIPanel_t1795085332 * get_clipPanel_10() const { return ___clipPanel_10; }
	inline UIPanel_t1795085332 ** get_address_of_clipPanel_10() { return &___clipPanel_10; }
	inline void set_clipPanel_10(UIPanel_t1795085332 * value)
	{
		___clipPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___clipPanel_10, value);
	}

	inline static int32_t get_offset_of_showMapUI_11() { return static_cast<int32_t>(offsetof(ChatmageddonPanelTransitionManager_t1399015365, ___showMapUI_11)); }
	inline bool get_showMapUI_11() const { return ___showMapUI_11; }
	inline bool* get_address_of_showMapUI_11() { return &___showMapUI_11; }
	inline void set_showMapUI_11(bool value)
	{
		___showMapUI_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
