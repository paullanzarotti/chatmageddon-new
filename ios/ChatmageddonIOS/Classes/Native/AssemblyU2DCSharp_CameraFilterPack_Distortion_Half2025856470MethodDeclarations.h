﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Half_Sphere
struct CameraFilterPack_Distortion_Half_Sphere_t2025856470;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Half_Sphere::.ctor()
extern "C"  void CameraFilterPack_Distortion_Half_Sphere__ctor_m4136415893 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Half_Sphere::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Half_Sphere_get_material_m1479298442 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::Start()
extern "C"  void CameraFilterPack_Distortion_Half_Sphere_Start_m403705949 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Half_Sphere_OnRenderImage_m669840461 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Half_Sphere_OnValidate_m869669066 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::Update()
extern "C"  void CameraFilterPack_Distortion_Half_Sphere_Update_m558476088 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Half_Sphere_OnDisable_m1117701974 (CameraFilterPack_Distortion_Half_Sphere_t2025856470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
