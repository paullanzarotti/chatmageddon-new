﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenColor
struct TweenColor_t3390486518;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen2319264450.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackSearchNavigationController
struct  AttackSearchNavigationController_t4133786906  : public NavigationController_2_t2319264450
{
public:
	// TweenColor AttackSearchNavigationController::friendsTab
	TweenColor_t3390486518 * ___friendsTab_8;
	// TweenColor AttackSearchNavigationController::globalTab
	TweenColor_t3390486518 * ___globalTab_9;
	// UnityEngine.GameObject AttackSearchNavigationController::friendsObject
	GameObject_t1756533147 * ___friendsObject_10;
	// UnityEngine.GameObject AttackSearchNavigationController::globalObject
	GameObject_t1756533147 * ___globalObject_11;
	// UnityEngine.Vector3 AttackSearchNavigationController::activePos
	Vector3_t2243707580  ___activePos_12;
	// UnityEngine.Vector3 AttackSearchNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_13;
	// UnityEngine.Vector3 AttackSearchNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_14;

public:
	inline static int32_t get_offset_of_friendsTab_8() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___friendsTab_8)); }
	inline TweenColor_t3390486518 * get_friendsTab_8() const { return ___friendsTab_8; }
	inline TweenColor_t3390486518 ** get_address_of_friendsTab_8() { return &___friendsTab_8; }
	inline void set_friendsTab_8(TweenColor_t3390486518 * value)
	{
		___friendsTab_8 = value;
		Il2CppCodeGenWriteBarrier(&___friendsTab_8, value);
	}

	inline static int32_t get_offset_of_globalTab_9() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___globalTab_9)); }
	inline TweenColor_t3390486518 * get_globalTab_9() const { return ___globalTab_9; }
	inline TweenColor_t3390486518 ** get_address_of_globalTab_9() { return &___globalTab_9; }
	inline void set_globalTab_9(TweenColor_t3390486518 * value)
	{
		___globalTab_9 = value;
		Il2CppCodeGenWriteBarrier(&___globalTab_9, value);
	}

	inline static int32_t get_offset_of_friendsObject_10() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___friendsObject_10)); }
	inline GameObject_t1756533147 * get_friendsObject_10() const { return ___friendsObject_10; }
	inline GameObject_t1756533147 ** get_address_of_friendsObject_10() { return &___friendsObject_10; }
	inline void set_friendsObject_10(GameObject_t1756533147 * value)
	{
		___friendsObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___friendsObject_10, value);
	}

	inline static int32_t get_offset_of_globalObject_11() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___globalObject_11)); }
	inline GameObject_t1756533147 * get_globalObject_11() const { return ___globalObject_11; }
	inline GameObject_t1756533147 ** get_address_of_globalObject_11() { return &___globalObject_11; }
	inline void set_globalObject_11(GameObject_t1756533147 * value)
	{
		___globalObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___globalObject_11, value);
	}

	inline static int32_t get_offset_of_activePos_12() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___activePos_12)); }
	inline Vector3_t2243707580  get_activePos_12() const { return ___activePos_12; }
	inline Vector3_t2243707580 * get_address_of_activePos_12() { return &___activePos_12; }
	inline void set_activePos_12(Vector3_t2243707580  value)
	{
		___activePos_12 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_13() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___leftInactivePos_13)); }
	inline Vector3_t2243707580  get_leftInactivePos_13() const { return ___leftInactivePos_13; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_13() { return &___leftInactivePos_13; }
	inline void set_leftInactivePos_13(Vector3_t2243707580  value)
	{
		___leftInactivePos_13 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_14() { return static_cast<int32_t>(offsetof(AttackSearchNavigationController_t4133786906, ___rightInactivePos_14)); }
	inline Vector3_t2243707580  get_rightInactivePos_14() const { return ___rightInactivePos_14; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_14() { return &___rightInactivePos_14; }
	inline void set_rightInactivePos_14(Vector3_t2243707580  value)
	{
		___rightInactivePos_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
