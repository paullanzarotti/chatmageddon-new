﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITable
struct UITable_t3717403602;
// UIScrollView
struct UIScrollView_t3033954930;
// SettingsProfileContent
struct SettingsProfileContent_t3280006997;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsProfileScreenList
struct  SettingsProfileScreenList_t677344198  : public MonoBehaviour_t1158329972
{
public:
	// UITable SettingsProfileScreenList::table
	UITable_t3717403602 * ___table_2;
	// UIScrollView SettingsProfileScreenList::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_3;
	// System.Boolean SettingsProfileScreenList::listLoaded
	bool ___listLoaded_4;
	// SettingsProfileContent SettingsProfileScreenList::content
	SettingsProfileContent_t3280006997 * ___content_5;
	// UnityEngine.Transform SettingsProfileScreenList::itemPrefab
	Transform_t3275118058 * ___itemPrefab_6;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(SettingsProfileScreenList_t677344198, ___table_2)); }
	inline UITable_t3717403602 * get_table_2() const { return ___table_2; }
	inline UITable_t3717403602 ** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(UITable_t3717403602 * value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier(&___table_2, value);
	}

	inline static int32_t get_offset_of_draggablePanel_3() { return static_cast<int32_t>(offsetof(SettingsProfileScreenList_t677344198, ___draggablePanel_3)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_3() const { return ___draggablePanel_3; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_3() { return &___draggablePanel_3; }
	inline void set_draggablePanel_3(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_3, value);
	}

	inline static int32_t get_offset_of_listLoaded_4() { return static_cast<int32_t>(offsetof(SettingsProfileScreenList_t677344198, ___listLoaded_4)); }
	inline bool get_listLoaded_4() const { return ___listLoaded_4; }
	inline bool* get_address_of_listLoaded_4() { return &___listLoaded_4; }
	inline void set_listLoaded_4(bool value)
	{
		___listLoaded_4 = value;
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(SettingsProfileScreenList_t677344198, ___content_5)); }
	inline SettingsProfileContent_t3280006997 * get_content_5() const { return ___content_5; }
	inline SettingsProfileContent_t3280006997 ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(SettingsProfileContent_t3280006997 * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier(&___content_5, value);
	}

	inline static int32_t get_offset_of_itemPrefab_6() { return static_cast<int32_t>(offsetof(SettingsProfileScreenList_t677344198, ___itemPrefab_6)); }
	inline Transform_t3275118058 * get_itemPrefab_6() const { return ___itemPrefab_6; }
	inline Transform_t3275118058 ** get_address_of_itemPrefab_6() { return &___itemPrefab_6; }
	inline void set_itemPrefab_6(Transform_t3275118058 * value)
	{
		___itemPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
