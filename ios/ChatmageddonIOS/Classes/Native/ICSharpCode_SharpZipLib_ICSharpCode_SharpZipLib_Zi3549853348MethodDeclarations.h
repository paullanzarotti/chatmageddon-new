﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.TestStatus
struct TestStatus_t3549853348;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t1110175137;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t1764014695;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1110175137.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4223507703.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1764014695.h"

// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile)
extern "C"  void TestStatus__ctor_m1152201749 (TestStatus_t3549853348 * __this, ZipFile_t1110175137 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.TestStatus::get_ErrorCount()
extern "C"  int32_t TestStatus_get_ErrorCount_m3841239444 (TestStatus_t3549853348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::AddError()
extern "C"  void TestStatus_AddError_m3731266275 (TestStatus_t3549853348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetOperation(ICSharpCode.SharpZipLib.Zip.TestOperation)
extern "C"  void TestStatus_SetOperation_m1653784204 (TestStatus_t3549853348 * __this, int32_t ___operation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void TestStatus_SetEntry_m3439478977 (TestStatus_t3549853348 * __this, ZipEntry_t1764014695 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetBytesTested(System.Int64)
extern "C"  void TestStatus_SetBytesTested_m1639387106 (TestStatus_t3549853348 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
