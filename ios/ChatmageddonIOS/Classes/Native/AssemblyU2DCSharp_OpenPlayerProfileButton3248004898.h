﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenPlayerProfileButton
struct  OpenPlayerProfileButton_t3248004898  : public SFXButton_t792651341
{
public:
	// System.Boolean OpenPlayerProfileButton::activePlayer
	bool ___activePlayer_5;
	// Friend OpenPlayerProfileButton::friend
	Friend_t3555014108 * ___friend_6;
	// System.Boolean OpenPlayerProfileButton::modalSwitch
	bool ___modalSwitch_7;

public:
	inline static int32_t get_offset_of_activePlayer_5() { return static_cast<int32_t>(offsetof(OpenPlayerProfileButton_t3248004898, ___activePlayer_5)); }
	inline bool get_activePlayer_5() const { return ___activePlayer_5; }
	inline bool* get_address_of_activePlayer_5() { return &___activePlayer_5; }
	inline void set_activePlayer_5(bool value)
	{
		___activePlayer_5 = value;
	}

	inline static int32_t get_offset_of_friend_6() { return static_cast<int32_t>(offsetof(OpenPlayerProfileButton_t3248004898, ___friend_6)); }
	inline Friend_t3555014108 * get_friend_6() const { return ___friend_6; }
	inline Friend_t3555014108 ** get_address_of_friend_6() { return &___friend_6; }
	inline void set_friend_6(Friend_t3555014108 * value)
	{
		___friend_6 = value;
		Il2CppCodeGenWriteBarrier(&___friend_6, value);
	}

	inline static int32_t get_offset_of_modalSwitch_7() { return static_cast<int32_t>(offsetof(OpenPlayerProfileButton_t3248004898, ___modalSwitch_7)); }
	inline bool get_modalSwitch_7() const { return ___modalSwitch_7; }
	inline bool* get_address_of_modalSwitch_7() { return &___modalSwitch_7; }
	inline void set_modalSwitch_7(bool value)
	{
		___modalSwitch_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
