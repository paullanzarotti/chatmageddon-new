﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraGUIManagerThree
struct EtceteraGUIManagerThree_t3698463085;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void EtceteraGUIManagerThree::.ctor()
extern "C"  void EtceteraGUIManagerThree__ctor_m2942502104 (EtceteraGUIManagerThree_t3698463085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerThree::OnGUI()
extern "C"  void EtceteraGUIManagerThree_OnGUI_m1383888392 (EtceteraGUIManagerThree_t3698463085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerThree::<OnGUI>m__0(UnityEngine.Texture2D)
extern "C"  void EtceteraGUIManagerThree_U3COnGUIU3Em__0_m1345812277 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
