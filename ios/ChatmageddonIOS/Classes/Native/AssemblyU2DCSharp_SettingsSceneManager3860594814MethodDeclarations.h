﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsSceneManager
struct SettingsSceneManager_t3860594814;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsSceneManager::.ctor()
extern "C"  void SettingsSceneManager__ctor_m2833552215 (SettingsSceneManager_t3860594814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsSceneManager::InitScene()
extern "C"  void SettingsSceneManager_InitScene_m1079628595 (SettingsSceneManager_t3860594814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsSceneManager::StartScene()
extern "C"  void SettingsSceneManager_StartScene_m471426753 (SettingsSceneManager_t3860594814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
