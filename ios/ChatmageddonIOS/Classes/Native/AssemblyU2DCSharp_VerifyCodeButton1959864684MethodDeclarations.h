﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VerifyCodeButton
struct VerifyCodeButton_t1959864684;

#include "codegen/il2cpp-codegen.h"

// System.Void VerifyCodeButton::.ctor()
extern "C"  void VerifyCodeButton__ctor_m3255555879 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerifyCodeButton::Start()
extern "C"  void VerifyCodeButton_Start_m85647915 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerifyCodeButton::CheckCount()
extern "C"  void VerifyCodeButton_CheckCount_m2811372368 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerifyCodeButton::OnButtonClick()
extern "C"  void VerifyCodeButton_OnButtonClick_m3263143044 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerifyCodeButton::<Start>m__0()
extern "C"  void VerifyCodeButton_U3CStartU3Em__0_m682529774 (VerifyCodeButton_t1959864684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
