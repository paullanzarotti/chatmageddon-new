﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Spot
struct CameraFilterPack_FX_Spot_t1081357216;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Spot::.ctor()
extern "C"  void CameraFilterPack_FX_Spot__ctor_m3996154401 (CameraFilterPack_FX_Spot_t1081357216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Spot::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Spot_get_material_m714996322 (CameraFilterPack_FX_Spot_t1081357216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::Start()
extern "C"  void CameraFilterPack_FX_Spot_Start_m2621336193 (CameraFilterPack_FX_Spot_t1081357216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Spot_OnRenderImage_m2551055601 (CameraFilterPack_FX_Spot_t1081357216 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::OnValidate()
extern "C"  void CameraFilterPack_FX_Spot_OnValidate_m1090258936 (CameraFilterPack_FX_Spot_t1081357216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::Update()
extern "C"  void CameraFilterPack_FX_Spot_Update_m1482028170 (CameraFilterPack_FX_Spot_t1081357216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::OnDisable()
extern "C"  void CameraFilterPack_FX_Spot_OnDisable_m2213021140 (CameraFilterPack_FX_Spot_t1081357216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
