﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsHomeUIScaler
struct SettingsHomeUIScaler_t3836684136;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsHomeUIScaler::.ctor()
extern "C"  void SettingsHomeUIScaler__ctor_m2567722655 (SettingsHomeUIScaler_t3836684136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsHomeUIScaler::GlobalUIScale()
extern "C"  void SettingsHomeUIScaler_GlobalUIScale_m1995132538 (SettingsHomeUIScaler_t3836684136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
