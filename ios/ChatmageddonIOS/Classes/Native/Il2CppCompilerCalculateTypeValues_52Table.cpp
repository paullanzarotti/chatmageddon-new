﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UICamera_VoidDelegate3315330999.h"
#include "AssemblyU2DCSharp_UICamera_BoolDelegate777098953.h"
#include "AssemblyU2DCSharp_UICamera_FloatDelegate3246177583.h"
#include "AssemblyU2DCSharp_UICamera_VectorDelegate1823833398.h"
#include "AssemblyU2DCSharp_UICamera_ObjectDelegate1722530114.h"
#include "AssemblyU2DCSharp_UICamera_KeyCodeDelegate1657654541.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry974746545.h"
#include "AssemblyU2DCSharp_UICamera_Touch560313529.h"
#include "AssemblyU2DCSharp_UICamera_GetTouchCountCallback2237370697.h"
#include "AssemblyU2DCSharp_UICamera_GetTouchCallback2038115210.h"
#include "AssemblyU2DCSharp_UIColorPicker954141641.h"
#include "AssemblyU2DCSharp_UIFont389944949.h"
#include "AssemblyU2DCSharp_UIInput860674234.h"
#include "AssemblyU2DCSharp_UIInput_InputType1695544313.h"
#include "AssemblyU2DCSharp_UIInput_KeyboardMode2358216949.h"
#include "AssemblyU2DCSharp_UIInput_Validation3865409168.h"
#include "AssemblyU2DCSharp_UIInput_KeyboardType865779294.h"
#include "AssemblyU2DCSharp_UIInput_OnReturnKey1716013855.h"
#include "AssemblyU2DCSharp_UIInput_OnValidate2431313412.h"
#include "AssemblyU2DCSharp_UIInputOnGUI4090193630.h"
#include "AssemblyU2DCSharp_UILabel1795115428.h"
#include "AssemblyU2DCSharp_UILabel_Effect541741196.h"
#include "AssemblyU2DCSharp_UILabel_Overflow2317266067.h"
#include "AssemblyU2DCSharp_UILabel_Crispness4281041871.h"
#include "AssemblyU2DCSharp_UILocalize3754440953.h"
#include "AssemblyU2DCSharp_UIOrthoCamera1890393437.h"
#include "AssemblyU2DCSharp_UIPanel1795085332.h"
#include "AssemblyU2DCSharp_UIPanel_RenderQueue1879567704.h"
#include "AssemblyU2DCSharp_UIPanel_OnGeometryUpdated754882645.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved4045505957.h"
#include "AssemblyU2DCSharp_UIRoot389944298.h"
#include "AssemblyU2DCSharp_UIRoot_Scaling2807791908.h"
#include "AssemblyU2DCSharp_UIRoot_Constraint134200928.h"
#include "AssemblyU2DCSharp_UISprite603616735.h"
#include "AssemblyU2DCSharp_UISpriteAnimation97731357.h"
#include "AssemblyU2DCSharp_UISpriteData2862501359.h"
#include "AssemblyU2DCSharp_UIStretch4076885229.h"
#include "AssemblyU2DCSharp_UIStretch_Style1693551353.h"
#include "AssemblyU2DCSharp_UITextList652111117.h"
#include "AssemblyU2DCSharp_UITextList_Style3198392425.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph2587095060.h"
#include "AssemblyU2DCSharp_UITexture2537039969.h"
#include "AssemblyU2DCSharp_UITooltip1873874935.h"
#include "AssemblyU2DCSharp_UIViewport1541362616.h"
#include "AssemblyU2DCSharp_DisplayPickerSprite2709899627.h"
#include "AssemblyU2DCSharp_DisplayPickerSprite_DisplayMode481136365.h"
#include "AssemblyU2DCSharp_DisplayPickerText3159543891.h"
#include "AssemblyU2DCSharp_PickerScrollButton2644611827.h"
#include "AssemblyU2DCSharp_PickerScrollButton_ScrollDirection49895970.h"
#include "AssemblyU2DCSharp_ScalePickerWidgetOnPress493153328.h"
#include "AssemblyU2DCSharp_TweenCenterWidgetScale549921132.h"
#include "AssemblyU2DCSharp_TweenCenterWidgetScale_U3CStopTwee23811805.h"
#include "AssemblyU2DCSharp_TweenCenterWidgetScaleAndAlpha2087610359.h"
#include "AssemblyU2DCSharp_DateTimeCompoundPicker558724190.h"
#include "AssemblyU2DCSharp_DateTimeCompoundPicker_U3CSetNew2903668326.h"
#include "AssemblyU2DCSharp_IPDatePicker1471736889.h"
#include "AssemblyU2DCSharp_IPDatePicker_Date2675184608.h"
#include "AssemblyU2DCSharp_IPNumberPicker1469221158.h"
#include "AssemblyU2DCSharp_IPSpritePicker2623787062.h"
#include "AssemblyU2DCSharp_IPTextPicker3074051364.h"
#include "AssemblyU2DCSharp_IPTexturePicker4004014186.h"
#include "AssemblyU2DCSharp_AutoScrollOnClick3306800435.h"
#include "AssemblyU2DCSharp_CyclerClamper2048159144.h"
#include "AssemblyU2DCSharp_DragPickerSprite1100164761.h"
#include "AssemblyU2DCSharp_DragPickerSprite_U3CDelayedSprit2423777032.h"
#include "AssemblyU2DCSharp_DragPickerSprite_U3CDelayedSprite857693091.h"
#include "AssemblyU2DCSharp_IPCycler1336138445.h"
#include "AssemblyU2DCSharp_IPCycler_Direction617647995.h"
#include "AssemblyU2DCSharp_IPCycler_CyclerIndexChangeHandler463772902.h"
#include "AssemblyU2DCSharp_IPCycler_CyclerStoppedHandler3894542237.h"
#include "AssemblyU2DCSharp_IPCycler_SelectionStartedHandler4151988355.h"
#include "AssemblyU2DCSharp_IPCycler_CenterOnChildStartedHandle5714103.h"
#include "AssemblyU2DCSharp_IPCycler_U3CRecenterOnThresholdU2709464871.h"
#include "AssemblyU2DCSharp_IPForwardPickerEvents3125981953.h"
#include "AssemblyU2DCSharp_IPForwardPickerEvents_GameObject1917906877.h"
#include "AssemblyU2DCSharp_PickerPopper344149016.h"
#include "AssemblyU2DCSharp_PickerPopper_U3COpenPickerU3Ec__2620335406.h"
#include "AssemblyU2DCSharp_PickerPopper_U3CClosePickerU3Ec_1264813121.h"
#include "AssemblyU2DCSharp_PickerPopper_U3CBlinkWidgetU3Ec__705118254.h"
#include "AssemblyU2DCSharp_PickerPopper_U3CLerpBackgroundCo2566777843.h"
#include "AssemblyU2DCSharp_ReceiveSpriteDrop3569287313.h"
#include "AssemblyU2DCSharp_TweenPanelClipRange596408274.h"
#include "AssemblyU2DCSharp_TweenPanelClipSoftness2480894570.h"
#include "AssemblyU2DCSharp_DateTimeLocalization1520012308.h"
#include "AssemblyU2DCSharp_DateTimeLocalization_Language3679215353.h"
#include "AssemblyU2DCSharp_IPDragScrollView145219383.h"
#include "AssemblyU2DCSharp_IPPickerBase4159478266.h"
#include "AssemblyU2DCSharp_IPPickerBase_PickerValueUpdatedH2401088127.h"
#include "AssemblyU2DCSharp_IPPickerLabelBase2669006230.h"
#include "AssemblyU2DCSharp_IPPickerSpriteBase2462226553.h"
#include "AssemblyU2DCSharp_IPTexturePickerBase1323538019.h"
#include "AssemblyU2DCSharp_IPTools4234691708.h"
#include "AssemblyU2DCSharp_IPUserInteraction4192479194.h"
#include "AssemblyU2DCSharp_IPUserInteraction_OnPickerClicke2482807019.h"
#include "AssemblyU2DCSharp_IPUserInteraction_OnDragExit3593337772.h"
#include "AssemblyU2DCSharp_IPUserInteraction_U3CDelayedDrag2879712700.h"
#include "AssemblyU2DCSharp_ScrollWheelEvents2022027795.h"
#include "AssemblyU2DCSharp_ScrollWheelEvents_ScrollStartSto1032307121.h"
#include "AssemblyU2DCSharp_CacheTiles3066957529.h"
#include "AssemblyU2DCSharp_ApplicationManager2110631419.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { sizeof (VoidDelegate_t3315330999), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (BoolDelegate_t777098953), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (FloatDelegate_t3246177583), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { sizeof (VectorDelegate_t1823833398), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { sizeof (ObjectDelegate_t1722530114), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { sizeof (KeyCodeDelegate_t1657654541), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { sizeof (DepthEntry_t974746545)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5206[4] = 
{
	DepthEntry_t974746545::get_offset_of_depth_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t974746545::get_offset_of_hit_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t974746545::get_offset_of_point_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t974746545::get_offset_of_go_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (Touch_t560313529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5207[4] = 
{
	Touch_t560313529::get_offset_of_fingerId_0(),
	Touch_t560313529::get_offset_of_phase_1(),
	Touch_t560313529::get_offset_of_position_2(),
	Touch_t560313529::get_offset_of_tapCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (GetTouchCountCallback_t2237370697), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (GetTouchCallback_t2038115210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (UIColorPicker_t954141641), -1, sizeof(UIColorPicker_t954141641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5210[14] = 
{
	UIColorPicker_t954141641_StaticFields::get_offset_of_current_2(),
	UIColorPicker_t954141641::get_offset_of_value_3(),
	UIColorPicker_t954141641::get_offset_of_selectionWidget_4(),
	UIColorPicker_t954141641::get_offset_of_onChange_5(),
	UIColorPicker_t954141641::get_offset_of_mTrans_6(),
	UIColorPicker_t954141641::get_offset_of_mUITex_7(),
	UIColorPicker_t954141641::get_offset_of_mTex_8(),
	UIColorPicker_t954141641::get_offset_of_mCam_9(),
	UIColorPicker_t954141641::get_offset_of_mPos_10(),
	UIColorPicker_t954141641::get_offset_of_mWidth_11(),
	UIColorPicker_t954141641::get_offset_of_mHeight_12(),
	UIColorPicker_t954141641_StaticFields::get_offset_of_mRed_13(),
	UIColorPicker_t954141641_StaticFields::get_offset_of_mGreen_14(),
	UIColorPicker_t954141641_StaticFields::get_offset_of_mBlue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (UIFont_t389944949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5211[12] = 
{
	UIFont_t389944949::get_offset_of_mMat_2(),
	UIFont_t389944949::get_offset_of_mUVRect_3(),
	UIFont_t389944949::get_offset_of_mFont_4(),
	UIFont_t389944949::get_offset_of_mAtlas_5(),
	UIFont_t389944949::get_offset_of_mReplacement_6(),
	UIFont_t389944949::get_offset_of_mSymbols_7(),
	UIFont_t389944949::get_offset_of_mDynamicFont_8(),
	UIFont_t389944949::get_offset_of_mDynamicFontSize_9(),
	UIFont_t389944949::get_offset_of_mDynamicFontStyle_10(),
	UIFont_t389944949::get_offset_of_mSprite_11(),
	UIFont_t389944949::get_offset_of_mPMA_12(),
	UIFont_t389944949::get_offset_of_mPacked_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (UIInput_t860674234), -1, sizeof(UIInput_t860674234_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5212[42] = 
{
	UIInput_t860674234_StaticFields::get_offset_of_current_2(),
	UIInput_t860674234_StaticFields::get_offset_of_selection_3(),
	UIInput_t860674234::get_offset_of_label_4(),
	UIInput_t860674234::get_offset_of_keyboardMode_5(),
	UIInput_t860674234::get_offset_of_inputType_6(),
	UIInput_t860674234::get_offset_of_onReturnKey_7(),
	UIInput_t860674234::get_offset_of_keyboardType_8(),
	UIInput_t860674234::get_offset_of_hideInput_9(),
	UIInput_t860674234::get_offset_of_selectAllTextOnFocus_10(),
	UIInput_t860674234::get_offset_of_validation_11(),
	UIInput_t860674234::get_offset_of_characterLimit_12(),
	UIInput_t860674234::get_offset_of_savedAs_13(),
	UIInput_t860674234::get_offset_of_selectOnTab_14(),
	UIInput_t860674234::get_offset_of_activeTextColor_15(),
	UIInput_t860674234::get_offset_of_caretColor_16(),
	UIInput_t860674234::get_offset_of_selectionColor_17(),
	UIInput_t860674234::get_offset_of_onSubmit_18(),
	UIInput_t860674234::get_offset_of_onChange_19(),
	UIInput_t860674234::get_offset_of_onValidate_20(),
	UIInput_t860674234::get_offset_of_mValue_21(),
	UIInput_t860674234::get_offset_of_mDefaultText_22(),
	UIInput_t860674234::get_offset_of_mDefaultColor_23(),
	UIInput_t860674234::get_offset_of_mPosition_24(),
	UIInput_t860674234::get_offset_of_mDoInit_25(),
	UIInput_t860674234::get_offset_of_mPivot_26(),
	UIInput_t860674234::get_offset_of_mLoadSavedValue_27(),
	UIInput_t860674234_StaticFields::get_offset_of_mDrawStart_28(),
	UIInput_t860674234_StaticFields::get_offset_of_mLastIME_29(),
	UIInput_t860674234_StaticFields::get_offset_of_mKeyboard_30(),
	UIInput_t860674234_StaticFields::get_offset_of_mWaitForKeyboard_31(),
	UIInput_t860674234::get_offset_of_mSelectionStart_32(),
	UIInput_t860674234::get_offset_of_mSelectionEnd_33(),
	UIInput_t860674234::get_offset_of_mHighlight_34(),
	UIInput_t860674234::get_offset_of_mCaret_35(),
	UIInput_t860674234::get_offset_of_mBlankTex_36(),
	UIInput_t860674234::get_offset_of_mNextBlink_37(),
	UIInput_t860674234::get_offset_of_mLastAlpha_38(),
	UIInput_t860674234::get_offset_of_mCached_39(),
	UIInput_t860674234::get_offset_of_mSelectMe_40(),
	UIInput_t860674234::get_offset_of_mSelectTime_41(),
	UIInput_t860674234::get_offset_of_mCam_42(),
	UIInput_t860674234_StaticFields::get_offset_of_mIgnoreKey_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (InputType_t1695544313)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5213[4] = 
{
	InputType_t1695544313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (KeyboardMode_t2358216949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5214[3] = 
{
	KeyboardMode_t2358216949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (Validation_t3865409168)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5215[8] = 
{
	Validation_t3865409168::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (KeyboardType_t865779294)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5216[9] = 
{
	KeyboardType_t865779294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (OnReturnKey_t1716013855)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5217[4] = 
{
	OnReturnKey_t1716013855::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (OnValidate_t2431313412), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (UIInputOnGUI_t4090193630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (UILabel_t1795115428), -1, sizeof(UILabel_t1795115428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5220[44] = 
{
	UILabel_t1795115428::get_offset_of_keepCrispWhenShrunk_52(),
	UILabel_t1795115428::get_offset_of_mTrueTypeFont_53(),
	UILabel_t1795115428::get_offset_of_mFont_54(),
	UILabel_t1795115428::get_offset_of_mText_55(),
	UILabel_t1795115428::get_offset_of_mFontSize_56(),
	UILabel_t1795115428::get_offset_of_mFontStyle_57(),
	UILabel_t1795115428::get_offset_of_mAlignment_58(),
	UILabel_t1795115428::get_offset_of_mEncoding_59(),
	UILabel_t1795115428::get_offset_of_mMaxLineCount_60(),
	UILabel_t1795115428::get_offset_of_mEffectStyle_61(),
	UILabel_t1795115428::get_offset_of_mEffectColor_62(),
	UILabel_t1795115428::get_offset_of_mSymbols_63(),
	UILabel_t1795115428::get_offset_of_mEffectDistance_64(),
	UILabel_t1795115428::get_offset_of_mOverflow_65(),
	UILabel_t1795115428::get_offset_of_mMaterial_66(),
	UILabel_t1795115428::get_offset_of_mApplyGradient_67(),
	UILabel_t1795115428::get_offset_of_mGradientTop_68(),
	UILabel_t1795115428::get_offset_of_mGradientBottom_69(),
	UILabel_t1795115428::get_offset_of_mSpacingX_70(),
	UILabel_t1795115428::get_offset_of_mSpacingY_71(),
	UILabel_t1795115428::get_offset_of_mUseFloatSpacing_72(),
	UILabel_t1795115428::get_offset_of_mFloatSpacingX_73(),
	UILabel_t1795115428::get_offset_of_mFloatSpacingY_74(),
	UILabel_t1795115428::get_offset_of_mShrinkToFit_75(),
	UILabel_t1795115428::get_offset_of_mMaxLineWidth_76(),
	UILabel_t1795115428::get_offset_of_mMaxLineHeight_77(),
	UILabel_t1795115428::get_offset_of_mLineWidth_78(),
	UILabel_t1795115428::get_offset_of_mMultiline_79(),
	UILabel_t1795115428::get_offset_of_mActiveTTF_80(),
	UILabel_t1795115428::get_offset_of_mDensity_81(),
	UILabel_t1795115428::get_offset_of_mShouldBeProcessed_82(),
	UILabel_t1795115428::get_offset_of_mProcessedText_83(),
	UILabel_t1795115428::get_offset_of_mPremultiply_84(),
	UILabel_t1795115428::get_offset_of_mCalculatedSize_85(),
	UILabel_t1795115428::get_offset_of_mScale_86(),
	UILabel_t1795115428::get_offset_of_mFinalFontSize_87(),
	UILabel_t1795115428::get_offset_of_mLastWidth_88(),
	UILabel_t1795115428::get_offset_of_mLastHeight_89(),
	UILabel_t1795115428_StaticFields::get_offset_of_mList_90(),
	UILabel_t1795115428_StaticFields::get_offset_of_mFontUsage_91(),
	UILabel_t1795115428_StaticFields::get_offset_of_mTexRebuildAdded_92(),
	UILabel_t1795115428_StaticFields::get_offset_of_mTempVerts_93(),
	UILabel_t1795115428_StaticFields::get_offset_of_mTempIndices_94(),
	UILabel_t1795115428_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (Effect_t541741196)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5221[5] = 
{
	Effect_t541741196::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (Overflow_t2317266067)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5222[5] = 
{
	Overflow_t2317266067::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (Crispness_t4281041871)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5223[4] = 
{
	Crispness_t4281041871::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (UILocalize_t3754440953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5224[2] = 
{
	UILocalize_t3754440953::get_offset_of_key_2(),
	UILocalize_t3754440953::get_offset_of_mStarted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (UIOrthoCamera_t1890393437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5225[2] = 
{
	UIOrthoCamera_t1890393437::get_offset_of_mCam_2(),
	UIOrthoCamera_t1890393437::get_offset_of_mTrans_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (UIPanel_t1795085332), -1, sizeof(UIPanel_t1795085332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5226[43] = 
{
	UIPanel_t1795085332_StaticFields::get_offset_of_list_22(),
	UIPanel_t1795085332::get_offset_of_onGeometryUpdated_23(),
	UIPanel_t1795085332::get_offset_of_showInPanelTool_24(),
	UIPanel_t1795085332::get_offset_of_generateNormals_25(),
	UIPanel_t1795085332::get_offset_of_widgetsAreStatic_26(),
	UIPanel_t1795085332::get_offset_of_cullWhileDragging_27(),
	UIPanel_t1795085332::get_offset_of_alwaysOnScreen_28(),
	UIPanel_t1795085332::get_offset_of_anchorOffset_29(),
	UIPanel_t1795085332::get_offset_of_softBorderPadding_30(),
	UIPanel_t1795085332::get_offset_of_renderQueue_31(),
	UIPanel_t1795085332::get_offset_of_startingRenderQueue_32(),
	UIPanel_t1795085332::get_offset_of_widgets_33(),
	UIPanel_t1795085332::get_offset_of_drawCalls_34(),
	UIPanel_t1795085332::get_offset_of_worldToLocal_35(),
	UIPanel_t1795085332::get_offset_of_drawCallClipRange_36(),
	UIPanel_t1795085332::get_offset_of_onClipMove_37(),
	UIPanel_t1795085332::get_offset_of_mClipTexture_38(),
	UIPanel_t1795085332::get_offset_of_mAlpha_39(),
	UIPanel_t1795085332::get_offset_of_mClipping_40(),
	UIPanel_t1795085332::get_offset_of_mClipRange_41(),
	UIPanel_t1795085332::get_offset_of_mClipSoftness_42(),
	UIPanel_t1795085332::get_offset_of_mDepth_43(),
	UIPanel_t1795085332::get_offset_of_mSortingOrder_44(),
	UIPanel_t1795085332::get_offset_of_mRebuild_45(),
	UIPanel_t1795085332::get_offset_of_mResized_46(),
	UIPanel_t1795085332::get_offset_of_mClipOffset_47(),
	UIPanel_t1795085332::get_offset_of_mMatrixFrame_48(),
	UIPanel_t1795085332::get_offset_of_mAlphaFrameID_49(),
	UIPanel_t1795085332::get_offset_of_mLayer_50(),
	UIPanel_t1795085332_StaticFields::get_offset_of_mTemp_51(),
	UIPanel_t1795085332::get_offset_of_mMin_52(),
	UIPanel_t1795085332::get_offset_of_mMax_53(),
	UIPanel_t1795085332::get_offset_of_mHalfPixelOffset_54(),
	UIPanel_t1795085332::get_offset_of_mSortWidgets_55(),
	UIPanel_t1795085332::get_offset_of_mUpdateScroll_56(),
	UIPanel_t1795085332::get_offset_of_mParentPanel_57(),
	UIPanel_t1795085332_StaticFields::get_offset_of_mCorners_58(),
	UIPanel_t1795085332_StaticFields::get_offset_of_mUpdateFrame_59(),
	UIPanel_t1795085332::get_offset_of_mOnRender_60(),
	UIPanel_t1795085332::get_offset_of_mForced_61(),
	UIPanel_t1795085332_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_62(),
	UIPanel_t1795085332_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_63(),
	UIPanel_t1795085332_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_64(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (RenderQueue_t1879567704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5227[4] = 
{
	RenderQueue_t1879567704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (OnGeometryUpdated_t754882645), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (OnClippingMoved_t4045505957), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (UIRoot_t389944298), -1, sizeof(UIRoot_t389944298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5230[11] = 
{
	UIRoot_t389944298_StaticFields::get_offset_of_list_2(),
	UIRoot_t389944298::get_offset_of_scalingStyle_3(),
	UIRoot_t389944298::get_offset_of_manualWidth_4(),
	UIRoot_t389944298::get_offset_of_manualHeight_5(),
	UIRoot_t389944298::get_offset_of_minimumHeight_6(),
	UIRoot_t389944298::get_offset_of_maximumHeight_7(),
	UIRoot_t389944298::get_offset_of_fitWidth_8(),
	UIRoot_t389944298::get_offset_of_fitHeight_9(),
	UIRoot_t389944298::get_offset_of_adjustByDPI_10(),
	UIRoot_t389944298::get_offset_of_shrinkPortraitUI_11(),
	UIRoot_t389944298::get_offset_of_mTrans_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (Scaling_t2807791908)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5231[4] = 
{
	Scaling_t2807791908::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (Constraint_t134200928)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5232[5] = 
{
	Constraint_t134200928::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (UISprite_t603616735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5233[5] = 
{
	UISprite_t603616735::get_offset_of_mAtlas_66(),
	UISprite_t603616735::get_offset_of_mSpriteName_67(),
	UISprite_t603616735::get_offset_of_mFillCenter_68(),
	UISprite_t603616735::get_offset_of_mSprite_69(),
	UISprite_t603616735::get_offset_of_mSpriteSet_70(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (UISpriteAnimation_t97731357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5234[9] = 
{
	UISpriteAnimation_t97731357::get_offset_of_mFPS_2(),
	UISpriteAnimation_t97731357::get_offset_of_mPrefix_3(),
	UISpriteAnimation_t97731357::get_offset_of_mLoop_4(),
	UISpriteAnimation_t97731357::get_offset_of_mSnap_5(),
	UISpriteAnimation_t97731357::get_offset_of_mSprite_6(),
	UISpriteAnimation_t97731357::get_offset_of_mDelta_7(),
	UISpriteAnimation_t97731357::get_offset_of_mIndex_8(),
	UISpriteAnimation_t97731357::get_offset_of_mActive_9(),
	UISpriteAnimation_t97731357::get_offset_of_mSpriteNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (UISpriteData_t2862501359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5235[13] = 
{
	UISpriteData_t2862501359::get_offset_of_name_0(),
	UISpriteData_t2862501359::get_offset_of_x_1(),
	UISpriteData_t2862501359::get_offset_of_y_2(),
	UISpriteData_t2862501359::get_offset_of_width_3(),
	UISpriteData_t2862501359::get_offset_of_height_4(),
	UISpriteData_t2862501359::get_offset_of_borderLeft_5(),
	UISpriteData_t2862501359::get_offset_of_borderRight_6(),
	UISpriteData_t2862501359::get_offset_of_borderTop_7(),
	UISpriteData_t2862501359::get_offset_of_borderBottom_8(),
	UISpriteData_t2862501359::get_offset_of_paddingLeft_9(),
	UISpriteData_t2862501359::get_offset_of_paddingRight_10(),
	UISpriteData_t2862501359::get_offset_of_paddingTop_11(),
	UISpriteData_t2862501359::get_offset_of_paddingBottom_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (UIStretch_t4076885229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5236[16] = 
{
	UIStretch_t4076885229::get_offset_of_uiCamera_2(),
	UIStretch_t4076885229::get_offset_of_container_3(),
	UIStretch_t4076885229::get_offset_of_style_4(),
	UIStretch_t4076885229::get_offset_of_runOnlyOnce_5(),
	UIStretch_t4076885229::get_offset_of_relativeSize_6(),
	UIStretch_t4076885229::get_offset_of_initialSize_7(),
	UIStretch_t4076885229::get_offset_of_borderPadding_8(),
	UIStretch_t4076885229::get_offset_of_widgetContainer_9(),
	UIStretch_t4076885229::get_offset_of_mTrans_10(),
	UIStretch_t4076885229::get_offset_of_mWidget_11(),
	UIStretch_t4076885229::get_offset_of_mSprite_12(),
	UIStretch_t4076885229::get_offset_of_mPanel_13(),
	UIStretch_t4076885229::get_offset_of_mRoot_14(),
	UIStretch_t4076885229::get_offset_of_mAnim_15(),
	UIStretch_t4076885229::get_offset_of_mRect_16(),
	UIStretch_t4076885229::get_offset_of_mStarted_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (Style_t1693551353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5237[8] = 
{
	Style_t1693551353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (UITextList_t652111117), -1, sizeof(UITextList_t652111117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5238[11] = 
{
	UITextList_t652111117::get_offset_of_textLabel_2(),
	UITextList_t652111117::get_offset_of_scrollBar_3(),
	UITextList_t652111117::get_offset_of_style_4(),
	UITextList_t652111117::get_offset_of_paragraphHistory_5(),
	UITextList_t652111117::get_offset_of_mSeparator_6(),
	UITextList_t652111117::get_offset_of_mScroll_7(),
	UITextList_t652111117::get_offset_of_mTotalLines_8(),
	UITextList_t652111117::get_offset_of_mLastWidth_9(),
	UITextList_t652111117::get_offset_of_mLastHeight_10(),
	UITextList_t652111117::get_offset_of_mParagraphs_11(),
	UITextList_t652111117_StaticFields::get_offset_of_mHistory_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (Style_t3198392425)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5239[3] = 
{
	Style_t3198392425::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (Paragraph_t2587095060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5240[2] = 
{
	Paragraph_t2587095060::get_offset_of_text_0(),
	Paragraph_t2587095060::get_offset_of_lines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (UITexture_t2537039969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5241[7] = 
{
	UITexture_t2537039969::get_offset_of_mRect_66(),
	UITexture_t2537039969::get_offset_of_mTexture_67(),
	UITexture_t2537039969::get_offset_of_mMat_68(),
	UITexture_t2537039969::get_offset_of_mShader_69(),
	UITexture_t2537039969::get_offset_of_mBorder_70(),
	UITexture_t2537039969::get_offset_of_mFixedAspect_71(),
	UITexture_t2537039969::get_offset_of_mPMA_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (UITooltip_t1873874935), -1, sizeof(UITooltip_t1873874935_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5242[13] = 
{
	UITooltip_t1873874935_StaticFields::get_offset_of_mInstance_2(),
	UITooltip_t1873874935::get_offset_of_uiCamera_3(),
	UITooltip_t1873874935::get_offset_of_text_4(),
	UITooltip_t1873874935::get_offset_of_background_5(),
	UITooltip_t1873874935::get_offset_of_appearSpeed_6(),
	UITooltip_t1873874935::get_offset_of_scalingTransitions_7(),
	UITooltip_t1873874935::get_offset_of_mTooltip_8(),
	UITooltip_t1873874935::get_offset_of_mTrans_9(),
	UITooltip_t1873874935::get_offset_of_mTarget_10(),
	UITooltip_t1873874935::get_offset_of_mCurrent_11(),
	UITooltip_t1873874935::get_offset_of_mPos_12(),
	UITooltip_t1873874935::get_offset_of_mSize_13(),
	UITooltip_t1873874935::get_offset_of_mWidgets_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (UIViewport_t1541362616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5243[5] = 
{
	UIViewport_t1541362616::get_offset_of_sourceCamera_2(),
	UIViewport_t1541362616::get_offset_of_topLeft_3(),
	UIViewport_t1541362616::get_offset_of_bottomRight_4(),
	UIViewport_t1541362616::get_offset_of_fullSize_5(),
	UIViewport_t1541362616::get_offset_of_mCam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (DisplayPickerSprite_t2709899627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5244[4] = 
{
	DisplayPickerSprite_t2709899627::get_offset_of_picker_2(),
	DisplayPickerSprite_t2709899627::get_offset_of_displayMode_3(),
	DisplayPickerSprite_t2709899627::get_offset_of_normalizedMax_4(),
	DisplayPickerSprite_t2709899627::get_offset_of__sprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (DisplayMode_t481136365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5245[4] = 
{
	DisplayMode_t481136365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (DisplayPickerText_t3159543891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5246[2] = 
{
	DisplayPickerText_t3159543891::get_offset_of_picker_2(),
	DisplayPickerText_t3159543891::get_offset_of__label_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (PickerScrollButton_t2644611827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5247[4] = 
{
	PickerScrollButton_t2644611827::get_offset_of_targetCycler_2(),
	PickerScrollButton_t2644611827::get_offset_of_scrollSpeed_3(),
	PickerScrollButton_t2644611827::get_offset_of_scrollDirection_4(),
	PickerScrollButton_t2644611827::get_offset_of__pressed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (ScrollDirection_t49895970)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5248[3] = 
{
	ScrollDirection_t49895970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (ScalePickerWidgetOnPress_t493153328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5249[5] = 
{
	ScalePickerWidgetOnPress_t493153328::get_offset_of_picker_2(),
	ScalePickerWidgetOnPress_t493153328::get_offset_of_duration_3(),
	ScalePickerWidgetOnPress_t493153328::get_offset_of_scaleFactor_4(),
	ScalePickerWidgetOnPress_t493153328::get_offset_of__widget_5(),
	ScalePickerWidgetOnPress_t493153328::get_offset_of__tweenScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (TweenCenterWidgetScale_t549921132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5250[6] = 
{
	TweenCenterWidgetScale_t549921132::get_offset_of_scaleFactor_2(),
	TweenCenterWidgetScale_t549921132::get_offset_of_duration_3(),
	TweenCenterWidgetScale_t549921132::get_offset_of_tweenMethod_4(),
	TweenCenterWidgetScale_t549921132::get_offset_of__picker_5(),
	TweenCenterWidgetScale_t549921132::get_offset_of__tweenScale_6(),
	TweenCenterWidgetScale_t549921132::get_offset_of__centerWidget_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5251[4] = 
{
	U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805::get_offset_of_U24this_0(),
	U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805::get_offset_of_U24current_1(),
	U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805::get_offset_of_U24disposing_2(),
	U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (TweenCenterWidgetScaleAndAlpha_t2087610359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5252[6] = 
{
	TweenCenterWidgetScaleAndAlpha_t2087610359::get_offset_of_picker_2(),
	TweenCenterWidgetScaleAndAlpha_t2087610359::get_offset_of_scaleFactor_3(),
	TweenCenterWidgetScaleAndAlpha_t2087610359::get_offset_of_duration_4(),
	TweenCenterWidgetScaleAndAlpha_t2087610359::get_offset_of_scaleTween_5(),
	TweenCenterWidgetScaleAndAlpha_t2087610359::get_offset_of_alphaTween_6(),
	TweenCenterWidgetScaleAndAlpha_t2087610359::get_offset_of_currentWidget_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (DateTimeCompoundPicker_t558724190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5253[8] = 
{
	DateTimeCompoundPicker_t558724190::get_offset_of_datePicker_2(),
	DateTimeCompoundPicker_t558724190::get_offset_of_hourPicker_3(),
	DateTimeCompoundPicker_t558724190::get_offset_of_minutesPicker_4(),
	DateTimeCompoundPicker_t558724190::get_offset_of_initAtNow_5(),
	DateTimeCompoundPicker_t558724190::get_offset_of_initDate_6(),
	DateTimeCompoundPicker_t558724190::get_offset_of_initHour_7(),
	DateTimeCompoundPicker_t558724190::get_offset_of_initMinute_8(),
	DateTimeCompoundPicker_t558724190::get_offset_of__isInitialized_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (U3CSetNewLanguageU3Ec__Iterator0_t2903668326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5254[5] = 
{
	U3CSetNewLanguageU3Ec__Iterator0_t2903668326::get_offset_of_language_0(),
	U3CSetNewLanguageU3Ec__Iterator0_t2903668326::get_offset_of_U24this_1(),
	U3CSetNewLanguageU3Ec__Iterator0_t2903668326::get_offset_of_U24current_2(),
	U3CSetNewLanguageU3Ec__Iterator0_t2903668326::get_offset_of_U24disposing_3(),
	U3CSetNewLanguageU3Ec__Iterator0_t2903668326::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (IPDatePicker_t1471736889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5255[7] = 
{
	IPDatePicker_t1471736889::get_offset_of_language_15(),
	IPDatePicker_t1471736889::get_offset_of_pickerMinDate_16(),
	IPDatePicker_t1471736889::get_offset_of_pickerMaxDate_17(),
	IPDatePicker_t1471736889::get_offset_of_initDate_18(),
	IPDatePicker_t1471736889::get_offset_of_dateFormat_19(),
	IPDatePicker_t1471736889::get_offset_of_U3CCurrentDateU3Ek__BackingField_20(),
	IPDatePicker_t1471736889::get_offset_of__minDate_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (Date_t2675184608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5256[3] = 
{
	Date_t2675184608::get_offset_of_day_0(),
	Date_t2675184608::get_offset_of_month_1(),
	Date_t2675184608::get_offset_of_year_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (IPNumberPicker_t1469221158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5257[6] = 
{
	IPNumberPicker_t1469221158::get_offset_of_min_15(),
	IPNumberPicker_t1469221158::get_offset_of_max_16(),
	IPNumberPicker_t1469221158::get_offset_of_step_17(),
	IPNumberPicker_t1469221158::get_offset_of_initValue_18(),
	IPNumberPicker_t1469221158::get_offset_of_toStringFormat_19(),
	IPNumberPicker_t1469221158::get_offset_of_U3CCurrentValueU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (IPSpritePicker_t2623787062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5258[5] = 
{
	IPSpritePicker_t2623787062::get_offset_of_spriteNames_15(),
	IPSpritePicker_t2623787062::get_offset_of_normalizeSprites_16(),
	IPSpritePicker_t2623787062::get_offset_of_normalizedMax_17(),
	IPSpritePicker_t2623787062::get_offset_of_initIndex_18(),
	IPSpritePicker_t2623787062::get_offset_of_U3CCurrentSpriteNameU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (IPTextPicker_t3074051364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5259[3] = 
{
	IPTextPicker_t3074051364::get_offset_of_labelsText_15(),
	IPTextPicker_t3074051364::get_offset_of_initIndex_16(),
	IPTextPicker_t3074051364::get_offset_of_U3CCurrentLabelTextU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (IPTexturePicker_t4004014186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5260[5] = 
{
	IPTexturePicker_t4004014186::get_offset_of_textures_15(),
	IPTexturePicker_t4004014186::get_offset_of_normalizeTextures_16(),
	IPTexturePicker_t4004014186::get_offset_of_normalizedMax_17(),
	IPTexturePicker_t4004014186::get_offset_of_initIndex_18(),
	IPTexturePicker_t4004014186::get_offset_of_U3CCurrentTextureU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (AutoScrollOnClick_t3306800435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5261[1] = 
{
	AutoScrollOnClick_t3306800435::get_offset_of_cycler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (CyclerClamper_t2048159144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5262[2] = 
{
	CyclerClamper_t2048159144::get_offset_of_observedPicker_2(),
	CyclerClamper_t2048159144::get_offset_of__cycler_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (DragPickerSprite_t1100164761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5263[7] = 
{
	DragPickerSprite_t1100164761::get_offset_of_draggedSprite_2(),
	DragPickerSprite_t1100164761::get_offset_of_picker_3(),
	DragPickerSprite_t1100164761::get_offset_of_tweenAlpha_4(),
	DragPickerSprite_t1100164761::get_offset_of_dragSelectedItemOnly_5(),
	DragPickerSprite_t1100164761::get_offset_of_delayAfterExit_6(),
	DragPickerSprite_t1100164761::get_offset_of__dragObject_7(),
	DragPickerSprite_t1100164761::get_offset_of__userInteraction_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5264[5] = 
{
	U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032::get_offset_of_delay_0(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032::get_offset_of_U24this_1(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032::get_offset_of_U24current_2(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032::get_offset_of_U24disposing_3(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5265[9] = 
{
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_delay_0(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_touchLocalPosInCycler_1(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U3CexitDistanceFromCenterU3E__0_2(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U3CdeltaIndexU3E__1_3(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U3CspriteIndexU3E__2_4(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U24this_5(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U24current_6(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U24disposing_7(),
	U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (IPCycler_t1336138445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5266[35] = 
{
	IPCycler_t1336138445::get_offset_of_direction_2(),
	IPCycler_t1336138445::get_offset_of_spacing_3(),
	IPCycler_t1336138445::get_offset_of_dragScale_4(),
	IPCycler_t1336138445::get_offset_of_recenterSpeedThreshold_5(),
	IPCycler_t1336138445::get_offset_of_recenterSpringStrength_6(),
	IPCycler_t1336138445::get_offset_of_restrictDragToPicker_7(),
	IPCycler_t1336138445::get_offset_of_U3CIsMovingU3Ek__BackingField_8(),
	IPCycler_t1336138445::get_offset_of_U3CCenterWidgetIndexU3Ek__BackingField_9(),
	IPCycler_t1336138445::get_offset_of_U3CRecenterTargetWidgetIndexU3Ek__BackingField_10(),
	IPCycler_t1336138445::get_offset_of_U3CClampIncrementU3Ek__BackingField_11(),
	IPCycler_t1336138445::get_offset_of_U3CClampDecrementU3Ek__BackingField_12(),
	IPCycler_t1336138445::get_offset_of_onCyclerIndexChange_13(),
	IPCycler_t1336138445::get_offset_of_onCyclerStopped_14(),
	IPCycler_t1336138445::get_offset_of_onCyclerSelectionStarted_15(),
	IPCycler_t1336138445::get_offset_of_onCenterOnChildStarted_16(),
	IPCycler_t1336138445::get_offset_of__cycledTransforms_17(),
	IPCycler_t1336138445::get_offset_of__draggablePanel_18(),
	IPCycler_t1336138445::get_offset_of_dragPanelContents_19(),
	IPCycler_t1336138445::get_offset_of_userInteraction_20(),
	IPCycler_t1336138445::get_offset_of__panel_21(),
	IPCycler_t1336138445::get_offset_of__uiCenterOnChild_22(),
	IPCycler_t1336138445::get_offset_of__pickerCollider_23(),
	IPCycler_t1336138445::get_offset_of__initFrame_24(),
	IPCycler_t1336138445::get_offset_of__lastResetFrame_25(),
	IPCycler_t1336138445::get_offset_of__decrementThreshold_26(),
	IPCycler_t1336138445::get_offset_of__incrementThreshold_27(),
	IPCycler_t1336138445::get_offset_of__transformJumpDelta_28(),
	IPCycler_t1336138445::get_offset_of__panelSignificantPosVector_29(),
	IPCycler_t1336138445::get_offset_of__panelPrevPos_30(),
	IPCycler_t1336138445::get_offset_of__deltaPos_31(),
	IPCycler_t1336138445::get_offset_of__resetPosition_32(),
	IPCycler_t1336138445::get_offset_of__isInitialized_33(),
	IPCycler_t1336138445::get_offset_of__hasMovedSincePress_34(),
	IPCycler_t1336138445::get_offset_of__isHorizontal_35(),
	IPCycler_t1336138445::get_offset_of__trueSpacing_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (Direction_t617647995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5267[3] = 
{
	Direction_t617647995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (CyclerIndexChangeHandler_t463772902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (CyclerStoppedHandler_t3894542237), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (SelectionStartedHandler_t4151988355), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (CenterOnChildStartedHandler_t5714103), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (U3CRecenterOnThresholdU3Ec__Iterator0_t2709464871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5272[4] = 
{
	U3CRecenterOnThresholdU3Ec__Iterator0_t2709464871::get_offset_of_U24this_0(),
	U3CRecenterOnThresholdU3Ec__Iterator0_t2709464871::get_offset_of_U24current_1(),
	U3CRecenterOnThresholdU3Ec__Iterator0_t2709464871::get_offset_of_U24disposing_2(),
	U3CRecenterOnThresholdU3Ec__Iterator0_t2709464871::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (IPForwardPickerEvents_t3125981953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5273[12] = 
{
	IPForwardPickerEvents_t3125981953::get_offset_of_observedPicker_2(),
	IPForwardPickerEvents_t3125981953::get_offset_of_notifyOnSelectionChange_3(),
	IPForwardPickerEvents_t3125981953::get_offset_of_onSelectionChangeNotification_4(),
	IPForwardPickerEvents_t3125981953::get_offset_of_notifyOnSelectionStarted_5(),
	IPForwardPickerEvents_t3125981953::get_offset_of_onStartedNotification_6(),
	IPForwardPickerEvents_t3125981953::get_offset_of_notifyOnCenterOnChildStarted_7(),
	IPForwardPickerEvents_t3125981953::get_offset_of_onCenterOnChildNotification_8(),
	IPForwardPickerEvents_t3125981953::get_offset_of_notifyOnPickerStopped_9(),
	IPForwardPickerEvents_t3125981953::get_offset_of_onStoppedNotification_10(),
	IPForwardPickerEvents_t3125981953::get_offset_of_notifyOnDragExit_11(),
	IPForwardPickerEvents_t3125981953::get_offset_of_onExitNotification_12(),
	IPForwardPickerEvents_t3125981953::get_offset_of__cycler_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (GameObjectAndMessage_t1917906877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5274[3] = 
{
	GameObjectAndMessage_t1917906877::get_offset_of_gameObject_0(),
	GameObjectAndMessage_t1917906877::get_offset_of_message_1(),
	GameObjectAndMessage_t1917906877::get_offset_of_notifyInStart_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (PickerPopper_t344149016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5275[23] = 
{
	PickerPopper_t344149016::get_offset_of_pickerColliderObject_2(),
	PickerPopper_t344149016::get_offset_of_picker_3(),
	PickerPopper_t344149016::get_offset_of_pickerBackground_4(),
	PickerPopper_t344149016::get_offset_of_cyclerPanel_5(),
	PickerPopper_t344149016::get_offset_of_openCloseScaleTweenDuration_6(),
	PickerPopper_t344149016::get_offset_of_openCloseClippingTweenDuration_7(),
	PickerPopper_t344149016::get_offset_of_widgetForBlinkConfirmation_8(),
	PickerPopper_t344149016::get_offset_of_blinkColor_9(),
	PickerPopper_t344149016::get_offset_of_confirmBlinkDuration_10(),
	PickerPopper_t344149016::get_offset_of_notifyWhenCollapsing_11(),
	PickerPopper_t344149016::get_offset_of_message_12(),
	PickerPopper_t344149016::get_offset_of__dragContents_13(),
	PickerPopper_t344149016::get_offset_of__pickerInteraction_14(),
	PickerPopper_t344149016::get_offset_of__cycler_15(),
	PickerPopper_t344149016::get_offset_of__clipRangeTween_16(),
	PickerPopper_t344149016::get_offset_of__softnessTween_17(),
	PickerPopper_t344149016::get_offset_of__scaleTween_18(),
	PickerPopper_t344149016::get_offset_of__pickerCollider_19(),
	PickerPopper_t344149016::get_offset_of__thisCollider_20(),
	PickerPopper_t344149016::get_offset_of__waitBeforeClosing_21(),
	PickerPopper_t344149016::get_offset_of__clippingIsTweening_22(),
	PickerPopper_t344149016::get_offset_of__initBlinkColor_23(),
	PickerPopper_t344149016::get_offset_of__cyclerCachedPos_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (U3COpenPickerU3Ec__Iterator0_t2620335406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5276[4] = 
{
	U3COpenPickerU3Ec__Iterator0_t2620335406::get_offset_of_U24this_0(),
	U3COpenPickerU3Ec__Iterator0_t2620335406::get_offset_of_U24current_1(),
	U3COpenPickerU3Ec__Iterator0_t2620335406::get_offset_of_U24disposing_2(),
	U3COpenPickerU3Ec__Iterator0_t2620335406::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (U3CClosePickerU3Ec__Iterator1_t1264813121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5277[4] = 
{
	U3CClosePickerU3Ec__Iterator1_t1264813121::get_offset_of_U24this_0(),
	U3CClosePickerU3Ec__Iterator1_t1264813121::get_offset_of_U24current_1(),
	U3CClosePickerU3Ec__Iterator1_t1264813121::get_offset_of_U24disposing_2(),
	U3CClosePickerU3Ec__Iterator1_t1264813121::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (U3CBlinkWidgetU3Ec__Iterator2_t705118254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5278[6] = 
{
	U3CBlinkWidgetU3Ec__Iterator2_t705118254::get_offset_of_U3CiU3E__0_0(),
	U3CBlinkWidgetU3Ec__Iterator2_t705118254::get_offset_of_nbOfBlinks_1(),
	U3CBlinkWidgetU3Ec__Iterator2_t705118254::get_offset_of_U24this_2(),
	U3CBlinkWidgetU3Ec__Iterator2_t705118254::get_offset_of_U24current_3(),
	U3CBlinkWidgetU3Ec__Iterator2_t705118254::get_offset_of_U24disposing_4(),
	U3CBlinkWidgetU3Ec__Iterator2_t705118254::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5279[9] = 
{
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U3CinitValueU3E__0_0(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U3CiU3E__1_1(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_duration_2(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U3CrateU3E__2_3(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_targetColor_4(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U24this_5(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U24current_6(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U24disposing_7(),
	U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (ReceiveSpriteDrop_t3569287313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5280[2] = 
{
	ReceiveSpriteDrop_t3569287313::get_offset_of_target_2(),
	ReceiveSpriteDrop_t3569287313::get_offset_of_message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (TweenPanelClipRange_t596408274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5281[3] = 
{
	TweenPanelClipRange_t596408274::get_offset_of_from_20(),
	TweenPanelClipRange_t596408274::get_offset_of_to_21(),
	TweenPanelClipRange_t596408274::get_offset_of__panel_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (TweenPanelClipSoftness_t2480894570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5282[3] = 
{
	TweenPanelClipSoftness_t2480894570::get_offset_of_from_20(),
	TweenPanelClipSoftness_t2480894570::get_offset_of_to_21(),
	TweenPanelClipSoftness_t2480894570::get_offset_of__panel_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (DateTimeLocalization_t1520012308), -1, sizeof(DateTimeLocalization_t1520012308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5283[3] = 
{
	DateTimeLocalization_t1520012308_StaticFields::get_offset_of__currentLanguage_0(),
	DateTimeLocalization_t1520012308_StaticFields::get_offset_of__languageCodes_1(),
	DateTimeLocalization_t1520012308_StaticFields::get_offset_of_U3CCultureU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (Language_t3679215353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5284[8] = 
{
	Language_t3679215353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (IPDragScrollView_t145219383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (IPPickerBase_t4159478266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5286[11] = 
{
	IPPickerBase_t4159478266::get_offset_of_pickerName_2(),
	IPPickerBase_t4159478266::get_offset_of_initInAwake_3(),
	IPPickerBase_t4159478266::get_offset_of_widgetsDepth_4(),
	IPPickerBase_t4159478266::get_offset_of_widgetOffsetInPicker_5(),
	IPPickerBase_t4159478266::get_offset_of_widgetsPivot_6(),
	IPPickerBase_t4159478266::get_offset_of_widgetsColor_7(),
	IPPickerBase_t4159478266::get_offset_of_onPickerValueUpdated_8(),
	IPPickerBase_t4159478266::get_offset_of_cycler_9(),
	IPPickerBase_t4159478266::get_offset_of__nbOfWidgets_10(),
	IPPickerBase_t4159478266::get_offset_of__nbOfVirtualElements_11(),
	IPPickerBase_t4159478266::get_offset_of__selectedIndex_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (PickerValueUpdatedHandler_t2401088127), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { sizeof (IPPickerLabelBase_t2669006230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5288[2] = 
{
	IPPickerLabelBase_t2669006230::get_offset_of_font_13(),
	IPPickerLabelBase_t2669006230::get_offset_of_uiLabels_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (IPPickerSpriteBase_t2462226553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5289[2] = 
{
	IPPickerSpriteBase_t2462226553::get_offset_of_atlas_13(),
	IPPickerSpriteBase_t2462226553::get_offset_of_uiSprites_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (IPTexturePickerBase_t1323538019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5290[2] = 
{
	IPTexturePickerBase_t1323538019::get_offset_of_shader_13(),
	IPTexturePickerBase_t1323538019::get_offset_of_uiTextures_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (IPTools_t4234691708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (IPUserInteraction_t4192479194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5292[8] = 
{
	IPUserInteraction_t4192479194::get_offset_of_cycler_2(),
	IPUserInteraction_t4192479194::get_offset_of_restrictWithinPicker_3(),
	IPUserInteraction_t4192479194::get_offset_of_exitRecenterDelay_4(),
	IPUserInteraction_t4192479194::get_offset_of__isScrolling_5(),
	IPUserInteraction_t4192479194::get_offset_of__isPressed_6(),
	IPUserInteraction_t4192479194::get_offset_of__isDraggingOutsideOfPicker_7(),
	IPUserInteraction_t4192479194::get_offset_of_onPickerClicked_8(),
	IPUserInteraction_t4192479194::get_offset_of_onDragExit_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (OnPickerClicked_t2482807019), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (OnDragExit_t3593337772), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (U3CDelayedDragExitU3Ec__Iterator0_t2879712700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5295[4] = 
{
	U3CDelayedDragExitU3Ec__Iterator0_t2879712700::get_offset_of_U24this_0(),
	U3CDelayedDragExitU3Ec__Iterator0_t2879712700::get_offset_of_U24current_1(),
	U3CDelayedDragExitU3Ec__Iterator0_t2879712700::get_offset_of_U24disposing_2(),
	U3CDelayedDragExitU3Ec__Iterator0_t2879712700::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (ScrollWheelEvents_t2022027795), -1, sizeof(ScrollWheelEvents_t2022027795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5296[3] = 
{
	ScrollWheelEvents_t2022027795_StaticFields::get_offset_of__instance_2(),
	ScrollWheelEvents_t2022027795_StaticFields::get_offset_of_onScrollStartOrStop_3(),
	ScrollWheelEvents_t2022027795::get_offset_of__isScrolling_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (ScrollStartStopHandler_t1032307121), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (CacheTiles_t3066957529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (ApplicationManager_t2110631419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5299[12] = 
{
	ApplicationManager_t2110631419::get_offset_of_forcedLogout_3(),
	ApplicationManager_t2110631419::get_offset_of_firstGameStart_4(),
	ApplicationManager_t2110631419::get_offset_of_sceneLoadingFrom_5(),
	ApplicationManager_t2110631419::get_offset_of_chatStartScreen_6(),
	ApplicationManager_t2110631419::get_offset_of_chatStartThread_7(),
	ApplicationManager_t2110631419::get_offset_of_chatStartFriend_8(),
	ApplicationManager_t2110631419::get_offset_of_startAttackNav_9(),
	ApplicationManager_t2110631419::get_offset_of_startAttackHomeNav_10(),
	ApplicationManager_t2110631419::get_offset_of_activeMissile_11(),
	ApplicationManager_t2110631419::get_offset_of_activeTarget_12(),
	ApplicationManager_t2110631419::get_offset_of_firedMissile_13(),
	ApplicationManager_t2110631419::get_offset_of_firedAccuracy_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
