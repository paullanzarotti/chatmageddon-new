﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Uniject.IUtil
struct IUtil_t2188430191;
// Uniject.IStorage
struct IStorage_t1347868490;
// Uniject.IURLFetcher
struct IURLFetcher_t2002848911;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// System.IO.FileStream
struct FileStream_t1695958676;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`2<System.String,System.Int32>
struct Action_2_t4277199140;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t3968615785;
// System.Random
struct Random_t1044426839;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.DownloadManager
struct  DownloadManager_t32803451  : public Il2CppObject
{
public:
	// Uniject.IUtil Unibill.Impl.DownloadManager::util
	Il2CppObject * ___util_0;
	// Uniject.IStorage Unibill.Impl.DownloadManager::storage
	Il2CppObject * ___storage_1;
	// Uniject.IURLFetcher Unibill.Impl.DownloadManager::fetcher
	Il2CppObject * ___fetcher_2;
	// Uniject.ILogger Unibill.Impl.DownloadManager::logger
	Il2CppObject * ___logger_3;
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) Unibill.Impl.DownloadManager::persistentDataPath
	String_t* ___persistentDataPath_4;
	// System.Collections.Generic.List`1<System.String> Unibill.Impl.DownloadManager::scheduledDownloads
	List_1_t1398341365 * ___scheduledDownloads_5;
	// System.Int32 Unibill.Impl.DownloadManager::bufferSize
	int32_t ___bufferSize_6;
	// System.Byte[] Unibill.Impl.DownloadManager::BUFFER
	ByteU5BU5D_t3397334013* ___BUFFER_10;
	// System.Threading.AutoResetEvent Unibill.Impl.DownloadManager::DATA_READY
	AutoResetEvent_t15112628 * ___DATA_READY_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Unibill.Impl.DownloadManager::UNPACK_FINISHED
	bool ___UNPACK_FINISHED_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Unibill.Impl.DownloadManager::DATA_FLUSHED
	bool ___DATA_FLUSHED_13;
	// System.IO.FileStream modreq(System.Runtime.CompilerServices.IsVolatile) Unibill.Impl.DownloadManager::fileStream
	FileStream_t1695958676 * ___fileStream_14;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) Unibill.Impl.DownloadManager::bytesReceived
	int32_t ___bytesReceived_15;
	// Unibill.Impl.BillingPlatform Unibill.Impl.DownloadManager::platform
	int32_t ___platform_16;
	// System.String Unibill.Impl.DownloadManager::appSecret
	String_t* ___appSecret_17;
	// System.String Unibill.Impl.DownloadManager::appId
	String_t* ___appId_18;
	// System.Action`2<System.String,System.String> Unibill.Impl.DownloadManager::onDownloadCompletedEvent
	Action_2_t4234541925 * ___onDownloadCompletedEvent_19;
	// System.Action`2<System.String,System.String> Unibill.Impl.DownloadManager::onDownloadFailedEvent
	Action_2_t4234541925 * ___onDownloadFailedEvent_20;
	// System.Action`2<System.String,System.Int32> Unibill.Impl.DownloadManager::onDownloadProgressedEvent
	Action_2_t4277199140 * ___onDownloadProgressedEvent_21;
	// UnityEngine.WaitForFixedUpdate Unibill.Impl.DownloadManager::waiter
	WaitForFixedUpdate_t3968615785 * ___waiter_22;
	// System.Random Unibill.Impl.DownloadManager::rand
	Random_t1044426839 * ___rand_23;

public:
	inline static int32_t get_offset_of_util_0() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___util_0)); }
	inline Il2CppObject * get_util_0() const { return ___util_0; }
	inline Il2CppObject ** get_address_of_util_0() { return &___util_0; }
	inline void set_util_0(Il2CppObject * value)
	{
		___util_0 = value;
		Il2CppCodeGenWriteBarrier(&___util_0, value);
	}

	inline static int32_t get_offset_of_storage_1() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___storage_1)); }
	inline Il2CppObject * get_storage_1() const { return ___storage_1; }
	inline Il2CppObject ** get_address_of_storage_1() { return &___storage_1; }
	inline void set_storage_1(Il2CppObject * value)
	{
		___storage_1 = value;
		Il2CppCodeGenWriteBarrier(&___storage_1, value);
	}

	inline static int32_t get_offset_of_fetcher_2() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___fetcher_2)); }
	inline Il2CppObject * get_fetcher_2() const { return ___fetcher_2; }
	inline Il2CppObject ** get_address_of_fetcher_2() { return &___fetcher_2; }
	inline void set_fetcher_2(Il2CppObject * value)
	{
		___fetcher_2 = value;
		Il2CppCodeGenWriteBarrier(&___fetcher_2, value);
	}

	inline static int32_t get_offset_of_logger_3() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___logger_3)); }
	inline Il2CppObject * get_logger_3() const { return ___logger_3; }
	inline Il2CppObject ** get_address_of_logger_3() { return &___logger_3; }
	inline void set_logger_3(Il2CppObject * value)
	{
		___logger_3 = value;
		Il2CppCodeGenWriteBarrier(&___logger_3, value);
	}

	inline static int32_t get_offset_of_persistentDataPath_4() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___persistentDataPath_4)); }
	inline String_t* get_persistentDataPath_4() const { return ___persistentDataPath_4; }
	inline String_t** get_address_of_persistentDataPath_4() { return &___persistentDataPath_4; }
	inline void set_persistentDataPath_4(String_t* value)
	{
		___persistentDataPath_4 = value;
		Il2CppCodeGenWriteBarrier(&___persistentDataPath_4, value);
	}

	inline static int32_t get_offset_of_scheduledDownloads_5() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___scheduledDownloads_5)); }
	inline List_1_t1398341365 * get_scheduledDownloads_5() const { return ___scheduledDownloads_5; }
	inline List_1_t1398341365 ** get_address_of_scheduledDownloads_5() { return &___scheduledDownloads_5; }
	inline void set_scheduledDownloads_5(List_1_t1398341365 * value)
	{
		___scheduledDownloads_5 = value;
		Il2CppCodeGenWriteBarrier(&___scheduledDownloads_5, value);
	}

	inline static int32_t get_offset_of_bufferSize_6() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___bufferSize_6)); }
	inline int32_t get_bufferSize_6() const { return ___bufferSize_6; }
	inline int32_t* get_address_of_bufferSize_6() { return &___bufferSize_6; }
	inline void set_bufferSize_6(int32_t value)
	{
		___bufferSize_6 = value;
	}

	inline static int32_t get_offset_of_BUFFER_10() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___BUFFER_10)); }
	inline ByteU5BU5D_t3397334013* get_BUFFER_10() const { return ___BUFFER_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_BUFFER_10() { return &___BUFFER_10; }
	inline void set_BUFFER_10(ByteU5BU5D_t3397334013* value)
	{
		___BUFFER_10 = value;
		Il2CppCodeGenWriteBarrier(&___BUFFER_10, value);
	}

	inline static int32_t get_offset_of_DATA_READY_11() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___DATA_READY_11)); }
	inline AutoResetEvent_t15112628 * get_DATA_READY_11() const { return ___DATA_READY_11; }
	inline AutoResetEvent_t15112628 ** get_address_of_DATA_READY_11() { return &___DATA_READY_11; }
	inline void set_DATA_READY_11(AutoResetEvent_t15112628 * value)
	{
		___DATA_READY_11 = value;
		Il2CppCodeGenWriteBarrier(&___DATA_READY_11, value);
	}

	inline static int32_t get_offset_of_UNPACK_FINISHED_12() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___UNPACK_FINISHED_12)); }
	inline bool get_UNPACK_FINISHED_12() const { return ___UNPACK_FINISHED_12; }
	inline bool* get_address_of_UNPACK_FINISHED_12() { return &___UNPACK_FINISHED_12; }
	inline void set_UNPACK_FINISHED_12(bool value)
	{
		___UNPACK_FINISHED_12 = value;
	}

	inline static int32_t get_offset_of_DATA_FLUSHED_13() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___DATA_FLUSHED_13)); }
	inline bool get_DATA_FLUSHED_13() const { return ___DATA_FLUSHED_13; }
	inline bool* get_address_of_DATA_FLUSHED_13() { return &___DATA_FLUSHED_13; }
	inline void set_DATA_FLUSHED_13(bool value)
	{
		___DATA_FLUSHED_13 = value;
	}

	inline static int32_t get_offset_of_fileStream_14() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___fileStream_14)); }
	inline FileStream_t1695958676 * get_fileStream_14() const { return ___fileStream_14; }
	inline FileStream_t1695958676 ** get_address_of_fileStream_14() { return &___fileStream_14; }
	inline void set_fileStream_14(FileStream_t1695958676 * value)
	{
		___fileStream_14 = value;
		Il2CppCodeGenWriteBarrier(&___fileStream_14, value);
	}

	inline static int32_t get_offset_of_bytesReceived_15() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___bytesReceived_15)); }
	inline int32_t get_bytesReceived_15() const { return ___bytesReceived_15; }
	inline int32_t* get_address_of_bytesReceived_15() { return &___bytesReceived_15; }
	inline void set_bytesReceived_15(int32_t value)
	{
		___bytesReceived_15 = value;
	}

	inline static int32_t get_offset_of_platform_16() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___platform_16)); }
	inline int32_t get_platform_16() const { return ___platform_16; }
	inline int32_t* get_address_of_platform_16() { return &___platform_16; }
	inline void set_platform_16(int32_t value)
	{
		___platform_16 = value;
	}

	inline static int32_t get_offset_of_appSecret_17() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___appSecret_17)); }
	inline String_t* get_appSecret_17() const { return ___appSecret_17; }
	inline String_t** get_address_of_appSecret_17() { return &___appSecret_17; }
	inline void set_appSecret_17(String_t* value)
	{
		___appSecret_17 = value;
		Il2CppCodeGenWriteBarrier(&___appSecret_17, value);
	}

	inline static int32_t get_offset_of_appId_18() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___appId_18)); }
	inline String_t* get_appId_18() const { return ___appId_18; }
	inline String_t** get_address_of_appId_18() { return &___appId_18; }
	inline void set_appId_18(String_t* value)
	{
		___appId_18 = value;
		Il2CppCodeGenWriteBarrier(&___appId_18, value);
	}

	inline static int32_t get_offset_of_onDownloadCompletedEvent_19() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___onDownloadCompletedEvent_19)); }
	inline Action_2_t4234541925 * get_onDownloadCompletedEvent_19() const { return ___onDownloadCompletedEvent_19; }
	inline Action_2_t4234541925 ** get_address_of_onDownloadCompletedEvent_19() { return &___onDownloadCompletedEvent_19; }
	inline void set_onDownloadCompletedEvent_19(Action_2_t4234541925 * value)
	{
		___onDownloadCompletedEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadCompletedEvent_19, value);
	}

	inline static int32_t get_offset_of_onDownloadFailedEvent_20() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___onDownloadFailedEvent_20)); }
	inline Action_2_t4234541925 * get_onDownloadFailedEvent_20() const { return ___onDownloadFailedEvent_20; }
	inline Action_2_t4234541925 ** get_address_of_onDownloadFailedEvent_20() { return &___onDownloadFailedEvent_20; }
	inline void set_onDownloadFailedEvent_20(Action_2_t4234541925 * value)
	{
		___onDownloadFailedEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadFailedEvent_20, value);
	}

	inline static int32_t get_offset_of_onDownloadProgressedEvent_21() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___onDownloadProgressedEvent_21)); }
	inline Action_2_t4277199140 * get_onDownloadProgressedEvent_21() const { return ___onDownloadProgressedEvent_21; }
	inline Action_2_t4277199140 ** get_address_of_onDownloadProgressedEvent_21() { return &___onDownloadProgressedEvent_21; }
	inline void set_onDownloadProgressedEvent_21(Action_2_t4277199140 * value)
	{
		___onDownloadProgressedEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadProgressedEvent_21, value);
	}

	inline static int32_t get_offset_of_waiter_22() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___waiter_22)); }
	inline WaitForFixedUpdate_t3968615785 * get_waiter_22() const { return ___waiter_22; }
	inline WaitForFixedUpdate_t3968615785 ** get_address_of_waiter_22() { return &___waiter_22; }
	inline void set_waiter_22(WaitForFixedUpdate_t3968615785 * value)
	{
		___waiter_22 = value;
		Il2CppCodeGenWriteBarrier(&___waiter_22, value);
	}

	inline static int32_t get_offset_of_rand_23() { return static_cast<int32_t>(offsetof(DownloadManager_t32803451, ___rand_23)); }
	inline Random_t1044426839 * get_rand_23() const { return ___rand_23; }
	inline Random_t1044426839 ** get_address_of_rand_23() { return &___rand_23; }
	inline void set_rand_23(Random_t1044426839 * value)
	{
		___rand_23 = value;
		Il2CppCodeGenWriteBarrier(&___rand_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
