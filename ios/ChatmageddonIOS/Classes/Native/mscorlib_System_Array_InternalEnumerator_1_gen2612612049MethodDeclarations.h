﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2612612049.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void System.Array/InternalEnumerator`1<UnibillError>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1174127012_gshared (InternalEnumerator_1_t2612612049 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1174127012(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2612612049 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1174127012_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnibillError>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1866251828_gshared (InternalEnumerator_1_t2612612049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1866251828(__this, method) ((  void (*) (InternalEnumerator_1_t2612612049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1866251828_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnibillError>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3030731160_gshared (InternalEnumerator_1_t2612612049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3030731160(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2612612049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3030731160_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnibillError>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m647177523_gshared (InternalEnumerator_1_t2612612049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m647177523(__this, method) ((  void (*) (InternalEnumerator_1_t2612612049 *, const MethodInfo*))InternalEnumerator_1_Dispose_m647177523_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnibillError>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m609887980_gshared (InternalEnumerator_1_t2612612049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m609887980(__this, method) ((  bool (*) (InternalEnumerator_1_t2612612049 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m609887980_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnibillError>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2155169523_gshared (InternalEnumerator_1_t2612612049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2155169523(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2612612049 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2155169523_gshared)(__this, method)
