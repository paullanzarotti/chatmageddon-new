﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2/<showMailComposerWithScreenshot>c__AnonStorey5
struct U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2/<showMailComposerWithScreenshot>c__AnonStorey5::.ctor()
extern "C"  void U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5__ctor_m2761527286 (U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2/<showMailComposerWithScreenshot>c__AnonStorey5::<>m__0(UnityEngine.Texture2D)
extern "C"  void U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_U3CU3Em__0_m3496580769 (U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
