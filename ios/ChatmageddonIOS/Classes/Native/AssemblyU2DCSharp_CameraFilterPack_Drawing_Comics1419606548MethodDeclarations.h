﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Comics
struct CameraFilterPack_Drawing_Comics_t1419606548;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Comics::.ctor()
extern "C"  void CameraFilterPack_Drawing_Comics__ctor_m614934263 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Comics::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Comics_get_material_m3638848832 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::Start()
extern "C"  void CameraFilterPack_Drawing_Comics_Start_m716171931 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Comics_OnRenderImage_m2316234323 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Comics_OnValidate_m2469387656 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::Update()
extern "C"  void CameraFilterPack_Drawing_Comics_Update_m2669168370 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Comics_OnDisable_m949217332 (CameraFilterPack_Drawing_Comics_t1419606548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
