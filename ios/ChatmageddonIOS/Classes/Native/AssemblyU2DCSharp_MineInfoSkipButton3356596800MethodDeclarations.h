﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineInfoSkipButton
struct MineInfoSkipButton_t3356596800;

#include "codegen/il2cpp-codegen.h"

// System.Void MineInfoSkipButton::.ctor()
extern "C"  void MineInfoSkipButton__ctor_m2608448521 (MineInfoSkipButton_t3356596800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoSkipButton::OnButtonClick()
extern "C"  void MineInfoSkipButton_OnButtonClick_m3745478984 (MineInfoSkipButton_t3356596800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
