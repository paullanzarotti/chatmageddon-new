﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<RefInt>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3444277789(__this, ___l0, method) ((  void (*) (Enumerator_t1842722160 *, List_1_t2307992486 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RefInt>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2288662925(__this, method) ((  void (*) (Enumerator_t1842722160 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<RefInt>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2788207697(__this, method) ((  Il2CppObject * (*) (Enumerator_t1842722160 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RefInt>::Dispose()
#define Enumerator_Dispose_m3741868374(__this, method) ((  void (*) (Enumerator_t1842722160 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RefInt>::VerifyState()
#define Enumerator_VerifyState_m1931241111(__this, method) ((  void (*) (Enumerator_t1842722160 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<RefInt>::MoveNext()
#define Enumerator_MoveNext_m466077413(__this, method) ((  bool (*) (Enumerator_t1842722160 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<RefInt>::get_Current()
#define Enumerator_get_Current_m3615019174(__this, method) ((  RefInt_t2938871354 * (*) (Enumerator_t1842722160 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
