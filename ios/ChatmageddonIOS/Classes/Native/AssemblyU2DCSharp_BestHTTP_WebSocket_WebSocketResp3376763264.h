﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BestHTTP.WebSocket.WebSocket
struct WebSocket_t71448861;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.String>
struct Action_2_t1029759410;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>
struct Action_2_t2397873190;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct Action_2_t3844780342;
// System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>
struct Action_3_t2791643950;
// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct List_1_t4213362297;
// BestHTTP.WebSocket.Frames.WebSocketFrameReader
struct WebSocketFrameReader_t549273869;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocketResponse
struct  WebSocketResponse_t3376763264  : public HTTPResponse_t62748825
{
public:
	// BestHTTP.WebSocket.WebSocket BestHTTP.WebSocket.WebSocketResponse::<WebSocket>k__BackingField
	WebSocket_t71448861 * ___U3CWebSocketU3Ek__BackingField_25;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.String> BestHTTP.WebSocket.WebSocketResponse::OnText
	Action_2_t1029759410 * ___OnText_26;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]> BestHTTP.WebSocket.WebSocketResponse::OnBinary
	Action_2_t2397873190 * ___OnBinary_27;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::OnIncompleteFrame
	Action_2_t3844780342 * ___OnIncompleteFrame_28;
	// System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String> BestHTTP.WebSocket.WebSocketResponse::OnClosed
	Action_3_t2791643950 * ___OnClosed_29;
	// System.TimeSpan BestHTTP.WebSocket.WebSocketResponse::<PingFrequnecy>k__BackingField
	TimeSpan_t3430258949  ___U3CPingFrequnecyU3Ek__BackingField_30;
	// System.UInt16 BestHTTP.WebSocket.WebSocketResponse::<MaxFragmentSize>k__BackingField
	uint16_t ___U3CMaxFragmentSizeU3Ek__BackingField_31;
	// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::IncompleteFrames
	List_1_t4213362297 * ___IncompleteFrames_32;
	// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::CompletedFrames
	List_1_t4213362297 * ___CompletedFrames_33;
	// BestHTTP.WebSocket.Frames.WebSocketFrameReader BestHTTP.WebSocket.WebSocketResponse::CloseFrame
	WebSocketFrameReader_t549273869 * ___CloseFrame_34;
	// System.Object BestHTTP.WebSocket.WebSocketResponse::FrameLock
	Il2CppObject * ___FrameLock_35;
	// System.Object BestHTTP.WebSocket.WebSocketResponse::SendLock
	Il2CppObject * ___SendLock_36;
	// System.Boolean BestHTTP.WebSocket.WebSocketResponse::closeSent
	bool ___closeSent_37;
	// System.Boolean BestHTTP.WebSocket.WebSocketResponse::closed
	bool ___closed_38;
	// System.DateTime BestHTTP.WebSocket.WebSocketResponse::lastPing
	DateTime_t693205669  ___lastPing_39;

public:
	inline static int32_t get_offset_of_U3CWebSocketU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___U3CWebSocketU3Ek__BackingField_25)); }
	inline WebSocket_t71448861 * get_U3CWebSocketU3Ek__BackingField_25() const { return ___U3CWebSocketU3Ek__BackingField_25; }
	inline WebSocket_t71448861 ** get_address_of_U3CWebSocketU3Ek__BackingField_25() { return &___U3CWebSocketU3Ek__BackingField_25; }
	inline void set_U3CWebSocketU3Ek__BackingField_25(WebSocket_t71448861 * value)
	{
		___U3CWebSocketU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWebSocketU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_OnText_26() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnText_26)); }
	inline Action_2_t1029759410 * get_OnText_26() const { return ___OnText_26; }
	inline Action_2_t1029759410 ** get_address_of_OnText_26() { return &___OnText_26; }
	inline void set_OnText_26(Action_2_t1029759410 * value)
	{
		___OnText_26 = value;
		Il2CppCodeGenWriteBarrier(&___OnText_26, value);
	}

	inline static int32_t get_offset_of_OnBinary_27() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnBinary_27)); }
	inline Action_2_t2397873190 * get_OnBinary_27() const { return ___OnBinary_27; }
	inline Action_2_t2397873190 ** get_address_of_OnBinary_27() { return &___OnBinary_27; }
	inline void set_OnBinary_27(Action_2_t2397873190 * value)
	{
		___OnBinary_27 = value;
		Il2CppCodeGenWriteBarrier(&___OnBinary_27, value);
	}

	inline static int32_t get_offset_of_OnIncompleteFrame_28() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnIncompleteFrame_28)); }
	inline Action_2_t3844780342 * get_OnIncompleteFrame_28() const { return ___OnIncompleteFrame_28; }
	inline Action_2_t3844780342 ** get_address_of_OnIncompleteFrame_28() { return &___OnIncompleteFrame_28; }
	inline void set_OnIncompleteFrame_28(Action_2_t3844780342 * value)
	{
		___OnIncompleteFrame_28 = value;
		Il2CppCodeGenWriteBarrier(&___OnIncompleteFrame_28, value);
	}

	inline static int32_t get_offset_of_OnClosed_29() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnClosed_29)); }
	inline Action_3_t2791643950 * get_OnClosed_29() const { return ___OnClosed_29; }
	inline Action_3_t2791643950 ** get_address_of_OnClosed_29() { return &___OnClosed_29; }
	inline void set_OnClosed_29(Action_3_t2791643950 * value)
	{
		___OnClosed_29 = value;
		Il2CppCodeGenWriteBarrier(&___OnClosed_29, value);
	}

	inline static int32_t get_offset_of_U3CPingFrequnecyU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___U3CPingFrequnecyU3Ek__BackingField_30)); }
	inline TimeSpan_t3430258949  get_U3CPingFrequnecyU3Ek__BackingField_30() const { return ___U3CPingFrequnecyU3Ek__BackingField_30; }
	inline TimeSpan_t3430258949 * get_address_of_U3CPingFrequnecyU3Ek__BackingField_30() { return &___U3CPingFrequnecyU3Ek__BackingField_30; }
	inline void set_U3CPingFrequnecyU3Ek__BackingField_30(TimeSpan_t3430258949  value)
	{
		___U3CPingFrequnecyU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CMaxFragmentSizeU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___U3CMaxFragmentSizeU3Ek__BackingField_31)); }
	inline uint16_t get_U3CMaxFragmentSizeU3Ek__BackingField_31() const { return ___U3CMaxFragmentSizeU3Ek__BackingField_31; }
	inline uint16_t* get_address_of_U3CMaxFragmentSizeU3Ek__BackingField_31() { return &___U3CMaxFragmentSizeU3Ek__BackingField_31; }
	inline void set_U3CMaxFragmentSizeU3Ek__BackingField_31(uint16_t value)
	{
		___U3CMaxFragmentSizeU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_IncompleteFrames_32() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___IncompleteFrames_32)); }
	inline List_1_t4213362297 * get_IncompleteFrames_32() const { return ___IncompleteFrames_32; }
	inline List_1_t4213362297 ** get_address_of_IncompleteFrames_32() { return &___IncompleteFrames_32; }
	inline void set_IncompleteFrames_32(List_1_t4213362297 * value)
	{
		___IncompleteFrames_32 = value;
		Il2CppCodeGenWriteBarrier(&___IncompleteFrames_32, value);
	}

	inline static int32_t get_offset_of_CompletedFrames_33() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___CompletedFrames_33)); }
	inline List_1_t4213362297 * get_CompletedFrames_33() const { return ___CompletedFrames_33; }
	inline List_1_t4213362297 ** get_address_of_CompletedFrames_33() { return &___CompletedFrames_33; }
	inline void set_CompletedFrames_33(List_1_t4213362297 * value)
	{
		___CompletedFrames_33 = value;
		Il2CppCodeGenWriteBarrier(&___CompletedFrames_33, value);
	}

	inline static int32_t get_offset_of_CloseFrame_34() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___CloseFrame_34)); }
	inline WebSocketFrameReader_t549273869 * get_CloseFrame_34() const { return ___CloseFrame_34; }
	inline WebSocketFrameReader_t549273869 ** get_address_of_CloseFrame_34() { return &___CloseFrame_34; }
	inline void set_CloseFrame_34(WebSocketFrameReader_t549273869 * value)
	{
		___CloseFrame_34 = value;
		Il2CppCodeGenWriteBarrier(&___CloseFrame_34, value);
	}

	inline static int32_t get_offset_of_FrameLock_35() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___FrameLock_35)); }
	inline Il2CppObject * get_FrameLock_35() const { return ___FrameLock_35; }
	inline Il2CppObject ** get_address_of_FrameLock_35() { return &___FrameLock_35; }
	inline void set_FrameLock_35(Il2CppObject * value)
	{
		___FrameLock_35 = value;
		Il2CppCodeGenWriteBarrier(&___FrameLock_35, value);
	}

	inline static int32_t get_offset_of_SendLock_36() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___SendLock_36)); }
	inline Il2CppObject * get_SendLock_36() const { return ___SendLock_36; }
	inline Il2CppObject ** get_address_of_SendLock_36() { return &___SendLock_36; }
	inline void set_SendLock_36(Il2CppObject * value)
	{
		___SendLock_36 = value;
		Il2CppCodeGenWriteBarrier(&___SendLock_36, value);
	}

	inline static int32_t get_offset_of_closeSent_37() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___closeSent_37)); }
	inline bool get_closeSent_37() const { return ___closeSent_37; }
	inline bool* get_address_of_closeSent_37() { return &___closeSent_37; }
	inline void set_closeSent_37(bool value)
	{
		___closeSent_37 = value;
	}

	inline static int32_t get_offset_of_closed_38() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___closed_38)); }
	inline bool get_closed_38() const { return ___closed_38; }
	inline bool* get_address_of_closed_38() { return &___closed_38; }
	inline void set_closed_38(bool value)
	{
		___closed_38 = value;
	}

	inline static int32_t get_offset_of_lastPing_39() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___lastPing_39)); }
	inline DateTime_t693205669  get_lastPing_39() const { return ___lastPing_39; }
	inline DateTime_t693205669 * get_address_of_lastPing_39() { return &___lastPing_39; }
	inline void set_lastPing_39(DateTime_t693205669  value)
	{
		___lastPing_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
