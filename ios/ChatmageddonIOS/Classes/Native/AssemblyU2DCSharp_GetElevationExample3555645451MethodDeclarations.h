﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetElevationExample
struct GetElevationExample_t3555645451;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GetElevationExample::.ctor()
extern "C"  void GetElevationExample__ctor_m830058182 (GetElevationExample_t3555645451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetElevationExample::Start()
extern "C"  void GetElevationExample_Start_m1368269458 (GetElevationExample_t3555645451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetElevationExample::OnMapClick()
extern "C"  void GetElevationExample_OnMapClick_m119316013 (GetElevationExample_t3555645451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetElevationExample::OnComplete(System.String)
extern "C"  void GetElevationExample_OnComplete_m1746926492 (GetElevationExample_t3555645451 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
