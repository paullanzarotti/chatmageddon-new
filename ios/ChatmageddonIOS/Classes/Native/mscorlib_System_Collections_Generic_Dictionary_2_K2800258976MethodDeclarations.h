﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>
struct KeyCollection_t2800258976;
// System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>
struct Dictionary_2_t316761205;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3006264643.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3863182762_gshared (KeyCollection_t2800258976 * __this, Dictionary_2_t316761205 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3863182762(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2800258976 *, Dictionary_2_t316761205 *, const MethodInfo*))KeyCollection__ctor_m3863182762_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1851497592_gshared (KeyCollection_t2800258976 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1851497592(__this, ___item0, method) ((  void (*) (KeyCollection_t2800258976 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1851497592_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2463495729_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2463495729(__this, method) ((  void (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2463495729_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m920020740_gshared (KeyCollection_t2800258976 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m920020740(__this, ___item0, method) ((  bool (*) (KeyCollection_t2800258976 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m920020740_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m816443629_gshared (KeyCollection_t2800258976 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m816443629(__this, ___item0, method) ((  bool (*) (KeyCollection_t2800258976 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m816443629_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3451485171_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3451485171(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3451485171_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3076228827_gshared (KeyCollection_t2800258976 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3076228827(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2800258976 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3076228827_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3072459780_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3072459780(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3072459780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2607975649_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2607975649(__this, method) ((  bool (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2607975649_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4173398147_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4173398147(__this, method) ((  bool (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4173398147_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2415405703_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2415405703(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2415405703_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3353117189_gshared (KeyCollection_t2800258976 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3353117189(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2800258976 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3353117189_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::GetEnumerator()
extern "C"  Enumerator_t3006264643  KeyCollection_GetEnumerator_m3892606472_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3892606472(__this, method) ((  Enumerator_t3006264643  (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_GetEnumerator_m3892606472_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2209461803_gshared (KeyCollection_t2800258976 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2209461803(__this, method) ((  int32_t (*) (KeyCollection_t2800258976 *, const MethodInfo*))KeyCollection_get_Count_m2209461803_gshared)(__this, method)
