﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ChatmageddonPanelTransitionManager>::.ctor()
#define MonoSingleton_1__ctor_m2504146511(__this, method) ((  void (*) (MonoSingleton_1_t1149681085 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonPanelTransitionManager>::Awake()
#define MonoSingleton_1_Awake_m4015629854(__this, method) ((  void (*) (MonoSingleton_1_t1149681085 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ChatmageddonPanelTransitionManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2797109228(__this /* static, unused */, method) ((  ChatmageddonPanelTransitionManager_t1399015365 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ChatmageddonPanelTransitionManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m395896004(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ChatmageddonPanelTransitionManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2121759537(__this, method) ((  void (*) (MonoSingleton_1_t1149681085 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonPanelTransitionManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m695987609(__this, method) ((  void (*) (MonoSingleton_1_t1149681085 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonPanelTransitionManager>::.cctor()
#define MonoSingleton_1__cctor_m312221828(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
