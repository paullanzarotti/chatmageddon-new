﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendInfoNavScreen/<WaitForSeconds>c__Iterator0
struct U3CWaitForSecondsU3Ec__Iterator0_t2549067708;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DefendInfoNavScreen/<WaitForSeconds>c__Iterator0::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0__ctor_m82552311 (U3CWaitForSecondsU3Ec__Iterator0_t2549067708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DefendInfoNavScreen/<WaitForSeconds>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m3907923357 (U3CWaitForSecondsU3Ec__Iterator0_t2549067708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DefendInfoNavScreen/<WaitForSeconds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2819736153 (U3CWaitForSecondsU3Ec__Iterator0_t2549067708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DefendInfoNavScreen/<WaitForSeconds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3877492257 (U3CWaitForSecondsU3Ec__Iterator0_t2549067708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen/<WaitForSeconds>c__Iterator0::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Dispose_m242204922 (U3CWaitForSecondsU3Ec__Iterator0_t2549067708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen/<WaitForSeconds>c__Iterator0::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Reset_m3308998192 (U3CWaitForSecondsU3Ec__Iterator0_t2549067708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
