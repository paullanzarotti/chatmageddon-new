﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatContact
struct  ChatContact_t3760700014  : public Il2CppObject
{
public:
	// System.Boolean ChatContact::alphaSeperator
	bool ___alphaSeperator_0;
	// System.String ChatContact::contactFirstName
	String_t* ___contactFirstName_1;
	// System.String ChatContact::contactLastName
	String_t* ___contactLastName_2;
	// System.String ChatContact::contactUsername
	String_t* ___contactUsername_3;
	// System.String ChatContact::contactID
	String_t* ___contactID_4;

public:
	inline static int32_t get_offset_of_alphaSeperator_0() { return static_cast<int32_t>(offsetof(ChatContact_t3760700014, ___alphaSeperator_0)); }
	inline bool get_alphaSeperator_0() const { return ___alphaSeperator_0; }
	inline bool* get_address_of_alphaSeperator_0() { return &___alphaSeperator_0; }
	inline void set_alphaSeperator_0(bool value)
	{
		___alphaSeperator_0 = value;
	}

	inline static int32_t get_offset_of_contactFirstName_1() { return static_cast<int32_t>(offsetof(ChatContact_t3760700014, ___contactFirstName_1)); }
	inline String_t* get_contactFirstName_1() const { return ___contactFirstName_1; }
	inline String_t** get_address_of_contactFirstName_1() { return &___contactFirstName_1; }
	inline void set_contactFirstName_1(String_t* value)
	{
		___contactFirstName_1 = value;
		Il2CppCodeGenWriteBarrier(&___contactFirstName_1, value);
	}

	inline static int32_t get_offset_of_contactLastName_2() { return static_cast<int32_t>(offsetof(ChatContact_t3760700014, ___contactLastName_2)); }
	inline String_t* get_contactLastName_2() const { return ___contactLastName_2; }
	inline String_t** get_address_of_contactLastName_2() { return &___contactLastName_2; }
	inline void set_contactLastName_2(String_t* value)
	{
		___contactLastName_2 = value;
		Il2CppCodeGenWriteBarrier(&___contactLastName_2, value);
	}

	inline static int32_t get_offset_of_contactUsername_3() { return static_cast<int32_t>(offsetof(ChatContact_t3760700014, ___contactUsername_3)); }
	inline String_t* get_contactUsername_3() const { return ___contactUsername_3; }
	inline String_t** get_address_of_contactUsername_3() { return &___contactUsername_3; }
	inline void set_contactUsername_3(String_t* value)
	{
		___contactUsername_3 = value;
		Il2CppCodeGenWriteBarrier(&___contactUsername_3, value);
	}

	inline static int32_t get_offset_of_contactID_4() { return static_cast<int32_t>(offsetof(ChatContact_t3760700014, ___contactID_4)); }
	inline String_t* get_contactID_4() const { return ___contactID_4; }
	inline String_t** get_address_of_contactID_4() { return &___contactID_4; }
	inline void set_contactID_4(String_t* value)
	{
		___contactID_4 = value;
		Il2CppCodeGenWriteBarrier(&___contactID_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
