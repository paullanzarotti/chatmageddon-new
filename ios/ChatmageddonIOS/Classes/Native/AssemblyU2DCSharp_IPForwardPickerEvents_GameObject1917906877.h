﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPForwardPickerEvents/GameObjectAndMessage
struct  GameObjectAndMessage_t1917906877  : public Il2CppObject
{
public:
	// UnityEngine.GameObject IPForwardPickerEvents/GameObjectAndMessage::gameObject
	GameObject_t1756533147 * ___gameObject_0;
	// System.String IPForwardPickerEvents/GameObjectAndMessage::message
	String_t* ___message_1;
	// System.Boolean IPForwardPickerEvents/GameObjectAndMessage::notifyInStart
	bool ___notifyInStart_2;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(GameObjectAndMessage_t1917906877, ___gameObject_0)); }
	inline GameObject_t1756533147 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1756533147 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_0, value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(GameObjectAndMessage_t1917906877, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}

	inline static int32_t get_offset_of_notifyInStart_2() { return static_cast<int32_t>(offsetof(GameObjectAndMessage_t1917906877, ___notifyInStart_2)); }
	inline bool get_notifyInStart_2() const { return ___notifyInStart_2; }
	inline bool* get_address_of_notifyInStart_2() { return &___notifyInStart_2; }
	inline void set_notifyInStart_2(bool value)
	{
		___notifyInStart_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
