﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.SecurityAttribute
struct SecurityAttribute_t2016950496;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction446643378.h"

// System.Void System.Security.Permissions.SecurityAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C"  void SecurityAttribute__ctor_m272524786 (SecurityAttribute_t2016950496 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityAttribute::get_Unrestricted()
extern "C"  bool SecurityAttribute_get_Unrestricted_m2442524274 (SecurityAttribute_t2016950496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityAttribute::set_Action(System.Security.Permissions.SecurityAction)
extern "C"  void SecurityAttribute_set_Action_m681927293 (SecurityAttribute_t2016950496 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
