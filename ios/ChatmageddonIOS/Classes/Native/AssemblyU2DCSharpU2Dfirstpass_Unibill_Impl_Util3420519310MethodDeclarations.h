﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.Util
struct Util_t3420519310;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.Util::.ctor()
extern "C"  void Util__ctor_m2355089846 (Util_t3420519310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.Util::ReadAllText(System.String)
extern "C"  String_t* Util_ReadAllText_m1606276369 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Util::WriteAllText(System.String,System.String)
extern "C"  void Util_WriteAllText_m2229288141 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
