﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemInfoController/<WaitForSeconds>c__Iterator0
struct U3CWaitForSecondsU3Ec__Iterator0_t3866018642;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemInfoController/<WaitForSeconds>c__Iterator0::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0__ctor_m26836387 (U3CWaitForSecondsU3Ec__Iterator0_t3866018642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ItemInfoController/<WaitForSeconds>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m2800870061 (U3CWaitForSecondsU3Ec__Iterator0_t3866018642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemInfoController/<WaitForSeconds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m309576365 (U3CWaitForSecondsU3Ec__Iterator0_t3866018642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ItemInfoController/<WaitForSeconds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1478471765 (U3CWaitForSecondsU3Ec__Iterator0_t3866018642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController/<WaitForSeconds>c__Iterator0::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Dispose_m3601490684 (U3CWaitForSecondsU3Ec__Iterator0_t3866018642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController/<WaitForSeconds>c__Iterator0::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Reset_m2573244030 (U3CWaitForSecondsU3Ec__Iterator0_t3866018642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
