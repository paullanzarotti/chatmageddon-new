﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3113306953(__this, ___l0, method) ((  void (*) (Enumerator_t2816583126 *, List_1_t3281853452 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3069111641(__this, method) ((  void (*) (Enumerator_t2816583126 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1182817257(__this, method) ((  Il2CppObject * (*) (Enumerator_t2816583126 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::Dispose()
#define Enumerator_Dispose_m3199858124(__this, method) ((  void (*) (Enumerator_t2816583126 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::VerifyState()
#define Enumerator_VerifyState_m3782311003(__this, method) ((  void (*) (Enumerator_t2816583126 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::MoveNext()
#define Enumerator_MoveNext_m2715660185(__this, method) ((  bool (*) (Enumerator_t2816583126 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.ComponentModel.MaskedTextProvider/EditPosition>::get_Current()
#define Enumerator_get_Current_m3433455374(__this, method) ((  EditPosition_t3912732320 * (*) (Enumerator_t2816583126 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
