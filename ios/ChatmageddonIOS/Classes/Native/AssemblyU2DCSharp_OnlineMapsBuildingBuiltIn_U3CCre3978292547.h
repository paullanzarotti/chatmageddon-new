﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuildingBuiltIn/<Create>c__AnonStorey0
struct  U3CCreateU3Ec__AnonStorey0_t3978292547  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 OnlineMapsBuildingBuiltIn/<Create>c__AnonStorey0::centerPoint
	Vector3_t2243707580  ___centerPoint_0;

public:
	inline static int32_t get_offset_of_centerPoint_0() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey0_t3978292547, ___centerPoint_0)); }
	inline Vector3_t2243707580  get_centerPoint_0() const { return ___centerPoint_0; }
	inline Vector3_t2243707580 * get_address_of_centerPoint_0() { return &___centerPoint_0; }
	inline void set_centerPoint_0(Vector3_t2243707580  value)
	{
		___centerPoint_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
