﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultModalUI
struct ResultModalUI_t969511824;
// UILabel
struct UILabel_t1795115428;
// ScoreZoneController
struct ScoreZoneController_t1333878292;
// ResultBonusController
struct ResultBonusController_t3929995048;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileResultsModal
struct  MissileResultsModal_t1615487503  : public MonoBehaviour_t1158329972
{
public:
	// ResultModalUI MissileResultsModal::modal
	ResultModalUI_t969511824 * ___modal_2;
	// UILabel MissileResultsModal::inLabel
	UILabel_t1795115428 * ___inLabel_3;
	// UILabel MissileResultsModal::timeLabel
	UILabel_t1795115428 * ___timeLabel_4;
	// ScoreZoneController MissileResultsModal::scoreZoneController
	ScoreZoneController_t1333878292 * ___scoreZoneController_5;
	// ResultBonusController MissileResultsModal::bonusController
	ResultBonusController_t3929995048 * ___bonusController_6;

public:
	inline static int32_t get_offset_of_modal_2() { return static_cast<int32_t>(offsetof(MissileResultsModal_t1615487503, ___modal_2)); }
	inline ResultModalUI_t969511824 * get_modal_2() const { return ___modal_2; }
	inline ResultModalUI_t969511824 ** get_address_of_modal_2() { return &___modal_2; }
	inline void set_modal_2(ResultModalUI_t969511824 * value)
	{
		___modal_2 = value;
		Il2CppCodeGenWriteBarrier(&___modal_2, value);
	}

	inline static int32_t get_offset_of_inLabel_3() { return static_cast<int32_t>(offsetof(MissileResultsModal_t1615487503, ___inLabel_3)); }
	inline UILabel_t1795115428 * get_inLabel_3() const { return ___inLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_inLabel_3() { return &___inLabel_3; }
	inline void set_inLabel_3(UILabel_t1795115428 * value)
	{
		___inLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___inLabel_3, value);
	}

	inline static int32_t get_offset_of_timeLabel_4() { return static_cast<int32_t>(offsetof(MissileResultsModal_t1615487503, ___timeLabel_4)); }
	inline UILabel_t1795115428 * get_timeLabel_4() const { return ___timeLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_4() { return &___timeLabel_4; }
	inline void set_timeLabel_4(UILabel_t1795115428 * value)
	{
		___timeLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_4, value);
	}

	inline static int32_t get_offset_of_scoreZoneController_5() { return static_cast<int32_t>(offsetof(MissileResultsModal_t1615487503, ___scoreZoneController_5)); }
	inline ScoreZoneController_t1333878292 * get_scoreZoneController_5() const { return ___scoreZoneController_5; }
	inline ScoreZoneController_t1333878292 ** get_address_of_scoreZoneController_5() { return &___scoreZoneController_5; }
	inline void set_scoreZoneController_5(ScoreZoneController_t1333878292 * value)
	{
		___scoreZoneController_5 = value;
		Il2CppCodeGenWriteBarrier(&___scoreZoneController_5, value);
	}

	inline static int32_t get_offset_of_bonusController_6() { return static_cast<int32_t>(offsetof(MissileResultsModal_t1615487503, ___bonusController_6)); }
	inline ResultBonusController_t3929995048 * get_bonusController_6() const { return ___bonusController_6; }
	inline ResultBonusController_t3929995048 ** get_address_of_bonusController_6() { return &___bonusController_6; }
	inline void set_bonusController_6(ResultBonusController_t3929995048 * value)
	{
		___bonusController_6 = value;
		Il2CppCodeGenWriteBarrier(&___bonusController_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
