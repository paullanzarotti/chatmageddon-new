﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultModalUI
struct ResultModalUI_t969511824;
// UILabel
struct UILabel_t1795115428;
// MissileTrajectoryDisplay
struct MissileTrajectoryDisplay_t1441887151;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreZoneController
struct  ScoreZoneController_t1333878292  : public MonoBehaviour_t1158329972
{
public:
	// ResultModalUI ScoreZoneController::resultModal
	ResultModalUI_t969511824 * ___resultModal_2;
	// UILabel ScoreZoneController::leftSectionLabel
	UILabel_t1795115428 * ___leftSectionLabel_3;
	// UILabel ScoreZoneController::middleSectionLabel
	UILabel_t1795115428 * ___middleSectionLabel_4;
	// UILabel ScoreZoneController::rightSectionLabel
	UILabel_t1795115428 * ___rightSectionLabel_5;
	// MissileTrajectoryDisplay ScoreZoneController::trajectory
	MissileTrajectoryDisplay_t1441887151 * ___trajectory_6;
	// UILabel ScoreZoneController::timeLabel1
	UILabel_t1795115428 * ___timeLabel1_7;
	// UILabel ScoreZoneController::timeLabel2
	UILabel_t1795115428 * ___timeLabel2_8;
	// UILabel ScoreZoneController::timeLabel3
	UILabel_t1795115428 * ___timeLabel3_9;
	// UILabel ScoreZoneController::timeLabel4
	UILabel_t1795115428 * ___timeLabel4_10;
	// UnityEngine.GameObject ScoreZoneController::stealthUI
	GameObject_t1756533147 * ___stealthUI_11;

public:
	inline static int32_t get_offset_of_resultModal_2() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___resultModal_2)); }
	inline ResultModalUI_t969511824 * get_resultModal_2() const { return ___resultModal_2; }
	inline ResultModalUI_t969511824 ** get_address_of_resultModal_2() { return &___resultModal_2; }
	inline void set_resultModal_2(ResultModalUI_t969511824 * value)
	{
		___resultModal_2 = value;
		Il2CppCodeGenWriteBarrier(&___resultModal_2, value);
	}

	inline static int32_t get_offset_of_leftSectionLabel_3() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___leftSectionLabel_3)); }
	inline UILabel_t1795115428 * get_leftSectionLabel_3() const { return ___leftSectionLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_leftSectionLabel_3() { return &___leftSectionLabel_3; }
	inline void set_leftSectionLabel_3(UILabel_t1795115428 * value)
	{
		___leftSectionLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___leftSectionLabel_3, value);
	}

	inline static int32_t get_offset_of_middleSectionLabel_4() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___middleSectionLabel_4)); }
	inline UILabel_t1795115428 * get_middleSectionLabel_4() const { return ___middleSectionLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_middleSectionLabel_4() { return &___middleSectionLabel_4; }
	inline void set_middleSectionLabel_4(UILabel_t1795115428 * value)
	{
		___middleSectionLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___middleSectionLabel_4, value);
	}

	inline static int32_t get_offset_of_rightSectionLabel_5() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___rightSectionLabel_5)); }
	inline UILabel_t1795115428 * get_rightSectionLabel_5() const { return ___rightSectionLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_rightSectionLabel_5() { return &___rightSectionLabel_5; }
	inline void set_rightSectionLabel_5(UILabel_t1795115428 * value)
	{
		___rightSectionLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___rightSectionLabel_5, value);
	}

	inline static int32_t get_offset_of_trajectory_6() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___trajectory_6)); }
	inline MissileTrajectoryDisplay_t1441887151 * get_trajectory_6() const { return ___trajectory_6; }
	inline MissileTrajectoryDisplay_t1441887151 ** get_address_of_trajectory_6() { return &___trajectory_6; }
	inline void set_trajectory_6(MissileTrajectoryDisplay_t1441887151 * value)
	{
		___trajectory_6 = value;
		Il2CppCodeGenWriteBarrier(&___trajectory_6, value);
	}

	inline static int32_t get_offset_of_timeLabel1_7() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___timeLabel1_7)); }
	inline UILabel_t1795115428 * get_timeLabel1_7() const { return ___timeLabel1_7; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel1_7() { return &___timeLabel1_7; }
	inline void set_timeLabel1_7(UILabel_t1795115428 * value)
	{
		___timeLabel1_7 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel1_7, value);
	}

	inline static int32_t get_offset_of_timeLabel2_8() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___timeLabel2_8)); }
	inline UILabel_t1795115428 * get_timeLabel2_8() const { return ___timeLabel2_8; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel2_8() { return &___timeLabel2_8; }
	inline void set_timeLabel2_8(UILabel_t1795115428 * value)
	{
		___timeLabel2_8 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel2_8, value);
	}

	inline static int32_t get_offset_of_timeLabel3_9() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___timeLabel3_9)); }
	inline UILabel_t1795115428 * get_timeLabel3_9() const { return ___timeLabel3_9; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel3_9() { return &___timeLabel3_9; }
	inline void set_timeLabel3_9(UILabel_t1795115428 * value)
	{
		___timeLabel3_9 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel3_9, value);
	}

	inline static int32_t get_offset_of_timeLabel4_10() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___timeLabel4_10)); }
	inline UILabel_t1795115428 * get_timeLabel4_10() const { return ___timeLabel4_10; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel4_10() { return &___timeLabel4_10; }
	inline void set_timeLabel4_10(UILabel_t1795115428 * value)
	{
		___timeLabel4_10 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel4_10, value);
	}

	inline static int32_t get_offset_of_stealthUI_11() { return static_cast<int32_t>(offsetof(ScoreZoneController_t1333878292, ___stealthUI_11)); }
	inline GameObject_t1756533147 * get_stealthUI_11() const { return ___stealthUI_11; }
	inline GameObject_t1756533147 ** get_address_of_stealthUI_11() { return &___stealthUI_11; }
	inline void set_stealthUI_11(GameObject_t1756533147 * value)
	{
		___stealthUI_11 = value;
		Il2CppCodeGenWriteBarrier(&___stealthUI_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
