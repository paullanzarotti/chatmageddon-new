﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HomeTabController
struct HomeTabController_t1746680606;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoSingleton`1<HomeTabController>
struct  MonoSingleton_1_t1497346326  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct MonoSingleton_1_t1497346326_StaticFields
{
public:
	// T MonoSingleton`1::m_Instance
	HomeTabController_t1746680606 * ___m_Instance_2;

public:
	inline static int32_t get_offset_of_m_Instance_2() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t1497346326_StaticFields, ___m_Instance_2)); }
	inline HomeTabController_t1746680606 * get_m_Instance_2() const { return ___m_Instance_2; }
	inline HomeTabController_t1746680606 ** get_address_of_m_Instance_2() { return &___m_Instance_2; }
	inline void set_m_Instance_2(HomeTabController_t1746680606 * value)
	{
		___m_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
