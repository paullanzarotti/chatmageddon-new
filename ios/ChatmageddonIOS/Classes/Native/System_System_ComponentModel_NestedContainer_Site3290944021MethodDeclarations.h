﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.NestedContainer/Site
struct Site_t3290944021;
// System.ComponentModel.IComponent
struct IComponent_t1000253244;
// System.ComponentModel.NestedContainer
struct NestedContainer_t4071050512;
// System.String
struct String_t;
// System.ComponentModel.IContainer
struct IContainer_t3025744548;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_NestedContainer4071050512.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.ComponentModel.NestedContainer/Site::.ctor(System.ComponentModel.IComponent,System.ComponentModel.NestedContainer,System.String)
extern "C"  void Site__ctor_m1924076883 (Site_t3290944021 * __this, Il2CppObject * ___component0, NestedContainer_t4071050512 * ___container1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.IComponent System.ComponentModel.NestedContainer/Site::get_Component()
extern "C"  Il2CppObject * Site_get_Component_m1248106239 (Site_t3290944021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.IContainer System.ComponentModel.NestedContainer/Site::get_Container()
extern "C"  Il2CppObject * Site_get_Container_m4092381823 (Site_t3290944021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.NestedContainer/Site::get_DesignMode()
extern "C"  bool Site_get_DesignMode_m1163147021 (Site_t3290944021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.NestedContainer/Site::get_Name()
extern "C"  String_t* Site_get_Name_m762696042 (Site_t3290944021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.NestedContainer/Site::set_Name(System.String)
extern "C"  void Site_set_Name_m1259186737 (Site_t3290944021 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.NestedContainer/Site::get_FullName()
extern "C"  String_t* Site_get_FullName_m3351477509 (Site_t3290944021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.NestedContainer/Site::GetService(System.Type)
extern "C"  Il2CppObject * Site_GetService_m2940673086 (Site_t3290944021 * __this, Type_t * ___service0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
