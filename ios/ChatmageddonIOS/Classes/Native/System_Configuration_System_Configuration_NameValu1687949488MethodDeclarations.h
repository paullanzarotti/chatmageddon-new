﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.NameValueConfigurationCollection
struct NameValueConfigurationCollection_t1687949488;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Configuration.NameValueConfigurationElement
struct NameValueConfigurationElement_t3040474424;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_NameValu3040474424.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Void System.Configuration.NameValueConfigurationCollection::.ctor()
extern "C"  void NameValueConfigurationCollection__ctor_m2809131243 (NameValueConfigurationCollection_t1687949488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationCollection::.cctor()
extern "C"  void NameValueConfigurationCollection__cctor_m1691930700 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Configuration.NameValueConfigurationCollection::get_AllKeys()
extern "C"  StringU5BU5D_t1642385972* NameValueConfigurationCollection_get_AllKeys_m4084859376 (NameValueConfigurationCollection_t1687949488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.NameValueConfigurationElement System.Configuration.NameValueConfigurationCollection::get_Item(System.String)
extern "C"  NameValueConfigurationElement_t3040474424 * NameValueConfigurationCollection_get_Item_m341835777 (NameValueConfigurationCollection_t1687949488 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationCollection::set_Item(System.String,System.Configuration.NameValueConfigurationElement)
extern "C"  void NameValueConfigurationCollection_set_Item_m2906595760 (NameValueConfigurationCollection_t1687949488 * __this, String_t* ___name0, NameValueConfigurationElement_t3040474424 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.NameValueConfigurationCollection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * NameValueConfigurationCollection_get_Properties_m2885066190 (NameValueConfigurationCollection_t1687949488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationCollection::Add(System.Configuration.NameValueConfigurationElement)
extern "C"  void NameValueConfigurationCollection_Add_m2977224169 (NameValueConfigurationCollection_t1687949488 * __this, NameValueConfigurationElement_t3040474424 * ___nameValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationCollection::Clear()
extern "C"  void NameValueConfigurationCollection_Clear_m1099177556 (NameValueConfigurationCollection_t1687949488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElement System.Configuration.NameValueConfigurationCollection::CreateNewElement()
extern "C"  ConfigurationElement_t1776195828 * NameValueConfigurationCollection_CreateNewElement_m3159434497 (NameValueConfigurationCollection_t1687949488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.NameValueConfigurationCollection::GetElementKey(System.Configuration.ConfigurationElement)
extern "C"  Il2CppObject * NameValueConfigurationCollection_GetElementKey_m1973074408 (NameValueConfigurationCollection_t1687949488 * __this, ConfigurationElement_t1776195828 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationCollection::Remove(System.Configuration.NameValueConfigurationElement)
extern "C"  void NameValueConfigurationCollection_Remove_m3168990006 (NameValueConfigurationCollection_t1687949488 * __this, NameValueConfigurationElement_t3040474424 * ___nameValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationCollection::Remove(System.String)
extern "C"  void NameValueConfigurationCollection_Remove_m2553536087 (NameValueConfigurationCollection_t1687949488 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
