﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Film_ColorPerfection
struct CameraFilterPack_Film_ColorPerfection_t1035791658;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Film_ColorPerfection::.ctor()
extern "C"  void CameraFilterPack_Film_ColorPerfection__ctor_m1323293779 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Film_ColorPerfection::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Film_ColorPerfection_get_material_m2884754346 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::Start()
extern "C"  void CameraFilterPack_Film_ColorPerfection_Start_m1044205039 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Film_ColorPerfection_OnRenderImage_m1187342391 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::OnValidate()
extern "C"  void CameraFilterPack_Film_ColorPerfection_OnValidate_m1661100066 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::Update()
extern "C"  void CameraFilterPack_Film_ColorPerfection_Update_m3076046392 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::OnDisable()
extern "C"  void CameraFilterPack_Film_ColorPerfection_OnDisable_m1291490550 (CameraFilterPack_Film_ColorPerfection_t1035791658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
