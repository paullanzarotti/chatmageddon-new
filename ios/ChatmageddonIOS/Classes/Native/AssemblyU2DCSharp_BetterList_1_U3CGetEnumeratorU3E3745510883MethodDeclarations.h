﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>
struct U3CGetEnumeratorU3Ec__Iterator0_t3745510883;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry3041229383.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m737776945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m737776945(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m737776945_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1567636987_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1567636987(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1567636987_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  FadeEntry_t3041229383  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m180255698_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m180255698(__this, method) ((  FadeEntry_t3041229383  (*) (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m180255698_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m972639715_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m972639715(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m972639715_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3049829212_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3049829212(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3049829212_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<TypewriterEffect/FadeEntry>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m3958249298_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m3958249298(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3745510883 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m3958249298_gshared)(__this, method)
