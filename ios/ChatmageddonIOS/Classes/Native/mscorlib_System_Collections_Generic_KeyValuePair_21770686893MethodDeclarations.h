﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Mine>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4138652330(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1770686893 *, String_t*, List_1_t2098562409 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Mine>>::get_Key()
#define KeyValuePair_2_get_Key_m902706773(__this, method) ((  String_t* (*) (KeyValuePair_2_t1770686893 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Mine>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3732379803(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1770686893 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Mine>>::get_Value()
#define KeyValuePair_2_get_Value_m1149518384(__this, method) ((  List_1_t2098562409 * (*) (KeyValuePair_2_t1770686893 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Mine>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2489020459(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1770686893 *, List_1_t2098562409 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Mine>>::ToString()
#define KeyValuePair_2_ToString_m1083857185(__this, method) ((  String_t* (*) (KeyValuePair_2_t1770686893 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
