﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// FriendManager
struct FriendManager_t36140827;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendManager/<DeclineFriendRequest>c__AnonStorey2
struct  U3CDeclineFriendRequestU3Ec__AnonStorey2_t1467099986  : public Il2CppObject
{
public:
	// Friend FriendManager/<DeclineFriendRequest>c__AnonStorey2::friend
	Friend_t3555014108 * ___friend_0;
	// FriendManager FriendManager/<DeclineFriendRequest>c__AnonStorey2::$this
	FriendManager_t36140827 * ___U24this_1;

public:
	inline static int32_t get_offset_of_friend_0() { return static_cast<int32_t>(offsetof(U3CDeclineFriendRequestU3Ec__AnonStorey2_t1467099986, ___friend_0)); }
	inline Friend_t3555014108 * get_friend_0() const { return ___friend_0; }
	inline Friend_t3555014108 ** get_address_of_friend_0() { return &___friend_0; }
	inline void set_friend_0(Friend_t3555014108 * value)
	{
		___friend_0 = value;
		Il2CppCodeGenWriteBarrier(&___friend_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDeclineFriendRequestU3Ec__AnonStorey2_t1467099986, ___U24this_1)); }
	inline FriendManager_t36140827 * get_U24this_1() const { return ___U24this_1; }
	inline FriendManager_t36140827 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FriendManager_t36140827 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
