﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemInfoController/OnPopulationComplete
struct OnPopulationComplete_t1301655353;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ItemInfoController/OnPopulationComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPopulationComplete__ctor_m878758620 (OnPopulationComplete_t1301655353 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController/OnPopulationComplete::Invoke()
extern "C"  void OnPopulationComplete_Invoke_m2900159358 (OnPopulationComplete_t1301655353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ItemInfoController/OnPopulationComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPopulationComplete_BeginInvoke_m4289869663 (OnPopulationComplete_t1301655353 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInfoController/OnPopulationComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnPopulationComplete_EndInvoke_m3099044690 (OnPopulationComplete_t1301655353 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
