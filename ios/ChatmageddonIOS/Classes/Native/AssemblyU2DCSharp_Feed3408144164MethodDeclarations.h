﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Feed
struct Feed_t3408144164;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void Feed::.ctor()
extern "C"  void Feed__ctor_m3203805317 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::InitFeed()
extern "C"  void Feed_InitFeed_m3446165509 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::OpenFeed()
extern "C"  void Feed_OpenFeed_m3443996255 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Feed::UpdateFeed()
extern "C"  Il2CppObject * Feed_UpdateFeed_m3441083346 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::PauseFeed()
extern "C"  void Feed_PauseFeed_m2282299893 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::CloseFeed()
extern "C"  void Feed_CloseFeed_m3385085237 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::GetFeedInfo()
extern "C"  void Feed_GetFeedInfo_m2681812927 (Feed_t3408144164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::ProcessFeedInfo(System.Collections.Hashtable)
extern "C"  void Feed_ProcessFeedInfo_m1800979426 (Feed_t3408144164 * __this, Hashtable_t909839986 * ___infoTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed::SetFeedPollTime(System.Single)
extern "C"  void Feed_SetFeedPollTime_m3938617064 (Feed_t3408144164 * __this, float ___newTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
