﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonoSingleton`1<System.Object>
struct MonoSingleton_1_t2440115015;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MonoSingleton`1<System.Object>::.ctor()
extern "C"  void MonoSingleton_1__ctor_m437210476_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method);
#define MonoSingleton_1__ctor_m437210476(__this, method) ((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<System.Object>::Awake()
extern "C"  void MonoSingleton_1_Awake_m2844725271_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method);
#define MonoSingleton_1_Awake_m2844725271(__this, method) ((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * MonoSingleton_1_get_Instance_m4076012193_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define MonoSingleton_1_get_Instance_m4076012193(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<System.Object>::IsAvailable()
extern "C"  bool MonoSingleton_1_IsAvailable_m527113581_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define MonoSingleton_1_IsAvailable_m527113581(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<System.Object>::ForceInstanceInit()
extern "C"  void MonoSingleton_1_ForceInstanceInit_m3222864930_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method);
#define MonoSingleton_1_ForceInstanceInit_m3222864930(__this, method) ((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<System.Object>::OnApplicationQuit()
extern "C"  void MonoSingleton_1_OnApplicationQuit_m750638130_gshared (MonoSingleton_1_t2440115015 * __this, const MethodInfo* method);
#define MonoSingleton_1_OnApplicationQuit_m750638130(__this, method) ((  void (*) (MonoSingleton_1_t2440115015 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<System.Object>::.cctor()
extern "C"  void MonoSingleton_1__cctor_m590081433_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define MonoSingleton_1__cctor_m590081433(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
