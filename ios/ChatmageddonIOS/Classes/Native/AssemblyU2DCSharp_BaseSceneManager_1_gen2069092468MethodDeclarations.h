﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<HomeSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m741845222(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m3153036579(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::Start()
#define BaseSceneManager_1_Start_m2565162722(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m662740676(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m1760234956(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m37157360(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<HomeSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m1875351150(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m3014212821(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<HomeSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m3360879782(__this, method) ((  void (*) (BaseSceneManager_1_t2069092468 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
