﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Regular
struct CameraFilterPack_Blur_Regular_t3592058821;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Regular::.ctor()
extern "C"  void CameraFilterPack_Blur_Regular__ctor_m3221555162 (CameraFilterPack_Blur_Regular_t3592058821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Regular::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Regular_get_material_m1987708193 (CameraFilterPack_Blur_Regular_t3592058821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::Start()
extern "C"  void CameraFilterPack_Blur_Regular_Start_m3959268106 (CameraFilterPack_Blur_Regular_t3592058821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Regular_OnRenderImage_m362450354 (CameraFilterPack_Blur_Regular_t3592058821 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::OnValidate()
extern "C"  void CameraFilterPack_Blur_Regular_OnValidate_m3512762331 (CameraFilterPack_Blur_Regular_t3592058821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::Update()
extern "C"  void CameraFilterPack_Blur_Regular_Update_m3230109933 (CameraFilterPack_Blur_Regular_t3592058821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::OnDisable()
extern "C"  void CameraFilterPack_Blur_Regular_OnDisable_m413113697 (CameraFilterPack_Blur_Regular_t3592058821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
