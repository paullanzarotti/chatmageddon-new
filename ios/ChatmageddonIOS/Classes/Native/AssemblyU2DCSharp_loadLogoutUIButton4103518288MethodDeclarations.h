﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// loadLogoutUIButton
struct loadLogoutUIButton_t4103518288;

#include "codegen/il2cpp-codegen.h"

// System.Void loadLogoutUIButton::.ctor()
extern "C"  void loadLogoutUIButton__ctor_m999326169 (loadLogoutUIButton_t4103518288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void loadLogoutUIButton::OnButtonClick()
extern "C"  void loadLogoutUIButton_OnButtonClick_m2636730168 (loadLogoutUIButton_t4103518288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
