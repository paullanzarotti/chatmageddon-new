﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Vision_Psycho
struct CameraFilterPack_Vision_Psycho_t3419713482;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Vision_Psycho::.ctor()
extern "C"  void CameraFilterPack_Vision_Psycho__ctor_m4107539261 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Psycho::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Vision_Psycho_get_material_m2325631972 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::Start()
extern "C"  void CameraFilterPack_Vision_Psycho_Start_m19859253 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Vision_Psycho_OnRenderImage_m853157181 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::OnValidate()
extern "C"  void CameraFilterPack_Vision_Psycho_OnValidate_m1555138974 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::Update()
extern "C"  void CameraFilterPack_Vision_Psycho_Update_m3462052428 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::OnDisable()
extern "C"  void CameraFilterPack_Vision_Psycho_OnDisable_m2200322738 (CameraFilterPack_Vision_Psycho_t3419713482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
