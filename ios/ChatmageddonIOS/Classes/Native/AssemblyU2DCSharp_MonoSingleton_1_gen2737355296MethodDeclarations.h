﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<FullScreenLoadingIndicator>::.ctor()
#define MonoSingleton_1__ctor_m2707273480(__this, method) ((  void (*) (MonoSingleton_1_t2737355296 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<FullScreenLoadingIndicator>::Awake()
#define MonoSingleton_1_Awake_m260693281(__this, method) ((  void (*) (MonoSingleton_1_t2737355296 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<FullScreenLoadingIndicator>::get_Instance()
#define MonoSingleton_1_get_Instance_m3688832211(__this /* static, unused */, method) ((  FullScreenLoadingIndicator_t2986689576 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<FullScreenLoadingIndicator>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2453576911(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<FullScreenLoadingIndicator>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3911321578(__this, method) ((  void (*) (MonoSingleton_1_t2737355296 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<FullScreenLoadingIndicator>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m4026236694(__this, method) ((  void (*) (MonoSingleton_1_t2737355296 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<FullScreenLoadingIndicator>::.cctor()
#define MonoSingleton_1__cctor_m740893703(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
