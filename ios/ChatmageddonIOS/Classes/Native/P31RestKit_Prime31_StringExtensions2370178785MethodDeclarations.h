﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Collections.Generic.Dictionary`2<System.String,System.String> Prime31.StringExtensions::parseQueryString(System.String)
extern "C"  Dictionary_2_t3943999495 * StringExtensions_parseQueryString_m1606929507 (Il2CppObject * __this /* static, unused */, String_t* ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
