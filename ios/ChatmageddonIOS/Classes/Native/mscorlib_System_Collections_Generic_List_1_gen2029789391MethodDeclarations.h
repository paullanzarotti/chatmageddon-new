﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PlayerProfileNavScreen>
struct List_1_t2029789391;
// System.Collections.Generic.IEnumerable`1<PlayerProfileNavScreen>
struct IEnumerable_1_t2952795304;
// PlayerProfileNavScreen[]
struct PlayerProfileNavScreenU5BU5D_t3066215346;
// System.Collections.Generic.IEnumerator`1<PlayerProfileNavScreen>
struct IEnumerator_1_t136192086;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<PlayerProfileNavScreen>
struct ICollection_1_t3612743564;
// System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>
struct ReadOnlyCollection_1_t2846453951;
// System.Predicate`1<PlayerProfileNavScreen>
struct Predicate_1_t1103638374;
// System.Action`1<PlayerProfileNavScreen>
struct Action_1_t2462467641;
// System.Collections.Generic.IComparer`1<PlayerProfileNavScreen>
struct IComparer_1_t615131381;
// System.Comparison`1<PlayerProfileNavScreen>
struct Comparison_1_t3922407110;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1564519065.h"

// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::.ctor()
extern "C"  void List_1__ctor_m2388971747_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1__ctor_m2388971747(__this, method) ((  void (*) (List_1_t2029789391 *, const MethodInfo*))List_1__ctor_m2388971747_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m867092250_gshared (List_1_t2029789391 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m867092250(__this, ___collection0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m867092250_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1376370256_gshared (List_1_t2029789391 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1376370256(__this, ___capacity0, method) ((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1__ctor_m1376370256_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1917472892_gshared (List_1_t2029789391 * __this, PlayerProfileNavScreenU5BU5D_t3066215346* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1917472892(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2029789391 *, PlayerProfileNavScreenU5BU5D_t3066215346*, int32_t, const MethodInfo*))List_1__ctor_m1917472892_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::.cctor()
extern "C"  void List_1__cctor_m3373081408_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3373081408(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3373081408_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m778873643_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m778873643(__this, method) ((  Il2CppObject* (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m778873643_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2674160895_gshared (List_1_t2029789391 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2674160895(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2029789391 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2674160895_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3983207862_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3983207862(__this, method) ((  Il2CppObject * (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3983207862_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m244756719_gshared (List_1_t2029789391 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m244756719(__this, ___item0, method) ((  int32_t (*) (List_1_t2029789391 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m244756719_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2265384295_gshared (List_1_t2029789391 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2265384295(__this, ___item0, method) ((  bool (*) (List_1_t2029789391 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2265384295_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1699865145_gshared (List_1_t2029789391 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1699865145(__this, ___item0, method) ((  int32_t (*) (List_1_t2029789391 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1699865145_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2514767036_gshared (List_1_t2029789391 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2514767036(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2029789391 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2514767036_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1590166466_gshared (List_1_t2029789391 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1590166466(__this, ___item0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1590166466_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2081891010_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2081891010(__this, method) ((  bool (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2081891010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2347498983_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2347498983(__this, method) ((  bool (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2347498983_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3044003363_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3044003363(__this, method) ((  Il2CppObject * (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3044003363_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2609199704_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2609199704(__this, method) ((  bool (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2609199704_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2551805067_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2551805067(__this, method) ((  bool (*) (List_1_t2029789391 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2551805067_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3912423078_gshared (List_1_t2029789391 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3912423078(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3912423078_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3209541717_gshared (List_1_t2029789391 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3209541717(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2029789391 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3209541717_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Add(T)
extern "C"  void List_1_Add_m2815435128_gshared (List_1_t2029789391 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2815435128(__this, ___item0, method) ((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_Add_m2815435128_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m799079267_gshared (List_1_t2029789391 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m799079267(__this, ___newCount0, method) ((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m799079267_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1422499616_gshared (List_1_t2029789391 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1422499616(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2029789391 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1422499616_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3312277459_gshared (List_1_t2029789391 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3312277459(__this, ___collection0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3312277459_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4113146323_gshared (List_1_t2029789391 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4113146323(__this, ___enumerable0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4113146323_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3908061046_gshared (List_1_t2029789391 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3908061046(__this, ___collection0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3908061046_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PlayerProfileNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2846453951 * List_1_AsReadOnly_m1505627139_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1505627139(__this, method) ((  ReadOnlyCollection_1_t2846453951 * (*) (List_1_t2029789391 *, const MethodInfo*))List_1_AsReadOnly_m1505627139_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Clear()
extern "C"  void List_1_Clear_m4085087148_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_Clear_m4085087148(__this, method) ((  void (*) (List_1_t2029789391 *, const MethodInfo*))List_1_Clear_m4085087148_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m2510970222_gshared (List_1_t2029789391 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2510970222(__this, ___item0, method) ((  bool (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_Contains_m2510970222_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1242253059_gshared (List_1_t2029789391 * __this, PlayerProfileNavScreenU5BU5D_t3066215346* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1242253059(__this, ___array0, method) ((  void (*) (List_1_t2029789391 *, PlayerProfileNavScreenU5BU5D_t3066215346*, const MethodInfo*))List_1_CopyTo_m1242253059_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3404189860_gshared (List_1_t2029789391 * __this, PlayerProfileNavScreenU5BU5D_t3066215346* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3404189860(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2029789391 *, PlayerProfileNavScreenU5BU5D_t3066215346*, int32_t, const MethodInfo*))List_1_CopyTo_m3404189860_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m1690889894_gshared (List_1_t2029789391 * __this, int32_t ___index0, PlayerProfileNavScreenU5BU5D_t3066215346* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m1690889894(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t2029789391 *, int32_t, PlayerProfileNavScreenU5BU5D_t3066215346*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1690889894_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m3060700138_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_Exists_m3060700138(__this, ___match0, method) ((  bool (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_Exists_m3060700138_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<PlayerProfileNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m3327949594_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_Find_m3327949594(__this, ___match0, method) ((  int32_t (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_Find_m3327949594_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1908740791_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1908740791(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1103638374 *, const MethodInfo*))List_1_CheckMatch_m1908740791_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<PlayerProfileNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t2029789391 * List_1_FindAll_m97653415_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m97653415(__this, ___match0, method) ((  List_1_t2029789391 * (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_FindAll_m97653415_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<PlayerProfileNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t2029789391 * List_1_FindAllStackBits_m3533461309_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3533461309(__this, ___match0, method) ((  List_1_t2029789391 * (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_FindAllStackBits_m3533461309_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<PlayerProfileNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t2029789391 * List_1_FindAllList_m2370377757_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2370377757(__this, ___match0, method) ((  List_1_t2029789391 * (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_FindAllList_m2370377757_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m4000854119_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m4000854119(__this, ___match0, method) ((  int32_t (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_FindIndex_m4000854119_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3309908228_gshared (List_1_t2029789391 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1103638374 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3309908228(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2029789391 *, int32_t, int32_t, Predicate_1_t1103638374 *, const MethodInfo*))List_1_GetIndex_m3309908228_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2872562701_gshared (List_1_t2029789391 * __this, Action_1_t2462467641 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2872562701(__this, ___action0, method) ((  void (*) (List_1_t2029789391 *, Action_1_t2462467641 *, const MethodInfo*))List_1_ForEach_m2872562701_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PlayerProfileNavScreen>::GetEnumerator()
extern "C"  Enumerator_t1564519065  List_1_GetEnumerator_m4118960659_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4118960659(__this, method) ((  Enumerator_t1564519065  (*) (List_1_t2029789391 *, const MethodInfo*))List_1_GetEnumerator_m4118960659_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2753160402_gshared (List_1_t2029789391 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2753160402(__this, ___item0, method) ((  int32_t (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_IndexOf_m2753160402_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1755950563_gshared (List_1_t2029789391 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1755950563(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2029789391 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1755950563_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m659108788_gshared (List_1_t2029789391 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m659108788(__this, ___index0, method) ((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_CheckIndex_m659108788_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3897235985_gshared (List_1_t2029789391 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3897235985(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2029789391 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m3897235985_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m805583586_gshared (List_1_t2029789391 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m805583586(__this, ___collection0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m805583586_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PlayerProfileNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m2180617977_gshared (List_1_t2029789391 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2180617977(__this, ___item0, method) ((  bool (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_Remove_m2180617977_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1305810143_gshared (List_1_t2029789391 * __this, Predicate_1_t1103638374 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1305810143(__this, ___match0, method) ((  int32_t (*) (List_1_t2029789391 *, Predicate_1_t1103638374 *, const MethodInfo*))List_1_RemoveAll_m1305810143_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m309823861_gshared (List_1_t2029789391 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m309823861(__this, ___index0, method) ((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_RemoveAt_m309823861_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3573261056_gshared (List_1_t2029789391 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3573261056(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2029789391 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3573261056_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m446333403_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_Reverse_m446333403(__this, method) ((  void (*) (List_1_t2029789391 *, const MethodInfo*))List_1_Reverse_m446333403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Sort()
extern "C"  void List_1_Sort_m616972769_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_Sort_m616972769(__this, method) ((  void (*) (List_1_t2029789391 *, const MethodInfo*))List_1_Sort_m616972769_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3882656821_gshared (List_1_t2029789391 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3882656821(__this, ___comparer0, method) ((  void (*) (List_1_t2029789391 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3882656821_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2134991496_gshared (List_1_t2029789391 * __this, Comparison_1_t3922407110 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2134991496(__this, ___comparison0, method) ((  void (*) (List_1_t2029789391 *, Comparison_1_t3922407110 *, const MethodInfo*))List_1_Sort_m2134991496_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PlayerProfileNavScreen>::ToArray()
extern "C"  PlayerProfileNavScreenU5BU5D_t3066215346* List_1_ToArray_m786890286_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_ToArray_m786890286(__this, method) ((  PlayerProfileNavScreenU5BU5D_t3066215346* (*) (List_1_t2029789391 *, const MethodInfo*))List_1_ToArray_m786890286_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1847838790_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1847838790(__this, method) ((  void (*) (List_1_t2029789391 *, const MethodInfo*))List_1_TrimExcess_m1847838790_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1594204912_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1594204912(__this, method) ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))List_1_get_Capacity_m1594204912_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1005462291_gshared (List_1_t2029789391 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1005462291(__this, ___value0, method) ((  void (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1005462291_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PlayerProfileNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m1517584239_gshared (List_1_t2029789391 * __this, const MethodInfo* method);
#define List_1_get_Count_m1517584239(__this, method) ((  int32_t (*) (List_1_t2029789391 *, const MethodInfo*))List_1_get_Count_m1517584239_gshared)(__this, method)
// T System.Collections.Generic.List`1<PlayerProfileNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m661523057_gshared (List_1_t2029789391 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m661523057(__this, ___index0, method) ((  int32_t (*) (List_1_t2029789391 *, int32_t, const MethodInfo*))List_1_get_Item_m661523057_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayerProfileNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1115664454_gshared (List_1_t2029789391 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1115664454(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2029789391 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1115664454_gshared)(__this, ___index0, ___value1, method)
