﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackListItem3405986821.h"
#include "AssemblyU2DCSharp_AttackHomeInnerNavScreen1310676258.h"
#include "AssemblyU2DCSharp_AttackHomeOuterNavScreen2515452089.h"
#include "AssemblyU2DCSharp_AttackOuterPhoneNumberButton2666821364.h"
#include "AssemblyU2DCSharp_AttackHomeWorldNavScreen3820258470.h"
#include "AssemblyU2DCSharp_AttackInventoryBuyButton1923642124.h"
#include "AssemblyU2DCSharp_AttackInventoryNavScreen33214553.h"
#include "AssemblyU2DCSharp_AttackInventoryNavScreen_U3CLoadD193882483.h"
#include "AssemblyU2DCSharp_MissileInfoButton2682635900.h"
#include "AssemblyU2DCSharp_MissileSelectionSwipe4045384104.h"
#include "AssemblyU2DCSharp_MissileSwipeController2686719534.h"
#include "AssemblyU2DCSharp_SelectMissileButton3959346244.h"
#include "AssemblyU2DCSharp_AttackReviewNavScreen173504869.h"
#include "AssemblyU2DCSharp_BuyActiveMissileButton2138134254.h"
#include "AssemblyU2DCSharp_ReviewItemLoader43765888.h"
#include "AssemblyU2DCSharp_ReviewTargetLoader163570306.h"
#include "AssemblyU2DCSharp_AttackSearchButton3089589276.h"
#include "AssemblyU2DCSharp_AttackSearchFriendsNavScreen2732178946.h"
#include "AssemblyU2DCSharp_AttackSearchGlobalNavScreen581792468.h"
#include "AssemblyU2DCSharp_AttackSearchInput1258707860.h"
#include "AssemblyU2DCSharp_AttackSearchNavScreen457319073.h"
#include "AssemblyU2DCSharp_AttackSearchNavigateForwardsButt3074088129.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "AssemblyU2DCSharp_AttackSearchNavigationController4133786906.h"
#include "AssemblyU2DCSharp_AttackGameUIScaler784344924.h"
#include "AssemblyU2DCSharp_AttackHomeUIScaler339922071.h"
#include "AssemblyU2DCSharp_AttackInventoryUIScaler3118091758.h"
#include "AssemblyU2DCSharp_AttackReviewUIScaler2473008382.h"
#include "AssemblyU2DCSharp_AttackSearchUIScaler3747490330.h"
#include "AssemblyU2DCSharp_AttackUIScaler1212281222.h"
#include "AssemblyU2DCSharp_TargetListItemUIScaler1612238308.h"
#include "AssemblyU2DCSharp_HomeCenterUIManager2267606293.h"
#include "AssemblyU2DCSharp_HomeCenterUIManager_U3CWaitToFade289852963.h"
#include "AssemblyU2DCSharp_HomeCenterUIManager_U3CWaitToFad2744774187.h"
#include "AssemblyU2DCSharp_HomeCenterUIScaler3067528588.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "AssemblyU2DCSharp_FriendHomeNavigationController1091974581.h"
#include "AssemblyU2DCSharp_FriendListType2635913116.h"
#include "AssemblyU2DCSharp_FriendManager36140827.h"
#include "AssemblyU2DCSharp_FriendManager_U3CSendFriendReque1266135576.h"
#include "AssemblyU2DCSharp_FriendManager_U3CAcceptFriendReq1248635307.h"
#include "AssemblyU2DCSharp_FriendManager_U3CDeclineFriendRe1467099986.h"
#include "AssemblyU2DCSharp_FriendSearchButton1070893876.h"
#include "AssemblyU2DCSharp_FriendSearchInput390073340.h"
#include "AssemblyU2DCSharp_FriendsAcceptButton784515205.h"
#include "AssemblyU2DCSharp_FriendsAttackButton1040921037.h"
#include "AssemblyU2DCSharp_FriendsChatButton1487098543.h"
#include "AssemblyU2DCSharp_FriendsDeclineButton2192164397.h"
#include "AssemblyU2DCSharp_FriendsFriendButton4044142171.h"
#include "AssemblyU2DCSharp_FriendsHomeNavScreen1258257937.h"
#include "AssemblyU2DCSharp_FriendsHomeNaviagetForwardButton3956477812.h"
#include "AssemblyU2DCSharp_FriendsILP207575760.h"
#include "AssemblyU2DCSharp_FriendsInnerNavScreen2659475988.h"
#include "AssemblyU2DCSharp_FriendsListItem521220246.h"
#include "AssemblyU2DCSharp_FriendsNavigateBackwardsButton1799964784.h"
#include "AssemblyU2DCSharp_FriendsNavigateForwardsButton433492960.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "AssemblyU2DCSharp_FriendsNavigationController702141233.h"
#include "AssemblyU2DCSharp_FriendsOtherNavScreen264443870.h"
#include "AssemblyU2DCSharp_FriendsOuterNavScreen1899896515.h"
#include "AssemblyU2DCSharp_FriendsSearchNavScreen2437737290.h"
#include "AssemblyU2DCSharp_FriendsSearchNavigateForwardButt2162038683.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "AssemblyU2DCSharp_FriendsSearchNavigationControlle1013932095.h"
#include "AssemblyU2DCSharp_CloseMultiPhoneNumberUI914254952.h"
#include "AssemblyU2DCSharp_MultiPhoneNumberILP3109982769.h"
#include "AssemblyU2DCSharp_MultiPhoneNumberListItem177127779.h"
#include "AssemblyU2DCSharp_OtherFriend3976068630.h"
#include "AssemblyU2DCSharp_OtherFriendsILP671720694.h"
#include "AssemblyU2DCSharp_OtherFriendsListItem1916582308.h"
#include "AssemblyU2DCSharp_OuterPhoneNumberButton2854054552.h"
#include "AssemblyU2DCSharp_PendingFriendIndicator2589313544.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "AssemblyU2DCSharp_ContactsNaviagationController22981102.h"
#include "AssemblyU2DCSharp_ContactsPhoneNumberManager4168190569.h"
#include "AssemblyU2DCSharp_EnterPhoneNumberButton3549119.h"
#include "AssemblyU2DCSharp_PNVerificationCodeInput3714898508.h"
#include "AssemblyU2DCSharp_PNVerificationNavScreen710347658.h"
#include "AssemblyU2DCSharp_PNVerifyCodeButton868136506.h"
#include "AssemblyU2DCSharp_PNYourNumberNavScreen1723852807.h"
#include "AssemblyU2DCSharp_PhoneNumberContinueButton1818508624.h"
#include "AssemblyU2DCSharp_PhoneNumberContinueButton_U3COnB1880716841.h"
#include "AssemblyU2DCSharp_PhoneNumberNaviagteForwardButton3286637755.h"
#include "AssemblyU2DCSharp_PhoneNumberNavigateBackButton3971219339.h"
#include "AssemblyU2DCSharp_PhoneNumberResendCode1736900895.h"
#include "AssemblyU2DCSharp_PhoneNumberUIScaler4195264923.h"
#include "AssemblyU2DCSharp_YourNumberUIScaler1339804644.h"
#include "AssemblyU2DCSharp_SearchFriendsNavScreen1265140554.h"
#include "AssemblyU2DCSharp_SearchGlobalNavScreen1709594696.h"
#include "AssemblyU2DCSharp_FriendsHomeUIScaler2886449736.h"
#include "AssemblyU2DCSharp_FriendsListItemUIScaler3047710612.h"
#include "AssemblyU2DCSharp_FriendsSearchUIScaler4167788223.h"
#include "AssemblyU2DCSharp_FriendsUIScaler2202545073.h"
#include "AssemblyU2DCSharp_OtherFriendListItemUIScaler173824305.h"
#include "AssemblyU2DCSharp_ChatButton3979308854.h"
#include "AssemblyU2DCSharp_FuelBarController2314862973.h"
#include "AssemblyU2DCSharp_FuelButton3326934984.h"
#include "AssemblyU2DCSharp_FuelButton_U3CWaitToAllowToastU33463864080.h"
#include "AssemblyU2DCSharp_HeaderManager49185160.h"
#include "AssemblyU2DCSharp_HeaderUIScaler1298375071.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof (AttackListItem_t3405986821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5600[24] = 
{
	AttackListItem_t3405986821::get_offset_of_friend_10(),
	AttackListItem_t3405986821::get_offset_of_listPopulator_11(),
	AttackListItem_t3405986821::get_offset_of_nameLabel_12(),
	AttackListItem_t3405986821::get_offset_of_avatarUpdater_13(),
	AttackListItem_t3405986821::get_offset_of_profileButton_14(),
	AttackListItem_t3405986821::get_offset_of_stealthTimeCalc_15(),
	AttackListItem_t3405986821::get_offset_of_knockoutCalc_16(),
	AttackListItem_t3405986821::get_offset_of_chatButton_17(),
	AttackListItem_t3405986821::get_offset_of_attackButton_18(),
	AttackListItem_t3405986821::get_offset_of_globalFriendButton_19(),
	AttackListItem_t3405986821::get_offset_of_requestSentLabel_20(),
	AttackListItem_t3405986821::get_offset_of_acceptFriendButton_21(),
	AttackListItem_t3405986821::get_offset_of_declineFriendButton_22(),
	AttackListItem_t3405986821::get_offset_of_actionLabel_23(),
	AttackListItem_t3405986821::get_offset_of_timeLabel_24(),
	AttackListItem_t3405986821::get_offset_of_activeColour_25(),
	AttackListItem_t3405986821::get_offset_of_inactiveColour_26(),
	AttackListItem_t3405986821::get_offset_of_friendRequestColour_27(),
	AttackListItem_t3405986821::get_offset_of_scrollView_28(),
	AttackListItem_t3405986821::get_offset_of_draggablePanel_29(),
	AttackListItem_t3405986821::get_offset_of_mTrans_30(),
	AttackListItem_t3405986821::get_offset_of_mScroll_31(),
	AttackListItem_t3405986821::get_offset_of_mAutoFind_32(),
	AttackListItem_t3405986821::get_offset_of_mStarted_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof (AttackHomeInnerNavScreen_t1310676258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5601[4] = 
{
	AttackHomeInnerNavScreen_t1310676258::get_offset_of_hasFriendsUI_3(),
	AttackHomeInnerNavScreen_t1310676258::get_offset_of_hasNoFriendsUI_4(),
	AttackHomeInnerNavScreen_t1310676258::get_offset_of_innerFriendsList_5(),
	AttackHomeInnerNavScreen_t1310676258::get_offset_of_UIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof (AttackHomeOuterNavScreen_t2515452089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5602[5] = 
{
	AttackHomeOuterNavScreen_t2515452089::get_offset_of_hasFriendsUI_3(),
	AttackHomeOuterNavScreen_t2515452089::get_offset_of_hasNoFriendsUI_4(),
	AttackHomeOuterNavScreen_t2515452089::get_offset_of_outerFriendsList_5(),
	AttackHomeOuterNavScreen_t2515452089::get_offset_of_phoneNumButton_6(),
	AttackHomeOuterNavScreen_t2515452089::get_offset_of_UIclosing_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof (AttackOuterPhoneNumberButton_t2666821364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5603[1] = 
{
	AttackOuterPhoneNumberButton_t2666821364::get_offset_of_phoneNumberlabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof (AttackHomeWorldNavScreen_t3820258470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5604[3] = 
{
	AttackHomeWorldNavScreen_t3820258470::get_offset_of_hasContactsUI_3(),
	AttackHomeWorldNavScreen_t3820258470::get_offset_of_noContactsUI_4(),
	AttackHomeWorldNavScreen_t3820258470::get_offset_of_UIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof (AttackInventoryBuyButton_t1923642124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5605[5] = 
{
	AttackInventoryBuyButton_t1923642124::get_offset_of_screen_5(),
	AttackInventoryBuyButton_t1923642124::get_offset_of_activeColour_6(),
	AttackInventoryBuyButton_t1923642124::get_offset_of_lockedColour_7(),
	AttackInventoryBuyButton_t1923642124::get_offset_of_inactiveColour_8(),
	AttackInventoryBuyButton_t1923642124::get_offset_of_buyLabel_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof (AttackInventoryNavScreen_t33214553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5606[19] = 
{
	AttackInventoryNavScreen_t33214553::get_offset_of_inventoryUIClosing_3(),
	AttackInventoryNavScreen_t33214553::get_offset_of_currentItemIndex_4(),
	AttackInventoryNavScreen_t33214553::get_offset_of_missileItems_5(),
	AttackInventoryNavScreen_t33214553::get_offset_of_infoObject_6(),
	AttackInventoryNavScreen_t33214553::get_offset_of_itemObject_7(),
	AttackInventoryNavScreen_t33214553::get_offset_of_lockedBackground_8(),
	AttackInventoryNavScreen_t33214553::get_offset_of_lockedLabel_9(),
	AttackInventoryNavScreen_t33214553::get_offset_of_infoButton_10(),
	AttackInventoryNavScreen_t33214553::get_offset_of_infoController_11(),
	AttackInventoryNavScreen_t33214553::get_offset_of_swipeController_12(),
	AttackInventoryNavScreen_t33214553::get_offset_of_itemNameLabel_13(),
	AttackInventoryNavScreen_t33214553::get_offset_of_itemAmountLabel_14(),
	AttackInventoryNavScreen_t33214553::get_offset_of_itemDamageLabel_15(),
	AttackInventoryNavScreen_t33214553::get_offset_of_infinitySprite_16(),
	AttackInventoryNavScreen_t33214553::get_offset_of_itemLight_17(),
	AttackInventoryNavScreen_t33214553::get_offset_of_buyButton_18(),
	AttackInventoryNavScreen_t33214553::get_offset_of_selectMissileButton_19(),
	AttackInventoryNavScreen_t33214553::get_offset_of_showingInfo_20(),
	AttackInventoryNavScreen_t33214553::get_offset_of_firstTimeLoading_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof (U3CLoadDefaultModelU3Ec__Iterator0_t193882483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5607[4] = 
{
	U3CLoadDefaultModelU3Ec__Iterator0_t193882483::get_offset_of_U24this_0(),
	U3CLoadDefaultModelU3Ec__Iterator0_t193882483::get_offset_of_U24current_1(),
	U3CLoadDefaultModelU3Ec__Iterator0_t193882483::get_offset_of_U24disposing_2(),
	U3CLoadDefaultModelU3Ec__Iterator0_t193882483::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof (MissileInfoButton_t2682635900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5608[1] = 
{
	MissileInfoButton_t2682635900::get_offset_of_screen_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof (MissileSelectionSwipe_t4045384104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5609[1] = 
{
	MissileSelectionSwipe_t4045384104::get_offset_of_screen_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof (MissileSwipeController_t2686719534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5610[1] = 
{
	MissileSwipeController_t2686719534::get_offset_of_screen_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof (SelectMissileButton_t3959346244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5611[6] = 
{
	SelectMissileButton_t3959346244::get_offset_of_screen_5(),
	SelectMissileButton_t3959346244::get_offset_of_activeColour_6(),
	SelectMissileButton_t3959346244::get_offset_of_lockedColour_7(),
	SelectMissileButton_t3959346244::get_offset_of_inactiveColour_8(),
	SelectMissileButton_t3959346244::get_offset_of_selectLabel_9(),
	SelectMissileButton_t3959346244::get_offset_of_buyFuel_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof (AttackReviewNavScreen_t173504869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5612[7] = 
{
	AttackReviewNavScreen_t173504869::get_offset_of_itemLoader_3(),
	AttackReviewNavScreen_t173504869::get_offset_of_targetLoader_4(),
	AttackReviewNavScreen_t173504869::get_offset_of_itemLight_5(),
	AttackReviewNavScreen_t173504869::get_offset_of_buyButton_6(),
	AttackReviewNavScreen_t173504869::get_offset_of_aimScaleTween_7(),
	AttackReviewNavScreen_t173504869::get_offset_of_aimRotationTween_8(),
	AttackReviewNavScreen_t173504869::get_offset_of_reviewUIClosing_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof (BuyActiveMissileButton_t2138134254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5613[4] = 
{
	BuyActiveMissileButton_t2138134254::get_offset_of_screen_5(),
	BuyActiveMissileButton_t2138134254::get_offset_of_activeColour_6(),
	BuyActiveMissileButton_t2138134254::get_offset_of_inactiveColour_7(),
	BuyActiveMissileButton_t2138134254::get_offset_of_buyLabel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof (ReviewItemLoader_t43765888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5614[3] = 
{
	ReviewItemLoader_t43765888::get_offset_of_itemNameLabel_2(),
	ReviewItemLoader_t43765888::get_offset_of_m_DisplayItemObject_3(),
	ReviewItemLoader_t43765888::get_offset_of_currentModel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof (ReviewTargetLoader_t163570306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5615[5] = 
{
	ReviewTargetLoader_t163570306::get_offset_of_firstNameLabel_2(),
	ReviewTargetLoader_t163570306::get_offset_of_lastNameLabel_3(),
	ReviewTargetLoader_t163570306::get_offset_of_avatarUpdater_4(),
	ReviewTargetLoader_t163570306::get_offset_of_firstStrikeObject_5(),
	ReviewTargetLoader_t163570306::get_offset_of_bonusAmountLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof (AttackSearchButton_t3089589276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5616[1] = 
{
	AttackSearchButton_t3089589276::get_offset_of_searchInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof (AttackSearchFriendsNavScreen_t2732178946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5617[1] = 
{
	AttackSearchFriendsNavScreen_t2732178946::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof (AttackSearchGlobalNavScreen_t581792468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5618[1] = 
{
	AttackSearchGlobalNavScreen_t581792468::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof (AttackSearchInput_t1258707860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5619[1] = 
{
	AttackSearchInput_t1258707860::get_offset_of_searchInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof (AttackSearchNavScreen_t457319073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5620[1] = 
{
	AttackSearchNavScreen_t457319073::get_offset_of_searchUIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof (AttackSearchNavigateForwardsButton_t3074088129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5621[2] = 
{
	AttackSearchNavigateForwardsButton_t3074088129::get_offset_of_nextScreen_5(),
	AttackSearchNavigateForwardsButton_t3074088129::get_offset_of_buttonLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof (AttackSearchNav_t4257884637)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5622[4] = 
{
	AttackSearchNav_t4257884637::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof (AttackSearchNavigationController_t4133786906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5623[7] = 
{
	AttackSearchNavigationController_t4133786906::get_offset_of_friendsTab_8(),
	AttackSearchNavigationController_t4133786906::get_offset_of_globalTab_9(),
	AttackSearchNavigationController_t4133786906::get_offset_of_friendsObject_10(),
	AttackSearchNavigationController_t4133786906::get_offset_of_globalObject_11(),
	AttackSearchNavigationController_t4133786906::get_offset_of_activePos_12(),
	AttackSearchNavigationController_t4133786906::get_offset_of_leftInactivePos_13(),
	AttackSearchNavigationController_t4133786906::get_offset_of_rightInactivePos_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof (AttackGameUIScaler_t784344924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5624[20] = 
{
	AttackGameUIScaler_t784344924::get_offset_of_tvBackground_14(),
	AttackGameUIScaler_t784344924::get_offset_of_gamePanel_15(),
	AttackGameUIScaler_t784344924::get_offset_of_pointsLabel_16(),
	AttackGameUIScaler_t784344924::get_offset_of_latLabel_17(),
	AttackGameUIScaler_t784344924::get_offset_of_longLabel_18(),
	AttackGameUIScaler_t784344924::get_offset_of_fireGuagePanel_19(),
	AttackGameUIScaler_t784344924::get_offset_of_fireBackground_20(),
	AttackGameUIScaler_t784344924::get_offset_of_fireGuage_21(),
	AttackGameUIScaler_t784344924::get_offset_of_aimButton_22(),
	AttackGameUIScaler_t784344924::get_offset_of_aimCollider_23(),
	AttackGameUIScaler_t784344924::get_offset_of_cancelButton_24(),
	AttackGameUIScaler_t784344924::get_offset_of_cancelLabel_25(),
	AttackGameUIScaler_t784344924::get_offset_of_cancelCollider_26(),
	AttackGameUIScaler_t784344924::get_offset_of_targetsButton_27(),
	AttackGameUIScaler_t784344924::get_offset_of_targetsLabel_28(),
	AttackGameUIScaler_t784344924::get_offset_of_targetsCollider_29(),
	AttackGameUIScaler_t784344924::get_offset_of_topHorizLine_30(),
	AttackGameUIScaler_t784344924::get_offset_of_aimLine_31(),
	AttackGameUIScaler_t784344924::get_offset_of_botHorizLine_32(),
	AttackGameUIScaler_t784344924::get_offset_of_launchAccuracy_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof (AttackHomeUIScaler_t339922071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5625[16] = 
{
	AttackHomeUIScaler_t339922071::get_offset_of_tabBackground_14(),
	AttackHomeUIScaler_t339922071::get_offset_of_bottomSeperator_15(),
	AttackHomeUIScaler_t339922071::get_offset_of_innerTab_16(),
	AttackHomeUIScaler_t339922071::get_offset_of_innerTabCollider_17(),
	AttackHomeUIScaler_t339922071::get_offset_of_outerTab_18(),
	AttackHomeUIScaler_t339922071::get_offset_of_outerTabCollider_19(),
	AttackHomeUIScaler_t339922071::get_offset_of_otherTab_20(),
	AttackHomeUIScaler_t339922071::get_offset_of_otherTabCollider_21(),
	AttackHomeUIScaler_t339922071::get_offset_of_leftSeperator_22(),
	AttackHomeUIScaler_t339922071::get_offset_of_rightSeperator_23(),
	AttackHomeUIScaler_t339922071::get_offset_of_innerPanel_24(),
	AttackHomeUIScaler_t339922071::get_offset_of_outerPanel_25(),
	AttackHomeUIScaler_t339922071::get_offset_of_otherPanel_26(),
	AttackHomeUIScaler_t339922071::get_offset_of_otherBonusBackground_27(),
	AttackHomeUIScaler_t339922071::get_offset_of_multiNumberBackground_28(),
	AttackHomeUIScaler_t339922071::get_offset_of_multiNumberCollider_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof (AttackInventoryUIScaler_t3118091758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5626[17] = 
{
	AttackInventoryUIScaler_t3118091758::get_offset_of_background_14(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_itemScaleMeasure_15(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_itemAmount_16(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_infinitySymbol_17(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_missileInfoButton_18(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_itemSwipeCollider_19(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_swipe_20(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_selectMissileButton_21(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_selectMissileCollider_22(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_buyMoreButton_23(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_buyMoreLabel_24(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_buyMoreCollider_25(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_targetsButton_26(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_targetsLabel_27(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_targetsCollider_28(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_topHorizLine_29(),
	AttackInventoryUIScaler_t3118091758::get_offset_of_botHorizLine_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof (AttackReviewUIScaler_t2473008382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5627[11] = 
{
	AttackReviewUIScaler_t2473008382::get_offset_of_itemScaleMeasure_14(),
	AttackReviewUIScaler_t2473008382::get_offset_of_launchButton_15(),
	AttackReviewUIScaler_t2473008382::get_offset_of_launchCollider_16(),
	AttackReviewUIScaler_t2473008382::get_offset_of_buyMoreButton_17(),
	AttackReviewUIScaler_t2473008382::get_offset_of_buyMoreLabel_18(),
	AttackReviewUIScaler_t2473008382::get_offset_of_buyMoreCollider_19(),
	AttackReviewUIScaler_t2473008382::get_offset_of_targetsButton_20(),
	AttackReviewUIScaler_t2473008382::get_offset_of_targetsLabel_21(),
	AttackReviewUIScaler_t2473008382::get_offset_of_targetsCollider_22(),
	AttackReviewUIScaler_t2473008382::get_offset_of_topHorizLine_23(),
	AttackReviewUIScaler_t2473008382::get_offset_of_botHorizLine_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof (AttackSearchUIScaler_t3747490330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5628[14] = 
{
	AttackSearchUIScaler_t3747490330::get_offset_of_tabBackground_14(),
	AttackSearchUIScaler_t3747490330::get_offset_of_bottomSeperator_15(),
	AttackSearchUIScaler_t3747490330::get_offset_of_friendTab_16(),
	AttackSearchUIScaler_t3747490330::get_offset_of_friendTabCollider_17(),
	AttackSearchUIScaler_t3747490330::get_offset_of_globalTab_18(),
	AttackSearchUIScaler_t3747490330::get_offset_of_globalTabCollider_19(),
	AttackSearchUIScaler_t3747490330::get_offset_of_friendPanel_20(),
	AttackSearchUIScaler_t3747490330::get_offset_of_globalPanel_21(),
	AttackSearchUIScaler_t3747490330::get_offset_of_searchCollider_22(),
	AttackSearchUIScaler_t3747490330::get_offset_of_searchLabel_23(),
	AttackSearchUIScaler_t3747490330::get_offset_of_searchButton_24(),
	AttackSearchUIScaler_t3747490330::get_offset_of_searchDivider_25(),
	AttackSearchUIScaler_t3747490330::get_offset_of_friendsNoResultsLabel_26(),
	AttackSearchUIScaler_t3747490330::get_offset_of_globalNoResultsLabel_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof (AttackUIScaler_t1212281222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5629[4] = 
{
	AttackUIScaler_t1212281222::get_offset_of_background_14(),
	AttackUIScaler_t1212281222::get_offset_of_titleSeperator_15(),
	AttackUIScaler_t1212281222::get_offset_of_navigateBackButton_16(),
	AttackUIScaler_t1212281222::get_offset_of_searchButton_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof (TargetListItemUIScaler_t1612238308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5630[7] = 
{
	TargetListItemUIScaler_t1612238308::get_offset_of_background_14(),
	TargetListItemUIScaler_t1612238308::get_offset_of_backgroundCollider_15(),
	TargetListItemUIScaler_t1612238308::get_offset_of_profilePicture_16(),
	TargetListItemUIScaler_t1612238308::get_offset_of_nameLabel_17(),
	TargetListItemUIScaler_t1612238308::get_offset_of_chatButton_18(),
	TargetListItemUIScaler_t1612238308::get_offset_of_attackButton_19(),
	TargetListItemUIScaler_t1612238308::get_offset_of_actionLabel_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof (HomeCenterUIManager_t2267606293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5631[9] = 
{
	HomeCenterUIManager_t2267606293::get_offset_of_radarAlpha_3(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapFade_4(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapViewLabel_5(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapViewTween_6(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapBlur_7(),
	HomeCenterUIManager_t2267606293::get_offset_of_radarOpening_8(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapOpening_9(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapViewOpening_10(),
	HomeCenterUIManager_t2267606293::get_offset_of_mapViewTweenRoutine_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof (U3CWaitToFadeU3Ec__Iterator0_t289852963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5632[4] = 
{
	U3CWaitToFadeU3Ec__Iterator0_t289852963::get_offset_of_U24this_0(),
	U3CWaitToFadeU3Ec__Iterator0_t289852963::get_offset_of_U24current_1(),
	U3CWaitToFadeU3Ec__Iterator0_t289852963::get_offset_of_U24disposing_2(),
	U3CWaitToFadeU3Ec__Iterator0_t289852963::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5633[4] = 
{
	U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187::get_offset_of_U24this_0(),
	U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187::get_offset_of_U24current_1(),
	U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187::get_offset_of_U24disposing_2(),
	U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { sizeof (HomeCenterUIScaler_t3067528588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5634[3] = 
{
	HomeCenterUIScaler_t3067528588::get_offset_of_topGradient_14(),
	HomeCenterUIScaler_t3067528588::get_offset_of_bottomGradient_15(),
	HomeCenterUIScaler_t3067528588::get_offset_of_mapFade_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof (FriendHomeNavScreen_t1599038296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5635[5] = 
{
	FriendHomeNavScreen_t1599038296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof (FriendHomeNavigationController_t1091974581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5636[9] = 
{
	FriendHomeNavigationController_t1091974581::get_offset_of_innerLabel_8(),
	FriendHomeNavigationController_t1091974581::get_offset_of_outerLabel_9(),
	FriendHomeNavigationController_t1091974581::get_offset_of_otherLabel_10(),
	FriendHomeNavigationController_t1091974581::get_offset_of_innerObject_11(),
	FriendHomeNavigationController_t1091974581::get_offset_of_outerObject_12(),
	FriendHomeNavigationController_t1091974581::get_offset_of_otherObject_13(),
	FriendHomeNavigationController_t1091974581::get_offset_of_activePos_14(),
	FriendHomeNavigationController_t1091974581::get_offset_of_leftInactivePos_15(),
	FriendHomeNavigationController_t1091974581::get_offset_of_rightInactivePos_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof (FriendListType_t2635913116)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5637[6] = 
{
	FriendListType_t2635913116::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof (FriendManager_t36140827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5638[11] = 
{
	FriendManager_t36140827::get_offset_of_globalSearchList_3(),
	FriendManager_t36140827::get_offset_of_friendsSearchList_4(),
	FriendManager_t36140827::get_offset_of_otherFriendsList_5(),
	FriendManager_t36140827::get_offset_of_globalUserSearchList_6(),
	FriendManager_t36140827::get_offset_of_globalListUpdated_7(),
	FriendManager_t36140827::get_offset_of_localUserSearchList_8(),
	FriendManager_t36140827::get_offset_of_localListUpdated_9(),
	FriendManager_t36140827::get_offset_of_innerListUpdated_10(),
	FriendManager_t36140827::get_offset_of_outerListUpdated_11(),
	FriendManager_t36140827::get_offset_of_otherListUpdated_12(),
	FriendManager_t36140827::get_offset_of_phoneNumberUI_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof (U3CSendFriendRequestU3Ec__AnonStorey0_t1266135576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5639[2] = 
{
	U3CSendFriendRequestU3Ec__AnonStorey0_t1266135576::get_offset_of_friend_0(),
	U3CSendFriendRequestU3Ec__AnonStorey0_t1266135576::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof (U3CAcceptFriendRequestU3Ec__AnonStorey1_t1248635307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5640[2] = 
{
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t1248635307::get_offset_of_friend_0(),
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t1248635307::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof (U3CDeclineFriendRequestU3Ec__AnonStorey2_t1467099986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5641[2] = 
{
	U3CDeclineFriendRequestU3Ec__AnonStorey2_t1467099986::get_offset_of_friend_0(),
	U3CDeclineFriendRequestU3Ec__AnonStorey2_t1467099986::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof (FriendSearchButton_t1070893876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5642[1] = 
{
	FriendSearchButton_t1070893876::get_offset_of_searchInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof (FriendSearchInput_t390073340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5643[1] = 
{
	FriendSearchInput_t390073340::get_offset_of_searchInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof (FriendsAcceptButton_t784515205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5644[1] = 
{
	FriendsAcceptButton_t784515205::get_offset_of_item_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof (FriendsAttackButton_t1040921037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5645[1] = 
{
	FriendsAttackButton_t1040921037::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof (FriendsChatButton_t1487098543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5646[1] = 
{
	FriendsChatButton_t1487098543::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof (FriendsDeclineButton_t2192164397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5647[1] = 
{
	FriendsDeclineButton_t2192164397::get_offset_of_item_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof (FriendsFriendButton_t4044142171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5648[1] = 
{
	FriendsFriendButton_t4044142171::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof (FriendsHomeNavScreen_t1258257937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5649[1] = 
{
	FriendsHomeNavScreen_t1258257937::get_offset_of_homeUIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof (FriendsHomeNaviagetForwardButton_t3956477812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5650[1] = 
{
	FriendsHomeNaviagetForwardButton_t3956477812::get_offset_of_nextScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof (FriendsILP_t207575760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5651[10] = 
{
	FriendsILP_t207575760::get_offset_of_noResultsLabel_33(),
	FriendsILP_t207575760::get_offset_of_maxNameLength_34(),
	FriendsILP_t207575760::get_offset_of_listLoaded_35(),
	FriendsILP_t207575760::get_offset_of_friendsArray_36(),
	FriendsILP_t207575760::get_offset_of_outerFriendList_37(),
	FriendsILP_t207575760::get_offset_of_searchList_38(),
	FriendsILP_t207575760::get_offset_of_globalSearchList_39(),
	FriendsILP_t207575760::get_offset_of_activeColor_40(),
	FriendsILP_t207575760::get_offset_of_inactiveColor_41(),
	FriendsILP_t207575760::get_offset_of_resetSearch_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof (FriendsInnerNavScreen_t2659475988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5652[4] = 
{
	FriendsInnerNavScreen_t2659475988::get_offset_of_hasFriendsUI_3(),
	FriendsInnerNavScreen_t2659475988::get_offset_of_hasNoFriendsUI_4(),
	FriendsInnerNavScreen_t2659475988::get_offset_of_innerFriendsList_5(),
	FriendsInnerNavScreen_t2659475988::get_offset_of_UIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof (FriendsListItem_t521220246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5653[24] = 
{
	FriendsListItem_t521220246::get_offset_of_friend_10(),
	FriendsListItem_t521220246::get_offset_of_listPopulator_11(),
	FriendsListItem_t521220246::get_offset_of_nameLabel_12(),
	FriendsListItem_t521220246::get_offset_of_avatarUpdater_13(),
	FriendsListItem_t521220246::get_offset_of_profileButton_14(),
	FriendsListItem_t521220246::get_offset_of_stealthTimeCalc_15(),
	FriendsListItem_t521220246::get_offset_of_knockoutCalc_16(),
	FriendsListItem_t521220246::get_offset_of_chatButton_17(),
	FriendsListItem_t521220246::get_offset_of_attackButton_18(),
	FriendsListItem_t521220246::get_offset_of_globalFriendButton_19(),
	FriendsListItem_t521220246::get_offset_of_requestSentLabel_20(),
	FriendsListItem_t521220246::get_offset_of_acceptFriendButton_21(),
	FriendsListItem_t521220246::get_offset_of_declineFriendButton_22(),
	FriendsListItem_t521220246::get_offset_of_actionLabel_23(),
	FriendsListItem_t521220246::get_offset_of_timeLabel_24(),
	FriendsListItem_t521220246::get_offset_of_activeColour_25(),
	FriendsListItem_t521220246::get_offset_of_inactiveColour_26(),
	FriendsListItem_t521220246::get_offset_of_friendRequestColour_27(),
	FriendsListItem_t521220246::get_offset_of_scrollView_28(),
	FriendsListItem_t521220246::get_offset_of_draggablePanel_29(),
	FriendsListItem_t521220246::get_offset_of_mTrans_30(),
	FriendsListItem_t521220246::get_offset_of_mScroll_31(),
	FriendsListItem_t521220246::get_offset_of_mAutoFind_32(),
	FriendsListItem_t521220246::get_offset_of_mStarted_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof (FriendsNavigateBackwardsButton_t1799964784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5654[2] = 
{
	FriendsNavigateBackwardsButton_t1799964784::get_offset_of_sfxToPlay_2(),
	FriendsNavigateBackwardsButton_t1799964784::get_offset_of_buttonSprite_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof (FriendsNavigateForwardsButton_t433492960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5655[4] = 
{
	FriendsNavigateForwardsButton_t433492960::get_offset_of_navScreen_5(),
	FriendsNavigateForwardsButton_t433492960::get_offset_of_activeColor_6(),
	FriendsNavigateForwardsButton_t433492960::get_offset_of_inActiveColor_7(),
	FriendsNavigateForwardsButton_t433492960::get_offset_of_buttonSprite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof (FriendNavScreen_t2175383453)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5656[4] = 
{
	FriendNavScreen_t2175383453::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof (FriendsNavigationController_t702141233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5657[8] = 
{
	FriendsNavigationController_t702141233::get_offset_of_titleLabel_8(),
	FriendsNavigationController_t702141233::get_offset_of_homeObject_9(),
	FriendsNavigationController_t702141233::get_offset_of_searchObject_10(),
	FriendsNavigationController_t702141233::get_offset_of_mainNavigateBackwardButton_11(),
	FriendsNavigationController_t702141233::get_offset_of_mainNavigateForwardButton_12(),
	FriendsNavigationController_t702141233::get_offset_of_activePos_13(),
	FriendsNavigationController_t702141233::get_offset_of_leftInactivePos_14(),
	FriendsNavigationController_t702141233::get_offset_of_rightInactivePos_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof (FriendsOtherNavScreen_t264443870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5658[3] = 
{
	FriendsOtherNavScreen_t264443870::get_offset_of_hasContactsUI_3(),
	FriendsOtherNavScreen_t264443870::get_offset_of_noContactsUI_4(),
	FriendsOtherNavScreen_t264443870::get_offset_of_UIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof (FriendsOuterNavScreen_t1899896515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5659[5] = 
{
	FriendsOuterNavScreen_t1899896515::get_offset_of_hasFriendsUI_3(),
	FriendsOuterNavScreen_t1899896515::get_offset_of_hasNoFriendsUI_4(),
	FriendsOuterNavScreen_t1899896515::get_offset_of_outerFriendsList_5(),
	FriendsOuterNavScreen_t1899896515::get_offset_of_phoneNumButton_6(),
	FriendsOuterNavScreen_t1899896515::get_offset_of_UIclosing_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof (FriendsSearchNavScreen_t2437737290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5660[1] = 
{
	FriendsSearchNavScreen_t2437737290::get_offset_of_searchUIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof (FriendsSearchNavigateForwardButton_t2162038683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5661[2] = 
{
	FriendsSearchNavigateForwardButton_t2162038683::get_offset_of_nextScreen_5(),
	FriendsSearchNavigateForwardButton_t2162038683::get_offset_of_buttonLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof (FriendSearchNavScreen_t3018389163)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5662[4] = 
{
	FriendSearchNavScreen_t3018389163::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof (FriendsSearchNavigationController_t1013932095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5663[7] = 
{
	FriendsSearchNavigationController_t1013932095::get_offset_of_friendsLabel_8(),
	FriendsSearchNavigationController_t1013932095::get_offset_of_globalLabel_9(),
	FriendsSearchNavigationController_t1013932095::get_offset_of_friendsObject_10(),
	FriendsSearchNavigationController_t1013932095::get_offset_of_globalObject_11(),
	FriendsSearchNavigationController_t1013932095::get_offset_of_activePos_12(),
	FriendsSearchNavigationController_t1013932095::get_offset_of_leftInactivePos_13(),
	FriendsSearchNavigationController_t1013932095::get_offset_of_rightInactivePos_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof (CloseMultiPhoneNumberUI_t914254952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5664[1] = 
{
	CloseMultiPhoneNumberUI_t914254952::get_offset_of_multiNumberUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof (MultiPhoneNumberILP_t3109982769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5665[9] = 
{
	MultiPhoneNumberILP_t3109982769::get_offset_of_maxNameLength_33(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_listLoaded_34(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_numbersArray_35(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_contentObject_36(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_nameLabel_37(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_alphaTween_38(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_bottomDivider_39(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_attackList_40(),
	MultiPhoneNumberILP_t3109982769::get_offset_of_uiClosing_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof (MultiPhoneNumberListItem_t177127779), -1, sizeof(MultiPhoneNumberListItem_t177127779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5666[12] = 
{
	MultiPhoneNumberListItem_t177127779::get_offset_of_phoneNumber_10(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_listPopulator_11(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_numberLabel_12(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_divider_13(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_attackItem_14(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_scrollView_15(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_draggablePanel_16(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_mTrans_17(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_mScroll_18(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_mAutoFind_19(),
	MultiPhoneNumberListItem_t177127779::get_offset_of_mStarted_20(),
	MultiPhoneNumberListItem_t177127779_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof (OtherFriend_t3976068630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5667[4] = 
{
	OtherFriend_t3976068630::get_offset_of_alphaSeperator_0(),
	OtherFriend_t3976068630::get_offset_of_contactFirstName_1(),
	OtherFriend_t3976068630::get_offset_of_contactLastName_2(),
	OtherFriend_t3976068630::get_offset_of_contactPhoneNumbers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof (OtherFriendsILP_t671720694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5668[6] = 
{
	OtherFriendsILP_t671720694::get_offset_of_sepeartorColour_33(),
	OtherFriendsILP_t671720694::get_offset_of_maxNameLength_34(),
	OtherFriendsILP_t671720694::get_offset_of_listLoaded_35(),
	OtherFriendsILP_t671720694::get_offset_of_friendsArray_36(),
	OtherFriendsILP_t671720694::get_offset_of_multiNumberUI_37(),
	OtherFriendsILP_t671720694::get_offset_of_attackList_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof (OtherFriendsListItem_t1916582308), -1, sizeof(OtherFriendsListItem_t1916582308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5669[13] = 
{
	OtherFriendsListItem_t1916582308::get_offset_of_oFriend_10(),
	OtherFriendsListItem_t1916582308::get_offset_of_listPopulator_11(),
	OtherFriendsListItem_t1916582308::get_offset_of_firstNameLabel_12(),
	OtherFriendsListItem_t1916582308::get_offset_of_lastNameLabel_13(),
	OtherFriendsListItem_t1916582308::get_offset_of_inviteButton_14(),
	OtherFriendsListItem_t1916582308::get_offset_of_attackItem_15(),
	OtherFriendsListItem_t1916582308::get_offset_of_scrollView_16(),
	OtherFriendsListItem_t1916582308::get_offset_of_draggablePanel_17(),
	OtherFriendsListItem_t1916582308::get_offset_of_mTrans_18(),
	OtherFriendsListItem_t1916582308::get_offset_of_mScroll_19(),
	OtherFriendsListItem_t1916582308::get_offset_of_mAutoFind_20(),
	OtherFriendsListItem_t1916582308::get_offset_of_mStarted_21(),
	OtherFriendsListItem_t1916582308_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof (OuterPhoneNumberButton_t2854054552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5670[1] = 
{
	OuterPhoneNumberButton_t2854054552::get_offset_of_phoneNumberlabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof (PendingFriendIndicator_t2589313544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5671[1] = 
{
	PendingFriendIndicator_t2589313544::get_offset_of_indicator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof (PhoneNumberNavScreen_t1263467250)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5672[5] = 
{
	PhoneNumberNavScreen_t1263467250::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof (ContactsNaviagationController_t22981102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5673[8] = 
{
	ContactsNaviagationController_t22981102::get_offset_of_activePos_8(),
	ContactsNaviagationController_t22981102::get_offset_of_leftInactivePos_9(),
	ContactsNaviagationController_t22981102::get_offset_of_rightInactivePos_10(),
	ContactsNaviagationController_t22981102::get_offset_of_contentObject_11(),
	ContactsNaviagationController_t22981102::get_offset_of_titleLabel_12(),
	ContactsNaviagationController_t22981102::get_offset_of_yourNumberObject_13(),
	ContactsNaviagationController_t22981102::get_offset_of_verificationObject_14(),
	ContactsNaviagationController_t22981102::get_offset_of_mainNavigateBackwardButton_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof (ContactsPhoneNumberManager_t4168190569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5674[12] = 
{
	ContactsPhoneNumberManager_t4168190569::get_offset_of_phoneNumberResent_3(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_numberVerified_4(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_selectedCountryCode_5(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_countryCode_6(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_phoneNumber_7(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_fullPhoneNumber_8(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_mobileHash_9(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_mobilePinID_10(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_mobilePin_11(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_attack_12(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_countryNameLabel_13(),
	ContactsPhoneNumberManager_t4168190569::get_offset_of_countryCodeLabel_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { sizeof (EnterPhoneNumberButton_t3549119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5675[1] = 
{
	EnterPhoneNumberButton_t3549119::get_offset_of_attack_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof (PNVerificationCodeInput_t3714898508), -1, sizeof(PNVerificationCodeInput_t3714898508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5676[3] = 
{
	PNVerificationCodeInput_t3714898508::get_offset_of_codeinput_5(),
	PNVerificationCodeInput_t3714898508_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	PNVerificationCodeInput_t3714898508_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof (PNVerificationNavScreen_t710347658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5677[6] = 
{
	PNVerificationNavScreen_t710347658::get_offset_of_phonenumLabel_3(),
	PNVerificationNavScreen_t710347658::get_offset_of_codeinput_4(),
	PNVerificationNavScreen_t710347658::get_offset_of_reSendButton_5(),
	PNVerificationNavScreen_t710347658::get_offset_of_contentObject_6(),
	PNVerificationNavScreen_t710347658::get_offset_of_registerUIclosing_7(),
	PNVerificationNavScreen_t710347658::get_offset_of_verificationUIclosing_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof (PNVerifyCodeButton_t868136506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5678[2] = 
{
	PNVerifyCodeButton_t868136506::get_offset_of_input_5(),
	PNVerifyCodeButton_t868136506::get_offset_of_buttonBackground_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof (PNYourNumberNavScreen_t1723852807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5679[4] = 
{
	PNYourNumberNavScreen_t1723852807::get_offset_of_contentObject_3(),
	PNYourNumberNavScreen_t1723852807::get_offset_of_phoneNumLabel_4(),
	PNYourNumberNavScreen_t1723852807::get_offset_of_registerUIclosing_5(),
	PNYourNumberNavScreen_t1723852807::get_offset_of_yourNumUIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof (PhoneNumberContinueButton_t1818508624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5680[2] = 
{
	PhoneNumberContinueButton_t1818508624::get_offset_of_countryCodeLabel_7(),
	PhoneNumberContinueButton_t1818508624::get_offset_of_phoneNumberLabel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof (U3COnButtonClickU3Ec__AnonStorey0_t1880716841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5681[2] = 
{
	U3COnButtonClickU3Ec__AnonStorey0_t1880716841::get_offset_of_countyCode_0(),
	U3COnButtonClickU3Ec__AnonStorey0_t1880716841::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (PhoneNumberNaviagteForwardButton_t3286637755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5682[2] = 
{
	PhoneNumberNaviagteForwardButton_t3286637755::get_offset_of_nextScreen_5(),
	PhoneNumberNaviagteForwardButton_t3286637755::get_offset_of_buttonLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (PhoneNumberNavigateBackButton_t3971219339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5683[1] = 
{
	PhoneNumberNavigateBackButton_t3971219339::get_offset_of_buttonSprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof (PhoneNumberResendCode_t1736900895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5684[1] = 
{
	PhoneNumberResendCode_t1736900895::get_offset_of_codeInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (PhoneNumberUIScaler_t4195264923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5685[4] = 
{
	PhoneNumberUIScaler_t4195264923::get_offset_of_background_14(),
	PhoneNumberUIScaler_t4195264923::get_offset_of_backgroundCollider_15(),
	PhoneNumberUIScaler_t4195264923::get_offset_of_titleDivider_16(),
	PhoneNumberUIScaler_t4195264923::get_offset_of_navBackButton_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (YourNumberUIScaler_t1339804644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5686[11] = 
{
	YourNumberUIScaler_t1339804644::get_offset_of_topDivider_14(),
	YourNumberUIScaler_t1339804644::get_offset_of_middleDivider_15(),
	YourNumberUIScaler_t1339804644::get_offset_of_bottomDivider_16(),
	YourNumberUIScaler_t1339804644::get_offset_of_continueButtonCollider_17(),
	YourNumberUIScaler_t1339804644::get_offset_of_continueButton_18(),
	YourNumberUIScaler_t1339804644::get_offset_of_countryPickerCollider_19(),
	YourNumberUIScaler_t1339804644::get_offset_of_countryPickerButton_20(),
	YourNumberUIScaler_t1339804644::get_offset_of_countryPickerLabel_21(),
	YourNumberUIScaler_t1339804644::get_offset_of_countryCodeLabel_22(),
	YourNumberUIScaler_t1339804644::get_offset_of_countryCodeSeperator_23(),
	YourNumberUIScaler_t1339804644::get_offset_of_phoneNumberCollider_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof (SearchFriendsNavScreen_t1265140554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5687[1] = 
{
	SearchFriendsNavScreen_t1265140554::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (SearchGlobalNavScreen_t1709594696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5688[1] = 
{
	SearchGlobalNavScreen_t1709594696::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (FriendsHomeUIScaler_t2886449736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5689[16] = 
{
	FriendsHomeUIScaler_t2886449736::get_offset_of_tabBackground_14(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_bottomSeperator_15(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_innerTab_16(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_innerTabCollider_17(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_outerTab_18(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_outerTabCollider_19(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_otherTab_20(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_otherTabCollider_21(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_leftSeperator_22(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_rightSeperator_23(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_innerPanel_24(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_outerPanel_25(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_otherPanel_26(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_otherBonusBackground_27(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_multiNumberBackground_28(),
	FriendsHomeUIScaler_t2886449736::get_offset_of_multiNumberCollider_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (FriendsListItemUIScaler_t3047710612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5690[11] = 
{
	FriendsListItemUIScaler_t3047710612::get_offset_of_background_14(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_backgroundCollider_15(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_profilePicture_16(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_nameLabel_17(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_chatButton_18(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_friendButton_19(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_attackButton_20(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_acceptButton_21(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_declineButton_22(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_actionLabel_23(),
	FriendsListItemUIScaler_t3047710612::get_offset_of_timeCalculator_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (FriendsSearchUIScaler_t4167788223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5691[14] = 
{
	FriendsSearchUIScaler_t4167788223::get_offset_of_tabBackground_14(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_bottomSeperator_15(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_friendTab_16(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_friendTabCollider_17(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_globalTab_18(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_globalTabCollider_19(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_friendPanel_20(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_globalPanel_21(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_searchCollider_22(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_searchLabel_23(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_searchButton_24(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_searchDivider_25(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_friendsNoResultsLabel_26(),
	FriendsSearchUIScaler_t4167788223::get_offset_of_globalNoResultsLabel_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof (FriendsUIScaler_t2202545073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5692[4] = 
{
	FriendsUIScaler_t2202545073::get_offset_of_background_14(),
	FriendsUIScaler_t2202545073::get_offset_of_titleSeperator_15(),
	FriendsUIScaler_t2202545073::get_offset_of_navigateBackButton_16(),
	FriendsUIScaler_t2202545073::get_offset_of_searchButton_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof (OtherFriendListItemUIScaler_t173824305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5693[5] = 
{
	OtherFriendListItemUIScaler_t173824305::get_offset_of_background_14(),
	OtherFriendListItemUIScaler_t173824305::get_offset_of_backgroundCollider_15(),
	OtherFriendListItemUIScaler_t173824305::get_offset_of_firstNameLabel_16(),
	OtherFriendListItemUIScaler_t173824305::get_offset_of_inviteButton_17(),
	OtherFriendListItemUIScaler_t173824305::get_offset_of_divider_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof (ChatButton_t3979308854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5694[4] = 
{
	ChatButton_t3979308854::get_offset_of_chatIcon_5(),
	ChatButton_t3979308854::get_offset_of_newChatsLabel_6(),
	ChatButton_t3979308854::get_offset_of_activeColour_7(),
	ChatButton_t3979308854::get_offset_of_inactiveColour_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (FuelBarController_t2314862973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5695[3] = 
{
	FuelBarController_t2314862973::get_offset_of_activeFueldColour_13(),
	FuelBarController_t2314862973::get_offset_of_inactiveFueldColour_14(),
	FuelBarController_t2314862973::get_offset_of_fuelIcon_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { sizeof (FuelButton_t3326934984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5696[2] = 
{
	FuelButton_t3326934984::get_offset_of_allowToast_5(),
	FuelButton_t3326934984::get_offset_of_toastWaitTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (U3CWaitToAllowToastU3Ec__Iterator0_t3463864080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5697[4] = 
{
	U3CWaitToAllowToastU3Ec__Iterator0_t3463864080::get_offset_of_U24this_0(),
	U3CWaitToAllowToastU3Ec__Iterator0_t3463864080::get_offset_of_U24current_1(),
	U3CWaitToAllowToastU3Ec__Iterator0_t3463864080::get_offset_of_U24disposing_2(),
	U3CWaitToAllowToastU3Ec__Iterator0_t3463864080::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (HeaderManager_t49185160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5698[8] = 
{
	HeaderManager_t49185160::get_offset_of_posTween_3(),
	HeaderManager_t49185160::get_offset_of_avatar_4(),
	HeaderManager_t49185160::get_offset_of_fuelBar_5(),
	HeaderManager_t49185160::get_offset_of_shieldBar_6(),
	HeaderManager_t49185160::get_offset_of_dailyPointsLabel_7(),
	HeaderManager_t49185160::get_offset_of_bucksLabel_8(),
	HeaderManager_t49185160::get_offset_of_chatButton_9(),
	HeaderManager_t49185160::get_offset_of_firstFuelUpdate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof (HeaderUIScaler_t1298375071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5699[9] = 
{
	HeaderUIScaler_t1298375071::get_offset_of_background_14(),
	HeaderUIScaler_t1298375071::get_offset_of_profilePicture_15(),
	HeaderUIScaler_t1298375071::get_offset_of_dailyPoints_16(),
	HeaderUIScaler_t1298375071::get_offset_of_bucks_17(),
	HeaderUIScaler_t1298375071::get_offset_of_leaderboard_18(),
	HeaderUIScaler_t1298375071::get_offset_of_fuel_19(),
	HeaderUIScaler_t1298375071::get_offset_of_shield_20(),
	HeaderUIScaler_t1298375071::get_offset_of_chat_21(),
	HeaderUIScaler_t1298375071::get_offset_of_botDivider_22(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
