﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusILP
struct StatusILP_t1076408775;
// System.Collections.Generic.List`1<StatusListItem>
struct List_1_t4123291041;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void StatusILP::.ctor()
extern "C"  void StatusILP__ctor_m1450995076 (StatusILP_t1076408775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusILP::StartIL()
extern "C"  void StatusILP_StartIL_m3476647131 (StatusILP_t1076408775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusILP::DrawIL()
extern "C"  void StatusILP_DrawIL_m4220499503 (StatusILP_t1076408775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusILP::SetDataArray()
extern "C"  void StatusILP_SetDataArray_m1835728629 (StatusILP_t1076408775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusILP::UpdateListData()
extern "C"  void StatusILP_UpdateListData_m1015524953 (StatusILP_t1076408775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<StatusListItem> StatusILP::GetItemsByUserID(System.String)
extern "C"  List_1_t4123291041 * StatusILP_GetItemsByUserID_m3988811097 (StatusILP_t1076408775 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void StatusILP_InitListItemWithIndex_m3031108258 (StatusILP_t1076408775 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void StatusILP_PopulateListItemWithIndex_m3201222869 (StatusILP_t1076408775 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
