﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AwardModal
struct  AwardModal_t1856366994  : public ModalUI_t2568752073
{
public:
	// UILabel AwardModal::rankNameLabel
	UILabel_t1795115428 * ___rankNameLabel_3;
	// UILabel AwardModal::rankLevelLabel
	UILabel_t1795115428 * ___rankLevelLabel_4;
	// UILabel AwardModal::rankBonusLabel
	UILabel_t1795115428 * ___rankBonusLabel_5;

public:
	inline static int32_t get_offset_of_rankNameLabel_3() { return static_cast<int32_t>(offsetof(AwardModal_t1856366994, ___rankNameLabel_3)); }
	inline UILabel_t1795115428 * get_rankNameLabel_3() const { return ___rankNameLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_rankNameLabel_3() { return &___rankNameLabel_3; }
	inline void set_rankNameLabel_3(UILabel_t1795115428 * value)
	{
		___rankNameLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___rankNameLabel_3, value);
	}

	inline static int32_t get_offset_of_rankLevelLabel_4() { return static_cast<int32_t>(offsetof(AwardModal_t1856366994, ___rankLevelLabel_4)); }
	inline UILabel_t1795115428 * get_rankLevelLabel_4() const { return ___rankLevelLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_rankLevelLabel_4() { return &___rankLevelLabel_4; }
	inline void set_rankLevelLabel_4(UILabel_t1795115428 * value)
	{
		___rankLevelLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___rankLevelLabel_4, value);
	}

	inline static int32_t get_offset_of_rankBonusLabel_5() { return static_cast<int32_t>(offsetof(AwardModal_t1856366994, ___rankBonusLabel_5)); }
	inline UILabel_t1795115428 * get_rankBonusLabel_5() const { return ___rankBonusLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_rankBonusLabel_5() { return &___rankBonusLabel_5; }
	inline void set_rankBonusLabel_5(UILabel_t1795115428 * value)
	{
		___rankBonusLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___rankBonusLabel_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
