﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchAccuracyController/<FlashRoutine>c__Iterator0
struct U3CFlashRoutineU3Ec__Iterator0_t3216582880;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchAccuracyController/<FlashRoutine>c__Iterator0::.ctor()
extern "C"  void U3CFlashRoutineU3Ec__Iterator0__ctor_m1891249649 (U3CFlashRoutineU3Ec__Iterator0_t3216582880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaunchAccuracyController/<FlashRoutine>c__Iterator0::MoveNext()
extern "C"  bool U3CFlashRoutineU3Ec__Iterator0_MoveNext_m2140874903 (U3CFlashRoutineU3Ec__Iterator0_t3216582880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchAccuracyController/<FlashRoutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFlashRoutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3325183035 (U3CFlashRoutineU3Ec__Iterator0_t3216582880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchAccuracyController/<FlashRoutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFlashRoutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2396433187 (U3CFlashRoutineU3Ec__Iterator0_t3216582880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController/<FlashRoutine>c__Iterator0::Dispose()
extern "C"  void U3CFlashRoutineU3Ec__Iterator0_Dispose_m4092125602 (U3CFlashRoutineU3Ec__Iterator0_t3216582880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController/<FlashRoutine>c__Iterator0::Reset()
extern "C"  void U3CFlashRoutineU3Ec__Iterator0_Reset_m3493143844 (U3CFlashRoutineU3Ec__Iterator0_t3216582880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
