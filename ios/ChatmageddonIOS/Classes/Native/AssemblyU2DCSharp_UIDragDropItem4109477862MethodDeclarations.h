﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropItem
struct UIDragDropItem_t4109477862;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void UIDragDropItem::.ctor()
extern "C"  void UIDragDropItem__ctor_m829720719 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::Awake()
extern "C"  void UIDragDropItem_Awake_m1461707250 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnEnable()
extern "C"  void UIDragDropItem_OnEnable_m4198009427 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDisable()
extern "C"  void UIDragDropItem_OnDisable_m1652897318 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::Start()
extern "C"  void UIDragDropItem_Start_m1985071347 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnPress(System.Boolean)
extern "C"  void UIDragDropItem_OnPress_m1571251264 (UIDragDropItem_t4109477862 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::Update()
extern "C"  void UIDragDropItem_Update_m31249776 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragStart()
extern "C"  void UIDragDropItem_OnDragStart_m4274963392 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::StartDragging()
extern "C"  void UIDragDropItem_StartDragging_m3580789092 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDrag(UnityEngine.Vector2)
extern "C"  void UIDragDropItem_OnDrag_m1758686670 (UIDragDropItem_t4109477862 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragEnd()
extern "C"  void UIDragDropItem_OnDragEnd_m1153002525 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::StopDragging(UnityEngine.GameObject)
extern "C"  void UIDragDropItem_StopDragging_m245410442 (UIDragDropItem_t4109477862 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropStart()
extern "C"  void UIDragDropItem_OnDragDropStart_m403518735 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropMove(UnityEngine.Vector2)
extern "C"  void UIDragDropItem_OnDragDropMove_m4228945590 (UIDragDropItem_t4109477862 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropRelease(UnityEngine.GameObject)
extern "C"  void UIDragDropItem_OnDragDropRelease_m222288340 (UIDragDropItem_t4109477862 * __this, GameObject_t1756533147 * ___surface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropEnd()
extern "C"  void UIDragDropItem_OnDragDropEnd_m3064206922 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIDragDropItem::EnableDragScrollView()
extern "C"  Il2CppObject * UIDragDropItem_EnableDragScrollView_m2884526460 (UIDragDropItem_t4109477862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::.cctor()
extern "C"  void UIDragDropItem__cctor_m4145761504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
