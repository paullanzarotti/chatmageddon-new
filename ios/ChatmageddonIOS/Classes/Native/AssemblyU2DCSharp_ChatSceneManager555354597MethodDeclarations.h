﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatSceneManager
struct ChatSceneManager_t555354597;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatSceneManager::.ctor()
extern "C"  void ChatSceneManager__ctor_m3251023152 (ChatSceneManager_t555354597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatSceneManager::InitScene()
extern "C"  void ChatSceneManager_InitScene_m3329031514 (ChatSceneManager_t555354597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatSceneManager::StartScene()
extern "C"  void ChatSceneManager_StartScene_m373318326 (ChatSceneManager_t555354597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatSceneManager::LoadSceneLoader(System.Boolean)
extern "C"  void ChatSceneManager_LoadSceneLoader_m735884734 (ChatSceneManager_t555354597 * __this, bool ___unload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatSceneManager::StartSceneUnLoader()
extern "C"  void ChatSceneManager_StartSceneUnLoader_m2117034170 (ChatSceneManager_t555354597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
