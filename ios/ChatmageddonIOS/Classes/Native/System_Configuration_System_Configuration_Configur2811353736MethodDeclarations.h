﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationCollectionAttribute
struct ConfigurationCollectionAttribute_t2811353736;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Configur1806001494.h"

// System.Void System.Configuration.ConfigurationCollectionAttribute::.ctor(System.Type)
extern "C"  void ConfigurationCollectionAttribute__ctor_m1251693102 (ConfigurationCollectionAttribute_t2811353736 * __this, Type_t * ___itemType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationCollectionAttribute::get_AddItemName()
extern "C"  String_t* ConfigurationCollectionAttribute_get_AddItemName_m1343590308 (ConfigurationCollectionAttribute_t2811353736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationCollectionAttribute::set_AddItemName(System.String)
extern "C"  void ConfigurationCollectionAttribute_set_AddItemName_m3040132619 (ConfigurationCollectionAttribute_t2811353736 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationCollectionAttribute::get_ClearItemsName()
extern "C"  String_t* ConfigurationCollectionAttribute_get_ClearItemsName_m2101089535 (ConfigurationCollectionAttribute_t2811353736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationCollectionAttribute::set_ClearItemsName(System.String)
extern "C"  void ConfigurationCollectionAttribute_set_ClearItemsName_m2400573114 (ConfigurationCollectionAttribute_t2811353736 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationCollectionAttribute::get_RemoveItemName()
extern "C"  String_t* ConfigurationCollectionAttribute_get_RemoveItemName_m1223332851 (ConfigurationCollectionAttribute_t2811353736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationCollectionAttribute::set_RemoveItemName(System.String)
extern "C"  void ConfigurationCollectionAttribute_set_RemoveItemName_m1279249518 (ConfigurationCollectionAttribute_t2811353736 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElementCollectionType System.Configuration.ConfigurationCollectionAttribute::get_CollectionType()
extern "C"  int32_t ConfigurationCollectionAttribute_get_CollectionType_m2786056072 (ConfigurationCollectionAttribute_t2811353736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationCollectionAttribute::set_CollectionType(System.Configuration.ConfigurationElementCollectionType)
extern "C"  void ConfigurationCollectionAttribute_set_CollectionType_m1820962205 (ConfigurationCollectionAttribute_t2811353736 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Configuration.ConfigurationCollectionAttribute::get_ItemType()
extern "C"  Type_t * ConfigurationCollectionAttribute_get_ItemType_m2478779947 (ConfigurationCollectionAttribute_t2811353736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
