﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ApplicationManager
struct ApplicationManager_t2110631419;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplicationManager/<LogoutFromServer>c__AnonStorey2
struct  U3CLogoutFromServerU3Ec__AnonStorey2_t833513242  : public Il2CppObject
{
public:
	// System.Boolean ApplicationManager/<LogoutFromServer>c__AnonStorey2::forced
	bool ___forced_0;
	// ApplicationManager ApplicationManager/<LogoutFromServer>c__AnonStorey2::$this
	ApplicationManager_t2110631419 * ___U24this_1;

public:
	inline static int32_t get_offset_of_forced_0() { return static_cast<int32_t>(offsetof(U3CLogoutFromServerU3Ec__AnonStorey2_t833513242, ___forced_0)); }
	inline bool get_forced_0() const { return ___forced_0; }
	inline bool* get_address_of_forced_0() { return &___forced_0; }
	inline void set_forced_0(bool value)
	{
		___forced_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLogoutFromServerU3Ec__AnonStorey2_t833513242, ___U24this_1)); }
	inline ApplicationManager_t2110631419 * get_U24this_1() const { return ___U24this_1; }
	inline ApplicationManager_t2110631419 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ApplicationManager_t2110631419 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
