﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPCycler/CyclerIndexChangeHandler
struct CyclerIndexChangeHandler_t463772902;
// IPCycler/CyclerStoppedHandler
struct CyclerStoppedHandler_t3894542237;
// IPCycler/SelectionStartedHandler
struct SelectionStartedHandler_t4151988355;
// IPCycler/CenterOnChildStartedHandler
struct CenterOnChildStartedHandler_t5714103;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UIScrollView
struct UIScrollView_t3033954930;
// IPDragScrollView
struct IPDragScrollView_t145219383;
// IPUserInteraction
struct IPUserInteraction_t4192479194;
// UIPanel
struct UIPanel_t1795085332;
// UICenterOnChild
struct UICenterOnChild_t1687745660;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_IPCycler_Direction617647995.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPCycler
struct  IPCycler_t1336138445  : public MonoBehaviour_t1158329972
{
public:
	// IPCycler/Direction IPCycler::direction
	int32_t ___direction_2;
	// System.Single IPCycler::spacing
	float ___spacing_3;
	// System.Single IPCycler::dragScale
	float ___dragScale_4;
	// System.Single IPCycler::recenterSpeedThreshold
	float ___recenterSpeedThreshold_5;
	// System.Single IPCycler::recenterSpringStrength
	float ___recenterSpringStrength_6;
	// System.Boolean IPCycler::restrictDragToPicker
	bool ___restrictDragToPicker_7;
	// System.Boolean IPCycler::<IsMoving>k__BackingField
	bool ___U3CIsMovingU3Ek__BackingField_8;
	// System.Int32 IPCycler::<CenterWidgetIndex>k__BackingField
	int32_t ___U3CCenterWidgetIndexU3Ek__BackingField_9;
	// System.Int32 IPCycler::<RecenterTargetWidgetIndex>k__BackingField
	int32_t ___U3CRecenterTargetWidgetIndexU3Ek__BackingField_10;
	// System.Boolean IPCycler::<ClampIncrement>k__BackingField
	bool ___U3CClampIncrementU3Ek__BackingField_11;
	// System.Boolean IPCycler::<ClampDecrement>k__BackingField
	bool ___U3CClampDecrementU3Ek__BackingField_12;
	// IPCycler/CyclerIndexChangeHandler IPCycler::onCyclerIndexChange
	CyclerIndexChangeHandler_t463772902 * ___onCyclerIndexChange_13;
	// IPCycler/CyclerStoppedHandler IPCycler::onCyclerStopped
	CyclerStoppedHandler_t3894542237 * ___onCyclerStopped_14;
	// IPCycler/SelectionStartedHandler IPCycler::onCyclerSelectionStarted
	SelectionStartedHandler_t4151988355 * ___onCyclerSelectionStarted_15;
	// IPCycler/CenterOnChildStartedHandler IPCycler::onCenterOnChildStarted
	CenterOnChildStartedHandler_t5714103 * ___onCenterOnChildStarted_16;
	// UnityEngine.Transform[] IPCycler::_cycledTransforms
	TransformU5BU5D_t3764228911* ____cycledTransforms_17;
	// UIScrollView IPCycler::_draggablePanel
	UIScrollView_t3033954930 * ____draggablePanel_18;
	// IPDragScrollView IPCycler::dragPanelContents
	IPDragScrollView_t145219383 * ___dragPanelContents_19;
	// IPUserInteraction IPCycler::userInteraction
	IPUserInteraction_t4192479194 * ___userInteraction_20;
	// UIPanel IPCycler::_panel
	UIPanel_t1795085332 * ____panel_21;
	// UICenterOnChild IPCycler::_uiCenterOnChild
	UICenterOnChild_t1687745660 * ____uiCenterOnChild_22;
	// UnityEngine.BoxCollider IPCycler::_pickerCollider
	BoxCollider_t22920061 * ____pickerCollider_23;
	// System.Int32 IPCycler::_initFrame
	int32_t ____initFrame_24;
	// System.Int32 IPCycler::_lastResetFrame
	int32_t ____lastResetFrame_25;
	// System.Single IPCycler::_decrementThreshold
	float ____decrementThreshold_26;
	// System.Single IPCycler::_incrementThreshold
	float ____incrementThreshold_27;
	// System.Single IPCycler::_transformJumpDelta
	float ____transformJumpDelta_28;
	// System.Single IPCycler::_panelSignificantPosVector
	float ____panelSignificantPosVector_29;
	// System.Single IPCycler::_panelPrevPos
	float ____panelPrevPos_30;
	// System.Single IPCycler::_deltaPos
	float ____deltaPos_31;
	// UnityEngine.Vector3 IPCycler::_resetPosition
	Vector3_t2243707580  ____resetPosition_32;
	// System.Boolean IPCycler::_isInitialized
	bool ____isInitialized_33;
	// System.Boolean IPCycler::_hasMovedSincePress
	bool ____hasMovedSincePress_34;
	// System.Boolean IPCycler::_isHorizontal
	bool ____isHorizontal_35;
	// System.Single IPCycler::_trueSpacing
	float ____trueSpacing_36;

public:
	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_spacing_3() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___spacing_3)); }
	inline float get_spacing_3() const { return ___spacing_3; }
	inline float* get_address_of_spacing_3() { return &___spacing_3; }
	inline void set_spacing_3(float value)
	{
		___spacing_3 = value;
	}

	inline static int32_t get_offset_of_dragScale_4() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___dragScale_4)); }
	inline float get_dragScale_4() const { return ___dragScale_4; }
	inline float* get_address_of_dragScale_4() { return &___dragScale_4; }
	inline void set_dragScale_4(float value)
	{
		___dragScale_4 = value;
	}

	inline static int32_t get_offset_of_recenterSpeedThreshold_5() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___recenterSpeedThreshold_5)); }
	inline float get_recenterSpeedThreshold_5() const { return ___recenterSpeedThreshold_5; }
	inline float* get_address_of_recenterSpeedThreshold_5() { return &___recenterSpeedThreshold_5; }
	inline void set_recenterSpeedThreshold_5(float value)
	{
		___recenterSpeedThreshold_5 = value;
	}

	inline static int32_t get_offset_of_recenterSpringStrength_6() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___recenterSpringStrength_6)); }
	inline float get_recenterSpringStrength_6() const { return ___recenterSpringStrength_6; }
	inline float* get_address_of_recenterSpringStrength_6() { return &___recenterSpringStrength_6; }
	inline void set_recenterSpringStrength_6(float value)
	{
		___recenterSpringStrength_6 = value;
	}

	inline static int32_t get_offset_of_restrictDragToPicker_7() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___restrictDragToPicker_7)); }
	inline bool get_restrictDragToPicker_7() const { return ___restrictDragToPicker_7; }
	inline bool* get_address_of_restrictDragToPicker_7() { return &___restrictDragToPicker_7; }
	inline void set_restrictDragToPicker_7(bool value)
	{
		___restrictDragToPicker_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsMovingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___U3CIsMovingU3Ek__BackingField_8)); }
	inline bool get_U3CIsMovingU3Ek__BackingField_8() const { return ___U3CIsMovingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsMovingU3Ek__BackingField_8() { return &___U3CIsMovingU3Ek__BackingField_8; }
	inline void set_U3CIsMovingU3Ek__BackingField_8(bool value)
	{
		___U3CIsMovingU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCenterWidgetIndexU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___U3CCenterWidgetIndexU3Ek__BackingField_9)); }
	inline int32_t get_U3CCenterWidgetIndexU3Ek__BackingField_9() const { return ___U3CCenterWidgetIndexU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CCenterWidgetIndexU3Ek__BackingField_9() { return &___U3CCenterWidgetIndexU3Ek__BackingField_9; }
	inline void set_U3CCenterWidgetIndexU3Ek__BackingField_9(int32_t value)
	{
		___U3CCenterWidgetIndexU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRecenterTargetWidgetIndexU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___U3CRecenterTargetWidgetIndexU3Ek__BackingField_10)); }
	inline int32_t get_U3CRecenterTargetWidgetIndexU3Ek__BackingField_10() const { return ___U3CRecenterTargetWidgetIndexU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CRecenterTargetWidgetIndexU3Ek__BackingField_10() { return &___U3CRecenterTargetWidgetIndexU3Ek__BackingField_10; }
	inline void set_U3CRecenterTargetWidgetIndexU3Ek__BackingField_10(int32_t value)
	{
		___U3CRecenterTargetWidgetIndexU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CClampIncrementU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___U3CClampIncrementU3Ek__BackingField_11)); }
	inline bool get_U3CClampIncrementU3Ek__BackingField_11() const { return ___U3CClampIncrementU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CClampIncrementU3Ek__BackingField_11() { return &___U3CClampIncrementU3Ek__BackingField_11; }
	inline void set_U3CClampIncrementU3Ek__BackingField_11(bool value)
	{
		___U3CClampIncrementU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CClampDecrementU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___U3CClampDecrementU3Ek__BackingField_12)); }
	inline bool get_U3CClampDecrementU3Ek__BackingField_12() const { return ___U3CClampDecrementU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CClampDecrementU3Ek__BackingField_12() { return &___U3CClampDecrementU3Ek__BackingField_12; }
	inline void set_U3CClampDecrementU3Ek__BackingField_12(bool value)
	{
		___U3CClampDecrementU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_onCyclerIndexChange_13() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___onCyclerIndexChange_13)); }
	inline CyclerIndexChangeHandler_t463772902 * get_onCyclerIndexChange_13() const { return ___onCyclerIndexChange_13; }
	inline CyclerIndexChangeHandler_t463772902 ** get_address_of_onCyclerIndexChange_13() { return &___onCyclerIndexChange_13; }
	inline void set_onCyclerIndexChange_13(CyclerIndexChangeHandler_t463772902 * value)
	{
		___onCyclerIndexChange_13 = value;
		Il2CppCodeGenWriteBarrier(&___onCyclerIndexChange_13, value);
	}

	inline static int32_t get_offset_of_onCyclerStopped_14() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___onCyclerStopped_14)); }
	inline CyclerStoppedHandler_t3894542237 * get_onCyclerStopped_14() const { return ___onCyclerStopped_14; }
	inline CyclerStoppedHandler_t3894542237 ** get_address_of_onCyclerStopped_14() { return &___onCyclerStopped_14; }
	inline void set_onCyclerStopped_14(CyclerStoppedHandler_t3894542237 * value)
	{
		___onCyclerStopped_14 = value;
		Il2CppCodeGenWriteBarrier(&___onCyclerStopped_14, value);
	}

	inline static int32_t get_offset_of_onCyclerSelectionStarted_15() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___onCyclerSelectionStarted_15)); }
	inline SelectionStartedHandler_t4151988355 * get_onCyclerSelectionStarted_15() const { return ___onCyclerSelectionStarted_15; }
	inline SelectionStartedHandler_t4151988355 ** get_address_of_onCyclerSelectionStarted_15() { return &___onCyclerSelectionStarted_15; }
	inline void set_onCyclerSelectionStarted_15(SelectionStartedHandler_t4151988355 * value)
	{
		___onCyclerSelectionStarted_15 = value;
		Il2CppCodeGenWriteBarrier(&___onCyclerSelectionStarted_15, value);
	}

	inline static int32_t get_offset_of_onCenterOnChildStarted_16() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___onCenterOnChildStarted_16)); }
	inline CenterOnChildStartedHandler_t5714103 * get_onCenterOnChildStarted_16() const { return ___onCenterOnChildStarted_16; }
	inline CenterOnChildStartedHandler_t5714103 ** get_address_of_onCenterOnChildStarted_16() { return &___onCenterOnChildStarted_16; }
	inline void set_onCenterOnChildStarted_16(CenterOnChildStartedHandler_t5714103 * value)
	{
		___onCenterOnChildStarted_16 = value;
		Il2CppCodeGenWriteBarrier(&___onCenterOnChildStarted_16, value);
	}

	inline static int32_t get_offset_of__cycledTransforms_17() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____cycledTransforms_17)); }
	inline TransformU5BU5D_t3764228911* get__cycledTransforms_17() const { return ____cycledTransforms_17; }
	inline TransformU5BU5D_t3764228911** get_address_of__cycledTransforms_17() { return &____cycledTransforms_17; }
	inline void set__cycledTransforms_17(TransformU5BU5D_t3764228911* value)
	{
		____cycledTransforms_17 = value;
		Il2CppCodeGenWriteBarrier(&____cycledTransforms_17, value);
	}

	inline static int32_t get_offset_of__draggablePanel_18() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____draggablePanel_18)); }
	inline UIScrollView_t3033954930 * get__draggablePanel_18() const { return ____draggablePanel_18; }
	inline UIScrollView_t3033954930 ** get_address_of__draggablePanel_18() { return &____draggablePanel_18; }
	inline void set__draggablePanel_18(UIScrollView_t3033954930 * value)
	{
		____draggablePanel_18 = value;
		Il2CppCodeGenWriteBarrier(&____draggablePanel_18, value);
	}

	inline static int32_t get_offset_of_dragPanelContents_19() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___dragPanelContents_19)); }
	inline IPDragScrollView_t145219383 * get_dragPanelContents_19() const { return ___dragPanelContents_19; }
	inline IPDragScrollView_t145219383 ** get_address_of_dragPanelContents_19() { return &___dragPanelContents_19; }
	inline void set_dragPanelContents_19(IPDragScrollView_t145219383 * value)
	{
		___dragPanelContents_19 = value;
		Il2CppCodeGenWriteBarrier(&___dragPanelContents_19, value);
	}

	inline static int32_t get_offset_of_userInteraction_20() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ___userInteraction_20)); }
	inline IPUserInteraction_t4192479194 * get_userInteraction_20() const { return ___userInteraction_20; }
	inline IPUserInteraction_t4192479194 ** get_address_of_userInteraction_20() { return &___userInteraction_20; }
	inline void set_userInteraction_20(IPUserInteraction_t4192479194 * value)
	{
		___userInteraction_20 = value;
		Il2CppCodeGenWriteBarrier(&___userInteraction_20, value);
	}

	inline static int32_t get_offset_of__panel_21() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____panel_21)); }
	inline UIPanel_t1795085332 * get__panel_21() const { return ____panel_21; }
	inline UIPanel_t1795085332 ** get_address_of__panel_21() { return &____panel_21; }
	inline void set__panel_21(UIPanel_t1795085332 * value)
	{
		____panel_21 = value;
		Il2CppCodeGenWriteBarrier(&____panel_21, value);
	}

	inline static int32_t get_offset_of__uiCenterOnChild_22() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____uiCenterOnChild_22)); }
	inline UICenterOnChild_t1687745660 * get__uiCenterOnChild_22() const { return ____uiCenterOnChild_22; }
	inline UICenterOnChild_t1687745660 ** get_address_of__uiCenterOnChild_22() { return &____uiCenterOnChild_22; }
	inline void set__uiCenterOnChild_22(UICenterOnChild_t1687745660 * value)
	{
		____uiCenterOnChild_22 = value;
		Il2CppCodeGenWriteBarrier(&____uiCenterOnChild_22, value);
	}

	inline static int32_t get_offset_of__pickerCollider_23() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____pickerCollider_23)); }
	inline BoxCollider_t22920061 * get__pickerCollider_23() const { return ____pickerCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of__pickerCollider_23() { return &____pickerCollider_23; }
	inline void set__pickerCollider_23(BoxCollider_t22920061 * value)
	{
		____pickerCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&____pickerCollider_23, value);
	}

	inline static int32_t get_offset_of__initFrame_24() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____initFrame_24)); }
	inline int32_t get__initFrame_24() const { return ____initFrame_24; }
	inline int32_t* get_address_of__initFrame_24() { return &____initFrame_24; }
	inline void set__initFrame_24(int32_t value)
	{
		____initFrame_24 = value;
	}

	inline static int32_t get_offset_of__lastResetFrame_25() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____lastResetFrame_25)); }
	inline int32_t get__lastResetFrame_25() const { return ____lastResetFrame_25; }
	inline int32_t* get_address_of__lastResetFrame_25() { return &____lastResetFrame_25; }
	inline void set__lastResetFrame_25(int32_t value)
	{
		____lastResetFrame_25 = value;
	}

	inline static int32_t get_offset_of__decrementThreshold_26() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____decrementThreshold_26)); }
	inline float get__decrementThreshold_26() const { return ____decrementThreshold_26; }
	inline float* get_address_of__decrementThreshold_26() { return &____decrementThreshold_26; }
	inline void set__decrementThreshold_26(float value)
	{
		____decrementThreshold_26 = value;
	}

	inline static int32_t get_offset_of__incrementThreshold_27() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____incrementThreshold_27)); }
	inline float get__incrementThreshold_27() const { return ____incrementThreshold_27; }
	inline float* get_address_of__incrementThreshold_27() { return &____incrementThreshold_27; }
	inline void set__incrementThreshold_27(float value)
	{
		____incrementThreshold_27 = value;
	}

	inline static int32_t get_offset_of__transformJumpDelta_28() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____transformJumpDelta_28)); }
	inline float get__transformJumpDelta_28() const { return ____transformJumpDelta_28; }
	inline float* get_address_of__transformJumpDelta_28() { return &____transformJumpDelta_28; }
	inline void set__transformJumpDelta_28(float value)
	{
		____transformJumpDelta_28 = value;
	}

	inline static int32_t get_offset_of__panelSignificantPosVector_29() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____panelSignificantPosVector_29)); }
	inline float get__panelSignificantPosVector_29() const { return ____panelSignificantPosVector_29; }
	inline float* get_address_of__panelSignificantPosVector_29() { return &____panelSignificantPosVector_29; }
	inline void set__panelSignificantPosVector_29(float value)
	{
		____panelSignificantPosVector_29 = value;
	}

	inline static int32_t get_offset_of__panelPrevPos_30() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____panelPrevPos_30)); }
	inline float get__panelPrevPos_30() const { return ____panelPrevPos_30; }
	inline float* get_address_of__panelPrevPos_30() { return &____panelPrevPos_30; }
	inline void set__panelPrevPos_30(float value)
	{
		____panelPrevPos_30 = value;
	}

	inline static int32_t get_offset_of__deltaPos_31() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____deltaPos_31)); }
	inline float get__deltaPos_31() const { return ____deltaPos_31; }
	inline float* get_address_of__deltaPos_31() { return &____deltaPos_31; }
	inline void set__deltaPos_31(float value)
	{
		____deltaPos_31 = value;
	}

	inline static int32_t get_offset_of__resetPosition_32() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____resetPosition_32)); }
	inline Vector3_t2243707580  get__resetPosition_32() const { return ____resetPosition_32; }
	inline Vector3_t2243707580 * get_address_of__resetPosition_32() { return &____resetPosition_32; }
	inline void set__resetPosition_32(Vector3_t2243707580  value)
	{
		____resetPosition_32 = value;
	}

	inline static int32_t get_offset_of__isInitialized_33() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____isInitialized_33)); }
	inline bool get__isInitialized_33() const { return ____isInitialized_33; }
	inline bool* get_address_of__isInitialized_33() { return &____isInitialized_33; }
	inline void set__isInitialized_33(bool value)
	{
		____isInitialized_33 = value;
	}

	inline static int32_t get_offset_of__hasMovedSincePress_34() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____hasMovedSincePress_34)); }
	inline bool get__hasMovedSincePress_34() const { return ____hasMovedSincePress_34; }
	inline bool* get_address_of__hasMovedSincePress_34() { return &____hasMovedSincePress_34; }
	inline void set__hasMovedSincePress_34(bool value)
	{
		____hasMovedSincePress_34 = value;
	}

	inline static int32_t get_offset_of__isHorizontal_35() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____isHorizontal_35)); }
	inline bool get__isHorizontal_35() const { return ____isHorizontal_35; }
	inline bool* get_address_of__isHorizontal_35() { return &____isHorizontal_35; }
	inline void set__isHorizontal_35(bool value)
	{
		____isHorizontal_35 = value;
	}

	inline static int32_t get_offset_of__trueSpacing_36() { return static_cast<int32_t>(offsetof(IPCycler_t1336138445, ____trueSpacing_36)); }
	inline float get__trueSpacing_36() const { return ____trueSpacing_36; }
	inline float* get_address_of__trueSpacing_36() { return &____trueSpacing_36; }
	inline void set__trueSpacing_36(float value)
	{
		____trueSpacing_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
