﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UIPanel
struct UIPanel_t1795085332;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatCenterUIScaler
struct  ChatCenterUIScaler_t436229885  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite ChatCenterUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UISprite ChatCenterUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_15;
	// UnityEngine.Transform ChatCenterUIScaler::navigateBackButton
	Transform_t3275118058 * ___navigateBackButton_16;
	// UnityEngine.Transform ChatCenterUIScaler::newChatButton
	Transform_t3275118058 * ___newChatButton_17;
	// UIPanel ChatCenterUIScaler::threadsPanel
	UIPanel_t1795085332 * ___threadsPanel_18;
	// UIPanel ChatCenterUIScaler::messagePanel
	UIPanel_t1795085332 * ___messagePanel_19;
	// UnityEngine.Transform ChatCenterUIScaler::messageTable
	Transform_t3275118058 * ___messageTable_20;
	// UILabel ChatCenterUIScaler::messageToLabel
	UILabel_t1795115428 * ___messageToLabel_21;
	// UILabel ChatCenterUIScaler::messageContactsLabel
	UILabel_t1795115428 * ___messageContactsLabel_22;
	// UnityEngine.BoxCollider ChatCenterUIScaler::messagePlusButtonCollider
	BoxCollider_t22920061 * ___messagePlusButtonCollider_23;
	// UILabel ChatCenterUIScaler::messagePlusLabel
	UILabel_t1795115428 * ___messagePlusLabel_24;
	// UISprite ChatCenterUIScaler::messageSeperator
	UISprite_t603616735 * ___messageSeperator_25;
	// UnityEngine.BoxCollider ChatCenterUIScaler::keyboardCollider
	BoxCollider_t22920061 * ___keyboardCollider_26;
	// UISprite ChatCenterUIScaler::keyboardBackground
	UISprite_t603616735 * ___keyboardBackground_27;
	// UISprite ChatCenterUIScaler::keyboardForeground
	UISprite_t603616735 * ___keyboardForeground_28;
	// UILabel ChatCenterUIScaler::keyboardLabel
	UILabel_t1795115428 * ___keyboardLabel_29;
	// UnityEngine.BoxCollider ChatCenterUIScaler::sendCollider
	BoxCollider_t22920061 * ___sendCollider_30;
	// UILabel ChatCenterUIScaler::sendLabel
	UILabel_t1795115428 * ___sendLabel_31;
	// UnityEngine.BoxCollider ChatCenterUIScaler::searchCollider
	BoxCollider_t22920061 * ___searchCollider_32;
	// UILabel ChatCenterUIScaler::searchLabel
	UILabel_t1795115428 * ___searchLabel_33;
	// UnityEngine.Transform ChatCenterUIScaler::searchButton
	Transform_t3275118058 * ___searchButton_34;
	// UISprite ChatCenterUIScaler::contactsSeperator
	UISprite_t603616735 * ___contactsSeperator_35;
	// UIPanel ChatCenterUIScaler::contactsPanel
	UIPanel_t1795085332 * ___contactsPanel_36;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_titleSeperator_15() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___titleSeperator_15)); }
	inline UISprite_t603616735 * get_titleSeperator_15() const { return ___titleSeperator_15; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_15() { return &___titleSeperator_15; }
	inline void set_titleSeperator_15(UISprite_t603616735 * value)
	{
		___titleSeperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_15, value);
	}

	inline static int32_t get_offset_of_navigateBackButton_16() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___navigateBackButton_16)); }
	inline Transform_t3275118058 * get_navigateBackButton_16() const { return ___navigateBackButton_16; }
	inline Transform_t3275118058 ** get_address_of_navigateBackButton_16() { return &___navigateBackButton_16; }
	inline void set_navigateBackButton_16(Transform_t3275118058 * value)
	{
		___navigateBackButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___navigateBackButton_16, value);
	}

	inline static int32_t get_offset_of_newChatButton_17() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___newChatButton_17)); }
	inline Transform_t3275118058 * get_newChatButton_17() const { return ___newChatButton_17; }
	inline Transform_t3275118058 ** get_address_of_newChatButton_17() { return &___newChatButton_17; }
	inline void set_newChatButton_17(Transform_t3275118058 * value)
	{
		___newChatButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___newChatButton_17, value);
	}

	inline static int32_t get_offset_of_threadsPanel_18() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___threadsPanel_18)); }
	inline UIPanel_t1795085332 * get_threadsPanel_18() const { return ___threadsPanel_18; }
	inline UIPanel_t1795085332 ** get_address_of_threadsPanel_18() { return &___threadsPanel_18; }
	inline void set_threadsPanel_18(UIPanel_t1795085332 * value)
	{
		___threadsPanel_18 = value;
		Il2CppCodeGenWriteBarrier(&___threadsPanel_18, value);
	}

	inline static int32_t get_offset_of_messagePanel_19() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messagePanel_19)); }
	inline UIPanel_t1795085332 * get_messagePanel_19() const { return ___messagePanel_19; }
	inline UIPanel_t1795085332 ** get_address_of_messagePanel_19() { return &___messagePanel_19; }
	inline void set_messagePanel_19(UIPanel_t1795085332 * value)
	{
		___messagePanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___messagePanel_19, value);
	}

	inline static int32_t get_offset_of_messageTable_20() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messageTable_20)); }
	inline Transform_t3275118058 * get_messageTable_20() const { return ___messageTable_20; }
	inline Transform_t3275118058 ** get_address_of_messageTable_20() { return &___messageTable_20; }
	inline void set_messageTable_20(Transform_t3275118058 * value)
	{
		___messageTable_20 = value;
		Il2CppCodeGenWriteBarrier(&___messageTable_20, value);
	}

	inline static int32_t get_offset_of_messageToLabel_21() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messageToLabel_21)); }
	inline UILabel_t1795115428 * get_messageToLabel_21() const { return ___messageToLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_messageToLabel_21() { return &___messageToLabel_21; }
	inline void set_messageToLabel_21(UILabel_t1795115428 * value)
	{
		___messageToLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___messageToLabel_21, value);
	}

	inline static int32_t get_offset_of_messageContactsLabel_22() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messageContactsLabel_22)); }
	inline UILabel_t1795115428 * get_messageContactsLabel_22() const { return ___messageContactsLabel_22; }
	inline UILabel_t1795115428 ** get_address_of_messageContactsLabel_22() { return &___messageContactsLabel_22; }
	inline void set_messageContactsLabel_22(UILabel_t1795115428 * value)
	{
		___messageContactsLabel_22 = value;
		Il2CppCodeGenWriteBarrier(&___messageContactsLabel_22, value);
	}

	inline static int32_t get_offset_of_messagePlusButtonCollider_23() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messagePlusButtonCollider_23)); }
	inline BoxCollider_t22920061 * get_messagePlusButtonCollider_23() const { return ___messagePlusButtonCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of_messagePlusButtonCollider_23() { return &___messagePlusButtonCollider_23; }
	inline void set_messagePlusButtonCollider_23(BoxCollider_t22920061 * value)
	{
		___messagePlusButtonCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&___messagePlusButtonCollider_23, value);
	}

	inline static int32_t get_offset_of_messagePlusLabel_24() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messagePlusLabel_24)); }
	inline UILabel_t1795115428 * get_messagePlusLabel_24() const { return ___messagePlusLabel_24; }
	inline UILabel_t1795115428 ** get_address_of_messagePlusLabel_24() { return &___messagePlusLabel_24; }
	inline void set_messagePlusLabel_24(UILabel_t1795115428 * value)
	{
		___messagePlusLabel_24 = value;
		Il2CppCodeGenWriteBarrier(&___messagePlusLabel_24, value);
	}

	inline static int32_t get_offset_of_messageSeperator_25() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___messageSeperator_25)); }
	inline UISprite_t603616735 * get_messageSeperator_25() const { return ___messageSeperator_25; }
	inline UISprite_t603616735 ** get_address_of_messageSeperator_25() { return &___messageSeperator_25; }
	inline void set_messageSeperator_25(UISprite_t603616735 * value)
	{
		___messageSeperator_25 = value;
		Il2CppCodeGenWriteBarrier(&___messageSeperator_25, value);
	}

	inline static int32_t get_offset_of_keyboardCollider_26() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___keyboardCollider_26)); }
	inline BoxCollider_t22920061 * get_keyboardCollider_26() const { return ___keyboardCollider_26; }
	inline BoxCollider_t22920061 ** get_address_of_keyboardCollider_26() { return &___keyboardCollider_26; }
	inline void set_keyboardCollider_26(BoxCollider_t22920061 * value)
	{
		___keyboardCollider_26 = value;
		Il2CppCodeGenWriteBarrier(&___keyboardCollider_26, value);
	}

	inline static int32_t get_offset_of_keyboardBackground_27() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___keyboardBackground_27)); }
	inline UISprite_t603616735 * get_keyboardBackground_27() const { return ___keyboardBackground_27; }
	inline UISprite_t603616735 ** get_address_of_keyboardBackground_27() { return &___keyboardBackground_27; }
	inline void set_keyboardBackground_27(UISprite_t603616735 * value)
	{
		___keyboardBackground_27 = value;
		Il2CppCodeGenWriteBarrier(&___keyboardBackground_27, value);
	}

	inline static int32_t get_offset_of_keyboardForeground_28() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___keyboardForeground_28)); }
	inline UISprite_t603616735 * get_keyboardForeground_28() const { return ___keyboardForeground_28; }
	inline UISprite_t603616735 ** get_address_of_keyboardForeground_28() { return &___keyboardForeground_28; }
	inline void set_keyboardForeground_28(UISprite_t603616735 * value)
	{
		___keyboardForeground_28 = value;
		Il2CppCodeGenWriteBarrier(&___keyboardForeground_28, value);
	}

	inline static int32_t get_offset_of_keyboardLabel_29() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___keyboardLabel_29)); }
	inline UILabel_t1795115428 * get_keyboardLabel_29() const { return ___keyboardLabel_29; }
	inline UILabel_t1795115428 ** get_address_of_keyboardLabel_29() { return &___keyboardLabel_29; }
	inline void set_keyboardLabel_29(UILabel_t1795115428 * value)
	{
		___keyboardLabel_29 = value;
		Il2CppCodeGenWriteBarrier(&___keyboardLabel_29, value);
	}

	inline static int32_t get_offset_of_sendCollider_30() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___sendCollider_30)); }
	inline BoxCollider_t22920061 * get_sendCollider_30() const { return ___sendCollider_30; }
	inline BoxCollider_t22920061 ** get_address_of_sendCollider_30() { return &___sendCollider_30; }
	inline void set_sendCollider_30(BoxCollider_t22920061 * value)
	{
		___sendCollider_30 = value;
		Il2CppCodeGenWriteBarrier(&___sendCollider_30, value);
	}

	inline static int32_t get_offset_of_sendLabel_31() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___sendLabel_31)); }
	inline UILabel_t1795115428 * get_sendLabel_31() const { return ___sendLabel_31; }
	inline UILabel_t1795115428 ** get_address_of_sendLabel_31() { return &___sendLabel_31; }
	inline void set_sendLabel_31(UILabel_t1795115428 * value)
	{
		___sendLabel_31 = value;
		Il2CppCodeGenWriteBarrier(&___sendLabel_31, value);
	}

	inline static int32_t get_offset_of_searchCollider_32() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___searchCollider_32)); }
	inline BoxCollider_t22920061 * get_searchCollider_32() const { return ___searchCollider_32; }
	inline BoxCollider_t22920061 ** get_address_of_searchCollider_32() { return &___searchCollider_32; }
	inline void set_searchCollider_32(BoxCollider_t22920061 * value)
	{
		___searchCollider_32 = value;
		Il2CppCodeGenWriteBarrier(&___searchCollider_32, value);
	}

	inline static int32_t get_offset_of_searchLabel_33() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___searchLabel_33)); }
	inline UILabel_t1795115428 * get_searchLabel_33() const { return ___searchLabel_33; }
	inline UILabel_t1795115428 ** get_address_of_searchLabel_33() { return &___searchLabel_33; }
	inline void set_searchLabel_33(UILabel_t1795115428 * value)
	{
		___searchLabel_33 = value;
		Il2CppCodeGenWriteBarrier(&___searchLabel_33, value);
	}

	inline static int32_t get_offset_of_searchButton_34() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___searchButton_34)); }
	inline Transform_t3275118058 * get_searchButton_34() const { return ___searchButton_34; }
	inline Transform_t3275118058 ** get_address_of_searchButton_34() { return &___searchButton_34; }
	inline void set_searchButton_34(Transform_t3275118058 * value)
	{
		___searchButton_34 = value;
		Il2CppCodeGenWriteBarrier(&___searchButton_34, value);
	}

	inline static int32_t get_offset_of_contactsSeperator_35() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___contactsSeperator_35)); }
	inline UISprite_t603616735 * get_contactsSeperator_35() const { return ___contactsSeperator_35; }
	inline UISprite_t603616735 ** get_address_of_contactsSeperator_35() { return &___contactsSeperator_35; }
	inline void set_contactsSeperator_35(UISprite_t603616735 * value)
	{
		___contactsSeperator_35 = value;
		Il2CppCodeGenWriteBarrier(&___contactsSeperator_35, value);
	}

	inline static int32_t get_offset_of_contactsPanel_36() { return static_cast<int32_t>(offsetof(ChatCenterUIScaler_t436229885, ___contactsPanel_36)); }
	inline UIPanel_t1795085332 * get_contactsPanel_36() const { return ___contactsPanel_36; }
	inline UIPanel_t1795085332 ** get_address_of_contactsPanel_36() { return &___contactsPanel_36; }
	inline void set_contactsPanel_36(UIPanel_t1795085332 * value)
	{
		___contactsPanel_36 = value;
		Il2CppCodeGenWriteBarrier(&___contactsPanel_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
