﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdatePasswordNav
struct UpdatePasswordNav_t2102371893;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void UpdatePasswordNav::.ctor()
extern "C"  void UpdatePasswordNav__ctor_m4256045576 (UpdatePasswordNav_t2102371893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdatePasswordNav::Start()
extern "C"  void UpdatePasswordNav_Start_m4058908112 (UpdatePasswordNav_t2102371893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdatePasswordNav::UIClosing()
extern "C"  void UpdatePasswordNav_UIClosing_m1338946087 (UpdatePasswordNav_t2102371893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdatePasswordNav::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void UpdatePasswordNav_ScreenLoad_m4033340344 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdatePasswordNav::ScreenUnload(NavigationDirection)
extern "C"  void UpdatePasswordNav_ScreenUnload_m2330175024 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UpdatePasswordNav::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool UpdatePasswordNav_ValidateScreenNavigation_m1804539695 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UpdatePasswordNav::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * UpdatePasswordNav_SaveScreenContent_m1455114760 (UpdatePasswordNav_t2102371893 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
