﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeactivateShield
struct DeactivateShield_t2656914733;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DeactivateShield::.ctor()
extern "C"  void DeactivateShield__ctor_m1851762074 (DeactivateShield_t2656914733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeactivateShield::OnButtonClick()
extern "C"  void DeactivateShield_OnButtonClick_m2928757233 (DeactivateShield_t2656914733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeactivateShield::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void DeactivateShield_U3COnButtonClickU3Em__0_m1034524015 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeactivateShield::<OnButtonClick>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void DeactivateShield_U3COnButtonClickU3Em__1_m1338809294 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
