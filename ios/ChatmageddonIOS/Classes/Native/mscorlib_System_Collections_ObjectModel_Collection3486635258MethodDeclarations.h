﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<AttackHomeNav>
struct Collection_1_t3486635258;
// System.Collections.Generic.IList`1<AttackHomeNav>
struct IList_1_t190863809;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AttackHomeNav[]
struct AttackHomeNavU5BU5D_t3086173273;
// System.Collections.Generic.IEnumerator`1<AttackHomeNav>
struct IEnumerator_1_t1420414331;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"

// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::.ctor()
extern "C"  void Collection_1__ctor_m2204098855_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2204098855(__this, method) ((  void (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1__ctor_m2204098855_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1867887994_gshared (Collection_1_t3486635258 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1867887994(__this, ___list0, method) ((  void (*) (Collection_1_t3486635258 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1867887994_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3824502796_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3824502796(__this, method) ((  bool (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3824502796_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2555977971_gshared (Collection_1_t3486635258 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2555977971(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3486635258 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2555977971_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1279785848_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1279785848(__this, method) ((  Il2CppObject * (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1279785848_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1198171771_gshared (Collection_1_t3486635258 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1198171771(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3486635258 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1198171771_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2292748459_gshared (Collection_1_t3486635258 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2292748459(__this, ___value0, method) ((  bool (*) (Collection_1_t3486635258 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2292748459_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2267069197_gshared (Collection_1_t3486635258 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2267069197(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3486635258 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2267069197_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2343815346_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2343815346(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2343815346_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2585776388_gshared (Collection_1_t3486635258 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2585776388(__this, ___value0, method) ((  void (*) (Collection_1_t3486635258 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2585776388_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2190080107_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2190080107(__this, method) ((  bool (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2190080107_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m333649827_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m333649827(__this, method) ((  Il2CppObject * (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m333649827_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4276942918_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4276942918(__this, method) ((  bool (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4276942918_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2460084887_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2460084887(__this, method) ((  bool (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2460084887_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1040808154_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1040808154(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1040808154_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3890465941_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3890465941(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3890465941_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::Add(T)
extern "C"  void Collection_1_Add_m1929034262_gshared (Collection_1_t3486635258 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1929034262(__this, ___item0, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_Add_m1929034262_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::Clear()
extern "C"  void Collection_1_Clear_m1954238394_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1954238394(__this, method) ((  void (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_Clear_m1954238394_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::ClearItems()
extern "C"  void Collection_1_ClearItems_m581534268_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m581534268(__this, method) ((  void (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_ClearItems_m581534268_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::Contains(T)
extern "C"  bool Collection_1_Contains_m1502836912_gshared (Collection_1_t3486635258 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1502836912(__this, ___item0, method) ((  bool (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_Contains_m1502836912_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3901783938_gshared (Collection_1_t3486635258 * __this, AttackHomeNavU5BU5D_t3086173273* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3901783938(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3486635258 *, AttackHomeNavU5BU5D_t3086173273*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3901783938_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<AttackHomeNav>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3860289843_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3860289843(__this, method) ((  Il2CppObject* (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_GetEnumerator_m3860289843_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackHomeNav>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2921554060_gshared (Collection_1_t3486635258 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2921554060(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2921554060_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3492708241_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3492708241(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m3492708241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m177942972_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m177942972(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m177942972_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::Remove(T)
extern "C"  bool Collection_1_Remove_m660337121_gshared (Collection_1_t3486635258 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m660337121(__this, ___item0, method) ((  bool (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_Remove_m660337121_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m843200117_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m843200117(__this, ___index0, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m843200117_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2075807025_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2075807025(__this, ___index0, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2075807025_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackHomeNav>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m560025163_gshared (Collection_1_t3486635258 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m560025163(__this, method) ((  int32_t (*) (Collection_1_t3486635258 *, const MethodInfo*))Collection_1_get_Count_m560025163_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<AttackHomeNav>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3670602257_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3670602257(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3486635258 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3670602257_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3453139272_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3453139272(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3453139272_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1066884757_gshared (Collection_1_t3486635258 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1066884757(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3486635258 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1066884757_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2281986198_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2281986198(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2281986198_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<AttackHomeNav>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m1488446948_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1488446948(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1488446948_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackHomeNav>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m605306746_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m605306746(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m605306746_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1633155844_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1633155844(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1633155844_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackHomeNav>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1780998961_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1780998961(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1780998961_gshared)(__this /* static, unused */, ___list0, method)
