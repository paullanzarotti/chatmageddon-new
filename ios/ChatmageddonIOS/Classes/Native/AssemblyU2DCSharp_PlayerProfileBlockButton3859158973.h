﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerProfileModal
struct PlayerProfileModal_t3260625137;

#include "AssemblyU2DCSharp_DoubleStateButton1032633262.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerProfileBlockButton
struct  PlayerProfileBlockButton_t3859158973  : public DoubleStateButton_t1032633262
{
public:
	// PlayerProfileModal PlayerProfileBlockButton::item
	PlayerProfileModal_t3260625137 * ___item_13;
	// System.Boolean PlayerProfileBlockButton::blocked
	bool ___blocked_14;

public:
	inline static int32_t get_offset_of_item_13() { return static_cast<int32_t>(offsetof(PlayerProfileBlockButton_t3859158973, ___item_13)); }
	inline PlayerProfileModal_t3260625137 * get_item_13() const { return ___item_13; }
	inline PlayerProfileModal_t3260625137 ** get_address_of_item_13() { return &___item_13; }
	inline void set_item_13(PlayerProfileModal_t3260625137 * value)
	{
		___item_13 = value;
		Il2CppCodeGenWriteBarrier(&___item_13, value);
	}

	inline static int32_t get_offset_of_blocked_14() { return static_cast<int32_t>(offsetof(PlayerProfileBlockButton_t3859158973, ___blocked_14)); }
	inline bool get_blocked_14() const { return ___blocked_14; }
	inline bool* get_address_of_blocked_14() { return &___blocked_14; }
	inline void set_blocked_14(bool value)
	{
		___blocked_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
