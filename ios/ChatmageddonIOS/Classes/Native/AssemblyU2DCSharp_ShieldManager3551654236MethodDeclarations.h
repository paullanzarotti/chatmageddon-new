﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldManager
struct ShieldManager_t3551654236;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Shield
struct Shield_t3327121081;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void ShieldManager::.ctor()
extern "C"  void ShieldManager__ctor_m4081222723 (ShieldManager_t3551654236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldManager::Init()
extern "C"  void ShieldManager_Init_m1077422009 (ShieldManager_t3551654236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldManager::TrackShields()
extern "C"  void ShieldManager_TrackShields_m2233988312 (ShieldManager_t3551654236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShieldManager::TrackShield(Shield)
extern "C"  Il2CppObject * ShieldManager_TrackShield_m2768318226 (ShieldManager_t3551654236 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ShieldManager::CalculateTotalShieldTime(Shield)
extern "C"  double ShieldManager_CalculateTotalShieldTime_m1946866625 (ShieldManager_t3551654236 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldManager::UpdateShieldDisplays(System.Double,System.TimeSpan,Shield)
extern "C"  void ShieldManager_UpdateShieldDisplays_m3016092615 (ShieldManager_t3551654236 * __this, double ___percent0, TimeSpan_t3430258949  ___timeDifference1, Shield_t3327121081 * ___shield2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldManager::PlayShieldExpiredToast(Shield)
extern "C"  void ShieldManager_PlayShieldExpiredToast_m3379087177 (ShieldManager_t3551654236 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
