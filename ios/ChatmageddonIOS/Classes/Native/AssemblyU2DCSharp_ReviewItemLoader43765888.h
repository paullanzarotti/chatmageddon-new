﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviewItemLoader
struct  ReviewItemLoader_t43765888  : public MonoBehaviour_t1158329972
{
public:
	// UILabel ReviewItemLoader::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_2;
	// UnityEngine.GameObject ReviewItemLoader::m_DisplayItemObject
	GameObject_t1756533147 * ___m_DisplayItemObject_3;
	// UnityEngine.GameObject ReviewItemLoader::currentModel
	GameObject_t1756533147 * ___currentModel_4;

public:
	inline static int32_t get_offset_of_itemNameLabel_2() { return static_cast<int32_t>(offsetof(ReviewItemLoader_t43765888, ___itemNameLabel_2)); }
	inline UILabel_t1795115428 * get_itemNameLabel_2() const { return ___itemNameLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_2() { return &___itemNameLabel_2; }
	inline void set_itemNameLabel_2(UILabel_t1795115428 * value)
	{
		___itemNameLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_2, value);
	}

	inline static int32_t get_offset_of_m_DisplayItemObject_3() { return static_cast<int32_t>(offsetof(ReviewItemLoader_t43765888, ___m_DisplayItemObject_3)); }
	inline GameObject_t1756533147 * get_m_DisplayItemObject_3() const { return ___m_DisplayItemObject_3; }
	inline GameObject_t1756533147 ** get_address_of_m_DisplayItemObject_3() { return &___m_DisplayItemObject_3; }
	inline void set_m_DisplayItemObject_3(GameObject_t1756533147 * value)
	{
		___m_DisplayItemObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_DisplayItemObject_3, value);
	}

	inline static int32_t get_offset_of_currentModel_4() { return static_cast<int32_t>(offsetof(ReviewItemLoader_t43765888, ___currentModel_4)); }
	inline GameObject_t1756533147 * get_currentModel_4() const { return ___currentModel_4; }
	inline GameObject_t1756533147 ** get_address_of_currentModel_4() { return &___currentModel_4; }
	inline void set_currentModel_4(GameObject_t1756533147 * value)
	{
		___currentModel_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentModel_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
