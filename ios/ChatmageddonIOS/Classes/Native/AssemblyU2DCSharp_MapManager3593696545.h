﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// System.Collections.Generic.List`1<OnlineMapsMarker3D>
struct List_1_t4240903967;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3344362265.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager
struct  MapManager_t3593696545  : public MonoSingleton_1_t3344362265
{
public:
	// System.Single MapManager::mapDragTime
	float ___mapDragTime_3;
	// UnityEngine.GameObject MapManager::mapTileset
	GameObject_t1756533147 * ___mapTileset_4;
	// System.Boolean MapManager::mapUIActive
	bool ___mapUIActive_5;
	// System.Int32 MapManager::minZoomLevel
	int32_t ___minZoomLevel_6;
	// System.Int32 MapManager::maxZoomLevel
	int32_t ___maxZoomLevel_7;
	// MapState MapManager::currentState
	int32_t ___currentState_8;
	// UnityEngine.Vector2 MapManager::userPosition
	Vector2_t2243707579  ___userPosition_9;
	// UnityEngine.GameObject MapManager::defaultFriendPinPrefab
	GameObject_t1756533147 * ___defaultFriendPinPrefab_10;
	// UnityEngine.GameObject MapManager::defaultGroupPinPrefab
	GameObject_t1756533147 * ___defaultGroupPinPrefab_11;
	// UnityEngine.GameObject MapManager::defaultPlayerPinPrefab
	GameObject_t1756533147 * ___defaultPlayerPinPrefab_12;
	// UnityEngine.GameObject MapManager::defaultMinePinPrefab
	GameObject_t1756533147 * ___defaultMinePinPrefab_13;
	// System.Int32 MapManager::maxFriendsToDisplay
	int32_t ___maxFriendsToDisplay_14;
	// UnityEngine.Color MapManager::colorBlue
	Color_t2020392075  ___colorBlue_15;
	// UnityEngine.Color MapManager::colorGreen
	Color_t2020392075  ___colorGreen_16;
	// UnityEngine.Color MapManager::colorYellow
	Color_t2020392075  ___colorYellow_17;
	// UnityEngine.GameObject MapManager::radar
	GameObject_t1756533147 * ___radar_18;
	// OnlineMapsMarker3D MapManager::playerMarker
	OnlineMapsMarker3D_t576815539 * ___playerMarker_19;
	// System.Collections.Generic.List`1<OnlineMapsMarker3D> MapManager::activeFriendMarkers
	List_1_t4240903967 * ___activeFriendMarkers_20;
	// System.Collections.Generic.List`1<OnlineMapsMarker3D> MapManager::activeFriendGroupMarkers
	List_1_t4240903967 * ___activeFriendGroupMarkers_21;
	// System.Boolean MapManager::initalized
	bool ___initalized_22;
	// System.Boolean MapManager::playZoomSFX
	bool ___playZoomSFX_23;
	// System.Single MapManager::groupDistance
	float ___groupDistance_24;
	// System.Single MapManager::calcGroupDistance
	float ___calcGroupDistance_25;
	// UnityEngine.Coroutine MapManager::centerVelocityRoutine
	Coroutine_t2299508840 * ___centerVelocityRoutine_26;

public:
	inline static int32_t get_offset_of_mapDragTime_3() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___mapDragTime_3)); }
	inline float get_mapDragTime_3() const { return ___mapDragTime_3; }
	inline float* get_address_of_mapDragTime_3() { return &___mapDragTime_3; }
	inline void set_mapDragTime_3(float value)
	{
		___mapDragTime_3 = value;
	}

	inline static int32_t get_offset_of_mapTileset_4() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___mapTileset_4)); }
	inline GameObject_t1756533147 * get_mapTileset_4() const { return ___mapTileset_4; }
	inline GameObject_t1756533147 ** get_address_of_mapTileset_4() { return &___mapTileset_4; }
	inline void set_mapTileset_4(GameObject_t1756533147 * value)
	{
		___mapTileset_4 = value;
		Il2CppCodeGenWriteBarrier(&___mapTileset_4, value);
	}

	inline static int32_t get_offset_of_mapUIActive_5() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___mapUIActive_5)); }
	inline bool get_mapUIActive_5() const { return ___mapUIActive_5; }
	inline bool* get_address_of_mapUIActive_5() { return &___mapUIActive_5; }
	inline void set_mapUIActive_5(bool value)
	{
		___mapUIActive_5 = value;
	}

	inline static int32_t get_offset_of_minZoomLevel_6() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___minZoomLevel_6)); }
	inline int32_t get_minZoomLevel_6() const { return ___minZoomLevel_6; }
	inline int32_t* get_address_of_minZoomLevel_6() { return &___minZoomLevel_6; }
	inline void set_minZoomLevel_6(int32_t value)
	{
		___minZoomLevel_6 = value;
	}

	inline static int32_t get_offset_of_maxZoomLevel_7() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___maxZoomLevel_7)); }
	inline int32_t get_maxZoomLevel_7() const { return ___maxZoomLevel_7; }
	inline int32_t* get_address_of_maxZoomLevel_7() { return &___maxZoomLevel_7; }
	inline void set_maxZoomLevel_7(int32_t value)
	{
		___maxZoomLevel_7 = value;
	}

	inline static int32_t get_offset_of_currentState_8() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___currentState_8)); }
	inline int32_t get_currentState_8() const { return ___currentState_8; }
	inline int32_t* get_address_of_currentState_8() { return &___currentState_8; }
	inline void set_currentState_8(int32_t value)
	{
		___currentState_8 = value;
	}

	inline static int32_t get_offset_of_userPosition_9() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___userPosition_9)); }
	inline Vector2_t2243707579  get_userPosition_9() const { return ___userPosition_9; }
	inline Vector2_t2243707579 * get_address_of_userPosition_9() { return &___userPosition_9; }
	inline void set_userPosition_9(Vector2_t2243707579  value)
	{
		___userPosition_9 = value;
	}

	inline static int32_t get_offset_of_defaultFriendPinPrefab_10() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___defaultFriendPinPrefab_10)); }
	inline GameObject_t1756533147 * get_defaultFriendPinPrefab_10() const { return ___defaultFriendPinPrefab_10; }
	inline GameObject_t1756533147 ** get_address_of_defaultFriendPinPrefab_10() { return &___defaultFriendPinPrefab_10; }
	inline void set_defaultFriendPinPrefab_10(GameObject_t1756533147 * value)
	{
		___defaultFriendPinPrefab_10 = value;
		Il2CppCodeGenWriteBarrier(&___defaultFriendPinPrefab_10, value);
	}

	inline static int32_t get_offset_of_defaultGroupPinPrefab_11() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___defaultGroupPinPrefab_11)); }
	inline GameObject_t1756533147 * get_defaultGroupPinPrefab_11() const { return ___defaultGroupPinPrefab_11; }
	inline GameObject_t1756533147 ** get_address_of_defaultGroupPinPrefab_11() { return &___defaultGroupPinPrefab_11; }
	inline void set_defaultGroupPinPrefab_11(GameObject_t1756533147 * value)
	{
		___defaultGroupPinPrefab_11 = value;
		Il2CppCodeGenWriteBarrier(&___defaultGroupPinPrefab_11, value);
	}

	inline static int32_t get_offset_of_defaultPlayerPinPrefab_12() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___defaultPlayerPinPrefab_12)); }
	inline GameObject_t1756533147 * get_defaultPlayerPinPrefab_12() const { return ___defaultPlayerPinPrefab_12; }
	inline GameObject_t1756533147 ** get_address_of_defaultPlayerPinPrefab_12() { return &___defaultPlayerPinPrefab_12; }
	inline void set_defaultPlayerPinPrefab_12(GameObject_t1756533147 * value)
	{
		___defaultPlayerPinPrefab_12 = value;
		Il2CppCodeGenWriteBarrier(&___defaultPlayerPinPrefab_12, value);
	}

	inline static int32_t get_offset_of_defaultMinePinPrefab_13() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___defaultMinePinPrefab_13)); }
	inline GameObject_t1756533147 * get_defaultMinePinPrefab_13() const { return ___defaultMinePinPrefab_13; }
	inline GameObject_t1756533147 ** get_address_of_defaultMinePinPrefab_13() { return &___defaultMinePinPrefab_13; }
	inline void set_defaultMinePinPrefab_13(GameObject_t1756533147 * value)
	{
		___defaultMinePinPrefab_13 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMinePinPrefab_13, value);
	}

	inline static int32_t get_offset_of_maxFriendsToDisplay_14() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___maxFriendsToDisplay_14)); }
	inline int32_t get_maxFriendsToDisplay_14() const { return ___maxFriendsToDisplay_14; }
	inline int32_t* get_address_of_maxFriendsToDisplay_14() { return &___maxFriendsToDisplay_14; }
	inline void set_maxFriendsToDisplay_14(int32_t value)
	{
		___maxFriendsToDisplay_14 = value;
	}

	inline static int32_t get_offset_of_colorBlue_15() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___colorBlue_15)); }
	inline Color_t2020392075  get_colorBlue_15() const { return ___colorBlue_15; }
	inline Color_t2020392075 * get_address_of_colorBlue_15() { return &___colorBlue_15; }
	inline void set_colorBlue_15(Color_t2020392075  value)
	{
		___colorBlue_15 = value;
	}

	inline static int32_t get_offset_of_colorGreen_16() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___colorGreen_16)); }
	inline Color_t2020392075  get_colorGreen_16() const { return ___colorGreen_16; }
	inline Color_t2020392075 * get_address_of_colorGreen_16() { return &___colorGreen_16; }
	inline void set_colorGreen_16(Color_t2020392075  value)
	{
		___colorGreen_16 = value;
	}

	inline static int32_t get_offset_of_colorYellow_17() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___colorYellow_17)); }
	inline Color_t2020392075  get_colorYellow_17() const { return ___colorYellow_17; }
	inline Color_t2020392075 * get_address_of_colorYellow_17() { return &___colorYellow_17; }
	inline void set_colorYellow_17(Color_t2020392075  value)
	{
		___colorYellow_17 = value;
	}

	inline static int32_t get_offset_of_radar_18() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___radar_18)); }
	inline GameObject_t1756533147 * get_radar_18() const { return ___radar_18; }
	inline GameObject_t1756533147 ** get_address_of_radar_18() { return &___radar_18; }
	inline void set_radar_18(GameObject_t1756533147 * value)
	{
		___radar_18 = value;
		Il2CppCodeGenWriteBarrier(&___radar_18, value);
	}

	inline static int32_t get_offset_of_playerMarker_19() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___playerMarker_19)); }
	inline OnlineMapsMarker3D_t576815539 * get_playerMarker_19() const { return ___playerMarker_19; }
	inline OnlineMapsMarker3D_t576815539 ** get_address_of_playerMarker_19() { return &___playerMarker_19; }
	inline void set_playerMarker_19(OnlineMapsMarker3D_t576815539 * value)
	{
		___playerMarker_19 = value;
		Il2CppCodeGenWriteBarrier(&___playerMarker_19, value);
	}

	inline static int32_t get_offset_of_activeFriendMarkers_20() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___activeFriendMarkers_20)); }
	inline List_1_t4240903967 * get_activeFriendMarkers_20() const { return ___activeFriendMarkers_20; }
	inline List_1_t4240903967 ** get_address_of_activeFriendMarkers_20() { return &___activeFriendMarkers_20; }
	inline void set_activeFriendMarkers_20(List_1_t4240903967 * value)
	{
		___activeFriendMarkers_20 = value;
		Il2CppCodeGenWriteBarrier(&___activeFriendMarkers_20, value);
	}

	inline static int32_t get_offset_of_activeFriendGroupMarkers_21() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___activeFriendGroupMarkers_21)); }
	inline List_1_t4240903967 * get_activeFriendGroupMarkers_21() const { return ___activeFriendGroupMarkers_21; }
	inline List_1_t4240903967 ** get_address_of_activeFriendGroupMarkers_21() { return &___activeFriendGroupMarkers_21; }
	inline void set_activeFriendGroupMarkers_21(List_1_t4240903967 * value)
	{
		___activeFriendGroupMarkers_21 = value;
		Il2CppCodeGenWriteBarrier(&___activeFriendGroupMarkers_21, value);
	}

	inline static int32_t get_offset_of_initalized_22() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___initalized_22)); }
	inline bool get_initalized_22() const { return ___initalized_22; }
	inline bool* get_address_of_initalized_22() { return &___initalized_22; }
	inline void set_initalized_22(bool value)
	{
		___initalized_22 = value;
	}

	inline static int32_t get_offset_of_playZoomSFX_23() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___playZoomSFX_23)); }
	inline bool get_playZoomSFX_23() const { return ___playZoomSFX_23; }
	inline bool* get_address_of_playZoomSFX_23() { return &___playZoomSFX_23; }
	inline void set_playZoomSFX_23(bool value)
	{
		___playZoomSFX_23 = value;
	}

	inline static int32_t get_offset_of_groupDistance_24() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___groupDistance_24)); }
	inline float get_groupDistance_24() const { return ___groupDistance_24; }
	inline float* get_address_of_groupDistance_24() { return &___groupDistance_24; }
	inline void set_groupDistance_24(float value)
	{
		___groupDistance_24 = value;
	}

	inline static int32_t get_offset_of_calcGroupDistance_25() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___calcGroupDistance_25)); }
	inline float get_calcGroupDistance_25() const { return ___calcGroupDistance_25; }
	inline float* get_address_of_calcGroupDistance_25() { return &___calcGroupDistance_25; }
	inline void set_calcGroupDistance_25(float value)
	{
		___calcGroupDistance_25 = value;
	}

	inline static int32_t get_offset_of_centerVelocityRoutine_26() { return static_cast<int32_t>(offsetof(MapManager_t3593696545, ___centerVelocityRoutine_26)); }
	inline Coroutine_t2299508840 * get_centerVelocityRoutine_26() const { return ___centerVelocityRoutine_26; }
	inline Coroutine_t2299508840 ** get_address_of_centerVelocityRoutine_26() { return &___centerVelocityRoutine_26; }
	inline void set_centerVelocityRoutine_26(Coroutine_t2299508840 * value)
	{
		___centerVelocityRoutine_26 = value;
		Il2CppCodeGenWriteBarrier(&___centerVelocityRoutine_26, value);
	}
};

struct MapManager_t3593696545_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> MapManager::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_28;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_28() { return static_cast<int32_t>(offsetof(MapManager_t3593696545_StaticFields, ___U3CU3Ef__amU24cache0_28)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_28() const { return ___U3CU3Ef__amU24cache0_28; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_28() { return &___U3CU3Ef__amU24cache0_28; }
	inline void set_U3CU3Ef__amU24cache0_28(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
