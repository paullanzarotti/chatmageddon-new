﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnibillError>
struct List_1_t1122980919;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat657710593.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnibillError>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1264014406_gshared (Enumerator_t657710593 * __this, List_1_t1122980919 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1264014406(__this, ___l0, method) ((  void (*) (Enumerator_t657710593 *, List_1_t1122980919 *, const MethodInfo*))Enumerator__ctor_m1264014406_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnibillError>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2908561892_gshared (Enumerator_t657710593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2908561892(__this, method) ((  void (*) (Enumerator_t657710593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2908561892_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnibillError>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1774303474_gshared (Enumerator_t657710593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1774303474(__this, method) ((  Il2CppObject * (*) (Enumerator_t657710593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1774303474_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnibillError>::Dispose()
extern "C"  void Enumerator_Dispose_m3547335023_gshared (Enumerator_t657710593 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3547335023(__this, method) ((  void (*) (Enumerator_t657710593 *, const MethodInfo*))Enumerator_Dispose_m3547335023_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnibillError>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2559114604_gshared (Enumerator_t657710593 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2559114604(__this, method) ((  void (*) (Enumerator_t657710593 *, const MethodInfo*))Enumerator_VerifyState_m2559114604_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnibillError>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2612989948_gshared (Enumerator_t657710593 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2612989948(__this, method) ((  bool (*) (Enumerator_t657710593 *, const MethodInfo*))Enumerator_MoveNext_m2612989948_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnibillError>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2720946539_gshared (Enumerator_t657710593 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2720946539(__this, method) ((  int32_t (*) (Enumerator_t657710593 *, const MethodInfo*))Enumerator_get_Current_m2720946539_gshared)(__this, method)
