﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MusicSwitch
struct MusicSwitch_t1246590877;
// SFXSwitch
struct SFXSwitch_t3206288945;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioNavScreen
struct  AudioNavScreen_t3651651169  : public NavigationScreen_t2333230110
{
public:
	// MusicSwitch AudioNavScreen::musicSwitch
	MusicSwitch_t1246590877 * ___musicSwitch_3;
	// SFXSwitch AudioNavScreen::sfxSwitch
	SFXSwitch_t3206288945 * ___sfxSwitch_4;
	// System.Boolean AudioNavScreen::UIclosing
	bool ___UIclosing_5;

public:
	inline static int32_t get_offset_of_musicSwitch_3() { return static_cast<int32_t>(offsetof(AudioNavScreen_t3651651169, ___musicSwitch_3)); }
	inline MusicSwitch_t1246590877 * get_musicSwitch_3() const { return ___musicSwitch_3; }
	inline MusicSwitch_t1246590877 ** get_address_of_musicSwitch_3() { return &___musicSwitch_3; }
	inline void set_musicSwitch_3(MusicSwitch_t1246590877 * value)
	{
		___musicSwitch_3 = value;
		Il2CppCodeGenWriteBarrier(&___musicSwitch_3, value);
	}

	inline static int32_t get_offset_of_sfxSwitch_4() { return static_cast<int32_t>(offsetof(AudioNavScreen_t3651651169, ___sfxSwitch_4)); }
	inline SFXSwitch_t3206288945 * get_sfxSwitch_4() const { return ___sfxSwitch_4; }
	inline SFXSwitch_t3206288945 ** get_address_of_sfxSwitch_4() { return &___sfxSwitch_4; }
	inline void set_sfxSwitch_4(SFXSwitch_t3206288945 * value)
	{
		___sfxSwitch_4 = value;
		Il2CppCodeGenWriteBarrier(&___sfxSwitch_4, value);
	}

	inline static int32_t get_offset_of_UIclosing_5() { return static_cast<int32_t>(offsetof(AudioNavScreen_t3651651169, ___UIclosing_5)); }
	inline bool get_UIclosing_5() const { return ___UIclosing_5; }
	inline bool* get_address_of_UIclosing_5() { return &___UIclosing_5; }
	inline void set_UIclosing_5(bool value)
	{
		___UIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
