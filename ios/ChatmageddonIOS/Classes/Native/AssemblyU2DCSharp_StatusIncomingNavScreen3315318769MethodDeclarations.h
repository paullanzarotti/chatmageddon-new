﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusIncomingNavScreen
struct StatusIncomingNavScreen_t3315318769;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void StatusIncomingNavScreen::.ctor()
extern "C"  void StatusIncomingNavScreen__ctor_m2573723718 (StatusIncomingNavScreen_t3315318769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusIncomingNavScreen::Start()
extern "C"  void StatusIncomingNavScreen_Start_m3311394550 (StatusIncomingNavScreen_t3315318769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusIncomingNavScreen::UIClosing()
extern "C"  void StatusIncomingNavScreen_UIClosing_m1635963691 (StatusIncomingNavScreen_t3315318769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusIncomingNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void StatusIncomingNavScreen_ScreenLoad_m3601666858 (StatusIncomingNavScreen_t3315318769 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusIncomingNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void StatusIncomingNavScreen_ScreenUnload_m839731602 (StatusIncomingNavScreen_t3315318769 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusIncomingNavScreen::UpdateUI()
extern "C"  void StatusIncomingNavScreen_UpdateUI_m4280956585 (StatusIncomingNavScreen_t3315318769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusIncomingNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool StatusIncomingNavScreen_ValidateScreenNavigation_m3752689027 (StatusIncomingNavScreen_t3315318769 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
