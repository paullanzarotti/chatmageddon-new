﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToasterUIScaler
struct ToasterUIScaler_t2057419170;

#include "codegen/il2cpp-codegen.h"

// System.Void ToasterUIScaler::.ctor()
extern "C"  void ToasterUIScaler__ctor_m3385393671 (ToasterUIScaler_t2057419170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToasterUIScaler::GlobalUIScale()
extern "C"  void ToasterUIScaler_GlobalUIScale_m4023268432 (ToasterUIScaler_t2057419170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
