﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatContact
struct ChatContact_t3760700014;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatContact::.ctor(System.Boolean,System.String,System.String,System.String,System.String)
extern "C"  void ChatContact__ctor_m940657580 (ChatContact_t3760700014 * __this, bool ___seperator0, String_t* ___firstName1, String_t* ___lastName2, String_t* ___username3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
