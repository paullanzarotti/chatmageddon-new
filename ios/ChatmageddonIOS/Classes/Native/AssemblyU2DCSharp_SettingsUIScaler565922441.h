﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// TweenPosition
struct TweenPosition_t1144714832;
// System.Collections.Generic.List`1<TweenPosition>
struct List_1_t513835964;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsUIScaler
struct  SettingsUIScaler_t565922441  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite SettingsUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UISprite SettingsUIScaler::seperator
	UISprite_t603616735 * ___seperator_15;
	// UnityEngine.Transform SettingsUIScaler::navBack
	Transform_t3275118058 * ___navBack_16;
	// UnityEngine.Transform SettingsUIScaler::navCancel
	Transform_t3275118058 * ___navCancel_17;
	// UnityEngine.Transform SettingsUIScaler::navSave
	Transform_t3275118058 * ___navSave_18;
	// UIPanel SettingsUIScaler::homeScreenPanel
	UIPanel_t1795085332 * ___homeScreenPanel_19;
	// UIPanel SettingsUIScaler::profileScreenPanel
	UIPanel_t1795085332 * ___profileScreenPanel_20;
	// UISprite SettingsUIScaler::logoutBackground
	UISprite_t603616735 * ___logoutBackground_21;
	// UnityEngine.BoxCollider SettingsUIScaler::logoutCollider
	BoxCollider_t22920061 * ___logoutCollider_22;
	// TweenPosition SettingsUIScaler::activeStartLeftTween
	TweenPosition_t1144714832 * ___activeStartLeftTween_23;
	// System.Collections.Generic.List`1<TweenPosition> SettingsUIScaler::rightStartActiveTweens
	List_1_t513835964 * ___rightStartActiveTweens_24;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_seperator_15() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___seperator_15)); }
	inline UISprite_t603616735 * get_seperator_15() const { return ___seperator_15; }
	inline UISprite_t603616735 ** get_address_of_seperator_15() { return &___seperator_15; }
	inline void set_seperator_15(UISprite_t603616735 * value)
	{
		___seperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___seperator_15, value);
	}

	inline static int32_t get_offset_of_navBack_16() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___navBack_16)); }
	inline Transform_t3275118058 * get_navBack_16() const { return ___navBack_16; }
	inline Transform_t3275118058 ** get_address_of_navBack_16() { return &___navBack_16; }
	inline void set_navBack_16(Transform_t3275118058 * value)
	{
		___navBack_16 = value;
		Il2CppCodeGenWriteBarrier(&___navBack_16, value);
	}

	inline static int32_t get_offset_of_navCancel_17() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___navCancel_17)); }
	inline Transform_t3275118058 * get_navCancel_17() const { return ___navCancel_17; }
	inline Transform_t3275118058 ** get_address_of_navCancel_17() { return &___navCancel_17; }
	inline void set_navCancel_17(Transform_t3275118058 * value)
	{
		___navCancel_17 = value;
		Il2CppCodeGenWriteBarrier(&___navCancel_17, value);
	}

	inline static int32_t get_offset_of_navSave_18() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___navSave_18)); }
	inline Transform_t3275118058 * get_navSave_18() const { return ___navSave_18; }
	inline Transform_t3275118058 ** get_address_of_navSave_18() { return &___navSave_18; }
	inline void set_navSave_18(Transform_t3275118058 * value)
	{
		___navSave_18 = value;
		Il2CppCodeGenWriteBarrier(&___navSave_18, value);
	}

	inline static int32_t get_offset_of_homeScreenPanel_19() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___homeScreenPanel_19)); }
	inline UIPanel_t1795085332 * get_homeScreenPanel_19() const { return ___homeScreenPanel_19; }
	inline UIPanel_t1795085332 ** get_address_of_homeScreenPanel_19() { return &___homeScreenPanel_19; }
	inline void set_homeScreenPanel_19(UIPanel_t1795085332 * value)
	{
		___homeScreenPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___homeScreenPanel_19, value);
	}

	inline static int32_t get_offset_of_profileScreenPanel_20() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___profileScreenPanel_20)); }
	inline UIPanel_t1795085332 * get_profileScreenPanel_20() const { return ___profileScreenPanel_20; }
	inline UIPanel_t1795085332 ** get_address_of_profileScreenPanel_20() { return &___profileScreenPanel_20; }
	inline void set_profileScreenPanel_20(UIPanel_t1795085332 * value)
	{
		___profileScreenPanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___profileScreenPanel_20, value);
	}

	inline static int32_t get_offset_of_logoutBackground_21() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___logoutBackground_21)); }
	inline UISprite_t603616735 * get_logoutBackground_21() const { return ___logoutBackground_21; }
	inline UISprite_t603616735 ** get_address_of_logoutBackground_21() { return &___logoutBackground_21; }
	inline void set_logoutBackground_21(UISprite_t603616735 * value)
	{
		___logoutBackground_21 = value;
		Il2CppCodeGenWriteBarrier(&___logoutBackground_21, value);
	}

	inline static int32_t get_offset_of_logoutCollider_22() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___logoutCollider_22)); }
	inline BoxCollider_t22920061 * get_logoutCollider_22() const { return ___logoutCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_logoutCollider_22() { return &___logoutCollider_22; }
	inline void set_logoutCollider_22(BoxCollider_t22920061 * value)
	{
		___logoutCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___logoutCollider_22, value);
	}

	inline static int32_t get_offset_of_activeStartLeftTween_23() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___activeStartLeftTween_23)); }
	inline TweenPosition_t1144714832 * get_activeStartLeftTween_23() const { return ___activeStartLeftTween_23; }
	inline TweenPosition_t1144714832 ** get_address_of_activeStartLeftTween_23() { return &___activeStartLeftTween_23; }
	inline void set_activeStartLeftTween_23(TweenPosition_t1144714832 * value)
	{
		___activeStartLeftTween_23 = value;
		Il2CppCodeGenWriteBarrier(&___activeStartLeftTween_23, value);
	}

	inline static int32_t get_offset_of_rightStartActiveTweens_24() { return static_cast<int32_t>(offsetof(SettingsUIScaler_t565922441, ___rightStartActiveTweens_24)); }
	inline List_1_t513835964 * get_rightStartActiveTweens_24() const { return ___rightStartActiveTweens_24; }
	inline List_1_t513835964 ** get_address_of_rightStartActiveTweens_24() { return &___rightStartActiveTweens_24; }
	inline void set_rightStartActiveTweens_24(List_1_t513835964 * value)
	{
		___rightStartActiveTweens_24 = value;
		Il2CppCodeGenWriteBarrier(&___rightStartActiveTweens_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
