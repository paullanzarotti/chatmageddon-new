﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatLoaderUIScaler
struct ChatLoaderUIScaler_t296991803;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatLoaderUIScaler::.ctor()
extern "C"  void ChatLoaderUIScaler__ctor_m1047954862 (ChatLoaderUIScaler_t296991803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatLoaderUIScaler::GlobalUIScale()
extern "C"  void ChatLoaderUIScaler_GlobalUIScale_m3848706501 (ChatLoaderUIScaler_t296991803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
