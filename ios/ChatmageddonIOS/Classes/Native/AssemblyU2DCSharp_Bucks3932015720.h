﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bucks
struct  Bucks_t3932015720  : public Il2CppObject
{
public:
	// System.String Bucks::displayName
	String_t* ___displayName_0;
	// System.String Bucks::internalName
	String_t* ___internalName_1;
	// System.Int32 Bucks::amount
	int32_t ___amount_2;
	// System.String Bucks::cost
	String_t* ___cost_3;
	// System.String Bucks::itunesID
	String_t* ___itunesID_4;
	// System.String Bucks::googleplayID
	String_t* ___googleplayID_5;

public:
	inline static int32_t get_offset_of_displayName_0() { return static_cast<int32_t>(offsetof(Bucks_t3932015720, ___displayName_0)); }
	inline String_t* get_displayName_0() const { return ___displayName_0; }
	inline String_t** get_address_of_displayName_0() { return &___displayName_0; }
	inline void set_displayName_0(String_t* value)
	{
		___displayName_0 = value;
		Il2CppCodeGenWriteBarrier(&___displayName_0, value);
	}

	inline static int32_t get_offset_of_internalName_1() { return static_cast<int32_t>(offsetof(Bucks_t3932015720, ___internalName_1)); }
	inline String_t* get_internalName_1() const { return ___internalName_1; }
	inline String_t** get_address_of_internalName_1() { return &___internalName_1; }
	inline void set_internalName_1(String_t* value)
	{
		___internalName_1 = value;
		Il2CppCodeGenWriteBarrier(&___internalName_1, value);
	}

	inline static int32_t get_offset_of_amount_2() { return static_cast<int32_t>(offsetof(Bucks_t3932015720, ___amount_2)); }
	inline int32_t get_amount_2() const { return ___amount_2; }
	inline int32_t* get_address_of_amount_2() { return &___amount_2; }
	inline void set_amount_2(int32_t value)
	{
		___amount_2 = value;
	}

	inline static int32_t get_offset_of_cost_3() { return static_cast<int32_t>(offsetof(Bucks_t3932015720, ___cost_3)); }
	inline String_t* get_cost_3() const { return ___cost_3; }
	inline String_t** get_address_of_cost_3() { return &___cost_3; }
	inline void set_cost_3(String_t* value)
	{
		___cost_3 = value;
		Il2CppCodeGenWriteBarrier(&___cost_3, value);
	}

	inline static int32_t get_offset_of_itunesID_4() { return static_cast<int32_t>(offsetof(Bucks_t3932015720, ___itunesID_4)); }
	inline String_t* get_itunesID_4() const { return ___itunesID_4; }
	inline String_t** get_address_of_itunesID_4() { return &___itunesID_4; }
	inline void set_itunesID_4(String_t* value)
	{
		___itunesID_4 = value;
		Il2CppCodeGenWriteBarrier(&___itunesID_4, value);
	}

	inline static int32_t get_offset_of_googleplayID_5() { return static_cast<int32_t>(offsetof(Bucks_t3932015720, ___googleplayID_5)); }
	inline String_t* get_googleplayID_5() const { return ___googleplayID_5; }
	inline String_t** get_address_of_googleplayID_5() { return &___googleplayID_5; }
	inline void set_googleplayID_5(String_t* value)
	{
		___googleplayID_5 = value;
		Il2CppCodeGenWriteBarrier(&___googleplayID_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
