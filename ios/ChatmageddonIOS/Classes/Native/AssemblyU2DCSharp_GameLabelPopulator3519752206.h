﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameLabelPopulator
struct  GameLabelPopulator_t3519752206  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color GameLabelPopulator::positiveColour
	Color_t2020392075  ___positiveColour_2;
	// UnityEngine.Color GameLabelPopulator::neutralColour
	Color_t2020392075  ___neutralColour_3;
	// UnityEngine.Color GameLabelPopulator::negativeColour
	Color_t2020392075  ___negativeColour_4;
	// UILabel GameLabelPopulator::descriptionLabel
	UILabel_t1795115428 * ___descriptionLabel_5;
	// UISprite GameLabelPopulator::descriptionBackground
	UISprite_t603616735 * ___descriptionBackground_6;
	// TweenAlpha GameLabelPopulator::descriptionTween
	TweenAlpha_t2421518635 * ___descriptionTween_7;
	// UILabel GameLabelPopulator::amountLabel
	UILabel_t1795115428 * ___amountLabel_8;
	// UISprite GameLabelPopulator::amountBackground
	UISprite_t603616735 * ___amountBackground_9;
	// TweenAlpha GameLabelPopulator::amountTween
	TweenAlpha_t2421518635 * ___amountTween_10;

public:
	inline static int32_t get_offset_of_positiveColour_2() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___positiveColour_2)); }
	inline Color_t2020392075  get_positiveColour_2() const { return ___positiveColour_2; }
	inline Color_t2020392075 * get_address_of_positiveColour_2() { return &___positiveColour_2; }
	inline void set_positiveColour_2(Color_t2020392075  value)
	{
		___positiveColour_2 = value;
	}

	inline static int32_t get_offset_of_neutralColour_3() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___neutralColour_3)); }
	inline Color_t2020392075  get_neutralColour_3() const { return ___neutralColour_3; }
	inline Color_t2020392075 * get_address_of_neutralColour_3() { return &___neutralColour_3; }
	inline void set_neutralColour_3(Color_t2020392075  value)
	{
		___neutralColour_3 = value;
	}

	inline static int32_t get_offset_of_negativeColour_4() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___negativeColour_4)); }
	inline Color_t2020392075  get_negativeColour_4() const { return ___negativeColour_4; }
	inline Color_t2020392075 * get_address_of_negativeColour_4() { return &___negativeColour_4; }
	inline void set_negativeColour_4(Color_t2020392075  value)
	{
		___negativeColour_4 = value;
	}

	inline static int32_t get_offset_of_descriptionLabel_5() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___descriptionLabel_5)); }
	inline UILabel_t1795115428 * get_descriptionLabel_5() const { return ___descriptionLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_descriptionLabel_5() { return &___descriptionLabel_5; }
	inline void set_descriptionLabel_5(UILabel_t1795115428 * value)
	{
		___descriptionLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionLabel_5, value);
	}

	inline static int32_t get_offset_of_descriptionBackground_6() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___descriptionBackground_6)); }
	inline UISprite_t603616735 * get_descriptionBackground_6() const { return ___descriptionBackground_6; }
	inline UISprite_t603616735 ** get_address_of_descriptionBackground_6() { return &___descriptionBackground_6; }
	inline void set_descriptionBackground_6(UISprite_t603616735 * value)
	{
		___descriptionBackground_6 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionBackground_6, value);
	}

	inline static int32_t get_offset_of_descriptionTween_7() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___descriptionTween_7)); }
	inline TweenAlpha_t2421518635 * get_descriptionTween_7() const { return ___descriptionTween_7; }
	inline TweenAlpha_t2421518635 ** get_address_of_descriptionTween_7() { return &___descriptionTween_7; }
	inline void set_descriptionTween_7(TweenAlpha_t2421518635 * value)
	{
		___descriptionTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionTween_7, value);
	}

	inline static int32_t get_offset_of_amountLabel_8() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___amountLabel_8)); }
	inline UILabel_t1795115428 * get_amountLabel_8() const { return ___amountLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_amountLabel_8() { return &___amountLabel_8; }
	inline void set_amountLabel_8(UILabel_t1795115428 * value)
	{
		___amountLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___amountLabel_8, value);
	}

	inline static int32_t get_offset_of_amountBackground_9() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___amountBackground_9)); }
	inline UISprite_t603616735 * get_amountBackground_9() const { return ___amountBackground_9; }
	inline UISprite_t603616735 ** get_address_of_amountBackground_9() { return &___amountBackground_9; }
	inline void set_amountBackground_9(UISprite_t603616735 * value)
	{
		___amountBackground_9 = value;
		Il2CppCodeGenWriteBarrier(&___amountBackground_9, value);
	}

	inline static int32_t get_offset_of_amountTween_10() { return static_cast<int32_t>(offsetof(GameLabelPopulator_t3519752206, ___amountTween_10)); }
	inline TweenAlpha_t2421518635 * get_amountTween_10() const { return ___amountTween_10; }
	inline TweenAlpha_t2421518635 ** get_address_of_amountTween_10() { return &___amountTween_10; }
	inline void set_amountTween_10(TweenAlpha_t2421518635 * value)
	{
		___amountTween_10 = value;
		Il2CppCodeGenWriteBarrier(&___amountTween_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
