﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_HSV
struct CameraFilterPack_Colors_HSV_t1033920197;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_HSV::.ctor()
extern "C"  void CameraFilterPack_Colors_HSV__ctor_m1626629882 (CameraFilterPack_Colors_HSV_t1033920197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_HSV::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_HSV_get_material_m1368272817 (CameraFilterPack_Colors_HSV_t1033920197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::Start()
extern "C"  void CameraFilterPack_Colors_HSV_Start_m2388387914 (CameraFilterPack_Colors_HSV_t1033920197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_HSV_OnRenderImage_m1141460178 (CameraFilterPack_Colors_HSV_t1033920197 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::OnValidate()
extern "C"  void CameraFilterPack_Colors_HSV_OnValidate_m2519129819 (CameraFilterPack_Colors_HSV_t1033920197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::Update()
extern "C"  void CameraFilterPack_Colors_HSV_Update_m1073720397 (CameraFilterPack_Colors_HSV_t1033920197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::OnDisable()
extern "C"  void CameraFilterPack_Colors_HSV_OnDisable_m751572353 (CameraFilterPack_Colors_HSV_t1033920197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
