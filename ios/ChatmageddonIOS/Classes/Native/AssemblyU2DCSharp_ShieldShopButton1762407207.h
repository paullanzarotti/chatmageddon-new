﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShieldModal
struct ShieldModal_t4049164426;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldShopButton
struct  ShieldShopButton_t1762407207  : public SFXButton_t792651341
{
public:
	// ShieldModal ShieldShopButton::modal
	ShieldModal_t4049164426 * ___modal_5;

public:
	inline static int32_t get_offset_of_modal_5() { return static_cast<int32_t>(offsetof(ShieldShopButton_t1762407207, ___modal_5)); }
	inline ShieldModal_t4049164426 * get_modal_5() const { return ___modal_5; }
	inline ShieldModal_t4049164426 ** get_address_of_modal_5() { return &___modal_5; }
	inline void set_modal_5(ShieldModal_t4049164426 * value)
	{
		___modal_5 = value;
		Il2CppCodeGenWriteBarrier(&___modal_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
