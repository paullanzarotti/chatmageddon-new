﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va538827551MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m902665151(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4277112867 *, Dictionary_2_t1279085728 *, const MethodInfo*))ValueCollection__ctor_m820028653_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3268448273(__this, ___item0, method) ((  void (*) (ValueCollection_t4277112867 *, Vector3_t2243707580 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1693977355_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3209931956(__this, method) ((  void (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1361468192_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2396816467(__this, ___item0, method) ((  bool (*) (ValueCollection_t4277112867 *, Vector3_t2243707580 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1154957181_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2110220734(__this, ___item0, method) ((  bool (*) (ValueCollection_t4277112867 *, Vector3_t2243707580 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1422136202_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1457392946(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m946147108_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2630704172(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4277112867 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2443423836_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3238068801(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3383749643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2400412650(__this, method) ((  bool (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m505000502_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3974714188(__this, method) ((  bool (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1381110256_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m963633762(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2197981756_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2793990740(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4277112867 *, Vector3U5BU5D_t1172311765*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3805350316_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4184413439(__this, method) ((  Enumerator_t2965618492  (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_GetEnumerator_m1642151537_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.GameObject,UnityEngine.Vector3>::get_Count()
#define ValueCollection_get_Count_m3173710192(__this, method) ((  int32_t (*) (ValueCollection_t4277112867 *, const MethodInfo*))ValueCollection_get_Count_m1851670216_gshared)(__this, method)
