﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Thread
struct Thread_t241561612;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2595592884;
// System.Threading.ThreadStart
struct ThreadStart_t3437517264;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsThreadManager
struct  OnlineMapsThreadManager_t2145254635  : public Il2CppObject
{
public:

public:
};

struct OnlineMapsThreadManager_t2145254635_StaticFields
{
public:
	// System.Threading.Thread OnlineMapsThreadManager::thread
	Thread_t241561612 * ___thread_0;
	// System.Collections.Generic.List`1<System.Action> OnlineMapsThreadManager::threadActions
	List_1_t2595592884 * ___threadActions_1;
	// System.Threading.ThreadStart OnlineMapsThreadManager::<>f__mg$cache0
	ThreadStart_t3437517264 * ___U3CU3Ef__mgU24cache0_2;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(OnlineMapsThreadManager_t2145254635_StaticFields, ___thread_0)); }
	inline Thread_t241561612 * get_thread_0() const { return ___thread_0; }
	inline Thread_t241561612 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(Thread_t241561612 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier(&___thread_0, value);
	}

	inline static int32_t get_offset_of_threadActions_1() { return static_cast<int32_t>(offsetof(OnlineMapsThreadManager_t2145254635_StaticFields, ___threadActions_1)); }
	inline List_1_t2595592884 * get_threadActions_1() const { return ___threadActions_1; }
	inline List_1_t2595592884 ** get_address_of_threadActions_1() { return &___threadActions_1; }
	inline void set_threadActions_1(List_1_t2595592884 * value)
	{
		___threadActions_1 = value;
		Il2CppCodeGenWriteBarrier(&___threadActions_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(OnlineMapsThreadManager_t2145254635_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline ThreadStart_t3437517264 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline ThreadStart_t3437517264 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(ThreadStart_t3437517264 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
