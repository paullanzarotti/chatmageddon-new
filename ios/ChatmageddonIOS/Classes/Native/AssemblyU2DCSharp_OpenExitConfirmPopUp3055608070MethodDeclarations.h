﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenExitConfirmPopUp
struct OpenExitConfirmPopUp_t3055608070;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenExitConfirmPopUp::.ctor()
extern "C"  void OpenExitConfirmPopUp__ctor_m603897955 (OpenExitConfirmPopUp_t3055608070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenExitConfirmPopUp::OnButtonClick()
extern "C"  void OpenExitConfirmPopUp_OnButtonClick_m651506542 (OpenExitConfirmPopUp_t3055608070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
