﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.HTTPClient/<pump>c__Iterator0
struct U3CpumpU3Ec__Iterator0_t873001920;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Unibill.Impl.HTTPClient/<pump>c__Iterator0::.ctor()
extern "C"  void U3CpumpU3Ec__Iterator0__ctor_m1950603125 (U3CpumpU3Ec__Iterator0_t873001920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.HTTPClient/<pump>c__Iterator0::MoveNext()
extern "C"  bool U3CpumpU3Ec__Iterator0_MoveNext_m4012177683 (U3CpumpU3Ec__Iterator0_t873001920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.HTTPClient/<pump>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CpumpU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1800515835 (U3CpumpU3Ec__Iterator0_t873001920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.HTTPClient/<pump>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CpumpU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3505993091 (U3CpumpU3Ec__Iterator0_t873001920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.HTTPClient/<pump>c__Iterator0::Dispose()
extern "C"  void U3CpumpU3Ec__Iterator0_Dispose_m323232044 (U3CpumpU3Ec__Iterator0_t873001920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.HTTPClient/<pump>c__Iterator0::Reset()
extern "C"  void U3CpumpU3Ec__Iterator0_Reset_m3673949990 (U3CpumpU3Ec__Iterator0_t873001920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
