﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<ChatContactsListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m3655073650(__this, method) ((  void (*) (BaseInfiniteListItem_1_t483143304 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<ChatContactsListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m440158107(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t483143304 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatContactsListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m1901157629(__this, method) ((  void (*) (BaseInfiniteListItem_1_t483143304 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatContactsListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m1955422276(__this, method) ((  void (*) (BaseInfiniteListItem_1_t483143304 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatContactsListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m589783679(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t483143304 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<ChatContactsListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m868613442(__this, method) ((  void (*) (BaseInfiniteListItem_1_t483143304 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatContactsListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m2535613325(__this, method) ((  void (*) (BaseInfiniteListItem_1_t483143304 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
