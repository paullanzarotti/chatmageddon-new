﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LongNotificationModal
struct LongNotificationModal_t834782082;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationOkButton
struct  NotificationOkButton_t3431423113  : public SFXButton_t792651341
{
public:
	// LongNotificationModal NotificationOkButton::modal
	LongNotificationModal_t834782082 * ___modal_5;

public:
	inline static int32_t get_offset_of_modal_5() { return static_cast<int32_t>(offsetof(NotificationOkButton_t3431423113, ___modal_5)); }
	inline LongNotificationModal_t834782082 * get_modal_5() const { return ___modal_5; }
	inline LongNotificationModal_t834782082 ** get_address_of_modal_5() { return &___modal_5; }
	inline void set_modal_5(LongNotificationModal_t834782082 * value)
	{
		___modal_5 = value;
		Il2CppCodeGenWriteBarrier(&___modal_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
