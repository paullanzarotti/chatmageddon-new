﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>
struct U3CSceneLoadU3Ec__Iterator0_t2795172931;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CSceneLoadU3Ec__Iterator0__ctor_m751785660_gshared (U3CSceneLoadU3Ec__Iterator0_t2795172931 * __this, const MethodInfo* method);
#define U3CSceneLoadU3Ec__Iterator0__ctor_m751785660(__this, method) ((  void (*) (U3CSceneLoadU3Ec__Iterator0_t2795172931 *, const MethodInfo*))U3CSceneLoadU3Ec__Iterator0__ctor_m751785660_gshared)(__this, method)
// System.Boolean BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CSceneLoadU3Ec__Iterator0_MoveNext_m673094800_gshared (U3CSceneLoadU3Ec__Iterator0_t2795172931 * __this, const MethodInfo* method);
#define U3CSceneLoadU3Ec__Iterator0_MoveNext_m673094800(__this, method) ((  bool (*) (U3CSceneLoadU3Ec__Iterator0_t2795172931 *, const MethodInfo*))U3CSceneLoadU3Ec__Iterator0_MoveNext_m673094800_gshared)(__this, method)
// System.Object BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSceneLoadU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3267786718_gshared (U3CSceneLoadU3Ec__Iterator0_t2795172931 * __this, const MethodInfo* method);
#define U3CSceneLoadU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3267786718(__this, method) ((  Il2CppObject * (*) (U3CSceneLoadU3Ec__Iterator0_t2795172931 *, const MethodInfo*))U3CSceneLoadU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3267786718_gshared)(__this, method)
// System.Object BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSceneLoadU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3116258038_gshared (U3CSceneLoadU3Ec__Iterator0_t2795172931 * __this, const MethodInfo* method);
#define U3CSceneLoadU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3116258038(__this, method) ((  Il2CppObject * (*) (U3CSceneLoadU3Ec__Iterator0_t2795172931 *, const MethodInfo*))U3CSceneLoadU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3116258038_gshared)(__this, method)
// System.Void BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CSceneLoadU3Ec__Iterator0_Dispose_m700570229_gshared (U3CSceneLoadU3Ec__Iterator0_t2795172931 * __this, const MethodInfo* method);
#define U3CSceneLoadU3Ec__Iterator0_Dispose_m700570229(__this, method) ((  void (*) (U3CSceneLoadU3Ec__Iterator0_t2795172931 *, const MethodInfo*))U3CSceneLoadU3Ec__Iterator0_Dispose_m700570229_gshared)(__this, method)
// System.Void BaseSceneManager`1/<SceneLoad>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CSceneLoadU3Ec__Iterator0_Reset_m1475827623_gshared (U3CSceneLoadU3Ec__Iterator0_t2795172931 * __this, const MethodInfo* method);
#define U3CSceneLoadU3Ec__Iterator0_Reset_m1475827623(__this, method) ((  void (*) (U3CSceneLoadU3Ec__Iterator0_t2795172931 *, const MethodInfo*))U3CSceneLoadU3Ec__Iterator0_Reset_m1475827623_gshared)(__this, method)
