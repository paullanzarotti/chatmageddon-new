﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Multiply
struct CameraFilterPack_Blend2Camera_Multiply_t4028476918;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Multiply::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Multiply__ctor_m1652389469 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Multiply::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Multiply_get_material_m2925796460 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Multiply_Start_m1911522869 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Multiply_OnRenderImage_m1020364261 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Multiply_OnValidate_m167187546 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Multiply_Update_m2851031092 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Multiply_OnEnable_m1553114677 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Multiply_OnDisable_m316702466 (CameraFilterPack_Blend2Camera_Multiply_t4028476918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
