﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIAnimation/OnAnimationLooped
struct OnAnimationLooped_t2655604368;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void NGUIAnimation/OnAnimationLooped::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAnimationLooped__ctor_m3241902173 (OnAnimationLooped_t2655604368 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationLooped::Invoke()
extern "C"  void OnAnimationLooped_Invoke_m4237509781 (OnAnimationLooped_t2655604368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NGUIAnimation/OnAnimationLooped::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAnimationLooped_BeginInvoke_m2745852158 (OnAnimationLooped_t2655604368 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationLooped::EndInvoke(System.IAsyncResult)
extern "C"  void OnAnimationLooped_EndInvoke_m548370139 (OnAnimationLooped_t2655604368 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
