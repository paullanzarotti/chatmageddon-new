﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapButtonOptions
struct MapButtonOptions_t2133642156;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"

// System.Void MapButtonOptions::.ctor()
extern "C"  void MapButtonOptions__ctor_m2241660745 (MapButtonOptions_t2133642156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::OnButtonClick()
extern "C"  void MapButtonOptions_OnButtonClick_m155990688 (MapButtonOptions_t2133642156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::SwitchMapButtons()
extern "C"  void MapButtonOptions_SwitchMapButtons_m2067643816 (MapButtonOptions_t2133642156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::ForceCloseMapButtons(System.Boolean)
extern "C"  void MapButtonOptions_ForceCloseMapButtons_m4221459284 (MapButtonOptions_t2133642156 * __this, bool ___fade0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::ForceOpenMapButtons()
extern "C"  void MapButtonOptions_ForceOpenMapButtons_m1308532213 (MapButtonOptions_t2133642156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::FadeButtonSwitch(System.Boolean)
extern "C"  void MapButtonOptions_FadeButtonSwitch_m912519504 (MapButtonOptions_t2133642156 * __this, bool ___fade0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::SwitchMapOptions(System.Boolean)
extern "C"  void MapButtonOptions_SwitchMapOptions_m1032150426 (MapButtonOptions_t2133642156 * __this, bool ___display0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapButtonOptions::SetActiveStateButton(MapState)
extern "C"  void MapButtonOptions_SetActiveStateButton_m2008650381 (MapButtonOptions_t2133642156 * __this, int32_t ___activeState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
