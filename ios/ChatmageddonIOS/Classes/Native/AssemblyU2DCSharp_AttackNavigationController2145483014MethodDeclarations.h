﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackNavigationController
struct AttackNavigationController_t2145483014;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackNavigationController::.ctor()
extern "C"  void AttackNavigationController__ctor_m1355048385 (AttackNavigationController_t2145483014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::Start()
extern "C"  void AttackNavigationController_Start_m3412924657 (AttackNavigationController_t2145483014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AttackNavigationController::FirstLoadWait()
extern "C"  Il2CppObject * AttackNavigationController_FirstLoadWait_m3430009894 (AttackNavigationController_t2145483014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::ActivateInputBlock(System.Boolean)
extern "C"  void AttackNavigationController_ActivateInputBlock_m3135762452 (AttackNavigationController_t2145483014 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::NavigateBackwards()
extern "C"  void AttackNavigationController_NavigateBackwards_m1639053074 (AttackNavigationController_t2145483014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::NavigateForwards(AttackNavScreen)
extern "C"  void AttackNavigationController_NavigateForwards_m1262402255 (AttackNavigationController_t2145483014 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackNavigationController::ValidateNavigation(AttackNavScreen,NavigationDirection)
extern "C"  bool AttackNavigationController_ValidateNavigation_m3652341875 (AttackNavigationController_t2145483014 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::LoadNavScreen(AttackNavScreen,NavigationDirection,System.Boolean)
extern "C"  void AttackNavigationController_LoadNavScreen_m4145610359 (AttackNavigationController_t2145483014 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::UnloadNavScreen(AttackNavScreen,NavigationDirection)
extern "C"  void AttackNavigationController_UnloadNavScreen_m1246042403 (AttackNavigationController_t2145483014 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController::PopulateTitleBar(AttackNavScreen)
extern "C"  void AttackNavigationController_PopulateTitleBar_m1774037087 (AttackNavigationController_t2145483014 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
