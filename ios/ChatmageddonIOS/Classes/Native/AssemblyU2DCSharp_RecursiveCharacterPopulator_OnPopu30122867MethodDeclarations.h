﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecursiveCharacterPopulator/OnPopulationUpdate
struct OnPopulationUpdate_t30122867;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void RecursiveCharacterPopulator/OnPopulationUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPopulationUpdate__ctor_m160573834 (OnPopulationUpdate_t30122867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator/OnPopulationUpdate::Invoke()
extern "C"  void OnPopulationUpdate_Invoke_m307144896 (OnPopulationUpdate_t30122867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult RecursiveCharacterPopulator/OnPopulationUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPopulationUpdate_BeginInvoke_m893949181 (OnPopulationUpdate_t30122867 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator/OnPopulationUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void OnPopulationUpdate_EndInvoke_m4247092764 (OnPopulationUpdate_t30122867 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
