﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_BooleanSwitch869062888.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BooleanSwitchButton
struct  BooleanSwitchButton_t1408929844  : public BooleanSwitch_t869062888
{
public:
	// SFX BooleanSwitchButton::sfxToPlay
	int32_t ___sfxToPlay_3;

public:
	inline static int32_t get_offset_of_sfxToPlay_3() { return static_cast<int32_t>(offsetof(BooleanSwitchButton_t1408929844, ___sfxToPlay_3)); }
	inline int32_t get_sfxToPlay_3() const { return ___sfxToPlay_3; }
	inline int32_t* get_address_of_sfxToPlay_3() { return &___sfxToPlay_3; }
	inline void set_sfxToPlay_3(int32_t value)
	{
		___sfxToPlay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
