﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Vision_Plasma
struct CameraFilterPack_Vision_Plasma_t1366310808;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Vision_Plasma::.ctor()
extern "C"  void CameraFilterPack_Vision_Plasma__ctor_m3885931109 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Plasma::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Vision_Plasma_get_material_m811747358 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::Start()
extern "C"  void CameraFilterPack_Vision_Plasma_Start_m2021331685 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Vision_Plasma_OnRenderImage_m219044725 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::OnValidate()
extern "C"  void CameraFilterPack_Vision_Plasma_OnValidate_m1302024824 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::Update()
extern "C"  void CameraFilterPack_Vision_Plasma_Update_m55785510 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::OnDisable()
extern "C"  void CameraFilterPack_Vision_Plasma_OnDisable_m2125335152 (CameraFilterPack_Vision_Plasma_t1366310808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
