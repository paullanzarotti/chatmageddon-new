﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_PinLight
struct CameraFilterPack_Blend2Camera_PinLight_t3427254393;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_PinLight::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_PinLight__ctor_m641154790 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_PinLight::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_PinLight_get_material_m284702713 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::Start()
extern "C"  void CameraFilterPack_Blend2Camera_PinLight_Start_m310977862 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_PinLight_OnRenderImage_m2901892862 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_PinLight_OnValidate_m1158985303 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::Update()
extern "C"  void CameraFilterPack_Blend2Camera_PinLight_Update_m561847549 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_PinLight_OnEnable_m3028966054 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_PinLight_OnDisable_m2168452217 (CameraFilterPack_Blend2Camera_PinLight_t3427254393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
