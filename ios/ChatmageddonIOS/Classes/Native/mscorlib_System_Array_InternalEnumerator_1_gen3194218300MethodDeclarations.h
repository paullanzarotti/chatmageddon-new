﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3194218300.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22335466038.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1831368629_gshared (InternalEnumerator_1_t3194218300 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1831368629(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3194218300 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1831368629_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m510021449_gshared (InternalEnumerator_1_t3194218300 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m510021449(__this, method) ((  void (*) (InternalEnumerator_1_t3194218300 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m510021449_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2193398509_gshared (InternalEnumerator_1_t3194218300 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2193398509(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3194218300 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2193398509_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m444117394_gshared (InternalEnumerator_1_t3194218300 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m444117394(__this, method) ((  void (*) (InternalEnumerator_1_t3194218300 *, const MethodInfo*))InternalEnumerator_1_Dispose_m444117394_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m466968985_gshared (InternalEnumerator_1_t3194218300 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m466968985(__this, method) ((  bool (*) (InternalEnumerator_1_t3194218300 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m466968985_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2335466038  InternalEnumerator_1_get_Current_m946515682_gshared (InternalEnumerator_1_t3194218300 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m946515682(__this, method) ((  KeyValuePair_2_t2335466038  (*) (InternalEnumerator_1_t3194218300 *, const MethodInfo*))InternalEnumerator_1_get_Current_m946515682_gshared)(__this, method)
