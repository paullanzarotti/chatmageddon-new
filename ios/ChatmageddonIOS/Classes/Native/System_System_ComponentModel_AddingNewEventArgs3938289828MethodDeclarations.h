﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.AddingNewEventArgs
struct AddingNewEventArgs_t3938289828;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.AddingNewEventArgs::.ctor()
extern "C"  void AddingNewEventArgs__ctor_m3368829417 (AddingNewEventArgs_t3938289828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AddingNewEventArgs::.ctor(System.Object)
extern "C"  void AddingNewEventArgs__ctor_m2628633791 (AddingNewEventArgs_t3938289828 * __this, Il2CppObject * ___newObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.AddingNewEventArgs::get_NewObject()
extern "C"  Il2CppObject * AddingNewEventArgs_get_NewObject_m2294604776 (AddingNewEventArgs_t3938289828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AddingNewEventArgs::set_NewObject(System.Object)
extern "C"  void AddingNewEventArgs_set_NewObject_m405364099 (AddingNewEventArgs_t3938289828 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
