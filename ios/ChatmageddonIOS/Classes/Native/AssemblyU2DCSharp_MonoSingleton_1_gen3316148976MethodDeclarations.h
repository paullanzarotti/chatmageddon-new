﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<HomeRightUIManager>::.ctor()
#define MonoSingleton_1__ctor_m784900216(__this, method) ((  void (*) (MonoSingleton_1_t3316148976 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeRightUIManager>::Awake()
#define MonoSingleton_1_Awake_m3744191673(__this, method) ((  void (*) (MonoSingleton_1_t3316148976 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<HomeRightUIManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3427929931(__this /* static, unused */, method) ((  HomeRightUIManager_t3565483256 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<HomeRightUIManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3983699719(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<HomeRightUIManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2402575818(__this, method) ((  void (*) (MonoSingleton_1_t3316148976 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeRightUIManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3074778878(__this, method) ((  void (*) (MonoSingleton_1_t3316148976 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeRightUIManager>::.cctor()
#define MonoSingleton_1__cctor_m3206421383(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
