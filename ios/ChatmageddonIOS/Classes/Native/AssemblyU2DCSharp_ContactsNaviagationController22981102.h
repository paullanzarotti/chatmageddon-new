﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// PhoneNumberNavigateBackButton
struct PhoneNumberNavigateBackButton_t3971219339;

#include "AssemblyU2DCSharp_NavigationController_2_gen65533235.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactsNaviagationController
struct  ContactsNaviagationController_t22981102  : public NavigationController_2_t65533235
{
public:
	// UnityEngine.Vector3 ContactsNaviagationController::activePos
	Vector3_t2243707580  ___activePos_8;
	// UnityEngine.Vector3 ContactsNaviagationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_9;
	// UnityEngine.Vector3 ContactsNaviagationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_10;
	// UnityEngine.GameObject ContactsNaviagationController::contentObject
	GameObject_t1756533147 * ___contentObject_11;
	// UILabel ContactsNaviagationController::titleLabel
	UILabel_t1795115428 * ___titleLabel_12;
	// UnityEngine.GameObject ContactsNaviagationController::yourNumberObject
	GameObject_t1756533147 * ___yourNumberObject_13;
	// UnityEngine.GameObject ContactsNaviagationController::verificationObject
	GameObject_t1756533147 * ___verificationObject_14;
	// PhoneNumberNavigateBackButton ContactsNaviagationController::mainNavigateBackwardButton
	PhoneNumberNavigateBackButton_t3971219339 * ___mainNavigateBackwardButton_15;

public:
	inline static int32_t get_offset_of_activePos_8() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___activePos_8)); }
	inline Vector3_t2243707580  get_activePos_8() const { return ___activePos_8; }
	inline Vector3_t2243707580 * get_address_of_activePos_8() { return &___activePos_8; }
	inline void set_activePos_8(Vector3_t2243707580  value)
	{
		___activePos_8 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_9() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___leftInactivePos_9)); }
	inline Vector3_t2243707580  get_leftInactivePos_9() const { return ___leftInactivePos_9; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_9() { return &___leftInactivePos_9; }
	inline void set_leftInactivePos_9(Vector3_t2243707580  value)
	{
		___leftInactivePos_9 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_10() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___rightInactivePos_10)); }
	inline Vector3_t2243707580  get_rightInactivePos_10() const { return ___rightInactivePos_10; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_10() { return &___rightInactivePos_10; }
	inline void set_rightInactivePos_10(Vector3_t2243707580  value)
	{
		___rightInactivePos_10 = value;
	}

	inline static int32_t get_offset_of_contentObject_11() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___contentObject_11)); }
	inline GameObject_t1756533147 * get_contentObject_11() const { return ___contentObject_11; }
	inline GameObject_t1756533147 ** get_address_of_contentObject_11() { return &___contentObject_11; }
	inline void set_contentObject_11(GameObject_t1756533147 * value)
	{
		___contentObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___contentObject_11, value);
	}

	inline static int32_t get_offset_of_titleLabel_12() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___titleLabel_12)); }
	inline UILabel_t1795115428 * get_titleLabel_12() const { return ___titleLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_12() { return &___titleLabel_12; }
	inline void set_titleLabel_12(UILabel_t1795115428 * value)
	{
		___titleLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_12, value);
	}

	inline static int32_t get_offset_of_yourNumberObject_13() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___yourNumberObject_13)); }
	inline GameObject_t1756533147 * get_yourNumberObject_13() const { return ___yourNumberObject_13; }
	inline GameObject_t1756533147 ** get_address_of_yourNumberObject_13() { return &___yourNumberObject_13; }
	inline void set_yourNumberObject_13(GameObject_t1756533147 * value)
	{
		___yourNumberObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___yourNumberObject_13, value);
	}

	inline static int32_t get_offset_of_verificationObject_14() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___verificationObject_14)); }
	inline GameObject_t1756533147 * get_verificationObject_14() const { return ___verificationObject_14; }
	inline GameObject_t1756533147 ** get_address_of_verificationObject_14() { return &___verificationObject_14; }
	inline void set_verificationObject_14(GameObject_t1756533147 * value)
	{
		___verificationObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___verificationObject_14, value);
	}

	inline static int32_t get_offset_of_mainNavigateBackwardButton_15() { return static_cast<int32_t>(offsetof(ContactsNaviagationController_t22981102, ___mainNavigateBackwardButton_15)); }
	inline PhoneNumberNavigateBackButton_t3971219339 * get_mainNavigateBackwardButton_15() const { return ___mainNavigateBackwardButton_15; }
	inline PhoneNumberNavigateBackButton_t3971219339 ** get_address_of_mainNavigateBackwardButton_15() { return &___mainNavigateBackwardButton_15; }
	inline void set_mainNavigateBackwardButton_15(PhoneNumberNavigateBackButton_t3971219339 * value)
	{
		___mainNavigateBackwardButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateBackwardButton_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
