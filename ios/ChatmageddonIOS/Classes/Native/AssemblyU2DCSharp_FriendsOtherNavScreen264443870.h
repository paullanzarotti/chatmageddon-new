﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsOtherNavScreen
struct  FriendsOtherNavScreen_t264443870  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject FriendsOtherNavScreen::hasContactsUI
	GameObject_t1756533147 * ___hasContactsUI_3;
	// UnityEngine.GameObject FriendsOtherNavScreen::noContactsUI
	GameObject_t1756533147 * ___noContactsUI_4;
	// System.Boolean FriendsOtherNavScreen::UIclosing
	bool ___UIclosing_5;

public:
	inline static int32_t get_offset_of_hasContactsUI_3() { return static_cast<int32_t>(offsetof(FriendsOtherNavScreen_t264443870, ___hasContactsUI_3)); }
	inline GameObject_t1756533147 * get_hasContactsUI_3() const { return ___hasContactsUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasContactsUI_3() { return &___hasContactsUI_3; }
	inline void set_hasContactsUI_3(GameObject_t1756533147 * value)
	{
		___hasContactsUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasContactsUI_3, value);
	}

	inline static int32_t get_offset_of_noContactsUI_4() { return static_cast<int32_t>(offsetof(FriendsOtherNavScreen_t264443870, ___noContactsUI_4)); }
	inline GameObject_t1756533147 * get_noContactsUI_4() const { return ___noContactsUI_4; }
	inline GameObject_t1756533147 ** get_address_of_noContactsUI_4() { return &___noContactsUI_4; }
	inline void set_noContactsUI_4(GameObject_t1756533147 * value)
	{
		___noContactsUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___noContactsUI_4, value);
	}

	inline static int32_t get_offset_of_UIclosing_5() { return static_cast<int32_t>(offsetof(FriendsOtherNavScreen_t264443870, ___UIclosing_5)); }
	inline bool get_UIclosing_5() const { return ___UIclosing_5; }
	inline bool* get_address_of_UIclosing_5() { return &___UIclosing_5; }
	inline void set_UIclosing_5(bool value)
	{
		___UIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
