﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.Facebook/<fetchImageAtUrl>c__Iterator0
struct U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.Facebook/<fetchImageAtUrl>c__Iterator0::.ctor()
extern "C"  void U3CfetchImageAtUrlU3Ec__Iterator0__ctor_m1078802115 (U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.Facebook/<fetchImageAtUrl>c__Iterator0::MoveNext()
extern "C"  bool U3CfetchImageAtUrlU3Ec__Iterator0_MoveNext_m4202916829 (U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Prime31.Facebook/<fetchImageAtUrl>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CfetchImageAtUrlU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2001947269 (U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Prime31.Facebook/<fetchImageAtUrl>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CfetchImageAtUrlU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m860657085 (U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook/<fetchImageAtUrl>c__Iterator0::Dispose()
extern "C"  void U3CfetchImageAtUrlU3Ec__Iterator0_Dispose_m3697669038 (U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook/<fetchImageAtUrl>c__Iterator0::Reset()
extern "C"  void U3CfetchImageAtUrlU3Ec__Iterator0_Reset_m3037621668 (U3CfetchImageAtUrlU3Ec__Iterator0_t3452867102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
