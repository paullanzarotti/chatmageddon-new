﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuffer/<CreateTileParent>c__AnonStorey0
struct U3CCreateTileParentU3Ec__AnonStorey0_t2137665930;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"

// System.Void OnlineMapsBuffer/<CreateTileParent>c__AnonStorey0::.ctor()
extern "C"  void U3CCreateTileParentU3Ec__AnonStorey0__ctor_m1603950637 (U3CCreateTileParentU3Ec__AnonStorey0_t2137665930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsBuffer/<CreateTileParent>c__AnonStorey0::<>m__0(OnlineMapsTile)
extern "C"  bool U3CCreateTileParentU3Ec__AnonStorey0_U3CU3Em__0_m2977606642 (U3CCreateTileParentU3Ec__AnonStorey0_t2137665930 * __this, OnlineMapsTile_t21329940 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
