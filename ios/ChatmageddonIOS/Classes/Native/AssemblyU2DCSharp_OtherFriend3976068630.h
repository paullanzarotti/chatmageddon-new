﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherFriend
struct  OtherFriend_t3976068630  : public Il2CppObject
{
public:
	// System.Boolean OtherFriend::alphaSeperator
	bool ___alphaSeperator_0;
	// System.String OtherFriend::contactFirstName
	String_t* ___contactFirstName_1;
	// System.String OtherFriend::contactLastName
	String_t* ___contactLastName_2;
	// System.Collections.Generic.List`1<System.String> OtherFriend::contactPhoneNumbers
	List_1_t1398341365 * ___contactPhoneNumbers_3;

public:
	inline static int32_t get_offset_of_alphaSeperator_0() { return static_cast<int32_t>(offsetof(OtherFriend_t3976068630, ___alphaSeperator_0)); }
	inline bool get_alphaSeperator_0() const { return ___alphaSeperator_0; }
	inline bool* get_address_of_alphaSeperator_0() { return &___alphaSeperator_0; }
	inline void set_alphaSeperator_0(bool value)
	{
		___alphaSeperator_0 = value;
	}

	inline static int32_t get_offset_of_contactFirstName_1() { return static_cast<int32_t>(offsetof(OtherFriend_t3976068630, ___contactFirstName_1)); }
	inline String_t* get_contactFirstName_1() const { return ___contactFirstName_1; }
	inline String_t** get_address_of_contactFirstName_1() { return &___contactFirstName_1; }
	inline void set_contactFirstName_1(String_t* value)
	{
		___contactFirstName_1 = value;
		Il2CppCodeGenWriteBarrier(&___contactFirstName_1, value);
	}

	inline static int32_t get_offset_of_contactLastName_2() { return static_cast<int32_t>(offsetof(OtherFriend_t3976068630, ___contactLastName_2)); }
	inline String_t* get_contactLastName_2() const { return ___contactLastName_2; }
	inline String_t** get_address_of_contactLastName_2() { return &___contactLastName_2; }
	inline void set_contactLastName_2(String_t* value)
	{
		___contactLastName_2 = value;
		Il2CppCodeGenWriteBarrier(&___contactLastName_2, value);
	}

	inline static int32_t get_offset_of_contactPhoneNumbers_3() { return static_cast<int32_t>(offsetof(OtherFriend_t3976068630, ___contactPhoneNumbers_3)); }
	inline List_1_t1398341365 * get_contactPhoneNumbers_3() const { return ___contactPhoneNumbers_3; }
	inline List_1_t1398341365 ** get_address_of_contactPhoneNumbers_3() { return &___contactPhoneNumbers_3; }
	inline void set_contactPhoneNumbers_3(List_1_t1398341365 * value)
	{
		___contactPhoneNumbers_3 = value;
		Il2CppCodeGenWriteBarrier(&___contactPhoneNumbers_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
