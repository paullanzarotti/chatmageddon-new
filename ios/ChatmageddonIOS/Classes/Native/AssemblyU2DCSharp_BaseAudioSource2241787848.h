﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseAudioSource
struct  BaseAudioSource_t2241787848  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BaseAudioSource::sourceActive
	bool ___sourceActive_2;
	// UnityEngine.AudioSource BaseAudioSource::source
	AudioSource_t1135106623 * ___source_3;
	// System.Single BaseAudioSource::maxVolume
	float ___maxVolume_4;
	// System.Single BaseAudioSource::minVolume
	float ___minVolume_5;
	// System.Single BaseAudioSource::audioDuration
	float ___audioDuration_6;
	// System.Boolean BaseAudioSource::checking
	bool ___checking_7;
	// UnityEngine.Coroutine BaseAudioSource::fadeInRoutine
	Coroutine_t2299508840 * ___fadeInRoutine_8;
	// UnityEngine.Coroutine BaseAudioSource::fadeOutRoutine
	Coroutine_t2299508840 * ___fadeOutRoutine_9;

public:
	inline static int32_t get_offset_of_sourceActive_2() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___sourceActive_2)); }
	inline bool get_sourceActive_2() const { return ___sourceActive_2; }
	inline bool* get_address_of_sourceActive_2() { return &___sourceActive_2; }
	inline void set_sourceActive_2(bool value)
	{
		___sourceActive_2 = value;
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___source_3)); }
	inline AudioSource_t1135106623 * get_source_3() const { return ___source_3; }
	inline AudioSource_t1135106623 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(AudioSource_t1135106623 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier(&___source_3, value);
	}

	inline static int32_t get_offset_of_maxVolume_4() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___maxVolume_4)); }
	inline float get_maxVolume_4() const { return ___maxVolume_4; }
	inline float* get_address_of_maxVolume_4() { return &___maxVolume_4; }
	inline void set_maxVolume_4(float value)
	{
		___maxVolume_4 = value;
	}

	inline static int32_t get_offset_of_minVolume_5() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___minVolume_5)); }
	inline float get_minVolume_5() const { return ___minVolume_5; }
	inline float* get_address_of_minVolume_5() { return &___minVolume_5; }
	inline void set_minVolume_5(float value)
	{
		___minVolume_5 = value;
	}

	inline static int32_t get_offset_of_audioDuration_6() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___audioDuration_6)); }
	inline float get_audioDuration_6() const { return ___audioDuration_6; }
	inline float* get_address_of_audioDuration_6() { return &___audioDuration_6; }
	inline void set_audioDuration_6(float value)
	{
		___audioDuration_6 = value;
	}

	inline static int32_t get_offset_of_checking_7() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___checking_7)); }
	inline bool get_checking_7() const { return ___checking_7; }
	inline bool* get_address_of_checking_7() { return &___checking_7; }
	inline void set_checking_7(bool value)
	{
		___checking_7 = value;
	}

	inline static int32_t get_offset_of_fadeInRoutine_8() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___fadeInRoutine_8)); }
	inline Coroutine_t2299508840 * get_fadeInRoutine_8() const { return ___fadeInRoutine_8; }
	inline Coroutine_t2299508840 ** get_address_of_fadeInRoutine_8() { return &___fadeInRoutine_8; }
	inline void set_fadeInRoutine_8(Coroutine_t2299508840 * value)
	{
		___fadeInRoutine_8 = value;
		Il2CppCodeGenWriteBarrier(&___fadeInRoutine_8, value);
	}

	inline static int32_t get_offset_of_fadeOutRoutine_9() { return static_cast<int32_t>(offsetof(BaseAudioSource_t2241787848, ___fadeOutRoutine_9)); }
	inline Coroutine_t2299508840 * get_fadeOutRoutine_9() const { return ___fadeOutRoutine_9; }
	inline Coroutine_t2299508840 ** get_address_of_fadeOutRoutine_9() { return &___fadeOutRoutine_9; }
	inline void set_fadeOutRoutine_9(Coroutine_t2299508840 * value)
	{
		___fadeOutRoutine_9 = value;
		Il2CppCodeGenWriteBarrier(&___fadeOutRoutine_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
