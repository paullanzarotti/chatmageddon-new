﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Collections.Generic.IEqualityComparer`1<System.Char>
struct IEqualityComparer_1_t2667114116;
// System.Collections.Generic.IDictionary`2<System.Char,System.Char>
struct IDictionary_2_t3054042048;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Char>
struct ICollection_1_t111589347;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>[]
struct KeyValuePair_2U5BU5D_t681335892;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>
struct IEnumerator_1_t287827676;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>
struct KeyCollection_t3243489102;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>
struct ValueCollection_t3758018470;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22812303849.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2080016033.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor()
extern "C"  void Dictionary_2__ctor_m857189358_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m857189358(__this, method) ((  void (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2__ctor_m857189358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1459403880_gshared (Dictionary_2_t759991331 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1459403880(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1459403880_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m390655491_gshared (Dictionary_2_t759991331 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m390655491(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m390655491_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m862727856_gshared (Dictionary_2_t759991331 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m862727856(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t759991331 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m862727856_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2376142640_gshared (Dictionary_2_t759991331 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2376142640(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2376142640_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1060626429_gshared (Dictionary_2_t759991331 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1060626429(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t759991331 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1060626429_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1410252286_gshared (Dictionary_2_t759991331 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1410252286(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t759991331 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1410252286_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4250910395_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4250910395(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4250910395_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2442603451_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2442603451(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2442603451_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4116626229_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4116626229(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4116626229_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2634034581_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2634034581(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2634034581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3974821916_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3974821916(__this, method) ((  bool (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3974821916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3583859857_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3583859857(__this, method) ((  bool (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3583859857_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1180572231_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1180572231(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t759991331 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1180572231_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m4096446626_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m4096446626(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m4096446626_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1658355861_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1658355861(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1658355861_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3424838385_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3424838385(__this, ___key0, method) ((  bool (*) (Dictionary_2_t759991331 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3424838385_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3158166358_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3158166358(__this, ___key0, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3158166358_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4019303891_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4019303891(__this, method) ((  bool (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4019303891_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1359573771_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1359573771(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1359573771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2971262661_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2971262661(__this, method) ((  bool (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2971262661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1744947348_gshared (Dictionary_2_t759991331 * __this, KeyValuePair_2_t2812303849  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1744947348(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t759991331 *, KeyValuePair_2_t2812303849 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1744947348_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m563416096_gshared (Dictionary_2_t759991331 * __this, KeyValuePair_2_t2812303849  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m563416096(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t759991331 *, KeyValuePair_2_t2812303849 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m563416096_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1905486336_gshared (Dictionary_2_t759991331 * __this, KeyValuePair_2U5BU5D_t681335892* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1905486336(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t759991331 *, KeyValuePair_2U5BU5D_t681335892*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1905486336_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1180468487_gshared (Dictionary_2_t759991331 * __this, KeyValuePair_2_t2812303849  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1180468487(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t759991331 *, KeyValuePair_2_t2812303849 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1180468487_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1299288603_gshared (Dictionary_2_t759991331 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1299288603(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1299288603_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3803088934_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3803088934(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3803088934_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1870875117_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1870875117(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1870875117_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Char,System.Char>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2104661800_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2104661800(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2104661800_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Char,System.Char>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3945994267_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3945994267(__this, method) ((  int32_t (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_get_Count_m3945994267_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Char,System.Char>::get_Item(TKey)
extern "C"  Il2CppChar Dictionary_2_get_Item_m3953505032_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3953505032(__this, ___key0, method) ((  Il2CppChar (*) (Dictionary_2_t759991331 *, Il2CppChar, const MethodInfo*))Dictionary_2_get_Item_m3953505032_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4078423261_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4078423261(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppChar, Il2CppChar, const MethodInfo*))Dictionary_2_set_Item_m4078423261_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4268733207_gshared (Dictionary_2_t759991331 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4268733207(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t759991331 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4268733207_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2297525844_gshared (Dictionary_2_t759991331 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2297525844(__this, ___size0, method) ((  void (*) (Dictionary_2_t759991331 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2297525844_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m210331278_gshared (Dictionary_2_t759991331 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m210331278(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m210331278_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2812303849  Dictionary_2_make_pair_m2204169452_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2204169452(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2812303849  (*) (Il2CppObject * /* static, unused */, Il2CppChar, Il2CppChar, const MethodInfo*))Dictionary_2_make_pair_m2204169452_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Char,System.Char>::pick_key(TKey,TValue)
extern "C"  Il2CppChar Dictionary_2_pick_key_m3206897794_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3206897794(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppChar (*) (Il2CppObject * /* static, unused */, Il2CppChar, Il2CppChar, const MethodInfo*))Dictionary_2_pick_key_m3206897794_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Char,System.Char>::pick_value(TKey,TValue)
extern "C"  Il2CppChar Dictionary_2_pick_value_m1233133514_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1233133514(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppChar (*) (Il2CppObject * /* static, unused */, Il2CppChar, Il2CppChar, const MethodInfo*))Dictionary_2_pick_value_m1233133514_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1273303923_gshared (Dictionary_2_t759991331 * __this, KeyValuePair_2U5BU5D_t681335892* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1273303923(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t759991331 *, KeyValuePair_2U5BU5D_t681335892*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1273303923_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::Resize()
extern "C"  void Dictionary_2_Resize_m2693422661_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2693422661(__this, method) ((  void (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_Resize_m2693422661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2959125558_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2959125558(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppChar, Il2CppChar, const MethodInfo*))Dictionary_2_Add_m2959125558_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::Clear()
extern "C"  void Dictionary_2_Clear_m3554082180_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3554082180(__this, method) ((  void (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_Clear_m3554082180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m718835688_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m718835688(__this, ___key0, method) ((  bool (*) (Dictionary_2_t759991331 *, Il2CppChar, const MethodInfo*))Dictionary_2_ContainsKey_m718835688_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1366848128_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1366848128(__this, ___value0, method) ((  bool (*) (Dictionary_2_t759991331 *, Il2CppChar, const MethodInfo*))Dictionary_2_ContainsValue_m1366848128_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2252840015_gshared (Dictionary_2_t759991331 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2252840015(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t759991331 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2252840015_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,System.Char>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1496674743_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1496674743(__this, ___sender0, method) ((  void (*) (Dictionary_2_t759991331 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1496674743_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1252140624_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1252140624(__this, ___key0, method) ((  bool (*) (Dictionary_2_t759991331 *, Il2CppChar, const MethodInfo*))Dictionary_2_Remove_m1252140624_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4130422425_gshared (Dictionary_2_t759991331 * __this, Il2CppChar ___key0, Il2CppChar* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4130422425(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t759991331 *, Il2CppChar, Il2CppChar*, const MethodInfo*))Dictionary_2_TryGetValue_m4130422425_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::get_Keys()
extern "C"  KeyCollection_t3243489102 * Dictionary_2_get_Keys_m3772458790_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3772458790(__this, method) ((  KeyCollection_t3243489102 * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_get_Keys_m3772458790_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::get_Values()
extern "C"  ValueCollection_t3758018470 * Dictionary_2_get_Values_m576877420_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m576877420(__this, method) ((  ValueCollection_t3758018470 * (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_get_Values_m576877420_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Char,System.Char>::ToTKey(System.Object)
extern "C"  Il2CppChar Dictionary_2_ToTKey_m1400810541_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1400810541(__this, ___key0, method) ((  Il2CppChar (*) (Dictionary_2_t759991331 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1400810541_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Char,System.Char>::ToTValue(System.Object)
extern "C"  Il2CppChar Dictionary_2_ToTValue_m742453981_gshared (Dictionary_2_t759991331 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m742453981(__this, ___value0, method) ((  Il2CppChar (*) (Dictionary_2_t759991331 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m742453981_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,System.Char>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m226896703_gshared (Dictionary_2_t759991331 * __this, KeyValuePair_2_t2812303849  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m226896703(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t759991331 *, KeyValuePair_2_t2812303849 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m226896703_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,System.Char>::GetEnumerator()
extern "C"  Enumerator_t2080016033  Dictionary_2_GetEnumerator_m3045704064_gshared (Dictionary_2_t759991331 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3045704064(__this, method) ((  Enumerator_t2080016033  (*) (Dictionary_2_t759991331 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3045704064_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Char,System.Char>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__2_m4021642171_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m4021642171(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppChar, Il2CppChar, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m4021642171_gshared)(__this /* static, unused */, ___key0, ___value1, method)
