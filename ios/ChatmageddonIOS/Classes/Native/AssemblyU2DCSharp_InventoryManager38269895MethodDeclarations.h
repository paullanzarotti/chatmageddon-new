﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InventoryManager
struct InventoryManager_t38269895;
// Missile
struct Missile_t813944928;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.String
struct String_t;
// Shield
struct Shield_t3327121081;
// Mine
struct Mine_t2729441277;
// QuantitativeItem
struct QuantitativeItem_t3036513780;
// System.Collections.Generic.List`1<Mine>
struct List_1_t2098562409;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_QuantitativeItem3036513780.h"

// System.Void InventoryManager::.ctor()
extern "C"  void InventoryManager__ctor_m3806513270 (InventoryManager_t38269895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::Init()
extern "C"  void InventoryManager_Init_m2315645254 (InventoryManager_t38269895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::ClearInventoryData()
extern "C"  void InventoryManager_ClearInventoryData_m3865728893 (InventoryManager_t38269895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::AddMissileToInventory(Missile)
extern "C"  void InventoryManager_AddMissileToInventory_m3305947952 (InventoryManager_t38269895 * __this, Missile_t813944928 * ___newItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::CheckStandardMissileInventory(System.Action`1<System.Boolean>)
extern "C"  void InventoryManager_CheckStandardMissileInventory_m2399700530 (InventoryManager_t38269895 * __this, Action_1_t3627374100 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::RemoveMissileFromInventory(Missile)
extern "C"  void InventoryManager_RemoveMissileFromInventory_m2879478090 (InventoryManager_t38269895 * __this, Missile_t813944928 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Missile InventoryManager::GetMissileFromInventory(System.String)
extern "C"  Missile_t813944928 * InventoryManager_GetMissileFromInventory_m262270705 (InventoryManager_t38269895 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::AddShieldToInventory(Shield)
extern "C"  void InventoryManager_AddShieldToInventory_m360238338 (InventoryManager_t38269895 * __this, Shield_t3327121081 * ___newItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::RemoveShieldFromInventory(Shield)
extern "C"  void InventoryManager_RemoveShieldFromInventory_m3726505674 (InventoryManager_t38269895 * __this, Shield_t3327121081 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::AddMineToInventory(Mine)
extern "C"  void InventoryManager_AddMineToInventory_m1375921602 (InventoryManager_t38269895 * __this, Mine_t2729441277 * ___newItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::RemoveMineFromInventory(Mine)
extern "C"  void InventoryManager_RemoveMineFromInventory_m673196714 (InventoryManager_t38269895 * __this, Mine_t2729441277 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryManager::RemoveMineFromInventory(System.String,System.String)
extern "C"  void InventoryManager_RemoveMineFromInventory_m3683215027 (InventoryManager_t38269895 * __this, String_t* ___internalName0, String_t* ___mineID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 InventoryManager::GetInventoryAmount(QuantitativeItem)
extern "C"  int32_t InventoryManager_GetInventoryAmount_m2022478038 (InventoryManager_t38269895 * __this, QuantitativeItem_t3036513780 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 InventoryManager::GetShieldInventoryAmount(Shield)
extern "C"  int32_t InventoryManager_GetShieldInventoryAmount_m1296843586 (InventoryManager_t38269895 * __this, Shield_t3327121081 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 InventoryManager::GetMissileInventoryAmount(Missile)
extern "C"  int32_t InventoryManager_GetMissileInventoryAmount_m4190438478 (InventoryManager_t38269895 * __this, Missile_t813944928 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 InventoryManager::GetMineInventoryAmount(Mine)
extern "C"  int32_t InventoryManager_GetMineInventoryAmount_m3484700578 (InventoryManager_t38269895 * __this, Mine_t2729441277 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Mine> InventoryManager::GetActiveMineInventory(System.String)
extern "C"  List_1_t2098562409 * InventoryManager_GetActiveMineInventory_m1004512741 (InventoryManager_t38269895 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
