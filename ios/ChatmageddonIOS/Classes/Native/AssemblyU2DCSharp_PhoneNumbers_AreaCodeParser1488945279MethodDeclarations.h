﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.AreaCodeParser
struct AreaCodeParser_t1488945279;
// PhoneNumbers.AreaCodeMap
struct AreaCodeMap_t3230759372;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void PhoneNumbers.AreaCodeParser::.ctor()
extern "C"  void AreaCodeParser__ctor_m3314308802 (AreaCodeParser_t1488945279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AreaCodeMap PhoneNumbers.AreaCodeParser::ParseAreaCodeMap(System.IO.Stream)
extern "C"  AreaCodeMap_t3230759372 * AreaCodeParser_ParseAreaCodeMap_m1811246963 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
