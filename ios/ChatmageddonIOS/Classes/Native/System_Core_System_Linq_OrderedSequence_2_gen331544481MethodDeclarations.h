﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.OrderedSequence`2<System.Object,System.DateTime>
struct OrderedSequence_2_t331544481;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<System.Object,System.DateTime>
struct Func_2_t829260555;
// System.Collections.Generic.IComparer`1<System.DateTime>
struct IComparer_1_t2942636087;
// System.Linq.OrderedEnumerable`1<System.Object>
struct OrderedEnumerable_1_t4214082274;
// System.Linq.SortContext`1<System.Object>
struct SortContext_1_t1798778454;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_SortDirection759359329.h"

// System.Void System.Linq.OrderedSequence`2<System.Object,System.DateTime>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m2131331646_gshared (OrderedSequence_2_t331544481 * __this, Il2CppObject* ___source0, Func_2_t829260555 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method);
#define OrderedSequence_2__ctor_m2131331646(__this, ___source0, ___key_selector1, ___comparer2, ___direction3, method) ((  void (*) (OrderedSequence_2_t331544481 *, Il2CppObject*, Func_2_t829260555 *, Il2CppObject*, int32_t, const MethodInfo*))OrderedSequence_2__ctor_m2131331646_gshared)(__this, ___source0, ___key_selector1, ___comparer2, ___direction3, method)
// System.Void System.Linq.OrderedSequence`2<System.Object,System.DateTime>::.ctor(System.Linq.OrderedEnumerable`1<TElement>,System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m3631086545_gshared (OrderedSequence_2_t331544481 * __this, OrderedEnumerable_1_t4214082274 * ___parent0, Il2CppObject* ___source1, Func_2_t829260555 * ___keySelector2, Il2CppObject* ___comparer3, int32_t ___direction4, const MethodInfo* method);
#define OrderedSequence_2__ctor_m3631086545(__this, ___parent0, ___source1, ___keySelector2, ___comparer3, ___direction4, method) ((  void (*) (OrderedSequence_2_t331544481 *, OrderedEnumerable_1_t4214082274 *, Il2CppObject*, Func_2_t829260555 *, Il2CppObject*, int32_t, const MethodInfo*))OrderedSequence_2__ctor_m3631086545_gshared)(__this, ___parent0, ___source1, ___keySelector2, ___comparer3, ___direction4, method)
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.DateTime>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m2667166768_gshared (OrderedSequence_2_t331544481 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method);
#define OrderedSequence_2_CreateContext_m2667166768(__this, ___current0, method) ((  SortContext_1_t1798778454 * (*) (OrderedSequence_2_t331544481 *, SortContext_1_t1798778454 *, const MethodInfo*))OrderedSequence_2_CreateContext_m2667166768_gshared)(__this, ___current0, method)
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.DateTime>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m945510653_gshared (OrderedSequence_2_t331544481 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define OrderedSequence_2_Sort_m945510653(__this, ___source0, method) ((  Il2CppObject* (*) (OrderedSequence_2_t331544481 *, Il2CppObject*, const MethodInfo*))OrderedSequence_2_Sort_m945510653_gshared)(__this, ___source0, method)
