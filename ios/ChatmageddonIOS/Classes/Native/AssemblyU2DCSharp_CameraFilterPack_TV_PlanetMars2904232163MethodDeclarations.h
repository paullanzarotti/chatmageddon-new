﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_PlanetMars
struct CameraFilterPack_TV_PlanetMars_t2904232163;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_PlanetMars::.ctor()
extern "C"  void CameraFilterPack_TV_PlanetMars__ctor_m3740369874 (CameraFilterPack_TV_PlanetMars_t2904232163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_PlanetMars::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_PlanetMars_get_material_m2908330439 (CameraFilterPack_TV_PlanetMars_t2904232163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::Start()
extern "C"  void CameraFilterPack_TV_PlanetMars_Start_m1210378766 (CameraFilterPack_TV_PlanetMars_t2904232163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_PlanetMars_OnRenderImage_m2412861918 (CameraFilterPack_TV_PlanetMars_t2904232163 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::Update()
extern "C"  void CameraFilterPack_TV_PlanetMars_Update_m2813292811 (CameraFilterPack_TV_PlanetMars_t2904232163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::OnDisable()
extern "C"  void CameraFilterPack_TV_PlanetMars_OnDisable_m3431531539 (CameraFilterPack_TV_PlanetMars_t2904232163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
