﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FriendHomeNavScreen>
struct List_1_t968159428;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat502889102.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3824964079_gshared (Enumerator_t502889102 * __this, List_1_t968159428 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3824964079(__this, ___l0, method) ((  void (*) (Enumerator_t502889102 *, List_1_t968159428 *, const MethodInfo*))Enumerator__ctor_m3824964079_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2972813867_gshared (Enumerator_t502889102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2972813867(__this, method) ((  void (*) (Enumerator_t502889102 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2972813867_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2554812839_gshared (Enumerator_t502889102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2554812839(__this, method) ((  Il2CppObject * (*) (Enumerator_t502889102 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2554812839_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m420439788_gshared (Enumerator_t502889102 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m420439788(__this, method) ((  void (*) (Enumerator_t502889102 *, const MethodInfo*))Enumerator_Dispose_m420439788_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m662605537_gshared (Enumerator_t502889102 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m662605537(__this, method) ((  void (*) (Enumerator_t502889102 *, const MethodInfo*))Enumerator_VerifyState_m662605537_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m340743855_gshared (Enumerator_t502889102 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m340743855(__this, method) ((  bool (*) (Enumerator_t502889102 *, const MethodInfo*))Enumerator_MoveNext_m340743855_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FriendHomeNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3860642722_gshared (Enumerator_t502889102 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3860642722(__this, method) ((  int32_t (*) (Enumerator_t502889102 *, const MethodInfo*))Enumerator_get_Current_m3860642722_gshared)(__this, method)
