﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusDoubleStateButton
struct StatusDoubleStateButton_t3675410548;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusDoubleStateButton::.ctor()
extern "C"  void StatusDoubleStateButton__ctor_m2998409599 (StatusDoubleStateButton_t3675410548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusDoubleStateButton::SetButtonActive(System.Boolean)
extern "C"  void StatusDoubleStateButton_SetButtonActive_m300485300 (StatusDoubleStateButton_t3675410548 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
