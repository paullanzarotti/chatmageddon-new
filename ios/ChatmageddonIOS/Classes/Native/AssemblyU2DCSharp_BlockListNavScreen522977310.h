﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// BlockILP
struct BlockILP_t879176280;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlockListNavScreen
struct  BlockListNavScreen_t522977310  : public NavigationScreen_t2333230110
{
public:
	// TweenPosition BlockListNavScreen::tableTween
	TweenPosition_t1144714832 * ___tableTween_3;
	// BlockILP BlockListNavScreen::blockList
	BlockILP_t879176280 * ___blockList_4;
	// System.Boolean BlockListNavScreen::UIclosing
	bool ___UIclosing_5;
	// System.Boolean BlockListNavScreen::settingsUIclosing
	bool ___settingsUIclosing_6;
	// System.Boolean BlockListNavScreen::loadingChat
	bool ___loadingChat_7;

public:
	inline static int32_t get_offset_of_tableTween_3() { return static_cast<int32_t>(offsetof(BlockListNavScreen_t522977310, ___tableTween_3)); }
	inline TweenPosition_t1144714832 * get_tableTween_3() const { return ___tableTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_tableTween_3() { return &___tableTween_3; }
	inline void set_tableTween_3(TweenPosition_t1144714832 * value)
	{
		___tableTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___tableTween_3, value);
	}

	inline static int32_t get_offset_of_blockList_4() { return static_cast<int32_t>(offsetof(BlockListNavScreen_t522977310, ___blockList_4)); }
	inline BlockILP_t879176280 * get_blockList_4() const { return ___blockList_4; }
	inline BlockILP_t879176280 ** get_address_of_blockList_4() { return &___blockList_4; }
	inline void set_blockList_4(BlockILP_t879176280 * value)
	{
		___blockList_4 = value;
		Il2CppCodeGenWriteBarrier(&___blockList_4, value);
	}

	inline static int32_t get_offset_of_UIclosing_5() { return static_cast<int32_t>(offsetof(BlockListNavScreen_t522977310, ___UIclosing_5)); }
	inline bool get_UIclosing_5() const { return ___UIclosing_5; }
	inline bool* get_address_of_UIclosing_5() { return &___UIclosing_5; }
	inline void set_UIclosing_5(bool value)
	{
		___UIclosing_5 = value;
	}

	inline static int32_t get_offset_of_settingsUIclosing_6() { return static_cast<int32_t>(offsetof(BlockListNavScreen_t522977310, ___settingsUIclosing_6)); }
	inline bool get_settingsUIclosing_6() const { return ___settingsUIclosing_6; }
	inline bool* get_address_of_settingsUIclosing_6() { return &___settingsUIclosing_6; }
	inline void set_settingsUIclosing_6(bool value)
	{
		___settingsUIclosing_6 = value;
	}

	inline static int32_t get_offset_of_loadingChat_7() { return static_cast<int32_t>(offsetof(BlockListNavScreen_t522977310, ___loadingChat_7)); }
	inline bool get_loadingChat_7() const { return ___loadingChat_7; }
	inline bool* get_address_of_loadingChat_7() { return &___loadingChat_7; }
	inline void set_loadingChat_7(bool value)
	{
		___loadingChat_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
