﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreManager/<ScaleAndShow>c__Iterator2
struct U3CScaleAndShowU3Ec__Iterator2_t815003340;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StoreManager/<ScaleAndShow>c__Iterator2::.ctor()
extern "C"  void U3CScaleAndShowU3Ec__Iterator2__ctor_m2105914025 (U3CScaleAndShowU3Ec__Iterator2_t815003340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StoreManager/<ScaleAndShow>c__Iterator2::MoveNext()
extern "C"  bool U3CScaleAndShowU3Ec__Iterator2_MoveNext_m682325755 (U3CScaleAndShowU3Ec__Iterator2_t815003340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<ScaleAndShow>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CScaleAndShowU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m651121767 (U3CScaleAndShowU3Ec__Iterator2_t815003340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<ScaleAndShow>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CScaleAndShowU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m732203279 (U3CScaleAndShowU3Ec__Iterator2_t815003340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<ScaleAndShow>c__Iterator2::Dispose()
extern "C"  void U3CScaleAndShowU3Ec__Iterator2_Dispose_m3490599878 (U3CScaleAndShowU3Ec__Iterator2_t815003340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<ScaleAndShow>c__Iterator2::Reset()
extern "C"  void U3CScaleAndShowU3Ec__Iterator2_Reset_m357342196 (U3CScaleAndShowU3Ec__Iterator2_t815003340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
