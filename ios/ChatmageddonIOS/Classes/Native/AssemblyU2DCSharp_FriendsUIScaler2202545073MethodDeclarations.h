﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsUIScaler
struct FriendsUIScaler_t2202545073;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsUIScaler::.ctor()
extern "C"  void FriendsUIScaler__ctor_m3983778918 (FriendsUIScaler_t2202545073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsUIScaler::GlobalUIScale()
extern "C"  void FriendsUIScaler_GlobalUIScale_m1913075587 (FriendsUIScaler_t2202545073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
