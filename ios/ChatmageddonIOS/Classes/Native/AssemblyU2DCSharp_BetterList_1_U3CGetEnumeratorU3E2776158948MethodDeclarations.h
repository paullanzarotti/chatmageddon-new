﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>
struct U3CGetEnumeratorU3Ec__Iterator0_t2776158948;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3642800649_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m3642800649(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m3642800649_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1849075671_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1849075671(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1849075671_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2575873546_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2575873546(__this, method) ((  int32_t (*) (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2575873546_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3770720887_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3770720887(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3770720887_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2165786792_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2165786792(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2165786792_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<System.Int32>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2199905266_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m2199905266(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2776158948 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m2199905266_gshared)(__this, method)
