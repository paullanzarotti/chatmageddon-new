﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseInfiniteListItem`1<System.Object>
struct BaseInfiniteListItem_1_t3464988659;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseInfiniteListItem`1<System.Object>::.ctor()
extern "C"  void BaseInfiniteListItem_1__ctor_m451819470_gshared (BaseInfiniteListItem_1_t3464988659 * __this, const MethodInfo* method);
#define BaseInfiniteListItem_1__ctor_m451819470(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3464988659 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<System.Object>::verifyVisibility()
extern "C"  bool BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared (BaseInfiniteListItem_1_t3464988659 * __this, const MethodInfo* method);
#define BaseInfiniteListItem_1_verifyVisibility_m1990849747(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t3464988659 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<System.Object>::InitItem()
extern "C"  void BaseInfiniteListItem_1_InitItem_m2730396605_gshared (BaseInfiniteListItem_1_t3464988659 * __this, const MethodInfo* method);
#define BaseInfiniteListItem_1_InitItem_m2730396605(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3464988659 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<System.Object>::SetUpItem()
extern "C"  void BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared (BaseInfiniteListItem_1_t3464988659 * __this, const MethodInfo* method);
#define BaseInfiniteListItem_1_SetUpItem_m1802181512(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3464988659 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<System.Object>::SetItemSelected(System.Boolean)
extern "C"  void BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared (BaseInfiniteListItem_1_t3464988659 * __this, bool ___selected0, const MethodInfo* method);
#define BaseInfiniteListItem_1_SetItemSelected_m718662847(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t3464988659 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<System.Object>::SetVisible()
extern "C"  void BaseInfiniteListItem_1_SetVisible_m2310471774_gshared (BaseInfiniteListItem_1_t3464988659 * __this, const MethodInfo* method);
#define BaseInfiniteListItem_1_SetVisible_m2310471774(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3464988659 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<System.Object>::SetInvisible()
extern "C"  void BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared (BaseInfiniteListItem_1_t3464988659 * __this, const MethodInfo* method);
#define BaseInfiniteListItem_1_SetInvisible_m1500163933(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3464988659 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
