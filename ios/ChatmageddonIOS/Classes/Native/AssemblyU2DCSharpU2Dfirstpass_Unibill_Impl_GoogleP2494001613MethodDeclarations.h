﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.GooglePlayBillingService
struct GooglePlayBillingService_t2494001613;
// Unibill.Impl.IRawGooglePlayInterface
struct IRawGooglePlayInterface_t4240979409;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.GooglePlayBillingService::.ctor(Unibill.Impl.IRawGooglePlayInterface,Unibill.Impl.UnibillConfiguration,Unibill.Impl.ProductIdRemapper,Uniject.ILogger)
extern "C"  void GooglePlayBillingService__ctor_m3050741933 (GooglePlayBillingService_t2494001613 * __this, Il2CppObject * ___rawInterface0, UnibillConfiguration_t2915611853 * ___config1, ProductIdRemapper_t3313438456 * ___remapper2, Il2CppObject * ___logger3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void GooglePlayBillingService_initialise_m670243789 (GooglePlayBillingService_t2494001613 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::restoreTransactions()
extern "C"  void GooglePlayBillingService_restoreTransactions_m3234858144 (GooglePlayBillingService_t2494001613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::purchase(System.String,System.String)
extern "C"  void GooglePlayBillingService_purchase_m1683360234 (GooglePlayBillingService_t2494001613 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onBillingNotSupported()
extern "C"  void GooglePlayBillingService_onBillingNotSupported_m1625728502 (GooglePlayBillingService_t2494001613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onPurchaseSucceeded(System.String)
extern "C"  void GooglePlayBillingService_onPurchaseSucceeded_m1890766438 (GooglePlayBillingService_t2494001613 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onPurchaseCancelled(System.String)
extern "C"  void GooglePlayBillingService_onPurchaseCancelled_m2870769312 (GooglePlayBillingService_t2494001613 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onPurchaseRefunded(System.String)
extern "C"  void GooglePlayBillingService_onPurchaseRefunded_m375810916 (GooglePlayBillingService_t2494001613 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onPurchaseFailed(System.String)
extern "C"  void GooglePlayBillingService_onPurchaseFailed_m3511705254 (GooglePlayBillingService_t2494001613 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onTransactionsRestored(System.String)
extern "C"  void GooglePlayBillingService_onTransactionsRestored_m2615524255 (GooglePlayBillingService_t2494001613 * __this, String_t* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onInvalidPublicKey(System.String)
extern "C"  void GooglePlayBillingService_onInvalidPublicKey_m983814895 (GooglePlayBillingService_t2494001613 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onProductListReceived(System.String)
extern "C"  void GooglePlayBillingService_onProductListReceived_m3199373078 (GooglePlayBillingService_t2494001613 * __this, String_t* ___productListString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.GooglePlayBillingService::onPollForConsumablesFinished(System.String)
extern "C"  void GooglePlayBillingService_onPollForConsumablesFinished_m3717590942 (GooglePlayBillingService_t2494001613 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.GooglePlayBillingService::hasReceipt(System.String)
extern "C"  bool GooglePlayBillingService_hasReceipt_m1765222023 (GooglePlayBillingService_t2494001613 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.GooglePlayBillingService::getReceipt(System.String)
extern "C"  String_t* GooglePlayBillingService_getReceipt_m2108149970 (GooglePlayBillingService_t2494001613 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.GooglePlayBillingService::verifyReceipt(System.String)
extern "C"  bool GooglePlayBillingService_verifyReceipt_m4286068768 (GooglePlayBillingService_t2494001613 * __this, String_t* ___receipt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
