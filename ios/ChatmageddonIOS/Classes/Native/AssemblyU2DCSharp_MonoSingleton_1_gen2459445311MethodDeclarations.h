﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ProfileNavigationController>::.ctor()
#define MonoSingleton_1__ctor_m827267241(__this, method) ((  void (*) (MonoSingleton_1_t2459445311 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ProfileNavigationController>::Awake()
#define MonoSingleton_1_Awake_m1456266846(__this, method) ((  void (*) (MonoSingleton_1_t2459445311 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ProfileNavigationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m1124318122(__this /* static, unused */, method) ((  ProfileNavigationController_t2708779591 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ProfileNavigationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4159324496(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ProfileNavigationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3783629875(__this, method) ((  void (*) (MonoSingleton_1_t2459445311 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ProfileNavigationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2962675063(__this, method) ((  void (*) (MonoSingleton_1_t2459445311 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ProfileNavigationController>::.cctor()
#define MonoSingleton_1__cctor_m41510204(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
