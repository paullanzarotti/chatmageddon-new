﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0
struct U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0::.ctor()
extern "C"  void U3CStopTweenOnDirectionU3Ec__Iterator0__ctor_m912384792 (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0::MoveNext()
extern "C"  bool U3CStopTweenOnDirectionU3Ec__Iterator0_MoveNext_m2731031416 (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStopTweenOnDirectionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m833443854 (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStopTweenOnDirectionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3556565238 (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0::Dispose()
extern "C"  void U3CStopTweenOnDirectionU3Ec__Iterator0_Dispose_m1656029927 (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScale/<StopTweenOnDirection>c__Iterator0::Reset()
extern "C"  void U3CStopTweenOnDirectionU3Ec__Iterator0_Reset_m3200809989 (U3CStopTweenOnDirectionU3Ec__Iterator0_t23811805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
