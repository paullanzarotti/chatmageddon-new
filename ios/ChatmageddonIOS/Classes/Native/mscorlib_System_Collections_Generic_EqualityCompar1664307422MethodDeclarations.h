﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ChatNavScreen>
struct DefaultComparer_t1664307422;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ChatNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2913496071_gshared (DefaultComparer_t1664307422 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2913496071(__this, method) ((  void (*) (DefaultComparer_t1664307422 *, const MethodInfo*))DefaultComparer__ctor_m2913496071_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ChatNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2682851570_gshared (DefaultComparer_t1664307422 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2682851570(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1664307422 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2682851570_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ChatNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m553775978_gshared (DefaultComparer_t1664307422 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m553775978(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1664307422 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m553775978_gshared)(__this, ___x0, ___y1, method)
