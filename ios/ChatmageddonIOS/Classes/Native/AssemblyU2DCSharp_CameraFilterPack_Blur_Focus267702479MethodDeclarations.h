﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Focus
struct CameraFilterPack_Blur_Focus_t267702479;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Focus::.ctor()
extern "C"  void CameraFilterPack_Blur_Focus__ctor_m129163162 (CameraFilterPack_Blur_Focus_t267702479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Focus::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Focus_get_material_m4119884759 (CameraFilterPack_Blur_Focus_t267702479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::Start()
extern "C"  void CameraFilterPack_Blur_Focus_Start_m2997645910 (CameraFilterPack_Blur_Focus_t267702479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Focus_OnRenderImage_m3934718406 (CameraFilterPack_Blur_Focus_t267702479 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::OnValidate()
extern "C"  void CameraFilterPack_Blur_Focus_OnValidate_m2864420645 (CameraFilterPack_Blur_Focus_t267702479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::Update()
extern "C"  void CameraFilterPack_Blur_Focus_Update_m1984278415 (CameraFilterPack_Blur_Focus_t267702479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::OnDisable()
extern "C"  void CameraFilterPack_Blur_Focus_OnDisable_m2558114911 (CameraFilterPack_Blur_Focus_t267702479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
