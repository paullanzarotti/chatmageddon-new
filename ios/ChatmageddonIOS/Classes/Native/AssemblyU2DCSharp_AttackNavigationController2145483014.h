﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AttackNavigateBackwardsButton
struct AttackNavigateBackwardsButton_t4019147023;
// AttackNavigateForwardsButton
struct AttackNavigateForwardsButton_t1401782687;

#include "AssemblyU2DCSharp_NavigationController_2_gen2569943916.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackNavigationController
struct  AttackNavigationController_t2145483014  : public NavigationController_2_t2569943916
{
public:
	// UILabel AttackNavigationController::titleLabel
	UILabel_t1795115428 * ___titleLabel_8;
	// UnityEngine.GameObject AttackNavigationController::homeObject
	GameObject_t1756533147 * ___homeObject_9;
	// UnityEngine.GameObject AttackNavigationController::inventoryObject
	GameObject_t1756533147 * ___inventoryObject_10;
	// UnityEngine.GameObject AttackNavigationController::reviewObject
	GameObject_t1756533147 * ___reviewObject_11;
	// UnityEngine.GameObject AttackNavigationController::searchObject
	GameObject_t1756533147 * ___searchObject_12;
	// UnityEngine.GameObject AttackNavigationController::attackGameObject
	GameObject_t1756533147 * ___attackGameObject_13;
	// AttackNavigateBackwardsButton AttackNavigationController::mainNavigateBackwardButton
	AttackNavigateBackwardsButton_t4019147023 * ___mainNavigateBackwardButton_14;
	// AttackNavigateForwardsButton AttackNavigationController::mainNavigateForwardButton
	AttackNavigateForwardsButton_t1401782687 * ___mainNavigateForwardButton_15;
	// UnityEngine.Vector3 AttackNavigationController::activePos
	Vector3_t2243707580  ___activePos_16;
	// UnityEngine.Vector3 AttackNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_17;
	// UnityEngine.Vector3 AttackNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_18;
	// UnityEngine.GameObject AttackNavigationController::inputBlock
	GameObject_t1756533147 * ___inputBlock_19;

public:
	inline static int32_t get_offset_of_titleLabel_8() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___titleLabel_8)); }
	inline UILabel_t1795115428 * get_titleLabel_8() const { return ___titleLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_8() { return &___titleLabel_8; }
	inline void set_titleLabel_8(UILabel_t1795115428 * value)
	{
		___titleLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_8, value);
	}

	inline static int32_t get_offset_of_homeObject_9() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___homeObject_9)); }
	inline GameObject_t1756533147 * get_homeObject_9() const { return ___homeObject_9; }
	inline GameObject_t1756533147 ** get_address_of_homeObject_9() { return &___homeObject_9; }
	inline void set_homeObject_9(GameObject_t1756533147 * value)
	{
		___homeObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___homeObject_9, value);
	}

	inline static int32_t get_offset_of_inventoryObject_10() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___inventoryObject_10)); }
	inline GameObject_t1756533147 * get_inventoryObject_10() const { return ___inventoryObject_10; }
	inline GameObject_t1756533147 ** get_address_of_inventoryObject_10() { return &___inventoryObject_10; }
	inline void set_inventoryObject_10(GameObject_t1756533147 * value)
	{
		___inventoryObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryObject_10, value);
	}

	inline static int32_t get_offset_of_reviewObject_11() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___reviewObject_11)); }
	inline GameObject_t1756533147 * get_reviewObject_11() const { return ___reviewObject_11; }
	inline GameObject_t1756533147 ** get_address_of_reviewObject_11() { return &___reviewObject_11; }
	inline void set_reviewObject_11(GameObject_t1756533147 * value)
	{
		___reviewObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___reviewObject_11, value);
	}

	inline static int32_t get_offset_of_searchObject_12() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___searchObject_12)); }
	inline GameObject_t1756533147 * get_searchObject_12() const { return ___searchObject_12; }
	inline GameObject_t1756533147 ** get_address_of_searchObject_12() { return &___searchObject_12; }
	inline void set_searchObject_12(GameObject_t1756533147 * value)
	{
		___searchObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___searchObject_12, value);
	}

	inline static int32_t get_offset_of_attackGameObject_13() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___attackGameObject_13)); }
	inline GameObject_t1756533147 * get_attackGameObject_13() const { return ___attackGameObject_13; }
	inline GameObject_t1756533147 ** get_address_of_attackGameObject_13() { return &___attackGameObject_13; }
	inline void set_attackGameObject_13(GameObject_t1756533147 * value)
	{
		___attackGameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___attackGameObject_13, value);
	}

	inline static int32_t get_offset_of_mainNavigateBackwardButton_14() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___mainNavigateBackwardButton_14)); }
	inline AttackNavigateBackwardsButton_t4019147023 * get_mainNavigateBackwardButton_14() const { return ___mainNavigateBackwardButton_14; }
	inline AttackNavigateBackwardsButton_t4019147023 ** get_address_of_mainNavigateBackwardButton_14() { return &___mainNavigateBackwardButton_14; }
	inline void set_mainNavigateBackwardButton_14(AttackNavigateBackwardsButton_t4019147023 * value)
	{
		___mainNavigateBackwardButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateBackwardButton_14, value);
	}

	inline static int32_t get_offset_of_mainNavigateForwardButton_15() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___mainNavigateForwardButton_15)); }
	inline AttackNavigateForwardsButton_t1401782687 * get_mainNavigateForwardButton_15() const { return ___mainNavigateForwardButton_15; }
	inline AttackNavigateForwardsButton_t1401782687 ** get_address_of_mainNavigateForwardButton_15() { return &___mainNavigateForwardButton_15; }
	inline void set_mainNavigateForwardButton_15(AttackNavigateForwardsButton_t1401782687 * value)
	{
		___mainNavigateForwardButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateForwardButton_15, value);
	}

	inline static int32_t get_offset_of_activePos_16() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___activePos_16)); }
	inline Vector3_t2243707580  get_activePos_16() const { return ___activePos_16; }
	inline Vector3_t2243707580 * get_address_of_activePos_16() { return &___activePos_16; }
	inline void set_activePos_16(Vector3_t2243707580  value)
	{
		___activePos_16 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_17() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___leftInactivePos_17)); }
	inline Vector3_t2243707580  get_leftInactivePos_17() const { return ___leftInactivePos_17; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_17() { return &___leftInactivePos_17; }
	inline void set_leftInactivePos_17(Vector3_t2243707580  value)
	{
		___leftInactivePos_17 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_18() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___rightInactivePos_18)); }
	inline Vector3_t2243707580  get_rightInactivePos_18() const { return ___rightInactivePos_18; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_18() { return &___rightInactivePos_18; }
	inline void set_rightInactivePos_18(Vector3_t2243707580  value)
	{
		___rightInactivePos_18 = value;
	}

	inline static int32_t get_offset_of_inputBlock_19() { return static_cast<int32_t>(offsetof(AttackNavigationController_t2145483014, ___inputBlock_19)); }
	inline GameObject_t1756533147 * get_inputBlock_19() const { return ___inputBlock_19; }
	inline GameObject_t1756533147 ** get_address_of_inputBlock_19() { return &___inputBlock_19; }
	inline void set_inputBlock_19(GameObject_t1756533147 * value)
	{
		___inputBlock_19 = value;
		Il2CppCodeGenWriteBarrier(&___inputBlock_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
