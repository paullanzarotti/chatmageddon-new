﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PendingFriendIndicator
struct PendingFriendIndicator_t2589313544;

#include "codegen/il2cpp-codegen.h"

// System.Void PendingFriendIndicator::.ctor()
extern "C"  void PendingFriendIndicator__ctor_m3723273975 (PendingFriendIndicator_t2589313544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PendingFriendIndicator::FixedUpdate()
extern "C"  void PendingFriendIndicator_FixedUpdate_m1851330796 (PendingFriendIndicator_t2589313544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
