﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumberNaviagteForwardButton
struct  PhoneNumberNaviagteForwardButton_t3286637755  : public SFXButton_t792651341
{
public:
	// PhoneNumberNavScreen PhoneNumberNaviagteForwardButton::nextScreen
	int32_t ___nextScreen_5;
	// UILabel PhoneNumberNaviagteForwardButton::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_6;

public:
	inline static int32_t get_offset_of_nextScreen_5() { return static_cast<int32_t>(offsetof(PhoneNumberNaviagteForwardButton_t3286637755, ___nextScreen_5)); }
	inline int32_t get_nextScreen_5() const { return ___nextScreen_5; }
	inline int32_t* get_address_of_nextScreen_5() { return &___nextScreen_5; }
	inline void set_nextScreen_5(int32_t value)
	{
		___nextScreen_5 = value;
	}

	inline static int32_t get_offset_of_buttonLabel_6() { return static_cast<int32_t>(offsetof(PhoneNumberNaviagteForwardButton_t3286637755, ___buttonLabel_6)); }
	inline UILabel_t1795115428 * get_buttonLabel_6() const { return ___buttonLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_6() { return &___buttonLabel_6; }
	inline void set_buttonLabel_6(UILabel_t1795115428 * value)
	{
		___buttonLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
