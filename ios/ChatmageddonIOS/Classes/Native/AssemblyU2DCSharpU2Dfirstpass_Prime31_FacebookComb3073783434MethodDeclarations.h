﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookSessi793857653.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.FacebookCombo::init()
extern "C"  void FacebookCombo_init_m494895472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookCombo::getAppLaunchUrl()
extern "C"  String_t* FacebookCombo_getAppLaunchUrl_m4086014972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::login()
extern "C"  void FacebookCombo_login_m2088733801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::loginWithReadPermissions(System.String[])
extern "C"  void FacebookCombo_loginWithReadPermissions_m1144571097 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::reauthorizeWithReadPermissions(System.String[])
extern "C"  void FacebookCombo_reauthorizeWithReadPermissions_m3570967920 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::reauthorizeWithPublishPermissions(System.String[],Prime31.FacebookSessionDefaultAudience)
extern "C"  void FacebookCombo_reauthorizeWithPublishPermissions_m2850894087 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, int32_t ___defaultAudience1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.FacebookCombo::isSessionValid()
extern "C"  bool FacebookCombo_isSessionValid_m3612712674 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookCombo::getAccessToken()
extern "C"  String_t* FacebookCombo_getAccessToken_m3058947262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Prime31.FacebookCombo::getSessionPermissions()
extern "C"  List_1_t2058570427 * FacebookCombo_getSessionPermissions_m4251492565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::logout()
extern "C"  void FacebookCombo_logout_m3821288436 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::showDialog(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FacebookCombo_showDialog_m3212941490 (Il2CppObject * __this /* static, unused */, String_t* ___dialogType0, Dictionary_2_t3943999495 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::showFacebookShareDialog(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookCombo_showFacebookShareDialog_m444996795 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::setAppVersion(System.String)
extern "C"  void FacebookCombo_setAppVersion_m4219697937 (Il2CppObject * __this /* static, unused */, String_t* ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::logEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookCombo_logEvent_m4201377315 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t309261261 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookCombo::logEvent(System.String,System.Double,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookCombo_logEvent_m3618418349 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, double ___valueToSum1, Dictionary_2_t309261261 * ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
