﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatButton
struct  ChatButton_t3979308854  : public SFXButton_t792651341
{
public:
	// UISprite ChatButton::chatIcon
	UISprite_t603616735 * ___chatIcon_5;
	// UILabel ChatButton::newChatsLabel
	UILabel_t1795115428 * ___newChatsLabel_6;
	// UnityEngine.Color ChatButton::activeColour
	Color_t2020392075  ___activeColour_7;
	// UnityEngine.Color ChatButton::inactiveColour
	Color_t2020392075  ___inactiveColour_8;

public:
	inline static int32_t get_offset_of_chatIcon_5() { return static_cast<int32_t>(offsetof(ChatButton_t3979308854, ___chatIcon_5)); }
	inline UISprite_t603616735 * get_chatIcon_5() const { return ___chatIcon_5; }
	inline UISprite_t603616735 ** get_address_of_chatIcon_5() { return &___chatIcon_5; }
	inline void set_chatIcon_5(UISprite_t603616735 * value)
	{
		___chatIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&___chatIcon_5, value);
	}

	inline static int32_t get_offset_of_newChatsLabel_6() { return static_cast<int32_t>(offsetof(ChatButton_t3979308854, ___newChatsLabel_6)); }
	inline UILabel_t1795115428 * get_newChatsLabel_6() const { return ___newChatsLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_newChatsLabel_6() { return &___newChatsLabel_6; }
	inline void set_newChatsLabel_6(UILabel_t1795115428 * value)
	{
		___newChatsLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___newChatsLabel_6, value);
	}

	inline static int32_t get_offset_of_activeColour_7() { return static_cast<int32_t>(offsetof(ChatButton_t3979308854, ___activeColour_7)); }
	inline Color_t2020392075  get_activeColour_7() const { return ___activeColour_7; }
	inline Color_t2020392075 * get_address_of_activeColour_7() { return &___activeColour_7; }
	inline void set_activeColour_7(Color_t2020392075  value)
	{
		___activeColour_7 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_8() { return static_cast<int32_t>(offsetof(ChatButton_t3979308854, ___inactiveColour_8)); }
	inline Color_t2020392075  get_inactiveColour_8() const { return ___inactiveColour_8; }
	inline Color_t2020392075 * get_address_of_inactiveColour_8() { return &___inactiveColour_8; }
	inline void set_inactiveColour_8(Color_t2020392075  value)
	{
		___inactiveColour_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
