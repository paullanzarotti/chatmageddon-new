﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Tiles
struct CameraFilterPack_TV_Tiles_t183441211;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Tiles::.ctor()
extern "C"  void CameraFilterPack_TV_Tiles__ctor_m2476874456 (CameraFilterPack_TV_Tiles_t183441211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Tiles::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Tiles_get_material_m1045390539 (CameraFilterPack_TV_Tiles_t183441211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::Start()
extern "C"  void CameraFilterPack_TV_Tiles_Start_m3362783660 (CameraFilterPack_TV_Tiles_t183441211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Tiles_OnRenderImage_m119139228 (CameraFilterPack_TV_Tiles_t183441211 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::OnValidate()
extern "C"  void CameraFilterPack_TV_Tiles_OnValidate_m3165293073 (CameraFilterPack_TV_Tiles_t183441211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::Update()
extern "C"  void CameraFilterPack_TV_Tiles_Update_m2590131299 (CameraFilterPack_TV_Tiles_t183441211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::OnDisable()
extern "C"  void CameraFilterPack_TV_Tiles_OnDisable_m3627827651 (CameraFilterPack_TV_Tiles_t183441211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
