﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vectrosity.VectorLine
struct VectorLine_t3390220087;
// LineManager
struct LineManager_t3088676951;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineManager/<DisableLine>c__Iterator0
struct  U3CDisableLineU3Ec__Iterator0_t1171422737  : public Il2CppObject
{
public:
	// System.Single LineManager/<DisableLine>c__Iterator0::time
	float ___time_0;
	// System.Boolean LineManager/<DisableLine>c__Iterator0::remove
	bool ___remove_1;
	// Vectrosity.VectorLine LineManager/<DisableLine>c__Iterator0::vectorLine
	VectorLine_t3390220087 * ___vectorLine_2;
	// LineManager LineManager/<DisableLine>c__Iterator0::$this
	LineManager_t3088676951 * ___U24this_3;
	// System.Object LineManager/<DisableLine>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean LineManager/<DisableLine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 LineManager/<DisableLine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_remove_1() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___remove_1)); }
	inline bool get_remove_1() const { return ___remove_1; }
	inline bool* get_address_of_remove_1() { return &___remove_1; }
	inline void set_remove_1(bool value)
	{
		___remove_1 = value;
	}

	inline static int32_t get_offset_of_vectorLine_2() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___vectorLine_2)); }
	inline VectorLine_t3390220087 * get_vectorLine_2() const { return ___vectorLine_2; }
	inline VectorLine_t3390220087 ** get_address_of_vectorLine_2() { return &___vectorLine_2; }
	inline void set_vectorLine_2(VectorLine_t3390220087 * value)
	{
		___vectorLine_2 = value;
		Il2CppCodeGenWriteBarrier(&___vectorLine_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___U24this_3)); }
	inline LineManager_t3088676951 * get_U24this_3() const { return ___U24this_3; }
	inline LineManager_t3088676951 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(LineManager_t3088676951 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDisableLineU3Ec__Iterator0_t1171422737, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
