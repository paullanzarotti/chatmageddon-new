﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PurchaseEvent
struct  PurchaseEvent_t743554429  : public Il2CppObject
{
public:
	// PurchasableItem PurchaseEvent::<PurchasedItem>k__BackingField
	PurchasableItem_t3963353899 * ___U3CPurchasedItemU3Ek__BackingField_0;
	// System.String PurchaseEvent::<Receipt>k__BackingField
	String_t* ___U3CReceiptU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPurchasedItemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseEvent_t743554429, ___U3CPurchasedItemU3Ek__BackingField_0)); }
	inline PurchasableItem_t3963353899 * get_U3CPurchasedItemU3Ek__BackingField_0() const { return ___U3CPurchasedItemU3Ek__BackingField_0; }
	inline PurchasableItem_t3963353899 ** get_address_of_U3CPurchasedItemU3Ek__BackingField_0() { return &___U3CPurchasedItemU3Ek__BackingField_0; }
	inline void set_U3CPurchasedItemU3Ek__BackingField_0(PurchasableItem_t3963353899 * value)
	{
		___U3CPurchasedItemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPurchasedItemU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CReceiptU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PurchaseEvent_t743554429, ___U3CReceiptU3Ek__BackingField_1)); }
	inline String_t* get_U3CReceiptU3Ek__BackingField_1() const { return ___U3CReceiptU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CReceiptU3Ek__BackingField_1() { return &___U3CReceiptU3Ek__BackingField_1; }
	inline void set_U3CReceiptU3Ek__BackingField_1(String_t* value)
	{
		___U3CReceiptU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReceiptU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
