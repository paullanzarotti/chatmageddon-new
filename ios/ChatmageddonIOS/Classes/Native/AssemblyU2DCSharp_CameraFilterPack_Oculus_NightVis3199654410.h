﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Oculus_NightVision1
struct  CameraFilterPack_Oculus_NightVision1_t3199654410  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Oculus_NightVision1::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Oculus_NightVision1::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Oculus_NightVision1::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_Oculus_NightVision1::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_NightVision1::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_6;
	// System.Single CameraFilterPack_Oculus_NightVision1::Vignette
	float ___Vignette_7;
	// System.Single CameraFilterPack_Oculus_NightVision1::Linecount
	float ___Linecount_8;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Distortion_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___Distortion_4)); }
	inline float get_Distortion_4() const { return ___Distortion_4; }
	inline float* get_address_of_Distortion_4() { return &___Distortion_4; }
	inline void set_Distortion_4(float value)
	{
		___Distortion_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_ScreenResolution_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___ScreenResolution_6)); }
	inline Vector4_t2243707581  get_ScreenResolution_6() const { return ___ScreenResolution_6; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_6() { return &___ScreenResolution_6; }
	inline void set_ScreenResolution_6(Vector4_t2243707581  value)
	{
		___ScreenResolution_6 = value;
	}

	inline static int32_t get_offset_of_Vignette_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___Vignette_7)); }
	inline float get_Vignette_7() const { return ___Vignette_7; }
	inline float* get_address_of_Vignette_7() { return &___Vignette_7; }
	inline void set_Vignette_7(float value)
	{
		___Vignette_7 = value;
	}

	inline static int32_t get_offset_of_Linecount_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410, ___Linecount_8)); }
	inline float get_Linecount_8() const { return ___Linecount_8; }
	inline float* get_address_of_Linecount_8() { return &___Linecount_8; }
	inline void set_Linecount_8(float value)
	{
		___Linecount_8 = value;
	}
};

struct CameraFilterPack_Oculus_NightVision1_t3199654410_StaticFields
{
public:
	// System.Single CameraFilterPack_Oculus_NightVision1::ChangeVignette
	float ___ChangeVignette_9;
	// System.Single CameraFilterPack_Oculus_NightVision1::ChangeLinecount
	float ___ChangeLinecount_10;

public:
	inline static int32_t get_offset_of_ChangeVignette_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410_StaticFields, ___ChangeVignette_9)); }
	inline float get_ChangeVignette_9() const { return ___ChangeVignette_9; }
	inline float* get_address_of_ChangeVignette_9() { return &___ChangeVignette_9; }
	inline void set_ChangeVignette_9(float value)
	{
		___ChangeVignette_9 = value;
	}

	inline static int32_t get_offset_of_ChangeLinecount_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision1_t3199654410_StaticFields, ___ChangeLinecount_10)); }
	inline float get_ChangeLinecount_10() const { return ___ChangeLinecount_10; }
	inline float* get_address_of_ChangeLinecount_10() { return &___ChangeLinecount_10; }
	inline void set_ChangeLinecount_10(float value)
	{
		___ChangeLinecount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
