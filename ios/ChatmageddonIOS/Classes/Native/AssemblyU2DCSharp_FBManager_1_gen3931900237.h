﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FacebookPlayer
struct FacebookPlayer_t2645999341;
// System.String
struct String_t;
// FacebookStory
struct FacebookStory_t448600801;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t278961118;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t3020292135;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen4280033800.h"
#include "AssemblyU2DCSharp_LoginEntrance2243089009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBManager`1<ChatmageddonFBManager>
struct  FBManager_1_t3931900237  : public MonoSingleton_1_t4280033800
{
public:
	// System.Boolean FBManager`1::init
	bool ___init_3;
	// System.Boolean FBManager`1::loggedIn
	bool ___loggedIn_4;
	// LoginEntrance FBManager`1::FBentrance
	int32_t ___FBentrance_5;
	// FacebookPlayer FBManager`1::loggedInUser
	FacebookPlayer_t2645999341 * ___loggedInUser_6;
	// System.String FBManager`1::pictureURL
	String_t* ___pictureURL_7;
	// FacebookStory FBManager`1::userStory
	FacebookStory_t448600801 * ___userStory_8;
	// System.Boolean FBManager`1::scoreBeingChecked
	bool ___scoreBeingChecked_10;

public:
	inline static int32_t get_offset_of_init_3() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___init_3)); }
	inline bool get_init_3() const { return ___init_3; }
	inline bool* get_address_of_init_3() { return &___init_3; }
	inline void set_init_3(bool value)
	{
		___init_3 = value;
	}

	inline static int32_t get_offset_of_loggedIn_4() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___loggedIn_4)); }
	inline bool get_loggedIn_4() const { return ___loggedIn_4; }
	inline bool* get_address_of_loggedIn_4() { return &___loggedIn_4; }
	inline void set_loggedIn_4(bool value)
	{
		___loggedIn_4 = value;
	}

	inline static int32_t get_offset_of_FBentrance_5() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___FBentrance_5)); }
	inline int32_t get_FBentrance_5() const { return ___FBentrance_5; }
	inline int32_t* get_address_of_FBentrance_5() { return &___FBentrance_5; }
	inline void set_FBentrance_5(int32_t value)
	{
		___FBentrance_5 = value;
	}

	inline static int32_t get_offset_of_loggedInUser_6() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___loggedInUser_6)); }
	inline FacebookPlayer_t2645999341 * get_loggedInUser_6() const { return ___loggedInUser_6; }
	inline FacebookPlayer_t2645999341 ** get_address_of_loggedInUser_6() { return &___loggedInUser_6; }
	inline void set_loggedInUser_6(FacebookPlayer_t2645999341 * value)
	{
		___loggedInUser_6 = value;
		Il2CppCodeGenWriteBarrier(&___loggedInUser_6, value);
	}

	inline static int32_t get_offset_of_pictureURL_7() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___pictureURL_7)); }
	inline String_t* get_pictureURL_7() const { return ___pictureURL_7; }
	inline String_t** get_address_of_pictureURL_7() { return &___pictureURL_7; }
	inline void set_pictureURL_7(String_t* value)
	{
		___pictureURL_7 = value;
		Il2CppCodeGenWriteBarrier(&___pictureURL_7, value);
	}

	inline static int32_t get_offset_of_userStory_8() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___userStory_8)); }
	inline FacebookStory_t448600801 * get_userStory_8() const { return ___userStory_8; }
	inline FacebookStory_t448600801 ** get_address_of_userStory_8() { return &___userStory_8; }
	inline void set_userStory_8(FacebookStory_t448600801 * value)
	{
		___userStory_8 = value;
		Il2CppCodeGenWriteBarrier(&___userStory_8, value);
	}

	inline static int32_t get_offset_of_scoreBeingChecked_10() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237, ___scoreBeingChecked_10)); }
	inline bool get_scoreBeingChecked_10() const { return ___scoreBeingChecked_10; }
	inline bool* get_address_of_scoreBeingChecked_10() { return &___scoreBeingChecked_10; }
	inline void set_scoreBeingChecked_10(bool value)
	{
		___scoreBeingChecked_10 = value;
	}
};

struct FBManager_1_t3931900237_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Hashtable> FBManager`1::scoreInfo
	List_1_t278961118 * ___scoreInfo_9;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult> FBManager`1::<>f__am$cache0
	FacebookDelegate_1_t3020292135 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_scoreInfo_9() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237_StaticFields, ___scoreInfo_9)); }
	inline List_1_t278961118 * get_scoreInfo_9() const { return ___scoreInfo_9; }
	inline List_1_t278961118 ** get_address_of_scoreInfo_9() { return &___scoreInfo_9; }
	inline void set_scoreInfo_9(List_1_t278961118 * value)
	{
		___scoreInfo_9 = value;
		Il2CppCodeGenWriteBarrier(&___scoreInfo_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(FBManager_1_t3931900237_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline FacebookDelegate_1_t3020292135 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline FacebookDelegate_1_t3020292135 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(FacebookDelegate_1_t3020292135 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
