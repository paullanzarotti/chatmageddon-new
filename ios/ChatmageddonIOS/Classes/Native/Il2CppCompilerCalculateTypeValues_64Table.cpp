﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofResolution2458755317.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofBlurriness3715737866.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BokehDestinatio1465375165.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfField342363513819.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca3436697182.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldSca3125489558.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DepthOfFieldScat622566757.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_EdgeDetectMode795813161.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_EdgeDetectEffec1361102129.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom2078071801.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom_Resol1213437956.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_FastBloom_BlurT1688240931.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Fisheye1962819339.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GlobalFog3266414943.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GlobalFog_FogMo3458294069.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_NoiseAndGrain2941273262.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsBase3757392499.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsHelp3411667524.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Quads1559374868.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ScreenOverlay927856912.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ScreenOverlay_O1055198027.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShaftsResolu2166148231.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBlen616022271.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShafts482045181.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr2199316477.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr_Ti2293276028.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TiltShiftHdr_Til305897960.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping2870642726.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping_Ton1434422982.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Tonemapping_Ada3691172207.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Triangles1920835479.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Vignetting1869961069.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Vignetting_Aber3883519246.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6400 = { sizeof (DofResolution_t2458755317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6400[4] = 
{
	DofResolution_t2458755317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6401 = { sizeof (DofBlurriness_t3715737866)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6401[4] = 
{
	DofBlurriness_t3715737866::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6402 = { sizeof (BokehDestination_t1465375165)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6402[4] = 
{
	BokehDestination_t1465375165::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6403 = { sizeof (DepthOfField34_t2363513819), -1, sizeof(DepthOfField34_t2363513819_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6403[42] = 
{
	DepthOfField34_t2363513819_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_5(),
	DepthOfField34_t2363513819_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_6(),
	DepthOfField34_t2363513819::get_offset_of_quality_7(),
	DepthOfField34_t2363513819::get_offset_of_resolution_8(),
	DepthOfField34_t2363513819::get_offset_of_simpleTweakMode_9(),
	DepthOfField34_t2363513819::get_offset_of_focalPoint_10(),
	DepthOfField34_t2363513819::get_offset_of_smoothness_11(),
	DepthOfField34_t2363513819::get_offset_of_focalZDistance_12(),
	DepthOfField34_t2363513819::get_offset_of_focalZStartCurve_13(),
	DepthOfField34_t2363513819::get_offset_of_focalZEndCurve_14(),
	DepthOfField34_t2363513819::get_offset_of_focalStartCurve_15(),
	DepthOfField34_t2363513819::get_offset_of_focalEndCurve_16(),
	DepthOfField34_t2363513819::get_offset_of_focalDistance01_17(),
	DepthOfField34_t2363513819::get_offset_of_objectFocus_18(),
	DepthOfField34_t2363513819::get_offset_of_focalSize_19(),
	DepthOfField34_t2363513819::get_offset_of_bluriness_20(),
	DepthOfField34_t2363513819::get_offset_of_maxBlurSpread_21(),
	DepthOfField34_t2363513819::get_offset_of_foregroundBlurExtrude_22(),
	DepthOfField34_t2363513819::get_offset_of_dofBlurShader_23(),
	DepthOfField34_t2363513819::get_offset_of_dofBlurMaterial_24(),
	DepthOfField34_t2363513819::get_offset_of_dofShader_25(),
	DepthOfField34_t2363513819::get_offset_of_dofMaterial_26(),
	DepthOfField34_t2363513819::get_offset_of_visualize_27(),
	DepthOfField34_t2363513819::get_offset_of_bokehDestination_28(),
	DepthOfField34_t2363513819::get_offset_of_widthOverHeight_29(),
	DepthOfField34_t2363513819::get_offset_of_oneOverBaseSize_30(),
	DepthOfField34_t2363513819::get_offset_of_bokeh_31(),
	DepthOfField34_t2363513819::get_offset_of_bokehSupport_32(),
	DepthOfField34_t2363513819::get_offset_of_bokehShader_33(),
	DepthOfField34_t2363513819::get_offset_of_bokehTexture_34(),
	DepthOfField34_t2363513819::get_offset_of_bokehScale_35(),
	DepthOfField34_t2363513819::get_offset_of_bokehIntensity_36(),
	DepthOfField34_t2363513819::get_offset_of_bokehThreshholdContrast_37(),
	DepthOfField34_t2363513819::get_offset_of_bokehThreshholdLuminance_38(),
	DepthOfField34_t2363513819::get_offset_of_bokehDownsample_39(),
	DepthOfField34_t2363513819::get_offset_of_bokehMaterial_40(),
	DepthOfField34_t2363513819::get_offset_of_foregroundTexture_41(),
	DepthOfField34_t2363513819::get_offset_of_mediumRezWorkTexture_42(),
	DepthOfField34_t2363513819::get_offset_of_finalDefocus_43(),
	DepthOfField34_t2363513819::get_offset_of_lowRezWorkTexture_44(),
	DepthOfField34_t2363513819::get_offset_of_bokehSource_45(),
	DepthOfField34_t2363513819::get_offset_of_bokehSource2_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6404 = { sizeof (DepthOfFieldScatter_t3436697182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6404[24] = 
{
	DepthOfFieldScatter_t3436697182::get_offset_of_visualizeFocus_5(),
	DepthOfFieldScatter_t3436697182::get_offset_of_focalLength_6(),
	DepthOfFieldScatter_t3436697182::get_offset_of_focalSize_7(),
	DepthOfFieldScatter_t3436697182::get_offset_of_aperture_8(),
	DepthOfFieldScatter_t3436697182::get_offset_of_focalTransform_9(),
	DepthOfFieldScatter_t3436697182::get_offset_of_maxBlurSize_10(),
	DepthOfFieldScatter_t3436697182::get_offset_of_highResolution_11(),
	DepthOfFieldScatter_t3436697182::get_offset_of_blurType_12(),
	DepthOfFieldScatter_t3436697182::get_offset_of_blurSampleCount_13(),
	DepthOfFieldScatter_t3436697182::get_offset_of_nearBlur_14(),
	DepthOfFieldScatter_t3436697182::get_offset_of_foregroundOverlap_15(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dofHdrShader_16(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dofHdrMaterial_17(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11BokehShader_18(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11bokehMaterial_19(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11BokehThreshhold_20(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11SpawnHeuristic_21(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11BokehTexture_22(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11BokehScale_23(),
	DepthOfFieldScatter_t3436697182::get_offset_of_dx11BokehIntensity_24(),
	DepthOfFieldScatter_t3436697182::get_offset_of_focalDistance01_25(),
	DepthOfFieldScatter_t3436697182::get_offset_of_cbDrawArgs_26(),
	DepthOfFieldScatter_t3436697182::get_offset_of_cbPoints_27(),
	DepthOfFieldScatter_t3436697182::get_offset_of_internalBlurWidth_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6405 = { sizeof (BlurType_t3125489558)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6405[3] = 
{
	BlurType_t3125489558::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6406 = { sizeof (BlurSampleCount_t622566757)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6406[4] = 
{
	BlurSampleCount_t622566757::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6407 = { sizeof (EdgeDetectMode_t795813161)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6407[6] = 
{
	EdgeDetectMode_t795813161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6408 = { sizeof (EdgeDetectEffectNormals_t1361102129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6408[11] = 
{
	EdgeDetectEffectNormals_t1361102129::get_offset_of_mode_5(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_sensitivityDepth_6(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_sensitivityNormals_7(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_lumThreshhold_8(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_edgeExp_9(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_sampleDist_10(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_edgesOnly_11(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_edgesOnlyBgColor_12(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_edgeDetectShader_13(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_edgeDetectMaterial_14(),
	EdgeDetectEffectNormals_t1361102129::get_offset_of_oldMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6409 = { sizeof (FastBloom_t2078071801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6409[8] = 
{
	FastBloom_t2078071801::get_offset_of_threshhold_5(),
	FastBloom_t2078071801::get_offset_of_intensity_6(),
	FastBloom_t2078071801::get_offset_of_blurSize_7(),
	FastBloom_t2078071801::get_offset_of_resolution_8(),
	FastBloom_t2078071801::get_offset_of_blurIterations_9(),
	FastBloom_t2078071801::get_offset_of_blurType_10(),
	FastBloom_t2078071801::get_offset_of_fastBloomShader_11(),
	FastBloom_t2078071801::get_offset_of_fastBloomMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6410 = { sizeof (Resolution_t1213437956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6410[3] = 
{
	Resolution_t1213437956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6411 = { sizeof (BlurType_t1688240931)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6411[3] = 
{
	BlurType_t1688240931::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6412 = { sizeof (Fisheye_t1962819339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6412[4] = 
{
	Fisheye_t1962819339::get_offset_of_strengthX_5(),
	Fisheye_t1962819339::get_offset_of_strengthY_6(),
	Fisheye_t1962819339::get_offset_of_fishEyeShader_7(),
	Fisheye_t1962819339::get_offset_of_fisheyeMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6413 = { sizeof (GlobalFog_t3266414943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6413[12] = 
{
	GlobalFog_t3266414943::get_offset_of_fogMode_5(),
	GlobalFog_t3266414943::get_offset_of_CAMERA_NEAR_6(),
	GlobalFog_t3266414943::get_offset_of_CAMERA_FAR_7(),
	GlobalFog_t3266414943::get_offset_of_CAMERA_FOV_8(),
	GlobalFog_t3266414943::get_offset_of_CAMERA_ASPECT_RATIO_9(),
	GlobalFog_t3266414943::get_offset_of_startDistance_10(),
	GlobalFog_t3266414943::get_offset_of_globalDensity_11(),
	GlobalFog_t3266414943::get_offset_of_heightScale_12(),
	GlobalFog_t3266414943::get_offset_of_height_13(),
	GlobalFog_t3266414943::get_offset_of_globalFogColor_14(),
	GlobalFog_t3266414943::get_offset_of_fogShader_15(),
	GlobalFog_t3266414943::get_offset_of_fogMaterial_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6414 = { sizeof (FogMode_t3458294069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6414[5] = 
{
	FogMode_t3458294069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6415 = { sizeof (NoiseAndGrain_t2941273262), -1, sizeof(NoiseAndGrain_t2941273262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6415[18] = 
{
	NoiseAndGrain_t2941273262::get_offset_of_intensityMultiplier_5(),
	NoiseAndGrain_t2941273262::get_offset_of_generalIntensity_6(),
	NoiseAndGrain_t2941273262::get_offset_of_blackIntensity_7(),
	NoiseAndGrain_t2941273262::get_offset_of_whiteIntensity_8(),
	NoiseAndGrain_t2941273262::get_offset_of_midGrey_9(),
	NoiseAndGrain_t2941273262::get_offset_of_dx11Grain_10(),
	NoiseAndGrain_t2941273262::get_offset_of_softness_11(),
	NoiseAndGrain_t2941273262::get_offset_of_monochrome_12(),
	NoiseAndGrain_t2941273262::get_offset_of_intensities_13(),
	NoiseAndGrain_t2941273262::get_offset_of_tiling_14(),
	NoiseAndGrain_t2941273262::get_offset_of_monochromeTiling_15(),
	NoiseAndGrain_t2941273262::get_offset_of_filterMode_16(),
	NoiseAndGrain_t2941273262::get_offset_of_noiseTexture_17(),
	NoiseAndGrain_t2941273262::get_offset_of_noiseShader_18(),
	NoiseAndGrain_t2941273262::get_offset_of_noiseMaterial_19(),
	NoiseAndGrain_t2941273262::get_offset_of_dx11NoiseShader_20(),
	NoiseAndGrain_t2941273262::get_offset_of_dx11NoiseMaterial_21(),
	NoiseAndGrain_t2941273262_StaticFields::get_offset_of_TILE_AMOUNT_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6416 = { sizeof (PostEffectsBase_t3757392499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6416[3] = 
{
	PostEffectsBase_t3757392499::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t3757392499::get_offset_of_supportDX11_3(),
	PostEffectsBase_t3757392499::get_offset_of_isSupported_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6417 = { sizeof (PostEffectsHelper_t3411667524), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6418 = { sizeof (Quads_t1559374868), -1, sizeof(Quads_t1559374868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6418[2] = 
{
	Quads_t1559374868_StaticFields::get_offset_of_meshes_2(),
	Quads_t1559374868_StaticFields::get_offset_of_currentQuads_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6419 = { sizeof (ScreenOverlay_t927856912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6419[5] = 
{
	ScreenOverlay_t927856912::get_offset_of_blendMode_5(),
	ScreenOverlay_t927856912::get_offset_of_intensity_6(),
	ScreenOverlay_t927856912::get_offset_of_texture_7(),
	ScreenOverlay_t927856912::get_offset_of_overlayShader_8(),
	ScreenOverlay_t927856912::get_offset_of_overlayMaterial_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6420 = { sizeof (OverlayBlendMode_t1055198027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6420[6] = 
{
	OverlayBlendMode_t1055198027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6421 = { sizeof (SunShaftsResolution_t2166148231)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6421[4] = 
{
	SunShaftsResolution_t2166148231::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6422 = { sizeof (ShaftsScreenBlendMode_t616022271)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6422[3] = 
{
	ShaftsScreenBlendMode_t616022271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6423 = { sizeof (SunShafts_t482045181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6423[14] = 
{
	SunShafts_t482045181::get_offset_of_resolution_5(),
	SunShafts_t482045181::get_offset_of_screenBlendMode_6(),
	SunShafts_t482045181::get_offset_of_sunTransform_7(),
	SunShafts_t482045181::get_offset_of_radialBlurIterations_8(),
	SunShafts_t482045181::get_offset_of_sunColor_9(),
	SunShafts_t482045181::get_offset_of_sunShaftBlurRadius_10(),
	SunShafts_t482045181::get_offset_of_sunShaftIntensity_11(),
	SunShafts_t482045181::get_offset_of_useSkyBoxAlpha_12(),
	SunShafts_t482045181::get_offset_of_maxRadius_13(),
	SunShafts_t482045181::get_offset_of_useDepthTexture_14(),
	SunShafts_t482045181::get_offset_of_sunShaftsShader_15(),
	SunShafts_t482045181::get_offset_of_sunShaftsMaterial_16(),
	SunShafts_t482045181::get_offset_of_simpleClearShader_17(),
	SunShafts_t482045181::get_offset_of_simpleClearMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6424 = { sizeof (TiltShiftHdr_t2199316477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6424[7] = 
{
	TiltShiftHdr_t2199316477::get_offset_of_mode_5(),
	TiltShiftHdr_t2199316477::get_offset_of_quality_6(),
	TiltShiftHdr_t2199316477::get_offset_of_blurArea_7(),
	TiltShiftHdr_t2199316477::get_offset_of_maxBlurSize_8(),
	TiltShiftHdr_t2199316477::get_offset_of_downsample_9(),
	TiltShiftHdr_t2199316477::get_offset_of_tiltShiftShader_10(),
	TiltShiftHdr_t2199316477::get_offset_of_tiltShiftMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6425 = { sizeof (TiltShiftMode_t2293276028)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6425[3] = 
{
	TiltShiftMode_t2293276028::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6426 = { sizeof (TiltShiftQuality_t305897960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6426[4] = 
{
	TiltShiftQuality_t305897960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6427 = { sizeof (Tonemapping_t2870642726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6427[13] = 
{
	Tonemapping_t2870642726::get_offset_of_type_5(),
	Tonemapping_t2870642726::get_offset_of_adaptiveTextureSize_6(),
	Tonemapping_t2870642726::get_offset_of_remapCurve_7(),
	Tonemapping_t2870642726::get_offset_of_curveTex_8(),
	Tonemapping_t2870642726::get_offset_of_exposureAdjustment_9(),
	Tonemapping_t2870642726::get_offset_of_middleGrey_10(),
	Tonemapping_t2870642726::get_offset_of_white_11(),
	Tonemapping_t2870642726::get_offset_of_adaptionSpeed_12(),
	Tonemapping_t2870642726::get_offset_of_tonemapper_13(),
	Tonemapping_t2870642726::get_offset_of_validRenderTextureFormat_14(),
	Tonemapping_t2870642726::get_offset_of_tonemapMaterial_15(),
	Tonemapping_t2870642726::get_offset_of_rt_16(),
	Tonemapping_t2870642726::get_offset_of_rtFormat_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6428 = { sizeof (TonemapperType_t1434422982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6428[8] = 
{
	TonemapperType_t1434422982::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6429 = { sizeof (AdaptiveTexSize_t3691172207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6429[8] = 
{
	AdaptiveTexSize_t3691172207::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6430 = { sizeof (Triangles_t1920835479), -1, sizeof(Triangles_t1920835479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6430[2] = 
{
	Triangles_t1920835479_StaticFields::get_offset_of_meshes_2(),
	Triangles_t1920835479_StaticFields::get_offset_of_currentTris_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6431 = { sizeof (Vignetting_t1869961069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6431[14] = 
{
	Vignetting_t1869961069::get_offset_of_mode_5(),
	Vignetting_t1869961069::get_offset_of_intensity_6(),
	Vignetting_t1869961069::get_offset_of_chromaticAberration_7(),
	Vignetting_t1869961069::get_offset_of_axialAberration_8(),
	Vignetting_t1869961069::get_offset_of_blur_9(),
	Vignetting_t1869961069::get_offset_of_blurSpread_10(),
	Vignetting_t1869961069::get_offset_of_luminanceDependency_11(),
	Vignetting_t1869961069::get_offset_of_blurDistance_12(),
	Vignetting_t1869961069::get_offset_of_vignetteShader_13(),
	Vignetting_t1869961069::get_offset_of_vignetteMaterial_14(),
	Vignetting_t1869961069::get_offset_of_separableBlurShader_15(),
	Vignetting_t1869961069::get_offset_of_separableBlurMaterial_16(),
	Vignetting_t1869961069::get_offset_of_chromAberrationShader_17(),
	Vignetting_t1869961069::get_offset_of_chromAberrationMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6432 = { sizeof (AberrationMode_t3883519246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6432[3] = 
{
	AberrationMode_t3883519246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
