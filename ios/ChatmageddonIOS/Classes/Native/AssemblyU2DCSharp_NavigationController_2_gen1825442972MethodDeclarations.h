﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen155028990MethodDeclarations.h"

// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::.ctor()
#define NavigationController_2__ctor_m2827120636(__this, method) ((  void (*) (NavigationController_2_t1825442972 *, const MethodInfo*))NavigationController_2__ctor_m3184207255_gshared)(__this, method)
// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m2002478018(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t1825442972 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m402874753_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m3576654917(__this, method) ((  void (*) (NavigationController_2_t1825442972 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3978899914_gshared)(__this, method)
// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m196388460(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t1825442972 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3977220309_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<SettingsNavigationController,SettingsNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m2240934160(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t1825442972 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m1643361941_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<SettingsNavigationController,SettingsNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m3065277867(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t1825442972 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m708187624_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m3589926294(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t1825442972 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m4096629085_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m2589348600(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t1825442972 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m126515209_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<SettingsNavigationController,SettingsNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m2294151040(__this, ___screen0, method) ((  void (*) (NavigationController_2_t1825442972 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1326194473_gshared)(__this, ___screen0, method)
