﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackReviewNavScreen
struct AttackReviewNavScreen_t173504869;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackReviewNavScreen::.ctor()
extern "C"  void AttackReviewNavScreen__ctor_m2137508336 (AttackReviewNavScreen_t173504869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewNavScreen::Start()
extern "C"  void AttackReviewNavScreen_Start_m258907648 (AttackReviewNavScreen_t173504869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewNavScreen::UIClosing()
extern "C"  void AttackReviewNavScreen_UIClosing_m2049993807 (AttackReviewNavScreen_t173504869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackReviewNavScreen_ScreenLoad_m1088782976 (AttackReviewNavScreen_t173504869 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewNavScreen::StartAimAnimation(System.Boolean)
extern "C"  void AttackReviewNavScreen_StartAimAnimation_m1615757144 (AttackReviewNavScreen_t173504869 * __this, bool ___end0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackReviewNavScreen_ScreenUnload_m533061656 (AttackReviewNavScreen_t173504869 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackReviewNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackReviewNavScreen_ValidateScreenNavigation_m3225578375 (AttackReviewNavScreen_t173504869 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewNavScreen::SetLightActive(System.Boolean)
extern "C"  void AttackReviewNavScreen_SetLightActive_m2276098595 (AttackReviewNavScreen_t173504869 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
