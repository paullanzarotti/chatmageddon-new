﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapGroupModal
struct MapGroupModal_t1530151518;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"

// System.Void MapGroupModal::.ctor()
extern "C"  void MapGroupModal__ctor_m631380011 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::OnEnable()
extern "C"  void MapGroupModal_OnEnable_m3056947439 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::ActivateGroup(System.Collections.Generic.List`1<Friend>)
extern "C"  void MapGroupModal_ActivateGroup_m1256453497 (MapGroupModal_t1530151518 * __this, List_1_t2924135240 * ___groupFriends0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::UpdateAvatar(System.String)
extern "C"  void MapGroupModal_UpdateAvatar_m3872885989 (MapGroupModal_t1530151518 * __this, String_t* ___userID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::OnUIActivate()
extern "C"  void MapGroupModal_OnUIActivate_m2637396251 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::DeactivateModal()
extern "C"  void MapGroupModal_DeactivateModal_m4194014718 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MapGroupModal::WaitToClose()
extern "C"  Il2CppObject * MapGroupModal_WaitToClose_m2203878151 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::OnUIDeactivate()
extern "C"  void MapGroupModal_OnUIDeactivate_m689922796 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::OnFadeTweenFinished()
extern "C"  void MapGroupModal_OnFadeTweenFinished_m1085171239 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal::OnUITweenFinished()
extern "C"  void MapGroupModal_OnUITweenFinished_m1566750381 (MapGroupModal_t1530151518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MapGroupModal::<ActivateGroup>m__0(Friend)
extern "C"  int32_t MapGroupModal_U3CActivateGroupU3Em__0_m3532149894 (Il2CppObject * __this /* static, unused */, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
