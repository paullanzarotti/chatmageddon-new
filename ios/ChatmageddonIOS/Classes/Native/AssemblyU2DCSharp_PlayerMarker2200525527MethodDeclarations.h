﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerMarker
struct PlayerMarker_t2200525527;
// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// Player
struct Player_t1147783557;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3D576815539.h"
#include "AssemblyU2DCSharp_Player1147783557.h"

// System.Void PlayerMarker::.ctor()
extern "C"  void PlayerMarker__ctor_m3543018174 (PlayerMarker_t2200525527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMarker::InitPlayerMarker(OnlineMapsMarker3D,Player)
extern "C"  void PlayerMarker_InitPlayerMarker_m1110454647 (PlayerMarker_t2200525527 * __this, OnlineMapsMarker3D_t576815539 * ___mapMarker0, Player_t1147783557 * ___initPlayer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMarker::UpdateMarker(System.Int32)
extern "C"  void PlayerMarker_UpdateMarker_m257941014 (PlayerMarker_t2200525527 * __this, int32_t ___zoomLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMarker::TriggerEntered()
extern "C"  void PlayerMarker_TriggerEntered_m1034773043 (PlayerMarker_t2200525527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMarker::TriggerExited()
extern "C"  void PlayerMarker_TriggerExited_m2015407503 (PlayerMarker_t2200525527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMarker::OnClick()
extern "C"  void PlayerMarker_OnClick_m3087440573 (PlayerMarker_t2200525527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
