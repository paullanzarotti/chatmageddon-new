﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPPickerBase
struct IPPickerBase_t4159478266;
// IPCycler
struct IPCycler_t1336138445;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CyclerClamper
struct  CyclerClamper_t2048159144  : public MonoBehaviour_t1158329972
{
public:
	// IPPickerBase CyclerClamper::observedPicker
	IPPickerBase_t4159478266 * ___observedPicker_2;
	// IPCycler CyclerClamper::_cycler
	IPCycler_t1336138445 * ____cycler_3;

public:
	inline static int32_t get_offset_of_observedPicker_2() { return static_cast<int32_t>(offsetof(CyclerClamper_t2048159144, ___observedPicker_2)); }
	inline IPPickerBase_t4159478266 * get_observedPicker_2() const { return ___observedPicker_2; }
	inline IPPickerBase_t4159478266 ** get_address_of_observedPicker_2() { return &___observedPicker_2; }
	inline void set_observedPicker_2(IPPickerBase_t4159478266 * value)
	{
		___observedPicker_2 = value;
		Il2CppCodeGenWriteBarrier(&___observedPicker_2, value);
	}

	inline static int32_t get_offset_of__cycler_3() { return static_cast<int32_t>(offsetof(CyclerClamper_t2048159144, ____cycler_3)); }
	inline IPCycler_t1336138445 * get__cycler_3() const { return ____cycler_3; }
	inline IPCycler_t1336138445 ** get_address_of__cycler_3() { return &____cycler_3; }
	inline void set__cycler_3(IPCycler_t1336138445 * value)
	{
		____cycler_3 = value;
		Il2CppCodeGenWriteBarrier(&____cycler_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
