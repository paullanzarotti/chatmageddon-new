﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.FlyweightMapStorage/ByteBuffer
struct ByteBuffer_t1505679999;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.FlyweightMapStorage/ByteBuffer::.ctor(System.Int32)
extern "C"  void ByteBuffer__ctor_m891012887 (ByteBuffer_t1505679999 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.FlyweightMapStorage/ByteBuffer::putShort(System.Int32,System.Int16)
extern "C"  void ByteBuffer_putShort_m6221503 (ByteBuffer_t1505679999 * __this, int32_t ___offset0, int16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.FlyweightMapStorage/ByteBuffer::putInt(System.Int32,System.Int32)
extern "C"  void ByteBuffer_putInt_m4205024702 (ByteBuffer_t1505679999 * __this, int32_t ___offset0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 PhoneNumbers.FlyweightMapStorage/ByteBuffer::getShort(System.Int32)
extern "C"  int16_t ByteBuffer_getShort_m242197431 (ByteBuffer_t1505679999 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage/ByteBuffer::getInt(System.Int32)
extern "C"  int32_t ByteBuffer_getInt_m2742933306 (ByteBuffer_t1505679999 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.FlyweightMapStorage/ByteBuffer::getCapacity()
extern "C"  int32_t ByteBuffer_getCapacity_m3713127276 (ByteBuffer_t1505679999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
