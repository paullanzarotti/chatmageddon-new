﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3266827797.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalManagersController
struct  GlobalManagersController_t3516162077  : public MonoSingleton_1_t3266827797
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> GlobalManagersController::activeGManagers
	Dictionary_2_t3671312409 * ___activeGManagers_3;

public:
	inline static int32_t get_offset_of_activeGManagers_3() { return static_cast<int32_t>(offsetof(GlobalManagersController_t3516162077, ___activeGManagers_3)); }
	inline Dictionary_2_t3671312409 * get_activeGManagers_3() const { return ___activeGManagers_3; }
	inline Dictionary_2_t3671312409 ** get_address_of_activeGManagers_3() { return &___activeGManagers_3; }
	inline void set_activeGManagers_3(Dictionary_2_t3671312409 * value)
	{
		___activeGManagers_3 = value;
		Il2CppCodeGenWriteBarrier(&___activeGManagers_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
