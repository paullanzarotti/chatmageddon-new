﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SearchFriendsNavScreen
struct SearchFriendsNavScreen_t1265140554;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void SearchFriendsNavScreen::.ctor()
extern "C"  void SearchFriendsNavScreen__ctor_m2771724609 (SearchFriendsNavScreen_t1265140554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchFriendsNavScreen::Start()
extern "C"  void SearchFriendsNavScreen_Start_m4132176649 (SearchFriendsNavScreen_t1265140554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchFriendsNavScreen::UIClosing()
extern "C"  void SearchFriendsNavScreen_UIClosing_m2743554796 (SearchFriendsNavScreen_t1265140554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchFriendsNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void SearchFriendsNavScreen_ScreenLoad_m3269056863 (SearchFriendsNavScreen_t1265140554 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchFriendsNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void SearchFriendsNavScreen_ScreenUnload_m4059882447 (SearchFriendsNavScreen_t1265140554 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SearchFriendsNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool SearchFriendsNavScreen_ValidateScreenNavigation_m3051014006 (SearchFriendsNavScreen_t1265140554 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
