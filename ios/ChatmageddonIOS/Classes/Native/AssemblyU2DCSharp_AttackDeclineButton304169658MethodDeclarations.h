﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackDeclineButton
struct AttackDeclineButton_t304169658;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackDeclineButton::.ctor()
extern "C"  void AttackDeclineButton__ctor_m3382668165 (AttackDeclineButton_t304169658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackDeclineButton::OnButtonClick()
extern "C"  void AttackDeclineButton_OnButtonClick_m274236050 (AttackDeclineButton_t304169658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
