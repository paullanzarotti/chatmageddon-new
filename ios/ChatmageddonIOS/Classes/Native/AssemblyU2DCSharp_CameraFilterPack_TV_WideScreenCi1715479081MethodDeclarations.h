﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_WideScreenCircle
struct CameraFilterPack_TV_WideScreenCircle_t1715479081;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_WideScreenCircle::.ctor()
extern "C"  void CameraFilterPack_TV_WideScreenCircle__ctor_m3668561610 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenCircle::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_WideScreenCircle_get_material_m3546075589 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::Start()
extern "C"  void CameraFilterPack_TV_WideScreenCircle_Start_m421622922 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_WideScreenCircle_OnRenderImage_m4179209938 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::OnValidate()
extern "C"  void CameraFilterPack_TV_WideScreenCircle_OnValidate_m3232214959 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::Update()
extern "C"  void CameraFilterPack_TV_WideScreenCircle_Update_m1540684489 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::OnDisable()
extern "C"  void CameraFilterPack_TV_WideScreenCircle_OnDisable_m27396981 (CameraFilterPack_TV_WideScreenCircle_t1715479081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
