﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenScale
struct TweenScale_t2697902175;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UISprite>
struct List_1_t4267705163;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LongNotificationModal
struct  LongNotificationModal_t834782082  : public ModalUI_t2568752073
{
public:
	// UIPanel LongNotificationModal::panel
	UIPanel_t1795085332 * ___panel_3;
	// TweenAlpha LongNotificationModal::backgroundFade
	TweenAlpha_t2421518635 * ___backgroundFade_4;
	// TweenScale LongNotificationModal::UITween
	TweenScale_t2697902175 * ___UITween_5;
	// UISprite LongNotificationModal::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_6;
	// UILabel LongNotificationModal::titleLabel
	UILabel_t1795115428 * ___titleLabel_7;
	// UILabel LongNotificationModal::bodyLabel
	UILabel_t1795115428 * ___bodyLabel_8;
	// UnityEngine.GameObject LongNotificationModal::okButton
	GameObject_t1756533147 * ___okButton_9;
	// UISprite LongNotificationModal::okButtonBackground
	UISprite_t603616735 * ___okButtonBackground_10;
	// UnityEngine.GameObject LongNotificationModal::clearAllButton
	GameObject_t1756533147 * ___clearAllButton_11;
	// UILabel LongNotificationModal::countLabel
	UILabel_t1795115428 * ___countLabel_12;
	// System.Int32 LongNotificationModal::currentNotification
	int32_t ___currentNotification_13;
	// System.Collections.Generic.List`1<UISprite> LongNotificationModal::spriteList
	List_1_t4267705163 * ___spriteList_14;
	// System.Boolean LongNotificationModal::modalClosing
	bool ___modalClosing_15;

public:
	inline static int32_t get_offset_of_panel_3() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___panel_3)); }
	inline UIPanel_t1795085332 * get_panel_3() const { return ___panel_3; }
	inline UIPanel_t1795085332 ** get_address_of_panel_3() { return &___panel_3; }
	inline void set_panel_3(UIPanel_t1795085332 * value)
	{
		___panel_3 = value;
		Il2CppCodeGenWriteBarrier(&___panel_3, value);
	}

	inline static int32_t get_offset_of_backgroundFade_4() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___backgroundFade_4)); }
	inline TweenAlpha_t2421518635 * get_backgroundFade_4() const { return ___backgroundFade_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundFade_4() { return &___backgroundFade_4; }
	inline void set_backgroundFade_4(TweenAlpha_t2421518635 * value)
	{
		___backgroundFade_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundFade_4, value);
	}

	inline static int32_t get_offset_of_UITween_5() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___UITween_5)); }
	inline TweenScale_t2697902175 * get_UITween_5() const { return ___UITween_5; }
	inline TweenScale_t2697902175 ** get_address_of_UITween_5() { return &___UITween_5; }
	inline void set_UITween_5(TweenScale_t2697902175 * value)
	{
		___UITween_5 = value;
		Il2CppCodeGenWriteBarrier(&___UITween_5, value);
	}

	inline static int32_t get_offset_of_backgroundSprite_6() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___backgroundSprite_6)); }
	inline UISprite_t603616735 * get_backgroundSprite_6() const { return ___backgroundSprite_6; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_6() { return &___backgroundSprite_6; }
	inline void set_backgroundSprite_6(UISprite_t603616735 * value)
	{
		___backgroundSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_6, value);
	}

	inline static int32_t get_offset_of_titleLabel_7() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___titleLabel_7)); }
	inline UILabel_t1795115428 * get_titleLabel_7() const { return ___titleLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_7() { return &___titleLabel_7; }
	inline void set_titleLabel_7(UILabel_t1795115428 * value)
	{
		___titleLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_7, value);
	}

	inline static int32_t get_offset_of_bodyLabel_8() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___bodyLabel_8)); }
	inline UILabel_t1795115428 * get_bodyLabel_8() const { return ___bodyLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_bodyLabel_8() { return &___bodyLabel_8; }
	inline void set_bodyLabel_8(UILabel_t1795115428 * value)
	{
		___bodyLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___bodyLabel_8, value);
	}

	inline static int32_t get_offset_of_okButton_9() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___okButton_9)); }
	inline GameObject_t1756533147 * get_okButton_9() const { return ___okButton_9; }
	inline GameObject_t1756533147 ** get_address_of_okButton_9() { return &___okButton_9; }
	inline void set_okButton_9(GameObject_t1756533147 * value)
	{
		___okButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___okButton_9, value);
	}

	inline static int32_t get_offset_of_okButtonBackground_10() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___okButtonBackground_10)); }
	inline UISprite_t603616735 * get_okButtonBackground_10() const { return ___okButtonBackground_10; }
	inline UISprite_t603616735 ** get_address_of_okButtonBackground_10() { return &___okButtonBackground_10; }
	inline void set_okButtonBackground_10(UISprite_t603616735 * value)
	{
		___okButtonBackground_10 = value;
		Il2CppCodeGenWriteBarrier(&___okButtonBackground_10, value);
	}

	inline static int32_t get_offset_of_clearAllButton_11() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___clearAllButton_11)); }
	inline GameObject_t1756533147 * get_clearAllButton_11() const { return ___clearAllButton_11; }
	inline GameObject_t1756533147 ** get_address_of_clearAllButton_11() { return &___clearAllButton_11; }
	inline void set_clearAllButton_11(GameObject_t1756533147 * value)
	{
		___clearAllButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___clearAllButton_11, value);
	}

	inline static int32_t get_offset_of_countLabel_12() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___countLabel_12)); }
	inline UILabel_t1795115428 * get_countLabel_12() const { return ___countLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_countLabel_12() { return &___countLabel_12; }
	inline void set_countLabel_12(UILabel_t1795115428 * value)
	{
		___countLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___countLabel_12, value);
	}

	inline static int32_t get_offset_of_currentNotification_13() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___currentNotification_13)); }
	inline int32_t get_currentNotification_13() const { return ___currentNotification_13; }
	inline int32_t* get_address_of_currentNotification_13() { return &___currentNotification_13; }
	inline void set_currentNotification_13(int32_t value)
	{
		___currentNotification_13 = value;
	}

	inline static int32_t get_offset_of_spriteList_14() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___spriteList_14)); }
	inline List_1_t4267705163 * get_spriteList_14() const { return ___spriteList_14; }
	inline List_1_t4267705163 ** get_address_of_spriteList_14() { return &___spriteList_14; }
	inline void set_spriteList_14(List_1_t4267705163 * value)
	{
		___spriteList_14 = value;
		Il2CppCodeGenWriteBarrier(&___spriteList_14, value);
	}

	inline static int32_t get_offset_of_modalClosing_15() { return static_cast<int32_t>(offsetof(LongNotificationModal_t834782082, ___modalClosing_15)); }
	inline bool get_modalClosing_15() const { return ___modalClosing_15; }
	inline bool* get_address_of_modalClosing_15() { return &___modalClosing_15; }
	inline void set_modalClosing_15(bool value)
	{
		___modalClosing_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
