﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneMetaNumberFormat
struct  PhoneMetaNumberFormat_t4038650015  : public Il2CppObject
{
public:
	// System.String PhoneMetaNumberFormat::m_pattern
	String_t* ___m_pattern_0;
	// System.String PhoneMetaNumberFormat::m_nationalPrefixFormattingRule
	String_t* ___m_nationalPrefixFormattingRule_1;
	// System.String PhoneMetaNumberFormat::m_carrierCodeFormattingRule
	String_t* ___m_carrierCodeFormattingRule_2;
	// System.Boolean PhoneMetaNumberFormat::m_nationalPrefixOptionalWhenFormatting
	bool ___m_nationalPrefixOptionalWhenFormatting_3;
	// System.String PhoneMetaNumberFormat::format
	String_t* ___format_4;
	// System.String PhoneMetaNumberFormat::intlFormat
	String_t* ___intlFormat_5;
	// System.String PhoneMetaNumberFormat::leadingDigits
	String_t* ___leadingDigits_6;

public:
	inline static int32_t get_offset_of_m_pattern_0() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___m_pattern_0)); }
	inline String_t* get_m_pattern_0() const { return ___m_pattern_0; }
	inline String_t** get_address_of_m_pattern_0() { return &___m_pattern_0; }
	inline void set_m_pattern_0(String_t* value)
	{
		___m_pattern_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_pattern_0, value);
	}

	inline static int32_t get_offset_of_m_nationalPrefixFormattingRule_1() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___m_nationalPrefixFormattingRule_1)); }
	inline String_t* get_m_nationalPrefixFormattingRule_1() const { return ___m_nationalPrefixFormattingRule_1; }
	inline String_t** get_address_of_m_nationalPrefixFormattingRule_1() { return &___m_nationalPrefixFormattingRule_1; }
	inline void set_m_nationalPrefixFormattingRule_1(String_t* value)
	{
		___m_nationalPrefixFormattingRule_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_nationalPrefixFormattingRule_1, value);
	}

	inline static int32_t get_offset_of_m_carrierCodeFormattingRule_2() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___m_carrierCodeFormattingRule_2)); }
	inline String_t* get_m_carrierCodeFormattingRule_2() const { return ___m_carrierCodeFormattingRule_2; }
	inline String_t** get_address_of_m_carrierCodeFormattingRule_2() { return &___m_carrierCodeFormattingRule_2; }
	inline void set_m_carrierCodeFormattingRule_2(String_t* value)
	{
		___m_carrierCodeFormattingRule_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_carrierCodeFormattingRule_2, value);
	}

	inline static int32_t get_offset_of_m_nationalPrefixOptionalWhenFormatting_3() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___m_nationalPrefixOptionalWhenFormatting_3)); }
	inline bool get_m_nationalPrefixOptionalWhenFormatting_3() const { return ___m_nationalPrefixOptionalWhenFormatting_3; }
	inline bool* get_address_of_m_nationalPrefixOptionalWhenFormatting_3() { return &___m_nationalPrefixOptionalWhenFormatting_3; }
	inline void set_m_nationalPrefixOptionalWhenFormatting_3(bool value)
	{
		___m_nationalPrefixOptionalWhenFormatting_3 = value;
	}

	inline static int32_t get_offset_of_format_4() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___format_4)); }
	inline String_t* get_format_4() const { return ___format_4; }
	inline String_t** get_address_of_format_4() { return &___format_4; }
	inline void set_format_4(String_t* value)
	{
		___format_4 = value;
		Il2CppCodeGenWriteBarrier(&___format_4, value);
	}

	inline static int32_t get_offset_of_intlFormat_5() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___intlFormat_5)); }
	inline String_t* get_intlFormat_5() const { return ___intlFormat_5; }
	inline String_t** get_address_of_intlFormat_5() { return &___intlFormat_5; }
	inline void set_intlFormat_5(String_t* value)
	{
		___intlFormat_5 = value;
		Il2CppCodeGenWriteBarrier(&___intlFormat_5, value);
	}

	inline static int32_t get_offset_of_leadingDigits_6() { return static_cast<int32_t>(offsetof(PhoneMetaNumberFormat_t4038650015, ___leadingDigits_6)); }
	inline String_t* get_leadingDigits_6() const { return ___leadingDigits_6; }
	inline String_t** get_address_of_leadingDigits_6() { return &___leadingDigits_6; }
	inline void set_leadingDigits_6(String_t* value)
	{
		___leadingDigits_6 = value;
		Il2CppCodeGenWriteBarrier(&___leadingDigits_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
