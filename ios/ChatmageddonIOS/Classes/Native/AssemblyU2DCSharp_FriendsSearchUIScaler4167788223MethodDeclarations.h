﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsSearchUIScaler
struct FriendsSearchUIScaler_t4167788223;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsSearchUIScaler::.ctor()
extern "C"  void FriendsSearchUIScaler__ctor_m3926257726 (FriendsSearchUIScaler_t4167788223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchUIScaler::GlobalUIScale()
extern "C"  void FriendsSearchUIScaler_GlobalUIScale_m2966679797 (FriendsSearchUIScaler_t4167788223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
