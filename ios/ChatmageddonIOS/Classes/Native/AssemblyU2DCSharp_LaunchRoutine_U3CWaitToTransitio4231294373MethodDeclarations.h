﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchRoutine/<WaitToTransitionScene>c__Iterator5
struct U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchRoutine/<WaitToTransitionScene>c__Iterator5::.ctor()
extern "C"  void U3CWaitToTransitionSceneU3Ec__Iterator5__ctor_m457302258 (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaunchRoutine/<WaitToTransitionScene>c__Iterator5::MoveNext()
extern "C"  bool U3CWaitToTransitionSceneU3Ec__Iterator5_MoveNext_m3819438606 (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<WaitToTransitionScene>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToTransitionSceneU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2266127328 (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<WaitToTransitionScene>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToTransitionSceneU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3847584904 (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<WaitToTransitionScene>c__Iterator5::Dispose()
extern "C"  void U3CWaitToTransitionSceneU3Ec__Iterator5_Dispose_m626541231 (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<WaitToTransitionScene>c__Iterator5::Reset()
extern "C"  void U3CWaitToTransitionSceneU3Ec__Iterator5_Reset_m3003697229 (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
