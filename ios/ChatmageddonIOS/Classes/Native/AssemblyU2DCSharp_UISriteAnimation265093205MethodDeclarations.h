﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISriteAnimation
struct UISriteAnimation_t265093205;

#include "codegen/il2cpp-codegen.h"

// System.Void UISriteAnimation::.ctor()
extern "C"  void UISriteAnimation__ctor_m3412193236 (UISriteAnimation_t265093205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISriteAnimation::SetAnimDepth(System.Int32)
extern "C"  void UISriteAnimation_SetAnimDepth_m482924649 (UISriteAnimation_t265093205 * __this, int32_t ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISriteAnimation::GetAnimDepth()
extern "C"  int32_t UISriteAnimation_GetAnimDepth_m4034045070 (UISriteAnimation_t265093205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISriteAnimation::SetAnimWidth(System.Int32)
extern "C"  void UISriteAnimation_SetAnimWidth_m1140204300 (UISriteAnimation_t265093205 * __this, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISriteAnimation::GetAnimWidth()
extern "C"  int32_t UISriteAnimation_GetAnimWidth_m89545769 (UISriteAnimation_t265093205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISriteAnimation::SetAnimHeight(System.Int32)
extern "C"  void UISriteAnimation_SetAnimHeight_m4151832331 (UISriteAnimation_t265093205 * __this, int32_t ___height0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISriteAnimation::GetAnimHeight()
extern "C"  int32_t UISriteAnimation_GetAnimHeight_m3460247492 (UISriteAnimation_t265093205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
