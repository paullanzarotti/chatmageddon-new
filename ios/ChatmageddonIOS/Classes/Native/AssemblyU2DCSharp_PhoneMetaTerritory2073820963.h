﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// PhoneMetaNumberFormat[]
struct PhoneMetaNumberFormatU5BU5D_t2158599686;
// PhoneMetaNumberInfo
struct PhoneMetaNumberInfo_t3299665448;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneMetaTerritory
struct  PhoneMetaTerritory_t2073820963  : public Il2CppObject
{
public:
	// System.String PhoneMetaTerritory::m_id
	String_t* ___m_id_0;
	// System.String PhoneMetaTerritory::m_countryCode
	String_t* ___m_countryCode_1;
	// System.String PhoneMetaTerritory::m_leadingDigits
	String_t* ___m_leadingDigits_2;
	// System.String PhoneMetaTerritory::m_preferredInternationalprefix
	String_t* ___m_preferredInternationalprefix_3;
	// System.String PhoneMetaTerritory::m_internationalPrefix
	String_t* ___m_internationalPrefix_4;
	// System.String PhoneMetaTerritory::m_nationalPrefix
	String_t* ___m_nationalPrefix_5;
	// System.String PhoneMetaTerritory::m_nationalPrefixForParsing
	String_t* ___m_nationalPrefixForParsing_6;
	// System.String PhoneMetaTerritory::m_nationalPrefixTransformRule
	String_t* ___m_nationalPrefixTransformRule_7;
	// System.String PhoneMetaTerritory::m_preferredExtnPrefix
	String_t* ___m_preferredExtnPrefix_8;
	// System.String PhoneMetaTerritory::m_nationalPrefixFormattingRule
	String_t* ___m_nationalPrefixFormattingRule_9;
	// System.String PhoneMetaTerritory::m_carrierCodeFormattingRule
	String_t* ___m_carrierCodeFormattingRule_10;
	// System.Boolean PhoneMetaTerritory::m_mainCountryForCode
	bool ___m_mainCountryForCode_11;
	// System.Boolean PhoneMetaTerritory::m_nationalPrefixOptionalWhenFormatting
	bool ___m_nationalPrefixOptionalWhenFormatting_12;
	// System.Boolean PhoneMetaTerritory::m_leadingZeroPossible
	bool ___m_leadingZeroPossible_13;
	// System.Boolean PhoneMetaTerritory::m_mobileNumberPortableRegion
	bool ___m_mobileNumberPortableRegion_14;
	// System.String[] PhoneMetaTerritory::references
	StringU5BU5D_t1642385972* ___references_15;
	// PhoneMetaNumberFormat[] PhoneMetaTerritory::availableFormats
	PhoneMetaNumberFormatU5BU5D_t2158599686* ___availableFormats_16;
	// PhoneMetaNumberInfo PhoneMetaTerritory::generalDesc
	PhoneMetaNumberInfo_t3299665448 * ___generalDesc_17;
	// PhoneMetaNumberInfo PhoneMetaTerritory::noInternationalDialling
	PhoneMetaNumberInfo_t3299665448 * ___noInternationalDialling_18;
	// PhoneMetaNumberInfo PhoneMetaTerritory::areaCodeOptional
	PhoneMetaNumberInfo_t3299665448 * ___areaCodeOptional_19;
	// PhoneMetaNumberInfo PhoneMetaTerritory::fixedLine
	PhoneMetaNumberInfo_t3299665448 * ___fixedLine_20;
	// PhoneMetaNumberInfo PhoneMetaTerritory::mobile
	PhoneMetaNumberInfo_t3299665448 * ___mobile_21;
	// PhoneMetaNumberInfo PhoneMetaTerritory::pager
	PhoneMetaNumberInfo_t3299665448 * ___pager_22;
	// PhoneMetaNumberInfo PhoneMetaTerritory::tollFree
	PhoneMetaNumberInfo_t3299665448 * ___tollFree_23;
	// PhoneMetaNumberInfo PhoneMetaTerritory::premiumRate
	PhoneMetaNumberInfo_t3299665448 * ___premiumRate_24;
	// PhoneMetaNumberInfo PhoneMetaTerritory::sharedCost
	PhoneMetaNumberInfo_t3299665448 * ___sharedCost_25;
	// PhoneMetaNumberInfo PhoneMetaTerritory::personalNumber
	PhoneMetaNumberInfo_t3299665448 * ___personalNumber_26;
	// PhoneMetaNumberInfo PhoneMetaTerritory::voip
	PhoneMetaNumberInfo_t3299665448 * ___voip_27;
	// PhoneMetaNumberInfo PhoneMetaTerritory::uan
	PhoneMetaNumberInfo_t3299665448 * ___uan_28;
	// PhoneMetaNumberInfo PhoneMetaTerritory::voicemail
	PhoneMetaNumberInfo_t3299665448 * ___voicemail_29;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_id_0)); }
	inline String_t* get_m_id_0() const { return ___m_id_0; }
	inline String_t** get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(String_t* value)
	{
		___m_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_id_0, value);
	}

	inline static int32_t get_offset_of_m_countryCode_1() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_countryCode_1)); }
	inline String_t* get_m_countryCode_1() const { return ___m_countryCode_1; }
	inline String_t** get_address_of_m_countryCode_1() { return &___m_countryCode_1; }
	inline void set_m_countryCode_1(String_t* value)
	{
		___m_countryCode_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_countryCode_1, value);
	}

	inline static int32_t get_offset_of_m_leadingDigits_2() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_leadingDigits_2)); }
	inline String_t* get_m_leadingDigits_2() const { return ___m_leadingDigits_2; }
	inline String_t** get_address_of_m_leadingDigits_2() { return &___m_leadingDigits_2; }
	inline void set_m_leadingDigits_2(String_t* value)
	{
		___m_leadingDigits_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_leadingDigits_2, value);
	}

	inline static int32_t get_offset_of_m_preferredInternationalprefix_3() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_preferredInternationalprefix_3)); }
	inline String_t* get_m_preferredInternationalprefix_3() const { return ___m_preferredInternationalprefix_3; }
	inline String_t** get_address_of_m_preferredInternationalprefix_3() { return &___m_preferredInternationalprefix_3; }
	inline void set_m_preferredInternationalprefix_3(String_t* value)
	{
		___m_preferredInternationalprefix_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_preferredInternationalprefix_3, value);
	}

	inline static int32_t get_offset_of_m_internationalPrefix_4() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_internationalPrefix_4)); }
	inline String_t* get_m_internationalPrefix_4() const { return ___m_internationalPrefix_4; }
	inline String_t** get_address_of_m_internationalPrefix_4() { return &___m_internationalPrefix_4; }
	inline void set_m_internationalPrefix_4(String_t* value)
	{
		___m_internationalPrefix_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_internationalPrefix_4, value);
	}

	inline static int32_t get_offset_of_m_nationalPrefix_5() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_nationalPrefix_5)); }
	inline String_t* get_m_nationalPrefix_5() const { return ___m_nationalPrefix_5; }
	inline String_t** get_address_of_m_nationalPrefix_5() { return &___m_nationalPrefix_5; }
	inline void set_m_nationalPrefix_5(String_t* value)
	{
		___m_nationalPrefix_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_nationalPrefix_5, value);
	}

	inline static int32_t get_offset_of_m_nationalPrefixForParsing_6() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_nationalPrefixForParsing_6)); }
	inline String_t* get_m_nationalPrefixForParsing_6() const { return ___m_nationalPrefixForParsing_6; }
	inline String_t** get_address_of_m_nationalPrefixForParsing_6() { return &___m_nationalPrefixForParsing_6; }
	inline void set_m_nationalPrefixForParsing_6(String_t* value)
	{
		___m_nationalPrefixForParsing_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_nationalPrefixForParsing_6, value);
	}

	inline static int32_t get_offset_of_m_nationalPrefixTransformRule_7() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_nationalPrefixTransformRule_7)); }
	inline String_t* get_m_nationalPrefixTransformRule_7() const { return ___m_nationalPrefixTransformRule_7; }
	inline String_t** get_address_of_m_nationalPrefixTransformRule_7() { return &___m_nationalPrefixTransformRule_7; }
	inline void set_m_nationalPrefixTransformRule_7(String_t* value)
	{
		___m_nationalPrefixTransformRule_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_nationalPrefixTransformRule_7, value);
	}

	inline static int32_t get_offset_of_m_preferredExtnPrefix_8() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_preferredExtnPrefix_8)); }
	inline String_t* get_m_preferredExtnPrefix_8() const { return ___m_preferredExtnPrefix_8; }
	inline String_t** get_address_of_m_preferredExtnPrefix_8() { return &___m_preferredExtnPrefix_8; }
	inline void set_m_preferredExtnPrefix_8(String_t* value)
	{
		___m_preferredExtnPrefix_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_preferredExtnPrefix_8, value);
	}

	inline static int32_t get_offset_of_m_nationalPrefixFormattingRule_9() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_nationalPrefixFormattingRule_9)); }
	inline String_t* get_m_nationalPrefixFormattingRule_9() const { return ___m_nationalPrefixFormattingRule_9; }
	inline String_t** get_address_of_m_nationalPrefixFormattingRule_9() { return &___m_nationalPrefixFormattingRule_9; }
	inline void set_m_nationalPrefixFormattingRule_9(String_t* value)
	{
		___m_nationalPrefixFormattingRule_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_nationalPrefixFormattingRule_9, value);
	}

	inline static int32_t get_offset_of_m_carrierCodeFormattingRule_10() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_carrierCodeFormattingRule_10)); }
	inline String_t* get_m_carrierCodeFormattingRule_10() const { return ___m_carrierCodeFormattingRule_10; }
	inline String_t** get_address_of_m_carrierCodeFormattingRule_10() { return &___m_carrierCodeFormattingRule_10; }
	inline void set_m_carrierCodeFormattingRule_10(String_t* value)
	{
		___m_carrierCodeFormattingRule_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_carrierCodeFormattingRule_10, value);
	}

	inline static int32_t get_offset_of_m_mainCountryForCode_11() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_mainCountryForCode_11)); }
	inline bool get_m_mainCountryForCode_11() const { return ___m_mainCountryForCode_11; }
	inline bool* get_address_of_m_mainCountryForCode_11() { return &___m_mainCountryForCode_11; }
	inline void set_m_mainCountryForCode_11(bool value)
	{
		___m_mainCountryForCode_11 = value;
	}

	inline static int32_t get_offset_of_m_nationalPrefixOptionalWhenFormatting_12() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_nationalPrefixOptionalWhenFormatting_12)); }
	inline bool get_m_nationalPrefixOptionalWhenFormatting_12() const { return ___m_nationalPrefixOptionalWhenFormatting_12; }
	inline bool* get_address_of_m_nationalPrefixOptionalWhenFormatting_12() { return &___m_nationalPrefixOptionalWhenFormatting_12; }
	inline void set_m_nationalPrefixOptionalWhenFormatting_12(bool value)
	{
		___m_nationalPrefixOptionalWhenFormatting_12 = value;
	}

	inline static int32_t get_offset_of_m_leadingZeroPossible_13() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_leadingZeroPossible_13)); }
	inline bool get_m_leadingZeroPossible_13() const { return ___m_leadingZeroPossible_13; }
	inline bool* get_address_of_m_leadingZeroPossible_13() { return &___m_leadingZeroPossible_13; }
	inline void set_m_leadingZeroPossible_13(bool value)
	{
		___m_leadingZeroPossible_13 = value;
	}

	inline static int32_t get_offset_of_m_mobileNumberPortableRegion_14() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___m_mobileNumberPortableRegion_14)); }
	inline bool get_m_mobileNumberPortableRegion_14() const { return ___m_mobileNumberPortableRegion_14; }
	inline bool* get_address_of_m_mobileNumberPortableRegion_14() { return &___m_mobileNumberPortableRegion_14; }
	inline void set_m_mobileNumberPortableRegion_14(bool value)
	{
		___m_mobileNumberPortableRegion_14 = value;
	}

	inline static int32_t get_offset_of_references_15() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___references_15)); }
	inline StringU5BU5D_t1642385972* get_references_15() const { return ___references_15; }
	inline StringU5BU5D_t1642385972** get_address_of_references_15() { return &___references_15; }
	inline void set_references_15(StringU5BU5D_t1642385972* value)
	{
		___references_15 = value;
		Il2CppCodeGenWriteBarrier(&___references_15, value);
	}

	inline static int32_t get_offset_of_availableFormats_16() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___availableFormats_16)); }
	inline PhoneMetaNumberFormatU5BU5D_t2158599686* get_availableFormats_16() const { return ___availableFormats_16; }
	inline PhoneMetaNumberFormatU5BU5D_t2158599686** get_address_of_availableFormats_16() { return &___availableFormats_16; }
	inline void set_availableFormats_16(PhoneMetaNumberFormatU5BU5D_t2158599686* value)
	{
		___availableFormats_16 = value;
		Il2CppCodeGenWriteBarrier(&___availableFormats_16, value);
	}

	inline static int32_t get_offset_of_generalDesc_17() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___generalDesc_17)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_generalDesc_17() const { return ___generalDesc_17; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_generalDesc_17() { return &___generalDesc_17; }
	inline void set_generalDesc_17(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___generalDesc_17 = value;
		Il2CppCodeGenWriteBarrier(&___generalDesc_17, value);
	}

	inline static int32_t get_offset_of_noInternationalDialling_18() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___noInternationalDialling_18)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_noInternationalDialling_18() const { return ___noInternationalDialling_18; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_noInternationalDialling_18() { return &___noInternationalDialling_18; }
	inline void set_noInternationalDialling_18(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___noInternationalDialling_18 = value;
		Il2CppCodeGenWriteBarrier(&___noInternationalDialling_18, value);
	}

	inline static int32_t get_offset_of_areaCodeOptional_19() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___areaCodeOptional_19)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_areaCodeOptional_19() const { return ___areaCodeOptional_19; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_areaCodeOptional_19() { return &___areaCodeOptional_19; }
	inline void set_areaCodeOptional_19(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___areaCodeOptional_19 = value;
		Il2CppCodeGenWriteBarrier(&___areaCodeOptional_19, value);
	}

	inline static int32_t get_offset_of_fixedLine_20() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___fixedLine_20)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_fixedLine_20() const { return ___fixedLine_20; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_fixedLine_20() { return &___fixedLine_20; }
	inline void set_fixedLine_20(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___fixedLine_20 = value;
		Il2CppCodeGenWriteBarrier(&___fixedLine_20, value);
	}

	inline static int32_t get_offset_of_mobile_21() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___mobile_21)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_mobile_21() const { return ___mobile_21; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_mobile_21() { return &___mobile_21; }
	inline void set_mobile_21(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___mobile_21 = value;
		Il2CppCodeGenWriteBarrier(&___mobile_21, value);
	}

	inline static int32_t get_offset_of_pager_22() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___pager_22)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_pager_22() const { return ___pager_22; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_pager_22() { return &___pager_22; }
	inline void set_pager_22(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___pager_22 = value;
		Il2CppCodeGenWriteBarrier(&___pager_22, value);
	}

	inline static int32_t get_offset_of_tollFree_23() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___tollFree_23)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_tollFree_23() const { return ___tollFree_23; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_tollFree_23() { return &___tollFree_23; }
	inline void set_tollFree_23(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___tollFree_23 = value;
		Il2CppCodeGenWriteBarrier(&___tollFree_23, value);
	}

	inline static int32_t get_offset_of_premiumRate_24() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___premiumRate_24)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_premiumRate_24() const { return ___premiumRate_24; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_premiumRate_24() { return &___premiumRate_24; }
	inline void set_premiumRate_24(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___premiumRate_24 = value;
		Il2CppCodeGenWriteBarrier(&___premiumRate_24, value);
	}

	inline static int32_t get_offset_of_sharedCost_25() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___sharedCost_25)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_sharedCost_25() const { return ___sharedCost_25; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_sharedCost_25() { return &___sharedCost_25; }
	inline void set_sharedCost_25(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___sharedCost_25 = value;
		Il2CppCodeGenWriteBarrier(&___sharedCost_25, value);
	}

	inline static int32_t get_offset_of_personalNumber_26() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___personalNumber_26)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_personalNumber_26() const { return ___personalNumber_26; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_personalNumber_26() { return &___personalNumber_26; }
	inline void set_personalNumber_26(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___personalNumber_26 = value;
		Il2CppCodeGenWriteBarrier(&___personalNumber_26, value);
	}

	inline static int32_t get_offset_of_voip_27() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___voip_27)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_voip_27() const { return ___voip_27; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_voip_27() { return &___voip_27; }
	inline void set_voip_27(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___voip_27 = value;
		Il2CppCodeGenWriteBarrier(&___voip_27, value);
	}

	inline static int32_t get_offset_of_uan_28() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___uan_28)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_uan_28() const { return ___uan_28; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_uan_28() { return &___uan_28; }
	inline void set_uan_28(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___uan_28 = value;
		Il2CppCodeGenWriteBarrier(&___uan_28, value);
	}

	inline static int32_t get_offset_of_voicemail_29() { return static_cast<int32_t>(offsetof(PhoneMetaTerritory_t2073820963, ___voicemail_29)); }
	inline PhoneMetaNumberInfo_t3299665448 * get_voicemail_29() const { return ___voicemail_29; }
	inline PhoneMetaNumberInfo_t3299665448 ** get_address_of_voicemail_29() { return &___voicemail_29; }
	inline void set_voicemail_29(PhoneMetaNumberInfo_t3299665448 * value)
	{
		___voicemail_29 = value;
		Il2CppCodeGenWriteBarrier(&___voicemail_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
