﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1229414204.h"
#include "AssemblyU2DCSharp_CameraRequestLocation942914719.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceCameraManager
struct  DeviceCameraManager_t1478748484  : public MonoSingleton_1_t1229414204
{
public:
	// UnityEngine.GameObject DeviceCameraManager::panel
	GameObject_t1756533147 * ___panel_3;
	// UnityEngine.GameObject DeviceCameraManager::background
	GameObject_t1756533147 * ___background_4;
	// UnityEngine.GameObject DeviceCameraManager::UI
	GameObject_t1756533147 * ___UI_5;
	// UISprite DeviceCameraManager::dividerSprite
	UISprite_t603616735 * ___dividerSprite_6;
	// UISprite DeviceCameraManager::tpSprite
	UISprite_t603616735 * ___tpSprite_7;
	// UISprite DeviceCameraManager::ceSprite
	UISprite_t603616735 * ___ceSprite_8;
	// UISprite DeviceCameraManager::cancelSprite
	UISprite_t603616735 * ___cancelSprite_9;
	// CameraRequestLocation DeviceCameraManager::currentLocation
	int32_t ___currentLocation_10;
	// System.Boolean DeviceCameraManager::cameraUIClosing
	bool ___cameraUIClosing_11;

public:
	inline static int32_t get_offset_of_panel_3() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___panel_3)); }
	inline GameObject_t1756533147 * get_panel_3() const { return ___panel_3; }
	inline GameObject_t1756533147 ** get_address_of_panel_3() { return &___panel_3; }
	inline void set_panel_3(GameObject_t1756533147 * value)
	{
		___panel_3 = value;
		Il2CppCodeGenWriteBarrier(&___panel_3, value);
	}

	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___background_4)); }
	inline GameObject_t1756533147 * get_background_4() const { return ___background_4; }
	inline GameObject_t1756533147 ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(GameObject_t1756533147 * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier(&___background_4, value);
	}

	inline static int32_t get_offset_of_UI_5() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___UI_5)); }
	inline GameObject_t1756533147 * get_UI_5() const { return ___UI_5; }
	inline GameObject_t1756533147 ** get_address_of_UI_5() { return &___UI_5; }
	inline void set_UI_5(GameObject_t1756533147 * value)
	{
		___UI_5 = value;
		Il2CppCodeGenWriteBarrier(&___UI_5, value);
	}

	inline static int32_t get_offset_of_dividerSprite_6() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___dividerSprite_6)); }
	inline UISprite_t603616735 * get_dividerSprite_6() const { return ___dividerSprite_6; }
	inline UISprite_t603616735 ** get_address_of_dividerSprite_6() { return &___dividerSprite_6; }
	inline void set_dividerSprite_6(UISprite_t603616735 * value)
	{
		___dividerSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___dividerSprite_6, value);
	}

	inline static int32_t get_offset_of_tpSprite_7() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___tpSprite_7)); }
	inline UISprite_t603616735 * get_tpSprite_7() const { return ___tpSprite_7; }
	inline UISprite_t603616735 ** get_address_of_tpSprite_7() { return &___tpSprite_7; }
	inline void set_tpSprite_7(UISprite_t603616735 * value)
	{
		___tpSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___tpSprite_7, value);
	}

	inline static int32_t get_offset_of_ceSprite_8() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___ceSprite_8)); }
	inline UISprite_t603616735 * get_ceSprite_8() const { return ___ceSprite_8; }
	inline UISprite_t603616735 ** get_address_of_ceSprite_8() { return &___ceSprite_8; }
	inline void set_ceSprite_8(UISprite_t603616735 * value)
	{
		___ceSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___ceSprite_8, value);
	}

	inline static int32_t get_offset_of_cancelSprite_9() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___cancelSprite_9)); }
	inline UISprite_t603616735 * get_cancelSprite_9() const { return ___cancelSprite_9; }
	inline UISprite_t603616735 ** get_address_of_cancelSprite_9() { return &___cancelSprite_9; }
	inline void set_cancelSprite_9(UISprite_t603616735 * value)
	{
		___cancelSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___cancelSprite_9, value);
	}

	inline static int32_t get_offset_of_currentLocation_10() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___currentLocation_10)); }
	inline int32_t get_currentLocation_10() const { return ___currentLocation_10; }
	inline int32_t* get_address_of_currentLocation_10() { return &___currentLocation_10; }
	inline void set_currentLocation_10(int32_t value)
	{
		___currentLocation_10 = value;
	}

	inline static int32_t get_offset_of_cameraUIClosing_11() { return static_cast<int32_t>(offsetof(DeviceCameraManager_t1478748484, ___cameraUIClosing_11)); }
	inline bool get_cameraUIClosing_11() const { return ___cameraUIClosing_11; }
	inline bool* get_address_of_cameraUIClosing_11() { return &___cameraUIClosing_11; }
	inline void set_cameraUIClosing_11(bool value)
	{
		___cameraUIClosing_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
