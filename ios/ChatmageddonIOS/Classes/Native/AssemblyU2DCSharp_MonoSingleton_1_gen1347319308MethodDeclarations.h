﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<PlayerManager>::.ctor()
#define MonoSingleton_1__ctor_m2526181214(__this, method) ((  void (*) (MonoSingleton_1_t1347319308 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<PlayerManager>::Awake()
#define MonoSingleton_1_Awake_m3154870553(__this, method) ((  void (*) (MonoSingleton_1_t1347319308 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<PlayerManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3629440119(__this /* static, unused */, method) ((  PlayerManager_t1596653588 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<PlayerManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m967196015(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<PlayerManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3486017656(__this, method) ((  void (*) (MonoSingleton_1_t1347319308 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<PlayerManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m4163580220(__this, method) ((  void (*) (MonoSingleton_1_t1347319308 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<PlayerManager>::.cctor()
#define MonoSingleton_1__cctor_m4019461279(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
