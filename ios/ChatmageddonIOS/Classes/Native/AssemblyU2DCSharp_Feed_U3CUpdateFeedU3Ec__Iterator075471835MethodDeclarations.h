﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Feed/<UpdateFeed>c__Iterator0
struct U3CUpdateFeedU3Ec__Iterator0_t75471835;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Feed/<UpdateFeed>c__Iterator0::.ctor()
extern "C"  void U3CUpdateFeedU3Ec__Iterator0__ctor_m1706774722 (U3CUpdateFeedU3Ec__Iterator0_t75471835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Feed/<UpdateFeed>c__Iterator0::MoveNext()
extern "C"  bool U3CUpdateFeedU3Ec__Iterator0_MoveNext_m12027770 (U3CUpdateFeedU3Ec__Iterator0_t75471835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Feed/<UpdateFeed>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateFeedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m18827820 (U3CUpdateFeedU3Ec__Iterator0_t75471835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Feed/<UpdateFeed>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateFeedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1905983636 (U3CUpdateFeedU3Ec__Iterator0_t75471835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed/<UpdateFeed>c__Iterator0::Dispose()
extern "C"  void U3CUpdateFeedU3Ec__Iterator0_Dispose_m2026296605 (U3CUpdateFeedU3Ec__Iterator0_t75471835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Feed/<UpdateFeed>c__Iterator0::Reset()
extern "C"  void U3CUpdateFeedU3Ec__Iterator0_Reset_m3083968895 (U3CUpdateFeedU3Ec__Iterator0_t75471835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
