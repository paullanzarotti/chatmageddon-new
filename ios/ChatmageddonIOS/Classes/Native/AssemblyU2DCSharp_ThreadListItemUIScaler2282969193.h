﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThreadListItemUIScaler
struct  ThreadListItemUIScaler_t2282969193  : public ChatmageddonUIScaler_t3374094653
{
public:
	// TweenPosition ThreadListItemUIScaler::itemTween
	TweenPosition_t1144714832 * ___itemTween_14;
	// UISprite ThreadListItemUIScaler::background
	UISprite_t603616735 * ___background_15;
	// UnityEngine.BoxCollider ThreadListItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_16;
	// UnityEngine.Transform ThreadListItemUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_17;
	// UILabel ThreadListItemUIScaler::contactsLabel
	UILabel_t1795115428 * ___contactsLabel_18;
	// UILabel ThreadListItemUIScaler::dateLabel
	UILabel_t1795115428 * ___dateLabel_19;
	// UILabel ThreadListItemUIScaler::lastMessageLabel
	UILabel_t1795115428 * ___lastMessageLabel_20;
	// UILabel ThreadListItemUIScaler::rankLabel
	UILabel_t1795115428 * ___rankLabel_21;
	// UnityEngine.Transform ThreadListItemUIScaler::newMessageObject
	Transform_t3275118058 * ___newMessageObject_22;
	// UnityEngine.Transform ThreadListItemUIScaler::deleteObject
	Transform_t3275118058 * ___deleteObject_23;

public:
	inline static int32_t get_offset_of_itemTween_14() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___itemTween_14)); }
	inline TweenPosition_t1144714832 * get_itemTween_14() const { return ___itemTween_14; }
	inline TweenPosition_t1144714832 ** get_address_of_itemTween_14() { return &___itemTween_14; }
	inline void set_itemTween_14(TweenPosition_t1144714832 * value)
	{
		___itemTween_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemTween_14, value);
	}

	inline static int32_t get_offset_of_background_15() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___background_15)); }
	inline UISprite_t603616735 * get_background_15() const { return ___background_15; }
	inline UISprite_t603616735 ** get_address_of_background_15() { return &___background_15; }
	inline void set_background_15(UISprite_t603616735 * value)
	{
		___background_15 = value;
		Il2CppCodeGenWriteBarrier(&___background_15, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_16() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___backgroundCollider_16)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_16() const { return ___backgroundCollider_16; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_16() { return &___backgroundCollider_16; }
	inline void set_backgroundCollider_16(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_16 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_16, value);
	}

	inline static int32_t get_offset_of_profilePicture_17() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___profilePicture_17)); }
	inline Transform_t3275118058 * get_profilePicture_17() const { return ___profilePicture_17; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_17() { return &___profilePicture_17; }
	inline void set_profilePicture_17(Transform_t3275118058 * value)
	{
		___profilePicture_17 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_17, value);
	}

	inline static int32_t get_offset_of_contactsLabel_18() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___contactsLabel_18)); }
	inline UILabel_t1795115428 * get_contactsLabel_18() const { return ___contactsLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_contactsLabel_18() { return &___contactsLabel_18; }
	inline void set_contactsLabel_18(UILabel_t1795115428 * value)
	{
		___contactsLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___contactsLabel_18, value);
	}

	inline static int32_t get_offset_of_dateLabel_19() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___dateLabel_19)); }
	inline UILabel_t1795115428 * get_dateLabel_19() const { return ___dateLabel_19; }
	inline UILabel_t1795115428 ** get_address_of_dateLabel_19() { return &___dateLabel_19; }
	inline void set_dateLabel_19(UILabel_t1795115428 * value)
	{
		___dateLabel_19 = value;
		Il2CppCodeGenWriteBarrier(&___dateLabel_19, value);
	}

	inline static int32_t get_offset_of_lastMessageLabel_20() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___lastMessageLabel_20)); }
	inline UILabel_t1795115428 * get_lastMessageLabel_20() const { return ___lastMessageLabel_20; }
	inline UILabel_t1795115428 ** get_address_of_lastMessageLabel_20() { return &___lastMessageLabel_20; }
	inline void set_lastMessageLabel_20(UILabel_t1795115428 * value)
	{
		___lastMessageLabel_20 = value;
		Il2CppCodeGenWriteBarrier(&___lastMessageLabel_20, value);
	}

	inline static int32_t get_offset_of_rankLabel_21() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___rankLabel_21)); }
	inline UILabel_t1795115428 * get_rankLabel_21() const { return ___rankLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_21() { return &___rankLabel_21; }
	inline void set_rankLabel_21(UILabel_t1795115428 * value)
	{
		___rankLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_21, value);
	}

	inline static int32_t get_offset_of_newMessageObject_22() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___newMessageObject_22)); }
	inline Transform_t3275118058 * get_newMessageObject_22() const { return ___newMessageObject_22; }
	inline Transform_t3275118058 ** get_address_of_newMessageObject_22() { return &___newMessageObject_22; }
	inline void set_newMessageObject_22(Transform_t3275118058 * value)
	{
		___newMessageObject_22 = value;
		Il2CppCodeGenWriteBarrier(&___newMessageObject_22, value);
	}

	inline static int32_t get_offset_of_deleteObject_23() { return static_cast<int32_t>(offsetof(ThreadListItemUIScaler_t2282969193, ___deleteObject_23)); }
	inline Transform_t3275118058 * get_deleteObject_23() const { return ___deleteObject_23; }
	inline Transform_t3275118058 ** get_address_of_deleteObject_23() { return &___deleteObject_23; }
	inline void set_deleteObject_23(Transform_t3275118058 * value)
	{
		___deleteObject_23 = value;
		Il2CppCodeGenWriteBarrier(&___deleteObject_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
