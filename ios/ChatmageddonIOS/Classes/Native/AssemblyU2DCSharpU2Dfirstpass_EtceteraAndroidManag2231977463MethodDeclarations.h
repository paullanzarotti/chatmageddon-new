﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraAndroidManager
struct EtceteraAndroidManager_t2231977463;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraAndroidManager::.ctor()
extern "C"  void EtceteraAndroidManager__ctor_m4055738014 (EtceteraAndroidManager_t2231977463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
