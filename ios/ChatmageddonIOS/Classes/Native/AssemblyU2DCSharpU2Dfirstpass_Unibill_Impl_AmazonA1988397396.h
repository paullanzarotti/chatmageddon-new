﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IRawAmazonAppStoreBillingInterface
struct IRawAmazonAppStoreBillingInterface_t2991800847;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// TransactionDatabase
struct TransactionDatabase_t201476183;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.AmazonAppStoreBillingService
struct  AmazonAppStoreBillingService_t1988397396  : public Il2CppObject
{
public:
	// Unibill.Impl.IBillingServiceCallback Unibill.Impl.AmazonAppStoreBillingService::callback
	Il2CppObject * ___callback_0;
	// Unibill.Impl.ProductIdRemapper Unibill.Impl.AmazonAppStoreBillingService::remapper
	ProductIdRemapper_t3313438456 * ___remapper_1;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.AmazonAppStoreBillingService::db
	UnibillConfiguration_t2915611853 * ___db_2;
	// Uniject.ILogger Unibill.Impl.AmazonAppStoreBillingService::logger
	Il2CppObject * ___logger_3;
	// Unibill.Impl.IRawAmazonAppStoreBillingInterface Unibill.Impl.AmazonAppStoreBillingService::amazon
	Il2CppObject * ___amazon_4;
	// System.Collections.Generic.HashSet`1<System.String> Unibill.Impl.AmazonAppStoreBillingService::unknownAmazonProducts
	HashSet_1_t362681087 * ___unknownAmazonProducts_5;
	// TransactionDatabase Unibill.Impl.AmazonAppStoreBillingService::tDb
	TransactionDatabase_t201476183 * ___tDb_6;
	// System.Boolean Unibill.Impl.AmazonAppStoreBillingService::finishedSetup
	bool ___finishedSetup_7;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___callback_0)); }
	inline Il2CppObject * get_callback_0() const { return ___callback_0; }
	inline Il2CppObject ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Il2CppObject * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_remapper_1() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___remapper_1)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_1() const { return ___remapper_1; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_1() { return &___remapper_1; }
	inline void set_remapper_1(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_1 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_1, value);
	}

	inline static int32_t get_offset_of_db_2() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___db_2)); }
	inline UnibillConfiguration_t2915611853 * get_db_2() const { return ___db_2; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_db_2() { return &___db_2; }
	inline void set_db_2(UnibillConfiguration_t2915611853 * value)
	{
		___db_2 = value;
		Il2CppCodeGenWriteBarrier(&___db_2, value);
	}

	inline static int32_t get_offset_of_logger_3() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___logger_3)); }
	inline Il2CppObject * get_logger_3() const { return ___logger_3; }
	inline Il2CppObject ** get_address_of_logger_3() { return &___logger_3; }
	inline void set_logger_3(Il2CppObject * value)
	{
		___logger_3 = value;
		Il2CppCodeGenWriteBarrier(&___logger_3, value);
	}

	inline static int32_t get_offset_of_amazon_4() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___amazon_4)); }
	inline Il2CppObject * get_amazon_4() const { return ___amazon_4; }
	inline Il2CppObject ** get_address_of_amazon_4() { return &___amazon_4; }
	inline void set_amazon_4(Il2CppObject * value)
	{
		___amazon_4 = value;
		Il2CppCodeGenWriteBarrier(&___amazon_4, value);
	}

	inline static int32_t get_offset_of_unknownAmazonProducts_5() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___unknownAmazonProducts_5)); }
	inline HashSet_1_t362681087 * get_unknownAmazonProducts_5() const { return ___unknownAmazonProducts_5; }
	inline HashSet_1_t362681087 ** get_address_of_unknownAmazonProducts_5() { return &___unknownAmazonProducts_5; }
	inline void set_unknownAmazonProducts_5(HashSet_1_t362681087 * value)
	{
		___unknownAmazonProducts_5 = value;
		Il2CppCodeGenWriteBarrier(&___unknownAmazonProducts_5, value);
	}

	inline static int32_t get_offset_of_tDb_6() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___tDb_6)); }
	inline TransactionDatabase_t201476183 * get_tDb_6() const { return ___tDb_6; }
	inline TransactionDatabase_t201476183 ** get_address_of_tDb_6() { return &___tDb_6; }
	inline void set_tDb_6(TransactionDatabase_t201476183 * value)
	{
		___tDb_6 = value;
		Il2CppCodeGenWriteBarrier(&___tDb_6, value);
	}

	inline static int32_t get_offset_of_finishedSetup_7() { return static_cast<int32_t>(offsetof(AmazonAppStoreBillingService_t1988397396, ___finishedSetup_7)); }
	inline bool get_finishedSetup_7() const { return ___finishedSetup_7; }
	inline bool* get_address_of_finishedSetup_7() { return &___finishedSetup_7; }
	inline void set_finishedSetup_7(bool value)
	{
		___finishedSetup_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
