﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<LeaderboardNavScreen>
struct List_1_t1725164844;
// System.Collections.Generic.IEnumerable`1<LeaderboardNavScreen>
struct IEnumerable_1_t2648170757;
// LeaderboardNavScreen[]
struct LeaderboardNavScreenU5BU5D_t2088986945;
// System.Collections.Generic.IEnumerator`1<LeaderboardNavScreen>
struct IEnumerator_1_t4126534835;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<LeaderboardNavScreen>
struct ICollection_1_t3308119017;
// System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>
struct ReadOnlyCollection_1_t2541829404;
// System.Predicate`1<LeaderboardNavScreen>
struct Predicate_1_t799013827;
// System.Action`1<LeaderboardNavScreen>
struct Action_1_t2157843094;
// System.Collections.Generic.IComparer`1<LeaderboardNavScreen>
struct IComparer_1_t310506834;
// System.Comparison`1<LeaderboardNavScreen>
struct Comparison_1_t3617782563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1259894518.h"

// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::.ctor()
extern "C"  void List_1__ctor_m3625405018_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1__ctor_m3625405018(__this, method) ((  void (*) (List_1_t1725164844 *, const MethodInfo*))List_1__ctor_m3625405018_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3376046721_gshared (List_1_t1725164844 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3376046721(__this, ___collection0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3376046721_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m864473815_gshared (List_1_t1725164844 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m864473815(__this, ___capacity0, method) ((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1__ctor_m864473815_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m837250053_gshared (List_1_t1725164844 * __this, LeaderboardNavScreenU5BU5D_t2088986945* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m837250053(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1725164844 *, LeaderboardNavScreenU5BU5D_t2088986945*, int32_t, const MethodInfo*))List_1__ctor_m837250053_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::.cctor()
extern "C"  void List_1__cctor_m432538793_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m432538793(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m432538793_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2298313540_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2298313540(__this, method) ((  Il2CppObject* (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2298313540_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m536259926_gshared (List_1_t1725164844 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m536259926(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1725164844 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m536259926_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3414708835_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3414708835(__this, method) ((  Il2CppObject * (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3414708835_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2517186658_gshared (List_1_t1725164844 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2517186658(__this, ___item0, method) ((  int32_t (*) (List_1_t1725164844 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2517186658_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2576416292_gshared (List_1_t1725164844 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2576416292(__this, ___item0, method) ((  bool (*) (List_1_t1725164844 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2576416292_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3941564652_gshared (List_1_t1725164844 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3941564652(__this, ___item0, method) ((  int32_t (*) (List_1_t1725164844 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3941564652_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m866990995_gshared (List_1_t1725164844 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m866990995(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1725164844 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m866990995_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m4269400267_gshared (List_1_t1725164844 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m4269400267(__this, ___item0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m4269400267_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3500715119_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3500715119(__this, method) ((  bool (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3500715119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1993692170_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1993692170(__this, method) ((  bool (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1993692170_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3308162988_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3308162988(__this, method) ((  Il2CppObject * (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3308162988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m710868107_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m710868107(__this, method) ((  bool (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m710868107_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3812229278_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3812229278(__this, method) ((  bool (*) (List_1_t1725164844 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3812229278_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4235432735_gshared (List_1_t1725164844 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4235432735(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4235432735_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3802601068_gshared (List_1_t1725164844 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3802601068(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1725164844 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3802601068_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Add(T)
extern "C"  void List_1_Add_m3849567279_gshared (List_1_t1725164844 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3849567279(__this, ___item0, method) ((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_Add_m3849567279_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2943378924_gshared (List_1_t1725164844 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2943378924(__this, ___newCount0, method) ((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2943378924_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2443868759_gshared (List_1_t1725164844 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2443868759(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1725164844 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2443868759_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2165463820_gshared (List_1_t1725164844 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2165463820(__this, ___collection0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2165463820_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m914510812_gshared (List_1_t1725164844 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m914510812(__this, ___enumerable0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m914510812_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2969348943_gshared (List_1_t1725164844 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2969348943(__this, ___collection0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2969348943_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<LeaderboardNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2541829404 * List_1_AsReadOnly_m3270371398_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3270371398(__this, method) ((  ReadOnlyCollection_1_t2541829404 * (*) (List_1_t1725164844 *, const MethodInfo*))List_1_AsReadOnly_m3270371398_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Clear()
extern "C"  void List_1_Clear_m1770158979_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_Clear_m1770158979(__this, method) ((  void (*) (List_1_t1725164844 *, const MethodInfo*))List_1_Clear_m1770158979_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m1265769825_gshared (List_1_t1725164844 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m1265769825(__this, ___item0, method) ((  bool (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_Contains_m1265769825_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m826508698_gshared (List_1_t1725164844 * __this, LeaderboardNavScreenU5BU5D_t2088986945* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m826508698(__this, ___array0, method) ((  void (*) (List_1_t1725164844 *, LeaderboardNavScreenU5BU5D_t2088986945*, const MethodInfo*))List_1_CopyTo_m826508698_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2375594411_gshared (List_1_t1725164844 * __this, LeaderboardNavScreenU5BU5D_t2088986945* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2375594411(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1725164844 *, LeaderboardNavScreenU5BU5D_t2088986945*, int32_t, const MethodInfo*))List_1_CopyTo_m2375594411_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m2723579487_gshared (List_1_t1725164844 * __this, int32_t ___index0, LeaderboardNavScreenU5BU5D_t2088986945* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m2723579487(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t1725164844 *, int32_t, LeaderboardNavScreenU5BU5D_t2088986945*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m2723579487_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m205892487_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_Exists_m205892487(__this, ___match0, method) ((  bool (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_Exists_m205892487_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<LeaderboardNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1515678065_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_Find_m1515678065(__this, ___match0, method) ((  int32_t (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_Find_m1515678065_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1814105710_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1814105710(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t799013827 *, const MethodInfo*))List_1_CheckMatch_m1814105710_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<LeaderboardNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t1725164844 * List_1_FindAll_m2244573738_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2244573738(__this, ___match0, method) ((  List_1_t1725164844 * (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_FindAll_m2244573738_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<LeaderboardNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t1725164844 * List_1_FindAllStackBits_m664242634_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m664242634(__this, ___match0, method) ((  List_1_t1725164844 * (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_FindAllStackBits_m664242634_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<LeaderboardNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t1725164844 * List_1_FindAllList_m443732378_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m443732378(__this, ___match0, method) ((  List_1_t1725164844 * (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_FindAllList_m443732378_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m4228752292_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m4228752292(__this, ___match0, method) ((  int32_t (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_FindIndex_m4228752292_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3838853793_gshared (List_1_t1725164844 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t799013827 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3838853793(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1725164844 *, int32_t, int32_t, Predicate_1_t799013827 *, const MethodInfo*))List_1_GetIndex_m3838853793_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1296447318_gshared (List_1_t1725164844 * __this, Action_1_t2157843094 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1296447318(__this, ___action0, method) ((  void (*) (List_1_t1725164844 *, Action_1_t2157843094 *, const MethodInfo*))List_1_ForEach_m1296447318_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<LeaderboardNavScreen>::GetEnumerator()
extern "C"  Enumerator_t1259894518  List_1_GetEnumerator_m1565069546_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1565069546(__this, method) ((  Enumerator_t1259894518  (*) (List_1_t1725164844 *, const MethodInfo*))List_1_GetEnumerator_m1565069546_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4182585535_gshared (List_1_t1725164844 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4182585535(__this, ___item0, method) ((  int32_t (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_IndexOf_m4182585535_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3487076794_gshared (List_1_t1725164844 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3487076794(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1725164844 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3487076794_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1213839147_gshared (List_1_t1725164844 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1213839147(__this, ___index0, method) ((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1213839147_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m533005368_gshared (List_1_t1725164844 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m533005368(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1725164844 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m533005368_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m998535017_gshared (List_1_t1725164844 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m998535017(__this, ___collection0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m998535017_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<LeaderboardNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m384447366_gshared (List_1_t1725164844 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m384447366(__this, ___item0, method) ((  bool (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_Remove_m384447366_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1798264434_gshared (List_1_t1725164844 * __this, Predicate_1_t799013827 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1798264434(__this, ___match0, method) ((  int32_t (*) (List_1_t1725164844 *, Predicate_1_t799013827 *, const MethodInfo*))List_1_RemoveAll_m1798264434_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m72859852_gshared (List_1_t1725164844 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m72859852(__this, ___index0, method) ((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_RemoveAt_m72859852_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m264077527_gshared (List_1_t1725164844 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m264077527(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1725164844 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m264077527_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m405633412_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_Reverse_m405633412(__this, method) ((  void (*) (List_1_t1725164844 *, const MethodInfo*))List_1_Reverse_m405633412_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Sort()
extern "C"  void List_1_Sort_m1501451128_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_Sort_m1501451128(__this, method) ((  void (*) (List_1_t1725164844 *, const MethodInfo*))List_1_Sort_m1501451128_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2989585964_gshared (List_1_t1725164844 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2989585964(__this, ___comparer0, method) ((  void (*) (List_1_t1725164844 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2989585964_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3633983537_gshared (List_1_t1725164844 * __this, Comparison_1_t3617782563 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3633983537(__this, ___comparison0, method) ((  void (*) (List_1_t1725164844 *, Comparison_1_t3617782563 *, const MethodInfo*))List_1_Sort_m3633983537_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<LeaderboardNavScreen>::ToArray()
extern "C"  LeaderboardNavScreenU5BU5D_t2088986945* List_1_ToArray_m959097141_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_ToArray_m959097141(__this, method) ((  LeaderboardNavScreenU5BU5D_t2088986945* (*) (List_1_t1725164844 *, const MethodInfo*))List_1_ToArray_m959097141_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m376607727_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m376607727(__this, method) ((  void (*) (List_1_t1725164844 *, const MethodInfo*))List_1_TrimExcess_m376607727_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m195326589_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m195326589(__this, method) ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))List_1_get_Capacity_m195326589_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3286449708_gshared (List_1_t1725164844 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3286449708(__this, ___value0, method) ((  void (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3286449708_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<LeaderboardNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m710550546_gshared (List_1_t1725164844 * __this, const MethodInfo* method);
#define List_1_get_Count_m710550546(__this, method) ((  int32_t (*) (List_1_t1725164844 *, const MethodInfo*))List_1_get_Count_m710550546_gshared)(__this, method)
// T System.Collections.Generic.List`1<LeaderboardNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1438584088_gshared (List_1_t1725164844 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1438584088(__this, ___index0, method) ((  int32_t (*) (List_1_t1725164844 *, int32_t, const MethodInfo*))List_1_get_Item_m1438584088_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<LeaderboardNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1688213599_gshared (List_1_t1725164844 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1688213599(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1725164844 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1688213599_gshared)(__this, ___index0, ___value1, method)
