﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Hypno
struct CameraFilterPack_FX_Hypno_t1332127830;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Hypno::.ctor()
extern "C"  void CameraFilterPack_FX_Hypno__ctor_m3535037501 (CameraFilterPack_FX_Hypno_t1332127830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Hypno::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Hypno_get_material_m4066773626 (CameraFilterPack_FX_Hypno_t1332127830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::Start()
extern "C"  void CameraFilterPack_FX_Hypno_Start_m3612210077 (CameraFilterPack_FX_Hypno_t1332127830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Hypno_OnRenderImage_m2981853901 (CameraFilterPack_FX_Hypno_t1332127830 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::OnValidate()
extern "C"  void CameraFilterPack_FX_Hypno_OnValidate_m3370386178 (CameraFilterPack_FX_Hypno_t1332127830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::Update()
extern "C"  void CameraFilterPack_FX_Hypno_Update_m3117316984 (CameraFilterPack_FX_Hypno_t1332127830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::OnDisable()
extern "C"  void CameraFilterPack_FX_Hypno_OnDisable_m603105262 (CameraFilterPack_FX_Hypno_t1332127830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
