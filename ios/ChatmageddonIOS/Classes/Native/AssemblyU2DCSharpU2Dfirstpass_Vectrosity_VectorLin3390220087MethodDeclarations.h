﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vectrosity.VectorLine
struct VectorLine_t3390220087;
// System.String
struct String_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t851691520;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t3873494194;
// UnityEngine.Canvas
struct Canvas_t209405766;
// LineManager
struct LineManager_t3088676951;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Vectrosity.VectorLine[]
struct VectorLineU5BU5D_t4219197966;
// System.Collections.Generic.List`1<Vectrosity.VectorLine>
struct List_1_t2759341219;
// Vectrosity.VectorPoints
struct VectorPoints_t440342714;
// Vectrosity.VectorPoints[]
struct VectorPointsU5BU5D_t3745766175;
// System.Collections.Generic.List`1<Vectrosity.VectorPoints>
struct List_1_t4104431142;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_LineType3958123504.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Joins3488019013.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D851691520.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin1052426507.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorPoin440342714.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_EndCap4262812865.h"

// System.Void Vectrosity.VectorLine::.ctor(System.String,UnityEngine.Vector3[],UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m1727006306 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Vector3U5BU5D_t1172311765* ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m520855962 (VectorLine_t3390220087 * __this, String_t* ___lineName0, List_1_t1612828712 * ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,UnityEngine.Vector3[],UnityEngine.Material,System.Single,Vectrosity.LineType)
extern "C"  void VectorLine__ctor_m1154702976 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Vector3U5BU5D_t1172311765* ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Material,System.Single,Vectrosity.LineType)
extern "C"  void VectorLine__ctor_m2890202228 (VectorLine_t3390220087 * __this, String_t* ___lineName0, List_1_t1612828712 * ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,UnityEngine.Vector3[],UnityEngine.Material,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern "C"  void VectorLine__ctor_m1796543317 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Vector3U5BU5D_t1172311765* ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, int32_t ___joins5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Material,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern "C"  void VectorLine__ctor_m4173415197 (VectorLine_t3390220087 * __this, String_t* ___lineName0, List_1_t1612828712 * ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, int32_t ___joins5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,UnityEngine.Vector2[],UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m3165903783 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Vector2U5BU5D_t686124026* ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m2418552447 (VectorLine_t3390220087 * __this, String_t* ___lineName0, List_1_t1612828711 * ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,UnityEngine.Vector2[],UnityEngine.Material,System.Single,Vectrosity.LineType)
extern "C"  void VectorLine__ctor_m724749957 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Vector2U5BU5D_t686124026* ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Material,System.Single,Vectrosity.LineType)
extern "C"  void VectorLine__ctor_m3180035737 (VectorLine_t3390220087 * __this, String_t* ___lineName0, List_1_t1612828711 * ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,UnityEngine.Vector2[],UnityEngine.Material,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern "C"  void VectorLine__ctor_m2894986576 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Vector2U5BU5D_t686124026* ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, int32_t ___joins5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Material,System.Single,Vectrosity.LineType,Vectrosity.Joins)
extern "C"  void VectorLine__ctor_m2725607650 (VectorLine_t3390220087 * __this, String_t* ___lineName0, List_1_t1612828711 * ___linePoints1, Material_t193706927 * ___lineMaterial2, float ___width3, int32_t ___lineType4, int32_t ___joins5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.Boolean,System.String,UnityEngine.Vector3[],UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m958362481 (VectorLine_t3390220087 * __this, bool ___usePoints0, String_t* ___lineName1, Vector3U5BU5D_t1172311765* ___linePoints2, Material_t193706927 * ___lineMaterial3, float ___width4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.Boolean,System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m2158511833 (VectorLine_t3390220087 * __this, bool ___usePoints0, String_t* ___lineName1, List_1_t1612828712 * ___linePoints2, Material_t193706927 * ___lineMaterial3, float ___width4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.Boolean,System.String,UnityEngine.Vector2[],UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m1221518096 (VectorLine_t3390220087 * __this, bool ___usePoints0, String_t* ___lineName1, Vector2U5BU5D_t686124026* ___linePoints2, Material_t193706927 * ___lineMaterial3, float ___width4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.ctor(System.Boolean,System.String,System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Material,System.Single)
extern "C"  void VectorLine__ctor_m741253976 (VectorLine_t3390220087 * __this, bool ___usePoints0, String_t* ___lineName1, List_1_t1612828711 * ___linePoints2, Material_t193706927 * ___lineMaterial3, float ___width4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vectrosity.VectorLine::Version()
extern "C"  String_t* VectorLine_Version_m3294018701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform Vectrosity.VectorLine::get_rectTransform()
extern "C"  RectTransform_t3349966182 * VectorLine_get_rectTransform_m261819299 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Vectrosity.VectorLine::get_color()
extern "C"  Color_t2020392075  VectorLine_get_color_m1004136679 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_color(UnityEngine.Color)
extern "C"  void VectorLine_set_color_m2912700626 (VectorLine_t3390220087 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::get_points2()
extern "C"  List_1_t1612828711 * VectorLine_get_points2_m2301455271 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::get_points3()
extern "C"  List_1_t1612828712 * VectorLine_get_points3_m3998425255 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::get_pointsCount()
extern "C"  int32_t VectorLine_get_pointsCount_m2100940717 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::get_lineWidth()
extern "C"  float VectorLine_get_lineWidth_m3562543673 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_lineWidth(System.Single)
extern "C"  void VectorLine_set_lineWidth_m1589283628 (VectorLine_t3390220087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::get_maxWeldDistance()
extern "C"  float VectorLine_get_maxWeldDistance_m139898454 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_maxWeldDistance(System.Single)
extern "C"  void VectorLine_set_maxWeldDistance_m1107889891 (VectorLine_t3390220087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vectrosity.VectorLine::get_name()
extern "C"  String_t* VectorLine_get_name_m421216173 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_name(System.String)
extern "C"  void VectorLine_set_name_m2640232092 (VectorLine_t3390220087 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material Vectrosity.VectorLine::get_material()
extern "C"  Material_t193706927 * VectorLine_get_material_m185237339 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_material(UnityEngine.Material)
extern "C"  void VectorLine_set_material_m1110151738 (VectorLine_t3390220087 * __this, Material_t193706927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_active()
extern "C"  bool VectorLine_get_active_m2289158459 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_active(System.Boolean)
extern "C"  void VectorLine_set_active_m2283737522 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::get_capLength()
extern "C"  float VectorLine_get_capLength_m3012559931 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_capLength(System.Single)
extern "C"  void VectorLine_set_capLength_m1703295470 (VectorLine_t3390220087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_smoothWidth()
extern "C"  bool VectorLine_get_smoothWidth_m3990146671 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_smoothWidth(System.Boolean)
extern "C"  void VectorLine_set_smoothWidth_m3138771926 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_smoothColor()
extern "C"  bool VectorLine_get_smoothColor_m2708122042 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_smoothColor(System.Boolean)
extern "C"  void VectorLine_set_smoothColor_m1752054435 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_continuous()
extern "C"  bool VectorLine_get_continuous_m3997971924 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.Joins Vectrosity.VectorLine::get_joins()
extern "C"  int32_t VectorLine_get_joins_m3500786576 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_joins(Vectrosity.Joins)
extern "C"  void VectorLine_set_joins_m2417218007 (VectorLine_t3390220087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_isAutoDrawing()
extern "C"  bool VectorLine_get_isAutoDrawing_m2365632628 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::get_drawStart()
extern "C"  int32_t VectorLine_get_drawStart_m2269785777 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_drawStart(System.Int32)
extern "C"  void VectorLine_set_drawStart_m3881409302 (VectorLine_t3390220087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::get_drawEnd()
extern "C"  int32_t VectorLine_get_drawEnd_m127267824 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_drawEnd(System.Int32)
extern "C"  void VectorLine_set_drawEnd_m1203305025 (VectorLine_t3390220087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::get_endPointsUpdate()
extern "C"  int32_t VectorLine_get_endPointsUpdate_m4149564092 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_endPointsUpdate(System.Int32)
extern "C"  void VectorLine_set_endPointsUpdate_m4215932967 (VectorLine_t3390220087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vectrosity.VectorLine::get_endCap()
extern "C"  String_t* VectorLine_get_endCap_m897994983 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_endCap(System.String)
extern "C"  void VectorLine_set_endCap_m3052503992 (VectorLine_t3390220087 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_continuousTexture()
extern "C"  bool VectorLine_get_continuousTexture_m109706165 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_continuousTexture(System.Boolean)
extern "C"  void VectorLine_set_continuousTexture_m44258570 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vectrosity.VectorLine::get_drawTransform()
extern "C"  Transform_t3275118058 * VectorLine_get_drawTransform_m3685162239 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_drawTransform(UnityEngine.Transform)
extern "C"  void VectorLine_set_drawTransform_m2836536900 (VectorLine_t3390220087 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_useViewportCoords()
extern "C"  bool VectorLine_get_useViewportCoords_m2479791640 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_useViewportCoords(System.Boolean)
extern "C"  void VectorLine_set_useViewportCoords_m282886201 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::get_textureScale()
extern "C"  float VectorLine_get_textureScale_m314191808 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_textureScale(System.Single)
extern "C"  void VectorLine_set_textureScale_m2340282401 (VectorLine_t3390220087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::get_textureOffset()
extern "C"  float VectorLine_get_textureOffset_m743894575 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_textureOffset(System.Single)
extern "C"  void VectorLine_set_textureOffset_m1605559506 (VectorLine_t3390220087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vectrosity.VectorLine::get_matrix()
extern "C"  Matrix4x4_t2933234003  VectorLine_get_matrix_m1377339771 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_matrix(UnityEngine.Matrix4x4)
extern "C"  void VectorLine_set_matrix_m2000552908 (VectorLine_t3390220087 * __this, Matrix4x4_t2933234003  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::get_drawDepth()
extern "C"  int32_t VectorLine_get_drawDepth_m1586983212 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_drawDepth(System.Int32)
extern "C"  void VectorLine_set_drawDepth_m3794752227 (VectorLine_t3390220087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_collider()
extern "C"  bool VectorLine_get_collider_m2634381069 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_collider(System.Boolean)
extern "C"  void VectorLine_set_collider_m594738940 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_trigger()
extern "C"  bool VectorLine_get_trigger_m2000630647 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_trigger(System.Boolean)
extern "C"  void VectorLine_set_trigger_m1807529648 (VectorLine_t3390220087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicsMaterial2D Vectrosity.VectorLine::get_physicsMaterial()
extern "C"  PhysicsMaterial2D_t851691520 * VectorLine_get_physicsMaterial_m3454641085 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_physicsMaterial(UnityEngine.PhysicsMaterial2D)
extern "C"  void VectorLine_set_physicsMaterial_m537997844 (VectorLine_t3390220087 * __this, PhysicsMaterial2D_t851691520 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::get_canvasID()
extern "C"  int32_t VectorLine_get_canvasID_m2304790818 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::set_canvasID(System.Int32)
extern "C"  void VectorLine_set_canvasID_m3398866179 (VectorLine_t3390220087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Canvas> Vectrosity.VectorLine::get_canvases()
extern "C"  List_1_t3873494194 * VectorLine_get_canvases_m4132387343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Canvas> Vectrosity.VectorLine::get_canvases3D()
extern "C"  List_1_t3873494194 * VectorLine_get_canvases3D_m825547508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Canvas Vectrosity.VectorLine::get_canvas()
extern "C"  Canvas_t209405766 * VectorLine_get_canvas_m2026443227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Canvas Vectrosity.VectorLine::get_canvas3D()
extern "C"  Canvas_t209405766 * VectorLine_get_canvas3D_m860538140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::get_camTransformPosition()
extern "C"  Vector3_t2243707580  VectorLine_get_camTransformPosition_m2147146579 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::get_camTransformExists()
extern "C"  bool VectorLine_get_camTransformExists_m4001313274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LineManager Vectrosity.VectorLine::get_lineManager()
extern "C"  LineManager_t3088676951 * VectorLine_get_lineManager_m297251350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::AddColliderIfNeeded()
extern "C"  void VectorLine_AddColliderIfNeeded_m2892278533 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetupLine(System.String,UnityEngine.Material,System.Single,Vectrosity.LineType,Vectrosity.Joins,System.Boolean,System.Boolean,System.Int32)
extern "C"  void VectorLine_SetupLine_m4172391472 (VectorLine_t3390220087 * __this, String_t* ___lineName0, Material_t193706927 * ___useMaterial1, float ___width2, int32_t ___lineType3, int32_t ___joins4, bool ___use2D5, bool ___usePoints6, int32_t ___count7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetupFillObject()
extern "C"  void VectorLine_SetupFillObject_m2304098043 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetupEndCap()
extern "C"  void VectorLine_SetupEndCap_m2164155464 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::CheckPointCount(System.Int32)
extern "C"  bool VectorLine_CheckPointCount_m2695977072 (VectorLine_t3390220087 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::GetVertexCount()
extern "C"  int32_t VectorLine_GetVertexCount_m544466937 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetupTransform(UnityEngine.RectTransform)
extern "C"  void VectorLine_SetupTransform_m441169346 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Resize(System.Int32)
extern "C"  void VectorLine_Resize_m3263448033 (VectorLine_t3390220087 * __this, int32_t ___newCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Resize()
extern "C"  void VectorLine_Resize_m2853088336 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetUVs(System.Int32,System.Int32)
extern "C"  void VectorLine_SetUVs_m201924088 (VectorLine_t3390220087 * __this, int32_t ___startIndex0, int32_t ___endIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::SetVertexCount()
extern "C"  bool VectorLine_SetVertexCount_m411976403 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::MaxSegmentIndex()
extern "C"  int32_t VectorLine_MaxSegmentIndex_m1766026509 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::MaxPoints()
extern "C"  int32_t VectorLine_MaxPoints_m1513001183 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::AddNormals()
extern "C"  void VectorLine_AddNormals_m855968835 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::AddTangents()
extern "C"  void VectorLine_AddTangents_m3519350923 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::CalculateNormals()
extern "C"  void VectorLine_CalculateNormals_m3910785540 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::CalculateTangents()
extern "C"  void VectorLine_CalculateTangents_m824333700 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vectrosity.VectorLine::GetTriangles()
extern "C"  Int32U5BU5D_t3030399641* VectorLine_GetTriangles_m724417885 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::RemoveEndCap()
extern "C"  void VectorLine_RemoveEndCap_m2525427605 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCanvas(System.Int32)
extern "C"  void VectorLine_SetCanvas_m243126217 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCanvas3D(System.Int32)
extern "C"  void VectorLine_SetCanvas3D_m242168580 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCanvasCamera(UnityEngine.Camera)
extern "C"  void VectorLine_SetCanvasCamera_m2896347507 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCanvasCamera(UnityEngine.Camera,System.Int32)
extern "C"  void VectorLine_SetCanvasCamera_m2550939984 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCamera3D()
extern "C"  void VectorLine_SetCamera3D_m4060151470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCamera3D(UnityEngine.Camera)
extern "C"  void VectorLine_SetCamera3D_m3233415682 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___thisCamera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::CameraHasMoved()
extern "C"  bool VectorLine_CameraHasMoved_m753945654 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::UpdateCameraInfo()
extern "C"  void VectorLine_UpdateCameraInfo_m4138685450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vectrosity.VectorLine::GetSegmentNumber()
extern "C"  int32_t VectorLine_GetSegmentNumber_m4146475074 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::WrongArrayLength(System.Int32,Vectrosity.VectorLine/FunctionName)
extern "C"  bool VectorLine_WrongArrayLength_m2774409390 (VectorLine_t3390220087 * __this, int32_t ___arrayLength0, int32_t ___functionName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::CheckArrayLength(Vectrosity.VectorLine/FunctionName,System.Int32,System.Int32)
extern "C"  bool VectorLine_CheckArrayLength_m940236782 (VectorLine_t3390220087 * __this, int32_t ___functionName0, int32_t ___segments1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetEndCapColors()
extern "C"  void VectorLine_SetEndCapColors_m684955295 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color)
extern "C"  void VectorLine_SetColor_m2588318371 (VectorLine_t3390220087 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color,System.Int32)
extern "C"  void VectorLine_SetColor_m1812863840 (VectorLine_t3390220087 * __this, Color_t2020392075  ___color0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetColor(UnityEngine.Color,System.Int32,System.Int32)
extern "C"  void VectorLine_SetColor_m808585903 (VectorLine_t3390220087 * __this, Color_t2020392075  ___color0, int32_t ___startIndex1, int32_t ___endIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetColors(System.Collections.Generic.List`1<UnityEngine.Color>)
extern "C"  void VectorLine_SetColors_m2968496176 (VectorLine_t3390220087 * __this, List_1_t1389513207 * ___lineColors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetColors(UnityEngine.Color[])
extern "C"  void VectorLine_SetColors_m3264185402 (VectorLine_t3390220087 * __this, ColorU5BU5D_t672350442* ___lineColors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetSegmentStartEnd(System.Int32&,System.Int32&)
extern "C"  void VectorLine_SetSegmentStartEnd_m254540202 (VectorLine_t3390220087 * __this, int32_t* ___start0, int32_t* ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetFillColors()
extern "C"  void VectorLine_SetFillColors_m3858814017 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Vectrosity.VectorLine::GetColor(System.Int32)
extern "C"  Color_t2020392075  VectorLine_GetColor_m882218109 (VectorLine_t3390220087 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidth(System.Single)
extern "C"  void VectorLine_SetWidth_m360437547 (VectorLine_t3390220087 * __this, float ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidth(System.Single,System.Int32)
extern "C"  void VectorLine_SetWidth_m1558342224 (VectorLine_t3390220087 * __this, float ___width0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidth(System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_SetWidth_m158899223 (VectorLine_t3390220087 * __this, float ___width0, int32_t ___startIndex1, int32_t ___endIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidthArray(System.Int32)
extern "C"  void VectorLine_SetWidthArray_m2981076294 (VectorLine_t3390220087 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidths(System.Collections.Generic.List`1<System.Single>)
extern "C"  void VectorLine_SetWidths_m2710170370 (VectorLine_t3390220087 * __this, List_1_t1445631064 * ___lineWidths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidths(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void VectorLine_SetWidths_m3082272800 (VectorLine_t3390220087 * __this, List_1_t1440998580 * ___lineWidths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidths(System.Single[])
extern "C"  void VectorLine_SetWidths_m4210076936 (VectorLine_t3390220087 * __this, SingleU5BU5D_t577127397* ___lineWidths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidths(System.Int32[])
extern "C"  void VectorLine_SetWidths_m2461362618 (VectorLine_t3390220087 * __this, Int32U5BU5D_t3030399641* ___lineWidths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetWidths(System.Single[],System.Int32[],System.Int32,System.Boolean)
extern "C"  void VectorLine_SetWidths_m216709371 (VectorLine_t3390220087 * __this, SingleU5BU5D_t577127397* ___lineWidthsFloat0, Int32U5BU5D_t3030399641* ___lineWidthsInt1, int32_t ___arrayLength2, bool ___doFloat3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::GetWidth(System.Int32)
extern "C"  float VectorLine_GetWidth_m54083509 (VectorLine_t3390220087 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetLine(UnityEngine.Color,UnityEngine.Vector2[])
extern "C"  VectorLine_t3390220087 * VectorLine_SetLine_m3052310416 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, Vector2U5BU5D_t686124026* ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetLine(UnityEngine.Color,System.Single,UnityEngine.Vector2[])
extern "C"  VectorLine_t3390220087 * VectorLine_SetLine_m567058683 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___time1, Vector2U5BU5D_t686124026* ___points2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetLine(UnityEngine.Color,UnityEngine.Vector3[])
extern "C"  VectorLine_t3390220087 * VectorLine_SetLine_m61305035 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, Vector3U5BU5D_t1172311765* ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetLine(UnityEngine.Color,System.Single,UnityEngine.Vector3[])
extern "C"  VectorLine_t3390220087 * VectorLine_SetLine_m3275739062 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___time1, Vector3U5BU5D_t1172311765* ___points2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetLine3D(UnityEngine.Color,UnityEngine.Vector3[])
extern "C"  VectorLine_t3390220087 * VectorLine_SetLine3D_m2811310988 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, Vector3U5BU5D_t1172311765* ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetLine3D(UnityEngine.Color,System.Single,UnityEngine.Vector3[])
extern "C"  VectorLine_t3390220087 * VectorLine_SetLine3D_m282308281 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___time1, Vector3U5BU5D_t1172311765* ___points2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetRay(UnityEngine.Color,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  VectorLine_t3390220087 * VectorLine_SetRay_m2367900830 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, Vector3_t2243707580  ___origin1, Vector3_t2243707580  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetRay(UnityEngine.Color,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  VectorLine_t3390220087 * VectorLine_SetRay_m1842217005 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___time1, Vector3_t2243707580  ___origin2, Vector3_t2243707580  ___direction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetRay3D(UnityEngine.Color,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  VectorLine_t3390220087 * VectorLine_SetRay3D_m3855581103 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, Vector3_t2243707580  ___origin1, Vector3_t2243707580  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vectrosity.VectorLine Vectrosity.VectorLine::SetRay3D(UnityEngine.Color,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  VectorLine_t3390220087 * VectorLine_SetRay3D_m180117512 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, float ___time1, Vector3_t2243707580  ___origin2, Vector3_t2243707580  ___direction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::CheckLine(System.Boolean)
extern "C"  bool VectorLine_CheckLine_m433724305 (VectorLine_t3390220087 * __this, bool ___draw3D0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::DrawFill()
extern "C"  void VectorLine_DrawFill_m3115441101 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::DrawEndCap(System.Boolean)
extern "C"  void VectorLine_DrawEndCap_m2005841632 (VectorLine_t3390220087 * __this, bool ___draw3D0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetContinuousTexture()
extern "C"  void VectorLine_SetContinuousTexture_m1662665266 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::CheckNormals()
extern "C"  void VectorLine_CheckNormals_m1967536532 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::UseMatrix(UnityEngine.Matrix4x4&)
extern "C"  bool VectorLine_UseMatrix_m3893068018 (VectorLine_t3390220087 * __this, Matrix4x4_t2933234003 * ___thisMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::CheckPointCount()
extern "C"  bool VectorLine_CheckPointCount_m665713683 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetupDrawStartEnd(System.Int32&,System.Int32&,System.Boolean)
extern "C"  void VectorLine_SetupDrawStartEnd_m733640161 (VectorLine_t3390220087 * __this, int32_t* ___start0, int32_t* ___end1, bool ___clearVertices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::ZeroVertices(System.Int32,System.Int32)
extern "C"  void VectorLine_ZeroVertices_m3119089157 (VectorLine_t3390220087 * __this, int32_t ___startIndex0, int32_t ___endIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Draw()
extern "C"  void VectorLine_Draw_m37097234 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Line2D(System.Int32,System.Int32,UnityEngine.Matrix4x4,System.Boolean)
extern "C"  void VectorLine_Line2D_m2608477237 (VectorLine_t3390220087 * __this, int32_t ___start0, int32_t ___end1, Matrix4x4_t2933234003  ___thisMatrix2, bool ___useTransformMatrix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Line3D(System.Int32,System.Int32,UnityEngine.Matrix4x4,System.Boolean)
extern "C"  void VectorLine_Line3D_m3574982928 (VectorLine_t3390220087 * __this, int32_t ___start0, int32_t ___end1, Matrix4x4_t2933234003  ___thisMatrix2, bool ___useTransformMatrix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Draw3D()
extern "C"  void VectorLine_Draw3D_m3402254297 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::DrawPoints()
extern "C"  void VectorLine_DrawPoints_m652135997 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::DrawPoints3D()
extern "C"  void VectorLine_DrawPoints3D_m1820158362 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SkipQuad(System.Int32&,System.Int32&,System.Int32&)
extern "C"  void VectorLine_SkipQuad_m3105607789 (VectorLine_t3390220087 * __this, int32_t* ___idx0, int32_t* ___widthIdx1, int32_t* ___widthIdxAdd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SkipQuad3D(System.Int32&,System.Int32&,System.Int32&)
extern "C"  void VectorLine_SkipQuad3D_m1773443282 (VectorLine_t3390220087 * __this, int32_t* ___idx0, int32_t* ___widthIdx1, int32_t* ___widthIdxAdd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::WeldJoins(System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_WeldJoins_m3325324716 (VectorLine_t3390220087 * __this, int32_t ___start0, int32_t ___end1, bool ___connectFirstAndLast2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::WeldJoinsDiscrete(System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_WeldJoinsDiscrete_m2528057965 (VectorLine_t3390220087 * __this, int32_t ___start0, int32_t ___end1, bool ___connectFirstAndLast2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetIntersectionPoint(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VectorLine_SetIntersectionPoint_m3919494541 (VectorLine_t3390220087 * __this, int32_t ___p10, int32_t ___p21, int32_t ___p32, int32_t ___p43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::WeldJoins3D(System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_WeldJoins3D_m1387166373 (VectorLine_t3390220087 * __this, int32_t ___start0, int32_t ___end1, bool ___connectFirstAndLast2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::WeldJoinsDiscrete3D(System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_WeldJoinsDiscrete3D_m1365959144 (VectorLine_t3390220087 * __this, int32_t ___start0, int32_t ___end1, bool ___connectFirstAndLast2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetIntersectionPoint3D(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VectorLine_SetIntersectionPoint3D_m2758311792 (VectorLine_t3390220087 * __this, int32_t ___p10, int32_t ___p21, int32_t ___p32, int32_t ___p43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::LineManagerCheckDistance()
extern "C"  void VectorLine_LineManagerCheckDistance_m633561808 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::LineManagerDisable()
extern "C"  void VectorLine_LineManagerDisable_m1456461575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::LineManagerEnable()
extern "C"  void VectorLine_LineManagerEnable_m3669088300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Draw3DAuto()
extern "C"  void VectorLine_Draw3DAuto_m2816228362 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Draw3DAuto(System.Single)
extern "C"  void VectorLine_Draw3DAuto_m2160748411 (VectorLine_t3390220087 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::StopDrawing3DAuto()
extern "C"  void VectorLine_StopDrawing3DAuto_m1174835984 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetTextureScale(System.Boolean)
extern "C"  void VectorLine_SetTextureScale_m2439981336 (VectorLine_t3390220087 * __this, bool ___updateUIVertices0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::ResetTextureScale()
extern "C"  void VectorLine_ResetTextureScale_m2691123730 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetCollider(System.Boolean)
extern "C"  void VectorLine_SetCollider_m737977485 (VectorLine_t3390220087 * __this, bool ___convertToWorldSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Vectrosity.VectorLine::BytesToVector3Array(System.Byte[])
extern "C"  Vector3U5BU5D_t1172311765* VectorLine_BytesToVector3Array_m1077103206 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___lineBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] Vectrosity.VectorLine::BytesToVector2Array(System.Byte[])
extern "C"  Vector2U5BU5D_t686124026* VectorLine_BytesToVector2Array_m2653979366 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___lineBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetupByteBlock()
extern "C"  void VectorLine_SetupByteBlock_m3993517876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::ConvertToFloat(System.Byte[],System.Int32)
extern "C"  float VectorLine_ConvertToFloat_m2929045280 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorLine&)
extern "C"  void VectorLine_Destroy_m3687583833 (Il2CppObject * __this /* static, unused */, VectorLine_t3390220087 ** ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorLine[])
extern "C"  void VectorLine_Destroy_m2542618849 (Il2CppObject * __this /* static, unused */, VectorLineU5BU5D_t4219197966* ___lines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(System.Collections.Generic.List`1<Vectrosity.VectorLine>)
extern "C"  void VectorLine_Destroy_m2472190125 (Il2CppObject * __this /* static, unused */, List_1_t2759341219 * ___lines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::DestroyLine(Vectrosity.VectorLine&)
extern "C"  void VectorLine_DestroyLine_m3403904859 (Il2CppObject * __this /* static, unused */, VectorLine_t3390220087 ** ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorPoints&)
extern "C"  void VectorLine_Destroy_m1416138262 (Il2CppObject * __this /* static, unused */, VectorPoints_t440342714 ** ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorPoints[])
extern "C"  void VectorLine_Destroy_m411073614 (Il2CppObject * __this /* static, unused */, VectorPointsU5BU5D_t3745766175* ___lines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(System.Collections.Generic.List`1<Vectrosity.VectorPoints>)
extern "C"  void VectorLine_Destroy_m3269577336 (Il2CppObject * __this /* static, unused */, List_1_t4104431142 * ___lines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::DestroyPoints(Vectrosity.VectorPoints&)
extern "C"  void VectorLine_DestroyPoints_m194560123 (Il2CppObject * __this /* static, unused */, VectorPoints_t440342714 ** ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorLine&,UnityEngine.GameObject)
extern "C"  void VectorLine_Destroy_m3128929253 (Il2CppObject * __this /* static, unused */, VectorLine_t3390220087 ** ___line0, GameObject_t1756533147 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::Destroy(Vectrosity.VectorPoints&,UnityEngine.GameObject)
extern "C"  void VectorLine_Destroy_m1224152522 (Il2CppObject * __this /* static, unused */, VectorPoints_t440342714 ** ___line0, GameObject_t1756533147 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Rect)
extern "C"  void VectorLine_MakeRect_m2057505443 (VectorLine_t3390220087 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Rect,System.Int32)
extern "C"  void VectorLine_MakeRect_m254715586 (VectorLine_t3390220087 * __this, Rect_t3681755626  ___rect0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void VectorLine_MakeRect_m2123845090 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___topLeft0, Vector3_t2243707580  ___bottomRight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeRect(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  void VectorLine_MakeRect_m1590963481 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___topLeft0, Vector3_t2243707580  ___bottomRight1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single)
extern "C"  void VectorLine_MakeCircle_m159670576 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  void VectorLine_MakeCircle_m3334730739 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___radius1, int32_t ___segments2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  void VectorLine_MakeCircle_m828971760 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___radius1, int32_t ___segments2, float ___pointRotation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeCircle_m3014015634 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___radius1, int32_t ___segments2, int32_t ___index3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Int32)
extern "C"  void VectorLine_MakeCircle_m1515020415 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___radius1, int32_t ___segments2, float ___pointRotation3, int32_t ___index4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void VectorLine_MakeCircle_m2457822803 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  void VectorLine_MakeCircle_m1722413054 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___radius2, int32_t ___segments3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  void VectorLine_MakeCircle_m4268713411 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___radius2, int32_t ___segments3, float ___pointRotation4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeCircle_m3772240815 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___radius2, int32_t ___segments3, int32_t ___index4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCircle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Int32)
extern "C"  void VectorLine_MakeCircle_m1531096068 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___radius2, int32_t ___segments3, float ___pointRotation4, int32_t ___index5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,System.Single,System.Single)
extern "C"  void VectorLine_MakeEllipse_m649544811 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,System.Single,System.Single,System.Int32)
extern "C"  void VectorLine_MakeEllipse_m902787322 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, int32_t ___segments3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeEllipse_m3796718199 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, int32_t ___segments3, int32_t ___index4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Single)
extern "C"  void VectorLine_MakeEllipse_m2134198699 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, int32_t ___segments3, float ___pointRotation4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  void VectorLine_MakeEllipse_m2817723260 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32)
extern "C"  void VectorLine_MakeEllipse_m1096712595 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, int32_t ___segments4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeEllipse_m3011309094 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, int32_t ___segments4, int32_t ___index5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Single)
extern "C"  void VectorLine_MakeEllipse_m3932280044 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, int32_t ___segments4, float ___pointRotation5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Single,System.Int32)
extern "C"  void VectorLine_MakeEllipse_m3236159951 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, int32_t ___segments4, float ___pointRotation5, int32_t ___index6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single)
extern "C"  void VectorLine_MakeArc_m2301332057 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, float ___startDegrees3, float ___endDegrees4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void VectorLine_MakeArc_m2248311124 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, float ___startDegrees3, float ___endDegrees4, int32_t ___segments5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeArc_m3717184613 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, float ___xRadius1, float ___yRadius2, float ___startDegrees3, float ___endDegrees4, int32_t ___segments5, int32_t ___index6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single)
extern "C"  void VectorLine_MakeArc_m1866154798 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, float ___startDegrees4, float ___endDegrees5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void VectorLine_MakeArc_m3943191449 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, float ___startDegrees4, float ___endDegrees5, int32_t ___segments6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeArc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeArc_m4065187736 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, float ___startDegrees4, float ___endDegrees5, int32_t ___segments6, int32_t ___index7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeEllipse(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Int32)
extern "C"  void VectorLine_MakeEllipse_m1936882095 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___upVector1, float ___xRadius2, float ___yRadius3, float ___startDegrees4, float ___endDegrees5, int32_t ___segments6, float ___pointRotation7, int32_t ___index8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector2[])
extern "C"  void VectorLine_MakeCurve_m1914378869 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___curvePoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector2[],System.Int32)
extern "C"  void VectorLine_MakeCurve_m4138538168 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___curvePoints0, int32_t ___segments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector2[],System.Int32,System.Int32)
extern "C"  void VectorLine_MakeCurve_m2259633857 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___curvePoints0, int32_t ___segments1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3[])
extern "C"  void VectorLine_MakeCurve_m3218340784 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___curvePoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3[],System.Int32)
extern "C"  void VectorLine_MakeCurve_m3546541981 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___curvePoints0, int32_t ___segments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3[],System.Int32,System.Int32)
extern "C"  void VectorLine_MakeCurve_m3487968678 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___curvePoints0, int32_t ___segments1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void VectorLine_MakeCurve_m494325551 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___anchor10, Vector3_t2243707580  ___control11, Vector3_t2243707580  ___anchor22, Vector3_t2243707580  ___control23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  void VectorLine_MakeCurve_m3654912566 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___anchor10, Vector3_t2243707580  ___control11, Vector3_t2243707580  ___anchor22, Vector3_t2243707580  ___control23, int32_t ___segments4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,System.Int32)
extern "C"  void VectorLine_MakeCurve_m3926062499 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___anchor10, Vector3_t2243707580  ___control11, Vector3_t2243707580  ___anchor22, Vector3_t2243707580  ___control23, int32_t ___segments4, int32_t ___index5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vectrosity.VectorLine::GetBezierPoint(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single)
extern "C"  Vector2_t2243707579  VectorLine_GetBezierPoint_m693663137 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___anchor10, Vector2_t2243707579 * ___control11, Vector2_t2243707579 * ___anchor22, Vector2_t2243707579 * ___control23, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::GetBezierPoint3D(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
extern "C"  Vector3_t2243707580  VectorLine_GetBezierPoint3D_m1333277703 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___anchor10, Vector3_t2243707580 * ___control11, Vector3_t2243707580 * ___anchor22, Vector3_t2243707580 * ___control23, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[])
extern "C"  void VectorLine_MakeSpline_m4203652291 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],System.Boolean)
extern "C"  void VectorLine_MakeSpline_m3365297320 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints0, bool ___loop1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],System.Int32)
extern "C"  void VectorLine_MakeSpline_m303329920 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints0, int32_t ___segments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],System.Int32,System.Boolean)
extern "C"  void VectorLine_MakeSpline_m1236307841 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints0, int32_t ___segments1, bool ___loop2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],System.Int32,System.Int32)
extern "C"  void VectorLine_MakeSpline_m2384179663 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints0, int32_t ___segments1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_MakeSpline_m1935211550 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints0, int32_t ___segments1, int32_t ___index2, bool ___loop3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[])
extern "C"  void VectorLine_MakeSpline_m4203652388 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___splinePoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[],System.Boolean)
extern "C"  void VectorLine_MakeSpline_m3082048393 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___splinePoints0, bool ___loop1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[],System.Int32)
extern "C"  void VectorLine_MakeSpline_m2377148799 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___splinePoints0, int32_t ___segments1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[],System.Int32,System.Boolean)
extern "C"  void VectorLine_MakeSpline_m121645602 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___splinePoints0, int32_t ___segments1, bool ___loop2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[],System.Int32,System.Int32)
extern "C"  void VectorLine_MakeSpline_m1048890318 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___splinePoints0, int32_t ___segments1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector3[],System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_MakeSpline_m231064413 (VectorLine_t3390220087 * __this, Vector3U5BU5D_t1172311765* ___splinePoints0, int32_t ___segments1, int32_t ___index2, bool ___loop3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeSpline(UnityEngine.Vector2[],UnityEngine.Vector3[],System.Int32,System.Int32,System.Boolean)
extern "C"  void VectorLine_MakeSpline_m2690739351 (VectorLine_t3390220087 * __this, Vector2U5BU5D_t686124026* ___splinePoints20, Vector3U5BU5D_t1172311765* ___splinePoints31, int32_t ___segments2, int32_t ___index3, bool ___loop4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vectrosity.VectorLine::GetSplinePoint(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single)
extern "C"  Vector2_t2243707579  VectorLine_GetSplinePoint_m2607427043 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___p00, Vector2_t2243707579 * ___p11, Vector2_t2243707579 * ___p22, Vector2_t2243707579 * ___p33, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::GetSplinePoint3D(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
extern "C"  Vector3_t2243707580  VectorLine_GetSplinePoint3D_m687863445 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___p00, Vector3_t2243707580 * ___p11, Vector3_t2243707580 * ___p22, Vector3_t2243707580 * ___p33, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::VectorDistanceSquared(UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  float VectorLine_VectorDistanceSquared_m3157422751 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___p0, Vector2_t2243707579 * ___q1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::VectorDistanceSquared(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  float VectorLine_VectorDistanceSquared_m3338311939 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___p0, Vector3_t2243707580 * ___q1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::InitNonuniformCatmullRom(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector4&)
extern "C"  void VectorLine_InitNonuniformCatmullRom_m1870201284 (Il2CppObject * __this /* static, unused */, float ___x00, float ___x11, float ___x22, float ___x33, float ___dt04, float ___dt15, float ___dt26, Vector4_t2243707581 * ___p7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::EvalCubicPoly(UnityEngine.Vector4&,System.Single)
extern "C"  float VectorLine_EvalCubicPoly_m218399359 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581 * ___p0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeText(System.String,UnityEngine.Vector3,System.Single)
extern "C"  void VectorLine_MakeText_m1905085387 (VectorLine_t3390220087 * __this, String_t* ___text0, Vector3_t2243707580  ___startPos1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeText(System.String,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  void VectorLine_MakeText_m1317309680 (VectorLine_t3390220087 * __this, String_t* ___text0, Vector3_t2243707580  ___startPos1, float ___size2, bool ___uppercaseOnly3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeText(System.String,UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern "C"  void VectorLine_MakeText_m1570316587 (VectorLine_t3390220087 * __this, String_t* ___text0, Vector3_t2243707580  ___startPos1, float ___size2, float ___charSpacing3, float ___lineSpacing4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeText(System.String,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void VectorLine_MakeText_m3669056336 (VectorLine_t3390220087 * __this, String_t* ___text0, Vector3_t2243707580  ___startPos1, float ___size2, float ___charSpacing3, float ___lineSpacing4, bool ___uppercaseOnly5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeWireframe(UnityEngine.Mesh)
extern "C"  void VectorLine_MakeWireframe_m2816555668 (VectorLine_t3390220087 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::CheckPairPoints(System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>,UnityEngine.Vector3,UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void VectorLine_CheckPairPoints_m3837104218 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3263405159 * ___pairs0, Vector3_t2243707580  ___p11, Vector3_t2243707580  ___p22, List_1_t1612828712 * ___linePoints3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCube(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern "C"  void VectorLine_MakeCube_m3364399641 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___position0, float ___xSize1, float ___ySize2, float ___zSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::MakeCube(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void VectorLine_MakeCube_m3696684002 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___position0, float ___xSize1, float ___ySize2, float ___zSize3, int32_t ___index4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetDistances()
extern "C"  void VectorLine_SetDistances_m3271522508 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vectrosity.VectorLine::GetLength()
extern "C"  float VectorLine_GetLength_m238717388 (VectorLine_t3390220087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vectrosity.VectorLine::GetPoint01(System.Single)
extern "C"  Vector2_t2243707579  VectorLine_GetPoint01_m3102057791 (VectorLine_t3390220087 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vectrosity.VectorLine::GetPoint01(System.Single,System.Int32&)
extern "C"  Vector2_t2243707579  VectorLine_GetPoint01_m2059994708 (VectorLine_t3390220087 * __this, float ___distance0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vectrosity.VectorLine::GetPoint(System.Single)
extern "C"  Vector2_t2243707579  VectorLine_GetPoint_m1869163360 (VectorLine_t3390220087 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vectrosity.VectorLine::GetPoint(System.Single,System.Int32&)
extern "C"  Vector2_t2243707579  VectorLine_GetPoint_m2498620021 (VectorLine_t3390220087 * __this, float ___distance0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::GetPoint3D01(System.Single)
extern "C"  Vector3_t2243707580  VectorLine_GetPoint3D01_m1921802289 (VectorLine_t3390220087 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::GetPoint3D01(System.Single,System.Int32&)
extern "C"  Vector3_t2243707580  VectorLine_GetPoint3D01_m2427526396 (VectorLine_t3390220087 * __this, float ___distance0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::GetPoint3D(System.Single)
extern "C"  Vector3_t2243707580  VectorLine_GetPoint3D_m1735282160 (VectorLine_t3390220087 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vectrosity.VectorLine::GetPoint3D(System.Single,System.Int32&)
extern "C"  Vector3_t2243707580  VectorLine_GetPoint3D_m2603446523 (VectorLine_t3390220087 * __this, float ___distance0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetDistanceIndex(System.Int32&,System.Single)
extern "C"  void VectorLine_SetDistanceIndex_m409714327 (VectorLine_t3390220087 * __this, int32_t* ___i0, float ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetEndCap(System.String,Vectrosity.EndCap)
extern "C"  void VectorLine_SetEndCap_m2151966608 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___capType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetEndCap(System.String,Vectrosity.EndCap,UnityEngine.Material,UnityEngine.Texture2D[])
extern "C"  void VectorLine_SetEndCap_m3426110492 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___capType1, Material_t193706927 * ___material2, Texture2DU5BU5D_t2724090252* ___textures3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::SetEndCap(System.String,Vectrosity.EndCap,UnityEngine.Material,System.Single,UnityEngine.Texture2D[])
extern "C"  void VectorLine_SetEndCap_m1108249587 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___capType1, Material_t193706927 * ___material2, float ___offset3, Texture2DU5BU5D_t2724090252* ___textures4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::RemoveEndCap(System.String)
extern "C"  void VectorLine_RemoveEndCap_m4242156599 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Selected(UnityEngine.Vector2)
extern "C"  bool VectorLine_Selected_m358647015 (VectorLine_t3390220087 * __this, Vector2_t2243707579  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Selected(UnityEngine.Vector2,System.Int32&)
extern "C"  bool VectorLine_Selected_m1754308664 (VectorLine_t3390220087 * __this, Vector2_t2243707579  ___p0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Selected(UnityEngine.Vector2,System.Int32,System.Int32&)
extern "C"  bool VectorLine_Selected_m1566458173 (VectorLine_t3390220087 * __this, Vector2_t2243707579  ___p0, int32_t ___extraDistance1, int32_t* ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Selected(UnityEngine.Vector2,System.Int32,System.Int32,System.Int32&)
extern "C"  bool VectorLine_Selected_m3041172206 (VectorLine_t3390220087 * __this, Vector2_t2243707579  ___p0, int32_t ___extraDistance1, int32_t ___extraLength2, int32_t* ___index3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Approximately(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool VectorLine_Approximately_m2215230831 (VectorLine_t3390220087 * __this, Vector2_t2243707579  ___p10, Vector2_t2243707579  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Approximately(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool VectorLine_Approximately_m3717415151 (VectorLine_t3390220087 * __this, Vector3_t2243707580  ___p10, Vector3_t2243707580  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vectrosity.VectorLine::Approximately(System.Single,System.Single)
extern "C"  bool VectorLine_Approximately_m935019663 (VectorLine_t3390220087 * __this, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vectrosity.VectorLine::.cctor()
extern "C"  void VectorLine__cctor_m2566424025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
