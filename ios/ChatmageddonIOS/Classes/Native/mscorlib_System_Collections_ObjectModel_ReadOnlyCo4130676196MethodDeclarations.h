﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>
struct ReadOnlyCollection_1_t4130676196;
// System.Collections.Generic.IList`1<AttackHomeNav>
struct IList_1_t190863809;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AttackHomeNav[]
struct AttackHomeNavU5BU5D_t3086173273;
// System.Collections.Generic.IEnumerator`1<AttackHomeNav>
struct IEnumerator_1_t1420414331;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3099435120_gshared (ReadOnlyCollection_1_t4130676196 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3099435120(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3099435120_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2688052864_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2688052864(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2688052864_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3951900324_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3951900324(__this, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3951900324_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1213394431_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1213394431(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1213394431_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2509937143_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2509937143(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2509937143_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1665903755_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1665903755(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1665903755_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2086064435_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2086064435(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2086064435_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1170529090_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1170529090(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1170529090_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m956825226_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m956825226(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m956825226_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1548727517_gshared (ReadOnlyCollection_1_t4130676196 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1548727517(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1548727517_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m216000286_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m216000286(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m216000286_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m4284569409_gshared (ReadOnlyCollection_1_t4130676196 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4284569409(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4130676196 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m4284569409_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2675301717_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2675301717(__this, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2675301717_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3714616973_gshared (ReadOnlyCollection_1_t4130676196 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3714616973(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3714616973_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4025359027_gshared (ReadOnlyCollection_1_t4130676196 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4025359027(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4130676196 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4025359027_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1339236904_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1339236904(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1339236904_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2305557922_gshared (ReadOnlyCollection_1_t4130676196 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2305557922(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2305557922_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1158660642_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1158660642(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1158660642_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1708595753_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1708595753(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1708595753_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3972721197_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3972721197(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3972721197_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m743606080_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m743606080(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m743606080_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m823820341_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m823820341(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m823820341_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m726163396_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m726163396(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m726163396_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m782694867_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m782694867(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m782694867_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1463703826_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1463703826(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1463703826_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2066618060_gshared (ReadOnlyCollection_1_t4130676196 * __this, AttackHomeNavU5BU5D_t3086173273* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2066618060(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4130676196 *, AttackHomeNavU5BU5D_t3086173273*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2066618060_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3233994345_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3233994345(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3233994345_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2420487974_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2420487974(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2420487974_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1431948785_gshared (ReadOnlyCollection_1_t4130676196 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1431948785(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4130676196 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1431948785_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AttackHomeNav>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m694667471_gshared (ReadOnlyCollection_1_t4130676196 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m694667471(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4130676196 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m694667471_gshared)(__this, ___index0, method)
