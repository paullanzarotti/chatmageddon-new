﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenToGeoExample
struct ScreenToGeoExample_t3755261980;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenToGeoExample::.ctor()
extern "C"  void ScreenToGeoExample__ctor_m2373681025 (ScreenToGeoExample_t3755261980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenToGeoExample::Update()
extern "C"  void ScreenToGeoExample_Update_m3312269962 (ScreenToGeoExample_t3755261980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
