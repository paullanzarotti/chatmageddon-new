﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProgressBar
struct ProgressBar_t192201240;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar/<Transition>c__Iterator0
struct  U3CTransitionU3Ec__Iterator0_t3581714401  : public Il2CppObject
{
public:
	// System.Single ProgressBar/<Transition>c__Iterator0::<amountToTransition>__0
	float ___U3CamountToTransitionU3E__0_0;
	// System.Boolean ProgressBar/<Transition>c__Iterator0::<decreasing>__1
	bool ___U3CdecreasingU3E__1_1;
	// System.Single ProgressBar/<Transition>c__Iterator0::percent
	float ___percent_2;
	// System.Single ProgressBar/<Transition>c__Iterator0::time
	float ___time_3;
	// System.Single ProgressBar/<Transition>c__Iterator0::<amountToChange>__2
	float ___U3CamountToChangeU3E__2_4;
	// ProgressBar ProgressBar/<Transition>c__Iterator0::$this
	ProgressBar_t192201240 * ___U24this_5;
	// System.Object ProgressBar/<Transition>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean ProgressBar/<Transition>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 ProgressBar/<Transition>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CamountToTransitionU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U3CamountToTransitionU3E__0_0)); }
	inline float get_U3CamountToTransitionU3E__0_0() const { return ___U3CamountToTransitionU3E__0_0; }
	inline float* get_address_of_U3CamountToTransitionU3E__0_0() { return &___U3CamountToTransitionU3E__0_0; }
	inline void set_U3CamountToTransitionU3E__0_0(float value)
	{
		___U3CamountToTransitionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdecreasingU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U3CdecreasingU3E__1_1)); }
	inline bool get_U3CdecreasingU3E__1_1() const { return ___U3CdecreasingU3E__1_1; }
	inline bool* get_address_of_U3CdecreasingU3E__1_1() { return &___U3CdecreasingU3E__1_1; }
	inline void set_U3CdecreasingU3E__1_1(bool value)
	{
		___U3CdecreasingU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_percent_2() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___percent_2)); }
	inline float get_percent_2() const { return ___percent_2; }
	inline float* get_address_of_percent_2() { return &___percent_2; }
	inline void set_percent_2(float value)
	{
		___percent_2 = value;
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___time_3)); }
	inline float get_time_3() const { return ___time_3; }
	inline float* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(float value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_U3CamountToChangeU3E__2_4() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U3CamountToChangeU3E__2_4)); }
	inline float get_U3CamountToChangeU3E__2_4() const { return ___U3CamountToChangeU3E__2_4; }
	inline float* get_address_of_U3CamountToChangeU3E__2_4() { return &___U3CamountToChangeU3E__2_4; }
	inline void set_U3CamountToChangeU3E__2_4(float value)
	{
		___U3CamountToChangeU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U24this_5)); }
	inline ProgressBar_t192201240 * get_U24this_5() const { return ___U24this_5; }
	inline ProgressBar_t192201240 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ProgressBar_t192201240 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ec__Iterator0_t3581714401, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
