﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmallBread
struct SmallBread_t121884373;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SmallBread::.ctor(System.String)
extern "C"  void SmallBread__ctor_m1587129548 (SmallBread_t121884373 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
