﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XComment
struct XComment_t1618005895;
// System.String
struct String_t;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XComment1618005895.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"

// System.Void System.Xml.Linq.XComment::.ctor(System.String)
extern "C"  void XComment__ctor_m2882971392 (XComment_t1618005895 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XComment::.ctor(System.Xml.Linq.XComment)
extern "C"  void XComment__ctor_m1201847697 (XComment_t1618005895 * __this, XComment_t1618005895 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XComment::get_NodeType()
extern "C"  int32_t XComment_get_NodeType_m201800269 (XComment_t1618005895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XComment::get_Value()
extern "C"  String_t* XComment_get_Value_m962028537 (XComment_t1618005895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XComment::WriteTo(System.Xml.XmlWriter)
extern "C"  void XComment_WriteTo_m1580837578 (XComment_t1618005895 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
