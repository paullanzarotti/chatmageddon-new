﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Fabric.Crashlytics.Internal.Impl
struct Impl_t2902205834;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fabric.Crashlytics.Crashlytics
struct  Crashlytics_t3790431817  : public Il2CppObject
{
public:

public:
};

struct Crashlytics_t3790431817_StaticFields
{
public:
	// Fabric.Crashlytics.Internal.Impl Fabric.Crashlytics.Crashlytics::impl
	Impl_t2902205834 * ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(Crashlytics_t3790431817_StaticFields, ___impl_0)); }
	inline Impl_t2902205834 * get_impl_0() const { return ___impl_0; }
	inline Impl_t2902205834 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(Impl_t2902205834 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier(&___impl_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
