﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConnectionStringSettingsCollection
struct ConnectionStringSettingsCollection_t2042791570;
// System.Configuration.ConnectionStringSettings
struct ConnectionStringSettings_t1154375560;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Connecti1154375560.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Void System.Configuration.ConnectionStringSettingsCollection::.ctor()
extern "C"  void ConnectionStringSettingsCollection__ctor_m2374738653 (ConnectionStringSettingsCollection_t2042791570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConnectionStringSettings System.Configuration.ConnectionStringSettingsCollection::get_Item(System.String)
extern "C"  ConnectionStringSettings_t1154375560 * ConnectionStringSettingsCollection_get_Item_m4175140951 (ConnectionStringSettingsCollection_t2042791570 * __this, String_t* ___Name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConnectionStringSettings System.Configuration.ConnectionStringSettingsCollection::get_Item(System.Int32)
extern "C"  ConnectionStringSettings_t1154375560 * ConnectionStringSettingsCollection_get_Item_m3614835886 (ConnectionStringSettingsCollection_t2042791570 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::set_Item(System.Int32,System.Configuration.ConnectionStringSettings)
extern "C"  void ConnectionStringSettingsCollection_set_Item_m1710566771 (ConnectionStringSettingsCollection_t2042791570 * __this, int32_t ___index0, ConnectionStringSettings_t1154375560 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConnectionStringSettingsCollection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * ConnectionStringSettingsCollection_get_Properties_m4049248428 (ConnectionStringSettingsCollection_t2042791570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElement System.Configuration.ConnectionStringSettingsCollection::CreateNewElement()
extern "C"  ConfigurationElement_t1776195828 * ConnectionStringSettingsCollection_CreateNewElement_m762535467 (ConnectionStringSettingsCollection_t2042791570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ConnectionStringSettingsCollection::GetElementKey(System.Configuration.ConfigurationElement)
extern "C"  Il2CppObject * ConnectionStringSettingsCollection_GetElementKey_m2388531382 (ConnectionStringSettingsCollection_t2042791570 * __this, ConfigurationElement_t1776195828 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::Add(System.Configuration.ConnectionStringSettings)
extern "C"  void ConnectionStringSettingsCollection_Add_m1670019115 (ConnectionStringSettingsCollection_t2042791570 * __this, ConnectionStringSettings_t1154375560 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::Clear()
extern "C"  void ConnectionStringSettingsCollection_Clear_m668539870 (ConnectionStringSettingsCollection_t2042791570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Configuration.ConnectionStringSettingsCollection::IndexOf(System.Configuration.ConnectionStringSettings)
extern "C"  int32_t ConnectionStringSettingsCollection_IndexOf_m957352371 (ConnectionStringSettingsCollection_t2042791570 * __this, ConnectionStringSettings_t1154375560 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::Remove(System.Configuration.ConnectionStringSettings)
extern "C"  void ConnectionStringSettingsCollection_Remove_m2291616272 (ConnectionStringSettingsCollection_t2042791570 * __this, ConnectionStringSettings_t1154375560 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::Remove(System.String)
extern "C"  void ConnectionStringSettingsCollection_Remove_m2472193005 (ConnectionStringSettingsCollection_t2042791570 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::RemoveAt(System.Int32)
extern "C"  void ConnectionStringSettingsCollection_RemoveAt_m2270639559 (ConnectionStringSettingsCollection_t2042791570 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettingsCollection::BaseAdd(System.Int32,System.Configuration.ConfigurationElement)
extern "C"  void ConnectionStringSettingsCollection_BaseAdd_m923220387 (ConnectionStringSettingsCollection_t2042791570 * __this, int32_t ___index0, ConfigurationElement_t1776195828 * ___element1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
