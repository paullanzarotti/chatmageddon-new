﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.WP8BillingService
struct WP8BillingService_t394118855;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WP8Eventhook
struct  WP8Eventhook_t2441977752  : public MonoBehaviour_t1158329972
{
public:
	// Unibill.Impl.WP8BillingService WP8Eventhook::callback
	WP8BillingService_t394118855 * ___callback_2;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(WP8Eventhook_t2441977752, ___callback_2)); }
	inline WP8BillingService_t394118855 * get_callback_2() const { return ___callback_2; }
	inline WP8BillingService_t394118855 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(WP8BillingService_t394118855 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
