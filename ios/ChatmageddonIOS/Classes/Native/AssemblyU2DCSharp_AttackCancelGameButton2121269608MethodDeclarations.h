﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackCancelGameButton
struct AttackCancelGameButton_t2121269608;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackCancelGameButton::.ctor()
extern "C"  void AttackCancelGameButton__ctor_m942648565 (AttackCancelGameButton_t2121269608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackCancelGameButton::Start()
extern "C"  void AttackCancelGameButton_Start_m1671087925 (AttackCancelGameButton_t2121269608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackCancelGameButton::Update()
extern "C"  void AttackCancelGameButton_Update_m3472887574 (AttackCancelGameButton_t2121269608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
