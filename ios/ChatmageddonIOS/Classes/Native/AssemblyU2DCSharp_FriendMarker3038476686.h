﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// TweenAlpha
struct TweenAlpha_t2421518635;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;

#include "AssemblyU2DCSharp_RadarMarkerBlip1130528275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendMarker
struct  FriendMarker_t3038476686  : public RadarMarkerBlip_t1130528275
{
public:
	// Friend FriendMarker::friend
	Friend_t3555014108 * ___friend_10;
	// System.Boolean FriendMarker::grouped
	bool ___grouped_11;
	// TweenAlpha FriendMarker::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_12;
	// UnityEngine.GameObject FriendMarker::displayLabel
	GameObject_t1756533147 * ___displayLabel_13;
	// UnityEngine.Camera FriendMarker::nguiCamera
	Camera_t189460977 * ___nguiCamera_14;
	// UnityEngine.GameObject FriendMarker::uiroot
	GameObject_t1756533147 * ___uiroot_15;

public:
	inline static int32_t get_offset_of_friend_10() { return static_cast<int32_t>(offsetof(FriendMarker_t3038476686, ___friend_10)); }
	inline Friend_t3555014108 * get_friend_10() const { return ___friend_10; }
	inline Friend_t3555014108 ** get_address_of_friend_10() { return &___friend_10; }
	inline void set_friend_10(Friend_t3555014108 * value)
	{
		___friend_10 = value;
		Il2CppCodeGenWriteBarrier(&___friend_10, value);
	}

	inline static int32_t get_offset_of_grouped_11() { return static_cast<int32_t>(offsetof(FriendMarker_t3038476686, ___grouped_11)); }
	inline bool get_grouped_11() const { return ___grouped_11; }
	inline bool* get_address_of_grouped_11() { return &___grouped_11; }
	inline void set_grouped_11(bool value)
	{
		___grouped_11 = value;
	}

	inline static int32_t get_offset_of_alphaTween_12() { return static_cast<int32_t>(offsetof(FriendMarker_t3038476686, ___alphaTween_12)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_12() const { return ___alphaTween_12; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_12() { return &___alphaTween_12; }
	inline void set_alphaTween_12(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_12 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_12, value);
	}

	inline static int32_t get_offset_of_displayLabel_13() { return static_cast<int32_t>(offsetof(FriendMarker_t3038476686, ___displayLabel_13)); }
	inline GameObject_t1756533147 * get_displayLabel_13() const { return ___displayLabel_13; }
	inline GameObject_t1756533147 ** get_address_of_displayLabel_13() { return &___displayLabel_13; }
	inline void set_displayLabel_13(GameObject_t1756533147 * value)
	{
		___displayLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___displayLabel_13, value);
	}

	inline static int32_t get_offset_of_nguiCamera_14() { return static_cast<int32_t>(offsetof(FriendMarker_t3038476686, ___nguiCamera_14)); }
	inline Camera_t189460977 * get_nguiCamera_14() const { return ___nguiCamera_14; }
	inline Camera_t189460977 ** get_address_of_nguiCamera_14() { return &___nguiCamera_14; }
	inline void set_nguiCamera_14(Camera_t189460977 * value)
	{
		___nguiCamera_14 = value;
		Il2CppCodeGenWriteBarrier(&___nguiCamera_14, value);
	}

	inline static int32_t get_offset_of_uiroot_15() { return static_cast<int32_t>(offsetof(FriendMarker_t3038476686, ___uiroot_15)); }
	inline GameObject_t1756533147 * get_uiroot_15() const { return ___uiroot_15; }
	inline GameObject_t1756533147 ** get_address_of_uiroot_15() { return &___uiroot_15; }
	inline void set_uiroot_15(GameObject_t1756533147 * value)
	{
		___uiroot_15 = value;
		Il2CppCodeGenWriteBarrier(&___uiroot_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
