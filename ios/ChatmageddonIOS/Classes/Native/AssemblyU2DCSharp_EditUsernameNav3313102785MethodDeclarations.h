﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditUsernameNav
struct EditUsernameNav_t3313102785;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void EditUsernameNav::.ctor()
extern "C"  void EditUsernameNav__ctor_m3414930804 (EditUsernameNav_t3313102785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUsernameNav::Start()
extern "C"  void EditUsernameNav_Start_m4258697628 (EditUsernameNav_t3313102785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUsernameNav::UIClosing()
extern "C"  void EditUsernameNav_UIClosing_m3442507019 (EditUsernameNav_t3313102785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUsernameNav::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void EditUsernameNav_ScreenLoad_m4057274724 (EditUsernameNav_t3313102785 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUsernameNav::ScreenUnload(NavigationDirection)
extern "C"  void EditUsernameNav_ScreenUnload_m3141754036 (EditUsernameNav_t3313102785 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EditUsernameNav::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool EditUsernameNav_ValidateScreenNavigation_m3864617627 (EditUsernameNav_t3313102785 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EditUsernameNav::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * EditUsernameNav_SaveScreenContent_m3438340132 (EditUsernameNav_t3313102785 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
