﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumberNaviagteForwardButton
struct PhoneNumberNaviagteForwardButton_t3286637755;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumberNaviagteForwardButton::.ctor()
extern "C"  void PhoneNumberNaviagteForwardButton__ctor_m3872316482 (PhoneNumberNaviagteForwardButton_t3286637755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberNaviagteForwardButton::OnButtonClick()
extern "C"  void PhoneNumberNaviagteForwardButton_OnButtonClick_m4292329119 (PhoneNumberNaviagteForwardButton_t3286637755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberNaviagteForwardButton::SetButtonActive(System.Boolean)
extern "C"  void PhoneNumberNaviagteForwardButton_SetButtonActive_m1662006497 (PhoneNumberNaviagteForwardButton_t3286637755 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
