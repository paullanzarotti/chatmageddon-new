﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EdgeCollider2D
struct EdgeCollider2D_t216857133;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EdgeCollider2D::set_points(UnityEngine.Vector2[])
extern "C"  void EdgeCollider2D_set_points_m4024779628 (EdgeCollider2D_t216857133 * __this, Vector2U5BU5D_t686124026* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
