﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.FlyweightMapStorage/ByteBuffer
struct ByteBuffer_t1505679999;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Func`2<System.String,System.Int32>
struct Func_2_t235684172;

#include "AssemblyU2DCSharp_PhoneNumbers_AreaCodeMapStorageS3709015996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.FlyweightMapStorage
struct  FlyweightMapStorage_t1600303080  : public AreaCodeMapStorageStrategy_t3709015996
{
public:
	// System.Int32 PhoneNumbers.FlyweightMapStorage::prefixSizeInBytes
	int32_t ___prefixSizeInBytes_4;
	// System.Int32 PhoneNumbers.FlyweightMapStorage::descIndexSizeInBytes
	int32_t ___descIndexSizeInBytes_5;
	// PhoneNumbers.FlyweightMapStorage/ByteBuffer PhoneNumbers.FlyweightMapStorage::phoneNumberPrefixes
	ByteBuffer_t1505679999 * ___phoneNumberPrefixes_6;
	// PhoneNumbers.FlyweightMapStorage/ByteBuffer PhoneNumbers.FlyweightMapStorage::descriptionIndexes
	ByteBuffer_t1505679999 * ___descriptionIndexes_7;
	// System.String[] PhoneNumbers.FlyweightMapStorage::descriptionPool
	StringU5BU5D_t1642385972* ___descriptionPool_8;

public:
	inline static int32_t get_offset_of_prefixSizeInBytes_4() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080, ___prefixSizeInBytes_4)); }
	inline int32_t get_prefixSizeInBytes_4() const { return ___prefixSizeInBytes_4; }
	inline int32_t* get_address_of_prefixSizeInBytes_4() { return &___prefixSizeInBytes_4; }
	inline void set_prefixSizeInBytes_4(int32_t value)
	{
		___prefixSizeInBytes_4 = value;
	}

	inline static int32_t get_offset_of_descIndexSizeInBytes_5() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080, ___descIndexSizeInBytes_5)); }
	inline int32_t get_descIndexSizeInBytes_5() const { return ___descIndexSizeInBytes_5; }
	inline int32_t* get_address_of_descIndexSizeInBytes_5() { return &___descIndexSizeInBytes_5; }
	inline void set_descIndexSizeInBytes_5(int32_t value)
	{
		___descIndexSizeInBytes_5 = value;
	}

	inline static int32_t get_offset_of_phoneNumberPrefixes_6() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080, ___phoneNumberPrefixes_6)); }
	inline ByteBuffer_t1505679999 * get_phoneNumberPrefixes_6() const { return ___phoneNumberPrefixes_6; }
	inline ByteBuffer_t1505679999 ** get_address_of_phoneNumberPrefixes_6() { return &___phoneNumberPrefixes_6; }
	inline void set_phoneNumberPrefixes_6(ByteBuffer_t1505679999 * value)
	{
		___phoneNumberPrefixes_6 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberPrefixes_6, value);
	}

	inline static int32_t get_offset_of_descriptionIndexes_7() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080, ___descriptionIndexes_7)); }
	inline ByteBuffer_t1505679999 * get_descriptionIndexes_7() const { return ___descriptionIndexes_7; }
	inline ByteBuffer_t1505679999 ** get_address_of_descriptionIndexes_7() { return &___descriptionIndexes_7; }
	inline void set_descriptionIndexes_7(ByteBuffer_t1505679999 * value)
	{
		___descriptionIndexes_7 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionIndexes_7, value);
	}

	inline static int32_t get_offset_of_descriptionPool_8() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080, ___descriptionPool_8)); }
	inline StringU5BU5D_t1642385972* get_descriptionPool_8() const { return ___descriptionPool_8; }
	inline StringU5BU5D_t1642385972** get_address_of_descriptionPool_8() { return &___descriptionPool_8; }
	inline void set_descriptionPool_8(StringU5BU5D_t1642385972* value)
	{
		___descriptionPool_8 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionPool_8, value);
	}
};

struct FlyweightMapStorage_t1600303080_StaticFields
{
public:
	// System.Int32 PhoneNumbers.FlyweightMapStorage::SHORT_NUM_BYTES
	int32_t ___SHORT_NUM_BYTES_2;
	// System.Int32 PhoneNumbers.FlyweightMapStorage::INT_NUM_BYTES
	int32_t ___INT_NUM_BYTES_3;
	// System.Func`2<System.String,System.Int32> PhoneNumbers.FlyweightMapStorage::<>f__am$cache0
	Func_2_t235684172 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_SHORT_NUM_BYTES_2() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080_StaticFields, ___SHORT_NUM_BYTES_2)); }
	inline int32_t get_SHORT_NUM_BYTES_2() const { return ___SHORT_NUM_BYTES_2; }
	inline int32_t* get_address_of_SHORT_NUM_BYTES_2() { return &___SHORT_NUM_BYTES_2; }
	inline void set_SHORT_NUM_BYTES_2(int32_t value)
	{
		___SHORT_NUM_BYTES_2 = value;
	}

	inline static int32_t get_offset_of_INT_NUM_BYTES_3() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080_StaticFields, ___INT_NUM_BYTES_3)); }
	inline int32_t get_INT_NUM_BYTES_3() const { return ___INT_NUM_BYTES_3; }
	inline int32_t* get_address_of_INT_NUM_BYTES_3() { return &___INT_NUM_BYTES_3; }
	inline void set_INT_NUM_BYTES_3(int32_t value)
	{
		___INT_NUM_BYTES_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(FlyweightMapStorage_t1600303080_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Func_2_t235684172 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Func_2_t235684172 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Func_2_t235684172 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
