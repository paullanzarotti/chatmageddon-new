﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mine
struct Mine_t2729441277;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_MineStatus2219122381.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mine::.ctor(System.Collections.Hashtable)
extern "C"  void Mine__ctor_m414532838 (Mine_t2729441277 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mine::.ctor(Mine)
extern "C"  void Mine__ctor_m2409328395 (Mine_t2729441277 * __this, Mine_t2729441277 * ___existingItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MineStatus Mine::GetStatusFromString(System.String)
extern "C"  int32_t Mine_GetStatusFromString_m631083967 (Mine_t2729441277 * __this, String_t* ___statusString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
