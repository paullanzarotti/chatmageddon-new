﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<PostMessageToThread>c__AnonStorey6<System.Object>
struct U3CPostMessageToThreadU3Ec__AnonStorey6_t247608154;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<PostMessageToThread>c__AnonStorey6<System.Object>::.ctor()
extern "C"  void U3CPostMessageToThreadU3Ec__AnonStorey6__ctor_m2904415483_gshared (U3CPostMessageToThreadU3Ec__AnonStorey6_t247608154 * __this, const MethodInfo* method);
#define U3CPostMessageToThreadU3Ec__AnonStorey6__ctor_m2904415483(__this, method) ((  void (*) (U3CPostMessageToThreadU3Ec__AnonStorey6_t247608154 *, const MethodInfo*))U3CPostMessageToThreadU3Ec__AnonStorey6__ctor_m2904415483_gshared)(__this, method)
// System.Void ChatServer`1/<PostMessageToThread>c__AnonStorey6<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CPostMessageToThreadU3Ec__AnonStorey6_U3CU3Em__0_m3685831914_gshared (U3CPostMessageToThreadU3Ec__AnonStorey6_t247608154 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CPostMessageToThreadU3Ec__AnonStorey6_U3CU3Em__0_m3685831914(__this, ___request0, ___response1, method) ((  void (*) (U3CPostMessageToThreadU3Ec__AnonStorey6_t247608154 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CPostMessageToThreadU3Ec__AnonStorey6_U3CU3Em__0_m3685831914_gshared)(__this, ___request0, ___response1, method)
