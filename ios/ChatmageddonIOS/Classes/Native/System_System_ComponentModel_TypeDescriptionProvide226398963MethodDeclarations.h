﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptionProviderAttribute
struct TypeDescriptionProviderAttribute_t226398963;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.ComponentModel.TypeDescriptionProviderAttribute::.ctor(System.String)
extern "C"  void TypeDescriptionProviderAttribute__ctor_m1677445968 (TypeDescriptionProviderAttribute_t226398963 * __this, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeDescriptionProviderAttribute::.ctor(System.Type)
extern "C"  void TypeDescriptionProviderAttribute__ctor_m2425677555 (TypeDescriptionProviderAttribute_t226398963 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.TypeDescriptionProviderAttribute::get_TypeName()
extern "C"  String_t* TypeDescriptionProviderAttribute_get_TypeName_m3780506907 (TypeDescriptionProviderAttribute_t226398963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
