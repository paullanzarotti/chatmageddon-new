﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginCenterUI
struct LoginCenterUI_t890505802;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginCenterUI::.ctor()
extern "C"  void LoginCenterUI__ctor_m2468811871 (LoginCenterUI_t890505802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginCenterUI::SetBackgroundActive(System.Boolean)
extern "C"  void LoginCenterUI_SetBackgroundActive_m3399614538 (LoginCenterUI_t890505802 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
