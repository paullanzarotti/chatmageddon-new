﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MultiPhoneNumberILP
struct MultiPhoneNumberILP_t3109982769;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen952667143.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiPhoneNumberListItem
struct  MultiPhoneNumberListItem_t177127779  : public BaseInfiniteListItem_1_t952667143
{
public:
	// System.String MultiPhoneNumberListItem::phoneNumber
	String_t* ___phoneNumber_10;
	// MultiPhoneNumberILP MultiPhoneNumberListItem::listPopulator
	MultiPhoneNumberILP_t3109982769 * ___listPopulator_11;
	// UILabel MultiPhoneNumberListItem::numberLabel
	UILabel_t1795115428 * ___numberLabel_12;
	// UnityEngine.GameObject MultiPhoneNumberListItem::divider
	GameObject_t1756533147 * ___divider_13;
	// System.Boolean MultiPhoneNumberListItem::attackItem
	bool ___attackItem_14;
	// UIScrollView MultiPhoneNumberListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_15;
	// UIScrollView MultiPhoneNumberListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_16;
	// UnityEngine.Transform MultiPhoneNumberListItem::mTrans
	Transform_t3275118058 * ___mTrans_17;
	// UIScrollView MultiPhoneNumberListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_18;
	// System.Boolean MultiPhoneNumberListItem::mAutoFind
	bool ___mAutoFind_19;
	// System.Boolean MultiPhoneNumberListItem::mStarted
	bool ___mStarted_20;

public:
	inline static int32_t get_offset_of_phoneNumber_10() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___phoneNumber_10)); }
	inline String_t* get_phoneNumber_10() const { return ___phoneNumber_10; }
	inline String_t** get_address_of_phoneNumber_10() { return &___phoneNumber_10; }
	inline void set_phoneNumber_10(String_t* value)
	{
		___phoneNumber_10 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumber_10, value);
	}

	inline static int32_t get_offset_of_listPopulator_11() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___listPopulator_11)); }
	inline MultiPhoneNumberILP_t3109982769 * get_listPopulator_11() const { return ___listPopulator_11; }
	inline MultiPhoneNumberILP_t3109982769 ** get_address_of_listPopulator_11() { return &___listPopulator_11; }
	inline void set_listPopulator_11(MultiPhoneNumberILP_t3109982769 * value)
	{
		___listPopulator_11 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_11, value);
	}

	inline static int32_t get_offset_of_numberLabel_12() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___numberLabel_12)); }
	inline UILabel_t1795115428 * get_numberLabel_12() const { return ___numberLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_numberLabel_12() { return &___numberLabel_12; }
	inline void set_numberLabel_12(UILabel_t1795115428 * value)
	{
		___numberLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___numberLabel_12, value);
	}

	inline static int32_t get_offset_of_divider_13() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___divider_13)); }
	inline GameObject_t1756533147 * get_divider_13() const { return ___divider_13; }
	inline GameObject_t1756533147 ** get_address_of_divider_13() { return &___divider_13; }
	inline void set_divider_13(GameObject_t1756533147 * value)
	{
		___divider_13 = value;
		Il2CppCodeGenWriteBarrier(&___divider_13, value);
	}

	inline static int32_t get_offset_of_attackItem_14() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___attackItem_14)); }
	inline bool get_attackItem_14() const { return ___attackItem_14; }
	inline bool* get_address_of_attackItem_14() { return &___attackItem_14; }
	inline void set_attackItem_14(bool value)
	{
		___attackItem_14 = value;
	}

	inline static int32_t get_offset_of_scrollView_15() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___scrollView_15)); }
	inline UIScrollView_t3033954930 * get_scrollView_15() const { return ___scrollView_15; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_15() { return &___scrollView_15; }
	inline void set_scrollView_15(UIScrollView_t3033954930 * value)
	{
		___scrollView_15 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_15, value);
	}

	inline static int32_t get_offset_of_draggablePanel_16() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___draggablePanel_16)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_16() const { return ___draggablePanel_16; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_16() { return &___draggablePanel_16; }
	inline void set_draggablePanel_16(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_16 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_16, value);
	}

	inline static int32_t get_offset_of_mTrans_17() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___mTrans_17)); }
	inline Transform_t3275118058 * get_mTrans_17() const { return ___mTrans_17; }
	inline Transform_t3275118058 ** get_address_of_mTrans_17() { return &___mTrans_17; }
	inline void set_mTrans_17(Transform_t3275118058 * value)
	{
		___mTrans_17 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_17, value);
	}

	inline static int32_t get_offset_of_mScroll_18() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___mScroll_18)); }
	inline UIScrollView_t3033954930 * get_mScroll_18() const { return ___mScroll_18; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_18() { return &___mScroll_18; }
	inline void set_mScroll_18(UIScrollView_t3033954930 * value)
	{
		___mScroll_18 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_18, value);
	}

	inline static int32_t get_offset_of_mAutoFind_19() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___mAutoFind_19)); }
	inline bool get_mAutoFind_19() const { return ___mAutoFind_19; }
	inline bool* get_address_of_mAutoFind_19() { return &___mAutoFind_19; }
	inline void set_mAutoFind_19(bool value)
	{
		___mAutoFind_19 = value;
	}

	inline static int32_t get_offset_of_mStarted_20() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779, ___mStarted_20)); }
	inline bool get_mStarted_20() const { return ___mStarted_20; }
	inline bool* get_address_of_mStarted_20() { return &___mStarted_20; }
	inline void set_mStarted_20(bool value)
	{
		___mStarted_20 = value;
	}
};

struct MultiPhoneNumberListItem_t177127779_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> MultiPhoneNumberListItem::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_21() { return static_cast<int32_t>(offsetof(MultiPhoneNumberListItem_t177127779_StaticFields, ___U3CU3Ef__amU24cache0_21)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_21() const { return ___U3CU3Ef__amU24cache0_21; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_21() { return &___U3CU3Ef__amU24cache0_21; }
	inline void set_U3CU3Ef__amU24cache0_21(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
