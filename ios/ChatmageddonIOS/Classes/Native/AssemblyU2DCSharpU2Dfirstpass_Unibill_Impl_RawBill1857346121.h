﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Uniject.ILevelLoadListener
struct ILevelLoadListener_t3681507369;
// Unibill.Impl.IHTTPClient
struct IHTTPClient_t1282637506;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.RawBillingPlatformProvider
struct  RawBillingPlatformProvider_t1857346121  : public Il2CppObject
{
public:
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.RawBillingPlatformProvider::config
	UnibillConfiguration_t2915611853 * ___config_0;
	// UnityEngine.GameObject Unibill.Impl.RawBillingPlatformProvider::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// Uniject.ILevelLoadListener Unibill.Impl.RawBillingPlatformProvider::listener
	Il2CppObject * ___listener_2;
	// Unibill.Impl.IHTTPClient Unibill.Impl.RawBillingPlatformProvider::client
	Il2CppObject * ___client_3;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(RawBillingPlatformProvider_t1857346121, ___config_0)); }
	inline UnibillConfiguration_t2915611853 * get_config_0() const { return ___config_0; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(UnibillConfiguration_t2915611853 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier(&___config_0, value);
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(RawBillingPlatformProvider_t1857346121, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}

	inline static int32_t get_offset_of_listener_2() { return static_cast<int32_t>(offsetof(RawBillingPlatformProvider_t1857346121, ___listener_2)); }
	inline Il2CppObject * get_listener_2() const { return ___listener_2; }
	inline Il2CppObject ** get_address_of_listener_2() { return &___listener_2; }
	inline void set_listener_2(Il2CppObject * value)
	{
		___listener_2 = value;
		Il2CppCodeGenWriteBarrier(&___listener_2, value);
	}

	inline static int32_t get_offset_of_client_3() { return static_cast<int32_t>(offsetof(RawBillingPlatformProvider_t1857346121, ___client_3)); }
	inline Il2CppObject * get_client_3() const { return ___client_3; }
	inline Il2CppObject ** get_address_of_client_3() { return &___client_3; }
	inline void set_client_3(Il2CppObject * value)
	{
		___client_3 = value;
		Il2CppCodeGenWriteBarrier(&___client_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
