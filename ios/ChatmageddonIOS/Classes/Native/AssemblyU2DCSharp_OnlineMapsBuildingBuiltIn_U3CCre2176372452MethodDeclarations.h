﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1
struct U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1::.ctor()
extern "C"  void U3CCreateHouseRoofU3Ec__AnonStorey1__ctor_m1755987987 (U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1::<>m__0(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  U3CCreateHouseRoofU3Ec__AnonStorey1_U3CU3Em__0_m3877533884 (U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452 * __this, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
