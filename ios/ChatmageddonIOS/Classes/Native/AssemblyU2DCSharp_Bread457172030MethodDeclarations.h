﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bread
struct Bread_t457172030;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ToastType1578101939.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

// System.Void Bread::.ctor(ToastType,System.Boolean,System.String,SFX)
extern "C"  void Bread__ctor_m3783694850 (Bread_t457172030 * __this, int32_t ___type0, bool ___fadeShow1, String_t* ___message2, int32_t ___sfx3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
