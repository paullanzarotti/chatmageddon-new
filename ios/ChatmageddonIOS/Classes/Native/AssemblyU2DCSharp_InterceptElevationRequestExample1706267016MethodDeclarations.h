﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InterceptElevationRequestExample
struct InterceptElevationRequestExample_t1706267016;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void InterceptElevationRequestExample::.ctor()
extern "C"  void InterceptElevationRequestExample__ctor_m479175875 (InterceptElevationRequestExample_t1706267016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterceptElevationRequestExample::Start()
extern "C"  void InterceptElevationRequestExample_Start_m3252709575 (InterceptElevationRequestExample_t1706267016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterceptElevationRequestExample::OnGetElevation(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void InterceptElevationRequestExample_OnGetElevation_m2841228055 (InterceptElevationRequestExample_t1706267016 * __this, Vector2_t2243707579  ___topLeftCoords0, Vector2_t2243707579  ___bottomRightCoords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
