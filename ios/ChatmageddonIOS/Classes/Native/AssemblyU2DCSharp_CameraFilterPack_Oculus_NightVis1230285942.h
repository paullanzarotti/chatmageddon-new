﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Oculus_NightVision5
struct  CameraFilterPack_Oculus_NightVision5_t1230285942  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Oculus_NightVision5::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Oculus_NightVision5::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// System.Single CameraFilterPack_Oculus_NightVision5::FadeFX
	float ___FadeFX_4;
	// System.Single CameraFilterPack_Oculus_NightVision5::_Size
	float ____Size_5;
	// System.Single CameraFilterPack_Oculus_NightVision5::_Smooth
	float ____Smooth_6;
	// System.Single CameraFilterPack_Oculus_NightVision5::_Dist
	float ____Dist_7;
	// System.Single CameraFilterPack_Oculus_NightVision5::TimeX
	float ___TimeX_8;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_NightVision5::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_9;
	// UnityEngine.Material CameraFilterPack_Oculus_NightVision5::SCMaterial
	Material_t193706927 * ___SCMaterial_10;
	// System.Single[] CameraFilterPack_Oculus_NightVision5::Matrix9
	SingleU5BU5D_t577127397* ___Matrix9_11;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_FadeFX_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___FadeFX_4)); }
	inline float get_FadeFX_4() const { return ___FadeFX_4; }
	inline float* get_address_of_FadeFX_4() { return &___FadeFX_4; }
	inline void set_FadeFX_4(float value)
	{
		___FadeFX_4 = value;
	}

	inline static int32_t get_offset_of__Size_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ____Size_5)); }
	inline float get__Size_5() const { return ____Size_5; }
	inline float* get_address_of__Size_5() { return &____Size_5; }
	inline void set__Size_5(float value)
	{
		____Size_5 = value;
	}

	inline static int32_t get_offset_of__Smooth_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ____Smooth_6)); }
	inline float get__Smooth_6() const { return ____Smooth_6; }
	inline float* get_address_of__Smooth_6() { return &____Smooth_6; }
	inline void set__Smooth_6(float value)
	{
		____Smooth_6 = value;
	}

	inline static int32_t get_offset_of__Dist_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ____Dist_7)); }
	inline float get__Dist_7() const { return ____Dist_7; }
	inline float* get_address_of__Dist_7() { return &____Dist_7; }
	inline void set__Dist_7(float value)
	{
		____Dist_7 = value;
	}

	inline static int32_t get_offset_of_TimeX_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___TimeX_8)); }
	inline float get_TimeX_8() const { return ___TimeX_8; }
	inline float* get_address_of_TimeX_8() { return &___TimeX_8; }
	inline void set_TimeX_8(float value)
	{
		___TimeX_8 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___ScreenResolution_9)); }
	inline Vector4_t2243707581  get_ScreenResolution_9() const { return ___ScreenResolution_9; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_9() { return &___ScreenResolution_9; }
	inline void set_ScreenResolution_9(Vector4_t2243707581  value)
	{
		___ScreenResolution_9 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___SCMaterial_10)); }
	inline Material_t193706927 * get_SCMaterial_10() const { return ___SCMaterial_10; }
	inline Material_t193706927 ** get_address_of_SCMaterial_10() { return &___SCMaterial_10; }
	inline void set_SCMaterial_10(Material_t193706927 * value)
	{
		___SCMaterial_10 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_10, value);
	}

	inline static int32_t get_offset_of_Matrix9_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Oculus_NightVision5_t1230285942, ___Matrix9_11)); }
	inline SingleU5BU5D_t577127397* get_Matrix9_11() const { return ___Matrix9_11; }
	inline SingleU5BU5D_t577127397** get_address_of_Matrix9_11() { return &___Matrix9_11; }
	inline void set_Matrix9_11(SingleU5BU5D_t577127397* value)
	{
		___Matrix9_11 = value;
		Il2CppCodeGenWriteBarrier(&___Matrix9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
