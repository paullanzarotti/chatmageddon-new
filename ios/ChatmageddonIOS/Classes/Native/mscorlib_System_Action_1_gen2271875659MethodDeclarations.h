﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<UICamera/MouseOrTouch>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3835044065(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2271875659 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2192865289_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<UICamera/MouseOrTouch>::Invoke(T)
#define Action_1_Invoke_m67725961(__this, ___obj0, method) ((  void (*) (Action_1_t2271875659 *, MouseOrTouch_t2470076277 *, const MethodInfo*))Action_1_Invoke_m3960033342_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<UICamera/MouseOrTouch>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m3193527616(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2271875659 *, MouseOrTouch_t2470076277 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<UICamera/MouseOrTouch>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m542325867(__this, ___result0, method) ((  void (*) (Action_1_t2271875659 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
