﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Radial_Fast
struct CameraFilterPack_Blur_Radial_Fast_t3110653519;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Radial_Fast::.ctor()
extern "C"  void CameraFilterPack_Blur_Radial_Fast__ctor_m712570280 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Radial_Fast::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Radial_Fast_get_material_m690223539 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::Start()
extern "C"  void CameraFilterPack_Blur_Radial_Fast_Start_m300720788 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Radial_Fast_OnRenderImage_m906197092 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::OnValidate()
extern "C"  void CameraFilterPack_Blur_Radial_Fast_OnValidate_m3681510701 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::Update()
extern "C"  void CameraFilterPack_Blur_Radial_Fast_Update_m1419230043 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::OnDisable()
extern "C"  void CameraFilterPack_Blur_Radial_Fast_OnDisable_m2971856051 (CameraFilterPack_Blur_Radial_Fast_t3110653519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
