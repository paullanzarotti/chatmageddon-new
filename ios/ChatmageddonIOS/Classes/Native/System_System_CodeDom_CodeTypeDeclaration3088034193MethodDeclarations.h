﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeTypeDeclaration
struct CodeTypeDeclaration_t3088034193;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeTypeDeclaration::.ctor()
extern "C"  void CodeTypeDeclaration__ctor_m348967825 (CodeTypeDeclaration_t3088034193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
