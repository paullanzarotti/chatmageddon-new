﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.DownloadManager/<download>c__Iterator2
struct U3CdownloadU3Ec__Iterator2_t3409902454;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Unibill.Impl.DownloadManager/<download>c__Iterator2::.ctor()
extern "C"  void U3CdownloadU3Ec__Iterator2__ctor_m2012693969 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager/<download>c__Iterator2::MoveNext()
extern "C"  bool U3CdownloadU3Ec__Iterator2_MoveNext_m3651424363 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager/<download>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdownloadU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2824097159 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager/<download>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdownloadU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3523732127 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<download>c__Iterator2::Dispose()
extern "C"  void U3CdownloadU3Ec__Iterator2_Dispose_m3655392686 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<download>c__Iterator2::Reset()
extern "C"  void U3CdownloadU3Ec__Iterator2_Reset_m2736740652 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<download>c__Iterator2::<>__Finally0()
extern "C"  void U3CdownloadU3Ec__Iterator2_U3CU3E__Finally0_m1786438914 (U3CdownloadU3Ec__Iterator2_t3409902454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
