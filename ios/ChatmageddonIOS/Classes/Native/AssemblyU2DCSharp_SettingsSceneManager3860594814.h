﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen3239421166.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsSceneManager
struct  SettingsSceneManager_t3860594814  : public BaseSceneManager_1_t3239421166
{
public:
	// UnityEngine.GameObject SettingsSceneManager::SettingsCenterUI
	GameObject_t1756533147 * ___SettingsCenterUI_17;
	// UnityEngine.GameObject SettingsSceneManager::DeviceCameraUI
	GameObject_t1756533147 * ___DeviceCameraUI_18;
	// UnityEngine.GameObject SettingsSceneManager::DeviceShareUI
	GameObject_t1756533147 * ___DeviceShareUI_19;
	// UnityEngine.GameObject SettingsSceneManager::ErrorMessageUI
	GameObject_t1756533147 * ___ErrorMessageUI_20;

public:
	inline static int32_t get_offset_of_SettingsCenterUI_17() { return static_cast<int32_t>(offsetof(SettingsSceneManager_t3860594814, ___SettingsCenterUI_17)); }
	inline GameObject_t1756533147 * get_SettingsCenterUI_17() const { return ___SettingsCenterUI_17; }
	inline GameObject_t1756533147 ** get_address_of_SettingsCenterUI_17() { return &___SettingsCenterUI_17; }
	inline void set_SettingsCenterUI_17(GameObject_t1756533147 * value)
	{
		___SettingsCenterUI_17 = value;
		Il2CppCodeGenWriteBarrier(&___SettingsCenterUI_17, value);
	}

	inline static int32_t get_offset_of_DeviceCameraUI_18() { return static_cast<int32_t>(offsetof(SettingsSceneManager_t3860594814, ___DeviceCameraUI_18)); }
	inline GameObject_t1756533147 * get_DeviceCameraUI_18() const { return ___DeviceCameraUI_18; }
	inline GameObject_t1756533147 ** get_address_of_DeviceCameraUI_18() { return &___DeviceCameraUI_18; }
	inline void set_DeviceCameraUI_18(GameObject_t1756533147 * value)
	{
		___DeviceCameraUI_18 = value;
		Il2CppCodeGenWriteBarrier(&___DeviceCameraUI_18, value);
	}

	inline static int32_t get_offset_of_DeviceShareUI_19() { return static_cast<int32_t>(offsetof(SettingsSceneManager_t3860594814, ___DeviceShareUI_19)); }
	inline GameObject_t1756533147 * get_DeviceShareUI_19() const { return ___DeviceShareUI_19; }
	inline GameObject_t1756533147 ** get_address_of_DeviceShareUI_19() { return &___DeviceShareUI_19; }
	inline void set_DeviceShareUI_19(GameObject_t1756533147 * value)
	{
		___DeviceShareUI_19 = value;
		Il2CppCodeGenWriteBarrier(&___DeviceShareUI_19, value);
	}

	inline static int32_t get_offset_of_ErrorMessageUI_20() { return static_cast<int32_t>(offsetof(SettingsSceneManager_t3860594814, ___ErrorMessageUI_20)); }
	inline GameObject_t1756533147 * get_ErrorMessageUI_20() const { return ___ErrorMessageUI_20; }
	inline GameObject_t1756533147 ** get_address_of_ErrorMessageUI_20() { return &___ErrorMessageUI_20; }
	inline void set_ErrorMessageUI_20(GameObject_t1756533147 * value)
	{
		___ErrorMessageUI_20 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorMessageUI_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
