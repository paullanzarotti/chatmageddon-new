﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryAndroidManager
struct FlurryAndroidManager_t1343776350;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FlurryAndroidManager::.cctor()
extern "C"  void FlurryAndroidManager__cctor_m1135728258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::.ctor()
extern "C"  void FlurryAndroidManager__ctor_m1079853763 (FlurryAndroidManager_t1343776350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_adAvailableForSpaceEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_adAvailableForSpaceEvent_m2320774472 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_adAvailableForSpaceEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_adAvailableForSpaceEvent_m1497384597 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_adNotAvailableForSpaceEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_adNotAvailableForSpaceEvent_m3793365883 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_adNotAvailableForSpaceEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_adNotAvailableForSpaceEvent_m1999750188 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_onAdClosedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_onAdClosedEvent_m1199989127 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_onAdClosedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_onAdClosedEvent_m1246234690 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_onApplicationExitEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_onApplicationExitEvent_m2464579130 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_onApplicationExitEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_onApplicationExitEvent_m1169400301 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_onRenderFailedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_onRenderFailedEvent_m1940321613 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_onRenderFailedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_onRenderFailedEvent_m3696084718 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_spaceDidFailToReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_spaceDidFailToReceiveAdEvent_m2542327899 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_spaceDidFailToReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_spaceDidFailToReceiveAdEvent_m1052088530 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_spaceDidReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_spaceDidReceiveAdEvent_m2187891578 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_spaceDidReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_spaceDidReceiveAdEvent_m3370336801 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_onAdClickedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_onAdClickedEvent_m1550502634 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_onAdClickedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_onAdClickedEvent_m4251978329 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_onAdOpenedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_onAdOpenedEvent_m4207893454 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_onAdOpenedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_onAdOpenedEvent_m3172378923 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::add_onVideoCompletedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_add_onVideoCompletedEvent_m3440856754 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::remove_onVideoCompletedEvent(System.Action`1<System.String>)
extern "C"  void FlurryAndroidManager_remove_onVideoCompletedEvent_m1545268351 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::adAvailableForSpace(System.String)
extern "C"  void FlurryAndroidManager_adAvailableForSpace_m4031392046 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::adNotAvailableForSpace(System.String)
extern "C"  void FlurryAndroidManager_adNotAvailableForSpace_m2708768067 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::onAdClosed(System.String)
extern "C"  void FlurryAndroidManager_onAdClosed_m3644860215 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::onApplicationExit(System.String)
extern "C"  void FlurryAndroidManager_onApplicationExit_m3217957684 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::onRenderFailed(System.String)
extern "C"  void FlurryAndroidManager_onRenderFailed_m230439981 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::spaceDidFailToReceiveAd(System.String)
extern "C"  void FlurryAndroidManager_spaceDidFailToReceiveAd_m3595682675 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::spaceDidReceiveAd(System.String)
extern "C"  void FlurryAndroidManager_spaceDidReceiveAd_m1958036708 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::onAdClicked(System.String)
extern "C"  void FlurryAndroidManager_onAdClicked_m705406460 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::onAdOpened(System.String)
extern "C"  void FlurryAndroidManager_onAdOpened_m2718345236 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAndroidManager::onVideoCompleted(System.String)
extern "C"  void FlurryAndroidManager_onVideoCompleted_m2934451048 (FlurryAndroidManager_t1343776350 * __this, String_t* ___adSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
