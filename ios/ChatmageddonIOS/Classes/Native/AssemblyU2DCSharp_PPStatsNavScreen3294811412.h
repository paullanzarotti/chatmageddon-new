﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerProfileModal
struct PlayerProfileModal_t3260625137;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PPStatsNavScreen
struct  PPStatsNavScreen_t3294811412  : public NavigationScreen_t2333230110
{
public:
	// PlayerProfileModal PPStatsNavScreen::modal
	PlayerProfileModal_t3260625137 * ___modal_3;
	// UISprite PPStatsNavScreen::statsTopDivider
	UISprite_t603616735 * ___statsTopDivider_4;
	// UISprite PPStatsNavScreen::statsBotDivider
	UISprite_t603616735 * ___statsBotDivider_5;
	// UILabel PPStatsNavScreen::firedValueLabel
	UILabel_t1795115428 * ___firedValueLabel_6;
	// UILabel PPStatsNavScreen::successRateValueLabel
	UILabel_t1795115428 * ___successRateValueLabel_7;
	// UILabel PPStatsNavScreen::attackHitsValueLabel
	UILabel_t1795115428 * ___attackHitsValueLabel_8;
	// UILabel PPStatsNavScreen::attacksValueLabel
	UILabel_t1795115428 * ___attacksValueLabel_9;
	// UILabel PPStatsNavScreen::hitByValueLabel
	UILabel_t1795115428 * ___hitByValueLabel_10;
	// UILabel PPStatsNavScreen::defencePercentageLabel
	UILabel_t1795115428 * ___defencePercentageLabel_11;
	// UILabel PPStatsNavScreen::laidValueLabel
	UILabel_t1795115428 * ___laidValueLabel_12;
	// UILabel PPStatsNavScreen::triggerredValueLabel
	UILabel_t1795115428 * ___triggerredValueLabel_13;
	// UILabel PPStatsNavScreen::defusedValueLabel
	UILabel_t1795115428 * ___defusedValueLabel_14;
	// System.Boolean PPStatsNavScreen::UIclosing
	bool ___UIclosing_15;

public:
	inline static int32_t get_offset_of_modal_3() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___modal_3)); }
	inline PlayerProfileModal_t3260625137 * get_modal_3() const { return ___modal_3; }
	inline PlayerProfileModal_t3260625137 ** get_address_of_modal_3() { return &___modal_3; }
	inline void set_modal_3(PlayerProfileModal_t3260625137 * value)
	{
		___modal_3 = value;
		Il2CppCodeGenWriteBarrier(&___modal_3, value);
	}

	inline static int32_t get_offset_of_statsTopDivider_4() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___statsTopDivider_4)); }
	inline UISprite_t603616735 * get_statsTopDivider_4() const { return ___statsTopDivider_4; }
	inline UISprite_t603616735 ** get_address_of_statsTopDivider_4() { return &___statsTopDivider_4; }
	inline void set_statsTopDivider_4(UISprite_t603616735 * value)
	{
		___statsTopDivider_4 = value;
		Il2CppCodeGenWriteBarrier(&___statsTopDivider_4, value);
	}

	inline static int32_t get_offset_of_statsBotDivider_5() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___statsBotDivider_5)); }
	inline UISprite_t603616735 * get_statsBotDivider_5() const { return ___statsBotDivider_5; }
	inline UISprite_t603616735 ** get_address_of_statsBotDivider_5() { return &___statsBotDivider_5; }
	inline void set_statsBotDivider_5(UISprite_t603616735 * value)
	{
		___statsBotDivider_5 = value;
		Il2CppCodeGenWriteBarrier(&___statsBotDivider_5, value);
	}

	inline static int32_t get_offset_of_firedValueLabel_6() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___firedValueLabel_6)); }
	inline UILabel_t1795115428 * get_firedValueLabel_6() const { return ___firedValueLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_firedValueLabel_6() { return &___firedValueLabel_6; }
	inline void set_firedValueLabel_6(UILabel_t1795115428 * value)
	{
		___firedValueLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___firedValueLabel_6, value);
	}

	inline static int32_t get_offset_of_successRateValueLabel_7() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___successRateValueLabel_7)); }
	inline UILabel_t1795115428 * get_successRateValueLabel_7() const { return ___successRateValueLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_successRateValueLabel_7() { return &___successRateValueLabel_7; }
	inline void set_successRateValueLabel_7(UILabel_t1795115428 * value)
	{
		___successRateValueLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___successRateValueLabel_7, value);
	}

	inline static int32_t get_offset_of_attackHitsValueLabel_8() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___attackHitsValueLabel_8)); }
	inline UILabel_t1795115428 * get_attackHitsValueLabel_8() const { return ___attackHitsValueLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_attackHitsValueLabel_8() { return &___attackHitsValueLabel_8; }
	inline void set_attackHitsValueLabel_8(UILabel_t1795115428 * value)
	{
		___attackHitsValueLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___attackHitsValueLabel_8, value);
	}

	inline static int32_t get_offset_of_attacksValueLabel_9() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___attacksValueLabel_9)); }
	inline UILabel_t1795115428 * get_attacksValueLabel_9() const { return ___attacksValueLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_attacksValueLabel_9() { return &___attacksValueLabel_9; }
	inline void set_attacksValueLabel_9(UILabel_t1795115428 * value)
	{
		___attacksValueLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___attacksValueLabel_9, value);
	}

	inline static int32_t get_offset_of_hitByValueLabel_10() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___hitByValueLabel_10)); }
	inline UILabel_t1795115428 * get_hitByValueLabel_10() const { return ___hitByValueLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_hitByValueLabel_10() { return &___hitByValueLabel_10; }
	inline void set_hitByValueLabel_10(UILabel_t1795115428 * value)
	{
		___hitByValueLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___hitByValueLabel_10, value);
	}

	inline static int32_t get_offset_of_defencePercentageLabel_11() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___defencePercentageLabel_11)); }
	inline UILabel_t1795115428 * get_defencePercentageLabel_11() const { return ___defencePercentageLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_defencePercentageLabel_11() { return &___defencePercentageLabel_11; }
	inline void set_defencePercentageLabel_11(UILabel_t1795115428 * value)
	{
		___defencePercentageLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___defencePercentageLabel_11, value);
	}

	inline static int32_t get_offset_of_laidValueLabel_12() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___laidValueLabel_12)); }
	inline UILabel_t1795115428 * get_laidValueLabel_12() const { return ___laidValueLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_laidValueLabel_12() { return &___laidValueLabel_12; }
	inline void set_laidValueLabel_12(UILabel_t1795115428 * value)
	{
		___laidValueLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___laidValueLabel_12, value);
	}

	inline static int32_t get_offset_of_triggerredValueLabel_13() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___triggerredValueLabel_13)); }
	inline UILabel_t1795115428 * get_triggerredValueLabel_13() const { return ___triggerredValueLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_triggerredValueLabel_13() { return &___triggerredValueLabel_13; }
	inline void set_triggerredValueLabel_13(UILabel_t1795115428 * value)
	{
		___triggerredValueLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___triggerredValueLabel_13, value);
	}

	inline static int32_t get_offset_of_defusedValueLabel_14() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___defusedValueLabel_14)); }
	inline UILabel_t1795115428 * get_defusedValueLabel_14() const { return ___defusedValueLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_defusedValueLabel_14() { return &___defusedValueLabel_14; }
	inline void set_defusedValueLabel_14(UILabel_t1795115428 * value)
	{
		___defusedValueLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___defusedValueLabel_14, value);
	}

	inline static int32_t get_offset_of_UIclosing_15() { return static_cast<int32_t>(offsetof(PPStatsNavScreen_t3294811412, ___UIclosing_15)); }
	inline bool get_UIclosing_15() const { return ___UIclosing_15; }
	inline bool* get_address_of_UIclosing_15() { return &___UIclosing_15; }
	inline void set_UIclosing_15(bool value)
	{
		___UIclosing_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
