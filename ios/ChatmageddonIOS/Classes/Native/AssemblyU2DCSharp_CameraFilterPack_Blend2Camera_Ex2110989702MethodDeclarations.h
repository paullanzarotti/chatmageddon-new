﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Exclusion
struct CameraFilterPack_Blend2Camera_Exclusion_t2110989702;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Exclusion::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion__ctor_m1937049585 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Exclusion::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Exclusion_get_material_m608885838 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion_Start_m1183867105 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion_OnRenderImage_m126713201 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion_OnValidate_m3911600698 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion_Update_m2845753572 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion_OnEnable_m3252179297 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Exclusion_OnDisable_m648270314 (CameraFilterPack_Blend2Camera_Exclusion_t2110989702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
