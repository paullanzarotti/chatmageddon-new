﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>
struct ReadOnlyCollection_1_t1312443546;
// System.Collections.Generic.IList`1<ProfileNavScreen>
struct IList_1_t1667598455;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ProfileNavScreen[]
struct ProfileNavScreenU5BU5D_t3517801995;
// System.Collections.Generic.IEnumerator`1<ProfileNavScreen>
struct IEnumerator_1_t2897148977;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2245537154_gshared (ReadOnlyCollection_1_t1312443546 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2245537154(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2245537154_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2253878618_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2253878618(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2253878618_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m894395542_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m894395542(__this, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m894395542_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1367138495_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1367138495(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1367138495_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3327382039_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3327382039(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3327382039_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m502100563_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m502100563(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m502100563_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3354112023_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3354112023(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3354112023_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1234657864_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1234657864(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1234657864_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m249400944_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m249400944(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m249400944_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m729011753_gshared (ReadOnlyCollection_1_t1312443546 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m729011753(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m729011753_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3450982456_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3450982456(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3450982456_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1481143553_gshared (ReadOnlyCollection_1_t1312443546 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1481143553(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1312443546 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1481143553_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m456459273_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m456459273(__this, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m456459273_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2086730361_gshared (ReadOnlyCollection_1_t1312443546 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2086730361(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2086730361_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m926106955_gshared (ReadOnlyCollection_1_t1312443546 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m926106955(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1312443546 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m926106955_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2023270114_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2023270114(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2023270114_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3952506072_gshared (ReadOnlyCollection_1_t1312443546 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3952506072(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3952506072_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4084264400_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4084264400(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4084264400_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1481735893_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1481735893(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1481735893_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2755458493_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2755458493(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2755458493_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m625005610_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m625005610(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m625005610_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m713171529_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m713171529(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m713171529_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3334811984_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3334811984(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3334811984_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941040955_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941040955(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941040955_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3558311296_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3558311296(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3558311296_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3212150270_gshared (ReadOnlyCollection_1_t1312443546 * __this, ProfileNavScreenU5BU5D_t3517801995* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3212150270(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1312443546 *, ProfileNavScreenU5BU5D_t3517801995*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3212150270_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2571144329_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2571144329(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2571144329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2562174720_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2562174720(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2562174720_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1757447921_gshared (ReadOnlyCollection_1_t1312443546 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1757447921(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1312443546 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1757447921_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m2416579147_gshared (ReadOnlyCollection_1_t1312443546 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2416579147(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1312443546 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2416579147_gshared)(__this, ___index0, method)
