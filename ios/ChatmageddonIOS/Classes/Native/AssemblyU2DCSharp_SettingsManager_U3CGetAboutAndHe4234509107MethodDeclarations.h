﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0
struct U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107;
// System.Collections.Generic.List`1<FAQ>
struct List_1_t2207118780;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0::.ctor()
extern "C"  void U3CGetAboutAndHelpInformationU3Ec__AnonStorey0__ctor_m1873124138 (U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0::<>m__0(System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String)
extern "C"  void U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_U3CU3Em__0_m1465058238 (U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107 * __this, bool ___success0, List_1_t2207118780 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0::<>m__1(System.Boolean,System.String,System.String)
extern "C"  void U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_U3CU3Em__1_m4282069799 (U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107 * __this, bool ___success0, String_t* ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
