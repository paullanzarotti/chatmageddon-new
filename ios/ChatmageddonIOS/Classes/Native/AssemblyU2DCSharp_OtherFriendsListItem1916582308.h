﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OtherFriend
struct OtherFriend_t3976068630;
// OtherFriendsILP
struct OtherFriendsILP_t671720694;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen2692121672.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherFriendsListItem
struct  OtherFriendsListItem_t1916582308  : public BaseInfiniteListItem_1_t2692121672
{
public:
	// OtherFriend OtherFriendsListItem::oFriend
	OtherFriend_t3976068630 * ___oFriend_10;
	// OtherFriendsILP OtherFriendsListItem::listPopulator
	OtherFriendsILP_t671720694 * ___listPopulator_11;
	// UILabel OtherFriendsListItem::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_12;
	// UILabel OtherFriendsListItem::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_13;
	// UnityEngine.GameObject OtherFriendsListItem::inviteButton
	GameObject_t1756533147 * ___inviteButton_14;
	// System.Boolean OtherFriendsListItem::attackItem
	bool ___attackItem_15;
	// UIScrollView OtherFriendsListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_16;
	// UIScrollView OtherFriendsListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_17;
	// UnityEngine.Transform OtherFriendsListItem::mTrans
	Transform_t3275118058 * ___mTrans_18;
	// UIScrollView OtherFriendsListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_19;
	// System.Boolean OtherFriendsListItem::mAutoFind
	bool ___mAutoFind_20;
	// System.Boolean OtherFriendsListItem::mStarted
	bool ___mStarted_21;

public:
	inline static int32_t get_offset_of_oFriend_10() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___oFriend_10)); }
	inline OtherFriend_t3976068630 * get_oFriend_10() const { return ___oFriend_10; }
	inline OtherFriend_t3976068630 ** get_address_of_oFriend_10() { return &___oFriend_10; }
	inline void set_oFriend_10(OtherFriend_t3976068630 * value)
	{
		___oFriend_10 = value;
		Il2CppCodeGenWriteBarrier(&___oFriend_10, value);
	}

	inline static int32_t get_offset_of_listPopulator_11() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___listPopulator_11)); }
	inline OtherFriendsILP_t671720694 * get_listPopulator_11() const { return ___listPopulator_11; }
	inline OtherFriendsILP_t671720694 ** get_address_of_listPopulator_11() { return &___listPopulator_11; }
	inline void set_listPopulator_11(OtherFriendsILP_t671720694 * value)
	{
		___listPopulator_11 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_11, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_12() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___firstNameLabel_12)); }
	inline UILabel_t1795115428 * get_firstNameLabel_12() const { return ___firstNameLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_12() { return &___firstNameLabel_12; }
	inline void set_firstNameLabel_12(UILabel_t1795115428 * value)
	{
		___firstNameLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_12, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_13() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___lastNameLabel_13)); }
	inline UILabel_t1795115428 * get_lastNameLabel_13() const { return ___lastNameLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_13() { return &___lastNameLabel_13; }
	inline void set_lastNameLabel_13(UILabel_t1795115428 * value)
	{
		___lastNameLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_13, value);
	}

	inline static int32_t get_offset_of_inviteButton_14() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___inviteButton_14)); }
	inline GameObject_t1756533147 * get_inviteButton_14() const { return ___inviteButton_14; }
	inline GameObject_t1756533147 ** get_address_of_inviteButton_14() { return &___inviteButton_14; }
	inline void set_inviteButton_14(GameObject_t1756533147 * value)
	{
		___inviteButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___inviteButton_14, value);
	}

	inline static int32_t get_offset_of_attackItem_15() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___attackItem_15)); }
	inline bool get_attackItem_15() const { return ___attackItem_15; }
	inline bool* get_address_of_attackItem_15() { return &___attackItem_15; }
	inline void set_attackItem_15(bool value)
	{
		___attackItem_15 = value;
	}

	inline static int32_t get_offset_of_scrollView_16() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___scrollView_16)); }
	inline UIScrollView_t3033954930 * get_scrollView_16() const { return ___scrollView_16; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_16() { return &___scrollView_16; }
	inline void set_scrollView_16(UIScrollView_t3033954930 * value)
	{
		___scrollView_16 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_16, value);
	}

	inline static int32_t get_offset_of_draggablePanel_17() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___draggablePanel_17)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_17() const { return ___draggablePanel_17; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_17() { return &___draggablePanel_17; }
	inline void set_draggablePanel_17(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_17, value);
	}

	inline static int32_t get_offset_of_mTrans_18() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___mTrans_18)); }
	inline Transform_t3275118058 * get_mTrans_18() const { return ___mTrans_18; }
	inline Transform_t3275118058 ** get_address_of_mTrans_18() { return &___mTrans_18; }
	inline void set_mTrans_18(Transform_t3275118058 * value)
	{
		___mTrans_18 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_18, value);
	}

	inline static int32_t get_offset_of_mScroll_19() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___mScroll_19)); }
	inline UIScrollView_t3033954930 * get_mScroll_19() const { return ___mScroll_19; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_19() { return &___mScroll_19; }
	inline void set_mScroll_19(UIScrollView_t3033954930 * value)
	{
		___mScroll_19 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_19, value);
	}

	inline static int32_t get_offset_of_mAutoFind_20() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___mAutoFind_20)); }
	inline bool get_mAutoFind_20() const { return ___mAutoFind_20; }
	inline bool* get_address_of_mAutoFind_20() { return &___mAutoFind_20; }
	inline void set_mAutoFind_20(bool value)
	{
		___mAutoFind_20 = value;
	}

	inline static int32_t get_offset_of_mStarted_21() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308, ___mStarted_21)); }
	inline bool get_mStarted_21() const { return ___mStarted_21; }
	inline bool* get_address_of_mStarted_21() { return &___mStarted_21; }
	inline void set_mStarted_21(bool value)
	{
		___mStarted_21 = value;
	}
};

struct OtherFriendsListItem_t1916582308_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> OtherFriendsListItem::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(OtherFriendsListItem_t1916582308_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
