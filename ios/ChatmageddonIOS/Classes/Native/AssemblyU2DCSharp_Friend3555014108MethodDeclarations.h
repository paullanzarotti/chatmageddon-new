﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Friend
struct Friend_t3555014108;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void Friend::.ctor(System.Collections.Hashtable)
extern "C"  void Friend__ctor_m1640256369 (Friend_t3555014108 * __this, Hashtable_t909839986 * ___friendHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Friend::AddFriendRequest(System.Collections.Hashtable)
extern "C"  void Friend_AddFriendRequest_m210257863 (Friend_t3555014108 * __this, Hashtable_t909839986 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Friend::DeleteFriendRequest()
extern "C"  void Friend_DeleteFriendRequest_m2571495599 (Friend_t3555014108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Friend::GetFriendRequestID()
extern "C"  String_t* Friend_GetFriendRequestID_m996243022 (Friend_t3555014108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Friend::SetGlobalFriend(System.Boolean)
extern "C"  void Friend_SetGlobalFriend_m3265428825 (Friend_t3555014108 * __this, bool ___global0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
