﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CompilerParameters
struct CompilerParameters_t2983628483;
// System.String
struct String_t;
// System.Security.Policy.Evidence
struct Evidence_t1407710183;
// System.Collections.Specialized.StringCollection
struct StringCollection_t352985975;
// System.CodeDom.Compiler.TempFileCollection
struct TempFileCollection_t3377240462;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.CodeDom.Compiler.CompilerParameters::.ctor()
extern "C"  void CompilerParameters__ctor_m265484919 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.CompilerParameters::get_CompilerOptions()
extern "C"  String_t* CompilerParameters_get_CompilerOptions_m3089767995 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.CodeDom.Compiler.CompilerParameters::get_Evidence()
extern "C"  Evidence_t1407710183 * CompilerParameters_get_Evidence_m1852897499 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.CompilerParameters::get_GenerateExecutable()
extern "C"  bool CompilerParameters_get_GenerateExecutable_m2795696220 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerParameters::set_GenerateExecutable(System.Boolean)
extern "C"  void CompilerParameters_set_GenerateExecutable_m2999893008 (CompilerParameters_t2983628483 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.CompilerParameters::get_GenerateInMemory()
extern "C"  bool CompilerParameters_get_GenerateInMemory_m2179230456 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerParameters::set_GenerateInMemory(System.Boolean)
extern "C"  void CompilerParameters_set_GenerateInMemory_m1795633964 (CompilerParameters_t2983628483 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.CompilerParameters::get_IncludeDebugInformation()
extern "C"  bool CompilerParameters_get_IncludeDebugInformation_m520661148 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerParameters::set_IncludeDebugInformation(System.Boolean)
extern "C"  void CompilerParameters_set_IncludeDebugInformation_m914006890 (CompilerParameters_t2983628483 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.CompilerParameters::get_OutputAssembly()
extern "C"  String_t* CompilerParameters_get_OutputAssembly_m2118335209 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerParameters::set_OutputAssembly(System.String)
extern "C"  void CompilerParameters_set_OutputAssembly_m1019408780 (CompilerParameters_t2983628483 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.StringCollection System.CodeDom.Compiler.CompilerParameters::get_ReferencedAssemblies()
extern "C"  StringCollection_t352985975 * CompilerParameters_get_ReferencedAssemblies_m1699585432 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.TempFileCollection System.CodeDom.Compiler.CompilerParameters::get_TempFiles()
extern "C"  TempFileCollection_t3377240462 * CompilerParameters_get_TempFiles_m4222867247 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.CompilerParameters::get_TreatWarningsAsErrors()
extern "C"  bool CompilerParameters_get_TreatWarningsAsErrors_m2629738875 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.CodeDom.Compiler.CompilerParameters::get_WarningLevel()
extern "C"  int32_t CompilerParameters_get_WarningLevel_m1173027931 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.CompilerParameters::get_Win32Resource()
extern "C"  String_t* CompilerParameters_get_Win32Resource_m1919949127 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.StringCollection System.CodeDom.Compiler.CompilerParameters::get_EmbeddedResources()
extern "C"  StringCollection_t352985975 * CompilerParameters_get_EmbeddedResources_m1059721363 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.StringCollection System.CodeDom.Compiler.CompilerParameters::get_LinkedResources()
extern "C"  StringCollection_t352985975 * CompilerParameters_get_LinkedResources_m3789868678 (CompilerParameters_t2983628483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
