﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<ChatSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m2263197923(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m2894818392(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::Start()
#define BaseSceneManager_1_Start_m4053732199(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m3217314983(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m4273325615(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m4066198281(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<ChatSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m650987873(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m255543766(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<ChatSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m3272994837(__this, method) ((  void (*) (BaseSceneManager_1_t4229148245 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
