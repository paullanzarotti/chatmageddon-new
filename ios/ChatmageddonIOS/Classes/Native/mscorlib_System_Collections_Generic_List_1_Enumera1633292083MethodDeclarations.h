﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mine>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1075168946(__this, ___l0, method) ((  void (*) (Enumerator_t1633292083 *, List_1_t2098562409 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mine>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m165949408(__this, method) ((  void (*) (Enumerator_t1633292083 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mine>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1731133302(__this, method) ((  Il2CppObject * (*) (Enumerator_t1633292083 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mine>::Dispose()
#define Enumerator_Dispose_m663963913(__this, method) ((  void (*) (Enumerator_t1633292083 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mine>::VerifyState()
#define Enumerator_VerifyState_m943144732(__this, method) ((  void (*) (Enumerator_t1633292083 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mine>::MoveNext()
#define Enumerator_MoveNext_m3453165228(__this, method) ((  bool (*) (Enumerator_t1633292083 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mine>::get_Current()
#define Enumerator_get_Current_m2117587097(__this, method) ((  Mine_t2729441277 * (*) (Enumerator_t1633292083 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
