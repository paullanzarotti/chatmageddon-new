﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateProfileImage>c__AnonStoreyD
struct U3CUpdateProfileImageU3Ec__AnonStoreyD_t2030450453;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateProfileImage>c__AnonStoreyD::.ctor()
extern "C"  void U3CUpdateProfileImageU3Ec__AnonStoreyD__ctor_m2851616290 (U3CUpdateProfileImageU3Ec__AnonStoreyD_t2030450453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateProfileImage>c__AnonStoreyD::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateProfileImageU3Ec__AnonStoreyD_U3CU3Em__0_m1996491625 (U3CUpdateProfileImageU3Ec__AnonStoreyD_t2030450453 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
