﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIToggledObjects3646139686.h"
#include "AssemblyU2DCSharp_UIWidgetContainer701016325.h"
#include "AssemblyU2DCSharp_UIWrapContent1931723019.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2948699028.h"
#include "AssemblyU2DCSharp_ActiveAnimation4137610604.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Trigger3949147154.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction1874854737.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition2151086590.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableCondition122284263.h"
#include "AssemblyU2DCSharp_BMFont421884312.h"
#include "AssemblyU2DCSharp_BMGlyph3903496831.h"
#include "AssemblyU2DCSharp_BMSymbol1865486779.h"
#include "AssemblyU2DCSharp_ByteReader2754005953.h"
#include "AssemblyU2DCSharp_EventDelegate3496309181.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter3376276275.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback2100910411.h"
#include "AssemblyU2DCSharp_Localization3725902693.h"
#include "AssemblyU2DCSharp_Localization_LoadFunction1013268478.h"
#include "AssemblyU2DCSharp_Localization_OnLocalizeNotificat3819587589.h"
#include "AssemblyU2DCSharp_NGUIDebug1735550160.h"
#include "AssemblyU2DCSharp_NGUIMath221371675.h"
#include "AssemblyU2DCSharp_NGUIText2042929214.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3620437664.h"
#include "AssemblyU2DCSharp_NGUIText_SymbolStyle2170314704.h"
#include "AssemblyU2DCSharp_NGUIText_GlyphInfo3590955929.h"
#include "AssemblyU2DCSharp_NGUITools2004302824.h"
#include "AssemblyU2DCSharp_PropertyBinding3567692958.h"
#include "AssemblyU2DCSharp_PropertyBinding_UpdateCondition2438470829.h"
#include "AssemblyU2DCSharp_PropertyBinding_Direction2332796344.h"
#include "AssemblyU2DCSharp_PropertyReference3287965026.h"
#include "AssemblyU2DCSharp_RealTime1079009241.h"
#include "AssemblyU2DCSharp_SpringPanel2962744957.h"
#include "AssemblyU2DCSharp_SpringPanel_OnFinished3595288269.h"
#include "AssemblyU2DCSharp_UIBasicSprite754925213.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type545852334.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection3739624076.h"
#include "AssemblyU2DCSharp_UIBasicSprite_AdvancedType2986673008.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip3496710405.h"
#include "AssemblyU2DCSharp_UIDrawCall3291843512.h"
#include "AssemblyU2DCSharp_UIDrawCall_Clipping4042448929.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2690968101.h"
#include "AssemblyU2DCSharp_UIEventListener3982099366.h"
#include "AssemblyU2DCSharp_UIEventListener_VoidDelegate3787195170.h"
#include "AssemblyU2DCSharp_UIEventListener_BoolDelegate2797443340.h"
#include "AssemblyU2DCSharp_UIEventListener_FloatDelegate1603297664.h"
#include "AssemblyU2DCSharp_UIEventListener_VectorDelegate3730950941.h"
#include "AssemblyU2DCSharp_UIEventListener_ObjectDelegate2995362177.h"
#include "AssemblyU2DCSharp_UIEventListener_KeyCodeDelegate3739953428.h"
#include "AssemblyU2DCSharp_UIGeometry1005900006.h"
#include "AssemblyU2DCSharp_UIRect4127168124.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint4057058294.h"
#include "AssemblyU2DCSharp_UIRect_AnchorUpdate1517700609.h"
#include "AssemblyU2DCSharp_UISnapshotPoint85978088.h"
#include "AssemblyU2DCSharp_UIWidget1453041918.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot752586349.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3620741577.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback836279582.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource2759843449.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3590501724.h"
#include "AssemblyU2DCSharp_AnimatedAlpha513307837.h"
#include "AssemblyU2DCSharp_AnimatedColor1057752172.h"
#include "AssemblyU2DCSharp_AnimatedWidget1936580709.h"
#include "AssemblyU2DCSharp_SpringPosition434217086.h"
#include "AssemblyU2DCSharp_SpringPosition_OnFinished3890054880.h"
#include "AssemblyU2DCSharp_TweenAlpha2421518635.h"
#include "AssemblyU2DCSharp_TweenColor3390486518.h"
#include "AssemblyU2DCSharp_TweenFOV612833154.h"
#include "AssemblyU2DCSharp_TweenHeight161374028.h"
#include "AssemblyU2DCSharp_TweenOrthoSize2754360798.h"
#include "AssemblyU2DCSharp_TweenPosition1144714832.h"
#include "AssemblyU2DCSharp_TweenRotation1747194511.h"
#include "AssemblyU2DCSharp_TweenScale2697902175.h"
#include "AssemblyU2DCSharp_TweenTransform3901935067.h"
#include "AssemblyU2DCSharp_TweenVolume1729630735.h"
#include "AssemblyU2DCSharp_TweenWidth511657453.h"
#include "AssemblyU2DCSharp_UITweener2986641582.h"
#include "AssemblyU2DCSharp_UITweener_Method1694901606.h"
#include "AssemblyU2DCSharp_UITweener_Style4221671544.h"
#include "AssemblyU2DCSharp_UI2DSprite1082505957.h"
#include "AssemblyU2DCSharp_UI2DSpriteAnimation3991849951.h"
#include "AssemblyU2DCSharp_UIAnchor624210015.h"
#include "AssemblyU2DCSharp_UIAnchor_Side3421105907.h"
#include "AssemblyU2DCSharp_UIAtlas1304615221.h"
#include "AssemblyU2DCSharp_UIAtlas_Sprite134787095.h"
#include "AssemblyU2DCSharp_UIAtlas_Coordinates2844876023.h"
#include "AssemblyU2DCSharp_UICamera1496819779.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme3098588360.h"
#include "AssemblyU2DCSharp_UICamera_ClickNotification3184017079.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch2470076277.h"
#include "AssemblyU2DCSharp_UICamera_EventType2218742030.h"
#include "AssemblyU2DCSharp_UICamera_GetKeyStateFunc1266514268.h"
#include "AssemblyU2DCSharp_UICamera_GetAxisFunc212136019.h"
#include "AssemblyU2DCSharp_UICamera_GetAnyKeyFunc2213225645.h"
#include "AssemblyU2DCSharp_UICamera_OnScreenResize1327492915.h"
#include "AssemblyU2DCSharp_UICamera_OnCustomInput3556372712.h"
#include "AssemblyU2DCSharp_UICamera_OnSchemeChange1272113120.h"
#include "AssemblyU2DCSharp_UICamera_MoveDelegate3125635652.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { sizeof (UIToggledObjects_t3646139686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5100[4] = 
{
	UIToggledObjects_t3646139686::get_offset_of_activate_2(),
	UIToggledObjects_t3646139686::get_offset_of_deactivate_3(),
	UIToggledObjects_t3646139686::get_offset_of_target_4(),
	UIToggledObjects_t3646139686::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { sizeof (UIWidgetContainer_t701016325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { sizeof (UIWrapContent_t1931723019), -1, sizeof(UIWrapContent_t1931723019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5102[14] = 
{
	UIWrapContent_t1931723019::get_offset_of_itemSize_2(),
	UIWrapContent_t1931723019::get_offset_of_cullContent_3(),
	UIWrapContent_t1931723019::get_offset_of_minIndex_4(),
	UIWrapContent_t1931723019::get_offset_of_maxIndex_5(),
	UIWrapContent_t1931723019::get_offset_of_onInitializeItem_6(),
	UIWrapContent_t1931723019::get_offset_of_mTrans_7(),
	UIWrapContent_t1931723019::get_offset_of_mPanel_8(),
	UIWrapContent_t1931723019::get_offset_of_mScroll_9(),
	UIWrapContent_t1931723019::get_offset_of_mHorizontal_10(),
	UIWrapContent_t1931723019::get_offset_of_mFirstTime_11(),
	UIWrapContent_t1931723019::get_offset_of_mChildren_12(),
	UIWrapContent_t1931723019_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
	UIWrapContent_t1931723019_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_14(),
	UIWrapContent_t1931723019_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { sizeof (OnInitializeItem_t2948699028), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { sizeof (ActiveAnimation_t4137610604), -1, sizeof(ActiveAnimation_t4137610604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5104[10] = 
{
	ActiveAnimation_t4137610604_StaticFields::get_offset_of_current_2(),
	ActiveAnimation_t4137610604::get_offset_of_onFinished_3(),
	ActiveAnimation_t4137610604::get_offset_of_eventReceiver_4(),
	ActiveAnimation_t4137610604::get_offset_of_callWhenFinished_5(),
	ActiveAnimation_t4137610604::get_offset_of_mAnim_6(),
	ActiveAnimation_t4137610604::get_offset_of_mLastDirection_7(),
	ActiveAnimation_t4137610604::get_offset_of_mDisableDirection_8(),
	ActiveAnimation_t4137610604::get_offset_of_mNotify_9(),
	ActiveAnimation_t4137610604::get_offset_of_mAnimator_10(),
	ActiveAnimation_t4137610604::get_offset_of_mClip_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { sizeof (Trigger_t3949147154)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5105[15] = 
{
	Trigger_t3949147154::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { sizeof (Direction_t1874854737)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5106[4] = 
{
	Direction_t1874854737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { sizeof (EnableCondition_t2151086590)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5107[4] = 
{
	EnableCondition_t2151086590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (DisableCondition_t122284263)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5108[4] = 
{
	DisableCondition_t122284263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (BMFont_t421884312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5109[7] = 
{
	BMFont_t421884312::get_offset_of_mSize_0(),
	BMFont_t421884312::get_offset_of_mBase_1(),
	BMFont_t421884312::get_offset_of_mWidth_2(),
	BMFont_t421884312::get_offset_of_mHeight_3(),
	BMFont_t421884312::get_offset_of_mSpriteName_4(),
	BMFont_t421884312::get_offset_of_mSaved_5(),
	BMFont_t421884312::get_offset_of_mDict_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (BMGlyph_t3903496831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5110[10] = 
{
	BMGlyph_t3903496831::get_offset_of_index_0(),
	BMGlyph_t3903496831::get_offset_of_x_1(),
	BMGlyph_t3903496831::get_offset_of_y_2(),
	BMGlyph_t3903496831::get_offset_of_width_3(),
	BMGlyph_t3903496831::get_offset_of_height_4(),
	BMGlyph_t3903496831::get_offset_of_offsetX_5(),
	BMGlyph_t3903496831::get_offset_of_offsetY_6(),
	BMGlyph_t3903496831::get_offset_of_advance_7(),
	BMGlyph_t3903496831::get_offset_of_channel_8(),
	BMGlyph_t3903496831::get_offset_of_kerning_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (BMSymbol_t1865486779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[11] = 
{
	BMSymbol_t1865486779::get_offset_of_sequence_0(),
	BMSymbol_t1865486779::get_offset_of_spriteName_1(),
	BMSymbol_t1865486779::get_offset_of_mSprite_2(),
	BMSymbol_t1865486779::get_offset_of_mIsValid_3(),
	BMSymbol_t1865486779::get_offset_of_mLength_4(),
	BMSymbol_t1865486779::get_offset_of_mOffsetX_5(),
	BMSymbol_t1865486779::get_offset_of_mOffsetY_6(),
	BMSymbol_t1865486779::get_offset_of_mWidth_7(),
	BMSymbol_t1865486779::get_offset_of_mHeight_8(),
	BMSymbol_t1865486779::get_offset_of_mAdvance_9(),
	BMSymbol_t1865486779::get_offset_of_mUV_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5112[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5114[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { sizeof (ByteReader_t2754005953), -1, sizeof(ByteReader_t2754005953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5115[3] = 
{
	ByteReader_t2754005953::get_offset_of_mBuffer_0(),
	ByteReader_t2754005953::get_offset_of_mOffset_1(),
	ByteReader_t2754005953_StaticFields::get_offset_of_mTemp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { sizeof (EventDelegate_t3496309181), -1, sizeof(EventDelegate_t3496309181_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5116[11] = 
{
	EventDelegate_t3496309181::get_offset_of_mTarget_0(),
	EventDelegate_t3496309181::get_offset_of_mMethodName_1(),
	EventDelegate_t3496309181::get_offset_of_mParameters_2(),
	EventDelegate_t3496309181::get_offset_of_oneShot_3(),
	EventDelegate_t3496309181::get_offset_of_mCachedCallback_4(),
	EventDelegate_t3496309181::get_offset_of_mRawDelegate_5(),
	EventDelegate_t3496309181::get_offset_of_mCached_6(),
	EventDelegate_t3496309181::get_offset_of_mMethod_7(),
	EventDelegate_t3496309181::get_offset_of_mParameterInfos_8(),
	EventDelegate_t3496309181::get_offset_of_mArgs_9(),
	EventDelegate_t3496309181_StaticFields::get_offset_of_s_Hash_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (Parameter_t3376276275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5117[7] = 
{
	Parameter_t3376276275::get_offset_of_obj_0(),
	Parameter_t3376276275::get_offset_of_field_1(),
	Parameter_t3376276275::get_offset_of_mValue_2(),
	Parameter_t3376276275::get_offset_of_expectedType_3(),
	Parameter_t3376276275::get_offset_of_cached_4(),
	Parameter_t3376276275::get_offset_of_propInfo_5(),
	Parameter_t3376276275::get_offset_of_fieldInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { sizeof (Callback_t2100910411), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { sizeof (Localization_t3725902693), -1, sizeof(Localization_t3725902693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5119[10] = 
{
	Localization_t3725902693_StaticFields::get_offset_of_loadFunction_0(),
	Localization_t3725902693_StaticFields::get_offset_of_onLocalize_1(),
	Localization_t3725902693_StaticFields::get_offset_of_localizationHasBeenSet_2(),
	Localization_t3725902693_StaticFields::get_offset_of_mLanguages_3(),
	Localization_t3725902693_StaticFields::get_offset_of_mOldDictionary_4(),
	Localization_t3725902693_StaticFields::get_offset_of_mDictionary_5(),
	Localization_t3725902693_StaticFields::get_offset_of_mReplacement_6(),
	Localization_t3725902693_StaticFields::get_offset_of_mLanguageIndex_7(),
	Localization_t3725902693_StaticFields::get_offset_of_mLanguage_8(),
	Localization_t3725902693_StaticFields::get_offset_of_mMerging_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { sizeof (LoadFunction_t1013268478), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (OnLocalizeNotification_t3819587589), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (NGUIDebug_t1735550160), -1, sizeof(NGUIDebug_t1735550160_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5122[3] = 
{
	NGUIDebug_t1735550160_StaticFields::get_offset_of_mRayDebug_2(),
	NGUIDebug_t1735550160_StaticFields::get_offset_of_mLines_3(),
	NGUIDebug_t1735550160_StaticFields::get_offset_of_mInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { sizeof (NGUIMath_t221371675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { sizeof (NGUIText_t2042929214), -1, sizeof(NGUIText_t2042929214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5124[35] = 
{
	NGUIText_t2042929214_StaticFields::get_offset_of_bitmapFont_0(),
	NGUIText_t2042929214_StaticFields::get_offset_of_dynamicFont_1(),
	NGUIText_t2042929214_StaticFields::get_offset_of_glyph_2(),
	NGUIText_t2042929214_StaticFields::get_offset_of_fontSize_3(),
	NGUIText_t2042929214_StaticFields::get_offset_of_fontScale_4(),
	NGUIText_t2042929214_StaticFields::get_offset_of_pixelDensity_5(),
	NGUIText_t2042929214_StaticFields::get_offset_of_fontStyle_6(),
	NGUIText_t2042929214_StaticFields::get_offset_of_alignment_7(),
	NGUIText_t2042929214_StaticFields::get_offset_of_tint_8(),
	NGUIText_t2042929214_StaticFields::get_offset_of_rectWidth_9(),
	NGUIText_t2042929214_StaticFields::get_offset_of_rectHeight_10(),
	NGUIText_t2042929214_StaticFields::get_offset_of_regionWidth_11(),
	NGUIText_t2042929214_StaticFields::get_offset_of_regionHeight_12(),
	NGUIText_t2042929214_StaticFields::get_offset_of_maxLines_13(),
	NGUIText_t2042929214_StaticFields::get_offset_of_gradient_14(),
	NGUIText_t2042929214_StaticFields::get_offset_of_gradientBottom_15(),
	NGUIText_t2042929214_StaticFields::get_offset_of_gradientTop_16(),
	NGUIText_t2042929214_StaticFields::get_offset_of_encoding_17(),
	NGUIText_t2042929214_StaticFields::get_offset_of_spacingX_18(),
	NGUIText_t2042929214_StaticFields::get_offset_of_spacingY_19(),
	NGUIText_t2042929214_StaticFields::get_offset_of_premultiply_20(),
	NGUIText_t2042929214_StaticFields::get_offset_of_symbolStyle_21(),
	NGUIText_t2042929214_StaticFields::get_offset_of_finalSize_22(),
	NGUIText_t2042929214_StaticFields::get_offset_of_finalSpacingX_23(),
	NGUIText_t2042929214_StaticFields::get_offset_of_finalLineHeight_24(),
	NGUIText_t2042929214_StaticFields::get_offset_of_baseline_25(),
	NGUIText_t2042929214_StaticFields::get_offset_of_useSymbols_26(),
	NGUIText_t2042929214_StaticFields::get_offset_of_mInvisible_27(),
	NGUIText_t2042929214_StaticFields::get_offset_of_mColors_28(),
	NGUIText_t2042929214_StaticFields::get_offset_of_mAlpha_29(),
	NGUIText_t2042929214_StaticFields::get_offset_of_mTempChar_30(),
	NGUIText_t2042929214_StaticFields::get_offset_of_mSizes_31(),
	NGUIText_t2042929214_StaticFields::get_offset_of_s_c0_32(),
	NGUIText_t2042929214_StaticFields::get_offset_of_s_c1_33(),
	NGUIText_t2042929214_StaticFields::get_offset_of_mBoldOffset_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (Alignment_t3620437664)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5125[6] = 
{
	Alignment_t3620437664::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (SymbolStyle_t2170314704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5126[4] = 
{
	SymbolStyle_t2170314704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (GlyphInfo_t3590955929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5127[8] = 
{
	GlyphInfo_t3590955929::get_offset_of_v0_0(),
	GlyphInfo_t3590955929::get_offset_of_v1_1(),
	GlyphInfo_t3590955929::get_offset_of_u0_2(),
	GlyphInfo_t3590955929::get_offset_of_u1_3(),
	GlyphInfo_t3590955929::get_offset_of_u2_4(),
	GlyphInfo_t3590955929::get_offset_of_u3_5(),
	GlyphInfo_t3590955929::get_offset_of_advance_6(),
	GlyphInfo_t3590955929::get_offset_of_channel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (NGUITools_t2004302824), -1, sizeof(NGUITools_t2004302824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5128[9] = 
{
	NGUITools_t2004302824_StaticFields::get_offset_of_mListener_0(),
	NGUITools_t2004302824_StaticFields::get_offset_of_mLoaded_1(),
	NGUITools_t2004302824_StaticFields::get_offset_of_mGlobalVolume_2(),
	NGUITools_t2004302824_StaticFields::get_offset_of_mLastTimestamp_3(),
	NGUITools_t2004302824_StaticFields::get_offset_of_mLastClip_4(),
	NGUITools_t2004302824_StaticFields::get_offset_of_mSides_5(),
	NGUITools_t2004302824_StaticFields::get_offset_of_keys_6(),
	NGUITools_t2004302824_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	NGUITools_t2004302824_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { sizeof (PropertyBinding_t3567692958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5129[6] = 
{
	PropertyBinding_t3567692958::get_offset_of_source_2(),
	PropertyBinding_t3567692958::get_offset_of_target_3(),
	PropertyBinding_t3567692958::get_offset_of_direction_4(),
	PropertyBinding_t3567692958::get_offset_of_update_5(),
	PropertyBinding_t3567692958::get_offset_of_editMode_6(),
	PropertyBinding_t3567692958::get_offset_of_mLastValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (UpdateCondition_t2438470829)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5130[5] = 
{
	UpdateCondition_t2438470829::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (Direction_t2332796344)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5131[4] = 
{
	Direction_t2332796344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (PropertyReference_t3287965026), -1, sizeof(PropertyReference_t3287965026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5132[5] = 
{
	PropertyReference_t3287965026::get_offset_of_mTarget_0(),
	PropertyReference_t3287965026::get_offset_of_mName_1(),
	PropertyReference_t3287965026::get_offset_of_mField_2(),
	PropertyReference_t3287965026::get_offset_of_mProperty_3(),
	PropertyReference_t3287965026_StaticFields::get_offset_of_s_Hash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { sizeof (RealTime_t1079009241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (SpringPanel_t2962744957), -1, sizeof(SpringPanel_t2962744957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5134[7] = 
{
	SpringPanel_t2962744957_StaticFields::get_offset_of_current_2(),
	SpringPanel_t2962744957::get_offset_of_target_3(),
	SpringPanel_t2962744957::get_offset_of_strength_4(),
	SpringPanel_t2962744957::get_offset_of_onFinished_5(),
	SpringPanel_t2962744957::get_offset_of_mPanel_6(),
	SpringPanel_t2962744957::get_offset_of_mTrans_7(),
	SpringPanel_t2962744957::get_offset_of_mDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { sizeof (OnFinished_t3595288269), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { sizeof (UIBasicSprite_t754925213), -1, sizeof(UIBasicSprite_t754925213_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5136[14] = 
{
	UIBasicSprite_t754925213::get_offset_of_mType_52(),
	UIBasicSprite_t754925213::get_offset_of_mFillDirection_53(),
	UIBasicSprite_t754925213::get_offset_of_mFillAmount_54(),
	UIBasicSprite_t754925213::get_offset_of_mInvert_55(),
	UIBasicSprite_t754925213::get_offset_of_mFlip_56(),
	UIBasicSprite_t754925213::get_offset_of_mInnerUV_57(),
	UIBasicSprite_t754925213::get_offset_of_mOuterUV_58(),
	UIBasicSprite_t754925213::get_offset_of_centerType_59(),
	UIBasicSprite_t754925213::get_offset_of_leftType_60(),
	UIBasicSprite_t754925213::get_offset_of_rightType_61(),
	UIBasicSprite_t754925213::get_offset_of_bottomType_62(),
	UIBasicSprite_t754925213::get_offset_of_topType_63(),
	UIBasicSprite_t754925213_StaticFields::get_offset_of_mTempPos_64(),
	UIBasicSprite_t754925213_StaticFields::get_offset_of_mTempUVs_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { sizeof (Type_t545852334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5137[6] = 
{
	Type_t545852334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { sizeof (FillDirection_t3739624076)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5138[6] = 
{
	FillDirection_t3739624076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (AdvancedType_t2986673008)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5139[4] = 
{
	AdvancedType_t2986673008::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (Flip_t3496710405)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5140[5] = 
{
	Flip_t3496710405::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (UIDrawCall_t3291843512), -1, sizeof(UIDrawCall_t3291843512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5141[35] = 
{
	UIDrawCall_t3291843512_StaticFields::get_offset_of_mActiveList_2(),
	UIDrawCall_t3291843512_StaticFields::get_offset_of_mInactiveList_3(),
	UIDrawCall_t3291843512::get_offset_of_widgetCount_4(),
	UIDrawCall_t3291843512::get_offset_of_depthStart_5(),
	UIDrawCall_t3291843512::get_offset_of_depthEnd_6(),
	UIDrawCall_t3291843512::get_offset_of_manager_7(),
	UIDrawCall_t3291843512::get_offset_of_panel_8(),
	UIDrawCall_t3291843512::get_offset_of_clipTexture_9(),
	UIDrawCall_t3291843512::get_offset_of_alwaysOnScreen_10(),
	UIDrawCall_t3291843512::get_offset_of_verts_11(),
	UIDrawCall_t3291843512::get_offset_of_norms_12(),
	UIDrawCall_t3291843512::get_offset_of_tans_13(),
	UIDrawCall_t3291843512::get_offset_of_uvs_14(),
	UIDrawCall_t3291843512::get_offset_of_cols_15(),
	UIDrawCall_t3291843512::get_offset_of_mMaterial_16(),
	UIDrawCall_t3291843512::get_offset_of_mTexture_17(),
	UIDrawCall_t3291843512::get_offset_of_mShader_18(),
	UIDrawCall_t3291843512::get_offset_of_mClipCount_19(),
	UIDrawCall_t3291843512::get_offset_of_mTrans_20(),
	UIDrawCall_t3291843512::get_offset_of_mMesh_21(),
	UIDrawCall_t3291843512::get_offset_of_mFilter_22(),
	UIDrawCall_t3291843512::get_offset_of_mRenderer_23(),
	UIDrawCall_t3291843512::get_offset_of_mDynamicMat_24(),
	UIDrawCall_t3291843512::get_offset_of_mIndices_25(),
	UIDrawCall_t3291843512::get_offset_of_mRebuildMat_26(),
	UIDrawCall_t3291843512::get_offset_of_mLegacyShader_27(),
	UIDrawCall_t3291843512::get_offset_of_mRenderQueue_28(),
	UIDrawCall_t3291843512::get_offset_of_mTriangles_29(),
	UIDrawCall_t3291843512::get_offset_of_isDirty_30(),
	UIDrawCall_t3291843512::get_offset_of_mTextureClip_31(),
	UIDrawCall_t3291843512::get_offset_of_onRender_32(),
	0,
	UIDrawCall_t3291843512_StaticFields::get_offset_of_mCache_34(),
	UIDrawCall_t3291843512_StaticFields::get_offset_of_ClipRange_35(),
	UIDrawCall_t3291843512_StaticFields::get_offset_of_ClipArgs_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (Clipping_t4042448929)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5142[5] = 
{
	Clipping_t4042448929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (OnRenderCallback_t2690968101), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (UIEventListener_t3982099366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5144[16] = 
{
	UIEventListener_t3982099366::get_offset_of_parameter_2(),
	UIEventListener_t3982099366::get_offset_of_onSubmit_3(),
	UIEventListener_t3982099366::get_offset_of_onClick_4(),
	UIEventListener_t3982099366::get_offset_of_onDoubleClick_5(),
	UIEventListener_t3982099366::get_offset_of_onHover_6(),
	UIEventListener_t3982099366::get_offset_of_onPress_7(),
	UIEventListener_t3982099366::get_offset_of_onSelect_8(),
	UIEventListener_t3982099366::get_offset_of_onScroll_9(),
	UIEventListener_t3982099366::get_offset_of_onDragStart_10(),
	UIEventListener_t3982099366::get_offset_of_onDrag_11(),
	UIEventListener_t3982099366::get_offset_of_onDragOver_12(),
	UIEventListener_t3982099366::get_offset_of_onDragOut_13(),
	UIEventListener_t3982099366::get_offset_of_onDragEnd_14(),
	UIEventListener_t3982099366::get_offset_of_onDrop_15(),
	UIEventListener_t3982099366::get_offset_of_onKey_16(),
	UIEventListener_t3982099366::get_offset_of_onTooltip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (VoidDelegate_t3787195170), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (BoolDelegate_t2797443340), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (FloatDelegate_t1603297664), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (VectorDelegate_t3730950941), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { sizeof (ObjectDelegate_t2995362177), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { sizeof (KeyCodeDelegate_t3739953428), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { sizeof (UIGeometry_t1005900006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5151[6] = 
{
	UIGeometry_t1005900006::get_offset_of_verts_0(),
	UIGeometry_t1005900006::get_offset_of_uvs_1(),
	UIGeometry_t1005900006::get_offset_of_cols_2(),
	UIGeometry_t1005900006::get_offset_of_mRtpVerts_3(),
	UIGeometry_t1005900006::get_offset_of_mRtpNormal_4(),
	UIGeometry_t1005900006::get_offset_of_mRtpTan_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { sizeof (UIRect_t4127168124), -1, sizeof(UIRect_t4127168124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5152[20] = 
{
	UIRect_t4127168124::get_offset_of_leftAnchor_2(),
	UIRect_t4127168124::get_offset_of_rightAnchor_3(),
	UIRect_t4127168124::get_offset_of_bottomAnchor_4(),
	UIRect_t4127168124::get_offset_of_topAnchor_5(),
	UIRect_t4127168124::get_offset_of_updateAnchors_6(),
	UIRect_t4127168124::get_offset_of_mGo_7(),
	UIRect_t4127168124::get_offset_of_mTrans_8(),
	UIRect_t4127168124::get_offset_of_mChildren_9(),
	UIRect_t4127168124::get_offset_of_mChanged_10(),
	UIRect_t4127168124::get_offset_of_mStarted_11(),
	UIRect_t4127168124::get_offset_of_mParentFound_12(),
	UIRect_t4127168124::get_offset_of_mUpdateAnchors_13(),
	UIRect_t4127168124::get_offset_of_mUpdateFrame_14(),
	UIRect_t4127168124::get_offset_of_mAnchorsCached_15(),
	UIRect_t4127168124::get_offset_of_mRoot_16(),
	UIRect_t4127168124::get_offset_of_mParent_17(),
	UIRect_t4127168124::get_offset_of_mRootSet_18(),
	UIRect_t4127168124::get_offset_of_mCam_19(),
	UIRect_t4127168124::get_offset_of_finalAlpha_20(),
	UIRect_t4127168124_StaticFields::get_offset_of_mSides_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (AnchorPoint_t4057058294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5153[5] = 
{
	AnchorPoint_t4057058294::get_offset_of_target_0(),
	AnchorPoint_t4057058294::get_offset_of_relative_1(),
	AnchorPoint_t4057058294::get_offset_of_absolute_2(),
	AnchorPoint_t4057058294::get_offset_of_rect_3(),
	AnchorPoint_t4057058294::get_offset_of_targetCam_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { sizeof (AnchorUpdate_t1517700609)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5154[4] = 
{
	AnchorUpdate_t1517700609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (UISnapshotPoint_t85978088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5155[6] = 
{
	UISnapshotPoint_t85978088::get_offset_of_isOrthographic_2(),
	UISnapshotPoint_t85978088::get_offset_of_nearClip_3(),
	UISnapshotPoint_t85978088::get_offset_of_farClip_4(),
	UISnapshotPoint_t85978088::get_offset_of_fieldOfView_5(),
	UISnapshotPoint_t85978088::get_offset_of_orthoSize_6(),
	UISnapshotPoint_t85978088::get_offset_of_thumbnail_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { sizeof (UIWidget_t1453041918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5156[30] = 
{
	UIWidget_t1453041918::get_offset_of_mColor_22(),
	UIWidget_t1453041918::get_offset_of_mPivot_23(),
	UIWidget_t1453041918::get_offset_of_mWidth_24(),
	UIWidget_t1453041918::get_offset_of_mHeight_25(),
	UIWidget_t1453041918::get_offset_of_mDepth_26(),
	UIWidget_t1453041918::get_offset_of_onChange_27(),
	UIWidget_t1453041918::get_offset_of_onPostFill_28(),
	UIWidget_t1453041918::get_offset_of_mOnRender_29(),
	UIWidget_t1453041918::get_offset_of_autoResizeBoxCollider_30(),
	UIWidget_t1453041918::get_offset_of_hideIfOffScreen_31(),
	UIWidget_t1453041918::get_offset_of_keepAspectRatio_32(),
	UIWidget_t1453041918::get_offset_of_aspectRatio_33(),
	UIWidget_t1453041918::get_offset_of_hitCheck_34(),
	UIWidget_t1453041918::get_offset_of_panel_35(),
	UIWidget_t1453041918::get_offset_of_geometry_36(),
	UIWidget_t1453041918::get_offset_of_fillGeometry_37(),
	UIWidget_t1453041918::get_offset_of_mPlayMode_38(),
	UIWidget_t1453041918::get_offset_of_mDrawRegion_39(),
	UIWidget_t1453041918::get_offset_of_mLocalToPanel_40(),
	UIWidget_t1453041918::get_offset_of_mIsVisibleByAlpha_41(),
	UIWidget_t1453041918::get_offset_of_mIsVisibleByPanel_42(),
	UIWidget_t1453041918::get_offset_of_mIsInFront_43(),
	UIWidget_t1453041918::get_offset_of_mLastAlpha_44(),
	UIWidget_t1453041918::get_offset_of_mMoved_45(),
	UIWidget_t1453041918::get_offset_of_drawCall_46(),
	UIWidget_t1453041918::get_offset_of_mCorners_47(),
	UIWidget_t1453041918::get_offset_of_mAlphaFrameID_48(),
	UIWidget_t1453041918::get_offset_of_mMatrixFrame_49(),
	UIWidget_t1453041918::get_offset_of_mOldV0_50(),
	UIWidget_t1453041918::get_offset_of_mOldV1_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (Pivot_t752586349)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5157[10] = 
{
	Pivot_t752586349::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { sizeof (OnDimensionsChanged_t3620741577), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5159 = { sizeof (OnPostFillCallback_t836279582), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5160 = { sizeof (AspectRatioSource_t2759843449)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5160[4] = 
{
	AspectRatioSource_t2759843449::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5161 = { sizeof (HitCheck_t3590501724), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5162 = { sizeof (AnimatedAlpha_t513307837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5162[3] = 
{
	AnimatedAlpha_t513307837::get_offset_of_alpha_2(),
	AnimatedAlpha_t513307837::get_offset_of_mWidget_3(),
	AnimatedAlpha_t513307837::get_offset_of_mPanel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5163 = { sizeof (AnimatedColor_t1057752172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5163[2] = 
{
	AnimatedColor_t1057752172::get_offset_of_color_2(),
	AnimatedColor_t1057752172::get_offset_of_mWidget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5164 = { sizeof (AnimatedWidget_t1936580709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5164[3] = 
{
	AnimatedWidget_t1936580709::get_offset_of_width_2(),
	AnimatedWidget_t1936580709::get_offset_of_height_3(),
	AnimatedWidget_t1936580709::get_offset_of_mWidget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5165 = { sizeof (SpringPosition_t434217086), -1, sizeof(SpringPosition_t434217086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5165[12] = 
{
	SpringPosition_t434217086_StaticFields::get_offset_of_current_2(),
	SpringPosition_t434217086::get_offset_of_target_3(),
	SpringPosition_t434217086::get_offset_of_strength_4(),
	SpringPosition_t434217086::get_offset_of_worldSpace_5(),
	SpringPosition_t434217086::get_offset_of_ignoreTimeScale_6(),
	SpringPosition_t434217086::get_offset_of_updateScrollView_7(),
	SpringPosition_t434217086::get_offset_of_onFinished_8(),
	SpringPosition_t434217086::get_offset_of_eventReceiver_9(),
	SpringPosition_t434217086::get_offset_of_callWhenFinished_10(),
	SpringPosition_t434217086::get_offset_of_mTrans_11(),
	SpringPosition_t434217086::get_offset_of_mThreshold_12(),
	SpringPosition_t434217086::get_offset_of_mSv_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5166 = { sizeof (OnFinished_t3890054880), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5167 = { sizeof (TweenAlpha_t2421518635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5167[6] = 
{
	TweenAlpha_t2421518635::get_offset_of_from_20(),
	TweenAlpha_t2421518635::get_offset_of_to_21(),
	TweenAlpha_t2421518635::get_offset_of_mCached_22(),
	TweenAlpha_t2421518635::get_offset_of_mRect_23(),
	TweenAlpha_t2421518635::get_offset_of_mMat_24(),
	TweenAlpha_t2421518635::get_offset_of_mSr_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5168 = { sizeof (TweenColor_t3390486518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5168[7] = 
{
	TweenColor_t3390486518::get_offset_of_from_20(),
	TweenColor_t3390486518::get_offset_of_to_21(),
	TweenColor_t3390486518::get_offset_of_mCached_22(),
	TweenColor_t3390486518::get_offset_of_mWidget_23(),
	TweenColor_t3390486518::get_offset_of_mMat_24(),
	TweenColor_t3390486518::get_offset_of_mLight_25(),
	TweenColor_t3390486518::get_offset_of_mSr_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5169 = { sizeof (TweenFOV_t612833154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5169[3] = 
{
	TweenFOV_t612833154::get_offset_of_from_20(),
	TweenFOV_t612833154::get_offset_of_to_21(),
	TweenFOV_t612833154::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5170 = { sizeof (TweenHeight_t161374028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5170[5] = 
{
	TweenHeight_t161374028::get_offset_of_from_20(),
	TweenHeight_t161374028::get_offset_of_to_21(),
	TweenHeight_t161374028::get_offset_of_updateTable_22(),
	TweenHeight_t161374028::get_offset_of_mWidget_23(),
	TweenHeight_t161374028::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5171 = { sizeof (TweenOrthoSize_t2754360798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5171[3] = 
{
	TweenOrthoSize_t2754360798::get_offset_of_from_20(),
	TweenOrthoSize_t2754360798::get_offset_of_to_21(),
	TweenOrthoSize_t2754360798::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5172 = { sizeof (TweenPosition_t1144714832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5172[5] = 
{
	TweenPosition_t1144714832::get_offset_of_from_20(),
	TweenPosition_t1144714832::get_offset_of_to_21(),
	TweenPosition_t1144714832::get_offset_of_worldSpace_22(),
	TweenPosition_t1144714832::get_offset_of_mTrans_23(),
	TweenPosition_t1144714832::get_offset_of_mRect_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5173 = { sizeof (TweenRotation_t1747194511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5173[4] = 
{
	TweenRotation_t1747194511::get_offset_of_from_20(),
	TweenRotation_t1747194511::get_offset_of_to_21(),
	TweenRotation_t1747194511::get_offset_of_quaternionLerp_22(),
	TweenRotation_t1747194511::get_offset_of_mTrans_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5174 = { sizeof (TweenScale_t2697902175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5174[5] = 
{
	TweenScale_t2697902175::get_offset_of_from_20(),
	TweenScale_t2697902175::get_offset_of_to_21(),
	TweenScale_t2697902175::get_offset_of_updateTable_22(),
	TweenScale_t2697902175::get_offset_of_mTrans_23(),
	TweenScale_t2697902175::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5175 = { sizeof (TweenTransform_t3901935067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5175[7] = 
{
	TweenTransform_t3901935067::get_offset_of_from_20(),
	TweenTransform_t3901935067::get_offset_of_to_21(),
	TweenTransform_t3901935067::get_offset_of_parentWhenFinished_22(),
	TweenTransform_t3901935067::get_offset_of_mTrans_23(),
	TweenTransform_t3901935067::get_offset_of_mPos_24(),
	TweenTransform_t3901935067::get_offset_of_mRot_25(),
	TweenTransform_t3901935067::get_offset_of_mScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5176 = { sizeof (TweenVolume_t1729630735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5176[3] = 
{
	TweenVolume_t1729630735::get_offset_of_from_20(),
	TweenVolume_t1729630735::get_offset_of_to_21(),
	TweenVolume_t1729630735::get_offset_of_mSource_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5177 = { sizeof (TweenWidth_t511657453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5177[5] = 
{
	TweenWidth_t511657453::get_offset_of_from_20(),
	TweenWidth_t511657453::get_offset_of_to_21(),
	TweenWidth_t511657453::get_offset_of_updateTable_22(),
	TweenWidth_t511657453::get_offset_of_mWidget_23(),
	TweenWidth_t511657453::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5178 = { sizeof (UITweener_t2986641582), -1, sizeof(UITweener_t2986641582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5178[18] = 
{
	UITweener_t2986641582_StaticFields::get_offset_of_current_2(),
	UITweener_t2986641582::get_offset_of_method_3(),
	UITweener_t2986641582::get_offset_of_style_4(),
	UITweener_t2986641582::get_offset_of_animationCurve_5(),
	UITweener_t2986641582::get_offset_of_ignoreTimeScale_6(),
	UITweener_t2986641582::get_offset_of_delay_7(),
	UITweener_t2986641582::get_offset_of_duration_8(),
	UITweener_t2986641582::get_offset_of_steeperCurves_9(),
	UITweener_t2986641582::get_offset_of_tweenGroup_10(),
	UITweener_t2986641582::get_offset_of_onFinished_11(),
	UITweener_t2986641582::get_offset_of_eventReceiver_12(),
	UITweener_t2986641582::get_offset_of_callWhenFinished_13(),
	UITweener_t2986641582::get_offset_of_mStarted_14(),
	UITweener_t2986641582::get_offset_of_mStartTime_15(),
	UITweener_t2986641582::get_offset_of_mDuration_16(),
	UITweener_t2986641582::get_offset_of_mAmountPerDelta_17(),
	UITweener_t2986641582::get_offset_of_mFactor_18(),
	UITweener_t2986641582::get_offset_of_mTemp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5179 = { sizeof (Method_t1694901606)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5179[7] = 
{
	Method_t1694901606::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5180 = { sizeof (Style_t4221671544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5180[4] = 
{
	Style_t4221671544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5181 = { sizeof (UI2DSprite_t1082505957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5181[8] = 
{
	UI2DSprite_t1082505957::get_offset_of_mSprite_66(),
	UI2DSprite_t1082505957::get_offset_of_mMat_67(),
	UI2DSprite_t1082505957::get_offset_of_mShader_68(),
	UI2DSprite_t1082505957::get_offset_of_mBorder_69(),
	UI2DSprite_t1082505957::get_offset_of_mFixedAspect_70(),
	UI2DSprite_t1082505957::get_offset_of_mPixelSize_71(),
	UI2DSprite_t1082505957::get_offset_of_nextSprite_72(),
	UI2DSprite_t1082505957::get_offset_of_mPMA_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5182 = { sizeof (UI2DSpriteAnimation_t3991849951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5182[8] = 
{
	UI2DSpriteAnimation_t3991849951::get_offset_of_framerate_2(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_ignoreTimeScale_3(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_loop_4(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_frames_5(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_mUnitySprite_6(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_mNguiSprite_7(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_mIndex_8(),
	UI2DSpriteAnimation_t3991849951::get_offset_of_mUpdate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5183 = { sizeof (UIAnchor_t624210015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5183[12] = 
{
	UIAnchor_t624210015::get_offset_of_uiCamera_2(),
	UIAnchor_t624210015::get_offset_of_container_3(),
	UIAnchor_t624210015::get_offset_of_side_4(),
	UIAnchor_t624210015::get_offset_of_runOnlyOnce_5(),
	UIAnchor_t624210015::get_offset_of_relativeOffset_6(),
	UIAnchor_t624210015::get_offset_of_pixelOffset_7(),
	UIAnchor_t624210015::get_offset_of_widgetContainer_8(),
	UIAnchor_t624210015::get_offset_of_mTrans_9(),
	UIAnchor_t624210015::get_offset_of_mAnim_10(),
	UIAnchor_t624210015::get_offset_of_mRect_11(),
	UIAnchor_t624210015::get_offset_of_mRoot_12(),
	UIAnchor_t624210015::get_offset_of_mStarted_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5184 = { sizeof (Side_t3421105907)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5184[10] = 
{
	Side_t3421105907::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5185 = { sizeof (UIAtlas_t1304615221), -1, sizeof(UIAtlas_t1304615221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5185[9] = 
{
	UIAtlas_t1304615221::get_offset_of_material_2(),
	UIAtlas_t1304615221::get_offset_of_mSprites_3(),
	UIAtlas_t1304615221::get_offset_of_mPixelSize_4(),
	UIAtlas_t1304615221::get_offset_of_mReplacement_5(),
	UIAtlas_t1304615221::get_offset_of_mCoordinates_6(),
	UIAtlas_t1304615221::get_offset_of_sprites_7(),
	UIAtlas_t1304615221::get_offset_of_mPMA_8(),
	UIAtlas_t1304615221::get_offset_of_mSpriteIndices_9(),
	UIAtlas_t1304615221_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5186 = { sizeof (Sprite_t134787095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5186[8] = 
{
	Sprite_t134787095::get_offset_of_name_0(),
	Sprite_t134787095::get_offset_of_outer_1(),
	Sprite_t134787095::get_offset_of_inner_2(),
	Sprite_t134787095::get_offset_of_rotated_3(),
	Sprite_t134787095::get_offset_of_paddingLeft_4(),
	Sprite_t134787095::get_offset_of_paddingRight_5(),
	Sprite_t134787095::get_offset_of_paddingTop_6(),
	Sprite_t134787095::get_offset_of_paddingBottom_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5187 = { sizeof (Coordinates_t2844876023)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5187[3] = 
{
	Coordinates_t2844876023::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5188 = { sizeof (UICamera_t1496819779), -1, sizeof(UICamera_t1496819779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5188[95] = 
{
	UICamera_t1496819779_StaticFields::get_offset_of_list_2(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetKeyDown_3(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetKeyUp_4(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetKey_5(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetAxis_6(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetAnyKeyDown_7(),
	UICamera_t1496819779_StaticFields::get_offset_of_onScreenResize_8(),
	UICamera_t1496819779::get_offset_of_eventType_9(),
	UICamera_t1496819779::get_offset_of_eventsGoToColliders_10(),
	UICamera_t1496819779::get_offset_of_eventReceiverMask_11(),
	UICamera_t1496819779::get_offset_of_debug_12(),
	UICamera_t1496819779::get_offset_of_useMouse_13(),
	UICamera_t1496819779::get_offset_of_useTouch_14(),
	UICamera_t1496819779::get_offset_of_allowMultiTouch_15(),
	UICamera_t1496819779::get_offset_of_useKeyboard_16(),
	UICamera_t1496819779::get_offset_of_useController_17(),
	UICamera_t1496819779::get_offset_of_stickyTooltip_18(),
	UICamera_t1496819779::get_offset_of_tooltipDelay_19(),
	UICamera_t1496819779::get_offset_of_longPressTooltip_20(),
	UICamera_t1496819779::get_offset_of_mouseDragThreshold_21(),
	UICamera_t1496819779::get_offset_of_mouseClickThreshold_22(),
	UICamera_t1496819779::get_offset_of_touchDragThreshold_23(),
	UICamera_t1496819779::get_offset_of_touchClickThreshold_24(),
	UICamera_t1496819779::get_offset_of_rangeDistance_25(),
	UICamera_t1496819779::get_offset_of_horizontalAxisName_26(),
	UICamera_t1496819779::get_offset_of_verticalAxisName_27(),
	UICamera_t1496819779::get_offset_of_horizontalPanAxisName_28(),
	UICamera_t1496819779::get_offset_of_verticalPanAxisName_29(),
	UICamera_t1496819779::get_offset_of_scrollAxisName_30(),
	UICamera_t1496819779::get_offset_of_commandClick_31(),
	UICamera_t1496819779::get_offset_of_submitKey0_32(),
	UICamera_t1496819779::get_offset_of_submitKey1_33(),
	UICamera_t1496819779::get_offset_of_cancelKey0_34(),
	UICamera_t1496819779::get_offset_of_cancelKey1_35(),
	UICamera_t1496819779_StaticFields::get_offset_of_onCustomInput_36(),
	UICamera_t1496819779_StaticFields::get_offset_of_showTooltips_37(),
	UICamera_t1496819779_StaticFields::get_offset_of_mDisableController_38(),
	UICamera_t1496819779_StaticFields::get_offset_of_mLastPos_39(),
	UICamera_t1496819779_StaticFields::get_offset_of_lastWorldPosition_40(),
	UICamera_t1496819779_StaticFields::get_offset_of_lastHit_41(),
	UICamera_t1496819779_StaticFields::get_offset_of_current_42(),
	UICamera_t1496819779_StaticFields::get_offset_of_currentCamera_43(),
	UICamera_t1496819779_StaticFields::get_offset_of_onSchemeChange_44(),
	UICamera_t1496819779_StaticFields::get_offset_of_currentTouchID_45(),
	UICamera_t1496819779_StaticFields::get_offset_of_mCurrentKey_46(),
	UICamera_t1496819779_StaticFields::get_offset_of_currentTouch_47(),
	UICamera_t1496819779_StaticFields::get_offset_of_mInputFocus_48(),
	UICamera_t1496819779_StaticFields::get_offset_of_mGenericHandler_49(),
	UICamera_t1496819779_StaticFields::get_offset_of_fallThrough_50(),
	UICamera_t1496819779_StaticFields::get_offset_of_onClick_51(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDoubleClick_52(),
	UICamera_t1496819779_StaticFields::get_offset_of_onHover_53(),
	UICamera_t1496819779_StaticFields::get_offset_of_onPress_54(),
	UICamera_t1496819779_StaticFields::get_offset_of_onSelect_55(),
	UICamera_t1496819779_StaticFields::get_offset_of_onScroll_56(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDrag_57(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDragStart_58(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDragOver_59(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDragOut_60(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDragEnd_61(),
	UICamera_t1496819779_StaticFields::get_offset_of_onDrop_62(),
	UICamera_t1496819779_StaticFields::get_offset_of_onKey_63(),
	UICamera_t1496819779_StaticFields::get_offset_of_onNavigate_64(),
	UICamera_t1496819779_StaticFields::get_offset_of_onPan_65(),
	UICamera_t1496819779_StaticFields::get_offset_of_onTooltip_66(),
	UICamera_t1496819779_StaticFields::get_offset_of_onMouseMove_67(),
	UICamera_t1496819779_StaticFields::get_offset_of_mMouse_68(),
	UICamera_t1496819779_StaticFields::get_offset_of_controller_69(),
	UICamera_t1496819779_StaticFields::get_offset_of_activeTouches_70(),
	UICamera_t1496819779_StaticFields::get_offset_of_mTouchIDs_71(),
	UICamera_t1496819779_StaticFields::get_offset_of_mWidth_72(),
	UICamera_t1496819779_StaticFields::get_offset_of_mHeight_73(),
	UICamera_t1496819779_StaticFields::get_offset_of_mTooltip_74(),
	UICamera_t1496819779::get_offset_of_mCam_75(),
	UICamera_t1496819779_StaticFields::get_offset_of_mTooltipTime_76(),
	UICamera_t1496819779::get_offset_of_mNextRaycast_77(),
	UICamera_t1496819779_StaticFields::get_offset_of_isDragging_78(),
	UICamera_t1496819779_StaticFields::get_offset_of_mRayHitObject_79(),
	UICamera_t1496819779_StaticFields::get_offset_of_mHover_80(),
	UICamera_t1496819779_StaticFields::get_offset_of_mSelected_81(),
	UICamera_t1496819779_StaticFields::get_offset_of_mHit_82(),
	UICamera_t1496819779_StaticFields::get_offset_of_mHits_83(),
	UICamera_t1496819779_StaticFields::get_offset_of_m2DPlane_84(),
	UICamera_t1496819779_StaticFields::get_offset_of_mNextEvent_85(),
	UICamera_t1496819779_StaticFields::get_offset_of_mNotifying_86(),
	UICamera_t1496819779_StaticFields::get_offset_of_mUsingTouchEvents_87(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetInputTouchCount_88(),
	UICamera_t1496819779_StaticFields::get_offset_of_GetInputTouch_89(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_90(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_91(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_92(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_93(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_94(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_95(),
	UICamera_t1496819779_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5189 = { sizeof (ControlScheme_t3098588360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5189[4] = 
{
	ControlScheme_t3098588360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5190 = { sizeof (ClickNotification_t3184017079)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5190[4] = 
{
	ClickNotification_t3184017079::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5191 = { sizeof (MouseOrTouch_t2470076277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5191[17] = 
{
	MouseOrTouch_t2470076277::get_offset_of_key_0(),
	MouseOrTouch_t2470076277::get_offset_of_pos_1(),
	MouseOrTouch_t2470076277::get_offset_of_lastPos_2(),
	MouseOrTouch_t2470076277::get_offset_of_delta_3(),
	MouseOrTouch_t2470076277::get_offset_of_totalDelta_4(),
	MouseOrTouch_t2470076277::get_offset_of_pressedCam_5(),
	MouseOrTouch_t2470076277::get_offset_of_last_6(),
	MouseOrTouch_t2470076277::get_offset_of_current_7(),
	MouseOrTouch_t2470076277::get_offset_of_pressed_8(),
	MouseOrTouch_t2470076277::get_offset_of_dragged_9(),
	MouseOrTouch_t2470076277::get_offset_of_pressTime_10(),
	MouseOrTouch_t2470076277::get_offset_of_clickTime_11(),
	MouseOrTouch_t2470076277::get_offset_of_clickNotification_12(),
	MouseOrTouch_t2470076277::get_offset_of_touchBegan_13(),
	MouseOrTouch_t2470076277::get_offset_of_pressStarted_14(),
	MouseOrTouch_t2470076277::get_offset_of_dragStarted_15(),
	MouseOrTouch_t2470076277::get_offset_of_ignoreDelta_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5192 = { sizeof (EventType_t2218742030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5192[5] = 
{
	EventType_t2218742030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5193 = { sizeof (GetKeyStateFunc_t1266514268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5194 = { sizeof (GetAxisFunc_t212136019), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5195 = { sizeof (GetAnyKeyFunc_t2213225645), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5196 = { sizeof (OnScreenResize_t1327492915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5197 = { sizeof (OnCustomInput_t3556372712), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5198 = { sizeof (OnSchemeChange_t1272113120), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5199 = { sizeof (MoveDelegate_t3125635652), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
