﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Radial
struct CameraFilterPack_Blur_Radial_t566155114;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Radial::.ctor()
extern "C"  void CameraFilterPack_Blur_Radial__ctor_m3140379809 (CameraFilterPack_Blur_Radial_t566155114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Radial::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Radial_get_material_m2700975392 (CameraFilterPack_Blur_Radial_t566155114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::Start()
extern "C"  void CameraFilterPack_Blur_Radial_Start_m1616511209 (CameraFilterPack_Blur_Radial_t566155114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Radial_OnRenderImage_m3679368609 (CameraFilterPack_Blur_Radial_t566155114 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::OnValidate()
extern "C"  void CameraFilterPack_Blur_Radial_OnValidate_m2142995894 (CameraFilterPack_Blur_Radial_t566155114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::Update()
extern "C"  void CameraFilterPack_Blur_Radial_Update_m3547921000 (CameraFilterPack_Blur_Radial_t566155114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::OnDisable()
extern "C"  void CameraFilterPack_Blur_Radial_OnDisable_m2915400654 (CameraFilterPack_Blur_Radial_t566155114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
