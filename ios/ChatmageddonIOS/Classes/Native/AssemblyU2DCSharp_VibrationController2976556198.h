﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_MonoSingleton_1_gen2727221918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VibrationController
struct  VibrationController_t2976556198  : public MonoSingleton_1_t2727221918
{
public:
	// System.Boolean VibrationController::vibrateActive
	bool ___vibrateActive_3;

public:
	inline static int32_t get_offset_of_vibrateActive_3() { return static_cast<int32_t>(offsetof(VibrationController_t2976556198, ___vibrateActive_3)); }
	inline bool get_vibrateActive_3() const { return ___vibrateActive_3; }
	inline bool* get_address_of_vibrateActive_3() { return &___vibrateActive_3; }
	inline void set_vibrateActive_3(bool value)
	{
		___vibrateActive_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
