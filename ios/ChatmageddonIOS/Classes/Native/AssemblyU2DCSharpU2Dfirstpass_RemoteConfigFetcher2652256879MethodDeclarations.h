﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RemoteConfigFetcher
struct RemoteConfigFetcher_t2652256879;
// Uniject.IStorage
struct IStorage_t1347868490;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void RemoteConfigFetcher::.ctor()
extern "C"  void RemoteConfigFetcher__ctor_m2586583280 (RemoteConfigFetcher_t2652256879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteConfigFetcher::Start()
extern "C"  void RemoteConfigFetcher_Start_m248867840 (RemoteConfigFetcher_t2652256879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteConfigFetcher::Fetch(Uniject.IStorage,System.String,System.String)
extern "C"  void RemoteConfigFetcher_Fetch_m2067093102 (RemoteConfigFetcher_t2652256879 * __this, Il2CppObject * ___storage0, String_t* ___url1, String_t* ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RemoteConfigFetcher::fetch(Uniject.IStorage,System.String,System.String)
extern "C"  Il2CppObject * RemoteConfigFetcher_fetch_m4025920082 (RemoteConfigFetcher_t2652256879 * __this, Il2CppObject * ___storage0, String_t* ___url1, String_t* ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteConfigFetcher::log(System.String)
extern "C"  void RemoteConfigFetcher_log_m2252443704 (RemoteConfigFetcher_t2652256879 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
