﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CountryCodeListManager
struct CountryCodeListManager_t203952622;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoSingleton`1<CountryCodeListManager>
struct  MonoSingleton_1_t4249585638  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct MonoSingleton_1_t4249585638_StaticFields
{
public:
	// T MonoSingleton`1::m_Instance
	CountryCodeListManager_t203952622 * ___m_Instance_2;

public:
	inline static int32_t get_offset_of_m_Instance_2() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t4249585638_StaticFields, ___m_Instance_2)); }
	inline CountryCodeListManager_t203952622 * get_m_Instance_2() const { return ___m_Instance_2; }
	inline CountryCodeListManager_t203952622 ** get_address_of_m_Instance_2() { return &___m_Instance_2; }
	inline void set_m_Instance_2(CountryCodeListManager_t203952622 * value)
	{
		___m_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
