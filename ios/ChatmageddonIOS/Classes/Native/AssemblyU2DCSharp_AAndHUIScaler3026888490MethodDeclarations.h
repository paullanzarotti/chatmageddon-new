﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AAndHUIScaler
struct AAndHUIScaler_t3026888490;

#include "codegen/il2cpp-codegen.h"

// System.Void AAndHUIScaler::.ctor()
extern "C"  void AAndHUIScaler__ctor_m233223559 (AAndHUIScaler_t3026888490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AAndHUIScaler::GlobalUIScale()
extern "C"  void AAndHUIScaler_GlobalUIScale_m2906950864 (AAndHUIScaler_t3026888490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
