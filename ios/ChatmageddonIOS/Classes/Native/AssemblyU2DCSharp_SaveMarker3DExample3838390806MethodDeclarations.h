﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveMarker3DExample
struct SaveMarker3DExample_t3838390806;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveMarker3DExample::.ctor()
extern "C"  void SaveMarker3DExample__ctor_m4248960063 (SaveMarker3DExample_t3838390806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarker3DExample::OnGUI()
extern "C"  void SaveMarker3DExample_OnGUI_m3245405733 (SaveMarker3DExample_t3838390806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarker3DExample::OnMapClick()
extern "C"  void SaveMarker3DExample_OnMapClick_m458856386 (SaveMarker3DExample_t3838390806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarker3DExample::SaveMarkers()
extern "C"  void SaveMarker3DExample_SaveMarkers_m2107571843 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarker3DExample::Start()
extern "C"  void SaveMarker3DExample_Start_m3993906843 (SaveMarker3DExample_t3838390806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarker3DExample::TryLoadMarkers()
extern "C"  void SaveMarker3DExample_TryLoadMarkers_m2016057375 (SaveMarker3DExample_t3838390806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveMarker3DExample::.cctor()
extern "C"  void SaveMarker3DExample__cctor_m1322862212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
