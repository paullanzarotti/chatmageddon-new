﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchedMissile
struct LaunchedMissile_t1542515810;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MissileStatus1722383730.h"
#include "AssemblyU2DCSharp_MissileResultStatus3272208871.h"
#include "AssemblyU2DCSharp_WarefareState701596006.h"

// System.Void LaunchedMissile::.ctor(System.Collections.Hashtable)
extern "C"  void LaunchedMissile__ctor_m1140740047 (LaunchedMissile_t1542515810 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMissile::UpdateMissile(System.Collections.Hashtable)
extern "C"  void LaunchedMissile_UpdateMissile_m1188196666 (LaunchedMissile_t1542515810 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMissile::SetFriend(System.String)
extern "C"  void LaunchedMissile_SetFriend_m1856115689 (LaunchedMissile_t1542515810 * __this, String_t* ___userID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMissile::AddResult(System.Collections.Hashtable)
extern "C"  void LaunchedMissile_AddResult_m2540565927 (LaunchedMissile_t1542515810 * __this, Hashtable_t909839986 * ___resultData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMissile::AddDefend(System.Collections.Hashtable)
extern "C"  void LaunchedMissile_AddDefend_m378980006 (LaunchedMissile_t1542515810 * __this, Hashtable_t909839986 * ___defendData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMissile::CalculateDefendTimeString()
extern "C"  void LaunchedMissile_CalculateDefendTimeString_m1045055819 (LaunchedMissile_t1542515810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MissileStatus LaunchedMissile::GetStatusFromString(System.String)
extern "C"  int32_t LaunchedMissile_GetStatusFromString_m628624395 (LaunchedMissile_t1542515810 * __this, String_t* ___statusString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MissileResultStatus LaunchedMissile::GetResultStatusFromString(System.String)
extern "C"  int32_t LaunchedMissile_GetResultStatusFromString_m1561866255 (LaunchedMissile_t1542515810 * __this, String_t* ___statusString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WarefareState LaunchedMissile::GetWarfareStateFromString(System.String)
extern "C"  int32_t LaunchedMissile_GetWarfareStateFromString_m161073802 (LaunchedMissile_t1542515810 * __this, String_t* ___stateString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMissile::<SetFriend>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LaunchedMissile_U3CSetFriendU3Em__0_m3750795097 (LaunchedMissile_t1542515810 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
