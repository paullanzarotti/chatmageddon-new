﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldBarController
struct ShieldBarController_t264539450;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ShieldBarController::.ctor()
extern "C"  void ShieldBarController__ctor_m1456253457 (ShieldBarController_t264539450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldBarController::TransitionToPercent(System.Single,System.Single)
extern "C"  void ShieldBarController_TransitionToPercent_m688738464 (ShieldBarController_t264539450 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldBarController::ProgressUpdated()
extern "C"  void ShieldBarController_ProgressUpdated_m3945765693 (ShieldBarController_t264539450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldBarController::ProgressFinished()
extern "C"  void ShieldBarController_ProgressFinished_m4235341820 (ShieldBarController_t264539450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldBarController::SetTimeLabel(System.String,System.Boolean)
extern "C"  void ShieldBarController_SetTimeLabel_m956136529 (ShieldBarController_t264539450 * __this, String_t* ___timeString0, bool ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldBarController::SetShieldIcon(System.String)
extern "C"  void ShieldBarController_SetShieldIcon_m3589168213 (ShieldBarController_t264539450 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
