﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PNVerifyCodeButton
struct PNVerifyCodeButton_t868136506;

#include "codegen/il2cpp-codegen.h"

// System.Void PNVerifyCodeButton::.ctor()
extern "C"  void PNVerifyCodeButton__ctor_m1761105317 (PNVerifyCodeButton_t868136506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerifyCodeButton::Start()
extern "C"  void PNVerifyCodeButton_Start_m3447698341 (PNVerifyCodeButton_t868136506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerifyCodeButton::CheckCount()
extern "C"  void PNVerifyCodeButton_CheckCount_m1292650614 (PNVerifyCodeButton_t868136506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerifyCodeButton::OnButtonClick()
extern "C"  void PNVerifyCodeButton_OnButtonClick_m1872062158 (PNVerifyCodeButton_t868136506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerifyCodeButton::<Start>m__0()
extern "C"  void PNVerifyCodeButton_U3CStartU3Em__0_m363241276 (PNVerifyCodeButton_t868136506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
