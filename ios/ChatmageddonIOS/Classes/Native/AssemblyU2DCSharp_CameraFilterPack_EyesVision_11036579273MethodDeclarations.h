﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_EyesVision_1
struct CameraFilterPack_EyesVision_1_t1036579273;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_EyesVision_1::.ctor()
extern "C"  void CameraFilterPack_EyesVision_1__ctor_m2808658454 (CameraFilterPack_EyesVision_1_t1036579273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_EyesVision_1::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_EyesVision_1_get_material_m4210609365 (CameraFilterPack_EyesVision_1_t1036579273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::Start()
extern "C"  void CameraFilterPack_EyesVision_1_Start_m1509681934 (CameraFilterPack_EyesVision_1_t1036579273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_EyesVision_1_OnRenderImage_m1420323870 (CameraFilterPack_EyesVision_1_t1036579273 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::Update()
extern "C"  void CameraFilterPack_EyesVision_1_Update_m2124262577 (CameraFilterPack_EyesVision_1_t1036579273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::OnDisable()
extern "C"  void CameraFilterPack_EyesVision_1_OnDisable_m3951790837 (CameraFilterPack_EyesVision_1_t1036579273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
