﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// SettingsNavigationController
struct SettingsNavigationController_t2581944457;
// System.Object
struct Il2CppObject;
// SettingsNavigationController/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1
struct U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsNavigationController/<SaveScreenContent>c__Iterator0
struct  U3CSaveScreenContentU3Ec__Iterator0_t861775105  : public Il2CppObject
{
public:
	// UnityEngine.GameObject SettingsNavigationController/<SaveScreenContent>c__Iterator0::<saveObject>__0
	GameObject_t1756533147 * ___U3CsaveObjectU3E__0_0;
	// SettingsNavScreen SettingsNavigationController/<SaveScreenContent>c__Iterator0::screen
	int32_t ___screen_1;
	// NavigationDirection SettingsNavigationController/<SaveScreenContent>c__Iterator0::direction
	int32_t ___direction_2;
	// System.Action`1<System.Boolean> SettingsNavigationController/<SaveScreenContent>c__Iterator0::savedAction
	Action_1_t3627374100 * ___savedAction_3;
	// SettingsNavigationController SettingsNavigationController/<SaveScreenContent>c__Iterator0::$this
	SettingsNavigationController_t2581944457 * ___U24this_4;
	// System.Object SettingsNavigationController/<SaveScreenContent>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean SettingsNavigationController/<SaveScreenContent>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 SettingsNavigationController/<SaveScreenContent>c__Iterator0::$PC
	int32_t ___U24PC_7;
	// SettingsNavigationController/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1 SettingsNavigationController/<SaveScreenContent>c__Iterator0::$locvar0
	U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008 * ___U24locvar0_8;

public:
	inline static int32_t get_offset_of_U3CsaveObjectU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___U3CsaveObjectU3E__0_0)); }
	inline GameObject_t1756533147 * get_U3CsaveObjectU3E__0_0() const { return ___U3CsaveObjectU3E__0_0; }
	inline GameObject_t1756533147 ** get_address_of_U3CsaveObjectU3E__0_0() { return &___U3CsaveObjectU3E__0_0; }
	inline void set_U3CsaveObjectU3E__0_0(GameObject_t1756533147 * value)
	{
		___U3CsaveObjectU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsaveObjectU3E__0_0, value);
	}

	inline static int32_t get_offset_of_screen_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___screen_1)); }
	inline int32_t get_screen_1() const { return ___screen_1; }
	inline int32_t* get_address_of_screen_1() { return &___screen_1; }
	inline void set_screen_1(int32_t value)
	{
		___screen_1 = value;
	}

	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_savedAction_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___savedAction_3)); }
	inline Action_1_t3627374100 * get_savedAction_3() const { return ___savedAction_3; }
	inline Action_1_t3627374100 ** get_address_of_savedAction_3() { return &___savedAction_3; }
	inline void set_savedAction_3(Action_1_t3627374100 * value)
	{
		___savedAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___savedAction_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___U24this_4)); }
	inline SettingsNavigationController_t2581944457 * get_U24this_4() const { return ___U24this_4; }
	inline SettingsNavigationController_t2581944457 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SettingsNavigationController_t2581944457 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CSaveScreenContentU3Ec__Iterator0_t861775105, ___U24locvar0_8)); }
	inline U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
