﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En871448018.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21603735834.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2916059171_gshared (Enumerator_t871448018 * __this, Dictionary_2_t3846390612 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2916059171(__this, ___dictionary0, method) ((  void (*) (Enumerator_t871448018 *, Dictionary_2_t3846390612 *, const MethodInfo*))Enumerator__ctor_m2916059171_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3022940580_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3022940580(__this, method) ((  Il2CppObject * (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3022940580_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3754034472_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3754034472(__this, method) ((  void (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3754034472_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3367077649_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3367077649(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3367077649_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2653554246_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2653554246(__this, method) ((  Il2CppObject * (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2653554246_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2154534068_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2154534068(__this, method) ((  Il2CppObject * (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2154534068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2336684632_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2336684632(__this, method) ((  bool (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_MoveNext_m2336684632_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1603735834  Enumerator_get_Current_m3770553840_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3770553840(__this, method) ((  KeyValuePair_2_t1603735834  (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_get_Current_m3770553840_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3233614783_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3233614783(__this, method) ((  int32_t (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_get_CurrentKey_m3233614783_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3518794015_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3518794015(__this, method) ((  Il2CppObject * (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_get_CurrentValue_m3518794015_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3156758209_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3156758209(__this, method) ((  void (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_Reset_m3156758209_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m19120032_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m19120032(__this, method) ((  void (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_VerifyState_m19120032_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1522794816_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1522794816(__this, method) ((  void (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_VerifyCurrent_m1522794816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PanelType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m426295975_gshared (Enumerator_t871448018 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m426295975(__this, method) ((  void (*) (Enumerator_t871448018 *, const MethodInfo*))Enumerator_Dispose_m426295975_gshared)(__this, method)
