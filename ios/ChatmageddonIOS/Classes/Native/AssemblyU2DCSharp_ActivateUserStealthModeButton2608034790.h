﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActivateUserStealthModeButton
struct  ActivateUserStealthModeButton_t2608034790  : public SFXButton_t792651341
{
public:
	// System.Int32 ActivateUserStealthModeButton::delayMins
	int32_t ___delayMins_5;
	// System.Int32 ActivateUserStealthModeButton::durationHours
	int32_t ___durationHours_6;

public:
	inline static int32_t get_offset_of_delayMins_5() { return static_cast<int32_t>(offsetof(ActivateUserStealthModeButton_t2608034790, ___delayMins_5)); }
	inline int32_t get_delayMins_5() const { return ___delayMins_5; }
	inline int32_t* get_address_of_delayMins_5() { return &___delayMins_5; }
	inline void set_delayMins_5(int32_t value)
	{
		___delayMins_5 = value;
	}

	inline static int32_t get_offset_of_durationHours_6() { return static_cast<int32_t>(offsetof(ActivateUserStealthModeButton_t2608034790, ___durationHours_6)); }
	inline int32_t get_durationHours_6() const { return ___durationHours_6; }
	inline int32_t* get_address_of_durationHours_6() { return &___durationHours_6; }
	inline void set_durationHours_6(int32_t value)
	{
		___durationHours_6 = value;
	}
};

struct ActivateUserStealthModeButton_t2608034790_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ActivateUserStealthModeButton::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ActivateUserStealthModeButton_t2608034790_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
