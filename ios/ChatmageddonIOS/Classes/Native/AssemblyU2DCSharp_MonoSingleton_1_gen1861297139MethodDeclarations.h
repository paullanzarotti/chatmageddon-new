﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ApplicationManager>::.ctor()
#define MonoSingleton_1__ctor_m2592856061(__this, method) ((  void (*) (MonoSingleton_1_t1861297139 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ApplicationManager>::Awake()
#define MonoSingleton_1_Awake_m2870435182(__this, method) ((  void (*) (MonoSingleton_1_t1861297139 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ApplicationManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1331888220(__this /* static, unused */, method) ((  ApplicationManager_t2110631419 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ApplicationManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3117168828(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ApplicationManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1838024955(__this, method) ((  void (*) (MonoSingleton_1_t1861297139 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ApplicationManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2223738127(__this, method) ((  void (*) (MonoSingleton_1_t1861297139 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ApplicationManager>::.cctor()
#define MonoSingleton_1__cctor_m1856458280(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
