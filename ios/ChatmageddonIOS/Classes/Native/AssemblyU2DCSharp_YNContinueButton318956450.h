﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_NavigateForwardButton3075171100.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YNContinueButton
struct  YNContinueButton_t318956450  : public NavigateForwardButton_t3075171100
{
public:
	// UILabel YNContinueButton::countryCodeLabel
	UILabel_t1795115428 * ___countryCodeLabel_7;
	// UIInput YNContinueButton::phoneNumberLabel
	UIInput_t860674234 * ___phoneNumberLabel_8;

public:
	inline static int32_t get_offset_of_countryCodeLabel_7() { return static_cast<int32_t>(offsetof(YNContinueButton_t318956450, ___countryCodeLabel_7)); }
	inline UILabel_t1795115428 * get_countryCodeLabel_7() const { return ___countryCodeLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_countryCodeLabel_7() { return &___countryCodeLabel_7; }
	inline void set_countryCodeLabel_7(UILabel_t1795115428 * value)
	{
		___countryCodeLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeLabel_7, value);
	}

	inline static int32_t get_offset_of_phoneNumberLabel_8() { return static_cast<int32_t>(offsetof(YNContinueButton_t318956450, ___phoneNumberLabel_8)); }
	inline UIInput_t860674234 * get_phoneNumberLabel_8() const { return ___phoneNumberLabel_8; }
	inline UIInput_t860674234 ** get_address_of_phoneNumberLabel_8() { return &___phoneNumberLabel_8; }
	inline void set_phoneNumberLabel_8(UIInput_t860674234 * value)
	{
		___phoneNumberLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberLabel_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
