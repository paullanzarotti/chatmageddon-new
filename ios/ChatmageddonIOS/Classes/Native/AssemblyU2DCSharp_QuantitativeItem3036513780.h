﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ItemType3455197591.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuantitativeItem
struct  QuantitativeItem_t3036513780  : public Il2CppObject
{
public:
	// System.String QuantitativeItem::internalName
	String_t* ___internalName_0;
	// ItemType QuantitativeItem::itemType
	int32_t ___itemType_1;
	// System.Int32 QuantitativeItem::quanitity
	int32_t ___quanitity_2;
	// System.Int32 QuantitativeItem::duration
	int32_t ___duration_3;

public:
	inline static int32_t get_offset_of_internalName_0() { return static_cast<int32_t>(offsetof(QuantitativeItem_t3036513780, ___internalName_0)); }
	inline String_t* get_internalName_0() const { return ___internalName_0; }
	inline String_t** get_address_of_internalName_0() { return &___internalName_0; }
	inline void set_internalName_0(String_t* value)
	{
		___internalName_0 = value;
		Il2CppCodeGenWriteBarrier(&___internalName_0, value);
	}

	inline static int32_t get_offset_of_itemType_1() { return static_cast<int32_t>(offsetof(QuantitativeItem_t3036513780, ___itemType_1)); }
	inline int32_t get_itemType_1() const { return ___itemType_1; }
	inline int32_t* get_address_of_itemType_1() { return &___itemType_1; }
	inline void set_itemType_1(int32_t value)
	{
		___itemType_1 = value;
	}

	inline static int32_t get_offset_of_quanitity_2() { return static_cast<int32_t>(offsetof(QuantitativeItem_t3036513780, ___quanitity_2)); }
	inline int32_t get_quanitity_2() const { return ___quanitity_2; }
	inline int32_t* get_address_of_quanitity_2() { return &___quanitity_2; }
	inline void set_quanitity_2(int32_t value)
	{
		___quanitity_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(QuantitativeItem_t3036513780, ___duration_3)); }
	inline int32_t get_duration_3() const { return ___duration_3; }
	inline int32_t* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(int32_t value)
	{
		___duration_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
