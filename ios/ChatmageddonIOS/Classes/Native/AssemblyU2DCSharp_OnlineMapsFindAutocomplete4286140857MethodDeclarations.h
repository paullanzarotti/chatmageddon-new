﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsFindAutocomplete
struct OnlineMapsFindAutocomplete_t4286140857;
// System.String
struct String_t;
// OnlineMapsFindAutocompleteResult[]
struct OnlineMapsFindAutocompleteResultU5BU5D_t97057023;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"

// System.Void OnlineMapsFindAutocomplete::.ctor(System.String,System.String,System.String,System.Int32,UnityEngine.Vector2,System.Int32,System.String,System.String)
extern "C"  void OnlineMapsFindAutocomplete__ctor_m727615256 (OnlineMapsFindAutocomplete_t4286140857 * __this, String_t* ___input0, String_t* ___key1, String_t* ___types2, int32_t ___offset3, Vector2_t2243707579  ___latlng4, int32_t ___radius5, String_t* ___language6, String_t* ___components7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsFindAutocomplete::get_type()
extern "C"  int32_t OnlineMapsFindAutocomplete_get_type_m1254262760 (OnlineMapsFindAutocomplete_t4286140857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindAutocomplete OnlineMapsFindAutocomplete::Find(System.String,System.String,System.String,System.Int32,UnityEngine.Vector2,System.Int32,System.String,System.String)
extern "C"  OnlineMapsFindAutocomplete_t4286140857 * OnlineMapsFindAutocomplete_Find_m2285731843 (Il2CppObject * __this /* static, unused */, String_t* ___input0, String_t* ___key1, String_t* ___types2, int32_t ___offset3, Vector2_t2243707579  ___latlng4, int32_t ___radius5, String_t* ___language6, String_t* ___components7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindAutocompleteResult[] OnlineMapsFindAutocomplete::GetResults(System.String)
extern "C"  OnlineMapsFindAutocompleteResultU5BU5D_t97057023* OnlineMapsFindAutocomplete_GetResults_m2227048765 (Il2CppObject * __this /* static, unused */, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
