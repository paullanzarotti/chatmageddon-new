﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.XmlSerializer/GenerationBatch
struct GenerationBatch_t285680286;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Serialization.XmlSerializer/GenerationBatch::.ctor()
extern "C"  void GenerationBatch__ctor_m169607857 (GenerationBatch_t285680286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
