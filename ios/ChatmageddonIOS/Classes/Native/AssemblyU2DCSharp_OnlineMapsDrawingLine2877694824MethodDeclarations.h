﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsDrawingLine
struct OnlineMapsDrawingLine_t2877694824;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// OnlineMapsVector2i
struct OnlineMapsVector2i_t2180897250;
// OnlineMapsTileSetControl
struct OnlineMapsTileSetControl_t3368302803;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_OnlineMapsVector2i2180897250.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl3368302803.h"

// System.Void OnlineMapsDrawingLine::.ctor()
extern "C"  void OnlineMapsDrawingLine__ctor_m3886484749 (OnlineMapsDrawingLine_t2877694824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingLine::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void OnlineMapsDrawingLine__ctor_m2131558807 (OnlineMapsDrawingLine_t2877694824 * __this, List_1_t1612828711 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingLine::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Color)
extern "C"  void OnlineMapsDrawingLine__ctor_m2514097901 (OnlineMapsDrawingLine_t2877694824 * __this, List_1_t1612828711 * ___points0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingLine::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Color,System.Single)
extern "C"  void OnlineMapsDrawingLine__ctor_m1430893044 (OnlineMapsDrawingLine_t2877694824 * __this, List_1_t1612828711 * ___points0, Color_t2020392075  ___color1, float ___weight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingLine::Draw(UnityEngine.Color[],OnlineMapsVector2i,System.Int32,System.Int32,System.Int32)
extern "C"  void OnlineMapsDrawingLine_Draw_m476014458 (OnlineMapsDrawingLine_t2877694824 * __this, ColorU5BU5D_t672350442* ___buffer0, OnlineMapsVector2i_t2180897250 * ___bufferPosition1, int32_t ___bufferWidth2, int32_t ___bufferHeight3, int32_t ___zoom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingLine::DrawOnTileset(OnlineMapsTileSetControl)
extern "C"  void OnlineMapsDrawingLine_DrawOnTileset_m604165647 (OnlineMapsDrawingLine_t2877694824 * __this, OnlineMapsTileSetControl_t3368302803 * ___control0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
