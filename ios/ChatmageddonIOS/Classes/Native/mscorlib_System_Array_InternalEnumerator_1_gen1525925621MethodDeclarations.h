﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1525925621.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"

// System.Void System.Array/InternalEnumerator`1<RegNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1336450232_gshared (InternalEnumerator_1_t1525925621 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1336450232(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1525925621 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1336450232_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<RegNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1698827808_gshared (InternalEnumerator_1_t1525925621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1698827808(__this, method) ((  void (*) (InternalEnumerator_1_t1525925621 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1698827808_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<RegNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3757080524_gshared (InternalEnumerator_1_t1525925621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3757080524(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1525925621 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3757080524_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<RegNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4202220575_gshared (InternalEnumerator_1_t1525925621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4202220575(__this, method) ((  void (*) (InternalEnumerator_1_t1525925621 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4202220575_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<RegNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3987582608_gshared (InternalEnumerator_1_t1525925621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3987582608(__this, method) ((  bool (*) (InternalEnumerator_1_t1525925621 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3987582608_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<RegNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3008282343_gshared (InternalEnumerator_1_t1525925621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3008282343(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1525925621 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3008282343_gshared)(__this, method)
