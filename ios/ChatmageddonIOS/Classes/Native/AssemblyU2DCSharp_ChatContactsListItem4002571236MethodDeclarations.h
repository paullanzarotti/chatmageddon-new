﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatContactsListItem
struct ChatContactsListItem_t4002571236;
// ChatContact
struct ChatContact_t3760700014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatContact3760700014.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void ChatContactsListItem::.ctor()
extern "C"  void ChatContactsListItem__ctor_m2985471033 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::SetUpItem()
extern "C"  void ChatContactsListItem_SetUpItem_m4010067243 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::PopulateItem(ChatContact)
extern "C"  void ChatContactsListItem_PopulateItem_m1878921814 (ChatContactsListItem_t4002571236 * __this, ChatContact_t3760700014 * ___contactInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::SetVisible()
extern "C"  void ChatContactsListItem_SetVisible_m1159734949 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::SetInvisible()
extern "C"  void ChatContactsListItem_SetInvisible_m1018774112 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatContactsListItem::verifyVisibility()
extern "C"  bool ChatContactsListItem_verifyVisibility_m9431120 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::CheckVisibilty()
extern "C"  void ChatContactsListItem_CheckVisibilty_m1481656940 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::OnEnable()
extern "C"  void ChatContactsListItem_OnEnable_m4102581009 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::FindScrollView()
extern "C"  void ChatContactsListItem_FindScrollView_m2961002300 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::OnPress(System.Boolean)
extern "C"  void ChatContactsListItem_OnPress_m2313908882 (ChatContactsListItem_t4002571236 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void ChatContactsListItem_OnDrag_m2585992760 (ChatContactsListItem_t4002571236 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::OnDragEnd()
extern "C"  void ChatContactsListItem_OnDragEnd_m248996195 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::OnScroll(System.Single)
extern "C"  void ChatContactsListItem_OnScroll_m2828128084 (ChatContactsListItem_t4002571236 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItem::OnClick()
extern "C"  void ChatContactsListItem_OnClick_m2418977636 (ChatContactsListItem_t4002571236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
