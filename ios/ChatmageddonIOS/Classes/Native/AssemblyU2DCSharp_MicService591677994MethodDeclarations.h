﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MicService
struct MicService_t591677994;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void MicService::.ctor()
extern "C"  void MicService__ctor_m1671444385 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MicService::get_loudness()
extern "C"  float MicService_get_loudness_m818665003 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MicService::set_loudness(System.Single)
extern "C"  void MicService_set_loudness_m2201439032 (MicService_t591677994 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MicService::MicStart()
extern "C"  void MicService_MicStart_m2108389836 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MicService::MicHasPermission()
extern "C"  void MicService_MicHasPermission_m168954455 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MicService::StopMic()
extern "C"  void MicService_StopMic_m1841843254 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MicService::MicUpdate()
extern "C"  Il2CppObject * MicService_MicUpdate_m981153365 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MicService::GetAveragedVolume()
extern "C"  float MicService_GetAveragedVolume_m1670751572 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MicService::GetLoudness()
extern "C"  float MicService_GetLoudness_m446851300 (MicService_t591677994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
