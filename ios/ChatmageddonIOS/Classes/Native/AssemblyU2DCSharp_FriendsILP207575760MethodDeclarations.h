﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsILP
struct FriendsILP_t207575760;
// FriendsListItem
struct FriendsListItem_t521220246;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void FriendsILP::.ctor()
extern "C"  void FriendsILP__ctor_m3250017271 (FriendsILP_t207575760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::Start()
extern "C"  void FriendsILP_Start_m3456729467 (FriendsILP_t207575760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::StartIL()
extern "C"  void FriendsILP_StartIL_m414642058 (FriendsILP_t207575760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::DrawIL()
extern "C"  void FriendsILP_DrawIL_m1401276416 (FriendsILP_t207575760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::SetDataArray()
extern "C"  void FriendsILP_SetDataArray_m869235698 (FriendsILP_t207575760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::UpdateListData()
extern "C"  void FriendsILP_UpdateListData_m2420723430 (FriendsILP_t207575760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FriendsListItem FriendsILP::GetItemByUserID(System.String)
extern "C"  FriendsListItem_t521220246 * FriendsILP_GetItemByUserID_m2997296388 (FriendsILP_t207575760 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void FriendsILP_InitListItemWithIndex_m3642232153 (FriendsILP_t207575760 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void FriendsILP_PopulateListItemWithIndex_m2960104440 (FriendsILP_t207575760 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
