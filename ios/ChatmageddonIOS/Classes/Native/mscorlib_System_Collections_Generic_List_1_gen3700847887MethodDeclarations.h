﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AttackNavScreen>
struct List_1_t3700847887;
// System.Collections.Generic.IEnumerable`1<AttackNavScreen>
struct IEnumerable_1_t328886504;
// AttackNavScreen[]
struct AttackNavScreenU5BU5D_t3684257394;
// System.Collections.Generic.IEnumerator`1<AttackNavScreen>
struct IEnumerator_1_t1807250582;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<AttackNavScreen>
struct ICollection_1_t988834764;
// System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>
struct ReadOnlyCollection_1_t222545151;
// System.Predicate`1<AttackNavScreen>
struct Predicate_1_t2774696870;
// System.Action`1<AttackNavScreen>
struct Action_1_t4133526137;
// System.Collections.Generic.IComparer`1<AttackNavScreen>
struct IComparer_1_t2286189877;
// System.Comparison`1<AttackNavScreen>
struct Comparison_1_t1298498310;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3235577561.h"

// System.Void System.Collections.Generic.List`1<AttackNavScreen>::.ctor()
extern "C"  void List_1__ctor_m3235660295_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1__ctor_m3235660295(__this, method) ((  void (*) (List_1_t3700847887 *, const MethodInfo*))List_1__ctor_m3235660295_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1433204458_gshared (List_1_t3700847887 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1433204458(__this, ___collection0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1433204458_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1193840368_gshared (List_1_t3700847887 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1193840368(__this, ___capacity0, method) ((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1__ctor_m1193840368_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m4097133036_gshared (List_1_t3700847887 * __this, AttackNavScreenU5BU5D_t3684257394* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m4097133036(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3700847887 *, AttackNavScreenU5BU5D_t3684257394*, int32_t, const MethodInfo*))List_1__ctor_m4097133036_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::.cctor()
extern "C"  void List_1__cctor_m941250168_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m941250168(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m941250168_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3543912183_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3543912183(__this, method) ((  Il2CppObject* (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3543912183_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m668384299_gshared (List_1_t3700847887 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m668384299(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3700847887 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m668384299_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1839554966_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1839554966(__this, method) ((  Il2CppObject * (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1839554966_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3876449499_gshared (List_1_t3700847887 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3876449499(__this, ___item0, method) ((  int32_t (*) (List_1_t3700847887 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3876449499_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4248819203_gshared (List_1_t3700847887 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4248819203(__this, ___item0, method) ((  bool (*) (List_1_t3700847887 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4248819203_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3033587425_gshared (List_1_t3700847887 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3033587425(__this, ___item0, method) ((  int32_t (*) (List_1_t3700847887 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3033587425_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2501737044_gshared (List_1_t3700847887 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2501737044(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3700847887 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2501737044_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2367139922_gshared (List_1_t3700847887 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2367139922(__this, ___item0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2367139922_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m268747090_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m268747090(__this, method) ((  bool (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m268747090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m4170552371_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4170552371(__this, method) ((  bool (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m4170552371_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1341315947_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1341315947(__this, method) ((  Il2CppObject * (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1341315947_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1187886552_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1187886552(__this, method) ((  bool (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1187886552_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m435196079_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m435196079(__this, method) ((  bool (*) (List_1_t3700847887 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m435196079_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1433823604_gshared (List_1_t3700847887 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1433823604(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1433823604_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2122398177_gshared (List_1_t3700847887 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2122398177(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3700847887 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2122398177_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Add(T)
extern "C"  void List_1_Add_m1062492056_gshared (List_1_t3700847887 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1062492056(__this, ___item0, method) ((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_Add_m1062492056_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3375072727_gshared (List_1_t3700847887 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3375072727(__this, ___newCount0, method) ((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3375072727_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m4146670640_gshared (List_1_t3700847887 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m4146670640(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3700847887 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m4146670640_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3568314919_gshared (List_1_t3700847887 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3568314919(__this, ___collection0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3568314919_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1243223623_gshared (List_1_t3700847887 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1243223623(__this, ___enumerable0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1243223623_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3431493054_gshared (List_1_t3700847887 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3431493054(__this, ___collection0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3431493054_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AttackNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t222545151 * List_1_AsReadOnly_m508059847_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m508059847(__this, method) ((  ReadOnlyCollection_1_t222545151 * (*) (List_1_t3700847887 *, const MethodInfo*))List_1_AsReadOnly_m508059847_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Clear()
extern "C"  void List_1_Clear_m915343044_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_Clear_m915343044(__this, method) ((  void (*) (List_1_t3700847887 *, const MethodInfo*))List_1_Clear_m915343044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m3450822064_gshared (List_1_t3700847887 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3450822064(__this, ___item0, method) ((  bool (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_Contains_m3450822064_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m3775140951_gshared (List_1_t3700847887 * __this, AttackNavScreenU5BU5D_t3684257394* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m3775140951(__this, ___array0, method) ((  void (*) (List_1_t3700847887 *, AttackNavScreenU5BU5D_t3684257394*, const MethodInfo*))List_1_CopyTo_m3775140951_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2097977516_gshared (List_1_t3700847887 * __this, AttackNavScreenU5BU5D_t3684257394* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2097977516(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3700847887 *, AttackNavScreenU5BU5D_t3684257394*, int32_t, const MethodInfo*))List_1_CopyTo_m2097977516_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m480984766_gshared (List_1_t3700847887 * __this, int32_t ___index0, AttackNavScreenU5BU5D_t3684257394* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m480984766(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3700847887 *, int32_t, AttackNavScreenU5BU5D_t3684257394*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m480984766_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m3428145882_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_Exists_m3428145882(__this, ___match0, method) ((  bool (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_Exists_m3428145882_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<AttackNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1679295200_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_Find_m1679295200(__this, ___match0, method) ((  int32_t (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_Find_m1679295200_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m538758787_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m538758787(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2774696870 *, const MethodInfo*))List_1_CheckMatch_m538758787_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3700847887 * List_1_FindAll_m313609231_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m313609231(__this, ___match0, method) ((  List_1_t3700847887 * (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_FindAll_m313609231_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3700847887 * List_1_FindAllStackBits_m3233724013_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3233724013(__this, ___match0, method) ((  List_1_t3700847887 * (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_FindAllStackBits_m3233724013_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3700847887 * List_1_FindAllList_m3654817069_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m3654817069(__this, ___match0, method) ((  List_1_t3700847887 * (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_FindAllList_m3654817069_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m48433067_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m48433067(__this, ___match0, method) ((  int32_t (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_FindIndex_m48433067_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3448454096_gshared (List_1_t3700847887 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2774696870 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3448454096(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3700847887 *, int32_t, int32_t, Predicate_1_t2774696870 *, const MethodInfo*))List_1_GetIndex_m3448454096_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m3448139313_gshared (List_1_t3700847887 * __this, Action_1_t4133526137 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m3448139313(__this, ___action0, method) ((  void (*) (List_1_t3700847887 *, Action_1_t4133526137 *, const MethodInfo*))List_1_ForEach_m3448139313_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AttackNavScreen>::GetEnumerator()
extern "C"  Enumerator_t3235577561  List_1_GetEnumerator_m3843207495_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3843207495(__this, method) ((  Enumerator_t3235577561  (*) (List_1_t3700847887 *, const MethodInfo*))List_1_GetEnumerator_m3843207495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2429754722_gshared (List_1_t3700847887 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2429754722(__this, ___item0, method) ((  int32_t (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_IndexOf_m2429754722_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3254110967_gshared (List_1_t3700847887 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3254110967(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3700847887 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3254110967_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m273709196_gshared (List_1_t3700847887 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m273709196(__this, ___index0, method) ((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_CheckIndex_m273709196_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2828714253_gshared (List_1_t3700847887 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2828714253(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3700847887 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2828714253_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2267784714_gshared (List_1_t3700847887 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2267784714(__this, ___collection0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2267784714_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<AttackNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m398610783_gshared (List_1_t3700847887 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m398610783(__this, ___item0, method) ((  bool (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_Remove_m398610783_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2957544843_gshared (List_1_t3700847887 * __this, Predicate_1_t2774696870 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2957544843(__this, ___match0, method) ((  int32_t (*) (List_1_t3700847887 *, Predicate_1_t2774696870 *, const MethodInfo*))List_1_RemoveAll_m2957544843_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3913114313_gshared (List_1_t3700847887 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3913114313(__this, ___index0, method) ((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3913114313_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3731703728_gshared (List_1_t3700847887 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3731703728(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3700847887 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3731703728_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m18736159_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_Reverse_m18736159(__this, method) ((  void (*) (List_1_t3700847887 *, const MethodInfo*))List_1_Reverse_m18736159_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Sort()
extern "C"  void List_1_Sort_m371093237_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_Sort_m371093237(__this, method) ((  void (*) (List_1_t3700847887 *, const MethodInfo*))List_1_Sort_m371093237_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3775956657_gshared (List_1_t3700847887 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3775956657(__this, ___comparer0, method) ((  void (*) (List_1_t3700847887 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3775956657_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m527811936_gshared (List_1_t3700847887 * __this, Comparison_1_t1298498310 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m527811936(__this, ___comparison0, method) ((  void (*) (List_1_t3700847887 *, Comparison_1_t1298498310 *, const MethodInfo*))List_1_Sort_m527811936_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<AttackNavScreen>::ToArray()
extern "C"  AttackNavScreenU5BU5D_t3684257394* List_1_ToArray_m2577945672_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_ToArray_m2577945672(__this, method) ((  AttackNavScreenU5BU5D_t3684257394* (*) (List_1_t3700847887 *, const MethodInfo*))List_1_ToArray_m2577945672_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m711485502_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m711485502(__this, method) ((  void (*) (List_1_t3700847887 *, const MethodInfo*))List_1_TrimExcess_m711485502_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3193689092_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3193689092(__this, method) ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))List_1_get_Capacity_m3193689092_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2341130695_gshared (List_1_t3700847887 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2341130695(__this, ___value0, method) ((  void (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2341130695_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<AttackNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m2236462936_gshared (List_1_t3700847887 * __this, const MethodInfo* method);
#define List_1_get_Count_m2236462936(__this, method) ((  int32_t (*) (List_1_t3700847887 *, const MethodInfo*))List_1_get_Count_m2236462936_gshared)(__this, method)
// T System.Collections.Generic.List`1<AttackNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1879595359_gshared (List_1_t3700847887 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1879595359(__this, ___index0, method) ((  int32_t (*) (List_1_t3700847887 *, int32_t, const MethodInfo*))List_1_get_Item_m1879595359_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m999510814_gshared (List_1_t3700847887 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m999510814(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3700847887 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m999510814_gshared)(__this, ___index0, ___value1, method)
