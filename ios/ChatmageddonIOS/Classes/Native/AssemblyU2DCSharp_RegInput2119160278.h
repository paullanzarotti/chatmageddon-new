﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegInput
struct  RegInput_t2119160278  : public SFXButton_t792651341
{
public:
	// System.Boolean RegInput::inputActive
	bool ___inputActive_5;
	// UISprite RegInput::topBar
	UISprite_t603616735 * ___topBar_6;
	// UISprite RegInput::bottomBar
	UISprite_t603616735 * ___bottomBar_7;

public:
	inline static int32_t get_offset_of_inputActive_5() { return static_cast<int32_t>(offsetof(RegInput_t2119160278, ___inputActive_5)); }
	inline bool get_inputActive_5() const { return ___inputActive_5; }
	inline bool* get_address_of_inputActive_5() { return &___inputActive_5; }
	inline void set_inputActive_5(bool value)
	{
		___inputActive_5 = value;
	}

	inline static int32_t get_offset_of_topBar_6() { return static_cast<int32_t>(offsetof(RegInput_t2119160278, ___topBar_6)); }
	inline UISprite_t603616735 * get_topBar_6() const { return ___topBar_6; }
	inline UISprite_t603616735 ** get_address_of_topBar_6() { return &___topBar_6; }
	inline void set_topBar_6(UISprite_t603616735 * value)
	{
		___topBar_6 = value;
		Il2CppCodeGenWriteBarrier(&___topBar_6, value);
	}

	inline static int32_t get_offset_of_bottomBar_7() { return static_cast<int32_t>(offsetof(RegInput_t2119160278, ___bottomBar_7)); }
	inline UISprite_t603616735 * get_bottomBar_7() const { return ___bottomBar_7; }
	inline UISprite_t603616735 ** get_address_of_bottomBar_7() { return &___bottomBar_7; }
	inline void set_bottomBar_7(UISprite_t603616735 * value)
	{
		___bottomBar_7 = value;
		Il2CppCodeGenWriteBarrier(&___bottomBar_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
