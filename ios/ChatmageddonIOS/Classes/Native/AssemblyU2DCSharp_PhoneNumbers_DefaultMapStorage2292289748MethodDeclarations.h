﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.DefaultMapStorage
struct DefaultMapStorage_t2292289748;
// System.String
struct String_t;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>
struct SortedDictionary_2_t2686657757;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.DefaultMapStorage::.ctor()
extern "C"  void DefaultMapStorage__ctor_m1198294435 (DefaultMapStorage_t2292289748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.DefaultMapStorage::getPrefix(System.Int32)
extern "C"  int32_t DefaultMapStorage_getPrefix_m2548715414 (DefaultMapStorage_t2292289748 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.DefaultMapStorage::getStorageSize()
extern "C"  int32_t DefaultMapStorage_getStorageSize_m3020106295 (DefaultMapStorage_t2292289748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.DefaultMapStorage::getDescription(System.Int32)
extern "C"  String_t* DefaultMapStorage_getDescription_m1665785335 (DefaultMapStorage_t2292289748 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.DefaultMapStorage::readFromSortedMap(System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>)
extern "C"  void DefaultMapStorage_readFromSortedMap_m1895787471 (DefaultMapStorage_t2292289748 * __this, SortedDictionary_2_t2686657757 * ___sortedAreaCodeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.DefaultMapStorage::<getStorageSize>m__0(System.String)
extern "C"  int32_t DefaultMapStorage_U3CgetStorageSizeU3Em__0_m4130890320 (Il2CppObject * __this /* static, unused */, String_t* ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
