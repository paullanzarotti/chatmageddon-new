﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeTypeMember
struct CodeTypeMember_t3928690109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_MemberAttributes1656695347.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.CodeDom.CodeTypeMember::.ctor()
extern "C"  void CodeTypeMember__ctor_m3941599393 (CodeTypeMember_t3928690109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.CodeTypeMember::set_Attributes(System.CodeDom.MemberAttributes)
extern "C"  void CodeTypeMember_set_Attributes_m51347044 (CodeTypeMember_t3928690109 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.CodeTypeMember::set_Name(System.String)
extern "C"  void CodeTypeMember_set_Name_m236917537 (CodeTypeMember_t3928690109 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
