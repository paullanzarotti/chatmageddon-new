﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayerProfileNavScreen>
struct DefaultComparer_t4282598420;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayerProfileNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m1896065985_gshared (DefaultComparer_t4282598420 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1896065985(__this, method) ((  void (*) (DefaultComparer_t4282598420 *, const MethodInfo*))DefaultComparer__ctor_m1896065985_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayerProfileNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2146514272_gshared (DefaultComparer_t4282598420 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2146514272(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4282598420 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2146514272_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayerProfileNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m179084192_gshared (DefaultComparer_t4282598420 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m179084192(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4282598420 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m179084192_gshared)(__this, ___x0, ___y1, method)
