﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<SettingsSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m1341661872(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m1975413477(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::Start()
#define BaseSceneManager_1_Start_m220701040(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m3551235162(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m3183440786(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m2763665414(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<SettingsSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m1643125832(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m3876769923(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<SettingsSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m4230740572(__this, method) ((  void (*) (BaseSceneManager_1_t3239421166 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
