﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen895511721.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"

// System.Void System.Array/InternalEnumerator`1<AttackNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3796537098_gshared (InternalEnumerator_1_t895511721 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3796537098(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t895511721 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3796537098_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AttackNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3883637026_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3883637026(__this, method) ((  void (*) (InternalEnumerator_1_t895511721 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3883637026_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AttackNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m285100348_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m285100348(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t895511721 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m285100348_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AttackNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3830106263_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3830106263(__this, method) ((  void (*) (InternalEnumerator_1_t895511721 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3830106263_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AttackNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m762896482_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m762896482(__this, method) ((  bool (*) (InternalEnumerator_1_t895511721 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m762896482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AttackNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m875940467_gshared (InternalEnumerator_1_t895511721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m875940467(__this, method) ((  int32_t (*) (InternalEnumerator_1_t895511721 *, const MethodInfo*))InternalEnumerator_1_get_Current_m875940467_gshared)(__this, method)
