﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StealthModeUIScaler
struct  StealthModeUIScaler_t786227838  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite StealthModeUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider StealthModeUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UISprite StealthModeUIScaler::exitButton
	UISprite_t603616735 * ___exitButton_16;
	// UISprite StealthModeUIScaler::exitButtonDivider
	UISprite_t603616735 * ___exitButtonDivider_17;
	// UISprite StealthModeUIScaler::confirmBackground
	UISprite_t603616735 * ___confirmBackground_18;
	// UnityEngine.BoxCollider StealthModeUIScaler::confirmBackgroundCollider
	BoxCollider_t22920061 * ___confirmBackgroundCollider_19;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(StealthModeUIScaler_t786227838, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(StealthModeUIScaler_t786227838, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_exitButton_16() { return static_cast<int32_t>(offsetof(StealthModeUIScaler_t786227838, ___exitButton_16)); }
	inline UISprite_t603616735 * get_exitButton_16() const { return ___exitButton_16; }
	inline UISprite_t603616735 ** get_address_of_exitButton_16() { return &___exitButton_16; }
	inline void set_exitButton_16(UISprite_t603616735 * value)
	{
		___exitButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___exitButton_16, value);
	}

	inline static int32_t get_offset_of_exitButtonDivider_17() { return static_cast<int32_t>(offsetof(StealthModeUIScaler_t786227838, ___exitButtonDivider_17)); }
	inline UISprite_t603616735 * get_exitButtonDivider_17() const { return ___exitButtonDivider_17; }
	inline UISprite_t603616735 ** get_address_of_exitButtonDivider_17() { return &___exitButtonDivider_17; }
	inline void set_exitButtonDivider_17(UISprite_t603616735 * value)
	{
		___exitButtonDivider_17 = value;
		Il2CppCodeGenWriteBarrier(&___exitButtonDivider_17, value);
	}

	inline static int32_t get_offset_of_confirmBackground_18() { return static_cast<int32_t>(offsetof(StealthModeUIScaler_t786227838, ___confirmBackground_18)); }
	inline UISprite_t603616735 * get_confirmBackground_18() const { return ___confirmBackground_18; }
	inline UISprite_t603616735 ** get_address_of_confirmBackground_18() { return &___confirmBackground_18; }
	inline void set_confirmBackground_18(UISprite_t603616735 * value)
	{
		___confirmBackground_18 = value;
		Il2CppCodeGenWriteBarrier(&___confirmBackground_18, value);
	}

	inline static int32_t get_offset_of_confirmBackgroundCollider_19() { return static_cast<int32_t>(offsetof(StealthModeUIScaler_t786227838, ___confirmBackgroundCollider_19)); }
	inline BoxCollider_t22920061 * get_confirmBackgroundCollider_19() const { return ___confirmBackgroundCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_confirmBackgroundCollider_19() { return &___confirmBackgroundCollider_19; }
	inline void set_confirmBackgroundCollider_19(BoxCollider_t22920061 * value)
	{
		___confirmBackgroundCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___confirmBackgroundCollider_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
