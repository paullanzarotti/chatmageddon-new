﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FAQILP
struct FAQILP_t1146604377;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void FAQILP::.ctor()
extern "C"  void FAQILP__ctor_m146123930 (FAQILP_t1146604377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQILP::Start()
extern "C"  void FAQILP_Start_m3284838906 (FAQILP_t1146604377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQILP::StartIL()
extern "C"  void FAQILP_StartIL_m3332189921 (FAQILP_t1146604377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQILP::SetDataArray()
extern "C"  void FAQILP_SetDataArray_m565857903 (FAQILP_t1146604377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void FAQILP_InitListItemWithIndex_m2721307044 (FAQILP_t1146604377 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void FAQILP_PopulateListItemWithIndex_m3690250639 (FAQILP_t1146604377 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
