﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventTrigger
struct UIEventTrigger_t1351778058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void UIEventTrigger::.ctor()
extern "C"  void UIEventTrigger__ctor_m2189288671 (UIEventTrigger_t1351778058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventTrigger::get_isColliderEnabled()
extern "C"  bool UIEventTrigger_get_isColliderEnabled_m4105292887 (UIEventTrigger_t1351778058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnHover(System.Boolean)
extern "C"  void UIEventTrigger_OnHover_m1475368379 (UIEventTrigger_t1351778058 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnPress(System.Boolean)
extern "C"  void UIEventTrigger_OnPress_m443793192 (UIEventTrigger_t1351778058 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnSelect(System.Boolean)
extern "C"  void UIEventTrigger_OnSelect_m791923961 (UIEventTrigger_t1351778058 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnClick()
extern "C"  void UIEventTrigger_OnClick_m952412442 (UIEventTrigger_t1351778058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnDoubleClick()
extern "C"  void UIEventTrigger_OnDoubleClick_m497082357 (UIEventTrigger_t1351778058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnDragStart()
extern "C"  void UIEventTrigger_OnDragStart_m2983871096 (UIEventTrigger_t1351778058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnDragEnd()
extern "C"  void UIEventTrigger_OnDragEnd_m4126013017 (UIEventTrigger_t1351778058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnDragOver(UnityEngine.GameObject)
extern "C"  void UIEventTrigger_OnDragOver_m2750646654 (UIEventTrigger_t1351778058 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnDragOut(UnityEngine.GameObject)
extern "C"  void UIEventTrigger_OnDragOut_m1824136486 (UIEventTrigger_t1351778058 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTrigger::OnDrag(UnityEngine.Vector2)
extern "C"  void UIEventTrigger_OnDrag_m4134929726 (UIEventTrigger_t1351778058 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
