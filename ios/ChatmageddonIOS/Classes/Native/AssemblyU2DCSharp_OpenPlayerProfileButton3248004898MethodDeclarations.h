﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenPlayerProfileButton
struct OpenPlayerProfileButton_t3248004898;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenPlayerProfileButton::.ctor()
extern "C"  void OpenPlayerProfileButton__ctor_m1234326621 (OpenPlayerProfileButton_t3248004898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenPlayerProfileButton::OnButtonClick()
extern "C"  void OpenPlayerProfileButton_OnButtonClick_m1773924954 (OpenPlayerProfileButton_t3248004898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
