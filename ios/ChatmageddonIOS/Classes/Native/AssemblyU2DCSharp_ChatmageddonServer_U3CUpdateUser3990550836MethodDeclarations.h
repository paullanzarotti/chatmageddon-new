﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserEmail>c__AnonStoreyF
struct U3CUpdateUserEmailU3Ec__AnonStoreyF_t3990550836;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserEmail>c__AnonStoreyF::.ctor()
extern "C"  void U3CUpdateUserEmailU3Ec__AnonStoreyF__ctor_m970312363 (U3CUpdateUserEmailU3Ec__AnonStoreyF_t3990550836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserEmail>c__AnonStoreyF::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserEmailU3Ec__AnonStoreyF_U3CU3Em__0_m98352904 (U3CUpdateUserEmailU3Ec__AnonStoreyF_t3990550836 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
