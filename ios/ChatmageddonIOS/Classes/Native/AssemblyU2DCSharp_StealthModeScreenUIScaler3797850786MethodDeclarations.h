﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeScreenUIScaler
struct StealthModeScreenUIScaler_t3797850786;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeScreenUIScaler::.ctor()
extern "C"  void StealthModeScreenUIScaler__ctor_m3176818569 (StealthModeScreenUIScaler_t3797850786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeScreenUIScaler::GlobalUIScale()
extern "C"  void StealthModeScreenUIScaler_GlobalUIScale_m2110362396 (StealthModeScreenUIScaler_t3797850786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
