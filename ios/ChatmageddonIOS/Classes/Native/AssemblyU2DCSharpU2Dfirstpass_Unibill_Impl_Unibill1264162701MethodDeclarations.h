﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.UnibillConfiguration/<UnibillConfiguration>c__AnonStorey0
struct U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701;
// PurchasableItem
struct PurchasableItem_t3963353899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void Unibill.Impl.UnibillConfiguration/<UnibillConfiguration>c__AnonStorey0::.ctor()
extern "C"  void U3CUnibillConfigurationU3Ec__AnonStorey0__ctor_m3242901060 (U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration/<UnibillConfiguration>c__AnonStorey0::<>m__0(PurchasableItem)
extern "C"  bool U3CUnibillConfigurationU3Ec__AnonStorey0_U3CU3Em__0_m3921943260 (U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701 * __this, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
