﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumberResendCode
struct PhoneNumberResendCode_t1736900895;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumberResendCode::.ctor()
extern "C"  void PhoneNumberResendCode__ctor_m2765112926 (PhoneNumberResendCode_t1736900895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberResendCode::OnButtonClick()
extern "C"  void PhoneNumberResendCode_OnButtonClick_m1708846391 (PhoneNumberResendCode_t1736900895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberResendCode::<OnButtonClick>__BaseCallProxy0()
extern "C"  void PhoneNumberResendCode_U3COnButtonClickU3E__BaseCallProxy0_m1727833610 (PhoneNumberResendCode_t1736900895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberResendCode::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void PhoneNumberResendCode_U3COnButtonClickU3Em__0_m2657195337 (PhoneNumberResendCode_t1736900895 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
