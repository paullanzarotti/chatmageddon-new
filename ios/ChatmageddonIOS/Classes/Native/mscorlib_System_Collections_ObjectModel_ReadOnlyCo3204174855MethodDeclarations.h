﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>
struct ReadOnlyCollection_1_t3204174855;
// System.Collections.Generic.IList`1<FriendSearchNavScreen>
struct IList_1_t3559329764;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FriendSearchNavScreen[]
struct FriendSearchNavScreenU5BU5D_t3949017674;
// System.Collections.Generic.IEnumerator`1<FriendSearchNavScreen>
struct IEnumerator_1_t493912990;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m135458469_gshared (ReadOnlyCollection_1_t3204174855 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m135458469(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m135458469_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2182572891_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2182572891(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2182572891_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1433240559_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1433240559(__this, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1433240559_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m784539674_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m784539674(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m784539674_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1797536564_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1797536564(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1797536564_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m276855830_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m276855830(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m276855830_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m497451304_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m497451304(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m497451304_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2686341879_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2686341879(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2686341879_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m146461515_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m146461515(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m146461515_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m249082528_gshared (ReadOnlyCollection_1_t3204174855 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m249082528(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m249082528_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2543390271_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2543390271(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2543390271_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3290020604_gshared (ReadOnlyCollection_1_t3204174855 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3290020604(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3204174855 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3290020604_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2269143910_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2269143910(__this, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2269143910_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m577002674_gshared (ReadOnlyCollection_1_t3204174855 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m577002674(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m577002674_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1787915126_gshared (ReadOnlyCollection_1_t3204174855 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1787915126(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3204174855 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1787915126_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m670793135_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m670793135(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m670793135_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m949266399_gshared (ReadOnlyCollection_1_t3204174855 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m949266399(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m949266399_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3601695453_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3601695453(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3601695453_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3976634796_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3976634796(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3976634796_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3329071664_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3329071664(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3329071664_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2410819703_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2410819703(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2410819703_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2499190720_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2499190720(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2499190720_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m786355383_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m786355383(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m786355383_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1038487146_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1038487146(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1038487146_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3583997569_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3583997569(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3583997569_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m108830639_gshared (ReadOnlyCollection_1_t3204174855 * __this, FriendSearchNavScreenU5BU5D_t3949017674* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m108830639(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3204174855 *, FriendSearchNavScreenU5BU5D_t3949017674*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m108830639_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2449147440_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2449147440(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2449147440_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1927407291_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1927407291(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1927407291_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1887180108_gshared (ReadOnlyCollection_1_t3204174855 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1887180108(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3204174855 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1887180108_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3943242092_gshared (ReadOnlyCollection_1_t3204174855 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3943242092(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3204174855 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3943242092_gshared)(__this, ___index0, method)
