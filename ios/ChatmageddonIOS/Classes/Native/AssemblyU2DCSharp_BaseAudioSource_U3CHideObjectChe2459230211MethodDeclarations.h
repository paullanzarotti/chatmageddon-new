﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseAudioSource/<HideObjectCheck>c__Iterator0
struct U3CHideObjectCheckU3Ec__Iterator0_t2459230211;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseAudioSource/<HideObjectCheck>c__Iterator0::.ctor()
extern "C"  void U3CHideObjectCheckU3Ec__Iterator0__ctor_m194804636 (U3CHideObjectCheckU3Ec__Iterator0_t2459230211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseAudioSource/<HideObjectCheck>c__Iterator0::MoveNext()
extern "C"  bool U3CHideObjectCheckU3Ec__Iterator0_MoveNext_m385636336 (U3CHideObjectCheckU3Ec__Iterator0_t2459230211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BaseAudioSource/<HideObjectCheck>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHideObjectCheckU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3882286014 (U3CHideObjectCheckU3Ec__Iterator0_t2459230211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BaseAudioSource/<HideObjectCheck>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHideObjectCheckU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m336299286 (U3CHideObjectCheckU3Ec__Iterator0_t2459230211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource/<HideObjectCheck>c__Iterator0::Dispose()
extern "C"  void U3CHideObjectCheckU3Ec__Iterator0_Dispose_m2539023541 (U3CHideObjectCheckU3Ec__Iterator0_t2459230211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource/<HideObjectCheck>c__Iterator0::Reset()
extern "C"  void U3CHideObjectCheckU3Ec__Iterator0_Reset_m1074142759 (U3CHideObjectCheckU3Ec__Iterator0_t2459230211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
