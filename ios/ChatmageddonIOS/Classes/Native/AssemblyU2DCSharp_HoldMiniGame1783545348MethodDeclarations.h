﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HoldMiniGame
struct HoldMiniGame_t1783545348;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void HoldMiniGame::.ctor()
extern "C"  void HoldMiniGame__ctor_m3827303633 (HoldMiniGame_t1783545348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HoldMiniGame::StartMiniGame()
extern "C"  void HoldMiniGame_StartMiniGame_m971595698 (HoldMiniGame_t1783545348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HoldMiniGame::StopMiniGame()
extern "C"  void HoldMiniGame_StopMiniGame_m1069745830 (HoldMiniGame_t1783545348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HoldMiniGame::OnPaused()
extern "C"  void HoldMiniGame_OnPaused_m3984506772 (HoldMiniGame_t1783545348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HoldMiniGame::ResetMiniGame()
extern "C"  void HoldMiniGame_ResetMiniGame_m2650209441 (HoldMiniGame_t1783545348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HoldMiniGame::TimerUpdated(System.TimeSpan)
extern "C"  void HoldMiniGame_TimerUpdated_m1291736589 (HoldMiniGame_t1783545348 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HoldMiniGame::UpdateModel()
extern "C"  void HoldMiniGame_UpdateModel_m2744593375 (HoldMiniGame_t1783545348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
