﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_DecimalConverter1618403211.h"
#include "System_System_ComponentModel_DefaultBindingPropert2993619957.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1962767338.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DerivedPropertyDescri1502098785.h"
#include "System_System_ComponentModel_DescriptionAttribute3207779672.h"
#include "System_System_ComponentModel_Design_DesignerTransa3880697857.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903.h"
#include "System_System_ComponentModel_DesignOnlyAttribute2394309572.h"
#include "System_System_ComponentModel_Design_RuntimeLicense1397748562.h"
#include "System_System_ComponentModel_Design_Serialization_2188593799.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "System_System_ComponentModel_Design_Serialization_1162957127.h"
#include "System_System_ComponentModel_DesignTimeVisibleAttr2120749151.h"
#include "System_System_ComponentModel_DisplayNameAttribute1935407093.h"
#include "System_System_ComponentModel_DoubleConverter864652623.h"
#include "System_System_ComponentModel_DoWorkEventArgs62745097.h"
#include "System_System_ComponentModel_EditorAttribute3559776959.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_EnumConverter2538808523.h"
#include "System_System_ComponentModel_EnumConverter_EnumCom2635778853.h"
#include "System_System_ComponentModel_EventDescriptorCollec3053042509.h"
#include "System_System_ComponentModel_EventDescriptor962731901.h"
#include "System_System_ComponentModel_ListEntry935815304.h"
#include "System_System_ComponentModel_EventHandlerList1298116880.h"
#include "System_System_ComponentModel_ExpandableObjectConve1139197709.h"
#include "System_System_ComponentModel_ExtenderProvidedPrope3223729015.h"
#include "System_System_ComponentModel_GuidConverter1547586607.h"
#include "System_System_ComponentModel_HandledEventArgs1685735245.h"
#include "System_System_ComponentModel_ImmutableObjectAttrib1990201979.h"
#include "System_System_ComponentModel_InheritanceAttribute2851181518.h"
#include "System_System_ComponentModel_InheritanceLevel4275660400.h"
#include "System_System_ComponentModel_InitializationEventAt4110451806.h"
#include "System_System_ComponentModel_InstallerTypeAttribut2978264484.h"
#include "System_System_ComponentModel_InstanceCreationEdito1706597557.h"
#include "System_System_ComponentModel_InvalidAsynchronousSta707889517.h"
#include "System_System_ComponentModel_Int16Converter903627590.h"
#include "System_System_ComponentModel_Int32Converter957938388.h"
#include "System_System_ComponentModel_Int64Converter3186343659.h"
#include "System_System_ComponentModel_InvalidEnumArgumentEx3709744516.h"
#include "System_System_ComponentModel_LicenseContext192650050.h"
#include "System_System_ComponentModel_License3326651051.h"
#include "System_System_ComponentModel_LicenseException1281499302.h"
#include "System_System_ComponentModel_LicenseManager764408642.h"
#include "System_System_ComponentModel_LicenseProviderAttrib1207066332.h"
#include "System_System_ComponentModel_LicenseProvider3536803528.h"
#include "System_System_ComponentModel_LicenseUsageMode2996119499.h"
#include "System_System_ComponentModel_LicFileLicenseProvide2962157156.h"
#include "System_System_ComponentModel_LicFileLicense1222627691.h"
#include "System_System_ComponentModel_ListBindableAttribute1092011273.h"
#include "System_System_ComponentModel_ListChangedEventArgs3132270315.h"
#include "System_System_ComponentModel_ListChangedType3463990274.h"
#include "System_System_ComponentModel_ListSortDescriptionCo2357028590.h"
#include "System_System_ComponentModel_ListSortDescription3194554012.h"
#include "System_System_ComponentModel_ListSortDirection4186912589.h"
#include "System_System_ComponentModel_LocalizableAttribute1267247394.h"
#include "System_System_ComponentModel_LookupBindingProperti2232070260.h"
#include "System_System_ComponentModel_MarshalByValueCompone3997823175.h"
#include "System_System_ComponentModel_MaskedTextProvider2280059057.h"
#include "System_System_ComponentModel_MaskedTextProvider_Edit81707772.h"
#include "System_System_ComponentModel_MaskedTextProvider_Ed3003187841.h"
#include "System_System_ComponentModel_MaskedTextProvider_Ed3912732320.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (DecimalConverter_t1618403211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (DefaultBindingPropertyAttribute_t2993619957), -1, sizeof(DefaultBindingPropertyAttribute_t2993619957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[2] = 
{
	DefaultBindingPropertyAttribute_t2993619957_StaticFields::get_offset_of_Default_0(),
	DefaultBindingPropertyAttribute_t2993619957::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (DefaultEventAttribute_t1079704873), -1, sizeof(DefaultEventAttribute_t1079704873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1902[2] = 
{
	DefaultEventAttribute_t1079704873::get_offset_of_eventName_0(),
	DefaultEventAttribute_t1079704873_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (DefaultPropertyAttribute_t1962767338), -1, sizeof(DefaultPropertyAttribute_t1962767338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1903[2] = 
{
	DefaultPropertyAttribute_t1962767338::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t1962767338_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (DefaultValueAttribute_t1302720498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[1] = 
{
	DefaultValueAttribute_t1302720498::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (DerivedPropertyDescriptor_t1502098785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[4] = 
{
	DerivedPropertyDescriptor_t1502098785::get_offset_of_readOnly_6(),
	DerivedPropertyDescriptor_t1502098785::get_offset_of_componentType_7(),
	DerivedPropertyDescriptor_t1502098785::get_offset_of_propertyType_8(),
	DerivedPropertyDescriptor_t1502098785::get_offset_of_prop_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (DescriptionAttribute_t3207779672), -1, sizeof(DescriptionAttribute_t3207779672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1906[2] = 
{
	DescriptionAttribute_t3207779672::get_offset_of_desc_0(),
	DescriptionAttribute_t3207779672_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (DesignerTransaction_t3880697857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[2] = 
{
	DesignerTransaction_t3880697857::get_offset_of_committed_0(),
	DesignerTransaction_t3880697857::get_offset_of_canceled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (DesignerAttribute_t2778719479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[2] = 
{
	DesignerAttribute_t2778719479::get_offset_of_name_0(),
	DesignerAttribute_t2778719479::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (DesignerCategoryAttribute_t1270090451), -1, sizeof(DesignerCategoryAttribute_t1270090451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1909[5] = 
{
	DesignerCategoryAttribute_t1270090451::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (DesignerSerializationVisibilityAttribute_t2980019899), -1, sizeof(DesignerSerializationVisibilityAttribute_t2980019899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1910[5] = 
{
	DesignerSerializationVisibilityAttribute_t2980019899::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (DesignerSerializationVisibility_t3751360903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1911[4] = 
{
	DesignerSerializationVisibility_t3751360903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (DesignOnlyAttribute_t2394309572), -1, sizeof(DesignOnlyAttribute_t2394309572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1920[4] = 
{
	DesignOnlyAttribute_t2394309572::get_offset_of_design_only_0(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_Default_1(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_No_2(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (RuntimeLicenseContext_t1397748562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[2] = 
{
	RuntimeLicenseContext_t1397748562::get_offset_of_extraassemblies_0(),
	RuntimeLicenseContext_t1397748562::get_offset_of_keys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (DesignerSerializerAttribute_t2188593799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[2] = 
{
	DesignerSerializerAttribute_t2188593799::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t2188593799::get_offset_of_baseSerializerTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (InstanceDescriptor_t1404033120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[3] = 
{
	InstanceDescriptor_t1404033120::get_offset_of_member_0(),
	InstanceDescriptor_t1404033120::get_offset_of_arguments_1(),
	InstanceDescriptor_t1404033120::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (RootDesignerSerializerAttribute_t1162957127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[3] = 
{
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_serializer_0(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_baseserializer_1(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_reload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (DesignTimeVisibleAttribute_t2120749151), -1, sizeof(DesignTimeVisibleAttribute_t2120749151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1925[4] = 
{
	DesignTimeVisibleAttribute_t2120749151::get_offset_of_visible_0(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_Default_1(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_No_2(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (DisplayNameAttribute_t1935407093), -1, sizeof(DisplayNameAttribute_t1935407093_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1926[2] = 
{
	DisplayNameAttribute_t1935407093_StaticFields::get_offset_of_Default_0(),
	DisplayNameAttribute_t1935407093::get_offset_of_attributeDisplayName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (DoubleConverter_t864652623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (DoWorkEventArgs_t62745097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	DoWorkEventArgs_t62745097::get_offset_of_arg_2(),
	DoWorkEventArgs_t62745097::get_offset_of_result_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (EditorAttribute_t3559776959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[2] = 
{
	EditorAttribute_t3559776959::get_offset_of_name_0(),
	EditorAttribute_t3559776959::get_offset_of_basename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (EditorBrowsableAttribute_t1050682502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[1] = 
{
	EditorBrowsableAttribute_t1050682502::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (EditorBrowsableState_t373498655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[4] = 
{
	EditorBrowsableState_t373498655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (EnumConverter_t2538808523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[2] = 
{
	EnumConverter_t2538808523::get_offset_of_type_0(),
	EnumConverter_t2538808523::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (EnumComparer_t2635778853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (EventDescriptorCollection_t3053042509), -1, sizeof(EventDescriptorCollection_t3053042509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1934[3] = 
{
	EventDescriptorCollection_t3053042509::get_offset_of_eventList_0(),
	EventDescriptorCollection_t3053042509::get_offset_of_isReadOnly_1(),
	EventDescriptorCollection_t3053042509_StaticFields::get_offset_of_Empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (EventDescriptor_t962731901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (ListEntry_t935815304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[3] = 
{
	ListEntry_t935815304::get_offset_of_key_0(),
	ListEntry_t935815304::get_offset_of_value_1(),
	ListEntry_t935815304::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (EventHandlerList_t1298116880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[2] = 
{
	EventHandlerList_t1298116880::get_offset_of_entries_0(),
	EventHandlerList_t1298116880::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (ExpandableObjectConverter_t1139197709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (ExtenderProvidedPropertyAttribute_t3223729015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[3] = 
{
	ExtenderProvidedPropertyAttribute_t3223729015::get_offset_of_extender_0(),
	ExtenderProvidedPropertyAttribute_t3223729015::get_offset_of_extenderProvider_1(),
	ExtenderProvidedPropertyAttribute_t3223729015::get_offset_of_receiver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (GuidConverter_t1547586607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (HandledEventArgs_t1685735245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[1] = 
{
	HandledEventArgs_t1685735245::get_offset_of_handled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (ImmutableObjectAttribute_t1990201979), -1, sizeof(ImmutableObjectAttribute_t1990201979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[4] = 
{
	ImmutableObjectAttribute_t1990201979::get_offset_of_immutable_0(),
	ImmutableObjectAttribute_t1990201979_StaticFields::get_offset_of_Default_1(),
	ImmutableObjectAttribute_t1990201979_StaticFields::get_offset_of_No_2(),
	ImmutableObjectAttribute_t1990201979_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (InheritanceAttribute_t2851181518), -1, sizeof(InheritanceAttribute_t2851181518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1958[5] = 
{
	InheritanceAttribute_t2851181518::get_offset_of_level_0(),
	InheritanceAttribute_t2851181518_StaticFields::get_offset_of_Default_1(),
	InheritanceAttribute_t2851181518_StaticFields::get_offset_of_Inherited_2(),
	InheritanceAttribute_t2851181518_StaticFields::get_offset_of_InheritedReadOnly_3(),
	InheritanceAttribute_t2851181518_StaticFields::get_offset_of_NotInherited_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (InheritanceLevel_t4275660400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1959[4] = 
{
	InheritanceLevel_t4275660400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (InitializationEventAttribute_t4110451806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[1] = 
{
	InitializationEventAttribute_t4110451806::get_offset_of_eventName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (InstallerTypeAttribute_t2978264484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[1] = 
{
	InstallerTypeAttribute_t2978264484::get_offset_of_installer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (InstanceCreationEditor_t1706597557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (InvalidAsynchronousStateException_t707889517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (Int16Converter_t903627590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (Int32Converter_t957938388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (Int64Converter_t3186343659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (InvalidEnumArgumentException_t3709744516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (LicenseContext_t192650050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (License_t3326651051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (LicenseException_t1281499302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[1] = 
{
	LicenseException_t1281499302::get_offset_of_type_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (LicenseManager_t764408642), -1, sizeof(LicenseManager_t764408642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1981[3] = 
{
	LicenseManager_t764408642_StaticFields::get_offset_of_mycontext_0(),
	LicenseManager_t764408642_StaticFields::get_offset_of_contextLockUser_1(),
	LicenseManager_t764408642_StaticFields::get_offset_of_lockObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (LicenseProviderAttribute_t1207066332), -1, sizeof(LicenseProviderAttribute_t1207066332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1982[2] = 
{
	LicenseProviderAttribute_t1207066332::get_offset_of_Provider_0(),
	LicenseProviderAttribute_t1207066332_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (LicenseProvider_t3536803528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (LicenseUsageMode_t2996119499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[3] = 
{
	LicenseUsageMode_t2996119499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (LicFileLicenseProvider_t2962157156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (LicFileLicense_t1222627691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[1] = 
{
	LicFileLicense_t1222627691::get_offset_of__key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (ListBindableAttribute_t1092011273), -1, sizeof(ListBindableAttribute_t1092011273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1987[4] = 
{
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_Default_0(),
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_No_1(),
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_Yes_2(),
	ListBindableAttribute_t1092011273::get_offset_of_bindable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (ListChangedEventArgs_t3132270315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[4] = 
{
	ListChangedEventArgs_t3132270315::get_offset_of_changedType_1(),
	ListChangedEventArgs_t3132270315::get_offset_of_oldIndex_2(),
	ListChangedEventArgs_t3132270315::get_offset_of_newIndex_3(),
	ListChangedEventArgs_t3132270315::get_offset_of_propDesc_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (ListChangedType_t3463990274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1989[9] = 
{
	ListChangedType_t3463990274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (ListSortDescriptionCollection_t2357028590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[1] = 
{
	ListSortDescriptionCollection_t2357028590::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (ListSortDescription_t3194554012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[2] = 
{
	ListSortDescription_t3194554012::get_offset_of_propertyDescriptor_0(),
	ListSortDescription_t3194554012::get_offset_of_sortDirection_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (ListSortDirection_t4186912589)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[3] = 
{
	ListSortDirection_t4186912589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (LocalizableAttribute_t1267247394), -1, sizeof(LocalizableAttribute_t1267247394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[4] = 
{
	LocalizableAttribute_t1267247394::get_offset_of_localizable_0(),
	LocalizableAttribute_t1267247394_StaticFields::get_offset_of_Default_1(),
	LocalizableAttribute_t1267247394_StaticFields::get_offset_of_No_2(),
	LocalizableAttribute_t1267247394_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (LookupBindingPropertiesAttribute_t2232070260), -1, sizeof(LookupBindingPropertiesAttribute_t2232070260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1994[5] = 
{
	LookupBindingPropertiesAttribute_t2232070260::get_offset_of_data_source_0(),
	LookupBindingPropertiesAttribute_t2232070260::get_offset_of_display_member_1(),
	LookupBindingPropertiesAttribute_t2232070260::get_offset_of_value_member_2(),
	LookupBindingPropertiesAttribute_t2232070260::get_offset_of_lookup_member_3(),
	LookupBindingPropertiesAttribute_t2232070260_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (MarshalByValueComponent_t3997823175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[3] = 
{
	MarshalByValueComponent_t3997823175::get_offset_of_eventList_0(),
	MarshalByValueComponent_t3997823175::get_offset_of_mySite_1(),
	MarshalByValueComponent_t3997823175::get_offset_of_disposedEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (MaskedTextProvider_t2280059057), -1, sizeof(MaskedTextProvider_t2280059057_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1996[15] = 
{
	MaskedTextProvider_t2280059057::get_offset_of_allow_prompt_as_input_0(),
	MaskedTextProvider_t2280059057::get_offset_of_ascii_only_1(),
	MaskedTextProvider_t2280059057::get_offset_of_culture_2(),
	MaskedTextProvider_t2280059057::get_offset_of_include_literals_3(),
	MaskedTextProvider_t2280059057::get_offset_of_include_prompt_4(),
	MaskedTextProvider_t2280059057::get_offset_of_is_password_5(),
	MaskedTextProvider_t2280059057::get_offset_of_mask_6(),
	MaskedTextProvider_t2280059057::get_offset_of_password_char_7(),
	MaskedTextProvider_t2280059057::get_offset_of_prompt_char_8(),
	MaskedTextProvider_t2280059057::get_offset_of_reset_on_prompt_9(),
	MaskedTextProvider_t2280059057::get_offset_of_reset_on_space_10(),
	MaskedTextProvider_t2280059057::get_offset_of_skip_literals_11(),
	MaskedTextProvider_t2280059057::get_offset_of_edit_positions_12(),
	MaskedTextProvider_t2280059057_StaticFields::get_offset_of_default_prompt_char_13(),
	MaskedTextProvider_t2280059057_StaticFields::get_offset_of_default_password_char_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (EditState_t81707772)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1997[4] = 
{
	EditState_t81707772::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (EditType_t3003187841)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1998[16] = 
{
	EditType_t3003187841::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (EditPosition_t3912732320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[5] = 
{
	EditPosition_t3912732320::get_offset_of_Parent_0(),
	EditPosition_t3912732320::get_offset_of_Type_1(),
	EditPosition_t3912732320::get_offset_of_State_2(),
	EditPosition_t3912732320::get_offset_of_MaskCharacter_3(),
	EditPosition_t3912732320::get_offset_of_input_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
