﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherFriendsListItem
struct OtherFriendsListItem_t1916582308;
// OtherFriend
struct OtherFriend_t3976068630;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OtherFriend3976068630.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void OtherFriendsListItem::.ctor()
extern "C"  void OtherFriendsListItem__ctor_m3954703889 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::SetUpItem()
extern "C"  void OtherFriendsListItem_SetUpItem_m453502491 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::PopulateItem(OtherFriend,System.Boolean)
extern "C"  void OtherFriendsListItem_PopulateItem_m198769475 (OtherFriendsListItem_t1916582308 * __this, OtherFriend_t3976068630 * ___otherFriend0, bool ___attack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::SetVisible()
extern "C"  void OtherFriendsListItem_SetVisible_m3176329469 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::SetInvisible()
extern "C"  void OtherFriendsListItem_SetInvisible_m3734339424 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OtherFriendsListItem::verifyVisibility()
extern "C"  bool OtherFriendsListItem_verifyVisibility_m4149380528 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::CheckVisibilty()
extern "C"  void OtherFriendsListItem_CheckVisibilty_m2336920668 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::Update()
extern "C"  void OtherFriendsListItem_Update_m200984818 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::OnEnable()
extern "C"  void OtherFriendsListItem_OnEnable_m3527282513 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::FindScrollView()
extern "C"  void OtherFriendsListItem_FindScrollView_m936337892 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::OnPress(System.Boolean)
extern "C"  void OtherFriendsListItem_OnPress_m757332290 (OtherFriendsListItem_t1916582308 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void OtherFriendsListItem_OnDrag_m1528858128 (OtherFriendsListItem_t1916582308 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::OnDragEnd()
extern "C"  void OtherFriendsListItem_OnDragEnd_m2998702427 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::OnScroll(System.Single)
extern "C"  void OtherFriendsListItem_OnScroll_m2350439628 (OtherFriendsListItem_t1916582308 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::OnClick()
extern "C"  void OtherFriendsListItem_OnClick_m2966904860 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OtherFriendsListItem::CheckSMSAvailability()
extern "C"  bool OtherFriendsListItem_CheckSMSAvailability_m1053418695 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::SendSMS(System.String,System.String)
extern "C"  void OtherFriendsListItem_SendSMS_m2823413038 (OtherFriendsListItem_t1916582308 * __this, String_t* ___contactPhoneNumber0, String_t* ___messageBody1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::smsComposerFinishedWithResult(System.String)
extern "C"  void OtherFriendsListItem_smsComposerFinishedWithResult_m2207554997 (OtherFriendsListItem_t1916582308 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::SMSSent()
extern "C"  void OtherFriendsListItem_SMSSent_m1099362068 (OtherFriendsListItem_t1916582308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsListItem::<OnClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void OtherFriendsListItem_U3COnClickU3Em__0_m3963617852 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
