﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.CipherReference
struct CipherReference_t2913200258;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.CipherReference::.ctor()
extern "C"  void CipherReference__ctor_m3469416308 (CipherReference_t2913200258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.CipherReference::GetXml()
extern "C"  XmlElement_t2877111883 * CipherReference_GetXml_m1887049847 (CipherReference_t2913200258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.CipherReference::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * CipherReference_GetXml_m566357749 (CipherReference_t2913200258 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.CipherReference::LoadXml(System.Xml.XmlElement)
extern "C"  void CipherReference_LoadXml_m575021836 (CipherReference_t2913200258 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
