﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HomeTabUIScaler2633114472.h"
#include "AssemblyU2DCSharp_LaunchLoader3640784488.h"
#include "AssemblyU2DCSharp_LeaderboardButton679202217.h"
#include "AssemblyU2DCSharp_LoaderUIScaler4194491137.h"
#include "AssemblyU2DCSharp_SettingsButton3510963965.h"
#include "AssemblyU2DCSharp_ShieldBarController264539450.h"
#include "AssemblyU2DCSharp_ShieldButton2669703779.h"
#include "AssemblyU2DCSharp_StatusTabButton3949570411.h"
#include "AssemblyU2DCSharp_StatusTabButton_U3CIncomingStatu3777268762.h"
#include "AssemblyU2DCSharp_TopTabButton865211906.h"
#include "AssemblyU2DCSharp_HomeLeftUIManager3932466733.h"
#include "AssemblyU2DCSharp_HomeRightUIManager3565483256.h"
#include "AssemblyU2DCSharp_HomeSceneManager2690266116.h"
#include "AssemblyU2DCSharp_CenterOnMapButton803353738.h"
#include "AssemblyU2DCSharp_FriendMarker3038476686.h"
#include "AssemblyU2DCSharp_GroupMarker1861645095.h"
#include "AssemblyU2DCSharp_GroupMarker_U3CWaitToOpenModalU32278094544.h"
#include "AssemblyU2DCSharp_MapButtonOptions2133642156.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"
#include "AssemblyU2DCSharp_MapManager3593696545.h"
#include "AssemblyU2DCSharp_MapManager_DistanceUnit2764741455.h"
#include "AssemblyU2DCSharp_MapManager_LatLon3749872052.h"
#include "AssemblyU2DCSharp_MapManager_U3CWaitToUpdateU3Ec__I415638116.h"
#include "AssemblyU2DCSharp_MineMarker2486518043.h"
#include "AssemblyU2DCSharp_OpenLayMineUIButton1217727427.h"
#include "AssemblyU2DCSharp_OpenLayMineUIButton_U3CWaitToOpe2426526286.h"
#include "AssemblyU2DCSharp_PlayerMarker2200525527.h"
#include "AssemblyU2DCSharp_RadarEffectScript3700381014.h"
#include "AssemblyU2DCSharp_RadarMarkerBlip1130528275.h"
#include "AssemblyU2DCSharp_RadarSweep3474354092.h"
#include "AssemblyU2DCSharp_SwitchMapStateButton2972050837.h"
#include "AssemblyU2DCSharp_AwardModal1856366994.h"
#include "AssemblyU2DCSharp_LevelAward2605567013.h"
#include "AssemblyU2DCSharp_BaseModalUIScaler2796740432.h"
#include "AssemblyU2DCSharp_CloseModalButton3895210965.h"
#include "AssemblyU2DCSharp_DefendGameNavScreen2935542205.h"
#include "AssemblyU2DCSharp_DefendInfoNavScreen2323813817.h"
#include "AssemblyU2DCSharp_DefendInfoNavScreen_U3CWaitForSe2549067708.h"
#include "AssemblyU2DCSharp_DefendModal2645228609.h"
#include "AssemblyU2DCSharp_DefendModalNavigateForwardButton4037284639.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "AssemblyU2DCSharp_DefendModalNavigationController472169731.h"
#include "AssemblyU2DCSharp_CloseMapGroupButton794394401.h"
#include "AssemblyU2DCSharp_GroupCircleAnimation1271941897.h"
#include "AssemblyU2DCSharp_GroupFriendUI1634706847.h"
#include "AssemblyU2DCSharp_MapGroupModal1530151518.h"
#include "AssemblyU2DCSharp_MapGroupModal_U3CWaitToCloseU3Ec3065553700.h"
#include "AssemblyU2DCSharp_MapGroupUIScaler4028507439.h"
#include "AssemblyU2DCSharp_OpenGroupFriendProfile894547916.h"
#include "AssemblyU2DCSharp_LayMineButton2913210547.h"
#include "AssemblyU2DCSharp_MineAudienceTab1025879672.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"
#include "AssemblyU2DCSharp_MineAudienceTabController1758951344.h"
#include "AssemblyU2DCSharp_MineInfoButton2712637181.h"
#include "AssemblyU2DCSharp_MineModal1141032720.h"
#include "AssemblyU2DCSharp_MineModal_U3CLoadDefaultModelU3Ec685440682.h"
#include "AssemblyU2DCSharp_MineSelectionSwipe3861158053.h"
#include "AssemblyU2DCSharp_MineShopButton202889731.h"
#include "AssemblyU2DCSharp_MineSwipeController1885659317.h"
#include "AssemblyU2DCSharp_OpenAudiencePopUpButton2472031080.h"
#include "AssemblyU2DCSharp_ModalType1526568337.h"
#include "AssemblyU2DCSharp_ModalManager4136513312.h"
#include "AssemblyU2DCSharp_ModalPanel3020707755.h"
#include "AssemblyU2DCSharp_ModalUI2568752073.h"
#include "AssemblyU2DCSharp_LongNotificationModal834782082.h"
#include "AssemblyU2DCSharp_LongNotificationUIScaler4012764123.h"
#include "AssemblyU2DCSharp_NotificationClearAllButton3531371129.h"
#include "AssemblyU2DCSharp_NotificationOkButton3431423113.h"
#include "AssemblyU2DCSharp_PPStatsNavScreen3294811412.h"
#include "AssemblyU2DCSharp_PPUserNavScreen245777140.h"
#include "AssemblyU2DCSharp_PlayerProfileAcceptButton251308446.h"
#include "AssemblyU2DCSharp_PlayerProfileAttackButton1361171206.h"
#include "AssemblyU2DCSharp_PlayerProfileBlockButton3859158973.h"
#include "AssemblyU2DCSharp_PlayerProfileChatButton1433962016.h"
#include "AssemblyU2DCSharp_PlayerProfileDeclineButton3344688636.h"
#include "AssemblyU2DCSharp_PlayerProfileFriendButton2898170674.h"
#include "AssemblyU2DCSharp_PlayerProfileModal3260625137.h"
#include "AssemblyU2DCSharp_PlayerProfileModal_U3CSendFriend2942902044.h"
#include "AssemblyU2DCSharp_PlayerProfileModal_U3CAcceptFrie3946986313.h"
#include "AssemblyU2DCSharp_PlayerProfileModal_U3CDeclineFri2154868554.h"
#include "AssemblyU2DCSharp_PlayerProfileNavigateForwardsBut1238707295.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "AssemblyU2DCSharp_PlayerProfileNavigationControlle1388421560.h"
#include "AssemblyU2DCSharp_MineResultsModal4210040424.h"
#include "AssemblyU2DCSharp_MissileResultsAttackButton3918897298.h"
#include "AssemblyU2DCSharp_MissileResultsChatButton36425224.h"
#include "AssemblyU2DCSharp_GameResult1017068805.h"
#include "AssemblyU2DCSharp_MissileResultsModal1615487503.h"
#include "AssemblyU2DCSharp_MissileResultsShareButton3260976859.h"
#include "AssemblyU2DCSharp_ResultBonusController3929995048.h"
#include "AssemblyU2DCSharp_ScoreZoneController1333878292.h"
#include "AssemblyU2DCSharp_ResultModalUI969511824.h"
#include "AssemblyU2DCSharp_ActivateShieldButton4092835112.h"
#include "AssemblyU2DCSharp_CloseWarningPopUpButton1445760822.h"
#include "AssemblyU2DCSharp_ShieldInfoButton822471353.h"
#include "AssemblyU2DCSharp_ShieldModal4049164426.h"
#include "AssemblyU2DCSharp_ShieldModal_U3CLoadDefaultModelU3873069052.h"
#include "AssemblyU2DCSharp_ShieldSelectionSwipe1465110497.h"
#include "AssemblyU2DCSharp_ShieldShopButton1762407207.h"
#include "AssemblyU2DCSharp_ShieldSwipeController2505851713.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof (HomeTabUIScaler_t2633114472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5700[8] = 
{
	HomeTabUIScaler_t2633114472::get_offset_of_leftDivider_14(),
	HomeTabUIScaler_t2633114472::get_offset_of_rightDivider_15(),
	HomeTabUIScaler_t2633114472::get_offset_of_friendCollider_16(),
	HomeTabUIScaler_t2633114472::get_offset_of_friendBackground_17(),
	HomeTabUIScaler_t2633114472::get_offset_of_statusCollider_18(),
	HomeTabUIScaler_t2633114472::get_offset_of_statusBackground_19(),
	HomeTabUIScaler_t2633114472::get_offset_of_inventoryCollider_20(),
	HomeTabUIScaler_t2633114472::get_offset_of_inventoryBackground_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof (LaunchLoader_t3640784488), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof (LeaderboardButton_t679202217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5702[1] = 
{
	LeaderboardButton_t679202217::get_offset_of_leaderboardIcon_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof (LoaderUIScaler_t4194491137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5703[1] = 
{
	LoaderUIScaler_t4194491137::get_offset_of_background_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof (SettingsButton_t3510963965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { sizeof (ShieldBarController_t264539450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5705[6] = 
{
	ShieldBarController_t264539450::get_offset_of_bronzeColour_13(),
	ShieldBarController_t264539450::get_offset_of_silverColour_14(),
	ShieldBarController_t264539450::get_offset_of_goldColour_15(),
	ShieldBarController_t264539450::get_offset_of_inactiveColour_16(),
	ShieldBarController_t264539450::get_offset_of_shieldIcon_17(),
	ShieldBarController_t264539450::get_offset_of_timeLabel_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof (ShieldButton_t2669703779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5706[1] = 
{
	ShieldButton_t2669703779::get_offset_of_barController_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof (StatusTabButton_t3949570411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5707[6] = 
{
	StatusTabButton_t3949570411::get_offset_of_descActiveColour_6(),
	StatusTabButton_t3949570411::get_offset_of_descInactiveColour_7(),
	StatusTabButton_t3949570411::get_offset_of_amountActiveColour_8(),
	StatusTabButton_t3949570411::get_offset_of_amountInactiveColour_9(),
	StatusTabButton_t3949570411::get_offset_of_descLabel_10(),
	StatusTabButton_t3949570411::get_offset_of_amountLabel_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5708[4] = 
{
	U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762::get_offset_of_U24this_0(),
	U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762::get_offset_of_U24current_1(),
	U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762::get_offset_of_U24disposing_2(),
	U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof (TopTabButton_t865211906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5709[1] = 
{
	TopTabButton_t865211906::get_offset_of_backgroundSprite_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof (HomeLeftUIManager_t3932466733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5710[7] = 
{
	HomeLeftUIManager_t3932466733::get_offset_of_mapOptions_3(),
	HomeLeftUIManager_t3932466733::get_offset_of_uiTween_4(),
	HomeLeftUIManager_t3932466733::get_offset_of_missileShieldBar_5(),
	HomeLeftUIManager_t3932466733::get_offset_of_mineShieldBar_6(),
	HomeLeftUIManager_t3932466733::get_offset_of_activeStateColour_7(),
	HomeLeftUIManager_t3932466733::get_offset_of_inactiveStateColour_8(),
	HomeLeftUIManager_t3932466733::get_offset_of_tabClosing_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof (HomeRightUIManager_t3565483256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5711[1] = 
{
	HomeRightUIManager_t3565483256::get_offset_of_uiTween_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof (HomeSceneManager_t2690266116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5712[20] = 
{
	HomeSceneManager_t2690266116::get_offset_of_worldMap_17(),
	HomeSceneManager_t2690266116::get_offset_of_homeMap_18(),
	HomeSceneManager_t2690266116::get_offset_of_homeCenterUI_19(),
	HomeSceneManager_t2690266116::get_offset_of_homeDebugUI_20(),
	HomeSceneManager_t2690266116::get_offset_of_homeHeaderUI_21(),
	HomeSceneManager_t2690266116::get_offset_of_homeTabUI_22(),
	HomeSceneManager_t2690266116::get_offset_of_homeLeftUI_23(),
	HomeSceneManager_t2690266116::get_offset_of_homeRightUI_24(),
	HomeSceneManager_t2690266116::get_offset_of_homeBottomUI_25(),
	HomeSceneManager_t2690266116::get_offset_of_sceneLoader_26(),
	HomeSceneManager_t2690266116::get_offset_of_friendsLoadedFromKnockout_27(),
	HomeSceneManager_t2690266116::get_offset_of_startAttackNav_28(),
	HomeSceneManager_t2690266116::get_offset_of_startAttackHomeNav_29(),
	HomeSceneManager_t2690266116::get_offset_of_activeMissile_30(),
	HomeSceneManager_t2690266116::get_offset_of_activeTarget_31(),
	HomeSceneManager_t2690266116::get_offset_of_startInventoryMissile_32(),
	HomeSceneManager_t2690266116::get_offset_of_startInventoryShieldItem_33(),
	HomeSceneManager_t2690266116::get_offset_of_startInventoryShield_34(),
	HomeSceneManager_t2690266116::get_offset_of_startInventoryMine_35(),
	HomeSceneManager_t2690266116::get_offset_of_startInventoryFuel_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof (CenterOnMapButton_t803353738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof (FriendMarker_t3038476686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5714[6] = 
{
	FriendMarker_t3038476686::get_offset_of_friend_10(),
	FriendMarker_t3038476686::get_offset_of_grouped_11(),
	FriendMarker_t3038476686::get_offset_of_alphaTween_12(),
	FriendMarker_t3038476686::get_offset_of_displayLabel_13(),
	FriendMarker_t3038476686::get_offset_of_nguiCamera_14(),
	FriendMarker_t3038476686::get_offset_of_uiroot_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof (GroupMarker_t1861645095), -1, sizeof(GroupMarker_t1861645095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5715[6] = 
{
	GroupMarker_t1861645095::get_offset_of_group_10(),
	GroupMarker_t1861645095::get_offset_of_alphaTween_11(),
	GroupMarker_t1861645095::get_offset_of_center_12(),
	GroupMarker_t1861645095::get_offset_of_tilePosition_13(),
	GroupMarker_t1861645095::get_offset_of_zoom_14(),
	GroupMarker_t1861645095_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof (U3CWaitToOpenModalU3Ec__Iterator0_t2278094544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5716[5] = 
{
	U3CWaitToOpenModalU3Ec__Iterator0_t2278094544::get_offset_of_U3CfriendsGroupU3E__0_0(),
	U3CWaitToOpenModalU3Ec__Iterator0_t2278094544::get_offset_of_U24this_1(),
	U3CWaitToOpenModalU3Ec__Iterator0_t2278094544::get_offset_of_U24current_2(),
	U3CWaitToOpenModalU3Ec__Iterator0_t2278094544::get_offset_of_U24disposing_3(),
	U3CWaitToOpenModalU3Ec__Iterator0_t2278094544::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof (MapButtonOptions_t2133642156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5717[5] = 
{
	MapButtonOptions_t2133642156::get_offset_of_rotTween_5(),
	MapButtonOptions_t2133642156::get_offset_of_alphaTween_6(),
	MapButtonOptions_t2133642156::get_offset_of_activateAlphaTween_7(),
	MapButtonOptions_t2133642156::get_offset_of_optionsOpen_8(),
	MapButtonOptions_t2133642156::get_offset_of_mapOptions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof (MapState_t1547378305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5718[5] = 
{
	MapState_t1547378305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof (MapManager_t3593696545), -1, sizeof(MapManager_t3593696545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5719[26] = 
{
	MapManager_t3593696545::get_offset_of_mapDragTime_3(),
	MapManager_t3593696545::get_offset_of_mapTileset_4(),
	MapManager_t3593696545::get_offset_of_mapUIActive_5(),
	MapManager_t3593696545::get_offset_of_minZoomLevel_6(),
	MapManager_t3593696545::get_offset_of_maxZoomLevel_7(),
	MapManager_t3593696545::get_offset_of_currentState_8(),
	MapManager_t3593696545::get_offset_of_userPosition_9(),
	MapManager_t3593696545::get_offset_of_defaultFriendPinPrefab_10(),
	MapManager_t3593696545::get_offset_of_defaultGroupPinPrefab_11(),
	MapManager_t3593696545::get_offset_of_defaultPlayerPinPrefab_12(),
	MapManager_t3593696545::get_offset_of_defaultMinePinPrefab_13(),
	MapManager_t3593696545::get_offset_of_maxFriendsToDisplay_14(),
	MapManager_t3593696545::get_offset_of_colorBlue_15(),
	MapManager_t3593696545::get_offset_of_colorGreen_16(),
	MapManager_t3593696545::get_offset_of_colorYellow_17(),
	MapManager_t3593696545::get_offset_of_radar_18(),
	MapManager_t3593696545::get_offset_of_playerMarker_19(),
	MapManager_t3593696545::get_offset_of_activeFriendMarkers_20(),
	MapManager_t3593696545::get_offset_of_activeFriendGroupMarkers_21(),
	MapManager_t3593696545::get_offset_of_initalized_22(),
	MapManager_t3593696545::get_offset_of_playZoomSFX_23(),
	MapManager_t3593696545::get_offset_of_groupDistance_24(),
	MapManager_t3593696545::get_offset_of_calcGroupDistance_25(),
	MapManager_t3593696545::get_offset_of_centerVelocityRoutine_26(),
	0,
	MapManager_t3593696545_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof (DistanceUnit_t2764741455)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5720[5] = 
{
	DistanceUnit_t2764741455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof (LatLon_t3749872052)+ sizeof (Il2CppObject), sizeof(LatLon_t3749872052 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5721[2] = 
{
	LatLon_t3749872052::get_offset_of_Latitude_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LatLon_t3749872052::get_offset_of_Longtitude_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof (U3CWaitToUpdateU3Ec__Iterator0_t415638116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5722[4] = 
{
	U3CWaitToUpdateU3Ec__Iterator0_t415638116::get_offset_of_U24this_0(),
	U3CWaitToUpdateU3Ec__Iterator0_t415638116::get_offset_of_U24current_1(),
	U3CWaitToUpdateU3Ec__Iterator0_t415638116::get_offset_of_U24disposing_2(),
	U3CWaitToUpdateU3Ec__Iterator0_t415638116::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof (MineMarker_t2486518043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5723[2] = 
{
	MineMarker_t2486518043::get_offset_of_mine_10(),
	MineMarker_t2486518043::get_offset_of_alphaTween_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof (OpenLayMineUIButton_t1217727427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5725[3] = 
{
	U3CWaitToOpenModalU3Ec__Iterator0_t2426526286::get_offset_of_U24current_0(),
	U3CWaitToOpenModalU3Ec__Iterator0_t2426526286::get_offset_of_U24disposing_1(),
	U3CWaitToOpenModalU3Ec__Iterator0_t2426526286::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof (PlayerMarker_t2200525527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5726[1] = 
{
	PlayerMarker_t2200525527::get_offset_of_player_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof (RadarEffectScript_t3700381014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5727[5] = 
{
	RadarEffectScript_t3700381014::get_offset_of_lineMaterial_2(),
	RadarEffectScript_t3700381014::get_offset_of_xRadius_3(),
	RadarEffectScript_t3700381014::get_offset_of_yRadius_4(),
	RadarEffectScript_t3700381014::get_offset_of_segments_5(),
	RadarEffectScript_t3700381014::get_offset_of_pointRotation_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof (RadarMarkerBlip_t1130528275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5728[8] = 
{
	RadarMarkerBlip_t1130528275::get_offset_of_instance_2(),
	RadarMarkerBlip_t1130528275::get_offset_of_colorTween_3(),
	RadarMarkerBlip_t1130528275::get_offset_of_blipColor_4(),
	RadarMarkerBlip_t1130528275::get_offset_of_sp_5(),
	RadarMarkerBlip_t1130528275::get_offset_of_markerScale_6(),
	RadarMarkerBlip_t1130528275::get_offset_of_colliderCenter_7(),
	RadarMarkerBlip_t1130528275::get_offset_of_colliderSize_8(),
	RadarMarkerBlip_t1130528275::get_offset_of_markerCollider_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof (RadarSweep_t3474354092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5729[1] = 
{
	RadarSweep_t3474354092::get_offset_of_rotTween_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof (SwitchMapStateButton_t2972050837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5730[7] = 
{
	SwitchMapStateButton_t2972050837::get_offset_of_mapState_5(),
	SwitchMapStateButton_t2972050837::get_offset_of_deactivated_6(),
	SwitchMapStateButton_t2972050837::get_offset_of_posTween_7(),
	SwitchMapStateButton_t2972050837::get_offset_of_buttonActive_8(),
	SwitchMapStateButton_t2972050837::get_offset_of_backgroundSprite_9(),
	SwitchMapStateButton_t2972050837::get_offset_of_buttonSprite_10(),
	SwitchMapStateButton_t2972050837::get_offset_of_isActiveState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof (AwardModal_t1856366994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5731[3] = 
{
	AwardModal_t1856366994::get_offset_of_rankNameLabel_3(),
	AwardModal_t1856366994::get_offset_of_rankLevelLabel_4(),
	AwardModal_t1856366994::get_offset_of_rankBonusLabel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof (LevelAward_t2605567013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5732[3] = 
{
	LevelAward_t2605567013::get_offset_of_rankLevel_0(),
	LevelAward_t2605567013::get_offset_of_bonusAmount_1(),
	LevelAward_t2605567013::get_offset_of_rankName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof (BaseModalUIScaler_t2796740432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5733[1] = 
{
	BaseModalUIScaler_t2796740432::get_offset_of_background_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof (CloseModalButton_t3895210965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof (DefendGameNavScreen_t2935542205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5735[12] = 
{
	DefendGameNavScreen_t2935542205::get_offset_of_modal_3(),
	DefendGameNavScreen_t2935542205::get_offset_of_missileNameLabel_4(),
	DefendGameNavScreen_t2935542205::get_offset_of_arriveTimer_5(),
	DefendGameNavScreen_t2935542205::get_offset_of_pointsPopulator_6(),
	DefendGameNavScreen_t2935542205::get_offset_of_miniGameResourcePath_7(),
	DefendGameNavScreen_t2935542205::get_offset_of_gameUI_8(),
	DefendGameNavScreen_t2935542205::get_offset_of_miniGameObject_9(),
	DefendGameNavScreen_t2935542205::get_offset_of_currentMiniGame_10(),
	DefendGameNavScreen_t2935542205::get_offset_of_uiClosing_11(),
	DefendGameNavScreen_t2935542205::get_offset_of_totalTimeAsSeconds_12(),
	DefendGameNavScreen_t2935542205::get_offset_of_currentState_13(),
	DefendGameNavScreen_t2935542205::get_offset_of_latestPointsAwarded_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof (DefendInfoNavScreen_t2323813817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5736[11] = 
{
	DefendInfoNavScreen_t2323813817::get_offset_of_modal_3(),
	DefendInfoNavScreen_t2323813817::get_offset_of_avatar_4(),
	DefendInfoNavScreen_t2323813817::get_offset_of_firstNameLabel_5(),
	DefendInfoNavScreen_t2323813817::get_offset_of_lastNameLabel_6(),
	DefendInfoNavScreen_t2323813817::get_offset_of_itemNameLabel_7(),
	DefendInfoNavScreen_t2323813817::get_offset_of_actionLabel_8(),
	DefendInfoNavScreen_t2323813817::get_offset_of_timer_9(),
	DefendInfoNavScreen_t2323813817::get_offset_of_modelParent_10(),
	DefendInfoNavScreen_t2323813817::get_offset_of_currentModel_11(),
	DefendInfoNavScreen_t2323813817::get_offset_of_itemLight_12(),
	DefendInfoNavScreen_t2323813817::get_offset_of_uiClosing_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof (U3CWaitForSecondsU3Ec__Iterator0_t2549067708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5737[5] = 
{
	U3CWaitForSecondsU3Ec__Iterator0_t2549067708::get_offset_of_amountToWait_0(),
	U3CWaitForSecondsU3Ec__Iterator0_t2549067708::get_offset_of_U24this_1(),
	U3CWaitForSecondsU3Ec__Iterator0_t2549067708::get_offset_of_U24current_2(),
	U3CWaitForSecondsU3Ec__Iterator0_t2549067708::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsU3Ec__Iterator0_t2549067708::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof (DefendModal_t2645228609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5738[4] = 
{
	DefendModal_t2645228609::get_offset_of_item_3(),
	DefendModal_t2645228609::get_offset_of_navigationUI_4(),
	DefendModal_t2645228609::get_offset_of_infoObject_5(),
	DefendModal_t2645228609::get_offset_of_defendGameObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof (DefendModalNavigateForwardButton_t4037284639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5739[1] = 
{
	DefendModalNavigateForwardButton_t4037284639::get_offset_of_navScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof (DefendNavScreen_t3838619703)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5740[4] = 
{
	DefendNavScreen_t3838619703::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof (DefendModalNavigationController_t472169731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5741[5] = 
{
	DefendModalNavigationController_t472169731::get_offset_of_infoObject_8(),
	DefendModalNavigationController_t472169731::get_offset_of_defendGameObject_9(),
	DefendModalNavigationController_t472169731::get_offset_of_activePos_10(),
	DefendModalNavigationController_t472169731::get_offset_of_leftInactivePos_11(),
	DefendModalNavigationController_t472169731::get_offset_of_rightInactivePos_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof (CloseMapGroupButton_t794394401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5742[1] = 
{
	CloseMapGroupButton_t794394401::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof (GroupCircleAnimation_t1271941897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5743[3] = 
{
	GroupCircleAnimation_t1271941897::get_offset_of_maxCircleRadius_2(),
	GroupCircleAnimation_t1271941897::get_offset_of_degree_3(),
	GroupCircleAnimation_t1271941897::get_offset_of_friendsGroupUI_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof (GroupFriendUI_t1634706847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5744[6] = 
{
	GroupFriendUI_t1634706847::get_offset_of_modal_2(),
	GroupFriendUI_t1634706847::get_offset_of_friend_3(),
	GroupFriendUI_t1634706847::get_offset_of_button_4(),
	GroupFriendUI_t1634706847::get_offset_of_avatar_5(),
	GroupFriendUI_t1634706847::get_offset_of_posTween_6(),
	GroupFriendUI_t1634706847::get_offset_of_scaleTween_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof (MapGroupModal_t1530151518), -1, sizeof(MapGroupModal_t1530151518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5745[10] = 
{
	MapGroupModal_t1530151518::get_offset_of_maxGroupAmount_3(),
	MapGroupModal_t1530151518::get_offset_of_panel_4(),
	MapGroupModal_t1530151518::get_offset_of_backgroundFade_5(),
	MapGroupModal_t1530151518::get_offset_of_UITween_6(),
	MapGroupModal_t1530151518::get_offset_of_friendsGroup_7(),
	MapGroupModal_t1530151518::get_offset_of_groupAnim_8(),
	MapGroupModal_t1530151518::get_offset_of_groupParent_9(),
	MapGroupModal_t1530151518::get_offset_of_profileToOpen_10(),
	MapGroupModal_t1530151518::get_offset_of_modalClosing_11(),
	MapGroupModal_t1530151518_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof (U3CWaitToCloseU3Ec__Iterator0_t3065553700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5746[4] = 
{
	U3CWaitToCloseU3Ec__Iterator0_t3065553700::get_offset_of_U24this_0(),
	U3CWaitToCloseU3Ec__Iterator0_t3065553700::get_offset_of_U24current_1(),
	U3CWaitToCloseU3Ec__Iterator0_t3065553700::get_offset_of_U24disposing_2(),
	U3CWaitToCloseU3Ec__Iterator0_t3065553700::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof (MapGroupUIScaler_t4028507439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5747[2] = 
{
	MapGroupUIScaler_t4028507439::get_offset_of_background_14(),
	MapGroupUIScaler_t4028507439::get_offset_of_backgroundCollider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof (OpenGroupFriendProfile_t894547916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5748[1] = 
{
	OpenGroupFriendProfile_t894547916::get_offset_of_ui_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof (LayMineButton_t2913210547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5749[2] = 
{
	LayMineButton_t2913210547::get_offset_of_modal_5(),
	LayMineButton_t2913210547::get_offset_of_mineAudience_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof (MineAudienceTab_t1025879672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5750[3] = 
{
	MineAudienceTab_t1025879672::get_offset_of_tabController_5(),
	MineAudienceTab_t1025879672::get_offset_of_audience_6(),
	MineAudienceTab_t1025879672::get_offset_of_colourTween_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof (MineAudience_t3529940549)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5751[4] = 
{
	MineAudience_t3529940549::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof (MineAudienceTabController_t1758951344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5752[2] = 
{
	MineAudienceTabController_t1758951344::get_offset_of_currentAudience_2(),
	MineAudienceTabController_t1758951344::get_offset_of_tabs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof (MineInfoButton_t2712637181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5753[1] = 
{
	MineInfoButton_t2712637181::get_offset_of_modal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof (MineModal_t1141032720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5754[18] = 
{
	MineModal_t1141032720::get_offset_of_currentItemIndex_3(),
	MineModal_t1141032720::get_offset_of_infoObject_4(),
	MineModal_t1141032720::get_offset_of_itemObject_5(),
	MineModal_t1141032720::get_offset_of_infoButton_6(),
	MineModal_t1141032720::get_offset_of_infoController_7(),
	MineModal_t1141032720::get_offset_of_swipeController_8(),
	MineModal_t1141032720::get_offset_of_itemNameLabel_9(),
	MineModal_t1141032720::get_offset_of_itemAmountLabel_10(),
	MineModal_t1141032720::get_offset_of_itemLight_11(),
	MineModal_t1141032720::get_offset_of_mineItems_12(),
	MineModal_t1141032720::get_offset_of_mineRotationTween_13(),
	MineModal_t1141032720::get_offset_of_openAudienceButton_14(),
	MineModal_t1141032720::get_offset_of_audiencePUTween_15(),
	MineModal_t1141032720::get_offset_of_chosenAudience_16(),
	MineModal_t1141032720::get_offset_of_swipe_17(),
	MineModal_t1141032720::get_offset_of_showingInfo_18(),
	MineModal_t1141032720::get_offset_of_firstTimeLoading_19(),
	MineModal_t1141032720::get_offset_of_audiencePUClosing_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof (U3CLoadDefaultModelU3Ec__Iterator0_t685440682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5755[4] = 
{
	U3CLoadDefaultModelU3Ec__Iterator0_t685440682::get_offset_of_U24this_0(),
	U3CLoadDefaultModelU3Ec__Iterator0_t685440682::get_offset_of_U24current_1(),
	U3CLoadDefaultModelU3Ec__Iterator0_t685440682::get_offset_of_U24disposing_2(),
	U3CLoadDefaultModelU3Ec__Iterator0_t685440682::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof (MineSelectionSwipe_t3861158053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5756[1] = 
{
	MineSelectionSwipe_t3861158053::get_offset_of_modal_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof (MineShopButton_t202889731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5757[1] = 
{
	MineShopButton_t202889731::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof (MineSwipeController_t1885659317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5758[1] = 
{
	MineSwipeController_t1885659317::get_offset_of_modal_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof (OpenAudiencePopUpButton_t2472031080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5759[1] = 
{
	OpenAudiencePopUpButton_t2472031080::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof (ModalType_t1526568337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5760[10] = 
{
	ModalType_t1526568337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof (ModalManager_t4136513312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5761[6] = 
{
	ModalManager_t4136513312::get_offset_of_activeModal_3(),
	ModalManager_t4136513312::get_offset_of_modalActive_4(),
	ModalManager_t4136513312::get_offset_of_modalPanelObject_5(),
	ModalManager_t4136513312::get_offset_of_modalPanel_6(),
	ModalManager_t4136513312::get_offset_of_currentModalUI_7(),
	ModalManager_t4136513312::get_offset_of_modalclosing_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof (ModalPanel_t3020707755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5762[9] = 
{
	ModalPanel_t3020707755::get_offset_of_panel_2(),
	ModalPanel_t3020707755::get_offset_of_panelAlphaTween_3(),
	ModalPanel_t3020707755::get_offset_of_titleLabel_4(),
	ModalPanel_t3020707755::get_offset_of_backgroundFade_5(),
	ModalPanel_t3020707755::get_offset_of_backgroundSprite_6(),
	ModalPanel_t3020707755::get_offset_of_closeSprite_7(),
	ModalPanel_t3020707755::get_offset_of_closeCollider_8(),
	ModalPanel_t3020707755::get_offset_of_dividerSprite_9(),
	ModalPanel_t3020707755::get_offset_of_baseModal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof (ModalUI_t2568752073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5763[1] = 
{
	ModalUI_t2568752073::get_offset_of_reactivateMap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof (LongNotificationModal_t834782082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5764[13] = 
{
	LongNotificationModal_t834782082::get_offset_of_panel_3(),
	LongNotificationModal_t834782082::get_offset_of_backgroundFade_4(),
	LongNotificationModal_t834782082::get_offset_of_UITween_5(),
	LongNotificationModal_t834782082::get_offset_of_backgroundSprite_6(),
	LongNotificationModal_t834782082::get_offset_of_titleLabel_7(),
	LongNotificationModal_t834782082::get_offset_of_bodyLabel_8(),
	LongNotificationModal_t834782082::get_offset_of_okButton_9(),
	LongNotificationModal_t834782082::get_offset_of_okButtonBackground_10(),
	LongNotificationModal_t834782082::get_offset_of_clearAllButton_11(),
	LongNotificationModal_t834782082::get_offset_of_countLabel_12(),
	LongNotificationModal_t834782082::get_offset_of_currentNotification_13(),
	LongNotificationModal_t834782082::get_offset_of_spriteList_14(),
	LongNotificationModal_t834782082::get_offset_of_modalClosing_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof (LongNotificationUIScaler_t4012764123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5765[2] = 
{
	LongNotificationUIScaler_t4012764123::get_offset_of_background_14(),
	LongNotificationUIScaler_t4012764123::get_offset_of_backgroundCollider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof (NotificationClearAllButton_t3531371129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5766[1] = 
{
	NotificationClearAllButton_t3531371129::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof (NotificationOkButton_t3431423113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5767[1] = 
{
	NotificationOkButton_t3431423113::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof (PPStatsNavScreen_t3294811412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5768[13] = 
{
	PPStatsNavScreen_t3294811412::get_offset_of_modal_3(),
	PPStatsNavScreen_t3294811412::get_offset_of_statsTopDivider_4(),
	PPStatsNavScreen_t3294811412::get_offset_of_statsBotDivider_5(),
	PPStatsNavScreen_t3294811412::get_offset_of_firedValueLabel_6(),
	PPStatsNavScreen_t3294811412::get_offset_of_successRateValueLabel_7(),
	PPStatsNavScreen_t3294811412::get_offset_of_attackHitsValueLabel_8(),
	PPStatsNavScreen_t3294811412::get_offset_of_attacksValueLabel_9(),
	PPStatsNavScreen_t3294811412::get_offset_of_hitByValueLabel_10(),
	PPStatsNavScreen_t3294811412::get_offset_of_defencePercentageLabel_11(),
	PPStatsNavScreen_t3294811412::get_offset_of_laidValueLabel_12(),
	PPStatsNavScreen_t3294811412::get_offset_of_triggerredValueLabel_13(),
	PPStatsNavScreen_t3294811412::get_offset_of_defusedValueLabel_14(),
	PPStatsNavScreen_t3294811412::get_offset_of_UIclosing_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof (PPUserNavScreen_t245777140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5769[21] = 
{
	PPUserNavScreen_t245777140::get_offset_of_modal_3(),
	PPUserNavScreen_t245777140::get_offset_of_tabBackground_4(),
	PPUserNavScreen_t245777140::get_offset_of_tabMidDivider_5(),
	PPUserNavScreen_t245777140::get_offset_of_tabBotDivider_6(),
	PPUserNavScreen_t245777140::get_offset_of_profileOutline_7(),
	PPUserNavScreen_t245777140::get_offset_of_profileOverlay_8(),
	PPUserNavScreen_t245777140::get_offset_of_profileDivider_9(),
	PPUserNavScreen_t245777140::get_offset_of_stealthDivider_10(),
	PPUserNavScreen_t245777140::get_offset_of_pointsDivider_11(),
	PPUserNavScreen_t245777140::get_offset_of_chatSprite_12(),
	PPUserNavScreen_t245777140::get_offset_of_attackForeground_13(),
	PPUserNavScreen_t245777140::get_offset_of_attackBackground_14(),
	PPUserNavScreen_t245777140::get_offset_of_blockForeground_15(),
	PPUserNavScreen_t245777140::get_offset_of_blockBackground_16(),
	PPUserNavScreen_t245777140::get_offset_of_settingsSprite_17(),
	PPUserNavScreen_t245777140::get_offset_of_friendSprite_18(),
	PPUserNavScreen_t245777140::get_offset_of_acceptSprite_19(),
	PPUserNavScreen_t245777140::get_offset_of_declineForeground_20(),
	PPUserNavScreen_t245777140::get_offset_of_declineBackground_21(),
	PPUserNavScreen_t245777140::get_offset_of_firstStrikeSprite_22(),
	PPUserNavScreen_t245777140::get_offset_of_UIclosing_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof (PlayerProfileAcceptButton_t251308446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5770[1] = 
{
	PlayerProfileAcceptButton_t251308446::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof (PlayerProfileAttackButton_t1361171206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5771[1] = 
{
	PlayerProfileAttackButton_t1361171206::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof (PlayerProfileBlockButton_t3859158973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5772[2] = 
{
	PlayerProfileBlockButton_t3859158973::get_offset_of_item_13(),
	PlayerProfileBlockButton_t3859158973::get_offset_of_blocked_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof (PlayerProfileChatButton_t1433962016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5773[1] = 
{
	PlayerProfileChatButton_t1433962016::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { sizeof (PlayerProfileDeclineButton_t3344688636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5774[1] = 
{
	PlayerProfileDeclineButton_t3344688636::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof (PlayerProfileFriendButton_t2898170674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5775[1] = 
{
	PlayerProfileFriendButton_t2898170674::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { sizeof (PlayerProfileModal_t3260625137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5776[41] = 
{
	PlayerProfileModal_t3260625137::get_offset_of_user_3(),
	PlayerProfileModal_t3260625137::get_offset_of_positiveColour_4(),
	PlayerProfileModal_t3260625137::get_offset_of_negativeColour_5(),
	PlayerProfileModal_t3260625137::get_offset_of_avatar_6(),
	PlayerProfileModal_t3260625137::get_offset_of_firstNameLabel_7(),
	PlayerProfileModal_t3260625137::get_offset_of_lastNameLabel_8(),
	PlayerProfileModal_t3260625137::get_offset_of_usernameLabel_9(),
	PlayerProfileModal_t3260625137::get_offset_of_rankLabel_10(),
	PlayerProfileModal_t3260625137::get_offset_of_rankNameLabel_11(),
	PlayerProfileModal_t3260625137::get_offset_of_rankLevelLabel_12(),
	PlayerProfileModal_t3260625137::get_offset_of_stealthLabel_13(),
	PlayerProfileModal_t3260625137::get_offset_of_stealthStateLabel_14(),
	PlayerProfileModal_t3260625137::get_offset_of_smCalculator_15(),
	PlayerProfileModal_t3260625137::get_offset_of_dailyPointsLabel_16(),
	PlayerProfileModal_t3260625137::get_offset_of_totalPointsLabel_17(),
	PlayerProfileModal_t3260625137::get_offset_of_chatButton_18(),
	PlayerProfileModal_t3260625137::get_offset_of_chatTween_19(),
	PlayerProfileModal_t3260625137::get_offset_of_attackButton_20(),
	PlayerProfileModal_t3260625137::get_offset_of_attackTween_21(),
	PlayerProfileModal_t3260625137::get_offset_of_blockButton_22(),
	PlayerProfileModal_t3260625137::get_offset_of_blockTween_23(),
	PlayerProfileModal_t3260625137::get_offset_of_settingsButton_24(),
	PlayerProfileModal_t3260625137::get_offset_of_friendButton_25(),
	PlayerProfileModal_t3260625137::get_offset_of_friendTween_26(),
	PlayerProfileModal_t3260625137::get_offset_of_requestSentLabel_27(),
	PlayerProfileModal_t3260625137::get_offset_of_acceptButton_28(),
	PlayerProfileModal_t3260625137::get_offset_of_acceptTween_29(),
	PlayerProfileModal_t3260625137::get_offset_of_declineButton_30(),
	PlayerProfileModal_t3260625137::get_offset_of_declineTween_31(),
	PlayerProfileModal_t3260625137::get_offset_of_missilesFiredLabel_32(),
	PlayerProfileModal_t3260625137::get_offset_of_successRateLabel_33(),
	PlayerProfileModal_t3260625137::get_offset_of_incomingAttacksLabel_34(),
	PlayerProfileModal_t3260625137::get_offset_of_hitByLabel_35(),
	PlayerProfileModal_t3260625137::get_offset_of_defenceRateLabel_36(),
	PlayerProfileModal_t3260625137::get_offset_of_laidLabel_37(),
	PlayerProfileModal_t3260625137::get_offset_of_triggeredLabel_38(),
	PlayerProfileModal_t3260625137::get_offset_of_defusedLabel_39(),
	PlayerProfileModal_t3260625137::get_offset_of_player_40(),
	PlayerProfileModal_t3260625137::get_offset_of_profileUpdated_41(),
	PlayerProfileModal_t3260625137::get_offset_of_friendUpdate_42(),
	PlayerProfileModal_t3260625137::get_offset_of_declined_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof (U3CSendFriendRequestU3Ec__AnonStorey0_t2942902044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5777[2] = 
{
	U3CSendFriendRequestU3Ec__AnonStorey0_t2942902044::get_offset_of_tempFriend_0(),
	U3CSendFriendRequestU3Ec__AnonStorey0_t2942902044::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof (U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5778[3] = 
{
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313::get_offset_of_tempFriend_0(),
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313::get_offset_of_innerFriend_1(),
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof (U3CDeclineFriendRequestU3Ec__AnonStorey2_t2154868554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5779[2] = 
{
	U3CDeclineFriendRequestU3Ec__AnonStorey2_t2154868554::get_offset_of_tempFriend_0(),
	U3CDeclineFriendRequestU3Ec__AnonStorey2_t2154868554::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof (PlayerProfileNavigateForwardsButton_t1238707295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5780[2] = 
{
	PlayerProfileNavigateForwardsButton_t1238707295::get_offset_of_nextScreen_5(),
	PlayerProfileNavigateForwardsButton_t1238707295::get_offset_of_buttonLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof (PlayerProfileNavScreen_t2660668259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5781[4] = 
{
	PlayerProfileNavScreen_t2660668259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof (PlayerProfileNavigationController_t1388421560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5782[7] = 
{
	PlayerProfileNavigationController_t1388421560::get_offset_of_userLabel_8(),
	PlayerProfileNavigationController_t1388421560::get_offset_of_statsLabel_9(),
	PlayerProfileNavigationController_t1388421560::get_offset_of_userObject_10(),
	PlayerProfileNavigationController_t1388421560::get_offset_of_statsObject_11(),
	PlayerProfileNavigationController_t1388421560::get_offset_of_activePos_12(),
	PlayerProfileNavigationController_t1388421560::get_offset_of_leftInactivePos_13(),
	PlayerProfileNavigationController_t1388421560::get_offset_of_rightInactivePos_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { sizeof (MineResultsModal_t4210040424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5783[1] = 
{
	MineResultsModal_t4210040424::get_offset_of_modal_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { sizeof (MissileResultsAttackButton_t3918897298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5784[1] = 
{
	MissileResultsAttackButton_t3918897298::get_offset_of_modal_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof (MissileResultsChatButton_t36425224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5785[1] = 
{
	MissileResultsChatButton_t36425224::get_offset_of_modal_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof (GameResult_t1017068805)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5786[4] = 
{
	GameResult_t1017068805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof (MissileResultsModal_t1615487503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5787[5] = 
{
	MissileResultsModal_t1615487503::get_offset_of_modal_2(),
	MissileResultsModal_t1615487503::get_offset_of_inLabel_3(),
	MissileResultsModal_t1615487503::get_offset_of_timeLabel_4(),
	MissileResultsModal_t1615487503::get_offset_of_scoreZoneController_5(),
	MissileResultsModal_t1615487503::get_offset_of_bonusController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof (MissileResultsShareButton_t3260976859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5788[1] = 
{
	MissileResultsShareButton_t3260976859::get_offset_of_modal_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof (ResultBonusController_t3929995048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5789[7] = 
{
	ResultBonusController_t3929995048::get_offset_of_modal_2(),
	ResultBonusController_t3929995048::get_offset_of_firstStrikeBonus_3(),
	ResultBonusController_t3929995048::get_offset_of_perfectLaunchBonus_4(),
	ResultBonusController_t3929995048::get_offset_of_scoreZone_5(),
	ResultBonusController_t3929995048::get_offset_of_leftXPos_6(),
	ResultBonusController_t3929995048::get_offset_of_middileXPos_7(),
	ResultBonusController_t3929995048::get_offset_of_rightXPos_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof (ScoreZoneController_t1333878292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5790[10] = 
{
	ScoreZoneController_t1333878292::get_offset_of_resultModal_2(),
	ScoreZoneController_t1333878292::get_offset_of_leftSectionLabel_3(),
	ScoreZoneController_t1333878292::get_offset_of_middleSectionLabel_4(),
	ScoreZoneController_t1333878292::get_offset_of_rightSectionLabel_5(),
	ScoreZoneController_t1333878292::get_offset_of_trajectory_6(),
	ScoreZoneController_t1333878292::get_offset_of_timeLabel1_7(),
	ScoreZoneController_t1333878292::get_offset_of_timeLabel2_8(),
	ScoreZoneController_t1333878292::get_offset_of_timeLabel3_9(),
	ScoreZoneController_t1333878292::get_offset_of_timeLabel4_10(),
	ScoreZoneController_t1333878292::get_offset_of_stealthUI_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof (ResultModalUI_t969511824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5791[21] = 
{
	ResultModalUI_t969511824::get_offset_of_item_3(),
	ResultModalUI_t969511824::get_offset_of_currentItemUI_4(),
	ResultModalUI_t969511824::get_offset_of_positiveColour_5(),
	ResultModalUI_t969511824::get_offset_of_neutralColour_6(),
	ResultModalUI_t969511824::get_offset_of_negativeColour_7(),
	ResultModalUI_t969511824::get_offset_of_avatar_8(),
	ResultModalUI_t969511824::get_offset_of_flameAnim_9(),
	ResultModalUI_t969511824::get_offset_of_avatarFadeColour_10(),
	ResultModalUI_t969511824::get_offset_of_nameLabel_11(),
	ResultModalUI_t969511824::get_offset_of_ptsLabel_12(),
	ResultModalUI_t969511824::get_offset_of_actionLabel_13(),
	ResultModalUI_t969511824::get_offset_of_inLabel_14(),
	ResultModalUI_t969511824::get_offset_of_timeLabel_15(),
	ResultModalUI_t969511824::get_offset_of_itemUI_16(),
	ResultModalUI_t969511824::get_offset_of_resultLabels_17(),
	ResultModalUI_t969511824::get_offset_of_resultLabel_18(),
	ResultModalUI_t969511824::get_offset_of_resultPointsLabel_19(),
	ResultModalUI_t969511824::get_offset_of_resultStateLabel_20(),
	ResultModalUI_t969511824::get_offset_of_resultStateTween_21(),
	ResultModalUI_t969511824::get_offset_of_chatButton_22(),
	ResultModalUI_t969511824::get_offset_of_attackButton_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof (ActivateShieldButton_t4092835112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5792[1] = 
{
	ActivateShieldButton_t4092835112::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof (CloseWarningPopUpButton_t1445760822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5793[1] = 
{
	CloseWarningPopUpButton_t1445760822::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { sizeof (ShieldInfoButton_t822471353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5794[1] = 
{
	ShieldInfoButton_t822471353::get_offset_of_modal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof (ShieldModal_t4049164426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5795[17] = 
{
	ShieldModal_t4049164426::get_offset_of_currentItemIndex_3(),
	ShieldModal_t4049164426::get_offset_of_infoObject_4(),
	ShieldModal_t4049164426::get_offset_of_itemObject_5(),
	ShieldModal_t4049164426::get_offset_of_infoButton_6(),
	ShieldModal_t4049164426::get_offset_of_infoController_7(),
	ShieldModal_t4049164426::get_offset_of_swipeController_8(),
	ShieldModal_t4049164426::get_offset_of_itemNameLabel_9(),
	ShieldModal_t4049164426::get_offset_of_itemAmountLabel_10(),
	ShieldModal_t4049164426::get_offset_of_itemDurationLabel_11(),
	ShieldModal_t4049164426::get_offset_of_warningPopUp_12(),
	ShieldModal_t4049164426::get_offset_of_popupScaleTween_13(),
	ShieldModal_t4049164426::get_offset_of_warningLabel_14(),
	ShieldModal_t4049164426::get_offset_of_itemLight_15(),
	ShieldModal_t4049164426::get_offset_of_shieldItems_16(),
	ShieldModal_t4049164426::get_offset_of_showingInfo_17(),
	ShieldModal_t4049164426::get_offset_of_firstTimeLoading_18(),
	ShieldModal_t4049164426::get_offset_of_popUpClosing_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5796[4] = 
{
	U3CLoadDefaultModelU3Ec__Iterator0_t3873069052::get_offset_of_U24this_0(),
	U3CLoadDefaultModelU3Ec__Iterator0_t3873069052::get_offset_of_U24current_1(),
	U3CLoadDefaultModelU3Ec__Iterator0_t3873069052::get_offset_of_U24disposing_2(),
	U3CLoadDefaultModelU3Ec__Iterator0_t3873069052::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof (ShieldSelectionSwipe_t1465110497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5797[1] = 
{
	ShieldSelectionSwipe_t1465110497::get_offset_of_modal_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof (ShieldShopButton_t1762407207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5798[1] = 
{
	ShieldShopButton_t1762407207::get_offset_of_modal_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof (ShieldSwipeController_t2505851713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5799[1] = 
{
	ShieldSwipeController_t2505851713::get_offset_of_modal_23(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
