﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<StatusNavScreen>
struct List_1_t2241490215;
// System.Collections.Generic.IEnumerable`1<StatusNavScreen>
struct IEnumerable_1_t3164496128;
// StatusNavScreen[]
struct StatusNavScreenU5BU5D_t3698178810;
// System.Collections.Generic.IEnumerator`1<StatusNavScreen>
struct IEnumerator_1_t347892910;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<StatusNavScreen>
struct ICollection_1_t3824444388;
// System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>
struct ReadOnlyCollection_1_t3058154775;
// System.Predicate`1<StatusNavScreen>
struct Predicate_1_t1315339198;
// System.Action`1<StatusNavScreen>
struct Action_1_t2674168465;
// System.Collections.Generic.IComparer`1<StatusNavScreen>
struct IComparer_1_t826832205;
// System.Comparison`1<StatusNavScreen>
struct Comparison_1_t4134107934;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1776219889.h"

// System.Void System.Collections.Generic.List`1<StatusNavScreen>::.ctor()
extern "C"  void List_1__ctor_m3275742751_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1__ctor_m3275742751(__this, method) ((  void (*) (List_1_t2241490215 *, const MethodInfo*))List_1__ctor_m3275742751_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1897126256_gshared (List_1_t2241490215 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1897126256(__this, ___collection0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1897126256_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3722545038_gshared (List_1_t2241490215 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3722545038(__this, ___capacity0, method) ((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1__ctor_m3722545038_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2606492510_gshared (List_1_t2241490215 * __this, StatusNavScreenU5BU5D_t3698178810* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2606492510(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2241490215 *, StatusNavScreenU5BU5D_t3698178810*, int32_t, const MethodInfo*))List_1__ctor_m2606492510_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::.cctor()
extern "C"  void List_1__cctor_m976716778_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m976716778(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m976716778_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m874891735_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m874891735(__this, method) ((  Il2CppObject* (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m874891735_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2597418243_gshared (List_1_t2241490215 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2597418243(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2241490215 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2597418243_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1299012104_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1299012104(__this, method) ((  Il2CppObject * (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1299012104_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2908029315_gshared (List_1_t2241490215 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2908029315(__this, ___item0, method) ((  int32_t (*) (List_1_t2241490215 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2908029315_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m449678219_gshared (List_1_t2241490215 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m449678219(__this, ___item0, method) ((  bool (*) (List_1_t2241490215 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m449678219_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2367236401_gshared (List_1_t2241490215 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2367236401(__this, ___item0, method) ((  int32_t (*) (List_1_t2241490215 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2367236401_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3666691970_gshared (List_1_t2241490215 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3666691970(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2241490215 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3666691970_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m13077308_gshared (List_1_t2241490215 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13077308(__this, ___item0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13077308_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2939209012_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2939209012(__this, method) ((  bool (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2939209012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m805754131_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m805754131(__this, method) ((  bool (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m805754131_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m4178435475_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m4178435475(__this, method) ((  Il2CppObject * (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4178435475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2418391102_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2418391102(__this, method) ((  bool (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2418391102_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m368285879_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m368285879(__this, method) ((  bool (*) (List_1_t2241490215 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m368285879_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3043511466_gshared (List_1_t2241490215 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3043511466(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3043511466_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1641184817_gshared (List_1_t2241490215 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1641184817(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2241490215 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1641184817_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Add(T)
extern "C"  void List_1_Add_m2072784878_gshared (List_1_t2241490215 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2072784878(__this, ___item0, method) ((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_Add_m2072784878_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2274698047_gshared (List_1_t2241490215 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2274698047(__this, ___newCount0, method) ((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2274698047_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m790644518_gshared (List_1_t2241490215 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m790644518(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2241490215 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m790644518_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1738507143_gshared (List_1_t2241490215 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1738507143(__this, ___collection0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1738507143_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1754463607_gshared (List_1_t2241490215 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1754463607(__this, ___enumerable0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1754463607_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2909569112_gshared (List_1_t2241490215 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2909569112(__this, ___collection0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2909569112_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<StatusNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3058154775 * List_1_AsReadOnly_m3384973327_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3384973327(__this, method) ((  ReadOnlyCollection_1_t3058154775 * (*) (List_1_t2241490215 *, const MethodInfo*))List_1_AsReadOnly_m3384973327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Clear()
extern "C"  void List_1_Clear_m3386193018_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_Clear_m3386193018(__this, method) ((  void (*) (List_1_t2241490215 *, const MethodInfo*))List_1_Clear_m3386193018_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m1078116628_gshared (List_1_t2241490215 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m1078116628(__this, ___item0, method) ((  bool (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_Contains_m1078116628_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2318522375_gshared (List_1_t2241490215 * __this, StatusNavScreenU5BU5D_t3698178810* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m2318522375(__this, ___array0, method) ((  void (*) (List_1_t2241490215 *, StatusNavScreenU5BU5D_t3698178810*, const MethodInfo*))List_1_CopyTo_m2318522375_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m505864370_gshared (List_1_t2241490215 * __this, StatusNavScreenU5BU5D_t3698178810* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m505864370(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2241490215 *, StatusNavScreenU5BU5D_t3698178810*, int32_t, const MethodInfo*))List_1_CopyTo_m505864370_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m1562645728_gshared (List_1_t2241490215 * __this, int32_t ___index0, StatusNavScreenU5BU5D_t3698178810* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m1562645728(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t2241490215 *, int32_t, StatusNavScreenU5BU5D_t3698178810*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1562645728_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m165851340_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_Exists_m165851340(__this, ___match0, method) ((  bool (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_Exists_m165851340_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<StatusNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m477424170_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_Find_m477424170(__this, ___match0, method) ((  int32_t (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_Find_m477424170_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2874325747_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2874325747(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1315339198 *, const MethodInfo*))List_1_CheckMatch_m2874325747_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<StatusNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t2241490215 * List_1_FindAll_m1578381767_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m1578381767(__this, ___match0, method) ((  List_1_t2241490215 * (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_FindAll_m1578381767_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<StatusNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t2241490215 * List_1_FindAllStackBits_m376866117_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m376866117(__this, ___match0, method) ((  List_1_t2241490215 * (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_FindAllStackBits_m376866117_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<StatusNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t2241490215 * List_1_FindAllList_m2923795125_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2923795125(__this, ___match0, method) ((  List_1_t2241490215 * (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_FindAllList_m2923795125_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m2644586283_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m2644586283(__this, ___match0, method) ((  int32_t (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_FindIndex_m2644586283_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3806840162_gshared (List_1_t2241490215 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1315339198 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3806840162(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2241490215 *, int32_t, int32_t, Predicate_1_t1315339198 *, const MethodInfo*))List_1_GetIndex_m3806840162_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1671269465_gshared (List_1_t2241490215 * __this, Action_1_t2674168465 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1671269465(__this, ___action0, method) ((  void (*) (List_1_t2241490215 *, Action_1_t2674168465 *, const MethodInfo*))List_1_ForEach_m1671269465_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<StatusNavScreen>::GetEnumerator()
extern "C"  Enumerator_t1776219889  List_1_GetEnumerator_m1255399095_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1255399095(__this, method) ((  Enumerator_t1776219889  (*) (List_1_t2241490215 *, const MethodInfo*))List_1_GetEnumerator_m1255399095_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4172938212_gshared (List_1_t2241490215 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4172938212(__this, ___item0, method) ((  int32_t (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_IndexOf_m4172938212_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m317674191_gshared (List_1_t2241490215 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m317674191(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2241490215 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m317674191_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4073823546_gshared (List_1_t2241490215 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4073823546(__this, ___index0, method) ((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4073823546_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4217578773_gshared (List_1_t2241490215 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m4217578773(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2241490215 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m4217578773_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1092163080_gshared (List_1_t2241490215 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1092163080(__this, ___collection0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1092163080_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<StatusNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3871497029_gshared (List_1_t2241490215 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3871497029(__this, ___item0, method) ((  bool (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_Remove_m3871497029_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3387006875_gshared (List_1_t2241490215 * __this, Predicate_1_t1315339198 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3387006875(__this, ___match0, method) ((  int32_t (*) (List_1_t2241490215 *, Predicate_1_t1315339198 *, const MethodInfo*))List_1_RemoveAll_m3387006875_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m948036417_gshared (List_1_t2241490215 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m948036417(__this, ___index0, method) ((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_RemoveAt_m948036417_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m4226828638_gshared (List_1_t2241490215 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m4226828638(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2241490215 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m4226828638_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m2706004935_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_Reverse_m2706004935(__this, method) ((  void (*) (List_1_t2241490215 *, const MethodInfo*))List_1_Reverse_m2706004935_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Sort()
extern "C"  void List_1_Sort_m3952029285_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_Sort_m3952029285(__this, method) ((  void (*) (List_1_t2241490215 *, const MethodInfo*))List_1_Sort_m3952029285_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4114416841_gshared (List_1_t2241490215 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4114416841(__this, ___comparer0, method) ((  void (*) (List_1_t2241490215 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4114416841_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1218782034_gshared (List_1_t2241490215 * __this, Comparison_1_t4134107934 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1218782034(__this, ___comparison0, method) ((  void (*) (List_1_t2241490215 *, Comparison_1_t4134107934 *, const MethodInfo*))List_1_Sort_m1218782034_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<StatusNavScreen>::ToArray()
extern "C"  StatusNavScreenU5BU5D_t3698178810* List_1_ToArray_m3021703938_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_ToArray_m3021703938(__this, method) ((  StatusNavScreenU5BU5D_t3698178810* (*) (List_1_t2241490215 *, const MethodInfo*))List_1_ToArray_m3021703938_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2078670184_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2078670184(__this, method) ((  void (*) (List_1_t2241490215 *, const MethodInfo*))List_1_TrimExcess_m2078670184_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3800357182_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3800357182(__this, method) ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))List_1_get_Capacity_m3800357182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3992538759_gshared (List_1_t2241490215 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3992538759(__this, ___value0, method) ((  void (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3992538759_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<StatusNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m2034320755_gshared (List_1_t2241490215 * __this, const MethodInfo* method);
#define List_1_get_Count_m2034320755(__this, method) ((  int32_t (*) (List_1_t2241490215 *, const MethodInfo*))List_1_get_Count_m2034320755_gshared)(__this, method)
// T System.Collections.Generic.List`1<StatusNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m3996121133_gshared (List_1_t2241490215 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3996121133(__this, ___index0, method) ((  int32_t (*) (List_1_t2241490215 *, int32_t, const MethodInfo*))List_1_get_Item_m3996121133_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m443147640_gshared (List_1_t2241490215 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m443147640(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2241490215 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m443147640_gshared)(__this, ___index0, ___value1, method)
