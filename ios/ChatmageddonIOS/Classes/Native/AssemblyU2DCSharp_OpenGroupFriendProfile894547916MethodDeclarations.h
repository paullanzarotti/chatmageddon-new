﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenGroupFriendProfile
struct OpenGroupFriendProfile_t894547916;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenGroupFriendProfile::.ctor()
extern "C"  void OpenGroupFriendProfile__ctor_m4074362257 (OpenGroupFriendProfile_t894547916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenGroupFriendProfile::OnButtonClick()
extern "C"  void OpenGroupFriendProfile_OnButtonClick_m1438390368 (OpenGroupFriendProfile_t894547916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
