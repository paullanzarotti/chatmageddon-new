﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapGroupModal/<WaitToClose>c__Iterator0
struct U3CWaitToCloseU3Ec__Iterator0_t3065553700;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MapGroupModal/<WaitToClose>c__Iterator0::.ctor()
extern "C"  void U3CWaitToCloseU3Ec__Iterator0__ctor_m2511251525 (U3CWaitToCloseU3Ec__Iterator0_t3065553700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MapGroupModal/<WaitToClose>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToCloseU3Ec__Iterator0_MoveNext_m580977875 (U3CWaitToCloseU3Ec__Iterator0_t3065553700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MapGroupModal/<WaitToClose>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToCloseU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1549321823 (U3CWaitToCloseU3Ec__Iterator0_t3065553700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MapGroupModal/<WaitToClose>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToCloseU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m127520263 (U3CWaitToCloseU3Ec__Iterator0_t3065553700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal/<WaitToClose>c__Iterator0::Dispose()
extern "C"  void U3CWaitToCloseU3Ec__Iterator0_Dispose_m476477918 (U3CWaitToCloseU3Ec__Iterator0_t3065553700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapGroupModal/<WaitToClose>c__Iterator0::Reset()
extern "C"  void U3CWaitToCloseU3Ec__Iterator0_Reset_m3111723152 (U3CWaitToCloseU3Ec__Iterator0_t3065553700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
