﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneNumberDesc/Builder
struct Builder_t474280692;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc922391174.h"

// System.Void PhoneNumbers.PhoneNumberDesc::.cctor()
extern "C"  void PhoneNumberDesc__cctor_m1318141106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberDesc::.ctor()
extern "C"  void PhoneNumberDesc__ctor_m3345226479 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc::get_DefaultInstance()
extern "C"  PhoneNumberDesc_t922391174 * PhoneNumberDesc_get_DefaultInstance_m3960143455 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc::get_DefaultInstanceForType()
extern "C"  PhoneNumberDesc_t922391174 * PhoneNumberDesc_get_DefaultInstanceForType_m2617587652 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc::get_ThisMessage()
extern "C"  PhoneNumberDesc_t922391174 * PhoneNumberDesc_get_ThisMessage_m197864450 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc::get_HasNationalNumberPattern()
extern "C"  bool PhoneNumberDesc_get_HasNationalNumberPattern_m957487549 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberDesc::get_NationalNumberPattern()
extern "C"  String_t* PhoneNumberDesc_get_NationalNumberPattern_m3924054728 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc::get_HasPossibleNumberPattern()
extern "C"  bool PhoneNumberDesc_get_HasPossibleNumberPattern_m3478052736 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberDesc::get_PossibleNumberPattern()
extern "C"  String_t* PhoneNumberDesc_get_PossibleNumberPattern_m2582573057 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc::get_HasExampleNumber()
extern "C"  bool PhoneNumberDesc_get_HasExampleNumber_m3639203867 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberDesc::get_ExampleNumber()
extern "C"  String_t* PhoneNumberDesc_get_ExampleNumber_m3329097832 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc::get_IsInitialized()
extern "C"  bool PhoneNumberDesc_get_IsInitialized_m3413865780 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberDesc::GetHashCode()
extern "C"  int32_t PhoneNumberDesc_GetHashCode_m3462824932 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberDesc::Equals(System.Object)
extern "C"  bool PhoneNumberDesc_Equals_m425029716 (PhoneNumberDesc_t922391174 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc::CreateBuilder()
extern "C"  Builder_t474280692 * PhoneNumberDesc_CreateBuilder_m533255291 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc::ToBuilder()
extern "C"  Builder_t474280692 * PhoneNumberDesc_ToBuilder_m1964568146 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc::CreateBuilderForType()
extern "C"  Builder_t474280692 * PhoneNumberDesc_CreateBuilderForType_m547349462 (PhoneNumberDesc_t922391174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc/Builder PhoneNumbers.PhoneNumberDesc::CreateBuilder(PhoneNumbers.PhoneNumberDesc)
extern "C"  Builder_t474280692 * PhoneNumberDesc_CreateBuilder_m4243862431 (Il2CppObject * __this /* static, unused */, PhoneNumberDesc_t922391174 * ___prototype0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
