﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ELANManager
struct ELANManager_t1808384203;
// ELANNotification
struct ELANNotification_t786863861;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ELANNotification786863861.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ELANManager::.ctor()
extern "C"  void ELANManager__ctor_m3000966678 (ELANManager_t1808384203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANManager::sendParametrizedNotification(ELANNotification)
extern "C"  void ELANManager_sendParametrizedNotification_m2345473750 (Il2CppObject * __this /* static, unused */, ELANNotification_t786863861 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ELANManager::SendNotification(System.String,System.String,System.Int64)
extern "C"  int32_t ELANManager_SendNotification_m2554984561 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, int64_t ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANManager::ScheduleRepeatingNotification(System.String,System.String,System.Int64,System.Int64)
extern "C"  void ELANManager_ScheduleRepeatingNotification_m2264488383 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, int64_t ___delay2, int64_t ___rep3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANManager::CancelRepeatingNotification()
extern "C"  void ELANManager_CancelRepeatingNotification_m882926606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANManager::CancelAllNotifications()
extern "C"  void ELANManager_CancelAllNotifications_m2725850445 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANManager::CancelLocalNotification(System.Int32)
extern "C"  void ELANManager_CancelLocalNotification_m1762503691 (Il2CppObject * __this /* static, unused */, int32_t ___nID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
