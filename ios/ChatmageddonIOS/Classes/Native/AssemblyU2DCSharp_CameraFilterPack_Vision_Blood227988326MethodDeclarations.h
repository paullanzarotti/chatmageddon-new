﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Vision_Blood
struct CameraFilterPack_Vision_Blood_t227988326;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Vision_Blood::.ctor()
extern "C"  void CameraFilterPack_Vision_Blood__ctor_m2253339149 (CameraFilterPack_Vision_Blood_t227988326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Blood::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Vision_Blood_get_material_m3274121434 (CameraFilterPack_Vision_Blood_t227988326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::Start()
extern "C"  void CameraFilterPack_Vision_Blood_Start_m3900021421 (CameraFilterPack_Vision_Blood_t227988326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Vision_Blood_OnRenderImage_m703055421 (CameraFilterPack_Vision_Blood_t227988326 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::OnValidate()
extern "C"  void CameraFilterPack_Vision_Blood_OnValidate_m4094778578 (CameraFilterPack_Vision_Blood_t227988326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::Update()
extern "C"  void CameraFilterPack_Vision_Blood_Update_m695164616 (CameraFilterPack_Vision_Blood_t227988326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::OnDisable()
extern "C"  void CameraFilterPack_Vision_Blood_OnDisable_m1816974110 (CameraFilterPack_Vision_Blood_t227988326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
