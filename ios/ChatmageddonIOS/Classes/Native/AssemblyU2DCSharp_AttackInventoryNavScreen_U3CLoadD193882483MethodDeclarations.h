﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0
struct U3CLoadDefaultModelU3Ec__Iterator0_t193882483;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0::.ctor()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0__ctor_m3710205828 (U3CLoadDefaultModelU3Ec__Iterator0_t193882483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadDefaultModelU3Ec__Iterator0_MoveNext_m114786600 (U3CLoadDefaultModelU3Ec__Iterator0_t193882483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3866334574 (U3CLoadDefaultModelU3Ec__Iterator0_t193882483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4244414550 (U3CLoadDefaultModelU3Ec__Iterator0_t193882483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0::Dispose()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0_Dispose_m164831933 (U3CLoadDefaultModelU3Ec__Iterator0_t193882483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen/<LoadDefaultModel>c__Iterator0::Reset()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0_Reset_m1743581151 (U3CLoadDefaultModelU3Ec__Iterator0_t193882483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
