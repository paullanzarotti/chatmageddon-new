﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EulaNavScreen
struct EulaNavScreen_t1232694410;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void EulaNavScreen::.ctor()
extern "C"  void EulaNavScreen__ctor_m3705818163 (EulaNavScreen_t1232694410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EulaNavScreen::Start()
extern "C"  void EulaNavScreen_Start_m2168524623 (EulaNavScreen_t1232694410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EulaNavScreen::UIClosing()
extern "C"  void EulaNavScreen_UIClosing_m3676450176 (EulaNavScreen_t1232694410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EulaNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void EulaNavScreen_ScreenLoad_m1265107353 (EulaNavScreen_t1232694410 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EulaNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void EulaNavScreen_ScreenUnload_m3231411349 (EulaNavScreen_t1232694410 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EulaNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool EulaNavScreen_ValidateScreenNavigation_m1049673610 (EulaNavScreen_t1232694410 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
