﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConnectionStringsSection
struct ConnectionStringsSection_t306961967;
// System.Configuration.ConnectionStringSettingsCollection
struct ConnectionStringSettingsCollection_t2042791570;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Configuration.ConnectionStringsSection::.ctor()
extern "C"  void ConnectionStringsSection__ctor_m1619232636 (ConnectionStringsSection_t306961967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringsSection::.cctor()
extern "C"  void ConnectionStringsSection__cctor_m2364551963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConnectionStringSettingsCollection System.Configuration.ConnectionStringsSection::get_ConnectionStrings()
extern "C"  ConnectionStringSettingsCollection_t2042791570 * ConnectionStringsSection_get_ConnectionStrings_m492400611 (ConnectionStringsSection_t306961967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConnectionStringsSection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * ConnectionStringsSection_get_Properties_m2498059919 (ConnectionStringsSection_t306961967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ConnectionStringsSection::GetRuntimeObject()
extern "C"  Il2CppObject * ConnectionStringsSection_GetRuntimeObject_m4218459792 (ConnectionStringsSection_t306961967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
