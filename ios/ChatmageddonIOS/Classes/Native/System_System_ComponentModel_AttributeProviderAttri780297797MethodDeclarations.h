﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.AttributeProviderAttribute
struct AttributeProviderAttribute_t780297797;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.ComponentModel.AttributeProviderAttribute::.ctor(System.Type)
extern "C"  void AttributeProviderAttribute__ctor_m1383786237 (AttributeProviderAttribute_t780297797 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AttributeProviderAttribute::.ctor(System.String,System.String)
extern "C"  void AttributeProviderAttribute__ctor_m1329573186 (AttributeProviderAttribute_t780297797 * __this, String_t* ___typeName0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AttributeProviderAttribute::.ctor(System.String)
extern "C"  void AttributeProviderAttribute__ctor_m1462981174 (AttributeProviderAttribute_t780297797 * __this, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.AttributeProviderAttribute::get_PropertyName()
extern "C"  String_t* AttributeProviderAttribute_get_PropertyName_m456197368 (AttributeProviderAttribute_t780297797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.AttributeProviderAttribute::get_TypeName()
extern "C"  String_t* AttributeProviderAttribute_get_TypeName_m2467934769 (AttributeProviderAttribute_t780297797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
