﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActivateUserStealthModeButton
struct ActivateUserStealthModeButton_t2608034790;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ActivateUserStealthModeButton::.ctor()
extern "C"  void ActivateUserStealthModeButton__ctor_m92245517 (ActivateUserStealthModeButton_t2608034790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateUserStealthModeButton::OnButtonClick()
extern "C"  void ActivateUserStealthModeButton_OnButtonClick_m2121465994 (ActivateUserStealthModeButton_t2608034790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateUserStealthModeButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ActivateUserStealthModeButton_U3COnButtonClickU3Em__0_m36416430 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
