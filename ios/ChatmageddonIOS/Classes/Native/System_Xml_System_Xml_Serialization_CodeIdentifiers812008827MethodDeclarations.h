﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t812008827;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Xml.Serialization.CodeIdentifiers::.ctor()
extern "C"  void CodeIdentifiers__ctor_m937221406 (CodeIdentifiers_t812008827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.CodeIdentifiers::.ctor(System.Boolean)
extern "C"  void CodeIdentifiers__ctor_m3539888483 (CodeIdentifiers_t812008827 * __this, bool ___caseSensitive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.CodeIdentifiers::Add(System.String,System.Object)
extern "C"  void CodeIdentifiers_Add_m426095185 (CodeIdentifiers_t812008827 * __this, String_t* ___identifier0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.CodeIdentifiers::AddUnique(System.String,System.Object)
extern "C"  String_t* CodeIdentifiers_AddUnique_m1984612683 (CodeIdentifiers_t812008827 * __this, String_t* ___identifier0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Serialization.CodeIdentifiers::IsInUse(System.String)
extern "C"  bool CodeIdentifiers_IsInUse_m4614636 (CodeIdentifiers_t812008827 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Serialization.CodeIdentifiers::MakeUnique(System.String)
extern "C"  String_t* CodeIdentifiers_MakeUnique_m3296989006 (CodeIdentifiers_t812008827 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
