﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.Vector3Pair
struct  Vector3Pair_t2859078138 
{
public:
	// UnityEngine.Vector3 Vectrosity.Vector3Pair::p1
	Vector3_t2243707580  ___p1_0;
	// UnityEngine.Vector3 Vectrosity.Vector3Pair::p2
	Vector3_t2243707580  ___p2_1;

public:
	inline static int32_t get_offset_of_p1_0() { return static_cast<int32_t>(offsetof(Vector3Pair_t2859078138, ___p1_0)); }
	inline Vector3_t2243707580  get_p1_0() const { return ___p1_0; }
	inline Vector3_t2243707580 * get_address_of_p1_0() { return &___p1_0; }
	inline void set_p1_0(Vector3_t2243707580  value)
	{
		___p1_0 = value;
	}

	inline static int32_t get_offset_of_p2_1() { return static_cast<int32_t>(offsetof(Vector3Pair_t2859078138, ___p2_1)); }
	inline Vector3_t2243707580  get_p2_1() const { return ___p2_1; }
	inline Vector3_t2243707580 * get_address_of_p2_1() { return &___p2_1; }
	inline void set_p2_1(Vector3_t2243707580  value)
	{
		___p2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
