﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<AttackSearchNav>
struct Collection_1_t3799629391;
// System.Collections.Generic.IList`1<AttackSearchNav>
struct IList_1_t503857942;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AttackSearchNav[]
struct AttackSearchNavU5BU5D_t406956560;
// System.Collections.Generic.IEnumerator`1<AttackSearchNav>
struct IEnumerator_1_t1733408464;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"

// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::.ctor()
extern "C"  void Collection_1__ctor_m3963809014_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3963809014(__this, method) ((  void (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1__ctor_m3963809014_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m538476169_gshared (Collection_1_t3799629391 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m538476169(__this, ___list0, method) ((  void (*) (Collection_1_t3799629391 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m538476169_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4250755383_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4250755383(__this, method) ((  bool (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4250755383_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m489462410_gshared (Collection_1_t3799629391 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m489462410(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3799629391 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m489462410_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m4057903435_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m4057903435(__this, method) ((  Il2CppObject * (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m4057903435_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1572550726_gshared (Collection_1_t3799629391 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1572550726(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3799629391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1572550726_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3993997672_gshared (Collection_1_t3799629391 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3993997672(__this, ___value0, method) ((  bool (*) (Collection_1_t3799629391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3993997672_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2052211936_gshared (Collection_1_t3799629391 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2052211936(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3799629391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2052211936_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m271761499_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m271761499(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m271761499_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m16654867_gshared (Collection_1_t3799629391 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16654867(__this, ___value0, method) ((  void (*) (Collection_1_t3799629391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16654867_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3759393622_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3759393622(__this, method) ((  bool (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3759393622_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2744667450_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2744667450(__this, method) ((  Il2CppObject * (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2744667450_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1314263035_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1314263035(__this, method) ((  bool (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1314263035_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m347752602_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m347752602(__this, method) ((  bool (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m347752602_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3732466547_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3732466547(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3732466547_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m931420004_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m931420004(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m931420004_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::Add(T)
extern "C"  void Collection_1_Add_m3161192455_gshared (Collection_1_t3799629391 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m3161192455(__this, ___item0, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_Add_m3161192455_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::Clear()
extern "C"  void Collection_1_Clear_m3763681811_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3763681811(__this, method) ((  void (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_Clear_m3763681811_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2273103341_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2273103341(__this, method) ((  void (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_ClearItems_m2273103341_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::Contains(T)
extern "C"  bool Collection_1_Contains_m356636517_gshared (Collection_1_t3799629391 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m356636517(__this, ___item0, method) ((  bool (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_Contains_m356636517_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2360609659_gshared (Collection_1_t3799629391 * __this, AttackSearchNavU5BU5D_t406956560* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2360609659(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3799629391 *, AttackSearchNavU5BU5D_t406956560*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2360609659_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<AttackSearchNav>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1803480090_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1803480090(__this, method) ((  Il2CppObject* (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_GetEnumerator_m1803480090_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackSearchNav>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m568028887_gshared (Collection_1_t3799629391 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m568028887(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m568028887_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m114934656_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m114934656(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m114934656_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1163677237_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1163677237(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1163677237_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::Remove(T)
extern "C"  bool Collection_1_Remove_m1840998366_gshared (Collection_1_t3799629391 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1840998366(__this, ___item0, method) ((  bool (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_Remove_m1840998366_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m57041644_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m57041644(__this, ___index0, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m57041644_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m848030042_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m848030042(__this, ___index0, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m848030042_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AttackSearchNav>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3619836150_gshared (Collection_1_t3799629391 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3619836150(__this, method) ((  int32_t (*) (Collection_1_t3799629391 *, const MethodInfo*))Collection_1_get_Count_m3619836150_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<AttackSearchNav>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m2496560482_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2496560482(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3799629391 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2496560482_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3516918703_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3516918703(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3516918703_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m164412836_gshared (Collection_1_t3799629391 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m164412836(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3799629391 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m164412836_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2787775361_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2787775361(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2787775361_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<AttackSearchNav>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m928447221_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m928447221(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m928447221_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AttackSearchNav>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1970266953_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1970266953(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1970266953_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2512973145_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2512973145(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2512973145_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AttackSearchNav>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2750628948_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2750628948(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2750628948_gshared)(__this /* static, unused */, ___list0, method)
