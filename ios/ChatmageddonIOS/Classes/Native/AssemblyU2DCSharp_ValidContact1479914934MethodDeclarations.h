﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ValidContact
struct ValidContact_t1479914934;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ValidContact::.ctor()
extern "C"  void ValidContact__ctor_m3865902013 (ValidContact_t1479914934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValidContact::AddContactName(System.String)
extern "C"  void ValidContact_AddContactName_m2146732781 (ValidContact_t1479914934 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValidContact::AddContactPhoneNumber(System.String)
extern "C"  void ValidContact_AddContactPhoneNumber_m2410896373 (ValidContact_t1479914934 * __this, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ValidContact::numberToHash(System.String)
extern "C"  String_t* ValidContact_numberToHash_m4186448226 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ValidContact::ByteArrayToString(System.Byte[])
extern "C"  String_t* ValidContact_ByteArrayToString_m3394199796 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ValidContact::getValidatedChecksum()
extern "C"  String_t* ValidContact_getValidatedChecksum_m1050687113 (ValidContact_t1479914934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
