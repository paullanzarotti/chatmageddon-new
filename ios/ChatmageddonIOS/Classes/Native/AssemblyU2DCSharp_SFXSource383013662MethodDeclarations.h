﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SFXSource
struct SFXSource_t383013662;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void SFXSource::.ctor()
extern "C"  void SFXSource__ctor_m4001573651 (SFXSource_t383013662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SFXSource::HideObjectCheck()
extern "C"  Il2CppObject * SFXSource_HideObjectCheck_m105990808 (SFXSource_t383013662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
