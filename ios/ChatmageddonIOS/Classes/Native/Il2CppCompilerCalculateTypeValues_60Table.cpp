﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MineDefuseGameUIScaler1486595461.h"
#include "AssemblyU2DCSharp_MineDefuseSwipe2955699161.h"
#include "AssemblyU2DCSharp_MineInfoController2990208339.h"
#include "AssemblyU2DCSharp_MineInfoController_U3CWaitForSec3074711450.h"
#include "AssemblyU2DCSharp_MineInfoSkipButton3356596800.h"
#include "AssemblyU2DCSharp_MineManager2821846902.h"
#include "AssemblyU2DCSharp_MineMiniGameController2791970086.h"
#include "AssemblyU2DCSharp_LocationDistanceController4191421708.h"
#include "AssemblyU2DCSharp_LocationDistanceController_U3CCh4198671817.h"
#include "AssemblyU2DCSharp_RunDistanceDisplay1358936964.h"
#include "AssemblyU2DCSharp_RunMiniGame569217060.h"
#include "AssemblyU2DCSharp_ScreamBarController3220293990.h"
#include "AssemblyU2DCSharp_ScreamBarController_U3CCheckScre1250448833.h"
#include "AssemblyU2DCSharp_ScreamMiniGame2794661384.h"
#include "AssemblyU2DCSharp_ShakeBarController4159664603.h"
#include "AssemblyU2DCSharp_ShakeBarController_U3CCheckShake3478000993.h"
#include "AssemblyU2DCSharp_ShakeMiniGame406515851.h"
#include "AssemblyU2DCSharp_AttemptTab3964708364.h"
#include "AssemblyU2DCSharp_DefendAttemptController232561863.h"
#include "AssemblyU2DCSharp_PlayState1063998429.h"
#include "AssemblyU2DCSharp_RotatingTab2077750253.h"
#include "AssemblyU2DCSharp_RotatingTab_U3CRotateTabU3Ec__It1022532947.h"
#include "AssemblyU2DCSharp_RotatingTabController2617193705.h"
#include "AssemblyU2DCSharp_StandardDefendButton3215661011.h"
#include "AssemblyU2DCSharp_StandardDefendButton_U3CWaitToAct760904360.h"
#include "AssemblyU2DCSharp_StandardMiniGame3672006024.h"
#include "AssemblyU2DCSharp_MiniGameController3067676539.h"
#include "AssemblyU2DCSharp_CloseLaunchSequenceButton1852628538.h"
#include "AssemblyU2DCSharp_TimeOfDay3632731134.h"
#include "AssemblyU2DCSharp_LaunchAnimationController825305345.h"
#include "AssemblyU2DCSharp_LaunchCountdownTimer1840221825.h"
#include "AssemblyU2DCSharp_LaunchCountdownTimer_OnZeroHit1439474620.h"
#include "AssemblyU2DCSharp_LaunchCountdownTimer_U3CCalculat2295680361.h"
#include "AssemblyU2DCSharp_Alphabet1535192461.h"
#include "AssemblyU2DCSharp_LaunchRoutine3791963153.h"
#include "AssemblyU2DCSharp_LaunchRoutine_U3CreverseAnimWait2216747469.h"
#include "AssemblyU2DCSharp_LaunchRoutine_U3CStep2AnimRender2943538468.h"
#include "AssemblyU2DCSharp_LaunchRoutine_U3CFinishAnimRende1156187826.h"
#include "AssemblyU2DCSharp_LaunchRoutine_U3CWaitForAnimEndU2692169692.h"
#include "AssemblyU2DCSharp_LaunchRoutine_U3CTimedGlitchEffe1802922878.h"
#include "AssemblyU2DCSharp_LaunchRoutine_U3CWaitToTransitio4231294373.h"
#include "AssemblyU2DCSharp_LaunchSequenceReport983071960.h"
#include "AssemblyU2DCSharp_LaunchState3704349340.h"
#include "AssemblyU2DCSharp_LaunchStateController3361989840.h"
#include "AssemblyU2DCSharp_LauncherSceneManager2478891917.h"
#include "AssemblyU2DCSharp_TVScaleEffect2256617735.h"
#include "AssemblyU2DCSharp_LaunchSequenceUIScaler2928648964.h"
#include "AssemblyU2DCSharp_ContactUsUI1917862594.h"
#include "AssemblyU2DCSharp_EULAUI3168190395.h"
#include "AssemblyU2DCSharp_InstructionsUI253378877.h"
#include "AssemblyU2DCSharp_PrivacyPolicyUI524063172.h"
#include "AssemblyU2DCSharp_QuestionUI3721436426.h"
#include "AssemblyU2DCSharp_TermsOfServiceUI3223215579.h"
#include "AssemblyU2DCSharp_AboutAndHelpUI3809615169.h"
#include "AssemblyU2DCSharp_AboutAndHelpNavScreen3086328594.h"
#include "AssemblyU2DCSharp_AudioNavScreen3651651169.h"
#include "AssemblyU2DCSharp_BlockILP879176280.h"
#include "AssemblyU2DCSharp_BlockListItem1514504046.h"
#include "AssemblyU2DCSharp_BlockListNavScreen522977310.h"
#include "AssemblyU2DCSharp_ContactUsScreenNav3711221493.h"
#include "AssemblyU2DCSharp_DisplayLocationSwitch413816511.h"
#include "AssemblyU2DCSharp_EditEmailNavScreen3910863955.h"
#include "AssemblyU2DCSharp_EditEmailNavScreen_U3CSaveScreen2708455943.h"
#include "AssemblyU2DCSharp_EditEmailNavScreen_U3CSaveScreenC930415982.h"
#include "AssemblyU2DCSharp_EditNameNavScreen2089604456.h"
#include "AssemblyU2DCSharp_EditNameNavScreen_U3CSaveScreenCo615035256.h"
#include "AssemblyU2DCSharp_EditNameNavScreen_U3CSaveScreenC3471157711.h"
#include "AssemblyU2DCSharp_EditUsernameNav3313102785.h"
#include "AssemblyU2DCSharp_EditUsernameNav_U3CSaveScreenCont285889113.h"
#include "AssemblyU2DCSharp_EditUsernameNav_U3CSaveScreenCont747550342.h"
#include "AssemblyU2DCSharp_EulaNavScreen1232694410.h"
#include "AssemblyU2DCSharp_FAQ2837997648.h"
#include "AssemblyU2DCSharp_FAQILP1146604377.h"
#include "AssemblyU2DCSharp_FAQListItem1761156371.h"
#include "AssemblyU2DCSharp_FAQNavScreen2266159941.h"
#include "AssemblyU2DCSharp_GenderNavScreen2608406338.h"
#include "AssemblyU2DCSharp_GenderNavScreen_U3CSaveScreenCont751163754.h"
#include "AssemblyU2DCSharp_GenderNavScreen_U3CSaveScreenCon2686197043.h"
#include "AssemblyU2DCSharp_GenderSwitch696294539.h"
#include "AssemblyU2DCSharp_HomeNavScreen3588214316.h"
#include "AssemblyU2DCSharp_IGNotificationSoundSwitch1960044298.h"
#include "AssemblyU2DCSharp_IGNotificationVibrationSwitch507453717.h"
#include "AssemblyU2DCSharp_IGNotificationsSwitch111074964.h"
#include "AssemblyU2DCSharp_InstructionNavScreen2629088027.h"
#include "AssemblyU2DCSharp_LogOutTransition3259858399.h"
#include "AssemblyU2DCSharp_MusicSwitch1246590877.h"
#include "AssemblyU2DCSharp_NotificationsNavScreen3203037389.h"
#include "AssemblyU2DCSharp_NumberPickerTransform3121159539.h"
#include "AssemblyU2DCSharp_OpenEmailButton3875630000.h"
#include "AssemblyU2DCSharp_TimeNumberPicker1112809442.h"
#include "AssemblyU2DCSharp_TimeNumberPicker_U3CWaitU3Ec__Ite237771877.h"
#include "AssemblyU2DCSharp_PrivacyPolicyNavScreen2706854281.h"
#include "AssemblyU2DCSharp_SFXSwitch3206288945.h"
#include "AssemblyU2DCSharp_SelectAudienceNavScreen4274980323.h"
#include "AssemblyU2DCSharp_SelectAudienceNavScreen_U3CSaveS2360964071.h"
#include "AssemblyU2DCSharp_SelectAudienceNavScreen_U3CSaveS1300706356.h"
#include "AssemblyU2DCSharp_SelectAudienceSwitch3539699246.h"
#include "AssemblyU2DCSharp_SelectAudienceSwitchButton3845187086.h"
#include "AssemblyU2DCSharp_SelectGenderSwitchButton1386910845.h"
#include "AssemblyU2DCSharp_SettingsHomeContent249573869.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000 = { sizeof (MineDefuseGameUIScaler_t1486595461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6000[7] = 
{
	MineDefuseGameUIScaler_t1486595461::get_offset_of_panelCollider_14(),
	MineDefuseGameUIScaler_t1486595461::get_offset_of_background_15(),
	MineDefuseGameUIScaler_t1486595461::get_offset_of_titleSeperator_16(),
	MineDefuseGameUIScaler_t1486595461::get_offset_of_mineAnim_17(),
	MineDefuseGameUIScaler_t1486595461::get_offset_of_swipeCollider1_18(),
	MineDefuseGameUIScaler_t1486595461::get_offset_of_swipeCollider2_19(),
	MineDefuseGameUIScaler_t1486595461::get_offset_of_swipeCollider3_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001 = { sizeof (MineDefuseSwipe_t2955699161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6001[2] = 
{
	MineDefuseSwipe_t2955699161::get_offset_of_gamecontroller_10(),
	MineDefuseSwipe_t2955699161::get_offset_of_swipeNumber_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002 = { sizeof (MineInfoController_t2990208339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6002[12] = 
{
	MineInfoController_t2990208339::get_offset_of_gameController_2(),
	MineInfoController_t2990208339::get_offset_of_posTween_3(),
	MineInfoController_t2990208339::get_offset_of_avatar_4(),
	MineInfoController_t2990208339::get_offset_of_firstNameLabel_5(),
	MineInfoController_t2990208339::get_offset_of_lastNameLabel_6(),
	MineInfoController_t2990208339::get_offset_of_itemNameLabel_7(),
	MineInfoController_t2990208339::get_offset_of_actionLabel_8(),
	MineInfoController_t2990208339::get_offset_of_timer_9(),
	MineInfoController_t2990208339::get_offset_of_modelParent_10(),
	MineInfoController_t2990208339::get_offset_of_currentModel_11(),
	MineInfoController_t2990208339::get_offset_of_itemLight_12(),
	MineInfoController_t2990208339::get_offset_of_uiClosing_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003 = { sizeof (U3CWaitForSecondsU3Ec__Iterator0_t3074711450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6003[5] = 
{
	U3CWaitForSecondsU3Ec__Iterator0_t3074711450::get_offset_of_amountToWait_0(),
	U3CWaitForSecondsU3Ec__Iterator0_t3074711450::get_offset_of_U24this_1(),
	U3CWaitForSecondsU3Ec__Iterator0_t3074711450::get_offset_of_U24current_2(),
	U3CWaitForSecondsU3Ec__Iterator0_t3074711450::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsU3Ec__Iterator0_t3074711450::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004 = { sizeof (MineInfoSkipButton_t3356596800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6004[1] = 
{
	MineInfoSkipButton_t3356596800::get_offset_of_infoController_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005 = { sizeof (MineManager_t2821846902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6005[4] = 
{
	MineManager_t2821846902::get_offset_of_activeMine_3(),
	MineManager_t2821846902::get_offset_of_miniGameObject_4(),
	MineManager_t2821846902::get_offset_of_gameController_5(),
	MineManager_t2821846902::get_offset_of_uiResourcePath_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006 = { sizeof (MineMiniGameController_t2791970086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6006[7] = 
{
	MineMiniGameController_t2791970086::get_offset_of_posTween_2(),
	MineMiniGameController_t2791970086::get_offset_of_infoController_3(),
	MineMiniGameController_t2791970086::get_offset_of_gameCountdownTimer_4(),
	MineMiniGameController_t2791970086::get_offset_of_timerAlphaTween_5(),
	MineMiniGameController_t2791970086::get_offset_of_swipes_6(),
	MineMiniGameController_t2791970086::get_offset_of_mineAnimation_7(),
	MineMiniGameController_t2791970086::get_offset_of_closingUI_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007 = { sizeof (LocationDistanceController_t4191421708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6007[6] = 
{
	LocationDistanceController_t4191421708::get_offset_of_miniGame_2(),
	LocationDistanceController_t4191421708::get_offset_of_gps_3(),
	LocationDistanceController_t4191421708::get_offset_of_maxDistance_4(),
	LocationDistanceController_t4191421708::get_offset_of_distanceDisplay_5(),
	LocationDistanceController_t4191421708::get_offset_of_remainingDistanceLabel_6(),
	LocationDistanceController_t4191421708::get_offset_of_distanceCheckRoutine_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008 = { sizeof (U3CCheckDistanceU3Ec__Iterator0_t4198671817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6008[4] = 
{
	U3CCheckDistanceU3Ec__Iterator0_t4198671817::get_offset_of_U24this_0(),
	U3CCheckDistanceU3Ec__Iterator0_t4198671817::get_offset_of_U24current_1(),
	U3CCheckDistanceU3Ec__Iterator0_t4198671817::get_offset_of_U24disposing_2(),
	U3CCheckDistanceU3Ec__Iterator0_t4198671817::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009 = { sizeof (RunDistanceDisplay_t1358936964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6009[5] = 
{
	RunDistanceDisplay_t1358936964::get_offset_of_safeRing_2(),
	RunDistanceDisplay_t1358936964::get_offset_of_dangerRing_3(),
	RunDistanceDisplay_t1358936964::get_offset_of_playerLocation_4(),
	RunDistanceDisplay_t1358936964::get_offset_of_screenStartPosition_5(),
	RunDistanceDisplay_t1358936964::get_offset_of_pxPerMeter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010 = { sizeof (RunMiniGame_t569217060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6010[1] = 
{
	RunMiniGame_t569217060::get_offset_of_distanceController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011 = { sizeof (ScreamBarController_t3220293990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6011[9] = 
{
	ScreamBarController_t3220293990::get_offset_of_miniGame_2(),
	ScreamBarController_t3220293990::get_offset_of_maxScreamSeconds_3(),
	ScreamBarController_t3220293990::get_offset_of_minimumMicVolume_4(),
	ScreamBarController_t3220293990::get_offset_of_maximumMicVolume_5(),
	ScreamBarController_t3220293990::get_offset_of_micChecker_6(),
	ScreamBarController_t3220293990::get_offset_of_progressBar_7(),
	ScreamBarController_t3220293990::get_offset_of_screamCheckRoutine_8(),
	ScreamBarController_t3220293990::get_offset_of_timeLabel_9(),
	ScreamBarController_t3220293990::get_offset_of_totalTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012 = { sizeof (U3CCheckScreamU3Ec__Iterator0_t1250448833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6012[5] = 
{
	U3CCheckScreamU3Ec__Iterator0_t1250448833::get_offset_of_U3CvolumeU3E__0_0(),
	U3CCheckScreamU3Ec__Iterator0_t1250448833::get_offset_of_U24this_1(),
	U3CCheckScreamU3Ec__Iterator0_t1250448833::get_offset_of_U24current_2(),
	U3CCheckScreamU3Ec__Iterator0_t1250448833::get_offset_of_U24disposing_3(),
	U3CCheckScreamU3Ec__Iterator0_t1250448833::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013 = { sizeof (ScreamMiniGame_t2794661384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6013[1] = 
{
	ScreamMiniGame_t2794661384::get_offset_of_screamBar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014 = { sizeof (ShakeBarController_t4159664603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6014[7] = 
{
	ShakeBarController_t4159664603::get_offset_of_miniGame_2(),
	ShakeBarController_t4159664603::get_offset_of_maxShakeSeconds_3(),
	ShakeBarController_t4159664603::get_offset_of_shakeChecker_4(),
	ShakeBarController_t4159664603::get_offset_of_progressBar_5(),
	ShakeBarController_t4159664603::get_offset_of_shakeCheckRoutine_6(),
	ShakeBarController_t4159664603::get_offset_of_timeLabel_7(),
	ShakeBarController_t4159664603::get_offset_of_totalTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015 = { sizeof (U3CCheckShakeU3Ec__Iterator0_t3478000993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6015[4] = 
{
	U3CCheckShakeU3Ec__Iterator0_t3478000993::get_offset_of_U24this_0(),
	U3CCheckShakeU3Ec__Iterator0_t3478000993::get_offset_of_U24current_1(),
	U3CCheckShakeU3Ec__Iterator0_t3478000993::get_offset_of_U24disposing_2(),
	U3CCheckShakeU3Ec__Iterator0_t3478000993::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016 = { sizeof (ShakeMiniGame_t406515851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6016[1] = 
{
	ShakeMiniGame_t406515851::get_offset_of_shakeController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017 = { sizeof (AttemptTab_t3964708364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6017[2] = 
{
	AttemptTab_t3964708364::get_offset_of_tabSprite_2(),
	AttemptTab_t3964708364::get_offset_of_tabHeightTween_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018 = { sizeof (DefendAttemptController_t232561863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6018[7] = 
{
	DefendAttemptController_t232561863::get_offset_of_maxAttempts_2(),
	DefendAttemptController_t232561863::get_offset_of_attemptFailedColour_3(),
	DefendAttemptController_t232561863::get_offset_of_attemptSuccessColour_4(),
	DefendAttemptController_t232561863::get_offset_of_tabs_5(),
	DefendAttemptController_t232561863::get_offset_of_tabDistance_6(),
	DefendAttemptController_t232561863::get_offset_of_attemptCount_7(),
	DefendAttemptController_t232561863::get_offset_of_miniGameResourcePath_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019 = { sizeof (PlayState_t1063998429)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6019[4] = 
{
	PlayState_t1063998429::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020 = { sizeof (RotatingTab_t2077750253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6020[6] = 
{
	RotatingTab_t2077750253::get_offset_of_controller_2(),
	RotatingTab_t2077750253::get_offset_of_tabSprite_3(),
	RotatingTab_t2077750253::get_offset_of_rotationAnchor_4(),
	RotatingTab_t2077750253::get_offset_of_speed_5(),
	RotatingTab_t2077750253::get_offset_of_currentState_6(),
	RotatingTab_t2077750253::get_offset_of_rotationRoutine_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021 = { sizeof (U3CRotateTabU3Ec__Iterator0_t1022532947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6021[5] = 
{
	U3CRotateTabU3Ec__Iterator0_t1022532947::get_offset_of_U3CeulerU3E__0_0(),
	U3CRotateTabU3Ec__Iterator0_t1022532947::get_offset_of_U24this_1(),
	U3CRotateTabU3Ec__Iterator0_t1022532947::get_offset_of_U24current_2(),
	U3CRotateTabU3Ec__Iterator0_t1022532947::get_offset_of_U24disposing_3(),
	U3CRotateTabU3Ec__Iterator0_t1022532947::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022 = { sizeof (RotatingTabController_t2617193705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6022[7] = 
{
	RotatingTabController_t2617193705::get_offset_of_leftTab_2(),
	RotatingTabController_t2617193705::get_offset_of_rightTab_3(),
	RotatingTabController_t2617193705::get_offset_of_leftTabRPS_4(),
	RotatingTabController_t2617193705::get_offset_of_rightTabRPS_5(),
	RotatingTabController_t2617193705::get_offset_of_tabCollidingColour_6(),
	RotatingTabController_t2617193705::get_offset_of_tabNotCollidingColour_7(),
	RotatingTabController_t2617193705::get_offset_of_tabsColliding_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023 = { sizeof (StandardDefendButton_t3215661011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6023[3] = 
{
	StandardDefendButton_t3215661011::get_offset_of_miniGame_5(),
	StandardDefendButton_t3215661011::get_offset_of_waitTime_6(),
	StandardDefendButton_t3215661011::get_offset_of_waitRoutine_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024 = { sizeof (U3CWaitToActivateButtonU3Ec__Iterator0_t760904360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6024[4] = 
{
	U3CWaitToActivateButtonU3Ec__Iterator0_t760904360::get_offset_of_U24this_0(),
	U3CWaitToActivateButtonU3Ec__Iterator0_t760904360::get_offset_of_U24current_1(),
	U3CWaitToActivateButtonU3Ec__Iterator0_t760904360::get_offset_of_U24disposing_2(),
	U3CWaitToActivateButtonU3Ec__Iterator0_t760904360::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025 = { sizeof (StandardMiniGame_t3672006024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6025[6] = 
{
	StandardMiniGame_t3672006024::get_offset_of_modelParent_6(),
	StandardMiniGame_t3672006024::get_offset_of_currentModel_7(),
	StandardMiniGame_t3672006024::get_offset_of_itemLight_8(),
	StandardMiniGame_t3672006024::get_offset_of_tabController_9(),
	StandardMiniGame_t3672006024::get_offset_of_countdown_10(),
	StandardMiniGame_t3672006024::get_offset_of_defendSuccess_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026 = { sizeof (MiniGameController_t3067676539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6026[2] = 
{
	MiniGameController_t3067676539::get_offset_of_paused_2(),
	MiniGameController_t3067676539::get_offset_of_stopped_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027 = { sizeof (CloseLaunchSequenceButton_t1852628538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6027[1] = 
{
	CloseLaunchSequenceButton_t1852628538::get_offset_of_routine_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028 = { sizeof (TimeOfDay_t3632731134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6028[4] = 
{
	TimeOfDay_t3632731134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029 = { sizeof (LaunchAnimationController_t825305345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6029[16] = 
{
	LaunchAnimationController_t825305345::get_offset_of_animationObject_2(),
	LaunchAnimationController_t825305345::get_offset_of_standardMissile_3(),
	LaunchAnimationController_t825305345::get_offset_of_stealthMissile_4(),
	LaunchAnimationController_t825305345::get_offset_of_shakerMissile_5(),
	LaunchAnimationController_t825305345::get_offset_of_screamerMissile_6(),
	LaunchAnimationController_t825305345::get_offset_of_heatSeekerMissile_7(),
	LaunchAnimationController_t825305345::get_offset_of_dayFogColour_8(),
	LaunchAnimationController_t825305345::get_offset_of_duskFogColour_9(),
	LaunchAnimationController_t825305345::get_offset_of_nightFogColour_10(),
	LaunchAnimationController_t825305345::get_offset_of_dayLight_11(),
	LaunchAnimationController_t825305345::get_offset_of_duskLight_12(),
	LaunchAnimationController_t825305345::get_offset_of_nightLight_13(),
	LaunchAnimationController_t825305345::get_offset_of_daySky_14(),
	LaunchAnimationController_t825305345::get_offset_of_duskSky_15(),
	LaunchAnimationController_t825305345::get_offset_of_nightSky_16(),
	LaunchAnimationController_t825305345::get_offset_of_cameraGlitch_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030 = { sizeof (LaunchCountdownTimer_t1840221825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6030[6] = 
{
	LaunchCountdownTimer_t1840221825::get_offset_of_countdownTime_2(),
	LaunchCountdownTimer_t1840221825::get_offset_of_timeLabel_3(),
	LaunchCountdownTimer_t1840221825::get_offset_of_calcRoutine_4(),
	LaunchCountdownTimer_t1840221825::get_offset_of_countdownPlayed_5(),
	LaunchCountdownTimer_t1840221825::get_offset_of_launchPlayed_6(),
	LaunchCountdownTimer_t1840221825::get_offset_of_onZeroHit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031 = { sizeof (OnZeroHit_t1439474620), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032 = { sizeof (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6032[6] = 
{
	U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361::get_offset_of_U3CincreasedTimeU3E__0_0(),
	U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361::get_offset_of_U3CdifferenceU3E__1_1(),
	U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361::get_offset_of_U24this_2(),
	U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361::get_offset_of_U24current_3(),
	U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361::get_offset_of_U24disposing_4(),
	U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033 = { sizeof (Alphabet_t1535192461)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6033[27] = 
{
	Alphabet_t1535192461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034 = { sizeof (LaunchRoutine_t3791963153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6034[25] = 
{
	LaunchRoutine_t3791963153::get_offset_of_stateController_2(),
	LaunchRoutine_t3791963153::get_offset_of_countdownTimer_3(),
	LaunchRoutine_t3791963153::get_offset_of_coutdownTween_4(),
	LaunchRoutine_t3791963153::get_offset_of_sequenceReport_5(),
	LaunchRoutine_t3791963153::get_offset_of_screenInfoController_6(),
	LaunchRoutine_t3791963153::get_offset_of_overlayTweens_7(),
	LaunchRoutine_t3791963153::get_offset_of_successInfoController_8(),
	LaunchRoutine_t3791963153::get_offset_of_backfireLabel_9(),
	LaunchRoutine_t3791963153::get_offset_of_backfireTween_10(),
	LaunchRoutine_t3791963153::get_offset_of_finishSequenceReport_11(),
	LaunchRoutine_t3791963153::get_offset_of_finishSequenceTween_12(),
	LaunchRoutine_t3791963153::get_offset_of_successLabelTween_13(),
	LaunchRoutine_t3791963153::get_offset_of_animRenderTween_14(),
	LaunchRoutine_t3791963153::get_offset_of_whiteoutAlphaTween_15(),
	LaunchRoutine_t3791963153::get_offset_of_animationController_16(),
	LaunchRoutine_t3791963153::get_offset_of_timedGlitch_17(),
	LaunchRoutine_t3791963153::get_offset_of_engage_18(),
	LaunchRoutine_t3791963153::get_offset_of_playingForward_19(),
	LaunchRoutine_t3791963153::get_offset_of_step1_20(),
	LaunchRoutine_t3791963153::get_offset_of_setp1Routine_21(),
	LaunchRoutine_t3791963153::get_offset_of_step2_22(),
	LaunchRoutine_t3791963153::get_offset_of_setp2Routine_23(),
	LaunchRoutine_t3791963153::get_offset_of_closing_24(),
	LaunchRoutine_t3791963153::get_offset_of_endAnimWaitRoutine_25(),
	LaunchRoutine_t3791963153::get_offset_of_sceneTransitionRoutine_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035 = { sizeof (U3CreverseAnimWaitU3Ec__Iterator0_t2216747469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6035[4] = 
{
	U3CreverseAnimWaitU3Ec__Iterator0_t2216747469::get_offset_of_U24this_0(),
	U3CreverseAnimWaitU3Ec__Iterator0_t2216747469::get_offset_of_U24current_1(),
	U3CreverseAnimWaitU3Ec__Iterator0_t2216747469::get_offset_of_U24disposing_2(),
	U3CreverseAnimWaitU3Ec__Iterator0_t2216747469::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036 = { sizeof (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6036[4] = 
{
	U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468::get_offset_of_U24this_0(),
	U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468::get_offset_of_U24current_1(),
	U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468::get_offset_of_U24disposing_2(),
	U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037 = { sizeof (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6037[4] = 
{
	U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826::get_offset_of_U24this_0(),
	U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826::get_offset_of_U24current_1(),
	U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826::get_offset_of_U24disposing_2(),
	U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6038 = { sizeof (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6038[4] = 
{
	U3CWaitForAnimEndU3Ec__Iterator3_t2692169692::get_offset_of_U24this_0(),
	U3CWaitForAnimEndU3Ec__Iterator3_t2692169692::get_offset_of_U24current_1(),
	U3CWaitForAnimEndU3Ec__Iterator3_t2692169692::get_offset_of_U24disposing_2(),
	U3CWaitForAnimEndU3Ec__Iterator3_t2692169692::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6039 = { sizeof (U3CTimedGlitchEffectU3Ec__Iterator4_t1802922878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6039[4] = 
{
	U3CTimedGlitchEffectU3Ec__Iterator4_t1802922878::get_offset_of_U24this_0(),
	U3CTimedGlitchEffectU3Ec__Iterator4_t1802922878::get_offset_of_U24current_1(),
	U3CTimedGlitchEffectU3Ec__Iterator4_t1802922878::get_offset_of_U24disposing_2(),
	U3CTimedGlitchEffectU3Ec__Iterator4_t1802922878::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6040 = { sizeof (U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6040[5] = 
{
	U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373::get_offset_of_waitTime_0(),
	U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373::get_offset_of_U24this_1(),
	U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373::get_offset_of_U24current_2(),
	U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373::get_offset_of_U24disposing_3(),
	U3CWaitToTransitionSceneU3Ec__Iterator5_t4231294373::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6041 = { sizeof (LaunchSequenceReport_t983071960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6041[4] = 
{
	LaunchSequenceReport_t983071960::get_offset_of_sequenceInfoParents_2(),
	LaunchSequenceReport_t983071960::get_offset_of_sequenceInfoLabels_3(),
	LaunchSequenceReport_t983071960::get_offset_of_height_4(),
	LaunchSequenceReport_t983071960::get_offset_of_startPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6042 = { sizeof (LaunchState_t3704349340)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6042[5] = 
{
	LaunchState_t3704349340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6043 = { sizeof (LaunchStateController_t3361989840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6043[3] = 
{
	LaunchStateController_t3361989840::get_offset_of_initateTween_2(),
	LaunchStateController_t3361989840::get_offset_of_engageTween_3(),
	LaunchStateController_t3361989840::get_offset_of_launchTween_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6044 = { sizeof (LauncherSceneManager_t2478891917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6044[2] = 
{
	LauncherSceneManager_t2478891917::get_offset_of_launcherCenterUI_17(),
	LauncherSceneManager_t2478891917::get_offset_of_launchAnimation_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6045 = { sizeof (TVScaleEffect_t2256617735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6046 = { sizeof (LaunchSequenceUIScaler_t2928648964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6046[19] = 
{
	LaunchSequenceUIScaler_t2928648964::get_offset_of_background_14(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_animationRender_15(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_whiteout_16(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_animationOutline_17(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_animationMask_18(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_crossHairs_19(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_labels_20(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_countdownTimers_21(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_statusButton_22(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_statusBackground_23(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_initBulb_24(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_launchBulb_25(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_skipButton_26(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_skipCollider_27(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_skipBackground_28(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_skipLabel_29(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_reportBackground_30(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_reportTitle_31(),
	LaunchSequenceUIScaler_t2928648964::get_offset_of_reportLabels_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6047 = { sizeof (ContactUsUI_t1917862594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6047[3] = 
{
	ContactUsUI_t1917862594::get_offset_of_contactUsText_2(),
	ContactUsUI_t1917862594::get_offset_of_uiCollider_3(),
	ContactUsUI_t1917862594::get_offset_of_sectionSplit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6048 = { sizeof (EULAUI_t3168190395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6048[3] = 
{
	EULAUI_t3168190395::get_offset_of_eulaText_2(),
	EULAUI_t3168190395::get_offset_of_uiCollider_3(),
	EULAUI_t3168190395::get_offset_of_sectionSplit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6049 = { sizeof (InstructionsUI_t253378877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6049[3] = 
{
	InstructionsUI_t253378877::get_offset_of_instructionsText_2(),
	InstructionsUI_t253378877::get_offset_of_uiCollider_3(),
	InstructionsUI_t253378877::get_offset_of_sectionSplit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6050 = { sizeof (PrivacyPolicyUI_t524063172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6050[3] = 
{
	PrivacyPolicyUI_t524063172::get_offset_of_privacyPolicyText_2(),
	PrivacyPolicyUI_t524063172::get_offset_of_uiCollider_3(),
	PrivacyPolicyUI_t524063172::get_offset_of_sectionSplit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6051 = { sizeof (QuestionUI_t3721436426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6051[4] = 
{
	QuestionUI_t3721436426::get_offset_of_questionText_2(),
	QuestionUI_t3721436426::get_offset_of_anwserText_3(),
	QuestionUI_t3721436426::get_offset_of_uiCollider_4(),
	QuestionUI_t3721436426::get_offset_of_sectionSplit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6052 = { sizeof (TermsOfServiceUI_t3223215579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6052[3] = 
{
	TermsOfServiceUI_t3223215579::get_offset_of_tosText_2(),
	TermsOfServiceUI_t3223215579::get_offset_of_uiCollider_3(),
	TermsOfServiceUI_t3223215579::get_offset_of_sectionSplit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6053 = { sizeof (AboutAndHelpUI_t3809615169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6053[9] = 
{
	AboutAndHelpUI_t3809615169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6054 = { sizeof (AboutAndHelpNavScreen_t3086328594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6054[1] = 
{
	AboutAndHelpNavScreen_t3086328594::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6055 = { sizeof (AudioNavScreen_t3651651169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6055[3] = 
{
	AudioNavScreen_t3651651169::get_offset_of_musicSwitch_3(),
	AudioNavScreen_t3651651169::get_offset_of_sfxSwitch_4(),
	AudioNavScreen_t3651651169::get_offset_of_UIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6056 = { sizeof (BlockILP_t879176280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6056[3] = 
{
	BlockILP_t879176280::get_offset_of_maxNameLength_33(),
	BlockILP_t879176280::get_offset_of_listLoaded_34(),
	BlockILP_t879176280::get_offset_of_blockedFriendsArray_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6057 = { sizeof (BlockListItem_t1514504046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6057[14] = 
{
	BlockListItem_t1514504046::get_offset_of_listPopulator_10(),
	BlockListItem_t1514504046::get_offset_of_blockedFriend_11(),
	BlockListItem_t1514504046::get_offset_of_nameLabel_12(),
	BlockListItem_t1514504046::get_offset_of_avatar_13(),
	BlockListItem_t1514504046::get_offset_of_playerProfile_14(),
	BlockListItem_t1514504046::get_offset_of_blockSprite_15(),
	BlockListItem_t1514504046::get_offset_of_blockLabel_16(),
	BlockListItem_t1514504046::get_offset_of_rankLabel_17(),
	BlockListItem_t1514504046::get_offset_of_scrollView_18(),
	BlockListItem_t1514504046::get_offset_of_draggablePanel_19(),
	BlockListItem_t1514504046::get_offset_of_mTrans_20(),
	BlockListItem_t1514504046::get_offset_of_mScroll_21(),
	BlockListItem_t1514504046::get_offset_of_mAutoFind_22(),
	BlockListItem_t1514504046::get_offset_of_mStarted_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6058 = { sizeof (BlockListNavScreen_t522977310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6058[5] = 
{
	BlockListNavScreen_t522977310::get_offset_of_tableTween_3(),
	BlockListNavScreen_t522977310::get_offset_of_blockList_4(),
	BlockListNavScreen_t522977310::get_offset_of_UIclosing_5(),
	BlockListNavScreen_t522977310::get_offset_of_settingsUIclosing_6(),
	BlockListNavScreen_t522977310::get_offset_of_loadingChat_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6059 = { sizeof (ContactUsScreenNav_t3711221493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6059[1] = 
{
	ContactUsScreenNav_t3711221493::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6060 = { sizeof (DisplayLocationSwitch_t413816511), -1, sizeof(DisplayLocationSwitch_t413816511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6060[1] = 
{
	DisplayLocationSwitch_t413816511_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6061 = { sizeof (EditEmailNavScreen_t3910863955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6061[2] = 
{
	EditEmailNavScreen_t3910863955::get_offset_of_emailInput_3(),
	EditEmailNavScreen_t3910863955::get_offset_of_UIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6062 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t2708455943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6062[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t2708455943::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t2708455943::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t2708455943::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t2708455943::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t2708455943::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t2708455943::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6063 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t930415982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6063[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t930415982::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t930415982::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6064 = { sizeof (EditNameNavScreen_t2089604456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6064[3] = 
{
	EditNameNavScreen_t2089604456::get_offset_of_firstNameInput_3(),
	EditNameNavScreen_t2089604456::get_offset_of_lastNameInput_4(),
	EditNameNavScreen_t2089604456::get_offset_of_UIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6065 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t615035256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6065[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t615035256::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t615035256::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t615035256::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t615035256::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t615035256::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t615035256::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6066 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t3471157711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6066[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t3471157711::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t3471157711::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6067 = { sizeof (EditUsernameNav_t3313102785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6067[2] = 
{
	EditUsernameNav_t3313102785::get_offset_of_usernameInput_3(),
	EditUsernameNav_t3313102785::get_offset_of_UIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6068 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t285889113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6068[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t285889113::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t285889113::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t285889113::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t285889113::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t285889113::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t285889113::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6069 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t747550342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6069[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t747550342::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t747550342::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6070 = { sizeof (EulaNavScreen_t1232694410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6070[1] = 
{
	EulaNavScreen_t1232694410::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6071 = { sizeof (FAQ_t2837997648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6071[2] = 
{
	FAQ_t2837997648::get_offset_of_question_0(),
	FAQ_t2837997648::get_offset_of_anwser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6072 = { sizeof (FAQILP_t1146604377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6072[3] = 
{
	FAQILP_t1146604377::get_offset_of_maxNameLength_33(),
	FAQILP_t1146604377::get_offset_of_listLoaded_34(),
	FAQILP_t1146604377::get_offset_of_FAQArray_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6073 = { sizeof (FAQListItem_t1761156371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6073[9] = 
{
	FAQListItem_t1761156371::get_offset_of_listPopulator_10(),
	FAQListItem_t1761156371::get_offset_of_faqItem_11(),
	FAQListItem_t1761156371::get_offset_of_questionLabel_12(),
	FAQListItem_t1761156371::get_offset_of_scrollView_13(),
	FAQListItem_t1761156371::get_offset_of_draggablePanel_14(),
	FAQListItem_t1761156371::get_offset_of_mTrans_15(),
	FAQListItem_t1761156371::get_offset_of_mScroll_16(),
	FAQListItem_t1761156371::get_offset_of_mAutoFind_17(),
	FAQListItem_t1761156371::get_offset_of_mStarted_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6074 = { sizeof (FAQNavScreen_t2266159941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6074[1] = 
{
	FAQNavScreen_t2266159941::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6075 = { sizeof (GenderNavScreen_t2608406338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6075[2] = 
{
	GenderNavScreen_t2608406338::get_offset_of_genderSwitch_3(),
	GenderNavScreen_t2608406338::get_offset_of_UIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6076 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t751163754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6076[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t751163754::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t751163754::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t751163754::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t751163754::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t751163754::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t751163754::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6077 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t2686197043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6077[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t2686197043::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t2686197043::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6078 = { sizeof (GenderSwitch_t696294539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6078[4] = 
{
	GenderSwitch_t696294539::get_offset_of_selectionObject_3(),
	GenderSwitch_t696294539::get_offset_of_selectionObjectPosition_4(),
	GenderSwitch_t696294539::get_offset_of_maleObject_5(),
	GenderSwitch_t696294539::get_offset_of_femaleObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6079 = { sizeof (HomeNavScreen_t3588214316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6079[4] = 
{
	HomeNavScreen_t3588214316::get_offset_of_tableTween_3(),
	HomeNavScreen_t3588214316::get_offset_of_profileList_4(),
	HomeNavScreen_t3588214316::get_offset_of_UIclosing_5(),
	HomeNavScreen_t3588214316::get_offset_of_settingsUIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6080 = { sizeof (IGNotificationSoundSwitch_t1960044298), -1, sizeof(IGNotificationSoundSwitch_t1960044298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6080[1] = 
{
	IGNotificationSoundSwitch_t1960044298_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6081 = { sizeof (IGNotificationVibrationSwitch_t507453717), -1, sizeof(IGNotificationVibrationSwitch_t507453717_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6081[1] = 
{
	IGNotificationVibrationSwitch_t507453717_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6082 = { sizeof (IGNotificationsSwitch_t111074964), -1, sizeof(IGNotificationsSwitch_t111074964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6082[1] = 
{
	IGNotificationsSwitch_t111074964_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6083 = { sizeof (InstructionNavScreen_t2629088027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6083[1] = 
{
	InstructionNavScreen_t2629088027::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6084 = { sizeof (LogOutTransition_t3259858399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6084[8] = 
{
	LogOutTransition_t3259858399::get_offset_of_posTween_2(),
	LogOutTransition_t3259858399::get_offset_of_scaleTween_3(),
	LogOutTransition_t3259858399::get_offset_of_alphaTween_4(),
	LogOutTransition_t3259858399::get_offset_of_backgroundFadeTween_5(),
	LogOutTransition_t3259858399::get_offset_of_alphaTweenFrom_6(),
	LogOutTransition_t3259858399::get_offset_of_alphaTweenTo_7(),
	LogOutTransition_t3259858399::get_offset_of_UIclosing_8(),
	LogOutTransition_t3259858399::get_offset_of_loggingOut_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6085 = { sizeof (MusicSwitch_t1246590877), -1, sizeof(MusicSwitch_t1246590877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6085[1] = 
{
	MusicSwitch_t1246590877_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6086 = { sizeof (NotificationsNavScreen_t3203037389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6086[4] = 
{
	NotificationsNavScreen_t3203037389::get_offset_of_notificationSwitch_3(),
	NotificationsNavScreen_t3203037389::get_offset_of_soundSwitch_4(),
	NotificationsNavScreen_t3203037389::get_offset_of_vibrationSwitch_5(),
	NotificationsNavScreen_t3203037389::get_offset_of_UIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6087 = { sizeof (NumberPickerTransform_t3121159539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6087[1] = 
{
	NumberPickerTransform_t3121159539::get_offset_of_pickerLabel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6088 = { sizeof (OpenEmailButton_t3875630000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6088[4] = 
{
	OpenEmailButton_t3875630000::get_offset_of_toAddress_5(),
	OpenEmailButton_t3875630000::get_offset_of_subject_6(),
	OpenEmailButton_t3875630000::get_offset_of_message_7(),
	OpenEmailButton_t3875630000::get_offset_of_isHTML_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6089 = { sizeof (TimeNumberPicker_t1112809442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6089[4] = 
{
	TimeNumberPicker_t1112809442::get_offset_of_timePicker_2(),
	TimeNumberPicker_t1112809442::get_offset_of_activeColour_3(),
	TimeNumberPicker_t1112809442::get_offset_of_inactiveColour_4(),
	TimeNumberPicker_t1112809442::get_offset_of_setup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6090 = { sizeof (U3CWaitU3Ec__Iterator0_t237771877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6090[5] = 
{
	U3CWaitU3Ec__Iterator0_t237771877::get_offset_of_value_0(),
	U3CWaitU3Ec__Iterator0_t237771877::get_offset_of_U24this_1(),
	U3CWaitU3Ec__Iterator0_t237771877::get_offset_of_U24current_2(),
	U3CWaitU3Ec__Iterator0_t237771877::get_offset_of_U24disposing_3(),
	U3CWaitU3Ec__Iterator0_t237771877::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6091 = { sizeof (PrivacyPolicyNavScreen_t2706854281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6091[1] = 
{
	PrivacyPolicyNavScreen_t2706854281::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6092 = { sizeof (SFXSwitch_t3206288945), -1, sizeof(SFXSwitch_t3206288945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6092[1] = 
{
	SFXSwitch_t3206288945_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6093 = { sizeof (SelectAudienceNavScreen_t4274980323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6093[2] = 
{
	SelectAudienceNavScreen_t4274980323::get_offset_of_selectAudienceSwitch_3(),
	SelectAudienceNavScreen_t4274980323::get_offset_of_UIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6094 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t2360964071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6094[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t2360964071::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t2360964071::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t2360964071::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t2360964071::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t2360964071::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t2360964071::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6095 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t1300706356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6095[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t1300706356::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t1300706356::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6096 = { sizeof (SelectAudienceSwitch_t3539699246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6096[5] = 
{
	SelectAudienceSwitch_t3539699246::get_offset_of_selectionObject_3(),
	SelectAudienceSwitch_t3539699246::get_offset_of_selectionObjectPosition_4(),
	SelectAudienceSwitch_t3539699246::get_offset_of_everyoneObject_5(),
	SelectAudienceSwitch_t3539699246::get_offset_of_contactsObject_6(),
	SelectAudienceSwitch_t3539699246::get_offset_of_nobodyObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6097 = { sizeof (SelectAudienceSwitchButton_t3845187086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6097[2] = 
{
	SelectAudienceSwitchButton_t3845187086::get_offset_of_enumSwitch_5(),
	SelectAudienceSwitchButton_t3845187086::get_offset_of_switchSelection_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6098 = { sizeof (SelectGenderSwitchButton_t1386910845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6098[2] = 
{
	SelectGenderSwitchButton_t1386910845::get_offset_of_enumSwitch_5(),
	SelectGenderSwitchButton_t1386910845::get_offset_of_switchSelection_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6099 = { sizeof (SettingsHomeContent_t249573869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6099[7] = 
{
	SettingsHomeContent_t249573869::get_offset_of_avatar_2(),
	SettingsHomeContent_t249573869::get_offset_of_firstNameLabel_3(),
	SettingsHomeContent_t249573869::get_offset_of_lastNameLabel_4(),
	SettingsHomeContent_t249573869::get_offset_of_userNameLabel_5(),
	SettingsHomeContent_t249573869::get_offset_of_SMStatusLabel_6(),
	SettingsHomeContent_t249573869::get_offset_of_SMTimeLabel_7(),
	SettingsHomeContent_t249573869::get_offset_of_SMDurationLabel_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
