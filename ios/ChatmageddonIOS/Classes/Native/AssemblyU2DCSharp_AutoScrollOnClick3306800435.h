﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPCycler
struct IPCycler_t1336138445;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoScrollOnClick
struct  AutoScrollOnClick_t3306800435  : public MonoBehaviour_t1158329972
{
public:
	// IPCycler AutoScrollOnClick::cycler
	IPCycler_t1336138445 * ___cycler_2;

public:
	inline static int32_t get_offset_of_cycler_2() { return static_cast<int32_t>(offsetof(AutoScrollOnClick_t3306800435, ___cycler_2)); }
	inline IPCycler_t1336138445 * get_cycler_2() const { return ___cycler_2; }
	inline IPCycler_t1336138445 ** get_address_of_cycler_2() { return &___cycler_2; }
	inline void set_cycler_2(IPCycler_t1336138445 * value)
	{
		___cycler_2 = value;
		Il2CppCodeGenWriteBarrier(&___cycler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
