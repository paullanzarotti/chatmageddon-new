﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t1194435593;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// PurchasableItem[]
struct PurchasableItemU5BU5D_t138351562;
// UnityEngine.Font
struct Font_t4239498691;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3476048858.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnibillManager
struct  UnibillManager_t3725383138  : public MonoSingleton_1_t3476048858
{
public:
	// UnityEngine.GUIContent[] UnibillManager::comboBoxList
	GUIContentU5BU5D_t1194435593* ___comboBoxList_3;
	// UnityEngine.GUIStyle UnibillManager::listStyle
	GUIStyle_t1799908754 * ___listStyle_4;
	// System.Int32 UnibillManager::selectedItemIndex
	int32_t ___selectedItemIndex_5;
	// PurchasableItem[] UnibillManager::consumableItems
	PurchasableItemU5BU5D_t138351562* ___consumableItems_6;
	// PurchasableItem[] UnibillManager::nonConsumableItems
	PurchasableItemU5BU5D_t138351562* ___nonConsumableItems_7;
	// PurchasableItem[] UnibillManager::subscriptionItems
	PurchasableItemU5BU5D_t138351562* ___subscriptionItems_8;
	// UnityEngine.Font UnibillManager::font
	Font_t4239498691 * ___font_9;
	// System.Boolean UnibillManager::unibillInitialised
	bool ___unibillInitialised_10;

public:
	inline static int32_t get_offset_of_comboBoxList_3() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___comboBoxList_3)); }
	inline GUIContentU5BU5D_t1194435593* get_comboBoxList_3() const { return ___comboBoxList_3; }
	inline GUIContentU5BU5D_t1194435593** get_address_of_comboBoxList_3() { return &___comboBoxList_3; }
	inline void set_comboBoxList_3(GUIContentU5BU5D_t1194435593* value)
	{
		___comboBoxList_3 = value;
		Il2CppCodeGenWriteBarrier(&___comboBoxList_3, value);
	}

	inline static int32_t get_offset_of_listStyle_4() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___listStyle_4)); }
	inline GUIStyle_t1799908754 * get_listStyle_4() const { return ___listStyle_4; }
	inline GUIStyle_t1799908754 ** get_address_of_listStyle_4() { return &___listStyle_4; }
	inline void set_listStyle_4(GUIStyle_t1799908754 * value)
	{
		___listStyle_4 = value;
		Il2CppCodeGenWriteBarrier(&___listStyle_4, value);
	}

	inline static int32_t get_offset_of_selectedItemIndex_5() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___selectedItemIndex_5)); }
	inline int32_t get_selectedItemIndex_5() const { return ___selectedItemIndex_5; }
	inline int32_t* get_address_of_selectedItemIndex_5() { return &___selectedItemIndex_5; }
	inline void set_selectedItemIndex_5(int32_t value)
	{
		___selectedItemIndex_5 = value;
	}

	inline static int32_t get_offset_of_consumableItems_6() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___consumableItems_6)); }
	inline PurchasableItemU5BU5D_t138351562* get_consumableItems_6() const { return ___consumableItems_6; }
	inline PurchasableItemU5BU5D_t138351562** get_address_of_consumableItems_6() { return &___consumableItems_6; }
	inline void set_consumableItems_6(PurchasableItemU5BU5D_t138351562* value)
	{
		___consumableItems_6 = value;
		Il2CppCodeGenWriteBarrier(&___consumableItems_6, value);
	}

	inline static int32_t get_offset_of_nonConsumableItems_7() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___nonConsumableItems_7)); }
	inline PurchasableItemU5BU5D_t138351562* get_nonConsumableItems_7() const { return ___nonConsumableItems_7; }
	inline PurchasableItemU5BU5D_t138351562** get_address_of_nonConsumableItems_7() { return &___nonConsumableItems_7; }
	inline void set_nonConsumableItems_7(PurchasableItemU5BU5D_t138351562* value)
	{
		___nonConsumableItems_7 = value;
		Il2CppCodeGenWriteBarrier(&___nonConsumableItems_7, value);
	}

	inline static int32_t get_offset_of_subscriptionItems_8() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___subscriptionItems_8)); }
	inline PurchasableItemU5BU5D_t138351562* get_subscriptionItems_8() const { return ___subscriptionItems_8; }
	inline PurchasableItemU5BU5D_t138351562** get_address_of_subscriptionItems_8() { return &___subscriptionItems_8; }
	inline void set_subscriptionItems_8(PurchasableItemU5BU5D_t138351562* value)
	{
		___subscriptionItems_8 = value;
		Il2CppCodeGenWriteBarrier(&___subscriptionItems_8, value);
	}

	inline static int32_t get_offset_of_font_9() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___font_9)); }
	inline Font_t4239498691 * get_font_9() const { return ___font_9; }
	inline Font_t4239498691 ** get_address_of_font_9() { return &___font_9; }
	inline void set_font_9(Font_t4239498691 * value)
	{
		___font_9 = value;
		Il2CppCodeGenWriteBarrier(&___font_9, value);
	}

	inline static int32_t get_offset_of_unibillInitialised_10() { return static_cast<int32_t>(offsetof(UnibillManager_t3725383138, ___unibillInitialised_10)); }
	inline bool get_unibillInitialised_10() const { return ___unibillInitialised_10; }
	inline bool* get_address_of_unibillInitialised_10() { return &___unibillInitialised_10; }
	inline void set_unibillInitialised_10(bool value)
	{
		___unibillInitialised_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
