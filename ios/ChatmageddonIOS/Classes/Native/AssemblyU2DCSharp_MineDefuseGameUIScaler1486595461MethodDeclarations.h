﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineDefuseGameUIScaler
struct MineDefuseGameUIScaler_t1486595461;

#include "codegen/il2cpp-codegen.h"

// System.Void MineDefuseGameUIScaler::.ctor()
extern "C"  void MineDefuseGameUIScaler__ctor_m2578397412 (MineDefuseGameUIScaler_t1486595461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineDefuseGameUIScaler::GlobalUIScale()
extern "C"  void MineDefuseGameUIScaler_GlobalUIScale_m3265681167 (MineDefuseGameUIScaler_t1486595461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
