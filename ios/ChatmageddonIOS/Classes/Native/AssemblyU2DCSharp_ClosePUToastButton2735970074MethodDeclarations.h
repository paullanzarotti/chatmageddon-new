﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClosePUToastButton
struct ClosePUToastButton_t2735970074;

#include "codegen/il2cpp-codegen.h"

// System.Void ClosePUToastButton::.ctor()
extern "C"  void ClosePUToastButton__ctor_m2500197295 (ClosePUToastButton_t2735970074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClosePUToastButton::OnButtonClick()
extern "C"  void ClosePUToastButton_OnButtonClick_m600738450 (ClosePUToastButton_t2735970074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
