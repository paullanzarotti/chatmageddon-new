﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t406167000;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.Collections.Generic.Dictionary`2<System.String,PhoneNumbers.PhoneMetadata>
struct Dictionary_2_t2281640665;
// System.Collections.Generic.Dictionary`2<System.Int32,PhoneNumbers.PhoneMetadata>
struct Dictionary_2_t3669654334;
// PhoneNumbers.RegexCache
struct RegexCache_t1271678307;
// PhoneNumbers.PhoneNumberMatcher/CheckGroups
struct CheckGroups_t2785688338;

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberUtil
struct  PhoneNumberUtil_t4155573397  : public Il2CppObject
{
public:
	// System.String PhoneNumbers.PhoneNumberUtil::currentFilePrefix_
	String_t* ___currentFilePrefix__7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>> PhoneNumbers.PhoneNumberUtil::countryCallingCodeToRegionCodeMap_
	Dictionary_2_t406167000 * ___countryCallingCodeToRegionCodeMap__8;
	// System.Collections.Generic.HashSet`1<System.String> PhoneNumbers.PhoneNumberUtil::supportedRegions_
	HashSet_1_t362681087 * ___supportedRegions__9;
	// System.Collections.Generic.HashSet`1<System.String> PhoneNumbers.PhoneNumberUtil::nanpaRegions_
	HashSet_1_t362681087 * ___nanpaRegions__10;
	// System.Collections.Generic.Dictionary`2<System.String,PhoneNumbers.PhoneMetadata> PhoneNumbers.PhoneNumberUtil::regionToMetadataMap
	Dictionary_2_t2281640665 * ___regionToMetadataMap_50;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhoneNumbers.PhoneMetadata> PhoneNumbers.PhoneNumberUtil::countryCodeToNonGeographicalMetadataMap
	Dictionary_2_t3669654334 * ___countryCodeToNonGeographicalMetadataMap_51;
	// PhoneNumbers.RegexCache PhoneNumbers.PhoneNumberUtil::regexCache
	RegexCache_t1271678307 * ___regexCache_52;

public:
	inline static int32_t get_offset_of_currentFilePrefix__7() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___currentFilePrefix__7)); }
	inline String_t* get_currentFilePrefix__7() const { return ___currentFilePrefix__7; }
	inline String_t** get_address_of_currentFilePrefix__7() { return &___currentFilePrefix__7; }
	inline void set_currentFilePrefix__7(String_t* value)
	{
		___currentFilePrefix__7 = value;
		Il2CppCodeGenWriteBarrier(&___currentFilePrefix__7, value);
	}

	inline static int32_t get_offset_of_countryCallingCodeToRegionCodeMap__8() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___countryCallingCodeToRegionCodeMap__8)); }
	inline Dictionary_2_t406167000 * get_countryCallingCodeToRegionCodeMap__8() const { return ___countryCallingCodeToRegionCodeMap__8; }
	inline Dictionary_2_t406167000 ** get_address_of_countryCallingCodeToRegionCodeMap__8() { return &___countryCallingCodeToRegionCodeMap__8; }
	inline void set_countryCallingCodeToRegionCodeMap__8(Dictionary_2_t406167000 * value)
	{
		___countryCallingCodeToRegionCodeMap__8 = value;
		Il2CppCodeGenWriteBarrier(&___countryCallingCodeToRegionCodeMap__8, value);
	}

	inline static int32_t get_offset_of_supportedRegions__9() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___supportedRegions__9)); }
	inline HashSet_1_t362681087 * get_supportedRegions__9() const { return ___supportedRegions__9; }
	inline HashSet_1_t362681087 ** get_address_of_supportedRegions__9() { return &___supportedRegions__9; }
	inline void set_supportedRegions__9(HashSet_1_t362681087 * value)
	{
		___supportedRegions__9 = value;
		Il2CppCodeGenWriteBarrier(&___supportedRegions__9, value);
	}

	inline static int32_t get_offset_of_nanpaRegions__10() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___nanpaRegions__10)); }
	inline HashSet_1_t362681087 * get_nanpaRegions__10() const { return ___nanpaRegions__10; }
	inline HashSet_1_t362681087 ** get_address_of_nanpaRegions__10() { return &___nanpaRegions__10; }
	inline void set_nanpaRegions__10(HashSet_1_t362681087 * value)
	{
		___nanpaRegions__10 = value;
		Il2CppCodeGenWriteBarrier(&___nanpaRegions__10, value);
	}

	inline static int32_t get_offset_of_regionToMetadataMap_50() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___regionToMetadataMap_50)); }
	inline Dictionary_2_t2281640665 * get_regionToMetadataMap_50() const { return ___regionToMetadataMap_50; }
	inline Dictionary_2_t2281640665 ** get_address_of_regionToMetadataMap_50() { return &___regionToMetadataMap_50; }
	inline void set_regionToMetadataMap_50(Dictionary_2_t2281640665 * value)
	{
		___regionToMetadataMap_50 = value;
		Il2CppCodeGenWriteBarrier(&___regionToMetadataMap_50, value);
	}

	inline static int32_t get_offset_of_countryCodeToNonGeographicalMetadataMap_51() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___countryCodeToNonGeographicalMetadataMap_51)); }
	inline Dictionary_2_t3669654334 * get_countryCodeToNonGeographicalMetadataMap_51() const { return ___countryCodeToNonGeographicalMetadataMap_51; }
	inline Dictionary_2_t3669654334 ** get_address_of_countryCodeToNonGeographicalMetadataMap_51() { return &___countryCodeToNonGeographicalMetadataMap_51; }
	inline void set_countryCodeToNonGeographicalMetadataMap_51(Dictionary_2_t3669654334 * value)
	{
		___countryCodeToNonGeographicalMetadataMap_51 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeToNonGeographicalMetadataMap_51, value);
	}

	inline static int32_t get_offset_of_regexCache_52() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397, ___regexCache_52)); }
	inline RegexCache_t1271678307 * get_regexCache_52() const { return ___regexCache_52; }
	inline RegexCache_t1271678307 ** get_address_of_regexCache_52() { return &___regexCache_52; }
	inline void set_regexCache_52(RegexCache_t1271678307 * value)
	{
		___regexCache_52 = value;
		Il2CppCodeGenWriteBarrier(&___regexCache_52, value);
	}
};

struct PhoneNumberUtil_t4155573397_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Char,System.Char> PhoneNumbers.PhoneNumberUtil::DIALLABLE_CHAR_MAPPINGS
	Dictionary_2_t759991331 * ___DIALLABLE_CHAR_MAPPINGS_19;
	// System.Collections.Generic.Dictionary`2<System.Char,System.Char> PhoneNumbers.PhoneNumberUtil::ALPHA_MAPPINGS
	Dictionary_2_t759991331 * ___ALPHA_MAPPINGS_20;
	// System.Collections.Generic.Dictionary`2<System.Char,System.Char> PhoneNumbers.PhoneNumberUtil::ALPHA_PHONE_MAPPINGS
	Dictionary_2_t759991331 * ___ALPHA_PHONE_MAPPINGS_21;
	// System.Collections.Generic.Dictionary`2<System.Char,System.Char> PhoneNumbers.PhoneNumberUtil::ALL_PLUS_NUMBER_GROUPING_SYMBOLS
	Dictionary_2_t759991331 * ___ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22;
	// System.Object PhoneNumbers.PhoneNumberUtil::thisLock
	Il2CppObject * ___thisLock_23;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberUtil::UNIQUE_INTERNATIONAL_PREFIX
	PhoneRegex_t3216508019 * ___UNIQUE_INTERNATIONAL_PREFIX_24;
	// System.String PhoneNumbers.PhoneNumberUtil::VALID_ALPHA
	String_t* ___VALID_ALPHA_27;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberUtil::PLUS_CHARS_PATTERN
	PhoneRegex_t3216508019 * ___PLUS_CHARS_PATTERN_29;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberUtil::SEPARATOR_PATTERN
	PhoneRegex_t3216508019 * ___SEPARATOR_PATTERN_30;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::CAPTURING_DIGIT_PATTERN
	Regex_t1803876613 * ___CAPTURING_DIGIT_PATTERN_31;
	// System.String PhoneNumbers.PhoneNumberUtil::VALID_START_CHAR
	String_t* ___VALID_START_CHAR_32;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberUtil::VALID_START_CHAR_PATTERN
	PhoneRegex_t3216508019 * ___VALID_START_CHAR_PATTERN_33;
	// System.String PhoneNumbers.PhoneNumberUtil::SECOND_NUMBER_START
	String_t* ___SECOND_NUMBER_START_34;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::SECOND_NUMBER_START_PATTERN
	Regex_t1803876613 * ___SECOND_NUMBER_START_PATTERN_35;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberUtil::VALID_ALPHA_PHONE_PATTERN
	PhoneRegex_t3216508019 * ___VALID_ALPHA_PHONE_PATTERN_36;
	// System.String PhoneNumbers.PhoneNumberUtil::VALID_PHONE_NUMBER
	String_t* ___VALID_PHONE_NUMBER_37;
	// System.String PhoneNumbers.PhoneNumberUtil::DEFAULT_EXTN_PREFIX
	String_t* ___DEFAULT_EXTN_PREFIX_38;
	// System.String PhoneNumbers.PhoneNumberUtil::CAPTURING_EXTN_DIGITS
	String_t* ___CAPTURING_EXTN_DIGITS_39;
	// System.String PhoneNumbers.PhoneNumberUtil::EXTN_PATTERNS_FOR_PARSING
	String_t* ___EXTN_PATTERNS_FOR_PARSING_40;
	// System.String PhoneNumbers.PhoneNumberUtil::EXTN_PATTERNS_FOR_MATCHING
	String_t* ___EXTN_PATTERNS_FOR_MATCHING_41;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::EXTN_PATTERN
	Regex_t1803876613 * ___EXTN_PATTERN_42;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberUtil::VALID_PHONE_NUMBER_PATTERN
	PhoneRegex_t3216508019 * ___VALID_PHONE_NUMBER_PATTERN_43;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::NON_DIGITS_PATTERN
	Regex_t1803876613 * ___NON_DIGITS_PATTERN_44;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::FIRST_GROUP_PATTERN
	Regex_t1803876613 * ___FIRST_GROUP_PATTERN_45;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::NP_PATTERN
	Regex_t1803876613 * ___NP_PATTERN_46;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::FG_PATTERN
	Regex_t1803876613 * ___FG_PATTERN_47;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberUtil::CC_PATTERN
	Regex_t1803876613 * ___CC_PATTERN_48;
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.PhoneNumberUtil::instance_
	PhoneNumberUtil_t4155573397 * ___instance__49;
	// PhoneNumbers.PhoneNumberMatcher/CheckGroups PhoneNumbers.PhoneNumberUtil::<>f__am$cache0
	CheckGroups_t2785688338 * ___U3CU3Ef__amU24cache0_54;
	// PhoneNumbers.PhoneNumberMatcher/CheckGroups PhoneNumbers.PhoneNumberUtil::<>f__am$cache1
	CheckGroups_t2785688338 * ___U3CU3Ef__amU24cache1_55;

public:
	inline static int32_t get_offset_of_DIALLABLE_CHAR_MAPPINGS_19() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___DIALLABLE_CHAR_MAPPINGS_19)); }
	inline Dictionary_2_t759991331 * get_DIALLABLE_CHAR_MAPPINGS_19() const { return ___DIALLABLE_CHAR_MAPPINGS_19; }
	inline Dictionary_2_t759991331 ** get_address_of_DIALLABLE_CHAR_MAPPINGS_19() { return &___DIALLABLE_CHAR_MAPPINGS_19; }
	inline void set_DIALLABLE_CHAR_MAPPINGS_19(Dictionary_2_t759991331 * value)
	{
		___DIALLABLE_CHAR_MAPPINGS_19 = value;
		Il2CppCodeGenWriteBarrier(&___DIALLABLE_CHAR_MAPPINGS_19, value);
	}

	inline static int32_t get_offset_of_ALPHA_MAPPINGS_20() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___ALPHA_MAPPINGS_20)); }
	inline Dictionary_2_t759991331 * get_ALPHA_MAPPINGS_20() const { return ___ALPHA_MAPPINGS_20; }
	inline Dictionary_2_t759991331 ** get_address_of_ALPHA_MAPPINGS_20() { return &___ALPHA_MAPPINGS_20; }
	inline void set_ALPHA_MAPPINGS_20(Dictionary_2_t759991331 * value)
	{
		___ALPHA_MAPPINGS_20 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHA_MAPPINGS_20, value);
	}

	inline static int32_t get_offset_of_ALPHA_PHONE_MAPPINGS_21() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___ALPHA_PHONE_MAPPINGS_21)); }
	inline Dictionary_2_t759991331 * get_ALPHA_PHONE_MAPPINGS_21() const { return ___ALPHA_PHONE_MAPPINGS_21; }
	inline Dictionary_2_t759991331 ** get_address_of_ALPHA_PHONE_MAPPINGS_21() { return &___ALPHA_PHONE_MAPPINGS_21; }
	inline void set_ALPHA_PHONE_MAPPINGS_21(Dictionary_2_t759991331 * value)
	{
		___ALPHA_PHONE_MAPPINGS_21 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHA_PHONE_MAPPINGS_21, value);
	}

	inline static int32_t get_offset_of_ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22)); }
	inline Dictionary_2_t759991331 * get_ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22() const { return ___ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22; }
	inline Dictionary_2_t759991331 ** get_address_of_ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22() { return &___ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22; }
	inline void set_ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22(Dictionary_2_t759991331 * value)
	{
		___ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22 = value;
		Il2CppCodeGenWriteBarrier(&___ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22, value);
	}

	inline static int32_t get_offset_of_thisLock_23() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___thisLock_23)); }
	inline Il2CppObject * get_thisLock_23() const { return ___thisLock_23; }
	inline Il2CppObject ** get_address_of_thisLock_23() { return &___thisLock_23; }
	inline void set_thisLock_23(Il2CppObject * value)
	{
		___thisLock_23 = value;
		Il2CppCodeGenWriteBarrier(&___thisLock_23, value);
	}

	inline static int32_t get_offset_of_UNIQUE_INTERNATIONAL_PREFIX_24() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___UNIQUE_INTERNATIONAL_PREFIX_24)); }
	inline PhoneRegex_t3216508019 * get_UNIQUE_INTERNATIONAL_PREFIX_24() const { return ___UNIQUE_INTERNATIONAL_PREFIX_24; }
	inline PhoneRegex_t3216508019 ** get_address_of_UNIQUE_INTERNATIONAL_PREFIX_24() { return &___UNIQUE_INTERNATIONAL_PREFIX_24; }
	inline void set_UNIQUE_INTERNATIONAL_PREFIX_24(PhoneRegex_t3216508019 * value)
	{
		___UNIQUE_INTERNATIONAL_PREFIX_24 = value;
		Il2CppCodeGenWriteBarrier(&___UNIQUE_INTERNATIONAL_PREFIX_24, value);
	}

	inline static int32_t get_offset_of_VALID_ALPHA_27() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___VALID_ALPHA_27)); }
	inline String_t* get_VALID_ALPHA_27() const { return ___VALID_ALPHA_27; }
	inline String_t** get_address_of_VALID_ALPHA_27() { return &___VALID_ALPHA_27; }
	inline void set_VALID_ALPHA_27(String_t* value)
	{
		___VALID_ALPHA_27 = value;
		Il2CppCodeGenWriteBarrier(&___VALID_ALPHA_27, value);
	}

	inline static int32_t get_offset_of_PLUS_CHARS_PATTERN_29() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___PLUS_CHARS_PATTERN_29)); }
	inline PhoneRegex_t3216508019 * get_PLUS_CHARS_PATTERN_29() const { return ___PLUS_CHARS_PATTERN_29; }
	inline PhoneRegex_t3216508019 ** get_address_of_PLUS_CHARS_PATTERN_29() { return &___PLUS_CHARS_PATTERN_29; }
	inline void set_PLUS_CHARS_PATTERN_29(PhoneRegex_t3216508019 * value)
	{
		___PLUS_CHARS_PATTERN_29 = value;
		Il2CppCodeGenWriteBarrier(&___PLUS_CHARS_PATTERN_29, value);
	}

	inline static int32_t get_offset_of_SEPARATOR_PATTERN_30() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___SEPARATOR_PATTERN_30)); }
	inline PhoneRegex_t3216508019 * get_SEPARATOR_PATTERN_30() const { return ___SEPARATOR_PATTERN_30; }
	inline PhoneRegex_t3216508019 ** get_address_of_SEPARATOR_PATTERN_30() { return &___SEPARATOR_PATTERN_30; }
	inline void set_SEPARATOR_PATTERN_30(PhoneRegex_t3216508019 * value)
	{
		___SEPARATOR_PATTERN_30 = value;
		Il2CppCodeGenWriteBarrier(&___SEPARATOR_PATTERN_30, value);
	}

	inline static int32_t get_offset_of_CAPTURING_DIGIT_PATTERN_31() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___CAPTURING_DIGIT_PATTERN_31)); }
	inline Regex_t1803876613 * get_CAPTURING_DIGIT_PATTERN_31() const { return ___CAPTURING_DIGIT_PATTERN_31; }
	inline Regex_t1803876613 ** get_address_of_CAPTURING_DIGIT_PATTERN_31() { return &___CAPTURING_DIGIT_PATTERN_31; }
	inline void set_CAPTURING_DIGIT_PATTERN_31(Regex_t1803876613 * value)
	{
		___CAPTURING_DIGIT_PATTERN_31 = value;
		Il2CppCodeGenWriteBarrier(&___CAPTURING_DIGIT_PATTERN_31, value);
	}

	inline static int32_t get_offset_of_VALID_START_CHAR_32() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___VALID_START_CHAR_32)); }
	inline String_t* get_VALID_START_CHAR_32() const { return ___VALID_START_CHAR_32; }
	inline String_t** get_address_of_VALID_START_CHAR_32() { return &___VALID_START_CHAR_32; }
	inline void set_VALID_START_CHAR_32(String_t* value)
	{
		___VALID_START_CHAR_32 = value;
		Il2CppCodeGenWriteBarrier(&___VALID_START_CHAR_32, value);
	}

	inline static int32_t get_offset_of_VALID_START_CHAR_PATTERN_33() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___VALID_START_CHAR_PATTERN_33)); }
	inline PhoneRegex_t3216508019 * get_VALID_START_CHAR_PATTERN_33() const { return ___VALID_START_CHAR_PATTERN_33; }
	inline PhoneRegex_t3216508019 ** get_address_of_VALID_START_CHAR_PATTERN_33() { return &___VALID_START_CHAR_PATTERN_33; }
	inline void set_VALID_START_CHAR_PATTERN_33(PhoneRegex_t3216508019 * value)
	{
		___VALID_START_CHAR_PATTERN_33 = value;
		Il2CppCodeGenWriteBarrier(&___VALID_START_CHAR_PATTERN_33, value);
	}

	inline static int32_t get_offset_of_SECOND_NUMBER_START_34() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___SECOND_NUMBER_START_34)); }
	inline String_t* get_SECOND_NUMBER_START_34() const { return ___SECOND_NUMBER_START_34; }
	inline String_t** get_address_of_SECOND_NUMBER_START_34() { return &___SECOND_NUMBER_START_34; }
	inline void set_SECOND_NUMBER_START_34(String_t* value)
	{
		___SECOND_NUMBER_START_34 = value;
		Il2CppCodeGenWriteBarrier(&___SECOND_NUMBER_START_34, value);
	}

	inline static int32_t get_offset_of_SECOND_NUMBER_START_PATTERN_35() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___SECOND_NUMBER_START_PATTERN_35)); }
	inline Regex_t1803876613 * get_SECOND_NUMBER_START_PATTERN_35() const { return ___SECOND_NUMBER_START_PATTERN_35; }
	inline Regex_t1803876613 ** get_address_of_SECOND_NUMBER_START_PATTERN_35() { return &___SECOND_NUMBER_START_PATTERN_35; }
	inline void set_SECOND_NUMBER_START_PATTERN_35(Regex_t1803876613 * value)
	{
		___SECOND_NUMBER_START_PATTERN_35 = value;
		Il2CppCodeGenWriteBarrier(&___SECOND_NUMBER_START_PATTERN_35, value);
	}

	inline static int32_t get_offset_of_VALID_ALPHA_PHONE_PATTERN_36() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___VALID_ALPHA_PHONE_PATTERN_36)); }
	inline PhoneRegex_t3216508019 * get_VALID_ALPHA_PHONE_PATTERN_36() const { return ___VALID_ALPHA_PHONE_PATTERN_36; }
	inline PhoneRegex_t3216508019 ** get_address_of_VALID_ALPHA_PHONE_PATTERN_36() { return &___VALID_ALPHA_PHONE_PATTERN_36; }
	inline void set_VALID_ALPHA_PHONE_PATTERN_36(PhoneRegex_t3216508019 * value)
	{
		___VALID_ALPHA_PHONE_PATTERN_36 = value;
		Il2CppCodeGenWriteBarrier(&___VALID_ALPHA_PHONE_PATTERN_36, value);
	}

	inline static int32_t get_offset_of_VALID_PHONE_NUMBER_37() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___VALID_PHONE_NUMBER_37)); }
	inline String_t* get_VALID_PHONE_NUMBER_37() const { return ___VALID_PHONE_NUMBER_37; }
	inline String_t** get_address_of_VALID_PHONE_NUMBER_37() { return &___VALID_PHONE_NUMBER_37; }
	inline void set_VALID_PHONE_NUMBER_37(String_t* value)
	{
		___VALID_PHONE_NUMBER_37 = value;
		Il2CppCodeGenWriteBarrier(&___VALID_PHONE_NUMBER_37, value);
	}

	inline static int32_t get_offset_of_DEFAULT_EXTN_PREFIX_38() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___DEFAULT_EXTN_PREFIX_38)); }
	inline String_t* get_DEFAULT_EXTN_PREFIX_38() const { return ___DEFAULT_EXTN_PREFIX_38; }
	inline String_t** get_address_of_DEFAULT_EXTN_PREFIX_38() { return &___DEFAULT_EXTN_PREFIX_38; }
	inline void set_DEFAULT_EXTN_PREFIX_38(String_t* value)
	{
		___DEFAULT_EXTN_PREFIX_38 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_EXTN_PREFIX_38, value);
	}

	inline static int32_t get_offset_of_CAPTURING_EXTN_DIGITS_39() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___CAPTURING_EXTN_DIGITS_39)); }
	inline String_t* get_CAPTURING_EXTN_DIGITS_39() const { return ___CAPTURING_EXTN_DIGITS_39; }
	inline String_t** get_address_of_CAPTURING_EXTN_DIGITS_39() { return &___CAPTURING_EXTN_DIGITS_39; }
	inline void set_CAPTURING_EXTN_DIGITS_39(String_t* value)
	{
		___CAPTURING_EXTN_DIGITS_39 = value;
		Il2CppCodeGenWriteBarrier(&___CAPTURING_EXTN_DIGITS_39, value);
	}

	inline static int32_t get_offset_of_EXTN_PATTERNS_FOR_PARSING_40() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___EXTN_PATTERNS_FOR_PARSING_40)); }
	inline String_t* get_EXTN_PATTERNS_FOR_PARSING_40() const { return ___EXTN_PATTERNS_FOR_PARSING_40; }
	inline String_t** get_address_of_EXTN_PATTERNS_FOR_PARSING_40() { return &___EXTN_PATTERNS_FOR_PARSING_40; }
	inline void set_EXTN_PATTERNS_FOR_PARSING_40(String_t* value)
	{
		___EXTN_PATTERNS_FOR_PARSING_40 = value;
		Il2CppCodeGenWriteBarrier(&___EXTN_PATTERNS_FOR_PARSING_40, value);
	}

	inline static int32_t get_offset_of_EXTN_PATTERNS_FOR_MATCHING_41() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___EXTN_PATTERNS_FOR_MATCHING_41)); }
	inline String_t* get_EXTN_PATTERNS_FOR_MATCHING_41() const { return ___EXTN_PATTERNS_FOR_MATCHING_41; }
	inline String_t** get_address_of_EXTN_PATTERNS_FOR_MATCHING_41() { return &___EXTN_PATTERNS_FOR_MATCHING_41; }
	inline void set_EXTN_PATTERNS_FOR_MATCHING_41(String_t* value)
	{
		___EXTN_PATTERNS_FOR_MATCHING_41 = value;
		Il2CppCodeGenWriteBarrier(&___EXTN_PATTERNS_FOR_MATCHING_41, value);
	}

	inline static int32_t get_offset_of_EXTN_PATTERN_42() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___EXTN_PATTERN_42)); }
	inline Regex_t1803876613 * get_EXTN_PATTERN_42() const { return ___EXTN_PATTERN_42; }
	inline Regex_t1803876613 ** get_address_of_EXTN_PATTERN_42() { return &___EXTN_PATTERN_42; }
	inline void set_EXTN_PATTERN_42(Regex_t1803876613 * value)
	{
		___EXTN_PATTERN_42 = value;
		Il2CppCodeGenWriteBarrier(&___EXTN_PATTERN_42, value);
	}

	inline static int32_t get_offset_of_VALID_PHONE_NUMBER_PATTERN_43() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___VALID_PHONE_NUMBER_PATTERN_43)); }
	inline PhoneRegex_t3216508019 * get_VALID_PHONE_NUMBER_PATTERN_43() const { return ___VALID_PHONE_NUMBER_PATTERN_43; }
	inline PhoneRegex_t3216508019 ** get_address_of_VALID_PHONE_NUMBER_PATTERN_43() { return &___VALID_PHONE_NUMBER_PATTERN_43; }
	inline void set_VALID_PHONE_NUMBER_PATTERN_43(PhoneRegex_t3216508019 * value)
	{
		___VALID_PHONE_NUMBER_PATTERN_43 = value;
		Il2CppCodeGenWriteBarrier(&___VALID_PHONE_NUMBER_PATTERN_43, value);
	}

	inline static int32_t get_offset_of_NON_DIGITS_PATTERN_44() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___NON_DIGITS_PATTERN_44)); }
	inline Regex_t1803876613 * get_NON_DIGITS_PATTERN_44() const { return ___NON_DIGITS_PATTERN_44; }
	inline Regex_t1803876613 ** get_address_of_NON_DIGITS_PATTERN_44() { return &___NON_DIGITS_PATTERN_44; }
	inline void set_NON_DIGITS_PATTERN_44(Regex_t1803876613 * value)
	{
		___NON_DIGITS_PATTERN_44 = value;
		Il2CppCodeGenWriteBarrier(&___NON_DIGITS_PATTERN_44, value);
	}

	inline static int32_t get_offset_of_FIRST_GROUP_PATTERN_45() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___FIRST_GROUP_PATTERN_45)); }
	inline Regex_t1803876613 * get_FIRST_GROUP_PATTERN_45() const { return ___FIRST_GROUP_PATTERN_45; }
	inline Regex_t1803876613 ** get_address_of_FIRST_GROUP_PATTERN_45() { return &___FIRST_GROUP_PATTERN_45; }
	inline void set_FIRST_GROUP_PATTERN_45(Regex_t1803876613 * value)
	{
		___FIRST_GROUP_PATTERN_45 = value;
		Il2CppCodeGenWriteBarrier(&___FIRST_GROUP_PATTERN_45, value);
	}

	inline static int32_t get_offset_of_NP_PATTERN_46() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___NP_PATTERN_46)); }
	inline Regex_t1803876613 * get_NP_PATTERN_46() const { return ___NP_PATTERN_46; }
	inline Regex_t1803876613 ** get_address_of_NP_PATTERN_46() { return &___NP_PATTERN_46; }
	inline void set_NP_PATTERN_46(Regex_t1803876613 * value)
	{
		___NP_PATTERN_46 = value;
		Il2CppCodeGenWriteBarrier(&___NP_PATTERN_46, value);
	}

	inline static int32_t get_offset_of_FG_PATTERN_47() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___FG_PATTERN_47)); }
	inline Regex_t1803876613 * get_FG_PATTERN_47() const { return ___FG_PATTERN_47; }
	inline Regex_t1803876613 ** get_address_of_FG_PATTERN_47() { return &___FG_PATTERN_47; }
	inline void set_FG_PATTERN_47(Regex_t1803876613 * value)
	{
		___FG_PATTERN_47 = value;
		Il2CppCodeGenWriteBarrier(&___FG_PATTERN_47, value);
	}

	inline static int32_t get_offset_of_CC_PATTERN_48() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___CC_PATTERN_48)); }
	inline Regex_t1803876613 * get_CC_PATTERN_48() const { return ___CC_PATTERN_48; }
	inline Regex_t1803876613 ** get_address_of_CC_PATTERN_48() { return &___CC_PATTERN_48; }
	inline void set_CC_PATTERN_48(Regex_t1803876613 * value)
	{
		___CC_PATTERN_48 = value;
		Il2CppCodeGenWriteBarrier(&___CC_PATTERN_48, value);
	}

	inline static int32_t get_offset_of_instance__49() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___instance__49)); }
	inline PhoneNumberUtil_t4155573397 * get_instance__49() const { return ___instance__49; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_instance__49() { return &___instance__49; }
	inline void set_instance__49(PhoneNumberUtil_t4155573397 * value)
	{
		___instance__49 = value;
		Il2CppCodeGenWriteBarrier(&___instance__49, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_54() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___U3CU3Ef__amU24cache0_54)); }
	inline CheckGroups_t2785688338 * get_U3CU3Ef__amU24cache0_54() const { return ___U3CU3Ef__amU24cache0_54; }
	inline CheckGroups_t2785688338 ** get_address_of_U3CU3Ef__amU24cache0_54() { return &___U3CU3Ef__amU24cache0_54; }
	inline void set_U3CU3Ef__amU24cache0_54(CheckGroups_t2785688338 * value)
	{
		___U3CU3Ef__amU24cache0_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_54, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_55() { return static_cast<int32_t>(offsetof(PhoneNumberUtil_t4155573397_StaticFields, ___U3CU3Ef__amU24cache1_55)); }
	inline CheckGroups_t2785688338 * get_U3CU3Ef__amU24cache1_55() const { return ___U3CU3Ef__amU24cache1_55; }
	inline CheckGroups_t2785688338 ** get_address_of_U3CU3Ef__amU24cache1_55() { return &___U3CU3Ef__amU24cache1_55; }
	inline void set_U3CU3Ef__amU24cache1_55(CheckGroups_t2785688338 * value)
	{
		___U3CU3Ef__amU24cache1_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_55, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
