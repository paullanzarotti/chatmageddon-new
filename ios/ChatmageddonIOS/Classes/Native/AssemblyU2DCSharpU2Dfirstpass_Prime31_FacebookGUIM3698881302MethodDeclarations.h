﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookGUIManager
struct FacebookGUIManager_t3698881302;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Prime31.FacebookMeResult
struct FacebookMeResult_t187696501;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookMeRes187696501.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void Prime31.FacebookGUIManager::.ctor()
extern "C"  void FacebookGUIManager__ctor_m1582390542 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::completionHandler(System.String,System.Object)
extern "C"  void FacebookGUIManager_completionHandler_m95847992 (FacebookGUIManager_t3698881302 * __this, String_t* ___error0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::Start()
extern "C"  void FacebookGUIManager_Start_m2774171986 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::OnGUI()
extern "C"  void FacebookGUIManager_OnGUI_m491476042 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::secondColumnButtonsGUI()
extern "C"  void FacebookGUIManager_secondColumnButtonsGUI_m165077666 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::secondColumnAdditionalButtonsGUI()
extern "C"  void FacebookGUIManager_secondColumnAdditionalButtonsGUI_m4175282027 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::.cctor()
extern "C"  void FacebookGUIManager__cctor_m3018517293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<Start>m__0(System.Object)
extern "C"  void FacebookGUIManager_U3CStartU3Em__0_m3803784879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<Start>m__1()
extern "C"  void FacebookGUIManager_U3CStartU3Em__1_m4044010324 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<Start>m__2()
extern "C"  void FacebookGUIManager_U3CStartU3Em__2_m4044010295 (FacebookGUIManager_t3698881302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<OnGUI>m__3(System.Boolean)
extern "C"  void FacebookGUIManager_U3COnGUIU3Em__3_m3616656167 (Il2CppObject * __this /* static, unused */, bool ___isValid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<OnGUI>m__4(System.String,System.Collections.Generic.List`1<System.String>)
extern "C"  void FacebookGUIManager_U3COnGUIU3Em__4_m580041377 (FacebookGUIManager_t3698881302 * __this, String_t* ___error0, List_1_t1398341365 * ___grantedPermissions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<secondColumnButtonsGUI>m__5(System.String,Prime31.FacebookMeResult)
extern "C"  void FacebookGUIManager_U3CsecondColumnButtonsGUIU3Em__5_m2635751928 (FacebookGUIManager_t3698881302 * __this, String_t* ___error0, FacebookMeResult_t187696501 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookGUIManager::<secondColumnAdditionalButtonsGUI>m__6(UnityEngine.Texture2D)
extern "C"  void FacebookGUIManager_U3CsecondColumnAdditionalButtonsGUIU3Em__6_m2473123348 (FacebookGUIManager_t3698881302 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
