﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardManager
struct LeaderboardManager_t1534320874;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardManager::.ctor()
extern "C"  void LeaderboardManager__ctor_m3647673801 (LeaderboardManager_t1534320874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardManager::Start()
extern "C"  void LeaderboardManager_Start_m716905001 (LeaderboardManager_t1534320874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardManager::LoadLeaderboardPage(System.Int32,System.Boolean)
extern "C"  void LeaderboardManager_LoadLeaderboardPage_m3995866831 (LeaderboardManager_t1534320874 * __this, int32_t ___page0, bool ___daily1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
