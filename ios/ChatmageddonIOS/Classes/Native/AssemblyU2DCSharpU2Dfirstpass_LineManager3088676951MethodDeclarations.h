﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LineManager
struct LineManager_t3088676951;
// Vectrosity.VectorLine
struct VectorLine_t3390220087;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void LineManager::.ctor()
extern "C"  void LineManager__ctor_m434530834 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::Awake()
extern "C"  void LineManager_Awake_m3023113841 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::Initialize()
extern "C"  void LineManager_Initialize_m2413523906 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::AddLine(Vectrosity.VectorLine,UnityEngine.Transform,System.Single)
extern "C"  void LineManager_AddLine_m2697344632 (LineManager_t3088676951 * __this, VectorLine_t3390220087 * ___vectorLine0, Transform_t3275118058 * ___thisTransform1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::DisableLine(Vectrosity.VectorLine,System.Single)
extern "C"  void LineManager_DisableLine_m2182974126 (LineManager_t3088676951 * __this, VectorLine_t3390220087 * ___vectorLine0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LineManager::DisableLine(Vectrosity.VectorLine,System.Single,System.Boolean)
extern "C"  Il2CppObject * LineManager_DisableLine_m3711087053 (LineManager_t3088676951 * __this, VectorLine_t3390220087 * ___vectorLine0, float ___time1, bool ___remove2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::LateUpdate()
extern "C"  void LineManager_LateUpdate_m3414976697 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::RemoveLine(System.Int32)
extern "C"  void LineManager_RemoveLine_m900798473 (LineManager_t3088676951 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::RemoveLine(Vectrosity.VectorLine)
extern "C"  void LineManager_RemoveLine_m984576055 (LineManager_t3088676951 * __this, VectorLine_t3390220087 * ___vectorLine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::DisableIfUnused()
extern "C"  void LineManager_DisableIfUnused_m3456357415 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::EnableIfUsed()
extern "C"  void LineManager_EnableIfUsed_m2618637949 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::StartCheckDistance()
extern "C"  void LineManager_StartCheckDistance_m3992721293 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::CheckDistance()
extern "C"  void LineManager_CheckDistance_m2879332057 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::OnDestroy()
extern "C"  void LineManager_OnDestroy_m3016977499 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::OnLevelWasLoaded()
extern "C"  void LineManager_OnLevelWasLoaded_m1537201533 (LineManager_t3088676951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager::.cctor()
extern "C"  void LineManager__cctor_m836770103 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
