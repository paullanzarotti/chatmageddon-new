﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AmazonAppStoreCallbackMonoBehaviour
struct AmazonAppStoreCallbackMonoBehaviour_t1679328205;
// Unibill.Impl.AmazonAppStoreBillingService
struct AmazonAppStoreBillingService_t1988397396;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_AmazonA1988397396.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AmazonAppStoreCallbackMonoBehaviour::.ctor()
extern "C"  void AmazonAppStoreCallbackMonoBehaviour__ctor_m1006439944 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::Start()
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_Start_m1708118484 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::initialise(Unibill.Impl.AmazonAppStoreBillingService)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_initialise_m436477100 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, AmazonAppStoreBillingService_t1988397396 * ___amazon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onSDKAvailable(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onSDKAvailable_m3035015578 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___isSandboxEnvironment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onGetItemDataFailed(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onGetItemDataFailed_m309733161 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onProductListReceived(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onProductListReceived_m1597569899 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___productCSVString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onPurchaseFailed(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onPurchaseFailed_m1385832905 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onPurchaseSucceeded(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onPurchaseSucceeded_m3027799785 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onTransactionsRestored(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onTransactionsRestored_m3937487202 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onPurchaseUpdateFailed(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onPurchaseUpdateFailed_m2597710934 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmazonAppStoreCallbackMonoBehaviour::onPurchaseUpdateSuccess(System.String)
extern "C"  void AmazonAppStoreCallbackMonoBehaviour_onPurchaseUpdateSuccess_m3282582876 (AmazonAppStoreCallbackMonoBehaviour_t1679328205 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
