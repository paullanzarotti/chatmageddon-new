﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<System.String,Prime31.FacebookMeResult>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1911342140(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2393018193 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2967680750_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.String,Prime31.FacebookMeResult>::Invoke(T1,T2)
#define Action_2_Invoke_m59481361(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2393018193 *, String_t*, FacebookMeResult_t187696501 *, const MethodInfo*))Action_2_Invoke_m715425719_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.String,Prime31.FacebookMeResult>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1126896002(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2393018193 *, String_t*, FacebookMeResult_t187696501 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.String,Prime31.FacebookMeResult>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1053113706(__this, ___result0, method) ((  void (*) (Action_2_t2393018193 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
