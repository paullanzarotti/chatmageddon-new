﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vectrosity.Vector3Pair>
struct DefaultComparer_t186041003;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vectrosity.Vector3Pair>::.ctor()
extern "C"  void DefaultComparer__ctor_m915741600_gshared (DefaultComparer_t186041003 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m915741600(__this, method) ((  void (*) (DefaultComparer_t186041003 *, const MethodInfo*))DefaultComparer__ctor_m915741600_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vectrosity.Vector3Pair>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3870221775_gshared (DefaultComparer_t186041003 * __this, Vector3Pair_t2859078138  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3870221775(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t186041003 *, Vector3Pair_t2859078138 , const MethodInfo*))DefaultComparer_GetHashCode_m3870221775_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vectrosity.Vector3Pair>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1661763119_gshared (DefaultComparer_t186041003 * __this, Vector3Pair_t2859078138  ___x0, Vector3Pair_t2859078138  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1661763119(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t186041003 *, Vector3Pair_t2859078138 , Vector3Pair_t2859078138 , const MethodInfo*))DefaultComparer_Equals_m1661763119_gshared)(__this, ___x0, ___y1, method)
