﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform
struct XmlDsigEnvelopedSignatureTransform_t3796416260;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform::.ctor()
extern "C"  void XmlDsigEnvelopedSignatureTransform__ctor_m1866557276 (XmlDsigEnvelopedSignatureTransform_t3796416260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform::.ctor(System.Boolean)
extern "C"  void XmlDsigEnvelopedSignatureTransform__ctor_m2554197983 (XmlDsigEnvelopedSignatureTransform_t3796416260 * __this, bool ___includeComments0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDsigEnvelopedSignatureTransform_GetInnerXml_m234614741 (XmlDsigEnvelopedSignatureTransform_t3796416260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDsigEnvelopedSignatureTransform_LoadInnerXml_m1514005772 (XmlDsigEnvelopedSignatureTransform_t3796416260 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
