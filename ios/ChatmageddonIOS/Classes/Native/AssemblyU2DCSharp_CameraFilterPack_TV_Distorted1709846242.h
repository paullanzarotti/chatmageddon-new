﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_TV_Distorted
struct  CameraFilterPack_TV_Distorted_t1709846242  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_TV_Distorted::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Distorted::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Distorted::Distortion
	float ___Distortion_4;
	// System.Single CameraFilterPack_TV_Distorted::RGB
	float ___RGB_5;
	// UnityEngine.Material CameraFilterPack_TV_Distorted::SCMaterial
	Material_t193706927 * ___SCMaterial_6;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Distorted_t1709846242, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Distorted_t1709846242, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Distortion_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Distorted_t1709846242, ___Distortion_4)); }
	inline float get_Distortion_4() const { return ___Distortion_4; }
	inline float* get_address_of_Distortion_4() { return &___Distortion_4; }
	inline void set_Distortion_4(float value)
	{
		___Distortion_4 = value;
	}

	inline static int32_t get_offset_of_RGB_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Distorted_t1709846242, ___RGB_5)); }
	inline float get_RGB_5() const { return ___RGB_5; }
	inline float* get_address_of_RGB_5() { return &___RGB_5; }
	inline void set_RGB_5(float value)
	{
		___RGB_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Distorted_t1709846242, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}
};

struct CameraFilterPack_TV_Distorted_t1709846242_StaticFields
{
public:
	// System.Single CameraFilterPack_TV_Distorted::ChangeDistortion
	float ___ChangeDistortion_7;

public:
	inline static int32_t get_offset_of_ChangeDistortion_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Distorted_t1709846242_StaticFields, ___ChangeDistortion_7)); }
	inline float get_ChangeDistortion_7() const { return ___ChangeDistortion_7; }
	inline float* get_address_of_ChangeDistortion_7() { return &___ChangeDistortion_7; }
	inline void set_ChangeDistortion_7(float value)
	{
		___ChangeDistortion_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
