﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendSearchNavScreen>
struct DefaultComparer_t345352028;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendSearchNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m3084931217_gshared (DefaultComparer_t345352028 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3084931217(__this, method) ((  void (*) (DefaultComparer_t345352028 *, const MethodInfo*))DefaultComparer__ctor_m3084931217_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendSearchNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m140271508_gshared (DefaultComparer_t345352028 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m140271508(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t345352028 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m140271508_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendSearchNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m793963372_gshared (DefaultComparer_t345352028 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m793963372(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t345352028 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m793963372_gshared)(__this, ___x0, ___y1, method)
