﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.PostParameter
struct PostParameter_t415229629;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.PostParameter::.ctor(System.String,System.String)
extern "C"  void PostParameter__ctor_m3886248801 (PostParameter_t415229629 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.PostParameter::get_name()
extern "C"  String_t* PostParameter_get_name_m3517733696 (PostParameter_t415229629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.PostParameter::set_name(System.String)
extern "C"  void PostParameter_set_name_m2377938553 (PostParameter_t415229629 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.PostParameter::get_value()
extern "C"  String_t* PostParameter_get_value_m1465125310 (PostParameter_t415229629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.PostParameter::set_value(System.String)
extern "C"  void PostParameter_set_value_m2110967389 (PostParameter_t415229629 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
