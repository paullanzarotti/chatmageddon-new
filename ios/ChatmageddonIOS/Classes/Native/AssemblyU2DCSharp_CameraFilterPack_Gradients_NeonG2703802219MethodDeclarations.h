﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_NeonGradient
struct CameraFilterPack_Gradients_NeonGradient_t2703802219;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_NeonGradient::.ctor()
extern "C"  void CameraFilterPack_Gradients_NeonGradient__ctor_m344745638 (CameraFilterPack_Gradients_NeonGradient_t2703802219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_NeonGradient::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_NeonGradient_get_material_m2020362419 (CameraFilterPack_Gradients_NeonGradient_t2703802219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::Start()
extern "C"  void CameraFilterPack_Gradients_NeonGradient_Start_m596459378 (CameraFilterPack_Gradients_NeonGradient_t2703802219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_NeonGradient_OnRenderImage_m3479955322 (CameraFilterPack_Gradients_NeonGradient_t2703802219 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::Update()
extern "C"  void CameraFilterPack_Gradients_NeonGradient_Update_m122684939 (CameraFilterPack_Gradients_NeonGradient_t2703802219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::OnDisable()
extern "C"  void CameraFilterPack_Gradients_NeonGradient_OnDisable_m2175775203 (CameraFilterPack_Gradients_NeonGradient_t2703802219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
