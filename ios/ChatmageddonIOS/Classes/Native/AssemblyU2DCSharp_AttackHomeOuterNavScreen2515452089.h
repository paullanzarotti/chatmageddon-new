﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// AttackILP
struct AttackILP_t2111768551;
// AttackOuterPhoneNumberButton
struct AttackOuterPhoneNumberButton_t2666821364;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackHomeOuterNavScreen
struct  AttackHomeOuterNavScreen_t2515452089  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject AttackHomeOuterNavScreen::hasFriendsUI
	GameObject_t1756533147 * ___hasFriendsUI_3;
	// UnityEngine.GameObject AttackHomeOuterNavScreen::hasNoFriendsUI
	GameObject_t1756533147 * ___hasNoFriendsUI_4;
	// AttackILP AttackHomeOuterNavScreen::outerFriendsList
	AttackILP_t2111768551 * ___outerFriendsList_5;
	// AttackOuterPhoneNumberButton AttackHomeOuterNavScreen::phoneNumButton
	AttackOuterPhoneNumberButton_t2666821364 * ___phoneNumButton_6;
	// System.Boolean AttackHomeOuterNavScreen::UIclosing
	bool ___UIclosing_7;

public:
	inline static int32_t get_offset_of_hasFriendsUI_3() { return static_cast<int32_t>(offsetof(AttackHomeOuterNavScreen_t2515452089, ___hasFriendsUI_3)); }
	inline GameObject_t1756533147 * get_hasFriendsUI_3() const { return ___hasFriendsUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasFriendsUI_3() { return &___hasFriendsUI_3; }
	inline void set_hasFriendsUI_3(GameObject_t1756533147 * value)
	{
		___hasFriendsUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasFriendsUI_3, value);
	}

	inline static int32_t get_offset_of_hasNoFriendsUI_4() { return static_cast<int32_t>(offsetof(AttackHomeOuterNavScreen_t2515452089, ___hasNoFriendsUI_4)); }
	inline GameObject_t1756533147 * get_hasNoFriendsUI_4() const { return ___hasNoFriendsUI_4; }
	inline GameObject_t1756533147 ** get_address_of_hasNoFriendsUI_4() { return &___hasNoFriendsUI_4; }
	inline void set_hasNoFriendsUI_4(GameObject_t1756533147 * value)
	{
		___hasNoFriendsUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___hasNoFriendsUI_4, value);
	}

	inline static int32_t get_offset_of_outerFriendsList_5() { return static_cast<int32_t>(offsetof(AttackHomeOuterNavScreen_t2515452089, ___outerFriendsList_5)); }
	inline AttackILP_t2111768551 * get_outerFriendsList_5() const { return ___outerFriendsList_5; }
	inline AttackILP_t2111768551 ** get_address_of_outerFriendsList_5() { return &___outerFriendsList_5; }
	inline void set_outerFriendsList_5(AttackILP_t2111768551 * value)
	{
		___outerFriendsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___outerFriendsList_5, value);
	}

	inline static int32_t get_offset_of_phoneNumButton_6() { return static_cast<int32_t>(offsetof(AttackHomeOuterNavScreen_t2515452089, ___phoneNumButton_6)); }
	inline AttackOuterPhoneNumberButton_t2666821364 * get_phoneNumButton_6() const { return ___phoneNumButton_6; }
	inline AttackOuterPhoneNumberButton_t2666821364 ** get_address_of_phoneNumButton_6() { return &___phoneNumButton_6; }
	inline void set_phoneNumButton_6(AttackOuterPhoneNumberButton_t2666821364 * value)
	{
		___phoneNumButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumButton_6, value);
	}

	inline static int32_t get_offset_of_UIclosing_7() { return static_cast<int32_t>(offsetof(AttackHomeOuterNavScreen_t2515452089, ___UIclosing_7)); }
	inline bool get_UIclosing_7() const { return ___UIclosing_7; }
	inline bool* get_address_of_UIclosing_7() { return &___UIclosing_7; }
	inline void set_UIclosing_7(bool value)
	{
		___UIclosing_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
