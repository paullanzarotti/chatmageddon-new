﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Toaster
struct Toaster_t2428368812;
// Bread
struct Bread_t457172030;
// ToastUI
struct ToastUI_t270783635;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Bread457172030.h"
#include "AssemblyU2DCSharp_ToastType1578101939.h"

// System.Void Toaster::.ctor()
extern "C"  void Toaster__ctor_m1099918545 (Toaster_t2428368812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toaster::Awake()
extern "C"  void Toaster_Awake_m1143958712 (Toaster_t2428368812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toaster::AddBread()
extern "C"  void Toaster_AddBread_m581413372 (Toaster_t2428368812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toaster::AddBreadToToaster(Bread,System.Int32)
extern "C"  void Toaster_AddBreadToToaster_m156421666 (Toaster_t2428368812 * __this, Bread_t457172030 * ___bread0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ToastUI Toaster::OpenToast(ToastType)
extern "C"  ToastUI_t270783635 * Toaster_OpenToast_m2793024731 (Toaster_t2428368812 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toaster::ActivateBackgroundFade(System.Boolean)
extern "C"  void Toaster_ActivateBackgroundFade_m1189831543 (Toaster_t2428368812 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toaster::OnBackgroundFadeFinished()
extern "C"  void Toaster_OnBackgroundFadeFinished_m1179701306 (Toaster_t2428368812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
