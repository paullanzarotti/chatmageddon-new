﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<GetExistingUser>c__AnonStorey1B<System.Object>
struct U3CGetExistingUserU3Ec__AnonStorey1B_t1874969556;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<GetExistingUser>c__AnonStorey1B<System.Object>::.ctor()
extern "C"  void U3CGetExistingUserU3Ec__AnonStorey1B__ctor_m626779159_gshared (U3CGetExistingUserU3Ec__AnonStorey1B_t1874969556 * __this, const MethodInfo* method);
#define U3CGetExistingUserU3Ec__AnonStorey1B__ctor_m626779159(__this, method) ((  void (*) (U3CGetExistingUserU3Ec__AnonStorey1B_t1874969556 *, const MethodInfo*))U3CGetExistingUserU3Ec__AnonStorey1B__ctor_m626779159_gshared)(__this, method)
// System.Void BaseServer`1/<GetExistingUser>c__AnonStorey1B<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetExistingUserU3Ec__AnonStorey1B_U3CU3Em__0_m2653146804_gshared (U3CGetExistingUserU3Ec__AnonStorey1B_t1874969556 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CGetExistingUserU3Ec__AnonStorey1B_U3CU3Em__0_m2653146804(__this, ___request0, ___response1, method) ((  void (*) (U3CGetExistingUserU3Ec__AnonStorey1B_t1874969556 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CGetExistingUserU3Ec__AnonStorey1B_U3CU3Em__0_m2653146804_gshared)(__this, ___request0, ___response1, method)
