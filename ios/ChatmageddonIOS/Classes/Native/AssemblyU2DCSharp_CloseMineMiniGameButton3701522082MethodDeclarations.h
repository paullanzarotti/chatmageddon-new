﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseMineMiniGameButton
struct CloseMineMiniGameButton_t3701522082;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseMineMiniGameButton::.ctor()
extern "C"  void CloseMineMiniGameButton__ctor_m2521928437 (CloseMineMiniGameButton_t3701522082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseMineMiniGameButton::OnButtonClick()
extern "C"  void CloseMineMiniGameButton_OnButtonClick_m4036808810 (CloseMineMiniGameButton_t3701522082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
