﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldButton
struct ShieldButton_t2669703779;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldButton::.ctor()
extern "C"  void ShieldButton__ctor_m2982413692 (ShieldButton_t2669703779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldButton::OnButtonClick()
extern "C"  void ShieldButton_OnButtonClick_m2375815015 (ShieldButton_t2669703779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
