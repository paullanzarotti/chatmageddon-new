﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Pixel_Pixelisation
struct  CameraFilterPack_Pixel_Pixelisation_t2392528693  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Pixel_Pixelisation::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Pixel_Pixelisation::_Pixelisation
	float ____Pixelisation_3;
	// System.Single CameraFilterPack_Pixel_Pixelisation::_SizeX
	float ____SizeX_4;
	// System.Single CameraFilterPack_Pixel_Pixelisation::_SizeY
	float ____SizeY_5;
	// UnityEngine.Material CameraFilterPack_Pixel_Pixelisation::SCMaterial
	Material_t193706927 * ___SCMaterial_6;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of__Pixelisation_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693, ____Pixelisation_3)); }
	inline float get__Pixelisation_3() const { return ____Pixelisation_3; }
	inline float* get_address_of__Pixelisation_3() { return &____Pixelisation_3; }
	inline void set__Pixelisation_3(float value)
	{
		____Pixelisation_3 = value;
	}

	inline static int32_t get_offset_of__SizeX_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693, ____SizeX_4)); }
	inline float get__SizeX_4() const { return ____SizeX_4; }
	inline float* get_address_of__SizeX_4() { return &____SizeX_4; }
	inline void set__SizeX_4(float value)
	{
		____SizeX_4 = value;
	}

	inline static int32_t get_offset_of__SizeY_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693, ____SizeY_5)); }
	inline float get__SizeY_5() const { return ____SizeY_5; }
	inline float* get_address_of__SizeY_5() { return &____SizeY_5; }
	inline void set__SizeY_5(float value)
	{
		____SizeY_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}
};

struct CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields
{
public:
	// System.Single CameraFilterPack_Pixel_Pixelisation::ChangePixel
	float ___ChangePixel_7;
	// System.Single CameraFilterPack_Pixel_Pixelisation::ChangePixelX
	float ___ChangePixelX_8;
	// System.Single CameraFilterPack_Pixel_Pixelisation::ChangePixelY
	float ___ChangePixelY_9;

public:
	inline static int32_t get_offset_of_ChangePixel_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields, ___ChangePixel_7)); }
	inline float get_ChangePixel_7() const { return ___ChangePixel_7; }
	inline float* get_address_of_ChangePixel_7() { return &___ChangePixel_7; }
	inline void set_ChangePixel_7(float value)
	{
		___ChangePixel_7 = value;
	}

	inline static int32_t get_offset_of_ChangePixelX_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields, ___ChangePixelX_8)); }
	inline float get_ChangePixelX_8() const { return ___ChangePixelX_8; }
	inline float* get_address_of_ChangePixelX_8() { return &___ChangePixelX_8; }
	inline void set_ChangePixelX_8(float value)
	{
		___ChangePixelX_8 = value;
	}

	inline static int32_t get_offset_of_ChangePixelY_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields, ___ChangePixelY_9)); }
	inline float get_ChangePixelY_9() const { return ___ChangePixelY_9; }
	inline float* get_address_of_ChangePixelY_9() { return &___ChangePixelY_9; }
	inline void set_ChangePixelY_9(float value)
	{
		___ChangePixelY_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
