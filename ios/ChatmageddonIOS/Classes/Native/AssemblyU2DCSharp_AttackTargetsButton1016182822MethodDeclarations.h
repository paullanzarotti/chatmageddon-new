﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackTargetsButton
struct AttackTargetsButton_t1016182822;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackTargetsButton::.ctor()
extern "C"  void AttackTargetsButton__ctor_m3918731319 (AttackTargetsButton_t1016182822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackTargetsButton::OnButtonClick()
extern "C"  void AttackTargetsButton_OnButtonClick_m163628014 (AttackTargetsButton_t1016182822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
