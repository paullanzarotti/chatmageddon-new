﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResendVerificationButton
struct ResendVerificationButton_t1304384308;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResendVerificationButton::.ctor()
extern "C"  void ResendVerificationButton__ctor_m608614687 (ResendVerificationButton_t1304384308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResendVerificationButton::OnButtonClick()
extern "C"  void ResendVerificationButton_OnButtonClick_m464579356 (ResendVerificationButton_t1304384308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResendVerificationButton::<OnButtonClick>__BaseCallProxy0()
extern "C"  void ResendVerificationButton_U3COnButtonClickU3E__BaseCallProxy0_m267641611 (ResendVerificationButton_t1304384308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResendVerificationButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ResendVerificationButton_U3COnButtonClickU3Em__0_m3250310200 (ResendVerificationButton_t1304384308 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
