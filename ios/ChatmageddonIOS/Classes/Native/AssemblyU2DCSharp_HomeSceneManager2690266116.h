﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Missile
struct Missile_t813944928;
// Friend
struct Friend_t3555014108;
// Shield
struct Shield_t3327121081;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2069092468.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeSceneManager
struct  HomeSceneManager_t2690266116  : public BaseSceneManager_1_t2069092468
{
public:
	// UnityEngine.GameObject HomeSceneManager::worldMap
	GameObject_t1756533147 * ___worldMap_17;
	// UnityEngine.GameObject HomeSceneManager::homeMap
	GameObject_t1756533147 * ___homeMap_18;
	// UnityEngine.GameObject HomeSceneManager::homeCenterUI
	GameObject_t1756533147 * ___homeCenterUI_19;
	// UnityEngine.GameObject HomeSceneManager::homeDebugUI
	GameObject_t1756533147 * ___homeDebugUI_20;
	// UnityEngine.GameObject HomeSceneManager::homeHeaderUI
	GameObject_t1756533147 * ___homeHeaderUI_21;
	// UnityEngine.GameObject HomeSceneManager::homeTabUI
	GameObject_t1756533147 * ___homeTabUI_22;
	// UnityEngine.GameObject HomeSceneManager::homeLeftUI
	GameObject_t1756533147 * ___homeLeftUI_23;
	// UnityEngine.GameObject HomeSceneManager::homeRightUI
	GameObject_t1756533147 * ___homeRightUI_24;
	// UnityEngine.GameObject HomeSceneManager::homeBottomUI
	GameObject_t1756533147 * ___homeBottomUI_25;
	// UnityEngine.GameObject HomeSceneManager::sceneLoader
	GameObject_t1756533147 * ___sceneLoader_26;
	// System.Boolean HomeSceneManager::friendsLoadedFromKnockout
	bool ___friendsLoadedFromKnockout_27;
	// AttackNavScreen HomeSceneManager::startAttackNav
	int32_t ___startAttackNav_28;
	// AttackHomeNav HomeSceneManager::startAttackHomeNav
	int32_t ___startAttackHomeNav_29;
	// Missile HomeSceneManager::activeMissile
	Missile_t813944928 * ___activeMissile_30;
	// Friend HomeSceneManager::activeTarget
	Friend_t3555014108 * ___activeTarget_31;
	// Missile HomeSceneManager::startInventoryMissile
	Missile_t813944928 * ___startInventoryMissile_32;
	// Shield HomeSceneManager::startInventoryShieldItem
	Shield_t3327121081 * ___startInventoryShieldItem_33;
	// System.Boolean HomeSceneManager::startInventoryShield
	bool ___startInventoryShield_34;
	// System.Boolean HomeSceneManager::startInventoryMine
	bool ___startInventoryMine_35;
	// System.Boolean HomeSceneManager::startInventoryFuel
	bool ___startInventoryFuel_36;

public:
	inline static int32_t get_offset_of_worldMap_17() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___worldMap_17)); }
	inline GameObject_t1756533147 * get_worldMap_17() const { return ___worldMap_17; }
	inline GameObject_t1756533147 ** get_address_of_worldMap_17() { return &___worldMap_17; }
	inline void set_worldMap_17(GameObject_t1756533147 * value)
	{
		___worldMap_17 = value;
		Il2CppCodeGenWriteBarrier(&___worldMap_17, value);
	}

	inline static int32_t get_offset_of_homeMap_18() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeMap_18)); }
	inline GameObject_t1756533147 * get_homeMap_18() const { return ___homeMap_18; }
	inline GameObject_t1756533147 ** get_address_of_homeMap_18() { return &___homeMap_18; }
	inline void set_homeMap_18(GameObject_t1756533147 * value)
	{
		___homeMap_18 = value;
		Il2CppCodeGenWriteBarrier(&___homeMap_18, value);
	}

	inline static int32_t get_offset_of_homeCenterUI_19() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeCenterUI_19)); }
	inline GameObject_t1756533147 * get_homeCenterUI_19() const { return ___homeCenterUI_19; }
	inline GameObject_t1756533147 ** get_address_of_homeCenterUI_19() { return &___homeCenterUI_19; }
	inline void set_homeCenterUI_19(GameObject_t1756533147 * value)
	{
		___homeCenterUI_19 = value;
		Il2CppCodeGenWriteBarrier(&___homeCenterUI_19, value);
	}

	inline static int32_t get_offset_of_homeDebugUI_20() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeDebugUI_20)); }
	inline GameObject_t1756533147 * get_homeDebugUI_20() const { return ___homeDebugUI_20; }
	inline GameObject_t1756533147 ** get_address_of_homeDebugUI_20() { return &___homeDebugUI_20; }
	inline void set_homeDebugUI_20(GameObject_t1756533147 * value)
	{
		___homeDebugUI_20 = value;
		Il2CppCodeGenWriteBarrier(&___homeDebugUI_20, value);
	}

	inline static int32_t get_offset_of_homeHeaderUI_21() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeHeaderUI_21)); }
	inline GameObject_t1756533147 * get_homeHeaderUI_21() const { return ___homeHeaderUI_21; }
	inline GameObject_t1756533147 ** get_address_of_homeHeaderUI_21() { return &___homeHeaderUI_21; }
	inline void set_homeHeaderUI_21(GameObject_t1756533147 * value)
	{
		___homeHeaderUI_21 = value;
		Il2CppCodeGenWriteBarrier(&___homeHeaderUI_21, value);
	}

	inline static int32_t get_offset_of_homeTabUI_22() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeTabUI_22)); }
	inline GameObject_t1756533147 * get_homeTabUI_22() const { return ___homeTabUI_22; }
	inline GameObject_t1756533147 ** get_address_of_homeTabUI_22() { return &___homeTabUI_22; }
	inline void set_homeTabUI_22(GameObject_t1756533147 * value)
	{
		___homeTabUI_22 = value;
		Il2CppCodeGenWriteBarrier(&___homeTabUI_22, value);
	}

	inline static int32_t get_offset_of_homeLeftUI_23() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeLeftUI_23)); }
	inline GameObject_t1756533147 * get_homeLeftUI_23() const { return ___homeLeftUI_23; }
	inline GameObject_t1756533147 ** get_address_of_homeLeftUI_23() { return &___homeLeftUI_23; }
	inline void set_homeLeftUI_23(GameObject_t1756533147 * value)
	{
		___homeLeftUI_23 = value;
		Il2CppCodeGenWriteBarrier(&___homeLeftUI_23, value);
	}

	inline static int32_t get_offset_of_homeRightUI_24() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeRightUI_24)); }
	inline GameObject_t1756533147 * get_homeRightUI_24() const { return ___homeRightUI_24; }
	inline GameObject_t1756533147 ** get_address_of_homeRightUI_24() { return &___homeRightUI_24; }
	inline void set_homeRightUI_24(GameObject_t1756533147 * value)
	{
		___homeRightUI_24 = value;
		Il2CppCodeGenWriteBarrier(&___homeRightUI_24, value);
	}

	inline static int32_t get_offset_of_homeBottomUI_25() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___homeBottomUI_25)); }
	inline GameObject_t1756533147 * get_homeBottomUI_25() const { return ___homeBottomUI_25; }
	inline GameObject_t1756533147 ** get_address_of_homeBottomUI_25() { return &___homeBottomUI_25; }
	inline void set_homeBottomUI_25(GameObject_t1756533147 * value)
	{
		___homeBottomUI_25 = value;
		Il2CppCodeGenWriteBarrier(&___homeBottomUI_25, value);
	}

	inline static int32_t get_offset_of_sceneLoader_26() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___sceneLoader_26)); }
	inline GameObject_t1756533147 * get_sceneLoader_26() const { return ___sceneLoader_26; }
	inline GameObject_t1756533147 ** get_address_of_sceneLoader_26() { return &___sceneLoader_26; }
	inline void set_sceneLoader_26(GameObject_t1756533147 * value)
	{
		___sceneLoader_26 = value;
		Il2CppCodeGenWriteBarrier(&___sceneLoader_26, value);
	}

	inline static int32_t get_offset_of_friendsLoadedFromKnockout_27() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___friendsLoadedFromKnockout_27)); }
	inline bool get_friendsLoadedFromKnockout_27() const { return ___friendsLoadedFromKnockout_27; }
	inline bool* get_address_of_friendsLoadedFromKnockout_27() { return &___friendsLoadedFromKnockout_27; }
	inline void set_friendsLoadedFromKnockout_27(bool value)
	{
		___friendsLoadedFromKnockout_27 = value;
	}

	inline static int32_t get_offset_of_startAttackNav_28() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startAttackNav_28)); }
	inline int32_t get_startAttackNav_28() const { return ___startAttackNav_28; }
	inline int32_t* get_address_of_startAttackNav_28() { return &___startAttackNav_28; }
	inline void set_startAttackNav_28(int32_t value)
	{
		___startAttackNav_28 = value;
	}

	inline static int32_t get_offset_of_startAttackHomeNav_29() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startAttackHomeNav_29)); }
	inline int32_t get_startAttackHomeNav_29() const { return ___startAttackHomeNav_29; }
	inline int32_t* get_address_of_startAttackHomeNav_29() { return &___startAttackHomeNav_29; }
	inline void set_startAttackHomeNav_29(int32_t value)
	{
		___startAttackHomeNav_29 = value;
	}

	inline static int32_t get_offset_of_activeMissile_30() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___activeMissile_30)); }
	inline Missile_t813944928 * get_activeMissile_30() const { return ___activeMissile_30; }
	inline Missile_t813944928 ** get_address_of_activeMissile_30() { return &___activeMissile_30; }
	inline void set_activeMissile_30(Missile_t813944928 * value)
	{
		___activeMissile_30 = value;
		Il2CppCodeGenWriteBarrier(&___activeMissile_30, value);
	}

	inline static int32_t get_offset_of_activeTarget_31() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___activeTarget_31)); }
	inline Friend_t3555014108 * get_activeTarget_31() const { return ___activeTarget_31; }
	inline Friend_t3555014108 ** get_address_of_activeTarget_31() { return &___activeTarget_31; }
	inline void set_activeTarget_31(Friend_t3555014108 * value)
	{
		___activeTarget_31 = value;
		Il2CppCodeGenWriteBarrier(&___activeTarget_31, value);
	}

	inline static int32_t get_offset_of_startInventoryMissile_32() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startInventoryMissile_32)); }
	inline Missile_t813944928 * get_startInventoryMissile_32() const { return ___startInventoryMissile_32; }
	inline Missile_t813944928 ** get_address_of_startInventoryMissile_32() { return &___startInventoryMissile_32; }
	inline void set_startInventoryMissile_32(Missile_t813944928 * value)
	{
		___startInventoryMissile_32 = value;
		Il2CppCodeGenWriteBarrier(&___startInventoryMissile_32, value);
	}

	inline static int32_t get_offset_of_startInventoryShieldItem_33() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startInventoryShieldItem_33)); }
	inline Shield_t3327121081 * get_startInventoryShieldItem_33() const { return ___startInventoryShieldItem_33; }
	inline Shield_t3327121081 ** get_address_of_startInventoryShieldItem_33() { return &___startInventoryShieldItem_33; }
	inline void set_startInventoryShieldItem_33(Shield_t3327121081 * value)
	{
		___startInventoryShieldItem_33 = value;
		Il2CppCodeGenWriteBarrier(&___startInventoryShieldItem_33, value);
	}

	inline static int32_t get_offset_of_startInventoryShield_34() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startInventoryShield_34)); }
	inline bool get_startInventoryShield_34() const { return ___startInventoryShield_34; }
	inline bool* get_address_of_startInventoryShield_34() { return &___startInventoryShield_34; }
	inline void set_startInventoryShield_34(bool value)
	{
		___startInventoryShield_34 = value;
	}

	inline static int32_t get_offset_of_startInventoryMine_35() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startInventoryMine_35)); }
	inline bool get_startInventoryMine_35() const { return ___startInventoryMine_35; }
	inline bool* get_address_of_startInventoryMine_35() { return &___startInventoryMine_35; }
	inline void set_startInventoryMine_35(bool value)
	{
		___startInventoryMine_35 = value;
	}

	inline static int32_t get_offset_of_startInventoryFuel_36() { return static_cast<int32_t>(offsetof(HomeSceneManager_t2690266116, ___startInventoryFuel_36)); }
	inline bool get_startInventoryFuel_36() const { return ___startInventoryFuel_36; }
	inline bool* get_address_of_startInventoryFuel_36() { return &___startInventoryFuel_36; }
	inline void set_startInventoryFuel_36(bool value)
	{
		___startInventoryFuel_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
