﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_BaseAudioSource2241787848.h"
#include "AssemblyU2DCSharp_Music799847391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicSource
struct  MusicSource_t904181206  : public BaseAudioSource_t2241787848
{
public:
	// Music MusicSource::musicType
	int32_t ___musicType_10;

public:
	inline static int32_t get_offset_of_musicType_10() { return static_cast<int32_t>(offsetof(MusicSource_t904181206, ___musicType_10)); }
	inline int32_t get_musicType_10() const { return ___musicType_10; }
	inline int32_t* get_address_of_musicType_10() { return &___musicType_10; }
	inline void set_musicType_10(int32_t value)
	{
		___musicType_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
