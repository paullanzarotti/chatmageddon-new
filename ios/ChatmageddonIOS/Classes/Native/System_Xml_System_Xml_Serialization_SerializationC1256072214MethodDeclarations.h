﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.SerializationCodeGenerator/HookInfo
struct HookInfo_t1256072214;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Serialization.SerializationCodeGenerator/HookInfo::.ctor()
extern "C"  void HookInfo__ctor_m3705316307 (HookInfo_t1256072214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
