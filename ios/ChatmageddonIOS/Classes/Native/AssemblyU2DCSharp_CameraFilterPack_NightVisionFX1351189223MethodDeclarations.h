﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_NightVisionFX
struct CameraFilterPack_NightVisionFX_t1351189223;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_NightVisionFX::.ctor()
extern "C"  void CameraFilterPack_NightVisionFX__ctor_m2775346070 (CameraFilterPack_NightVisionFX_t1351189223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_NightVisionFX::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_NightVisionFX_get_material_m1100161819 (CameraFilterPack_NightVisionFX_t1351189223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::Start()
extern "C"  void CameraFilterPack_NightVisionFX_Start_m2019945362 (CameraFilterPack_NightVisionFX_t1351189223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_NightVisionFX_OnRenderImage_m1150751194 (CameraFilterPack_NightVisionFX_t1351189223 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::Update()
extern "C"  void CameraFilterPack_NightVisionFX_Update_m2084971663 (CameraFilterPack_NightVisionFX_t1351189223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::OnDisable()
extern "C"  void CameraFilterPack_NightVisionFX_OnDisable_m3941028879 (CameraFilterPack_NightVisionFX_t1351189223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
