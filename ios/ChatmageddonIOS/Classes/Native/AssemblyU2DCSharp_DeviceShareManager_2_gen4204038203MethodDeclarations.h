﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DeviceShareManager_2_gen3099449981MethodDeclarations.h"

// System.Void DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::.ctor()
#define DeviceShareManager_2__ctor_m4129594259(__this, method) ((  void (*) (DeviceShareManager_2_t4204038203 *, const MethodInfo*))DeviceShareManager_2__ctor_m2042261962_gshared)(__this, method)
// System.Void DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::OpenUI(RequestLocation)
#define DeviceShareManager_2_OpenUI_m2303955795(__this, ___location0, method) ((  void (*) (DeviceShareManager_2_t4204038203 *, int32_t, const MethodInfo*))DeviceShareManager_2_OpenUI_m3055098682_gshared)(__this, ___location0, method)
// System.Void DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::CloseUI()
#define DeviceShareManager_2_CloseUI_m3059380833(__this, method) ((  void (*) (DeviceShareManager_2_t4204038203 *, const MethodInfo*))DeviceShareManager_2_CloseUI_m3020373880_gshared)(__this, method)
// System.Void DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::SetLocationAtlas(RequestLocation)
#define DeviceShareManager_2_SetLocationAtlas_m2659556735(__this, ___location0, method) ((  void (*) (DeviceShareManager_2_t4204038203 *, int32_t, const MethodInfo*))DeviceShareManager_2_SetLocationAtlas_m3523870158_gshared)(__this, ___location0, method)
// System.Boolean DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::CheckEmailAvailability()
#define DeviceShareManager_2_CheckEmailAvailability_m2235197286(__this, method) ((  bool (*) (DeviceShareManager_2_t4204038203 *, const MethodInfo*))DeviceShareManager_2_CheckEmailAvailability_m334056221_gshared)(__this, method)
// System.Void DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::SendEmail(System.String,System.String,System.String,System.Boolean)
#define DeviceShareManager_2_SendEmail_m2583020328(__this, ___toAddress0, ___subject1, ___messageBody2, ___isHTML3, method) ((  void (*) (DeviceShareManager_2_t4204038203 *, String_t*, String_t*, String_t*, bool, const MethodInfo*))DeviceShareManager_2_SendEmail_m1948246187_gshared)(__this, ___toAddress0, ___subject1, ___messageBody2, ___isHTML3, method)
// System.Boolean DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::CheckSMSAvailability()
#define DeviceShareManager_2_CheckSMSAvailability_m666418549(__this, method) ((  bool (*) (DeviceShareManager_2_t4204038203 *, const MethodInfo*))DeviceShareManager_2_CheckSMSAvailability_m425375544_gshared)(__this, method)
// System.Void DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>::SendSMS(System.String)
#define DeviceShareManager_2_SendSMS_m4284196954(__this, ___messageBody0, method) ((  void (*) (DeviceShareManager_2_t4204038203 *, String_t*, const MethodInfo*))DeviceShareManager_2_SendSMS_m211121685_gshared)(__this, ___messageBody0, method)
