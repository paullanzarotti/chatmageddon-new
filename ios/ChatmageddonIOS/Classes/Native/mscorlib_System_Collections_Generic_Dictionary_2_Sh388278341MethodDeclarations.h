﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>
struct ShimEnumerator_t388278341;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct Dictionary_2_t283153520;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2928189507_gshared (ShimEnumerator_t388278341 * __this, Dictionary_2_t283153520 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2928189507(__this, ___host0, method) ((  void (*) (ShimEnumerator_t388278341 *, Dictionary_2_t283153520 *, const MethodInfo*))ShimEnumerator__ctor_m2928189507_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m740855412_gshared (ShimEnumerator_t388278341 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m740855412(__this, method) ((  bool (*) (ShimEnumerator_t388278341 *, const MethodInfo*))ShimEnumerator_MoveNext_m740855412_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3297173478_gshared (ShimEnumerator_t388278341 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3297173478(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t388278341 *, const MethodInfo*))ShimEnumerator_get_Entry_m3297173478_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4177662399_gshared (ShimEnumerator_t388278341 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m4177662399(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t388278341 *, const MethodInfo*))ShimEnumerator_get_Key_m4177662399_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m884789247_gshared (ShimEnumerator_t388278341 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m884789247(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t388278341 *, const MethodInfo*))ShimEnumerator_get_Value_m884789247_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1890787421_gshared (ShimEnumerator_t388278341 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1890787421(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t388278341 *, const MethodInfo*))ShimEnumerator_get_Current_m1890787421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Unibill.Impl.BillingPlatform,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3012282725_gshared (ShimEnumerator_t388278341 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3012282725(__this, method) ((  void (*) (ShimEnumerator_t388278341 *, const MethodInfo*))ShimEnumerator_Reset_m3012282725_gshared)(__this, method)
