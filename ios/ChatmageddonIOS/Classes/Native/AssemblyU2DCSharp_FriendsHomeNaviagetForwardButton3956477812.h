﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsHomeNaviagetForwardButton
struct  FriendsHomeNaviagetForwardButton_t3956477812  : public SFXButton_t792651341
{
public:
	// FriendHomeNavScreen FriendsHomeNaviagetForwardButton::nextScreen
	int32_t ___nextScreen_5;

public:
	inline static int32_t get_offset_of_nextScreen_5() { return static_cast<int32_t>(offsetof(FriendsHomeNaviagetForwardButton_t3956477812, ___nextScreen_5)); }
	inline int32_t get_nextScreen_5() const { return ___nextScreen_5; }
	inline int32_t* get_address_of_nextScreen_5() { return &___nextScreen_5; }
	inline void set_nextScreen_5(int32_t value)
	{
		___nextScreen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
