﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageNavScreen
struct  MessageNavScreen_t601131416  : public NavigationScreen_t2333230110
{
public:
	// UILabel MessageNavScreen::messageContactsLabel
	UILabel_t1795115428 * ___messageContactsLabel_3;
	// System.Boolean MessageNavScreen::messageUIclosing
	bool ___messageUIclosing_4;

public:
	inline static int32_t get_offset_of_messageContactsLabel_3() { return static_cast<int32_t>(offsetof(MessageNavScreen_t601131416, ___messageContactsLabel_3)); }
	inline UILabel_t1795115428 * get_messageContactsLabel_3() const { return ___messageContactsLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_messageContactsLabel_3() { return &___messageContactsLabel_3; }
	inline void set_messageContactsLabel_3(UILabel_t1795115428 * value)
	{
		___messageContactsLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___messageContactsLabel_3, value);
	}

	inline static int32_t get_offset_of_messageUIclosing_4() { return static_cast<int32_t>(offsetof(MessageNavScreen_t601131416, ___messageUIclosing_4)); }
	inline bool get_messageUIclosing_4() const { return ___messageUIclosing_4; }
	inline bool* get_address_of_messageUIclosing_4() { return &___messageUIclosing_4; }
	inline void set_messageUIclosing_4(bool value)
	{
		___messageUIclosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
