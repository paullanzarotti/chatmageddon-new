﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextPageUIScaler
struct TextPageUIScaler_t3568234746;

#include "codegen/il2cpp-codegen.h"

// System.Void TextPageUIScaler::.ctor()
extern "C"  void TextPageUIScaler__ctor_m1092881743 (TextPageUIScaler_t3568234746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageUIScaler::GlobalUIScale()
extern "C"  void TextPageUIScaler_GlobalUIScale_m3478960420 (TextPageUIScaler_t3568234746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
