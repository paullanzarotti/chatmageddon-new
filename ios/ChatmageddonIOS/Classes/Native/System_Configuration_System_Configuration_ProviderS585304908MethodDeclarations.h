﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ProviderSettingsCollection
struct ProviderSettingsCollection_t585304908;
// System.Configuration.ProviderSettings
struct ProviderSettings_t873049714;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_ProviderS873049714.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.ProviderSettingsCollection::.ctor()
extern "C"  void ProviderSettingsCollection__ctor_m2315192167 (ProviderSettingsCollection_t585304908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProviderSettingsCollection::.cctor()
extern "C"  void ProviderSettingsCollection__cctor_m2098499738 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProviderSettingsCollection::Add(System.Configuration.ProviderSettings)
extern "C"  void ProviderSettingsCollection_Add_m790744723 (ProviderSettingsCollection_t585304908 * __this, ProviderSettings_t873049714 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProviderSettingsCollection::Clear()
extern "C"  void ProviderSettingsCollection_Clear_m617478214 (ProviderSettingsCollection_t585304908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElement System.Configuration.ProviderSettingsCollection::CreateNewElement()
extern "C"  ConfigurationElement_t1776195828 * ProviderSettingsCollection_CreateNewElement_m278470757 (ProviderSettingsCollection_t585304908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ProviderSettingsCollection::GetElementKey(System.Configuration.ConfigurationElement)
extern "C"  Il2CppObject * ProviderSettingsCollection_GetElementKey_m2598146650 (ProviderSettingsCollection_t585304908 * __this, ConfigurationElement_t1776195828 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProviderSettingsCollection::Remove(System.String)
extern "C"  void ProviderSettingsCollection_Remove_m425439395 (ProviderSettingsCollection_t585304908 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProviderSettings System.Configuration.ProviderSettingsCollection::get_Item(System.Int32)
extern "C"  ProviderSettings_t873049714 * ProviderSettingsCollection_get_Item_m1368642030 (ProviderSettingsCollection_t585304908 * __this, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProviderSettingsCollection::set_Item(System.Int32,System.Configuration.ProviderSettings)
extern "C"  void ProviderSettingsCollection_set_Item_m892789211 (ProviderSettingsCollection_t585304908 * __this, int32_t ___n0, ProviderSettings_t873049714 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProviderSettings System.Configuration.ProviderSettingsCollection::get_Item(System.String)
extern "C"  ProviderSettings_t873049714 * ProviderSettingsCollection_get_Item_m1877501527 (ProviderSettingsCollection_t585304908 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettingsCollection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * ProviderSettingsCollection_get_Properties_m2175226160 (ProviderSettingsCollection_t585304908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
