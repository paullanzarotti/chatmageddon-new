﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2052945962(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1747320370 *, Dictionary_2_t427295668 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2598283851(__this, method) ((  Il2CppObject * (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1954681163(__this, method) ((  void (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m663259046(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m506836901(__this, method) ((  Il2CppObject * (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m550852149(__this, method) ((  Il2CppObject * (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::MoveNext()
#define Enumerator_MoveNext_m2159275407(__this, method) ((  bool (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::get_Current()
#define Enumerator_get_Current_m2435021615(__this, method) ((  KeyValuePair_2_t2479608186  (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1842281690(__this, method) ((  String_t* (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3551401410(__this, method) ((  BetterList_1_t2807483702 * (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::Reset()
#define Enumerator_Reset_m3672434280(__this, method) ((  void (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::VerifyState()
#define Enumerator_VerifyState_m1824470133(__this, method) ((  void (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3412064283(__this, method) ((  void (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BetterList`1<UITextList/Paragraph>>::Dispose()
#define Enumerator_Dispose_m377925990(__this, method) ((  void (*) (Enumerator_t1747320370 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
