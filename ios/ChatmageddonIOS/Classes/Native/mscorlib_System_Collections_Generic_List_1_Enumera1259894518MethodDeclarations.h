﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<LeaderboardNavScreen>
struct List_1_t1725164844;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1259894518.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"

// System.Void System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1082568591_gshared (Enumerator_t1259894518 * __this, List_1_t1725164844 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1082568591(__this, ___l0, method) ((  void (*) (Enumerator_t1259894518 *, List_1_t1725164844 *, const MethodInfo*))Enumerator__ctor_m1082568591_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2625597331_gshared (Enumerator_t1259894518 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2625597331(__this, method) ((  void (*) (Enumerator_t1259894518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2625597331_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1169145155_gshared (Enumerator_t1259894518 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1169145155(__this, method) ((  Il2CppObject * (*) (Enumerator_t1259894518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1169145155_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m3297537416_gshared (Enumerator_t1259894518 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3297537416(__this, method) ((  void (*) (Enumerator_t1259894518 *, const MethodInfo*))Enumerator_Dispose_m3297537416_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3071539625_gshared (Enumerator_t1259894518 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3071539625(__this, method) ((  void (*) (Enumerator_t1259894518 *, const MethodInfo*))Enumerator_VerifyState_m3071539625_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1448326551_gshared (Enumerator_t1259894518 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1448326551(__this, method) ((  bool (*) (Enumerator_t1259894518 *, const MethodInfo*))Enumerator_MoveNext_m1448326551_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<LeaderboardNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m628689356_gshared (Enumerator_t1259894518 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m628689356(__this, method) ((  int32_t (*) (Enumerator_t1259894518 *, const MethodInfo*))Enumerator_get_Current_m628689356_gshared)(__this, method)
