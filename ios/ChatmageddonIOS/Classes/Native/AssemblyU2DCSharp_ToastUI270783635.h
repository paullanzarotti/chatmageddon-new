﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToastUI
struct  ToastUI_t270783635  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ToastUI::toastID
	int32_t ___toastID_2;
	// UILabel ToastUI::messageLabel
	UILabel_t1795115428 * ___messageLabel_3;
	// UISprite ToastUI::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_4;
	// System.Single ToastUI::displaySeconds
	float ___displaySeconds_5;

public:
	inline static int32_t get_offset_of_toastID_2() { return static_cast<int32_t>(offsetof(ToastUI_t270783635, ___toastID_2)); }
	inline int32_t get_toastID_2() const { return ___toastID_2; }
	inline int32_t* get_address_of_toastID_2() { return &___toastID_2; }
	inline void set_toastID_2(int32_t value)
	{
		___toastID_2 = value;
	}

	inline static int32_t get_offset_of_messageLabel_3() { return static_cast<int32_t>(offsetof(ToastUI_t270783635, ___messageLabel_3)); }
	inline UILabel_t1795115428 * get_messageLabel_3() const { return ___messageLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_messageLabel_3() { return &___messageLabel_3; }
	inline void set_messageLabel_3(UILabel_t1795115428 * value)
	{
		___messageLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___messageLabel_3, value);
	}

	inline static int32_t get_offset_of_backgroundSprite_4() { return static_cast<int32_t>(offsetof(ToastUI_t270783635, ___backgroundSprite_4)); }
	inline UISprite_t603616735 * get_backgroundSprite_4() const { return ___backgroundSprite_4; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_4() { return &___backgroundSprite_4; }
	inline void set_backgroundSprite_4(UISprite_t603616735 * value)
	{
		___backgroundSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_4, value);
	}

	inline static int32_t get_offset_of_displaySeconds_5() { return static_cast<int32_t>(offsetof(ToastUI_t270783635, ___displaySeconds_5)); }
	inline float get_displaySeconds_5() const { return ___displaySeconds_5; }
	inline float* get_address_of_displaySeconds_5() { return &___displaySeconds_5; }
	inline void set_displaySeconds_5(float value)
	{
		___displaySeconds_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
