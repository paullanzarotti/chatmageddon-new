﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.MiniJSON
struct MiniJSON_t333594015;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"

// System.Void Unibill.Impl.MiniJSON::.ctor()
extern "C"  void MiniJSON__ctor_m1138227735 (MiniJSON_t333594015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.MiniJSON::jsonDecode(System.String)
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m479867114 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.MiniJSON::jsonEncode(System.Object)
extern "C"  String_t* MiniJSON_jsonEncode_m4082087854 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJSON::lastDecodeSuccessful()
extern "C"  bool MiniJSON_lastDecodeSuccessful_m579523925 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Impl.MiniJSON::getLastErrorIndex()
extern "C"  int32_t MiniJSON_getLastErrorIndex_m3125518187 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.MiniJSON::getLastErrorSnippet()
extern "C"  String_t* MiniJSON_getLastErrorSnippet_m4188153375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.MiniJSON::parseObject(System.Char[],System.Int32&)
extern "C"  Dictionary_2_t309261261 * MiniJSON_parseObject_m139944321 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Unibill.Impl.MiniJSON::parseArray(System.Char[],System.Int32&)
extern "C"  List_1_t2058570427 * MiniJSON_parseArray_m2417152594 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * MiniJSON_parseValue_m2426902167 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.MiniJSON::parseString(System.Char[],System.Int32&)
extern "C"  String_t* MiniJSON_parseString_m3233973526 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Unibill.Impl.MiniJSON::parseNumber(System.Char[],System.Int32&)
extern "C"  double MiniJSON_parseNumber_m3325690766 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Impl.MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m2153263829 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern "C"  void MiniJSON_eatWhitespace_m1601671410 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Impl.MiniJSON::lookAhead(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_lookAhead_m3308513531 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Impl.MiniJSON::nextToken(System.Char[],System.Int32&)
extern "C"  int32_t MiniJSON_nextToken_m3406073459 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJSON::serializeObjectOrArray(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObjectOrArray_m2134633982 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objectOrArray0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJSON::serializeObject(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObject_m831267143 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___anObject0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJSON::serializeDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeDictionary_m3845160288 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___dict0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJSON::serializeArray(System.Collections.Generic.List`1<System.Object>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeArray_m4248055834 (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___anArray0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJSON::serializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeValue_m2853151032 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeString_m3487508160 (Il2CppObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeNumber_m4059591184 (Il2CppObject * __this /* static, unused */, double ___number0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.MiniJSON::.cctor()
extern "C"  void MiniJSON__cctor_m1979758556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
