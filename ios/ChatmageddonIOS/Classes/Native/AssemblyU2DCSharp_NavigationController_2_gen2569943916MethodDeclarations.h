﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2219628511MethodDeclarations.h"

// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::.ctor()
#define NavigationController_2__ctor_m95389190(__this, method) ((  void (*) (NavigationController_2_t2569943916 *, const MethodInfo*))NavigationController_2__ctor_m2423512526_gshared)(__this, method)
// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m2921426028(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2569943916 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1224047836_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m353263625(__this, method) ((  void (*) (NavigationController_2_t2569943916 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m312182917_gshared)(__this, method)
// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3238015450(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2569943916 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1564361018_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<AttackNavigationController,AttackNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m2681961982(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2569943916 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m3710279390_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<AttackNavigationController,AttackNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m3817335063(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2569943916 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m775835947_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m650467344(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2569943916 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m2565550832_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m3304002286(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2569943916 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m1108255294_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<AttackNavigationController,AttackNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m3815436558(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2569943916 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m236222014_gshared)(__this, ___screen0, method)
