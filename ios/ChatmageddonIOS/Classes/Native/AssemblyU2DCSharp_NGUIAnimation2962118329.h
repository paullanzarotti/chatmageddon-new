﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// NGUIAnimation/OnAnimationStart
struct OnAnimationStart_t3313445917;
// NGUIAnimation/OnAnimationDrawn
struct OnAnimationDrawn_t3207005891;
// NGUIAnimation/OnAnimationUpdated
struct OnAnimationUpdated_t4231811366;
// NGUIAnimation/OnAnimationLooped
struct OnAnimationLooped_t2655604368;
// NGUIAnimation/OnAnimationFinished
struct OnAnimationFinished_t3859016523;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIAnimation
struct  NGUIAnimation_t2962118329  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 NGUIAnimation::fps
	int32_t ___fps_2;
	// System.Int32 NGUIAnimation::totalFrames
	int32_t ___totalFrames_3;
	// System.Boolean NGUIAnimation::paused
	bool ___paused_4;
	// System.Int32 NGUIAnimation::currentFrame
	int32_t ___currentFrame_5;
	// UnityEngine.Coroutine NGUIAnimation::animationRoutine
	Coroutine_t2299508840 * ___animationRoutine_6;
	// NGUIAnimation/OnAnimationStart NGUIAnimation::onAnimationStart
	OnAnimationStart_t3313445917 * ___onAnimationStart_7;
	// NGUIAnimation/OnAnimationDrawn NGUIAnimation::onAnimationDrawn
	OnAnimationDrawn_t3207005891 * ___onAnimationDrawn_8;
	// NGUIAnimation/OnAnimationUpdated NGUIAnimation::onAnimationUpdated
	OnAnimationUpdated_t4231811366 * ___onAnimationUpdated_9;
	// NGUIAnimation/OnAnimationLooped NGUIAnimation::onAnimationLooped
	OnAnimationLooped_t2655604368 * ___onAnimationLooped_10;
	// NGUIAnimation/OnAnimationFinished NGUIAnimation::onAnimationFinished
	OnAnimationFinished_t3859016523 * ___onAnimationFinished_11;

public:
	inline static int32_t get_offset_of_fps_2() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___fps_2)); }
	inline int32_t get_fps_2() const { return ___fps_2; }
	inline int32_t* get_address_of_fps_2() { return &___fps_2; }
	inline void set_fps_2(int32_t value)
	{
		___fps_2 = value;
	}

	inline static int32_t get_offset_of_totalFrames_3() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___totalFrames_3)); }
	inline int32_t get_totalFrames_3() const { return ___totalFrames_3; }
	inline int32_t* get_address_of_totalFrames_3() { return &___totalFrames_3; }
	inline void set_totalFrames_3(int32_t value)
	{
		___totalFrames_3 = value;
	}

	inline static int32_t get_offset_of_paused_4() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___paused_4)); }
	inline bool get_paused_4() const { return ___paused_4; }
	inline bool* get_address_of_paused_4() { return &___paused_4; }
	inline void set_paused_4(bool value)
	{
		___paused_4 = value;
	}

	inline static int32_t get_offset_of_currentFrame_5() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___currentFrame_5)); }
	inline int32_t get_currentFrame_5() const { return ___currentFrame_5; }
	inline int32_t* get_address_of_currentFrame_5() { return &___currentFrame_5; }
	inline void set_currentFrame_5(int32_t value)
	{
		___currentFrame_5 = value;
	}

	inline static int32_t get_offset_of_animationRoutine_6() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___animationRoutine_6)); }
	inline Coroutine_t2299508840 * get_animationRoutine_6() const { return ___animationRoutine_6; }
	inline Coroutine_t2299508840 ** get_address_of_animationRoutine_6() { return &___animationRoutine_6; }
	inline void set_animationRoutine_6(Coroutine_t2299508840 * value)
	{
		___animationRoutine_6 = value;
		Il2CppCodeGenWriteBarrier(&___animationRoutine_6, value);
	}

	inline static int32_t get_offset_of_onAnimationStart_7() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___onAnimationStart_7)); }
	inline OnAnimationStart_t3313445917 * get_onAnimationStart_7() const { return ___onAnimationStart_7; }
	inline OnAnimationStart_t3313445917 ** get_address_of_onAnimationStart_7() { return &___onAnimationStart_7; }
	inline void set_onAnimationStart_7(OnAnimationStart_t3313445917 * value)
	{
		___onAnimationStart_7 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimationStart_7, value);
	}

	inline static int32_t get_offset_of_onAnimationDrawn_8() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___onAnimationDrawn_8)); }
	inline OnAnimationDrawn_t3207005891 * get_onAnimationDrawn_8() const { return ___onAnimationDrawn_8; }
	inline OnAnimationDrawn_t3207005891 ** get_address_of_onAnimationDrawn_8() { return &___onAnimationDrawn_8; }
	inline void set_onAnimationDrawn_8(OnAnimationDrawn_t3207005891 * value)
	{
		___onAnimationDrawn_8 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimationDrawn_8, value);
	}

	inline static int32_t get_offset_of_onAnimationUpdated_9() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___onAnimationUpdated_9)); }
	inline OnAnimationUpdated_t4231811366 * get_onAnimationUpdated_9() const { return ___onAnimationUpdated_9; }
	inline OnAnimationUpdated_t4231811366 ** get_address_of_onAnimationUpdated_9() { return &___onAnimationUpdated_9; }
	inline void set_onAnimationUpdated_9(OnAnimationUpdated_t4231811366 * value)
	{
		___onAnimationUpdated_9 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimationUpdated_9, value);
	}

	inline static int32_t get_offset_of_onAnimationLooped_10() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___onAnimationLooped_10)); }
	inline OnAnimationLooped_t2655604368 * get_onAnimationLooped_10() const { return ___onAnimationLooped_10; }
	inline OnAnimationLooped_t2655604368 ** get_address_of_onAnimationLooped_10() { return &___onAnimationLooped_10; }
	inline void set_onAnimationLooped_10(OnAnimationLooped_t2655604368 * value)
	{
		___onAnimationLooped_10 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimationLooped_10, value);
	}

	inline static int32_t get_offset_of_onAnimationFinished_11() { return static_cast<int32_t>(offsetof(NGUIAnimation_t2962118329, ___onAnimationFinished_11)); }
	inline OnAnimationFinished_t3859016523 * get_onAnimationFinished_11() const { return ___onAnimationFinished_11; }
	inline OnAnimationFinished_t3859016523 ** get_address_of_onAnimationFinished_11() { return &___onAnimationFinished_11; }
	inline void set_onAnimationFinished_11(OnAnimationFinished_t3859016523 * value)
	{
		___onAnimationFinished_11 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimationFinished_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
