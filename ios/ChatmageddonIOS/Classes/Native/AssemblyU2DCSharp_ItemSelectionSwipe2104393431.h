﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemSelectionSwipe
struct  ItemSelectionSwipe_t2104393431  : public NGUISwipe_t4135300327
{
public:
	// UnityEngine.GameObject ItemSelectionSwipe::infoObject
	GameObject_t1756533147 * ___infoObject_10;

public:
	inline static int32_t get_offset_of_infoObject_10() { return static_cast<int32_t>(offsetof(ItemSelectionSwipe_t2104393431, ___infoObject_10)); }
	inline GameObject_t1756533147 * get_infoObject_10() const { return ___infoObject_10; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_10() { return &___infoObject_10; }
	inline void set_infoObject_10(GameObject_t1756533147 * value)
	{
		___infoObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
