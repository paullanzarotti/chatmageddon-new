﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FeedManager
struct FeedManager_t2709865823;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoSingleton`1<FeedManager>
struct  MonoSingleton_1_t2460531543  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct MonoSingleton_1_t2460531543_StaticFields
{
public:
	// T MonoSingleton`1::m_Instance
	FeedManager_t2709865823 * ___m_Instance_2;

public:
	inline static int32_t get_offset_of_m_Instance_2() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t2460531543_StaticFields, ___m_Instance_2)); }
	inline FeedManager_t2709865823 * get_m_Instance_2() const { return ___m_Instance_2; }
	inline FeedManager_t2709865823 ** get_address_of_m_Instance_2() { return &___m_Instance_2; }
	inline void set_m_Instance_2(FeedManager_t2709865823 * value)
	{
		___m_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
