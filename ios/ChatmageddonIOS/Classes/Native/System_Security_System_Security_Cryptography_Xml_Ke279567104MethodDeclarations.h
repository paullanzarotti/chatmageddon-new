﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfoClause
struct KeyInfoClause_t279567104;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.Xml.KeyInfoClause::.ctor()
extern "C"  void KeyInfoClause__ctor_m1480307586 (KeyInfoClause_t279567104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
