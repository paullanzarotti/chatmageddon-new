﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackNavigateBackwardsButton
struct AttackNavigateBackwardsButton_t4019147023;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void AttackNavigateBackwardsButton::.ctor()
extern "C"  void AttackNavigateBackwardsButton__ctor_m638406406 (AttackNavigateBackwardsButton_t4019147023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigateBackwardsButton::OnButtonClick()
extern "C"  void AttackNavigateBackwardsButton_OnButtonClick_m1736005543 (AttackNavigateBackwardsButton_t4019147023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigateBackwardsButton::SetButtonDirection(ButtonDirection)
extern "C"  void AttackNavigateBackwardsButton_SetButtonDirection_m4150958210 (AttackNavigateBackwardsButton_t4019147023 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
