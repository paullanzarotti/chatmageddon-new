﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Crashlytics.Crashlytics
struct Crashlytics_t3790431817;
// System.String
struct String_t;
// System.Diagnostics.StackTrace
struct StackTrace_t2500644597;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Diagnostics_StackTrace2500644597.h"

// System.Void Fabric.Crashlytics.Crashlytics::.cctor()
extern "C"  void Crashlytics__cctor_m4115810197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::.ctor()
extern "C"  void Crashlytics__ctor_m1156355032 (Crashlytics_t3790431817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::SetDebugMode(System.Boolean)
extern "C"  void Crashlytics_SetDebugMode_m945865225 (Il2CppObject * __this /* static, unused */, bool ___debugMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::Crash()
extern "C"  void Crashlytics_Crash_m2810198901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::ThrowNonFatal()
extern "C"  void Crashlytics_ThrowNonFatal_m4140051837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::Log(System.String)
extern "C"  void Crashlytics_Log_m174116500 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::SetKeyValue(System.String,System.String)
extern "C"  void Crashlytics_SetKeyValue_m2904962798 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::SetUserIdentifier(System.String)
extern "C"  void Crashlytics_SetUserIdentifier_m1646790566 (Il2CppObject * __this /* static, unused */, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::SetUserEmail(System.String)
extern "C"  void Crashlytics_SetUserEmail_m1229484403 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::SetUserName(System.String)
extern "C"  void Crashlytics_SetUserName_m2558674906 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::RecordCustomException(System.String,System.String,System.Diagnostics.StackTrace)
extern "C"  void Crashlytics_RecordCustomException_m840604173 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___reason1, StackTrace_t2500644597 * ___stackTrace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Crashlytics::RecordCustomException(System.String,System.String,System.String)
extern "C"  void Crashlytics_RecordCustomException_m1834105209 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___reason1, String_t* ___stackTraceString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
