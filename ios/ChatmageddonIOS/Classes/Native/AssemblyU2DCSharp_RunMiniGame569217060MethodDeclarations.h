﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RunMiniGame
struct RunMiniGame_t569217060;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void RunMiniGame::.ctor()
extern "C"  void RunMiniGame__ctor_m2301011963 (RunMiniGame_t569217060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunMiniGame::StartMiniGame()
extern "C"  void RunMiniGame_StartMiniGame_m2753425322 (RunMiniGame_t569217060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunMiniGame::StopMiniGame()
extern "C"  void RunMiniGame_StopMiniGame_m1166527618 (RunMiniGame_t569217060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunMiniGame::OnPaused()
extern "C"  void RunMiniGame_OnPaused_m323868092 (RunMiniGame_t569217060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunMiniGame::ResetMiniGame()
extern "C"  void RunMiniGame_ResetMiniGame_m1583444723 (RunMiniGame_t569217060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunMiniGame::TimerUpdated(System.TimeSpan)
extern "C"  void RunMiniGame_TimerUpdated_m1827601575 (RunMiniGame_t569217060 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
