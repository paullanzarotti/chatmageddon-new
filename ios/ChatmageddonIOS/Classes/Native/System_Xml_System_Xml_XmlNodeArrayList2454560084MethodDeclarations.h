﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlNodeArrayList
struct XmlNodeArrayList_t2454560084;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Xml.XmlNode
struct XmlNode_t616554813;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void System.Xml.XmlNodeArrayList::.ctor(System.Collections.ArrayList)
extern "C"  void XmlNodeArrayList__ctor_m2105563044 (XmlNodeArrayList_t2454560084 * __this, ArrayList_t4252133567 * ___rgNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XmlNodeArrayList::get_Count()
extern "C"  int32_t XmlNodeArrayList_get_Count_m1597038967 (XmlNodeArrayList_t2454560084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.XmlNodeArrayList::GetEnumerator()
extern "C"  Il2CppObject * XmlNodeArrayList_GetEnumerator_m1050411051 (XmlNodeArrayList_t2454560084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNodeArrayList::Item(System.Int32)
extern "C"  XmlNode_t616554813 * XmlNodeArrayList_Item_m4212294581 (XmlNodeArrayList_t2454560084 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
