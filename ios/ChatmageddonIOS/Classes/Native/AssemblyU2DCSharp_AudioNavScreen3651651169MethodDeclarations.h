﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioNavScreen
struct AudioNavScreen_t3651651169;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AudioNavScreen::.ctor()
extern "C"  void AudioNavScreen__ctor_m81075050 (AudioNavScreen_t3651651169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioNavScreen::Start()
extern "C"  void AudioNavScreen_Start_m2857527170 (AudioNavScreen_t3651651169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioNavScreen::UIClosing()
extern "C"  void AudioNavScreen_UIClosing_m4048914819 (AudioNavScreen_t3651651169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AudioNavScreen_ScreenLoad_m1189725766 (AudioNavScreen_t3651651169 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AudioNavScreen_ScreenUnload_m2115170934 (AudioNavScreen_t3651651169 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AudioNavScreen_ValidateScreenNavigation_m1642482227 (AudioNavScreen_t3651651169 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
