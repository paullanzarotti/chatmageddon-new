﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/OnSchemeChange
struct OnSchemeChange_t1272113120;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICamera/OnSchemeChange::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSchemeChange__ctor_m2034699539 (OnSchemeChange_t1272113120 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera/OnSchemeChange::Invoke()
extern "C"  void OnSchemeChange_Invoke_m2468207623 (OnSchemeChange_t1272113120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICamera/OnSchemeChange::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSchemeChange_BeginInvoke_m836164186 (OnSchemeChange_t1272113120 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera/OnSchemeChange::EndInvoke(System.IAsyncResult)
extern "C"  void OnSchemeChange_EndInvoke_m3236588105 (OnSchemeChange_t1272113120 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
