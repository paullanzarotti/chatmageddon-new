﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EnumTimeType570234944.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ELANNotification
struct  ELANNotification_t786863861  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ELANNotification::ID
	int32_t ___ID_2;
	// System.String ELANNotification::fullClassName
	String_t* ___fullClassName_3;
	// System.String ELANNotification::title
	String_t* ___title_4;
	// System.String ELANNotification::message
	String_t* ___message_5;
	// EnumTimeType ELANNotification::delayTypeTime
	int32_t ___delayTypeTime_6;
	// System.Int32 ELANNotification::delay
	int32_t ___delay_7;
	// EnumTimeType ELANNotification::repetitionTypeTime
	int32_t ___repetitionTypeTime_8;
	// System.Int32 ELANNotification::repetition
	int32_t ___repetition_9;
	// System.Boolean ELANNotification::advancedNotification
	bool ___advancedNotification_10;
	// System.Boolean ELANNotification::useVibration
	bool ___useVibration_11;
	// System.Boolean ELANNotification::useSound
	bool ___useSound_12;
	// System.Boolean ELANNotification::sendOnStart
	bool ___sendOnStart_13;

public:
	inline static int32_t get_offset_of_ID_2() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___ID_2)); }
	inline int32_t get_ID_2() const { return ___ID_2; }
	inline int32_t* get_address_of_ID_2() { return &___ID_2; }
	inline void set_ID_2(int32_t value)
	{
		___ID_2 = value;
	}

	inline static int32_t get_offset_of_fullClassName_3() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___fullClassName_3)); }
	inline String_t* get_fullClassName_3() const { return ___fullClassName_3; }
	inline String_t** get_address_of_fullClassName_3() { return &___fullClassName_3; }
	inline void set_fullClassName_3(String_t* value)
	{
		___fullClassName_3 = value;
		Il2CppCodeGenWriteBarrier(&___fullClassName_3, value);
	}

	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___title_4)); }
	inline String_t* get_title_4() const { return ___title_4; }
	inline String_t** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(String_t* value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier(&___title_4, value);
	}

	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___message_5)); }
	inline String_t* get_message_5() const { return ___message_5; }
	inline String_t** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(String_t* value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_5, value);
	}

	inline static int32_t get_offset_of_delayTypeTime_6() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___delayTypeTime_6)); }
	inline int32_t get_delayTypeTime_6() const { return ___delayTypeTime_6; }
	inline int32_t* get_address_of_delayTypeTime_6() { return &___delayTypeTime_6; }
	inline void set_delayTypeTime_6(int32_t value)
	{
		___delayTypeTime_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___delay_7)); }
	inline int32_t get_delay_7() const { return ___delay_7; }
	inline int32_t* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(int32_t value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_repetitionTypeTime_8() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___repetitionTypeTime_8)); }
	inline int32_t get_repetitionTypeTime_8() const { return ___repetitionTypeTime_8; }
	inline int32_t* get_address_of_repetitionTypeTime_8() { return &___repetitionTypeTime_8; }
	inline void set_repetitionTypeTime_8(int32_t value)
	{
		___repetitionTypeTime_8 = value;
	}

	inline static int32_t get_offset_of_repetition_9() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___repetition_9)); }
	inline int32_t get_repetition_9() const { return ___repetition_9; }
	inline int32_t* get_address_of_repetition_9() { return &___repetition_9; }
	inline void set_repetition_9(int32_t value)
	{
		___repetition_9 = value;
	}

	inline static int32_t get_offset_of_advancedNotification_10() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___advancedNotification_10)); }
	inline bool get_advancedNotification_10() const { return ___advancedNotification_10; }
	inline bool* get_address_of_advancedNotification_10() { return &___advancedNotification_10; }
	inline void set_advancedNotification_10(bool value)
	{
		___advancedNotification_10 = value;
	}

	inline static int32_t get_offset_of_useVibration_11() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___useVibration_11)); }
	inline bool get_useVibration_11() const { return ___useVibration_11; }
	inline bool* get_address_of_useVibration_11() { return &___useVibration_11; }
	inline void set_useVibration_11(bool value)
	{
		___useVibration_11 = value;
	}

	inline static int32_t get_offset_of_useSound_12() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___useSound_12)); }
	inline bool get_useSound_12() const { return ___useSound_12; }
	inline bool* get_address_of_useSound_12() { return &___useSound_12; }
	inline void set_useSound_12(bool value)
	{
		___useSound_12 = value;
	}

	inline static int32_t get_offset_of_sendOnStart_13() { return static_cast<int32_t>(offsetof(ELANNotification_t786863861, ___sendOnStart_13)); }
	inline bool get_sendOnStart_13() const { return ___sendOnStart_13; }
	inline bool* get_address_of_sendOnStart_13() { return &___sendOnStart_13; }
	inline void set_sendOnStart_13(bool value)
	{
		___sendOnStart_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
