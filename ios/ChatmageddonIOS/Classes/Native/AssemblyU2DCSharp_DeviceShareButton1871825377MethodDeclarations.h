﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceShareButton
struct DeviceShareButton_t1871825377;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceShareButton::.ctor()
extern "C"  void DeviceShareButton__ctor_m3866883216 (DeviceShareButton_t1871825377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceShareButton::OnButtonClick()
extern "C"  void DeviceShareButton_OnButtonClick_m211766933 (DeviceShareButton_t1871825377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
