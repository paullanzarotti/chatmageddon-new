﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Win8Eventhook
struct Win8Eventhook_t3380410125;

#include "codegen/il2cpp-codegen.h"

// System.Void Win8Eventhook::.ctor()
extern "C"  void Win8Eventhook__ctor_m2260117806 (Win8Eventhook_t3380410125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Win8Eventhook::Start()
extern "C"  void Win8Eventhook_Start_m2798294922 (Win8Eventhook_t3380410125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Win8Eventhook::OnApplicationPause(System.Boolean)
extern "C"  void Win8Eventhook_OnApplicationPause_m2126004976 (Win8Eventhook_t3380410125 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
