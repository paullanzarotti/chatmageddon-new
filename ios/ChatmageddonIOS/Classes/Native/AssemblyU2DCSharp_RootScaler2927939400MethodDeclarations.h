﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RootScaler
struct RootScaler_t2927939400;

#include "codegen/il2cpp-codegen.h"

// System.Void RootScaler::.ctor()
extern "C"  void RootScaler__ctor_m1917710391 (RootScaler_t2927939400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RootScaler::Awake()
extern "C"  void RootScaler_Awake_m2546431476 (RootScaler_t2927939400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
