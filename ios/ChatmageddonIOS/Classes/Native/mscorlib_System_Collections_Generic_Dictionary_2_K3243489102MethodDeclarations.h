﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>
struct KeyCollection_t3243489102;
// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t930005165;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t1328083999;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3449494769.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3562918664_gshared (KeyCollection_t3243489102 * __this, Dictionary_2_t759991331 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3562918664(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3243489102 *, Dictionary_2_t759991331 *, const MethodInfo*))KeyCollection__ctor_m3562918664_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2031910998_gshared (KeyCollection_t3243489102 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2031910998(__this, ___item0, method) ((  void (*) (KeyCollection_t3243489102 *, Il2CppChar, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2031910998_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2703945343_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2703945343(__this, method) ((  void (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2703945343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1585393262_gshared (KeyCollection_t3243489102 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1585393262(__this, ___item0, method) ((  bool (*) (KeyCollection_t3243489102 *, Il2CppChar, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1585393262_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27251255_gshared (KeyCollection_t3243489102 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27251255(__this, ___item0, method) ((  bool (*) (KeyCollection_t3243489102 *, Il2CppChar, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27251255_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3991137561_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3991137561(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3991137561_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1945696029_gshared (KeyCollection_t3243489102 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1945696029(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3243489102 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1945696029_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2634438742_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2634438742(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2634438742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3785208675_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3785208675(__this, method) ((  bool (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3785208675_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1885424537_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1885424537(__this, method) ((  bool (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1885424537_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3648513853_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3648513853(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3648513853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2987833023_gshared (KeyCollection_t3243489102 * __this, CharU5BU5D_t1328083999* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2987833023(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3243489102 *, CharU5BU5D_t1328083999*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2987833023_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::GetEnumerator()
extern "C"  Enumerator_t3449494769  KeyCollection_GetEnumerator_m3569543776_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3569543776(__this, method) ((  Enumerator_t3449494769  (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_GetEnumerator_m3569543776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,System.Char>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3320059193_gshared (KeyCollection_t3243489102 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3320059193(__this, method) ((  int32_t (*) (KeyCollection_t3243489102 *, const MethodInfo*))KeyCollection_get_Count_m3320059193_gshared)(__this, method)
