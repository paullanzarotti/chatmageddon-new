﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_AAA_WaterDrop
struct  CameraFilterPack_AAA_WaterDrop_t2481644515  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_AAA_WaterDrop::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_AAA_WaterDrop::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_AAA_WaterDrop::Distortion
	float ___Distortion_4;
	// System.Single CameraFilterPack_AAA_WaterDrop::SizeX
	float ___SizeX_5;
	// System.Single CameraFilterPack_AAA_WaterDrop::SizeY
	float ___SizeY_6;
	// System.Single CameraFilterPack_AAA_WaterDrop::Speed
	float ___Speed_7;
	// UnityEngine.Material CameraFilterPack_AAA_WaterDrop::SCMaterial
	Material_t193706927 * ___SCMaterial_8;
	// UnityEngine.Texture2D CameraFilterPack_AAA_WaterDrop::Texture2
	Texture2D_t3542995729 * ___Texture2_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Distortion_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___Distortion_4)); }
	inline float get_Distortion_4() const { return ___Distortion_4; }
	inline float* get_address_of_Distortion_4() { return &___Distortion_4; }
	inline void set_Distortion_4(float value)
	{
		___Distortion_4 = value;
	}

	inline static int32_t get_offset_of_SizeX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___SizeX_5)); }
	inline float get_SizeX_5() const { return ___SizeX_5; }
	inline float* get_address_of_SizeX_5() { return &___SizeX_5; }
	inline void set_SizeX_5(float value)
	{
		___SizeX_5 = value;
	}

	inline static int32_t get_offset_of_SizeY_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___SizeY_6)); }
	inline float get_SizeY_6() const { return ___SizeY_6; }
	inline float* get_address_of_SizeY_6() { return &___SizeY_6; }
	inline void set_SizeY_6(float value)
	{
		___SizeY_6 = value;
	}

	inline static int32_t get_offset_of_Speed_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___Speed_7)); }
	inline float get_Speed_7() const { return ___Speed_7; }
	inline float* get_address_of_Speed_7() { return &___Speed_7; }
	inline void set_Speed_7(float value)
	{
		___Speed_7 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___SCMaterial_8)); }
	inline Material_t193706927 * get_SCMaterial_8() const { return ___SCMaterial_8; }
	inline Material_t193706927 ** get_address_of_SCMaterial_8() { return &___SCMaterial_8; }
	inline void set_SCMaterial_8(Material_t193706927 * value)
	{
		___SCMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_8, value);
	}

	inline static int32_t get_offset_of_Texture2_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515, ___Texture2_9)); }
	inline Texture2D_t3542995729 * get_Texture2_9() const { return ___Texture2_9; }
	inline Texture2D_t3542995729 ** get_address_of_Texture2_9() { return &___Texture2_9; }
	inline void set_Texture2_9(Texture2D_t3542995729 * value)
	{
		___Texture2_9 = value;
		Il2CppCodeGenWriteBarrier(&___Texture2_9, value);
	}
};

struct CameraFilterPack_AAA_WaterDrop_t2481644515_StaticFields
{
public:
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeDistortion
	float ___ChangeDistortion_10;
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeSizeX
	float ___ChangeSizeX_11;
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeSizeY
	float ___ChangeSizeY_12;
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeSpeed
	float ___ChangeSpeed_13;

public:
	inline static int32_t get_offset_of_ChangeDistortion_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515_StaticFields, ___ChangeDistortion_10)); }
	inline float get_ChangeDistortion_10() const { return ___ChangeDistortion_10; }
	inline float* get_address_of_ChangeDistortion_10() { return &___ChangeDistortion_10; }
	inline void set_ChangeDistortion_10(float value)
	{
		___ChangeDistortion_10 = value;
	}

	inline static int32_t get_offset_of_ChangeSizeX_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515_StaticFields, ___ChangeSizeX_11)); }
	inline float get_ChangeSizeX_11() const { return ___ChangeSizeX_11; }
	inline float* get_address_of_ChangeSizeX_11() { return &___ChangeSizeX_11; }
	inline void set_ChangeSizeX_11(float value)
	{
		___ChangeSizeX_11 = value;
	}

	inline static int32_t get_offset_of_ChangeSizeY_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515_StaticFields, ___ChangeSizeY_12)); }
	inline float get_ChangeSizeY_12() const { return ___ChangeSizeY_12; }
	inline float* get_address_of_ChangeSizeY_12() { return &___ChangeSizeY_12; }
	inline void set_ChangeSizeY_12(float value)
	{
		___ChangeSizeY_12 = value;
	}

	inline static int32_t get_offset_of_ChangeSpeed_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_WaterDrop_t2481644515_StaticFields, ___ChangeSpeed_13)); }
	inline float get_ChangeSpeed_13() const { return ___ChangeSpeed_13; }
	inline float* get_address_of_ChangeSpeed_13() { return &___ChangeSpeed_13; }
	inline void set_ChangeSpeed_13(float value)
	{
		___ChangeSpeed_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
