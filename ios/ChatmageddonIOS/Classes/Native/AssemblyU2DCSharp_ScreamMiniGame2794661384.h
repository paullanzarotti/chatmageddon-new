﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScreamBarController
struct ScreamBarController_t3220293990;

#include "AssemblyU2DCSharp_DefendMiniGameController1844356491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreamMiniGame
struct  ScreamMiniGame_t2794661384  : public DefendMiniGameController_t1844356491
{
public:
	// ScreamBarController ScreamMiniGame::screamBar
	ScreamBarController_t3220293990 * ___screamBar_6;

public:
	inline static int32_t get_offset_of_screamBar_6() { return static_cast<int32_t>(offsetof(ScreamMiniGame_t2794661384, ___screamBar_6)); }
	inline ScreamBarController_t3220293990 * get_screamBar_6() const { return ___screamBar_6; }
	inline ScreamBarController_t3220293990 ** get_address_of_screamBar_6() { return &___screamBar_6; }
	inline void set_screamBar_6(ScreamBarController_t3220293990 * value)
	{
		___screamBar_6 = value;
		Il2CppCodeGenWriteBarrier(&___screamBar_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
