﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_AAA_SuperHexagon
struct CameraFilterPack_AAA_SuperHexagon_t116370066;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_AAA_SuperHexagon::.ctor()
extern "C"  void CameraFilterPack_AAA_SuperHexagon__ctor_m908881665 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_AAA_SuperHexagon::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_AAA_SuperHexagon_get_material_m540727926 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::Start()
extern "C"  void CameraFilterPack_AAA_SuperHexagon_Start_m1136527577 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_AAA_SuperHexagon_OnRenderImage_m2228845217 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::OnValidate()
extern "C"  void CameraFilterPack_AAA_SuperHexagon_OnValidate_m2678112510 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::Update()
extern "C"  void CameraFilterPack_AAA_SuperHexagon_Update_m2816663508 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::OnDisable()
extern "C"  void CameraFilterPack_AAA_SuperHexagon_OnDisable_m292323642 (CameraFilterPack_AAA_SuperHexagon_t116370066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::.cctor()
extern "C"  void CameraFilterPack_AAA_SuperHexagon__cctor_m2111312108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
