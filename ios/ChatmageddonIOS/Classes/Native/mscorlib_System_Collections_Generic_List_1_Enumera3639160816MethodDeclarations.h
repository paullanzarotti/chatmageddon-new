﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2524089505(__this, ___l0, method) ((  void (*) (Enumerator_t3639160816 *, List_1_t4104431142 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1544738097(__this, method) ((  void (*) (Enumerator_t3639160816 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1573720601(__this, method) ((  Il2CppObject * (*) (Enumerator_t3639160816 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::Dispose()
#define Enumerator_Dispose_m283806756(__this, method) ((  void (*) (Enumerator_t3639160816 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::VerifyState()
#define Enumerator_VerifyState_m2668401707(__this, method) ((  void (*) (Enumerator_t3639160816 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::MoveNext()
#define Enumerator_MoveNext_m35608161(__this, method) ((  bool (*) (Enumerator_t3639160816 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorPoints>::get_Current()
#define Enumerator_get_Current_m3143608254(__this, method) ((  VectorPoints_t440342714 * (*) (Enumerator_t3639160816 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
