﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadingIndicator/IndicatorShown
struct IndicatorShown_t76852832;
// LoadingIndicator/IndicatorHidden
struct IndicatorHidden_t1529613817;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingIndicator
struct  LoadingIndicator_t3396405409  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean LoadingIndicator::showingIndicator
	bool ___showingIndicator_2;
	// LoadingIndicator/IndicatorShown LoadingIndicator::onIndicatorShown
	IndicatorShown_t76852832 * ___onIndicatorShown_3;
	// LoadingIndicator/IndicatorHidden LoadingIndicator::onIndicatorHidden
	IndicatorHidden_t1529613817 * ___onIndicatorHidden_4;

public:
	inline static int32_t get_offset_of_showingIndicator_2() { return static_cast<int32_t>(offsetof(LoadingIndicator_t3396405409, ___showingIndicator_2)); }
	inline bool get_showingIndicator_2() const { return ___showingIndicator_2; }
	inline bool* get_address_of_showingIndicator_2() { return &___showingIndicator_2; }
	inline void set_showingIndicator_2(bool value)
	{
		___showingIndicator_2 = value;
	}

	inline static int32_t get_offset_of_onIndicatorShown_3() { return static_cast<int32_t>(offsetof(LoadingIndicator_t3396405409, ___onIndicatorShown_3)); }
	inline IndicatorShown_t76852832 * get_onIndicatorShown_3() const { return ___onIndicatorShown_3; }
	inline IndicatorShown_t76852832 ** get_address_of_onIndicatorShown_3() { return &___onIndicatorShown_3; }
	inline void set_onIndicatorShown_3(IndicatorShown_t76852832 * value)
	{
		___onIndicatorShown_3 = value;
		Il2CppCodeGenWriteBarrier(&___onIndicatorShown_3, value);
	}

	inline static int32_t get_offset_of_onIndicatorHidden_4() { return static_cast<int32_t>(offsetof(LoadingIndicator_t3396405409, ___onIndicatorHidden_4)); }
	inline IndicatorHidden_t1529613817 * get_onIndicatorHidden_4() const { return ___onIndicatorHidden_4; }
	inline IndicatorHidden_t1529613817 ** get_address_of_onIndicatorHidden_4() { return &___onIndicatorHidden_4; }
	inline void set_onIndicatorHidden_4(IndicatorHidden_t1529613817 * value)
	{
		___onIndicatorHidden_4 = value;
		Il2CppCodeGenWriteBarrier(&___onIndicatorHidden_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
