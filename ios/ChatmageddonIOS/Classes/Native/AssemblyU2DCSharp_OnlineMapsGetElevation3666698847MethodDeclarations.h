﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsGetElevation
struct OnlineMapsGetElevation_t3666698847;
// System.String
struct String_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// OnlineMapsGoogleAPIQuery
struct OnlineMapsGoogleAPIQuery_t356009153;
// OnlineMapsGetElevationResult[]
struct OnlineMapsGetElevationResultU5BU5D_t4025479291;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"

// System.Void OnlineMapsGetElevation::.ctor(UnityEngine.Vector2,System.String,System.String,System.String)
extern "C"  void OnlineMapsGetElevation__ctor_m64905450 (OnlineMapsGetElevation_t3666698847 * __this, Vector2_t2243707579  ___location0, String_t* ___key1, String_t* ___client2, String_t* ___signature3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsGetElevation::.ctor(UnityEngine.Vector2[],System.String,System.String,System.String)
extern "C"  void OnlineMapsGetElevation__ctor_m3766317190 (OnlineMapsGetElevation_t3666698847 * __this, Vector2U5BU5D_t686124026* ___locations0, String_t* ___key1, String_t* ___client2, String_t* ___signature3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsGetElevation::.ctor(UnityEngine.Vector2[],System.Int32,System.String,System.String,System.String)
extern "C"  void OnlineMapsGetElevation__ctor_m3660160331 (OnlineMapsGetElevation_t3666698847 * __this, Vector2U5BU5D_t686124026* ___path0, int32_t ___samples1, String_t* ___key2, String_t* ___client3, String_t* ___signature4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsGetElevation::get_type()
extern "C"  int32_t OnlineMapsGetElevation_get_type_m1795908596 (OnlineMapsGetElevation_t3666698847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsGetElevation::Find(UnityEngine.Vector2,System.String,System.String,System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsGetElevation_Find_m2789622889 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___location0, String_t* ___key1, String_t* ___client2, String_t* ___signature3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsGetElevation::Find(UnityEngine.Vector2[],System.String,System.String,System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsGetElevation_Find_m2460284643 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___locations0, String_t* ___key1, String_t* ___client2, String_t* ___signature3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsGetElevation::Find(UnityEngine.Vector2[],System.Int32,System.String,System.String,System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsGetElevation_Find_m3821835742 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___path0, int32_t ___samples1, String_t* ___key2, String_t* ___client3, String_t* ___signature4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGetElevationResult[] OnlineMapsGetElevation::GetResults(System.String)
extern "C"  OnlineMapsGetElevationResultU5BU5D_t4025479291* OnlineMapsGetElevation_GetResults_m1757848969 (Il2CppObject * __this /* static, unused */, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
