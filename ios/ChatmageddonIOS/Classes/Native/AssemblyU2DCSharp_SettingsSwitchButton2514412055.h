﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// TweenColor
struct TweenColor_t3390486518;

#include "AssemblyU2DCSharp_BooleanSwitchButton1408929844.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsSwitchButton
struct  SettingsSwitchButton_t2514412055  : public BooleanSwitchButton_t1408929844
{
public:
	// TweenPosition SettingsSwitchButton::switchTween
	TweenPosition_t1144714832 * ___switchTween_4;
	// TweenColor SettingsSwitchButton::backgroundColourTween
	TweenColor_t3390486518 * ___backgroundColourTween_5;
	// System.Boolean SettingsSwitchButton::switchStartState
	bool ___switchStartState_6;
	// UnityEngine.Vector3 SettingsSwitchButton::falsePos
	Vector3_t2243707580  ___falsePos_7;
	// UnityEngine.Vector3 SettingsSwitchButton::truePos
	Vector3_t2243707580  ___truePos_8;
	// UnityEngine.Color SettingsSwitchButton::backgroundInactiveColour
	Color_t2020392075  ___backgroundInactiveColour_9;
	// UnityEngine.Color SettingsSwitchButton::backgroundActiveColour
	Color_t2020392075  ___backgroundActiveColour_10;
	// System.Boolean SettingsSwitchButton::startSet
	bool ___startSet_11;

public:
	inline static int32_t get_offset_of_switchTween_4() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___switchTween_4)); }
	inline TweenPosition_t1144714832 * get_switchTween_4() const { return ___switchTween_4; }
	inline TweenPosition_t1144714832 ** get_address_of_switchTween_4() { return &___switchTween_4; }
	inline void set_switchTween_4(TweenPosition_t1144714832 * value)
	{
		___switchTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___switchTween_4, value);
	}

	inline static int32_t get_offset_of_backgroundColourTween_5() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___backgroundColourTween_5)); }
	inline TweenColor_t3390486518 * get_backgroundColourTween_5() const { return ___backgroundColourTween_5; }
	inline TweenColor_t3390486518 ** get_address_of_backgroundColourTween_5() { return &___backgroundColourTween_5; }
	inline void set_backgroundColourTween_5(TweenColor_t3390486518 * value)
	{
		___backgroundColourTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundColourTween_5, value);
	}

	inline static int32_t get_offset_of_switchStartState_6() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___switchStartState_6)); }
	inline bool get_switchStartState_6() const { return ___switchStartState_6; }
	inline bool* get_address_of_switchStartState_6() { return &___switchStartState_6; }
	inline void set_switchStartState_6(bool value)
	{
		___switchStartState_6 = value;
	}

	inline static int32_t get_offset_of_falsePos_7() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___falsePos_7)); }
	inline Vector3_t2243707580  get_falsePos_7() const { return ___falsePos_7; }
	inline Vector3_t2243707580 * get_address_of_falsePos_7() { return &___falsePos_7; }
	inline void set_falsePos_7(Vector3_t2243707580  value)
	{
		___falsePos_7 = value;
	}

	inline static int32_t get_offset_of_truePos_8() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___truePos_8)); }
	inline Vector3_t2243707580  get_truePos_8() const { return ___truePos_8; }
	inline Vector3_t2243707580 * get_address_of_truePos_8() { return &___truePos_8; }
	inline void set_truePos_8(Vector3_t2243707580  value)
	{
		___truePos_8 = value;
	}

	inline static int32_t get_offset_of_backgroundInactiveColour_9() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___backgroundInactiveColour_9)); }
	inline Color_t2020392075  get_backgroundInactiveColour_9() const { return ___backgroundInactiveColour_9; }
	inline Color_t2020392075 * get_address_of_backgroundInactiveColour_9() { return &___backgroundInactiveColour_9; }
	inline void set_backgroundInactiveColour_9(Color_t2020392075  value)
	{
		___backgroundInactiveColour_9 = value;
	}

	inline static int32_t get_offset_of_backgroundActiveColour_10() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___backgroundActiveColour_10)); }
	inline Color_t2020392075  get_backgroundActiveColour_10() const { return ___backgroundActiveColour_10; }
	inline Color_t2020392075 * get_address_of_backgroundActiveColour_10() { return &___backgroundActiveColour_10; }
	inline void set_backgroundActiveColour_10(Color_t2020392075  value)
	{
		___backgroundActiveColour_10 = value;
	}

	inline static int32_t get_offset_of_startSet_11() { return static_cast<int32_t>(offsetof(SettingsSwitchButton_t2514412055, ___startSet_11)); }
	inline bool get_startSet_11() const { return ___startSet_11; }
	inline bool* get_address_of_startSet_11() { return &___startSet_11; }
	inline void set_startSet_11(bool value)
	{
		___startSet_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
