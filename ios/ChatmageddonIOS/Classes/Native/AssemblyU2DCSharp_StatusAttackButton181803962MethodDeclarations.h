﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusAttackButton
struct StatusAttackButton_t181803962;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusAttackButton::.ctor()
extern "C"  void StatusAttackButton__ctor_m3967631469 (StatusAttackButton_t181803962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusAttackButton::OnClick()
extern "C"  void StatusAttackButton_OnClick_m1260186510 (StatusAttackButton_t181803962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
