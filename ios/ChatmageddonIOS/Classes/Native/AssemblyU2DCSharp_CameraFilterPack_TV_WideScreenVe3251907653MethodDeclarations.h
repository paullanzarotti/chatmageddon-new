﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_WideScreenVertical
struct CameraFilterPack_TV_WideScreenVertical_t3251907653;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_WideScreenVertical::.ctor()
extern "C"  void CameraFilterPack_TV_WideScreenVertical__ctor_m868213636 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenVertical::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_WideScreenVertical_get_material_m53499329 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::Start()
extern "C"  void CameraFilterPack_TV_WideScreenVertical_Start_m3840309892 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_WideScreenVertical_OnRenderImage_m1411191916 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::OnValidate()
extern "C"  void CameraFilterPack_TV_WideScreenVertical_OnValidate_m3149811091 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::Update()
extern "C"  void CameraFilterPack_TV_WideScreenVertical_Update_m1832825549 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::OnDisable()
extern "C"  void CameraFilterPack_TV_WideScreenVertical_OnDisable_m1632430737 (CameraFilterPack_TV_WideScreenVertical_t3251907653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
