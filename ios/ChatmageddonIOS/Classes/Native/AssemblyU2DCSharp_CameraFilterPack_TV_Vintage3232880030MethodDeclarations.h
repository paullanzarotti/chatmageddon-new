﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Vintage
struct CameraFilterPack_TV_Vintage_t3232880030;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Vintage::.ctor()
extern "C"  void CameraFilterPack_TV_Vintage__ctor_m2515721943 (CameraFilterPack_TV_Vintage_t3232880030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Vintage::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Vintage_get_material_m2439865998 (CameraFilterPack_TV_Vintage_t3232880030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::Start()
extern "C"  void CameraFilterPack_TV_Vintage_Start_m2127944867 (CameraFilterPack_TV_Vintage_t3232880030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Vintage_OnRenderImage_m1001361715 (CameraFilterPack_TV_Vintage_t3232880030 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::OnValidate()
extern "C"  void CameraFilterPack_TV_Vintage_OnValidate_m1391022046 (CameraFilterPack_TV_Vintage_t3232880030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::Update()
extern "C"  void CameraFilterPack_TV_Vintage_Update_m114693836 (CameraFilterPack_TV_Vintage_t3232880030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::OnDisable()
extern "C"  void CameraFilterPack_TV_Vintage_OnDisable_m1547474786 (CameraFilterPack_TV_Vintage_t3232880030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
