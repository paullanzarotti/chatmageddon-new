﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIAnimation/OnAnimationDrawn
struct OnAnimationDrawn_t3207005891;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void NGUIAnimation/OnAnimationDrawn::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAnimationDrawn__ctor_m1685366844 (OnAnimationDrawn_t3207005891 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationDrawn::Invoke()
extern "C"  void OnAnimationDrawn_Invoke_m2498774502 (OnAnimationDrawn_t3207005891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NGUIAnimation/OnAnimationDrawn::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAnimationDrawn_BeginInvoke_m1986159925 (OnAnimationDrawn_t3207005891 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationDrawn::EndInvoke(System.IAsyncResult)
extern "C"  void OnAnimationDrawn_EndInvoke_m3872876502 (OnAnimationDrawn_t3207005891 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
