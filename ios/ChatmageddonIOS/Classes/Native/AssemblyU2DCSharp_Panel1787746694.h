﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// PanelContainer
struct PanelContainer_t1029301983;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Panel
struct  Panel_t1787746694  : public MonoBehaviour_t1158329972
{
public:
	// PanelType Panel::panelName
	int32_t ___panelName_2;
	// TweenPosition Panel::panelPosTween
	TweenPosition_t1144714832 * ___panelPosTween_3;
	// PanelContainer Panel::container
	PanelContainer_t1029301983 * ___container_4;
	// System.Boolean Panel::destructable
	bool ___destructable_5;
	// System.Boolean Panel::panelEnabled
	bool ___panelEnabled_6;
	// System.Boolean Panel::panelOpening
	bool ___panelOpening_7;
	// System.Boolean Panel::panelClosing
	bool ___panelClosing_8;
	// System.Boolean Panel::allowsBottomUI
	bool ___allowsBottomUI_9;
	// System.Boolean Panel::allowsTopTabs
	bool ___allowsTopTabs_10;
	// System.Boolean Panel::allowsTopUI
	bool ___allowsTopUI_11;
	// System.Boolean Panel::delayBottomUI
	bool ___delayBottomUI_12;

public:
	inline static int32_t get_offset_of_panelName_2() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___panelName_2)); }
	inline int32_t get_panelName_2() const { return ___panelName_2; }
	inline int32_t* get_address_of_panelName_2() { return &___panelName_2; }
	inline void set_panelName_2(int32_t value)
	{
		___panelName_2 = value;
	}

	inline static int32_t get_offset_of_panelPosTween_3() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___panelPosTween_3)); }
	inline TweenPosition_t1144714832 * get_panelPosTween_3() const { return ___panelPosTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_panelPosTween_3() { return &___panelPosTween_3; }
	inline void set_panelPosTween_3(TweenPosition_t1144714832 * value)
	{
		___panelPosTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___panelPosTween_3, value);
	}

	inline static int32_t get_offset_of_container_4() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___container_4)); }
	inline PanelContainer_t1029301983 * get_container_4() const { return ___container_4; }
	inline PanelContainer_t1029301983 ** get_address_of_container_4() { return &___container_4; }
	inline void set_container_4(PanelContainer_t1029301983 * value)
	{
		___container_4 = value;
		Il2CppCodeGenWriteBarrier(&___container_4, value);
	}

	inline static int32_t get_offset_of_destructable_5() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___destructable_5)); }
	inline bool get_destructable_5() const { return ___destructable_5; }
	inline bool* get_address_of_destructable_5() { return &___destructable_5; }
	inline void set_destructable_5(bool value)
	{
		___destructable_5 = value;
	}

	inline static int32_t get_offset_of_panelEnabled_6() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___panelEnabled_6)); }
	inline bool get_panelEnabled_6() const { return ___panelEnabled_6; }
	inline bool* get_address_of_panelEnabled_6() { return &___panelEnabled_6; }
	inline void set_panelEnabled_6(bool value)
	{
		___panelEnabled_6 = value;
	}

	inline static int32_t get_offset_of_panelOpening_7() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___panelOpening_7)); }
	inline bool get_panelOpening_7() const { return ___panelOpening_7; }
	inline bool* get_address_of_panelOpening_7() { return &___panelOpening_7; }
	inline void set_panelOpening_7(bool value)
	{
		___panelOpening_7 = value;
	}

	inline static int32_t get_offset_of_panelClosing_8() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___panelClosing_8)); }
	inline bool get_panelClosing_8() const { return ___panelClosing_8; }
	inline bool* get_address_of_panelClosing_8() { return &___panelClosing_8; }
	inline void set_panelClosing_8(bool value)
	{
		___panelClosing_8 = value;
	}

	inline static int32_t get_offset_of_allowsBottomUI_9() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___allowsBottomUI_9)); }
	inline bool get_allowsBottomUI_9() const { return ___allowsBottomUI_9; }
	inline bool* get_address_of_allowsBottomUI_9() { return &___allowsBottomUI_9; }
	inline void set_allowsBottomUI_9(bool value)
	{
		___allowsBottomUI_9 = value;
	}

	inline static int32_t get_offset_of_allowsTopTabs_10() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___allowsTopTabs_10)); }
	inline bool get_allowsTopTabs_10() const { return ___allowsTopTabs_10; }
	inline bool* get_address_of_allowsTopTabs_10() { return &___allowsTopTabs_10; }
	inline void set_allowsTopTabs_10(bool value)
	{
		___allowsTopTabs_10 = value;
	}

	inline static int32_t get_offset_of_allowsTopUI_11() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___allowsTopUI_11)); }
	inline bool get_allowsTopUI_11() const { return ___allowsTopUI_11; }
	inline bool* get_address_of_allowsTopUI_11() { return &___allowsTopUI_11; }
	inline void set_allowsTopUI_11(bool value)
	{
		___allowsTopUI_11 = value;
	}

	inline static int32_t get_offset_of_delayBottomUI_12() { return static_cast<int32_t>(offsetof(Panel_t1787746694, ___delayBottomUI_12)); }
	inline bool get_delayBottomUI_12() const { return ___delayBottomUI_12; }
	inline bool* get_address_of_delayBottomUI_12() { return &___delayBottomUI_12; }
	inline void set_delayBottomUI_12(bool value)
	{
		___delayBottomUI_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
