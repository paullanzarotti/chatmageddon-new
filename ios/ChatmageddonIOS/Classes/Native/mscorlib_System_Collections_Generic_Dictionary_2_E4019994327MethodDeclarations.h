﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m718962676(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4019994327 *, Dictionary_2_t2699969625 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3186720819(__this, method) ((  Il2CppObject * (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1445454647(__this, method) ((  void (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3002220234(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1642843433(__this, method) ((  Il2CppObject * (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m155287545(__this, method) ((  Il2CppObject * (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::MoveNext()
#define Enumerator_MoveNext_m1816511379(__this, method) ((  bool (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::get_Current()
#define Enumerator_get_Current_m2528706027(__this, method) ((  KeyValuePair_2_t457314847  (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2278021236(__this, method) ((  String_t* (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4007788212(__this, method) ((  XName_t785190363 * (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::Reset()
#define Enumerator_Reset_m1055572398(__this, method) ((  void (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::VerifyState()
#define Enumerator_VerifyState_m1814020813(__this, method) ((  void (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2403055439(__this, method) ((  void (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XName>::Dispose()
#define Enumerator_Dispose_m1845340600(__this, method) ((  void (*) (Enumerator_t4019994327 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
