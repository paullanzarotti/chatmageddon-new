﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackInventoryNavScreen
struct AttackInventoryNavScreen_t33214553;

#include "AssemblyU2DCSharp_RotateButton2565449149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileInfoButton
struct  MissileInfoButton_t2682635900  : public RotateButton_t2565449149
{
public:
	// AttackInventoryNavScreen MissileInfoButton::screen
	AttackInventoryNavScreen_t33214553 * ___screen_10;

public:
	inline static int32_t get_offset_of_screen_10() { return static_cast<int32_t>(offsetof(MissileInfoButton_t2682635900, ___screen_10)); }
	inline AttackInventoryNavScreen_t33214553 * get_screen_10() const { return ___screen_10; }
	inline AttackInventoryNavScreen_t33214553 ** get_address_of_screen_10() { return &___screen_10; }
	inline void set_screen_10(AttackInventoryNavScreen_t33214553 * value)
	{
		___screen_10 = value;
		Il2CppCodeGenWriteBarrier(&___screen_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
