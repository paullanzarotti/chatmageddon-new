﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Uniject.Impl.UnityLogger
struct UnityLogger_t296816683;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Uniject.Impl.UnityLogger::.ctor()
extern "C"  void UnityLogger__ctor_m2918117276 (UnityLogger_t296816683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLogger::LogWarning(System.String,System.Object[])
extern "C"  void UnityLogger_LogWarning_m251417636 (UnityLogger_t296816683 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___formatArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Uniject.Impl.UnityLogger::get_prefix()
extern "C"  String_t* UnityLogger_get_prefix_m3258551066 (UnityLogger_t296816683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLogger::set_prefix(System.String)
extern "C"  void UnityLogger_set_prefix_m2395088035 (UnityLogger_t296816683 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLogger::Log(System.String)
extern "C"  void UnityLogger_Log_m119978404 (UnityLogger_t296816683 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLogger::Log(System.String,System.Object[])
extern "C"  void UnityLogger_Log_m3420201834 (UnityLogger_t296816683 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLogger::LogError(System.String,System.Object[])
extern "C"  void UnityLogger_LogError_m158610690 (UnityLogger_t296816683 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___formatArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Uniject.Impl.UnityLogger::safeFormat(System.String,System.Object[])
extern "C"  String_t* UnityLogger_safeFormat_m3472222149 (UnityLogger_t296816683 * __this, String_t* ___message0, ObjectU5BU5D_t3614634134* ___formatArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Uniject.Impl.UnityLogger::formatMessageWithPrefix(System.String)
extern "C"  String_t* UnityLogger_formatMessageWithPrefix_m2475218961 (UnityLogger_t296816683 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
