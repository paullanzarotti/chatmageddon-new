﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<LayMine>c__AnonStorey2A
struct U3CLayMineU3Ec__AnonStorey2A_t2968755004;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<LayMine>c__AnonStorey2A::.ctor()
extern "C"  void U3CLayMineU3Ec__AnonStorey2A__ctor_m1469134433 (U3CLayMineU3Ec__AnonStorey2A_t2968755004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<LayMine>c__AnonStorey2A::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLayMineU3Ec__AnonStorey2A_U3CU3Em__0_m4258974544 (U3CLayMineU3Ec__AnonStorey2A_t2968755004 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
