﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdatePasswordUIScaler
struct UpdatePasswordUIScaler_t4097640474;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdatePasswordUIScaler::.ctor()
extern "C"  void UpdatePasswordUIScaler__ctor_m3075515183 (UpdatePasswordUIScaler_t4097640474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdatePasswordUIScaler::GlobalUIScale()
extern "C"  void UpdatePasswordUIScaler_GlobalUIScale_m540768084 (UpdatePasswordUIScaler_t4097640474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
