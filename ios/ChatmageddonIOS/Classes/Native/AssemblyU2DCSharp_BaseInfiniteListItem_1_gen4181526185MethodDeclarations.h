﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<AttackListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m3939061399(__this, method) ((  void (*) (BaseInfiniteListItem_1_t4181526185 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<AttackListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m3785827466(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t4181526185 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<AttackListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m4183277448(__this, method) ((  void (*) (BaseInfiniteListItem_1_t4181526185 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<AttackListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m1143016297(__this, method) ((  void (*) (BaseInfiniteListItem_1_t4181526185 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<AttackListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m3432028610(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t4181526185 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<AttackListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m2415867543(__this, method) ((  void (*) (BaseInfiniteListItem_1_t4181526185 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<AttackListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m1724505538(__this, method) ((  void (*) (BaseInfiniteListItem_1_t4181526185 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
