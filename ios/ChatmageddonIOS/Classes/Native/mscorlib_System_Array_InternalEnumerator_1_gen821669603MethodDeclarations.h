﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen821669603.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"

// System.Void System.Array/InternalEnumerator`1<AttackSearchNav>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3531111938_gshared (InternalEnumerator_1_t821669603 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3531111938(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t821669603 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3531111938_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AttackSearchNav>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1786218918_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1786218918(__this, method) ((  void (*) (InternalEnumerator_1_t821669603 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1786218918_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AttackSearchNav>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1808662040_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1808662040(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t821669603 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1808662040_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AttackSearchNav>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m171365285_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m171365285(__this, method) ((  void (*) (InternalEnumerator_1_t821669603 *, const MethodInfo*))InternalEnumerator_1_Dispose_m171365285_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AttackSearchNav>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1374684162_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1374684162(__this, method) ((  bool (*) (InternalEnumerator_1_t821669603 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1374684162_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AttackSearchNav>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1184022829_gshared (InternalEnumerator_1_t821669603 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1184022829(__this, method) ((  int32_t (*) (InternalEnumerator_1_t821669603 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1184022829_gshared)(__this, method)
