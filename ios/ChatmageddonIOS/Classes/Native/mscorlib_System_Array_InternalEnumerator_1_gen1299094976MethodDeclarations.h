﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Vectrosity.VectorPoints>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3381299113(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1299094976 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Vectrosity.VectorPoints>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2068705437(__this, method) ((  void (*) (InternalEnumerator_1_t1299094976 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vectrosity.VectorPoints>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2719975873(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1299094976 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vectrosity.VectorPoints>::Dispose()
#define InternalEnumerator_1_Dispose_m2806240700(__this, method) ((  void (*) (InternalEnumerator_1_t1299094976 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vectrosity.VectorPoints>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1979864149(__this, method) ((  bool (*) (InternalEnumerator_1_t1299094976 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vectrosity.VectorPoints>::get_Current()
#define InternalEnumerator_1_get_Current_m632924932(__this, method) ((  VectorPoints_t440342714 * (*) (InternalEnumerator_1_t1299094976 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
