﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.Win8_1BillingService
struct  Win8_1BillingService_t3938749894  : public Il2CppObject
{
public:

public:
};

struct Win8_1BillingService_t3938749894_StaticFields
{
public:
	// System.Int32 Unibill.Impl.Win8_1BillingService::count
	int32_t ___count_0;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(Win8_1BillingService_t3938749894_StaticFields, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
