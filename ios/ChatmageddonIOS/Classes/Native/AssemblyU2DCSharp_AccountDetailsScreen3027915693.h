﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumberButton
struct PhoneNumberButton_t1790400683;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountDetailsScreen
struct  AccountDetailsScreen_t3027915693  : public NavigationScreen_t2333230110
{
public:
	// PhoneNumberButton AccountDetailsScreen::pnButton
	PhoneNumberButton_t1790400683 * ___pnButton_3;
	// System.Boolean AccountDetailsScreen::ADUIclosing
	bool ___ADUIclosing_4;

public:
	inline static int32_t get_offset_of_pnButton_3() { return static_cast<int32_t>(offsetof(AccountDetailsScreen_t3027915693, ___pnButton_3)); }
	inline PhoneNumberButton_t1790400683 * get_pnButton_3() const { return ___pnButton_3; }
	inline PhoneNumberButton_t1790400683 ** get_address_of_pnButton_3() { return &___pnButton_3; }
	inline void set_pnButton_3(PhoneNumberButton_t1790400683 * value)
	{
		___pnButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___pnButton_3, value);
	}

	inline static int32_t get_offset_of_ADUIclosing_4() { return static_cast<int32_t>(offsetof(AccountDetailsScreen_t3027915693, ___ADUIclosing_4)); }
	inline bool get_ADUIclosing_4() const { return ___ADUIclosing_4; }
	inline bool* get_address_of_ADUIclosing_4() { return &___ADUIclosing_4; }
	inline void set_ADUIclosing_4(bool value)
	{
		___ADUIclosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
