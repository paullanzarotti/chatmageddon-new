﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CubeRotator
struct CubeRotator_t965177170;

#include "codegen/il2cpp-codegen.h"

// System.Void CubeRotator::.ctor()
extern "C"  void CubeRotator__ctor_m3060215581 (CubeRotator_t965177170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeRotator::Start()
extern "C"  void CubeRotator_Start_m3982110981 (CubeRotator_t965177170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeRotator::Update()
extern "C"  void CubeRotator_Update_m3028921642 (CubeRotator_t965177170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeRotator::togglePauseRotation()
extern "C"  void CubeRotator_togglePauseRotation_m2372351665 (CubeRotator_t965177170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
