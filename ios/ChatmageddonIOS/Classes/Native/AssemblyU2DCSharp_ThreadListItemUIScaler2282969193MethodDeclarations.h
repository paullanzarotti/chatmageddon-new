﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThreadListItemUIScaler
struct ThreadListItemUIScaler_t2282969193;

#include "codegen/il2cpp-codegen.h"

// System.Void ThreadListItemUIScaler::.ctor()
extern "C"  void ThreadListItemUIScaler__ctor_m3028477920 (ThreadListItemUIScaler_t2282969193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThreadListItemUIScaler::GlobalUIScale()
extern "C"  void ThreadListItemUIScaler_GlobalUIScale_m2358825547 (ThreadListItemUIScaler_t2282969193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
