﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerPrefSaveData
struct PlayerPrefSaveData_t170835669;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Void PlayerPrefSaveData::.ctor()
extern "C"  void PlayerPrefSaveData__ctor_m2357420896 (PlayerPrefSaveData_t170835669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::SaveAllData()
extern "C"  void PlayerPrefSaveData_SaveAllData_m1702127556 (PlayerPrefSaveData_t170835669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::DeleteKey(System.String)
extern "C"  void PlayerPrefSaveData_DeleteKey_m3258808784 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::DeleteAllKeys()
extern "C"  void PlayerPrefSaveData_DeleteAllKeys_m2173734738 (PlayerPrefSaveData_t170835669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::SaveInt(System.String,System.Int32)
extern "C"  void PlayerPrefSaveData_SaveInt_m3290821593 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, int32_t ___dataValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayerPrefSaveData::GetSavedInt(System.String)
extern "C"  Nullable_1_t334943763  PlayerPrefSaveData_GetSavedInt_m496537477 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::SaveFloat(System.String,System.Single)
extern "C"  void PlayerPrefSaveData_SaveFloat_m2709496874 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, float ___dataValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> PlayerPrefSaveData::GetSavedFloat(System.String)
extern "C"  Nullable_1_t339576247  PlayerPrefSaveData_GetSavedFloat_m131974040 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::SaveString(System.String,System.String)
extern "C"  void PlayerPrefSaveData_SaveString_m1693394744 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, String_t* ___dataValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayerPrefSaveData::GetSavedString(System.String)
extern "C"  String_t* PlayerPrefSaveData_GetSavedString_m242975485 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefSaveData::SaveBool(System.String,System.Boolean)
extern "C"  void PlayerPrefSaveData_SaveBool_m3741535874 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, bool ___dataValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayerPrefSaveData::GetSavedBool(System.String)
extern "C"  Nullable_1_t2088641033  PlayerPrefSaveData_GetSavedBool_m3919766722 (PlayerPrefSaveData_t170835669 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
