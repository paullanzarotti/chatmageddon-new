﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_GaussianBlur
struct CameraFilterPack_Blur_GaussianBlur_t3355292571;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_GaussianBlur::.ctor()
extern "C"  void CameraFilterPack_Blur_GaussianBlur__ctor_m1790611618 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_GaussianBlur::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_GaussianBlur_get_material_m109440735 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::Start()
extern "C"  void CameraFilterPack_Blur_GaussianBlur_Start_m1833361990 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_GaussianBlur_OnRenderImage_m4125364374 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::OnValidate()
extern "C"  void CameraFilterPack_Blur_GaussianBlur_OnValidate_m1220140373 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::Update()
extern "C"  void CameraFilterPack_Blur_GaussianBlur_Update_m3397074051 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::OnDisable()
extern "C"  void CameraFilterPack_Blur_GaussianBlur_OnDisable_m2078785427 (CameraFilterPack_Blur_GaussianBlur_t3355292571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
