﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key24298183MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1491010615(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3762583499 *, Dictionary_2_t1279085728 *, const MethodInfo*))KeyCollection__ctor_m3196740589_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3253425113(__this, ___item0, method) ((  void (*) (KeyCollection_t3762583499 *, GameObject_t1756533147 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m5544995_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4096527908(__this, method) ((  void (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2473137888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3755139939(__this, ___item0, method) ((  bool (*) (KeyCollection_t3762583499 *, GameObject_t1756533147 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3123457549_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3933312440(__this, ___item0, method) ((  bool (*) (KeyCollection_t3762583499 *, GameObject_t1756533147 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3313980640_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2381000880(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m217042226_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1875918870(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3762583499 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1356632946_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1822327745(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m395225875_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4033174242(__this, method) ((  bool (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m438898550_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2027949526(__this, method) ((  bool (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3695195230_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1973802672(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2887770946_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3300330004(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3762583499 *, GameObjectU5BU5D_t3057952154*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m651244716_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2047085695(__this, method) ((  Enumerator_t3968589166  (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_GetEnumerator_m574105777_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.GameObject,UnityEngine.Vector3>::get_Count()
#define KeyCollection_get_Count_m1259228730(__this, method) ((  int32_t (*) (KeyCollection_t3762583499 *, const MethodInfo*))KeyCollection_get_Count_m2328186558_gshared)(__this, method)
