﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera
struct UICamera_t1496819779;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UICamera/MouseOrTouch
struct MouseOrTouch_t2470076277;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme3098588360.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_UICamera1496819779.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch2470076277.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry974746545.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UICamera::.ctor()
extern "C"  void UICamera__ctor_m3166615176 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_stickyPress()
extern "C"  bool UICamera_get_stickyPress_m1278227735 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_disableController()
extern "C"  bool UICamera_get_disableController_m1283740971 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_disableController(System.Boolean)
extern "C"  void UICamera_set_disableController_m103968910 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UICamera::get_lastTouchPosition()
extern "C"  Vector2_t2243707579  UICamera_get_lastTouchPosition_m3193010682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_lastTouchPosition(UnityEngine.Vector2)
extern "C"  void UICamera_set_lastTouchPosition_m1259895441 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UICamera::get_lastEventPosition()
extern "C"  Vector2_t2243707579  UICamera_get_lastEventPosition_m1181734875 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_lastEventPosition(UnityEngine.Vector2)
extern "C"  void UICamera_set_lastEventPosition_m2240507856 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ControlScheme UICamera::get_currentScheme()
extern "C"  int32_t UICamera_get_currentScheme_m1862744624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_currentScheme(UICamera/ControlScheme)
extern "C"  void UICamera_set_currentScheme_m1584585679 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.KeyCode UICamera::get_currentKey()
extern "C"  int32_t UICamera_get_currentKey_m609049387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_currentKey(UnityEngine.KeyCode)
extern "C"  void UICamera_set_currentKey_m3469196258 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UICamera::get_currentRay()
extern "C"  Ray_t2469606224  UICamera_get_currentRay_m1984465614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_inputHasFocus()
extern "C"  bool UICamera_get_inputHasFocus_m2760684483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_genericEventHandler()
extern "C"  GameObject_t1756533147 * UICamera_get_genericEventHandler_m3829906589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_genericEventHandler(UnityEngine.GameObject)
extern "C"  void UICamera_set_genericEventHandler_m1620038712 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_handlesEvents()
extern "C"  bool UICamera_get_handlesEvents_m968479517 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UICamera::get_cachedCamera()
extern "C"  Camera_t189460977 * UICamera_get_cachedCamera_m2689485703 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_tooltipObject()
extern "C"  GameObject_t1756533147 * UICamera_get_tooltipObject_m3664181168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_isOverUI()
extern "C"  bool UICamera_get_isOverUI_m3832910535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_hoveredObject()
extern "C"  GameObject_t1756533147 * UICamera_get_hoveredObject_m2557312108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_hoveredObject(UnityEngine.GameObject)
extern "C"  void UICamera_set_hoveredObject_m2682958993 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_controllerNavigationObject()
extern "C"  GameObject_t1756533147 * UICamera_get_controllerNavigationObject_m2050144259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_controllerNavigationObject(UnityEngine.GameObject)
extern "C"  void UICamera_set_controllerNavigationObject_m2090322856 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_selectedObject()
extern "C"  GameObject_t1756533147 * UICamera_get_selectedObject_m317248128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_selectedObject(UnityEngine.GameObject)
extern "C"  void UICamera_set_selectedObject_m3213041587 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsPressed(UnityEngine.GameObject)
extern "C"  bool UICamera_IsPressed_m545745070 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::get_touchCount()
extern "C"  int32_t UICamera_get_touchCount_m465207483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::CountInputSources()
extern "C"  int32_t UICamera_CountInputSources_m3814805083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::get_dragCount()
extern "C"  int32_t UICamera_get_dragCount_m68086712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UICamera::get_mainCamera()
extern "C"  Camera_t189460977 * UICamera_get_mainCamera_m2710288362 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera UICamera::get_eventHandler()
extern "C"  UICamera_t1496819779 * UICamera_get_eventHandler_m3089660681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::CompareFunc(UICamera,UICamera)
extern "C"  int32_t UICamera_CompareFunc_m1448566153 (Il2CppObject * __this /* static, unused */, UICamera_t1496819779 * ___a0, UICamera_t1496819779 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UICamera::FindRootRigidbody(UnityEngine.Transform)
extern "C"  Rigidbody_t4233889191 * UICamera_FindRootRigidbody_m3509318412 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UICamera::FindRootRigidbody2D(UnityEngine.Transform)
extern "C"  Rigidbody2D_t502193897 * UICamera_FindRootRigidbody2D_m2304416364 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Raycast(UICamera/MouseOrTouch)
extern "C"  void UICamera_Raycast_m2263165570 (Il2CppObject * __this /* static, unused */, MouseOrTouch_t2470076277 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::Raycast(UnityEngine.Vector3)
extern "C"  bool UICamera_Raycast_m4102400114 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___inPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsVisible(UnityEngine.Vector3,UnityEngine.GameObject)
extern "C"  bool UICamera_IsVisible_m1078905463 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___worldPoint0, GameObject_t1756533147 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsVisible(UICamera/DepthEntry&)
extern "C"  bool UICamera_IsVisible_m3021451623 (Il2CppObject * __this /* static, unused */, DepthEntry_t974746545 * ___de0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsHighlighted(UnityEngine.GameObject)
extern "C"  bool UICamera_IsHighlighted_m1075874621 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera UICamera::FindCameraForLayer(System.Int32)
extern "C"  UICamera_t1496819779 * UICamera_FindCameraForLayer_m141587329 (Il2CppObject * __this /* static, unused */, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::GetDirection(UnityEngine.KeyCode,UnityEngine.KeyCode)
extern "C"  int32_t UICamera_GetDirection_m130592707 (Il2CppObject * __this /* static, unused */, int32_t ___up0, int32_t ___down1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::GetDirection(UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode)
extern "C"  int32_t UICamera_GetDirection_m2195203651 (Il2CppObject * __this /* static, unused */, int32_t ___up00, int32_t ___up11, int32_t ___down02, int32_t ___down13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::GetDirection(System.String)
extern "C"  int32_t UICamera_GetDirection_m1910630809 (Il2CppObject * __this /* static, unused */, String_t* ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Notify(UnityEngine.GameObject,System.String,System.Object)
extern "C"  void UICamera_Notify_m2858345893 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___funcName1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MouseOrTouch UICamera::GetMouse(System.Int32)
extern "C"  MouseOrTouch_t2470076277 * UICamera_GetMouse_m2026747984 (Il2CppObject * __this /* static, unused */, int32_t ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MouseOrTouch UICamera::GetTouch(System.Int32,System.Boolean)
extern "C"  MouseOrTouch_t2470076277 * UICamera_GetTouch_m2299483007 (Il2CppObject * __this /* static, unused */, int32_t ___id0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::RemoveTouch(System.Int32)
extern "C"  void UICamera_RemoveTouch_m2691586914 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Awake()
extern "C"  void UICamera_Awake_m3800014275 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::OnEnable()
extern "C"  void UICamera_OnEnable_m2736726468 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::OnDisable()
extern "C"  void UICamera_OnDisable_m1171936983 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Start()
extern "C"  void UICamera_Start_m1649977700 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Update()
extern "C"  void UICamera_Update_m927241191 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::LateUpdate()
extern "C"  void UICamera_LateUpdate_m1662225475 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessMouse()
extern "C"  void UICamera_ProcessMouse_m936784574 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessTouches()
extern "C"  void UICamera_ProcessTouches_m449364206 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessFakeTouches()
extern "C"  void UICamera_ProcessFakeTouches_m4201770379 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessOthers()
extern "C"  void UICamera_ProcessOthers_m2431473538 (UICamera_t1496819779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessPress(System.Boolean,System.Single,System.Single)
extern "C"  void UICamera_ProcessPress_m3513285001 (UICamera_t1496819779 * __this, bool ___pressed0, float ___click1, float ___drag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessRelease(System.Boolean,System.Single)
extern "C"  void UICamera_ProcessRelease_m3839209750 (UICamera_t1496819779 * __this, bool ___isMouse0, float ___drag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::HasCollider(UnityEngine.GameObject)
extern "C"  bool UICamera_HasCollider_m3057986356 (UICamera_t1496819779 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessTouch(System.Boolean,System.Boolean)
extern "C"  void UICamera_ProcessTouch_m203963946 (UICamera_t1496819779 * __this, bool ___pressed0, bool ___released1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ShowTooltip(UnityEngine.GameObject)
extern "C"  bool UICamera_ShowTooltip_m2339531924 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::HideTooltip()
extern "C"  bool UICamera_HideTooltip_m3463923695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::.cctor()
extern "C"  void UICamera__cctor_m2426361557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::<Raycast>m__0(UICamera/DepthEntry,UICamera/DepthEntry)
extern "C"  int32_t UICamera_U3CRaycastU3Em__0_m3675673388 (Il2CppObject * __this /* static, unused */, DepthEntry_t974746545  ___r10, DepthEntry_t974746545  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::<Raycast>m__1(UICamera/DepthEntry,UICamera/DepthEntry)
extern "C"  int32_t UICamera_U3CRaycastU3Em__1_m1601296785 (Il2CppObject * __this /* static, unused */, DepthEntry_t974746545  ___r10, DepthEntry_t974746545  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
