﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t2148616552;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::.ctor(System.String,System.Byte[],System.Int32,System.Boolean)
extern "C"  void ZipAESTransform__ctor_m1528222691 (ZipAESTransform_t2148616552 * __this, String_t* ___key0, ByteU5BU5D_t3397334013* ___saltBytes1, int32_t ___blockSize2, bool ___writeMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t ZipAESTransform_TransformBlock_m1966603526 (ZipAESTransform_t2148616552 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t3397334013* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_PwdVerifier()
extern "C"  ByteU5BU5D_t3397334013* ZipAESTransform_get_PwdVerifier_m1365083820 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::GetAuthCode()
extern "C"  ByteU5BU5D_t3397334013* ZipAESTransform_GetAuthCode_m771081471 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* ZipAESTransform_TransformFinalBlock_m704909432 (ZipAESTransform_t2148616552 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_InputBlockSize()
extern "C"  int32_t ZipAESTransform_get_InputBlockSize_m3304664717 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_OutputBlockSize()
extern "C"  int32_t ZipAESTransform_get_OutputBlockSize_m3614661226 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanTransformMultipleBlocks()
extern "C"  bool ZipAESTransform_get_CanTransformMultipleBlocks_m338158751 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanReuseTransform()
extern "C"  bool ZipAESTransform_get_CanReuseTransform_m3504446733 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::Dispose()
extern "C"  void ZipAESTransform_Dispose_m2147833679 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
