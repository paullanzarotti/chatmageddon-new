﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XContainer/<Descendants>c__Iterator1C
struct U3CDescendantsU3Ec__Iterator1C_t2337692641;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement>
struct IEnumerator_1_t2324312173;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::.ctor()
extern "C"  void U3CDescendantsU3Ec__Iterator1C__ctor_m2480433788 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XElement System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::System.Collections.Generic.IEnumerator<System.Xml.Linq.XElement>.get_Current()
extern "C"  XElement_t553821050 * U3CDescendantsU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_m917009115 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDescendantsU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m507433462 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CDescendantsU3Ec__Iterator1C_System_Collections_IEnumerable_GetEnumerator_m2519966259 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CDescendantsU3Ec__Iterator1C_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m650650952 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::MoveNext()
extern "C"  bool U3CDescendantsU3Ec__Iterator1C_MoveNext_m2292210084 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::Dispose()
extern "C"  void U3CDescendantsU3Ec__Iterator1C_Dispose_m3069904629 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Descendants>c__Iterator1C::Reset()
extern "C"  void U3CDescendantsU3Ec__Iterator1C_Reset_m3141916199 (U3CDescendantsU3Ec__Iterator1C_t2337692641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
