﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisplayLocationSwitch
struct DisplayLocationSwitch_t413816511;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DisplayLocationSwitch::.ctor()
extern "C"  void DisplayLocationSwitch__ctor_m1037005920 (DisplayLocationSwitch_t413816511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisplayLocationSwitch::OnSwitch(System.Boolean)
extern "C"  void DisplayLocationSwitch_OnSwitch_m3041850818 (DisplayLocationSwitch_t413816511 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisplayLocationSwitch::<OnSwitch>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void DisplayLocationSwitch_U3COnSwitchU3Em__0_m3721988699 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
