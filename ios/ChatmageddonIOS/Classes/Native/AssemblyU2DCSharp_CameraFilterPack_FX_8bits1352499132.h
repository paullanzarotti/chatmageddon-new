﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_FX_8bits
struct  CameraFilterPack_FX_8bits_t1352499132  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_FX_8bits::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_8bits::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_FX_8bits::SCMaterial
	Material_t193706927 * ___SCMaterial_4;
	// System.Single CameraFilterPack_FX_8bits::Brightness
	float ___Brightness_5;
	// System.Int32 CameraFilterPack_FX_8bits::ResolutionX
	int32_t ___ResolutionX_6;
	// System.Int32 CameraFilterPack_FX_8bits::ResolutionY
	int32_t ___ResolutionY_7;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132, ___SCMaterial_4)); }
	inline Material_t193706927 * get_SCMaterial_4() const { return ___SCMaterial_4; }
	inline Material_t193706927 ** get_address_of_SCMaterial_4() { return &___SCMaterial_4; }
	inline void set_SCMaterial_4(Material_t193706927 * value)
	{
		___SCMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_4, value);
	}

	inline static int32_t get_offset_of_Brightness_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132, ___Brightness_5)); }
	inline float get_Brightness_5() const { return ___Brightness_5; }
	inline float* get_address_of_Brightness_5() { return &___Brightness_5; }
	inline void set_Brightness_5(float value)
	{
		___Brightness_5 = value;
	}

	inline static int32_t get_offset_of_ResolutionX_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132, ___ResolutionX_6)); }
	inline int32_t get_ResolutionX_6() const { return ___ResolutionX_6; }
	inline int32_t* get_address_of_ResolutionX_6() { return &___ResolutionX_6; }
	inline void set_ResolutionX_6(int32_t value)
	{
		___ResolutionX_6 = value;
	}

	inline static int32_t get_offset_of_ResolutionY_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132, ___ResolutionY_7)); }
	inline int32_t get_ResolutionY_7() const { return ___ResolutionY_7; }
	inline int32_t* get_address_of_ResolutionY_7() { return &___ResolutionY_7; }
	inline void set_ResolutionY_7(int32_t value)
	{
		___ResolutionY_7 = value;
	}
};

struct CameraFilterPack_FX_8bits_t1352499132_StaticFields
{
public:
	// System.Single CameraFilterPack_FX_8bits::ChangeBrightness
	float ___ChangeBrightness_8;
	// System.Int32 CameraFilterPack_FX_8bits::ChangeResolutionX
	int32_t ___ChangeResolutionX_9;
	// System.Int32 CameraFilterPack_FX_8bits::ChangeResolutionY
	int32_t ___ChangeResolutionY_10;

public:
	inline static int32_t get_offset_of_ChangeBrightness_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132_StaticFields, ___ChangeBrightness_8)); }
	inline float get_ChangeBrightness_8() const { return ___ChangeBrightness_8; }
	inline float* get_address_of_ChangeBrightness_8() { return &___ChangeBrightness_8; }
	inline void set_ChangeBrightness_8(float value)
	{
		___ChangeBrightness_8 = value;
	}

	inline static int32_t get_offset_of_ChangeResolutionX_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132_StaticFields, ___ChangeResolutionX_9)); }
	inline int32_t get_ChangeResolutionX_9() const { return ___ChangeResolutionX_9; }
	inline int32_t* get_address_of_ChangeResolutionX_9() { return &___ChangeResolutionX_9; }
	inline void set_ChangeResolutionX_9(int32_t value)
	{
		___ChangeResolutionX_9 = value;
	}

	inline static int32_t get_offset_of_ChangeResolutionY_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_t1352499132_StaticFields, ___ChangeResolutionY_10)); }
	inline int32_t get_ChangeResolutionY_10() const { return ___ChangeResolutionY_10; }
	inline int32_t* get_address_of_ChangeResolutionY_10() { return &___ChangeResolutionY_10; }
	inline void set_ChangeResolutionY_10(int32_t value)
	{
		___ChangeResolutionY_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
