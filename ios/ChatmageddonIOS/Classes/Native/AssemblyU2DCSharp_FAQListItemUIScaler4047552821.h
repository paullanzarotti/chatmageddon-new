﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FAQListItemUIScaler
struct  FAQListItemUIScaler_t4047552821  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.BoxCollider FAQListItemUIScaler::itemCollider
	BoxCollider_t22920061 * ___itemCollider_14;
	// UISprite FAQListItemUIScaler::background
	UISprite_t603616735 * ___background_15;
	// UnityEngine.Transform FAQListItemUIScaler::leftTransform
	Transform_t3275118058 * ___leftTransform_16;
	// UnityEngine.Transform FAQListItemUIScaler::rightTransform
	Transform_t3275118058 * ___rightTransform_17;
	// UISprite FAQListItemUIScaler::divider
	UISprite_t603616735 * ___divider_18;

public:
	inline static int32_t get_offset_of_itemCollider_14() { return static_cast<int32_t>(offsetof(FAQListItemUIScaler_t4047552821, ___itemCollider_14)); }
	inline BoxCollider_t22920061 * get_itemCollider_14() const { return ___itemCollider_14; }
	inline BoxCollider_t22920061 ** get_address_of_itemCollider_14() { return &___itemCollider_14; }
	inline void set_itemCollider_14(BoxCollider_t22920061 * value)
	{
		___itemCollider_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemCollider_14, value);
	}

	inline static int32_t get_offset_of_background_15() { return static_cast<int32_t>(offsetof(FAQListItemUIScaler_t4047552821, ___background_15)); }
	inline UISprite_t603616735 * get_background_15() const { return ___background_15; }
	inline UISprite_t603616735 ** get_address_of_background_15() { return &___background_15; }
	inline void set_background_15(UISprite_t603616735 * value)
	{
		___background_15 = value;
		Il2CppCodeGenWriteBarrier(&___background_15, value);
	}

	inline static int32_t get_offset_of_leftTransform_16() { return static_cast<int32_t>(offsetof(FAQListItemUIScaler_t4047552821, ___leftTransform_16)); }
	inline Transform_t3275118058 * get_leftTransform_16() const { return ___leftTransform_16; }
	inline Transform_t3275118058 ** get_address_of_leftTransform_16() { return &___leftTransform_16; }
	inline void set_leftTransform_16(Transform_t3275118058 * value)
	{
		___leftTransform_16 = value;
		Il2CppCodeGenWriteBarrier(&___leftTransform_16, value);
	}

	inline static int32_t get_offset_of_rightTransform_17() { return static_cast<int32_t>(offsetof(FAQListItemUIScaler_t4047552821, ___rightTransform_17)); }
	inline Transform_t3275118058 * get_rightTransform_17() const { return ___rightTransform_17; }
	inline Transform_t3275118058 ** get_address_of_rightTransform_17() { return &___rightTransform_17; }
	inline void set_rightTransform_17(Transform_t3275118058 * value)
	{
		___rightTransform_17 = value;
		Il2CppCodeGenWriteBarrier(&___rightTransform_17, value);
	}

	inline static int32_t get_offset_of_divider_18() { return static_cast<int32_t>(offsetof(FAQListItemUIScaler_t4047552821, ___divider_18)); }
	inline UISprite_t603616735 * get_divider_18() const { return ___divider_18; }
	inline UISprite_t603616735 ** get_address_of_divider_18() { return &___divider_18; }
	inline void set_divider_18(UISprite_t603616735 * value)
	{
		___divider_18 = value;
		Il2CppCodeGenWriteBarrier(&___divider_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
