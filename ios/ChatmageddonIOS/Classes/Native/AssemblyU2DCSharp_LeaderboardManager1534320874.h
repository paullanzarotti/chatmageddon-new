﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<User>
struct List_1_t89046591;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1284986594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardManager
struct  LeaderboardManager_t1534320874  : public MonoSingleton_1_t1284986594
{
public:
	// System.Int32 LeaderboardManager::pageDataCount
	int32_t ___pageDataCount_3;
	// System.Collections.Generic.List`1<User> LeaderboardManager::dailyWorldLeaderboard
	List_1_t89046591 * ___dailyWorldLeaderboard_4;
	// System.Int32 LeaderboardManager::playerDailyRank
	int32_t ___playerDailyRank_5;
	// System.Collections.Generic.List`1<User> LeaderboardManager::allTimeWorldLeaderboard
	List_1_t89046591 * ___allTimeWorldLeaderboard_6;
	// System.Int32 LeaderboardManager::playerAllTimeRank
	int32_t ___playerAllTimeRank_7;

public:
	inline static int32_t get_offset_of_pageDataCount_3() { return static_cast<int32_t>(offsetof(LeaderboardManager_t1534320874, ___pageDataCount_3)); }
	inline int32_t get_pageDataCount_3() const { return ___pageDataCount_3; }
	inline int32_t* get_address_of_pageDataCount_3() { return &___pageDataCount_3; }
	inline void set_pageDataCount_3(int32_t value)
	{
		___pageDataCount_3 = value;
	}

	inline static int32_t get_offset_of_dailyWorldLeaderboard_4() { return static_cast<int32_t>(offsetof(LeaderboardManager_t1534320874, ___dailyWorldLeaderboard_4)); }
	inline List_1_t89046591 * get_dailyWorldLeaderboard_4() const { return ___dailyWorldLeaderboard_4; }
	inline List_1_t89046591 ** get_address_of_dailyWorldLeaderboard_4() { return &___dailyWorldLeaderboard_4; }
	inline void set_dailyWorldLeaderboard_4(List_1_t89046591 * value)
	{
		___dailyWorldLeaderboard_4 = value;
		Il2CppCodeGenWriteBarrier(&___dailyWorldLeaderboard_4, value);
	}

	inline static int32_t get_offset_of_playerDailyRank_5() { return static_cast<int32_t>(offsetof(LeaderboardManager_t1534320874, ___playerDailyRank_5)); }
	inline int32_t get_playerDailyRank_5() const { return ___playerDailyRank_5; }
	inline int32_t* get_address_of_playerDailyRank_5() { return &___playerDailyRank_5; }
	inline void set_playerDailyRank_5(int32_t value)
	{
		___playerDailyRank_5 = value;
	}

	inline static int32_t get_offset_of_allTimeWorldLeaderboard_6() { return static_cast<int32_t>(offsetof(LeaderboardManager_t1534320874, ___allTimeWorldLeaderboard_6)); }
	inline List_1_t89046591 * get_allTimeWorldLeaderboard_6() const { return ___allTimeWorldLeaderboard_6; }
	inline List_1_t89046591 ** get_address_of_allTimeWorldLeaderboard_6() { return &___allTimeWorldLeaderboard_6; }
	inline void set_allTimeWorldLeaderboard_6(List_1_t89046591 * value)
	{
		___allTimeWorldLeaderboard_6 = value;
		Il2CppCodeGenWriteBarrier(&___allTimeWorldLeaderboard_6, value);
	}

	inline static int32_t get_offset_of_playerAllTimeRank_7() { return static_cast<int32_t>(offsetof(LeaderboardManager_t1534320874, ___playerAllTimeRank_7)); }
	inline int32_t get_playerAllTimeRank_7() const { return ___playerAllTimeRank_7; }
	inline int32_t* get_address_of_playerAllTimeRank_7() { return &___playerAllTimeRank_7; }
	inline void set_playerAllTimeRank_7(int32_t value)
	{
		___playerAllTimeRank_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
