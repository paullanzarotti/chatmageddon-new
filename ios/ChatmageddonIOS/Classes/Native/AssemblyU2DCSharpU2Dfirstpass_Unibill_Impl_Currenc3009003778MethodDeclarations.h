﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.CurrencyManager
struct CurrencyManager_t3009003778;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Uniject.IStorage
struct IStorage_t1347868490;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// VirtualCurrency
struct VirtualCurrency_t1115497880;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_VirtualCurrency1115497880.h"

// System.Void Unibill.Impl.CurrencyManager::.ctor(Unibill.Impl.UnibillConfiguration,Uniject.IStorage)
extern "C"  void CurrencyManager__ctor_m476098854 (CurrencyManager_t3009003778 * __this, UnibillConfiguration_t2915611853 * ___config0, Il2CppObject * ___storage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Unibill.Impl.CurrencyManager::get_Currencies()
extern "C"  StringU5BU5D_t1642385972* CurrencyManager_get_Currencies_m1298298399 (CurrencyManager_t3009003778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.CurrencyManager::set_Currencies(System.String[])
extern "C"  void CurrencyManager_set_Currencies_m1077657744 (CurrencyManager_t3009003778 * __this, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.CurrencyManager::OnPurchased(System.String)
extern "C"  void CurrencyManager_OnPurchased_m1902679366 (CurrencyManager_t3009003778 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Unibill.Impl.CurrencyManager::GetCurrencyBalance(System.String)
extern "C"  Decimal_t724701077  CurrencyManager_GetCurrencyBalance_m705785068 (CurrencyManager_t3009003778 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.CurrencyManager::CreditBalance(System.String,System.Decimal)
extern "C"  void CurrencyManager_CreditBalance_m2731343665 (CurrencyManager_t3009003778 * __this, String_t* ___id0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.CurrencyManager::SetBalance(System.String,System.Decimal)
extern "C"  void CurrencyManager_SetBalance_m1129618572 (CurrencyManager_t3009003778 * __this, String_t* ___id0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.CurrencyManager::DebitBalance(System.String,System.Decimal)
extern "C"  bool CurrencyManager_DebitBalance_m4122213592 (CurrencyManager_t3009003778 * __this, String_t* ___id0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.CurrencyManager::getKey(System.String)
extern "C"  String_t* CurrencyManager_getKey_m2564872836 (CurrencyManager_t3009003778 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.CurrencyManager::<CurrencyManager>m__0(VirtualCurrency)
extern "C"  String_t* CurrencyManager_U3CCurrencyManagerU3Em__0_m3083276008 (Il2CppObject * __this /* static, unused */, VirtualCurrency_t1115497880 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
