﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_RGB
struct CameraFilterPack_Color_RGB_t235976532;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_RGB::.ctor()
extern "C"  void CameraFilterPack_Color_RGB__ctor_m3087455021 (CameraFilterPack_Color_RGB_t235976532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_RGB::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_RGB_get_material_m4025569622 (CameraFilterPack_Color_RGB_t235976532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::Start()
extern "C"  void CameraFilterPack_Color_RGB_Start_m3418698741 (CameraFilterPack_Color_RGB_t235976532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_RGB_OnRenderImage_m1237994029 (CameraFilterPack_Color_RGB_t235976532 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::OnValidate()
extern "C"  void CameraFilterPack_Color_RGB_OnValidate_m3381675692 (CameraFilterPack_Color_RGB_t235976532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::Update()
extern "C"  void CameraFilterPack_Color_RGB_Update_m557264734 (CameraFilterPack_Color_RGB_t235976532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::OnDisable()
extern "C"  void CameraFilterPack_Color_RGB_OnDisable_m667092728 (CameraFilterPack_Color_RGB_t235976532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
