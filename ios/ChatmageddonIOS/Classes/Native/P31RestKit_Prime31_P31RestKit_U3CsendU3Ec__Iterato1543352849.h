﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`2<System.String,System.Object>
struct Action_2_t599803691;
// Prime31.P31RestKit
struct P31RestKit_t1799911290;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "P31RestKit_Prime31_HTTPVerb1395544859.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.P31RestKit/<send>c__Iterator0
struct  U3CsendU3Ec__Iterator0_t1543352849  : public Il2CppObject
{
public:
	// System.String Prime31.P31RestKit/<send>c__Iterator0::path
	String_t* ___path_0;
	// Prime31.HTTPVerb Prime31.P31RestKit/<send>c__Iterator0::httpVerb
	int32_t ___httpVerb_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.P31RestKit/<send>c__Iterator0::parameters
	Dictionary_2_t309261261 * ___parameters_2;
	// UnityEngine.WWW Prime31.P31RestKit/<send>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_3;
	// System.Action`2<System.String,System.Object> Prime31.P31RestKit/<send>c__Iterator0::onComplete
	Action_2_t599803691 * ___onComplete_4;
	// Prime31.P31RestKit Prime31.P31RestKit/<send>c__Iterator0::$this
	P31RestKit_t1799911290 * ___U24this_5;
	// System.Object Prime31.P31RestKit/<send>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean Prime31.P31RestKit/<send>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Prime31.P31RestKit/<send>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_httpVerb_1() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___httpVerb_1)); }
	inline int32_t get_httpVerb_1() const { return ___httpVerb_1; }
	inline int32_t* get_address_of_httpVerb_1() { return &___httpVerb_1; }
	inline void set_httpVerb_1(int32_t value)
	{
		___httpVerb_1 = value;
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___parameters_2)); }
	inline Dictionary_2_t309261261 * get_parameters_2() const { return ___parameters_2; }
	inline Dictionary_2_t309261261 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(Dictionary_2_t309261261 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___U3CwwwU3E__0_3)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_3() const { return ___U3CwwwU3E__0_3; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_3() { return &___U3CwwwU3E__0_3; }
	inline void set_U3CwwwU3E__0_3(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_3, value);
	}

	inline static int32_t get_offset_of_onComplete_4() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___onComplete_4)); }
	inline Action_2_t599803691 * get_onComplete_4() const { return ___onComplete_4; }
	inline Action_2_t599803691 ** get_address_of_onComplete_4() { return &___onComplete_4; }
	inline void set_onComplete_4(Action_2_t599803691 * value)
	{
		___onComplete_4 = value;
		Il2CppCodeGenWriteBarrier(&___onComplete_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___U24this_5)); }
	inline P31RestKit_t1799911290 * get_U24this_5() const { return ___U24this_5; }
	inline P31RestKit_t1799911290 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(P31RestKit_t1799911290 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsendU3Ec__Iterator0_t1543352849, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
