﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldSlider
struct ShieldSlider_t1483508804;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldSlider::.ctor()
extern "C"  void ShieldSlider__ctor_m3905542425 (ShieldSlider_t1483508804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldSlider::LabelsUpdated(System.Int32)
extern "C"  void ShieldSlider_LabelsUpdated_m734577620 (ShieldSlider_t1483508804 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
