﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<FriendHomeNavScreen>
struct DefaultComparer_t88308529;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<FriendHomeNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2575365778_gshared (DefaultComparer_t88308529 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2575365778(__this, method) ((  void (*) (DefaultComparer_t88308529 *, const MethodInfo*))DefaultComparer__ctor_m2575365778_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<FriendHomeNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m38992513_gshared (DefaultComparer_t88308529 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m38992513(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t88308529 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m38992513_gshared)(__this, ___x0, ___y1, method)
