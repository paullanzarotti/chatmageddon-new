﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Constructe3468994639.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerExterna2315310584.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerExterna2840903253.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGenerato951913470.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerOctetSt3154381398.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSequenc1671991799.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSetGene2437150008.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSetPars3304453660.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DefiniteLe1065419872.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerApplica2286556739.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerBmpStrin177902387.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerBitStri2717907355.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerBoolean857650049.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerEnumerat514019671.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGeneral2384300942.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGeneral1810943552.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerIA5Strin460424437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerInteger967720487.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerNull559715134.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerNumeric3931265727.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerObjectI3495876513.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerOctetSt2503196699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerOutputS2807883870.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerPrintabl696797023.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSequenc4250014174.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSet2720781401.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerStringB1343648701.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerT61Stri2556947549.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerTaggedO2520525900.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerUtcTime3495696610.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerUtf8Str1713298971.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerUnivers2286946381.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerVisibleS512908706.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Indefinite1999368943.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LazyAsn1In2434367547.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LazyDerSeq4187101828.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LazyDerSet1112924515.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LimitedInpu781897436.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_OidTokeniz1891371977.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Anssi_Anss1870423751.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Anssi_AnssiN28633283.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Anssi_Anssi107688019.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_C378628739.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_1984881998.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_3870759210.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_2624318576.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_4257955415.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Iana_IanaO2977840882.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_MiscOb808030413.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_Netsca668167463.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_Netsc2439051718.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_Veris3134672961.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Nist_NistNa941520945.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Nist_NistO1742042649.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_OcspR2399692236.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_OcspR2886715370.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_Respon853298195.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_Respon561211756.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Oiw_ElGama1755000518.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Oiw_OiwObj2484194348.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_Conte1565361645.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_DHPara310369835.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_PkcsO1103055686.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_Rsass1481623005.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_Signe1315018810.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName558061916.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3753151333.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2526217440.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2159797956.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName170864723.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1154406297.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1153592480.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3459626543.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3762826772.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3762621805.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1547647928.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1546834111.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3240468911.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3239722744.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1133933760.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4048294485.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2345758350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNamed14293759.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4037539526.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName944075595.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2343088644.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2342274827.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNamed10810236.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2355228822.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNamed23764231.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName164911864.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName164174145.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2003561066.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName170425007.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName170160838.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2814401075.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (ConstructedOctetStream_t3468994639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[3] = 
{
	ConstructedOctetStream_t3468994639::get_offset_of__parser_3(),
	ConstructedOctetStream_t3468994639::get_offset_of__first_4(),
	ConstructedOctetStream_t3468994639::get_offset_of__currentStream_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (DerExternal_t2315310584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[5] = 
{
	DerExternal_t2315310584::get_offset_of_directReference_2(),
	DerExternal_t2315310584::get_offset_of_indirectReference_3(),
	DerExternal_t2315310584::get_offset_of_dataValueDescriptor_4(),
	DerExternal_t2315310584::get_offset_of_encoding_5(),
	DerExternal_t2315310584::get_offset_of_externalContent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (DerExternalParser_t2840903253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[1] = 
{
	DerExternalParser_t2840903253::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (DerGenerator_t951913470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[3] = 
{
	DerGenerator_t951913470::get_offset_of__tagged_1(),
	DerGenerator_t951913470::get_offset_of__isExplicit_2(),
	DerGenerator_t951913470::get_offset_of__tagNo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (DerOctetStringParser_t3154381398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[1] = 
{
	DerOctetStringParser_t3154381398::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (DerSequenceParser_t1671991799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[1] = 
{
	DerSequenceParser_t1671991799::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (DerSetGenerator_t2437150008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3706[1] = 
{
	DerSetGenerator_t2437150008::get_offset_of__bOut_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (DerSetParser_t3304453660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[1] = 
{
	DerSetParser_t3304453660::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (DefiniteLengthInputStream_t1065419872), -1, sizeof(DefiniteLengthInputStream_t1065419872_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3708[3] = 
{
	DefiniteLengthInputStream_t1065419872_StaticFields::get_offset_of_EmptyBytes_5(),
	DefiniteLengthInputStream_t1065419872::get_offset_of__originalLength_6(),
	DefiniteLengthInputStream_t1065419872::get_offset_of__remaining_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (DerApplicationSpecific_t2286556739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[3] = 
{
	DerApplicationSpecific_t2286556739::get_offset_of_isConstructed_2(),
	DerApplicationSpecific_t2286556739::get_offset_of_tag_3(),
	DerApplicationSpecific_t2286556739::get_offset_of_octets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (DerBmpString_t177902387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3710[1] = 
{
	DerBmpString_t177902387::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (DerBitString_t2717907355), -1, sizeof(DerBitString_t2717907355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3711[3] = 
{
	DerBitString_t2717907355_StaticFields::get_offset_of_table_2(),
	DerBitString_t2717907355::get_offset_of_data_3(),
	DerBitString_t2717907355::get_offset_of_padBits_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (DerBoolean_t857650049), -1, sizeof(DerBoolean_t857650049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3712[3] = 
{
	DerBoolean_t857650049::get_offset_of_value_2(),
	DerBoolean_t857650049_StaticFields::get_offset_of_False_3(),
	DerBoolean_t857650049_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (DerEnumerated_t514019671), -1, sizeof(DerEnumerated_t514019671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3713[2] = 
{
	DerEnumerated_t514019671::get_offset_of_bytes_2(),
	DerEnumerated_t514019671_StaticFields::get_offset_of_cache_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (DerGeneralString_t2384300942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[1] = 
{
	DerGeneralString_t2384300942::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (DerGeneralizedTime_t1810943552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[1] = 
{
	DerGeneralizedTime_t1810943552::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (DerIA5String_t460424437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[1] = 
{
	DerIA5String_t460424437::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (DerInteger_t967720487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[1] = 
{
	DerInteger_t967720487::get_offset_of_bytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (DerNull_t559715134), -1, sizeof(DerNull_t559715134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3718[2] = 
{
	DerNull_t559715134_StaticFields::get_offset_of_Instance_2(),
	DerNull_t559715134::get_offset_of_zeroBytes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (DerNumericString_t3931265727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[1] = 
{
	DerNumericString_t3931265727::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (DerObjectIdentifier_t3495876513), -1, sizeof(DerObjectIdentifier_t3495876513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3720[4] = 
{
	DerObjectIdentifier_t3495876513::get_offset_of_identifier_2(),
	DerObjectIdentifier_t3495876513::get_offset_of_body_3(),
	0,
	DerObjectIdentifier_t3495876513_StaticFields::get_offset_of_cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (DerOctetString_t2503196699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (DerOutputStream_t2807883870), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (DerPrintableString_t696797023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[1] = 
{
	DerPrintableString_t696797023::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (DerSequence_t4250014174), -1, sizeof(DerSequence_t4250014174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3724[1] = 
{
	DerSequence_t4250014174_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (DerSet_t2720781401), -1, sizeof(DerSet_t2720781401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3725[1] = 
{
	DerSet_t2720781401_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (DerStringBase_t1343648701), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (DerT61String_t2556947549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[1] = 
{
	DerT61String_t2556947549::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (DerTaggedObject_t2520525900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (DerUtcTime_t3495696610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[1] = 
{
	DerUtcTime_t3495696610::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (DerUtf8String_t1713298971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[1] = 
{
	DerUtf8String_t1713298971::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (DerUniversalString_t2286946381), -1, sizeof(DerUniversalString_t2286946381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3731[2] = 
{
	DerUniversalString_t2286946381_StaticFields::get_offset_of_table_2(),
	DerUniversalString_t2286946381::get_offset_of_str_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (DerVisibleString_t512908706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[1] = 
{
	DerVisibleString_t512908706::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (IndefiniteLengthInputStream_t1999368943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3737[2] = 
{
	IndefiniteLengthInputStream_t1999368943::get_offset_of__lookAhead_5(),
	IndefiniteLengthInputStream_t1999368943::get_offset_of__eofOn00_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (LazyAsn1InputStream_t2434367547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (LazyDerSequence_t4187101828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[1] = 
{
	LazyDerSequence_t4187101828::get_offset_of_encoded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (LazyDerSet_t1112924515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[1] = 
{
	LazyDerSet_t1112924515::get_offset_of_encoded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (LimitedInputStream_t781897436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3741[2] = 
{
	LimitedInputStream_t781897436::get_offset_of__in_3(),
	LimitedInputStream_t781897436::get_offset_of__limit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (OidTokenizer_t1891371977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3742[2] = 
{
	OidTokenizer_t1891371977::get_offset_of_oid_0(),
	OidTokenizer_t1891371977::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (AnssiNamedCurves_t1870423751), -1, sizeof(AnssiNamedCurves_t1870423751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3743[3] = 
{
	AnssiNamedCurves_t1870423751_StaticFields::get_offset_of_objIds_0(),
	AnssiNamedCurves_t1870423751_StaticFields::get_offset_of_curves_1(),
	AnssiNamedCurves_t1870423751_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (Frp256v1Holder_t28633283), -1, sizeof(Frp256v1Holder_t28633283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3744[1] = 
{
	Frp256v1Holder_t28633283_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (AnssiObjectIdentifiers_t107688019), -1, sizeof(AnssiObjectIdentifiers_t107688019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3745[1] = 
{
	AnssiObjectIdentifiers_t107688019_StaticFields::get_offset_of_FRP256v1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (CryptoProObjectIdentifiers_t378628739), -1, sizeof(CryptoProObjectIdentifiers_t378628739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3746[24] = 
{
	0,
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411_1(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411Hmac_2(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR28147Cbc_3(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_ID_Gost28147_89_CryptoPro_A_ParamSet_4(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94_5(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001_6(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411x94WithGostR3410x94_7(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411x94WithGostR3410x2001_8(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411x94CryptoProParamSet_9(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProA_10(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProB_11(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProC_12(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProD_13(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProXchA_14(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProXchB_15(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProXchC_16(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProA_17(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProB_18(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProC_19(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProXchA_20(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProXchB_21(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostElSgDH3410Default_22(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostElSgDH3410x1_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (ECGost3410NamedCurves_t1984881998), -1, sizeof(ECGost3410NamedCurves_t1984881998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3747[3] = 
{
	ECGost3410NamedCurves_t1984881998_StaticFields::get_offset_of_objIds_0(),
	ECGost3410NamedCurves_t1984881998_StaticFields::get_offset_of_parameters_1(),
	ECGost3410NamedCurves_t1984881998_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (Gost3410NamedParameters_t3870759210), -1, sizeof(Gost3410NamedParameters_t3870759210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3748[5] = 
{
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_objIds_0(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_parameters_1(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_cryptoProA_2(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_cryptoProB_3(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_cryptoProXchA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (Gost3410ParamSetParameters_t2624318576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[4] = 
{
	Gost3410ParamSetParameters_t2624318576::get_offset_of_keySize_2(),
	Gost3410ParamSetParameters_t2624318576::get_offset_of_p_3(),
	Gost3410ParamSetParameters_t2624318576::get_offset_of_q_4(),
	Gost3410ParamSetParameters_t2624318576::get_offset_of_a_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (Gost3410PublicKeyAlgParameters_t4257955415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[3] = 
{
	Gost3410PublicKeyAlgParameters_t4257955415::get_offset_of_publicKeyParamSet_2(),
	Gost3410PublicKeyAlgParameters_t4257955415::get_offset_of_digestParamSet_3(),
	Gost3410PublicKeyAlgParameters_t4257955415::get_offset_of_encryptionParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (IanaObjectIdentifiers_t2977840882), -1, sizeof(IanaObjectIdentifiers_t2977840882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3751[5] = 
{
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_IsakmpOakley_0(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacMD5_1(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacSha1_2(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacTiger_3(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacRipeMD160_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (MiscObjectIdentifiers_t808030413), -1, sizeof(MiscObjectIdentifiers_t808030413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3752[20] = 
{
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Netscape_0(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCertType_1(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeBaseUrl_2(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeRevocationUrl_3(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCARevocationUrl_4(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeRenewalUrl_5(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCAPolicyUrl_6(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeSslServerName_7(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCertComment_8(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Verisign_9(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignCzagExtension_10(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignPrivate_6_9_11(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignOnSiteJurisdictionHash_12(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignBitString_6_13_13(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignDnbDunsNumber_14(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignIssStrongCrypto_15(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Novell_16(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NovellSecurityAttribs_17(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Entrust_18(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_EntrustVersionExtension_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (NetscapeCertType_t668167463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (NetscapeRevocationUrl_t2439051718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (VerisignCzagExtension_t3134672961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (NistNamedCurves_t941520945), -1, sizeof(NistNamedCurves_t941520945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3756[2] = 
{
	NistNamedCurves_t941520945_StaticFields::get_offset_of_objIds_0(),
	NistNamedCurves_t941520945_StaticFields::get_offset_of_names_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (NistObjectIdentifiers_t1742042649), -1, sizeof(NistObjectIdentifiers_t1742042649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3757[35] = 
{
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_NistAlgorithm_0(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_HashAlgs_1(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha256_2(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha384_3(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha512_4(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha224_5(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha512_224_6(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha512_256_7(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_Aes_8(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Ecb_9(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Cbc_10(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Ofb_11(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Cfb_12(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Wrap_13(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Gcm_14(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Ccm_15(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Ecb_16(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Cbc_17(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Ofb_18(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Cfb_19(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Wrap_20(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Gcm_21(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Ccm_22(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Ecb_23(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Cbc_24(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Ofb_25(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Cfb_26(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Wrap_27(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Gcm_28(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Ccm_29(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdDsaWithSha2_30(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha224_31(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha256_32(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha384_33(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha512_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (OcspResponse_t2399692236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[2] = 
{
	OcspResponse_t2399692236::get_offset_of_responseStatus_2(),
	OcspResponse_t2399692236::get_offset_of_responseBytes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (OcspResponseStatus_t2886715370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3759[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (ResponderID_t853298195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[1] = 
{
	ResponderID_t853298195::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (ResponseBytes_t561211756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[2] = 
{
	ResponseBytes_t561211756::get_offset_of_responseType_2(),
	ResponseBytes_t561211756::get_offset_of_response_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (ElGamalParameter_t1755000518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[2] = 
{
	ElGamalParameter_t1755000518::get_offset_of_p_2(),
	ElGamalParameter_t1755000518::get_offset_of_g_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (OiwObjectIdentifiers_t2484194348), -1, sizeof(OiwObjectIdentifiers_t2484194348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3763[12] = 
{
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_MD4WithRsa_0(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_MD5WithRsa_1(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_MD4WithRsaEncryption_2(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesEcb_3(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesCbc_4(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesOfb_5(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesCfb_6(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesEde_7(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_IdSha1_8(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DsaWithSha1_9(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_Sha1WithRsa_10(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_ElGamalAlgorithm_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (ContentInfo_t1565361645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3764[2] = 
{
	ContentInfo_t1565361645::get_offset_of_contentType_2(),
	ContentInfo_t1565361645::get_offset_of_content_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (DHParameter_t310369835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[3] = 
{
	DHParameter_t310369835::get_offset_of_p_2(),
	DHParameter_t310369835::get_offset_of_g_3(),
	DHParameter_t310369835::get_offset_of_l_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (PkcsObjectIdentifiers_t1103055686), -1, sizeof(PkcsObjectIdentifiers_t1103055686_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3766[130] = 
{
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_RsaEncryption_1(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD2WithRsaEncryption_2(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD4WithRsaEncryption_3(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD5WithRsaEncryption_4(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha1WithRsaEncryption_5(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SrsaOaepEncryptionSet_6(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdRsaesOaep_7(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdMgf1_8(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdPSpecified_9(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdRsassaPss_10(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha256WithRsaEncryption_11(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha384WithRsaEncryption_12(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha512WithRsaEncryption_13(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha224WithRsaEncryption_14(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_DhKeyAgreement_16(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD2AndDesCbc_18(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD2AndRC2Cbc_19(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD5AndDesCbc_20(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD5AndRC2Cbc_21(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithSha1AndDesCbc_22(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithSha1AndRC2Cbc_23(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdPbeS2_24(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdPbkdf2_25(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_DesEde3Cbc_27(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_RC2Cbc_28(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD2_30(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD4_31(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD5_32(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha1_33(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha224_34(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha256_35(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha384_36(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha512_37(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Data_39(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SignedData_40(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_EnvelopedData_41(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SignedAndEnvelopedData_42(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_DigestedData_43(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_EncryptedData_44(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtEmailAddress_46(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtUnstructuredName_47(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtContentType_48(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtMessageDigest_49(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtSigningTime_50(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtCounterSignature_51(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtChallengePassword_52(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtUnstructuredAddress_53(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtExtendedCertificateAttributes_54(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtSigningDescription_55(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtExtensionRequest_56(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtSmimeCapabilities_57(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtFriendlyName_58(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtLocalKeyID_59(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_X509CertType_60(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_X509Certificate_62(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SdsiCertificate_63(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_X509Crl_65(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgPwriKek_66(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PreferSignedData_67(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_CannotDecryptAny_68(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SmimeCapabilitiesVersions_69(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAReceiptRequest_70(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTAuthData_72(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTTstInfo_73(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTCompressedData_74(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTAuthEnvelopedData_75(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTTimestampedData_76(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfOrigin_78(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfReceipt_79(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfDelivery_80(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfSender_81(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfApproval_82(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfCreation_83(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAContentHint_85(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAMsgSigDigest_86(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAContentReference_87(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEncrypKeyPref_88(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASigningCertificate_89(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASigningCertificateV2_90(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAContentIdentifier_91(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASignatureTimeStampToken_92(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsSigPolicyID_93(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCommitmentType_94(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsSignerLocation_95(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsSignerAttr_96(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsOtherSigCert_97(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsContentTimestamp_98(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCertificateRefs_99(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsRevocationRefs_100(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCertValues_101(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsRevocationValues_102(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsEscTimeStamp_103(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCertCrlTimestamp_104(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsArchiveTimestamp_105(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASigPolicyID_106(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAACommitmentType_107(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASignerLocation_108(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAOtherSigCert_109(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdSpqEtsUri_111(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdSpqEtsUNotice_112(),
	0,
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_KeyBag_115(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs8ShroudedKeyBag_116(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_CertBag_117(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_CrlBag_118(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SecretBag_119(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SafeContentsBag_120(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd128BitRC4_122(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd40BitRC4_123(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd3KeyTripleDesCbc_124(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd2KeyTripleDesCbc_125(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd128BitRC2Cbc_126(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbewithShaAnd40BitRC2Cbc_127(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgCms3DesWrap_128(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgCmsRC2Wrap_129(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (RsassaPssParameters_t1481623005), -1, sizeof(RsassaPssParameters_t1481623005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3767[8] = 
{
	RsassaPssParameters_t1481623005::get_offset_of_hashAlgorithm_2(),
	RsassaPssParameters_t1481623005::get_offset_of_maskGenAlgorithm_3(),
	RsassaPssParameters_t1481623005::get_offset_of_saltLength_4(),
	RsassaPssParameters_t1481623005::get_offset_of_trailerField_5(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultHashAlgorithm_6(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultMaskGenFunction_7(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultSaltLength_8(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultTrailerField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (SignedData_t1315018810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3768[6] = 
{
	SignedData_t1315018810::get_offset_of_version_2(),
	SignedData_t1315018810::get_offset_of_digestAlgorithms_3(),
	SignedData_t1315018810::get_offset_of_contentInfo_4(),
	SignedData_t1315018810::get_offset_of_certificates_5(),
	SignedData_t1315018810::get_offset_of_crls_6(),
	SignedData_t1315018810::get_offset_of_signerInfos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (SecNamedCurves_t558061916), -1, sizeof(SecNamedCurves_t558061916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3769[3] = 
{
	SecNamedCurves_t558061916_StaticFields::get_offset_of_objIds_0(),
	SecNamedCurves_t558061916_StaticFields::get_offset_of_curves_1(),
	SecNamedCurves_t558061916_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (Secp112r1Holder_t3753151333), -1, sizeof(Secp112r1Holder_t3753151333_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3770[1] = 
{
	Secp112r1Holder_t3753151333_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (Secp112r2Holder_t2526217440), -1, sizeof(Secp112r2Holder_t2526217440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3771[1] = 
{
	Secp112r2Holder_t2526217440_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (Secp128r1Holder_t2159797956), -1, sizeof(Secp128r1Holder_t2159797956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3772[1] = 
{
	Secp128r1Holder_t2159797956_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (Secp128r2Holder_t170864723), -1, sizeof(Secp128r2Holder_t170864723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3773[1] = 
{
	Secp128r2Holder_t170864723_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (Secp160k1Holder_t1154406297), -1, sizeof(Secp160k1Holder_t1154406297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3774[1] = 
{
	Secp160k1Holder_t1154406297_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (Secp160r1Holder_t1153592480), -1, sizeof(Secp160r1Holder_t1153592480_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3775[1] = 
{
	Secp160r1Holder_t1153592480_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (Secp160r2Holder_t3459626543), -1, sizeof(Secp160r2Holder_t3459626543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3776[1] = 
{
	Secp160r2Holder_t3459626543_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (Secp192k1Holder_t3762826772), -1, sizeof(Secp192k1Holder_t3762826772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3777[1] = 
{
	Secp192k1Holder_t3762826772_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (Secp192r1Holder_t3762621805), -1, sizeof(Secp192r1Holder_t3762621805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3778[1] = 
{
	Secp192r1Holder_t3762621805_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (Secp224k1Holder_t1547647928), -1, sizeof(Secp224k1Holder_t1547647928_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3779[1] = 
{
	Secp224k1Holder_t1547647928_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (Secp224r1Holder_t1546834111), -1, sizeof(Secp224r1Holder_t1546834111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3780[1] = 
{
	Secp224r1Holder_t1546834111_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (Secp256k1Holder_t3240468911), -1, sizeof(Secp256k1Holder_t3240468911_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3781[1] = 
{
	Secp256k1Holder_t3240468911_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (Secp256r1Holder_t3239722744), -1, sizeof(Secp256r1Holder_t3239722744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3782[1] = 
{
	Secp256r1Holder_t3239722744_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (Secp384r1Holder_t1133933760), -1, sizeof(Secp384r1Holder_t1133933760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3783[1] = 
{
	Secp384r1Holder_t1133933760_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (Secp521r1Holder_t4048294485), -1, sizeof(Secp521r1Holder_t4048294485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3784[1] = 
{
	Secp521r1Holder_t4048294485_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (Sect113r1Holder_t2345758350), -1, sizeof(Sect113r1Holder_t2345758350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3785[3] = 
{
	Sect113r1Holder_t2345758350_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (Sect113r2Holder_t14293759), -1, sizeof(Sect113r2Holder_t14293759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3786[3] = 
{
	Sect113r2Holder_t14293759_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (Sect131r1Holder_t4037539526), -1, sizeof(Sect131r1Holder_t4037539526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3787[5] = 
{
	Sect131r1Holder_t4037539526_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (Sect131r2Holder_t944075595), -1, sizeof(Sect131r2Holder_t944075595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3788[5] = 
{
	Sect131r2Holder_t944075595_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (Sect163k1Holder_t2343088644), -1, sizeof(Sect163k1Holder_t2343088644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3789[5] = 
{
	Sect163k1Holder_t2343088644_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (Sect163r1Holder_t2342274827), -1, sizeof(Sect163r1Holder_t2342274827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3790[5] = 
{
	Sect163r1Holder_t2342274827_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (Sect163r2Holder_t10810236), -1, sizeof(Sect163r2Holder_t10810236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3791[5] = 
{
	Sect163r2Holder_t10810236_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (Sect193r1Holder_t2355228822), -1, sizeof(Sect193r1Holder_t2355228822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3792[3] = 
{
	Sect193r1Holder_t2355228822_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (Sect193r2Holder_t23764231), -1, sizeof(Sect193r2Holder_t23764231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3793[3] = 
{
	Sect193r2Holder_t23764231_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (Sect233k1Holder_t164911864), -1, sizeof(Sect233k1Holder_t164911864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3794[3] = 
{
	Sect233k1Holder_t164911864_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (Sect233r1Holder_t164174145), -1, sizeof(Sect233r1Holder_t164174145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3795[3] = 
{
	Sect233r1Holder_t164174145_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (Sect239k1Holder_t2003561066), -1, sizeof(Sect239k1Holder_t2003561066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3796[3] = 
{
	Sect239k1Holder_t2003561066_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (Sect283k1Holder_t170425007), -1, sizeof(Sect283k1Holder_t170425007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3797[5] = 
{
	Sect283k1Holder_t170425007_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (Sect283r1Holder_t170160838), -1, sizeof(Sect283r1Holder_t170160838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3798[5] = 
{
	Sect283r1Holder_t170160838_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (Sect409k1Holder_t2814401075), -1, sizeof(Sect409k1Holder_t2814401075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3799[3] = 
{
	Sect409k1Holder_t2814401075_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
