﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LanguageModel
struct LanguageModel_t2428340347;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageConfig/<GetLanguageStringUsingModel>c__AnonStorey0
struct  U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384  : public Il2CppObject
{
public:
	// LanguageModel LanguageConfig/<GetLanguageStringUsingModel>c__AnonStorey0::model
	LanguageModel_t2428340347 * ___model_0;

public:
	inline static int32_t get_offset_of_model_0() { return static_cast<int32_t>(offsetof(U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384, ___model_0)); }
	inline LanguageModel_t2428340347 * get_model_0() const { return ___model_0; }
	inline LanguageModel_t2428340347 ** get_address_of_model_0() { return &___model_0; }
	inline void set_model_0(LanguageModel_t2428340347 * value)
	{
		___model_0 = value;
		Il2CppCodeGenWriteBarrier(&___model_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
