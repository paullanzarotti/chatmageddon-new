﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// YNContinueButton
struct YNContinueButton_t318956450;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YourNumberScreen
struct  YourNumberScreen_t2760251358  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject YourNumberScreen::contentObject
	GameObject_t1756533147 * ___contentObject_3;
	// UILabel YourNumberScreen::phoneNumLabel
	UILabel_t1795115428 * ___phoneNumLabel_4;
	// YNContinueButton YourNumberScreen::continueButton
	YNContinueButton_t318956450 * ___continueButton_5;
	// System.Boolean YourNumberScreen::registerUIclosing
	bool ___registerUIclosing_6;
	// System.Boolean YourNumberScreen::yourNumUIclosing
	bool ___yourNumUIclosing_7;

public:
	inline static int32_t get_offset_of_contentObject_3() { return static_cast<int32_t>(offsetof(YourNumberScreen_t2760251358, ___contentObject_3)); }
	inline GameObject_t1756533147 * get_contentObject_3() const { return ___contentObject_3; }
	inline GameObject_t1756533147 ** get_address_of_contentObject_3() { return &___contentObject_3; }
	inline void set_contentObject_3(GameObject_t1756533147 * value)
	{
		___contentObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___contentObject_3, value);
	}

	inline static int32_t get_offset_of_phoneNumLabel_4() { return static_cast<int32_t>(offsetof(YourNumberScreen_t2760251358, ___phoneNumLabel_4)); }
	inline UILabel_t1795115428 * get_phoneNumLabel_4() const { return ___phoneNumLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_phoneNumLabel_4() { return &___phoneNumLabel_4; }
	inline void set_phoneNumLabel_4(UILabel_t1795115428 * value)
	{
		___phoneNumLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumLabel_4, value);
	}

	inline static int32_t get_offset_of_continueButton_5() { return static_cast<int32_t>(offsetof(YourNumberScreen_t2760251358, ___continueButton_5)); }
	inline YNContinueButton_t318956450 * get_continueButton_5() const { return ___continueButton_5; }
	inline YNContinueButton_t318956450 ** get_address_of_continueButton_5() { return &___continueButton_5; }
	inline void set_continueButton_5(YNContinueButton_t318956450 * value)
	{
		___continueButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___continueButton_5, value);
	}

	inline static int32_t get_offset_of_registerUIclosing_6() { return static_cast<int32_t>(offsetof(YourNumberScreen_t2760251358, ___registerUIclosing_6)); }
	inline bool get_registerUIclosing_6() const { return ___registerUIclosing_6; }
	inline bool* get_address_of_registerUIclosing_6() { return &___registerUIclosing_6; }
	inline void set_registerUIclosing_6(bool value)
	{
		___registerUIclosing_6 = value;
	}

	inline static int32_t get_offset_of_yourNumUIclosing_7() { return static_cast<int32_t>(offsetof(YourNumberScreen_t2760251358, ___yourNumUIclosing_7)); }
	inline bool get_yourNumUIclosing_7() const { return ___yourNumUIclosing_7; }
	inline bool* get_address_of_yourNumUIclosing_7() { return &___yourNumUIclosing_7; }
	inline void set_yourNumUIclosing_7(bool value)
	{
		___yourNumUIclosing_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
