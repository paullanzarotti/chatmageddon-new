﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<ActivateUserStealthMode>c__AnonStorey3B
struct U3CActivateUserStealthModeU3Ec__AnonStorey3B_t499954021;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<ActivateUserStealthMode>c__AnonStorey3B::.ctor()
extern "C"  void U3CActivateUserStealthModeU3Ec__AnonStorey3B__ctor_m1567278898 (U3CActivateUserStealthModeU3Ec__AnonStorey3B_t499954021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<ActivateUserStealthMode>c__AnonStorey3B::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CActivateUserStealthModeU3Ec__AnonStorey3B_U3CU3Em__0_m457228153 (U3CActivateUserStealthModeU3Ec__AnonStorey3B_t499954021 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
