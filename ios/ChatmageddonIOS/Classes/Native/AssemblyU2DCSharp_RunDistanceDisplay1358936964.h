﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UISprite
struct UISprite_t603616735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RunDistanceDisplay
struct  RunDistanceDisplay_t1358936964  : public MonoBehaviour_t1158329972
{
public:
	// UITexture RunDistanceDisplay::safeRing
	UITexture_t2537039969 * ___safeRing_2;
	// UITexture RunDistanceDisplay::dangerRing
	UITexture_t2537039969 * ___dangerRing_3;
	// UISprite RunDistanceDisplay::playerLocation
	UISprite_t603616735 * ___playerLocation_4;
	// UnityEngine.Vector3 RunDistanceDisplay::screenStartPosition
	Vector3_t2243707580  ___screenStartPosition_5;
	// System.Int32 RunDistanceDisplay::pxPerMeter
	int32_t ___pxPerMeter_6;

public:
	inline static int32_t get_offset_of_safeRing_2() { return static_cast<int32_t>(offsetof(RunDistanceDisplay_t1358936964, ___safeRing_2)); }
	inline UITexture_t2537039969 * get_safeRing_2() const { return ___safeRing_2; }
	inline UITexture_t2537039969 ** get_address_of_safeRing_2() { return &___safeRing_2; }
	inline void set_safeRing_2(UITexture_t2537039969 * value)
	{
		___safeRing_2 = value;
		Il2CppCodeGenWriteBarrier(&___safeRing_2, value);
	}

	inline static int32_t get_offset_of_dangerRing_3() { return static_cast<int32_t>(offsetof(RunDistanceDisplay_t1358936964, ___dangerRing_3)); }
	inline UITexture_t2537039969 * get_dangerRing_3() const { return ___dangerRing_3; }
	inline UITexture_t2537039969 ** get_address_of_dangerRing_3() { return &___dangerRing_3; }
	inline void set_dangerRing_3(UITexture_t2537039969 * value)
	{
		___dangerRing_3 = value;
		Il2CppCodeGenWriteBarrier(&___dangerRing_3, value);
	}

	inline static int32_t get_offset_of_playerLocation_4() { return static_cast<int32_t>(offsetof(RunDistanceDisplay_t1358936964, ___playerLocation_4)); }
	inline UISprite_t603616735 * get_playerLocation_4() const { return ___playerLocation_4; }
	inline UISprite_t603616735 ** get_address_of_playerLocation_4() { return &___playerLocation_4; }
	inline void set_playerLocation_4(UISprite_t603616735 * value)
	{
		___playerLocation_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerLocation_4, value);
	}

	inline static int32_t get_offset_of_screenStartPosition_5() { return static_cast<int32_t>(offsetof(RunDistanceDisplay_t1358936964, ___screenStartPosition_5)); }
	inline Vector3_t2243707580  get_screenStartPosition_5() const { return ___screenStartPosition_5; }
	inline Vector3_t2243707580 * get_address_of_screenStartPosition_5() { return &___screenStartPosition_5; }
	inline void set_screenStartPosition_5(Vector3_t2243707580  value)
	{
		___screenStartPosition_5 = value;
	}

	inline static int32_t get_offset_of_pxPerMeter_6() { return static_cast<int32_t>(offsetof(RunDistanceDisplay_t1358936964, ___pxPerMeter_6)); }
	inline int32_t get_pxPerMeter_6() const { return ___pxPerMeter_6; }
	inline int32_t* get_address_of_pxPerMeter_6() { return &___pxPerMeter_6; }
	inline void set_pxPerMeter_6(int32_t value)
	{
		___pxPerMeter_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
