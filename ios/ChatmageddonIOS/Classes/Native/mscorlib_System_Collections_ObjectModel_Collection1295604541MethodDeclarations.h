﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnibillError>
struct Collection_1_t1295604541;
// System.Collections.Generic.IList`1<UnibillError>
struct IList_1_t2294800388;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnibillError[]
struct UnibillErrorU5BU5D_t4092036522;
// System.Collections.Generic.IEnumerator`1<UnibillError>
struct IEnumerator_1_t3524350910;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::.ctor()
extern "C"  void Collection_1__ctor_m2303396344_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2303396344(__this, method) ((  void (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1__ctor_m2303396344_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m4130134931_gshared (Collection_1_t1295604541 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m4130134931(__this, ___list0, method) ((  void (*) (Collection_1_t1295604541 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m4130134931_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2615786597_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2615786597(__this, method) ((  bool (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2615786597_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2513968784_gshared (Collection_1_t1295604541 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2513968784(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1295604541 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2513968784_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2730918357_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2730918357(__this, method) ((  Il2CppObject * (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2730918357_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2554559676_gshared (Collection_1_t1295604541 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2554559676(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1295604541 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2554559676_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3723842534_gshared (Collection_1_t1295604541 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3723842534(__this, ___value0, method) ((  bool (*) (Collection_1_t1295604541 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3723842534_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1188828430_gshared (Collection_1_t1295604541 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1188828430(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1295604541 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1188828430_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1767585257_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1767585257(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1767585257_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m95773657_gshared (Collection_1_t1295604541 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m95773657(__this, ___value0, method) ((  void (*) (Collection_1_t1295604541 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m95773657_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m203178432_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m203178432(__this, method) ((  bool (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m203178432_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2953152502_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2953152502(__this, method) ((  Il2CppObject * (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2953152502_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3553018773_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3553018773(__this, method) ((  bool (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3553018773_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m128085276_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m128085276(__this, method) ((  bool (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m128085276_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3953526313_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3953526313(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3953526313_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2679593210_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2679593210(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2679593210_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::Add(T)
extern "C"  void Collection_1_Add_m956318093_gshared (Collection_1_t1295604541 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m956318093(__this, ___item0, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_Add_m956318093_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::Clear()
extern "C"  void Collection_1_Clear_m2964689465_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2964689465(__this, method) ((  void (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_Clear_m2964689465_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4015970759_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4015970759(__this, method) ((  void (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_ClearItems_m4015970759_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::Contains(T)
extern "C"  bool Collection_1_Contains_m3173624923_gshared (Collection_1_t1295604541 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3173624923(__this, ___item0, method) ((  bool (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_Contains_m3173624923_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m685991637_gshared (Collection_1_t1295604541 * __this, UnibillErrorU5BU5D_t4092036522* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m685991637(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1295604541 *, UnibillErrorU5BU5D_t4092036522*, int32_t, const MethodInfo*))Collection_1_CopyTo_m685991637_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnibillError>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2101310528_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2101310528(__this, method) ((  Il2CppObject* (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_GetEnumerator_m2101310528_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnibillError>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2086885941_gshared (Collection_1_t1295604541 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2086885941(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2086885941_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1572711850_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1572711850(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1572711850_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2128448163_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2128448163(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2128448163_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::Remove(T)
extern "C"  bool Collection_1_Remove_m4135474036_gshared (Collection_1_t1295604541 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m4135474036(__this, ___item0, method) ((  bool (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_Remove_m4135474036_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2931555654_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2931555654(__this, ___index0, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2931555654_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3199341324_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3199341324(__this, ___index0, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3199341324_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnibillError>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3389668060_gshared (Collection_1_t1295604541 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3389668060(__this, method) ((  int32_t (*) (Collection_1_t1295604541 *, const MethodInfo*))Collection_1_get_Count_m3389668060_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnibillError>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3524434570_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3524434570(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1295604541 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3524434570_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m49162293_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m49162293(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m49162293_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1526459978_gshared (Collection_1_t1295604541 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1526459978(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1295604541 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1526459978_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m4200928663_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m4200928663(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m4200928663_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnibillError>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m452744427_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m452744427(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m452744427_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnibillError>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1427721563_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1427721563(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1427721563_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m4099117483_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m4099117483(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m4099117483_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnibillError>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1252238782_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1252238782(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1252238782_gshared)(__this /* static, unused */, ___list0, method)
