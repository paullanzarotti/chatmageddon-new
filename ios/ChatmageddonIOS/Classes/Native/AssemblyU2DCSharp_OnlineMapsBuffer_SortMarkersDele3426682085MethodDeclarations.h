﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuffer/SortMarkersDelegate
struct SortMarkersDelegate_t3426682085;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<OnlineMapsMarker>
struct IEnumerable_1_t3784293727;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void OnlineMapsBuffer/SortMarkersDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SortMarkersDelegate__ctor_m2285930108 (SortMarkersDelegate_t3426682085 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<OnlineMapsMarker> OnlineMapsBuffer/SortMarkersDelegate::Invoke(System.Collections.Generic.IEnumerable`1<OnlineMapsMarker>)
extern "C"  Il2CppObject* SortMarkersDelegate_Invoke_m122320471 (SortMarkersDelegate_t3426682085 * __this, Il2CppObject* ___markers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnlineMapsBuffer/SortMarkersDelegate::BeginInvoke(System.Collections.Generic.IEnumerable`1<OnlineMapsMarker>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SortMarkersDelegate_BeginInvoke_m2773487924 (SortMarkersDelegate_t3426682085 * __this, Il2CppObject* ___markers0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<OnlineMapsMarker> OnlineMapsBuffer/SortMarkersDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject* SortMarkersDelegate_EndInvoke_m2799401346 (SortMarkersDelegate_t3426682085 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
