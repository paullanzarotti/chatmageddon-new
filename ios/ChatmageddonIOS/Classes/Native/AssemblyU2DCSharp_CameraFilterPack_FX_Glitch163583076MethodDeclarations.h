﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Glitch1
struct CameraFilterPack_FX_Glitch1_t63583076;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Glitch1::.ctor()
extern "C"  void CameraFilterPack_FX_Glitch1__ctor_m3326058809 (CameraFilterPack_FX_Glitch1_t63583076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Glitch1::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Glitch1_get_material_m2385737852 (CameraFilterPack_FX_Glitch1_t63583076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::Start()
extern "C"  void CameraFilterPack_FX_Glitch1_Start_m1368775145 (CameraFilterPack_FX_Glitch1_t63583076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Glitch1_OnRenderImage_m2273830161 (CameraFilterPack_FX_Glitch1_t63583076 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::OnValidate()
extern "C"  void CameraFilterPack_FX_Glitch1_OnValidate_m1137106588 (CameraFilterPack_FX_Glitch1_t63583076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::Update()
extern "C"  void CameraFilterPack_FX_Glitch1_Update_m1427446926 (CameraFilterPack_FX_Glitch1_t63583076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::OnDisable()
extern "C"  void CameraFilterPack_FX_Glitch1_OnDisable_m2462052064 (CameraFilterPack_FX_Glitch1_t63583076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
