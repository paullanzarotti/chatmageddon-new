﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProfileNavigateForwardsButton
struct ProfileNavigateForwardsButton_t1057056812;

#include "codegen/il2cpp-codegen.h"

// System.Void ProfileNavigateForwardsButton::.ctor()
extern "C"  void ProfileNavigateForwardsButton__ctor_m1040398533 (ProfileNavigateForwardsButton_t1057056812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigateForwardsButton::OnButtonClick()
extern "C"  void ProfileNavigateForwardsButton_OnButtonClick_m982342624 (ProfileNavigateForwardsButton_t1057056812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
