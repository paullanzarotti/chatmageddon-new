﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumberUIScaler
struct  PhoneNumberUIScaler_t4195264923  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite PhoneNumberUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider PhoneNumberUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UISprite PhoneNumberUIScaler::titleDivider
	UISprite_t603616735 * ___titleDivider_16;
	// UnityEngine.Transform PhoneNumberUIScaler::navBackButton
	Transform_t3275118058 * ___navBackButton_17;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(PhoneNumberUIScaler_t4195264923, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(PhoneNumberUIScaler_t4195264923, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_titleDivider_16() { return static_cast<int32_t>(offsetof(PhoneNumberUIScaler_t4195264923, ___titleDivider_16)); }
	inline UISprite_t603616735 * get_titleDivider_16() const { return ___titleDivider_16; }
	inline UISprite_t603616735 ** get_address_of_titleDivider_16() { return &___titleDivider_16; }
	inline void set_titleDivider_16(UISprite_t603616735 * value)
	{
		___titleDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___titleDivider_16, value);
	}

	inline static int32_t get_offset_of_navBackButton_17() { return static_cast<int32_t>(offsetof(PhoneNumberUIScaler_t4195264923, ___navBackButton_17)); }
	inline Transform_t3275118058 * get_navBackButton_17() const { return ___navBackButton_17; }
	inline Transform_t3275118058 ** get_address_of_navBackButton_17() { return &___navBackButton_17; }
	inline void set_navBackButton_17(Transform_t3275118058 * value)
	{
		___navBackButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___navBackButton_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
