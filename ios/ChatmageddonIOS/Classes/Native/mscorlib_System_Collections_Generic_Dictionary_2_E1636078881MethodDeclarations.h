﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m162441308(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1636078881 *, Dictionary_2_t316054179 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m545090937(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m945354737(__this, method) ((  void (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3324078398(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m79818311(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1977403631(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::MoveNext()
#define Enumerator_MoveNext_m2817415184(__this, method) ((  bool (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::get_Current()
#define Enumerator_get_Current_m527684495(__this, method) ((  KeyValuePair_2_t2368366697  (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3830590764(__this, method) ((  String_t* (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1755371756(__this, method) ((  List_1_t2696242213 * (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::Reset()
#define Enumerator_Reset_m911580438(__this, method) ((  void (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::VerifyState()
#define Enumerator_VerifyState_m2711863935(__this, method) ((  void (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4291637025(__this, method) ((  void (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Shield>>::Dispose()
#define Enumerator_Dispose_m99519564(__this, method) ((  void (*) (Enumerator_t1636078881 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
