﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeCalculator/OnZeroHit
struct OnZeroHit_t2015899103;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void StealthModeCalculator/OnZeroHit::.ctor(System.Object,System.IntPtr)
extern "C"  void OnZeroHit__ctor_m2228251438 (OnZeroHit_t2015899103 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator/OnZeroHit::Invoke(System.Boolean)
extern "C"  void OnZeroHit_Invoke_m1504939237 (OnZeroHit_t2015899103 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult StealthModeCalculator/OnZeroHit::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnZeroHit_BeginInvoke_m216750996 (OnZeroHit_t2015899103 * __this, bool ___inStealth0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator/OnZeroHit::EndInvoke(System.IAsyncResult)
extern "C"  void OnZeroHit_EndInvoke_m2545262368 (OnZeroHit_t2015899103 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
