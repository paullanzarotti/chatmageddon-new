﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetAxisFunc
struct GetAxisFunc_t212136019;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICamera/GetAxisFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAxisFunc__ctor_m3905488856 (GetAxisFunc_t212136019 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UICamera/GetAxisFunc::Invoke(System.String)
extern "C"  float GetAxisFunc_Invoke_m2549223234 (GetAxisFunc_t212136019 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICamera/GetAxisFunc::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAxisFunc_BeginInvoke_m2279621307 (GetAxisFunc_t212136019 * __this, String_t* ___name0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UICamera/GetAxisFunc::EndInvoke(System.IAsyncResult)
extern "C"  float GetAxisFunc_EndInvoke_m1245859004 (GetAxisFunc_t212136019 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
