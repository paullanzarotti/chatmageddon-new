﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFaceboo847746195.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFacebo4261124525.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFacebo3391486318.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFacebo3879535236.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFaceboo1229262421.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFacebookG63110230.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFaceboo2109505584.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFaceboo2244501619.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorWrapper2290865494.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_Empty2854396319.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_MockLo531765055.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_MockL1402467746.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_MockS1542668653.h"
#include "Facebook_Unity_Facebook_Unity_AccessTokenRefreshRes170484741.h"
#include "Facebook_Unity_Facebook_Unity_AppInviteResult780050231.h"
#include "Facebook_Unity_Facebook_Unity_AppLinkResult2690573342.h"
#include "Facebook_Unity_Facebook_Unity_AppRequestResult2863124367.h"
#include "Facebook_Unity_Facebook_Unity_GraphResult2230822669.h"
#include "Facebook_Unity_Facebook_Unity_GroupCreateResult605038084.h"
#include "Facebook_Unity_Facebook_Unity_GroupJoinResult2959566706.h"
#include "Facebook_Unity_Facebook_Unity_LoginResult2244791044.h"
#include "Facebook_Unity_Facebook_Unity_PayResult2427424063.h"
#include "Facebook_Unity_Facebook_Unity_ResultBase864677574.h"
#include "Facebook_Unity_Facebook_Unity_ResultContainer2148006712.h"
#include "Facebook_Unity_Facebook_Unity_ShareResult3478035170.h"
#include "Facebook_Unity_Facebook_Unity_AsyncRequestString2888107224.h"
#include "Facebook_Unity_Facebook_Unity_AsyncRequestString_U2524552799.h"
#include "Facebook_Unity_Facebook_Unity_FacebookLogger821309934.h"
#include "Facebook_Unity_Facebook_Unity_FacebookLogger_DebugL156570771.h"
#include "Facebook_Unity_Facebook_Unity_HttpMethod3673888207.h"
#include "Facebook_Unity_Facebook_MiniJSON_Json151732106.h"
#include "Facebook_Unity_Facebook_MiniJSON_Json_Parser3380793361.h"
#include "Facebook_Unity_Facebook_MiniJSON_Json_Parser_TOKEN3846031241.h"
#include "Facebook_Unity_Facebook_MiniJSON_Json_Serializer4234385906.h"
#include "Facebook_Unity_Facebook_Unity_Utilities2249569324.h"
#include "Facebook_Unity_Facebook_Unity_FBUnityUtility4080304177.h"
#include "Facebook_Unity_Facebook_Unity_AsyncRequestStringWr3730940759.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasConstan3977900379.h"
#include "Facebook_Unity_Facebook_Unity_FacebookScheduler3329969041.h"
#include "Facebook_Unity_Facebook_Unity_FacebookScheduler_U31257811067.h"
#include "System_Xml_Linq_U3CModuleU3E3783534214.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"
#include "System_Xml_Linq_System_Xml_Linq_SaveOptions2961005337.h"
#include "System_Xml_Linq_System_Xml_Linq_XAttribute3858477518.h"
#include "System_Xml_Linq_System_Xml_Linq_XCData1306050895.h"
#include "System_Xml_Linq_System_Xml_Linq_XComment1618005895.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer1445911831.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer_U3CNode1473861320.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer_U3CDesce137499000.h"
#include "System_Xml_Linq_System_Xml_Linq_XContainer_U3CDesc2337692641.h"
#include "System_Xml_Linq_System_Xml_Linq_XDeclaration3367285402.h"
#include "System_Xml_Linq_System_Xml_Linq_XDocument2733326047.h"
#include "System_Xml_Linq_System_Xml_Linq_XDocumentType738990919.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement_U3CAttrib3474816789.h"
#include "System_Xml_Linq_System_Xml_Linq_XName785190363.h"
#include "System_Xml_Linq_System_Xml_Linq_XNamespace1613015075.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeDocumentOrder1208684534.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeDocumentOrder4155354831.h"
#include "System_Xml_Linq_System_Xml_Linq_XNodeEqualityCompa3116038621.h"
#include "System_Xml_Linq_System_Xml_Linq_XObject3550811009.h"
#include "System_Xml_Linq_System_Xml_Linq_XProcessingInstruc2854903359.h"
#include "System_Xml_Linq_System_Xml_Linq_XText1860529549.h"
#include "System_Xml_Linq_System_Xml_Linq_XUtil3957506230.h"
#include "System_Xml_Linq_System_Xml_Linq_XUtil_U3CExpandArra507968046.h"
#include "System_Xml_Linq_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_Linq_U3CPrivateImplementationDetailsU3E1703410334.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (IOSFacebook_t847746195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[2] = 
{
	IOSFacebook_t847746195::get_offset_of_limitEventUsage_5(),
	IOSFacebook_t847746195::get_offset_of_iosWrapper_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (NativeDict_t4261124525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[3] = 
{
	NativeDict_t4261124525::get_offset_of_U3CNumEntriesU3Ek__BackingField_0(),
	NativeDict_t4261124525::get_offset_of_U3CKeysU3Ek__BackingField_1(),
	NativeDict_t4261124525::get_offset_of_U3CValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (IOSFacebookGameObject_t3391486318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (IOSFacebookLoader_t3879535236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (EditorFacebook_t1229262421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[3] = 
{
	EditorFacebook_t1229262421::get_offset_of_editorWrapper_4(),
	EditorFacebook_t1229262421::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5(),
	EditorFacebook_t1229262421::get_offset_of_U3CShareDialogModeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (EditorFacebookGameObject_t63110230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (EditorFacebookLoader_t2109505584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (EditorFacebookMockDialog_t2244501619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[4] = 
{
	EditorFacebookMockDialog_t2244501619::get_offset_of_modalRect_2(),
	EditorFacebookMockDialog_t2244501619::get_offset_of_modalStyle_3(),
	EditorFacebookMockDialog_t2244501619::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	EditorFacebookMockDialog_t2244501619::get_offset_of_U3CCallbackIDU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (EditorWrapper_t2290865494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[1] = 
{
	EditorWrapper_t2290865494::get_offset_of_callbackHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (EmptyMockDialog_t2854396319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[1] = 
{
	EmptyMockDialog_t2854396319::get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (MockLoginDialog_t531765055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[1] = 
{
	MockLoginDialog_t531765055::get_offset_of_accessToken_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (U3CSendSuccessResultU3Ec__AnonStorey0_t1402467746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[2] = 
{
	U3CSendSuccessResultU3Ec__AnonStorey0_t1402467746::get_offset_of_facebookID_0(),
	U3CSendSuccessResultU3Ec__AnonStorey0_t1402467746::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (MockShareDialog_t1542668653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[1] = 
{
	MockShareDialog_t1542668653::get_offset_of_U3CSubTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (AccessTokenRefreshResult_t170484741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[1] = 
{
	AccessTokenRefreshResult_t170484741::get_offset_of_U3CAccessTokenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (AppInviteResult_t780050231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (AppLinkResult_t2690573342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[4] = 
{
	AppLinkResult_t2690573342::get_offset_of_U3CUrlU3Ek__BackingField_6(),
	AppLinkResult_t2690573342::get_offset_of_U3CTargetUrlU3Ek__BackingField_7(),
	AppLinkResult_t2690573342::get_offset_of_U3CRefU3Ek__BackingField_8(),
	AppLinkResult_t2690573342::get_offset_of_U3CExtrasU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (AppRequestResult_t2863124367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[2] = 
{
	AppRequestResult_t2863124367::get_offset_of_U3CRequestIDU3Ek__BackingField_6(),
	AppRequestResult_t2863124367::get_offset_of_U3CToU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (GraphResult_t2230822669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[2] = 
{
	GraphResult_t2230822669::get_offset_of_U3CResultListU3Ek__BackingField_6(),
	GraphResult_t2230822669::get_offset_of_U3CTextureU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (GroupCreateResult_t605038084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[1] = 
{
	GroupCreateResult_t605038084::get_offset_of_U3CGroupIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (GroupJoinResult_t2959566706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (LoginResult_t2244791044), -1, sizeof(LoginResult_t2244791044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3134[5] = 
{
	LoginResult_t2244791044_StaticFields::get_offset_of_UserIdKey_6(),
	LoginResult_t2244791044_StaticFields::get_offset_of_ExpirationTimestampKey_7(),
	LoginResult_t2244791044_StaticFields::get_offset_of_PermissionsKey_8(),
	LoginResult_t2244791044_StaticFields::get_offset_of_AccessTokenKey_9(),
	LoginResult_t2244791044::get_offset_of_U3CAccessTokenU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (PayResult_t2427424063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (ResultBase_t864677574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[6] = 
{
	ResultBase_t864677574::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	ResultBase_t864677574::get_offset_of_U3CResultDictionaryU3Ek__BackingField_1(),
	ResultBase_t864677574::get_offset_of_U3CRawResultU3Ek__BackingField_2(),
	ResultBase_t864677574::get_offset_of_U3CCancelledU3Ek__BackingField_3(),
	ResultBase_t864677574::get_offset_of_U3CCallbackIdU3Ek__BackingField_4(),
	ResultBase_t864677574::get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (ResultContainer_t2148006712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[3] = 
{
	0,
	ResultContainer_t2148006712::get_offset_of_U3CRawResultU3Ek__BackingField_1(),
	ResultContainer_t2148006712::get_offset_of_U3CResultDictionaryU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (ShareResult_t3478035170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[1] = 
{
	ShareResult_t3478035170::get_offset_of_U3CPostIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (AsyncRequestString_t2888107224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[5] = 
{
	AsyncRequestString_t2888107224::get_offset_of_url_2(),
	AsyncRequestString_t2888107224::get_offset_of_method_3(),
	AsyncRequestString_t2888107224::get_offset_of_formData_4(),
	AsyncRequestString_t2888107224::get_offset_of_query_5(),
	AsyncRequestString_t2888107224::get_offset_of_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (U3CStartU3Ec__Iterator0_t2524552799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[5] = 
{
	U3CStartU3Ec__Iterator0_t2524552799::get_offset_of_U3CwwwU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2524552799::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2524552799::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2524552799::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2524552799::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (FacebookLogger_t821309934), -1, sizeof(FacebookLogger_t821309934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3141[1] = 
{
	FacebookLogger_t821309934_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (DebugLogger_t156570771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (HttpMethod_t3673888207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3143[4] = 
{
	HttpMethod_t3673888207::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (Json_t151732106), -1, sizeof(Json_t151732106_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3145[1] = 
{
	Json_t151732106_StaticFields::get_offset_of_numberFormat_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (Parser_t3380793361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[1] = 
{
	Parser_t3380793361::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (TOKEN_t3846031241)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3147[13] = 
{
	TOKEN_t3846031241::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (Serializer_t4234385906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[1] = 
{
	Serializer_t4234385906::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (Utilities_t2249569324), -1, sizeof(Utilities_t2249569324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3149[2] = 
{
	Utilities_t2249569324_StaticFields::get_offset_of_commandLineArguments_0(),
	Utilities_t2249569324_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (FBUnityUtility_t4080304177), -1, sizeof(FBUnityUtility_t4080304177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3151[1] = 
{
	FBUnityUtility_t4080304177_StaticFields::get_offset_of_asyncRequestStringWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (AsyncRequestStringWrapper_t3730940759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (CanvasConstants_t3977900379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (FacebookScheduler_t3329969041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (U3CDelayEventU3Ec__Iterator0_t1257811067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[5] = 
{
	U3CDelayEventU3Ec__Iterator0_t1257811067::get_offset_of_delay_0(),
	U3CDelayEventU3Ec__Iterator0_t1257811067::get_offset_of_action_1(),
	U3CDelayEventU3Ec__Iterator0_t1257811067::get_offset_of_U24current_2(),
	U3CDelayEventU3Ec__Iterator0_t1257811067::get_offset_of_U24disposing_3(),
	U3CDelayEventU3Ec__Iterator0_t1257811067::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (LoadOptions_t53198144)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3159[5] = 
{
	LoadOptions_t53198144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (SaveOptions_t2961005337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3160[3] = 
{
	SaveOptions_t2961005337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (XAttribute_t3858477518), -1, sizeof(XAttribute_t3858477518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3161[6] = 
{
	XAttribute_t3858477518_StaticFields::get_offset_of_empty_array_4(),
	XAttribute_t3858477518::get_offset_of_name_5(),
	XAttribute_t3858477518::get_offset_of_value_6(),
	XAttribute_t3858477518::get_offset_of_next_7(),
	XAttribute_t3858477518::get_offset_of_previous_8(),
	XAttribute_t3858477518_StaticFields::get_offset_of_escapeChars_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (XCData_t1306050895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (XComment_t1618005895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[1] = 
{
	XComment_t1618005895::get_offset_of_value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (XContainer_t1445911831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[2] = 
{
	XContainer_t1445911831::get_offset_of_first_8(),
	XContainer_t1445911831::get_offset_of_last_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (U3CNodesU3Ec__Iterator1A_t1473861320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[5] = 
{
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U3CnU3E__0_0(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U3CnextU3E__1_1(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U24PC_2(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U24current_3(),
	U3CNodesU3Ec__Iterator1A_t1473861320::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (U3CDescendantNodesU3Ec__Iterator1B_t137499000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[8] = 
{
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U3CU24s_45U3E__0_0(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U3CnU3E__1_1(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U3CcU3E__2_2(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U3CU24s_46U3E__3_3(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U3CdU3E__4_4(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U24PC_5(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U24current_6(),
	U3CDescendantNodesU3Ec__Iterator1B_t137499000::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (U3CDescendantsU3Ec__Iterator1C_t2337692641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[6] = 
{
	U3CDescendantsU3Ec__Iterator1C_t2337692641::get_offset_of_U3CU24s_47U3E__0_0(),
	U3CDescendantsU3Ec__Iterator1C_t2337692641::get_offset_of_U3CnU3E__1_1(),
	U3CDescendantsU3Ec__Iterator1C_t2337692641::get_offset_of_U3CelU3E__2_2(),
	U3CDescendantsU3Ec__Iterator1C_t2337692641::get_offset_of_U24PC_3(),
	U3CDescendantsU3Ec__Iterator1C_t2337692641::get_offset_of_U24current_4(),
	U3CDescendantsU3Ec__Iterator1C_t2337692641::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (XDeclaration_t3367285402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[3] = 
{
	XDeclaration_t3367285402::get_offset_of_encoding_0(),
	XDeclaration_t3367285402::get_offset_of_standalone_1(),
	XDeclaration_t3367285402::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (XDocument_t2733326047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[1] = 
{
	XDocument_t2733326047::get_offset_of_xmldecl_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (XDocumentType_t738990919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[4] = 
{
	XDocumentType_t738990919::get_offset_of_name_8(),
	XDocumentType_t738990919::get_offset_of_pubid_9(),
	XDocumentType_t738990919::get_offset_of_sysid_10(),
	XDocumentType_t738990919::get_offset_of_intSubset_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (XElement_t553821050), -1, sizeof(XElement_t553821050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3171[5] = 
{
	XElement_t553821050_StaticFields::get_offset_of_emptySequence_10(),
	XElement_t553821050::get_offset_of_name_11(),
	XElement_t553821050::get_offset_of_attr_first_12(),
	XElement_t553821050::get_offset_of_attr_last_13(),
	XElement_t553821050::get_offset_of_explicit_is_empty_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (U3CAttributesU3Ec__Iterator20_t3474816789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[5] = 
{
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U3CaU3E__0_0(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U3CnextU3E__1_1(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U24PC_2(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U24current_3(),
	U3CAttributesU3Ec__Iterator20_t3474816789::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (XName_t785190363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[2] = 
{
	XName_t785190363::get_offset_of_local_0(),
	XName_t785190363::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (XNamespace_t1613015075), -1, sizeof(XNamespace_t1613015075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3174[6] = 
{
	XNamespace_t1613015075_StaticFields::get_offset_of_blank_0(),
	XNamespace_t1613015075_StaticFields::get_offset_of_xml_1(),
	XNamespace_t1613015075_StaticFields::get_offset_of_xmlns_2(),
	XNamespace_t1613015075_StaticFields::get_offset_of_nstable_3(),
	XNamespace_t1613015075::get_offset_of_uri_4(),
	XNamespace_t1613015075::get_offset_of_table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (XNode_t2707504214), -1, sizeof(XNode_t2707504214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3175[4] = 
{
	XNode_t2707504214_StaticFields::get_offset_of_eq_comparer_4(),
	XNode_t2707504214_StaticFields::get_offset_of_order_comparer_5(),
	XNode_t2707504214::get_offset_of_previous_6(),
	XNode_t2707504214::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (XNodeDocumentOrderComparer_t1208684534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (CompareResult_t4155354831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3177[9] = 
{
	CompareResult_t4155354831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (XNodeEqualityComparer_t3116038621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (XObject_t3550811009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[4] = 
{
	XObject_t3550811009::get_offset_of_owner_0(),
	XObject_t3550811009::get_offset_of_baseuri_1(),
	XObject_t3550811009::get_offset_of_line_2(),
	XObject_t3550811009::get_offset_of_column_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (XProcessingInstruction_t2854903359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[2] = 
{
	XProcessingInstruction_t2854903359::get_offset_of_name_8(),
	XProcessingInstruction_t2854903359::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (XText_t1860529549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[1] = 
{
	XText_t1860529549::get_offset_of_value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (XUtil_t3957506230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (U3CExpandArrayU3Ec__Iterator25_t507968046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[9] = 
{
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_o_0(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CnU3E__0_1(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CU24s_86U3E__1_2(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CobjU3E__2_3(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CU24s_87U3E__3_4(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CooU3E__4_5(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U24PC_6(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U24current_7(),
	U3CExpandArrayU3Ec__Iterator25_t507968046::get_offset_of_U3CU24U3Eo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3184[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (U24ArrayTypeU2416_t1703410337)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410337 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
