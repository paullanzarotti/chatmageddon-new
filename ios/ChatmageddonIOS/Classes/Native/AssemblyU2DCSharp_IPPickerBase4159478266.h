﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// IPPickerBase/PickerValueUpdatedHandler
struct PickerValueUpdatedHandler_t2401088127;
// IPCycler
struct IPCycler_t1336138445;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot752586349.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPPickerBase
struct  IPPickerBase_t4159478266  : public MonoBehaviour_t1158329972
{
public:
	// System.String IPPickerBase::pickerName
	String_t* ___pickerName_2;
	// System.Boolean IPPickerBase::initInAwake
	bool ___initInAwake_3;
	// System.Int32 IPPickerBase::widgetsDepth
	int32_t ___widgetsDepth_4;
	// UnityEngine.Vector3 IPPickerBase::widgetOffsetInPicker
	Vector3_t2243707580  ___widgetOffsetInPicker_5;
	// UIWidget/Pivot IPPickerBase::widgetsPivot
	int32_t ___widgetsPivot_6;
	// UnityEngine.Color IPPickerBase::widgetsColor
	Color_t2020392075  ___widgetsColor_7;
	// IPPickerBase/PickerValueUpdatedHandler IPPickerBase::onPickerValueUpdated
	PickerValueUpdatedHandler_t2401088127 * ___onPickerValueUpdated_8;
	// IPCycler IPPickerBase::cycler
	IPCycler_t1336138445 * ___cycler_9;
	// System.Int32 IPPickerBase::_nbOfWidgets
	int32_t ____nbOfWidgets_10;
	// System.Int32 IPPickerBase::_nbOfVirtualElements
	int32_t ____nbOfVirtualElements_11;
	// System.Int32 IPPickerBase::_selectedIndex
	int32_t ____selectedIndex_12;

public:
	inline static int32_t get_offset_of_pickerName_2() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___pickerName_2)); }
	inline String_t* get_pickerName_2() const { return ___pickerName_2; }
	inline String_t** get_address_of_pickerName_2() { return &___pickerName_2; }
	inline void set_pickerName_2(String_t* value)
	{
		___pickerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___pickerName_2, value);
	}

	inline static int32_t get_offset_of_initInAwake_3() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___initInAwake_3)); }
	inline bool get_initInAwake_3() const { return ___initInAwake_3; }
	inline bool* get_address_of_initInAwake_3() { return &___initInAwake_3; }
	inline void set_initInAwake_3(bool value)
	{
		___initInAwake_3 = value;
	}

	inline static int32_t get_offset_of_widgetsDepth_4() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___widgetsDepth_4)); }
	inline int32_t get_widgetsDepth_4() const { return ___widgetsDepth_4; }
	inline int32_t* get_address_of_widgetsDepth_4() { return &___widgetsDepth_4; }
	inline void set_widgetsDepth_4(int32_t value)
	{
		___widgetsDepth_4 = value;
	}

	inline static int32_t get_offset_of_widgetOffsetInPicker_5() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___widgetOffsetInPicker_5)); }
	inline Vector3_t2243707580  get_widgetOffsetInPicker_5() const { return ___widgetOffsetInPicker_5; }
	inline Vector3_t2243707580 * get_address_of_widgetOffsetInPicker_5() { return &___widgetOffsetInPicker_5; }
	inline void set_widgetOffsetInPicker_5(Vector3_t2243707580  value)
	{
		___widgetOffsetInPicker_5 = value;
	}

	inline static int32_t get_offset_of_widgetsPivot_6() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___widgetsPivot_6)); }
	inline int32_t get_widgetsPivot_6() const { return ___widgetsPivot_6; }
	inline int32_t* get_address_of_widgetsPivot_6() { return &___widgetsPivot_6; }
	inline void set_widgetsPivot_6(int32_t value)
	{
		___widgetsPivot_6 = value;
	}

	inline static int32_t get_offset_of_widgetsColor_7() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___widgetsColor_7)); }
	inline Color_t2020392075  get_widgetsColor_7() const { return ___widgetsColor_7; }
	inline Color_t2020392075 * get_address_of_widgetsColor_7() { return &___widgetsColor_7; }
	inline void set_widgetsColor_7(Color_t2020392075  value)
	{
		___widgetsColor_7 = value;
	}

	inline static int32_t get_offset_of_onPickerValueUpdated_8() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___onPickerValueUpdated_8)); }
	inline PickerValueUpdatedHandler_t2401088127 * get_onPickerValueUpdated_8() const { return ___onPickerValueUpdated_8; }
	inline PickerValueUpdatedHandler_t2401088127 ** get_address_of_onPickerValueUpdated_8() { return &___onPickerValueUpdated_8; }
	inline void set_onPickerValueUpdated_8(PickerValueUpdatedHandler_t2401088127 * value)
	{
		___onPickerValueUpdated_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPickerValueUpdated_8, value);
	}

	inline static int32_t get_offset_of_cycler_9() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ___cycler_9)); }
	inline IPCycler_t1336138445 * get_cycler_9() const { return ___cycler_9; }
	inline IPCycler_t1336138445 ** get_address_of_cycler_9() { return &___cycler_9; }
	inline void set_cycler_9(IPCycler_t1336138445 * value)
	{
		___cycler_9 = value;
		Il2CppCodeGenWriteBarrier(&___cycler_9, value);
	}

	inline static int32_t get_offset_of__nbOfWidgets_10() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ____nbOfWidgets_10)); }
	inline int32_t get__nbOfWidgets_10() const { return ____nbOfWidgets_10; }
	inline int32_t* get_address_of__nbOfWidgets_10() { return &____nbOfWidgets_10; }
	inline void set__nbOfWidgets_10(int32_t value)
	{
		____nbOfWidgets_10 = value;
	}

	inline static int32_t get_offset_of__nbOfVirtualElements_11() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ____nbOfVirtualElements_11)); }
	inline int32_t get__nbOfVirtualElements_11() const { return ____nbOfVirtualElements_11; }
	inline int32_t* get_address_of__nbOfVirtualElements_11() { return &____nbOfVirtualElements_11; }
	inline void set__nbOfVirtualElements_11(int32_t value)
	{
		____nbOfVirtualElements_11 = value;
	}

	inline static int32_t get_offset_of__selectedIndex_12() { return static_cast<int32_t>(offsetof(IPPickerBase_t4159478266, ____selectedIndex_12)); }
	inline int32_t get__selectedIndex_12() const { return ____selectedIndex_12; }
	inline int32_t* get_address_of__selectedIndex_12() { return &____selectedIndex_12; }
	inline void set__selectedIndex_12(int32_t value)
	{
		____selectedIndex_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
