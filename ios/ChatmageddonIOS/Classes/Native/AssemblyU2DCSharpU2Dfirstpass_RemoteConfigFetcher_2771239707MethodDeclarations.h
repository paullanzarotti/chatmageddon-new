﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RemoteConfigFetcher/<fetch>c__Iterator0
struct U3CfetchU3Ec__Iterator0_t2771239707;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RemoteConfigFetcher/<fetch>c__Iterator0::.ctor()
extern "C"  void U3CfetchU3Ec__Iterator0__ctor_m3726455142 (U3CfetchU3Ec__Iterator0_t2771239707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RemoteConfigFetcher/<fetch>c__Iterator0::MoveNext()
extern "C"  bool U3CfetchU3Ec__Iterator0_MoveNext_m1212561046 (U3CfetchU3Ec__Iterator0_t2771239707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RemoteConfigFetcher/<fetch>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CfetchU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2578989152 (U3CfetchU3Ec__Iterator0_t2771239707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RemoteConfigFetcher/<fetch>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CfetchU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m501334584 (U3CfetchU3Ec__Iterator0_t2771239707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteConfigFetcher/<fetch>c__Iterator0::Dispose()
extern "C"  void U3CfetchU3Ec__Iterator0_Dispose_m3703280639 (U3CfetchU3Ec__Iterator0_t2771239707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RemoteConfigFetcher/<fetch>c__Iterator0::Reset()
extern "C"  void U3CfetchU3Ec__Iterator0_Reset_m1009304049 (U3CfetchU3Ec__Iterator0_t2771239707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
