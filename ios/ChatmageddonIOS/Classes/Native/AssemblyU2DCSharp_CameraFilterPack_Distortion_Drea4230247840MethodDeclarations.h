﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Dream2
struct CameraFilterPack_Distortion_Dream2_t4230247840;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Dream2::.ctor()
extern "C"  void CameraFilterPack_Distortion_Dream2__ctor_m257673633 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Dream2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Dream2_get_material_m1054171394 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::Start()
extern "C"  void CameraFilterPack_Distortion_Dream2_Start_m2067592129 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Dream2_OnRenderImage_m209914481 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Dream2_OnValidate_m1894861368 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::Update()
extern "C"  void CameraFilterPack_Distortion_Dream2_Update_m1210738570 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Dream2_OnDisable_m839345748 (CameraFilterPack_Distortion_Dream2_t4230247840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
