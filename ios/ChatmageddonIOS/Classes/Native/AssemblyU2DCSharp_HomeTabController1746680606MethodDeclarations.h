﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeTabController
struct HomeTabController_t1746680606;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeTabController::.ctor()
extern "C"  void HomeTabController__ctor_m546303903 (HomeTabController_t1746680606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeTabController::Start()
extern "C"  void HomeTabController_Start_m2989548387 (HomeTabController_t1746680606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeTabController::OnTweenFinished()
extern "C"  void HomeTabController_OnTweenFinished_m44308765 (HomeTabController_t1746680606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeTabController::ShowTab(System.Boolean)
extern "C"  void HomeTabController_ShowTab_m4023484632 (HomeTabController_t1746680606 * __this, bool ___show0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeTabController::PlayPositionTween(System.Boolean,System.Single)
extern "C"  void HomeTabController_PlayPositionTween_m1463538197 (HomeTabController_t1746680606 * __this, bool ___forward0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeTabController::ChangeLabelLayer(System.Int32)
extern "C"  void HomeTabController_ChangeLabelLayer_m3532685789 (HomeTabController_t1746680606 * __this, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
