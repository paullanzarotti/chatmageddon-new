﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProfileNavigateBackwardsButton4036937052.h"
#include "AssemblyU2DCSharp_ProfileNavigateForwardsButton1057056812.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "AssemblyU2DCSharp_ProfileNavigationController2708779591.h"
#include "AssemblyU2DCSharp_SceneLoaderUI1806459353.h"
#include "AssemblyU2DCSharp_ChatLoader2661087257.h"
#include "AssemblyU2DCSharp_LauncherLoader1212054665.h"
#include "AssemblyU2DCSharp_LeaderboardLoader1482897644.h"
#include "AssemblyU2DCSharp_Scene4200804392.h"
#include "AssemblyU2DCSharp_SceneLoader1952549817.h"
#include "AssemblyU2DCSharp_SceneLoader_U3CwaitU3Ec__Iterator010913998.h"
#include "AssemblyU2DCSharp_SettingsLoader2246286398.h"
#include "AssemblyU2DCSharp_ChatLoaderUIScaler296991803.h"
#include "AssemblyU2DCSharp_LauncherLoaderUIScaler1442786027.h"
#include "AssemblyU2DCSharp_LeaderboardLoaderUIScaler1071922518.h"
#include "AssemblyU2DCSharp_SettingsLoaderUIScaler190890564.h"
#include "AssemblyU2DCSharp_ShieldProgressBar2832359661.h"
#include "AssemblyU2DCSharp_ClearFullStatusButton3074485066.h"
#include "AssemblyU2DCSharp_CloseStatusButton3819466894.h"
#include "AssemblyU2DCSharp_StatusILP1076408775.h"
#include "AssemblyU2DCSharp_StatusIncomingNavScreen3315318769.h"
#include "AssemblyU2DCSharp_ActiveMissileUI356948948.h"
#include "AssemblyU2DCSharp_InactiveMissileUI409112257.h"
#include "AssemblyU2DCSharp_ItemFinishTimePopulator2301874695.h"
#include "AssemblyU2DCSharp_MineStatusUI2517460477.h"
#include "AssemblyU2DCSharp_MissileArriveTimer44929408.h"
#include "AssemblyU2DCSharp_MissileArriveTimer_OnZeroHit3980744303.h"
#include "AssemblyU2DCSharp_MissileArriveTimer_OnTimerUpdate2669454040.h"
#include "AssemblyU2DCSharp_MissileArriveTimer_U3CCalculateA3440279944.h"
#include "AssemblyU2DCSharp_MissilePointsPopulator2262625279.h"
#include "AssemblyU2DCSharp_ResultSprite623266392.h"
#include "AssemblyU2DCSharp_ContentColour2912031623.h"
#include "AssemblyU2DCSharp_MissileTrajectoryDisplay1441887151.h"
#include "AssemblyU2DCSharp_MissileTravelController534821896.h"
#include "AssemblyU2DCSharp_PendingMissileUI3261005045.h"
#include "AssemblyU2DCSharp_ShieldDisplayController1783798351.h"
#include "AssemblyU2DCSharp_StatusAttackButton181803962.h"
#include "AssemblyU2DCSharp_StatusChatButton749649916.h"
#include "AssemblyU2DCSharp_StatusDoubleStateButton3675410548.h"
#include "AssemblyU2DCSharp_StatusSwipeState381899333.h"
#include "AssemblyU2DCSharp_StatusItemSwipe1669089155.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"
#include "AssemblyU2DCSharp_StatusListItem459202613.h"
#include "AssemblyU2DCSharp_StatusListItem_U3CWaitToAllowToa1783910557.h"
#include "AssemblyU2DCSharp_StatusSwipeComponent1744404785.h"
#include "AssemblyU2DCSharp_StatusUI2174902556.h"
#include "AssemblyU2DCSharp_StatusListType2488921270.h"
#include "AssemblyU2DCSharp_StatusManager2720247037.h"
#include "AssemblyU2DCSharp_StatusNavigateForwardsButton4116613479.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "AssemblyU2DCSharp_StatusNavigationController3357829236.h"
#include "AssemblyU2DCSharp_StatusOutgoingNavScreen2187904061.h"
#include "AssemblyU2DCSharp_StatusListItemUIScaler1403897979.h"
#include "AssemblyU2DCSharp_StatusUIScaler1399255572.h"
#include "AssemblyU2DCSharp_BottomButtonsController3250435864.h"
#include "AssemblyU2DCSharp_BuyItemButton2593913003.h"
#include "AssemblyU2DCSharp_BuyItemButton_U3CPurchaseShieldI4155480970.h"
#include "AssemblyU2DCSharp_BuyItemButton_U3CPurchaseStaticI2538291704.h"
#include "AssemblyU2DCSharp_CloseStoreButton141935289.h"
#include "AssemblyU2DCSharp_InventoryItemButton3515382031.h"
#include "AssemblyU2DCSharp_InventoryManager38269895.h"
#include "AssemblyU2DCSharp_InventoryManager_U3CCheckStandar1452096246.h"
#include "AssemblyU2DCSharp_InventoryPanel2677987186.h"
#include "AssemblyU2DCSharp_ItemInfoController4231956141.h"
#include "AssemblyU2DCSharp_ItemInfoController_OnPopulationC1301655353.h"
#include "AssemblyU2DCSharp_ItemInfoController_U3CWaitForSec3866018642.h"
#include "AssemblyU2DCSharp_ItemSelectionSwipe2104393431.h"
#include "AssemblyU2DCSharp_ItemSwipeController3011021799.h"
#include "AssemblyU2DCSharp_ItemSwipeController_OnLeftComple1529246307.h"
#include "AssemblyU2DCSharp_ItemSwipeController_OnRightCompl1521737374.h"
#include "AssemblyU2DCSharp_ItemSwipeController_OnHideComple4141700430.h"
#include "AssemblyU2DCSharp_MissileSwipeSelection3143859656.h"
#include "AssemblyU2DCSharp_PanelUIScaler4049405372.h"
#include "AssemblyU2DCSharp_BuyBucksPUButton1827949611.h"
#include "AssemblyU2DCSharp_CloseStorePUButton1133311818.h"
#include "AssemblyU2DCSharp_NotEnoughBucksPU2637555884.h"
#include "AssemblyU2DCSharp_PurchasePU4126238880.h"
#include "AssemblyU2DCSharp_PurchasePU_U3CWaitForSecondsU3Ec1848252903.h"
#include "AssemblyU2DCSharp_StorePopUpController952004567.h"
#include "AssemblyU2DCSharp_ProductCatergoryNavItem1498614491.h"
#include "AssemblyU2DCSharp_ShieldSliderType2078175364.h"
#include "AssemblyU2DCSharp_ShieldSlider1483508804.h"
#include "AssemblyU2DCSharp_SliderControllerSlider2300726312.h"
#include "AssemblyU2DCSharp_StoreManager650776524.h"
#include "AssemblyU2DCSharp_StoreManager_U3CWaitToCheckStart2500631203.h"
#include "AssemblyU2DCSharp_StoreManager_U3CScaleAndHideU3Ec2634602714.h"
#include "AssemblyU2DCSharp_StoreManager_U3CScaleAndShowU3Ec_815003340.h"
#include "AssemblyU2DCSharp_StoreManager_U3CLoadDefaultModelU378789387.h"
#include "AssemblyU2DCSharp_StoreTargetsButton3560946605.h"
#include "AssemblyU2DCSharp_StoreUIScaler2133310967.h"
#include "AssemblyU2DCSharp_HomeTabController1746680606.h"
#include "AssemblyU2DCSharp_HomeButton266229155.h"
#include "AssemblyU2DCSharp_GlobalManagersController3516162077.h"
#include "AssemblyU2DCSharp_InitSceneManager1690944945.h"
#include "AssemblyU2DCSharp_InitSceneManager_U3CWaitForScree2950940155.h"
#include "AssemblyU2DCSharp_InitSceneManager_U3CWaitForSecond235271859.h"
#include "AssemblyU2DCSharp_InitSceneUIScaler296852604.h"
#include "AssemblyU2DCSharp_AllTimeLeaderboardNavScreen2566129190.h"
#include "AssemblyU2DCSharp_DailyLeaderboardNavScreen4105499011.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof (ProfileNavigateBackwardsButton_t4036937052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5800[2] = 
{
	ProfileNavigateBackwardsButton_t4036937052::get_offset_of_buttonSprite_5(),
	ProfileNavigateBackwardsButton_t4036937052::get_offset_of_buttonLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof (ProfileNavigateForwardsButton_t1057056812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5801[1] = 
{
	ProfileNavigateForwardsButton_t1057056812::get_offset_of_navScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof (ProfileNavScreen_t1126657854)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5802[4] = 
{
	ProfileNavScreen_t1126657854::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof (ProfileNavigationController_t2708779591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5803[8] = 
{
	ProfileNavigationController_t2708779591::get_offset_of_titleLabel_8(),
	ProfileNavigationController_t2708779591::get_offset_of_homeObject_9(),
	ProfileNavigationController_t2708779591::get_offset_of_profileObject_10(),
	ProfileNavigationController_t2708779591::get_offset_of_mainNavigateBackwardButton_11(),
	ProfileNavigationController_t2708779591::get_offset_of_mainNavigateForwardButton_12(),
	ProfileNavigationController_t2708779591::get_offset_of_activePos_13(),
	ProfileNavigationController_t2708779591::get_offset_of_leftInactivePos_14(),
	ProfileNavigationController_t2708779591::get_offset_of_rightInactivePos_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof (SceneLoaderUI_t1806459353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof (ChatLoader_t2661087257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5805[5] = 
{
	ChatLoader_t2661087257::get_offset_of_chatTitle_2(),
	ChatLoader_t2661087257::get_offset_of_backButtonSprite_3(),
	ChatLoader_t2661087257::get_offset_of_forwardButtonSprite_4(),
	ChatLoader_t2661087257::get_offset_of_newMessageButton_5(),
	ChatLoader_t2661087257::get_offset_of_newMessage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof (LauncherLoader_t1212054665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5806[6] = 
{
	LauncherLoader_t1212054665::get_offset_of_sequenceReport_2(),
	LauncherLoader_t1212054665::get_offset_of_finishSequenceReport_3(),
	LauncherLoader_t1212054665::get_offset_of_backfireLabel_4(),
	LauncherLoader_t1212054665::get_offset_of_initBulb_5(),
	LauncherLoader_t1212054665::get_offset_of_engageBulb_6(),
	LauncherLoader_t1212054665::get_offset_of_launchBulb_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof (LeaderboardLoader_t1482897644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof (Scene_t4200804392)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5808[6] = 
{
	Scene_t4200804392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof (SceneLoader_t1952549817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5809[4] = 
{
	SceneLoader_t1952549817::get_offset_of_sceneToLoad_2(),
	SceneLoader_t1952549817::get_offset_of_UIParent_3(),
	SceneLoader_t1952549817::get_offset_of_sceneUI_4(),
	SceneLoader_t1952549817::get_offset_of_UIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof (U3CwaitU3Ec__Iterator0_t10913998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5810[4] = 
{
	U3CwaitU3Ec__Iterator0_t10913998::get_offset_of_U24this_0(),
	U3CwaitU3Ec__Iterator0_t10913998::get_offset_of_U24current_1(),
	U3CwaitU3Ec__Iterator0_t10913998::get_offset_of_U24disposing_2(),
	U3CwaitU3Ec__Iterator0_t10913998::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof (SettingsLoader_t2246286398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof (ChatLoaderUIScaler_t296991803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5812[3] = 
{
	ChatLoaderUIScaler_t296991803::get_offset_of_titleSeperator_14(),
	ChatLoaderUIScaler_t296991803::get_offset_of_navigateBackButton_15(),
	ChatLoaderUIScaler_t296991803::get_offset_of_newChatButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof (LauncherLoaderUIScaler_t1442786027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5813[12] = 
{
	LauncherLoaderUIScaler_t1442786027::get_offset_of_animationOutline_14(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_animationMask_15(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_statusButton_16(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_statusBackground_17(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_initBulb_18(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_launchBulb_19(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_skipButton_20(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_skipBackground_21(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_skipLabel_22(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_reportBackground_23(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_reportTitle_24(),
	LauncherLoaderUIScaler_t1442786027::get_offset_of_reportLabels_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof (LeaderboardLoaderUIScaler_t1071922518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5814[13] = 
{
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_titleSeperator_14(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_navigateBackButton_15(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_timeTabBackground_16(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_dailyTabButton_17(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_allTimeTabButton_18(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_timeTabSeperator_19(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_groupTabBackground_20(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_innerTabButton_21(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_outerTabButton_22(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_worldTabButton_23(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_groupTabSeperator_24(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_groupLeftSeperator_25(),
	LeaderboardLoaderUIScaler_t1071922518::get_offset_of_groupRightSeperator_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof (SettingsLoaderUIScaler_t190890564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5815[2] = 
{
	SettingsLoaderUIScaler_t190890564::get_offset_of_titleSeperator_14(),
	SettingsLoaderUIScaler_t190890564::get_offset_of_navigateBackButton_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof (ShieldProgressBar_t2832359661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5816[7] = 
{
	ShieldProgressBar_t2832359661::get_offset_of_bronzeColour_9(),
	ShieldProgressBar_t2832359661::get_offset_of_silverColour_10(),
	ShieldProgressBar_t2832359661::get_offset_of_goldColour_11(),
	ShieldProgressBar_t2832359661::get_offset_of_mineColour_12(),
	ShieldProgressBar_t2832359661::get_offset_of_foregroundBar_13(),
	ShieldProgressBar_t2832359661::get_offset_of_shieldLabel_14(),
	ShieldProgressBar_t2832359661::get_offset_of_activeLabel_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof (ClearFullStatusButton_t3074485066), -1, sizeof(ClearFullStatusButton_t3074485066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5817[1] = 
{
	ClearFullStatusButton_t3074485066_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof (CloseStatusButton_t3819466894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5818[1] = 
{
	CloseStatusButton_t3819466894::get_offset_of_panel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof (StatusILP_t1076408775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5819[4] = 
{
	StatusILP_t1076408775::get_offset_of_maxNameLength_33(),
	StatusILP_t1076408775::get_offset_of_listLoaded_34(),
	StatusILP_t1076408775::get_offset_of_statusArray_35(),
	StatusILP_t1076408775::get_offset_of_outgoingList_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof (StatusIncomingNavScreen_t3315318769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5820[4] = 
{
	StatusIncomingNavScreen_t3315318769::get_offset_of_hasIncomingUI_3(),
	StatusIncomingNavScreen_t3315318769::get_offset_of_noIncomingUI_4(),
	StatusIncomingNavScreen_t3315318769::get_offset_of_incomingList_5(),
	StatusIncomingNavScreen_t3315318769::get_offset_of_uiClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof (ActiveMissileUI_t356948948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5821[9] = 
{
	ActiveMissileUI_t356948948::get_offset_of_travelController_4(),
	ActiveMissileUI_t356948948::get_offset_of_initalsLabel_5(),
	ActiveMissileUI_t356948948::get_offset_of_shieldDisplay_6(),
	ActiveMissileUI_t356948948::get_offset_of_flameSprite_7(),
	ActiveMissileUI_t356948948::get_offset_of_scaleTween_8(),
	ActiveMissileUI_t356948948::get_offset_of_shieldActive_9(),
	ActiveMissileUI_t356948948::get_offset_of_warningTriangle_10(),
	ActiveMissileUI_t356948948::get_offset_of_zeroHit_11(),
	ActiveMissileUI_t356948948::get_offset_of_uiUnloading_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof (InactiveMissileUI_t409112257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5822[9] = 
{
	InactiveMissileUI_t409112257::get_offset_of_stateInfoLabel_4(),
	InactiveMissileUI_t409112257::get_offset_of_stateTimeLabel_5(),
	InactiveMissileUI_t409112257::get_offset_of_pointsInfoLabel_6(),
	InactiveMissileUI_t409112257::get_offset_of_pointsLabel_7(),
	InactiveMissileUI_t409112257::get_offset_of_flameSprite_8(),
	InactiveMissileUI_t409112257::get_offset_of_flameAnim_9(),
	InactiveMissileUI_t409112257::get_offset_of_avatarFadeColour_10(),
	InactiveMissileUI_t409112257::get_offset_of_scaleTween_11(),
	InactiveMissileUI_t409112257::get_offset_of_uiUnloading_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof (ItemFinishTimePopulator_t2301874695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5823[2] = 
{
	ItemFinishTimePopulator_t2301874695::get_offset_of_dateLabel_2(),
	ItemFinishTimePopulator_t2301874695::get_offset_of_timeLabel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof (MineStatusUI_t2517460477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5824[7] = 
{
	MineStatusUI_t2517460477::get_offset_of_stateInfoLabel_4(),
	MineStatusUI_t2517460477::get_offset_of_stateTimeLabel_5(),
	MineStatusUI_t2517460477::get_offset_of_pointsInfoLabel_6(),
	MineStatusUI_t2517460477::get_offset_of_pointsLabel_7(),
	MineStatusUI_t2517460477::get_offset_of_flameSprite_8(),
	MineStatusUI_t2517460477::get_offset_of_flameAnim_9(),
	MineStatusUI_t2517460477::get_offset_of_avatarFadeColour_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof (MissileArriveTimer_t44929408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5825[5] = 
{
	MissileArriveTimer_t44929408::get_offset_of_timeLabel_2(),
	MissileArriveTimer_t44929408::get_offset_of_missileItem_3(),
	MissileArriveTimer_t44929408::get_offset_of_calcRoutine_4(),
	MissileArriveTimer_t44929408::get_offset_of_onZeroHit_5(),
	MissileArriveTimer_t44929408::get_offset_of_onTimerUpdate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof (OnZeroHit_t3980744303), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof (OnTimerUpdate_t2669454040), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof (U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5828[5] = 
{
	U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944::get_offset_of_U3CdifferenceU3E__0_0(),
	U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944::get_offset_of_U24this_1(),
	U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944::get_offset_of_U24current_2(),
	U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944::get_offset_of_U24disposing_3(),
	U3CCalculateArriveTimeU3Ec__Iterator0_t3440279944::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof (MissilePointsPopulator_t2262625279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5829[7] = 
{
	MissilePointsPopulator_t2262625279::get_offset_of_pointsStateLabel_2(),
	MissilePointsPopulator_t2262625279::get_offset_of_pointsLabel_3(),
	MissilePointsPopulator_t2262625279::get_offset_of_ptsLabel_4(),
	MissilePointsPopulator_t2262625279::get_offset_of_stateColourUpdate_5(),
	MissilePointsPopulator_t2262625279::get_offset_of_pointsColourUpdate_6(),
	MissilePointsPopulator_t2262625279::get_offset_of_ptsColourUpdate_7(),
	MissilePointsPopulator_t2262625279::get_offset_of_labelPadding_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof (ResultSprite_t623266392)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5830[4] = 
{
	ResultSprite_t623266392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof (ContentColour_t2912031623)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5831[5] = 
{
	ContentColour_t2912031623::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { sizeof (MissileTrajectoryDisplay_t1441887151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5832[12] = 
{
	MissileTrajectoryDisplay_t1441887151::get_offset_of_missileItem_2(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_travelLinePanel_3(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_missileContainer_4(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_missileTexture_5(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_resultTexture_6(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_missileTravelLine_7(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_travelLineTween_8(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_stealthIncResultAnchor_9(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_stealthOutResultAnchor_10(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_totalTimeAsSeconds_11(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_startRotation_12(),
	MissileTrajectoryDisplay_t1441887151::get_offset_of_endRotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof (MissileTravelController_t534821896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5833[9] = 
{
	MissileTravelController_t534821896::get_offset_of_currentState_2(),
	MissileTravelController_t534821896::get_offset_of_previousState_3(),
	MissileTravelController_t534821896::get_offset_of_listItem_4(),
	MissileTravelController_t534821896::get_offset_of_missileItem_5(),
	MissileTravelController_t534821896::get_offset_of_trajectory_6(),
	MissileTravelController_t534821896::get_offset_of_arriveTimer_7(),
	MissileTravelController_t534821896::get_offset_of_userLogoBackground_8(),
	MissileTravelController_t534821896::get_offset_of_totalTimeAsSeconds_9(),
	MissileTravelController_t534821896::get_offset_of_progressTimeAsSeconds_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof (PendingMissileUI_t3261005045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5834[2] = 
{
	PendingMissileUI_t3261005045::get_offset_of_scaleTween_4(),
	PendingMissileUI_t3261005045::get_offset_of_uiUnloading_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof (ShieldDisplayController_t1783798351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5835[6] = 
{
	ShieldDisplayController_t1783798351::get_offset_of_shield_2(),
	ShieldDisplayController_t1783798351::get_offset_of_missile_3(),
	ShieldDisplayController_t1783798351::get_offset_of_shieldSprite_4(),
	ShieldDisplayController_t1783798351::get_offset_of_bronzeColour_5(),
	ShieldDisplayController_t1783798351::get_offset_of_silverColour_6(),
	ShieldDisplayController_t1783798351::get_offset_of_goldColour_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { sizeof (StatusAttackButton_t181803962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5836[1] = 
{
	StatusAttackButton_t181803962::get_offset_of_item_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { sizeof (StatusChatButton_t749649916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5837[1] = 
{
	StatusChatButton_t749649916::get_offset_of_item_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof (StatusDoubleStateButton_t3675410548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5838[8] = 
{
	StatusDoubleStateButton_t3675410548::get_offset_of_activeForegroundColor_11(),
	StatusDoubleStateButton_t3675410548::get_offset_of_activeBackgroundColor_12(),
	StatusDoubleStateButton_t3675410548::get_offset_of_inactiveForegroundColor_13(),
	StatusDoubleStateButton_t3675410548::get_offset_of_inactiveBackgroundColor_14(),
	StatusDoubleStateButton_t3675410548::get_offset_of_forgroundSprite_15(),
	StatusDoubleStateButton_t3675410548::get_offset_of_backgroundSprite_16(),
	StatusDoubleStateButton_t3675410548::get_offset_of_buttonLabel_17(),
	StatusDoubleStateButton_t3675410548::get_offset_of_buttonActive_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { sizeof (StatusSwipeState_t381899333)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5839[4] = 
{
	StatusSwipeState_t381899333::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { sizeof (StatusItemSwipe_t1669089155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5840[3] = 
{
	StatusItemSwipe_t1669089155::get_offset_of_currentState_10(),
	StatusItemSwipe_t1669089155::get_offset_of_listItem_11(),
	StatusItemSwipe_t1669089155::get_offset_of_posTween_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { sizeof (MissileTravelState_t1612660365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5841[5] = 
{
	MissileTravelState_t1612660365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof (StatusListItem_t459202613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5842[22] = 
{
	StatusListItem_t459202613::get_offset_of_listPopulator_10(),
	StatusListItem_t459202613::get_offset_of_outgoingItem_11(),
	StatusListItem_t459202613::get_offset_of_launchedItem_12(),
	StatusListItem_t459202613::get_offset_of_avatar_13(),
	StatusListItem_t459202613::get_offset_of_profileButton_14(),
	StatusListItem_t459202613::get_offset_of_nameLabel_15(),
	StatusListItem_t459202613::get_offset_of_actionLabel_16(),
	StatusListItem_t459202613::get_offset_of_itemGraphic_17(),
	StatusListItem_t459202613::get_offset_of_pointsPopulator_18(),
	StatusListItem_t459202613::get_offset_of_missileInactiveUI_19(),
	StatusListItem_t459202613::get_offset_of_missilePendingUI_20(),
	StatusListItem_t459202613::get_offset_of_missileActiveUI_21(),
	StatusListItem_t459202613::get_offset_of_mineUI_22(),
	StatusListItem_t459202613::get_offset_of_finishTime_23(),
	StatusListItem_t459202613::get_offset_of_scrollView_24(),
	StatusListItem_t459202613::get_offset_of_draggablePanel_25(),
	StatusListItem_t459202613::get_offset_of_mTrans_26(),
	StatusListItem_t459202613::get_offset_of_mScroll_27(),
	StatusListItem_t459202613::get_offset_of_mAutoFind_28(),
	StatusListItem_t459202613::get_offset_of_mStarted_29(),
	StatusListItem_t459202613::get_offset_of_allowToast_30(),
	StatusListItem_t459202613::get_offset_of_toastWaitTime_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof (U3CWaitToAllowToastU3Ec__Iterator0_t1783910557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5843[4] = 
{
	U3CWaitToAllowToastU3Ec__Iterator0_t1783910557::get_offset_of_U24this_0(),
	U3CWaitToAllowToastU3Ec__Iterator0_t1783910557::get_offset_of_U24current_1(),
	U3CWaitToAllowToastU3Ec__Iterator0_t1783910557::get_offset_of_U24disposing_2(),
	U3CWaitToAllowToastU3Ec__Iterator0_t1783910557::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof (StatusSwipeComponent_t1744404785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5844[1] = 
{
	StatusSwipeComponent_t1744404785::get_offset_of_statusSwipe_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof (StatusUI_t2174902556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5845[2] = 
{
	StatusUI_t2174902556::get_offset_of_item_2(),
	StatusUI_t2174902556::get_offset_of_uiActive_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof (StatusListType_t2488921270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5846[3] = 
{
	StatusListType_t2488921270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof (StatusManager_t2720247037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5847[9] = 
{
	StatusManager_t2720247037::get_offset_of_incomingListUpdated_3(),
	StatusManager_t2720247037::get_offset_of_outgoingListUpdated_4(),
	StatusManager_t2720247037::get_offset_of_darkGreenColour_5(),
	StatusManager_t2720247037::get_offset_of_greenColour_6(),
	StatusManager_t2720247037::get_offset_of_darkOrangeColour_7(),
	StatusManager_t2720247037::get_offset_of_orangeColour_8(),
	StatusManager_t2720247037::get_offset_of_darkRedColour_9(),
	StatusManager_t2720247037::get_offset_of_redColour_10(),
	StatusManager_t2720247037::get_offset_of_greyColour_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof (StatusNavigateForwardsButton_t4116613479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5848[1] = 
{
	StatusNavigateForwardsButton_t4116613479::get_offset_of_navScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof (StatusNavScreen_t2872369083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5849[4] = 
{
	StatusNavScreen_t2872369083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { sizeof (StatusNavigationController_t3357829236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5850[7] = 
{
	StatusNavigationController_t3357829236::get_offset_of_incomingTween_8(),
	StatusNavigationController_t3357829236::get_offset_of_outgoingTween_9(),
	StatusNavigationController_t3357829236::get_offset_of_incomingObject_10(),
	StatusNavigationController_t3357829236::get_offset_of_outgoingObject_11(),
	StatusNavigationController_t3357829236::get_offset_of_activePos_12(),
	StatusNavigationController_t3357829236::get_offset_of_leftInactivePos_13(),
	StatusNavigationController_t3357829236::get_offset_of_rightInactivePos_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { sizeof (StatusOutgoingNavScreen_t2187904061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5851[4] = 
{
	StatusOutgoingNavScreen_t2187904061::get_offset_of_hasOutgoingUI_3(),
	StatusOutgoingNavScreen_t2187904061::get_offset_of_noOutgoingUI_4(),
	StatusOutgoingNavScreen_t2187904061::get_offset_of_outgoingList_5(),
	StatusOutgoingNavScreen_t2187904061::get_offset_of_uiClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof (StatusListItemUIScaler_t1403897979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5852[15] = 
{
	StatusListItemUIScaler_t1403897979::get_offset_of_background_14(),
	StatusListItemUIScaler_t1403897979::get_offset_of_backgroundCollider_15(),
	StatusListItemUIScaler_t1403897979::get_offset_of_profilePicture_16(),
	StatusListItemUIScaler_t1403897979::get_offset_of_nameLabel_17(),
	StatusListItemUIScaler_t1403897979::get_offset_of_actionLabel_18(),
	StatusListItemUIScaler_t1403897979::get_offset_of_itemGraphic_19(),
	StatusListItemUIScaler_t1403897979::get_offset_of_flameSprite_20(),
	StatusListItemUIScaler_t1403897979::get_offset_of_points_21(),
	StatusListItemUIScaler_t1403897979::get_offset_of_activeShield_22(),
	StatusListItemUIScaler_t1403897979::get_offset_of_activeWarning_23(),
	StatusListItemUIScaler_t1403897979::get_offset_of_activeUserLogo_24(),
	StatusListItemUIScaler_t1403897979::get_offset_of_pendingInfoLabel_25(),
	StatusListItemUIScaler_t1403897979::get_offset_of_inactiveStateLabel_26(),
	StatusListItemUIScaler_t1403897979::get_offset_of_inactivePointsLabel_27(),
	StatusListItemUIScaler_t1403897979::get_offset_of_finishTime_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof (StatusUIScaler_t1399255572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5853[13] = 
{
	StatusUIScaler_t1399255572::get_offset_of_background_14(),
	StatusUIScaler_t1399255572::get_offset_of_titleSeperator_15(),
	StatusUIScaler_t1399255572::get_offset_of_navigateBackButton_16(),
	StatusUIScaler_t1399255572::get_offset_of_clearButton_17(),
	StatusUIScaler_t1399255572::get_offset_of_tabBackground_18(),
	StatusUIScaler_t1399255572::get_offset_of_bottomSeperator_19(),
	StatusUIScaler_t1399255572::get_offset_of_incomingTab_20(),
	StatusUIScaler_t1399255572::get_offset_of_incomingTabCollider_21(),
	StatusUIScaler_t1399255572::get_offset_of_outgoingTab_22(),
	StatusUIScaler_t1399255572::get_offset_of_outgoingTabCollider_23(),
	StatusUIScaler_t1399255572::get_offset_of_topPanel_24(),
	StatusUIScaler_t1399255572::get_offset_of_incomingPanel_25(),
	StatusUIScaler_t1399255572::get_offset_of_outgoingPanel_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof (BottomButtonsController_t3250435864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5854[4] = 
{
	BottomButtonsController_t3250435864::get_offset_of_targetsButton_2(),
	BottomButtonsController_t3250435864::get_offset_of_buttonsTween_3(),
	BottomButtonsController_t3250435864::get_offset_of_labelsTween_4(),
	BottomButtonsController_t3250435864::get_offset_of_targetsUIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof (BuyItemButton_t2593913003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5855[4] = 
{
	BuyItemButton_t2593913003::get_offset_of_activeColour_5(),
	BuyItemButton_t2593913003::get_offset_of_lockedColour_6(),
	BuyItemButton_t2593913003::get_offset_of_inactiveColour_7(),
	BuyItemButton_t2593913003::get_offset_of_buyLabel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof (U3CPurchaseShieldItemU3Ec__AnonStorey0_t4155480970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5856[2] = 
{
	U3CPurchaseShieldItemU3Ec__AnonStorey0_t4155480970::get_offset_of_cPurchaseableItem_0(),
	U3CPurchaseShieldItemU3Ec__AnonStorey0_t4155480970::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof (U3CPurchaseStaticItemU3Ec__AnonStorey1_t2538291704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5857[2] = 
{
	U3CPurchaseStaticItemU3Ec__AnonStorey1_t2538291704::get_offset_of_cPurchaseableItem_0(),
	U3CPurchaseStaticItemU3Ec__AnonStorey1_t2538291704::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof (CloseStoreButton_t141935289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5858[1] = 
{
	CloseStoreButton_t141935289::get_offset_of_panel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof (InventoryItemButton_t3515382031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof (InventoryManager_t38269895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5860[3] = 
{
	InventoryManager_t38269895::get_offset_of_missileInventory_3(),
	InventoryManager_t38269895::get_offset_of_shieldInventory_4(),
	InventoryManager_t38269895::get_offset_of_mineInventory_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof (U3CCheckStandardMissileInventoryU3Ec__AnonStorey0_t1452096246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5861[1] = 
{
	U3CCheckStandardMissileInventoryU3Ec__AnonStorey0_t1452096246::get_offset_of_callBack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof (InventoryPanel_t2677987186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof (ItemInfoController_t4231956141), -1, sizeof(ItemInfoController_t4231956141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5863[17] = 
{
	ItemInfoController_t4231956141::get_offset_of_titleSeperation_2(),
	ItemInfoController_t4231956141::get_offset_of_contentSeperation_3(),
	ItemInfoController_t4231956141::get_offset_of_timeBetweenSections_4(),
	ItemInfoController_t4231956141::get_offset_of_updateTitlePositions_5(),
	ItemInfoController_t4231956141::get_offset_of_updateDescriptionPositions_6(),
	ItemInfoController_t4231956141::get_offset_of_updateDescriptionXPos_7(),
	ItemInfoController_t4231956141::get_offset_of_titleLabels_8(),
	ItemInfoController_t4231956141::get_offset_of_descriptionLabels_9(),
	ItemInfoController_t4231956141::get_offset_of_titleStrings_10(),
	ItemInfoController_t4231956141::get_offset_of_descriptionStrings_11(),
	ItemInfoController_t4231956141::get_offset_of_populator_12(),
	ItemInfoController_t4231956141::get_offset_of_onPopulationComplete_13(),
	ItemInfoController_t4231956141::get_offset_of_waitingRoutine_14(),
	ItemInfoController_t4231956141::get_offset_of_updatingLabels_15(),
	ItemInfoController_t4231956141::get_offset_of_activeLabelNum_16(),
	ItemInfoController_t4231956141::get_offset_of_title_17(),
	ItemInfoController_t4231956141_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof (OnPopulationComplete_t1301655353), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof (U3CWaitForSecondsU3Ec__Iterator0_t3866018642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5865[6] = 
{
	U3CWaitForSecondsU3Ec__Iterator0_t3866018642::get_offset_of_waitTime_0(),
	U3CWaitForSecondsU3Ec__Iterator0_t3866018642::get_offset_of_action_1(),
	U3CWaitForSecondsU3Ec__Iterator0_t3866018642::get_offset_of_U24this_2(),
	U3CWaitForSecondsU3Ec__Iterator0_t3866018642::get_offset_of_U24current_3(),
	U3CWaitForSecondsU3Ec__Iterator0_t3866018642::get_offset_of_U24disposing_4(),
	U3CWaitForSecondsU3Ec__Iterator0_t3866018642::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof (ItemSelectionSwipe_t2104393431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5866[1] = 
{
	ItemSelectionSwipe_t2104393431::get_offset_of_infoObject_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof (ItemSwipeController_t3011021799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5867[21] = 
{
	ItemSwipeController_t3011021799::get_offset_of_itemSwipe_2(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemPosition_3(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemScale_4(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemLeftPosition_5(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemRightPosition_6(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemLeftScale_7(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemRightScale_8(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemDescriptionPosition_9(),
	ItemSwipeController_t3011021799::get_offset_of_m_ItemDescriptionScale_10(),
	ItemSwipeController_t3011021799::get_offset_of_m_DisplayItemObject_11(),
	ItemSwipeController_t3011021799::get_offset_of_defaultPositionTime_12(),
	ItemSwipeController_t3011021799::get_offset_of_leftrightPositionTime_13(),
	ItemSwipeController_t3011021799::get_offset_of_tween_14(),
	ItemSwipeController_t3011021799::get_offset_of_tweenScale_15(),
	ItemSwipeController_t3011021799::get_offset_of_tweenRotation_16(),
	ItemSwipeController_t3011021799::get_offset_of_curve_17(),
	ItemSwipeController_t3011021799::get_offset_of_currentModel_18(),
	ItemSwipeController_t3011021799::get_offset_of_onLeftComplete_19(),
	ItemSwipeController_t3011021799::get_offset_of_onRightComplete_20(),
	ItemSwipeController_t3011021799::get_offset_of_onHideComplete_21(),
	ItemSwipeController_t3011021799::get_offset_of_itemClosing_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof (OnLeftComplete_t1529246307), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof (OnRightComplete_t1521737374), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { sizeof (OnHideComplete_t4141700430), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { sizeof (MissileSwipeSelection_t3143859656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5871[2] = 
{
	MissileSwipeSelection_t3143859656::get_offset_of_carouselObject_10(),
	MissileSwipeSelection_t3143859656::get_offset_of_itemStart_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof (PanelUIScaler_t4049405372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5872[1] = 
{
	PanelUIScaler_t4049405372::get_offset_of_panel_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof (BuyBucksPUButton_t1827949611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof (CloseStorePUButton_t1133311818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof (NotEnoughBucksPU_t2637555884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5875[4] = 
{
	NotEnoughBucksPU_t2637555884::get_offset_of_puActive_2(),
	NotEnoughBucksPU_t2637555884::get_offset_of_puTween_3(),
	NotEnoughBucksPU_t2637555884::get_offset_of_bucksLabel_4(),
	NotEnoughBucksPU_t2637555884::get_offset_of_puClosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof (PurchasePU_t4126238880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5876[5] = 
{
	PurchasePU_t4126238880::get_offset_of_puActive_2(),
	PurchasePU_t4126238880::get_offset_of_puTween_3(),
	PurchasePU_t4126238880::get_offset_of_purchaseLabel_4(),
	PurchasePU_t4126238880::get_offset_of_successWaitTime_5(),
	PurchasePU_t4126238880::get_offset_of_puClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof (U3CWaitForSecondsU3Ec__Iterator0_t1848252903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5877[4] = 
{
	U3CWaitForSecondsU3Ec__Iterator0_t1848252903::get_offset_of_U24this_0(),
	U3CWaitForSecondsU3Ec__Iterator0_t1848252903::get_offset_of_U24current_1(),
	U3CWaitForSecondsU3Ec__Iterator0_t1848252903::get_offset_of_U24disposing_2(),
	U3CWaitForSecondsU3Ec__Iterator0_t1848252903::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof (StorePopUpController_t952004567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5878[5] = 
{
	StorePopUpController_t952004567::get_offset_of_puPanel_3(),
	StorePopUpController_t952004567::get_offset_of_fadeTween_4(),
	StorePopUpController_t952004567::get_offset_of_nebPU_5(),
	StorePopUpController_t952004567::get_offset_of_purchasePU_6(),
	StorePopUpController_t952004567::get_offset_of_puClosing_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof (ProductCatergoryNavItem_t1498614491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5879[3] = 
{
	ProductCatergoryNavItem_t1498614491::get_offset_of_navController_5(),
	ProductCatergoryNavItem_t1498614491::get_offset_of_category_6(),
	ProductCatergoryNavItem_t1498614491::get_offset_of_index_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof (ShieldSliderType_t2078175364)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5880[4] = 
{
	ShieldSliderType_t2078175364::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof (ShieldSlider_t1483508804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5881[1] = 
{
	ShieldSlider_t1483508804::get_offset_of_type_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { sizeof (SliderControllerSlider_t2300726312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5882[7] = 
{
	SliderControllerSlider_t2300726312::get_offset_of_sliderController_2(),
	SliderControllerSlider_t2300726312::get_offset_of_debugArray_3(),
	SliderControllerSlider_t2300726312::get_offset_of_firstHit_4(),
	SliderControllerSlider_t2300726312::get_offset_of_newPos_5(),
	SliderControllerSlider_t2300726312::get_offset_of_pickedObject_6(),
	SliderControllerSlider_t2300726312::get_offset_of_posLocked_7(),
	SliderControllerSlider_t2300726312::get_offset_of_pointPos_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof (StoreManager_t650776524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5883[36] = 
{
	StoreManager_t650776524::get_offset_of_categoryNavController_3(),
	StoreManager_t650776524::get_offset_of_infoObject_4(),
	StoreManager_t650776524::get_offset_of_itemObject_5(),
	StoreManager_t650776524::get_offset_of_lockedBackground_6(),
	StoreManager_t650776524::get_offset_of_lockedLabel_7(),
	StoreManager_t650776524::get_offset_of_bucksLabel_8(),
	StoreManager_t650776524::get_offset_of_infoButton_9(),
	StoreManager_t650776524::get_offset_of_infoController_10(),
	StoreManager_t650776524::get_offset_of_swipeController_11(),
	StoreManager_t650776524::get_offset_of_currentShieldSlider_12(),
	StoreManager_t650776524::get_offset_of_curve_13(),
	StoreManager_t650776524::get_offset_of_missileSpecificItems_14(),
	StoreManager_t650776524::get_offset_of_shieldHourSpecificItems_15(),
	StoreManager_t650776524::get_offset_of_shieldDaySpecificItems_16(),
	StoreManager_t650776524::get_offset_of_mineSpecificItems_17(),
	StoreManager_t650776524::get_offset_of_fuelSpecificItems_18(),
	StoreManager_t650776524::get_offset_of_bundleSpecificItems_19(),
	StoreManager_t650776524::get_offset_of_bucksSpecificItems_20(),
	StoreManager_t650776524::get_offset_of_newItems_21(),
	StoreManager_t650776524::get_offset_of_currentItems_22(),
	StoreManager_t650776524::get_offset_of_currentCategory_23(),
	StoreManager_t650776524::get_offset_of_categoryItems_24(),
	StoreManager_t650776524::get_offset_of_currentItemIndex_25(),
	StoreManager_t650776524::get_offset_of_itemNameLabel_26(),
	StoreManager_t650776524::get_offset_of_itemPriceLabel_27(),
	StoreManager_t650776524::get_offset_of_itemAmountLabel_28(),
	StoreManager_t650776524::get_offset_of_infinitySprite_29(),
	StoreManager_t650776524::get_offset_of_buttonsController_30(),
	StoreManager_t650776524::get_offset_of_buyButton_31(),
	StoreManager_t650776524::get_offset_of_targetsButton_32(),
	StoreManager_t650776524::get_offset_of_showingInfo_33(),
	StoreManager_t650776524::get_offset_of_itemLight_34(),
	StoreManager_t650776524::get_offset_of_startCheck_35(),
	StoreManager_t650776524::get_offset_of_bucksLightColour_36(),
	StoreManager_t650776524::get_offset_of_scaleTween_37(),
	StoreManager_t650776524::get_offset_of_firstTimeLoading_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5884[4] = 
{
	U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203::get_offset_of_U24this_0(),
	U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203::get_offset_of_U24current_1(),
	U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203::get_offset_of_U24disposing_2(),
	U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof (U3CScaleAndHideU3Ec__Iterator1_t2634602714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5885[9] = 
{
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_gameobjects_0(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24locvar0_1(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24locvar1_2(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24locvar2_3(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24locvar3_4(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24this_5(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24current_6(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24disposing_7(),
	U3CScaleAndHideU3Ec__Iterator1_t2634602714::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof (U3CScaleAndShowU3Ec__Iterator2_t815003340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5886[9] = 
{
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_gameobjects_0(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24locvar0_1(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24locvar1_2(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24locvar2_3(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24locvar3_4(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24this_5(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24current_6(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24disposing_7(),
	U3CScaleAndShowU3Ec__Iterator2_t815003340::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof (U3CLoadDefaultModelU3Ec__Iterator3_t378789387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5887[4] = 
{
	U3CLoadDefaultModelU3Ec__Iterator3_t378789387::get_offset_of_U24this_0(),
	U3CLoadDefaultModelU3Ec__Iterator3_t378789387::get_offset_of_U24current_1(),
	U3CLoadDefaultModelU3Ec__Iterator3_t378789387::get_offset_of_U24disposing_2(),
	U3CLoadDefaultModelU3Ec__Iterator3_t378789387::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof (StoreTargetsButton_t3560946605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5888[4] = 
{
	StoreTargetsButton_t3560946605::get_offset_of_activeColour_5(),
	StoreTargetsButton_t3560946605::get_offset_of_lockedColour_6(),
	StoreTargetsButton_t3560946605::get_offset_of_inactiveColour_7(),
	StoreTargetsButton_t3560946605::get_offset_of_targetLabel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof (StoreUIScaler_t2133310967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5889[25] = 
{
	StoreUIScaler_t2133310967::get_offset_of_background_14(),
	StoreUIScaler_t2133310967::get_offset_of_backgroundHatching_15(),
	StoreUIScaler_t2133310967::get_offset_of_titleSeperator_16(),
	StoreUIScaler_t2133310967::get_offset_of_navigateBackButton_17(),
	StoreUIScaler_t2133310967::get_offset_of_tabBackground_18(),
	StoreUIScaler_t2133310967::get_offset_of_tabPanel_19(),
	StoreUIScaler_t2133310967::get_offset_of_bottomSeperator_20(),
	StoreUIScaler_t2133310967::get_offset_of_popUpPanel_21(),
	StoreUIScaler_t2133310967::get_offset_of_popUpBackground_22(),
	StoreUIScaler_t2133310967::get_offset_of_popUpCollider_23(),
	StoreUIScaler_t2133310967::get_offset_of_itemDescriptionAmount_24(),
	StoreUIScaler_t2133310967::get_offset_of_infinitySprite_25(),
	StoreUIScaler_t2133310967::get_offset_of_itemScaleMeasure_26(),
	StoreUIScaler_t2133310967::get_offset_of_itemInfoButton_27(),
	StoreUIScaler_t2133310967::get_offset_of_swipe_28(),
	StoreUIScaler_t2133310967::get_offset_of_containerCollider_29(),
	StoreUIScaler_t2133310967::get_offset_of_horizLine_30(),
	StoreUIScaler_t2133310967::get_offset_of_buttonsTween_31(),
	StoreUIScaler_t2133310967::get_offset_of_buyButtonCollider_32(),
	StoreUIScaler_t2133310967::get_offset_of_buyButtonBackground_33(),
	StoreUIScaler_t2133310967::get_offset_of_buyButtonText_34(),
	StoreUIScaler_t2133310967::get_offset_of_buyButtonTextTween_35(),
	StoreUIScaler_t2133310967::get_offset_of_targetButtonCollider_36(),
	StoreUIScaler_t2133310967::get_offset_of_targetButtonBackground_37(),
	StoreUIScaler_t2133310967::get_offset_of_targetButtonText_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof (HomeTabController_t1746680606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5890[4] = 
{
	HomeTabController_t1746680606::get_offset_of_posTween_3(),
	HomeTabController_t1746680606::get_offset_of_incomingLabel_4(),
	HomeTabController_t1746680606::get_offset_of_incomingAmountLabel_5(),
	HomeTabController_t1746680606::get_offset_of_tabClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof (HomeButton_t266229155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5891[3] = 
{
	HomeButton_t266229155::get_offset_of_loadingFromScene_5(),
	HomeButton_t266229155::get_offset_of_dailyTween_6(),
	HomeButton_t266229155::get_offset_of_allTimeTween_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof (GlobalManagersController_t3516162077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5892[1] = 
{
	GlobalManagersController_t3516162077::get_offset_of_activeGManagers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof (InitSceneManager_t1690944945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5893[5] = 
{
	InitSceneManager_t1690944945::get_offset_of_clearData_17(),
	InitSceneManager_t1690944945::get_offset_of_InitCenterUI_18(),
	InitSceneManager_t1690944945::get_offset_of_ConnectionPopUpUI_19(),
	InitSceneManager_t1690944945::get_offset_of_launchAnimation_20(),
	InitSceneManager_t1690944945::get_offset_of_internetManagersLoaded_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5894[4] = 
{
	U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155::get_offset_of_U24this_0(),
	U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155::get_offset_of_U24current_1(),
	U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155::get_offset_of_U24disposing_2(),
	U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof (U3CWaitForSecondsU3Ec__Iterator1_t235271859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5895[4] = 
{
	U3CWaitForSecondsU3Ec__Iterator1_t235271859::get_offset_of_U24this_0(),
	U3CWaitForSecondsU3Ec__Iterator1_t235271859::get_offset_of_U24current_1(),
	U3CWaitForSecondsU3Ec__Iterator1_t235271859::get_offset_of_U24disposing_2(),
	U3CWaitForSecondsU3Ec__Iterator1_t235271859::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof (InitSceneUIScaler_t296852604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5896[1] = 
{
	InitSceneUIScaler_t296852604::get_offset_of_backgroundTexture_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof (AllTimeLeaderboardNavScreen_t2566129190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5897[4] = 
{
	AllTimeLeaderboardNavScreen_t2566129190::get_offset_of_hasResultsUI_3(),
	AllTimeLeaderboardNavScreen_t2566129190::get_offset_of_hasNoResultsUI_4(),
	AllTimeLeaderboardNavScreen_t2566129190::get_offset_of_leaderboardList_5(),
	AllTimeLeaderboardNavScreen_t2566129190::get_offset_of_uiClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof (DailyLeaderboardNavScreen_t4105499011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5898[4] = 
{
	DailyLeaderboardNavScreen_t4105499011::get_offset_of_hasResultsUI_3(),
	DailyLeaderboardNavScreen_t4105499011::get_offset_of_hasNoResultsUI_4(),
	DailyLeaderboardNavScreen_t4105499011::get_offset_of_leaderboardList_5(),
	DailyLeaderboardNavScreen_t4105499011::get_offset_of_uiClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof (LeaderboardType_t1980945291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5899[5] = 
{
	LeaderboardType_t1980945291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
