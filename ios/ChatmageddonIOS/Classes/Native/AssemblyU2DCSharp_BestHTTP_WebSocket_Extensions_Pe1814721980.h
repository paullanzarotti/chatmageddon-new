﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// BestHTTP.Decompression.Zlib.DeflateStream
struct DeflateStream_t2274450459;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp4151391442.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.Extensions.PerMessageCompression
struct  PerMessageCompression_t1814721980  : public Il2CppObject
{
public:
	// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::<ClientNoContextTakeover>k__BackingField
	bool ___U3CClientNoContextTakeoverU3Ek__BackingField_1;
	// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::<ServerNoContextTakeover>k__BackingField
	bool ___U3CServerNoContextTakeoverU3Ek__BackingField_2;
	// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::<ClientMaxWindowBits>k__BackingField
	int32_t ___U3CClientMaxWindowBitsU3Ek__BackingField_3;
	// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::<ServerMaxWindowBits>k__BackingField
	int32_t ___U3CServerMaxWindowBitsU3Ek__BackingField_4;
	// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.WebSocket.Extensions.PerMessageCompression::<Level>k__BackingField
	int32_t ___U3CLevelU3Ek__BackingField_5;
	// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::<MinimumDataLegthToCompress>k__BackingField
	int32_t ___U3CMinimumDataLegthToCompressU3Ek__BackingField_6;
	// System.IO.MemoryStream BestHTTP.WebSocket.Extensions.PerMessageCompression::compressorOutputStream
	MemoryStream_t743994179 * ___compressorOutputStream_7;
	// BestHTTP.Decompression.Zlib.DeflateStream BestHTTP.WebSocket.Extensions.PerMessageCompression::compressorDeflateStream
	DeflateStream_t2274450459 * ___compressorDeflateStream_8;
	// System.IO.MemoryStream BestHTTP.WebSocket.Extensions.PerMessageCompression::decompressorInputStream
	MemoryStream_t743994179 * ___decompressorInputStream_9;
	// System.IO.MemoryStream BestHTTP.WebSocket.Extensions.PerMessageCompression::decompressorOutputStream
	MemoryStream_t743994179 * ___decompressorOutputStream_10;
	// BestHTTP.Decompression.Zlib.DeflateStream BestHTTP.WebSocket.Extensions.PerMessageCompression::decompressorDeflateStream
	DeflateStream_t2274450459 * ___decompressorDeflateStream_11;
	// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::copyBuffer
	ByteU5BU5D_t3397334013* ___copyBuffer_12;

public:
	inline static int32_t get_offset_of_U3CClientNoContextTakeoverU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___U3CClientNoContextTakeoverU3Ek__BackingField_1)); }
	inline bool get_U3CClientNoContextTakeoverU3Ek__BackingField_1() const { return ___U3CClientNoContextTakeoverU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CClientNoContextTakeoverU3Ek__BackingField_1() { return &___U3CClientNoContextTakeoverU3Ek__BackingField_1; }
	inline void set_U3CClientNoContextTakeoverU3Ek__BackingField_1(bool value)
	{
		___U3CClientNoContextTakeoverU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CServerNoContextTakeoverU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___U3CServerNoContextTakeoverU3Ek__BackingField_2)); }
	inline bool get_U3CServerNoContextTakeoverU3Ek__BackingField_2() const { return ___U3CServerNoContextTakeoverU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CServerNoContextTakeoverU3Ek__BackingField_2() { return &___U3CServerNoContextTakeoverU3Ek__BackingField_2; }
	inline void set_U3CServerNoContextTakeoverU3Ek__BackingField_2(bool value)
	{
		___U3CServerNoContextTakeoverU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CClientMaxWindowBitsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___U3CClientMaxWindowBitsU3Ek__BackingField_3)); }
	inline int32_t get_U3CClientMaxWindowBitsU3Ek__BackingField_3() const { return ___U3CClientMaxWindowBitsU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CClientMaxWindowBitsU3Ek__BackingField_3() { return &___U3CClientMaxWindowBitsU3Ek__BackingField_3; }
	inline void set_U3CClientMaxWindowBitsU3Ek__BackingField_3(int32_t value)
	{
		___U3CClientMaxWindowBitsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CServerMaxWindowBitsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___U3CServerMaxWindowBitsU3Ek__BackingField_4)); }
	inline int32_t get_U3CServerMaxWindowBitsU3Ek__BackingField_4() const { return ___U3CServerMaxWindowBitsU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CServerMaxWindowBitsU3Ek__BackingField_4() { return &___U3CServerMaxWindowBitsU3Ek__BackingField_4; }
	inline void set_U3CServerMaxWindowBitsU3Ek__BackingField_4(int32_t value)
	{
		___U3CServerMaxWindowBitsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLevelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___U3CLevelU3Ek__BackingField_5)); }
	inline int32_t get_U3CLevelU3Ek__BackingField_5() const { return ___U3CLevelU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CLevelU3Ek__BackingField_5() { return &___U3CLevelU3Ek__BackingField_5; }
	inline void set_U3CLevelU3Ek__BackingField_5(int32_t value)
	{
		___U3CLevelU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___U3CMinimumDataLegthToCompressU3Ek__BackingField_6)); }
	inline int32_t get_U3CMinimumDataLegthToCompressU3Ek__BackingField_6() const { return ___U3CMinimumDataLegthToCompressU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_6() { return &___U3CMinimumDataLegthToCompressU3Ek__BackingField_6; }
	inline void set_U3CMinimumDataLegthToCompressU3Ek__BackingField_6(int32_t value)
	{
		___U3CMinimumDataLegthToCompressU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_compressorOutputStream_7() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___compressorOutputStream_7)); }
	inline MemoryStream_t743994179 * get_compressorOutputStream_7() const { return ___compressorOutputStream_7; }
	inline MemoryStream_t743994179 ** get_address_of_compressorOutputStream_7() { return &___compressorOutputStream_7; }
	inline void set_compressorOutputStream_7(MemoryStream_t743994179 * value)
	{
		___compressorOutputStream_7 = value;
		Il2CppCodeGenWriteBarrier(&___compressorOutputStream_7, value);
	}

	inline static int32_t get_offset_of_compressorDeflateStream_8() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___compressorDeflateStream_8)); }
	inline DeflateStream_t2274450459 * get_compressorDeflateStream_8() const { return ___compressorDeflateStream_8; }
	inline DeflateStream_t2274450459 ** get_address_of_compressorDeflateStream_8() { return &___compressorDeflateStream_8; }
	inline void set_compressorDeflateStream_8(DeflateStream_t2274450459 * value)
	{
		___compressorDeflateStream_8 = value;
		Il2CppCodeGenWriteBarrier(&___compressorDeflateStream_8, value);
	}

	inline static int32_t get_offset_of_decompressorInputStream_9() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___decompressorInputStream_9)); }
	inline MemoryStream_t743994179 * get_decompressorInputStream_9() const { return ___decompressorInputStream_9; }
	inline MemoryStream_t743994179 ** get_address_of_decompressorInputStream_9() { return &___decompressorInputStream_9; }
	inline void set_decompressorInputStream_9(MemoryStream_t743994179 * value)
	{
		___decompressorInputStream_9 = value;
		Il2CppCodeGenWriteBarrier(&___decompressorInputStream_9, value);
	}

	inline static int32_t get_offset_of_decompressorOutputStream_10() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___decompressorOutputStream_10)); }
	inline MemoryStream_t743994179 * get_decompressorOutputStream_10() const { return ___decompressorOutputStream_10; }
	inline MemoryStream_t743994179 ** get_address_of_decompressorOutputStream_10() { return &___decompressorOutputStream_10; }
	inline void set_decompressorOutputStream_10(MemoryStream_t743994179 * value)
	{
		___decompressorOutputStream_10 = value;
		Il2CppCodeGenWriteBarrier(&___decompressorOutputStream_10, value);
	}

	inline static int32_t get_offset_of_decompressorDeflateStream_11() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___decompressorDeflateStream_11)); }
	inline DeflateStream_t2274450459 * get_decompressorDeflateStream_11() const { return ___decompressorDeflateStream_11; }
	inline DeflateStream_t2274450459 ** get_address_of_decompressorDeflateStream_11() { return &___decompressorDeflateStream_11; }
	inline void set_decompressorDeflateStream_11(DeflateStream_t2274450459 * value)
	{
		___decompressorDeflateStream_11 = value;
		Il2CppCodeGenWriteBarrier(&___decompressorDeflateStream_11, value);
	}

	inline static int32_t get_offset_of_copyBuffer_12() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980, ___copyBuffer_12)); }
	inline ByteU5BU5D_t3397334013* get_copyBuffer_12() const { return ___copyBuffer_12; }
	inline ByteU5BU5D_t3397334013** get_address_of_copyBuffer_12() { return &___copyBuffer_12; }
	inline void set_copyBuffer_12(ByteU5BU5D_t3397334013* value)
	{
		___copyBuffer_12 = value;
		Il2CppCodeGenWriteBarrier(&___copyBuffer_12, value);
	}
};

struct PerMessageCompression_t1814721980_StaticFields
{
public:
	// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::Trailer
	ByteU5BU5D_t3397334013* ___Trailer_0;

public:
	inline static int32_t get_offset_of_Trailer_0() { return static_cast<int32_t>(offsetof(PerMessageCompression_t1814721980_StaticFields, ___Trailer_0)); }
	inline ByteU5BU5D_t3397334013* get_Trailer_0() const { return ___Trailer_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_Trailer_0() { return &___Trailer_0; }
	inline void set_Trailer_0(ByteU5BU5D_t3397334013* value)
	{
		___Trailer_0 = value;
		Il2CppCodeGenWriteBarrier(&___Trailer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
