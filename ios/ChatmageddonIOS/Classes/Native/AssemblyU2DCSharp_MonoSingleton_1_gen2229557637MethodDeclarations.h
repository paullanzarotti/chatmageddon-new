﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<LauncherSceneManager>::.ctor()
#define MonoSingleton_1__ctor_m209268919(__this, method) ((  void (*) (MonoSingleton_1_t2229557637 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<LauncherSceneManager>::Awake()
#define MonoSingleton_1_Awake_m738041552(__this, method) ((  void (*) (MonoSingleton_1_t2229557637 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<LauncherSceneManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1099999618(__this /* static, unused */, method) ((  LauncherSceneManager_t2478891917 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<LauncherSceneManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3112360114(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<LauncherSceneManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2971092021(__this, method) ((  void (*) (MonoSingleton_1_t2229557637 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<LauncherSceneManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2559218045(__this, method) ((  void (*) (MonoSingleton_1_t2229557637 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<LauncherSceneManager>::.cctor()
#define MonoSingleton_1__cctor_m2659255378(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
