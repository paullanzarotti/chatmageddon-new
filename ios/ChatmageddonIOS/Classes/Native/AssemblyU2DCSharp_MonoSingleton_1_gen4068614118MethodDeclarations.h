﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ContactsNaviagationController>::.ctor()
#define MonoSingleton_1__ctor_m3711003912(__this, method) ((  void (*) (MonoSingleton_1_t4068614118 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ContactsNaviagationController>::Awake()
#define MonoSingleton_1_Awake_m45788589(__this, method) ((  void (*) (MonoSingleton_1_t4068614118 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ContactsNaviagationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m17319439(__this /* static, unused */, method) ((  ContactsNaviagationController_t22981102 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ContactsNaviagationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m387681903(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ContactsNaviagationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2426565806(__this, method) ((  void (*) (MonoSingleton_1_t4068614118 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ContactsNaviagationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m773372370(__this, method) ((  void (*) (MonoSingleton_1_t4068614118 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ContactsNaviagationController>::.cctor()
#define MonoSingleton_1__cctor_m1209623975(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
