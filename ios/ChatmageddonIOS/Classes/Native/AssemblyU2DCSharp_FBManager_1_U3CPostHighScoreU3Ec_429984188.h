﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FBManager`1<System.Object>
struct FBManager_1_t2091981452;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>
struct  U3CPostHighScoreU3Ec__AnonStorey2_t429984188  : public Il2CppObject
{
public:
	// System.Int32 FBManager`1/<PostHighScore>c__AnonStorey2::score
	int32_t ___score_0;
	// FBManager`1<ManagerType> FBManager`1/<PostHighScore>c__AnonStorey2::$this
	FBManager_1_t2091981452 * ___U24this_1;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(U3CPostHighScoreU3Ec__AnonStorey2_t429984188, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPostHighScoreU3Ec__AnonStorey2_t429984188, ___U24this_1)); }
	inline FBManager_1_t2091981452 * get_U24this_1() const { return ___U24this_1; }
	inline FBManager_1_t2091981452 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FBManager_1_t2091981452 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
