﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BestHTTP.Decompression.Zlib.ZlibCodec
struct ZlibCodec_t1899545627;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;
// BestHTTP.Decompression.Crc.CRC32
struct CRC32_t2268741539;
// System.String
struct String_t;

#include "mscorlib_System_IO_Stream3255436806.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib3915258526.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Flus1182037460.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZlibS953065013.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp2282214205.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp4151391442.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp2530143933.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibBaseStream
struct  ZlibBaseStream_t3383394762  : public Stream_t3255436806
{
public:
	// BestHTTP.Decompression.Zlib.ZlibCodec BestHTTP.Decompression.Zlib.ZlibBaseStream::_z
	ZlibCodec_t1899545627 * ____z_2;
	// BestHTTP.Decompression.Zlib.ZlibBaseStream/StreamMode BestHTTP.Decompression.Zlib.ZlibBaseStream::_streamMode
	int32_t ____streamMode_3;
	// BestHTTP.Decompression.Zlib.FlushType BestHTTP.Decompression.Zlib.ZlibBaseStream::_flushMode
	int32_t ____flushMode_4;
	// BestHTTP.Decompression.Zlib.ZlibStreamFlavor BestHTTP.Decompression.Zlib.ZlibBaseStream::_flavor
	int32_t ____flavor_5;
	// BestHTTP.Decompression.Zlib.CompressionMode BestHTTP.Decompression.Zlib.ZlibBaseStream::_compressionMode
	int32_t ____compressionMode_6;
	// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.Decompression.Zlib.ZlibBaseStream::_level
	int32_t ____level_7;
	// System.Boolean BestHTTP.Decompression.Zlib.ZlibBaseStream::_leaveOpen
	bool ____leaveOpen_8;
	// System.Byte[] BestHTTP.Decompression.Zlib.ZlibBaseStream::_workingBuffer
	ByteU5BU5D_t3397334013* ____workingBuffer_9;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream::_bufferSize
	int32_t ____bufferSize_10;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream::windowBitsMax
	int32_t ___windowBitsMax_11;
	// System.Byte[] BestHTTP.Decompression.Zlib.ZlibBaseStream::_buf1
	ByteU5BU5D_t3397334013* ____buf1_12;
	// System.IO.Stream BestHTTP.Decompression.Zlib.ZlibBaseStream::_stream
	Stream_t3255436806 * ____stream_13;
	// BestHTTP.Decompression.Zlib.CompressionStrategy BestHTTP.Decompression.Zlib.ZlibBaseStream::Strategy
	int32_t ___Strategy_14;
	// BestHTTP.Decompression.Crc.CRC32 BestHTTP.Decompression.Zlib.ZlibBaseStream::crc
	CRC32_t2268741539 * ___crc_15;
	// System.String BestHTTP.Decompression.Zlib.ZlibBaseStream::_GzipFileName
	String_t* ____GzipFileName_16;
	// System.String BestHTTP.Decompression.Zlib.ZlibBaseStream::_GzipComment
	String_t* ____GzipComment_17;
	// System.DateTime BestHTTP.Decompression.Zlib.ZlibBaseStream::_GzipMtime
	DateTime_t693205669  ____GzipMtime_18;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream::_gzipHeaderByteCount
	int32_t ____gzipHeaderByteCount_19;
	// System.Boolean BestHTTP.Decompression.Zlib.ZlibBaseStream::nomoreinput
	bool ___nomoreinput_20;

public:
	inline static int32_t get_offset_of__z_2() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____z_2)); }
	inline ZlibCodec_t1899545627 * get__z_2() const { return ____z_2; }
	inline ZlibCodec_t1899545627 ** get_address_of__z_2() { return &____z_2; }
	inline void set__z_2(ZlibCodec_t1899545627 * value)
	{
		____z_2 = value;
		Il2CppCodeGenWriteBarrier(&____z_2, value);
	}

	inline static int32_t get_offset_of__streamMode_3() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____streamMode_3)); }
	inline int32_t get__streamMode_3() const { return ____streamMode_3; }
	inline int32_t* get_address_of__streamMode_3() { return &____streamMode_3; }
	inline void set__streamMode_3(int32_t value)
	{
		____streamMode_3 = value;
	}

	inline static int32_t get_offset_of__flushMode_4() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____flushMode_4)); }
	inline int32_t get__flushMode_4() const { return ____flushMode_4; }
	inline int32_t* get_address_of__flushMode_4() { return &____flushMode_4; }
	inline void set__flushMode_4(int32_t value)
	{
		____flushMode_4 = value;
	}

	inline static int32_t get_offset_of__flavor_5() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____flavor_5)); }
	inline int32_t get__flavor_5() const { return ____flavor_5; }
	inline int32_t* get_address_of__flavor_5() { return &____flavor_5; }
	inline void set__flavor_5(int32_t value)
	{
		____flavor_5 = value;
	}

	inline static int32_t get_offset_of__compressionMode_6() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____compressionMode_6)); }
	inline int32_t get__compressionMode_6() const { return ____compressionMode_6; }
	inline int32_t* get_address_of__compressionMode_6() { return &____compressionMode_6; }
	inline void set__compressionMode_6(int32_t value)
	{
		____compressionMode_6 = value;
	}

	inline static int32_t get_offset_of__level_7() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____level_7)); }
	inline int32_t get__level_7() const { return ____level_7; }
	inline int32_t* get_address_of__level_7() { return &____level_7; }
	inline void set__level_7(int32_t value)
	{
		____level_7 = value;
	}

	inline static int32_t get_offset_of__leaveOpen_8() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____leaveOpen_8)); }
	inline bool get__leaveOpen_8() const { return ____leaveOpen_8; }
	inline bool* get_address_of__leaveOpen_8() { return &____leaveOpen_8; }
	inline void set__leaveOpen_8(bool value)
	{
		____leaveOpen_8 = value;
	}

	inline static int32_t get_offset_of__workingBuffer_9() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____workingBuffer_9)); }
	inline ByteU5BU5D_t3397334013* get__workingBuffer_9() const { return ____workingBuffer_9; }
	inline ByteU5BU5D_t3397334013** get_address_of__workingBuffer_9() { return &____workingBuffer_9; }
	inline void set__workingBuffer_9(ByteU5BU5D_t3397334013* value)
	{
		____workingBuffer_9 = value;
		Il2CppCodeGenWriteBarrier(&____workingBuffer_9, value);
	}

	inline static int32_t get_offset_of__bufferSize_10() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____bufferSize_10)); }
	inline int32_t get__bufferSize_10() const { return ____bufferSize_10; }
	inline int32_t* get_address_of__bufferSize_10() { return &____bufferSize_10; }
	inline void set__bufferSize_10(int32_t value)
	{
		____bufferSize_10 = value;
	}

	inline static int32_t get_offset_of_windowBitsMax_11() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ___windowBitsMax_11)); }
	inline int32_t get_windowBitsMax_11() const { return ___windowBitsMax_11; }
	inline int32_t* get_address_of_windowBitsMax_11() { return &___windowBitsMax_11; }
	inline void set_windowBitsMax_11(int32_t value)
	{
		___windowBitsMax_11 = value;
	}

	inline static int32_t get_offset_of__buf1_12() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____buf1_12)); }
	inline ByteU5BU5D_t3397334013* get__buf1_12() const { return ____buf1_12; }
	inline ByteU5BU5D_t3397334013** get_address_of__buf1_12() { return &____buf1_12; }
	inline void set__buf1_12(ByteU5BU5D_t3397334013* value)
	{
		____buf1_12 = value;
		Il2CppCodeGenWriteBarrier(&____buf1_12, value);
	}

	inline static int32_t get_offset_of__stream_13() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____stream_13)); }
	inline Stream_t3255436806 * get__stream_13() const { return ____stream_13; }
	inline Stream_t3255436806 ** get_address_of__stream_13() { return &____stream_13; }
	inline void set__stream_13(Stream_t3255436806 * value)
	{
		____stream_13 = value;
		Il2CppCodeGenWriteBarrier(&____stream_13, value);
	}

	inline static int32_t get_offset_of_Strategy_14() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ___Strategy_14)); }
	inline int32_t get_Strategy_14() const { return ___Strategy_14; }
	inline int32_t* get_address_of_Strategy_14() { return &___Strategy_14; }
	inline void set_Strategy_14(int32_t value)
	{
		___Strategy_14 = value;
	}

	inline static int32_t get_offset_of_crc_15() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ___crc_15)); }
	inline CRC32_t2268741539 * get_crc_15() const { return ___crc_15; }
	inline CRC32_t2268741539 ** get_address_of_crc_15() { return &___crc_15; }
	inline void set_crc_15(CRC32_t2268741539 * value)
	{
		___crc_15 = value;
		Il2CppCodeGenWriteBarrier(&___crc_15, value);
	}

	inline static int32_t get_offset_of__GzipFileName_16() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____GzipFileName_16)); }
	inline String_t* get__GzipFileName_16() const { return ____GzipFileName_16; }
	inline String_t** get_address_of__GzipFileName_16() { return &____GzipFileName_16; }
	inline void set__GzipFileName_16(String_t* value)
	{
		____GzipFileName_16 = value;
		Il2CppCodeGenWriteBarrier(&____GzipFileName_16, value);
	}

	inline static int32_t get_offset_of__GzipComment_17() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____GzipComment_17)); }
	inline String_t* get__GzipComment_17() const { return ____GzipComment_17; }
	inline String_t** get_address_of__GzipComment_17() { return &____GzipComment_17; }
	inline void set__GzipComment_17(String_t* value)
	{
		____GzipComment_17 = value;
		Il2CppCodeGenWriteBarrier(&____GzipComment_17, value);
	}

	inline static int32_t get_offset_of__GzipMtime_18() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____GzipMtime_18)); }
	inline DateTime_t693205669  get__GzipMtime_18() const { return ____GzipMtime_18; }
	inline DateTime_t693205669 * get_address_of__GzipMtime_18() { return &____GzipMtime_18; }
	inline void set__GzipMtime_18(DateTime_t693205669  value)
	{
		____GzipMtime_18 = value;
	}

	inline static int32_t get_offset_of__gzipHeaderByteCount_19() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ____gzipHeaderByteCount_19)); }
	inline int32_t get__gzipHeaderByteCount_19() const { return ____gzipHeaderByteCount_19; }
	inline int32_t* get_address_of__gzipHeaderByteCount_19() { return &____gzipHeaderByteCount_19; }
	inline void set__gzipHeaderByteCount_19(int32_t value)
	{
		____gzipHeaderByteCount_19 = value;
	}

	inline static int32_t get_offset_of_nomoreinput_20() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t3383394762, ___nomoreinput_20)); }
	inline bool get_nomoreinput_20() const { return ___nomoreinput_20; }
	inline bool* get_address_of_nomoreinput_20() { return &___nomoreinput_20; }
	inline void set_nomoreinput_20(bool value)
	{
		___nomoreinput_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
