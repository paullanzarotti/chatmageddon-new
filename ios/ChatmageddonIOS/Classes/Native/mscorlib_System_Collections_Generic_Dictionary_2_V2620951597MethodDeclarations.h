﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3281180659MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1516460273(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2620951597 *, Dictionary_2_t3917891754 *, const MethodInfo*))ValueCollection__ctor_m1130550637_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3168902815(__this, ___item0, method) ((  void (*) (ValueCollection_t2620951597 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m217425835_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3104967968(__this, method) ((  void (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2289377654_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2831243769(__this, ___item0, method) ((  bool (*) (ValueCollection_t2620951597 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m366745989_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1122936842(__this, ___item0, method) ((  bool (*) (ValueCollection_t2620951597 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m851237364_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2229052582(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3711002716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m289622180(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2620951597 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3834982446_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2434933975(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m462519307_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3764786750(__this, method) ((  bool (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1697003576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1205585312(__this, method) ((  bool (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1852695594_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m400918526(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1586717140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4211489868(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2620951597 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1062615890_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3781385873(__this, method) ((  Enumerator_t1309457222  (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_GetEnumerator_m978813709_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.String>::get_Count()
#define ValueCollection_get_Count_m2415117672(__this, method) ((  int32_t (*) (ValueCollection_t2620951597 *, const MethodInfo*))ValueCollection_get_Count_m2397638386_gshared)(__this, method)
