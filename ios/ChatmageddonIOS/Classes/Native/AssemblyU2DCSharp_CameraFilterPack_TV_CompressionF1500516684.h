﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_TV_CompressionFX
struct  CameraFilterPack_TV_CompressionFX_t1500516684  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_TV_CompressionFX::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// UnityEngine.Vector4 CameraFilterPack_TV_CompressionFX::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_3;
	// System.Single CameraFilterPack_TV_CompressionFX::TimeX
	float ___TimeX_4;
	// System.Single CameraFilterPack_TV_CompressionFX::Parasite
	float ___Parasite_5;
	// UnityEngine.Material CameraFilterPack_TV_CompressionFX::SCMaterial
	Material_t193706927 * ___SCMaterial_6;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_CompressionFX_t1500516684, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_ScreenResolution_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_CompressionFX_t1500516684, ___ScreenResolution_3)); }
	inline Vector4_t2243707581  get_ScreenResolution_3() const { return ___ScreenResolution_3; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_3() { return &___ScreenResolution_3; }
	inline void set_ScreenResolution_3(Vector4_t2243707581  value)
	{
		___ScreenResolution_3 = value;
	}

	inline static int32_t get_offset_of_TimeX_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_CompressionFX_t1500516684, ___TimeX_4)); }
	inline float get_TimeX_4() const { return ___TimeX_4; }
	inline float* get_address_of_TimeX_4() { return &___TimeX_4; }
	inline void set_TimeX_4(float value)
	{
		___TimeX_4 = value;
	}

	inline static int32_t get_offset_of_Parasite_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_CompressionFX_t1500516684, ___Parasite_5)); }
	inline float get_Parasite_5() const { return ___Parasite_5; }
	inline float* get_address_of_Parasite_5() { return &___Parasite_5; }
	inline void set_Parasite_5(float value)
	{
		___Parasite_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_CompressionFX_t1500516684, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
