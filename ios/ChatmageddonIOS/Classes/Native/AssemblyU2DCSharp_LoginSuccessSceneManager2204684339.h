﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen1583510691.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginSuccessSceneManager
struct  LoginSuccessSceneManager_t2204684339  : public BaseSceneManager_1_t1583510691
{
public:
	// UnityEngine.GameObject LoginSuccessSceneManager::loginSuccessCenterUI
	GameObject_t1756533147 * ___loginSuccessCenterUI_17;
	// UnityEngine.GameObject LoginSuccessSceneManager::connectionPopUp
	GameObject_t1756533147 * ___connectionPopUp_18;
	// System.Action LoginSuccessSceneManager::LoadFunctionToRecall
	Action_t3226471752 * ___LoadFunctionToRecall_19;

public:
	inline static int32_t get_offset_of_loginSuccessCenterUI_17() { return static_cast<int32_t>(offsetof(LoginSuccessSceneManager_t2204684339, ___loginSuccessCenterUI_17)); }
	inline GameObject_t1756533147 * get_loginSuccessCenterUI_17() const { return ___loginSuccessCenterUI_17; }
	inline GameObject_t1756533147 ** get_address_of_loginSuccessCenterUI_17() { return &___loginSuccessCenterUI_17; }
	inline void set_loginSuccessCenterUI_17(GameObject_t1756533147 * value)
	{
		___loginSuccessCenterUI_17 = value;
		Il2CppCodeGenWriteBarrier(&___loginSuccessCenterUI_17, value);
	}

	inline static int32_t get_offset_of_connectionPopUp_18() { return static_cast<int32_t>(offsetof(LoginSuccessSceneManager_t2204684339, ___connectionPopUp_18)); }
	inline GameObject_t1756533147 * get_connectionPopUp_18() const { return ___connectionPopUp_18; }
	inline GameObject_t1756533147 ** get_address_of_connectionPopUp_18() { return &___connectionPopUp_18; }
	inline void set_connectionPopUp_18(GameObject_t1756533147 * value)
	{
		___connectionPopUp_18 = value;
		Il2CppCodeGenWriteBarrier(&___connectionPopUp_18, value);
	}

	inline static int32_t get_offset_of_LoadFunctionToRecall_19() { return static_cast<int32_t>(offsetof(LoginSuccessSceneManager_t2204684339, ___LoadFunctionToRecall_19)); }
	inline Action_t3226471752 * get_LoadFunctionToRecall_19() const { return ___LoadFunctionToRecall_19; }
	inline Action_t3226471752 ** get_address_of_LoadFunctionToRecall_19() { return &___LoadFunctionToRecall_19; }
	inline void set_LoadFunctionToRecall_19(Action_t3226471752 * value)
	{
		___LoadFunctionToRecall_19 = value;
		Il2CppCodeGenWriteBarrier(&___LoadFunctionToRecall_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
