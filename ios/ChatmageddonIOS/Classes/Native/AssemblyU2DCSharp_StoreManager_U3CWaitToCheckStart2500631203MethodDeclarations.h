﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreManager/<WaitToCheckStarts>c__Iterator0
struct U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StoreManager/<WaitToCheckStarts>c__Iterator0::.ctor()
extern "C"  void U3CWaitToCheckStartsU3Ec__Iterator0__ctor_m2887329584 (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StoreManager/<WaitToCheckStarts>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToCheckStartsU3Ec__Iterator0_MoveNext_m965012672 (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<WaitToCheckStarts>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToCheckStartsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3163625952 (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<WaitToCheckStarts>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToCheckStartsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2533018136 (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<WaitToCheckStarts>c__Iterator0::Dispose()
extern "C"  void U3CWaitToCheckStartsU3Ec__Iterator0_Dispose_m1036833649 (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<WaitToCheckStarts>c__Iterator0::Reset()
extern "C"  void U3CWaitToCheckStartsU3Ec__Iterator0_Reset_m1814713623 (U3CWaitToCheckStartsU3Ec__Iterator0_t2500631203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
