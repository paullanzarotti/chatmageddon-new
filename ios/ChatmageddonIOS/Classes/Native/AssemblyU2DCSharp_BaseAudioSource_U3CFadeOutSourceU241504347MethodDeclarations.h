﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseAudioSource/<FadeOutSource>c__Iterator2
struct U3CFadeOutSourceU3Ec__Iterator2_t241504347;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseAudioSource/<FadeOutSource>c__Iterator2::.ctor()
extern "C"  void U3CFadeOutSourceU3Ec__Iterator2__ctor_m2452456344 (U3CFadeOutSourceU3Ec__Iterator2_t241504347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseAudioSource/<FadeOutSource>c__Iterator2::MoveNext()
extern "C"  bool U3CFadeOutSourceU3Ec__Iterator2_MoveNext_m3375166400 (U3CFadeOutSourceU3Ec__Iterator2_t241504347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BaseAudioSource/<FadeOutSource>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeOutSourceU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3848062006 (U3CFadeOutSourceU3Ec__Iterator2_t241504347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BaseAudioSource/<FadeOutSource>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeOutSourceU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2153255502 (U3CFadeOutSourceU3Ec__Iterator2_t241504347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource/<FadeOutSource>c__Iterator2::Dispose()
extern "C"  void U3CFadeOutSourceU3Ec__Iterator2_Dispose_m2629866573 (U3CFadeOutSourceU3Ec__Iterator2_t241504347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource/<FadeOutSource>c__Iterator2::Reset()
extern "C"  void U3CFadeOutSourceU3Ec__Iterator2_Reset_m3091421179 (U3CFadeOutSourceU3Ec__Iterator2_t241504347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
