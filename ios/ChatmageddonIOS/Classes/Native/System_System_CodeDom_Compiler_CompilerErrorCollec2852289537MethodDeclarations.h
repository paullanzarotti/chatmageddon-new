﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CompilerErrorCollection
struct CompilerErrorCollection_t2852289537;
// System.CodeDom.Compiler.CompilerError
struct CompilerError_t2965933621;

#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_Compiler_CompilerError2965933621.h"

// System.Void System.CodeDom.Compiler.CompilerErrorCollection::.ctor()
extern "C"  void CompilerErrorCollection__ctor_m1747532528 (CompilerErrorCollection_t2852289537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.CodeDom.Compiler.CompilerErrorCollection::Add(System.CodeDom.Compiler.CompilerError)
extern "C"  int32_t CompilerErrorCollection_Add_m406376595 (CompilerErrorCollection_t2852289537 * __this, CompilerError_t2965933621 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.CompilerErrorCollection::get_HasErrors()
extern "C"  bool CompilerErrorCollection_get_HasErrors_m1218000327 (CompilerErrorCollection_t2852289537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
