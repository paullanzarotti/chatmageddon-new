﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DateTimeCompoundPicker
struct DateTimeCompoundPicker_t558724190;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DateTimeCompoundPicker::.ctor()
extern "C"  void DateTimeCompoundPicker__ctor_m4096027105 (DateTimeCompoundPicker_t558724190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateTimeCompoundPicker::get_IsMoving()
extern "C"  bool DateTimeCompoundPicker_get_IsMoving_m967491512 (DateTimeCompoundPicker_t558724190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateTimeCompoundPicker::Awake()
extern "C"  void DateTimeCompoundPicker_Awake_m1689231022 (DateTimeCompoundPicker_t558724190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateTimeCompoundPicker::Start()
extern "C"  void DateTimeCompoundPicker_Start_m3767801929 (DateTimeCompoundPicker_t558724190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime DateTimeCompoundPicker::GetSelectedDateTime(System.Boolean&)
extern "C"  DateTime_t693205669  DateTimeCompoundPicker_GetSelectedDateTime_m3327517059 (DateTimeCompoundPicker_t558724190 * __this, bool* ___isStillMoving0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateTimeCompoundPicker::SetSelectedDateTime(System.DateTime)
extern "C"  void DateTimeCompoundPicker_SetSelectedDateTime_m2550895921 (DateTimeCompoundPicker_t558724190 * __this, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DateTimeCompoundPicker::SetNewLanguage(System.String)
extern "C"  Il2CppObject * DateTimeCompoundPicker_SetNewLanguage_m362743049 (DateTimeCompoundPicker_t558724190 * __this, String_t* ___language0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
