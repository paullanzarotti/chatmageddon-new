﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FeedManager
struct FeedManager_t2709865823;
// System.String
struct String_t;
// Feed
struct Feed_t3408144164;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FeedManager::.ctor()
extern "C"  void FeedManager__ctor_m1271240854 (FeedManager_t2709865823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeedManager::Init()
extern "C"  void FeedManager_Init_m2860345190 (FeedManager_t2709865823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeedManager::OpenAllFeeds()
extern "C"  void FeedManager_OpenAllFeeds_m3634706820 (FeedManager_t2709865823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeedManager::ForceFeedUpdate(System.String)
extern "C"  void FeedManager_ForceFeedUpdate_m1785209650 (FeedManager_t2709865823 * __this, String_t* ___feedName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Feed FeedManager::GetFeed(System.String)
extern "C"  Feed_t3408144164 * FeedManager_GetFeed_m146721921 (FeedManager_t2709865823 * __this, String_t* ___feedName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeedManager::CloseAllFeeds()
extern "C"  void FeedManager_CloseAllFeeds_m4023578938 (FeedManager_t2709865823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
