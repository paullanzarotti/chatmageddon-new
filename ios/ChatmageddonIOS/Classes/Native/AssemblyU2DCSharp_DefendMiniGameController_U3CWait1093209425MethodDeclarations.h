﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendMiniGameController/<WaitToUnloadModal>c__Iterator0
struct U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DefendMiniGameController/<WaitToUnloadModal>c__Iterator0::.ctor()
extern "C"  void U3CWaitToUnloadModalU3Ec__Iterator0__ctor_m1160845434 (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DefendMiniGameController/<WaitToUnloadModal>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToUnloadModalU3Ec__Iterator0_MoveNext_m2031931566 (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DefendMiniGameController/<WaitToUnloadModal>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToUnloadModalU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2054065322 (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DefendMiniGameController/<WaitToUnloadModal>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToUnloadModalU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3910609714 (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController/<WaitToUnloadModal>c__Iterator0::Dispose()
extern "C"  void U3CWaitToUnloadModalU3Ec__Iterator0_Dispose_m2285660131 (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController/<WaitToUnloadModal>c__Iterator0::Reset()
extern "C"  void U3CWaitToUnloadModalU3Ec__Iterator0_Reset_m3124038569 (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
