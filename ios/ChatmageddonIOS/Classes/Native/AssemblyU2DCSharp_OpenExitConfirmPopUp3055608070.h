﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitConfirmPopUp
struct ExitConfirmPopUp_t674592142;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenExitConfirmPopUp
struct  OpenExitConfirmPopUp_t3055608070  : public SFXButton_t792651341
{
public:
	// ExitConfirmPopUp OpenExitConfirmPopUp::popUp
	ExitConfirmPopUp_t674592142 * ___popUp_5;

public:
	inline static int32_t get_offset_of_popUp_5() { return static_cast<int32_t>(offsetof(OpenExitConfirmPopUp_t3055608070, ___popUp_5)); }
	inline ExitConfirmPopUp_t674592142 * get_popUp_5() const { return ___popUp_5; }
	inline ExitConfirmPopUp_t674592142 ** get_address_of_popUp_5() { return &___popUp_5; }
	inline void set_popUp_5(ExitConfirmPopUp_t674592142 * value)
	{
		___popUp_5 = value;
		Il2CppCodeGenWriteBarrier(&___popUp_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
