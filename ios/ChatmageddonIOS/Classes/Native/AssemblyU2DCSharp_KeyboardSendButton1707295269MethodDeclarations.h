﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KeyboardSendButton
struct KeyboardSendButton_t1707295269;

#include "codegen/il2cpp-codegen.h"

// System.Void KeyboardSendButton::.ctor()
extern "C"  void KeyboardSendButton__ctor_m2856582118 (KeyboardSendButton_t1707295269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KeyboardSendButton::OnButtonClick()
extern "C"  void KeyboardSendButton_OnButtonClick_m553106221 (KeyboardSendButton_t1707295269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
