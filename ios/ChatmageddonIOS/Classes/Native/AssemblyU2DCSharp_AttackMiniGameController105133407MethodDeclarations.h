﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackMiniGameController
struct AttackMiniGameController_t105133407;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackGameState4284764851.h"

// System.Void AttackMiniGameController::.ctor()
extern "C"  void AttackMiniGameController__ctor_m829240884 (AttackMiniGameController_t105133407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::StartMiniGame()
extern "C"  void AttackMiniGameController_StartMiniGame_m1378311519 (AttackMiniGameController_t105133407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::StopMiniGame()
extern "C"  void AttackMiniGameController_StopMiniGame_m2333053199 (AttackMiniGameController_t105133407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::ChangeGameState(AttackGameState)
extern "C"  void AttackMiniGameController_ChangeGameState_m2030128426 (AttackMiniGameController_t105133407 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::CalculateFinalScores()
extern "C"  void AttackMiniGameController_CalculateFinalScores_m530655745 (AttackMiniGameController_t105133407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::CheckLaunchMissileType()
extern "C"  void AttackMiniGameController_CheckLaunchMissileType_m281627197 (AttackMiniGameController_t105133407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AttackMiniGameController::WaitToLaunchMissile(System.Boolean)
extern "C"  Il2CppObject * AttackMiniGameController_WaitToLaunchMissile_m1605060268 (AttackMiniGameController_t105133407 * __this, bool ___standard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::LaunchMissile(System.Boolean)
extern "C"  void AttackMiniGameController_LaunchMissile_m2896987176 (AttackMiniGameController_t105133407 * __this, bool ___standard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AttackMiniGameController::wait()
extern "C"  Il2CppObject * AttackMiniGameController_wait_m1322318247 (AttackMiniGameController_t105133407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController::<CheckLaunchMissileType>m__0(System.Boolean)
extern "C"  void AttackMiniGameController_U3CCheckLaunchMissileTypeU3Em__0_m1618407469 (AttackMiniGameController_t105133407 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
