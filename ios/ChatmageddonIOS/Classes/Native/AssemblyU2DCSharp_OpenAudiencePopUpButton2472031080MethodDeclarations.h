﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenAudiencePopUpButton
struct OpenAudiencePopUpButton_t2472031080;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenAudiencePopUpButton::.ctor()
extern "C"  void OpenAudiencePopUpButton__ctor_m1619506979 (OpenAudiencePopUpButton_t2472031080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenAudiencePopUpButton::OnButtonClick()
extern "C"  void OpenAudiencePopUpButton_OnButtonClick_m2281342700 (OpenAudiencePopUpButton_t2472031080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
