﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CustomDownloadTileExample
struct CustomDownloadTileExample_t2726765381;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"

// System.Void CustomDownloadTileExample::.ctor()
extern "C"  void CustomDownloadTileExample__ctor_m1798350382 (CustomDownloadTileExample_t2726765381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomDownloadTileExample::Start()
extern "C"  void CustomDownloadTileExample_Start_m845962806 (CustomDownloadTileExample_t2726765381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomDownloadTileExample::OnStartDownloadTile(OnlineMapsTile)
extern "C"  void CustomDownloadTileExample_OnStartDownloadTile_m1005645789 (CustomDownloadTileExample_t2726765381 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
