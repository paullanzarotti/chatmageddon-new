﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.IHTTPClient
struct IHTTPClient_t1282637506;
// Uniject.IUtil
struct IUtil_t2188430191;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.AnalyticsReporter
struct  AnalyticsReporter_t4061074961  : public Il2CppObject
{
public:
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.AnalyticsReporter::config
	UnibillConfiguration_t2915611853 * ___config_3;
	// Unibill.Impl.IHTTPClient Unibill.Impl.AnalyticsReporter::client
	Il2CppObject * ___client_4;
	// Uniject.IUtil Unibill.Impl.AnalyticsReporter::util
	Il2CppObject * ___util_5;
	// System.String Unibill.Impl.AnalyticsReporter::userId
	String_t* ___userId_6;
	// System.Boolean Unibill.Impl.AnalyticsReporter::restoreInProgress
	bool ___restoreInProgress_7;
	// System.String Unibill.Impl.AnalyticsReporter::levelName
	String_t* ___levelName_8;
	// System.DateTime Unibill.Impl.AnalyticsReporter::levelLoadTime
	DateTime_t693205669  ___levelLoadTime_9;

public:
	inline static int32_t get_offset_of_config_3() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___config_3)); }
	inline UnibillConfiguration_t2915611853 * get_config_3() const { return ___config_3; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_config_3() { return &___config_3; }
	inline void set_config_3(UnibillConfiguration_t2915611853 * value)
	{
		___config_3 = value;
		Il2CppCodeGenWriteBarrier(&___config_3, value);
	}

	inline static int32_t get_offset_of_client_4() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___client_4)); }
	inline Il2CppObject * get_client_4() const { return ___client_4; }
	inline Il2CppObject ** get_address_of_client_4() { return &___client_4; }
	inline void set_client_4(Il2CppObject * value)
	{
		___client_4 = value;
		Il2CppCodeGenWriteBarrier(&___client_4, value);
	}

	inline static int32_t get_offset_of_util_5() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___util_5)); }
	inline Il2CppObject * get_util_5() const { return ___util_5; }
	inline Il2CppObject ** get_address_of_util_5() { return &___util_5; }
	inline void set_util_5(Il2CppObject * value)
	{
		___util_5 = value;
		Il2CppCodeGenWriteBarrier(&___util_5, value);
	}

	inline static int32_t get_offset_of_userId_6() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___userId_6)); }
	inline String_t* get_userId_6() const { return ___userId_6; }
	inline String_t** get_address_of_userId_6() { return &___userId_6; }
	inline void set_userId_6(String_t* value)
	{
		___userId_6 = value;
		Il2CppCodeGenWriteBarrier(&___userId_6, value);
	}

	inline static int32_t get_offset_of_restoreInProgress_7() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___restoreInProgress_7)); }
	inline bool get_restoreInProgress_7() const { return ___restoreInProgress_7; }
	inline bool* get_address_of_restoreInProgress_7() { return &___restoreInProgress_7; }
	inline void set_restoreInProgress_7(bool value)
	{
		___restoreInProgress_7 = value;
	}

	inline static int32_t get_offset_of_levelName_8() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___levelName_8)); }
	inline String_t* get_levelName_8() const { return ___levelName_8; }
	inline String_t** get_address_of_levelName_8() { return &___levelName_8; }
	inline void set_levelName_8(String_t* value)
	{
		___levelName_8 = value;
		Il2CppCodeGenWriteBarrier(&___levelName_8, value);
	}

	inline static int32_t get_offset_of_levelLoadTime_9() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t4061074961, ___levelLoadTime_9)); }
	inline DateTime_t693205669  get_levelLoadTime_9() const { return ___levelLoadTime_9; }
	inline DateTime_t693205669 * get_address_of_levelLoadTime_9() { return &___levelLoadTime_9; }
	inline void set_levelLoadTime_9(DateTime_t693205669  value)
	{
		___levelLoadTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
