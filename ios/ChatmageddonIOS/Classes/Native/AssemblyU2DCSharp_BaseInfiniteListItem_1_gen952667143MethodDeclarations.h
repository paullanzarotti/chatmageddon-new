﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<MultiPhoneNumberListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m2142097877(__this, method) ((  void (*) (BaseInfiniteListItem_1_t952667143 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<MultiPhoneNumberListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m3456628796(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t952667143 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<MultiPhoneNumberListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m2366494570(__this, method) ((  void (*) (BaseInfiniteListItem_1_t952667143 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<MultiPhoneNumberListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m2745423559(__this, method) ((  void (*) (BaseInfiniteListItem_1_t952667143 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<MultiPhoneNumberListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m369403548(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t952667143 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<MultiPhoneNumberListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m1002551697(__this, method) ((  void (*) (BaseInfiniteListItem_1_t952667143 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<MultiPhoneNumberListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m3321168364(__this, method) ((  void (*) (BaseInfiniteListItem_1_t952667143 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
