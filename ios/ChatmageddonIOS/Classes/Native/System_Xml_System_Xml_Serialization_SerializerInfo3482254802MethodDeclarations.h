﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.SerializerInfo
struct SerializerInfo_t3482254802;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Xml.Serialization.Hook[]
struct HookU5BU5D_t914467364;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_HookType2601538049.h"
#include "System_Xml_System_Xml_Serialization_XmlMappingAcce3616682003.h"
#include "System_Xml_System_Xml_Serialization_HookAction2799868295.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"

// System.Collections.ArrayList System.Xml.Serialization.SerializerInfo::GetHooks(System.Xml.Serialization.HookType,System.Xml.Serialization.XmlMappingAccess,System.Xml.Serialization.HookAction,System.Type,System.String)
extern "C"  ArrayList_t4252133567 * SerializerInfo_GetHooks_m1428205903 (SerializerInfo_t3482254802 * __this, int32_t ___hookType0, int32_t ___dir1, int32_t ___action2, Type_t * ___type3, String_t* ___member4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Xml.Serialization.SerializerInfo::FindHook(System.Xml.Serialization.Hook[],System.Xml.Serialization.HookType,System.Xml.Serialization.HookAction,System.Type,System.String)
extern "C"  ArrayList_t4252133567 * SerializerInfo_FindHook_m3400810595 (SerializerInfo_t3482254802 * __this, HookU5BU5D_t914467364* ___hooks0, int32_t ___hookType1, int32_t ___action2, Type_t * ___type3, String_t* ___member4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
