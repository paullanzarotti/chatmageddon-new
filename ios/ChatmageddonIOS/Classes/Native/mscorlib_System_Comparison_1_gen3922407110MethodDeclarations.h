﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<PlayerProfileNavScreen>
struct Comparison_1_t3922407110;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Comparison`1<PlayerProfileNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m4037638390_gshared (Comparison_1_t3922407110 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m4037638390(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3922407110 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m4037638390_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<PlayerProfileNavScreen>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1091509370_gshared (Comparison_1_t3922407110 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m1091509370(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3922407110 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m1091509370_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<PlayerProfileNavScreen>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m1400085727_gshared (Comparison_1_t3922407110 * __this, int32_t ___x0, int32_t ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m1400085727(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3922407110 *, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1400085727_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<PlayerProfileNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m4253633476_gshared (Comparison_1_t3922407110 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m4253633476(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3922407110 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m4253633476_gshared)(__this, ___result0, method)
