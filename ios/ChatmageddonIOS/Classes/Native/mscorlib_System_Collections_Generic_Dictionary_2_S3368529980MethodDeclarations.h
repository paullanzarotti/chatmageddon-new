﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>
struct ShimEnumerator_t3368529980;
// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m525897983_gshared (ShimEnumerator_t3368529980 * __this, Dictionary_2_t3263405159 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m525897983(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3368529980 *, Dictionary_2_t3263405159 *, const MethodInfo*))ShimEnumerator__ctor_m525897983_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3160870186_gshared (ShimEnumerator_t3368529980 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3160870186(__this, method) ((  bool (*) (ShimEnumerator_t3368529980 *, const MethodInfo*))ShimEnumerator_MoveNext_m3160870186_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2060599818_gshared (ShimEnumerator_t3368529980 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2060599818(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3368529980 *, const MethodInfo*))ShimEnumerator_get_Entry_m2060599818_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2136116095_gshared (ShimEnumerator_t3368529980 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2136116095(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3368529980 *, const MethodInfo*))ShimEnumerator_get_Key_m2136116095_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4274585239_gshared (ShimEnumerator_t3368529980 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m4274585239(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3368529980 *, const MethodInfo*))ShimEnumerator_get_Value_m4274585239_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1083795577_gshared (ShimEnumerator_t3368529980 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1083795577(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3368529980 *, const MethodInfo*))ShimEnumerator_get_Current_m1083795577_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vectrosity.Vector3Pair,System.Boolean>::Reset()
extern "C"  void ShimEnumerator_Reset_m3015671813_gshared (ShimEnumerator_t3368529980 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3015671813(__this, method) ((  void (*) (ShimEnumerator_t3368529980 *, const MethodInfo*))ShimEnumerator_Reset_m3015671813_gshared)(__this, method)
