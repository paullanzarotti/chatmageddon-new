﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>
struct GenericEqualityComparer_1_t4003550509;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3770532840_gshared (GenericEqualityComparer_1_t4003550509 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m3770532840(__this, method) ((  void (*) (GenericEqualityComparer_1_t4003550509 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m3770532840_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3019823587_gshared (GenericEqualityComparer_1_t4003550509 * __this, Decimal_t724701077  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m3019823587(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t4003550509 *, Decimal_t724701077 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m3019823587_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m880188891_gshared (GenericEqualityComparer_1_t4003550509 * __this, Decimal_t724701077  ___x0, Decimal_t724701077  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m880188891(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t4003550509 *, Decimal_t724701077 , Decimal_t724701077 , const MethodInfo*))GenericEqualityComparer_1_Equals_m880188891_gshared)(__this, ___x0, ___y1, method)
