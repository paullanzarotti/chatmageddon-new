﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PNYourNumberNavScreen
struct PNYourNumberNavScreen_t1723852807;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void PNYourNumberNavScreen::.ctor()
extern "C"  void PNYourNumberNavScreen__ctor_m2707790252 (PNYourNumberNavScreen_t1723852807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNYourNumberNavScreen::Start()
extern "C"  void PNYourNumberNavScreen_Start_m2065982584 (PNYourNumberNavScreen_t1723852807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNYourNumberNavScreen::RegisterUIClose()
extern "C"  void PNYourNumberNavScreen_RegisterUIClose_m4188026801 (PNYourNumberNavScreen_t1723852807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNYourNumberNavScreen::YourNumberUIClosing()
extern "C"  void PNYourNumberNavScreen_YourNumberUIClosing_m3961722639 (PNYourNumberNavScreen_t1723852807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNYourNumberNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void PNYourNumberNavScreen_ScreenLoad_m2082265180 (PNYourNumberNavScreen_t1723852807 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNYourNumberNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void PNYourNumberNavScreen_ScreenUnload_m1617029712 (PNYourNumberNavScreen_t1723852807 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PNYourNumberNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool PNYourNumberNavScreen_ValidateScreenNavigation_m2425165505 (PNYourNumberNavScreen_t1723852807 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
