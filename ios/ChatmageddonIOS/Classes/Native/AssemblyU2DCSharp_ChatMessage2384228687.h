﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatMessage
struct  ChatMessage_t2384228687  : public Il2CppObject
{
public:
	// System.String ChatMessage::messageID
	String_t* ___messageID_0;
	// System.String ChatMessage::threadID
	String_t* ___threadID_1;
	// System.String ChatMessage::userID
	String_t* ___userID_2;
	// System.String ChatMessage::messageBody
	String_t* ___messageBody_3;
	// System.String ChatMessage::multichatUser
	String_t* ___multichatUser_4;
	// System.DateTime ChatMessage::createDateTime
	DateTime_t693205669  ___createDateTime_5;

public:
	inline static int32_t get_offset_of_messageID_0() { return static_cast<int32_t>(offsetof(ChatMessage_t2384228687, ___messageID_0)); }
	inline String_t* get_messageID_0() const { return ___messageID_0; }
	inline String_t** get_address_of_messageID_0() { return &___messageID_0; }
	inline void set_messageID_0(String_t* value)
	{
		___messageID_0 = value;
		Il2CppCodeGenWriteBarrier(&___messageID_0, value);
	}

	inline static int32_t get_offset_of_threadID_1() { return static_cast<int32_t>(offsetof(ChatMessage_t2384228687, ___threadID_1)); }
	inline String_t* get_threadID_1() const { return ___threadID_1; }
	inline String_t** get_address_of_threadID_1() { return &___threadID_1; }
	inline void set_threadID_1(String_t* value)
	{
		___threadID_1 = value;
		Il2CppCodeGenWriteBarrier(&___threadID_1, value);
	}

	inline static int32_t get_offset_of_userID_2() { return static_cast<int32_t>(offsetof(ChatMessage_t2384228687, ___userID_2)); }
	inline String_t* get_userID_2() const { return ___userID_2; }
	inline String_t** get_address_of_userID_2() { return &___userID_2; }
	inline void set_userID_2(String_t* value)
	{
		___userID_2 = value;
		Il2CppCodeGenWriteBarrier(&___userID_2, value);
	}

	inline static int32_t get_offset_of_messageBody_3() { return static_cast<int32_t>(offsetof(ChatMessage_t2384228687, ___messageBody_3)); }
	inline String_t* get_messageBody_3() const { return ___messageBody_3; }
	inline String_t** get_address_of_messageBody_3() { return &___messageBody_3; }
	inline void set_messageBody_3(String_t* value)
	{
		___messageBody_3 = value;
		Il2CppCodeGenWriteBarrier(&___messageBody_3, value);
	}

	inline static int32_t get_offset_of_multichatUser_4() { return static_cast<int32_t>(offsetof(ChatMessage_t2384228687, ___multichatUser_4)); }
	inline String_t* get_multichatUser_4() const { return ___multichatUser_4; }
	inline String_t** get_address_of_multichatUser_4() { return &___multichatUser_4; }
	inline void set_multichatUser_4(String_t* value)
	{
		___multichatUser_4 = value;
		Il2CppCodeGenWriteBarrier(&___multichatUser_4, value);
	}

	inline static int32_t get_offset_of_createDateTime_5() { return static_cast<int32_t>(offsetof(ChatMessage_t2384228687, ___createDateTime_5)); }
	inline DateTime_t693205669  get_createDateTime_5() const { return ___createDateTime_5; }
	inline DateTime_t693205669 * get_address_of_createDateTime_5() { return &___createDateTime_5; }
	inline void set_createDateTime_5(DateTime_t693205669  value)
	{
		___createDateTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
