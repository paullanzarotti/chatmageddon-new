﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen3412936559.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendModalNavigationController
struct  DefendModalNavigationController_t472169731  : public NavigationController_2_t3412936559
{
public:
	// UnityEngine.GameObject DefendModalNavigationController::infoObject
	GameObject_t1756533147 * ___infoObject_8;
	// UnityEngine.GameObject DefendModalNavigationController::defendGameObject
	GameObject_t1756533147 * ___defendGameObject_9;
	// UnityEngine.Vector3 DefendModalNavigationController::activePos
	Vector3_t2243707580  ___activePos_10;
	// UnityEngine.Vector3 DefendModalNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_11;
	// UnityEngine.Vector3 DefendModalNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_12;

public:
	inline static int32_t get_offset_of_infoObject_8() { return static_cast<int32_t>(offsetof(DefendModalNavigationController_t472169731, ___infoObject_8)); }
	inline GameObject_t1756533147 * get_infoObject_8() const { return ___infoObject_8; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_8() { return &___infoObject_8; }
	inline void set_infoObject_8(GameObject_t1756533147 * value)
	{
		___infoObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_8, value);
	}

	inline static int32_t get_offset_of_defendGameObject_9() { return static_cast<int32_t>(offsetof(DefendModalNavigationController_t472169731, ___defendGameObject_9)); }
	inline GameObject_t1756533147 * get_defendGameObject_9() const { return ___defendGameObject_9; }
	inline GameObject_t1756533147 ** get_address_of_defendGameObject_9() { return &___defendGameObject_9; }
	inline void set_defendGameObject_9(GameObject_t1756533147 * value)
	{
		___defendGameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___defendGameObject_9, value);
	}

	inline static int32_t get_offset_of_activePos_10() { return static_cast<int32_t>(offsetof(DefendModalNavigationController_t472169731, ___activePos_10)); }
	inline Vector3_t2243707580  get_activePos_10() const { return ___activePos_10; }
	inline Vector3_t2243707580 * get_address_of_activePos_10() { return &___activePos_10; }
	inline void set_activePos_10(Vector3_t2243707580  value)
	{
		___activePos_10 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_11() { return static_cast<int32_t>(offsetof(DefendModalNavigationController_t472169731, ___leftInactivePos_11)); }
	inline Vector3_t2243707580  get_leftInactivePos_11() const { return ___leftInactivePos_11; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_11() { return &___leftInactivePos_11; }
	inline void set_leftInactivePos_11(Vector3_t2243707580  value)
	{
		___leftInactivePos_11 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_12() { return static_cast<int32_t>(offsetof(DefendModalNavigationController_t472169731, ___rightInactivePos_12)); }
	inline Vector3_t2243707580  get_rightInactivePos_12() const { return ___rightInactivePos_12; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_12() { return &___rightInactivePos_12; }
	inline void set_rightInactivePos_12(Vector3_t2243707580  value)
	{
		___rightInactivePos_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
