﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.TwitterComboUI
struct TwitterComboUI_t471191695;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Prime31.TwitterComboUI::.ctor()
extern "C"  void TwitterComboUI__ctor_m2521891295 (TwitterComboUI_t471191695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterComboUI::Start()
extern "C"  void TwitterComboUI_Start_m3880653107 (TwitterComboUI_t471191695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterComboUI::completionHandler(System.String,System.Object)
extern "C"  void TwitterComboUI_completionHandler_m1619272743 (TwitterComboUI_t471191695 * __this, String_t* ___error0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterComboUI::OnGUI()
extern "C"  void TwitterComboUI_OnGUI_m1057970153 (TwitterComboUI_t471191695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
