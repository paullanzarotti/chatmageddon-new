﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector2[][]
struct Vector2U5BU5DU5BU5D_t4105933535;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorChar
struct  VectorChar_t1924892741  : public Il2CppObject
{
public:

public:
};

struct VectorChar_t1924892741_StaticFields
{
public:
	// UnityEngine.Vector2[][] Vectrosity.VectorChar::points
	Vector2U5BU5DU5BU5D_t4105933535* ___points_1;

public:
	inline static int32_t get_offset_of_points_1() { return static_cast<int32_t>(offsetof(VectorChar_t1924892741_StaticFields, ___points_1)); }
	inline Vector2U5BU5DU5BU5D_t4105933535* get_points_1() const { return ___points_1; }
	inline Vector2U5BU5DU5BU5D_t4105933535** get_address_of_points_1() { return &___points_1; }
	inline void set_points_1(Vector2U5BU5DU5BU5D_t4105933535* value)
	{
		___points_1 = value;
		Il2CppCodeGenWriteBarrier(&___points_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
