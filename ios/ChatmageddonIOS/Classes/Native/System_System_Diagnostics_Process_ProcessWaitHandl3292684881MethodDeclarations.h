﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.Process/ProcessWaitHandle
struct ProcessWaitHandle_t3292684881;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Diagnostics.Process/ProcessWaitHandle::.ctor(System.IntPtr)
extern "C"  void ProcessWaitHandle__ctor_m2107170811 (ProcessWaitHandle_t3292684881 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Diagnostics.Process/ProcessWaitHandle::ProcessHandle_duplicate(System.IntPtr)
extern "C"  IntPtr_t ProcessWaitHandle_ProcessHandle_duplicate_m201499283 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
