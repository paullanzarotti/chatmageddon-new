﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseMultiPhoneNumberUI
struct CloseMultiPhoneNumberUI_t914254952;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseMultiPhoneNumberUI::.ctor()
extern "C"  void CloseMultiPhoneNumberUI__ctor_m2421525219 (CloseMultiPhoneNumberUI_t914254952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseMultiPhoneNumberUI::OnButtonClick()
extern "C"  void CloseMultiPhoneNumberUI_OnButtonClick_m781767900 (CloseMultiPhoneNumberUI_t914254952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
