﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Film_Grain
struct CameraFilterPack_Film_Grain_t1407189687;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Film_Grain::.ctor()
extern "C"  void CameraFilterPack_Film_Grain__ctor_m3153607514 (CameraFilterPack_Film_Grain_t1407189687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Film_Grain::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Film_Grain_get_material_m1054810207 (CameraFilterPack_Film_Grain_t1407189687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::Start()
extern "C"  void CameraFilterPack_Film_Grain_Start_m1863188670 (CameraFilterPack_Film_Grain_t1407189687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Film_Grain_OnRenderImage_m2804883438 (CameraFilterPack_Film_Grain_t1407189687 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::OnValidate()
extern "C"  void CameraFilterPack_Film_Grain_OnValidate_m3696439237 (CameraFilterPack_Film_Grain_t1407189687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::Update()
extern "C"  void CameraFilterPack_Film_Grain_Update_m3423599543 (CameraFilterPack_Film_Grain_t1407189687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::OnDisable()
extern "C"  void CameraFilterPack_Film_Grain_OnDisable_m4002892175 (CameraFilterPack_Film_Grain_t1407189687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
