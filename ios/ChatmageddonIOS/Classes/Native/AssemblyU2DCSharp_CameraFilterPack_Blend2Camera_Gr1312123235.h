﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blend2Camera_GreenScreen
struct  CameraFilterPack_Blend2Camera_GreenScreen_t1312123235  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Blend2Camera_GreenScreen::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_GreenScreen::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_GreenScreen::Camera2
	Camera_t189460977 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::TimeX
	float ___TimeX_5;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_GreenScreen::SCMaterial
	Material_t193706927 * ___SCMaterial_6;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::BlendFX
	float ___BlendFX_7;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::Adjust
	float ___Adjust_8;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::Precision
	float ___Precision_9;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::Luminosity
	float ___Luminosity_10;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::Change_Red
	float ___Change_Red_11;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::Change_Green
	float ___Change_Green_12;
	// System.Single CameraFilterPack_Blend2Camera_GreenScreen::Change_Blue
	float ___Change_Blue_13;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_GreenScreen::Camera2tex
	RenderTexture_t2666733923 * ___Camera2tex_14;
	// UnityEngine.Vector2 CameraFilterPack_Blend2Camera_GreenScreen::ScreenSize
	Vector2_t2243707579  ___ScreenSize_15;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_Camera2_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Camera2_4)); }
	inline Camera_t189460977 * get_Camera2_4() const { return ___Camera2_4; }
	inline Camera_t189460977 ** get_address_of_Camera2_4() { return &___Camera2_4; }
	inline void set_Camera2_4(Camera_t189460977 * value)
	{
		___Camera2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2_4, value);
	}

	inline static int32_t get_offset_of_TimeX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___TimeX_5)); }
	inline float get_TimeX_5() const { return ___TimeX_5; }
	inline float* get_address_of_TimeX_5() { return &___TimeX_5; }
	inline void set_TimeX_5(float value)
	{
		___TimeX_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}

	inline static int32_t get_offset_of_BlendFX_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___BlendFX_7)); }
	inline float get_BlendFX_7() const { return ___BlendFX_7; }
	inline float* get_address_of_BlendFX_7() { return &___BlendFX_7; }
	inline void set_BlendFX_7(float value)
	{
		___BlendFX_7 = value;
	}

	inline static int32_t get_offset_of_Adjust_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Adjust_8)); }
	inline float get_Adjust_8() const { return ___Adjust_8; }
	inline float* get_address_of_Adjust_8() { return &___Adjust_8; }
	inline void set_Adjust_8(float value)
	{
		___Adjust_8 = value;
	}

	inline static int32_t get_offset_of_Precision_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Precision_9)); }
	inline float get_Precision_9() const { return ___Precision_9; }
	inline float* get_address_of_Precision_9() { return &___Precision_9; }
	inline void set_Precision_9(float value)
	{
		___Precision_9 = value;
	}

	inline static int32_t get_offset_of_Luminosity_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Luminosity_10)); }
	inline float get_Luminosity_10() const { return ___Luminosity_10; }
	inline float* get_address_of_Luminosity_10() { return &___Luminosity_10; }
	inline void set_Luminosity_10(float value)
	{
		___Luminosity_10 = value;
	}

	inline static int32_t get_offset_of_Change_Red_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Change_Red_11)); }
	inline float get_Change_Red_11() const { return ___Change_Red_11; }
	inline float* get_address_of_Change_Red_11() { return &___Change_Red_11; }
	inline void set_Change_Red_11(float value)
	{
		___Change_Red_11 = value;
	}

	inline static int32_t get_offset_of_Change_Green_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Change_Green_12)); }
	inline float get_Change_Green_12() const { return ___Change_Green_12; }
	inline float* get_address_of_Change_Green_12() { return &___Change_Green_12; }
	inline void set_Change_Green_12(float value)
	{
		___Change_Green_12 = value;
	}

	inline static int32_t get_offset_of_Change_Blue_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Change_Blue_13)); }
	inline float get_Change_Blue_13() const { return ___Change_Blue_13; }
	inline float* get_address_of_Change_Blue_13() { return &___Change_Blue_13; }
	inline void set_Change_Blue_13(float value)
	{
		___Change_Blue_13 = value;
	}

	inline static int32_t get_offset_of_Camera2tex_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___Camera2tex_14)); }
	inline RenderTexture_t2666733923 * get_Camera2tex_14() const { return ___Camera2tex_14; }
	inline RenderTexture_t2666733923 ** get_address_of_Camera2tex_14() { return &___Camera2tex_14; }
	inline void set_Camera2tex_14(RenderTexture_t2666733923 * value)
	{
		___Camera2tex_14 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2tex_14, value);
	}

	inline static int32_t get_offset_of_ScreenSize_15() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_GreenScreen_t1312123235, ___ScreenSize_15)); }
	inline Vector2_t2243707579  get_ScreenSize_15() const { return ___ScreenSize_15; }
	inline Vector2_t2243707579 * get_address_of_ScreenSize_15() { return &___ScreenSize_15; }
	inline void set_ScreenSize_15(Vector2_t2243707579  value)
	{
		___ScreenSize_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
