﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2766651291MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3030309812(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t386463257 *, Dictionary_2_t2197932782 *, const MethodInfo*))KeyCollection__ctor_m3838914573_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2995561586(__this, ___item0, method) ((  void (*) (KeyCollection_t386463257 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2987932827_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3215464313(__this, method) ((  void (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1722159286_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m253556850(__this, ___item0, method) ((  bool (*) (KeyCollection_t386463257 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1396153341_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4129627485(__this, ___item0, method) ((  bool (*) (KeyCollection_t386463257 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3920768710_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2983066967(__this, method) ((  Il2CppObject* (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2220136930_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2071920899(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t386463257 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3823530176_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3449318318(__this, method) ((  Il2CppObject * (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2718737043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m519048785(__this, method) ((  bool (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2556458832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1540849283(__this, method) ((  bool (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497235644_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m882369107(__this, method) ((  Il2CppObject * (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3123193354_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1349345085(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t386463257 *, BillingPlatformU5BU5D_t1616278231*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3539548866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3873192632(__this, method) ((  Enumerator_t592468924  (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_GetEnumerator_m1419934541_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Count()
#define KeyCollection_get_Count_m3344656891(__this, method) ((  int32_t (*) (KeyCollection_t386463257 *, const MethodInfo*))KeyCollection_get_Count_m95902596_gshared)(__this, method)
