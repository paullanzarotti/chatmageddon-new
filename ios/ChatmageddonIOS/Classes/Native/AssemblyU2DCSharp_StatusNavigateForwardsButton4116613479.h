﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusNavigateForwardsButton
struct  StatusNavigateForwardsButton_t4116613479  : public SFXButton_t792651341
{
public:
	// StatusNavScreen StatusNavigateForwardsButton::navScreen
	int32_t ___navScreen_5;

public:
	inline static int32_t get_offset_of_navScreen_5() { return static_cast<int32_t>(offsetof(StatusNavigateForwardsButton_t4116613479, ___navScreen_5)); }
	inline int32_t get_navScreen_5() const { return ___navScreen_5; }
	inline int32_t* get_address_of_navScreen_5() { return &___navScreen_5; }
	inline void set_navScreen_5(int32_t value)
	{
		___navScreen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
