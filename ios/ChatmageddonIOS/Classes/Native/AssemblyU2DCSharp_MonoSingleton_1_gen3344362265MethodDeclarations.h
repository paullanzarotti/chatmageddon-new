﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<MapManager>::.ctor()
#define MonoSingleton_1__ctor_m2289588051(__this, method) ((  void (*) (MonoSingleton_1_t3344362265 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<MapManager>::Awake()
#define MonoSingleton_1_Awake_m2475731026(__this, method) ((  void (*) (MonoSingleton_1_t3344362265 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<MapManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3809739556(__this /* static, unused */, method) ((  MapManager_t3593696545 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<MapManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2547723652(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<MapManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3785016977(__this, method) ((  void (*) (MonoSingleton_1_t3344362265 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<MapManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3007696553(__this, method) ((  void (*) (MonoSingleton_1_t3344362265 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<MapManager>::.cctor()
#define MonoSingleton_1__cctor_m3686971092(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
