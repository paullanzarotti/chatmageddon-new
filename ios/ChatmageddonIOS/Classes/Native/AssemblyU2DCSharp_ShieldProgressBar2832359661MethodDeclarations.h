﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldProgressBar
struct ShieldProgressBar_t2832359661;
// Shield
struct Shield_t3327121081;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"

// System.Void ShieldProgressBar::.ctor()
extern "C"  void ShieldProgressBar__ctor_m1951765254 (ShieldProgressBar_t2832359661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldProgressBar::SetShieldLabels(Shield)
extern "C"  void ShieldProgressBar_SetShieldLabels_m1875177559 (ShieldProgressBar_t2832359661 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldProgressBar::SetBarActive(System.Boolean,System.Boolean)
extern "C"  void ShieldProgressBar_SetBarActive_m3176832503 (ShieldProgressBar_t2832359661 * __this, bool ___active0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldProgressBar::SetShieldColour(Shield)
extern "C"  void ShieldProgressBar_SetShieldColour_m1120049984 (ShieldProgressBar_t2832359661 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
