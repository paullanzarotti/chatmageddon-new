﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrivacyPolicyNavScreen
struct PrivacyPolicyNavScreen_t2706854281;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void PrivacyPolicyNavScreen::.ctor()
extern "C"  void PrivacyPolicyNavScreen__ctor_m259959114 (PrivacyPolicyNavScreen_t2706854281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivacyPolicyNavScreen::Start()
extern "C"  void PrivacyPolicyNavScreen_Start_m1920187050 (PrivacyPolicyNavScreen_t2706854281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivacyPolicyNavScreen::UIClosing()
extern "C"  void PrivacyPolicyNavScreen_UIClosing_m1384657763 (PrivacyPolicyNavScreen_t2706854281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivacyPolicyNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void PrivacyPolicyNavScreen_ScreenLoad_m641035462 (PrivacyPolicyNavScreen_t2706854281 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivacyPolicyNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void PrivacyPolicyNavScreen_ScreenUnload_m326795062 (PrivacyPolicyNavScreen_t2706854281 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PrivacyPolicyNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool PrivacyPolicyNavScreen_ValidateScreenNavigation_m3628504179 (PrivacyPolicyNavScreen_t2706854281 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
