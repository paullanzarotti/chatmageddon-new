﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje4244038468MethodDeclarations.h"

// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::.ctor()
#define ObservableDictionary_2__ctor_m3806233651(__this, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2__ctor_m3482229747_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define ObservableDictionary_2__ctor_m158527829(__this, ___dictionary0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m2273999893_gshared)(__this, ___dictionary0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define ObservableDictionary_2__ctor_m1557709816(__this, ___comparer0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m3424814776_gshared)(__this, ___comparer0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::.ctor(System.Int32)
#define ObservableDictionary_2__ctor_m2699214116(__this, ___capacity0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, int32_t, const MethodInfo*))ObservableDictionary_2__ctor_m4217517412_gshared)(__this, ___capacity0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define ObservableDictionary_2__ctor_m3163480996(__this, ___dictionary0, ___comparer1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m1850798116_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define ObservableDictionary_2__ctor_m3136684741(__this, ___capacity0, ___comparer1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, int32_t, Il2CppObject*, const MethodInfo*))ObservableDictionary_2__ctor_m2924843205_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Collections.Generic.IDictionary`2<TKey,TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::get_Dictionary()
#define ObservableDictionary_2_get_Dictionary_m2681050223(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_get_Dictionary_m267445679_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Add(TKey,TValue)
#define ObservableDictionary_2_Add_m3871506192(__this, ___key0, ___value1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, String_t*, String_t*, const MethodInfo*))ObservableDictionary_2_Add_m809576144_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::ContainsKey(TKey)
#define ObservableDictionary_2_ContainsKey_m2276545306(__this, ___key0, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, String_t*, const MethodInfo*))ObservableDictionary_2_ContainsKey_m3273400666_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::get_Keys()
#define ObservableDictionary_2_get_Keys_m2226258797(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_get_Keys_m4034744365_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Remove(TKey)
#define ObservableDictionary_2_Remove_m248707542(__this, ___key0, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, String_t*, const MethodInfo*))ObservableDictionary_2_Remove_m1597064470_gshared)(__this, ___key0, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::TryGetValue(TKey,TValue&)
#define ObservableDictionary_2_TryGetValue_m361371951(__this, ___key0, ___value1, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, String_t*, String_t**, const MethodInfo*))ObservableDictionary_2_TryGetValue_m2959695535_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::get_Values()
#define ObservableDictionary_2_get_Values_m939320173(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_get_Values_m2365880877_gshared)(__this, method)
// TValue PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::get_Item(TKey)
#define ObservableDictionary_2_get_Item_m2832872370(__this, ___key0, method) ((  String_t* (*) (ObservableDictionary_2_t1611561244 *, String_t*, const MethodInfo*))ObservableDictionary_2_get_Item_m1340976434_gshared)(__this, ___key0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::set_Item(TKey,TValue)
#define ObservableDictionary_2_set_Item_m2788790387(__this, ___key0, ___value1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, String_t*, String_t*, const MethodInfo*))ObservableDictionary_2_set_Item_m1965010995_gshared)(__this, ___key0, ___value1, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ObservableDictionary_2_Add_m1355685667(__this, ___item0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, KeyValuePair_2_t1701344717 , const MethodInfo*))ObservableDictionary_2_Add_m219092131_gshared)(__this, ___item0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Clear()
#define ObservableDictionary_2_Clear_m2091423632(__this, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_Clear_m2078003600_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ObservableDictionary_2_Contains_m43739729(__this, ___item0, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, KeyValuePair_2_t1701344717 , const MethodInfo*))ObservableDictionary_2_Contains_m156958993_gshared)(__this, ___item0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ObservableDictionary_2_CopyTo_m3539068159(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, KeyValuePair_2U5BU5D_t1360691296*, int32_t, const MethodInfo*))ObservableDictionary_2_CopyTo_m3898517055_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::get_Count()
#define ObservableDictionary_2_get_Count_m3128719523(__this, method) ((  int32_t (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_get_Count_m3837408227_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::get_IsReadOnly()
#define ObservableDictionary_2_get_IsReadOnly_m625810924(__this, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_get_IsReadOnly_m2217790444_gshared)(__this, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ObservableDictionary_2_Remove_m3654582546(__this, ___item0, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, KeyValuePair_2_t1701344717 , const MethodInfo*))ObservableDictionary_2_Remove_m117162194_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::GetEnumerator()
#define ObservableDictionary_2_GetEnumerator_m2463775206(__this, method) ((  Il2CppObject* (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_GetEnumerator_m2468868070_gshared)(__this, method)
// System.Collections.IEnumerator PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m247772480(__this, method) ((  Il2CppObject * (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2329354304_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::add_CollectionChanged(PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler)
#define ObservableDictionary_2_add_CollectionChanged_m3879893983(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, NotifyCollectionChangedEventHandler_t1310059589 *, const MethodInfo*))ObservableDictionary_2_add_CollectionChanged_m3033885855_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::remove_CollectionChanged(PlatformSupport.Collections.ObjectModel.NotifyCollectionChangedEventHandler)
#define ObservableDictionary_2_remove_CollectionChanged_m634122574(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, NotifyCollectionChangedEventHandler_t1310059589 *, const MethodInfo*))ObservableDictionary_2_remove_CollectionChanged_m2705002958_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
#define ObservableDictionary_2_add_PropertyChanged_m1873430840(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, PropertyChangedEventHandler_t3042952059 *, const MethodInfo*))ObservableDictionary_2_add_PropertyChanged_m1468847672_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
#define ObservableDictionary_2_remove_PropertyChanged_m1029959511(__this, ___value0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, PropertyChangedEventHandler_t3042952059 *, const MethodInfo*))ObservableDictionary_2_remove_PropertyChanged_m26290967_gshared)(__this, ___value0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::AddRange(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define ObservableDictionary_2_AddRange_m1016827879(__this, ___items0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, Il2CppObject*, const MethodInfo*))ObservableDictionary_2_AddRange_m3987702119_gshared)(__this, ___items0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::Insert(TKey,TValue,System.Boolean)
#define ObservableDictionary_2_Insert_m3478743967(__this, ___key0, ___value1, ___add2, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, String_t*, String_t*, bool, const MethodInfo*))ObservableDictionary_2_Insert_m2164254687_gshared)(__this, ___key0, ___value1, ___add2, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::OnPropertyChanged()
#define ObservableDictionary_2_OnPropertyChanged_m2466466627(__this, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_OnPropertyChanged_m1628940995_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::OnPropertyChanged(System.String)
#define ObservableDictionary_2_OnPropertyChanged_m352571737(__this, ___propertyName0, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, String_t*, const MethodInfo*))ObservableDictionary_2_OnPropertyChanged_m643605273_gshared)(__this, ___propertyName0, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::OnCollectionChanged()
#define ObservableDictionary_2_OnCollectionChanged_m790863302(__this, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m1062698694_gshared)(__this, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ObservableDictionary_2_OnCollectionChanged_m3153673210(__this, ___action0, ___changedItem1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, int32_t, KeyValuePair_2_t1701344717 , const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m78342394_gshared)(__this, ___action0, ___changedItem1, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.Generic.KeyValuePair`2<TKey,TValue>,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ObservableDictionary_2_OnCollectionChanged_m900614943(__this, ___action0, ___newItem1, ___oldItem2, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, int32_t, KeyValuePair_2_t1701344717 , KeyValuePair_2_t1701344717 , const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m20718175_gshared)(__this, ___action0, ___newItem1, ___oldItem2, method)
// System.Void PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::OnCollectionChanged(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList)
#define ObservableDictionary_2_OnCollectionChanged_m4185398402(__this, ___action0, ___newItems1, method) ((  void (*) (ObservableDictionary_2_t1611561244 *, int32_t, Il2CppObject *, const MethodInfo*))ObservableDictionary_2_OnCollectionChanged_m908791682_gshared)(__this, ___action0, ___newItems1, method)
// System.Boolean PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>::<AddRange>m__0(TKey)
#define ObservableDictionary_2_U3CAddRangeU3Em__0_m1350530837(__this, ___k0, method) ((  bool (*) (ObservableDictionary_2_t1611561244 *, String_t*, const MethodInfo*))ObservableDictionary_2_U3CAddRangeU3Em__0_m3926150165_gshared)(__this, ___k0, method)
