﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LPNController
struct  LPNController_t1966948550  : public MonoBehaviour_t1158329972
{
public:
	// PhoneNumbers.PhoneNumberUtil LPNController::util
	PhoneNumberUtil_t4155573397 * ___util_2;

public:
	inline static int32_t get_offset_of_util_2() { return static_cast<int32_t>(offsetof(LPNController_t1966948550, ___util_2)); }
	inline PhoneNumberUtil_t4155573397 * get_util_2() const { return ___util_2; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_util_2() { return &___util_2; }
	inline void set_util_2(PhoneNumberUtil_t4155573397 * value)
	{
		___util_2 = value;
		Il2CppCodeGenWriteBarrier(&___util_2, value);
	}
};

struct LPNController_t1966948550_StaticFields
{
public:
	// System.String LPNController::TEST_META_DATA_FILE_PREFIX
	String_t* ___TEST_META_DATA_FILE_PREFIX_3;

public:
	inline static int32_t get_offset_of_TEST_META_DATA_FILE_PREFIX_3() { return static_cast<int32_t>(offsetof(LPNController_t1966948550_StaticFields, ___TEST_META_DATA_FILE_PREFIX_3)); }
	inline String_t* get_TEST_META_DATA_FILE_PREFIX_3() const { return ___TEST_META_DATA_FILE_PREFIX_3; }
	inline String_t** get_address_of_TEST_META_DATA_FILE_PREFIX_3() { return &___TEST_META_DATA_FILE_PREFIX_3; }
	inline void set_TEST_META_DATA_FILE_PREFIX_3(String_t* value)
	{
		___TEST_META_DATA_FILE_PREFIX_3 = value;
		Il2CppCodeGenWriteBarrier(&___TEST_META_DATA_FILE_PREFIX_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
