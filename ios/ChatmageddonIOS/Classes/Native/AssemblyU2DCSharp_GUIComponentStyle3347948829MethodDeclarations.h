﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUIComponentStyle
struct GUIComponentStyle_t3347948829;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GUIComponentStyle::.ctor()
extern "C"  void GUIComponentStyle__ctor_m3332972762 (GUIComponentStyle_t3347948829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIComponentStyle::Awake()
extern "C"  void GUIComponentStyle_Awake_m1626553625 (GUIComponentStyle_t3347948829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIComponentStyle::SetSpriteColour(System.String)
extern "C"  void GUIComponentStyle_SetSpriteColour_m3426270061 (GUIComponentStyle_t3347948829 * __this, String_t* ___style0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIComponentStyle::SetTextureColour(System.String)
extern "C"  void GUIComponentStyle_SetTextureColour_m618447619 (GUIComponentStyle_t3347948829 * __this, String_t* ___style0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIComponentStyle::SetTextColour(System.String)
extern "C"  void GUIComponentStyle_SetTextColour_m1978109069 (GUIComponentStyle_t3347948829 * __this, String_t* ___style0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
