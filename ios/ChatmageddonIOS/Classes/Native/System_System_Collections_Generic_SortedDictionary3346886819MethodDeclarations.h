﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>
struct SortedDictionary_2_t3346886819;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t26340570;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3023952753;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1225111275;
// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t3561032432;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t598529833;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary_420481023.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m674714269_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2__ctor_m674714269(__this, method) ((  void (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2__ctor_m674714269_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedDictionary_2__ctor_m4130201156_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedDictionary_2__ctor_m4130201156(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t3346886819 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m4130201156_gshared)(__this, ___comparer0, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1040024625_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1040024625(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1040024625_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3905150097_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3905150097(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3905150097_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m166699524_gshared (SortedDictionary_2_t3346886819 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m166699524(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t3346886819 *, KeyValuePair_2_t3749587448 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m166699524_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1516177424_gshared (SortedDictionary_2_t3346886819 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1516177424(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, KeyValuePair_2_t3749587448 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1516177424_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1747061843_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1747061843(__this, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1747061843_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2218591521_gshared (SortedDictionary_2_t3346886819 * __this, KeyValuePair_2_t3749587448  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2218591521(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, KeyValuePair_2_t3749587448 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2218591521_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Add_m1213244071_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1213244071(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1213244071_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_Contains_m1437439451_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m1437439451(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m1437439451_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m518365436_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m518365436(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m518365436_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1965119572_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1965119572(__this, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1965119572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1699106999_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1699106999(__this, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1699106999_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2972533211_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2972533211(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2972533211_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Remove_m283317966_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m283317966(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m283317966_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Values_m3009893083_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m3009893083(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m3009893083_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Item_m3224431377_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m3224431377(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m3224431377_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_set_Item_m2135080018_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m2135080018(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2135080018_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedDictionary_2_System_Collections_ICollection_CopyTo_m2962606917_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m2962606917(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t3346886819 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m2962606917_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1251533845_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1251533845(__this, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1251533845_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m289765249_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m289765249(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m289765249_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m356205410_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m356205410(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m356205410_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1313061119_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1313061119(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1313061119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m3476867889_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Count_m3476867889(__this, method) ((  int32_t (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_get_Count_m3476867889_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SortedDictionary_2_get_Item_m3013028876_gshared (SortedDictionary_2_t3346886819 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_get_Item_m3013028876(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, int32_t, const MethodInfo*))SortedDictionary_2_get_Item_m3013028876_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C"  void SortedDictionary_2_set_Item_m3432483157_gshared (SortedDictionary_2_t3346886819 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_set_Item_m3432483157(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3346886819 *, int32_t, Il2CppObject *, const MethodInfo*))SortedDictionary_2_set_Item_m3432483157_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::get_Keys()
extern "C"  KeyCollection_t3561032432 * SortedDictionary_2_get_Keys_m2311581057_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Keys_m2311581057(__this, method) ((  KeyCollection_t3561032432 * (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_get_Keys_m2311581057_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C"  void SortedDictionary_2_Add_m3384498232_gshared (SortedDictionary_2_t3346886819 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_Add_m3384498232(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3346886819 *, int32_t, Il2CppObject *, const MethodInfo*))SortedDictionary_2_Add_m3384498232_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::Clear()
extern "C"  void SortedDictionary_2_Clear_m2537365212_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_Clear_m2537365212(__this, method) ((  void (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_Clear_m2537365212_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C"  bool SortedDictionary_2_ContainsKey_m2099208916_gshared (SortedDictionary_2_t3346886819 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_ContainsKey_m2099208916(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsKey_m2099208916_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C"  bool SortedDictionary_2_ContainsValue_m2660776548_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ContainsValue_m2660776548(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ContainsValue_m2660776548_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedDictionary_2_CopyTo_m1830145497_gshared (SortedDictionary_2_t3346886819 * __this, KeyValuePair_2U5BU5D_t598529833* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedDictionary_2_CopyTo_m1830145497(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t3346886819 *, KeyValuePair_2U5BU5D_t598529833*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m1830145497_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t420481023  SortedDictionary_2_GetEnumerator_m369699555_gshared (SortedDictionary_2_t3346886819 * __this, const MethodInfo* method);
#define SortedDictionary_2_GetEnumerator_m369699555(__this, method) ((  Enumerator_t420481023  (*) (SortedDictionary_2_t3346886819 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m369699555_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C"  bool SortedDictionary_2_Remove_m2306315880_gshared (SortedDictionary_2_t3346886819 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_Remove_m2306315880(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, int32_t, const MethodInfo*))SortedDictionary_2_Remove_m2306315880_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedDictionary_2_TryGetValue_m2911573889_gshared (SortedDictionary_2_t3346886819 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SortedDictionary_2_TryGetValue_m2911573889(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t3346886819 *, int32_t, Il2CppObject **, const MethodInfo*))SortedDictionary_2_TryGetValue_m2911573889_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::ToKey(System.Object)
extern "C"  int32_t SortedDictionary_2_ToKey_m4262124275_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ToKey_m4262124275(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m4262124275_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>::ToValue(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_ToValue_m817036307_gshared (SortedDictionary_2_t3346886819 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ToValue_m817036307(__this, ___value0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3346886819 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m817036307_gshared)(__this, ___value0, method)
