﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22369073723.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3942196043_gshared (KeyValuePair_2_t2369073723 * __this, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3942196043(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2369073723 *, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))KeyValuePair_2__ctor_m3942196043_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2346079417_gshared (KeyValuePair_2_t2369073723 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2346079417(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2369073723 *, const MethodInfo*))KeyValuePair_2_get_Key_m2346079417_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3953321286_gshared (KeyValuePair_2_t2369073723 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3953321286(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2369073723 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3953321286_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>::get_Value()
extern "C"  Decimal_t724701077  KeyValuePair_2_get_Value_m3796378361_gshared (KeyValuePair_2_t2369073723 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3796378361(__this, method) ((  Decimal_t724701077  (*) (KeyValuePair_2_t2369073723 *, const MethodInfo*))KeyValuePair_2_get_Value_m3796378361_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2333318902_gshared (KeyValuePair_2_t2369073723 * __this, Decimal_t724701077  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2333318902(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2369073723 *, Decimal_t724701077 , const MethodInfo*))KeyValuePair_2_set_Value_m2333318902_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m778560236_gshared (KeyValuePair_2_t2369073723 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m778560236(__this, method) ((  String_t* (*) (KeyValuePair_2_t2369073723 *, const MethodInfo*))KeyValuePair_2_ToString_m778560236_gshared)(__this, method)
