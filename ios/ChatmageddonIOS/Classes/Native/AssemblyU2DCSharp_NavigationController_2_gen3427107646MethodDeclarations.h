﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen243945468MethodDeclarations.h"

// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::.ctor()
#define NavigationController_2__ctor_m3207846558(__this, method) ((  void (*) (NavigationController_2_t3427107646 *, const MethodInfo*))NavigationController_2__ctor_m2424262531_gshared)(__this, method)
// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m457691736(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3427107646 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m3784836201_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m1900626133(__this, method) ((  void (*) (NavigationController_2_t3427107646 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3704736956_gshared)(__this, method)
// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m521939502(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3427107646 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3528757533_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m631648010(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3427107646 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m1438642517_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m4164596751(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3427107646 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1383451070_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m3221804996(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3427107646 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m4192486605_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m1920558978(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3427107646 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m3110878433_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<LeaderboardNavigationController,LeaderboardNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m3215672410(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3427107646 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1727917841_gshared)(__this, ___screen0, method)
