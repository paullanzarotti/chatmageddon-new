﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_FX_Glitch1
struct  CameraFilterPack_FX_Glitch1_t63583076  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_FX_Glitch1::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Glitch1::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Glitch1::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Glitch1::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_Glitch1::Value
	float ___Value_6;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_Glitch1_t63583076, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_Glitch1_t63583076, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_Glitch1_t63583076, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_Glitch1_t63583076, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Value_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_Glitch1_t63583076, ___Value_6)); }
	inline float get_Value_6() const { return ___Value_6; }
	inline float* get_address_of_Value_6() { return &___Value_6; }
	inline void set_Value_6(float value)
	{
		___Value_6 = value;
	}
};

struct CameraFilterPack_FX_Glitch1_t63583076_StaticFields
{
public:
	// System.Single CameraFilterPack_FX_Glitch1::ChangeValue
	float ___ChangeValue_7;

public:
	inline static int32_t get_offset_of_ChangeValue_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_Glitch1_t63583076_StaticFields, ___ChangeValue_7)); }
	inline float get_ChangeValue_7() const { return ___ChangeValue_7; }
	inline float* get_address_of_ChangeValue_7() { return &___ChangeValue_7; }
	inline void set_ChangeValue_7(float value)
	{
		___ChangeValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
