﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "P31RestKit_Prime31_AbstractManager1005944057.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlurryAndroidManager
struct  FlurryAndroidManager_t1343776350  : public AbstractManager_t1005944057
{
public:

public:
};

struct FlurryAndroidManager_t1343776350_StaticFields
{
public:
	// System.Action`1<System.String> FlurryAndroidManager::adAvailableForSpaceEvent
	Action_1_t1831019615 * ___adAvailableForSpaceEvent_5;
	// System.Action`1<System.String> FlurryAndroidManager::adNotAvailableForSpaceEvent
	Action_1_t1831019615 * ___adNotAvailableForSpaceEvent_6;
	// System.Action`1<System.String> FlurryAndroidManager::onAdClosedEvent
	Action_1_t1831019615 * ___onAdClosedEvent_7;
	// System.Action`1<System.String> FlurryAndroidManager::onApplicationExitEvent
	Action_1_t1831019615 * ___onApplicationExitEvent_8;
	// System.Action`1<System.String> FlurryAndroidManager::onRenderFailedEvent
	Action_1_t1831019615 * ___onRenderFailedEvent_9;
	// System.Action`1<System.String> FlurryAndroidManager::spaceDidFailToReceiveAdEvent
	Action_1_t1831019615 * ___spaceDidFailToReceiveAdEvent_10;
	// System.Action`1<System.String> FlurryAndroidManager::spaceDidReceiveAdEvent
	Action_1_t1831019615 * ___spaceDidReceiveAdEvent_11;
	// System.Action`1<System.String> FlurryAndroidManager::onAdClickedEvent
	Action_1_t1831019615 * ___onAdClickedEvent_12;
	// System.Action`1<System.String> FlurryAndroidManager::onAdOpenedEvent
	Action_1_t1831019615 * ___onAdOpenedEvent_13;
	// System.Action`1<System.String> FlurryAndroidManager::onVideoCompletedEvent
	Action_1_t1831019615 * ___onVideoCompletedEvent_14;

public:
	inline static int32_t get_offset_of_adAvailableForSpaceEvent_5() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___adAvailableForSpaceEvent_5)); }
	inline Action_1_t1831019615 * get_adAvailableForSpaceEvent_5() const { return ___adAvailableForSpaceEvent_5; }
	inline Action_1_t1831019615 ** get_address_of_adAvailableForSpaceEvent_5() { return &___adAvailableForSpaceEvent_5; }
	inline void set_adAvailableForSpaceEvent_5(Action_1_t1831019615 * value)
	{
		___adAvailableForSpaceEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___adAvailableForSpaceEvent_5, value);
	}

	inline static int32_t get_offset_of_adNotAvailableForSpaceEvent_6() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___adNotAvailableForSpaceEvent_6)); }
	inline Action_1_t1831019615 * get_adNotAvailableForSpaceEvent_6() const { return ___adNotAvailableForSpaceEvent_6; }
	inline Action_1_t1831019615 ** get_address_of_adNotAvailableForSpaceEvent_6() { return &___adNotAvailableForSpaceEvent_6; }
	inline void set_adNotAvailableForSpaceEvent_6(Action_1_t1831019615 * value)
	{
		___adNotAvailableForSpaceEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___adNotAvailableForSpaceEvent_6, value);
	}

	inline static int32_t get_offset_of_onAdClosedEvent_7() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___onAdClosedEvent_7)); }
	inline Action_1_t1831019615 * get_onAdClosedEvent_7() const { return ___onAdClosedEvent_7; }
	inline Action_1_t1831019615 ** get_address_of_onAdClosedEvent_7() { return &___onAdClosedEvent_7; }
	inline void set_onAdClosedEvent_7(Action_1_t1831019615 * value)
	{
		___onAdClosedEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___onAdClosedEvent_7, value);
	}

	inline static int32_t get_offset_of_onApplicationExitEvent_8() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___onApplicationExitEvent_8)); }
	inline Action_1_t1831019615 * get_onApplicationExitEvent_8() const { return ___onApplicationExitEvent_8; }
	inline Action_1_t1831019615 ** get_address_of_onApplicationExitEvent_8() { return &___onApplicationExitEvent_8; }
	inline void set_onApplicationExitEvent_8(Action_1_t1831019615 * value)
	{
		___onApplicationExitEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationExitEvent_8, value);
	}

	inline static int32_t get_offset_of_onRenderFailedEvent_9() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___onRenderFailedEvent_9)); }
	inline Action_1_t1831019615 * get_onRenderFailedEvent_9() const { return ___onRenderFailedEvent_9; }
	inline Action_1_t1831019615 ** get_address_of_onRenderFailedEvent_9() { return &___onRenderFailedEvent_9; }
	inline void set_onRenderFailedEvent_9(Action_1_t1831019615 * value)
	{
		___onRenderFailedEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___onRenderFailedEvent_9, value);
	}

	inline static int32_t get_offset_of_spaceDidFailToReceiveAdEvent_10() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___spaceDidFailToReceiveAdEvent_10)); }
	inline Action_1_t1831019615 * get_spaceDidFailToReceiveAdEvent_10() const { return ___spaceDidFailToReceiveAdEvent_10; }
	inline Action_1_t1831019615 ** get_address_of_spaceDidFailToReceiveAdEvent_10() { return &___spaceDidFailToReceiveAdEvent_10; }
	inline void set_spaceDidFailToReceiveAdEvent_10(Action_1_t1831019615 * value)
	{
		___spaceDidFailToReceiveAdEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___spaceDidFailToReceiveAdEvent_10, value);
	}

	inline static int32_t get_offset_of_spaceDidReceiveAdEvent_11() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___spaceDidReceiveAdEvent_11)); }
	inline Action_1_t1831019615 * get_spaceDidReceiveAdEvent_11() const { return ___spaceDidReceiveAdEvent_11; }
	inline Action_1_t1831019615 ** get_address_of_spaceDidReceiveAdEvent_11() { return &___spaceDidReceiveAdEvent_11; }
	inline void set_spaceDidReceiveAdEvent_11(Action_1_t1831019615 * value)
	{
		___spaceDidReceiveAdEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___spaceDidReceiveAdEvent_11, value);
	}

	inline static int32_t get_offset_of_onAdClickedEvent_12() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___onAdClickedEvent_12)); }
	inline Action_1_t1831019615 * get_onAdClickedEvent_12() const { return ___onAdClickedEvent_12; }
	inline Action_1_t1831019615 ** get_address_of_onAdClickedEvent_12() { return &___onAdClickedEvent_12; }
	inline void set_onAdClickedEvent_12(Action_1_t1831019615 * value)
	{
		___onAdClickedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___onAdClickedEvent_12, value);
	}

	inline static int32_t get_offset_of_onAdOpenedEvent_13() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___onAdOpenedEvent_13)); }
	inline Action_1_t1831019615 * get_onAdOpenedEvent_13() const { return ___onAdOpenedEvent_13; }
	inline Action_1_t1831019615 ** get_address_of_onAdOpenedEvent_13() { return &___onAdOpenedEvent_13; }
	inline void set_onAdOpenedEvent_13(Action_1_t1831019615 * value)
	{
		___onAdOpenedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___onAdOpenedEvent_13, value);
	}

	inline static int32_t get_offset_of_onVideoCompletedEvent_14() { return static_cast<int32_t>(offsetof(FlurryAndroidManager_t1343776350_StaticFields, ___onVideoCompletedEvent_14)); }
	inline Action_1_t1831019615 * get_onVideoCompletedEvent_14() const { return ___onVideoCompletedEvent_14; }
	inline Action_1_t1831019615 ** get_address_of_onVideoCompletedEvent_14() { return &___onVideoCompletedEvent_14; }
	inline void set_onVideoCompletedEvent_14(Action_1_t1831019615 * value)
	{
		___onVideoCompletedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___onVideoCompletedEvent_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
