﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickableObject
struct ClickableObject_t3964308383;

#include "codegen/il2cpp-codegen.h"

// System.Void ClickableObject::.ctor()
extern "C"  void ClickableObject__ctor_m3506550678 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::OnMouseDown()
extern "C"  void ClickableObject_OnMouseDown_m1097033738 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::OnMouseUp()
extern "C"  void ClickableObject_OnMouseUp_m836681391 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::OnMouseOver()
extern "C"  void ClickableObject_OnMouseOver_m4131205896 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::OnMouseDrag()
extern "C"  void ClickableObject_OnMouseDrag_m2902711024 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::OnMouseEnter()
extern "C"  void ClickableObject_OnMouseEnter_m2597480498 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::LeftClickEffect()
extern "C"  void ClickableObject_LeftClickEffect_m963639840 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableObject::RightClickEffect()
extern "C"  void ClickableObject_RightClickEffect_m3045395927 (ClickableObject_t3964308383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
