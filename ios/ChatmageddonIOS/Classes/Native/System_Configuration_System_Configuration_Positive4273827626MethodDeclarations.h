﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.PositiveTimeSpanValidator
struct PositiveTimeSpanValidator_t4273827626;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.PositiveTimeSpanValidator::.ctor()
extern "C"  void PositiveTimeSpanValidator__ctor_m1009485089 (PositiveTimeSpanValidator_t4273827626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.PositiveTimeSpanValidator::CanValidate(System.Type)
extern "C"  bool PositiveTimeSpanValidator_CanValidate_m3187793868 (PositiveTimeSpanValidator_t4273827626 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.PositiveTimeSpanValidator::Validate(System.Object)
extern "C"  void PositiveTimeSpanValidator_Validate_m944790293 (PositiveTimeSpanValidator_t4273827626 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
