﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/LatLon
struct  LatLon_t3749872052 
{
public:
	// System.Double MapManager/LatLon::Latitude
	double ___Latitude_0;
	// System.Double MapManager/LatLon::Longtitude
	double ___Longtitude_1;

public:
	inline static int32_t get_offset_of_Latitude_0() { return static_cast<int32_t>(offsetof(LatLon_t3749872052, ___Latitude_0)); }
	inline double get_Latitude_0() const { return ___Latitude_0; }
	inline double* get_address_of_Latitude_0() { return &___Latitude_0; }
	inline void set_Latitude_0(double value)
	{
		___Latitude_0 = value;
	}

	inline static int32_t get_offset_of_Longtitude_1() { return static_cast<int32_t>(offsetof(LatLon_t3749872052, ___Longtitude_1)); }
	inline double get_Longtitude_1() const { return ___Longtitude_1; }
	inline double* get_address_of_Longtitude_1() { return &___Longtitude_1; }
	inline void set_Longtitude_1(double value)
	{
		___Longtitude_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
