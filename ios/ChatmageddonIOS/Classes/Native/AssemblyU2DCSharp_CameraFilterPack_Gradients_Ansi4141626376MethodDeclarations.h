﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Ansi
struct CameraFilterPack_Gradients_Ansi_t4141626376;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Ansi::.ctor()
extern "C"  void CameraFilterPack_Gradients_Ansi__ctor_m2507806295 (CameraFilterPack_Gradients_Ansi_t4141626376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Ansi::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Ansi_get_material_m1938476680 (CameraFilterPack_Gradients_Ansi_t4141626376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::Start()
extern "C"  void CameraFilterPack_Gradients_Ansi_Start_m100592483 (CameraFilterPack_Gradients_Ansi_t4141626376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Ansi_OnRenderImage_m1769154187 (CameraFilterPack_Gradients_Ansi_t4141626376 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::Update()
extern "C"  void CameraFilterPack_Gradients_Ansi_Update_m3886879178 (CameraFilterPack_Gradients_Ansi_t4141626376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Ansi_OnDisable_m1883627844 (CameraFilterPack_Gradients_Ansi_t4141626376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
