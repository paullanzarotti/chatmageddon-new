﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherFriend
struct OtherFriend_t3976068630;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void OtherFriend::.ctor(System.Boolean,System.String,System.String,System.Collections.Generic.List`1<System.String>)
extern "C"  void OtherFriend__ctor_m3366904810 (OtherFriend_t3976068630 * __this, bool ___seperator0, String_t* ___firstName1, String_t* ___lastName2, List_1_t1398341365 * ___phoneNumbers3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
