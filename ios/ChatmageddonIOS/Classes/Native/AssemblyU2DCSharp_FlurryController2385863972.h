﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BestHTTP.OnRequestFinishedDelegate
struct OnRequestFinishedDelegate_t3180754735;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2136529692.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlurryController
struct  FlurryController_t2385863972  : public MonoSingleton_1_t2136529692
{
public:

public:
};

struct FlurryController_t2385863972_StaticFields
{
public:
	// BestHTTP.OnRequestFinishedDelegate FlurryController::<>f__am$cache0
	OnRequestFinishedDelegate_t3180754735 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(FlurryController_t2385863972_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline OnRequestFinishedDelegate_t3180754735 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline OnRequestFinishedDelegate_t3180754735 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(OnRequestFinishedDelegate_t3180754735 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
