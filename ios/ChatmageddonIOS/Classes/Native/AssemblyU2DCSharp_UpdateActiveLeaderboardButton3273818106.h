﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateActiveLeaderboardButton
struct  UpdateActiveLeaderboardButton_t3273818106  : public SFXButton_t792651341
{
public:
	// LeaderboardType UpdateActiveLeaderboardButton::type
	int32_t ___type_5;

public:
	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(UpdateActiveLeaderboardButton_t3273818106, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
