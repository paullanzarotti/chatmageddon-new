﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiPhoneNumberListItem
struct MultiPhoneNumberListItem_t177127779;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void MultiPhoneNumberListItem::.ctor()
extern "C"  void MultiPhoneNumberListItem__ctor_m2235276644 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::Awake()
extern "C"  void MultiPhoneNumberListItem_Awake_m1607993599 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::SetUpItem()
extern "C"  void MultiPhoneNumberListItem_SetUpItem_m4117144282 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::PopulateItem(System.String,System.Boolean)
extern "C"  void MultiPhoneNumberListItem_PopulateItem_m335371298 (MultiPhoneNumberListItem_t177127779 * __this, String_t* ___number0, bool ___attack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::SetVisible()
extern "C"  void MultiPhoneNumberListItem_SetVisible_m3749167940 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::SetInvisible()
extern "C"  void MultiPhoneNumberListItem_SetInvisible_m3289418881 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MultiPhoneNumberListItem::verifyVisibility()
extern "C"  bool MultiPhoneNumberListItem_verifyVisibility_m1216914963 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::CheckVisibilty()
extern "C"  void MultiPhoneNumberListItem_CheckVisibilty_m610263255 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::Update()
extern "C"  void MultiPhoneNumberListItem_Update_m1331124515 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::OnEnable()
extern "C"  void MultiPhoneNumberListItem_OnEnable_m1415106896 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::FindScrollView()
extern "C"  void MultiPhoneNumberListItem_FindScrollView_m2225234235 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::OnPress(System.Boolean)
extern "C"  void MultiPhoneNumberListItem_OnPress_m3180713203 (MultiPhoneNumberListItem_t177127779 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void MultiPhoneNumberListItem_OnDrag_m143207811 (MultiPhoneNumberListItem_t177127779 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::OnDragEnd()
extern "C"  void MultiPhoneNumberListItem_OnDragEnd_m1644939352 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::OnScroll(System.Single)
extern "C"  void MultiPhoneNumberListItem_OnScroll_m2171902323 (MultiPhoneNumberListItem_t177127779 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::OnClick()
extern "C"  void MultiPhoneNumberListItem_OnClick_m2922185849 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MultiPhoneNumberListItem::CheckSMSAvailability()
extern "C"  bool MultiPhoneNumberListItem_CheckSMSAvailability_m3671238726 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::SendSMS(System.String,System.String)
extern "C"  void MultiPhoneNumberListItem_SendSMS_m22514503 (MultiPhoneNumberListItem_t177127779 * __this, String_t* ___contactPhoneNumber0, String_t* ___messageBody1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::smsComposerFinishedWithResult(System.String)
extern "C"  void MultiPhoneNumberListItem_smsComposerFinishedWithResult_m1117705546 (MultiPhoneNumberListItem_t177127779 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::SMSSent()
extern "C"  void MultiPhoneNumberListItem_SMSSent_m2873353951 (MultiPhoneNumberListItem_t177127779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiPhoneNumberListItem::<OnClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void MultiPhoneNumberListItem_U3COnClickU3Em__0_m1449905571 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
