﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsSearchNavScreen
struct FriendsSearchNavScreen_t2437737290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsSearchNavScreen::.ctor()
extern "C"  void FriendsSearchNavScreen__ctor_m3423029949 (FriendsSearchNavScreen_t2437737290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavScreen::Start()
extern "C"  void FriendsSearchNavScreen_Start_m1978358069 (FriendsSearchNavScreen_t2437737290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavScreen::UIClosing()
extern "C"  void FriendsSearchNavScreen_UIClosing_m1788307796 (FriendsSearchNavScreen_t2437737290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void FriendsSearchNavScreen_ScreenLoad_m4146139287 (FriendsSearchNavScreen_t2437737290 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void FriendsSearchNavScreen_ScreenUnload_m4271003207 (FriendsSearchNavScreen_t2437737290 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsSearchNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool FriendsSearchNavScreen_ValidateScreenNavigation_m4068480478 (FriendsSearchNavScreen_t2437737290 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
