﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_ARCADE_2
struct CameraFilterPack_TV_ARCADE_2_t1599056049;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_ARCADE_2::.ctor()
extern "C"  void CameraFilterPack_TV_ARCADE_2__ctor_m712896420 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_ARCADE_2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_ARCADE_2_get_material_m3861017057 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::Start()
extern "C"  void CameraFilterPack_TV_ARCADE_2_Start_m2477036124 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_ARCADE_2_OnRenderImage_m1198829132 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::OnValidate()
extern "C"  void CameraFilterPack_TV_ARCADE_2_OnValidate_m506634775 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::Update()
extern "C"  void CameraFilterPack_TV_ARCADE_2_Update_m809605597 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::OnDisable()
extern "C"  void CameraFilterPack_TV_ARCADE_2_OnDisable_m1091957697 (CameraFilterPack_TV_ARCADE_2_t1599056049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
