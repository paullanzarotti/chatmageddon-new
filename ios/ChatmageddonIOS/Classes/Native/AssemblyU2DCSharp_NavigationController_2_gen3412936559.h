﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DefendNavScreen>
struct List_1_t3207740835;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen222835451.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NavigationController`2<DefendModalNavigationController,DefendNavScreen>
struct  NavigationController_2_t3412936559  : public MonoSingleton_1_t222835451
{
public:
	// System.Boolean NavigationController`2::titleBar
	bool ___titleBar_3;
	// System.Boolean NavigationController`2::tabNavigation
	bool ___tabNavigation_4;
	// System.Collections.Generic.List`1<NavScreenType> NavigationController`2::previousScreens
	List_1_t3207740835 * ___previousScreens_5;
	// NavScreenType NavigationController`2::currentScreen
	int32_t ___currentScreen_6;
	// System.Boolean NavigationController`2::overrideCurrentCheck
	bool ___overrideCurrentCheck_7;

public:
	inline static int32_t get_offset_of_titleBar_3() { return static_cast<int32_t>(offsetof(NavigationController_2_t3412936559, ___titleBar_3)); }
	inline bool get_titleBar_3() const { return ___titleBar_3; }
	inline bool* get_address_of_titleBar_3() { return &___titleBar_3; }
	inline void set_titleBar_3(bool value)
	{
		___titleBar_3 = value;
	}

	inline static int32_t get_offset_of_tabNavigation_4() { return static_cast<int32_t>(offsetof(NavigationController_2_t3412936559, ___tabNavigation_4)); }
	inline bool get_tabNavigation_4() const { return ___tabNavigation_4; }
	inline bool* get_address_of_tabNavigation_4() { return &___tabNavigation_4; }
	inline void set_tabNavigation_4(bool value)
	{
		___tabNavigation_4 = value;
	}

	inline static int32_t get_offset_of_previousScreens_5() { return static_cast<int32_t>(offsetof(NavigationController_2_t3412936559, ___previousScreens_5)); }
	inline List_1_t3207740835 * get_previousScreens_5() const { return ___previousScreens_5; }
	inline List_1_t3207740835 ** get_address_of_previousScreens_5() { return &___previousScreens_5; }
	inline void set_previousScreens_5(List_1_t3207740835 * value)
	{
		___previousScreens_5 = value;
		Il2CppCodeGenWriteBarrier(&___previousScreens_5, value);
	}

	inline static int32_t get_offset_of_currentScreen_6() { return static_cast<int32_t>(offsetof(NavigationController_2_t3412936559, ___currentScreen_6)); }
	inline int32_t get_currentScreen_6() const { return ___currentScreen_6; }
	inline int32_t* get_address_of_currentScreen_6() { return &___currentScreen_6; }
	inline void set_currentScreen_6(int32_t value)
	{
		___currentScreen_6 = value;
	}

	inline static int32_t get_offset_of_overrideCurrentCheck_7() { return static_cast<int32_t>(offsetof(NavigationController_2_t3412936559, ___overrideCurrentCheck_7)); }
	inline bool get_overrideCurrentCheck_7() const { return ___overrideCurrentCheck_7; }
	inline bool* get_address_of_overrideCurrentCheck_7() { return &___overrideCurrentCheck_7; }
	inline void set_overrideCurrentCheck_7(bool value)
	{
		___overrideCurrentCheck_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
