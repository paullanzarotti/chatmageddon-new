﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HoldBarController
struct HoldBarController_t3195732834;

#include "codegen/il2cpp-codegen.h"

// System.Void HoldBarController::.ctor()
extern "C"  void HoldBarController__ctor_m123475125 (HoldBarController_t3195732834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
