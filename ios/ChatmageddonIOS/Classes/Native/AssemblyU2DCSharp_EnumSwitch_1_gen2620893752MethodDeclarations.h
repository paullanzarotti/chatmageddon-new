﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnumSwitch`1<Gender>
struct EnumSwitch_1_t2620893752;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"

// System.Void EnumSwitch`1<Gender>::.ctor()
extern "C"  void EnumSwitch_1__ctor_m537285034_gshared (EnumSwitch_1_t2620893752 * __this, const MethodInfo* method);
#define EnumSwitch_1__ctor_m537285034(__this, method) ((  void (*) (EnumSwitch_1_t2620893752 *, const MethodInfo*))EnumSwitch_1__ctor_m537285034_gshared)(__this, method)
// System.Void EnumSwitch`1<Gender>::SelectSwitchState(SwitchState)
extern "C"  void EnumSwitch_1_SelectSwitchState_m2758952452_gshared (EnumSwitch_1_t2620893752 * __this, int32_t ___newState0, const MethodInfo* method);
#define EnumSwitch_1_SelectSwitchState_m2758952452(__this, ___newState0, method) ((  void (*) (EnumSwitch_1_t2620893752 *, int32_t, const MethodInfo*))EnumSwitch_1_SelectSwitchState_m2758952452_gshared)(__this, ___newState0, method)
// System.Void EnumSwitch`1<Gender>::OnSwitch(SwitchState)
extern "C"  void EnumSwitch_1_OnSwitch_m1393136478_gshared (EnumSwitch_1_t2620893752 * __this, int32_t ___newState0, const MethodInfo* method);
#define EnumSwitch_1_OnSwitch_m1393136478(__this, ___newState0, method) ((  void (*) (EnumSwitch_1_t2620893752 *, int32_t, const MethodInfo*))EnumSwitch_1_OnSwitch_m1393136478_gshared)(__this, ___newState0, method)
