﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendNavScreen>
struct DefaultComparer_t3797313614;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m773502103_gshared (DefaultComparer_t3797313614 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m773502103(__this, method) ((  void (*) (DefaultComparer_t3797313614 *, const MethodInfo*))DefaultComparer__ctor_m773502103_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2174728620_gshared (DefaultComparer_t3797313614 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2174728620(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3797313614 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2174728620_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<FriendNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1414698348_gshared (DefaultComparer_t3797313614 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1414698348(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3797313614 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1414698348_gshared)(__this, ___x0, ___y1, method)
