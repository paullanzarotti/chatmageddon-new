﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TermsOfServiceUI
struct  TermsOfServiceUI_t3223215579  : public MonoBehaviour_t1158329972
{
public:
	// UILabel TermsOfServiceUI::tosText
	UILabel_t1795115428 * ___tosText_2;
	// UnityEngine.BoxCollider TermsOfServiceUI::uiCollider
	BoxCollider_t22920061 * ___uiCollider_3;
	// System.Single TermsOfServiceUI::sectionSplit
	float ___sectionSplit_4;

public:
	inline static int32_t get_offset_of_tosText_2() { return static_cast<int32_t>(offsetof(TermsOfServiceUI_t3223215579, ___tosText_2)); }
	inline UILabel_t1795115428 * get_tosText_2() const { return ___tosText_2; }
	inline UILabel_t1795115428 ** get_address_of_tosText_2() { return &___tosText_2; }
	inline void set_tosText_2(UILabel_t1795115428 * value)
	{
		___tosText_2 = value;
		Il2CppCodeGenWriteBarrier(&___tosText_2, value);
	}

	inline static int32_t get_offset_of_uiCollider_3() { return static_cast<int32_t>(offsetof(TermsOfServiceUI_t3223215579, ___uiCollider_3)); }
	inline BoxCollider_t22920061 * get_uiCollider_3() const { return ___uiCollider_3; }
	inline BoxCollider_t22920061 ** get_address_of_uiCollider_3() { return &___uiCollider_3; }
	inline void set_uiCollider_3(BoxCollider_t22920061 * value)
	{
		___uiCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___uiCollider_3, value);
	}

	inline static int32_t get_offset_of_sectionSplit_4() { return static_cast<int32_t>(offsetof(TermsOfServiceUI_t3223215579, ___sectionSplit_4)); }
	inline float get_sectionSplit_4() const { return ___sectionSplit_4; }
	inline float* get_address_of_sectionSplit_4() { return &___sectionSplit_4; }
	inline void set_sectionSplit_4(float value)
	{
		___sectionSplit_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
