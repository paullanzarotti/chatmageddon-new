﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,AttackHomeNav>
struct NavigationController_2_t1832792260;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,AttackHomeNav>::.ctor()
extern "C"  void NavigationController_2__ctor_m890861153_gshared (NavigationController_2_t1832792260 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m890861153(__this, method) ((  void (*) (NavigationController_2_t1832792260 *, const MethodInfo*))NavigationController_2__ctor_m890861153_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,AttackHomeNav>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1452032531_gshared (NavigationController_2_t1832792260 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1452032531(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t1832792260 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1452032531_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,AttackHomeNav>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1579095628_gshared (NavigationController_2_t1832792260 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m1579095628(__this, method) ((  void (*) (NavigationController_2_t1832792260 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1579095628_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,AttackHomeNav>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m2230020555_gshared (NavigationController_2_t1832792260 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m2230020555(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t1832792260 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m2230020555_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,AttackHomeNav>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m4172558399_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m4172558399(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t1832792260 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m4172558399_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,AttackHomeNav>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1897244818_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m1897244818(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t1832792260 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1897244818_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,AttackHomeNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3898453107_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m3898453107(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t1832792260 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3898453107_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,AttackHomeNav>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m602541607_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m602541607(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t1832792260 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m602541607_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,AttackHomeNav>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2502305939_gshared (NavigationController_2_t1832792260 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m2502305939(__this, ___screen0, method) ((  void (*) (NavigationController_2_t1832792260 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2502305939_gshared)(__this, ___screen0, method)
