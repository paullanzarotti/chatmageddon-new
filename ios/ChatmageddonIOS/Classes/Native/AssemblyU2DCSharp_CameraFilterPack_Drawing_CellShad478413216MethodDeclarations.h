﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_CellShading
struct CameraFilterPack_Drawing_CellShading_t478413216;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_CellShading::.ctor()
extern "C"  void CameraFilterPack_Drawing_CellShading__ctor_m1003841427 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_CellShading::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_CellShading_get_material_m3452115042 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::Start()
extern "C"  void CameraFilterPack_Drawing_CellShading_Start_m2368614751 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_CellShading_OnRenderImage_m2270799655 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::OnValidate()
extern "C"  void CameraFilterPack_Drawing_CellShading_OnValidate_m1784213960 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::Update()
extern "C"  void CameraFilterPack_Drawing_CellShading_Update_m3996670978 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::OnDisable()
extern "C"  void CameraFilterPack_Drawing_CellShading_OnDisable_m361673788 (CameraFilterPack_Drawing_CellShading_t478413216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
