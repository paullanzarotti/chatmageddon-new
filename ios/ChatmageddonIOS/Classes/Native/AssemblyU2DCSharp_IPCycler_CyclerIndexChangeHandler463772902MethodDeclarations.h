﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPCycler/CyclerIndexChangeHandler
struct CyclerIndexChangeHandler_t463772902;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void IPCycler/CyclerIndexChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CyclerIndexChangeHandler__ctor_m3817846329 (CyclerIndexChangeHandler_t463772902 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler/CyclerIndexChangeHandler::Invoke(System.Boolean,System.Int32)
extern "C"  void CyclerIndexChangeHandler_Invoke_m1569172479 (CyclerIndexChangeHandler_t463772902 * __this, bool ___increase0, int32_t ___indexToUpdate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IPCycler/CyclerIndexChangeHandler::BeginInvoke(System.Boolean,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CyclerIndexChangeHandler_BeginInvoke_m752258318 (CyclerIndexChangeHandler_t463772902 * __this, bool ___increase0, int32_t ___indexToUpdate1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler/CyclerIndexChangeHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CyclerIndexChangeHandler_EndInvoke_m3298443559 (CyclerIndexChangeHandler_t463772902 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
