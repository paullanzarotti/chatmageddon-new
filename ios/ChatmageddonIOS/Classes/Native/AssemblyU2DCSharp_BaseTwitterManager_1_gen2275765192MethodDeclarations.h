﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseTwitterManager`1<System.Object>
struct BaseTwitterManager_1_t2275765192;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_BaseTwitterManager_1_RequestType_g63364694.h"
#include "AssemblyU2DCSharp_TwitterLoginEntrance1132015978.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void BaseTwitterManager`1<System.Object>::.ctor()
extern "C"  void BaseTwitterManager_1__ctor_m2106005101_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1__ctor_m2106005101(__this, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1__ctor_m2106005101_gshared)(__this, method)
// System.Void BaseTwitterManager`1<System.Object>::Start()
extern "C"  void BaseTwitterManager_1_Start_m782943989_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_Start_m782943989(__this, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_Start_m782943989_gshared)(__this, method)
// System.Void BaseTwitterManager`1<System.Object>::Initialise(System.String,System.String)
extern "C"  void BaseTwitterManager_1_Initialise_m967028770_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___consumerKey0, String_t* ___consumerSecret1, const MethodInfo* method);
#define BaseTwitterManager_1_Initialise_m967028770(__this, ___consumerKey0, ___consumerSecret1, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_Initialise_m967028770_gshared)(__this, ___consumerKey0, ___consumerSecret1, method)
// System.Void BaseTwitterManager`1<System.Object>::PerformAPIRequest(BaseTwitterManager`1/RequestType<ManagerType>,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void BaseTwitterManager_1_PerformAPIRequest_m34965517_gshared (BaseTwitterManager_1_t2275765192 * __this, int32_t ___type0, String_t* ___path1, Dictionary_2_t3943999495 * ___requestParameters2, const MethodInfo* method);
#define BaseTwitterManager_1_PerformAPIRequest_m34965517(__this, ___type0, ___path1, ___requestParameters2, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, int32_t, String_t*, Dictionary_2_t3943999495 *, const MethodInfo*))BaseTwitterManager_1_PerformAPIRequest_m34965517_gshared)(__this, ___type0, ___path1, ___requestParameters2, method)
// System.Void BaseTwitterManager`1<System.Object>::Login(TwitterLoginEntrance)
extern "C"  void BaseTwitterManager_1_Login_m2724261672_gshared (BaseTwitterManager_1_t2275765192 * __this, int32_t ___loginEntrance0, const MethodInfo* method);
#define BaseTwitterManager_1_Login_m2724261672(__this, ___loginEntrance0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, int32_t, const MethodInfo*))BaseTwitterManager_1_Login_m2724261672_gshared)(__this, ___loginEntrance0, method)
// System.Void BaseTwitterManager`1<System.Object>::Logout()
extern "C"  void BaseTwitterManager_1_Logout_m446240679_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_Logout_m446240679(__this, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_Logout_m446240679_gshared)(__this, method)
// System.Boolean BaseTwitterManager`1<System.Object>::IsLoggedIn()
extern "C"  bool BaseTwitterManager_1_IsLoggedIn_m3542423240_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_IsLoggedIn_m3542423240(__this, method) ((  bool (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_IsLoggedIn_m3542423240_gshared)(__this, method)
// System.String BaseTwitterManager`1<System.Object>::GetLoggedInUsername()
extern "C"  String_t* BaseTwitterManager_1_GetLoggedInUsername_m3874150437_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_GetLoggedInUsername_m3874150437(__this, method) ((  String_t* (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_GetLoggedInUsername_m3874150437_gshared)(__this, method)
// System.Void BaseTwitterManager`1<System.Object>::PostStatusUpdate(System.String)
extern "C"  void BaseTwitterManager_1_PostStatusUpdate_m3539375508_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___status0, const MethodInfo* method);
#define BaseTwitterManager_1_PostStatusUpdate_m3539375508(__this, ___status0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, const MethodInfo*))BaseTwitterManager_1_PostStatusUpdate_m3539375508_gshared)(__this, ___status0, method)
// System.Void BaseTwitterManager`1<System.Object>::PostImageStatusUpdate(System.String,System.String)
extern "C"  void BaseTwitterManager_1_PostImageStatusUpdate_m2760878927_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___status0, String_t* ___imagePath1, const MethodInfo* method);
#define BaseTwitterManager_1_PostImageStatusUpdate_m2760878927(__this, ___status0, ___imagePath1, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_PostImageStatusUpdate_m2760878927_gshared)(__this, ___status0, ___imagePath1, method)
// System.Void BaseTwitterManager`1<System.Object>::ShowTweetComposer(System.String)
extern "C"  void BaseTwitterManager_1_ShowTweetComposer_m3417481457_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___status0, const MethodInfo* method);
#define BaseTwitterManager_1_ShowTweetComposer_m3417481457(__this, ___status0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, const MethodInfo*))BaseTwitterManager_1_ShowTweetComposer_m3417481457_gshared)(__this, ___status0, method)
// System.Void BaseTwitterManager`1<System.Object>::ShowTweetComposer(System.String,System.String)
extern "C"  void BaseTwitterManager_1_ShowTweetComposer_m3875334979_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___status0, String_t* ___imagePath1, const MethodInfo* method);
#define BaseTwitterManager_1_ShowTweetComposer_m3875334979(__this, ___status0, ___imagePath1, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_ShowTweetComposer_m3875334979_gshared)(__this, ___status0, ___imagePath1, method)
// System.Action`1<System.String> BaseTwitterManager`1<System.Object>::LoginSuccess()
extern "C"  Action_1_t1831019615 * BaseTwitterManager_1_LoginSuccess_m256501760_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_LoginSuccess_m256501760(__this, method) ((  Action_1_t1831019615 * (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_LoginSuccess_m256501760_gshared)(__this, method)
// System.Collections.IEnumerator BaseTwitterManager`1<System.Object>::CheckLogingSuccess()
extern "C"  Il2CppObject * BaseTwitterManager_1_CheckLogingSuccess_m2776892846_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_CheckLogingSuccess_m2776892846(__this, method) ((  Il2CppObject * (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_CheckLogingSuccess_m2776892846_gshared)(__this, method)
// System.Void BaseTwitterManager`1<System.Object>::TwitterLoginSuccess()
extern "C"  void BaseTwitterManager_1_TwitterLoginSuccess_m3082522860_gshared (BaseTwitterManager_1_t2275765192 * __this, const MethodInfo* method);
#define BaseTwitterManager_1_TwitterLoginSuccess_m3082522860(__this, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, const MethodInfo*))BaseTwitterManager_1_TwitterLoginSuccess_m3082522860_gshared)(__this, method)
// System.Void BaseTwitterManager`1<System.Object>::TwitterRequestSuccessEvent(System.Object)
extern "C"  void BaseTwitterManager_1_TwitterRequestSuccessEvent_m2359635270_gshared (BaseTwitterManager_1_t2275765192 * __this, Il2CppObject * ___requestObject0, const MethodInfo* method);
#define BaseTwitterManager_1_TwitterRequestSuccessEvent_m2359635270(__this, ___requestObject0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, Il2CppObject *, const MethodInfo*))BaseTwitterManager_1_TwitterRequestSuccessEvent_m2359635270_gshared)(__this, ___requestObject0, method)
// System.Void BaseTwitterManager`1<System.Object>::TwitterRequestSuccess(System.String,System.String)
extern "C"  void BaseTwitterManager_1_TwitterRequestSuccess_m3075721710_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___code0, String_t* ___message1, const MethodInfo* method);
#define BaseTwitterManager_1_TwitterRequestSuccess_m3075721710(__this, ___code0, ___message1, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_TwitterRequestSuccess_m3075721710_gshared)(__this, ___code0, ___message1, method)
// System.Void BaseTwitterManager`1<System.Object>::TwitterRequestFailedEvent(System.String)
extern "C"  void BaseTwitterManager_1_TwitterRequestFailedEvent_m1998231770_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___requestString0, const MethodInfo* method);
#define BaseTwitterManager_1_TwitterRequestFailedEvent_m1998231770(__this, ___requestString0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, const MethodInfo*))BaseTwitterManager_1_TwitterRequestFailedEvent_m1998231770_gshared)(__this, ___requestString0, method)
// System.Void BaseTwitterManager`1<System.Object>::TwitterRequestFailed(System.String)
extern "C"  void BaseTwitterManager_1_TwitterRequestFailed_m1151393876_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___failString0, const MethodInfo* method);
#define BaseTwitterManager_1_TwitterRequestFailed_m1151393876(__this, ___failString0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, const MethodInfo*))BaseTwitterManager_1_TwitterRequestFailed_m1151393876_gshared)(__this, ___failString0, method)
// System.Void BaseTwitterManager`1<System.Object>::<Start>m__0(System.Object)
extern "C"  void BaseTwitterManager_1_U3CStartU3Em__0_m2718740694_gshared (BaseTwitterManager_1_t2275765192 * __this, Il2CppObject * ___requestObject0, const MethodInfo* method);
#define BaseTwitterManager_1_U3CStartU3Em__0_m2718740694(__this, ___requestObject0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, Il2CppObject *, const MethodInfo*))BaseTwitterManager_1_U3CStartU3Em__0_m2718740694_gshared)(__this, ___requestObject0, method)
// System.Void BaseTwitterManager`1<System.Object>::<Start>m__1(System.String)
extern "C"  void BaseTwitterManager_1_U3CStartU3Em__1_m3162299589_gshared (BaseTwitterManager_1_t2275765192 * __this, String_t* ___requestString0, const MethodInfo* method);
#define BaseTwitterManager_1_U3CStartU3Em__1_m3162299589(__this, ___requestString0, method) ((  void (*) (BaseTwitterManager_1_t2275765192 *, String_t*, const MethodInfo*))BaseTwitterManager_1_U3CStartU3Em__1_m3162299589_gshared)(__this, ___requestString0, method)
