﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen402404669.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"

// System.Void System.Array/InternalEnumerator`1<DefendNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2280560146_gshared (InternalEnumerator_1_t402404669 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2280560146(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t402404669 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2280560146_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<DefendNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m577257706_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m577257706(__this, method) ((  void (*) (InternalEnumerator_1_t402404669 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m577257706_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<DefendNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2610194196_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2610194196(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t402404669 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2610194196_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<DefendNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m398501439_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m398501439(__this, method) ((  void (*) (InternalEnumerator_1_t402404669 *, const MethodInfo*))InternalEnumerator_1_Dispose_m398501439_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<DefendNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1431212986_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1431212986(__this, method) ((  bool (*) (InternalEnumerator_1_t402404669 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1431212986_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<DefendNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2824285659_gshared (InternalEnumerator_1_t402404669 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2824285659(__this, method) ((  int32_t (*) (InternalEnumerator_1_t402404669 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2824285659_gshared)(__this, method)
