﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m2036600211(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m4019755228(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::Start()
#define BaseSceneManager_1_Start_m2734918919(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m3753802975(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m3925053879(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m1271953153(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<LoginRegistrationSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m1466064005(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m2208722634(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginRegistrationSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m1402064685(__this, method) ((  void (*) (BaseSceneManager_1_t1706330337 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
