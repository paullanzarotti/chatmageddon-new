﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<LoadingPopUp>::.ctor()
#define MonoSingleton_1__ctor_m2376505770(__this, method) ((  void (*) (MonoSingleton_1_t766945646 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<LoadingPopUp>::Awake()
#define MonoSingleton_1_Awake_m1309624681(__this, method) ((  void (*) (MonoSingleton_1_t766945646 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<LoadingPopUp>::get_Instance()
#define MonoSingleton_1_get_Instance_m1981948315(__this /* static, unused */, method) ((  LoadingPopUp_t1016279926 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<LoadingPopUp>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1811982823(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<LoadingPopUp>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m502370428(__this, method) ((  void (*) (MonoSingleton_1_t766945646 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<LoadingPopUp>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m688320768(__this, method) ((  void (*) (MonoSingleton_1_t766945646 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<LoadingPopUp>::.cctor()
#define MonoSingleton_1__cctor_m4264560927(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
