﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsProfileScreenList
struct SettingsProfileScreenList_t677344198;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"

// System.Void SettingsProfileScreenList::.ctor()
extern "C"  void SettingsProfileScreenList__ctor_m2068699011 (SettingsProfileScreenList_t677344198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileScreenList::SetUpList()
extern "C"  void SettingsProfileScreenList_SetUpList_m1734648220 (SettingsProfileScreenList_t677344198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileScreenList::AssignProfileData(System.String,System.String,System.String)
extern "C"  void SettingsProfileScreenList_AssignProfileData_m3673419723 (SettingsProfileScreenList_t677344198 * __this, String_t* ___firstName0, String_t* ___lastName1, String_t* ___userName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileScreenList::AssignGenderData(Gender)
extern "C"  void SettingsProfileScreenList_AssignGenderData_m1581420522 (SettingsProfileScreenList_t677344198 * __this, int32_t ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileScreenList::AssignAudienceData(Audience)
extern "C"  void SettingsProfileScreenList_AssignAudienceData_m1964787624 (SettingsProfileScreenList_t677344198 * __this, int32_t ___audience0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileScreenList::AssignLocationData(System.Boolean)
extern "C"  void SettingsProfileScreenList_AssignLocationData_m3016851990 (SettingsProfileScreenList_t677344198 * __this, bool ___allow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
