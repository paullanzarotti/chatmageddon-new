﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsSearchNavScreen
struct  FriendsSearchNavScreen_t2437737290  : public NavigationScreen_t2333230110
{
public:
	// System.Boolean FriendsSearchNavScreen::searchUIclosing
	bool ___searchUIclosing_3;

public:
	inline static int32_t get_offset_of_searchUIclosing_3() { return static_cast<int32_t>(offsetof(FriendsSearchNavScreen_t2437737290, ___searchUIclosing_3)); }
	inline bool get_searchUIclosing_3() const { return ___searchUIclosing_3; }
	inline bool* get_address_of_searchUIclosing_3() { return &___searchUIclosing_3; }
	inline void set_searchUIclosing_3(bool value)
	{
		___searchUIclosing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
