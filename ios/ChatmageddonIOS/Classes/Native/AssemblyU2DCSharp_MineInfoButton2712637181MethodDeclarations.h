﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineInfoButton
struct MineInfoButton_t2712637181;

#include "codegen/il2cpp-codegen.h"

// System.Void MineInfoButton::.ctor()
extern "C"  void MineInfoButton__ctor_m3646966826 (MineInfoButton_t2712637181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoButton::OnButtonReset()
extern "C"  void MineInfoButton_OnButtonReset_m451639326 (MineInfoButton_t2712637181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoButton::OnButtonRotate()
extern "C"  void MineInfoButton_OnButtonRotate_m379613370 (MineInfoButton_t2712637181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
