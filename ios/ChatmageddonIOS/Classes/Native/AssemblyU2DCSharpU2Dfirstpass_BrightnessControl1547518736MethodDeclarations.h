﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BrightnessControl
struct BrightnessControl_t1547518736;
// RefInt
struct RefInt_t2938871354;
// Vectrosity.VectorLine
struct VectorLine_t3390220087;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"

// System.Void BrightnessControl::.ctor()
extern "C"  void BrightnessControl__ctor_m1185115371 (BrightnessControl_t1547518736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RefInt BrightnessControl::get_objectNumber()
extern "C"  RefInt_t2938871354 * BrightnessControl_get_objectNumber_m1966244405 (BrightnessControl_t1547518736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrightnessControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C"  void BrightnessControl_Setup_m2406766712 (BrightnessControl_t1547518736 * __this, VectorLine_t3390220087 * ___line0, bool ___m_useLine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrightnessControl::SetUseLine(System.Boolean)
extern "C"  void BrightnessControl_SetUseLine_m1194400395 (BrightnessControl_t1547518736 * __this, bool ___useLine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrightnessControl::OnBecameVisible()
extern "C"  void BrightnessControl_OnBecameVisible_m974549453 (BrightnessControl_t1547518736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrightnessControl::OnBecameInvisible()
extern "C"  void BrightnessControl_OnBecameInvisible_m1181764460 (BrightnessControl_t1547518736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrightnessControl::OnDestroy()
extern "C"  void BrightnessControl_OnDestroy_m2063604992 (BrightnessControl_t1547518736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
