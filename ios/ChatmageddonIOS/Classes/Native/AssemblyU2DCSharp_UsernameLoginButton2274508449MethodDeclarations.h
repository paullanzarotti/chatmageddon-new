﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UsernameLoginButton
struct UsernameLoginButton_t2274508449;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UsernameLoginButton::.ctor()
extern "C"  void UsernameLoginButton__ctor_m1114319632 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UsernameLoginButton::Start()
extern "C"  void UsernameLoginButton_Start_m1341925416 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UsernameLoginButton::OnButtonClick()
extern "C"  void UsernameLoginButton_OnButtonClick_m2993763317 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UsernameLoginButton::UserLogin()
extern "C"  void UsernameLoginButton_UserLogin_m1143078920 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UsernameLoginButton::ValidateInput()
extern "C"  bool UsernameLoginButton_ValidateInput_m567769604 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UsernameLoginButton::UnloadUserLoginUI()
extern "C"  void UsernameLoginButton_UnloadUserLoginUI_m1160369251 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UsernameLoginButton::UnloadRemainingUI()
extern "C"  void UsernameLoginButton_UnloadRemainingUI_m3560200141 (UsernameLoginButton_t2274508449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UsernameLoginButton::<UserLogin>m__0(System.Boolean,System.String)
extern "C"  void UsernameLoginButton_U3CUserLoginU3Em__0_m535833292 (UsernameLoginButton_t2274508449 * __this, bool ___loginSuccess0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
