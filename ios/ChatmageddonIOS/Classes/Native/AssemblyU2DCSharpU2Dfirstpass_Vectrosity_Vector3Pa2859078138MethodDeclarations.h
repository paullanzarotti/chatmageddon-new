﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Vectrosity.Vector3Pair::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Vector3Pair__ctor_m122554125 (Vector3Pair_t2859078138 * __this, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
