﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Darken
struct CameraFilterPack_Blend2Camera_Darken_t558638219;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Darken::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Darken__ctor_m2878112352 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Darken::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Darken_get_material_m1938958387 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Darken_Start_m746478764 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Darken_OnRenderImage_m3508282940 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Darken_OnValidate_m1082941117 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Darken_Update_m3860461231 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Darken_OnEnable_m3800678284 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Darken_OnDisable_m1776271503 (CameraFilterPack_Blend2Camera_Darken_t558638219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
