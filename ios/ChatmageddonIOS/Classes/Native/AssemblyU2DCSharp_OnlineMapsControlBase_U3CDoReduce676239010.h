﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsControlBase
struct OnlineMapsControlBase_t473237564;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0
struct  U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010  : public Il2CppObject
{
public:
	// System.Single OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::<totalTime>__0
	float ___U3CtotalTimeU3E__0_0;
	// System.Single OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::<differenceTime>__1
	float ___U3CdifferenceTimeU3E__1_1;
	// System.Double OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::distanceX
	double ___distanceX_2;
	// System.Double OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::<offsetX>__2
	double ___U3CoffsetXU3E__2_3;
	// System.Double OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::distanceY
	double ___distanceY_4;
	// System.Double OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::<offsetY>__3
	double ___U3CoffsetYU3E__3_5;
	// System.Double OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::<px>__4
	double ___U3CpxU3E__4_6;
	// System.Double OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::<py>__5
	double ___U3CpyU3E__5_7;
	// System.Single OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::durationTime
	float ___durationTime_8;
	// OnlineMapsControlBase OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::$this
	OnlineMapsControlBase_t473237564 * ___U24this_9;
	// System.Object OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CtotalTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U3CtotalTimeU3E__0_0)); }
	inline float get_U3CtotalTimeU3E__0_0() const { return ___U3CtotalTimeU3E__0_0; }
	inline float* get_address_of_U3CtotalTimeU3E__0_0() { return &___U3CtotalTimeU3E__0_0; }
	inline void set_U3CtotalTimeU3E__0_0(float value)
	{
		___U3CtotalTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdifferenceTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U3CdifferenceTimeU3E__1_1)); }
	inline float get_U3CdifferenceTimeU3E__1_1() const { return ___U3CdifferenceTimeU3E__1_1; }
	inline float* get_address_of_U3CdifferenceTimeU3E__1_1() { return &___U3CdifferenceTimeU3E__1_1; }
	inline void set_U3CdifferenceTimeU3E__1_1(float value)
	{
		___U3CdifferenceTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_distanceX_2() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___distanceX_2)); }
	inline double get_distanceX_2() const { return ___distanceX_2; }
	inline double* get_address_of_distanceX_2() { return &___distanceX_2; }
	inline void set_distanceX_2(double value)
	{
		___distanceX_2 = value;
	}

	inline static int32_t get_offset_of_U3CoffsetXU3E__2_3() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U3CoffsetXU3E__2_3)); }
	inline double get_U3CoffsetXU3E__2_3() const { return ___U3CoffsetXU3E__2_3; }
	inline double* get_address_of_U3CoffsetXU3E__2_3() { return &___U3CoffsetXU3E__2_3; }
	inline void set_U3CoffsetXU3E__2_3(double value)
	{
		___U3CoffsetXU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_distanceY_4() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___distanceY_4)); }
	inline double get_distanceY_4() const { return ___distanceY_4; }
	inline double* get_address_of_distanceY_4() { return &___distanceY_4; }
	inline void set_distanceY_4(double value)
	{
		___distanceY_4 = value;
	}

	inline static int32_t get_offset_of_U3CoffsetYU3E__3_5() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U3CoffsetYU3E__3_5)); }
	inline double get_U3CoffsetYU3E__3_5() const { return ___U3CoffsetYU3E__3_5; }
	inline double* get_address_of_U3CoffsetYU3E__3_5() { return &___U3CoffsetYU3E__3_5; }
	inline void set_U3CoffsetYU3E__3_5(double value)
	{
		___U3CoffsetYU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CpxU3E__4_6() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U3CpxU3E__4_6)); }
	inline double get_U3CpxU3E__4_6() const { return ___U3CpxU3E__4_6; }
	inline double* get_address_of_U3CpxU3E__4_6() { return &___U3CpxU3E__4_6; }
	inline void set_U3CpxU3E__4_6(double value)
	{
		___U3CpxU3E__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CpyU3E__5_7() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U3CpyU3E__5_7)); }
	inline double get_U3CpyU3E__5_7() const { return ___U3CpyU3E__5_7; }
	inline double* get_address_of_U3CpyU3E__5_7() { return &___U3CpyU3E__5_7; }
	inline void set_U3CpyU3E__5_7(double value)
	{
		___U3CpyU3E__5_7 = value;
	}

	inline static int32_t get_offset_of_durationTime_8() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___durationTime_8)); }
	inline float get_durationTime_8() const { return ___durationTime_8; }
	inline float* get_address_of_durationTime_8() { return &___durationTime_8; }
	inline void set_durationTime_8(float value)
	{
		___durationTime_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U24this_9)); }
	inline OnlineMapsControlBase_t473237564 * get_U24this_9() const { return ___U24this_9; }
	inline OnlineMapsControlBase_t473237564 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(OnlineMapsControlBase_t473237564 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
