﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchasePU
struct PurchasePU_t4126238880;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PurchasePU::.ctor()
extern "C"  void PurchasePU__ctor_m1666992501 (PurchasePU_t4126238880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU::Start()
extern "C"  void PurchasePU_Start_m2817996269 (PurchasePU_t4126238880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU::PopUptweenFinish()
extern "C"  void PurchasePU_PopUptweenFinish_m4139895685 (PurchasePU_t4126238880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU::OpenPU()
extern "C"  void PurchasePU_OpenPU_m4100902892 (PurchasePU_t4126238880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU::SetPurchaseSuccess(System.Boolean)
extern "C"  void PurchasePU_SetPurchaseSuccess_m2261010400 (PurchasePU_t4126238880 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PurchasePU::WaitForSeconds()
extern "C"  Il2CppObject * PurchasePU_WaitForSeconds_m3301744054 (PurchasePU_t4126238880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU::ClosePU()
extern "C"  void PurchasePU_ClosePU_m2559979876 (PurchasePU_t4126238880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
