﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApplicationManager/<LogoutFromServer>c__AnonStorey2
struct U3CLogoutFromServerU3Ec__AnonStorey2_t833513242;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ApplicationManager/<LogoutFromServer>c__AnonStorey2::.ctor()
extern "C"  void U3CLogoutFromServerU3Ec__AnonStorey2__ctor_m3115403255 (U3CLogoutFromServerU3Ec__AnonStorey2_t833513242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager/<LogoutFromServer>c__AnonStorey2::<>m__0(System.Boolean,System.String)
extern "C"  void U3CLogoutFromServerU3Ec__AnonStorey2_U3CU3Em__0_m3861243093 (U3CLogoutFromServerU3Ec__AnonStorey2_t833513242 * __this, bool ___logoutSuccess0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
