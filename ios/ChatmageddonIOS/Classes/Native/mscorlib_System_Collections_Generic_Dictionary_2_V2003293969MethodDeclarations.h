﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>
struct Dictionary_2_t316761205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2003293969.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Decimal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m524528375_gshared (Enumerator_t2003293969 * __this, Dictionary_2_t316761205 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m524528375(__this, ___host0, method) ((  void (*) (Enumerator_t2003293969 *, Dictionary_2_t316761205 *, const MethodInfo*))Enumerator__ctor_m524528375_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m185564968_gshared (Enumerator_t2003293969 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m185564968(__this, method) ((  Il2CppObject * (*) (Enumerator_t2003293969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m185564968_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3899088182_gshared (Enumerator_t2003293969 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3899088182(__this, method) ((  void (*) (Enumerator_t2003293969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3899088182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Decimal>::Dispose()
extern "C"  void Enumerator_Dispose_m2103843755_gshared (Enumerator_t2003293969 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2103843755(__this, method) ((  void (*) (Enumerator_t2003293969 *, const MethodInfo*))Enumerator_Dispose_m2103843755_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Decimal>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1215774862_gshared (Enumerator_t2003293969 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1215774862(__this, method) ((  bool (*) (Enumerator_t2003293969 *, const MethodInfo*))Enumerator_MoveNext_m1215774862_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Decimal>::get_Current()
extern "C"  Decimal_t724701077  Enumerator_get_Current_m2022574640_gshared (Enumerator_t2003293969 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2022574640(__this, method) ((  Decimal_t724701077  (*) (Enumerator_t2003293969 *, const MethodInfo*))Enumerator_get_Current_m2022574640_gshared)(__this, method)
