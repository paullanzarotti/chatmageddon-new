﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenCenterWidgetScaleAndAlpha
struct TweenCenterWidgetScaleAndAlpha_t2087610359;

#include "codegen/il2cpp-codegen.h"

// System.Void TweenCenterWidgetScaleAndAlpha::.ctor()
extern "C"  void TweenCenterWidgetScaleAndAlpha__ctor_m2051653802 (TweenCenterWidgetScaleAndAlpha_t2087610359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScaleAndAlpha::Start()
extern "C"  void TweenCenterWidgetScaleAndAlpha_Start_m4287490166 (TweenCenterWidgetScaleAndAlpha_t2087610359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScaleAndAlpha::Grow()
extern "C"  void TweenCenterWidgetScaleAndAlpha_Grow_m2015649219 (TweenCenterWidgetScaleAndAlpha_t2087610359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScaleAndAlpha::Shrink()
extern "C"  void TweenCenterWidgetScaleAndAlpha_Shrink_m756491215 (TweenCenterWidgetScaleAndAlpha_t2087610359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScaleAndAlpha::AddTweeners()
extern "C"  void TweenCenterWidgetScaleAndAlpha_AddTweeners_m676554764 (TweenCenterWidgetScaleAndAlpha_t2087610359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
