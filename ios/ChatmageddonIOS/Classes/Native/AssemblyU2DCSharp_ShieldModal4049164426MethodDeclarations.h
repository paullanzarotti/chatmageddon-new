﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldModal
struct ShieldModal_t4049164426;
// Shield
struct Shield_t3327121081;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "AssemblyU2DCSharp_ShieldType96263953.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ShieldModal::.ctor()
extern "C"  void ShieldModal__ctor_m1389801511 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::OnUIActivate()
extern "C"  void ShieldModal_OnUIActivate_m3167421199 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::OnUIDeactivate()
extern "C"  void ShieldModal_OnUIDeactivate_m306214376 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::OnUICleared()
extern "C"  void ShieldModal_OnUICleared_m1867335520 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldModal::SetExternalLightsActive(System.Boolean)
extern "C"  bool ShieldModal_SetExternalLightsActive_m352708974 (ShieldModal_t4049164426 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::LoadShieldInventory()
extern "C"  void ShieldModal_LoadShieldInventory_m759528294 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldModal::ShieldItemsContains(Shield)
extern "C"  bool ShieldModal_ShieldItemsContains_m251708866 (ShieldModal_t4049164426 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::LoadDefaultItem()
extern "C"  void ShieldModal_LoadDefaultItem_m52446063 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShieldModal::LoadDefaultModel()
extern "C"  Il2CppObject * ShieldModal_LoadDefaultModel_m1140664199 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::OnDefaultItemShow(System.Boolean)
extern "C"  void ShieldModal_OnDefaultItemShow_m35763636 (ShieldModal_t4049164426 * __this, bool ___closed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::LoadNextItem()
extern "C"  void ShieldModal_LoadNextItem_m2867827175 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::LoadPreviousItem()
extern "C"  void ShieldModal_LoadPreviousItem_m3845706721 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::TweenItem(System.Boolean)
extern "C"  void ShieldModal_TweenItem_m451758704 (ShieldModal_t4049164426 * __this, bool ___right0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::OnTweenItemComplete()
extern "C"  void ShieldModal_OnTweenItemComplete_m920737579 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::LoadItem(Shield)
extern "C"  void ShieldModal_LoadItem_m955815387 (ShieldModal_t4049164426 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::AlphaTweenLabels(System.Boolean)
extern "C"  void ShieldModal_AlphaTweenLabels_m622057140 (ShieldModal_t4049164426 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::ShowItemDescription(System.Boolean)
extern "C"  void ShieldModal_ShowItemDescription_m192905664 (ShieldModal_t4049164426 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::ActivateCurrentShield()
extern "C"  void ShieldModal_ActivateCurrentShield_m3245228350 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::ShowWarningPopUp(ShieldType)
extern "C"  void ShieldModal_ShowWarningPopUp_m2290258179 (ShieldModal_t4049164426 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::CloseWarningPopUp()
extern "C"  void ShieldModal_CloseWarningPopUp_m1707461753 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::PopUpClosed()
extern "C"  void ShieldModal_PopUpClosed_m1359853951 (ShieldModal_t4049164426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::PlayShieldActivationsSFX(Shield)
extern "C"  void ShieldModal_PlayShieldActivationsSFX_m1521258485 (ShieldModal_t4049164426 * __this, Shield_t3327121081 * ___shield0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal::<ActivateCurrentShield>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ShieldModal_U3CActivateCurrentShieldU3Em__0_m2396375314 (ShieldModal_t4049164426 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
