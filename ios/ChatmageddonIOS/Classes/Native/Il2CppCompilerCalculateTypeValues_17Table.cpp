﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgor1654661965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClie3823629320.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3166895267.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro1946181211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProt173216930.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSign389653629.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig1282301050.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC3722381418.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2290372928.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityPr155967584.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerCon3823737132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Validatio1782558132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3918817353.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1404755603.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh3044322977.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamB934199321.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1610391122.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherS396038680.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient2311449551.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcepti583514812.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerS403340211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream4089752859.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3938752374.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2540099417.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2537917473.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4150496570.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3939745042.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2939633944.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3808761250.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_905088469.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2187269356.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1869592958.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1289300668.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_530021076.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2172608670.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest572679901.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certificat989458295.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3318447433.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3721235490.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe1663566523.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1892466092.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2038352954.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2540610921.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778804.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2896841275.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2866209745.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778802.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1703410334.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1957337331.h"
#include "System_Security_U3CModuleU3E3783534214.h"
#include "System_Security_Mono_Xml_XmlCanonicalizer2205787307.h"
#include "System_Security_System_Security_Cryptography_Xml_C2270233253.h"
#include "System_Security_System_Security_Cryptography_Xml_C2913200258.h"
#include "System_Security_System_Security_Cryptography_Xml_D2617499975.h"
#include "System_Security_System_Security_Cryptography_Xml_D2994118318.h"
#include "System_Security_System_Security_Cryptography_Xml_E1564008194.h"
#include "System_Security_System_Security_Cryptography_Xml_E3186175257.h"
#include "System_Security_System_Security_Cryptography_Xml_E2364822029.h"
#include "System_Security_System_Security_Cryptography_Xml_E2404077154.h"
#include "System_Security_System_Security_Cryptography_Xml_E2829945639.h"
#include "System_Security_System_Security_Cryptography_Xml_En718466202.h"
#include "System_Security_System_Security_Cryptography_Xml_E2509585554.h"
#include "System_Security_System_Security_Cryptography_Xml_En773336898.h"
#include "System_Security_System_Security_Cryptography_Xml_Ke279567104.h"
#include "System_Security_System_Security_Cryptography_Xml_Ke862955101.h"
#include "System_Security_System_Security_Cryptography_Xml_K3815382222.h"
#include "System_Security_System_Security_Cryptography_Xml_K3176207690.h"
#include "System_Security_System_Security_Cryptography_Xml_KeyI3633849.h"
#include "System_Security_System_Security_Cryptography_Xml_K1087857306.h"
#include "System_Security_System_Security_Cryptography_Xml_K1803297193.h"
#include "System_Security_System_Security_Cryptography_Xml_K1620770554.h"
#include "System_Security_System_Security_Cryptography_Xml_R1582251325.h"
#include "System_Security_System_Security_Cryptography_Xml_R2328376860.h"
#include "System_Security_System_Security_Cryptography_Xml_Sym86457392.h"
#include "System_Security_System_Security_Cryptography_Xml_Tr251592321.h"
#include "System_Security_System_Security_Cryptography_Xml_Tr729562346.h"
#include "System_Security_System_Security_Cryptography_Xml_X1582182491.h"
#include "System_Security_System_Security_Cryptography_Xml_X3146098518.h"
#include "System_Security_System_Security_Cryptography_Xml_X3785997915.h"
#include "System_Security_System_Security_Cryptography_Xml_X4201013104.h"
#include "System_Security_System_Security_Cryptography_Xml_X2962768146.h"
#include "System_Security_System_Security_Cryptography_Xml_X3796416260.h"
#include "System_Security_System_Security_Cryptography_Xml_Xm834118254.h"
#include "System_Security_System_Security_Cryptography_Xml_X1136189132.h"
#include "System_Security_System_Security_Cryptography_Xml_X4119907213.h"
#include "System_Security_System_Security_Cryptography_Xml_X1991025147.h"
#include "System_Security_System_Security_Cryptography_Xml_X2950489621.h"
#include "System_Security_System_MonoTODOAttribute3487514019.h"
#include "System_Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Security_U3CPrivateImplementationDetailsU3E1957337327.h"
#include "System_U3CModuleU3E3783534214.h"
#include "System_Locale4255929014.h"
#include "System_System_MonoTODOAttribute3487514019.h"
#include "System_System_MonoNotSupportedAttribute2197455930.h"
#include "System_Mono_CSharp_CSharpCodeCompiler1800184175.h"
#include "System_Mono_CSharp_CSharpCodeGenerator1166552479.h"
#include "System_Microsoft_CSharp_CSharpCodeProvider2882678967.h"
#include "System_System_CodeDom_CodeConstructor2209283105.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (HashAlgorithmType_t1654661965)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[4] = 
{
	HashAlgorithmType_t1654661965::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (HttpsClientStream_t3823629320), -1, sizeof(HttpsClientStream_t3823629320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	HttpsClientStream_t3823629320::get_offset_of__request_21(),
	HttpsClientStream_t3823629320::get_offset_of__status_22(),
	HttpsClientStream_t3823629320_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_23(),
	HttpsClientStream_t3823629320_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (RecordProtocol_t3166895267), -1, sizeof(RecordProtocol_t3166895267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[3] = 
{
	RecordProtocol_t3166895267_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3166895267::get_offset_of_innerStream_1(),
	RecordProtocol_t3166895267::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (ReceiveRecordAsyncResult_t1946181211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[9] = 
{
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (SendRecordAsyncResult_t173216930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[7] = 
{
	SendRecordAsyncResult_t173216930::get_offset_of_locker_0(),
	SendRecordAsyncResult_t173216930::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t173216930::get_offset_of__userState_2(),
	SendRecordAsyncResult_t173216930::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t173216930::get_offset_of_handle_4(),
	SendRecordAsyncResult_t173216930::get_offset_of__message_5(),
	SendRecordAsyncResult_t173216930::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (RSASslSignatureDeformatter_t389653629), -1, sizeof(RSASslSignatureDeformatter_t389653629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[3] = 
{
	RSASslSignatureDeformatter_t389653629::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t389653629::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t389653629_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (RSASslSignatureFormatter_t1282301050), -1, sizeof(RSASslSignatureFormatter_t1282301050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1706[3] = 
{
	RSASslSignatureFormatter_t1282301050::get_offset_of_key_0(),
	RSASslSignatureFormatter_t1282301050::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t1282301050_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (SecurityCompressionType_t3722381418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[3] = 
{
	SecurityCompressionType_t3722381418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (SecurityParameters_t2290372928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[3] = 
{
	SecurityParameters_t2290372928::get_offset_of_cipher_0(),
	SecurityParameters_t2290372928::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2290372928::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (SecurityProtocolType_t155967584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1709[5] = 
{
	SecurityProtocolType_t155967584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (ServerContext_t3823737132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (ValidationResult_t1782558132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[3] = 
{
	ValidationResult_t1782558132::get_offset_of_trusted_0(),
	ValidationResult_t1782558132::get_offset_of_user_denied_1(),
	ValidationResult_t1782558132::get_offset_of_error_code_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (SslClientStream_t3918817353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[4] = 
{
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation_17(),
	SslClientStream_t3918817353::get_offset_of_ClientCertSelection_18(),
	SslClientStream_t3918817353::get_offset_of_PrivateKeySelection_19(),
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation2_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (SslCipherSuite_t1404755603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[3] = 
{
	SslCipherSuite_t1404755603::get_offset_of_pad1_21(),
	SslCipherSuite_t1404755603::get_offset_of_pad2_22(),
	SslCipherSuite_t1404755603::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (SslHandshakeHash_t3044322977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[8] = 
{
	SslHandshakeHash_t3044322977::get_offset_of_md5_4(),
	SslHandshakeHash_t3044322977::get_offset_of_sha_5(),
	SslHandshakeHash_t3044322977::get_offset_of_hashing_6(),
	SslHandshakeHash_t3044322977::get_offset_of_secret_7(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (SslStreamBase_t934199321), -1, sizeof(SslStreamBase_t934199321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[15] = 
{
	0,
	SslStreamBase_t934199321_StaticFields::get_offset_of_record_processing_3(),
	SslStreamBase_t934199321::get_offset_of_innerStream_4(),
	SslStreamBase_t934199321::get_offset_of_inputBuffer_5(),
	SslStreamBase_t934199321::get_offset_of_context_6(),
	SslStreamBase_t934199321::get_offset_of_protocol_7(),
	SslStreamBase_t934199321::get_offset_of_ownsStream_8(),
	SslStreamBase_t934199321::get_offset_of_disposed_9(),
	SslStreamBase_t934199321::get_offset_of_checkCertRevocationStatus_10(),
	SslStreamBase_t934199321::get_offset_of_negotiate_11(),
	SslStreamBase_t934199321::get_offset_of_read_12(),
	SslStreamBase_t934199321::get_offset_of_write_13(),
	SslStreamBase_t934199321::get_offset_of_negotiationComplete_14(),
	SslStreamBase_t934199321::get_offset_of_recbuf_15(),
	SslStreamBase_t934199321::get_offset_of_recordStream_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (InternalAsyncResult_t1610391122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[12] = 
{
	InternalAsyncResult_t1610391122::get_offset_of_locker_0(),
	InternalAsyncResult_t1610391122::get_offset_of__userCallback_1(),
	InternalAsyncResult_t1610391122::get_offset_of__userState_2(),
	InternalAsyncResult_t1610391122::get_offset_of__asyncException_3(),
	InternalAsyncResult_t1610391122::get_offset_of_handle_4(),
	InternalAsyncResult_t1610391122::get_offset_of_completed_5(),
	InternalAsyncResult_t1610391122::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t1610391122::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t1610391122::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t1610391122::get_offset_of__buffer_9(),
	InternalAsyncResult_t1610391122::get_offset_of__offset_10(),
	InternalAsyncResult_t1610391122::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (TlsCipherSuite_t396038680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[2] = 
{
	TlsCipherSuite_t396038680::get_offset_of_header_21(),
	TlsCipherSuite_t396038680::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (TlsClientSettings_t2311449551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[4] = 
{
	TlsClientSettings_t2311449551::get_offset_of_targetHost_0(),
	TlsClientSettings_t2311449551::get_offset_of_certificates_1(),
	TlsClientSettings_t2311449551::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2311449551::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (TlsException_t583514812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	TlsException_t583514812::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (TlsServerSettings_t403340211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[8] = 
{
	TlsServerSettings_t403340211::get_offset_of_certificates_0(),
	TlsServerSettings_t403340211::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t403340211::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t403340211::get_offset_of_signedParams_3(),
	TlsServerSettings_t403340211::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t403340211::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t403340211::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t403340211::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (TlsStream_t4089752859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[4] = 
{
	TlsStream_t4089752859::get_offset_of_canRead_2(),
	TlsStream_t4089752859::get_offset_of_canWrite_3(),
	TlsStream_t4089752859::get_offset_of_buffer_4(),
	TlsStream_t4089752859::get_offset_of_temp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (ClientCertificateType_t4001384466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1722[6] = 
{
	ClientCertificateType_t4001384466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (HandshakeMessage_t3938752374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[4] = 
{
	HandshakeMessage_t3938752374::get_offset_of_context_6(),
	HandshakeMessage_t3938752374::get_offset_of_handshakeType_7(),
	HandshakeMessage_t3938752374::get_offset_of_contentType_8(),
	HandshakeMessage_t3938752374::get_offset_of_cache_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (HandshakeType_t2540099417)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[12] = 
{
	HandshakeType_t2540099417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (TlsClientCertificate_t2537917473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[2] = 
{
	TlsClientCertificate_t2537917473::get_offset_of_clientCertSelected_10(),
	TlsClientCertificate_t2537917473::get_offset_of_clientCert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (TlsClientCertificateVerify_t4150496570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (TlsClientFinished_t3939745042), -1, sizeof(TlsClientFinished_t3939745042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	TlsClientFinished_t3939745042_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (TlsClientHello_t2939633944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	TlsClientHello_t2939633944::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (TlsClientKeyExchange_t3808761250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (TlsServerCertificate_t905088469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[1] = 
{
	TlsServerCertificate_t905088469::get_offset_of_certificates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (TlsServerCertificateRequest_t2187269356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[2] = 
{
	TlsServerCertificateRequest_t2187269356::get_offset_of_certificateTypes_10(),
	TlsServerCertificateRequest_t2187269356::get_offset_of_distinguisedNames_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (TlsServerFinished_t1869592958), -1, sizeof(TlsServerFinished_t1869592958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1732[1] = 
{
	TlsServerFinished_t1869592958_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (TlsServerHello_t1289300668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[4] = 
{
	TlsServerHello_t1289300668::get_offset_of_compressionMethod_10(),
	TlsServerHello_t1289300668::get_offset_of_random_11(),
	TlsServerHello_t1289300668::get_offset_of_sessionId_12(),
	TlsServerHello_t1289300668::get_offset_of_cipherSuite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (TlsServerHelloDone_t530021076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (TlsServerKeyExchange_t2172608670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[2] = 
{
	TlsServerKeyExchange_t2172608670::get_offset_of_rsaParams_10(),
	TlsServerKeyExchange_t2172608670::get_offset_of_signedParams_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (PrimalityTest_t572679902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (CertificateValidationCallback_t989458295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (CertificateValidationCallback2_t3318447433), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (CertificateSelectionCallback_t3721235490), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (PrivateKeySelectionCallback_t1663566523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1741[15] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (U24ArrayTypeU243132_t1892466093)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t1892466093 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (U24ArrayTypeU24256_t2038352956)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352956 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (U24ArrayTypeU2420_t540610922)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t540610922 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (U24ArrayTypeU2432_t3672778805)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3672778805 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (U24ArrayTypeU2448_t896841276)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t896841276 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (U24ArrayTypeU2464_t2866209746)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209746 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (U24ArrayTypeU2412_t3672778806)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778806 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U24ArrayTypeU2416_t1703410335)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410335 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (U24ArrayTypeU244_t1957337331)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1957337331 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (U3CModuleU3E_t3783534217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (XmlCanonicalizer_t2205787307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[4] = 
{
	XmlCanonicalizer_t2205787307::get_offset_of_comments_0(),
	XmlCanonicalizer_t2205787307::get_offset_of_exclusive_1(),
	XmlCanonicalizer_t2205787307::get_offset_of_res_2(),
	XmlCanonicalizer_t2205787307::get_offset_of_propagatedNss_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (CipherData_t2270233253), -1, sizeof(CipherData_t2270233253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1753[3] = 
{
	CipherData_t2270233253::get_offset_of_cipherValue_0(),
	CipherData_t2270233253::get_offset_of_cipherReference_1(),
	CipherData_t2270233253_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (CipherReference_t2913200258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (DataReference_t2617499975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (DSAKeyValue_t2994118318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[1] = 
{
	DSAKeyValue_t2994118318::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (EncryptedData_t1564008194), -1, sizeof(EncryptedData_t1564008194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1757[1] = 
{
	EncryptedData_t1564008194_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (EncryptedKey_t3186175257), -1, sizeof(EncryptedKey_t3186175257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1758[5] = 
{
	EncryptedKey_t3186175257::get_offset_of_carriedKeyName_8(),
	EncryptedKey_t3186175257::get_offset_of_recipient_9(),
	EncryptedKey_t3186175257::get_offset_of_referenceList_10(),
	EncryptedKey_t3186175257_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_11(),
	EncryptedKey_t3186175257_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (EncryptedReference_t2364822029), -1, sizeof(EncryptedReference_t2364822029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1759[5] = 
{
	EncryptedReference_t2364822029::get_offset_of_referenceType_0(),
	EncryptedReference_t2364822029::get_offset_of_uri_1(),
	EncryptedReference_t2364822029::get_offset_of_tc_2(),
	EncryptedReference_t2364822029_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_3(),
	EncryptedReference_t2364822029_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (EncryptedType_t2404077154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[8] = 
{
	EncryptedType_t2404077154::get_offset_of_cipherData_0(),
	EncryptedType_t2404077154::get_offset_of_encoding_1(),
	EncryptedType_t2404077154::get_offset_of_encryptionMethod_2(),
	EncryptedType_t2404077154::get_offset_of_encryptionProperties_3(),
	EncryptedType_t2404077154::get_offset_of_id_4(),
	EncryptedType_t2404077154::get_offset_of_keyInfo_5(),
	EncryptedType_t2404077154::get_offset_of_mimeType_6(),
	EncryptedType_t2404077154::get_offset_of_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (EncryptedXml_t2829945639), -1, sizeof(EncryptedXml_t2829945639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1761[7] = 
{
	EncryptedXml_t2829945639::get_offset_of_encoding_0(),
	EncryptedXml_t2829945639::get_offset_of_keyNameMapping_1(),
	EncryptedXml_t2829945639::get_offset_of_mode_2(),
	EncryptedXml_t2829945639::get_offset_of_padding_3(),
	EncryptedXml_t2829945639::get_offset_of_document_4(),
	EncryptedXml_t2829945639_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_5(),
	EncryptedXml_t2829945639_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (EncryptionMethod_t718466202), -1, sizeof(EncryptionMethod_t718466202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1762[3] = 
{
	EncryptionMethod_t718466202::get_offset_of_algorithm_0(),
	EncryptionMethod_t718466202::get_offset_of_keySize_1(),
	EncryptionMethod_t718466202_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (EncryptionPropertyCollection_t2509585554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[1] = 
{
	EncryptionPropertyCollection_t2509585554::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (EncryptionProperty_t773336898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[2] = 
{
	EncryptionProperty_t773336898::get_offset_of_id_0(),
	EncryptionProperty_t773336898::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (KeyInfoClause_t279567104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (KeyInfo_t862955101), -1, sizeof(KeyInfo_t862955101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1766[4] = 
{
	KeyInfo_t862955101::get_offset_of_Info_0(),
	KeyInfo_t862955101::get_offset_of_id_1(),
	KeyInfo_t862955101_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_2(),
	KeyInfo_t862955101_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (KeyInfoEncryptedKey_t3815382222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[1] = 
{
	KeyInfoEncryptedKey_t3815382222::get_offset_of_encryptedKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (KeyInfoName_t3176207690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[1] = 
{
	KeyInfoName_t3176207690::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (KeyInfoNode_t3633849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[1] = 
{
	KeyInfoNode_t3633849::get_offset_of_Node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (KeyInfoRetrievalMethod_t1087857306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[3] = 
{
	KeyInfoRetrievalMethod_t1087857306::get_offset_of_URI_0(),
	KeyInfoRetrievalMethod_t1087857306::get_offset_of_element_1(),
	KeyInfoRetrievalMethod_t1087857306::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (KeyInfoX509Data_t1803297193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[5] = 
{
	KeyInfoX509Data_t1803297193::get_offset_of_x509crl_0(),
	KeyInfoX509Data_t1803297193::get_offset_of_IssuerSerialList_1(),
	KeyInfoX509Data_t1803297193::get_offset_of_SubjectKeyIdList_2(),
	KeyInfoX509Data_t1803297193::get_offset_of_SubjectNameList_3(),
	KeyInfoX509Data_t1803297193::get_offset_of_X509CertificateList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (KeyReference_t1620770554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (ReferenceList_t1582251325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[1] = 
{
	ReferenceList_t1582251325::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (RSAKeyValue_t2328376860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[1] = 
{
	RSAKeyValue_t2328376860::get_offset_of_rsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (SymmetricKeyWrap_t86457392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (TransformChain_t251592321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[1] = 
{
	TransformChain_t251592321::get_offset_of_chain_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (Transform_t729562346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[3] = 
{
	Transform_t729562346::get_offset_of_algo_0(),
	Transform_t729562346::get_offset_of_xmlResolver_1(),
	Transform_t729562346::get_offset_of_propagated_namespaces_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (X509IssuerSerial_t1582182491)+ sizeof (Il2CppObject), sizeof(X509IssuerSerial_t1582182491_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[2] = 
{
	X509IssuerSerial_t1582182491::get_offset_of__issuerName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509IssuerSerial_t1582182491::get_offset_of__serialNumber_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (XmlDecryptionTransform_t3146098518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[2] = 
{
	XmlDecryptionTransform_t3146098518::get_offset_of_encryptedXml_3(),
	XmlDecryptionTransform_t3146098518::get_offset_of_exceptUris_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (XmlDsigBase64Transform_t3785997915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (XmlDsigC14NTransform_t4201013104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[1] = 
{
	XmlDsigC14NTransform_t4201013104::get_offset_of_canonicalizer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (XmlDsigC14NWithCommentsTransform_t2962768146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (XmlDsigEnvelopedSignatureTransform_t3796416260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[1] = 
{
	XmlDsigEnvelopedSignatureTransform_t3796416260::get_offset_of_comments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (XmlDsigExcC14NTransform_t834118254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[2] = 
{
	XmlDsigExcC14NTransform_t834118254::get_offset_of_canonicalizer_3(),
	XmlDsigExcC14NTransform_t834118254::get_offset_of_inclusiveNamespacesPrefixList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (XmlDsigExcC14NWithCommentsTransform_t1136189132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (XmlDsigXPathTransform_t4119907213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[1] = 
{
	XmlDsigXPathTransform_t4119907213::get_offset_of_xpath_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (XmlDsigXsltTransform_t1991025147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[2] = 
{
	XmlDsigXsltTransform_t1991025147::get_offset_of_comments_3(),
	XmlDsigXsltTransform_t1991025147::get_offset_of_xnl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (XmlSignature_t2950489621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[1] = 
{
	MonoTODOAttribute_t3487514021::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1790[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (U24ArrayTypeU248_t1957337329)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337329 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (Locale_t4255929017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[1] = 
{
	MonoTODOAttribute_t3487514022::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (MonoNotSupportedAttribute_t2197455930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (CSharpCodeCompiler_t1800184175), -1, sizeof(CSharpCodeCompiler_t1800184175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1796[5] = 
{
	CSharpCodeCompiler_t1800184175_StaticFields::get_offset_of_windowsMcsPath_5(),
	CSharpCodeCompiler_t1800184175_StaticFields::get_offset_of_windowsMonoPath_6(),
	CSharpCodeCompiler_t1800184175::get_offset_of_mcsOutMutex_7(),
	CSharpCodeCompiler_t1800184175::get_offset_of_mcsOutput_8(),
	CSharpCodeCompiler_t1800184175_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (CSharpCodeGenerator_t1166552479), -1, sizeof(CSharpCodeGenerator_t1166552479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[3] = 
{
	CSharpCodeGenerator_t1166552479::get_offset_of_providerOptions_2(),
	CSharpCodeGenerator_t1166552479::get_offset_of_dont_write_semicolon_3(),
	CSharpCodeGenerator_t1166552479_StaticFields::get_offset_of_keywords_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (CSharpCodeProvider_t2882678967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[1] = 
{
	CSharpCodeProvider_t2882678967::get_offset_of_providerOptions_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (CodeConstructor_t2209283105), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
