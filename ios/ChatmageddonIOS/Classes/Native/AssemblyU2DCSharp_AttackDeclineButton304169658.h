﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackListItem
struct AttackListItem_t3405986821;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackDeclineButton
struct  AttackDeclineButton_t304169658  : public SFXButton_t792651341
{
public:
	// AttackListItem AttackDeclineButton::item
	AttackListItem_t3405986821 * ___item_5;

public:
	inline static int32_t get_offset_of_item_5() { return static_cast<int32_t>(offsetof(AttackDeclineButton_t304169658, ___item_5)); }
	inline AttackListItem_t3405986821 * get_item_5() const { return ___item_5; }
	inline AttackListItem_t3405986821 ** get_address_of_item_5() { return &___item_5; }
	inline void set_item_5(AttackListItem_t3405986821 * value)
	{
		___item_5 = value;
		Il2CppCodeGenWriteBarrier(&___item_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
