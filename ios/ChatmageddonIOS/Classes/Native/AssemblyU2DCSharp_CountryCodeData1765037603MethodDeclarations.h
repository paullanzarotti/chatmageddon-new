﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountryCodeData
struct CountryCodeData_t1765037603;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CountryCodeData::.ctor(System.String,System.String,System.String)
extern "C"  void CountryCodeData__ctor_m3209444860 (CountryCodeData_t1765037603 * __this, String_t* ___name0, String_t* ___shortName1, String_t* ___code2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
