﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPCycler
struct IPCycler_t1336138445;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_PickerScrollButton_ScrollDirection49895970.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickerScrollButton
struct  PickerScrollButton_t2644611827  : public MonoBehaviour_t1158329972
{
public:
	// IPCycler PickerScrollButton::targetCycler
	IPCycler_t1336138445 * ___targetCycler_2;
	// System.Single PickerScrollButton::scrollSpeed
	float ___scrollSpeed_3;
	// PickerScrollButton/ScrollDirection PickerScrollButton::scrollDirection
	int32_t ___scrollDirection_4;
	// System.Boolean PickerScrollButton::_pressed
	bool ____pressed_5;

public:
	inline static int32_t get_offset_of_targetCycler_2() { return static_cast<int32_t>(offsetof(PickerScrollButton_t2644611827, ___targetCycler_2)); }
	inline IPCycler_t1336138445 * get_targetCycler_2() const { return ___targetCycler_2; }
	inline IPCycler_t1336138445 ** get_address_of_targetCycler_2() { return &___targetCycler_2; }
	inline void set_targetCycler_2(IPCycler_t1336138445 * value)
	{
		___targetCycler_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetCycler_2, value);
	}

	inline static int32_t get_offset_of_scrollSpeed_3() { return static_cast<int32_t>(offsetof(PickerScrollButton_t2644611827, ___scrollSpeed_3)); }
	inline float get_scrollSpeed_3() const { return ___scrollSpeed_3; }
	inline float* get_address_of_scrollSpeed_3() { return &___scrollSpeed_3; }
	inline void set_scrollSpeed_3(float value)
	{
		___scrollSpeed_3 = value;
	}

	inline static int32_t get_offset_of_scrollDirection_4() { return static_cast<int32_t>(offsetof(PickerScrollButton_t2644611827, ___scrollDirection_4)); }
	inline int32_t get_scrollDirection_4() const { return ___scrollDirection_4; }
	inline int32_t* get_address_of_scrollDirection_4() { return &___scrollDirection_4; }
	inline void set_scrollDirection_4(int32_t value)
	{
		___scrollDirection_4 = value;
	}

	inline static int32_t get_offset_of__pressed_5() { return static_cast<int32_t>(offsetof(PickerScrollButton_t2644611827, ____pressed_5)); }
	inline bool get__pressed_5() const { return ____pressed_5; }
	inline bool* get_address_of__pressed_5() { return &____pressed_5; }
	inline void set__pressed_5(bool value)
	{
		____pressed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
