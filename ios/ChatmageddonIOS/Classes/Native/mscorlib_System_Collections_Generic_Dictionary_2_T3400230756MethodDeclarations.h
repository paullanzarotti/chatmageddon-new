﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,Vectrosity.Vector3Pair>
struct Transform_1_t3400230756;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,Vectrosity.Vector3Pair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3941911292_gshared (Transform_1_t3400230756 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3941911292(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3400230756 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3941911292_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,Vectrosity.Vector3Pair>::Invoke(TKey,TValue)
extern "C"  Vector3Pair_t2859078138  Transform_1_Invoke_m1625573284_gshared (Transform_1_t3400230756 * __this, Vector3Pair_t2859078138  ___key0, bool ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1625573284(__this, ___key0, ___value1, method) ((  Vector3Pair_t2859078138  (*) (Transform_1_t3400230756 *, Vector3Pair_t2859078138 , bool, const MethodInfo*))Transform_1_Invoke_m1625573284_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,Vectrosity.Vector3Pair>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3563367569_gshared (Transform_1_t3400230756 * __this, Vector3Pair_t2859078138  ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m3563367569(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3400230756 *, Vector3Pair_t2859078138 , bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3563367569_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,Vectrosity.Vector3Pair>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3Pair_t2859078138  Transform_1_EndInvoke_m867339886_gshared (Transform_1_t3400230756 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m867339886(__this, ___result0, method) ((  Vector3Pair_t2859078138  (*) (Transform_1_t3400230756 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m867339886_gshared)(__this, ___result0, method)
