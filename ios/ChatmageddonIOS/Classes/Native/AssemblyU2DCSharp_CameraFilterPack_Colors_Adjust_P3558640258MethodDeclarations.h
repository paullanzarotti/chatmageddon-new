﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_Adjust_PreFilters
struct CameraFilterPack_Colors_Adjust_PreFilters_t3558640258;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_Adjust_PreFilters::.ctor()
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters__ctor_m2412758739 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Adjust_PreFilters::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_Adjust_PreFilters_get_material_m2779659298 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::ChangeFilters()
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters_ChangeFilters_m2715209726 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::Start()
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters_Start_m1089702087 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters_OnRenderImage_m2130752671 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::OnValidate()
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters_OnValidate_m3536648258 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::Update()
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters_Update_m166631888 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::OnDisable()
extern "C"  void CameraFilterPack_Colors_Adjust_PreFilters_OnDisable_m216425142 (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
