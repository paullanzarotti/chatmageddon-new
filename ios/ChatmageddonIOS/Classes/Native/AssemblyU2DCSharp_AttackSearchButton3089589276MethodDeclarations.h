﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchButton
struct AttackSearchButton_t3089589276;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackSearchButton::.ctor()
extern "C"  void AttackSearchButton__ctor_m900798723 (AttackSearchButton_t3089589276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchButton::OnButtonClick()
extern "C"  void AttackSearchButton_OnButtonClick_m2189414736 (AttackSearchButton_t3089589276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
