﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.KeyValueConfigurationElement
struct KeyValueConfigurationElement_t1726605464;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.KeyValueConfigurationElement::.ctor()
extern "C"  void KeyValueConfigurationElement__ctor_m3799979 (KeyValueConfigurationElement_t1726605464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationElement::.ctor(System.String,System.String)
extern "C"  void KeyValueConfigurationElement__ctor_m2050175099 (KeyValueConfigurationElement_t1726605464 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationElement::.cctor()
extern "C"  void KeyValueConfigurationElement__cctor_m2043889340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.KeyValueConfigurationElement::get_Key()
extern "C"  String_t* KeyValueConfigurationElement_get_Key_m2947092008 (KeyValueConfigurationElement_t1726605464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.KeyValueConfigurationElement::get_Value()
extern "C"  String_t* KeyValueConfigurationElement_get_Value_m2142438890 (KeyValueConfigurationElement_t1726605464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationElement::set_Value(System.String)
extern "C"  void KeyValueConfigurationElement_set_Value_m2353018263 (KeyValueConfigurationElement_t1726605464 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationElement::Init()
extern "C"  void KeyValueConfigurationElement_Init_m1051596769 (KeyValueConfigurationElement_t1726605464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.KeyValueConfigurationElement::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * KeyValueConfigurationElement_get_Properties_m146133254 (KeyValueConfigurationElement_t1726605464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
