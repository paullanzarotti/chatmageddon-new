﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YourNumberUIScaler
struct YourNumberUIScaler_t1339804644;

#include "codegen/il2cpp-codegen.h"

// System.Void YourNumberUIScaler::.ctor()
extern "C"  void YourNumberUIScaler__ctor_m2610499143 (YourNumberUIScaler_t1339804644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YourNumberUIScaler::GlobalUIScale()
extern "C"  void YourNumberUIScaler_GlobalUIScale_m3177150830 (YourNumberUIScaler_t1339804644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
