﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ChatServer_1_gen1153901193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonServer
struct  ChatmageddonServer_t594474938  : public ChatServer_1_t1153901193
{
public:
	// System.String ChatmageddonServer::baseWebsiteURL
	String_t* ___baseWebsiteURL_9;

public:
	inline static int32_t get_offset_of_baseWebsiteURL_9() { return static_cast<int32_t>(offsetof(ChatmageddonServer_t594474938, ___baseWebsiteURL_9)); }
	inline String_t* get_baseWebsiteURL_9() const { return ___baseWebsiteURL_9; }
	inline String_t** get_address_of_baseWebsiteURL_9() { return &___baseWebsiteURL_9; }
	inline void set_baseWebsiteURL_9(String_t* value)
	{
		___baseWebsiteURL_9 = value;
		Il2CppCodeGenWriteBarrier(&___baseWebsiteURL_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
