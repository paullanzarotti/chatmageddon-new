﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackHomeUIScaler
struct  AttackHomeUIScaler_t339922071  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite AttackHomeUIScaler::tabBackground
	UISprite_t603616735 * ___tabBackground_14;
	// UISprite AttackHomeUIScaler::bottomSeperator
	UISprite_t603616735 * ___bottomSeperator_15;
	// UnityEngine.Transform AttackHomeUIScaler::innerTab
	Transform_t3275118058 * ___innerTab_16;
	// UnityEngine.BoxCollider AttackHomeUIScaler::innerTabCollider
	BoxCollider_t22920061 * ___innerTabCollider_17;
	// UnityEngine.Transform AttackHomeUIScaler::outerTab
	Transform_t3275118058 * ___outerTab_18;
	// UnityEngine.BoxCollider AttackHomeUIScaler::outerTabCollider
	BoxCollider_t22920061 * ___outerTabCollider_19;
	// UnityEngine.Transform AttackHomeUIScaler::otherTab
	Transform_t3275118058 * ___otherTab_20;
	// UnityEngine.BoxCollider AttackHomeUIScaler::otherTabCollider
	BoxCollider_t22920061 * ___otherTabCollider_21;
	// UnityEngine.Transform AttackHomeUIScaler::leftSeperator
	Transform_t3275118058 * ___leftSeperator_22;
	// UnityEngine.Transform AttackHomeUIScaler::rightSeperator
	Transform_t3275118058 * ___rightSeperator_23;
	// UIPanel AttackHomeUIScaler::innerPanel
	UIPanel_t1795085332 * ___innerPanel_24;
	// UIPanel AttackHomeUIScaler::outerPanel
	UIPanel_t1795085332 * ___outerPanel_25;
	// UIPanel AttackHomeUIScaler::otherPanel
	UIPanel_t1795085332 * ___otherPanel_26;
	// UISprite AttackHomeUIScaler::otherBonusBackground
	UISprite_t603616735 * ___otherBonusBackground_27;
	// UISprite AttackHomeUIScaler::multiNumberBackground
	UISprite_t603616735 * ___multiNumberBackground_28;
	// UnityEngine.BoxCollider AttackHomeUIScaler::multiNumberCollider
	BoxCollider_t22920061 * ___multiNumberCollider_29;

public:
	inline static int32_t get_offset_of_tabBackground_14() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___tabBackground_14)); }
	inline UISprite_t603616735 * get_tabBackground_14() const { return ___tabBackground_14; }
	inline UISprite_t603616735 ** get_address_of_tabBackground_14() { return &___tabBackground_14; }
	inline void set_tabBackground_14(UISprite_t603616735 * value)
	{
		___tabBackground_14 = value;
		Il2CppCodeGenWriteBarrier(&___tabBackground_14, value);
	}

	inline static int32_t get_offset_of_bottomSeperator_15() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___bottomSeperator_15)); }
	inline UISprite_t603616735 * get_bottomSeperator_15() const { return ___bottomSeperator_15; }
	inline UISprite_t603616735 ** get_address_of_bottomSeperator_15() { return &___bottomSeperator_15; }
	inline void set_bottomSeperator_15(UISprite_t603616735 * value)
	{
		___bottomSeperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSeperator_15, value);
	}

	inline static int32_t get_offset_of_innerTab_16() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___innerTab_16)); }
	inline Transform_t3275118058 * get_innerTab_16() const { return ___innerTab_16; }
	inline Transform_t3275118058 ** get_address_of_innerTab_16() { return &___innerTab_16; }
	inline void set_innerTab_16(Transform_t3275118058 * value)
	{
		___innerTab_16 = value;
		Il2CppCodeGenWriteBarrier(&___innerTab_16, value);
	}

	inline static int32_t get_offset_of_innerTabCollider_17() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___innerTabCollider_17)); }
	inline BoxCollider_t22920061 * get_innerTabCollider_17() const { return ___innerTabCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_innerTabCollider_17() { return &___innerTabCollider_17; }
	inline void set_innerTabCollider_17(BoxCollider_t22920061 * value)
	{
		___innerTabCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___innerTabCollider_17, value);
	}

	inline static int32_t get_offset_of_outerTab_18() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___outerTab_18)); }
	inline Transform_t3275118058 * get_outerTab_18() const { return ___outerTab_18; }
	inline Transform_t3275118058 ** get_address_of_outerTab_18() { return &___outerTab_18; }
	inline void set_outerTab_18(Transform_t3275118058 * value)
	{
		___outerTab_18 = value;
		Il2CppCodeGenWriteBarrier(&___outerTab_18, value);
	}

	inline static int32_t get_offset_of_outerTabCollider_19() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___outerTabCollider_19)); }
	inline BoxCollider_t22920061 * get_outerTabCollider_19() const { return ___outerTabCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_outerTabCollider_19() { return &___outerTabCollider_19; }
	inline void set_outerTabCollider_19(BoxCollider_t22920061 * value)
	{
		___outerTabCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___outerTabCollider_19, value);
	}

	inline static int32_t get_offset_of_otherTab_20() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___otherTab_20)); }
	inline Transform_t3275118058 * get_otherTab_20() const { return ___otherTab_20; }
	inline Transform_t3275118058 ** get_address_of_otherTab_20() { return &___otherTab_20; }
	inline void set_otherTab_20(Transform_t3275118058 * value)
	{
		___otherTab_20 = value;
		Il2CppCodeGenWriteBarrier(&___otherTab_20, value);
	}

	inline static int32_t get_offset_of_otherTabCollider_21() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___otherTabCollider_21)); }
	inline BoxCollider_t22920061 * get_otherTabCollider_21() const { return ___otherTabCollider_21; }
	inline BoxCollider_t22920061 ** get_address_of_otherTabCollider_21() { return &___otherTabCollider_21; }
	inline void set_otherTabCollider_21(BoxCollider_t22920061 * value)
	{
		___otherTabCollider_21 = value;
		Il2CppCodeGenWriteBarrier(&___otherTabCollider_21, value);
	}

	inline static int32_t get_offset_of_leftSeperator_22() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___leftSeperator_22)); }
	inline Transform_t3275118058 * get_leftSeperator_22() const { return ___leftSeperator_22; }
	inline Transform_t3275118058 ** get_address_of_leftSeperator_22() { return &___leftSeperator_22; }
	inline void set_leftSeperator_22(Transform_t3275118058 * value)
	{
		___leftSeperator_22 = value;
		Il2CppCodeGenWriteBarrier(&___leftSeperator_22, value);
	}

	inline static int32_t get_offset_of_rightSeperator_23() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___rightSeperator_23)); }
	inline Transform_t3275118058 * get_rightSeperator_23() const { return ___rightSeperator_23; }
	inline Transform_t3275118058 ** get_address_of_rightSeperator_23() { return &___rightSeperator_23; }
	inline void set_rightSeperator_23(Transform_t3275118058 * value)
	{
		___rightSeperator_23 = value;
		Il2CppCodeGenWriteBarrier(&___rightSeperator_23, value);
	}

	inline static int32_t get_offset_of_innerPanel_24() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___innerPanel_24)); }
	inline UIPanel_t1795085332 * get_innerPanel_24() const { return ___innerPanel_24; }
	inline UIPanel_t1795085332 ** get_address_of_innerPanel_24() { return &___innerPanel_24; }
	inline void set_innerPanel_24(UIPanel_t1795085332 * value)
	{
		___innerPanel_24 = value;
		Il2CppCodeGenWriteBarrier(&___innerPanel_24, value);
	}

	inline static int32_t get_offset_of_outerPanel_25() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___outerPanel_25)); }
	inline UIPanel_t1795085332 * get_outerPanel_25() const { return ___outerPanel_25; }
	inline UIPanel_t1795085332 ** get_address_of_outerPanel_25() { return &___outerPanel_25; }
	inline void set_outerPanel_25(UIPanel_t1795085332 * value)
	{
		___outerPanel_25 = value;
		Il2CppCodeGenWriteBarrier(&___outerPanel_25, value);
	}

	inline static int32_t get_offset_of_otherPanel_26() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___otherPanel_26)); }
	inline UIPanel_t1795085332 * get_otherPanel_26() const { return ___otherPanel_26; }
	inline UIPanel_t1795085332 ** get_address_of_otherPanel_26() { return &___otherPanel_26; }
	inline void set_otherPanel_26(UIPanel_t1795085332 * value)
	{
		___otherPanel_26 = value;
		Il2CppCodeGenWriteBarrier(&___otherPanel_26, value);
	}

	inline static int32_t get_offset_of_otherBonusBackground_27() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___otherBonusBackground_27)); }
	inline UISprite_t603616735 * get_otherBonusBackground_27() const { return ___otherBonusBackground_27; }
	inline UISprite_t603616735 ** get_address_of_otherBonusBackground_27() { return &___otherBonusBackground_27; }
	inline void set_otherBonusBackground_27(UISprite_t603616735 * value)
	{
		___otherBonusBackground_27 = value;
		Il2CppCodeGenWriteBarrier(&___otherBonusBackground_27, value);
	}

	inline static int32_t get_offset_of_multiNumberBackground_28() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___multiNumberBackground_28)); }
	inline UISprite_t603616735 * get_multiNumberBackground_28() const { return ___multiNumberBackground_28; }
	inline UISprite_t603616735 ** get_address_of_multiNumberBackground_28() { return &___multiNumberBackground_28; }
	inline void set_multiNumberBackground_28(UISprite_t603616735 * value)
	{
		___multiNumberBackground_28 = value;
		Il2CppCodeGenWriteBarrier(&___multiNumberBackground_28, value);
	}

	inline static int32_t get_offset_of_multiNumberCollider_29() { return static_cast<int32_t>(offsetof(AttackHomeUIScaler_t339922071, ___multiNumberCollider_29)); }
	inline BoxCollider_t22920061 * get_multiNumberCollider_29() const { return ___multiNumberCollider_29; }
	inline BoxCollider_t22920061 ** get_address_of_multiNumberCollider_29() { return &___multiNumberCollider_29; }
	inline void set_multiNumberCollider_29(BoxCollider_t22920061 * value)
	{
		___multiNumberCollider_29 = value;
		Il2CppCodeGenWriteBarrier(&___multiNumberCollider_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
