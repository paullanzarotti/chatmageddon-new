﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2262123388.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardSceneManager
struct  LeaderboardSceneManager_t2883297036  : public BaseSceneManager_1_t2262123388
{
public:
	// UnityEngine.GameObject LeaderboardSceneManager::leaderboardCenterUI
	GameObject_t1756533147 * ___leaderboardCenterUI_17;

public:
	inline static int32_t get_offset_of_leaderboardCenterUI_17() { return static_cast<int32_t>(offsetof(LeaderboardSceneManager_t2883297036, ___leaderboardCenterUI_17)); }
	inline GameObject_t1756533147 * get_leaderboardCenterUI_17() const { return ___leaderboardCenterUI_17; }
	inline GameObject_t1756533147 ** get_address_of_leaderboardCenterUI_17() { return &___leaderboardCenterUI_17; }
	inline void set_leaderboardCenterUI_17(GameObject_t1756533147 * value)
	{
		___leaderboardCenterUI_17 = value;
		Il2CppCodeGenWriteBarrier(&___leaderboardCenterUI_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
