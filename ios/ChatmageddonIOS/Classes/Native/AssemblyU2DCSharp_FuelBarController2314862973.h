﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_CircularProgressBar2980095063.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuelBarController
struct  FuelBarController_t2314862973  : public CircularProgressBar_t2980095063
{
public:
	// UnityEngine.Color FuelBarController::activeFueldColour
	Color_t2020392075  ___activeFueldColour_13;
	// UnityEngine.Color FuelBarController::inactiveFueldColour
	Color_t2020392075  ___inactiveFueldColour_14;
	// UISprite FuelBarController::fuelIcon
	UISprite_t603616735 * ___fuelIcon_15;

public:
	inline static int32_t get_offset_of_activeFueldColour_13() { return static_cast<int32_t>(offsetof(FuelBarController_t2314862973, ___activeFueldColour_13)); }
	inline Color_t2020392075  get_activeFueldColour_13() const { return ___activeFueldColour_13; }
	inline Color_t2020392075 * get_address_of_activeFueldColour_13() { return &___activeFueldColour_13; }
	inline void set_activeFueldColour_13(Color_t2020392075  value)
	{
		___activeFueldColour_13 = value;
	}

	inline static int32_t get_offset_of_inactiveFueldColour_14() { return static_cast<int32_t>(offsetof(FuelBarController_t2314862973, ___inactiveFueldColour_14)); }
	inline Color_t2020392075  get_inactiveFueldColour_14() const { return ___inactiveFueldColour_14; }
	inline Color_t2020392075 * get_address_of_inactiveFueldColour_14() { return &___inactiveFueldColour_14; }
	inline void set_inactiveFueldColour_14(Color_t2020392075  value)
	{
		___inactiveFueldColour_14 = value;
	}

	inline static int32_t get_offset_of_fuelIcon_15() { return static_cast<int32_t>(offsetof(FuelBarController_t2314862973, ___fuelIcon_15)); }
	inline UISprite_t603616735 * get_fuelIcon_15() const { return ___fuelIcon_15; }
	inline UISprite_t603616735 ** get_address_of_fuelIcon_15() { return &___fuelIcon_15; }
	inline void set_fuelIcon_15(UISprite_t603616735 * value)
	{
		___fuelIcon_15 = value;
		Il2CppCodeGenWriteBarrier(&___fuelIcon_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
