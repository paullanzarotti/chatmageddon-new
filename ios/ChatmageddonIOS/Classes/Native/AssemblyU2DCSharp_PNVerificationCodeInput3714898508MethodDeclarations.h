﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PNVerificationCodeInput
struct PNVerificationCodeInput_t3714898508;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PNVerificationCodeInput::.ctor()
extern "C"  void PNVerificationCodeInput__ctor_m455195507 (PNVerificationCodeInput_t3714898508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationCodeInput::InputSubmit()
extern "C"  void PNVerificationCodeInput_InputSubmit_m2318560881 (PNVerificationCodeInput_t3714898508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PNVerificationCodeInput::ValidateInput()
extern "C"  bool PNVerificationCodeInput_ValidateInput_m4037739995 (PNVerificationCodeInput_t3714898508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationCodeInput::<InputSubmit>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void PNVerificationCodeInput_U3CInputSubmitU3Em__0_m3068931763 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationCodeInput::<InputSubmit>m__1(System.Boolean,System.String)
extern "C"  void PNVerificationCodeInput_U3CInputSubmitU3Em__1_m2940045348 (Il2CppObject * __this /* static, unused */, bool ___submitSuccess0, String_t* ___submitErrorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
