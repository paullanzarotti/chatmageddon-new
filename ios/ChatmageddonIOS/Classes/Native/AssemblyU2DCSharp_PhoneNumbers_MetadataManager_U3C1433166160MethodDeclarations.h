﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.MetadataManager/<LoadMedataFromFile>c__AnonStorey0
struct U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.MetadataManager/<LoadMedataFromFile>c__AnonStorey0::.ctor()
extern "C"  void U3CLoadMedataFromFileU3Ec__AnonStorey0__ctor_m3675158115 (U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.MetadataManager/<LoadMedataFromFile>c__AnonStorey0::<>m__0(System.String)
extern "C"  bool U3CLoadMedataFromFileU3Ec__AnonStorey0_U3CU3Em__0_m1273980526 (U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160 * __this, String_t* ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
