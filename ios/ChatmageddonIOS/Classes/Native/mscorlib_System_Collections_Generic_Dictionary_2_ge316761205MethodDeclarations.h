﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>
struct Dictionary_2_t316761205;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.IDictionary`2<System.Object,System.Decimal>
struct IDictionary_2_t2610811922;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.ICollection`1<System.Decimal>
struct ICollection_1_t1676776382;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>[]
struct KeyValuePair_2U5BU5D_t4159262330;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>
struct IEnumerator_1_t4139564846;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Decimal>
struct KeyCollection_t2800258976;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>
struct ValueCollection_t3314788344;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22369073723.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1636785907.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor()
extern "C"  void Dictionary_2__ctor_m2759464733_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2759464733(__this, method) ((  void (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2__ctor_m2759464733_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m923697374_gshared (Dictionary_2_t316761205 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m923697374(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m923697374_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2887894187_gshared (Dictionary_2_t316761205 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2887894187(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2887894187_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2220553478_gshared (Dictionary_2_t316761205 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2220553478(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t316761205 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2220553478_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1149158454_gshared (Dictionary_2_t316761205 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1149158454(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1149158454_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2869244751_gshared (Dictionary_2_t316761205 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2869244751(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t316761205 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2869244751_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1583885120_gshared (Dictionary_2_t316761205 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1583885120(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t316761205 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1583885120_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2896567357_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2896567357(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2896567357_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2686950429_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2686950429(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2686950429_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m421399799_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m421399799(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m421399799_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3170236311_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3170236311(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3170236311_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2647384014_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2647384014(__this, method) ((  bool (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2647384014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2479435419_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2479435419(__this, method) ((  bool (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2479435419_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1637156521_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1637156521(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1637156521_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3087816872_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3087816872(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3087816872_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m4072985907_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m4072985907(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m4072985907_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3901206887_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3901206887(__this, ___key0, method) ((  bool (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3901206887_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3879137228_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3879137228(__this, ___key0, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3879137228_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3023918649_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3023918649(__this, method) ((  bool (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3023918649_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m46918937_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m46918937(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m46918937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1273542143_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1273542143(__this, method) ((  bool (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1273542143_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m645117302_gshared (Dictionary_2_t316761205 * __this, KeyValuePair_2_t2369073723  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m645117302(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t316761205 *, KeyValuePair_2_t2369073723 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m645117302_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1860612406_gshared (Dictionary_2_t316761205 * __this, KeyValuePair_2_t2369073723  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1860612406(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t316761205 *, KeyValuePair_2_t2369073723 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1860612406_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2699896882_gshared (Dictionary_2_t316761205 * __this, KeyValuePair_2U5BU5D_t4159262330* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2699896882(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t316761205 *, KeyValuePair_2U5BU5D_t4159262330*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2699896882_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2027546701_gshared (Dictionary_2_t316761205 * __this, KeyValuePair_2_t2369073723  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2027546701(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t316761205 *, KeyValuePair_2_t2369073723 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2027546701_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1502931861_gshared (Dictionary_2_t316761205 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1502931861(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1502931861_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1833396484_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1833396484(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1833396484_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2329442723_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2329442723(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2329442723_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1656732838_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1656732838(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1656732838_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1775206613_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1775206613(__this, method) ((  int32_t (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_get_Count_m1775206613_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::get_Item(TKey)
extern "C"  Decimal_t724701077  Dictionary_2_get_Item_m2328390502_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2328390502(__this, ___key0, method) ((  Decimal_t724701077  (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m2328390502_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1049856837_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1049856837(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_set_Item_m1049856837_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3250346313_gshared (Dictionary_2_t316761205 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3250346313(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t316761205 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3250346313_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2552625486_gshared (Dictionary_2_t316761205 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2552625486(__this, ___size0, method) ((  void (*) (Dictionary_2_t316761205 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2552625486_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m672407892_gshared (Dictionary_2_t316761205 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m672407892(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m672407892_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2369073723  Dictionary_2_make_pair_m819321950_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m819321950(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2369073723  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_make_pair_m819321950_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m1913055992_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1913055992(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_pick_key_m1913055992_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::pick_value(TKey,TValue)
extern "C"  Decimal_t724701077  Dictionary_2_pick_value_m1437533240_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1437533240(__this /* static, unused */, ___key0, ___value1, method) ((  Decimal_t724701077  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_pick_value_m1437533240_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3463468257_gshared (Dictionary_2_t316761205 * __this, KeyValuePair_2U5BU5D_t4159262330* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3463468257(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t316761205 *, KeyValuePair_2U5BU5D_t4159262330*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3463468257_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::Resize()
extern "C"  void Dictionary_2_Resize_m826332635_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m826332635(__this, method) ((  void (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_Resize_m826332635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1207855746_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1207855746(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_Add_m1207855746_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::Clear()
extern "C"  void Dictionary_2_Clear_m2676873214_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2676873214(__this, method) ((  void (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_Clear_m2676873214_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4127506526_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4127506526(__this, ___key0, method) ((  bool (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m4127506526_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3923871886_gshared (Dictionary_2_t316761205 * __this, Decimal_t724701077  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3923871886(__this, ___value0, method) ((  bool (*) (Dictionary_2_t316761205 *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_ContainsValue_m3923871886_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2201090257_gshared (Dictionary_2_t316761205 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2201090257(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t316761205 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2201090257_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m862686645_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m862686645(__this, ___sender0, method) ((  void (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m862686645_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3297960414_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3297960414(__this, ___key0, method) ((  bool (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m3297960414_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m61035629_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, Decimal_t724701077 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m61035629(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t316761205 *, Il2CppObject *, Decimal_t724701077 *, const MethodInfo*))Dictionary_2_TryGetValue_m61035629_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::get_Keys()
extern "C"  KeyCollection_t2800258976 * Dictionary_2_get_Keys_m934019522_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m934019522(__this, method) ((  KeyCollection_t2800258976 * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_get_Keys_m934019522_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::get_Values()
extern "C"  ValueCollection_t3314788344 * Dictionary_2_get_Values_m2948984802_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2948984802(__this, method) ((  ValueCollection_t3314788344 * (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_get_Values_m2948984802_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m398850019_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m398850019(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m398850019_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::ToTValue(System.Object)
extern "C"  Decimal_t724701077  Dictionary_2_ToTValue_m389822979_gshared (Dictionary_2_t316761205 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m389822979(__this, ___value0, method) ((  Decimal_t724701077  (*) (Dictionary_2_t316761205 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m389822979_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1248885077_gshared (Dictionary_2_t316761205 * __this, KeyValuePair_2_t2369073723  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1248885077(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t316761205 *, KeyValuePair_2_t2369073723 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1248885077_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::GetEnumerator()
extern "C"  Enumerator_t1636785907  Dictionary_2_GetEnumerator_m40461320_gshared (Dictionary_2_t316761205 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m40461320(__this, method) ((  Enumerator_t1636785907  (*) (Dictionary_2_t316761205 *, const MethodInfo*))Dictionary_2_GetEnumerator_m40461320_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__2_m1074683845_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m1074683845(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m1074683845_gshared)(__this /* static, unused */, ___key0, ___value1, method)
