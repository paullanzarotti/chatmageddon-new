﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineInfoController
struct MineInfoController_t2990208339;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"

// System.Void MineInfoController::.ctor()
extern "C"  void MineInfoController__ctor_m714339650 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController::Awake()
extern "C"  void MineInfoController_Awake_m1340938047 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController::PopulateInfoUI()
extern "C"  void MineInfoController_PopulateInfoUI_m844764080 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MineInfoController::WaitForSeconds(System.Single)
extern "C"  Il2CppObject * MineInfoController_WaitForSeconds_m1525594518 (MineInfoController_t2990208339 * __this, float ___amountToWait0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController::UpdateModel()
extern "C"  void MineInfoController_UpdateModel_m1838156528 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject MineInfoController::GetItemModel()
extern "C"  GameObject_t1756533147 * MineInfoController_GetItemModel_m3639476537 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController::CloseInfoModal()
extern "C"  void MineInfoController_CloseInfoModal_m378027417 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController::OnPosTweenFinished()
extern "C"  void MineInfoController_OnPosTweenFinished_m1446320898 (MineInfoController_t2990208339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
