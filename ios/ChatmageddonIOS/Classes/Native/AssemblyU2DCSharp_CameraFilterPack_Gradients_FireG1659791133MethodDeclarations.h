﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_FireGradient
struct CameraFilterPack_Gradients_FireGradient_t1659791133;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_FireGradient::.ctor()
extern "C"  void CameraFilterPack_Gradients_FireGradient__ctor_m4183137198 (CameraFilterPack_Gradients_FireGradient_t1659791133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_FireGradient::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_FireGradient_get_material_m2798764613 (CameraFilterPack_Gradients_FireGradient_t1659791133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::Start()
extern "C"  void CameraFilterPack_Gradients_FireGradient_Start_m3080309710 (CameraFilterPack_Gradients_FireGradient_t1659791133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_FireGradient_OnRenderImage_m1028151174 (CameraFilterPack_Gradients_FireGradient_t1659791133 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::Update()
extern "C"  void CameraFilterPack_Gradients_FireGradient_Update_m2774196809 (CameraFilterPack_Gradients_FireGradient_t1659791133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::OnDisable()
extern "C"  void CameraFilterPack_Gradients_FireGradient_OnDisable_m2691076765 (CameraFilterPack_Gradients_FireGradient_t1659791133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
