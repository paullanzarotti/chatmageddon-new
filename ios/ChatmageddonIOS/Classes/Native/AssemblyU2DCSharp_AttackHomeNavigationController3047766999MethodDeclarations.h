﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeNavigationController
struct AttackHomeNavigationController_t3047766999;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackHomeNavigationController::.ctor()
extern "C"  void AttackHomeNavigationController__ctor_m2741542844 (AttackHomeNavigationController_t3047766999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigationController::Start()
extern "C"  void AttackHomeNavigationController_Start_m3492640056 (AttackHomeNavigationController_t3047766999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigationController::NavigateBackwards()
extern "C"  void AttackHomeNavigationController_NavigateBackwards_m1810106009 (AttackHomeNavigationController_t3047766999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigationController::NavigateForwards(AttackHomeNav)
extern "C"  void AttackHomeNavigationController_NavigateForwards_m535470887 (AttackHomeNavigationController_t3047766999 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackHomeNavigationController::ValidateNavigation(AttackHomeNav,NavigationDirection)
extern "C"  bool AttackHomeNavigationController_ValidateNavigation_m1112497251 (AttackHomeNavigationController_t3047766999 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigationController::LoadNavScreen(AttackHomeNav,NavigationDirection,System.Boolean)
extern "C"  void AttackHomeNavigationController_LoadNavScreen_m601576691 (AttackHomeNavigationController_t3047766999 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigationController::UnloadNavScreen(AttackHomeNav,NavigationDirection)
extern "C"  void AttackHomeNavigationController_UnloadNavScreen_m1996090423 (AttackHomeNavigationController_t3047766999 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigationController::PopulateTitleBar(AttackHomeNav)
extern "C"  void AttackHomeNavigationController_PopulateTitleBar_m2340449663 (AttackHomeNavigationController_t3047766999 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
