﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_NewCellShading
struct CameraFilterPack_Drawing_NewCellShading_t1609771822;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_NewCellShading::.ctor()
extern "C"  void CameraFilterPack_Drawing_NewCellShading__ctor_m3328290823 (CameraFilterPack_Drawing_NewCellShading_t1609771822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_NewCellShading::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_NewCellShading_get_material_m1013061838 (CameraFilterPack_Drawing_NewCellShading_t1609771822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::Start()
extern "C"  void CameraFilterPack_Drawing_NewCellShading_Start_m2916443315 (CameraFilterPack_Drawing_NewCellShading_t1609771822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_NewCellShading_OnRenderImage_m2669037507 (CameraFilterPack_Drawing_NewCellShading_t1609771822 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::Update()
extern "C"  void CameraFilterPack_Drawing_NewCellShading_Update_m31582620 (CameraFilterPack_Drawing_NewCellShading_t1609771822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::OnDisable()
extern "C"  void CameraFilterPack_Drawing_NewCellShading_OnDisable_m3930444690 (CameraFilterPack_Drawing_NewCellShading_t1609771822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
