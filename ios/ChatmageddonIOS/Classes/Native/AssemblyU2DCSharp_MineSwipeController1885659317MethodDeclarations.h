﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineSwipeController
struct MineSwipeController_t1885659317;

#include "codegen/il2cpp-codegen.h"

// System.Void MineSwipeController::.ctor()
extern "C"  void MineSwipeController__ctor_m3567586690 (MineSwipeController_t1885659317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineSwipeController::DisplayItemModel()
extern "C"  void MineSwipeController_DisplayItemModel_m423357478 (MineSwipeController_t1885659317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
