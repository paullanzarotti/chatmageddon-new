﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameState_1_gen846809420MethodDeclarations.h"

// System.Void GameState`1<ChatmageddonGamestate>::.ctor()
#define GameState_1__ctor_m2086363017(__this, method) ((  void (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1__ctor_m299346719_gshared)(__this, method)
// System.Void GameState`1<ChatmageddonGamestate>::Init()
#define GameState_1_Init_m4116118371(__this, method) ((  void (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_Init_m290996569_gshared)(__this, method)
// System.Void GameState`1<ChatmageddonGamestate>::DontDestroyOnLoad()
#define GameState_1_DontDestroyOnLoad_m4241968593(__this, method) ((  void (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_DontDestroyOnLoad_m4221039203_gshared)(__this, method)
// System.Void GameState`1<ChatmageddonGamestate>::LoadScene(System.String,System.Boolean,System.Boolean)
#define GameState_1_LoadScene_m3018880911(__this, ___sceneName0, ___aSync1, ___garbageCollect2, method) ((  void (*) (GameState_1_t3261017281 *, String_t*, bool, bool, const MethodInfo*))GameState_1_LoadScene_m1399394461_gshared)(__this, ___sceneName0, ___aSync1, ___garbageCollect2, method)
// System.Void GameState`1<ChatmageddonGamestate>::StartActiveScene()
#define GameState_1_StartActiveScene_m3229203027(__this, method) ((  void (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_StartActiveScene_m2386984001_gshared)(__this, method)
// System.Void GameState`1<ChatmageddonGamestate>::LoadLevel(System.String)
#define GameState_1_LoadLevel_m1423978015(__this, ___level0, method) ((  void (*) (GameState_1_t3261017281 *, String_t*, const MethodInfo*))GameState_1_LoadLevel_m1307252913_gshared)(__this, ___level0, method)
// System.Void GameState`1<ChatmageddonGamestate>::PreLevelLoad()
#define GameState_1_PreLevelLoad_m3935789028(__this, method) ((  void (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_PreLevelLoad_m3232301818_gshared)(__this, method)
// System.Void GameState`1<ChatmageddonGamestate>::LoadLevelAsync(System.String,System.Boolean)
#define GameState_1_LoadLevelAsync_m690006442(__this, ___level0, ___garbageCollect1, method) ((  void (*) (GameState_1_t3261017281 *, String_t*, bool, const MethodInfo*))GameState_1_LoadLevelAsync_m731125564_gshared)(__this, ___level0, ___garbageCollect1, method)
// System.Void GameState`1<ChatmageddonGamestate>::LoadState(System.String)
#define GameState_1_LoadState_m1080431230(__this, ___nextState0, method) ((  void (*) (GameState_1_t3261017281 *, String_t*, const MethodInfo*))GameState_1_LoadState_m3432771088_gshared)(__this, ___nextState0, method)
// System.Void GameState`1<ChatmageddonGamestate>::SetPreviousState()
#define GameState_1_SetPreviousState_m3673942693(__this, method) ((  void (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_SetPreviousState_m485008655_gshared)(__this, method)
// System.Void GameState`1<ChatmageddonGamestate>::SetNextState(System.String)
#define GameState_1_SetNextState_m896754013(__this, ___nextState0, method) ((  void (*) (GameState_1_t3261017281 *, String_t*, const MethodInfo*))GameState_1_SetNextState_m1387511123_gshared)(__this, ___nextState0, method)
// System.String GameState`1<ChatmageddonGamestate>::getActiveScene()
#define GameState_1_getActiveScene_m3685571322(__this, method) ((  String_t* (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_getActiveScene_m4069071568_gshared)(__this, method)
// System.String GameState`1<ChatmageddonGamestate>::getPreviousScene()
#define GameState_1_getPreviousScene_m2885762497(__this, method) ((  String_t* (*) (GameState_1_t3261017281 *, const MethodInfo*))GameState_1_getPreviousScene_m1851301131_gshared)(__this, method)
