﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// NoPointsWaitButton/OnZeroHit
struct OnZeroHit_t3971658986;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoPointsWaitButton
struct  NoPointsWaitButton_t1249750581  : public SFXButton_t792651341
{
public:
	// UILabel NoPointsWaitButton::timeLabel
	UILabel_t1795115428 * ___timeLabel_5;
	// UnityEngine.GameObject NoPointsWaitButton::waitTimer
	GameObject_t1756533147 * ___waitTimer_6;
	// UnityEngine.GameObject NoPointsWaitButton::exitButton
	GameObject_t1756533147 * ___exitButton_7;
	// System.Boolean NoPointsWaitButton::allowExit
	bool ___allowExit_8;
	// UnityEngine.Coroutine NoPointsWaitButton::calcRoutine
	Coroutine_t2299508840 * ___calcRoutine_9;
	// NoPointsWaitButton/OnZeroHit NoPointsWaitButton::onZeroHit
	OnZeroHit_t3971658986 * ___onZeroHit_10;

public:
	inline static int32_t get_offset_of_timeLabel_5() { return static_cast<int32_t>(offsetof(NoPointsWaitButton_t1249750581, ___timeLabel_5)); }
	inline UILabel_t1795115428 * get_timeLabel_5() const { return ___timeLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_5() { return &___timeLabel_5; }
	inline void set_timeLabel_5(UILabel_t1795115428 * value)
	{
		___timeLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_5, value);
	}

	inline static int32_t get_offset_of_waitTimer_6() { return static_cast<int32_t>(offsetof(NoPointsWaitButton_t1249750581, ___waitTimer_6)); }
	inline GameObject_t1756533147 * get_waitTimer_6() const { return ___waitTimer_6; }
	inline GameObject_t1756533147 ** get_address_of_waitTimer_6() { return &___waitTimer_6; }
	inline void set_waitTimer_6(GameObject_t1756533147 * value)
	{
		___waitTimer_6 = value;
		Il2CppCodeGenWriteBarrier(&___waitTimer_6, value);
	}

	inline static int32_t get_offset_of_exitButton_7() { return static_cast<int32_t>(offsetof(NoPointsWaitButton_t1249750581, ___exitButton_7)); }
	inline GameObject_t1756533147 * get_exitButton_7() const { return ___exitButton_7; }
	inline GameObject_t1756533147 ** get_address_of_exitButton_7() { return &___exitButton_7; }
	inline void set_exitButton_7(GameObject_t1756533147 * value)
	{
		___exitButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___exitButton_7, value);
	}

	inline static int32_t get_offset_of_allowExit_8() { return static_cast<int32_t>(offsetof(NoPointsWaitButton_t1249750581, ___allowExit_8)); }
	inline bool get_allowExit_8() const { return ___allowExit_8; }
	inline bool* get_address_of_allowExit_8() { return &___allowExit_8; }
	inline void set_allowExit_8(bool value)
	{
		___allowExit_8 = value;
	}

	inline static int32_t get_offset_of_calcRoutine_9() { return static_cast<int32_t>(offsetof(NoPointsWaitButton_t1249750581, ___calcRoutine_9)); }
	inline Coroutine_t2299508840 * get_calcRoutine_9() const { return ___calcRoutine_9; }
	inline Coroutine_t2299508840 ** get_address_of_calcRoutine_9() { return &___calcRoutine_9; }
	inline void set_calcRoutine_9(Coroutine_t2299508840 * value)
	{
		___calcRoutine_9 = value;
		Il2CppCodeGenWriteBarrier(&___calcRoutine_9, value);
	}

	inline static int32_t get_offset_of_onZeroHit_10() { return static_cast<int32_t>(offsetof(NoPointsWaitButton_t1249750581, ___onZeroHit_10)); }
	inline OnZeroHit_t3971658986 * get_onZeroHit_10() const { return ___onZeroHit_10; }
	inline OnZeroHit_t3971658986 ** get_address_of_onZeroHit_10() { return &___onZeroHit_10; }
	inline void set_onZeroHit_10(OnZeroHit_t3971658986 * value)
	{
		___onZeroHit_10 = value;
		Il2CppCodeGenWriteBarrier(&___onZeroHit_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
