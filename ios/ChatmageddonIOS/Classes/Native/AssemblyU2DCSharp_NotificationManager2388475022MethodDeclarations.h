﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationManager
struct NotificationManager_t2388475022;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void NotificationManager::.ctor()
extern "C"  void NotificationManager__ctor_m3160846001 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::Init()
extern "C"  void NotificationManager_Init_m2919509259 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::RegisterForRemoteNotifications()
extern "C"  void NotificationManager_RegisterForRemoteNotifications_m1529454869 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::FixedUpdate()
extern "C"  void NotificationManager_FixedUpdate_m3808369890 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::SendDeviceToken(System.Boolean)
extern "C"  void NotificationManager_SendDeviceToken_m3365924173 (NotificationManager_t2388475022 * __this, bool ___ios0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::SendPushTokenToServer(System.String,System.Boolean,System.Boolean)
extern "C"  void NotificationManager_SendPushTokenToServer_m1376495808 (NotificationManager_t2388475022 * __this, String_t* ___tokenString0, bool ___development1, bool ___ios2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NotificationManager::ClearOverTime()
extern "C"  Il2CppObject * NotificationManager_ClearOverTime_m3540163089 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::ClearAllPushNotifications()
extern "C"  void NotificationManager_ClearAllPushNotifications_m1503840965 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::InitInternalNotifications()
extern "C"  void NotificationManager_InitInternalNotifications_m3518646612 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::AddUnreadNotification(System.String)
extern "C"  void NotificationManager_AddUnreadNotification_m147346916 (NotificationManager_t2388475022 * __this, String_t* ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NotificationManager::CheckUnreadNotifications()
extern "C"  Il2CppObject * NotificationManager_CheckUnreadNotifications_m711964500 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NotificationManager::ShowNotifcation()
extern "C"  bool NotificationManager_ShowNotifcation_m412576024 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::ClearNotifications()
extern "C"  void NotificationManager_ClearNotifications_m541151290 (NotificationManager_t2388475022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::<SendPushTokenToServer>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void NotificationManager_U3CSendPushTokenToServerU3Em__0_m177277624 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager::<ClearNotifications>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void NotificationManager_U3CClearNotificationsU3Em__1_m1135915337 (NotificationManager_t2388475022 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
