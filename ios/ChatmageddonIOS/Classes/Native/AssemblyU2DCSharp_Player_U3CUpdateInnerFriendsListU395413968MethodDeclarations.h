﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player/<UpdateInnerFriendsList>c__AnonStorey1
struct U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968;

#include "codegen/il2cpp-codegen.h"

// System.Void Player/<UpdateInnerFriendsList>c__AnonStorey1::.ctor()
extern "C"  void U3CUpdateInnerFriendsListU3Ec__AnonStorey1__ctor_m2615783183 (U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
