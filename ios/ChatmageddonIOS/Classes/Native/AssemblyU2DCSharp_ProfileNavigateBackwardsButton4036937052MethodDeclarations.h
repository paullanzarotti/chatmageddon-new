﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProfileNavigateBackwardsButton
struct ProfileNavigateBackwardsButton_t4036937052;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void ProfileNavigateBackwardsButton::.ctor()
extern "C"  void ProfileNavigateBackwardsButton__ctor_m694980449 (ProfileNavigateBackwardsButton_t4036937052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigateBackwardsButton::OnButtonClick()
extern "C"  void ProfileNavigateBackwardsButton_OnButtonClick_m1522891536 (ProfileNavigateBackwardsButton_t4036937052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigateBackwardsButton::SetButtonDirection(ButtonDirection)
extern "C"  void ProfileNavigateBackwardsButton_SetButtonDirection_m2105343463 (ProfileNavigateBackwardsButton_t4036937052 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
