﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackListItem
struct AttackListItem_t3405986821;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void AttackListItem::.ctor()
extern "C"  void AttackListItem__ctor_m80506018 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnEnable()
extern "C"  void AttackListItem_OnEnable_m4040087986 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnDisable()
extern "C"  void AttackListItem_OnDisable_m1742315565 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::CalculatorHitZero(System.Boolean)
extern "C"  void AttackListItem_CalculatorHitZero_m2866343952 (AttackListItem_t3405986821 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetUpItem()
extern "C"  void AttackListItem_SetUpItem_m484135436 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::PopulateItem(Friend,System.Boolean)
extern "C"  void AttackListItem_PopulateItem_m3399974620 (AttackListItem_t3405986821 * __this, Friend_t3555014108 * ___newFriend0, bool ___globalFriend1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::UpdateAvatar()
extern "C"  void AttackListItem_UpdateAvatar_m3023872376 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetFriendRequestRecieved(System.Boolean)
extern "C"  void AttackListItem_SetFriendRequestRecieved_m1043076237 (AttackListItem_t3405986821 * __this, bool ___requestRecieved0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetFriendRequestSent(System.Boolean)
extern "C"  void AttackListItem_SetFriendRequestSent_m703669134 (AttackListItem_t3405986821 * __this, bool ___requestSent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::UpdateLockoutStatus()
extern "C"  void AttackListItem_UpdateLockoutStatus_m4101485122 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetBackgroundColour(System.Boolean)
extern "C"  void AttackListItem_SetBackgroundColour_m2644867371 (AttackListItem_t3405986821 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetBackgroundFriendRequest()
extern "C"  void AttackListItem_SetBackgroundFriendRequest_m4139574447 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetBackgroundSpriteColour(UnityEngine.Color)
extern "C"  void AttackListItem_SetBackgroundSpriteColour_m3353509783 (AttackListItem_t3405986821 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetProfileOutlineColour(UnityEngine.Color)
extern "C"  void AttackListItem_SetProfileOutlineColour_m3964656259 (AttackListItem_t3405986821 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetVisible()
extern "C"  void AttackListItem_SetVisible_m2326013694 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::SetInvisible()
extern "C"  void AttackListItem_SetInvisible_m1207610855 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackListItem::verifyVisibility()
extern "C"  bool AttackListItem_verifyVisibility_m250177661 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::CheckVisibilty()
extern "C"  void AttackListItem_CheckVisibilty_m3028887333 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::Update()
extern "C"  void AttackListItem_Update_m3985015113 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::FindScrollView()
extern "C"  void AttackListItem_FindScrollView_m3906948477 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnPress(System.Boolean)
extern "C"  void AttackListItem_OnPress_m3955462689 (AttackListItem_t3405986821 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void AttackListItem_OnDrag_m2361793121 (AttackListItem_t3405986821 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnDragEnd()
extern "C"  void AttackListItem_OnDragEnd_m2076529810 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnScroll(System.Single)
extern "C"  void AttackListItem_OnScroll_m1512125773 (AttackListItem_t3405986821 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackListItem::OnClick()
extern "C"  void AttackListItem_OnClick_m3786944619 (AttackListItem_t3405986821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
