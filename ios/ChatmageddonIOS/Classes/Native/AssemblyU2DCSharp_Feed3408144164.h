﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Feed
struct  Feed_t3408144164  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Feed::feedActive
	bool ___feedActive_2;
	// System.String Feed::feedName
	String_t* ___feedName_3;
	// System.Single Feed::feedPollInterval
	float ___feedPollInterval_4;
	// System.Boolean Feed::feedPolled
	bool ___feedPolled_5;

public:
	inline static int32_t get_offset_of_feedActive_2() { return static_cast<int32_t>(offsetof(Feed_t3408144164, ___feedActive_2)); }
	inline bool get_feedActive_2() const { return ___feedActive_2; }
	inline bool* get_address_of_feedActive_2() { return &___feedActive_2; }
	inline void set_feedActive_2(bool value)
	{
		___feedActive_2 = value;
	}

	inline static int32_t get_offset_of_feedName_3() { return static_cast<int32_t>(offsetof(Feed_t3408144164, ___feedName_3)); }
	inline String_t* get_feedName_3() const { return ___feedName_3; }
	inline String_t** get_address_of_feedName_3() { return &___feedName_3; }
	inline void set_feedName_3(String_t* value)
	{
		___feedName_3 = value;
		Il2CppCodeGenWriteBarrier(&___feedName_3, value);
	}

	inline static int32_t get_offset_of_feedPollInterval_4() { return static_cast<int32_t>(offsetof(Feed_t3408144164, ___feedPollInterval_4)); }
	inline float get_feedPollInterval_4() const { return ___feedPollInterval_4; }
	inline float* get_address_of_feedPollInterval_4() { return &___feedPollInterval_4; }
	inline void set_feedPollInterval_4(float value)
	{
		___feedPollInterval_4 = value;
	}

	inline static int32_t get_offset_of_feedPolled_5() { return static_cast<int32_t>(offsetof(Feed_t3408144164, ___feedPolled_5)); }
	inline bool get_feedPolled_5() const { return ___feedPolled_5; }
	inline bool* get_address_of_feedPolled_5() { return &___feedPolled_5; }
	inline void set_feedPolled_5(bool value)
	{
		___feedPolled_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
