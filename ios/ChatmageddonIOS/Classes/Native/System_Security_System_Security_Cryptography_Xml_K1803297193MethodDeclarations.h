﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfoX509Data
struct KeyInfoX509Data_t1803297193;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.KeyInfoX509Data::.ctor()
extern "C"  void KeyInfoX509Data__ctor_m2429154427 (KeyInfoX509Data_t1803297193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoX509Data::AddCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C"  void KeyInfoX509Data_AddCertificate_m1970773557 (KeyInfoX509Data_t1803297193 * __this, X509Certificate_t283079845 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoX509Data::AddIssuerSerial(System.String,System.String)
extern "C"  void KeyInfoX509Data_AddIssuerSerial_m3503575153 (KeyInfoX509Data_t1803297193 * __this, String_t* ___issuerName0, String_t* ___serialNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoX509Data::AddSubjectKeyId(System.Byte[])
extern "C"  void KeyInfoX509Data_AddSubjectKeyId_m2375874079 (KeyInfoX509Data_t1803297193 * __this, ByteU5BU5D_t3397334013* ___subjectKeyId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoX509Data::AddSubjectName(System.String)
extern "C"  void KeyInfoX509Data_AddSubjectName_m4033418263 (KeyInfoX509Data_t1803297193 * __this, String_t* ___subjectName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfoX509Data::GetXml()
extern "C"  XmlElement_t2877111883 * KeyInfoX509Data_GetXml_m1633930984 (KeyInfoX509Data_t1803297193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoX509Data::LoadXml(System.Xml.XmlElement)
extern "C"  void KeyInfoX509Data_LoadXml_m288895395 (KeyInfoX509Data_t1803297193 * __this, XmlElement_t2877111883 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
