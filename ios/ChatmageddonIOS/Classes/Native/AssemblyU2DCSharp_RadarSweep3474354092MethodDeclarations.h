﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RadarSweep
struct RadarSweep_t3474354092;

#include "codegen/il2cpp-codegen.h"

// System.Void RadarSweep::.ctor()
extern "C"  void RadarSweep__ctor_m1466881843 (RadarSweep_t3474354092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarSweep::Start()
extern "C"  void RadarSweep_Start_m3957859159 (RadarSweep_t3474354092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarSweep::RotTweenFinished()
extern "C"  void RadarSweep_RotTweenFinished_m2136882113 (RadarSweep_t3474354092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
