﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1954480006MethodDeclarations.h"

// System.Void System.Action`2<System.String,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2025175377(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t4277199140 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m631378381_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.String,System.Int32>::Invoke(T1,T2)
#define Action_2_Invoke_m1791442244(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t4277199140 *, String_t*, int32_t, const MethodInfo*))Action_2_Invoke_m1256339344_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.String,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3158136273(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t4277199140 *, String_t*, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m2449831909_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.String,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3499013379(__this, ___result0, method) ((  void (*) (Action_2_t4277199140 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1881200271_gshared)(__this, ___result0, method)
