﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginBottomUIManager
struct LoginBottomUIManager_t3738095009;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginBottomUIManager::.ctor()
extern "C"  void LoginBottomUIManager__ctor_m3625144618 (LoginBottomUIManager_t3738095009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginBottomUIManager::LoadStartPage()
extern "C"  void LoginBottomUIManager_LoadStartPage_m2576743829 (LoginBottomUIManager_t3738095009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
