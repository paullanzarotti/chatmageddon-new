﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FriendSearchNavScreen>
struct List_1_t2387510295;
// System.Collections.Generic.IEnumerable`1<FriendSearchNavScreen>
struct IEnumerable_1_t3310516208;
// FriendSearchNavScreen[]
struct FriendSearchNavScreenU5BU5D_t3949017674;
// System.Collections.Generic.IEnumerator`1<FriendSearchNavScreen>
struct IEnumerator_1_t493912990;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<FriendSearchNavScreen>
struct ICollection_1_t3970464468;
// System.Collections.ObjectModel.ReadOnlyCollection`1<FriendSearchNavScreen>
struct ReadOnlyCollection_1_t3204174855;
// System.Predicate`1<FriendSearchNavScreen>
struct Predicate_1_t1461359278;
// System.Action`1<FriendSearchNavScreen>
struct Action_1_t2820188545;
// System.Collections.Generic.IComparer`1<FriendSearchNavScreen>
struct IComparer_1_t972852285;
// System.Comparison`1<FriendSearchNavScreen>
struct Comparison_1_t4280128014;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1922239969.h"

// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::.ctor()
extern "C"  void List_1__ctor_m2125408963_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1__ctor_m2125408963(__this, method) ((  void (*) (List_1_t2387510295 *, const MethodInfo*))List_1__ctor_m2125408963_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2829447954_gshared (List_1_t2387510295 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2829447954(__this, ___collection0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2829447954_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1353616404_gshared (List_1_t2387510295 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1353616404(__this, ___capacity0, method) ((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1__ctor_m1353616404_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1050343668_gshared (List_1_t2387510295 * __this, FriendSearchNavScreenU5BU5D_t3949017674* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1050343668(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2387510295 *, FriendSearchNavScreenU5BU5D_t3949017674*, int32_t, const MethodInfo*))List_1__ctor_m1050343668_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::.cctor()
extern "C"  void List_1__cctor_m4083377992_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4083377992(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4083377992_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3193840251_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3193840251(__this, method) ((  Il2CppObject* (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3193840251_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1090490351_gshared (List_1_t2387510295 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1090490351(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2387510295 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1090490351_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m258062986_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m258062986(__this, method) ((  Il2CppObject * (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m258062986_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4116008175_gshared (List_1_t2387510295 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4116008175(__this, ___item0, method) ((  int32_t (*) (List_1_t2387510295 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4116008175_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3529420039_gshared (List_1_t2387510295 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3529420039(__this, ___item0, method) ((  bool (*) (List_1_t2387510295 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3529420039_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2520966209_gshared (List_1_t2387510295 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2520966209(__this, ___item0, method) ((  int32_t (*) (List_1_t2387510295 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2520966209_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3616275280_gshared (List_1_t2387510295 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3616275280(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2387510295 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3616275280_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1687819078_gshared (List_1_t2387510295 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1687819078(__this, ___item0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1687819078_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3614610094_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3614610094(__this, method) ((  bool (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3614610094_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3692412079_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3692412079(__this, method) ((  bool (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3692412079_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2615931567_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2615931567(__this, method) ((  Il2CppObject * (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2615931567_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2985128228_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2985128228(__this, method) ((  bool (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2985128228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1901925131_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1901925131(__this, method) ((  bool (*) (List_1_t2387510295 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1901925131_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1743031640_gshared (List_1_t2387510295 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1743031640(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1743031640_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1605903473_gshared (List_1_t2387510295 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1605903473(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2387510295 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1605903473_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Add(T)
extern "C"  void List_1_Add_m2380253140_gshared (List_1_t2387510295 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2380253140(__this, ___item0, method) ((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_Add_m2380253140_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3382062195_gshared (List_1_t2387510295 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3382062195(__this, ___newCount0, method) ((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3382062195_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1495076604_gshared (List_1_t2387510295 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1495076604(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2387510295 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1495076604_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3249622587_gshared (List_1_t2387510295 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3249622587(__this, ___collection0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3249622587_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m358325883_gshared (List_1_t2387510295 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m358325883(__this, ___enumerable0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m358325883_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2376241802_gshared (List_1_t2387510295 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2376241802(__this, ___collection0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2376241802_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<FriendSearchNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3204174855 * List_1_AsReadOnly_m2177853955_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2177853955(__this, method) ((  ReadOnlyCollection_1_t3204174855 * (*) (List_1_t2387510295 *, const MethodInfo*))List_1_AsReadOnly_m2177853955_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Clear()
extern "C"  void List_1_Clear_m1200087960_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_Clear_m1200087960(__this, method) ((  void (*) (List_1_t2387510295 *, const MethodInfo*))List_1_Clear_m1200087960_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m2355884334_gshared (List_1_t2387510295 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2355884334(__this, ___item0, method) ((  bool (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_Contains_m2355884334_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m4186533835_gshared (List_1_t2387510295 * __this, FriendSearchNavScreenU5BU5D_t3949017674* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m4186533835(__this, ___array0, method) ((  void (*) (List_1_t2387510295 *, FriendSearchNavScreenU5BU5D_t3949017674*, const MethodInfo*))List_1_CopyTo_m4186533835_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2359305104_gshared (List_1_t2387510295 * __this, FriendSearchNavScreenU5BU5D_t3949017674* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2359305104(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2387510295 *, FriendSearchNavScreenU5BU5D_t3949017674*, int32_t, const MethodInfo*))List_1_CopyTo_m2359305104_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m3474390290_gshared (List_1_t2387510295 * __this, int32_t ___index0, FriendSearchNavScreenU5BU5D_t3949017674* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m3474390290(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t2387510295 *, int32_t, FriendSearchNavScreenU5BU5D_t3949017674*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3474390290_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m3495771062_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_Exists_m3495771062(__this, ___match0, method) ((  bool (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_Exists_m3495771062_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<FriendSearchNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2578565784_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_Find_m2578565784(__this, ___match0, method) ((  int32_t (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_Find_m2578565784_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3722040623_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3722040623(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1461359278 *, const MethodInfo*))List_1_CheckMatch_m3722040623_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendSearchNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t2387510295 * List_1_FindAll_m2319002411_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2319002411(__this, ___match0, method) ((  List_1_t2387510295 * (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_FindAll_m2319002411_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendSearchNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t2387510295 * List_1_FindAllStackBits_m3960080973_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3960080973(__this, ___match0, method) ((  List_1_t2387510295 * (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_FindAllStackBits_m3960080973_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendSearchNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t2387510295 * List_1_FindAllList_m2114137357_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2114137357(__this, ___match0, method) ((  List_1_t2387510295 * (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_FindAllList_m2114137357_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m774348775_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m774348775(__this, ___match0, method) ((  int32_t (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_FindIndex_m774348775_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4072638288_gshared (List_1_t2387510295 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1461359278 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4072638288(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2387510295 *, int32_t, int32_t, Predicate_1_t1461359278 *, const MethodInfo*))List_1_GetIndex_m4072638288_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1990807609_gshared (List_1_t2387510295 * __this, Action_1_t2820188545 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1990807609(__this, ___action0, method) ((  void (*) (List_1_t2387510295 *, Action_1_t2820188545 *, const MethodInfo*))List_1_ForEach_m1990807609_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<FriendSearchNavScreen>::GetEnumerator()
extern "C"  Enumerator_t1922239969  List_1_GetEnumerator_m1806934747_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1806934747(__this, method) ((  Enumerator_t1922239969  (*) (List_1_t2387510295 *, const MethodInfo*))List_1_GetEnumerator_m1806934747_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1703057966_gshared (List_1_t2387510295 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1703057966(__this, ___item0, method) ((  int32_t (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_IndexOf_m1703057966_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2640262563_gshared (List_1_t2387510295 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2640262563(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2387510295 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2640262563_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2854267848_gshared (List_1_t2387510295 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2854267848(__this, ___index0, method) ((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2854267848_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m329268189_gshared (List_1_t2387510295 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m329268189(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2387510295 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m329268189_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m4025875130_gshared (List_1_t2387510295 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m4025875130(__this, ___collection0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m4025875130_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<FriendSearchNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3808890029_gshared (List_1_t2387510295 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3808890029(__this, ___item0, method) ((  bool (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_Remove_m3808890029_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1471757511_gshared (List_1_t2387510295 * __this, Predicate_1_t1461359278 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1471757511(__this, ___match0, method) ((  int32_t (*) (List_1_t2387510295 *, Predicate_1_t1461359278 *, const MethodInfo*))List_1_RemoveAll_m1471757511_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m778083825_gshared (List_1_t2387510295 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m778083825(__this, ___index0, method) ((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_RemoveAt_m778083825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2515370340_gshared (List_1_t2387510295 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2515370340(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2387510295 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2515370340_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m1809913931_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_Reverse_m1809913931(__this, method) ((  void (*) (List_1_t2387510295 *, const MethodInfo*))List_1_Reverse_m1809913931_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Sort()
extern "C"  void List_1_Sort_m2618183693_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_Sort_m2618183693(__this, method) ((  void (*) (List_1_t2387510295 *, const MethodInfo*))List_1_Sort_m2618183693_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1237676233_gshared (List_1_t2387510295 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1237676233(__this, ___comparer0, method) ((  void (*) (List_1_t2387510295 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1237676233_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m499520816_gshared (List_1_t2387510295 * __this, Comparison_1_t4280128014 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m499520816(__this, ___comparison0, method) ((  void (*) (List_1_t2387510295 *, Comparison_1_t4280128014 *, const MethodInfo*))List_1_Sort_m499520816_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<FriendSearchNavScreen>::ToArray()
extern "C"  FriendSearchNavScreenU5BU5D_t3949017674* List_1_ToArray_m88512752_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_ToArray_m88512752(__this, method) ((  FriendSearchNavScreenU5BU5D_t3949017674* (*) (List_1_t2387510295 *, const MethodInfo*))List_1_ToArray_m88512752_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2941402906_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2941402906(__this, method) ((  void (*) (List_1_t2387510295 *, const MethodInfo*))List_1_TrimExcess_m2941402906_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2067184996_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2067184996(__this, method) ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))List_1_get_Capacity_m2067184996_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3231467003_gshared (List_1_t2387510295 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3231467003(__this, ___value0, method) ((  void (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3231467003_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<FriendSearchNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m1528728463_gshared (List_1_t2387510295 * __this, const MethodInfo* method);
#define List_1_get_Count_m1528728463(__this, method) ((  int32_t (*) (List_1_t2387510295 *, const MethodInfo*))List_1_get_Count_m1528728463_gshared)(__this, method)
// T System.Collections.Generic.List`1<FriendSearchNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2641582501_gshared (List_1_t2387510295 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2641582501(__this, ___index0, method) ((  int32_t (*) (List_1_t2387510295 *, int32_t, const MethodInfo*))List_1_get_Item_m2641582501_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendSearchNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3318118202_gshared (List_1_t2387510295 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3318118202(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2387510295 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3318118202_gshared)(__this, ___index0, ___value1, method)
