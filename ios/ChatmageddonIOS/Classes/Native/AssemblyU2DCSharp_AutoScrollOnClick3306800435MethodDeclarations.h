﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoScrollOnClick
struct AutoScrollOnClick_t3306800435;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoScrollOnClick::.ctor()
extern "C"  void AutoScrollOnClick__ctor_m609439064 (AutoScrollOnClick_t3306800435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoScrollOnClick::Awake()
extern "C"  void AutoScrollOnClick_Awake_m749783807 (AutoScrollOnClick_t3306800435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoScrollOnClick::OnClick()
extern "C"  void AutoScrollOnClick_OnClick_m3133286525 (AutoScrollOnClick_t3306800435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
