﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.UnibillConfiguration/<getCurrency>c__AnonStorey2
struct U3CgetCurrencyU3Ec__AnonStorey2_t2627465703;
// VirtualCurrency
struct VirtualCurrency_t1115497880;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_VirtualCurrency1115497880.h"

// System.Void Unibill.Impl.UnibillConfiguration/<getCurrency>c__AnonStorey2::.ctor()
extern "C"  void U3CgetCurrencyU3Ec__AnonStorey2__ctor_m788385880 (U3CgetCurrencyU3Ec__AnonStorey2_t2627465703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration/<getCurrency>c__AnonStorey2::<>m__0(VirtualCurrency)
extern "C"  bool U3CgetCurrencyU3Ec__AnonStorey2_U3CU3Em__0_m545479585 (U3CgetCurrencyU3Ec__AnonStorey2_t2627465703 * __this, VirtualCurrency_t1115497880 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
