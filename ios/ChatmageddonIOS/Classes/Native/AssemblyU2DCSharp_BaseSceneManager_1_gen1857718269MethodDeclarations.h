﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<LauncherSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m1018459619(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m387573734(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::Start()
#define BaseSceneManager_1_Start_m1419913063(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m2517940367(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m3154004487(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m2112199705(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<LauncherSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m3255660137(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m867264812(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<LauncherSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m3303230597(__this, method) ((  void (*) (BaseSceneManager_1_t1857718269 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
