﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FriendNavScreen>
struct List_1_t1544504585;
// System.Collections.Generic.IEnumerable`1<FriendNavScreen>
struct IEnumerable_1_t2467510498;
// FriendNavScreen[]
struct FriendNavScreenU5BU5D_t3451088208;
// System.Collections.Generic.IEnumerator`1<FriendNavScreen>
struct IEnumerator_1_t3945874576;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<FriendNavScreen>
struct ICollection_1_t3127458758;
// System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>
struct ReadOnlyCollection_1_t2361169145;
// System.Predicate`1<FriendNavScreen>
struct Predicate_1_t618353568;
// System.Action`1<FriendNavScreen>
struct Action_1_t1977182835;
// System.Collections.Generic.IComparer`1<FriendNavScreen>
struct IComparer_1_t129846575;
// System.Comparison`1<FriendNavScreen>
struct Comparison_1_t3437122304;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1079234259.h"

// System.Void System.Collections.Generic.List`1<FriendNavScreen>::.ctor()
extern "C"  void List_1__ctor_m783221705_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1__ctor_m783221705(__this, method) ((  void (*) (List_1_t1544504585 *, const MethodInfo*))List_1__ctor_m783221705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3470810694_gshared (List_1_t1544504585 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3470810694(__this, ___collection0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3470810694_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1739613412_gshared (List_1_t1544504585 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1739613412(__this, ___capacity0, method) ((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1__ctor_m1739613412_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1027055092_gshared (List_1_t1544504585 * __this, FriendNavScreenU5BU5D_t3451088208* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1027055092(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1544504585 *, FriendNavScreenU5BU5D_t3451088208*, int32_t, const MethodInfo*))List_1__ctor_m1027055092_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::.cctor()
extern "C"  void List_1__cctor_m2134193768_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2134193768(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2134193768_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1275102765_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1275102765(__this, method) ((  Il2CppObject* (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1275102765_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3303097945_gshared (List_1_t1544504585 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3303097945(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1544504585 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3303097945_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m85946594_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m85946594(__this, method) ((  Il2CppObject * (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m85946594_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1444337693_gshared (List_1_t1544504585 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1444337693(__this, ___item0, method) ((  int32_t (*) (List_1_t1544504585 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1444337693_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1629560233_gshared (List_1_t1544504585 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1629560233(__this, ___item0, method) ((  bool (*) (List_1_t1544504585 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1629560233_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3048067019_gshared (List_1_t1544504585 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3048067019(__this, ___item0, method) ((  int32_t (*) (List_1_t1544504585 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3048067019_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m627358444_gshared (List_1_t1544504585 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m627358444(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1544504585 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m627358444_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3382834942_gshared (List_1_t1544504585 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3382834942(__this, ___item0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3382834942_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3009701526_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3009701526(__this, method) ((  bool (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3009701526_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m479944565_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m479944565(__this, method) ((  bool (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m479944565_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m428648681_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m428648681(__this, method) ((  Il2CppObject * (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m428648681_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4279021156_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4279021156(__this, method) ((  bool (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4279021156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2735361753_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2735361753(__this, method) ((  bool (*) (List_1_t1544504585 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2735361753_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3712839808_gshared (List_1_t1544504585 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3712839808(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3712839808_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2900301683_gshared (List_1_t1544504585 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2900301683(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1544504585 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2900301683_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Add(T)
extern "C"  void List_1_Add_m636962264_gshared (List_1_t1544504585 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m636962264(__this, ___item0, method) ((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_Add_m636962264_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1324313193_gshared (List_1_t1544504585 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1324313193(__this, ___newCount0, method) ((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1324313193_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m991920420_gshared (List_1_t1544504585 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m991920420(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1544504585 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m991920420_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2332049929_gshared (List_1_t1544504585 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2332049929(__this, ___collection0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2332049929_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2587202425_gshared (List_1_t1544504585 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2587202425(__this, ___enumerable0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2587202425_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m737653334_gshared (List_1_t1544504585 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m737653334(__this, ___collection0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m737653334_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<FriendNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2361169145 * List_1_AsReadOnly_m3609213233_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3609213233(__this, method) ((  ReadOnlyCollection_1_t2361169145 * (*) (List_1_t1544504585 *, const MethodInfo*))List_1_AsReadOnly_m3609213233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Clear()
extern "C"  void List_1_Clear_m3656236540_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_Clear_m3656236540(__this, method) ((  void (*) (List_1_t1544504585 *, const MethodInfo*))List_1_Clear_m3656236540_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m2629273010_gshared (List_1_t1544504585 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2629273010(__this, ___item0, method) ((  bool (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_Contains_m2629273010_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m3567568137_gshared (List_1_t1544504585 * __this, FriendNavScreenU5BU5D_t3451088208* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m3567568137(__this, ___array0, method) ((  void (*) (List_1_t1544504585 *, FriendNavScreenU5BU5D_t3451088208*, const MethodInfo*))List_1_CopyTo_m3567568137_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2125652936_gshared (List_1_t1544504585 * __this, FriendNavScreenU5BU5D_t3451088208* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2125652936(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1544504585 *, FriendNavScreenU5BU5D_t3451088208*, int32_t, const MethodInfo*))List_1_CopyTo_m2125652936_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m2255453602_gshared (List_1_t1544504585 * __this, int32_t ___index0, FriendNavScreenU5BU5D_t3451088208* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m2255453602(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t1544504585 *, int32_t, FriendNavScreenU5BU5D_t3451088208*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m2255453602_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m945381870_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_Exists_m945381870(__this, ___match0, method) ((  bool (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_Exists_m945381870_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<FriendNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m4028778708_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_Find_m4028778708(__this, ___match0, method) ((  int32_t (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_Find_m4028778708_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2363268145_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2363268145(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t618353568 *, const MethodInfo*))List_1_CheckMatch_m2363268145_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t1544504585 * List_1_FindAll_m1285307625_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m1285307625(__this, ___match0, method) ((  List_1_t1544504585 * (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_FindAll_m1285307625_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t1544504585 * List_1_FindAllStackBits_m1547129887_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1547129887(__this, ___match0, method) ((  List_1_t1544504585 * (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_FindAllStackBits_m1547129887_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t1544504585 * List_1_FindAllList_m2458475279_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2458475279(__this, ___match0, method) ((  List_1_t1544504585 * (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_FindAllList_m2458475279_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m3160441869_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m3160441869(__this, ___match0, method) ((  int32_t (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_FindIndex_m3160441869_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3011127676_gshared (List_1_t1544504585 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t618353568 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3011127676(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1544504585 *, int32_t, int32_t, Predicate_1_t618353568 *, const MethodInfo*))List_1_GetIndex_m3011127676_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m57713135_gshared (List_1_t1544504585 * __this, Action_1_t1977182835 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m57713135(__this, ___action0, method) ((  void (*) (List_1_t1544504585 *, Action_1_t1977182835 *, const MethodInfo*))List_1_ForEach_m57713135_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<FriendNavScreen>::GetEnumerator()
extern "C"  Enumerator_t1079234259  List_1_GetEnumerator_m1784190969_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1784190969(__this, method) ((  Enumerator_t1079234259  (*) (List_1_t1544504585 *, const MethodInfo*))List_1_GetEnumerator_m1784190969_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1624164106_gshared (List_1_t1544504585 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1624164106(__this, ___item0, method) ((  int32_t (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_IndexOf_m1624164106_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m551172557_gshared (List_1_t1544504585 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m551172557(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1544504585 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m551172557_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4222796836_gshared (List_1_t1544504585 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4222796836(__this, ___index0, method) ((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4222796836_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2113794515_gshared (List_1_t1544504585 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2113794515(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1544504585 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2113794515_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m4061811466_gshared (List_1_t1544504585 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m4061811466(__this, ___collection0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m4061811466_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<FriendNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3871792351_gshared (List_1_t1544504585 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3871792351(__this, ___item0, method) ((  bool (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_Remove_m3871792351_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1925318397_gshared (List_1_t1544504585 * __this, Predicate_1_t618353568 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1925318397(__this, ___match0, method) ((  int32_t (*) (List_1_t1544504585 *, Predicate_1_t618353568 *, const MethodInfo*))List_1_RemoveAll_m1925318397_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2297813719_gshared (List_1_t1544504585 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2297813719(__this, ___index0, method) ((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2297813719_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1737942304_gshared (List_1_t1544504585 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1737942304(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1544504585 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1737942304_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m3873055493_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_Reverse_m3873055493(__this, method) ((  void (*) (List_1_t1544504585 *, const MethodInfo*))List_1_Reverse_m3873055493_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Sort()
extern "C"  void List_1_Sort_m2373571023_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_Sort_m2373571023(__this, method) ((  void (*) (List_1_t1544504585 *, const MethodInfo*))List_1_Sort_m2373571023_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m705805683_gshared (List_1_t1544504585 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m705805683(__this, ___comparer0, method) ((  void (*) (List_1_t1544504585 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m705805683_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1548006864_gshared (List_1_t1544504585 * __this, Comparison_1_t3437122304 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1548006864(__this, ___comparison0, method) ((  void (*) (List_1_t1544504585 *, Comparison_1_t3437122304 *, const MethodInfo*))List_1_Sort_m1548006864_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<FriendNavScreen>::ToArray()
extern "C"  FriendNavScreenU5BU5D_t3451088208* List_1_ToArray_m3537592344_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_ToArray_m3537592344(__this, method) ((  FriendNavScreenU5BU5D_t3451088208* (*) (List_1_t1544504585 *, const MethodInfo*))List_1_ToArray_m3537592344_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4281998290_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4281998290(__this, method) ((  void (*) (List_1_t1544504585 *, const MethodInfo*))List_1_TrimExcess_m4281998290_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1730904932_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1730904932(__this, method) ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))List_1_get_Capacity_m1730904932_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m472873033_gshared (List_1_t1544504585 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m472873033(__this, ___value0, method) ((  void (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_set_Capacity_m472873033_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<FriendNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m920549069_gshared (List_1_t1544504585 * __this, const MethodInfo* method);
#define List_1_get_Count_m920549069(__this, method) ((  int32_t (*) (List_1_t1544504585 *, const MethodInfo*))List_1_get_Count_m920549069_gshared)(__this, method)
// T System.Collections.Generic.List`1<FriendNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m3408527151_gshared (List_1_t1544504585 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3408527151(__this, ___index0, method) ((  int32_t (*) (List_1_t1544504585 *, int32_t, const MethodInfo*))List_1_get_Item_m3408527151_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3425853794_gshared (List_1_t1544504585 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3425853794(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1544504585 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3425853794_gshared)(__this, ___index0, ___value1, method)
