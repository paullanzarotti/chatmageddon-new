﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShakeBarController/<CheckShake>c__Iterator0
struct U3CCheckShakeU3Ec__Iterator0_t3478000993;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ShakeBarController/<CheckShake>c__Iterator0::.ctor()
extern "C"  void U3CCheckShakeU3Ec__Iterator0__ctor_m197389630 (U3CCheckShakeU3Ec__Iterator0_t3478000993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShakeBarController/<CheckShake>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckShakeU3Ec__Iterator0_MoveNext_m667116882 (U3CCheckShakeU3Ec__Iterator0_t3478000993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShakeBarController/<CheckShake>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckShakeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1968337660 (U3CCheckShakeU3Ec__Iterator0_t3478000993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShakeBarController/<CheckShake>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckShakeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1339416164 (U3CCheckShakeU3Ec__Iterator0_t3478000993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeBarController/<CheckShake>c__Iterator0::Dispose()
extern "C"  void U3CCheckShakeU3Ec__Iterator0_Dispose_m2284998195 (U3CCheckShakeU3Ec__Iterator0_t3478000993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeBarController/<CheckShake>c__Iterator0::Reset()
extern "C"  void U3CCheckShakeU3Ec__Iterator0_Reset_m810442817 (U3CCheckShakeU3Ec__Iterator0_t3478000993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
