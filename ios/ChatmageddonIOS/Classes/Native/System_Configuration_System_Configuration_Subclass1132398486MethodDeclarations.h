﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.SubclassTypeValidatorAttribute
struct SubclassTypeValidatorAttribute_t1132398486;
// System.Type
struct Type_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Configuration.SubclassTypeValidatorAttribute::.ctor(System.Type)
extern "C"  void SubclassTypeValidatorAttribute__ctor_m3625462488 (SubclassTypeValidatorAttribute_t1132398486 * __this, Type_t * ___baseClass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Configuration.SubclassTypeValidatorAttribute::get_BaseClass()
extern "C"  Type_t * SubclassTypeValidatorAttribute_get_BaseClass_m2700918667 (SubclassTypeValidatorAttribute_t1132398486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.SubclassTypeValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * SubclassTypeValidatorAttribute_get_ValidatorInstance_m1696449592 (SubclassTypeValidatorAttribute_t1132398486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
