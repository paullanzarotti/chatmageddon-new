﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;

#include "AssemblyU2DCSharp_NGUIAnimation2962118329.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITextureAnimation
struct  UITextureAnimation_t3587053699  : public NGUIAnimation_t2962118329
{
public:
	// UITexture UITextureAnimation::animTexture
	UITexture_t2537039969 * ___animTexture_12;

public:
	inline static int32_t get_offset_of_animTexture_12() { return static_cast<int32_t>(offsetof(UITextureAnimation_t3587053699, ___animTexture_12)); }
	inline UITexture_t2537039969 * get_animTexture_12() const { return ___animTexture_12; }
	inline UITexture_t2537039969 ** get_address_of_animTexture_12() { return &___animTexture_12; }
	inline void set_animTexture_12(UITexture_t2537039969 * value)
	{
		___animTexture_12 = value;
		Il2CppCodeGenWriteBarrier(&___animTexture_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
