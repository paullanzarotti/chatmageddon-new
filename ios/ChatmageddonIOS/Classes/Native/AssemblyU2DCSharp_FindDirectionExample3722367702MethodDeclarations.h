﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FindDirectionExample
struct FindDirectionExample_t3722367702;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FindDirectionExample::.ctor()
extern "C"  void FindDirectionExample__ctor_m655716903 (FindDirectionExample_t3722367702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FindDirectionExample::Start()
extern "C"  void FindDirectionExample_Start_m3086381347 (FindDirectionExample_t3722367702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FindDirectionExample::OnFindDirectionComplete(System.String)
extern "C"  void FindDirectionExample_OnFindDirectionComplete_m3017509657 (FindDirectionExample_t3722367702 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
