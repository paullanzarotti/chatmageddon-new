﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MapGroupModal
struct MapGroupModal_t1530151518;
// Friend
struct Friend_t3555014108;
// OpenGroupFriendProfile
struct OpenGroupFriendProfile_t894547916;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// TweenPosition
struct TweenPosition_t1144714832;
// TweenScale
struct TweenScale_t2697902175;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupFriendUI
struct  GroupFriendUI_t1634706847  : public MonoBehaviour_t1158329972
{
public:
	// MapGroupModal GroupFriendUI::modal
	MapGroupModal_t1530151518 * ___modal_2;
	// Friend GroupFriendUI::friend
	Friend_t3555014108 * ___friend_3;
	// OpenGroupFriendProfile GroupFriendUI::button
	OpenGroupFriendProfile_t894547916 * ___button_4;
	// AvatarUpdater GroupFriendUI::avatar
	AvatarUpdater_t2404165026 * ___avatar_5;
	// TweenPosition GroupFriendUI::posTween
	TweenPosition_t1144714832 * ___posTween_6;
	// TweenScale GroupFriendUI::scaleTween
	TweenScale_t2697902175 * ___scaleTween_7;

public:
	inline static int32_t get_offset_of_modal_2() { return static_cast<int32_t>(offsetof(GroupFriendUI_t1634706847, ___modal_2)); }
	inline MapGroupModal_t1530151518 * get_modal_2() const { return ___modal_2; }
	inline MapGroupModal_t1530151518 ** get_address_of_modal_2() { return &___modal_2; }
	inline void set_modal_2(MapGroupModal_t1530151518 * value)
	{
		___modal_2 = value;
		Il2CppCodeGenWriteBarrier(&___modal_2, value);
	}

	inline static int32_t get_offset_of_friend_3() { return static_cast<int32_t>(offsetof(GroupFriendUI_t1634706847, ___friend_3)); }
	inline Friend_t3555014108 * get_friend_3() const { return ___friend_3; }
	inline Friend_t3555014108 ** get_address_of_friend_3() { return &___friend_3; }
	inline void set_friend_3(Friend_t3555014108 * value)
	{
		___friend_3 = value;
		Il2CppCodeGenWriteBarrier(&___friend_3, value);
	}

	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(GroupFriendUI_t1634706847, ___button_4)); }
	inline OpenGroupFriendProfile_t894547916 * get_button_4() const { return ___button_4; }
	inline OpenGroupFriendProfile_t894547916 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(OpenGroupFriendProfile_t894547916 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier(&___button_4, value);
	}

	inline static int32_t get_offset_of_avatar_5() { return static_cast<int32_t>(offsetof(GroupFriendUI_t1634706847, ___avatar_5)); }
	inline AvatarUpdater_t2404165026 * get_avatar_5() const { return ___avatar_5; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_5() { return &___avatar_5; }
	inline void set_avatar_5(AvatarUpdater_t2404165026 * value)
	{
		___avatar_5 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_5, value);
	}

	inline static int32_t get_offset_of_posTween_6() { return static_cast<int32_t>(offsetof(GroupFriendUI_t1634706847, ___posTween_6)); }
	inline TweenPosition_t1144714832 * get_posTween_6() const { return ___posTween_6; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_6() { return &___posTween_6; }
	inline void set_posTween_6(TweenPosition_t1144714832 * value)
	{
		___posTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_6, value);
	}

	inline static int32_t get_offset_of_scaleTween_7() { return static_cast<int32_t>(offsetof(GroupFriendUI_t1634706847, ___scaleTween_7)); }
	inline TweenScale_t2697902175 * get_scaleTween_7() const { return ___scaleTween_7; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_7() { return &___scaleTween_7; }
	inline void set_scaleTween_7(TweenScale_t2697902175 * value)
	{
		___scaleTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
