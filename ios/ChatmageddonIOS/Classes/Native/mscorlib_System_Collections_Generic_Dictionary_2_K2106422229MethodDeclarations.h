﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2766651291MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1059969689(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2106422229 *, Dictionary_2_t3917891754 *, const MethodInfo*))KeyCollection__ctor_m3838914573_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3969636599(__this, ___item0, method) ((  void (*) (KeyCollection_t2106422229 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2987932827_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4284693120(__this, method) ((  void (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1722159286_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1488277753(__this, ___item0, method) ((  bool (*) (KeyCollection_t2106422229 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1396153341_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2801387008(__this, ___item0, method) ((  bool (*) (KeyCollection_t2106422229 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3920768710_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2389424200(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2220136930_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2596654938(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2106422229 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3823530176_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1276023159(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2718737043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1786328982(__this, method) ((  bool (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2556458832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3115043574(__this, method) ((  bool (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497235644_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3560895312(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3123193354_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2177459708(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2106422229 *, BillingPlatformU5BU5D_t1616278231*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3539548866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1826025425(__this, method) ((  Enumerator_t2312427896  (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_GetEnumerator_m1419934541_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.String>::get_Count()
#define KeyCollection_get_Count_m36169918(__this, method) ((  int32_t (*) (KeyCollection_t2106422229 *, const MethodInfo*))KeyCollection_get_Count_m95902596_gshared)(__this, method)
