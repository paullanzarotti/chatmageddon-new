﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PPUserNavScreen
struct PPUserNavScreen_t245777140;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void PPUserNavScreen::.ctor()
extern "C"  void PPUserNavScreen__ctor_m640285355 (PPUserNavScreen_t245777140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPUserNavScreen::Start()
extern "C"  void PPUserNavScreen_Start_m2012669263 (PPUserNavScreen_t245777140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPUserNavScreen::UIClosing()
extern "C"  void PPUserNavScreen_UIClosing_m1330644186 (PPUserNavScreen_t245777140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPUserNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void PPUserNavScreen_ScreenLoad_m2240155001 (PPUserNavScreen_t245777140 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPUserNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void PPUserNavScreen_ScreenUnload_m3713018845 (PPUserNavScreen_t245777140 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PPUserNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool PPUserNavScreen_ValidateScreenNavigation_m1131521936 (PPUserNavScreen_t245777140 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPUserNavScreen::SetUIScene()
extern "C"  void PPUserNavScreen_SetUIScene_m252266111 (PPUserNavScreen_t245777140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
