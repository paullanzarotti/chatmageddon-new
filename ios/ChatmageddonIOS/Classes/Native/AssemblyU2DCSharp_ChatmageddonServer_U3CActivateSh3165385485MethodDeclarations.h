﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<ActivateShield>c__AnonStorey21
struct U3CActivateShieldU3Ec__AnonStorey21_t3165385485;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<ActivateShield>c__AnonStorey21::.ctor()
extern "C"  void U3CActivateShieldU3Ec__AnonStorey21__ctor_m1954534874 (U3CActivateShieldU3Ec__AnonStorey21_t3165385485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<ActivateShield>c__AnonStorey21::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CActivateShieldU3Ec__AnonStorey21_U3CU3Em__0_m3132579413 (U3CActivateShieldU3Ec__AnonStorey21_t3165385485 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
