﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<ProfileNavScreen>
struct Collection_1_t668402608;
// System.Collections.Generic.IList`1<ProfileNavScreen>
struct IList_1_t1667598455;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ProfileNavScreen[]
struct ProfileNavScreenU5BU5D_t3517801995;
// System.Collections.Generic.IEnumerator`1<ProfileNavScreen>
struct IEnumerator_1_t2897148977;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"

// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m124216735_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1__ctor_m124216735(__this, method) ((  void (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1__ctor_m124216735_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m3025844260_gshared (Collection_1_t668402608 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m3025844260(__this, ___list0, method) ((  void (*) (Collection_1_t668402608 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3025844260_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m483877678_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m483877678(__this, method) ((  bool (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m483877678_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2705677547_gshared (Collection_1_t668402608 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2705677547(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t668402608 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2705677547_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3838354962_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3838354962(__this, method) ((  Il2CppObject * (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3838354962_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2186580667_gshared (Collection_1_t668402608 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2186580667(__this, ___value0, method) ((  int32_t (*) (Collection_1_t668402608 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2186580667_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2230590739_gshared (Collection_1_t668402608 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2230590739(__this, ___value0, method) ((  bool (*) (Collection_1_t668402608 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2230590739_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4091495217_gshared (Collection_1_t668402608 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m4091495217(__this, ___value0, method) ((  int32_t (*) (Collection_1_t668402608 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m4091495217_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2471246976_gshared (Collection_1_t668402608 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2471246976(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t668402608 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2471246976_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m970490422_gshared (Collection_1_t668402608 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m970490422(__this, ___value0, method) ((  void (*) (Collection_1_t668402608 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m970490422_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1522554003_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1522554003(__this, method) ((  bool (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1522554003_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2078047135_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2078047135(__this, method) ((  Il2CppObject * (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2078047135_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2256717284_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2256717284(__this, method) ((  bool (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2256717284_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m482168943_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m482168943(__this, method) ((  bool (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m482168943_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2322054866_gshared (Collection_1_t668402608 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2322054866(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2322054866_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m181874085_gshared (Collection_1_t668402608 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m181874085(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t668402608 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m181874085_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m2550171092_gshared (Collection_1_t668402608 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2550171092(__this, ___item0, method) ((  void (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_Add_m2550171092_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m1673055560_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1673055560(__this, method) ((  void (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_Clear_m1673055560_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2138983282_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2138983282(__this, method) ((  void (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_ClearItems_m2138983282_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m80888678_gshared (Collection_1_t668402608 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m80888678(__this, ___item0, method) ((  bool (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_Contains_m80888678_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2718817888_gshared (Collection_1_t668402608 * __this, ProfileNavScreenU5BU5D_t3517801995* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2718817888(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t668402608 *, ProfileNavScreenU5BU5D_t3517801995*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2718817888_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3617875923_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3617875923(__this, method) ((  Il2CppObject* (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_GetEnumerator_m3617875923_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1987867046_gshared (Collection_1_t668402608 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1987867046(__this, ___item0, method) ((  int32_t (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m1987867046_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1244189729_gshared (Collection_1_t668402608 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1244189729(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t668402608 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1244189729_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m628715394_gshared (Collection_1_t668402608 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m628715394(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t668402608 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m628715394_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m2167491777_gshared (Collection_1_t668402608 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2167491777(__this, ___item0, method) ((  bool (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_Remove_m2167491777_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3465755445_gshared (Collection_1_t668402608 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3465755445(__this, ___index0, method) ((  void (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3465755445_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1162636785_gshared (Collection_1_t668402608 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1162636785(__this, ___index0, method) ((  void (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1162636785_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m4186124235_gshared (Collection_1_t668402608 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m4186124235(__this, method) ((  int32_t (*) (Collection_1_t668402608 *, const MethodInfo*))Collection_1_get_Count_m4186124235_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3648747785_gshared (Collection_1_t668402608 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3648747785(__this, ___index0, method) ((  int32_t (*) (Collection_1_t668402608 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3648747785_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3585441194_gshared (Collection_1_t668402608 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3585441194(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t668402608 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3585441194_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3497165541_gshared (Collection_1_t668402608 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3497165541(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t668402608 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m3497165541_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2971943904_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2971943904(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2971943904_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m374172304_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m374172304(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m374172304_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2176038212_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2176038212(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2176038212_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m198637818_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m198637818(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m198637818_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ProfileNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3926796881_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3926796881(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3926796881_gshared)(__this /* static, unused */, ___list0, method)
