﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SamsungAppsCallbackMonoBehaviour
struct SamsungAppsCallbackMonoBehaviour_t2214262867;
// Unibill.Impl.SamsungAppsBillingService
struct SamsungAppsBillingService_t2236131154;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2236131154.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SamsungAppsCallbackMonoBehaviour::.ctor()
extern "C"  void SamsungAppsCallbackMonoBehaviour__ctor_m1463639744 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::Start()
extern "C"  void SamsungAppsCallbackMonoBehaviour_Start_m2656832896 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::initialise(Unibill.Impl.SamsungAppsBillingService)
extern "C"  void SamsungAppsCallbackMonoBehaviour_initialise_m120503622 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, SamsungAppsBillingService_t2236131154 * ___samsung0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::onProductListReceived(System.String)
extern "C"  void SamsungAppsCallbackMonoBehaviour_onProductListReceived_m659524637 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, String_t* ___productCSVString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::onPurchaseFailed(System.String)
extern "C"  void SamsungAppsCallbackMonoBehaviour_onPurchaseFailed_m3847692755 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::onPurchaseSucceeded(System.String)
extern "C"  void SamsungAppsCallbackMonoBehaviour_onPurchaseSucceeded_m2929365971 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::onPurchaseCancelled(System.String)
extern "C"  void SamsungAppsCallbackMonoBehaviour_onPurchaseCancelled_m710851687 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::onTransactionsRestored(System.String)
extern "C"  void SamsungAppsCallbackMonoBehaviour_onTransactionsRestored_m1653075734 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, String_t* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SamsungAppsCallbackMonoBehaviour::onInitFail()
extern "C"  void SamsungAppsCallbackMonoBehaviour_onInitFail_m938099741 (SamsungAppsCallbackMonoBehaviour_t2214262867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
