﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct Dictionary_2_t283153520;
// System.Collections.Generic.IEqualityComparer`1<Unibill.Impl.BillingPlatform>
struct IEqualityComparer_1_t4059659308;
// System.Collections.Generic.IDictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct IDictionary_2_t2577204237;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<Unibill.Impl.BillingPlatform>
struct ICollection_1_t1504134539;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>[]
struct KeyValuePair_2U5BU5D_t213004339;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>>
struct IEnumerator_1_t4105957161;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<Unibill.Impl.BillingPlatform,System.Object>
struct KeyCollection_t2766651291;
// System.Collections.Generic.Dictionary`2/ValueCollection<Unibill.Impl.BillingPlatform,System.Object>
struct ValueCollection_t3281180659;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22335466038.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1603178222.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1033682712_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1033682712(__this, method) ((  void (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2__ctor_m1033682712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3892128955_gshared (Dictionary_2_t283153520 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3892128955(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3892128955_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1468234022_gshared (Dictionary_2_t283153520 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1468234022(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1468234022_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2028518121_gshared (Dictionary_2_t283153520 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2028518121(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t283153520 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2028518121_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2287308185_gshared (Dictionary_2_t283153520 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2287308185(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2287308185_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m4161368692_gshared (Dictionary_2_t283153520 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m4161368692(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t283153520 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4161368692_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3453210779_gshared (Dictionary_2_t283153520 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3453210779(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t283153520 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m3453210779_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1060288044_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1060288044(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1060288044_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3916247020_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3916247020(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3916247020_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1277119262_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1277119262(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1277119262_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3032645168_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3032645168(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3032645168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3480370663_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3480370663(__this, method) ((  bool (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3480370663_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2582494956_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2582494956(__this, method) ((  bool (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2582494956_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2302145102_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2302145102(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2302145102_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1554593571_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1554593571(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1554593571_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2200873390_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2200873390(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2200873390_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2829477718_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2829477718(__this, ___key0, method) ((  bool (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2829477718_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m4100914631_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m4100914631(__this, ___key0, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m4100914631_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2269030336_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2269030336(__this, method) ((  bool (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2269030336_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3947691414_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3947691414(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3947691414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m660903790_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m660903790(__this, method) ((  bool (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m660903790_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m522799763_gshared (Dictionary_2_t283153520 * __this, KeyValuePair_2_t2335466038  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m522799763(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t283153520 *, KeyValuePair_2_t2335466038 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m522799763_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1213216573_gshared (Dictionary_2_t283153520 * __this, KeyValuePair_2_t2335466038  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1213216573(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t283153520 *, KeyValuePair_2_t2335466038 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1213216573_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3137020591_gshared (Dictionary_2_t283153520 * __this, KeyValuePair_2U5BU5D_t213004339* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3137020591(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t283153520 *, KeyValuePair_2U5BU5D_t213004339*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3137020591_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3725783220_gshared (Dictionary_2_t283153520 * __this, KeyValuePair_2_t2335466038  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3725783220(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t283153520 *, KeyValuePair_2_t2335466038 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3725783220_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2661533040_gshared (Dictionary_2_t283153520 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2661533040(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2661533040_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m304240821_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m304240821(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m304240821_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m779152212_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m779152212(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m779152212_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1671343743_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1671343743(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1671343743_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1200402556_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1200402556(__this, method) ((  int32_t (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_get_Count_m1200402556_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2940552023_gshared (Dictionary_2_t283153520 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2940552023(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2940552023_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1539755722_gshared (Dictionary_2_t283153520 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1539755722(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t283153520 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1539755722_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2276643148_gshared (Dictionary_2_t283153520 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2276643148(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t283153520 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2276643148_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2162453961_gshared (Dictionary_2_t283153520 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2162453961(__this, ___size0, method) ((  void (*) (Dictionary_2_t283153520 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2162453961_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2226368815_gshared (Dictionary_2_t283153520 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2226368815(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2226368815_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2335466038  Dictionary_2_make_pair_m1267686829_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1267686829(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2335466038  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1267686829_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1557822833_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1557822833(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1557822833_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3224578321_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3224578321(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3224578321_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2955700638_gshared (Dictionary_2_t283153520 * __this, KeyValuePair_2U5BU5D_t213004339* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2955700638(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t283153520 *, KeyValuePair_2U5BU5D_t213004339*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2955700638_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1013814526_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1013814526(__this, method) ((  void (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_Resize_m1013814526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1398283805_gshared (Dictionary_2_t283153520 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1398283805(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t283153520 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1398283805_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3467983289_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3467983289(__this, method) ((  void (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_Clear_m3467983289_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1872730615_gshared (Dictionary_2_t283153520 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1872730615(__this, ___key0, method) ((  bool (*) (Dictionary_2_t283153520 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1872730615_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2350424383_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2350424383(__this, ___value0, method) ((  bool (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2350424383_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m923519668_gshared (Dictionary_2_t283153520 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m923519668(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t283153520 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m923519668_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2108121266_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2108121266(__this, ___sender0, method) ((  void (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2108121266_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2524929357_gshared (Dictionary_2_t283153520 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2524929357(__this, ___key0, method) ((  bool (*) (Dictionary_2_t283153520 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2524929357_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3188225182_gshared (Dictionary_2_t283153520 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3188225182(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t283153520 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3188225182_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::get_Keys()
extern "C"  KeyCollection_t2766651291 * Dictionary_2_get_Keys_m4228095987_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4228095987(__this, method) ((  KeyCollection_t2766651291 * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_get_Keys_m4228095987_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::get_Values()
extern "C"  ValueCollection_t3281180659 * Dictionary_2_get_Values_m3632018995_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3632018995(__this, method) ((  ValueCollection_t3281180659 * (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_get_Values_m3632018995_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1857544756_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1857544756(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1857544756_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1294699572_gshared (Dictionary_2_t283153520 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1294699572(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t283153520 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1294699572_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2500935886_gshared (Dictionary_2_t283153520 * __this, KeyValuePair_2_t2335466038  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2500935886(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t283153520 *, KeyValuePair_2_t2335466038 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2500935886_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1603178222  Dictionary_2_GetEnumerator_m2395652429_gshared (Dictionary_2_t283153520 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2395652429(__this, method) ((  Enumerator_t1603178222  (*) (Dictionary_2_t283153520 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2395652429_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__2_m1514025868_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m1514025868(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m1514025868_gshared)(__this /* static, unused */, ___key0, ___value1, method)
