﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PUToastUI/<DisplayWait>c__Iterator0
struct U3CDisplayWaitU3Ec__Iterator0_t4015913167;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PUToastUI/<DisplayWait>c__Iterator0::.ctor()
extern "C"  void U3CDisplayWaitU3Ec__Iterator0__ctor_m107943758 (U3CDisplayWaitU3Ec__Iterator0_t4015913167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PUToastUI/<DisplayWait>c__Iterator0::MoveNext()
extern "C"  bool U3CDisplayWaitU3Ec__Iterator0_MoveNext_m3370297566 (U3CDisplayWaitU3Ec__Iterator0_t4015913167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PUToastUI/<DisplayWait>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDisplayWaitU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3141150784 (U3CDisplayWaitU3Ec__Iterator0_t4015913167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PUToastUI/<DisplayWait>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDisplayWaitU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1237704472 (U3CDisplayWaitU3Ec__Iterator0_t4015913167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI/<DisplayWait>c__Iterator0::Dispose()
extern "C"  void U3CDisplayWaitU3Ec__Iterator0_Dispose_m326477201 (U3CDisplayWaitU3Ec__Iterator0_t4015913167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI/<DisplayWait>c__Iterator0::Reset()
extern "C"  void U3CDisplayWaitU3Ec__Iterator0_Reset_m2335157683 (U3CDisplayWaitU3Ec__Iterator0_t4015913167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
