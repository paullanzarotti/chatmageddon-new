﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.WP8BillingService
struct WP8BillingService_t394118855;
// unibill.Dummy.IWindowsIAP
struct IWindowsIAP_t2654797964;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;
// unibill.Dummy.Product[]
struct ProductU5BU5D_t4190069746;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// PurchasableItem
struct PurchasableItem_t3963353899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TransactionDatabase201476183.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void Unibill.Impl.WP8BillingService::.ctor(unibill.Dummy.IWindowsIAP,Unibill.Impl.UnibillConfiguration,Unibill.Impl.ProductIdRemapper,TransactionDatabase,Uniject.ILogger)
extern "C"  void WP8BillingService__ctor_m404951761 (WP8BillingService_t394118855 * __this, Il2CppObject * ___wp80, UnibillConfiguration_t2915611853 * ___config1, ProductIdRemapper_t3313438456 * ___remapper2, TransactionDatabase_t201476183 * ___tDb3, Il2CppObject * ___logger4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void WP8BillingService_initialise_m226189335 (WP8BillingService_t394118855 * __this, Il2CppObject * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::init(System.Int32)
extern "C"  void WP8BillingService_init_m669603910 (WP8BillingService_t394118855 * __this, int32_t ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::log(System.String)
extern "C"  void WP8BillingService_log_m2499499983 (WP8BillingService_t394118855 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::purchase(System.String,System.String)
extern "C"  void WP8BillingService_purchase_m2428115440 (WP8BillingService_t394118855 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::restoreTransactions()
extern "C"  void WP8BillingService_restoreTransactions_m55470394 (WP8BillingService_t394118855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::enumerateLicenses()
extern "C"  void WP8BillingService_enumerateLicenses_m3490508925 (WP8BillingService_t394118855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::logError(System.String)
extern "C"  void WP8BillingService_logError_m228145007 (WP8BillingService_t394118855 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::OnProductListReceived(unibill.Dummy.Product[])
extern "C"  void WP8BillingService_OnProductListReceived_m94414956 (WP8BillingService_t394118855 * __this, ProductU5BU5D_t4190069746* ___products0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::RunOnUIThread(System.Action`1<System.Int32>)
extern "C"  void WP8BillingService_RunOnUIThread_m4213509466 (WP8BillingService_t394118855 * __this, Action_1_t1873676830 * ___act0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::OnPurchaseFailed(System.String,System.String)
extern "C"  void WP8BillingService_OnPurchaseFailed_m375393548 (WP8BillingService_t394118855 * __this, String_t* ___productId0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::OnPurchaseCancelled(System.String)
extern "C"  void WP8BillingService_OnPurchaseCancelled_m964037370 (WP8BillingService_t394118855 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::OnPurchaseSucceeded(System.String,System.String)
extern "C"  void WP8BillingService_OnPurchaseSucceeded_m2974859104 (WP8BillingService_t394118855 * __this, String_t* ___productId0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::OnPurchaseSucceeded(System.String)
extern "C"  void WP8BillingService_OnPurchaseSucceeded_m1380684160 (WP8BillingService_t394118855 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WP8BillingService::OnProductListError(System.String)
extern "C"  void WP8BillingService_OnProductListError_m111458015 (WP8BillingService_t394118855 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.WP8BillingService::hasReceipt(System.String)
extern "C"  bool WP8BillingService_hasReceipt_m2111092117 (WP8BillingService_t394118855 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.WP8BillingService::getReceipt(System.String)
extern "C"  String_t* WP8BillingService_getReceipt_m3911882634 (WP8BillingService_t394118855 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.WP8BillingService::<OnProductListReceived>m__0(PurchasableItem)
extern "C"  String_t* WP8BillingService_U3COnProductListReceivedU3Em__0_m3295610405 (WP8BillingService_t394118855 * __this, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
