﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalManagersController
struct GlobalManagersController_t3516162077;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void GlobalManagersController::.ctor()
extern "C"  void GlobalManagersController__ctor_m1059665442 (GlobalManagersController_t3516162077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalManagersController::AddManager(System.String,UnityEngine.GameObject)
extern "C"  void GlobalManagersController_AddManager_m413210484 (GlobalManagersController_t3516162077 * __this, String_t* ___mKey0, GameObject_t1756533147 * ___mObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalManagersController::RemoveManager(System.String)
extern "C"  bool GlobalManagersController_RemoveManager_m2965643371 (GlobalManagersController_t3516162077 * __this, String_t* ___mKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
