﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Converter_2_gen2114790100MethodDeclarations.h"

// System.Void System.Converter`2<System.Char,System.String>::.ctor(System.Object,System.IntPtr)
#define Converter_2__ctor_m2092832237(__this, ___object0, ___method1, method) ((  void (*) (Converter_2_t1454561038 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Converter_2__ctor_m3723121118_gshared)(__this, ___object0, ___method1, method)
// TOutput System.Converter`2<System.Char,System.String>::Invoke(TInput)
#define Converter_2_Invoke_m3529852232(__this, ___input0, method) ((  String_t* (*) (Converter_2_t1454561038 *, Il2CppChar, const MethodInfo*))Converter_2_Invoke_m1040764222_gshared)(__this, ___input0, method)
// System.IAsyncResult System.Converter`2<System.Char,System.String>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
#define Converter_2_BeginInvoke_m863950479(__this, ___input0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Converter_2_t1454561038 *, Il2CppChar, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Converter_2_BeginInvoke_m1449120299_gshared)(__this, ___input0, ___callback1, ___object2, method)
// TOutput System.Converter`2<System.Char,System.String>::EndInvoke(System.IAsyncResult)
#define Converter_2_EndInvoke_m2072187080(__this, ___result0, method) ((  String_t* (*) (Converter_2_t1454561038 *, Il2CppObject *, const MethodInfo*))Converter_2_EndInvoke_m1621881278_gshared)(__this, ___result0, method)
