﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountryCodeItemUIScaler
struct CountryCodeItemUIScaler_t2043820862;

#include "codegen/il2cpp-codegen.h"

// System.Void CountryCodeItemUIScaler::.ctor()
extern "C"  void CountryCodeItemUIScaler__ctor_m1521932077 (CountryCodeItemUIScaler_t2043820862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeItemUIScaler::GlobalUIScale()
extern "C"  void CountryCodeItemUIScaler_GlobalUIScale_m1465170832 (CountryCodeItemUIScaler_t2043820862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
