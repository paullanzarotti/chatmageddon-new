﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SettingsManager
struct SettingsManager_t2519859232;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AboutAndHelpType4001655409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0
struct  U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107  : public Il2CppObject
{
public:
	// AboutAndHelpType SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0::type
	int32_t ___type_0;
	// SettingsManager SettingsManager/<GetAboutAndHelpInformation>c__AnonStorey0::$this
	SettingsManager_t2519859232 * ___U24this_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107, ___U24this_1)); }
	inline SettingsManager_t2519859232 * get_U24this_1() const { return ___U24this_1; }
	inline SettingsManager_t2519859232 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SettingsManager_t2519859232 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
