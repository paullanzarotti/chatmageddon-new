﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenderUIScaler
struct GenderUIScaler_t2269357119;

#include "codegen/il2cpp-codegen.h"

// System.Void GenderUIScaler::.ctor()
extern "C"  void GenderUIScaler__ctor_m1852532790 (GenderUIScaler_t2269357119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderUIScaler::GlobalUIScale()
extern "C"  void GenderUIScaler_GlobalUIScale_m3339774225 (GenderUIScaler_t2269357119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
