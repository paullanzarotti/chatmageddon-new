﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsXMLEnum
struct OnlineMapsXMLEnum_t1817127220;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"

// System.Void OnlineMapsXMLEnum::.ctor(OnlineMapsXML)
extern "C"  void OnlineMapsXMLEnum__ctor_m2959656132 (OnlineMapsXMLEnum_t1817127220 * __this, OnlineMapsXML_t3341520387 * ___el0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsXMLEnum::MoveNext()
extern "C"  bool OnlineMapsXMLEnum_MoveNext_m3217041219 (OnlineMapsXMLEnum_t1817127220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsXMLEnum::Reset()
extern "C"  void OnlineMapsXMLEnum_Reset_m2114823196 (OnlineMapsXMLEnum_t1817127220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsXMLEnum::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * OnlineMapsXMLEnum_System_Collections_IEnumerator_get_Current_m1443662855 (OnlineMapsXMLEnum_t1817127220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsXML OnlineMapsXMLEnum::get_Current()
extern "C"  OnlineMapsXML_t3341520387 * OnlineMapsXMLEnum_get_Current_m609383359 (OnlineMapsXMLEnum_t1817127220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
