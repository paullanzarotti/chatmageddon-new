﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatNavigationController
struct ChatNavigationController_t880507216;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void ChatNavigationController::.ctor()
extern "C"  void ChatNavigationController__ctor_m3294692867 (ChatNavigationController_t880507216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::SetupStartScreen(ChatNavScreen)
extern "C"  void ChatNavigationController_SetupStartScreen_m3408887559 (ChatNavigationController_t880507216 * __this, int32_t ___startScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::AddPreviousScreen(ChatNavScreen)
extern "C"  void ChatNavigationController_AddPreviousScreen_m2070735548 (ChatNavigationController_t880507216 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::RemovePreviousNavScreen(ChatNavScreen)
extern "C"  void ChatNavigationController_RemovePreviousNavScreen_m2542741600 (ChatNavigationController_t880507216 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::NavigateBackwards()
extern "C"  void ChatNavigationController_NavigateBackwards_m3467150928 (ChatNavigationController_t880507216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::NavigateForwards(ChatNavScreen)
extern "C"  void ChatNavigationController_NavigateForwards_m2753466919 (ChatNavigationController_t880507216 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatNavigationController::ValidateNavigation(ChatNavScreen,NavigationDirection)
extern "C"  bool ChatNavigationController_ValidateNavigation_m1261355883 (ChatNavigationController_t880507216 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::LoadNavScreen(ChatNavScreen,NavigationDirection,System.Boolean)
extern "C"  void ChatNavigationController_LoadNavScreen_m3510847895 (ChatNavigationController_t880507216 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::UnloadNavScreen(ChatNavScreen,NavigationDirection)
extern "C"  void ChatNavigationController_UnloadNavScreen_m1076198051 (ChatNavigationController_t880507216 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigationController::PopulateTitleBar(ChatNavScreen)
extern "C"  void ChatNavigationController_PopulateTitleBar_m1794382887 (ChatNavigationController_t880507216 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
