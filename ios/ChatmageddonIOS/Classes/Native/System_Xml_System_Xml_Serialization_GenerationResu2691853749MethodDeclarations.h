﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.GenerationResult
struct GenerationResult_t2691853749;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Serialization.GenerationResult::.ctor()
extern "C"  void GenerationResult__ctor_m1803334774 (GenerationResult_t2691853749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
