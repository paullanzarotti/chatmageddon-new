﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0
struct U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0::.ctor()
extern "C"  void U3CWaitToLaunchMissileU3Ec__Iterator0__ctor_m2010255791 (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToLaunchMissileU3Ec__Iterator0_MoveNext_m286059813 (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToLaunchMissileU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2111774169 (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToLaunchMissileU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m787146257 (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0::Dispose()
extern "C"  void U3CWaitToLaunchMissileU3Ec__Iterator0_Dispose_m2514736386 (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController/<WaitToLaunchMissile>c__Iterator0::Reset()
extern "C"  void U3CWaitToLaunchMissileU3Ec__Iterator0_Reset_m2192287800 (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
