﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<VerifyInAppPurchase>c__AnonStorey34
struct U3CVerifyInAppPurchaseU3Ec__AnonStorey34_t4078131213;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<VerifyInAppPurchase>c__AnonStorey34::.ctor()
extern "C"  void U3CVerifyInAppPurchaseU3Ec__AnonStorey34__ctor_m126874312 (U3CVerifyInAppPurchaseU3Ec__AnonStorey34_t4078131213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<VerifyInAppPurchase>c__AnonStorey34::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CVerifyInAppPurchaseU3Ec__AnonStorey34_U3CU3Em__0_m885078753 (U3CVerifyInAppPurchaseU3Ec__AnonStorey34_t4078131213 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
