﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameLabelPopulator
struct GameLabelPopulator_t3519752206;
// System.String
struct String_t;
// UISprite
struct UISprite_t603616735;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_LableType1010844952.h"
#include "AssemblyU2DCSharp_UISprite603616735.h"

// System.Void GameLabelPopulator::.ctor()
extern "C"  void GameLabelPopulator__ctor_m3553674041 (GameLabelPopulator_t3519752206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLabelPopulator::PopulateDescription(System.String,LableType)
extern "C"  void GameLabelPopulator_PopulateDescription_m2524520485 (GameLabelPopulator_t3519752206 * __this, String_t* ___text0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLabelPopulator::HideDescription(System.Boolean)
extern "C"  void GameLabelPopulator_HideDescription_m4207579666 (GameLabelPopulator_t3519752206 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLabelPopulator::PopulateAmount(System.String,LableType)
extern "C"  void GameLabelPopulator_PopulateAmount_m4267834647 (GameLabelPopulator_t3519752206 * __this, String_t* ___text0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLabelPopulator::HideAmount(System.Boolean)
extern "C"  void GameLabelPopulator_HideAmount_m3658962692 (GameLabelPopulator_t3519752206 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLabelPopulator::SetSpriteColour(LableType,UISprite)
extern "C"  void GameLabelPopulator_SetSpriteColour_m1016404373 (GameLabelPopulator_t3519752206 * __this, int32_t ___type0, UISprite_t603616735 * ___sprite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
