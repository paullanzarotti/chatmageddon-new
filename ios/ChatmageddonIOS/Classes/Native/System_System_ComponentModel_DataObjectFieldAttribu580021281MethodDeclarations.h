﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.DataObjectFieldAttribute
struct DataObjectFieldAttribute_t580021281;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.DataObjectFieldAttribute::.ctor(System.Boolean)
extern "C"  void DataObjectFieldAttribute__ctor_m1798496831 (DataObjectFieldAttribute_t580021281 * __this, bool ___primaryKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DataObjectFieldAttribute::.ctor(System.Boolean,System.Boolean)
extern "C"  void DataObjectFieldAttribute__ctor_m2514479952 (DataObjectFieldAttribute_t580021281 * __this, bool ___primaryKey0, bool ___isIdentity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DataObjectFieldAttribute::.ctor(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void DataObjectFieldAttribute__ctor_m84243379 (DataObjectFieldAttribute_t580021281 * __this, bool ___primaryKey0, bool ___isIdentity1, bool ___isNullable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DataObjectFieldAttribute::.ctor(System.Boolean,System.Boolean,System.Boolean,System.Int32)
extern "C"  void DataObjectFieldAttribute__ctor_m807470598 (DataObjectFieldAttribute_t580021281 * __this, bool ___primaryKey0, bool ___isIdentity1, bool ___isNullable2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectFieldAttribute::get_IsIdentity()
extern "C"  bool DataObjectFieldAttribute_get_IsIdentity_m3371054977 (DataObjectFieldAttribute_t580021281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectFieldAttribute::get_IsNullable()
extern "C"  bool DataObjectFieldAttribute_get_IsNullable_m4213696882 (DataObjectFieldAttribute_t580021281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.DataObjectFieldAttribute::get_Length()
extern "C"  int32_t DataObjectFieldAttribute_get_Length_m1711839971 (DataObjectFieldAttribute_t580021281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectFieldAttribute::get_PrimaryKey()
extern "C"  bool DataObjectFieldAttribute_get_PrimaryKey_m2998478394 (DataObjectFieldAttribute_t580021281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectFieldAttribute::Equals(System.Object)
extern "C"  bool DataObjectFieldAttribute_Equals_m1381408129 (DataObjectFieldAttribute_t580021281 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.DataObjectFieldAttribute::GetHashCode()
extern "C"  int32_t DataObjectFieldAttribute_GetHashCode_m435610999 (DataObjectFieldAttribute_t580021281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
