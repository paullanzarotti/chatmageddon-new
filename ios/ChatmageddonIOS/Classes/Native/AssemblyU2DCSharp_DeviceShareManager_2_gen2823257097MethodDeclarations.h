﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceShareManager`2<System.Object,System.Object>
struct DeviceShareManager_2_t2823257097;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DeviceShareManager`2<System.Object,System.Object>::.ctor()
extern "C"  void DeviceShareManager_2__ctor_m1208203965_gshared (DeviceShareManager_2_t2823257097 * __this, const MethodInfo* method);
#define DeviceShareManager_2__ctor_m1208203965(__this, method) ((  void (*) (DeviceShareManager_2_t2823257097 *, const MethodInfo*))DeviceShareManager_2__ctor_m1208203965_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,System.Object>::OpenUI(RequestLocation)
extern "C"  void DeviceShareManager_2_OpenUI_m3354705917_gshared (DeviceShareManager_2_t2823257097 * __this, Il2CppObject * ___location0, const MethodInfo* method);
#define DeviceShareManager_2_OpenUI_m3354705917(__this, ___location0, method) ((  void (*) (DeviceShareManager_2_t2823257097 *, Il2CppObject *, const MethodInfo*))DeviceShareManager_2_OpenUI_m3354705917_gshared)(__this, ___location0, method)
// System.Void DeviceShareManager`2<System.Object,System.Object>::CloseUI()
extern "C"  void DeviceShareManager_2_CloseUI_m3269699575_gshared (DeviceShareManager_2_t2823257097 * __this, const MethodInfo* method);
#define DeviceShareManager_2_CloseUI_m3269699575(__this, method) ((  void (*) (DeviceShareManager_2_t2823257097 *, const MethodInfo*))DeviceShareManager_2_CloseUI_m3269699575_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,System.Object>::SetLocationAtlas(RequestLocation)
extern "C"  void DeviceShareManager_2_SetLocationAtlas_m812648081_gshared (DeviceShareManager_2_t2823257097 * __this, Il2CppObject * ___location0, const MethodInfo* method);
#define DeviceShareManager_2_SetLocationAtlas_m812648081(__this, ___location0, method) ((  void (*) (DeviceShareManager_2_t2823257097 *, Il2CppObject *, const MethodInfo*))DeviceShareManager_2_SetLocationAtlas_m812648081_gshared)(__this, ___location0, method)
// System.Boolean DeviceShareManager`2<System.Object,System.Object>::CheckEmailAvailability()
extern "C"  bool DeviceShareManager_2_CheckEmailAvailability_m1609062354_gshared (DeviceShareManager_2_t2823257097 * __this, const MethodInfo* method);
#define DeviceShareManager_2_CheckEmailAvailability_m1609062354(__this, method) ((  bool (*) (DeviceShareManager_2_t2823257097 *, const MethodInfo*))DeviceShareManager_2_CheckEmailAvailability_m1609062354_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,System.Object>::SendEmail(System.String,System.String,System.String,System.Boolean)
extern "C"  void DeviceShareManager_2_SendEmail_m1253888944_gshared (DeviceShareManager_2_t2823257097 * __this, String_t* ___toAddress0, String_t* ___subject1, String_t* ___messageBody2, bool ___isHTML3, const MethodInfo* method);
#define DeviceShareManager_2_SendEmail_m1253888944(__this, ___toAddress0, ___subject1, ___messageBody2, ___isHTML3, method) ((  void (*) (DeviceShareManager_2_t2823257097 *, String_t*, String_t*, String_t*, bool, const MethodInfo*))DeviceShareManager_2_SendEmail_m1253888944_gshared)(__this, ___toAddress0, ___subject1, ___messageBody2, ___isHTML3, method)
// System.Boolean DeviceShareManager`2<System.Object,System.Object>::CheckSMSAvailability()
extern "C"  bool DeviceShareManager_2_CheckSMSAvailability_m153421443_gshared (DeviceShareManager_2_t2823257097 * __this, const MethodInfo* method);
#define DeviceShareManager_2_CheckSMSAvailability_m153421443(__this, method) ((  bool (*) (DeviceShareManager_2_t2823257097 *, const MethodInfo*))DeviceShareManager_2_CheckSMSAvailability_m153421443_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,System.Object>::SendSMS(System.String)
extern "C"  void DeviceShareManager_2_SendSMS_m194035870_gshared (DeviceShareManager_2_t2823257097 * __this, String_t* ___messageBody0, const MethodInfo* method);
#define DeviceShareManager_2_SendSMS_m194035870(__this, ___messageBody0, method) ((  void (*) (DeviceShareManager_2_t2823257097 *, String_t*, const MethodInfo*))DeviceShareManager_2_SendSMS_m194035870_gshared)(__this, ___messageBody0, method)
