﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetMissileInventory>c__AnonStorey36
struct U3CGetMissileInventoryU3Ec__AnonStorey36_t519334381;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetMissileInventory>c__AnonStorey36::.ctor()
extern "C"  void U3CGetMissileInventoryU3Ec__AnonStorey36__ctor_m262940840 (U3CGetMissileInventoryU3Ec__AnonStorey36_t519334381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetMissileInventory>c__AnonStorey36::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetMissileInventoryU3Ec__AnonStorey36_U3CU3Em__0_m957698049 (U3CGetMissileInventoryU3Ec__AnonStorey36_t519334381 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
