﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UIInput
struct UIInput_t860674234;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VerificationScreen
struct  VerificationScreen_t1800753931  : public NavigationScreen_t2333230110
{
public:
	// UILabel VerificationScreen::phonenumLabel
	UILabel_t1795115428 * ___phonenumLabel_3;
	// UIInput VerificationScreen::codeinput
	UIInput_t860674234 * ___codeinput_4;
	// UnityEngine.GameObject VerificationScreen::reSendButton
	GameObject_t1756533147 * ___reSendButton_5;
	// System.Boolean VerificationScreen::verificationUIclosing
	bool ___verificationUIclosing_6;

public:
	inline static int32_t get_offset_of_phonenumLabel_3() { return static_cast<int32_t>(offsetof(VerificationScreen_t1800753931, ___phonenumLabel_3)); }
	inline UILabel_t1795115428 * get_phonenumLabel_3() const { return ___phonenumLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_phonenumLabel_3() { return &___phonenumLabel_3; }
	inline void set_phonenumLabel_3(UILabel_t1795115428 * value)
	{
		___phonenumLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___phonenumLabel_3, value);
	}

	inline static int32_t get_offset_of_codeinput_4() { return static_cast<int32_t>(offsetof(VerificationScreen_t1800753931, ___codeinput_4)); }
	inline UIInput_t860674234 * get_codeinput_4() const { return ___codeinput_4; }
	inline UIInput_t860674234 ** get_address_of_codeinput_4() { return &___codeinput_4; }
	inline void set_codeinput_4(UIInput_t860674234 * value)
	{
		___codeinput_4 = value;
		Il2CppCodeGenWriteBarrier(&___codeinput_4, value);
	}

	inline static int32_t get_offset_of_reSendButton_5() { return static_cast<int32_t>(offsetof(VerificationScreen_t1800753931, ___reSendButton_5)); }
	inline GameObject_t1756533147 * get_reSendButton_5() const { return ___reSendButton_5; }
	inline GameObject_t1756533147 ** get_address_of_reSendButton_5() { return &___reSendButton_5; }
	inline void set_reSendButton_5(GameObject_t1756533147 * value)
	{
		___reSendButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___reSendButton_5, value);
	}

	inline static int32_t get_offset_of_verificationUIclosing_6() { return static_cast<int32_t>(offsetof(VerificationScreen_t1800753931, ___verificationUIclosing_6)); }
	inline bool get_verificationUIclosing_6() const { return ___verificationUIclosing_6; }
	inline bool* get_address_of_verificationUIclosing_6() { return &___verificationUIclosing_6; }
	inline void set_verificationUIclosing_6(bool value)
	{
		___verificationUIclosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
