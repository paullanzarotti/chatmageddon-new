﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChooseExistingButton
struct ChooseExistingButton_t274093920;

#include "codegen/il2cpp-codegen.h"

// System.Void ChooseExistingButton::.ctor()
extern "C"  void ChooseExistingButton__ctor_m1267469203 (ChooseExistingButton_t274093920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChooseExistingButton::OnButtonClick()
extern "C"  void ChooseExistingButton_OnButtonClick_m2215283624 (ChooseExistingButton_t274093920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
