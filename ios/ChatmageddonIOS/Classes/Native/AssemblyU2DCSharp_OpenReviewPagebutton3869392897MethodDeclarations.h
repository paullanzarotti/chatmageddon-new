﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenReviewPagebutton
struct OpenReviewPagebutton_t3869392897;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenReviewPagebutton::.ctor()
extern "C"  void OpenReviewPagebutton__ctor_m969562334 (OpenReviewPagebutton_t3869392897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenReviewPagebutton::OnButtonClick()
extern "C"  void OpenReviewPagebutton_OnButtonClick_m1522803701 (OpenReviewPagebutton_t3869392897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenReviewPagebutton::OpenReviewPage()
extern "C"  void OpenReviewPagebutton_OpenReviewPage_m1668071013 (OpenReviewPagebutton_t3869392897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
