﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PickerPopper
struct PickerPopper_t344149016;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickerPopper/<LerpBackgroundColor>c__Iterator3
struct  U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843  : public Il2CppObject
{
public:
	// UnityEngine.Color PickerPopper/<LerpBackgroundColor>c__Iterator3::<initValue>__0
	Color_t2020392075  ___U3CinitValueU3E__0_0;
	// System.Single PickerPopper/<LerpBackgroundColor>c__Iterator3::<i>__1
	float ___U3CiU3E__1_1;
	// System.Single PickerPopper/<LerpBackgroundColor>c__Iterator3::duration
	float ___duration_2;
	// System.Single PickerPopper/<LerpBackgroundColor>c__Iterator3::<rate>__2
	float ___U3CrateU3E__2_3;
	// UnityEngine.Color PickerPopper/<LerpBackgroundColor>c__Iterator3::targetColor
	Color_t2020392075  ___targetColor_4;
	// PickerPopper PickerPopper/<LerpBackgroundColor>c__Iterator3::$this
	PickerPopper_t344149016 * ___U24this_5;
	// System.Object PickerPopper/<LerpBackgroundColor>c__Iterator3::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean PickerPopper/<LerpBackgroundColor>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 PickerPopper/<LerpBackgroundColor>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CinitValueU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U3CinitValueU3E__0_0)); }
	inline Color_t2020392075  get_U3CinitValueU3E__0_0() const { return ___U3CinitValueU3E__0_0; }
	inline Color_t2020392075 * get_address_of_U3CinitValueU3E__0_0() { return &___U3CinitValueU3E__0_0; }
	inline void set_U3CinitValueU3E__0_0(Color_t2020392075  value)
	{
		___U3CinitValueU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U3CiU3E__1_1)); }
	inline float get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline float* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(float value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_U3CrateU3E__2_3() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U3CrateU3E__2_3)); }
	inline float get_U3CrateU3E__2_3() const { return ___U3CrateU3E__2_3; }
	inline float* get_address_of_U3CrateU3E__2_3() { return &___U3CrateU3E__2_3; }
	inline void set_U3CrateU3E__2_3(float value)
	{
		___U3CrateU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_targetColor_4() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___targetColor_4)); }
	inline Color_t2020392075  get_targetColor_4() const { return ___targetColor_4; }
	inline Color_t2020392075 * get_address_of_targetColor_4() { return &___targetColor_4; }
	inline void set_targetColor_4(Color_t2020392075  value)
	{
		___targetColor_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U24this_5)); }
	inline PickerPopper_t344149016 * get_U24this_5() const { return ___U24this_5; }
	inline PickerPopper_t344149016 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(PickerPopper_t344149016 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
