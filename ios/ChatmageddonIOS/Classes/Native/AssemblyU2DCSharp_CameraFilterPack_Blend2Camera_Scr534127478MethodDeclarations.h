﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Screen
struct CameraFilterPack_Blend2Camera_Screen_t534127478;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Screen::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Screen__ctor_m1077086621 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Screen::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Screen_get_material_m78423116 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Screen::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Screen_Start_m1336220021 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Screen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Screen_OnRenderImage_m591452197 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Screen::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Screen_OnValidate_m848818970 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Screen::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Screen_Update_m4053872884 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Screen::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Screen_OnEnable_m124806261 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Screen::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Screen_OnDisable_m1656899650 (CameraFilterPack_Blend2Camera_Screen_t534127478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
