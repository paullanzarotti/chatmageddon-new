﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.RegexCache/Entry
struct Entry_t1519365992;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.RegexCache/Entry::.ctor()
extern "C"  void Entry__ctor_m109491855 (Entry_t1519365992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
