﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>
struct U3CGetEnumeratorU3Ec__Iterator0_t2724673575;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3844325098_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m3844325098(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m3844325098_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2507656882_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2507656882(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2507656882_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color_t2020392075  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m260016607_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m260016607(__this, method) ((  Color_t2020392075  (*) (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m260016607_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1365599308_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1365599308(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1365599308_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3815790629_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3815790629(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3815790629_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<UnityEngine.Color>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2822556151_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m2822556151(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2724673575 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m2822556151_gshared)(__this, method)
