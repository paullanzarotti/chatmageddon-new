﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PhoneNumberNavScreen>
struct DefaultComparer_t2885397411;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PhoneNumberNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m1224640842_gshared (DefaultComparer_t2885397411 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1224640842(__this, method) ((  void (*) (DefaultComparer_t2885397411 *, const MethodInfo*))DefaultComparer__ctor_m1224640842_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PhoneNumberNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3555821305_gshared (DefaultComparer_t2885397411 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3555821305(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2885397411 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3555821305_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PhoneNumberNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2839980373_gshared (DefaultComparer_t2885397411 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2839980373(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2885397411 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2839980373_gshared)(__this, ___x0, ___y1, method)
