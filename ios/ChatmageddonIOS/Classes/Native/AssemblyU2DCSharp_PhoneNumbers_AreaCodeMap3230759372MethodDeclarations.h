﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.AreaCodeMap
struct AreaCodeMap_t3230759372;
// PhoneNumbers.AreaCodeMapStorageStrategy
struct AreaCodeMapStorageStrategy_t3709015996;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>
struct SortedDictionary_2_t2686657757;
// System.String
struct String_t;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_AreaCodeMapStorageS3709015996.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"

// System.Void PhoneNumbers.AreaCodeMap::.ctor()
extern "C"  void AreaCodeMap__ctor_m469135491 (AreaCodeMap_t3230759372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AreaCodeMapStorageStrategy PhoneNumbers.AreaCodeMap::getAreaCodeMapStorage()
extern "C"  AreaCodeMapStorageStrategy_t3709015996 * AreaCodeMap_getAreaCodeMapStorage_m2403670899 (AreaCodeMap_t3230759372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.AreaCodeMap::getSizeOfAreaCodeMapStorage(PhoneNumbers.AreaCodeMapStorageStrategy,System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>)
extern "C"  int32_t AreaCodeMap_getSizeOfAreaCodeMapStorage_m3749126697 (Il2CppObject * __this /* static, unused */, AreaCodeMapStorageStrategy_t3709015996 * ___mapStorage0, SortedDictionary_2_t2686657757 * ___areaCodeMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AreaCodeMapStorageStrategy PhoneNumbers.AreaCodeMap::createDefaultMapStorage()
extern "C"  AreaCodeMapStorageStrategy_t3709015996 * AreaCodeMap_createDefaultMapStorage_m1316746918 (AreaCodeMap_t3230759372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AreaCodeMapStorageStrategy PhoneNumbers.AreaCodeMap::createFlyweightMapStorage()
extern "C"  AreaCodeMapStorageStrategy_t3709015996 * AreaCodeMap_createFlyweightMapStorage_m3099706914 (AreaCodeMap_t3230759372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AreaCodeMapStorageStrategy PhoneNumbers.AreaCodeMap::getSmallerMapStorage(System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>)
extern "C"  AreaCodeMapStorageStrategy_t3709015996 * AreaCodeMap_getSmallerMapStorage_m2266855786 (AreaCodeMap_t3230759372 * __this, SortedDictionary_2_t2686657757 * ___areaCodeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.AreaCodeMap::readAreaCodeMap(System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>)
extern "C"  void AreaCodeMap_readAreaCodeMap_m851428928 (AreaCodeMap_t3230759372 * __this, SortedDictionary_2_t2686657757 * ___sortedAreaCodeMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AreaCodeMap::Lookup(PhoneNumbers.PhoneNumber)
extern "C"  String_t* AreaCodeMap_Lookup_m2793080089 (AreaCodeMap_t3230759372 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.AreaCodeMap::binarySearch(System.Int32,System.Int32,System.Int64)
extern "C"  int32_t AreaCodeMap_binarySearch_m406503496 (AreaCodeMap_t3230759372 * __this, int32_t ___start0, int32_t ___end1, int64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AreaCodeMap::ToString()
extern "C"  String_t* AreaCodeMap_ToString_m2795705064 (AreaCodeMap_t3230759372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
