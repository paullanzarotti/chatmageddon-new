﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.InstallerTypeAttribute
struct InstallerTypeAttribute_t2978264484;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.InstallerTypeAttribute::.ctor(System.String)
extern "C"  void InstallerTypeAttribute__ctor_m875732515 (InstallerTypeAttribute_t2978264484 * __this, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.InstallerTypeAttribute::.ctor(System.Type)
extern "C"  void InstallerTypeAttribute__ctor_m3492581070 (InstallerTypeAttribute_t2978264484 * __this, Type_t * ___installerType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.ComponentModel.InstallerTypeAttribute::get_InstallerType()
extern "C"  Type_t * InstallerTypeAttribute_get_InstallerType_m804090672 (InstallerTypeAttribute_t2978264484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.InstallerTypeAttribute::Equals(System.Object)
extern "C"  bool InstallerTypeAttribute_Equals_m2977698932 (InstallerTypeAttribute_t2978264484 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.InstallerTypeAttribute::GetHashCode()
extern "C"  int32_t InstallerTypeAttribute_GetHashCode_m2474448708 (InstallerTypeAttribute_t2978264484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
