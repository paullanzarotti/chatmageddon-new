﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.RawSamsungAppsBillingInterface
struct RawSamsungAppsBillingInterface_t3420523086;
// Unibill.Impl.SamsungAppsBillingService
struct SamsungAppsBillingService_t2236131154;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2236131154.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.RawSamsungAppsBillingInterface::.ctor()
extern "C"  void RawSamsungAppsBillingInterface__ctor_m106230438 (RawSamsungAppsBillingInterface_t3420523086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawSamsungAppsBillingInterface::initialise(Unibill.Impl.SamsungAppsBillingService)
extern "C"  void RawSamsungAppsBillingInterface_initialise_m3315738544 (RawSamsungAppsBillingInterface_t3420523086 * __this, SamsungAppsBillingService_t2236131154 * ___samsung0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawSamsungAppsBillingInterface::getProductList(System.String)
extern "C"  void RawSamsungAppsBillingInterface_getProductList_m4240159453 (RawSamsungAppsBillingInterface_t3420523086 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawSamsungAppsBillingInterface::initiatePurchaseRequest(System.String)
extern "C"  void RawSamsungAppsBillingInterface_initiatePurchaseRequest_m3163818903 (RawSamsungAppsBillingInterface_t3420523086 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawSamsungAppsBillingInterface::restoreTransactions()
extern "C"  void RawSamsungAppsBillingInterface_restoreTransactions_m334226719 (RawSamsungAppsBillingInterface_t3420523086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
