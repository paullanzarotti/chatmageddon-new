﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadarEffectScript
struct  RadarEffectScript_t3700381014  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material RadarEffectScript::lineMaterial
	Material_t193706927 * ___lineMaterial_2;
	// System.Single RadarEffectScript::xRadius
	float ___xRadius_3;
	// System.Single RadarEffectScript::yRadius
	float ___yRadius_4;
	// System.Int32 RadarEffectScript::segments
	int32_t ___segments_5;
	// System.Int32 RadarEffectScript::pointRotation
	int32_t ___pointRotation_6;

public:
	inline static int32_t get_offset_of_lineMaterial_2() { return static_cast<int32_t>(offsetof(RadarEffectScript_t3700381014, ___lineMaterial_2)); }
	inline Material_t193706927 * get_lineMaterial_2() const { return ___lineMaterial_2; }
	inline Material_t193706927 ** get_address_of_lineMaterial_2() { return &___lineMaterial_2; }
	inline void set_lineMaterial_2(Material_t193706927 * value)
	{
		___lineMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___lineMaterial_2, value);
	}

	inline static int32_t get_offset_of_xRadius_3() { return static_cast<int32_t>(offsetof(RadarEffectScript_t3700381014, ___xRadius_3)); }
	inline float get_xRadius_3() const { return ___xRadius_3; }
	inline float* get_address_of_xRadius_3() { return &___xRadius_3; }
	inline void set_xRadius_3(float value)
	{
		___xRadius_3 = value;
	}

	inline static int32_t get_offset_of_yRadius_4() { return static_cast<int32_t>(offsetof(RadarEffectScript_t3700381014, ___yRadius_4)); }
	inline float get_yRadius_4() const { return ___yRadius_4; }
	inline float* get_address_of_yRadius_4() { return &___yRadius_4; }
	inline void set_yRadius_4(float value)
	{
		___yRadius_4 = value;
	}

	inline static int32_t get_offset_of_segments_5() { return static_cast<int32_t>(offsetof(RadarEffectScript_t3700381014, ___segments_5)); }
	inline int32_t get_segments_5() const { return ___segments_5; }
	inline int32_t* get_address_of_segments_5() { return &___segments_5; }
	inline void set_segments_5(int32_t value)
	{
		___segments_5 = value;
	}

	inline static int32_t get_offset_of_pointRotation_6() { return static_cast<int32_t>(offsetof(RadarEffectScript_t3700381014, ___pointRotation_6)); }
	inline int32_t get_pointRotation_6() const { return ___pointRotation_6; }
	inline int32_t* get_address_of_pointRotation_6() { return &___pointRotation_6; }
	inline void set_pointRotation_6(int32_t value)
	{
		___pointRotation_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
