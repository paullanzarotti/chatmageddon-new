﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookUIManager
struct FacebookUIManager_t2097403185;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookUIManager::.ctor()
extern "C"  void FacebookUIManager__ctor_m182785685 (FacebookUIManager_t2097403185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
