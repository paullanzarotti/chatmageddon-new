﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuyActiveMissileButton
struct BuyActiveMissileButton_t2138134254;

#include "codegen/il2cpp-codegen.h"

// System.Void BuyActiveMissileButton::.ctor()
extern "C"  void BuyActiveMissileButton__ctor_m2046200495 (BuyActiveMissileButton_t2138134254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyActiveMissileButton::SetButtonActive(System.Int32)
extern "C"  void BuyActiveMissileButton_SetButtonActive_m1601445258 (BuyActiveMissileButton_t2138134254 * __this, int32_t ___inventoryAmount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyActiveMissileButton::OnButtonClick()
extern "C"  void BuyActiveMissileButton_OnButtonClick_m1786214722 (BuyActiveMissileButton_t2138134254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
