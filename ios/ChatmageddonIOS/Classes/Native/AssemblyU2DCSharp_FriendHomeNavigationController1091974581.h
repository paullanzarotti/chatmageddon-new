﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen79093462.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendHomeNavigationController
struct  FriendHomeNavigationController_t1091974581  : public NavigationController_2_t79093462
{
public:
	// UILabel FriendHomeNavigationController::innerLabel
	UILabel_t1795115428 * ___innerLabel_8;
	// UILabel FriendHomeNavigationController::outerLabel
	UILabel_t1795115428 * ___outerLabel_9;
	// UILabel FriendHomeNavigationController::otherLabel
	UILabel_t1795115428 * ___otherLabel_10;
	// UnityEngine.GameObject FriendHomeNavigationController::innerObject
	GameObject_t1756533147 * ___innerObject_11;
	// UnityEngine.GameObject FriendHomeNavigationController::outerObject
	GameObject_t1756533147 * ___outerObject_12;
	// UnityEngine.GameObject FriendHomeNavigationController::otherObject
	GameObject_t1756533147 * ___otherObject_13;
	// UnityEngine.Vector3 FriendHomeNavigationController::activePos
	Vector3_t2243707580  ___activePos_14;
	// UnityEngine.Vector3 FriendHomeNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_15;
	// UnityEngine.Vector3 FriendHomeNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_16;

public:
	inline static int32_t get_offset_of_innerLabel_8() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___innerLabel_8)); }
	inline UILabel_t1795115428 * get_innerLabel_8() const { return ___innerLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_innerLabel_8() { return &___innerLabel_8; }
	inline void set_innerLabel_8(UILabel_t1795115428 * value)
	{
		___innerLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___innerLabel_8, value);
	}

	inline static int32_t get_offset_of_outerLabel_9() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___outerLabel_9)); }
	inline UILabel_t1795115428 * get_outerLabel_9() const { return ___outerLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_outerLabel_9() { return &___outerLabel_9; }
	inline void set_outerLabel_9(UILabel_t1795115428 * value)
	{
		___outerLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___outerLabel_9, value);
	}

	inline static int32_t get_offset_of_otherLabel_10() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___otherLabel_10)); }
	inline UILabel_t1795115428 * get_otherLabel_10() const { return ___otherLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_otherLabel_10() { return &___otherLabel_10; }
	inline void set_otherLabel_10(UILabel_t1795115428 * value)
	{
		___otherLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___otherLabel_10, value);
	}

	inline static int32_t get_offset_of_innerObject_11() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___innerObject_11)); }
	inline GameObject_t1756533147 * get_innerObject_11() const { return ___innerObject_11; }
	inline GameObject_t1756533147 ** get_address_of_innerObject_11() { return &___innerObject_11; }
	inline void set_innerObject_11(GameObject_t1756533147 * value)
	{
		___innerObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___innerObject_11, value);
	}

	inline static int32_t get_offset_of_outerObject_12() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___outerObject_12)); }
	inline GameObject_t1756533147 * get_outerObject_12() const { return ___outerObject_12; }
	inline GameObject_t1756533147 ** get_address_of_outerObject_12() { return &___outerObject_12; }
	inline void set_outerObject_12(GameObject_t1756533147 * value)
	{
		___outerObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___outerObject_12, value);
	}

	inline static int32_t get_offset_of_otherObject_13() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___otherObject_13)); }
	inline GameObject_t1756533147 * get_otherObject_13() const { return ___otherObject_13; }
	inline GameObject_t1756533147 ** get_address_of_otherObject_13() { return &___otherObject_13; }
	inline void set_otherObject_13(GameObject_t1756533147 * value)
	{
		___otherObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___otherObject_13, value);
	}

	inline static int32_t get_offset_of_activePos_14() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___activePos_14)); }
	inline Vector3_t2243707580  get_activePos_14() const { return ___activePos_14; }
	inline Vector3_t2243707580 * get_address_of_activePos_14() { return &___activePos_14; }
	inline void set_activePos_14(Vector3_t2243707580  value)
	{
		___activePos_14 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_15() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___leftInactivePos_15)); }
	inline Vector3_t2243707580  get_leftInactivePos_15() const { return ___leftInactivePos_15; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_15() { return &___leftInactivePos_15; }
	inline void set_leftInactivePos_15(Vector3_t2243707580  value)
	{
		___leftInactivePos_15 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_16() { return static_cast<int32_t>(offsetof(FriendHomeNavigationController_t1091974581, ___rightInactivePos_16)); }
	inline Vector3_t2243707580  get_rightInactivePos_16() const { return ___rightInactivePos_16; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_16() { return &___rightInactivePos_16; }
	inline void set_rightInactivePos_16(Vector3_t2243707580  value)
	{
		___rightInactivePos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
