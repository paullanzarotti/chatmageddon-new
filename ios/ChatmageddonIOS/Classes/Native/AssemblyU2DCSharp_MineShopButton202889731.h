﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineModal
struct MineModal_t1141032720;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineShopButton
struct  MineShopButton_t202889731  : public SFXButton_t792651341
{
public:
	// MineModal MineShopButton::modal
	MineModal_t1141032720 * ___modal_5;

public:
	inline static int32_t get_offset_of_modal_5() { return static_cast<int32_t>(offsetof(MineShopButton_t202889731, ___modal_5)); }
	inline MineModal_t1141032720 * get_modal_5() const { return ___modal_5; }
	inline MineModal_t1141032720 ** get_address_of_modal_5() { return &___modal_5; }
	inline void set_modal_5(MineModal_t1141032720 * value)
	{
		___modal_5 = value;
		Il2CppCodeGenWriteBarrier(&___modal_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
