﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneMetadata/Builder
struct  Builder_t1569388123  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata/Builder::result
	PhoneMetadata_t366861403 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(Builder_t1569388123, ___result_0)); }
	inline PhoneMetadata_t366861403 * get_result_0() const { return ___result_0; }
	inline PhoneMetadata_t366861403 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(PhoneMetadata_t366861403 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
