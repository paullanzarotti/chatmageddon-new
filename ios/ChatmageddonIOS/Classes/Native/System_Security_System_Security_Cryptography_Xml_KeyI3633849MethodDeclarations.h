﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfoNode
struct KeyInfoNode_t3633849;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.KeyInfoNode::.ctor()
extern "C"  void KeyInfoNode__ctor_m1781431575 (KeyInfoNode_t3633849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfoNode::GetXml()
extern "C"  XmlElement_t2877111883 * KeyInfoNode_GetXml_m30762968 (KeyInfoNode_t3633849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoNode::LoadXml(System.Xml.XmlElement)
extern "C"  void KeyInfoNode_LoadXml_m3472757143 (KeyInfoNode_t3633849 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
