﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessageUIScaler
struct  ErrorMessageUIScaler_t1585671755  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.BoxCollider ErrorMessageUIScaler::errorCollider
	BoxCollider_t22920061 * ___errorCollider_14;

public:
	inline static int32_t get_offset_of_errorCollider_14() { return static_cast<int32_t>(offsetof(ErrorMessageUIScaler_t1585671755, ___errorCollider_14)); }
	inline BoxCollider_t22920061 * get_errorCollider_14() const { return ___errorCollider_14; }
	inline BoxCollider_t22920061 ** get_address_of_errorCollider_14() { return &___errorCollider_14; }
	inline void set_errorCollider_14(BoxCollider_t22920061 * value)
	{
		___errorCollider_14 = value;
		Il2CppCodeGenWriteBarrier(&___errorCollider_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
