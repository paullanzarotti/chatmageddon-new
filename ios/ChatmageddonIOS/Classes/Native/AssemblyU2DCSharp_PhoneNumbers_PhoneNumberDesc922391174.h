﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberDesc
struct  PhoneNumberDesc_t922391174  : public Il2CppObject
{
public:
	// System.Boolean PhoneNumbers.PhoneNumberDesc::hasNationalNumberPattern
	bool ___hasNationalNumberPattern_2;
	// System.String PhoneNumbers.PhoneNumberDesc::nationalNumberPattern_
	String_t* ___nationalNumberPattern__3;
	// System.Boolean PhoneNumbers.PhoneNumberDesc::hasPossibleNumberPattern
	bool ___hasPossibleNumberPattern_5;
	// System.String PhoneNumbers.PhoneNumberDesc::possibleNumberPattern_
	String_t* ___possibleNumberPattern__6;
	// System.Boolean PhoneNumbers.PhoneNumberDesc::hasExampleNumber
	bool ___hasExampleNumber_8;
	// System.String PhoneNumbers.PhoneNumberDesc::exampleNumber_
	String_t* ___exampleNumber__9;

public:
	inline static int32_t get_offset_of_hasNationalNumberPattern_2() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174, ___hasNationalNumberPattern_2)); }
	inline bool get_hasNationalNumberPattern_2() const { return ___hasNationalNumberPattern_2; }
	inline bool* get_address_of_hasNationalNumberPattern_2() { return &___hasNationalNumberPattern_2; }
	inline void set_hasNationalNumberPattern_2(bool value)
	{
		___hasNationalNumberPattern_2 = value;
	}

	inline static int32_t get_offset_of_nationalNumberPattern__3() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174, ___nationalNumberPattern__3)); }
	inline String_t* get_nationalNumberPattern__3() const { return ___nationalNumberPattern__3; }
	inline String_t** get_address_of_nationalNumberPattern__3() { return &___nationalNumberPattern__3; }
	inline void set_nationalNumberPattern__3(String_t* value)
	{
		___nationalNumberPattern__3 = value;
		Il2CppCodeGenWriteBarrier(&___nationalNumberPattern__3, value);
	}

	inline static int32_t get_offset_of_hasPossibleNumberPattern_5() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174, ___hasPossibleNumberPattern_5)); }
	inline bool get_hasPossibleNumberPattern_5() const { return ___hasPossibleNumberPattern_5; }
	inline bool* get_address_of_hasPossibleNumberPattern_5() { return &___hasPossibleNumberPattern_5; }
	inline void set_hasPossibleNumberPattern_5(bool value)
	{
		___hasPossibleNumberPattern_5 = value;
	}

	inline static int32_t get_offset_of_possibleNumberPattern__6() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174, ___possibleNumberPattern__6)); }
	inline String_t* get_possibleNumberPattern__6() const { return ___possibleNumberPattern__6; }
	inline String_t** get_address_of_possibleNumberPattern__6() { return &___possibleNumberPattern__6; }
	inline void set_possibleNumberPattern__6(String_t* value)
	{
		___possibleNumberPattern__6 = value;
		Il2CppCodeGenWriteBarrier(&___possibleNumberPattern__6, value);
	}

	inline static int32_t get_offset_of_hasExampleNumber_8() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174, ___hasExampleNumber_8)); }
	inline bool get_hasExampleNumber_8() const { return ___hasExampleNumber_8; }
	inline bool* get_address_of_hasExampleNumber_8() { return &___hasExampleNumber_8; }
	inline void set_hasExampleNumber_8(bool value)
	{
		___hasExampleNumber_8 = value;
	}

	inline static int32_t get_offset_of_exampleNumber__9() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174, ___exampleNumber__9)); }
	inline String_t* get_exampleNumber__9() const { return ___exampleNumber__9; }
	inline String_t** get_address_of_exampleNumber__9() { return &___exampleNumber__9; }
	inline void set_exampleNumber__9(String_t* value)
	{
		___exampleNumber__9 = value;
		Il2CppCodeGenWriteBarrier(&___exampleNumber__9, value);
	}
};

struct PhoneNumberDesc_t922391174_StaticFields
{
public:
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberDesc::defaultInstance
	PhoneNumberDesc_t922391174 * ___defaultInstance_0;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(PhoneNumberDesc_t922391174_StaticFields, ___defaultInstance_0)); }
	inline PhoneNumberDesc_t922391174 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(PhoneNumberDesc_t922391174 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
