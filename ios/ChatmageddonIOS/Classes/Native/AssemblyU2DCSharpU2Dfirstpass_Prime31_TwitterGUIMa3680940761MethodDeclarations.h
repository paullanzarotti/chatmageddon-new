﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.TwitterGUIManager
struct TwitterGUIManager_t3680940761;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Prime31.TwitterGUIManager::.ctor()
extern "C"  void TwitterGUIManager__ctor_m561674823 (TwitterGUIManager_t3680940761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterGUIManager::Start()
extern "C"  void TwitterGUIManager_Start_m3880174803 (TwitterGUIManager_t3680940761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterGUIManager::completionHandler(System.String,System.Object)
extern "C"  void TwitterGUIManager_completionHandler_m4250028823 (TwitterGUIManager_t3680940761 * __this, String_t* ___error0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.TwitterGUIManager::OnGUI()
extern "C"  void TwitterGUIManager_OnGUI_m418949729 (TwitterGUIManager_t3680940761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
