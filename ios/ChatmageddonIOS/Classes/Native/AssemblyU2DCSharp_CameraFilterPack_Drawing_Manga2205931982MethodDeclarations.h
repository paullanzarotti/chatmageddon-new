﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Manga
struct CameraFilterPack_Drawing_Manga_t2205931982;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Manga::.ctor()
extern "C"  void CameraFilterPack_Drawing_Manga__ctor_m836221403 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Manga_get_material_m1520175708 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::Start()
extern "C"  void CameraFilterPack_Drawing_Manga_Start_m4052530479 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Manga_OnRenderImage_m4084743167 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Manga_OnValidate_m464683010 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::Update()
extern "C"  void CameraFilterPack_Drawing_Manga_Update_m2264867548 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Manga_OnDisable_m2791781882 (CameraFilterPack_Drawing_Manga_t2205931982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
