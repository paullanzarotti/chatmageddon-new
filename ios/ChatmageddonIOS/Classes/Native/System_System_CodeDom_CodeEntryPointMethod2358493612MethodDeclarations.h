﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeEntryPointMethod
struct CodeEntryPointMethod_t2358493612;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeEntryPointMethod::.ctor()
extern "C"  void CodeEntryPointMethod__ctor_m1184666644 (CodeEntryPointMethod_t2358493612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
