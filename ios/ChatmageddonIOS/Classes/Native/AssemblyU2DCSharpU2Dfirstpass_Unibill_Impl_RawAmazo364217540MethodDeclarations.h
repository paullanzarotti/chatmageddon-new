﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.RawAmazonAppStoreBillingInterface
struct RawAmazonAppStoreBillingInterface_t364217540;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.AmazonAppStoreBillingService
struct AmazonAppStoreBillingService_t1988397396;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_AmazonA1988397396.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.RawAmazonAppStoreBillingInterface::.ctor(Unibill.Impl.UnibillConfiguration)
extern "C"  void RawAmazonAppStoreBillingInterface__ctor_m3011985784 (RawAmazonAppStoreBillingInterface_t364217540 * __this, UnibillConfiguration_t2915611853 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawAmazonAppStoreBillingInterface::initialise(Unibill.Impl.AmazonAppStoreBillingService)
extern "C"  void RawAmazonAppStoreBillingInterface_initialise_m2489725368 (RawAmazonAppStoreBillingInterface_t364217540 * __this, AmazonAppStoreBillingService_t1988397396 * ___amazon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawAmazonAppStoreBillingInterface::initiateItemDataRequest(System.String[])
extern "C"  void RawAmazonAppStoreBillingInterface_initiateItemDataRequest_m2348772525 (RawAmazonAppStoreBillingInterface_t364217540 * __this, StringU5BU5D_t1642385972* ___productIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawAmazonAppStoreBillingInterface::initiatePurchaseRequest(System.String)
extern "C"  void RawAmazonAppStoreBillingInterface_initiatePurchaseRequest_m181691717 (RawAmazonAppStoreBillingInterface_t364217540 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawAmazonAppStoreBillingInterface::restoreTransactions()
extern "C"  void RawAmazonAppStoreBillingInterface_restoreTransactions_m1978909021 (RawAmazonAppStoreBillingInterface_t364217540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
