﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigC14NTransform
struct XmlDsigC14NTransform_t4201013104;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigC14NTransform::.ctor()
extern "C"  void XmlDsigC14NTransform__ctor_m2024810768 (XmlDsigC14NTransform_t4201013104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigC14NTransform::.ctor(System.Boolean)
extern "C"  void XmlDsigC14NTransform__ctor_m370340753 (XmlDsigC14NTransform_t4201013104 * __this, bool ___includeComments0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigC14NTransform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDsigC14NTransform_GetInnerXml_m1952138451 (XmlDsigC14NTransform_t4201013104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigC14NTransform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDsigC14NTransform_LoadInnerXml_m3422349952 (XmlDsigC14NTransform_t4201013104 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
