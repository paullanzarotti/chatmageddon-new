﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_ColorDodge
struct CameraFilterPack_Blend2Camera_ColorDodge_t2559283428;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_ColorDodge::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge__ctor_m434373373 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_ColorDodge::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_ColorDodge_get_material_m3615947734 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::Start()
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge_Start_m154637509 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge_OnRenderImage_m4113403549 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge_OnValidate_m1228129084 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::Update()
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge_Update_m516953902 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge_OnEnable_m4224536357 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_ColorDodge_OnDisable_m272849832 (CameraFilterPack_Blend2Camera_ColorDodge_t2559283428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
