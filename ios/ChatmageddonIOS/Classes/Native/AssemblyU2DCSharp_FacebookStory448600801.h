﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookStory
struct  FacebookStory_t448600801  : public Il2CppObject
{
public:
	// System.String FacebookStory::toID
	String_t* ___toID_0;
	// System.String FacebookStory::linkURL
	String_t* ___linkURL_1;
	// System.String FacebookStory::linkName
	String_t* ___linkName_2;
	// System.String FacebookStory::linkCaption
	String_t* ___linkCaption_3;
	// System.String FacebookStory::linkDescription
	String_t* ___linkDescription_4;
	// System.String FacebookStory::linkImage
	String_t* ___linkImage_5;
	// System.String FacebookStory::linkMedia
	String_t* ___linkMedia_6;
	// System.String FacebookStory::actionLink
	String_t* ___actionLink_7;
	// System.String FacebookStory::actionName
	String_t* ___actionName_8;
	// System.String FacebookStory::linkReference
	String_t* ___linkReference_9;

public:
	inline static int32_t get_offset_of_toID_0() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___toID_0)); }
	inline String_t* get_toID_0() const { return ___toID_0; }
	inline String_t** get_address_of_toID_0() { return &___toID_0; }
	inline void set_toID_0(String_t* value)
	{
		___toID_0 = value;
		Il2CppCodeGenWriteBarrier(&___toID_0, value);
	}

	inline static int32_t get_offset_of_linkURL_1() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkURL_1)); }
	inline String_t* get_linkURL_1() const { return ___linkURL_1; }
	inline String_t** get_address_of_linkURL_1() { return &___linkURL_1; }
	inline void set_linkURL_1(String_t* value)
	{
		___linkURL_1 = value;
		Il2CppCodeGenWriteBarrier(&___linkURL_1, value);
	}

	inline static int32_t get_offset_of_linkName_2() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkName_2)); }
	inline String_t* get_linkName_2() const { return ___linkName_2; }
	inline String_t** get_address_of_linkName_2() { return &___linkName_2; }
	inline void set_linkName_2(String_t* value)
	{
		___linkName_2 = value;
		Il2CppCodeGenWriteBarrier(&___linkName_2, value);
	}

	inline static int32_t get_offset_of_linkCaption_3() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkCaption_3)); }
	inline String_t* get_linkCaption_3() const { return ___linkCaption_3; }
	inline String_t** get_address_of_linkCaption_3() { return &___linkCaption_3; }
	inline void set_linkCaption_3(String_t* value)
	{
		___linkCaption_3 = value;
		Il2CppCodeGenWriteBarrier(&___linkCaption_3, value);
	}

	inline static int32_t get_offset_of_linkDescription_4() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkDescription_4)); }
	inline String_t* get_linkDescription_4() const { return ___linkDescription_4; }
	inline String_t** get_address_of_linkDescription_4() { return &___linkDescription_4; }
	inline void set_linkDescription_4(String_t* value)
	{
		___linkDescription_4 = value;
		Il2CppCodeGenWriteBarrier(&___linkDescription_4, value);
	}

	inline static int32_t get_offset_of_linkImage_5() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkImage_5)); }
	inline String_t* get_linkImage_5() const { return ___linkImage_5; }
	inline String_t** get_address_of_linkImage_5() { return &___linkImage_5; }
	inline void set_linkImage_5(String_t* value)
	{
		___linkImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___linkImage_5, value);
	}

	inline static int32_t get_offset_of_linkMedia_6() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkMedia_6)); }
	inline String_t* get_linkMedia_6() const { return ___linkMedia_6; }
	inline String_t** get_address_of_linkMedia_6() { return &___linkMedia_6; }
	inline void set_linkMedia_6(String_t* value)
	{
		___linkMedia_6 = value;
		Il2CppCodeGenWriteBarrier(&___linkMedia_6, value);
	}

	inline static int32_t get_offset_of_actionLink_7() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___actionLink_7)); }
	inline String_t* get_actionLink_7() const { return ___actionLink_7; }
	inline String_t** get_address_of_actionLink_7() { return &___actionLink_7; }
	inline void set_actionLink_7(String_t* value)
	{
		___actionLink_7 = value;
		Il2CppCodeGenWriteBarrier(&___actionLink_7, value);
	}

	inline static int32_t get_offset_of_actionName_8() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___actionName_8)); }
	inline String_t* get_actionName_8() const { return ___actionName_8; }
	inline String_t** get_address_of_actionName_8() { return &___actionName_8; }
	inline void set_actionName_8(String_t* value)
	{
		___actionName_8 = value;
		Il2CppCodeGenWriteBarrier(&___actionName_8, value);
	}

	inline static int32_t get_offset_of_linkReference_9() { return static_cast<int32_t>(offsetof(FacebookStory_t448600801, ___linkReference_9)); }
	inline String_t* get_linkReference_9() const { return ___linkReference_9; }
	inline String_t** get_address_of_linkReference_9() { return &___linkReference_9; }
	inline void set_linkReference_9(String_t* value)
	{
		___linkReference_9 = value;
		Il2CppCodeGenWriteBarrier(&___linkReference_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
