﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct ZipAESStream_t1390211376;
// System.IO.Stream
struct Stream_t3255436806;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t2148616552;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En2148616552.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream1337713182.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Encryption.ZipAESTransform,System.Security.Cryptography.CryptoStreamMode)
extern "C"  void ZipAESStream__ctor_m2578024602 (ZipAESStream_t1390211376 * __this, Stream_t3255436806 * ___stream0, ZipAESTransform_t2148616552 * ___transform1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipAESStream_Read_m3324505803 (ZipAESStream_t1390211376 * __this, ByteU5BU5D_t3397334013* ___outBuffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZipAESStream_Write_m3423171332 (ZipAESStream_t1390211376 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
