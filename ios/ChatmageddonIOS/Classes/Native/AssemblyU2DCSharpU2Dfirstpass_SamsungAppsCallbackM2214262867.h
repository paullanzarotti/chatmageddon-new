﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.SamsungAppsBillingService
struct SamsungAppsBillingService_t2236131154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SamsungAppsCallbackMonoBehaviour
struct  SamsungAppsCallbackMonoBehaviour_t2214262867  : public MonoBehaviour_t1158329972
{
public:
	// Unibill.Impl.SamsungAppsBillingService SamsungAppsCallbackMonoBehaviour::samsung
	SamsungAppsBillingService_t2236131154 * ___samsung_2;

public:
	inline static int32_t get_offset_of_samsung_2() { return static_cast<int32_t>(offsetof(SamsungAppsCallbackMonoBehaviour_t2214262867, ___samsung_2)); }
	inline SamsungAppsBillingService_t2236131154 * get_samsung_2() const { return ___samsung_2; }
	inline SamsungAppsBillingService_t2236131154 ** get_address_of_samsung_2() { return &___samsung_2; }
	inline void set_samsung_2(SamsungAppsBillingService_t2236131154 * value)
	{
		___samsung_2 = value;
		Il2CppCodeGenWriteBarrier(&___samsung_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
