﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LockoutUI
struct  LockoutUI_t1337294023  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean LockoutUI::unloading
	bool ___unloading_2;

public:
	inline static int32_t get_offset_of_unloading_2() { return static_cast<int32_t>(offsetof(LockoutUI_t1337294023, ___unloading_2)); }
	inline bool get_unloading_2() const { return ___unloading_2; }
	inline bool* get_address_of_unloading_2() { return &___unloading_2; }
	inline void set_unloading_2(bool value)
	{
		___unloading_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
