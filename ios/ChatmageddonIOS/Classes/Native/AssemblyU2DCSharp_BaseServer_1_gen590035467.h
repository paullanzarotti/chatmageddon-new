﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UserPayload
struct UserPayload_t146188169;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseServer`1<System.Object>
struct  BaseServer_1_t590035467  : public MonoSingleton_1_t2440115015
{
public:
	// System.String BaseServer`1::serverURL
	String_t* ___serverURL_3;
	// System.Int32 BaseServer`1::defaultTimeoutSeconds
	int32_t ___defaultTimeoutSeconds_4;
	// UserPayload BaseServer`1::userPayload
	UserPayload_t146188169 * ___userPayload_5;
	// System.Int32 BaseServer`1::serverTimeSecondOffset
	int32_t ___serverTimeSecondOffset_6;
	// System.Int32 BaseServer`1::serverTimeMinOffset
	int32_t ___serverTimeMinOffset_7;
	// System.Boolean BaseServer`1::serverTimeAhead
	bool ___serverTimeAhead_8;

public:
	inline static int32_t get_offset_of_serverURL_3() { return static_cast<int32_t>(offsetof(BaseServer_1_t590035467, ___serverURL_3)); }
	inline String_t* get_serverURL_3() const { return ___serverURL_3; }
	inline String_t** get_address_of_serverURL_3() { return &___serverURL_3; }
	inline void set_serverURL_3(String_t* value)
	{
		___serverURL_3 = value;
		Il2CppCodeGenWriteBarrier(&___serverURL_3, value);
	}

	inline static int32_t get_offset_of_defaultTimeoutSeconds_4() { return static_cast<int32_t>(offsetof(BaseServer_1_t590035467, ___defaultTimeoutSeconds_4)); }
	inline int32_t get_defaultTimeoutSeconds_4() const { return ___defaultTimeoutSeconds_4; }
	inline int32_t* get_address_of_defaultTimeoutSeconds_4() { return &___defaultTimeoutSeconds_4; }
	inline void set_defaultTimeoutSeconds_4(int32_t value)
	{
		___defaultTimeoutSeconds_4 = value;
	}

	inline static int32_t get_offset_of_userPayload_5() { return static_cast<int32_t>(offsetof(BaseServer_1_t590035467, ___userPayload_5)); }
	inline UserPayload_t146188169 * get_userPayload_5() const { return ___userPayload_5; }
	inline UserPayload_t146188169 ** get_address_of_userPayload_5() { return &___userPayload_5; }
	inline void set_userPayload_5(UserPayload_t146188169 * value)
	{
		___userPayload_5 = value;
		Il2CppCodeGenWriteBarrier(&___userPayload_5, value);
	}

	inline static int32_t get_offset_of_serverTimeSecondOffset_6() { return static_cast<int32_t>(offsetof(BaseServer_1_t590035467, ___serverTimeSecondOffset_6)); }
	inline int32_t get_serverTimeSecondOffset_6() const { return ___serverTimeSecondOffset_6; }
	inline int32_t* get_address_of_serverTimeSecondOffset_6() { return &___serverTimeSecondOffset_6; }
	inline void set_serverTimeSecondOffset_6(int32_t value)
	{
		___serverTimeSecondOffset_6 = value;
	}

	inline static int32_t get_offset_of_serverTimeMinOffset_7() { return static_cast<int32_t>(offsetof(BaseServer_1_t590035467, ___serverTimeMinOffset_7)); }
	inline int32_t get_serverTimeMinOffset_7() const { return ___serverTimeMinOffset_7; }
	inline int32_t* get_address_of_serverTimeMinOffset_7() { return &___serverTimeMinOffset_7; }
	inline void set_serverTimeMinOffset_7(int32_t value)
	{
		___serverTimeMinOffset_7 = value;
	}

	inline static int32_t get_offset_of_serverTimeAhead_8() { return static_cast<int32_t>(offsetof(BaseServer_1_t590035467, ___serverTimeAhead_8)); }
	inline bool get_serverTimeAhead_8() const { return ___serverTimeAhead_8; }
	inline bool* get_address_of_serverTimeAhead_8() { return &___serverTimeAhead_8; }
	inline void set_serverTimeAhead_8(bool value)
	{
		___serverTimeAhead_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
