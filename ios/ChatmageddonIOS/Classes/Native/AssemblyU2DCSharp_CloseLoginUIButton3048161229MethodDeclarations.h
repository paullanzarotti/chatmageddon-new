﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseLoginUIButton
struct CloseLoginUIButton_t3048161229;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseLoginUIButton::.ctor()
extern "C"  void CloseLoginUIButton__ctor_m1074888734 (CloseLoginUIButton_t3048161229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseLoginUIButton::OnButtonClick()
extern "C"  void CloseLoginUIButton_OnButtonClick_m3211446229 (CloseLoginUIButton_t3048161229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
