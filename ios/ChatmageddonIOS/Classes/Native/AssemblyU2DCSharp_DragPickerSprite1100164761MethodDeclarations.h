﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragPickerSprite
struct DragPickerSprite_t1100164761;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void DragPickerSprite::.ctor()
extern "C"  void DragPickerSprite__ctor_m807106000 (DragPickerSprite_t1100164761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite::Start()
extern "C"  void DragPickerSprite_Start_m97074456 (DragPickerSprite_t1100164761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite::OnPress(System.Boolean)
extern "C"  void DragPickerSprite_OnPress_m1195087513 (DragPickerSprite_t1100164761 * __this, bool ___press0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite::OnDragExit()
extern "C"  void DragPickerSprite_OnDragExit_m339896649 (DragPickerSprite_t1100164761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DragPickerSprite::DelayedSpriteAppearance(System.Single)
extern "C"  Il2CppObject * DragPickerSprite_DelayedSpriteAppearance_m2100303466 (DragPickerSprite_t1100164761 * __this, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DragPickerSprite::DelayedSpriteAppearance(System.Single,UnityEngine.Vector3)
extern "C"  Il2CppObject * DragPickerSprite_DelayedSpriteAppearance_m3631911853 (DragPickerSprite_t1100164761 * __this, float ___delay0, Vector3_t2243707580  ___touchLocalPosInCycler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
