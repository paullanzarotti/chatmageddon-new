﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Player
struct Player_t1147783557;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/<UpdateInnerFriendsList>c__AnonStorey1
struct  U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968  : public Il2CppObject
{
public:
	// System.Collections.DictionaryEntry Player/<UpdateInnerFriendsList>c__AnonStorey1::pair
	DictionaryEntry_t3048875398  ___pair_0;
	// Player Player/<UpdateInnerFriendsList>c__AnonStorey1::$this
	Player_t1147783557 * ___U24this_1;

public:
	inline static int32_t get_offset_of_pair_0() { return static_cast<int32_t>(offsetof(U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968, ___pair_0)); }
	inline DictionaryEntry_t3048875398  get_pair_0() const { return ___pair_0; }
	inline DictionaryEntry_t3048875398 * get_address_of_pair_0() { return &___pair_0; }
	inline void set_pair_0(DictionaryEntry_t3048875398  value)
	{
		___pair_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968, ___U24this_1)); }
	inline Player_t1147783557 * get_U24this_1() const { return ___U24this_1; }
	inline Player_t1147783557 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Player_t1147783557 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
