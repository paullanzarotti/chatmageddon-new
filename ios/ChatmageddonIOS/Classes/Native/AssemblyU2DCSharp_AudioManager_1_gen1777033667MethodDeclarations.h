﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AudioManager_1_gen3699525966MethodDeclarations.h"

// System.Void AudioManager`1<ChatmageddonAudioManager>::.ctor()
#define AudioManager_1__ctor_m2178684725(__this, method) ((  void (*) (AudioManager_1_t1777033667 *, const MethodInfo*))AudioManager_1__ctor_m3811624603_gshared)(__this, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::InitAudioObjects(System.String)
#define AudioManager_1_InitAudioObjects_m3299552353(__this, ___ResourcePath0, method) ((  void (*) (AudioManager_1_t1777033667 *, String_t*, const MethodInfo*))AudioManager_1_InitAudioObjects_m3363084627_gshared)(__this, ___ResourcePath0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::LoadAudioSettings()
#define AudioManager_1_LoadAudioSettings_m1129807856(__this, method) ((  void (*) (AudioManager_1_t1777033667 *, const MethodInfo*))AudioManager_1_LoadAudioSettings_m77945118_gshared)(__this, method)
// System.Boolean AudioManager`1<ChatmageddonAudioManager>::CheckSFXToggle()
#define AudioManager_1_CheckSFXToggle_m1850087986(__this, method) ((  bool (*) (AudioManager_1_t1777033667 *, const MethodInfo*))AudioManager_1_CheckSFXToggle_m4066422776_gshared)(__this, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::SetSFXActive(System.Boolean)
#define AudioManager_1_SetSFXActive_m3525183665(__this, ___active0, method) ((  void (*) (AudioManager_1_t1777033667 *, bool, const MethodInfo*))AudioManager_1_SetSFXActive_m4010709183_gshared)(__this, ___active0, method)
// SFXSource AudioManager`1<ChatmageddonAudioManager>::InitSFXObject(SFX)
#define AudioManager_1_InitSFXObject_m390791749(__this, ___sfxToPlay0, method) ((  SFXSource_t383013662 * (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_InitSFXObject_m2253683943_gshared)(__this, ___sfxToPlay0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::PlaySFX(SFX,System.Boolean)
#define AudioManager_1_PlaySFX_m4066470916(__this, ___sfxToPlay0, ___loop1, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, bool, const MethodInfo*))AudioManager_1_PlaySFX_m1516199870_gshared)(__this, ___sfxToPlay0, ___loop1, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::FadeInSFX(SFX,System.Single)
#define AudioManager_1_FadeInSFX_m4080496633(__this, ___sfxToFade0, ___fadeDuration1, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, float, const MethodInfo*))AudioManager_1_FadeInSFX_m2831456923_gshared)(__this, ___sfxToFade0, ___fadeDuration1, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::FadeOutSFX(SFX,System.Single,System.Boolean)
#define AudioManager_1_FadeOutSFX_m3306737627(__this, ___sfxToFade0, ___fadeDuration1, ___pause2, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, float, bool, const MethodInfo*))AudioManager_1_FadeOutSFX_m2898713101_gshared)(__this, ___sfxToFade0, ___fadeDuration1, ___pause2, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::StopSFX(SFX)
#define AudioManager_1_StopSFX_m313338869(__this, ___sfxToStop0, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_StopSFX_m3596533875_gshared)(__this, ___sfxToStop0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::PauseSFX(SFX)
#define AudioManager_1_PauseSFX_m3317955515(__this, ___sfxToPause0, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_PauseSFX_m2124942313_gshared)(__this, ___sfxToPause0, method)
// System.Boolean AudioManager`1<ChatmageddonAudioManager>::CheckMusicToggle()
#define AudioManager_1_CheckMusicToggle_m1446840266(__this, method) ((  bool (*) (AudioManager_1_t1777033667 *, const MethodInfo*))AudioManager_1_CheckMusicToggle_m326464212_gshared)(__this, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::SetMusicActive(System.Boolean)
#define AudioManager_1_SetMusicActive_m3713900169(__this, ___active0, method) ((  void (*) (AudioManager_1_t1777033667 *, bool, const MethodInfo*))AudioManager_1_SetMusicActive_m1244833635_gshared)(__this, ___active0, method)
// MusicSource AudioManager`1<ChatmageddonAudioManager>::InitMusicObject(Music)
#define AudioManager_1_InitMusicObject_m226867421(__this, ___musicToPlay0, method) ((  MusicSource_t904181206 * (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_InitMusicObject_m1835014451_gshared)(__this, ___musicToPlay0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::PlayMusic(Music,System.Boolean)
#define AudioManager_1_PlayMusic_m662073764(__this, ___musicToPlay0, ___loop1, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, bool, const MethodInfo*))AudioManager_1_PlayMusic_m3461114206_gshared)(__this, ___musicToPlay0, ___loop1, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::StopMusic(Music)
#define AudioManager_1_StopMusic_m3557607893(__this, ___musicToStop0, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_StopMusic_m2497180243_gshared)(__this, ___musicToStop0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::PauseMusic(Music)
#define AudioManager_1_PauseMusic_m2527538203(__this, ___musicToPause0, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_PauseMusic_m600535241_gshared)(__this, ___musicToPause0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::FadeInMusic(Music,System.Single)
#define AudioManager_1_FadeInMusic_m3819589337(__this, ___musicToFade0, ___fadeDuration1, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, float, const MethodInfo*))AudioManager_1_FadeInMusic_m1751265275_gshared)(__this, ___musicToFade0, ___fadeDuration1, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::FadeOutMusic(Music,System.Single,System.Boolean)
#define AudioManager_1_FadeOutMusic_m3023062523(__this, ___musicToFade0, ___fadeDuration1, ___pause2, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, float, bool, const MethodInfo*))AudioManager_1_FadeOutMusic_m977247021_gshared)(__this, ___musicToFade0, ___fadeDuration1, ___pause2, method)
// System.Boolean AudioManager`1<ChatmageddonAudioManager>::CheckMusicPlaying(Music)
#define AudioManager_1_CheckMusicPlaying_m872215203(__this, ___musicToCheck0, method) ((  bool (*) (AudioManager_1_t1777033667 *, int32_t, const MethodInfo*))AudioManager_1_CheckMusicPlaying_m237763017_gshared)(__this, ___musicToCheck0, method)
// System.Void AudioManager`1<ChatmageddonAudioManager>::SetMusicVolume(Music,System.Single)
#define AudioManager_1_SetMusicVolume_m945758414(__this, ___music0, ___newVolume1, method) ((  void (*) (AudioManager_1_t1777033667 *, int32_t, float, const MethodInfo*))AudioManager_1_SetMusicVolume_m2460378328_gshared)(__this, ___music0, ___newVolume1, method)
