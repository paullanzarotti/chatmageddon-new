﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationsNavScreen
struct NotificationsNavScreen_t3203037389;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NotificationsNavScreen::.ctor()
extern "C"  void NotificationsNavScreen__ctor_m1012392316 (NotificationsNavScreen_t3203037389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsNavScreen::Start()
extern "C"  void NotificationsNavScreen_Start_m2815916172 (NotificationsNavScreen_t3203037389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsNavScreen::UIClosing()
extern "C"  void NotificationsNavScreen_UIClosing_m658412071 (NotificationsNavScreen_t3203037389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void NotificationsNavScreen_ScreenLoad_m3602481092 (NotificationsNavScreen_t3203037389 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void NotificationsNavScreen_ScreenUnload_m4066823700 (NotificationsNavScreen_t3203037389 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NotificationsNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool NotificationsNavScreen_ValidateScreenNavigation_m2791508503 (NotificationsNavScreen_t3203037389 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
