﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CodeGenerator/Visitor
struct Visitor_t4003864182;
// System.CodeDom.Compiler.CodeGenerator
struct CodeGenerator_t1245030040;

#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_Compiler_CodeGenerator1245030040.h"

// System.Void System.CodeDom.Compiler.CodeGenerator/Visitor::.ctor(System.CodeDom.Compiler.CodeGenerator)
extern "C"  void Visitor__ctor_m1941456385 (Visitor_t4003864182 * __this, CodeGenerator_t1245030040 * ___generator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
