﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackInventoryBuyButton
struct AttackInventoryBuyButton_t1923642124;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackInventoryBuyButton::.ctor()
extern "C"  void AttackInventoryBuyButton__ctor_m39187753 (AttackInventoryBuyButton_t1923642124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryBuyButton::SetButtonActive(System.Int32,System.Boolean)
extern "C"  void AttackInventoryBuyButton_SetButtonActive_m1609388795 (AttackInventoryBuyButton_t1923642124 * __this, int32_t ___inventoryAmount0, bool ___locked1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryBuyButton::OnButtonClick()
extern "C"  void AttackInventoryBuyButton_OnButtonClick_m104121264 (AttackInventoryBuyButton_t1923642124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
