﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<ClearContactsFromServer>c__AnonStorey31
struct U3CClearContactsFromServerU3Ec__AnonStorey31_t3782285915;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<ClearContactsFromServer>c__AnonStorey31::.ctor()
extern "C"  void U3CClearContactsFromServerU3Ec__AnonStorey31__ctor_m2579775190 (U3CClearContactsFromServerU3Ec__AnonStorey31_t3782285915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<ClearContactsFromServer>c__AnonStorey31::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CClearContactsFromServerU3Ec__AnonStorey31_U3CU3Em__0_m2598840111 (U3CClearContactsFromServerU3Ec__AnonStorey31_t3782285915 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
