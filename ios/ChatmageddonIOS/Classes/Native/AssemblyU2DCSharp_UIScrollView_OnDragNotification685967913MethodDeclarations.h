﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollView/OnDragNotification
struct OnDragNotification_t685967913;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UIScrollView/OnDragNotification::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDragNotification__ctor_m2201650390 (OnDragNotification_t685967913 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView/OnDragNotification::Invoke()
extern "C"  void OnDragNotification_Invoke_m2397674476 (OnDragNotification_t685967913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UIScrollView/OnDragNotification::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDragNotification_BeginInvoke_m2923873415 (OnDragNotification_t685967913 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView/OnDragNotification::EndInvoke(System.IAsyncResult)
extern "C"  void OnDragNotification_EndInvoke_m3923927076 (OnDragNotification_t685967913 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
