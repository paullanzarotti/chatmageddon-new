﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackNavigateForwardsButton
struct AttackNavigateForwardsButton_t1401782687;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackNavigateForwardsButton::.ctor()
extern "C"  void AttackNavigateForwardsButton__ctor_m2954109516 (AttackNavigateForwardsButton_t1401782687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigateForwardsButton::OnButtonClick()
extern "C"  void AttackNavigateForwardsButton_OnButtonClick_m4204339415 (AttackNavigateForwardsButton_t1401782687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigateForwardsButton::SetButtonActive(System.Boolean)
extern "C"  void AttackNavigateForwardsButton_SetButtonActive_m2656237477 (AttackNavigateForwardsButton_t1401782687 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
