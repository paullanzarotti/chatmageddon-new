﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISlider
struct UISlider_t2191058247;

#include "AssemblyU2DCSharp_ProgressBar192201240.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISliderProgressBar
struct  UISliderProgressBar_t2494061391  : public ProgressBar_t192201240
{
public:
	// UISlider UISliderProgressBar::slider
	UISlider_t2191058247 * ___slider_8;

public:
	inline static int32_t get_offset_of_slider_8() { return static_cast<int32_t>(offsetof(UISliderProgressBar_t2494061391, ___slider_8)); }
	inline UISlider_t2191058247 * get_slider_8() const { return ___slider_8; }
	inline UISlider_t2191058247 ** get_address_of_slider_8() { return &___slider_8; }
	inline void set_slider_8(UISlider_t2191058247 * value)
	{
		___slider_8 = value;
		Il2CppCodeGenWriteBarrier(&___slider_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
