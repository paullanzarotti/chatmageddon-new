﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0
struct U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::.ctor()
extern "C"  void U3CCheckAvatarLoadedU3Ec__Iterator0__ctor_m2464353803 (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckAvatarLoadedU3Ec__Iterator0_MoveNext_m1762865481 (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckAvatarLoadedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3410269305 (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckAvatarLoadedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4236808289 (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::Dispose()
extern "C"  void U3CCheckAvatarLoadedU3Ec__Iterator0_Dispose_m2884894090 (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::Reset()
extern "C"  void U3CCheckAvatarLoadedU3Ec__Iterator0_Reset_m435507400 (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
