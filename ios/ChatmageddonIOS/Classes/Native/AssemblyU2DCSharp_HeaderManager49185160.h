﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// FuelBarController
struct FuelBarController_t2314862973;
// ShieldBarController
struct ShieldBarController_t264539450;
// UILabel
struct UILabel_t1795115428;
// ChatButton
struct ChatButton_t3979308854;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen4094818176.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeaderManager
struct  HeaderManager_t49185160  : public MonoSingleton_1_t4094818176
{
public:
	// TweenPosition HeaderManager::posTween
	TweenPosition_t1144714832 * ___posTween_3;
	// AvatarUpdater HeaderManager::avatar
	AvatarUpdater_t2404165026 * ___avatar_4;
	// FuelBarController HeaderManager::fuelBar
	FuelBarController_t2314862973 * ___fuelBar_5;
	// ShieldBarController HeaderManager::shieldBar
	ShieldBarController_t264539450 * ___shieldBar_6;
	// UILabel HeaderManager::dailyPointsLabel
	UILabel_t1795115428 * ___dailyPointsLabel_7;
	// UILabel HeaderManager::bucksLabel
	UILabel_t1795115428 * ___bucksLabel_8;
	// ChatButton HeaderManager::chatButton
	ChatButton_t3979308854 * ___chatButton_9;
	// System.Boolean HeaderManager::firstFuelUpdate
	bool ___firstFuelUpdate_10;

public:
	inline static int32_t get_offset_of_posTween_3() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___posTween_3)); }
	inline TweenPosition_t1144714832 * get_posTween_3() const { return ___posTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_3() { return &___posTween_3; }
	inline void set_posTween_3(TweenPosition_t1144714832 * value)
	{
		___posTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_3, value);
	}

	inline static int32_t get_offset_of_avatar_4() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___avatar_4)); }
	inline AvatarUpdater_t2404165026 * get_avatar_4() const { return ___avatar_4; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_4() { return &___avatar_4; }
	inline void set_avatar_4(AvatarUpdater_t2404165026 * value)
	{
		___avatar_4 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_4, value);
	}

	inline static int32_t get_offset_of_fuelBar_5() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___fuelBar_5)); }
	inline FuelBarController_t2314862973 * get_fuelBar_5() const { return ___fuelBar_5; }
	inline FuelBarController_t2314862973 ** get_address_of_fuelBar_5() { return &___fuelBar_5; }
	inline void set_fuelBar_5(FuelBarController_t2314862973 * value)
	{
		___fuelBar_5 = value;
		Il2CppCodeGenWriteBarrier(&___fuelBar_5, value);
	}

	inline static int32_t get_offset_of_shieldBar_6() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___shieldBar_6)); }
	inline ShieldBarController_t264539450 * get_shieldBar_6() const { return ___shieldBar_6; }
	inline ShieldBarController_t264539450 ** get_address_of_shieldBar_6() { return &___shieldBar_6; }
	inline void set_shieldBar_6(ShieldBarController_t264539450 * value)
	{
		___shieldBar_6 = value;
		Il2CppCodeGenWriteBarrier(&___shieldBar_6, value);
	}

	inline static int32_t get_offset_of_dailyPointsLabel_7() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___dailyPointsLabel_7)); }
	inline UILabel_t1795115428 * get_dailyPointsLabel_7() const { return ___dailyPointsLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_dailyPointsLabel_7() { return &___dailyPointsLabel_7; }
	inline void set_dailyPointsLabel_7(UILabel_t1795115428 * value)
	{
		___dailyPointsLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___dailyPointsLabel_7, value);
	}

	inline static int32_t get_offset_of_bucksLabel_8() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___bucksLabel_8)); }
	inline UILabel_t1795115428 * get_bucksLabel_8() const { return ___bucksLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_bucksLabel_8() { return &___bucksLabel_8; }
	inline void set_bucksLabel_8(UILabel_t1795115428 * value)
	{
		___bucksLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___bucksLabel_8, value);
	}

	inline static int32_t get_offset_of_chatButton_9() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___chatButton_9)); }
	inline ChatButton_t3979308854 * get_chatButton_9() const { return ___chatButton_9; }
	inline ChatButton_t3979308854 ** get_address_of_chatButton_9() { return &___chatButton_9; }
	inline void set_chatButton_9(ChatButton_t3979308854 * value)
	{
		___chatButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___chatButton_9, value);
	}

	inline static int32_t get_offset_of_firstFuelUpdate_10() { return static_cast<int32_t>(offsetof(HeaderManager_t49185160, ___firstFuelUpdate_10)); }
	inline bool get_firstFuelUpdate_10() const { return ___firstFuelUpdate_10; }
	inline bool* get_address_of_firstFuelUpdate_10() { return &___firstFuelUpdate_10; }
	inline void set_firstFuelUpdate_10(bool value)
	{
		___firstFuelUpdate_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
