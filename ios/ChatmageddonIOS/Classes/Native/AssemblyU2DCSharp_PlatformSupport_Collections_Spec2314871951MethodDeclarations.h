﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler
struct NotifyCollectionChangedEventHandler_t2314871951;
// System.Object
struct Il2CppObject;
// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs
struct NotifyCollectionChangedEventArgs_t3926133854;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec3926133854.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void NotifyCollectionChangedEventHandler__ctor_m2587194482 (NotifyCollectionChangedEventHandler_t2314871951 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler::Invoke(System.Object,PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs)
extern "C"  void NotifyCollectionChangedEventHandler_Invoke_m2052369174 (NotifyCollectionChangedEventHandler_t2314871951 * __this, Il2CppObject * ___sender0, NotifyCollectionChangedEventArgs_t3926133854 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler::BeginInvoke(System.Object,PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NotifyCollectionChangedEventHandler_BeginInvoke_m1802710125 (NotifyCollectionChangedEventHandler_t2314871951 * __this, Il2CppObject * ___sender0, NotifyCollectionChangedEventArgs_t3926133854 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void NotifyCollectionChangedEventHandler_EndInvoke_m779170456 (NotifyCollectionChangedEventHandler_t2314871951 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
