﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibiller
struct Unibiller_t1392804696;
// System.Action`1<UnibillState>
struct Action_1_t4073934390;
// System.Action`1<PurchasableItem>
struct Action_1_t3765153281;
// System.Action`1<PurchaseEvent>
struct Action_1_t545353811;
// System.Action`2<System.String,System.IO.DirectoryInfo>
struct Action_2_t4139768145;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`2<System.String,System.Int32>
struct Action_2_t4277199140;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.Generic.List`1<Unibill.ProductDefinition>
struct List_1_t888775120;
// UnibillError[]
struct UnibillErrorU5BU5D_t4092036522;
// PurchasableItem[]
struct PurchasableItemU5BU5D_t138351562;
// System.String[]
struct StringU5BU5D_t1642385972;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1934446453;
// Unibill.Biller
struct Biller_t1615588570;
// Unibill.Impl.BillerFactory
struct BillerFactory_t1450757922;
// PurchaseEvent
struct PurchaseEvent_t743554429;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Biller1615588570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillerF1450757922.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseEvent743554429.h"

// System.Void Unibiller::.ctor()
extern "C"  void Unibiller__ctor_m3056167539 (Unibiller_t1392804696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onBillerReady(System.Action`1<UnibillState>)
extern "C"  void Unibiller_add_onBillerReady_m2427151891 (Il2CppObject * __this /* static, unused */, Action_1_t4073934390 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onBillerReady(System.Action`1<UnibillState>)
extern "C"  void Unibiller_remove_onBillerReady_m721965800 (Il2CppObject * __this /* static, unused */, Action_1_t4073934390 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onPurchaseCancelled(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_add_onPurchaseCancelled_m1335752479 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onPurchaseCancelled(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_remove_onPurchaseCancelled_m3119141462 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onPurchaseCompleteEvent(System.Action`1<PurchaseEvent>)
extern "C"  void Unibiller_add_onPurchaseCompleteEvent_m2452521383 (Il2CppObject * __this /* static, unused */, Action_1_t545353811 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onPurchaseCompleteEvent(System.Action`1<PurchaseEvent>)
extern "C"  void Unibiller_remove_onPurchaseCompleteEvent_m2645362590 (Il2CppObject * __this /* static, unused */, Action_1_t545353811 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onPurchaseComplete(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_add_onPurchaseComplete_m3286043425 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onPurchaseComplete(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_remove_onPurchaseComplete_m642353100 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onPurchaseFailed(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_add_onPurchaseFailed_m1295750959 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onPurchaseFailed(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_remove_onPurchaseFailed_m37748030 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onPurchaseDeferred(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_add_onPurchaseDeferred_m1938384491 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onPurchaseDeferred(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_remove_onPurchaseDeferred_m918652220 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onPurchaseRefunded(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_add_onPurchaseRefunded_m1285281167 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onPurchaseRefunded(System.Action`1<PurchasableItem>)
extern "C"  void Unibiller_remove_onPurchaseRefunded_m2751390956 (Il2CppObject * __this /* static, unused */, Action_1_t3765153281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onDownloadCompletedEvent(System.Action`2<System.String,System.IO.DirectoryInfo>)
extern "C"  void Unibiller_add_onDownloadCompletedEvent_m1522142360 (Il2CppObject * __this /* static, unused */, Action_2_t4139768145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onDownloadCompletedEvent(System.Action`2<System.String,System.IO.DirectoryInfo>)
extern "C"  void Unibiller_remove_onDownloadCompletedEvent_m1795051329 (Il2CppObject * __this /* static, unused */, Action_2_t4139768145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onDownloadCompletedEventString(System.Action`2<System.String,System.String>)
extern "C"  void Unibiller_add_onDownloadCompletedEventString_m2825921105 (Il2CppObject * __this /* static, unused */, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onDownloadCompletedEventString(System.Action`2<System.String,System.String>)
extern "C"  void Unibiller_remove_onDownloadCompletedEventString_m644003040 (Il2CppObject * __this /* static, unused */, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onDownloadProgressedEvent(System.Action`2<System.String,System.Int32>)
extern "C"  void Unibiller_add_onDownloadProgressedEvent_m1626761212 (Il2CppObject * __this /* static, unused */, Action_2_t4277199140 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onDownloadProgressedEvent(System.Action`2<System.String,System.Int32>)
extern "C"  void Unibiller_remove_onDownloadProgressedEvent_m996185149 (Il2CppObject * __this /* static, unused */, Action_2_t4277199140 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onDownloadFailedEvent(System.Action`2<System.String,System.String>)
extern "C"  void Unibiller_add_onDownloadFailedEvent_m178615486 (Il2CppObject * __this /* static, unused */, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onDownloadFailedEvent(System.Action`2<System.String,System.String>)
extern "C"  void Unibiller_remove_onDownloadFailedEvent_m2341729961 (Il2CppObject * __this /* static, unused */, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::add_onTransactionsRestored(System.Action`1<System.Boolean>)
extern "C"  void Unibiller_add_onTransactionsRestored_m2411787806 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::remove_onTransactionsRestored(System.Action`1<System.Boolean>)
extern "C"  void Unibiller_remove_onTransactionsRestored_m851612673 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.BillingPlatform Unibiller::get_BillingPlatform()
extern "C"  int32_t Unibiller_get_BillingPlatform_m2006699260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibiller::get_Initialised()
extern "C"  bool Unibiller_get_Initialised_m1698649647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::Initialise(System.Collections.Generic.List`1<Unibill.ProductDefinition>)
extern "C"  void Unibiller_Initialise_m1050417409 (Il2CppObject * __this /* static, unused */, List_1_t888775120 * ___runtimeProducts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnibillError[] Unibiller::get_Errors()
extern "C"  UnibillErrorU5BU5D_t4092036522* Unibiller_get_Errors_m3622792281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem[] Unibiller::get_AllPurchasableItems()
extern "C"  PurchasableItemU5BU5D_t138351562* Unibiller_get_AllPurchasableItems_m2319943145 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem[] Unibiller::get_AllNonConsumablePurchasableItems()
extern "C"  PurchasableItemU5BU5D_t138351562* Unibiller_get_AllNonConsumablePurchasableItems_m1390806025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem[] Unibiller::get_AllConsumablePurchasableItems()
extern "C"  PurchasableItemU5BU5D_t138351562* Unibiller_get_AllConsumablePurchasableItems_m3219259434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem[] Unibiller::get_AllSubscriptions()
extern "C"  PurchasableItemU5BU5D_t138351562* Unibiller_get_AllSubscriptions_m3664097855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Unibiller::get_AllCurrencies()
extern "C"  StringU5BU5D_t1642385972* Unibiller_get_AllCurrencies_m2669090501 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem Unibiller::GetPurchasableItemById(System.String)
extern "C"  PurchasableItem_t3963353899 * Unibiller_GetPurchasableItemById_m1089428912 (Il2CppObject * __this /* static, unused */, String_t* ___unibillPurchasableId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::initiatePurchase(PurchasableItem,System.String)
extern "C"  void Unibiller_initiatePurchase_m216438704 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___purchasable0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::initiatePurchase(System.String,System.String)
extern "C"  void Unibiller_initiatePurchase_m2715067315 (Il2CppObject * __this /* static, unused */, String_t* ___purchasableId0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibiller::GetPurchaseCount(PurchasableItem)
extern "C"  int32_t Unibiller_GetPurchaseCount_m3738977176 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibiller::GetPurchaseCount(System.String)
extern "C"  int32_t Unibiller_GetPurchaseCount_m1618964305 (Il2CppObject * __this /* static, unused */, String_t* ___purchasableId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Unibiller::GetCurrencyBalance(System.String)
extern "C"  Decimal_t724701077  Unibiller_GetCurrencyBalance_m2525037153 (Il2CppObject * __this /* static, unused */, String_t* ___currencyIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::CreditBalance(System.String,System.Decimal)
extern "C"  void Unibiller_CreditBalance_m2871834810 (Il2CppObject * __this /* static, unused */, String_t* ___currencyIdentifier0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibiller::DebitBalance(System.String,System.Decimal)
extern "C"  bool Unibiller_DebitBalance_m1957064385 (Il2CppObject * __this /* static, unused */, String_t* ___currencyIdentifier0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::restoreTransactions()
extern "C"  void Unibiller_restoreTransactions_m3392368774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::clearTransactions()
extern "C"  void Unibiller_clearTransactions_m346006341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::DownloadContent(System.String,PurchasableItem)
extern "C"  void Unibiller_DownloadContent_m1962909589 (Il2CppObject * __this /* static, unused */, String_t* ___bundleId0, PurchasableItem_t3963353899 * ___proofOfPurchase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo Unibiller::GetDownloadableContentFor(PurchasableItem)
extern "C"  DirectoryInfo_t1934446453 * Unibiller_GetDownloadableContentFor_m4153777591 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibiller::GetDownloadableContentPathFor(System.String)
extern "C"  String_t* Unibiller_GetDownloadableContentPathFor_m412290411 (Il2CppObject * __this /* static, unused */, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibiller::IsContentDownloaded(System.String)
extern "C"  bool Unibiller_IsContentDownloaded_m156819051 (Il2CppObject * __this /* static, unused */, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibiller::IsDownloadScheduled(System.String)
extern "C"  bool Unibiller_IsDownloadScheduled_m2604243338 (Il2CppObject * __this /* static, unused */, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::DeleteDownloadedContent(System.String)
extern "C"  void Unibiller_DeleteDownloadedContent_m1121884572 (Il2CppObject * __this /* static, unused */, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_internal_hook_events(Unibill.Biller,Unibill.Impl.BillerFactory)
extern "C"  void Unibiller__internal_hook_events_m2103971289 (Il2CppObject * __this /* static, unused */, Biller_t1615588570 * ___biller0, BillerFactory_t1450757922 * ___factory1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_onPurchaseCancelled(PurchasableItem)
extern "C"  void Unibiller__onPurchaseCancelled_m4097929318 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_onPurchaseComplete(PurchaseEvent)
extern "C"  void Unibiller__onPurchaseComplete_m50871750 (Il2CppObject * __this /* static, unused */, PurchaseEvent_t743554429 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_onPurchaseFailed(PurchasableItem)
extern "C"  void Unibiller__onPurchaseFailed_m2190431938 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_onPurchaseDeferred(PurchasableItem)
extern "C"  void Unibiller__onPurchaseDeferred_m2969927960 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_onPurchaseRefunded(PurchasableItem)
extern "C"  void Unibiller__onPurchaseRefunded_m2990216488 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibiller::_onTransactionsRestored(System.Boolean)
extern "C"  void Unibiller__onTransactionsRestored_m1891799425 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
