﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<OnlineMapsFindAutocompleteResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m759927222(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2421447472 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2192865289_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<OnlineMapsFindAutocompleteResult>::Invoke(T)
#define Action_1_Invoke_m1684978198(__this, ___obj0, method) ((  void (*) (Action_1_t2421447472 *, OnlineMapsFindAutocompleteResult_t2619648090 *, const MethodInfo*))Action_1_Invoke_m3960033342_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<OnlineMapsFindAutocompleteResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m812191183(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2421447472 *, OnlineMapsFindAutocompleteResult_t2619648090 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<OnlineMapsFindAutocompleteResult>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1181226968(__this, ___result0, method) ((  void (*) (Action_1_t2421447472 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
