﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Colors_HSV
struct  CameraFilterPack_Colors_HSV_t1033920197  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Colors_HSV::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_HSV::_HueShift
	float ____HueShift_3;
	// System.Single CameraFilterPack_Colors_HSV::_Saturation
	float ____Saturation_4;
	// System.Single CameraFilterPack_Colors_HSV::_ValueBrightness
	float ____ValueBrightness_5;
	// UnityEngine.Material CameraFilterPack_Colors_HSV::SCMaterial
	Material_t193706927 * ___SCMaterial_6;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of__HueShift_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197, ____HueShift_3)); }
	inline float get__HueShift_3() const { return ____HueShift_3; }
	inline float* get_address_of__HueShift_3() { return &____HueShift_3; }
	inline void set__HueShift_3(float value)
	{
		____HueShift_3 = value;
	}

	inline static int32_t get_offset_of__Saturation_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197, ____Saturation_4)); }
	inline float get__Saturation_4() const { return ____Saturation_4; }
	inline float* get_address_of__Saturation_4() { return &____Saturation_4; }
	inline void set__Saturation_4(float value)
	{
		____Saturation_4 = value;
	}

	inline static int32_t get_offset_of__ValueBrightness_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197, ____ValueBrightness_5)); }
	inline float get__ValueBrightness_5() const { return ____ValueBrightness_5; }
	inline float* get_address_of__ValueBrightness_5() { return &____ValueBrightness_5; }
	inline void set__ValueBrightness_5(float value)
	{
		____ValueBrightness_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}
};

struct CameraFilterPack_Colors_HSV_t1033920197_StaticFields
{
public:
	// System.Single CameraFilterPack_Colors_HSV::ChangeHueShift
	float ___ChangeHueShift_7;
	// System.Single CameraFilterPack_Colors_HSV::ChangeSaturation
	float ___ChangeSaturation_8;
	// System.Single CameraFilterPack_Colors_HSV::ChangeValueBrightness
	float ___ChangeValueBrightness_9;

public:
	inline static int32_t get_offset_of_ChangeHueShift_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197_StaticFields, ___ChangeHueShift_7)); }
	inline float get_ChangeHueShift_7() const { return ___ChangeHueShift_7; }
	inline float* get_address_of_ChangeHueShift_7() { return &___ChangeHueShift_7; }
	inline void set_ChangeHueShift_7(float value)
	{
		___ChangeHueShift_7 = value;
	}

	inline static int32_t get_offset_of_ChangeSaturation_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197_StaticFields, ___ChangeSaturation_8)); }
	inline float get_ChangeSaturation_8() const { return ___ChangeSaturation_8; }
	inline float* get_address_of_ChangeSaturation_8() { return &___ChangeSaturation_8; }
	inline void set_ChangeSaturation_8(float value)
	{
		___ChangeSaturation_8 = value;
	}

	inline static int32_t get_offset_of_ChangeValueBrightness_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_HSV_t1033920197_StaticFields, ___ChangeValueBrightness_9)); }
	inline float get_ChangeValueBrightness_9() const { return ___ChangeValueBrightness_9; }
	inline float* get_address_of_ChangeValueBrightness_9() { return &___ChangeValueBrightness_9; }
	inline void set_ChangeValueBrightness_9(float value)
	{
		___ChangeValueBrightness_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
