﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollWheelEvents/ScrollStartStopHandler
struct ScrollStartStopHandler_t1032307121;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ScrollWheelEvents/ScrollStartStopHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ScrollStartStopHandler__ctor_m3179029546 (ScrollStartStopHandler_t1032307121 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollWheelEvents/ScrollStartStopHandler::Invoke(System.Boolean)
extern "C"  void ScrollStartStopHandler_Invoke_m1158516387 (ScrollStartStopHandler_t1032307121 * __this, bool ___start0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ScrollWheelEvents/ScrollStartStopHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ScrollStartStopHandler_BeginInvoke_m3824963316 (ScrollStartStopHandler_t1032307121 * __this, bool ___start0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollWheelEvents/ScrollStartStopHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ScrollStartStopHandler_EndInvoke_m2493762888 (ScrollStartStopHandler_t1032307121 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
