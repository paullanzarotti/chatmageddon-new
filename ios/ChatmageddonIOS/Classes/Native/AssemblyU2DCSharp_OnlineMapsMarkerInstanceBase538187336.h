﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsMarkerInstanceBase
struct  OnlineMapsMarkerInstanceBase_t538187336  : public MonoBehaviour_t1158329972
{
public:
	// OnlineMapsMarkerBase OnlineMapsMarkerInstanceBase::marker
	OnlineMapsMarkerBase_t3900955221 * ___marker_2;

public:
	inline static int32_t get_offset_of_marker_2() { return static_cast<int32_t>(offsetof(OnlineMapsMarkerInstanceBase_t538187336, ___marker_2)); }
	inline OnlineMapsMarkerBase_t3900955221 * get_marker_2() const { return ___marker_2; }
	inline OnlineMapsMarkerBase_t3900955221 ** get_address_of_marker_2() { return &___marker_2; }
	inline void set_marker_2(OnlineMapsMarkerBase_t3900955221 * value)
	{
		___marker_2 = value;
		Il2CppCodeGenWriteBarrier(&___marker_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
