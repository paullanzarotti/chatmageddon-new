﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineAudienceTabController
struct MineAudienceTabController_t1758951344;
// TweenColor
struct TweenColor_t3390486518;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineAudienceTab
struct  MineAudienceTab_t1025879672  : public SFXButton_t792651341
{
public:
	// MineAudienceTabController MineAudienceTab::tabController
	MineAudienceTabController_t1758951344 * ___tabController_5;
	// MineAudience MineAudienceTab::audience
	int32_t ___audience_6;
	// TweenColor MineAudienceTab::colourTween
	TweenColor_t3390486518 * ___colourTween_7;

public:
	inline static int32_t get_offset_of_tabController_5() { return static_cast<int32_t>(offsetof(MineAudienceTab_t1025879672, ___tabController_5)); }
	inline MineAudienceTabController_t1758951344 * get_tabController_5() const { return ___tabController_5; }
	inline MineAudienceTabController_t1758951344 ** get_address_of_tabController_5() { return &___tabController_5; }
	inline void set_tabController_5(MineAudienceTabController_t1758951344 * value)
	{
		___tabController_5 = value;
		Il2CppCodeGenWriteBarrier(&___tabController_5, value);
	}

	inline static int32_t get_offset_of_audience_6() { return static_cast<int32_t>(offsetof(MineAudienceTab_t1025879672, ___audience_6)); }
	inline int32_t get_audience_6() const { return ___audience_6; }
	inline int32_t* get_address_of_audience_6() { return &___audience_6; }
	inline void set_audience_6(int32_t value)
	{
		___audience_6 = value;
	}

	inline static int32_t get_offset_of_colourTween_7() { return static_cast<int32_t>(offsetof(MineAudienceTab_t1025879672, ___colourTween_7)); }
	inline TweenColor_t3390486518 * get_colourTween_7() const { return ___colourTween_7; }
	inline TweenColor_t3390486518 ** get_address_of_colourTween_7() { return &___colourTween_7; }
	inline void set_colourTween_7(TweenColor_t3390486518 * value)
	{
		___colourTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___colourTween_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
