﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StandardDefendButton
struct StandardDefendButton_t3215661011;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void StandardDefendButton::.ctor()
extern "C"  void StandardDefendButton__ctor_m1102001528 (StandardDefendButton_t3215661011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardDefendButton::OnButtonClick()
extern "C"  void StandardDefendButton_OnButtonClick_m830612491 (StandardDefendButton_t3215661011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StandardDefendButton::WaitToActivateButton()
extern "C"  Il2CppObject * StandardDefendButton_WaitToActivateButton_m2604425991 (StandardDefendButton_t3215661011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardDefendButton::SetButtonActive(System.Boolean)
extern "C"  void StandardDefendButton_SetButtonActive_m859306105 (StandardDefendButton_t3215661011 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
