﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendSearchInput
struct FriendSearchInput_t390073340;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendSearchInput::.ctor()
extern "C"  void FriendSearchInput__ctor_m1170790221 (FriendSearchInput_t390073340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendSearchInput::OnSearchSubmit()
extern "C"  void FriendSearchInput_OnSearchSubmit_m481898578 (FriendSearchInput_t390073340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendSearchInput::OnSelect(System.Boolean)
extern "C"  void FriendSearchInput_OnSelect_m1124830875 (FriendSearchInput_t390073340 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
