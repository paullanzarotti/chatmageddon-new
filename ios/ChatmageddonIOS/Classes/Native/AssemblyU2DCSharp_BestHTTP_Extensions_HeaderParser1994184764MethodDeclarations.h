﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BestHTTP.Extensions.HeaderParser
struct HeaderParser_t1994184764;
// System.String
struct String_t;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>
struct List_1_t191583276;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BestHTTP.Extensions.HeaderParser::.ctor(System.String)
extern "C"  void HeaderParser__ctor_m382575119 (HeaderParser_t1994184764 * __this, String_t* ___headerStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.HeaderParser::Parse(System.String)
extern "C"  List_1_t191583276 * HeaderParser_Parse_m3912245929 (HeaderParser_t1994184764 * __this, String_t* ___headerStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
