﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_TV_BrokenGlass
struct  CameraFilterPack_TV_BrokenGlass_t3283168121  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_TV_BrokenGlass::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_BrokenGlass::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_Small
	float ___Broken_Small_4;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_Medium
	float ___Broken_Medium_5;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_High
	float ___Broken_High_6;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_Big
	float ___Broken_Big_7;
	// System.Single CameraFilterPack_TV_BrokenGlass::LightReflect
	float ___LightReflect_8;
	// UnityEngine.Material CameraFilterPack_TV_BrokenGlass::SCMaterial
	Material_t193706927 * ___SCMaterial_9;
	// UnityEngine.Texture2D CameraFilterPack_TV_BrokenGlass::Texture2
	Texture2D_t3542995729 * ___Texture2_10;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Broken_Small_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___Broken_Small_4)); }
	inline float get_Broken_Small_4() const { return ___Broken_Small_4; }
	inline float* get_address_of_Broken_Small_4() { return &___Broken_Small_4; }
	inline void set_Broken_Small_4(float value)
	{
		___Broken_Small_4 = value;
	}

	inline static int32_t get_offset_of_Broken_Medium_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___Broken_Medium_5)); }
	inline float get_Broken_Medium_5() const { return ___Broken_Medium_5; }
	inline float* get_address_of_Broken_Medium_5() { return &___Broken_Medium_5; }
	inline void set_Broken_Medium_5(float value)
	{
		___Broken_Medium_5 = value;
	}

	inline static int32_t get_offset_of_Broken_High_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___Broken_High_6)); }
	inline float get_Broken_High_6() const { return ___Broken_High_6; }
	inline float* get_address_of_Broken_High_6() { return &___Broken_High_6; }
	inline void set_Broken_High_6(float value)
	{
		___Broken_High_6 = value;
	}

	inline static int32_t get_offset_of_Broken_Big_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___Broken_Big_7)); }
	inline float get_Broken_Big_7() const { return ___Broken_Big_7; }
	inline float* get_address_of_Broken_Big_7() { return &___Broken_Big_7; }
	inline void set_Broken_Big_7(float value)
	{
		___Broken_Big_7 = value;
	}

	inline static int32_t get_offset_of_LightReflect_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___LightReflect_8)); }
	inline float get_LightReflect_8() const { return ___LightReflect_8; }
	inline float* get_address_of_LightReflect_8() { return &___LightReflect_8; }
	inline void set_LightReflect_8(float value)
	{
		___LightReflect_8 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___SCMaterial_9)); }
	inline Material_t193706927 * get_SCMaterial_9() const { return ___SCMaterial_9; }
	inline Material_t193706927 ** get_address_of_SCMaterial_9() { return &___SCMaterial_9; }
	inline void set_SCMaterial_9(Material_t193706927 * value)
	{
		___SCMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_9, value);
	}

	inline static int32_t get_offset_of_Texture2_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_BrokenGlass_t3283168121, ___Texture2_10)); }
	inline Texture2D_t3542995729 * get_Texture2_10() const { return ___Texture2_10; }
	inline Texture2D_t3542995729 ** get_address_of_Texture2_10() { return &___Texture2_10; }
	inline void set_Texture2_10(Texture2D_t3542995729 * value)
	{
		___Texture2_10 = value;
		Il2CppCodeGenWriteBarrier(&___Texture2_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
