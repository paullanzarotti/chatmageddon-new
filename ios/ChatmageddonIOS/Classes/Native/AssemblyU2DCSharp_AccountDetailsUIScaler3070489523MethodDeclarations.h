﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccountDetailsUIScaler
struct AccountDetailsUIScaler_t3070489523;

#include "codegen/il2cpp-codegen.h"

// System.Void AccountDetailsUIScaler::.ctor()
extern "C"  void AccountDetailsUIScaler__ctor_m807754872 (AccountDetailsUIScaler_t3070489523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccountDetailsUIScaler::GlobalUIScale()
extern "C"  void AccountDetailsUIScaler_GlobalUIScale_m1129855197 (AccountDetailsUIScaler_t3070489523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
