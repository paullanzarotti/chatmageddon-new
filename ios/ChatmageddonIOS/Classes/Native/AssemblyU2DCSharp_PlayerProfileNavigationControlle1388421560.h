﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen2867551090.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerProfileNavigationController
struct  PlayerProfileNavigationController_t1388421560  : public NavigationController_2_t2867551090
{
public:
	// UILabel PlayerProfileNavigationController::userLabel
	UILabel_t1795115428 * ___userLabel_8;
	// UILabel PlayerProfileNavigationController::statsLabel
	UILabel_t1795115428 * ___statsLabel_9;
	// UnityEngine.GameObject PlayerProfileNavigationController::userObject
	GameObject_t1756533147 * ___userObject_10;
	// UnityEngine.GameObject PlayerProfileNavigationController::statsObject
	GameObject_t1756533147 * ___statsObject_11;
	// UnityEngine.Vector3 PlayerProfileNavigationController::activePos
	Vector3_t2243707580  ___activePos_12;
	// UnityEngine.Vector3 PlayerProfileNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_13;
	// UnityEngine.Vector3 PlayerProfileNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_14;

public:
	inline static int32_t get_offset_of_userLabel_8() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___userLabel_8)); }
	inline UILabel_t1795115428 * get_userLabel_8() const { return ___userLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_userLabel_8() { return &___userLabel_8; }
	inline void set_userLabel_8(UILabel_t1795115428 * value)
	{
		___userLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___userLabel_8, value);
	}

	inline static int32_t get_offset_of_statsLabel_9() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___statsLabel_9)); }
	inline UILabel_t1795115428 * get_statsLabel_9() const { return ___statsLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_statsLabel_9() { return &___statsLabel_9; }
	inline void set_statsLabel_9(UILabel_t1795115428 * value)
	{
		___statsLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___statsLabel_9, value);
	}

	inline static int32_t get_offset_of_userObject_10() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___userObject_10)); }
	inline GameObject_t1756533147 * get_userObject_10() const { return ___userObject_10; }
	inline GameObject_t1756533147 ** get_address_of_userObject_10() { return &___userObject_10; }
	inline void set_userObject_10(GameObject_t1756533147 * value)
	{
		___userObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___userObject_10, value);
	}

	inline static int32_t get_offset_of_statsObject_11() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___statsObject_11)); }
	inline GameObject_t1756533147 * get_statsObject_11() const { return ___statsObject_11; }
	inline GameObject_t1756533147 ** get_address_of_statsObject_11() { return &___statsObject_11; }
	inline void set_statsObject_11(GameObject_t1756533147 * value)
	{
		___statsObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___statsObject_11, value);
	}

	inline static int32_t get_offset_of_activePos_12() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___activePos_12)); }
	inline Vector3_t2243707580  get_activePos_12() const { return ___activePos_12; }
	inline Vector3_t2243707580 * get_address_of_activePos_12() { return &___activePos_12; }
	inline void set_activePos_12(Vector3_t2243707580  value)
	{
		___activePos_12 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_13() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___leftInactivePos_13)); }
	inline Vector3_t2243707580  get_leftInactivePos_13() const { return ___leftInactivePos_13; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_13() { return &___leftInactivePos_13; }
	inline void set_leftInactivePos_13(Vector3_t2243707580  value)
	{
		___leftInactivePos_13 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_14() { return static_cast<int32_t>(offsetof(PlayerProfileNavigationController_t1388421560, ___rightInactivePos_14)); }
	inline Vector3_t2243707580  get_rightInactivePos_14() const { return ___rightInactivePos_14; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_14() { return &___rightInactivePos_14; }
	inline void set_rightInactivePos_14(Vector3_t2243707580  value)
	{
		___rightInactivePos_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
