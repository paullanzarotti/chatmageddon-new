﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.BuildMetadataFromXml
struct  BuildMetadataFromXml_t3731095708  : public Il2CppObject
{
public:

public:
};

struct BuildMetadataFromXml_t3731095708_StaticFields
{
public:
	// System.String PhoneNumbers.BuildMetadataFromXml::CARRIER_CODE_FORMATTING_RULE
	String_t* ___CARRIER_CODE_FORMATTING_RULE_0;
	// System.String PhoneNumbers.BuildMetadataFromXml::COUNTRY_CODE
	String_t* ___COUNTRY_CODE_1;
	// System.String PhoneNumbers.BuildMetadataFromXml::EMERGENCY
	String_t* ___EMERGENCY_2;
	// System.String PhoneNumbers.BuildMetadataFromXml::EXAMPLE_NUMBER
	String_t* ___EXAMPLE_NUMBER_3;
	// System.String PhoneNumbers.BuildMetadataFromXml::FIXED_LINE
	String_t* ___FIXED_LINE_4;
	// System.String PhoneNumbers.BuildMetadataFromXml::FORMAT
	String_t* ___FORMAT_5;
	// System.String PhoneNumbers.BuildMetadataFromXml::GENERAL_DESC
	String_t* ___GENERAL_DESC_6;
	// System.String PhoneNumbers.BuildMetadataFromXml::INTERNATIONAL_PREFIX
	String_t* ___INTERNATIONAL_PREFIX_7;
	// System.String PhoneNumbers.BuildMetadataFromXml::INTL_FORMAT
	String_t* ___INTL_FORMAT_8;
	// System.String PhoneNumbers.BuildMetadataFromXml::LEADING_DIGITS
	String_t* ___LEADING_DIGITS_9;
	// System.String PhoneNumbers.BuildMetadataFromXml::LEADING_ZERO_POSSIBLE
	String_t* ___LEADING_ZERO_POSSIBLE_10;
	// System.String PhoneNumbers.BuildMetadataFromXml::MAIN_COUNTRY_FOR_CODE
	String_t* ___MAIN_COUNTRY_FOR_CODE_11;
	// System.String PhoneNumbers.BuildMetadataFromXml::MOBILE
	String_t* ___MOBILE_12;
	// System.String PhoneNumbers.BuildMetadataFromXml::NATIONAL_NUMBER_PATTERN
	String_t* ___NATIONAL_NUMBER_PATTERN_13;
	// System.String PhoneNumbers.BuildMetadataFromXml::NATIONAL_PREFIX
	String_t* ___NATIONAL_PREFIX_14;
	// System.String PhoneNumbers.BuildMetadataFromXml::NATIONAL_PREFIX_FORMATTING_RULE
	String_t* ___NATIONAL_PREFIX_FORMATTING_RULE_15;
	// System.String PhoneNumbers.BuildMetadataFromXml::NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING
	String_t* ___NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16;
	// System.String PhoneNumbers.BuildMetadataFromXml::NATIONAL_PREFIX_FOR_PARSING
	String_t* ___NATIONAL_PREFIX_FOR_PARSING_17;
	// System.String PhoneNumbers.BuildMetadataFromXml::NATIONAL_PREFIX_TRANSFORM_RULE
	String_t* ___NATIONAL_PREFIX_TRANSFORM_RULE_18;
	// System.String PhoneNumbers.BuildMetadataFromXml::NO_INTERNATIONAL_DIALLING
	String_t* ___NO_INTERNATIONAL_DIALLING_19;
	// System.String PhoneNumbers.BuildMetadataFromXml::NUMBER_FORMAT
	String_t* ___NUMBER_FORMAT_20;
	// System.String PhoneNumbers.BuildMetadataFromXml::PAGER
	String_t* ___PAGER_21;
	// System.String PhoneNumbers.BuildMetadataFromXml::PATTERN
	String_t* ___PATTERN_22;
	// System.String PhoneNumbers.BuildMetadataFromXml::PERSONAL_NUMBER
	String_t* ___PERSONAL_NUMBER_23;
	// System.String PhoneNumbers.BuildMetadataFromXml::POSSIBLE_NUMBER_PATTERN
	String_t* ___POSSIBLE_NUMBER_PATTERN_24;
	// System.String PhoneNumbers.BuildMetadataFromXml::PREFERRED_EXTN_PREFIX
	String_t* ___PREFERRED_EXTN_PREFIX_25;
	// System.String PhoneNumbers.BuildMetadataFromXml::PREFERRED_INTERNATIONAL_PREFIX
	String_t* ___PREFERRED_INTERNATIONAL_PREFIX_26;
	// System.String PhoneNumbers.BuildMetadataFromXml::PREMIUM_RATE
	String_t* ___PREMIUM_RATE_27;
	// System.String PhoneNumbers.BuildMetadataFromXml::SHARED_COST
	String_t* ___SHARED_COST_28;
	// System.String PhoneNumbers.BuildMetadataFromXml::TOLL_FREE
	String_t* ___TOLL_FREE_29;
	// System.String PhoneNumbers.BuildMetadataFromXml::UAN
	String_t* ___UAN_30;
	// System.String PhoneNumbers.BuildMetadataFromXml::VOICEMAIL
	String_t* ___VOICEMAIL_31;
	// System.String PhoneNumbers.BuildMetadataFromXml::VOIP
	String_t* ___VOIP_32;

public:
	inline static int32_t get_offset_of_CARRIER_CODE_FORMATTING_RULE_0() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___CARRIER_CODE_FORMATTING_RULE_0)); }
	inline String_t* get_CARRIER_CODE_FORMATTING_RULE_0() const { return ___CARRIER_CODE_FORMATTING_RULE_0; }
	inline String_t** get_address_of_CARRIER_CODE_FORMATTING_RULE_0() { return &___CARRIER_CODE_FORMATTING_RULE_0; }
	inline void set_CARRIER_CODE_FORMATTING_RULE_0(String_t* value)
	{
		___CARRIER_CODE_FORMATTING_RULE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CARRIER_CODE_FORMATTING_RULE_0, value);
	}

	inline static int32_t get_offset_of_COUNTRY_CODE_1() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___COUNTRY_CODE_1)); }
	inline String_t* get_COUNTRY_CODE_1() const { return ___COUNTRY_CODE_1; }
	inline String_t** get_address_of_COUNTRY_CODE_1() { return &___COUNTRY_CODE_1; }
	inline void set_COUNTRY_CODE_1(String_t* value)
	{
		___COUNTRY_CODE_1 = value;
		Il2CppCodeGenWriteBarrier(&___COUNTRY_CODE_1, value);
	}

	inline static int32_t get_offset_of_EMERGENCY_2() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___EMERGENCY_2)); }
	inline String_t* get_EMERGENCY_2() const { return ___EMERGENCY_2; }
	inline String_t** get_address_of_EMERGENCY_2() { return &___EMERGENCY_2; }
	inline void set_EMERGENCY_2(String_t* value)
	{
		___EMERGENCY_2 = value;
		Il2CppCodeGenWriteBarrier(&___EMERGENCY_2, value);
	}

	inline static int32_t get_offset_of_EXAMPLE_NUMBER_3() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___EXAMPLE_NUMBER_3)); }
	inline String_t* get_EXAMPLE_NUMBER_3() const { return ___EXAMPLE_NUMBER_3; }
	inline String_t** get_address_of_EXAMPLE_NUMBER_3() { return &___EXAMPLE_NUMBER_3; }
	inline void set_EXAMPLE_NUMBER_3(String_t* value)
	{
		___EXAMPLE_NUMBER_3 = value;
		Il2CppCodeGenWriteBarrier(&___EXAMPLE_NUMBER_3, value);
	}

	inline static int32_t get_offset_of_FIXED_LINE_4() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___FIXED_LINE_4)); }
	inline String_t* get_FIXED_LINE_4() const { return ___FIXED_LINE_4; }
	inline String_t** get_address_of_FIXED_LINE_4() { return &___FIXED_LINE_4; }
	inline void set_FIXED_LINE_4(String_t* value)
	{
		___FIXED_LINE_4 = value;
		Il2CppCodeGenWriteBarrier(&___FIXED_LINE_4, value);
	}

	inline static int32_t get_offset_of_FORMAT_5() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___FORMAT_5)); }
	inline String_t* get_FORMAT_5() const { return ___FORMAT_5; }
	inline String_t** get_address_of_FORMAT_5() { return &___FORMAT_5; }
	inline void set_FORMAT_5(String_t* value)
	{
		___FORMAT_5 = value;
		Il2CppCodeGenWriteBarrier(&___FORMAT_5, value);
	}

	inline static int32_t get_offset_of_GENERAL_DESC_6() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___GENERAL_DESC_6)); }
	inline String_t* get_GENERAL_DESC_6() const { return ___GENERAL_DESC_6; }
	inline String_t** get_address_of_GENERAL_DESC_6() { return &___GENERAL_DESC_6; }
	inline void set_GENERAL_DESC_6(String_t* value)
	{
		___GENERAL_DESC_6 = value;
		Il2CppCodeGenWriteBarrier(&___GENERAL_DESC_6, value);
	}

	inline static int32_t get_offset_of_INTERNATIONAL_PREFIX_7() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___INTERNATIONAL_PREFIX_7)); }
	inline String_t* get_INTERNATIONAL_PREFIX_7() const { return ___INTERNATIONAL_PREFIX_7; }
	inline String_t** get_address_of_INTERNATIONAL_PREFIX_7() { return &___INTERNATIONAL_PREFIX_7; }
	inline void set_INTERNATIONAL_PREFIX_7(String_t* value)
	{
		___INTERNATIONAL_PREFIX_7 = value;
		Il2CppCodeGenWriteBarrier(&___INTERNATIONAL_PREFIX_7, value);
	}

	inline static int32_t get_offset_of_INTL_FORMAT_8() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___INTL_FORMAT_8)); }
	inline String_t* get_INTL_FORMAT_8() const { return ___INTL_FORMAT_8; }
	inline String_t** get_address_of_INTL_FORMAT_8() { return &___INTL_FORMAT_8; }
	inline void set_INTL_FORMAT_8(String_t* value)
	{
		___INTL_FORMAT_8 = value;
		Il2CppCodeGenWriteBarrier(&___INTL_FORMAT_8, value);
	}

	inline static int32_t get_offset_of_LEADING_DIGITS_9() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___LEADING_DIGITS_9)); }
	inline String_t* get_LEADING_DIGITS_9() const { return ___LEADING_DIGITS_9; }
	inline String_t** get_address_of_LEADING_DIGITS_9() { return &___LEADING_DIGITS_9; }
	inline void set_LEADING_DIGITS_9(String_t* value)
	{
		___LEADING_DIGITS_9 = value;
		Il2CppCodeGenWriteBarrier(&___LEADING_DIGITS_9, value);
	}

	inline static int32_t get_offset_of_LEADING_ZERO_POSSIBLE_10() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___LEADING_ZERO_POSSIBLE_10)); }
	inline String_t* get_LEADING_ZERO_POSSIBLE_10() const { return ___LEADING_ZERO_POSSIBLE_10; }
	inline String_t** get_address_of_LEADING_ZERO_POSSIBLE_10() { return &___LEADING_ZERO_POSSIBLE_10; }
	inline void set_LEADING_ZERO_POSSIBLE_10(String_t* value)
	{
		___LEADING_ZERO_POSSIBLE_10 = value;
		Il2CppCodeGenWriteBarrier(&___LEADING_ZERO_POSSIBLE_10, value);
	}

	inline static int32_t get_offset_of_MAIN_COUNTRY_FOR_CODE_11() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___MAIN_COUNTRY_FOR_CODE_11)); }
	inline String_t* get_MAIN_COUNTRY_FOR_CODE_11() const { return ___MAIN_COUNTRY_FOR_CODE_11; }
	inline String_t** get_address_of_MAIN_COUNTRY_FOR_CODE_11() { return &___MAIN_COUNTRY_FOR_CODE_11; }
	inline void set_MAIN_COUNTRY_FOR_CODE_11(String_t* value)
	{
		___MAIN_COUNTRY_FOR_CODE_11 = value;
		Il2CppCodeGenWriteBarrier(&___MAIN_COUNTRY_FOR_CODE_11, value);
	}

	inline static int32_t get_offset_of_MOBILE_12() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___MOBILE_12)); }
	inline String_t* get_MOBILE_12() const { return ___MOBILE_12; }
	inline String_t** get_address_of_MOBILE_12() { return &___MOBILE_12; }
	inline void set_MOBILE_12(String_t* value)
	{
		___MOBILE_12 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILE_12, value);
	}

	inline static int32_t get_offset_of_NATIONAL_NUMBER_PATTERN_13() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NATIONAL_NUMBER_PATTERN_13)); }
	inline String_t* get_NATIONAL_NUMBER_PATTERN_13() const { return ___NATIONAL_NUMBER_PATTERN_13; }
	inline String_t** get_address_of_NATIONAL_NUMBER_PATTERN_13() { return &___NATIONAL_NUMBER_PATTERN_13; }
	inline void set_NATIONAL_NUMBER_PATTERN_13(String_t* value)
	{
		___NATIONAL_NUMBER_PATTERN_13 = value;
		Il2CppCodeGenWriteBarrier(&___NATIONAL_NUMBER_PATTERN_13, value);
	}

	inline static int32_t get_offset_of_NATIONAL_PREFIX_14() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NATIONAL_PREFIX_14)); }
	inline String_t* get_NATIONAL_PREFIX_14() const { return ___NATIONAL_PREFIX_14; }
	inline String_t** get_address_of_NATIONAL_PREFIX_14() { return &___NATIONAL_PREFIX_14; }
	inline void set_NATIONAL_PREFIX_14(String_t* value)
	{
		___NATIONAL_PREFIX_14 = value;
		Il2CppCodeGenWriteBarrier(&___NATIONAL_PREFIX_14, value);
	}

	inline static int32_t get_offset_of_NATIONAL_PREFIX_FORMATTING_RULE_15() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NATIONAL_PREFIX_FORMATTING_RULE_15)); }
	inline String_t* get_NATIONAL_PREFIX_FORMATTING_RULE_15() const { return ___NATIONAL_PREFIX_FORMATTING_RULE_15; }
	inline String_t** get_address_of_NATIONAL_PREFIX_FORMATTING_RULE_15() { return &___NATIONAL_PREFIX_FORMATTING_RULE_15; }
	inline void set_NATIONAL_PREFIX_FORMATTING_RULE_15(String_t* value)
	{
		___NATIONAL_PREFIX_FORMATTING_RULE_15 = value;
		Il2CppCodeGenWriteBarrier(&___NATIONAL_PREFIX_FORMATTING_RULE_15, value);
	}

	inline static int32_t get_offset_of_NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16)); }
	inline String_t* get_NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16() const { return ___NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16; }
	inline String_t** get_address_of_NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16() { return &___NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16; }
	inline void set_NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16(String_t* value)
	{
		___NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16 = value;
		Il2CppCodeGenWriteBarrier(&___NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16, value);
	}

	inline static int32_t get_offset_of_NATIONAL_PREFIX_FOR_PARSING_17() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NATIONAL_PREFIX_FOR_PARSING_17)); }
	inline String_t* get_NATIONAL_PREFIX_FOR_PARSING_17() const { return ___NATIONAL_PREFIX_FOR_PARSING_17; }
	inline String_t** get_address_of_NATIONAL_PREFIX_FOR_PARSING_17() { return &___NATIONAL_PREFIX_FOR_PARSING_17; }
	inline void set_NATIONAL_PREFIX_FOR_PARSING_17(String_t* value)
	{
		___NATIONAL_PREFIX_FOR_PARSING_17 = value;
		Il2CppCodeGenWriteBarrier(&___NATIONAL_PREFIX_FOR_PARSING_17, value);
	}

	inline static int32_t get_offset_of_NATIONAL_PREFIX_TRANSFORM_RULE_18() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NATIONAL_PREFIX_TRANSFORM_RULE_18)); }
	inline String_t* get_NATIONAL_PREFIX_TRANSFORM_RULE_18() const { return ___NATIONAL_PREFIX_TRANSFORM_RULE_18; }
	inline String_t** get_address_of_NATIONAL_PREFIX_TRANSFORM_RULE_18() { return &___NATIONAL_PREFIX_TRANSFORM_RULE_18; }
	inline void set_NATIONAL_PREFIX_TRANSFORM_RULE_18(String_t* value)
	{
		___NATIONAL_PREFIX_TRANSFORM_RULE_18 = value;
		Il2CppCodeGenWriteBarrier(&___NATIONAL_PREFIX_TRANSFORM_RULE_18, value);
	}

	inline static int32_t get_offset_of_NO_INTERNATIONAL_DIALLING_19() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NO_INTERNATIONAL_DIALLING_19)); }
	inline String_t* get_NO_INTERNATIONAL_DIALLING_19() const { return ___NO_INTERNATIONAL_DIALLING_19; }
	inline String_t** get_address_of_NO_INTERNATIONAL_DIALLING_19() { return &___NO_INTERNATIONAL_DIALLING_19; }
	inline void set_NO_INTERNATIONAL_DIALLING_19(String_t* value)
	{
		___NO_INTERNATIONAL_DIALLING_19 = value;
		Il2CppCodeGenWriteBarrier(&___NO_INTERNATIONAL_DIALLING_19, value);
	}

	inline static int32_t get_offset_of_NUMBER_FORMAT_20() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___NUMBER_FORMAT_20)); }
	inline String_t* get_NUMBER_FORMAT_20() const { return ___NUMBER_FORMAT_20; }
	inline String_t** get_address_of_NUMBER_FORMAT_20() { return &___NUMBER_FORMAT_20; }
	inline void set_NUMBER_FORMAT_20(String_t* value)
	{
		___NUMBER_FORMAT_20 = value;
		Il2CppCodeGenWriteBarrier(&___NUMBER_FORMAT_20, value);
	}

	inline static int32_t get_offset_of_PAGER_21() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___PAGER_21)); }
	inline String_t* get_PAGER_21() const { return ___PAGER_21; }
	inline String_t** get_address_of_PAGER_21() { return &___PAGER_21; }
	inline void set_PAGER_21(String_t* value)
	{
		___PAGER_21 = value;
		Il2CppCodeGenWriteBarrier(&___PAGER_21, value);
	}

	inline static int32_t get_offset_of_PATTERN_22() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___PATTERN_22)); }
	inline String_t* get_PATTERN_22() const { return ___PATTERN_22; }
	inline String_t** get_address_of_PATTERN_22() { return &___PATTERN_22; }
	inline void set_PATTERN_22(String_t* value)
	{
		___PATTERN_22 = value;
		Il2CppCodeGenWriteBarrier(&___PATTERN_22, value);
	}

	inline static int32_t get_offset_of_PERSONAL_NUMBER_23() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___PERSONAL_NUMBER_23)); }
	inline String_t* get_PERSONAL_NUMBER_23() const { return ___PERSONAL_NUMBER_23; }
	inline String_t** get_address_of_PERSONAL_NUMBER_23() { return &___PERSONAL_NUMBER_23; }
	inline void set_PERSONAL_NUMBER_23(String_t* value)
	{
		___PERSONAL_NUMBER_23 = value;
		Il2CppCodeGenWriteBarrier(&___PERSONAL_NUMBER_23, value);
	}

	inline static int32_t get_offset_of_POSSIBLE_NUMBER_PATTERN_24() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___POSSIBLE_NUMBER_PATTERN_24)); }
	inline String_t* get_POSSIBLE_NUMBER_PATTERN_24() const { return ___POSSIBLE_NUMBER_PATTERN_24; }
	inline String_t** get_address_of_POSSIBLE_NUMBER_PATTERN_24() { return &___POSSIBLE_NUMBER_PATTERN_24; }
	inline void set_POSSIBLE_NUMBER_PATTERN_24(String_t* value)
	{
		___POSSIBLE_NUMBER_PATTERN_24 = value;
		Il2CppCodeGenWriteBarrier(&___POSSIBLE_NUMBER_PATTERN_24, value);
	}

	inline static int32_t get_offset_of_PREFERRED_EXTN_PREFIX_25() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___PREFERRED_EXTN_PREFIX_25)); }
	inline String_t* get_PREFERRED_EXTN_PREFIX_25() const { return ___PREFERRED_EXTN_PREFIX_25; }
	inline String_t** get_address_of_PREFERRED_EXTN_PREFIX_25() { return &___PREFERRED_EXTN_PREFIX_25; }
	inline void set_PREFERRED_EXTN_PREFIX_25(String_t* value)
	{
		___PREFERRED_EXTN_PREFIX_25 = value;
		Il2CppCodeGenWriteBarrier(&___PREFERRED_EXTN_PREFIX_25, value);
	}

	inline static int32_t get_offset_of_PREFERRED_INTERNATIONAL_PREFIX_26() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___PREFERRED_INTERNATIONAL_PREFIX_26)); }
	inline String_t* get_PREFERRED_INTERNATIONAL_PREFIX_26() const { return ___PREFERRED_INTERNATIONAL_PREFIX_26; }
	inline String_t** get_address_of_PREFERRED_INTERNATIONAL_PREFIX_26() { return &___PREFERRED_INTERNATIONAL_PREFIX_26; }
	inline void set_PREFERRED_INTERNATIONAL_PREFIX_26(String_t* value)
	{
		___PREFERRED_INTERNATIONAL_PREFIX_26 = value;
		Il2CppCodeGenWriteBarrier(&___PREFERRED_INTERNATIONAL_PREFIX_26, value);
	}

	inline static int32_t get_offset_of_PREMIUM_RATE_27() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___PREMIUM_RATE_27)); }
	inline String_t* get_PREMIUM_RATE_27() const { return ___PREMIUM_RATE_27; }
	inline String_t** get_address_of_PREMIUM_RATE_27() { return &___PREMIUM_RATE_27; }
	inline void set_PREMIUM_RATE_27(String_t* value)
	{
		___PREMIUM_RATE_27 = value;
		Il2CppCodeGenWriteBarrier(&___PREMIUM_RATE_27, value);
	}

	inline static int32_t get_offset_of_SHARED_COST_28() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___SHARED_COST_28)); }
	inline String_t* get_SHARED_COST_28() const { return ___SHARED_COST_28; }
	inline String_t** get_address_of_SHARED_COST_28() { return &___SHARED_COST_28; }
	inline void set_SHARED_COST_28(String_t* value)
	{
		___SHARED_COST_28 = value;
		Il2CppCodeGenWriteBarrier(&___SHARED_COST_28, value);
	}

	inline static int32_t get_offset_of_TOLL_FREE_29() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___TOLL_FREE_29)); }
	inline String_t* get_TOLL_FREE_29() const { return ___TOLL_FREE_29; }
	inline String_t** get_address_of_TOLL_FREE_29() { return &___TOLL_FREE_29; }
	inline void set_TOLL_FREE_29(String_t* value)
	{
		___TOLL_FREE_29 = value;
		Il2CppCodeGenWriteBarrier(&___TOLL_FREE_29, value);
	}

	inline static int32_t get_offset_of_UAN_30() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___UAN_30)); }
	inline String_t* get_UAN_30() const { return ___UAN_30; }
	inline String_t** get_address_of_UAN_30() { return &___UAN_30; }
	inline void set_UAN_30(String_t* value)
	{
		___UAN_30 = value;
		Il2CppCodeGenWriteBarrier(&___UAN_30, value);
	}

	inline static int32_t get_offset_of_VOICEMAIL_31() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___VOICEMAIL_31)); }
	inline String_t* get_VOICEMAIL_31() const { return ___VOICEMAIL_31; }
	inline String_t** get_address_of_VOICEMAIL_31() { return &___VOICEMAIL_31; }
	inline void set_VOICEMAIL_31(String_t* value)
	{
		___VOICEMAIL_31 = value;
		Il2CppCodeGenWriteBarrier(&___VOICEMAIL_31, value);
	}

	inline static int32_t get_offset_of_VOIP_32() { return static_cast<int32_t>(offsetof(BuildMetadataFromXml_t3731095708_StaticFields, ___VOIP_32)); }
	inline String_t* get_VOIP_32() const { return ___VOIP_32; }
	inline String_t** get_address_of_VOIP_32() { return &___VOIP_32; }
	inline void set_VOIP_32(String_t* value)
	{
		___VOIP_32 = value;
		Il2CppCodeGenWriteBarrier(&___VOIP_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
