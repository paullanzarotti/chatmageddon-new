﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LauncherSceneManager
struct LauncherSceneManager_t2478891917;

#include "codegen/il2cpp-codegen.h"

// System.Void LauncherSceneManager::.ctor()
extern "C"  void LauncherSceneManager__ctor_m3222011986 (LauncherSceneManager_t2478891917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LauncherSceneManager::InitScene()
extern "C"  void LauncherSceneManager_InitScene_m3055147824 (LauncherSceneManager_t2478891917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LauncherSceneManager::StartScene()
extern "C"  void LauncherSceneManager_StartScene_m1376894600 (LauncherSceneManager_t2478891917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
