﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeNavigateForwardButton
struct AttackHomeNavigateForwardButton_t3562703987;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackHomeNavigateForwardButton::.ctor()
extern "C"  void AttackHomeNavigateForwardButton__ctor_m2389718744 (AttackHomeNavigateForwardButton_t3562703987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavigateForwardButton::OnButtonClick()
extern "C"  void AttackHomeNavigateForwardButton_OnButtonClick_m3817742103 (AttackHomeNavigateForwardButton_t3562703987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
