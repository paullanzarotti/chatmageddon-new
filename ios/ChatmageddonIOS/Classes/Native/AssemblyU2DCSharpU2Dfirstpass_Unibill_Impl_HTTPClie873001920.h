﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.HTTPClient/PostRequest
struct PostRequest_t4132869030;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.WWW
struct WWW_t2919945039;
// Unibill.Impl.HTTPClient
struct HTTPClient_t1865772921;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.HTTPClient/<pump>c__Iterator0
struct  U3CpumpU3Ec__Iterator0_t873001920  : public Il2CppObject
{
public:
	// Unibill.Impl.HTTPClient/PostRequest Unibill.Impl.HTTPClient/<pump>c__Iterator0::<e>__0
	PostRequest_t4132869030 * ___U3CeU3E__0_0;
	// UnityEngine.WWWForm Unibill.Impl.HTTPClient/<pump>c__Iterator0::<form>__1
	WWWForm_t3950226929 * ___U3CformU3E__1_1;
	// UnityEngine.WWW Unibill.Impl.HTTPClient/<pump>c__Iterator0::<w>__2
	WWW_t2919945039 * ___U3CwU3E__2_2;
	// Unibill.Impl.HTTPClient Unibill.Impl.HTTPClient/<pump>c__Iterator0::$this
	HTTPClient_t1865772921 * ___U24this_3;
	// System.Object Unibill.Impl.HTTPClient/<pump>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean Unibill.Impl.HTTPClient/<pump>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Unibill.Impl.HTTPClient/<pump>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U3CeU3E__0_0)); }
	inline PostRequest_t4132869030 * get_U3CeU3E__0_0() const { return ___U3CeU3E__0_0; }
	inline PostRequest_t4132869030 ** get_address_of_U3CeU3E__0_0() { return &___U3CeU3E__0_0; }
	inline void set_U3CeU3E__0_0(PostRequest_t4132869030 * value)
	{
		___U3CeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CformU3E__1_1() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U3CformU3E__1_1)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__1_1() const { return ___U3CformU3E__1_1; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__1_1() { return &___U3CformU3E__1_1; }
	inline void set_U3CformU3E__1_1(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CwU3E__2_2() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U3CwU3E__2_2)); }
	inline WWW_t2919945039 * get_U3CwU3E__2_2() const { return ___U3CwU3E__2_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwU3E__2_2() { return &___U3CwU3E__2_2; }
	inline void set_U3CwU3E__2_2(WWW_t2919945039 * value)
	{
		___U3CwU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U24this_3)); }
	inline HTTPClient_t1865772921 * get_U24this_3() const { return ___U24this_3; }
	inline HTTPClient_t1865772921 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(HTTPClient_t1865772921 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CpumpU3Ec__Iterator0_t873001920, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
