﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_HardMix
struct CameraFilterPack_Blend2Camera_HardMix_t2750075575;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_HardMix::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_HardMix__ctor_m220329502 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_HardMix::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_HardMix_get_material_m155007379 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::Start()
extern "C"  void CameraFilterPack_Blend2Camera_HardMix_Start_m2322270930 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_HardMix_OnRenderImage_m2899866538 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_HardMix_OnValidate_m2416991437 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::Update()
extern "C"  void CameraFilterPack_Blend2Camera_HardMix_Update_m2442373787 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_HardMix_OnEnable_m592878866 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_HardMix_OnDisable_m276952555 (CameraFilterPack_Blend2Camera_HardMix_t2750075575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
