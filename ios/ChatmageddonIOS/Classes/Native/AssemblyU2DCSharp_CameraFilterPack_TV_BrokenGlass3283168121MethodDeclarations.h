﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_BrokenGlass
struct CameraFilterPack_TV_BrokenGlass_t3283168121;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_BrokenGlass::.ctor()
extern "C"  void CameraFilterPack_TV_BrokenGlass__ctor_m1729951730 (CameraFilterPack_TV_BrokenGlass_t3283168121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_BrokenGlass::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_BrokenGlass_get_material_m3133155937 (CameraFilterPack_TV_BrokenGlass_t3283168121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::Start()
extern "C"  void CameraFilterPack_TV_BrokenGlass_Start_m3216490794 (CameraFilterPack_TV_BrokenGlass_t3283168121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_BrokenGlass_OnRenderImage_m3917622042 (CameraFilterPack_TV_BrokenGlass_t3283168121 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::Update()
extern "C"  void CameraFilterPack_TV_BrokenGlass_Update_m2063335429 (CameraFilterPack_TV_BrokenGlass_t3283168121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::OnDisable()
extern "C"  void CameraFilterPack_TV_BrokenGlass_OnDisable_m3744152841 (CameraFilterPack_TV_BrokenGlass_t3283168121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
