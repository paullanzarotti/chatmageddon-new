﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPDragScrollView
struct IPDragScrollView_t145219383;

#include "codegen/il2cpp-codegen.h"

// System.Void IPDragScrollView::.ctor()
extern "C"  void IPDragScrollView__ctor_m2395660646 (IPDragScrollView_t145219383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDragScrollView::OnPress(System.Boolean)
extern "C"  void IPDragScrollView_OnPress_m3433261359 (IPDragScrollView_t145219383 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
