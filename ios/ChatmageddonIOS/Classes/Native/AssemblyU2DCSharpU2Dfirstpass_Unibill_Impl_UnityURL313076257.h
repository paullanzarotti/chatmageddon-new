﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.UnityHTTPRequest
struct UnityHTTPRequest_t3521437524;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.UnityURLFetcher
struct  UnityURLFetcher_t313076257  : public Il2CppObject
{
public:
	// Unibill.Impl.UnityHTTPRequest Unibill.Impl.UnityURLFetcher::request
	UnityHTTPRequest_t3521437524 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(UnityURLFetcher_t313076257, ___request_0)); }
	inline UnityHTTPRequest_t3521437524 * get_request_0() const { return ___request_0; }
	inline UnityHTTPRequest_t3521437524 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(UnityHTTPRequest_t3521437524 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier(&___request_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
