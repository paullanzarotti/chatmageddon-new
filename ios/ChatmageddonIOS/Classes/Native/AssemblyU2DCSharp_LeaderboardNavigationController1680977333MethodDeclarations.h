﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardNavigationController
struct LeaderboardNavigationController_t1680977333;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"

// System.Void LeaderboardNavigationController::.ctor()
extern "C"  void LeaderboardNavigationController__ctor_m1901932808 (LeaderboardNavigationController_t1680977333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::SetupStartScreen(LeaderboardNavScreen)
extern "C"  void LeaderboardNavigationController_SetupStartScreen_m933706187 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___startScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::NavigateBackwards()
extern "C"  void LeaderboardNavigationController_NavigateBackwards_m3879452555 (LeaderboardNavigationController_t1680977333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::NavigateForwards(LeaderboardNavScreen)
extern "C"  void LeaderboardNavigationController_NavigateForwards_m2837029683 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LeaderboardNavigationController::ValidateNavigation(LeaderboardNavScreen,NavigationDirection)
extern "C"  bool LeaderboardNavigationController_ValidateNavigation_m3053513947 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::LoadNavScreen(LeaderboardNavScreen,NavigationDirection,System.Boolean)
extern "C"  void LeaderboardNavigationController_LoadNavScreen_m2467391679 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::UnloadNavScreen(LeaderboardNavScreen,NavigationDirection)
extern "C"  void LeaderboardNavigationController_UnloadNavScreen_m929505751 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::PopulateTitleBar(LeaderboardNavScreen)
extern "C"  void LeaderboardNavigationController_PopulateTitleBar_m3489770051 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::UpdateActiveLeaderboardType(LeaderboardType)
extern "C"  void LeaderboardNavigationController_UpdateActiveLeaderboardType_m566824345 (LeaderboardNavigationController_t1680977333 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardNavigationController::UpdateLeaderboardSwitch()
extern "C"  void LeaderboardNavigationController_UpdateLeaderboardSwitch_m747836636 (LeaderboardNavigationController_t1680977333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
