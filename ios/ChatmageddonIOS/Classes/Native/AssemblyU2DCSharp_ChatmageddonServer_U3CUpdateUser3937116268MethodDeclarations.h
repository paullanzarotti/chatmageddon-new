﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserNotifications>c__AnonStorey1A
struct U3CUpdateUserNotificationsU3Ec__AnonStorey1A_t3937116268;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserNotifications>c__AnonStorey1A::.ctor()
extern "C"  void U3CUpdateUserNotificationsU3Ec__AnonStorey1A__ctor_m657994771 (U3CUpdateUserNotificationsU3Ec__AnonStorey1A_t3937116268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserNotifications>c__AnonStorey1A::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserNotificationsU3Ec__AnonStorey1A_U3CU3Em__0_m4213464260 (U3CUpdateUserNotificationsU3Ec__AnonStorey1A_t3937116268 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
