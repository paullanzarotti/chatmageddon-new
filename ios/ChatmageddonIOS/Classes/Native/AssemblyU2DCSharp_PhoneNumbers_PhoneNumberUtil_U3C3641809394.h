﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Len3057047663.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0
struct  U3CFindNumbersU3Ec__AnonStorey0_t3641809394  : public Il2CppObject
{
public:
	// System.String PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::text
	String_t* ___text_0;
	// System.String PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::defaultRegion
	String_t* ___defaultRegion_1;
	// PhoneNumbers.PhoneNumberUtil/Leniency PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::leniency
	int32_t ___leniency_2;
	// System.Int64 PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::maxTries
	int64_t ___maxTries_3;
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::$this
	PhoneNumberUtil_t4155573397 * ___U24this_4;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(U3CFindNumbersU3Ec__AnonStorey0_t3641809394, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}

	inline static int32_t get_offset_of_defaultRegion_1() { return static_cast<int32_t>(offsetof(U3CFindNumbersU3Ec__AnonStorey0_t3641809394, ___defaultRegion_1)); }
	inline String_t* get_defaultRegion_1() const { return ___defaultRegion_1; }
	inline String_t** get_address_of_defaultRegion_1() { return &___defaultRegion_1; }
	inline void set_defaultRegion_1(String_t* value)
	{
		___defaultRegion_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRegion_1, value);
	}

	inline static int32_t get_offset_of_leniency_2() { return static_cast<int32_t>(offsetof(U3CFindNumbersU3Ec__AnonStorey0_t3641809394, ___leniency_2)); }
	inline int32_t get_leniency_2() const { return ___leniency_2; }
	inline int32_t* get_address_of_leniency_2() { return &___leniency_2; }
	inline void set_leniency_2(int32_t value)
	{
		___leniency_2 = value;
	}

	inline static int32_t get_offset_of_maxTries_3() { return static_cast<int32_t>(offsetof(U3CFindNumbersU3Ec__AnonStorey0_t3641809394, ___maxTries_3)); }
	inline int64_t get_maxTries_3() const { return ___maxTries_3; }
	inline int64_t* get_address_of_maxTries_3() { return &___maxTries_3; }
	inline void set_maxTries_3(int64_t value)
	{
		___maxTries_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFindNumbersU3Ec__AnonStorey0_t3641809394, ___U24this_4)); }
	inline PhoneNumberUtil_t4155573397 * get_U24this_4() const { return ___U24this_4; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(PhoneNumberUtil_t4155573397 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
