﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.InstanceCreationEditor
struct InstanceCreationEditor_t1706597557;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ComponentModel.InstanceCreationEditor::.ctor()
extern "C"  void InstanceCreationEditor__ctor_m103129716 (InstanceCreationEditor_t1706597557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.InstanceCreationEditor::get_Text()
extern "C"  String_t* InstanceCreationEditor_get_Text_m4043687927 (InstanceCreationEditor_t1706597557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
