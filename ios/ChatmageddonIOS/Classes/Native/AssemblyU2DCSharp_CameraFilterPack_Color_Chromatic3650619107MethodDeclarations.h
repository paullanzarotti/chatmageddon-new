﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_Chromatic_Aberration
struct CameraFilterPack_Color_Chromatic_Aberration_t3650619107;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_Chromatic_Aberration::.ctor()
extern "C"  void CameraFilterPack_Color_Chromatic_Aberration__ctor_m1329699272 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Chromatic_Aberration::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_Chromatic_Aberration_get_material_m1487586995 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::Start()
extern "C"  void CameraFilterPack_Color_Chromatic_Aberration_Start_m2031386260 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_Chromatic_Aberration_OnRenderImage_m3597230916 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::OnValidate()
extern "C"  void CameraFilterPack_Color_Chromatic_Aberration_OnValidate_m497152065 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::Update()
extern "C"  void CameraFilterPack_Color_Chromatic_Aberration_Update_m8659979 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::OnDisable()
extern "C"  void CameraFilterPack_Color_Chromatic_Aberration_OnDisable_m2284217155 (CameraFilterPack_Color_Chromatic_Aberration_t3650619107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
