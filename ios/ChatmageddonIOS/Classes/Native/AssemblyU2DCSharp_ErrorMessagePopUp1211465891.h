﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// UISprite
struct UISprite_t603616735;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen962131611.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessagePopUp
struct  ErrorMessagePopUp_t1211465891  : public MonoSingleton_1_t962131611
{
public:
	// UIPanel ErrorMessagePopUp::panel
	UIPanel_t1795085332 * ___panel_3;
	// UISprite ErrorMessagePopUp::background
	UISprite_t603616735 * ___background_4;
	// UITexture ErrorMessagePopUp::backgroundTexture
	UITexture_t2537039969 * ___backgroundTexture_5;
	// UnityEngine.BoxCollider ErrorMessagePopUp::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_6;
	// UILabel ErrorMessagePopUp::errorLabel
	UILabel_t1795115428 * ___errorLabel_7;
	// TweenAlpha ErrorMessagePopUp::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_8;
	// System.Single ErrorMessagePopUp::errorViewTime
	float ___errorViewTime_9;
	// System.Boolean ErrorMessagePopUp::timed
	bool ___timed_10;
	// System.Boolean ErrorMessagePopUp::unloadingMessage
	bool ___unloadingMessage_11;

public:
	inline static int32_t get_offset_of_panel_3() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___panel_3)); }
	inline UIPanel_t1795085332 * get_panel_3() const { return ___panel_3; }
	inline UIPanel_t1795085332 ** get_address_of_panel_3() { return &___panel_3; }
	inline void set_panel_3(UIPanel_t1795085332 * value)
	{
		___panel_3 = value;
		Il2CppCodeGenWriteBarrier(&___panel_3, value);
	}

	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___background_4)); }
	inline UISprite_t603616735 * get_background_4() const { return ___background_4; }
	inline UISprite_t603616735 ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(UISprite_t603616735 * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier(&___background_4, value);
	}

	inline static int32_t get_offset_of_backgroundTexture_5() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___backgroundTexture_5)); }
	inline UITexture_t2537039969 * get_backgroundTexture_5() const { return ___backgroundTexture_5; }
	inline UITexture_t2537039969 ** get_address_of_backgroundTexture_5() { return &___backgroundTexture_5; }
	inline void set_backgroundTexture_5(UITexture_t2537039969 * value)
	{
		___backgroundTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTexture_5, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_6() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___backgroundCollider_6)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_6() const { return ___backgroundCollider_6; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_6() { return &___backgroundCollider_6; }
	inline void set_backgroundCollider_6(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_6 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_6, value);
	}

	inline static int32_t get_offset_of_errorLabel_7() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___errorLabel_7)); }
	inline UILabel_t1795115428 * get_errorLabel_7() const { return ___errorLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_errorLabel_7() { return &___errorLabel_7; }
	inline void set_errorLabel_7(UILabel_t1795115428 * value)
	{
		___errorLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___errorLabel_7, value);
	}

	inline static int32_t get_offset_of_alphaTween_8() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___alphaTween_8)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_8() const { return ___alphaTween_8; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_8() { return &___alphaTween_8; }
	inline void set_alphaTween_8(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_8, value);
	}

	inline static int32_t get_offset_of_errorViewTime_9() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___errorViewTime_9)); }
	inline float get_errorViewTime_9() const { return ___errorViewTime_9; }
	inline float* get_address_of_errorViewTime_9() { return &___errorViewTime_9; }
	inline void set_errorViewTime_9(float value)
	{
		___errorViewTime_9 = value;
	}

	inline static int32_t get_offset_of_timed_10() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___timed_10)); }
	inline bool get_timed_10() const { return ___timed_10; }
	inline bool* get_address_of_timed_10() { return &___timed_10; }
	inline void set_timed_10(bool value)
	{
		___timed_10 = value;
	}

	inline static int32_t get_offset_of_unloadingMessage_11() { return static_cast<int32_t>(offsetof(ErrorMessagePopUp_t1211465891, ___unloadingMessage_11)); }
	inline bool get_unloadingMessage_11() const { return ___unloadingMessage_11; }
	inline bool* get_address_of_unloadingMessage_11() { return &___unloadingMessage_11; }
	inline void set_unloadingMessage_11(bool value)
	{
		___unloadingMessage_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
