﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// MissileSwipeController
struct MissileSwipeController_t2686719534;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackInventoryUIScaler
struct  AttackInventoryUIScaler_t3118091758  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UITexture AttackInventoryUIScaler::background
	UITexture_t2537039969 * ___background_14;
	// UnityEngine.Transform AttackInventoryUIScaler::itemScaleMeasure
	Transform_t3275118058 * ___itemScaleMeasure_15;
	// UnityEngine.Transform AttackInventoryUIScaler::itemAmount
	Transform_t3275118058 * ___itemAmount_16;
	// UnityEngine.Transform AttackInventoryUIScaler::infinitySymbol
	Transform_t3275118058 * ___infinitySymbol_17;
	// UnityEngine.Transform AttackInventoryUIScaler::missileInfoButton
	Transform_t3275118058 * ___missileInfoButton_18;
	// UnityEngine.BoxCollider AttackInventoryUIScaler::itemSwipeCollider
	BoxCollider_t22920061 * ___itemSwipeCollider_19;
	// MissileSwipeController AttackInventoryUIScaler::swipe
	MissileSwipeController_t2686719534 * ___swipe_20;
	// UISprite AttackInventoryUIScaler::selectMissileButton
	UISprite_t603616735 * ___selectMissileButton_21;
	// UnityEngine.BoxCollider AttackInventoryUIScaler::selectMissileCollider
	BoxCollider_t22920061 * ___selectMissileCollider_22;
	// UISprite AttackInventoryUIScaler::buyMoreButton
	UISprite_t603616735 * ___buyMoreButton_23;
	// UILabel AttackInventoryUIScaler::buyMoreLabel
	UILabel_t1795115428 * ___buyMoreLabel_24;
	// UnityEngine.BoxCollider AttackInventoryUIScaler::buyMoreCollider
	BoxCollider_t22920061 * ___buyMoreCollider_25;
	// UISprite AttackInventoryUIScaler::targetsButton
	UISprite_t603616735 * ___targetsButton_26;
	// UILabel AttackInventoryUIScaler::targetsLabel
	UILabel_t1795115428 * ___targetsLabel_27;
	// UnityEngine.BoxCollider AttackInventoryUIScaler::targetsCollider
	BoxCollider_t22920061 * ___targetsCollider_28;
	// UISprite AttackInventoryUIScaler::topHorizLine
	UISprite_t603616735 * ___topHorizLine_29;
	// UISprite AttackInventoryUIScaler::botHorizLine
	UISprite_t603616735 * ___botHorizLine_30;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___background_14)); }
	inline UITexture_t2537039969 * get_background_14() const { return ___background_14; }
	inline UITexture_t2537039969 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UITexture_t2537039969 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_itemScaleMeasure_15() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___itemScaleMeasure_15)); }
	inline Transform_t3275118058 * get_itemScaleMeasure_15() const { return ___itemScaleMeasure_15; }
	inline Transform_t3275118058 ** get_address_of_itemScaleMeasure_15() { return &___itemScaleMeasure_15; }
	inline void set_itemScaleMeasure_15(Transform_t3275118058 * value)
	{
		___itemScaleMeasure_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemScaleMeasure_15, value);
	}

	inline static int32_t get_offset_of_itemAmount_16() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___itemAmount_16)); }
	inline Transform_t3275118058 * get_itemAmount_16() const { return ___itemAmount_16; }
	inline Transform_t3275118058 ** get_address_of_itemAmount_16() { return &___itemAmount_16; }
	inline void set_itemAmount_16(Transform_t3275118058 * value)
	{
		___itemAmount_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemAmount_16, value);
	}

	inline static int32_t get_offset_of_infinitySymbol_17() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___infinitySymbol_17)); }
	inline Transform_t3275118058 * get_infinitySymbol_17() const { return ___infinitySymbol_17; }
	inline Transform_t3275118058 ** get_address_of_infinitySymbol_17() { return &___infinitySymbol_17; }
	inline void set_infinitySymbol_17(Transform_t3275118058 * value)
	{
		___infinitySymbol_17 = value;
		Il2CppCodeGenWriteBarrier(&___infinitySymbol_17, value);
	}

	inline static int32_t get_offset_of_missileInfoButton_18() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___missileInfoButton_18)); }
	inline Transform_t3275118058 * get_missileInfoButton_18() const { return ___missileInfoButton_18; }
	inline Transform_t3275118058 ** get_address_of_missileInfoButton_18() { return &___missileInfoButton_18; }
	inline void set_missileInfoButton_18(Transform_t3275118058 * value)
	{
		___missileInfoButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___missileInfoButton_18, value);
	}

	inline static int32_t get_offset_of_itemSwipeCollider_19() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___itemSwipeCollider_19)); }
	inline BoxCollider_t22920061 * get_itemSwipeCollider_19() const { return ___itemSwipeCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_itemSwipeCollider_19() { return &___itemSwipeCollider_19; }
	inline void set_itemSwipeCollider_19(BoxCollider_t22920061 * value)
	{
		___itemSwipeCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___itemSwipeCollider_19, value);
	}

	inline static int32_t get_offset_of_swipe_20() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___swipe_20)); }
	inline MissileSwipeController_t2686719534 * get_swipe_20() const { return ___swipe_20; }
	inline MissileSwipeController_t2686719534 ** get_address_of_swipe_20() { return &___swipe_20; }
	inline void set_swipe_20(MissileSwipeController_t2686719534 * value)
	{
		___swipe_20 = value;
		Il2CppCodeGenWriteBarrier(&___swipe_20, value);
	}

	inline static int32_t get_offset_of_selectMissileButton_21() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___selectMissileButton_21)); }
	inline UISprite_t603616735 * get_selectMissileButton_21() const { return ___selectMissileButton_21; }
	inline UISprite_t603616735 ** get_address_of_selectMissileButton_21() { return &___selectMissileButton_21; }
	inline void set_selectMissileButton_21(UISprite_t603616735 * value)
	{
		___selectMissileButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___selectMissileButton_21, value);
	}

	inline static int32_t get_offset_of_selectMissileCollider_22() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___selectMissileCollider_22)); }
	inline BoxCollider_t22920061 * get_selectMissileCollider_22() const { return ___selectMissileCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_selectMissileCollider_22() { return &___selectMissileCollider_22; }
	inline void set_selectMissileCollider_22(BoxCollider_t22920061 * value)
	{
		___selectMissileCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___selectMissileCollider_22, value);
	}

	inline static int32_t get_offset_of_buyMoreButton_23() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___buyMoreButton_23)); }
	inline UISprite_t603616735 * get_buyMoreButton_23() const { return ___buyMoreButton_23; }
	inline UISprite_t603616735 ** get_address_of_buyMoreButton_23() { return &___buyMoreButton_23; }
	inline void set_buyMoreButton_23(UISprite_t603616735 * value)
	{
		___buyMoreButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreButton_23, value);
	}

	inline static int32_t get_offset_of_buyMoreLabel_24() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___buyMoreLabel_24)); }
	inline UILabel_t1795115428 * get_buyMoreLabel_24() const { return ___buyMoreLabel_24; }
	inline UILabel_t1795115428 ** get_address_of_buyMoreLabel_24() { return &___buyMoreLabel_24; }
	inline void set_buyMoreLabel_24(UILabel_t1795115428 * value)
	{
		___buyMoreLabel_24 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreLabel_24, value);
	}

	inline static int32_t get_offset_of_buyMoreCollider_25() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___buyMoreCollider_25)); }
	inline BoxCollider_t22920061 * get_buyMoreCollider_25() const { return ___buyMoreCollider_25; }
	inline BoxCollider_t22920061 ** get_address_of_buyMoreCollider_25() { return &___buyMoreCollider_25; }
	inline void set_buyMoreCollider_25(BoxCollider_t22920061 * value)
	{
		___buyMoreCollider_25 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreCollider_25, value);
	}

	inline static int32_t get_offset_of_targetsButton_26() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___targetsButton_26)); }
	inline UISprite_t603616735 * get_targetsButton_26() const { return ___targetsButton_26; }
	inline UISprite_t603616735 ** get_address_of_targetsButton_26() { return &___targetsButton_26; }
	inline void set_targetsButton_26(UISprite_t603616735 * value)
	{
		___targetsButton_26 = value;
		Il2CppCodeGenWriteBarrier(&___targetsButton_26, value);
	}

	inline static int32_t get_offset_of_targetsLabel_27() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___targetsLabel_27)); }
	inline UILabel_t1795115428 * get_targetsLabel_27() const { return ___targetsLabel_27; }
	inline UILabel_t1795115428 ** get_address_of_targetsLabel_27() { return &___targetsLabel_27; }
	inline void set_targetsLabel_27(UILabel_t1795115428 * value)
	{
		___targetsLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___targetsLabel_27, value);
	}

	inline static int32_t get_offset_of_targetsCollider_28() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___targetsCollider_28)); }
	inline BoxCollider_t22920061 * get_targetsCollider_28() const { return ___targetsCollider_28; }
	inline BoxCollider_t22920061 ** get_address_of_targetsCollider_28() { return &___targetsCollider_28; }
	inline void set_targetsCollider_28(BoxCollider_t22920061 * value)
	{
		___targetsCollider_28 = value;
		Il2CppCodeGenWriteBarrier(&___targetsCollider_28, value);
	}

	inline static int32_t get_offset_of_topHorizLine_29() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___topHorizLine_29)); }
	inline UISprite_t603616735 * get_topHorizLine_29() const { return ___topHorizLine_29; }
	inline UISprite_t603616735 ** get_address_of_topHorizLine_29() { return &___topHorizLine_29; }
	inline void set_topHorizLine_29(UISprite_t603616735 * value)
	{
		___topHorizLine_29 = value;
		Il2CppCodeGenWriteBarrier(&___topHorizLine_29, value);
	}

	inline static int32_t get_offset_of_botHorizLine_30() { return static_cast<int32_t>(offsetof(AttackInventoryUIScaler_t3118091758, ___botHorizLine_30)); }
	inline UISprite_t603616735 * get_botHorizLine_30() const { return ___botHorizLine_30; }
	inline UISprite_t603616735 ** get_address_of_botHorizLine_30() { return &___botHorizLine_30; }
	inline void set_botHorizLine_30(UISprite_t603616735 * value)
	{
		___botHorizLine_30 = value;
		Il2CppCodeGenWriteBarrier(&___botHorizLine_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
