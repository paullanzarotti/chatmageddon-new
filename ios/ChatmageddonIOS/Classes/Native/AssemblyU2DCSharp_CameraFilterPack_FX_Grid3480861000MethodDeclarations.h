﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Grid
struct CameraFilterPack_FX_Grid_t3480861000;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Grid::.ctor()
extern "C"  void CameraFilterPack_FX_Grid__ctor_m3853496845 (CameraFilterPack_FX_Grid_t3480861000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Grid::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Grid_get_material_m1500795470 (CameraFilterPack_FX_Grid_t3480861000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::Start()
extern "C"  void CameraFilterPack_FX_Grid_Start_m1325308757 (CameraFilterPack_FX_Grid_t3480861000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Grid_OnRenderImage_m1409610309 (CameraFilterPack_FX_Grid_t3480861000 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::OnValidate()
extern "C"  void CameraFilterPack_FX_Grid_OnValidate_m2352062864 (CameraFilterPack_FX_Grid_t3480861000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::Update()
extern "C"  void CameraFilterPack_FX_Grid_Update_m3572196950 (CameraFilterPack_FX_Grid_t3480861000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::OnDisable()
extern "C"  void CameraFilterPack_FX_Grid_OnDisable_m3979762632 (CameraFilterPack_FX_Grid_t3480861000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
