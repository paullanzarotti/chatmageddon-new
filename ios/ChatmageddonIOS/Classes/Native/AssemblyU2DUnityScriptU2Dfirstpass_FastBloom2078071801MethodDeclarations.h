﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FastBloom
struct FastBloom_t2078071801;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void FastBloom::.ctor()
extern "C"  void FastBloom__ctor_m340061435 (FastBloom_t2078071801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FastBloom::CheckResources()
extern "C"  bool FastBloom_CheckResources_m2033007424 (FastBloom_t2078071801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastBloom::OnDisable()
extern "C"  void FastBloom_OnDisable_m2578696506 (FastBloom_t2078071801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastBloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void FastBloom_OnRenderImage_m3652901015 (FastBloom_t2078071801 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastBloom::Main()
extern "C"  void FastBloom_Main_m3398367532 (FastBloom_t2078071801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
