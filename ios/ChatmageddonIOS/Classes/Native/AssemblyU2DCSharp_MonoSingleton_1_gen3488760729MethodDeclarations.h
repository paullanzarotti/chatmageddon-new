﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<LoginBottomUIManager>::.ctor()
#define MonoSingleton_1__ctor_m3261432883(__this, method) ((  void (*) (MonoSingleton_1_t3488760729 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<LoginBottomUIManager>::Awake()
#define MonoSingleton_1_Awake_m1653766828(__this, method) ((  void (*) (MonoSingleton_1_t3488760729 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<LoginBottomUIManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3398713466(__this /* static, unused */, method) ((  LoginBottomUIManager_t3738095009 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<LoginBottomUIManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2001157810(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<LoginBottomUIManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1141598405(__this, method) ((  void (*) (MonoSingleton_1_t3488760729 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<LoginBottomUIManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m603338349(__this, method) ((  void (*) (MonoSingleton_1_t3488760729 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<LoginBottomUIManager>::.cctor()
#define MonoSingleton_1__cctor_m2455849634(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
