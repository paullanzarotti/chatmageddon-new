﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookMeResult/FacebookMeLocation
struct FacebookMeLocation_t1093501248;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookMeResult/FacebookMeLocation::.ctor()
extern "C"  void FacebookMeLocation__ctor_m15041825 (FacebookMeLocation_t1093501248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
