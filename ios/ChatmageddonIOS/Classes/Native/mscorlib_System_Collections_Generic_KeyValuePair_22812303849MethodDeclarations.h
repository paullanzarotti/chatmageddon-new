﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22812303849.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1919189137_gshared (KeyValuePair_2_t2812303849 * __this, Il2CppChar ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1919189137(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2812303849 *, Il2CppChar, Il2CppChar, const MethodInfo*))KeyValuePair_2__ctor_m1919189137_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>::get_Key()
extern "C"  Il2CppChar KeyValuePair_2_get_Key_m1316286598_gshared (KeyValuePair_2_t2812303849 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1316286598(__this, method) ((  Il2CppChar (*) (KeyValuePair_2_t2812303849 *, const MethodInfo*))KeyValuePair_2_get_Key_m1316286598_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3381729268_gshared (KeyValuePair_2_t2812303849 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3381729268(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2812303849 *, Il2CppChar, const MethodInfo*))KeyValuePair_2_set_Key_m3381729268_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>::get_Value()
extern "C"  Il2CppChar KeyValuePair_2_get_Value_m2495043251_gshared (KeyValuePair_2_t2812303849 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2495043251(__this, method) ((  Il2CppChar (*) (KeyValuePair_2_t2812303849 *, const MethodInfo*))KeyValuePair_2_get_Value_m2495043251_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m40225932_gshared (KeyValuePair_2_t2812303849 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m40225932(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2812303849 *, Il2CppChar, const MethodInfo*))KeyValuePair_2_set_Value_m40225932_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m607016332_gshared (KeyValuePair_2_t2812303849 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m607016332(__this, method) ((  String_t* (*) (KeyValuePair_2_t2812303849 *, const MethodInfo*))KeyValuePair_2_ToString_m607016332_gshared)(__this, method)
