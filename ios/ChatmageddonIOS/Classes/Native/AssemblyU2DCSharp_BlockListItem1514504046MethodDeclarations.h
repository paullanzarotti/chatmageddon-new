﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlockListItem
struct BlockListItem_t1514504046;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void BlockListItem::.ctor()
extern "C"  void BlockListItem__ctor_m2326207953 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::SetUpItem()
extern "C"  void BlockListItem_SetUpItem_m2854500247 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::PopulateItem(Friend)
extern "C"  void BlockListItem_PopulateItem_m3589852264 (BlockListItem_t1514504046 * __this, Friend_t3555014108 * ___blockedfriend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::SetVisible()
extern "C"  void BlockListItem_SetVisible_m1930897301 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::SetInvisible()
extern "C"  void BlockListItem_SetInvisible_m1024419782 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlockListItem::verifyVisibility()
extern "C"  bool BlockListItem_verifyVisibility_m531998366 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::CheckVisibilty()
extern "C"  void BlockListItem_CheckVisibilty_m1119180474 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::Update()
extern "C"  void BlockListItem_Update_m534766636 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::OnEnable()
extern "C"  void BlockListItem_OnEnable_m770204297 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::FindScrollView()
extern "C"  void BlockListItem_FindScrollView_m141041962 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::OnPress(System.Boolean)
extern "C"  void BlockListItem_OnPress_m265336500 (BlockListItem_t1514504046 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void BlockListItem_OnDrag_m4250214774 (BlockListItem_t1514504046 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::OnDragEnd()
extern "C"  void BlockListItem_OnDragEnd_m2202742983 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::OnScroll(System.Single)
extern "C"  void BlockListItem_OnScroll_m3018090794 (BlockListItem_t1514504046 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItem::OnClick()
extern "C"  void BlockListItem_OnClick_m1107475066 (BlockListItem_t1514504046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
