﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsUIScaler
struct SettingsUIScaler_t565922441;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsUIScaler::.ctor()
extern "C"  void SettingsUIScaler__ctor_m2819295434 (SettingsUIScaler_t565922441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsUIScaler::GlobalUIScale()
extern "C"  void SettingsUIScaler_GlobalUIScale_m1571787827 (SettingsUIScaler_t565922441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
