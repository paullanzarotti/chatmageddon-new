﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetFullWarefareStatus>c__AnonStorey25
struct U3CGetFullWarefareStatusU3Ec__AnonStorey25_t3527477823;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetFullWarefareStatus>c__AnonStorey25::.ctor()
extern "C"  void U3CGetFullWarefareStatusU3Ec__AnonStorey25__ctor_m2926347466 (U3CGetFullWarefareStatusU3Ec__AnonStorey25_t3527477823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetFullWarefareStatus>c__AnonStorey25::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetFullWarefareStatusU3Ec__AnonStorey25_U3CU3Em__0_m2548047203 (U3CGetFullWarefareStatusU3Ec__AnonStorey25_t3527477823 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
