﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AimMarker
struct AimMarker_t327093161;
// AttackMiniGameStateButton
struct AttackMiniGameStateButton_t3439103466;
// GameLabelPopulator
struct GameLabelPopulator_t3519752206;
// LaunchAccuracyController
struct LaunchAccuracyController_t4092159714;

#include "AssemblyU2DCSharp_MiniGameController3067676539.h"
#include "AssemblyU2DCSharp_AttackGameState4284764851.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackMiniGameController
struct  AttackMiniGameController_t105133407  : public MiniGameController_t3067676539
{
public:
	// AttackGameState AttackMiniGameController::currentState
	int32_t ___currentState_4;
	// AimMarker AttackMiniGameController::aimMarker
	AimMarker_t327093161 * ___aimMarker_5;
	// AttackMiniGameStateButton AttackMiniGameController::stateButton
	AttackMiniGameStateButton_t3439103466 * ___stateButton_6;
	// System.Single AttackMiniGameController::aimAccuracy
	float ___aimAccuracy_7;
	// System.Single AttackMiniGameController::fireAccuracy
	float ___fireAccuracy_8;
	// System.Single AttackMiniGameController::totalAccuracy
	float ___totalAccuracy_9;
	// System.Boolean AttackMiniGameController::perfectLaunch
	bool ___perfectLaunch_10;
	// System.Boolean AttackMiniGameController::backfireLaunch
	bool ___backfireLaunch_11;
	// GameLabelPopulator AttackMiniGameController::labelPopulator
	GameLabelPopulator_t3519752206 * ___labelPopulator_12;
	// LaunchAccuracyController AttackMiniGameController::laController
	LaunchAccuracyController_t4092159714 * ___laController_13;

public:
	inline static int32_t get_offset_of_currentState_4() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___currentState_4)); }
	inline int32_t get_currentState_4() const { return ___currentState_4; }
	inline int32_t* get_address_of_currentState_4() { return &___currentState_4; }
	inline void set_currentState_4(int32_t value)
	{
		___currentState_4 = value;
	}

	inline static int32_t get_offset_of_aimMarker_5() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___aimMarker_5)); }
	inline AimMarker_t327093161 * get_aimMarker_5() const { return ___aimMarker_5; }
	inline AimMarker_t327093161 ** get_address_of_aimMarker_5() { return &___aimMarker_5; }
	inline void set_aimMarker_5(AimMarker_t327093161 * value)
	{
		___aimMarker_5 = value;
		Il2CppCodeGenWriteBarrier(&___aimMarker_5, value);
	}

	inline static int32_t get_offset_of_stateButton_6() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___stateButton_6)); }
	inline AttackMiniGameStateButton_t3439103466 * get_stateButton_6() const { return ___stateButton_6; }
	inline AttackMiniGameStateButton_t3439103466 ** get_address_of_stateButton_6() { return &___stateButton_6; }
	inline void set_stateButton_6(AttackMiniGameStateButton_t3439103466 * value)
	{
		___stateButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___stateButton_6, value);
	}

	inline static int32_t get_offset_of_aimAccuracy_7() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___aimAccuracy_7)); }
	inline float get_aimAccuracy_7() const { return ___aimAccuracy_7; }
	inline float* get_address_of_aimAccuracy_7() { return &___aimAccuracy_7; }
	inline void set_aimAccuracy_7(float value)
	{
		___aimAccuracy_7 = value;
	}

	inline static int32_t get_offset_of_fireAccuracy_8() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___fireAccuracy_8)); }
	inline float get_fireAccuracy_8() const { return ___fireAccuracy_8; }
	inline float* get_address_of_fireAccuracy_8() { return &___fireAccuracy_8; }
	inline void set_fireAccuracy_8(float value)
	{
		___fireAccuracy_8 = value;
	}

	inline static int32_t get_offset_of_totalAccuracy_9() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___totalAccuracy_9)); }
	inline float get_totalAccuracy_9() const { return ___totalAccuracy_9; }
	inline float* get_address_of_totalAccuracy_9() { return &___totalAccuracy_9; }
	inline void set_totalAccuracy_9(float value)
	{
		___totalAccuracy_9 = value;
	}

	inline static int32_t get_offset_of_perfectLaunch_10() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___perfectLaunch_10)); }
	inline bool get_perfectLaunch_10() const { return ___perfectLaunch_10; }
	inline bool* get_address_of_perfectLaunch_10() { return &___perfectLaunch_10; }
	inline void set_perfectLaunch_10(bool value)
	{
		___perfectLaunch_10 = value;
	}

	inline static int32_t get_offset_of_backfireLaunch_11() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___backfireLaunch_11)); }
	inline bool get_backfireLaunch_11() const { return ___backfireLaunch_11; }
	inline bool* get_address_of_backfireLaunch_11() { return &___backfireLaunch_11; }
	inline void set_backfireLaunch_11(bool value)
	{
		___backfireLaunch_11 = value;
	}

	inline static int32_t get_offset_of_labelPopulator_12() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___labelPopulator_12)); }
	inline GameLabelPopulator_t3519752206 * get_labelPopulator_12() const { return ___labelPopulator_12; }
	inline GameLabelPopulator_t3519752206 ** get_address_of_labelPopulator_12() { return &___labelPopulator_12; }
	inline void set_labelPopulator_12(GameLabelPopulator_t3519752206 * value)
	{
		___labelPopulator_12 = value;
		Il2CppCodeGenWriteBarrier(&___labelPopulator_12, value);
	}

	inline static int32_t get_offset_of_laController_13() { return static_cast<int32_t>(offsetof(AttackMiniGameController_t105133407, ___laController_13)); }
	inline LaunchAccuracyController_t4092159714 * get_laController_13() const { return ___laController_13; }
	inline LaunchAccuracyController_t4092159714 ** get_address_of_laController_13() { return &___laController_13; }
	inline void set_laController_13(LaunchAccuracyController_t4092159714 * value)
	{
		___laController_13 = value;
		Il2CppCodeGenWriteBarrier(&___laController_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
