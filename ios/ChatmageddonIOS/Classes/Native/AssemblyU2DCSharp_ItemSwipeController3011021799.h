﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemSelectionSwipe
struct ItemSelectionSwipe_t2104393431;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TweenPosition
struct TweenPosition_t1144714832;
// TweenScale
struct TweenScale_t2697902175;
// TweenRotation
struct TweenRotation_t1747194511;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// ItemSwipeController/OnLeftComplete
struct OnLeftComplete_t1529246307;
// ItemSwipeController/OnRightComplete
struct OnRightComplete_t1521737374;
// ItemSwipeController/OnHideComplete
struct OnHideComplete_t4141700430;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemSwipeController
struct  ItemSwipeController_t3011021799  : public MonoBehaviour_t1158329972
{
public:
	// ItemSelectionSwipe ItemSwipeController::itemSwipe
	ItemSelectionSwipe_t2104393431 * ___itemSwipe_2;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemPosition
	Vector3_t2243707580  ___m_ItemPosition_3;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemScale
	Vector3_t2243707580  ___m_ItemScale_4;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemLeftPosition
	Vector3_t2243707580  ___m_ItemLeftPosition_5;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemRightPosition
	Vector3_t2243707580  ___m_ItemRightPosition_6;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemLeftScale
	Vector3_t2243707580  ___m_ItemLeftScale_7;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemRightScale
	Vector3_t2243707580  ___m_ItemRightScale_8;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemDescriptionPosition
	Vector3_t2243707580  ___m_ItemDescriptionPosition_9;
	// UnityEngine.Vector3 ItemSwipeController::m_ItemDescriptionScale
	Vector3_t2243707580  ___m_ItemDescriptionScale_10;
	// UnityEngine.GameObject ItemSwipeController::m_DisplayItemObject
	GameObject_t1756533147 * ___m_DisplayItemObject_11;
	// System.Single ItemSwipeController::defaultPositionTime
	float ___defaultPositionTime_12;
	// System.Single ItemSwipeController::leftrightPositionTime
	float ___leftrightPositionTime_13;
	// TweenPosition ItemSwipeController::tween
	TweenPosition_t1144714832 * ___tween_14;
	// TweenScale ItemSwipeController::tweenScale
	TweenScale_t2697902175 * ___tweenScale_15;
	// TweenRotation ItemSwipeController::tweenRotation
	TweenRotation_t1747194511 * ___tweenRotation_16;
	// UnityEngine.AnimationCurve ItemSwipeController::curve
	AnimationCurve_t3306541151 * ___curve_17;
	// UnityEngine.GameObject ItemSwipeController::currentModel
	GameObject_t1756533147 * ___currentModel_18;
	// ItemSwipeController/OnLeftComplete ItemSwipeController::onLeftComplete
	OnLeftComplete_t1529246307 * ___onLeftComplete_19;
	// ItemSwipeController/OnRightComplete ItemSwipeController::onRightComplete
	OnRightComplete_t1521737374 * ___onRightComplete_20;
	// ItemSwipeController/OnHideComplete ItemSwipeController::onHideComplete
	OnHideComplete_t4141700430 * ___onHideComplete_21;
	// System.Boolean ItemSwipeController::itemClosing
	bool ___itemClosing_22;

public:
	inline static int32_t get_offset_of_itemSwipe_2() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___itemSwipe_2)); }
	inline ItemSelectionSwipe_t2104393431 * get_itemSwipe_2() const { return ___itemSwipe_2; }
	inline ItemSelectionSwipe_t2104393431 ** get_address_of_itemSwipe_2() { return &___itemSwipe_2; }
	inline void set_itemSwipe_2(ItemSelectionSwipe_t2104393431 * value)
	{
		___itemSwipe_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemSwipe_2, value);
	}

	inline static int32_t get_offset_of_m_ItemPosition_3() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemPosition_3)); }
	inline Vector3_t2243707580  get_m_ItemPosition_3() const { return ___m_ItemPosition_3; }
	inline Vector3_t2243707580 * get_address_of_m_ItemPosition_3() { return &___m_ItemPosition_3; }
	inline void set_m_ItemPosition_3(Vector3_t2243707580  value)
	{
		___m_ItemPosition_3 = value;
	}

	inline static int32_t get_offset_of_m_ItemScale_4() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemScale_4)); }
	inline Vector3_t2243707580  get_m_ItemScale_4() const { return ___m_ItemScale_4; }
	inline Vector3_t2243707580 * get_address_of_m_ItemScale_4() { return &___m_ItemScale_4; }
	inline void set_m_ItemScale_4(Vector3_t2243707580  value)
	{
		___m_ItemScale_4 = value;
	}

	inline static int32_t get_offset_of_m_ItemLeftPosition_5() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemLeftPosition_5)); }
	inline Vector3_t2243707580  get_m_ItemLeftPosition_5() const { return ___m_ItemLeftPosition_5; }
	inline Vector3_t2243707580 * get_address_of_m_ItemLeftPosition_5() { return &___m_ItemLeftPosition_5; }
	inline void set_m_ItemLeftPosition_5(Vector3_t2243707580  value)
	{
		___m_ItemLeftPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_ItemRightPosition_6() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemRightPosition_6)); }
	inline Vector3_t2243707580  get_m_ItemRightPosition_6() const { return ___m_ItemRightPosition_6; }
	inline Vector3_t2243707580 * get_address_of_m_ItemRightPosition_6() { return &___m_ItemRightPosition_6; }
	inline void set_m_ItemRightPosition_6(Vector3_t2243707580  value)
	{
		___m_ItemRightPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_ItemLeftScale_7() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemLeftScale_7)); }
	inline Vector3_t2243707580  get_m_ItemLeftScale_7() const { return ___m_ItemLeftScale_7; }
	inline Vector3_t2243707580 * get_address_of_m_ItemLeftScale_7() { return &___m_ItemLeftScale_7; }
	inline void set_m_ItemLeftScale_7(Vector3_t2243707580  value)
	{
		___m_ItemLeftScale_7 = value;
	}

	inline static int32_t get_offset_of_m_ItemRightScale_8() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemRightScale_8)); }
	inline Vector3_t2243707580  get_m_ItemRightScale_8() const { return ___m_ItemRightScale_8; }
	inline Vector3_t2243707580 * get_address_of_m_ItemRightScale_8() { return &___m_ItemRightScale_8; }
	inline void set_m_ItemRightScale_8(Vector3_t2243707580  value)
	{
		___m_ItemRightScale_8 = value;
	}

	inline static int32_t get_offset_of_m_ItemDescriptionPosition_9() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemDescriptionPosition_9)); }
	inline Vector3_t2243707580  get_m_ItemDescriptionPosition_9() const { return ___m_ItemDescriptionPosition_9; }
	inline Vector3_t2243707580 * get_address_of_m_ItemDescriptionPosition_9() { return &___m_ItemDescriptionPosition_9; }
	inline void set_m_ItemDescriptionPosition_9(Vector3_t2243707580  value)
	{
		___m_ItemDescriptionPosition_9 = value;
	}

	inline static int32_t get_offset_of_m_ItemDescriptionScale_10() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_ItemDescriptionScale_10)); }
	inline Vector3_t2243707580  get_m_ItemDescriptionScale_10() const { return ___m_ItemDescriptionScale_10; }
	inline Vector3_t2243707580 * get_address_of_m_ItemDescriptionScale_10() { return &___m_ItemDescriptionScale_10; }
	inline void set_m_ItemDescriptionScale_10(Vector3_t2243707580  value)
	{
		___m_ItemDescriptionScale_10 = value;
	}

	inline static int32_t get_offset_of_m_DisplayItemObject_11() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___m_DisplayItemObject_11)); }
	inline GameObject_t1756533147 * get_m_DisplayItemObject_11() const { return ___m_DisplayItemObject_11; }
	inline GameObject_t1756533147 ** get_address_of_m_DisplayItemObject_11() { return &___m_DisplayItemObject_11; }
	inline void set_m_DisplayItemObject_11(GameObject_t1756533147 * value)
	{
		___m_DisplayItemObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_DisplayItemObject_11, value);
	}

	inline static int32_t get_offset_of_defaultPositionTime_12() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___defaultPositionTime_12)); }
	inline float get_defaultPositionTime_12() const { return ___defaultPositionTime_12; }
	inline float* get_address_of_defaultPositionTime_12() { return &___defaultPositionTime_12; }
	inline void set_defaultPositionTime_12(float value)
	{
		___defaultPositionTime_12 = value;
	}

	inline static int32_t get_offset_of_leftrightPositionTime_13() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___leftrightPositionTime_13)); }
	inline float get_leftrightPositionTime_13() const { return ___leftrightPositionTime_13; }
	inline float* get_address_of_leftrightPositionTime_13() { return &___leftrightPositionTime_13; }
	inline void set_leftrightPositionTime_13(float value)
	{
		___leftrightPositionTime_13 = value;
	}

	inline static int32_t get_offset_of_tween_14() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___tween_14)); }
	inline TweenPosition_t1144714832 * get_tween_14() const { return ___tween_14; }
	inline TweenPosition_t1144714832 ** get_address_of_tween_14() { return &___tween_14; }
	inline void set_tween_14(TweenPosition_t1144714832 * value)
	{
		___tween_14 = value;
		Il2CppCodeGenWriteBarrier(&___tween_14, value);
	}

	inline static int32_t get_offset_of_tweenScale_15() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___tweenScale_15)); }
	inline TweenScale_t2697902175 * get_tweenScale_15() const { return ___tweenScale_15; }
	inline TweenScale_t2697902175 ** get_address_of_tweenScale_15() { return &___tweenScale_15; }
	inline void set_tweenScale_15(TweenScale_t2697902175 * value)
	{
		___tweenScale_15 = value;
		Il2CppCodeGenWriteBarrier(&___tweenScale_15, value);
	}

	inline static int32_t get_offset_of_tweenRotation_16() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___tweenRotation_16)); }
	inline TweenRotation_t1747194511 * get_tweenRotation_16() const { return ___tweenRotation_16; }
	inline TweenRotation_t1747194511 ** get_address_of_tweenRotation_16() { return &___tweenRotation_16; }
	inline void set_tweenRotation_16(TweenRotation_t1747194511 * value)
	{
		___tweenRotation_16 = value;
		Il2CppCodeGenWriteBarrier(&___tweenRotation_16, value);
	}

	inline static int32_t get_offset_of_curve_17() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___curve_17)); }
	inline AnimationCurve_t3306541151 * get_curve_17() const { return ___curve_17; }
	inline AnimationCurve_t3306541151 ** get_address_of_curve_17() { return &___curve_17; }
	inline void set_curve_17(AnimationCurve_t3306541151 * value)
	{
		___curve_17 = value;
		Il2CppCodeGenWriteBarrier(&___curve_17, value);
	}

	inline static int32_t get_offset_of_currentModel_18() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___currentModel_18)); }
	inline GameObject_t1756533147 * get_currentModel_18() const { return ___currentModel_18; }
	inline GameObject_t1756533147 ** get_address_of_currentModel_18() { return &___currentModel_18; }
	inline void set_currentModel_18(GameObject_t1756533147 * value)
	{
		___currentModel_18 = value;
		Il2CppCodeGenWriteBarrier(&___currentModel_18, value);
	}

	inline static int32_t get_offset_of_onLeftComplete_19() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___onLeftComplete_19)); }
	inline OnLeftComplete_t1529246307 * get_onLeftComplete_19() const { return ___onLeftComplete_19; }
	inline OnLeftComplete_t1529246307 ** get_address_of_onLeftComplete_19() { return &___onLeftComplete_19; }
	inline void set_onLeftComplete_19(OnLeftComplete_t1529246307 * value)
	{
		___onLeftComplete_19 = value;
		Il2CppCodeGenWriteBarrier(&___onLeftComplete_19, value);
	}

	inline static int32_t get_offset_of_onRightComplete_20() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___onRightComplete_20)); }
	inline OnRightComplete_t1521737374 * get_onRightComplete_20() const { return ___onRightComplete_20; }
	inline OnRightComplete_t1521737374 ** get_address_of_onRightComplete_20() { return &___onRightComplete_20; }
	inline void set_onRightComplete_20(OnRightComplete_t1521737374 * value)
	{
		___onRightComplete_20 = value;
		Il2CppCodeGenWriteBarrier(&___onRightComplete_20, value);
	}

	inline static int32_t get_offset_of_onHideComplete_21() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___onHideComplete_21)); }
	inline OnHideComplete_t4141700430 * get_onHideComplete_21() const { return ___onHideComplete_21; }
	inline OnHideComplete_t4141700430 ** get_address_of_onHideComplete_21() { return &___onHideComplete_21; }
	inline void set_onHideComplete_21(OnHideComplete_t4141700430 * value)
	{
		___onHideComplete_21 = value;
		Il2CppCodeGenWriteBarrier(&___onHideComplete_21, value);
	}

	inline static int32_t get_offset_of_itemClosing_22() { return static_cast<int32_t>(offsetof(ItemSwipeController_t3011021799, ___itemClosing_22)); }
	inline bool get_itemClosing_22() const { return ___itemClosing_22; }
	inline bool* get_address_of_itemClosing_22() { return &___itemClosing_22; }
	inline void set_itemClosing_22(bool value)
	{
		___itemClosing_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
