﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsFindAutocompleteResultMatchedSubstring
struct  OnlineMapsFindAutocompleteResultMatchedSubstring_t3111891439  : public Il2CppObject
{
public:
	// System.Int32 OnlineMapsFindAutocompleteResultMatchedSubstring::offset
	int32_t ___offset_0;
	// System.Int32 OnlineMapsFindAutocompleteResultMatchedSubstring::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_offset_0() { return static_cast<int32_t>(offsetof(OnlineMapsFindAutocompleteResultMatchedSubstring_t3111891439, ___offset_0)); }
	inline int32_t get_offset_0() const { return ___offset_0; }
	inline int32_t* get_address_of_offset_0() { return &___offset_0; }
	inline void set_offset_0(int32_t value)
	{
		___offset_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(OnlineMapsFindAutocompleteResultMatchedSubstring_t3111891439, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
