﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<DeclineFriendRequest>c__AnonStorey15<System.Object>
struct U3CDeclineFriendRequestU3Ec__AnonStorey15_t1335111756;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<DeclineFriendRequest>c__AnonStorey15<System.Object>::.ctor()
extern "C"  void U3CDeclineFriendRequestU3Ec__AnonStorey15__ctor_m2295282479_gshared (U3CDeclineFriendRequestU3Ec__AnonStorey15_t1335111756 * __this, const MethodInfo* method);
#define U3CDeclineFriendRequestU3Ec__AnonStorey15__ctor_m2295282479(__this, method) ((  void (*) (U3CDeclineFriendRequestU3Ec__AnonStorey15_t1335111756 *, const MethodInfo*))U3CDeclineFriendRequestU3Ec__AnonStorey15__ctor_m2295282479_gshared)(__this, method)
// System.Void BaseServer`1/<DeclineFriendRequest>c__AnonStorey15<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeclineFriendRequestU3Ec__AnonStorey15_U3CU3Em__0_m20933064_gshared (U3CDeclineFriendRequestU3Ec__AnonStorey15_t1335111756 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeclineFriendRequestU3Ec__AnonStorey15_U3CU3Em__0_m20933064(__this, ___request0, ___response1, method) ((  void (*) (U3CDeclineFriendRequestU3Ec__AnonStorey15_t1335111756 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeclineFriendRequestU3Ec__AnonStorey15_U3CU3Em__0_m20933064_gshared)(__this, ___request0, ___response1, method)
