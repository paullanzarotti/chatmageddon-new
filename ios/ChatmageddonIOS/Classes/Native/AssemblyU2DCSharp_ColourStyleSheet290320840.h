﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen40986560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColourStyleSheet
struct  ColourStyleSheet_t290320840  : public MonoSingleton_1_t40986560
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Color> ColourStyleSheet::colours
	List_1_t1389513207 * ___colours_3;
	// System.Collections.Generic.List`1<System.String> ColourStyleSheet::styleNames
	List_1_t1398341365 * ___styleNames_4;

public:
	inline static int32_t get_offset_of_colours_3() { return static_cast<int32_t>(offsetof(ColourStyleSheet_t290320840, ___colours_3)); }
	inline List_1_t1389513207 * get_colours_3() const { return ___colours_3; }
	inline List_1_t1389513207 ** get_address_of_colours_3() { return &___colours_3; }
	inline void set_colours_3(List_1_t1389513207 * value)
	{
		___colours_3 = value;
		Il2CppCodeGenWriteBarrier(&___colours_3, value);
	}

	inline static int32_t get_offset_of_styleNames_4() { return static_cast<int32_t>(offsetof(ColourStyleSheet_t290320840, ___styleNames_4)); }
	inline List_1_t1398341365 * get_styleNames_4() const { return ___styleNames_4; }
	inline List_1_t1398341365 ** get_address_of_styleNames_4() { return &___styleNames_4; }
	inline void set_styleNames_4(List_1_t1398341365 * value)
	{
		___styleNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___styleNames_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
