﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<AcceptFriendRequest>c__AnonStorey14<System.Object>
struct U3CAcceptFriendRequestU3Ec__AnonStorey14_t2670103383;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<AcceptFriendRequest>c__AnonStorey14<System.Object>::.ctor()
extern "C"  void U3CAcceptFriendRequestU3Ec__AnonStorey14__ctor_m2868570386_gshared (U3CAcceptFriendRequestU3Ec__AnonStorey14_t2670103383 * __this, const MethodInfo* method);
#define U3CAcceptFriendRequestU3Ec__AnonStorey14__ctor_m2868570386(__this, method) ((  void (*) (U3CAcceptFriendRequestU3Ec__AnonStorey14_t2670103383 *, const MethodInfo*))U3CAcceptFriendRequestU3Ec__AnonStorey14__ctor_m2868570386_gshared)(__this, method)
// System.Void BaseServer`1/<AcceptFriendRequest>c__AnonStorey14<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CAcceptFriendRequestU3Ec__AnonStorey14_U3CU3Em__0_m2320094063_gshared (U3CAcceptFriendRequestU3Ec__AnonStorey14_t2670103383 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CAcceptFriendRequestU3Ec__AnonStorey14_U3CU3Em__0_m2320094063(__this, ___request0, ___response1, method) ((  void (*) (U3CAcceptFriendRequestU3Ec__AnonStorey14_t2670103383 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CAcceptFriendRequestU3Ec__AnonStorey14_U3CU3Em__0_m2320094063_gshared)(__this, ___request0, ___response1, method)
