﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AvatarChatButton
struct AvatarChatButton_t228579527;

#include "codegen/il2cpp-codegen.h"

// System.Void AvatarChatButton::.ctor()
extern "C"  void AvatarChatButton__ctor_m910586934 (AvatarChatButton_t228579527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarChatButton::OnButtonClick()
extern "C"  void AvatarChatButton_OnButtonClick_m1394698507 (AvatarChatButton_t228579527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
