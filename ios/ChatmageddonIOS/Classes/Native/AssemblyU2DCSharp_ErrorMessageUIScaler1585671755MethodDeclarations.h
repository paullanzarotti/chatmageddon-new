﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessageUIScaler
struct ErrorMessageUIScaler_t1585671755;

#include "codegen/il2cpp-codegen.h"

// System.Void ErrorMessageUIScaler::.ctor()
extern "C"  void ErrorMessageUIScaler__ctor_m4084247314 (ErrorMessageUIScaler_t1585671755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessageUIScaler::GlobalUIScale()
extern "C"  void ErrorMessageUIScaler_GlobalUIScale_m1993443501 (ErrorMessageUIScaler_t1585671755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
