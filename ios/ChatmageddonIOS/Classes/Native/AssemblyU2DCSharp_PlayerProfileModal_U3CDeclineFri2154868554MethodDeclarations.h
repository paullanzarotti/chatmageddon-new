﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileModal/<DeclineFriendRequest>c__AnonStorey2
struct U3CDeclineFriendRequestU3Ec__AnonStorey2_t2154868554;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayerProfileModal/<DeclineFriendRequest>c__AnonStorey2::.ctor()
extern "C"  void U3CDeclineFriendRequestU3Ec__AnonStorey2__ctor_m1200557619 (U3CDeclineFriendRequestU3Ec__AnonStorey2_t2154868554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileModal/<DeclineFriendRequest>c__AnonStorey2::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CDeclineFriendRequestU3Ec__AnonStorey2_U3CU3Em__0_m2983155201 (U3CDeclineFriendRequestU3Ec__AnonStorey2_t2154868554 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
