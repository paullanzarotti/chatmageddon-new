﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3561032432MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3941832635(__this, ___dic0, method) ((  void (*) (KeyCollection_t1234264224 *, SortedDictionary_2_t1020118611 *, const MethodInfo*))KeyCollection__ctor_m2360962727_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2186745414(__this, ___item0, method) ((  void (*) (KeyCollection_t1234264224 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1452078018_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2696066589(__this, method) ((  void (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1877705257_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3804247446(__this, ___item0, method) ((  bool (*) (KeyCollection_t1234264224 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3023445058_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m880054635(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2985680479_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m775099577(__this, method) ((  bool (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m945847853_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4117781005(__this, ___item0, method) ((  bool (*) (KeyCollection_t1234264224 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4130601921_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2820405579(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1234264224 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1628337375_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3823900451(__this, method) ((  bool (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m633672767_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m390018319(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4189355763_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1828055194(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2399335214_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2823004353(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t1234264224 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2361880853_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_Count()
#define KeyCollection_get_Count_m3093333323(__this, method) ((  int32_t (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_get_Count_m1497005991_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3641308319(__this, method) ((  Enumerator_t885240877  (*) (KeyCollection_t1234264224 *, const MethodInfo*))KeyCollection_GetEnumerator_m253781891_gshared)(__this, method)
