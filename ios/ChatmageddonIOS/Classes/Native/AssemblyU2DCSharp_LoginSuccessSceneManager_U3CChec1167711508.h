﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;
// LoginSuccessSceneManager
struct LoginSuccessSceneManager_t2204684339;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginSuccessSceneManager/<CheckInternetConnection>c__AnonStorey2
struct  U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508  : public Il2CppObject
{
public:
	// System.Action LoginSuccessSceneManager/<CheckInternetConnection>c__AnonStorey2::loadFunction
	Action_t3226471752 * ___loadFunction_0;
	// LoginSuccessSceneManager LoginSuccessSceneManager/<CheckInternetConnection>c__AnonStorey2::$this
	LoginSuccessSceneManager_t2204684339 * ___U24this_1;

public:
	inline static int32_t get_offset_of_loadFunction_0() { return static_cast<int32_t>(offsetof(U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508, ___loadFunction_0)); }
	inline Action_t3226471752 * get_loadFunction_0() const { return ___loadFunction_0; }
	inline Action_t3226471752 ** get_address_of_loadFunction_0() { return &___loadFunction_0; }
	inline void set_loadFunction_0(Action_t3226471752 * value)
	{
		___loadFunction_0 = value;
		Il2CppCodeGenWriteBarrier(&___loadFunction_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508, ___U24this_1)); }
	inline LoginSuccessSceneManager_t2204684339 * get_U24this_1() const { return ___U24this_1; }
	inline LoginSuccessSceneManager_t2204684339 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoginSuccessSceneManager_t2204684339 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
