﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetMineWarefareStatus>c__AnonStorey27
struct U3CGetMineWarefareStatusU3Ec__AnonStorey27_t2059945141;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetMineWarefareStatus>c__AnonStorey27::.ctor()
extern "C"  void U3CGetMineWarefareStatusU3Ec__AnonStorey27__ctor_m3195373982 (U3CGetMineWarefareStatusU3Ec__AnonStorey27_t2059945141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetMineWarefareStatus>c__AnonStorey27::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetMineWarefareStatusU3Ec__AnonStorey27_U3CU3Em__0_m3132158413 (U3CGetMineWarefareStatusU3Ec__AnonStorey27_t2059945141 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
