﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>
struct ReadOnlyCollection_1_t2846453951;
// System.Collections.Generic.IList`1<PlayerProfileNavScreen>
struct IList_1_t3201608860;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PlayerProfileNavScreen[]
struct PlayerProfileNavScreenU5BU5D_t3066215346;
// System.Collections.Generic.IEnumerator`1<PlayerProfileNavScreen>
struct IEnumerator_1_t136192086;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3757535709_gshared (ReadOnlyCollection_1_t2846453951 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3757535709(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3757535709_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3160709407_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3160709407(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3160709407_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m185694259_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m185694259(__this, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m185694259_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1863885436_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1863885436(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1863885436_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3484827218_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3484827218(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3484827218_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2460950040_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2460950040(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2460950040_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1123647764_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1123647764(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1123647764_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1549983651_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1549983651(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1549983651_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3810878871_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3810878871(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3810878871_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3454456678_gshared (ReadOnlyCollection_1_t2846453951 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3454456678(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3454456678_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2795154451_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2795154451(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2795154451_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m4081742346_gshared (ReadOnlyCollection_1_t2846453951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4081742346(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2846453951 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m4081742346_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m900476432_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m900476432(__this, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m900476432_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2348968724_gshared (ReadOnlyCollection_1_t2846453951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2348968724(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2348968724_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1885381500_gshared (ReadOnlyCollection_1_t2846453951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1885381500(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2846453951 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1885381500_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3179602979_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3179602979(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3179602979_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1252921315_gshared (ReadOnlyCollection_1_t2846453951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1252921315(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1252921315_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2400776053_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2400776053(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2400776053_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3879953842_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3879953842(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3879953842_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m691051420_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m691051420(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m691051420_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1221863923_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1221863923(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1221863923_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2682070446_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2682070446(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2682070446_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1514399343_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1514399343(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1514399343_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1063338980_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1063338980(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1063338980_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3757870809_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3757870809(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3757870809_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m376330843_gshared (ReadOnlyCollection_1_t2846453951 * __this, PlayerProfileNavScreenU5BU5D_t3066215346* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m376330843(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2846453951 *, PlayerProfileNavScreenU5BU5D_t3066215346*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m376330843_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3814981254_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3814981254(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3814981254_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2479671095_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2479671095(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2479671095_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1350638938_gshared (ReadOnlyCollection_1_t2846453951 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1350638938(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2846453951 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1350638938_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PlayerProfileNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3578025808_gshared (ReadOnlyCollection_1_t2846453951 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3578025808(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2846453951 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3578025808_gshared)(__this, ___index0, method)
