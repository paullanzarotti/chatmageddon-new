﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileNavigationController
struct PlayerProfileNavigationController_t1388421560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void PlayerProfileNavigationController::.ctor()
extern "C"  void PlayerProfileNavigationController__ctor_m2347344773 (PlayerProfileNavigationController_t1388421560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileNavigationController::NavigateBackwards()
extern "C"  void PlayerProfileNavigationController_NavigateBackwards_m2588569944 (PlayerProfileNavigationController_t1388421560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileNavigationController::NavigateForwards(PlayerProfileNavScreen)
extern "C"  void PlayerProfileNavigationController_NavigateForwards_m537720627 (PlayerProfileNavigationController_t1388421560 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerProfileNavigationController::ValidateNavigation(PlayerProfileNavScreen,NavigationDirection)
extern "C"  bool PlayerProfileNavigationController_ValidateNavigation_m2958583387 (PlayerProfileNavigationController_t1388421560 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileNavigationController::LoadNavScreen(PlayerProfileNavScreen,NavigationDirection,System.Boolean)
extern "C"  void PlayerProfileNavigationController_LoadNavScreen_m3357082283 (PlayerProfileNavigationController_t1388421560 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileNavigationController::UnloadNavScreen(PlayerProfileNavScreen,NavigationDirection)
extern "C"  void PlayerProfileNavigationController_UnloadNavScreen_m1530230547 (PlayerProfileNavigationController_t1388421560 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileNavigationController::PopulateTitleBar(PlayerProfileNavScreen)
extern "C"  void PlayerProfileNavigationController_PopulateTitleBar_m3319812611 (PlayerProfileNavigationController_t1388421560 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
