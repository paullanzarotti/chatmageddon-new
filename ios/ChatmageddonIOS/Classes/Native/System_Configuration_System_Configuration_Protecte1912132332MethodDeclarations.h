﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ProtectedProviderSettings
struct ProtectedProviderSettings_t1912132332;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ProviderSettingsCollection
struct ProviderSettingsCollection_t585304908;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Configuration.ProtectedProviderSettings::.ctor()
extern "C"  void ProtectedProviderSettings__ctor_m3917995251 (ProtectedProviderSettings_t1912132332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ProtectedProviderSettings::.cctor()
extern "C"  void ProtectedProviderSettings__cctor_m1715609144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProtectedProviderSettings::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * ProtectedProviderSettings_get_Properties_m528583432 (ProtectedProviderSettings_t1912132332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProviderSettingsCollection System.Configuration.ProtectedProviderSettings::get_Providers()
extern "C"  ProviderSettingsCollection_t585304908 * ProtectedProviderSettings_get_Providers_m2479129358 (ProtectedProviderSettings_t1912132332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
