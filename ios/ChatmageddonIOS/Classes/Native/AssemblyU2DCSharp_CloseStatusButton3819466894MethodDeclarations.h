﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseStatusButton
struct CloseStatusButton_t3819466894;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseStatusButton::.ctor()
extern "C"  void CloseStatusButton__ctor_m575949447 (CloseStatusButton_t3819466894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseStatusButton::OnButtonClick()
extern "C"  void CloseStatusButton_OnButtonClick_m1317852870 (CloseStatusButton_t3819466894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
