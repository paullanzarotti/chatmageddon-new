﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>
struct U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::.ctor()
extern "C"  void U3CLoadPictureEnumeratorU3Ec__Iterator1__ctor_m600969494_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method);
#define U3CLoadPictureEnumeratorU3Ec__Iterator1__ctor_m600969494(__this, method) ((  void (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))U3CLoadPictureEnumeratorU3Ec__Iterator1__ctor_m600969494_gshared)(__this, method)
// System.Boolean FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::MoveNext()
extern "C"  bool U3CLoadPictureEnumeratorU3Ec__Iterator1_MoveNext_m3366050890_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method);
#define U3CLoadPictureEnumeratorU3Ec__Iterator1_MoveNext_m3366050890(__this, method) ((  bool (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))U3CLoadPictureEnumeratorU3Ec__Iterator1_MoveNext_m3366050890_gshared)(__this, method)
// System.Object FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4250567546_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method);
#define U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4250567546(__this, method) ((  Il2CppObject * (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4250567546_gshared)(__this, method)
// System.Object FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3131561906_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method);
#define U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3131561906(__this, method) ((  Il2CppObject * (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))U3CLoadPictureEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3131561906_gshared)(__this, method)
// System.Void FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::Dispose()
extern "C"  void U3CLoadPictureEnumeratorU3Ec__Iterator1_Dispose_m1466434241_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method);
#define U3CLoadPictureEnumeratorU3Ec__Iterator1_Dispose_m1466434241(__this, method) ((  void (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))U3CLoadPictureEnumeratorU3Ec__Iterator1_Dispose_m1466434241_gshared)(__this, method)
// System.Void FBManager`1/<LoadPictureEnumerator>c__Iterator1<System.Object>::Reset()
extern "C"  void U3CLoadPictureEnumeratorU3Ec__Iterator1_Reset_m2048700007_gshared (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 * __this, const MethodInfo* method);
#define U3CLoadPictureEnumeratorU3Ec__Iterator1_Reset_m2048700007(__this, method) ((  void (*) (U3CLoadPictureEnumeratorU3Ec__Iterator1_t2129548707 *, const MethodInfo*))U3CLoadPictureEnumeratorU3Ec__Iterator1_Reset_m2048700007_gshared)(__this, method)
