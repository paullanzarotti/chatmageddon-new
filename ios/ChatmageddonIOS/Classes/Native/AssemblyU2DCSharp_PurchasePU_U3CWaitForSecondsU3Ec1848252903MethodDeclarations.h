﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchasePU/<WaitForSeconds>c__Iterator0
struct U3CWaitForSecondsU3Ec__Iterator0_t1848252903;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PurchasePU/<WaitForSeconds>c__Iterator0::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0__ctor_m880102262 (U3CWaitForSecondsU3Ec__Iterator0_t1848252903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PurchasePU/<WaitForSeconds>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m1907430678 (U3CWaitForSecondsU3Ec__Iterator0_t1848252903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PurchasePU/<WaitForSeconds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3057437336 (U3CWaitForSecondsU3Ec__Iterator0_t1848252903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PurchasePU/<WaitForSeconds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2322837632 (U3CWaitForSecondsU3Ec__Iterator0_t1848252903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU/<WaitForSeconds>c__Iterator0::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Dispose_m184276729 (U3CWaitForSecondsU3Ec__Iterator0_t1848252903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasePU/<WaitForSeconds>c__Iterator0::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Reset_m551721035 (U3CWaitForSecondsU3Ec__Iterator0_t1848252903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
