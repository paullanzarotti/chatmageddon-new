﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatButton
struct ChatButton_t3979308854;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatButton::.ctor()
extern "C"  void ChatButton__ctor_m1712503495 (ChatButton_t3979308854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatButton::OnButtonClick()
extern "C"  void ChatButton_OnButtonClick_m1973024474 (ChatButton_t3979308854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatButton::SetUnreadCount(System.Int32)
extern "C"  void ChatButton_SetUnreadCount_m1094027730 (ChatButton_t3979308854 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
