﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blur_BlurHole
struct  CameraFilterPack_Blur_BlurHole_t258387902  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Blur_BlurHole::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_BlurHole::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Blur_BlurHole::Size
	float ___Size_4;
	// System.Single CameraFilterPack_Blur_BlurHole::_Radius
	float ____Radius_5;
	// System.Single CameraFilterPack_Blur_BlurHole::_SpotSize
	float ____SpotSize_6;
	// System.Single CameraFilterPack_Blur_BlurHole::_CenterX
	float ____CenterX_7;
	// System.Single CameraFilterPack_Blur_BlurHole::_CenterY
	float ____CenterY_8;
	// System.Single CameraFilterPack_Blur_BlurHole::_AlphaBlur
	float ____AlphaBlur_9;
	// System.Single CameraFilterPack_Blur_BlurHole::_AlphaBlurInside
	float ____AlphaBlurInside_10;
	// UnityEngine.Vector4 CameraFilterPack_Blur_BlurHole::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_11;
	// UnityEngine.Material CameraFilterPack_Blur_BlurHole::SCMaterial
	Material_t193706927 * ___SCMaterial_12;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Size_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ___Size_4)); }
	inline float get_Size_4() const { return ___Size_4; }
	inline float* get_address_of_Size_4() { return &___Size_4; }
	inline void set_Size_4(float value)
	{
		___Size_4 = value;
	}

	inline static int32_t get_offset_of__Radius_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ____Radius_5)); }
	inline float get__Radius_5() const { return ____Radius_5; }
	inline float* get_address_of__Radius_5() { return &____Radius_5; }
	inline void set__Radius_5(float value)
	{
		____Radius_5 = value;
	}

	inline static int32_t get_offset_of__SpotSize_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ____SpotSize_6)); }
	inline float get__SpotSize_6() const { return ____SpotSize_6; }
	inline float* get_address_of__SpotSize_6() { return &____SpotSize_6; }
	inline void set__SpotSize_6(float value)
	{
		____SpotSize_6 = value;
	}

	inline static int32_t get_offset_of__CenterX_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ____CenterX_7)); }
	inline float get__CenterX_7() const { return ____CenterX_7; }
	inline float* get_address_of__CenterX_7() { return &____CenterX_7; }
	inline void set__CenterX_7(float value)
	{
		____CenterX_7 = value;
	}

	inline static int32_t get_offset_of__CenterY_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ____CenterY_8)); }
	inline float get__CenterY_8() const { return ____CenterY_8; }
	inline float* get_address_of__CenterY_8() { return &____CenterY_8; }
	inline void set__CenterY_8(float value)
	{
		____CenterY_8 = value;
	}

	inline static int32_t get_offset_of__AlphaBlur_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ____AlphaBlur_9)); }
	inline float get__AlphaBlur_9() const { return ____AlphaBlur_9; }
	inline float* get_address_of__AlphaBlur_9() { return &____AlphaBlur_9; }
	inline void set__AlphaBlur_9(float value)
	{
		____AlphaBlur_9 = value;
	}

	inline static int32_t get_offset_of__AlphaBlurInside_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ____AlphaBlurInside_10)); }
	inline float get__AlphaBlurInside_10() const { return ____AlphaBlurInside_10; }
	inline float* get_address_of__AlphaBlurInside_10() { return &____AlphaBlurInside_10; }
	inline void set__AlphaBlurInside_10(float value)
	{
		____AlphaBlurInside_10 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ___ScreenResolution_11)); }
	inline Vector4_t2243707581  get_ScreenResolution_11() const { return ___ScreenResolution_11; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_11() { return &___ScreenResolution_11; }
	inline void set_ScreenResolution_11(Vector4_t2243707581  value)
	{
		___ScreenResolution_11 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902, ___SCMaterial_12)); }
	inline Material_t193706927 * get_SCMaterial_12() const { return ___SCMaterial_12; }
	inline Material_t193706927 ** get_address_of_SCMaterial_12() { return &___SCMaterial_12; }
	inline void set_SCMaterial_12(Material_t193706927 * value)
	{
		___SCMaterial_12 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_12, value);
	}
};

struct CameraFilterPack_Blur_BlurHole_t258387902_StaticFields
{
public:
	// System.Single CameraFilterPack_Blur_BlurHole::ChangeSize
	float ___ChangeSize_13;

public:
	inline static int32_t get_offset_of_ChangeSize_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_BlurHole_t258387902_StaticFields, ___ChangeSize_13)); }
	inline float get_ChangeSize_13() const { return ___ChangeSize_13; }
	inline float* get_address_of_ChangeSize_13() { return &___ChangeSize_13; }
	inline void set_ChangeSize_13(float value)
	{
		___ChangeSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
