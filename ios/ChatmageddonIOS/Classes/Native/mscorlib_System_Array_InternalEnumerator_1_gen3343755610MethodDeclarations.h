﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3343755610.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_ComponentModel_PropertyTabScope2485003348.h"

// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.PropertyTabScope>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3134003690_gshared (InternalEnumerator_1_t3343755610 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3134003690(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3343755610 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3134003690_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.PropertyTabScope>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1692939662_gshared (InternalEnumerator_1_t3343755610 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1692939662(__this, method) ((  void (*) (InternalEnumerator_1_t3343755610 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1692939662_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.PropertyTabScope>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1634738206_gshared (InternalEnumerator_1_t3343755610 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1634738206(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3343755610 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1634738206_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.PropertyTabScope>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3810594273_gshared (InternalEnumerator_1_t3343755610 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3810594273(__this, method) ((  void (*) (InternalEnumerator_1_t3343755610 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3810594273_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.PropertyTabScope>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1319463114_gshared (InternalEnumerator_1_t3343755610 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1319463114(__this, method) ((  bool (*) (InternalEnumerator_1_t3343755610 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1319463114_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.ComponentModel.PropertyTabScope>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m694985369_gshared (InternalEnumerator_1_t3343755610 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m694985369(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3343755610 *, const MethodInfo*))InternalEnumerator_1_get_Current_m694985369_gshared)(__this, method)
