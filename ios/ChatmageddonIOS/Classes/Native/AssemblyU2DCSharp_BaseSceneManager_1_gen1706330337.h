﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2078169705.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseSceneManager`1<LoginRegistrationSceneManager>
struct  BaseSceneManager_1_t1706330337  : public MonoSingleton_1_t2078169705
{
public:
	// System.String BaseSceneManager`1::sceneName
	String_t* ___sceneName_3;
	// UnityEngine.Transform BaseSceneManager`1::UIRoot
	Transform_t3275118058 * ___UIRoot_4;
	// System.String BaseSceneManager`1::resourcePath
	String_t* ___resourcePath_5;
	// System.Boolean BaseSceneManager`1::sceneInit
	bool ___sceneInit_6;
	// System.Single BaseSceneManager`1::sceneStartWaitTime
	float ___sceneStartWaitTime_7;
	// UnityEngine.GameObject BaseSceneManager`1::topLeftAnchor
	GameObject_t1756533147 * ___topLeftAnchor_8;
	// UnityEngine.GameObject BaseSceneManager`1::topAnchor
	GameObject_t1756533147 * ___topAnchor_9;
	// UnityEngine.GameObject BaseSceneManager`1::topRightAnchor
	GameObject_t1756533147 * ___topRightAnchor_10;
	// UnityEngine.GameObject BaseSceneManager`1::leftAnchor
	GameObject_t1756533147 * ___leftAnchor_11;
	// UnityEngine.GameObject BaseSceneManager`1::centerAnchor
	GameObject_t1756533147 * ___centerAnchor_12;
	// UnityEngine.GameObject BaseSceneManager`1::rightAnchor
	GameObject_t1756533147 * ___rightAnchor_13;
	// UnityEngine.GameObject BaseSceneManager`1::bottomLeftAnchor
	GameObject_t1756533147 * ___bottomLeftAnchor_14;
	// UnityEngine.GameObject BaseSceneManager`1::bottomAnchor
	GameObject_t1756533147 * ___bottomAnchor_15;
	// UnityEngine.GameObject BaseSceneManager`1::bottomRightAnchor
	GameObject_t1756533147 * ___bottomRightAnchor_16;

public:
	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_3, value);
	}

	inline static int32_t get_offset_of_UIRoot_4() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___UIRoot_4)); }
	inline Transform_t3275118058 * get_UIRoot_4() const { return ___UIRoot_4; }
	inline Transform_t3275118058 ** get_address_of_UIRoot_4() { return &___UIRoot_4; }
	inline void set_UIRoot_4(Transform_t3275118058 * value)
	{
		___UIRoot_4 = value;
		Il2CppCodeGenWriteBarrier(&___UIRoot_4, value);
	}

	inline static int32_t get_offset_of_resourcePath_5() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___resourcePath_5)); }
	inline String_t* get_resourcePath_5() const { return ___resourcePath_5; }
	inline String_t** get_address_of_resourcePath_5() { return &___resourcePath_5; }
	inline void set_resourcePath_5(String_t* value)
	{
		___resourcePath_5 = value;
		Il2CppCodeGenWriteBarrier(&___resourcePath_5, value);
	}

	inline static int32_t get_offset_of_sceneInit_6() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___sceneInit_6)); }
	inline bool get_sceneInit_6() const { return ___sceneInit_6; }
	inline bool* get_address_of_sceneInit_6() { return &___sceneInit_6; }
	inline void set_sceneInit_6(bool value)
	{
		___sceneInit_6 = value;
	}

	inline static int32_t get_offset_of_sceneStartWaitTime_7() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___sceneStartWaitTime_7)); }
	inline float get_sceneStartWaitTime_7() const { return ___sceneStartWaitTime_7; }
	inline float* get_address_of_sceneStartWaitTime_7() { return &___sceneStartWaitTime_7; }
	inline void set_sceneStartWaitTime_7(float value)
	{
		___sceneStartWaitTime_7 = value;
	}

	inline static int32_t get_offset_of_topLeftAnchor_8() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___topLeftAnchor_8)); }
	inline GameObject_t1756533147 * get_topLeftAnchor_8() const { return ___topLeftAnchor_8; }
	inline GameObject_t1756533147 ** get_address_of_topLeftAnchor_8() { return &___topLeftAnchor_8; }
	inline void set_topLeftAnchor_8(GameObject_t1756533147 * value)
	{
		___topLeftAnchor_8 = value;
		Il2CppCodeGenWriteBarrier(&___topLeftAnchor_8, value);
	}

	inline static int32_t get_offset_of_topAnchor_9() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___topAnchor_9)); }
	inline GameObject_t1756533147 * get_topAnchor_9() const { return ___topAnchor_9; }
	inline GameObject_t1756533147 ** get_address_of_topAnchor_9() { return &___topAnchor_9; }
	inline void set_topAnchor_9(GameObject_t1756533147 * value)
	{
		___topAnchor_9 = value;
		Il2CppCodeGenWriteBarrier(&___topAnchor_9, value);
	}

	inline static int32_t get_offset_of_topRightAnchor_10() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___topRightAnchor_10)); }
	inline GameObject_t1756533147 * get_topRightAnchor_10() const { return ___topRightAnchor_10; }
	inline GameObject_t1756533147 ** get_address_of_topRightAnchor_10() { return &___topRightAnchor_10; }
	inline void set_topRightAnchor_10(GameObject_t1756533147 * value)
	{
		___topRightAnchor_10 = value;
		Il2CppCodeGenWriteBarrier(&___topRightAnchor_10, value);
	}

	inline static int32_t get_offset_of_leftAnchor_11() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___leftAnchor_11)); }
	inline GameObject_t1756533147 * get_leftAnchor_11() const { return ___leftAnchor_11; }
	inline GameObject_t1756533147 ** get_address_of_leftAnchor_11() { return &___leftAnchor_11; }
	inline void set_leftAnchor_11(GameObject_t1756533147 * value)
	{
		___leftAnchor_11 = value;
		Il2CppCodeGenWriteBarrier(&___leftAnchor_11, value);
	}

	inline static int32_t get_offset_of_centerAnchor_12() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___centerAnchor_12)); }
	inline GameObject_t1756533147 * get_centerAnchor_12() const { return ___centerAnchor_12; }
	inline GameObject_t1756533147 ** get_address_of_centerAnchor_12() { return &___centerAnchor_12; }
	inline void set_centerAnchor_12(GameObject_t1756533147 * value)
	{
		___centerAnchor_12 = value;
		Il2CppCodeGenWriteBarrier(&___centerAnchor_12, value);
	}

	inline static int32_t get_offset_of_rightAnchor_13() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___rightAnchor_13)); }
	inline GameObject_t1756533147 * get_rightAnchor_13() const { return ___rightAnchor_13; }
	inline GameObject_t1756533147 ** get_address_of_rightAnchor_13() { return &___rightAnchor_13; }
	inline void set_rightAnchor_13(GameObject_t1756533147 * value)
	{
		___rightAnchor_13 = value;
		Il2CppCodeGenWriteBarrier(&___rightAnchor_13, value);
	}

	inline static int32_t get_offset_of_bottomLeftAnchor_14() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___bottomLeftAnchor_14)); }
	inline GameObject_t1756533147 * get_bottomLeftAnchor_14() const { return ___bottomLeftAnchor_14; }
	inline GameObject_t1756533147 ** get_address_of_bottomLeftAnchor_14() { return &___bottomLeftAnchor_14; }
	inline void set_bottomLeftAnchor_14(GameObject_t1756533147 * value)
	{
		___bottomLeftAnchor_14 = value;
		Il2CppCodeGenWriteBarrier(&___bottomLeftAnchor_14, value);
	}

	inline static int32_t get_offset_of_bottomAnchor_15() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___bottomAnchor_15)); }
	inline GameObject_t1756533147 * get_bottomAnchor_15() const { return ___bottomAnchor_15; }
	inline GameObject_t1756533147 ** get_address_of_bottomAnchor_15() { return &___bottomAnchor_15; }
	inline void set_bottomAnchor_15(GameObject_t1756533147 * value)
	{
		___bottomAnchor_15 = value;
		Il2CppCodeGenWriteBarrier(&___bottomAnchor_15, value);
	}

	inline static int32_t get_offset_of_bottomRightAnchor_16() { return static_cast<int32_t>(offsetof(BaseSceneManager_1_t1706330337, ___bottomRightAnchor_16)); }
	inline GameObject_t1756533147 * get_bottomRightAnchor_16() const { return ___bottomRightAnchor_16; }
	inline GameObject_t1756533147 ** get_address_of_bottomRightAnchor_16() { return &___bottomRightAnchor_16; }
	inline void set_bottomRightAnchor_16(GameObject_t1756533147 * value)
	{
		___bottomRightAnchor_16 = value;
		Il2CppCodeGenWriteBarrier(&___bottomRightAnchor_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
