﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// TweenScale
struct TweenScale_t2697902175;
// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenColor
struct TweenColor_t3390486518;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogOutTransition
struct  LogOutTransition_t3259858399  : public MonoBehaviour_t1158329972
{
public:
	// TweenPosition LogOutTransition::posTween
	TweenPosition_t1144714832 * ___posTween_2;
	// TweenScale LogOutTransition::scaleTween
	TweenScale_t2697902175 * ___scaleTween_3;
	// TweenAlpha LogOutTransition::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_4;
	// TweenColor LogOutTransition::backgroundFadeTween
	TweenColor_t3390486518 * ___backgroundFadeTween_5;
	// System.Single LogOutTransition::alphaTweenFrom
	float ___alphaTweenFrom_6;
	// System.Single LogOutTransition::alphaTweenTo
	float ___alphaTweenTo_7;
	// System.Boolean LogOutTransition::UIclosing
	bool ___UIclosing_8;
	// System.Boolean LogOutTransition::loggingOut
	bool ___loggingOut_9;

public:
	inline static int32_t get_offset_of_posTween_2() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___posTween_2)); }
	inline TweenPosition_t1144714832 * get_posTween_2() const { return ___posTween_2; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_2() { return &___posTween_2; }
	inline void set_posTween_2(TweenPosition_t1144714832 * value)
	{
		___posTween_2 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_2, value);
	}

	inline static int32_t get_offset_of_scaleTween_3() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___scaleTween_3)); }
	inline TweenScale_t2697902175 * get_scaleTween_3() const { return ___scaleTween_3; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_3() { return &___scaleTween_3; }
	inline void set_scaleTween_3(TweenScale_t2697902175 * value)
	{
		___scaleTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_3, value);
	}

	inline static int32_t get_offset_of_alphaTween_4() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___alphaTween_4)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_4() const { return ___alphaTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_4() { return &___alphaTween_4; }
	inline void set_alphaTween_4(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_4, value);
	}

	inline static int32_t get_offset_of_backgroundFadeTween_5() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___backgroundFadeTween_5)); }
	inline TweenColor_t3390486518 * get_backgroundFadeTween_5() const { return ___backgroundFadeTween_5; }
	inline TweenColor_t3390486518 ** get_address_of_backgroundFadeTween_5() { return &___backgroundFadeTween_5; }
	inline void set_backgroundFadeTween_5(TweenColor_t3390486518 * value)
	{
		___backgroundFadeTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundFadeTween_5, value);
	}

	inline static int32_t get_offset_of_alphaTweenFrom_6() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___alphaTweenFrom_6)); }
	inline float get_alphaTweenFrom_6() const { return ___alphaTweenFrom_6; }
	inline float* get_address_of_alphaTweenFrom_6() { return &___alphaTweenFrom_6; }
	inline void set_alphaTweenFrom_6(float value)
	{
		___alphaTweenFrom_6 = value;
	}

	inline static int32_t get_offset_of_alphaTweenTo_7() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___alphaTweenTo_7)); }
	inline float get_alphaTweenTo_7() const { return ___alphaTweenTo_7; }
	inline float* get_address_of_alphaTweenTo_7() { return &___alphaTweenTo_7; }
	inline void set_alphaTweenTo_7(float value)
	{
		___alphaTweenTo_7 = value;
	}

	inline static int32_t get_offset_of_UIclosing_8() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___UIclosing_8)); }
	inline bool get_UIclosing_8() const { return ___UIclosing_8; }
	inline bool* get_address_of_UIclosing_8() { return &___UIclosing_8; }
	inline void set_UIclosing_8(bool value)
	{
		___UIclosing_8 = value;
	}

	inline static int32_t get_offset_of_loggingOut_9() { return static_cast<int32_t>(offsetof(LogOutTransition_t3259858399, ___loggingOut_9)); }
	inline bool get_loggingOut_9() const { return ___loggingOut_9; }
	inline bool* get_address_of_loggingOut_9() { return &___loggingOut_9; }
	inline void set_loggingOut_9(bool value)
	{
		___loggingOut_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
