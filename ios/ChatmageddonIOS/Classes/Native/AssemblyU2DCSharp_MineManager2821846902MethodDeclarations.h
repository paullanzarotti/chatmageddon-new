﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineManager
struct MineManager_t2821846902;
// LaunchedMine
struct LaunchedMine_t3677282773;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedMine3677282773.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MineManager::.ctor()
extern "C"  void MineManager__ctor_m479513055 (MineManager_t2821846902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::Init()
extern "C"  void MineManager_Init_m2106299477 (MineManager_t2821846902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MineManager::get_MiniGameActive()
extern "C"  bool MineManager_get_MiniGameActive_m4049364139 (MineManager_t2821846902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::TriggerMineUI(LaunchedMine)
extern "C"  void MineManager_TriggerMineUI_m712622103 (MineManager_t2821846902 * __this, LaunchedMine_t3677282773 * ___triggeredMine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::ForceRemoveMineUI()
extern "C"  void MineManager_ForceRemoveMineUI_m1788555135 (MineManager_t2821846902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::ActivateMineMiniGame()
extern "C"  void MineManager_ActivateMineMiniGame_m3195140154 (MineManager_t2821846902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::ClearMineMiniGame()
extern "C"  void MineManager_ClearMineMiniGame_m382998810 (MineManager_t2821846902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::DisarmActiveMine(System.Boolean)
extern "C"  void MineManager_DisarmActiveMine_m23981551 (MineManager_t2821846902 * __this, bool ___disarmSuccess0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::ShowMineResultToast(LaunchedMine)
extern "C"  void MineManager_ShowMineResultToast_m2670627472 (MineManager_t2821846902 * __this, LaunchedMine_t3677282773 * ___mine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineManager::<DisarmActiveMine>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void MineManager_U3CDisarmActiveMineU3Em__0_m80979686 (MineManager_t2821846902 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
