﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3346886819MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::.ctor()
#define SortedDictionary_2__ctor_m2319713028(__this, method) ((  void (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2__ctor_m674714269_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define SortedDictionary_2__ctor_m634451454(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t2686657757 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m4130201156_gshared)(__this, ___comparer0, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2412533973(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1040024625_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4040127157(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3905150097_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2534305518(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t2686657757 *, KeyValuePair_2_t3089358386 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m166699524_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m793679942(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, KeyValuePair_2_t3089358386 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1516177424_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m554302359(__this, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1747061843_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1994444597(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, KeyValuePair_2_t3089358386 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2218591521_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1398441443(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1213244071_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.Contains(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m1745099775(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m1437439451_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.GetEnumerator()
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1587511574(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m518365436_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.get_IsFixedSize()
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3181165502(__this, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1965119572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2065342851(__this, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1699106999_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.get_Keys()
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m4062874847(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2972533211_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.Remove(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m4237549236(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m283317966_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.get_Values()
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m1620007815(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m3009893083_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.get_Item(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m2509170053(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m3224431377_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m197212552(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2135080018_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m749323633(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t2686657757 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m2962606917_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m2410941513(__this, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1251533845_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.ICollection.get_SyncRoot()
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m4287646453(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m289765249_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1954673132(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m356205410_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3008693707(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1313061119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::get_Count()
#define SortedDictionary_2_get_Count_m883997082(__this, method) ((  int32_t (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_get_Count_m3476867889_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::get_Item(TKey)
#define SortedDictionary_2_get_Item_m3772598140(__this, ___key0, method) ((  String_t* (*) (SortedDictionary_2_t2686657757 *, int32_t, const MethodInfo*))SortedDictionary_2_get_Item_m3013028876_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::set_Item(TKey,TValue)
#define SortedDictionary_2_set_Item_m2062002825(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2686657757 *, int32_t, String_t*, const MethodInfo*))SortedDictionary_2_set_Item_m3432483157_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::get_Keys()
#define SortedDictionary_2_get_Keys_m767207037(__this, method) ((  KeyCollection_t2900803370 * (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_get_Keys_m2311581057_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::Add(TKey,TValue)
#define SortedDictionary_2_Add_m1865225950(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2686657757 *, int32_t, String_t*, const MethodInfo*))SortedDictionary_2_Add_m3384498232_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::Clear()
#define SortedDictionary_2_Clear_m2083731378(__this, method) ((  void (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_Clear_m2537365212_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::ContainsKey(TKey)
#define SortedDictionary_2_ContainsKey_m1967754446(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsKey_m2099208916_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::ContainsValue(TValue)
#define SortedDictionary_2_ContainsValue_m1580442398(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, String_t*, const MethodInfo*))SortedDictionary_2_ContainsValue_m2660776548_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SortedDictionary_2_CopyTo_m2686052637(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t2686657757 *, KeyValuePair_2U5BU5D_t2921248967*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m1830145497_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m3353318127(__this, method) ((  Enumerator_t4055219257  (*) (SortedDictionary_2_t2686657757 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m369699555_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::Remove(TKey)
#define SortedDictionary_2_Remove_m1503516494(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, int32_t, const MethodInfo*))SortedDictionary_2_Remove_m2306315880_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::TryGetValue(TKey,TValue&)
#define SortedDictionary_2_TryGetValue_m651206397(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t2686657757 *, int32_t, String_t**, const MethodInfo*))SortedDictionary_2_TryGetValue_m2911573889_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::ToKey(System.Object)
#define SortedDictionary_2_ToKey_m1022843967(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m4262124275_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.String>::ToValue(System.Object)
#define SortedDictionary_2_ToValue_m254365055(__this, ___value0, method) ((  String_t* (*) (SortedDictionary_2_t2686657757 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m817036307_gshared)(__this, ___value0, method)
