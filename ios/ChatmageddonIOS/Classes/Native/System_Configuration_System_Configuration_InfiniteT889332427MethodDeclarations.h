﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.InfiniteTimeSpanConverter
struct InfiniteTimeSpanConverter_t889332427;
// System.Object
struct Il2CppObject;
// System.ComponentModel.ITypeDescriptorContext
struct ITypeDescriptorContext_t3633625151;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Configuration.InfiniteTimeSpanConverter::.ctor()
extern "C"  void InfiniteTimeSpanConverter__ctor_m2681144406 (InfiniteTimeSpanConverter_t889332427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InfiniteTimeSpanConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern "C"  Il2CppObject * InfiniteTimeSpanConverter_ConvertFrom_m4168434854 (InfiniteTimeSpanConverter_t889332427 * __this, Il2CppObject * ___ctx0, CultureInfo_t3500843524 * ___ci1, Il2CppObject * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InfiniteTimeSpanConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern "C"  Il2CppObject * InfiniteTimeSpanConverter_ConvertTo_m3818194294 (InfiniteTimeSpanConverter_t889332427 * __this, Il2CppObject * ___ctx0, CultureInfo_t3500843524 * ___ci1, Il2CppObject * ___value2, Type_t * ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
