﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AvatarUpdater
struct AvatarUpdater_t2404165026;
// AttackMiniGameController
struct AttackMiniGameController_t105133407;
// UITexture
struct UITexture_t2537039969;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackGameNavScreen
struct  AttackGameNavScreen_t4158424117  : public NavigationScreen_t2333230110
{
public:
	// AvatarUpdater AttackGameNavScreen::avatarUpdater
	AvatarUpdater_t2404165026 * ___avatarUpdater_3;
	// AttackMiniGameController AttackGameNavScreen::gameController
	AttackMiniGameController_t105133407 * ___gameController_4;
	// UITexture AttackGameNavScreen::backgroundTexture
	UITexture_t2537039969 * ___backgroundTexture_5;
	// System.Boolean AttackGameNavScreen::gameUIClosing
	bool ___gameUIClosing_6;

public:
	inline static int32_t get_offset_of_avatarUpdater_3() { return static_cast<int32_t>(offsetof(AttackGameNavScreen_t4158424117, ___avatarUpdater_3)); }
	inline AvatarUpdater_t2404165026 * get_avatarUpdater_3() const { return ___avatarUpdater_3; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatarUpdater_3() { return &___avatarUpdater_3; }
	inline void set_avatarUpdater_3(AvatarUpdater_t2404165026 * value)
	{
		___avatarUpdater_3 = value;
		Il2CppCodeGenWriteBarrier(&___avatarUpdater_3, value);
	}

	inline static int32_t get_offset_of_gameController_4() { return static_cast<int32_t>(offsetof(AttackGameNavScreen_t4158424117, ___gameController_4)); }
	inline AttackMiniGameController_t105133407 * get_gameController_4() const { return ___gameController_4; }
	inline AttackMiniGameController_t105133407 ** get_address_of_gameController_4() { return &___gameController_4; }
	inline void set_gameController_4(AttackMiniGameController_t105133407 * value)
	{
		___gameController_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameController_4, value);
	}

	inline static int32_t get_offset_of_backgroundTexture_5() { return static_cast<int32_t>(offsetof(AttackGameNavScreen_t4158424117, ___backgroundTexture_5)); }
	inline UITexture_t2537039969 * get_backgroundTexture_5() const { return ___backgroundTexture_5; }
	inline UITexture_t2537039969 ** get_address_of_backgroundTexture_5() { return &___backgroundTexture_5; }
	inline void set_backgroundTexture_5(UITexture_t2537039969 * value)
	{
		___backgroundTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTexture_5, value);
	}

	inline static int32_t get_offset_of_gameUIClosing_6() { return static_cast<int32_t>(offsetof(AttackGameNavScreen_t4158424117, ___gameUIClosing_6)); }
	inline bool get_gameUIClosing_6() const { return ___gameUIClosing_6; }
	inline bool* get_address_of_gameUIClosing_6() { return &___gameUIClosing_6; }
	inline void set_gameUIClosing_6(bool value)
	{
		___gameUIClosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
