﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefendModal
struct DefendModal_t2645228609;
// UILabel
struct UILabel_t1795115428;
// MissileArriveTimer
struct MissileArriveTimer_t44929408;
// MissilePointsPopulator
struct MissilePointsPopulator_t2262625279;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// DefendMiniGameController
struct DefendMiniGameController_t1844356491;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendGameNavScreen
struct  DefendGameNavScreen_t2935542205  : public NavigationScreen_t2333230110
{
public:
	// DefendModal DefendGameNavScreen::modal
	DefendModal_t2645228609 * ___modal_3;
	// UILabel DefendGameNavScreen::missileNameLabel
	UILabel_t1795115428 * ___missileNameLabel_4;
	// MissileArriveTimer DefendGameNavScreen::arriveTimer
	MissileArriveTimer_t44929408 * ___arriveTimer_5;
	// MissilePointsPopulator DefendGameNavScreen::pointsPopulator
	MissilePointsPopulator_t2262625279 * ___pointsPopulator_6;
	// System.String DefendGameNavScreen::miniGameResourcePath
	String_t* ___miniGameResourcePath_7;
	// UnityEngine.GameObject DefendGameNavScreen::gameUI
	GameObject_t1756533147 * ___gameUI_8;
	// UnityEngine.GameObject DefendGameNavScreen::miniGameObject
	GameObject_t1756533147 * ___miniGameObject_9;
	// DefendMiniGameController DefendGameNavScreen::currentMiniGame
	DefendMiniGameController_t1844356491 * ___currentMiniGame_10;
	// System.Boolean DefendGameNavScreen::uiClosing
	bool ___uiClosing_11;
	// System.Double DefendGameNavScreen::totalTimeAsSeconds
	double ___totalTimeAsSeconds_12;
	// MissileTravelState DefendGameNavScreen::currentState
	int32_t ___currentState_13;
	// System.Single DefendGameNavScreen::latestPointsAwarded
	float ___latestPointsAwarded_14;

public:
	inline static int32_t get_offset_of_modal_3() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___modal_3)); }
	inline DefendModal_t2645228609 * get_modal_3() const { return ___modal_3; }
	inline DefendModal_t2645228609 ** get_address_of_modal_3() { return &___modal_3; }
	inline void set_modal_3(DefendModal_t2645228609 * value)
	{
		___modal_3 = value;
		Il2CppCodeGenWriteBarrier(&___modal_3, value);
	}

	inline static int32_t get_offset_of_missileNameLabel_4() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___missileNameLabel_4)); }
	inline UILabel_t1795115428 * get_missileNameLabel_4() const { return ___missileNameLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_missileNameLabel_4() { return &___missileNameLabel_4; }
	inline void set_missileNameLabel_4(UILabel_t1795115428 * value)
	{
		___missileNameLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___missileNameLabel_4, value);
	}

	inline static int32_t get_offset_of_arriveTimer_5() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___arriveTimer_5)); }
	inline MissileArriveTimer_t44929408 * get_arriveTimer_5() const { return ___arriveTimer_5; }
	inline MissileArriveTimer_t44929408 ** get_address_of_arriveTimer_5() { return &___arriveTimer_5; }
	inline void set_arriveTimer_5(MissileArriveTimer_t44929408 * value)
	{
		___arriveTimer_5 = value;
		Il2CppCodeGenWriteBarrier(&___arriveTimer_5, value);
	}

	inline static int32_t get_offset_of_pointsPopulator_6() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___pointsPopulator_6)); }
	inline MissilePointsPopulator_t2262625279 * get_pointsPopulator_6() const { return ___pointsPopulator_6; }
	inline MissilePointsPopulator_t2262625279 ** get_address_of_pointsPopulator_6() { return &___pointsPopulator_6; }
	inline void set_pointsPopulator_6(MissilePointsPopulator_t2262625279 * value)
	{
		___pointsPopulator_6 = value;
		Il2CppCodeGenWriteBarrier(&___pointsPopulator_6, value);
	}

	inline static int32_t get_offset_of_miniGameResourcePath_7() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___miniGameResourcePath_7)); }
	inline String_t* get_miniGameResourcePath_7() const { return ___miniGameResourcePath_7; }
	inline String_t** get_address_of_miniGameResourcePath_7() { return &___miniGameResourcePath_7; }
	inline void set_miniGameResourcePath_7(String_t* value)
	{
		___miniGameResourcePath_7 = value;
		Il2CppCodeGenWriteBarrier(&___miniGameResourcePath_7, value);
	}

	inline static int32_t get_offset_of_gameUI_8() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___gameUI_8)); }
	inline GameObject_t1756533147 * get_gameUI_8() const { return ___gameUI_8; }
	inline GameObject_t1756533147 ** get_address_of_gameUI_8() { return &___gameUI_8; }
	inline void set_gameUI_8(GameObject_t1756533147 * value)
	{
		___gameUI_8 = value;
		Il2CppCodeGenWriteBarrier(&___gameUI_8, value);
	}

	inline static int32_t get_offset_of_miniGameObject_9() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___miniGameObject_9)); }
	inline GameObject_t1756533147 * get_miniGameObject_9() const { return ___miniGameObject_9; }
	inline GameObject_t1756533147 ** get_address_of_miniGameObject_9() { return &___miniGameObject_9; }
	inline void set_miniGameObject_9(GameObject_t1756533147 * value)
	{
		___miniGameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___miniGameObject_9, value);
	}

	inline static int32_t get_offset_of_currentMiniGame_10() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___currentMiniGame_10)); }
	inline DefendMiniGameController_t1844356491 * get_currentMiniGame_10() const { return ___currentMiniGame_10; }
	inline DefendMiniGameController_t1844356491 ** get_address_of_currentMiniGame_10() { return &___currentMiniGame_10; }
	inline void set_currentMiniGame_10(DefendMiniGameController_t1844356491 * value)
	{
		___currentMiniGame_10 = value;
		Il2CppCodeGenWriteBarrier(&___currentMiniGame_10, value);
	}

	inline static int32_t get_offset_of_uiClosing_11() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___uiClosing_11)); }
	inline bool get_uiClosing_11() const { return ___uiClosing_11; }
	inline bool* get_address_of_uiClosing_11() { return &___uiClosing_11; }
	inline void set_uiClosing_11(bool value)
	{
		___uiClosing_11 = value;
	}

	inline static int32_t get_offset_of_totalTimeAsSeconds_12() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___totalTimeAsSeconds_12)); }
	inline double get_totalTimeAsSeconds_12() const { return ___totalTimeAsSeconds_12; }
	inline double* get_address_of_totalTimeAsSeconds_12() { return &___totalTimeAsSeconds_12; }
	inline void set_totalTimeAsSeconds_12(double value)
	{
		___totalTimeAsSeconds_12 = value;
	}

	inline static int32_t get_offset_of_currentState_13() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___currentState_13)); }
	inline int32_t get_currentState_13() const { return ___currentState_13; }
	inline int32_t* get_address_of_currentState_13() { return &___currentState_13; }
	inline void set_currentState_13(int32_t value)
	{
		___currentState_13 = value;
	}

	inline static int32_t get_offset_of_latestPointsAwarded_14() { return static_cast<int32_t>(offsetof(DefendGameNavScreen_t2935542205, ___latestPointsAwarded_14)); }
	inline float get_latestPointsAwarded_14() const { return ___latestPointsAwarded_14; }
	inline float* get_address_of_latestPointsAwarded_14() { return &___latestPointsAwarded_14; }
	inline void set_latestPointsAwarded_14(float value)
	{
		___latestPointsAwarded_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
