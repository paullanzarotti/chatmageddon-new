﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraManager/<textureFromFileAtPath>c__Iterator0
struct U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraManager/<textureFromFileAtPath>c__Iterator0::.ctor()
extern "C"  void U3CtextureFromFileAtPathU3Ec__Iterator0__ctor_m1335685698 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraManager/<textureFromFileAtPath>c__Iterator0::MoveNext()
extern "C"  bool U3CtextureFromFileAtPathU3Ec__Iterator0_MoveNext_m2176145366 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraManager/<textureFromFileAtPath>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CtextureFromFileAtPathU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2644956208 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraManager/<textureFromFileAtPath>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CtextureFromFileAtPathU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1786005976 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager/<textureFromFileAtPath>c__Iterator0::Dispose()
extern "C"  void U3CtextureFromFileAtPathU3Ec__Iterator0_Dispose_m1194933631 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager/<textureFromFileAtPath>c__Iterator0::Reset()
extern "C"  void U3CtextureFromFileAtPathU3Ec__Iterator0_Reset_m1948738653 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager/<textureFromFileAtPath>c__Iterator0::<>__Finally0()
extern "C"  void U3CtextureFromFileAtPathU3Ec__Iterator0_U3CU3E__Finally0_m2035209435 (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
