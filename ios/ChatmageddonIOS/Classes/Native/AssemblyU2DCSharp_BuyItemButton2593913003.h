﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyItemButton
struct  BuyItemButton_t2593913003  : public SFXButton_t792651341
{
public:
	// UnityEngine.Color BuyItemButton::activeColour
	Color_t2020392075  ___activeColour_5;
	// UnityEngine.Color BuyItemButton::lockedColour
	Color_t2020392075  ___lockedColour_6;
	// UnityEngine.Color BuyItemButton::inactiveColour
	Color_t2020392075  ___inactiveColour_7;
	// UILabel BuyItemButton::buyLabel
	UILabel_t1795115428 * ___buyLabel_8;

public:
	inline static int32_t get_offset_of_activeColour_5() { return static_cast<int32_t>(offsetof(BuyItemButton_t2593913003, ___activeColour_5)); }
	inline Color_t2020392075  get_activeColour_5() const { return ___activeColour_5; }
	inline Color_t2020392075 * get_address_of_activeColour_5() { return &___activeColour_5; }
	inline void set_activeColour_5(Color_t2020392075  value)
	{
		___activeColour_5 = value;
	}

	inline static int32_t get_offset_of_lockedColour_6() { return static_cast<int32_t>(offsetof(BuyItemButton_t2593913003, ___lockedColour_6)); }
	inline Color_t2020392075  get_lockedColour_6() const { return ___lockedColour_6; }
	inline Color_t2020392075 * get_address_of_lockedColour_6() { return &___lockedColour_6; }
	inline void set_lockedColour_6(Color_t2020392075  value)
	{
		___lockedColour_6 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_7() { return static_cast<int32_t>(offsetof(BuyItemButton_t2593913003, ___inactiveColour_7)); }
	inline Color_t2020392075  get_inactiveColour_7() const { return ___inactiveColour_7; }
	inline Color_t2020392075 * get_address_of_inactiveColour_7() { return &___inactiveColour_7; }
	inline void set_inactiveColour_7(Color_t2020392075  value)
	{
		___inactiveColour_7 = value;
	}

	inline static int32_t get_offset_of_buyLabel_8() { return static_cast<int32_t>(offsetof(BuyItemButton_t2593913003, ___buyLabel_8)); }
	inline UILabel_t1795115428 * get_buyLabel_8() const { return ___buyLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_buyLabel_8() { return &___buyLabel_8; }
	inline void set_buyLabel_8(UILabel_t1795115428 * value)
	{
		___buyLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___buyLabel_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
