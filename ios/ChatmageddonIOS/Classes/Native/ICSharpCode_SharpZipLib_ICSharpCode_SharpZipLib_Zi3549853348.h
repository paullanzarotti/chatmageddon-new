﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t1110175137;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t1764014695;

#include "mscorlib_System_Object2689449295.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4223507703.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.TestStatus
struct  TestStatus_t3549853348  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.TestStatus::file_
	ZipFile_t1110175137 * ___file__0;
	// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.TestStatus::entry_
	ZipEntry_t1764014695 * ___entry__1;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.TestStatus::entryValid_
	bool ___entryValid__2;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.TestStatus::errorCount_
	int32_t ___errorCount__3;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.TestStatus::bytesTested_
	int64_t ___bytesTested__4;
	// ICSharpCode.SharpZipLib.Zip.TestOperation ICSharpCode.SharpZipLib.Zip.TestStatus::operation_
	int32_t ___operation__5;

public:
	inline static int32_t get_offset_of_file__0() { return static_cast<int32_t>(offsetof(TestStatus_t3549853348, ___file__0)); }
	inline ZipFile_t1110175137 * get_file__0() const { return ___file__0; }
	inline ZipFile_t1110175137 ** get_address_of_file__0() { return &___file__0; }
	inline void set_file__0(ZipFile_t1110175137 * value)
	{
		___file__0 = value;
		Il2CppCodeGenWriteBarrier(&___file__0, value);
	}

	inline static int32_t get_offset_of_entry__1() { return static_cast<int32_t>(offsetof(TestStatus_t3549853348, ___entry__1)); }
	inline ZipEntry_t1764014695 * get_entry__1() const { return ___entry__1; }
	inline ZipEntry_t1764014695 ** get_address_of_entry__1() { return &___entry__1; }
	inline void set_entry__1(ZipEntry_t1764014695 * value)
	{
		___entry__1 = value;
		Il2CppCodeGenWriteBarrier(&___entry__1, value);
	}

	inline static int32_t get_offset_of_entryValid__2() { return static_cast<int32_t>(offsetof(TestStatus_t3549853348, ___entryValid__2)); }
	inline bool get_entryValid__2() const { return ___entryValid__2; }
	inline bool* get_address_of_entryValid__2() { return &___entryValid__2; }
	inline void set_entryValid__2(bool value)
	{
		___entryValid__2 = value;
	}

	inline static int32_t get_offset_of_errorCount__3() { return static_cast<int32_t>(offsetof(TestStatus_t3549853348, ___errorCount__3)); }
	inline int32_t get_errorCount__3() const { return ___errorCount__3; }
	inline int32_t* get_address_of_errorCount__3() { return &___errorCount__3; }
	inline void set_errorCount__3(int32_t value)
	{
		___errorCount__3 = value;
	}

	inline static int32_t get_offset_of_bytesTested__4() { return static_cast<int32_t>(offsetof(TestStatus_t3549853348, ___bytesTested__4)); }
	inline int64_t get_bytesTested__4() const { return ___bytesTested__4; }
	inline int64_t* get_address_of_bytesTested__4() { return &___bytesTested__4; }
	inline void set_bytesTested__4(int64_t value)
	{
		___bytesTested__4 = value;
	}

	inline static int32_t get_offset_of_operation__5() { return static_cast<int32_t>(offsetof(TestStatus_t3549853348, ___operation__5)); }
	inline int32_t get_operation__5() const { return ___operation__5; }
	inline int32_t* get_address_of_operation__5() { return &___operation__5; }
	inline void set_operation__5(int32_t value)
	{
		___operation__5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
