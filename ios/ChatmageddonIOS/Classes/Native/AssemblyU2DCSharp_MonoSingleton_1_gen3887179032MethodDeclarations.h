﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ModalManager>::.ctor()
#define MonoSingleton_1__ctor_m2010713456(__this, method) ((  void (*) (MonoSingleton_1_t3887179032 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ModalManager>::Awake()
#define MonoSingleton_1_Awake_m1569511281(__this, method) ((  void (*) (MonoSingleton_1_t3887179032 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ModalManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3651464771(__this /* static, unused */, method) ((  ModalManager_t4136513312 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ModalManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1749275871(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ModalManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1579752082(__this, method) ((  void (*) (MonoSingleton_1_t3887179032 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ModalManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m351964486(__this, method) ((  void (*) (MonoSingleton_1_t3887179032 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ModalManager>::.cctor()
#define MonoSingleton_1__cctor_m3013538479(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
