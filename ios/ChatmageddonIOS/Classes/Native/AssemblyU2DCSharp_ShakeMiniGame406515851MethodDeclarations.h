﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShakeMiniGame
struct ShakeMiniGame_t406515851;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void ShakeMiniGame::.ctor()
extern "C"  void ShakeMiniGame__ctor_m1430178566 (ShakeMiniGame_t406515851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeMiniGame::StartMiniGame()
extern "C"  void ShakeMiniGame_StartMiniGame_m1307991463 (ShakeMiniGame_t406515851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeMiniGame::StopMiniGame()
extern "C"  void ShakeMiniGame_StopMiniGame_m2133266759 (ShakeMiniGame_t406515851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeMiniGame::OnPaused()
extern "C"  void ShakeMiniGame_OnPaused_m1439729565 (ShakeMiniGame_t406515851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeMiniGame::ResetMiniGame()
extern "C"  void ShakeMiniGame_ResetMiniGame_m3134208992 (ShakeMiniGame_t406515851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeMiniGame::TimerUpdated(System.TimeSpan)
extern "C"  void ShakeMiniGame_TimerUpdated_m3009008730 (ShakeMiniGame_t406515851 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
