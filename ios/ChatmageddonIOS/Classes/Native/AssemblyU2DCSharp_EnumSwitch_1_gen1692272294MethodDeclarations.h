﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnumSwitch`1<System.Object>
struct EnumSwitch_1_t1692272294;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void EnumSwitch`1<System.Object>::.ctor()
extern "C"  void EnumSwitch_1__ctor_m2582214191_gshared (EnumSwitch_1_t1692272294 * __this, const MethodInfo* method);
#define EnumSwitch_1__ctor_m2582214191(__this, method) ((  void (*) (EnumSwitch_1_t1692272294 *, const MethodInfo*))EnumSwitch_1__ctor_m2582214191_gshared)(__this, method)
// System.Void EnumSwitch`1<System.Object>::SelectSwitchState(SwitchState)
extern "C"  void EnumSwitch_1_SelectSwitchState_m3525231895_gshared (EnumSwitch_1_t1692272294 * __this, Il2CppObject * ___newState0, const MethodInfo* method);
#define EnumSwitch_1_SelectSwitchState_m3525231895(__this, ___newState0, method) ((  void (*) (EnumSwitch_1_t1692272294 *, Il2CppObject *, const MethodInfo*))EnumSwitch_1_SelectSwitchState_m3525231895_gshared)(__this, ___newState0, method)
// System.Void EnumSwitch`1<System.Object>::OnSwitch(SwitchState)
extern "C"  void EnumSwitch_1_OnSwitch_m978664305_gshared (EnumSwitch_1_t1692272294 * __this, Il2CppObject * ___newState0, const MethodInfo* method);
#define EnumSwitch_1_OnSwitch_m978664305(__this, ___newState0, method) ((  void (*) (EnumSwitch_1_t1692272294 *, Il2CppObject *, const MethodInfo*))EnumSwitch_1_OnSwitch_m978664305_gshared)(__this, ___newState0, method)
