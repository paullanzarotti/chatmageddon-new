﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>
struct SortedDictionary_2_t3346886819;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3212009085.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m245074370_gshared (Enumerator_t3212009085 * __this, SortedDictionary_2_t3346886819 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m245074370(__this, ___dic0, method) ((  void (*) (Enumerator_t3212009085 *, SortedDictionary_2_t3346886819 *, const MethodInfo*))Enumerator__ctor_m245074370_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m451777246_gshared (Enumerator_t3212009085 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m451777246(__this, method) ((  Il2CppObject * (*) (Enumerator_t3212009085 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m451777246_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1175503104_gshared (Enumerator_t3212009085 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1175503104(__this, method) ((  void (*) (Enumerator_t3212009085 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1175503104_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4004123576_gshared (Enumerator_t3212009085 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4004123576(__this, method) ((  int32_t (*) (Enumerator_t3212009085 *, const MethodInfo*))Enumerator_get_Current_m4004123576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m17822024_gshared (Enumerator_t3212009085 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17822024(__this, method) ((  bool (*) (Enumerator_t3212009085 *, const MethodInfo*))Enumerator_MoveNext_m17822024_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4250516907_gshared (Enumerator_t3212009085 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4250516907(__this, method) ((  void (*) (Enumerator_t3212009085 *, const MethodInfo*))Enumerator_Dispose_m4250516907_gshared)(__this, method)
