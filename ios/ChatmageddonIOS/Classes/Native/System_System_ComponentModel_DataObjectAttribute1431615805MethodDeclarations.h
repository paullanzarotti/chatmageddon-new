﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.DataObjectAttribute
struct DataObjectAttribute_t1431615805;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.DataObjectAttribute::.ctor()
extern "C"  void DataObjectAttribute__ctor_m807974022 (DataObjectAttribute_t1431615805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DataObjectAttribute::.ctor(System.Boolean)
extern "C"  void DataObjectAttribute__ctor_m868678109 (DataObjectAttribute_t1431615805 * __this, bool ___isDataObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DataObjectAttribute::.cctor()
extern "C"  void DataObjectAttribute__cctor_m280842709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectAttribute::get_IsDataObject()
extern "C"  bool DataObjectAttribute_get_IsDataObject_m551433978 (DataObjectAttribute_t1431615805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectAttribute::Equals(System.Object)
extern "C"  bool DataObjectAttribute_Equals_m1750622091 (DataObjectAttribute_t1431615805 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.DataObjectAttribute::GetHashCode()
extern "C"  int32_t DataObjectAttribute_GetHashCode_m2268733821 (DataObjectAttribute_t1431615805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectAttribute::IsDefaultAttribute()
extern "C"  bool DataObjectAttribute_IsDefaultAttribute_m2598712571 (DataObjectAttribute_t1431615805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
