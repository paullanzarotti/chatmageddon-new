﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.MetadataManager/<LoadMedataFromFile>c__AnonStorey0
struct  U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160  : public Il2CppObject
{
public:
	// System.String PhoneNumbers.MetadataManager/<LoadMedataFromFile>c__AnonStorey0::filePrefix
	String_t* ___filePrefix_0;

public:
	inline static int32_t get_offset_of_filePrefix_0() { return static_cast<int32_t>(offsetof(U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160, ___filePrefix_0)); }
	inline String_t* get_filePrefix_0() const { return ___filePrefix_0; }
	inline String_t** get_address_of_filePrefix_0() { return &___filePrefix_0; }
	inline void set_filePrefix_0(String_t* value)
	{
		___filePrefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___filePrefix_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
