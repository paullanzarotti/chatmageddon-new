﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuantitativeItem
struct QuantitativeItem_t3036513780;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_QuantitativeItem3036513780.h"
#include "AssemblyU2DCSharp_ItemType3455197591.h"
#include "mscorlib_System_String2029220233.h"

// System.Void QuantitativeItem::.ctor(System.Collections.Hashtable)
extern "C"  void QuantitativeItem__ctor_m2147152727 (QuantitativeItem_t3036513780 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuantitativeItem::.ctor(QuantitativeItem)
extern "C"  void QuantitativeItem__ctor_m1867119627 (QuantitativeItem_t3036513780 * __this, QuantitativeItem_t3036513780 * ___existingItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ItemType QuantitativeItem::GetItemTypeFromString(System.String)
extern "C"  int32_t QuantitativeItem_GetItemTypeFromString_m2403294987 (QuantitativeItem_t3036513780 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
