﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstStrikeUI
struct  FirstStrikeUI_t2168768222  : public MonoBehaviour_t1158329972
{
public:
	// UISprite FirstStrikeUI::overlay
	UISprite_t603616735 * ___overlay_2;
	// UILabel FirstStrikeUI::firstStrikeLabel
	UILabel_t1795115428 * ___firstStrikeLabel_3;
	// UILabel FirstStrikeUI::firstStrikeAmount
	UILabel_t1795115428 * ___firstStrikeAmount_4;
	// System.String FirstStrikeUI::prePointsString
	String_t* ___prePointsString_5;
	// System.String FirstStrikeUI::postPointsString
	String_t* ___postPointsString_6;

public:
	inline static int32_t get_offset_of_overlay_2() { return static_cast<int32_t>(offsetof(FirstStrikeUI_t2168768222, ___overlay_2)); }
	inline UISprite_t603616735 * get_overlay_2() const { return ___overlay_2; }
	inline UISprite_t603616735 ** get_address_of_overlay_2() { return &___overlay_2; }
	inline void set_overlay_2(UISprite_t603616735 * value)
	{
		___overlay_2 = value;
		Il2CppCodeGenWriteBarrier(&___overlay_2, value);
	}

	inline static int32_t get_offset_of_firstStrikeLabel_3() { return static_cast<int32_t>(offsetof(FirstStrikeUI_t2168768222, ___firstStrikeLabel_3)); }
	inline UILabel_t1795115428 * get_firstStrikeLabel_3() const { return ___firstStrikeLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_firstStrikeLabel_3() { return &___firstStrikeLabel_3; }
	inline void set_firstStrikeLabel_3(UILabel_t1795115428 * value)
	{
		___firstStrikeLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___firstStrikeLabel_3, value);
	}

	inline static int32_t get_offset_of_firstStrikeAmount_4() { return static_cast<int32_t>(offsetof(FirstStrikeUI_t2168768222, ___firstStrikeAmount_4)); }
	inline UILabel_t1795115428 * get_firstStrikeAmount_4() const { return ___firstStrikeAmount_4; }
	inline UILabel_t1795115428 ** get_address_of_firstStrikeAmount_4() { return &___firstStrikeAmount_4; }
	inline void set_firstStrikeAmount_4(UILabel_t1795115428 * value)
	{
		___firstStrikeAmount_4 = value;
		Il2CppCodeGenWriteBarrier(&___firstStrikeAmount_4, value);
	}

	inline static int32_t get_offset_of_prePointsString_5() { return static_cast<int32_t>(offsetof(FirstStrikeUI_t2168768222, ___prePointsString_5)); }
	inline String_t* get_prePointsString_5() const { return ___prePointsString_5; }
	inline String_t** get_address_of_prePointsString_5() { return &___prePointsString_5; }
	inline void set_prePointsString_5(String_t* value)
	{
		___prePointsString_5 = value;
		Il2CppCodeGenWriteBarrier(&___prePointsString_5, value);
	}

	inline static int32_t get_offset_of_postPointsString_6() { return static_cast<int32_t>(offsetof(FirstStrikeUI_t2168768222, ___postPointsString_6)); }
	inline String_t* get_postPointsString_6() const { return ___postPointsString_6; }
	inline String_t** get_address_of_postPointsString_6() { return &___postPointsString_6; }
	inline void set_postPointsString_6(String_t* value)
	{
		___postPointsString_6 = value;
		Il2CppCodeGenWriteBarrier(&___postPointsString_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
