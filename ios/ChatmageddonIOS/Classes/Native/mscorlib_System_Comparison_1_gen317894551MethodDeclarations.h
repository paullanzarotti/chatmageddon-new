﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3951188146MethodDeclarations.h"

// System.Void System.Comparison`1<PurchaseableItem>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m815268585(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t317894551 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<PurchaseableItem>::Invoke(T,T)
#define Comparison_1_Invoke_m1662748655(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t317894551 *, PurchaseableItem_t3351122996 *, PurchaseableItem_t3351122996 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<PurchaseableItem>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1097438128(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t317894551 *, PurchaseableItem_t3351122996 *, PurchaseableItem_t3351122996 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<PurchaseableItem>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1276171165(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t317894551 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
