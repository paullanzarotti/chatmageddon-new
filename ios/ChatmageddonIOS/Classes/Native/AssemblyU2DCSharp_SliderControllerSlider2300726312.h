﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SliderController
struct SliderController_t2812089311;
// System.Collections.Generic.List`1<System.String[]>
struct List_1_t1011507104;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliderControllerSlider
struct  SliderControllerSlider_t2300726312  : public MonoBehaviour_t1158329972
{
public:
	// SliderController SliderControllerSlider::sliderController
	SliderController_t2812089311 * ___sliderController_2;
	// System.Collections.Generic.List`1<System.String[]> SliderControllerSlider::debugArray
	List_1_t1011507104 * ___debugArray_3;
	// UnityEngine.Vector2 SliderControllerSlider::firstHit
	Vector2_t2243707579  ___firstHit_4;
	// UnityEngine.Vector2 SliderControllerSlider::newPos
	Vector2_t2243707579  ___newPos_5;
	// UnityEngine.Transform SliderControllerSlider::pickedObject
	Transform_t3275118058 * ___pickedObject_6;
	// System.Boolean SliderControllerSlider::posLocked
	bool ___posLocked_7;
	// System.Single[] SliderControllerSlider::pointPos
	SingleU5BU5D_t577127397* ___pointPos_8;

public:
	inline static int32_t get_offset_of_sliderController_2() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___sliderController_2)); }
	inline SliderController_t2812089311 * get_sliderController_2() const { return ___sliderController_2; }
	inline SliderController_t2812089311 ** get_address_of_sliderController_2() { return &___sliderController_2; }
	inline void set_sliderController_2(SliderController_t2812089311 * value)
	{
		___sliderController_2 = value;
		Il2CppCodeGenWriteBarrier(&___sliderController_2, value);
	}

	inline static int32_t get_offset_of_debugArray_3() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___debugArray_3)); }
	inline List_1_t1011507104 * get_debugArray_3() const { return ___debugArray_3; }
	inline List_1_t1011507104 ** get_address_of_debugArray_3() { return &___debugArray_3; }
	inline void set_debugArray_3(List_1_t1011507104 * value)
	{
		___debugArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___debugArray_3, value);
	}

	inline static int32_t get_offset_of_firstHit_4() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___firstHit_4)); }
	inline Vector2_t2243707579  get_firstHit_4() const { return ___firstHit_4; }
	inline Vector2_t2243707579 * get_address_of_firstHit_4() { return &___firstHit_4; }
	inline void set_firstHit_4(Vector2_t2243707579  value)
	{
		___firstHit_4 = value;
	}

	inline static int32_t get_offset_of_newPos_5() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___newPos_5)); }
	inline Vector2_t2243707579  get_newPos_5() const { return ___newPos_5; }
	inline Vector2_t2243707579 * get_address_of_newPos_5() { return &___newPos_5; }
	inline void set_newPos_5(Vector2_t2243707579  value)
	{
		___newPos_5 = value;
	}

	inline static int32_t get_offset_of_pickedObject_6() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___pickedObject_6)); }
	inline Transform_t3275118058 * get_pickedObject_6() const { return ___pickedObject_6; }
	inline Transform_t3275118058 ** get_address_of_pickedObject_6() { return &___pickedObject_6; }
	inline void set_pickedObject_6(Transform_t3275118058 * value)
	{
		___pickedObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___pickedObject_6, value);
	}

	inline static int32_t get_offset_of_posLocked_7() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___posLocked_7)); }
	inline bool get_posLocked_7() const { return ___posLocked_7; }
	inline bool* get_address_of_posLocked_7() { return &___posLocked_7; }
	inline void set_posLocked_7(bool value)
	{
		___posLocked_7 = value;
	}

	inline static int32_t get_offset_of_pointPos_8() { return static_cast<int32_t>(offsetof(SliderControllerSlider_t2300726312, ___pointPos_8)); }
	inline SingleU5BU5D_t577127397* get_pointPos_8() const { return ___pointPos_8; }
	inline SingleU5BU5D_t577127397** get_address_of_pointPos_8() { return &___pointPos_8; }
	inline void set_pointPos_8(SingleU5BU5D_t577127397* value)
	{
		___pointPos_8 = value;
		Il2CppCodeGenWriteBarrier(&___pointPos_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
