﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPDatePicker/Date
struct Date_t2675184608;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void IPDatePicker/Date::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void Date__ctor_m2272025164 (Date_t2675184608 * __this, int32_t ___iday0, int32_t ___imonth1, int32_t ___iyear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime IPDatePicker/Date::GetDateTime()
extern "C"  DateTime_t693205669  Date_GetDateTime_m3340793061 (Date_t2675184608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
