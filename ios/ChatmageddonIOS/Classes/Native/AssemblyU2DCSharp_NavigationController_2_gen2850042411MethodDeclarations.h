﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,RegNavScreen>
struct NavigationController_2_t2850042411;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,RegNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m356458940_gshared (NavigationController_2_t2850042411 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m356458940(__this, method) ((  void (*) (NavigationController_2_t2850042411 *, const MethodInfo*))NavigationController_2__ctor_m356458940_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,RegNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m550598550_gshared (NavigationController_2_t2850042411 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m550598550(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2850042411 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m550598550_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,RegNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1751947769_gshared (NavigationController_2_t2850042411 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m1751947769(__this, method) ((  void (*) (NavigationController_2_t2850042411 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1751947769_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,RegNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m639547216_gshared (NavigationController_2_t2850042411 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m639547216(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2850042411 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m639547216_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,RegNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m2164337852_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m2164337852(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2850042411 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m2164337852_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,RegNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m4143251419_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m4143251419(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2850042411 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m4143251419_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,RegNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m2387324626_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m2387324626(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2850042411 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m2387324626_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,RegNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2311448660_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m2311448660(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2850042411 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2311448660_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,RegNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m366324300_gshared (NavigationController_2_t2850042411 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m366324300(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2850042411 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m366324300_gshared)(__this, ___screen0, method)
