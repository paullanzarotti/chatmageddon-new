﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CountryCodeData
struct CountryCodeData_t1765037603;
// System.String
struct String_t;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3918856289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactsPhoneNumberManager
struct  ContactsPhoneNumberManager_t4168190569  : public MonoSingleton_1_t3918856289
{
public:
	// System.Boolean ContactsPhoneNumberManager::phoneNumberResent
	bool ___phoneNumberResent_3;
	// System.Boolean ContactsPhoneNumberManager::numberVerified
	bool ___numberVerified_4;
	// CountryCodeData ContactsPhoneNumberManager::selectedCountryCode
	CountryCodeData_t1765037603 * ___selectedCountryCode_5;
	// System.String ContactsPhoneNumberManager::countryCode
	String_t* ___countryCode_6;
	// System.String ContactsPhoneNumberManager::phoneNumber
	String_t* ___phoneNumber_7;
	// System.String ContactsPhoneNumberManager::fullPhoneNumber
	String_t* ___fullPhoneNumber_8;
	// System.String ContactsPhoneNumberManager::mobileHash
	String_t* ___mobileHash_9;
	// System.String ContactsPhoneNumberManager::mobilePinID
	String_t* ___mobilePinID_10;
	// System.String ContactsPhoneNumberManager::mobilePin
	String_t* ___mobilePin_11;
	// System.Boolean ContactsPhoneNumberManager::attack
	bool ___attack_12;
	// UILabel ContactsPhoneNumberManager::countryNameLabel
	UILabel_t1795115428 * ___countryNameLabel_13;
	// UILabel ContactsPhoneNumberManager::countryCodeLabel
	UILabel_t1795115428 * ___countryCodeLabel_14;

public:
	inline static int32_t get_offset_of_phoneNumberResent_3() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___phoneNumberResent_3)); }
	inline bool get_phoneNumberResent_3() const { return ___phoneNumberResent_3; }
	inline bool* get_address_of_phoneNumberResent_3() { return &___phoneNumberResent_3; }
	inline void set_phoneNumberResent_3(bool value)
	{
		___phoneNumberResent_3 = value;
	}

	inline static int32_t get_offset_of_numberVerified_4() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___numberVerified_4)); }
	inline bool get_numberVerified_4() const { return ___numberVerified_4; }
	inline bool* get_address_of_numberVerified_4() { return &___numberVerified_4; }
	inline void set_numberVerified_4(bool value)
	{
		___numberVerified_4 = value;
	}

	inline static int32_t get_offset_of_selectedCountryCode_5() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___selectedCountryCode_5)); }
	inline CountryCodeData_t1765037603 * get_selectedCountryCode_5() const { return ___selectedCountryCode_5; }
	inline CountryCodeData_t1765037603 ** get_address_of_selectedCountryCode_5() { return &___selectedCountryCode_5; }
	inline void set_selectedCountryCode_5(CountryCodeData_t1765037603 * value)
	{
		___selectedCountryCode_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectedCountryCode_5, value);
	}

	inline static int32_t get_offset_of_countryCode_6() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___countryCode_6)); }
	inline String_t* get_countryCode_6() const { return ___countryCode_6; }
	inline String_t** get_address_of_countryCode_6() { return &___countryCode_6; }
	inline void set_countryCode_6(String_t* value)
	{
		___countryCode_6 = value;
		Il2CppCodeGenWriteBarrier(&___countryCode_6, value);
	}

	inline static int32_t get_offset_of_phoneNumber_7() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___phoneNumber_7)); }
	inline String_t* get_phoneNumber_7() const { return ___phoneNumber_7; }
	inline String_t** get_address_of_phoneNumber_7() { return &___phoneNumber_7; }
	inline void set_phoneNumber_7(String_t* value)
	{
		___phoneNumber_7 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumber_7, value);
	}

	inline static int32_t get_offset_of_fullPhoneNumber_8() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___fullPhoneNumber_8)); }
	inline String_t* get_fullPhoneNumber_8() const { return ___fullPhoneNumber_8; }
	inline String_t** get_address_of_fullPhoneNumber_8() { return &___fullPhoneNumber_8; }
	inline void set_fullPhoneNumber_8(String_t* value)
	{
		___fullPhoneNumber_8 = value;
		Il2CppCodeGenWriteBarrier(&___fullPhoneNumber_8, value);
	}

	inline static int32_t get_offset_of_mobileHash_9() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___mobileHash_9)); }
	inline String_t* get_mobileHash_9() const { return ___mobileHash_9; }
	inline String_t** get_address_of_mobileHash_9() { return &___mobileHash_9; }
	inline void set_mobileHash_9(String_t* value)
	{
		___mobileHash_9 = value;
		Il2CppCodeGenWriteBarrier(&___mobileHash_9, value);
	}

	inline static int32_t get_offset_of_mobilePinID_10() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___mobilePinID_10)); }
	inline String_t* get_mobilePinID_10() const { return ___mobilePinID_10; }
	inline String_t** get_address_of_mobilePinID_10() { return &___mobilePinID_10; }
	inline void set_mobilePinID_10(String_t* value)
	{
		___mobilePinID_10 = value;
		Il2CppCodeGenWriteBarrier(&___mobilePinID_10, value);
	}

	inline static int32_t get_offset_of_mobilePin_11() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___mobilePin_11)); }
	inline String_t* get_mobilePin_11() const { return ___mobilePin_11; }
	inline String_t** get_address_of_mobilePin_11() { return &___mobilePin_11; }
	inline void set_mobilePin_11(String_t* value)
	{
		___mobilePin_11 = value;
		Il2CppCodeGenWriteBarrier(&___mobilePin_11, value);
	}

	inline static int32_t get_offset_of_attack_12() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___attack_12)); }
	inline bool get_attack_12() const { return ___attack_12; }
	inline bool* get_address_of_attack_12() { return &___attack_12; }
	inline void set_attack_12(bool value)
	{
		___attack_12 = value;
	}

	inline static int32_t get_offset_of_countryNameLabel_13() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___countryNameLabel_13)); }
	inline UILabel_t1795115428 * get_countryNameLabel_13() const { return ___countryNameLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_countryNameLabel_13() { return &___countryNameLabel_13; }
	inline void set_countryNameLabel_13(UILabel_t1795115428 * value)
	{
		___countryNameLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___countryNameLabel_13, value);
	}

	inline static int32_t get_offset_of_countryCodeLabel_14() { return static_cast<int32_t>(offsetof(ContactsPhoneNumberManager_t4168190569, ___countryCodeLabel_14)); }
	inline UILabel_t1795115428 * get_countryCodeLabel_14() const { return ___countryCodeLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_countryCodeLabel_14() { return &___countryCodeLabel_14; }
	inline void set_countryCodeLabel_14(UILabel_t1795115428 * value)
	{
		___countryCodeLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeLabel_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
