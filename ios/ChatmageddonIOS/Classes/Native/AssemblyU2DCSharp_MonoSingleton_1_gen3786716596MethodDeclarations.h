﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ChatmageddonSaveData>::.ctor()
#define MonoSingleton_1__ctor_m3004872892(__this, method) ((  void (*) (MonoSingleton_1_t3786716596 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonSaveData>::Awake()
#define MonoSingleton_1_Awake_m1532009605(__this, method) ((  void (*) (MonoSingleton_1_t3786716596 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ChatmageddonSaveData>::get_Instance()
#define MonoSingleton_1_get_Instance_m3183586707(__this /* static, unused */, method) ((  ChatmageddonSaveData_t4036050876 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ChatmageddonSaveData>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m194221679(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ChatmageddonSaveData>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3649649898(__this, method) ((  void (*) (MonoSingleton_1_t3786716596 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonSaveData>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1216274422(__this, method) ((  void (*) (MonoSingleton_1_t3786716596 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonSaveData>::.cctor()
#define MonoSingleton_1__cctor_m3971738935(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
