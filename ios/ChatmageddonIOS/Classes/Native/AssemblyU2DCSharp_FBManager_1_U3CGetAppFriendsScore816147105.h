﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t278961118;
// FBManager`1<System.Object>
struct FBManager_1_t2091981452;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>
struct  U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Collections.Hashtable> FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3::UsersInfoList
	List_1_t278961118 * ___UsersInfoList_0;
	// FBManager`1<ManagerType> FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3::$this
	FBManager_1_t2091981452 * ___U24this_1;

public:
	inline static int32_t get_offset_of_UsersInfoList_0() { return static_cast<int32_t>(offsetof(U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105, ___UsersInfoList_0)); }
	inline List_1_t278961118 * get_UsersInfoList_0() const { return ___UsersInfoList_0; }
	inline List_1_t278961118 ** get_address_of_UsersInfoList_0() { return &___UsersInfoList_0; }
	inline void set_UsersInfoList_0(List_1_t278961118 * value)
	{
		___UsersInfoList_0 = value;
		Il2CppCodeGenWriteBarrier(&___UsersInfoList_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105, ___U24this_1)); }
	inline FBManager_1_t2091981452 * get_U24this_1() const { return ___U24this_1; }
	inline FBManager_1_t2091981452 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FBManager_1_t2091981452 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
