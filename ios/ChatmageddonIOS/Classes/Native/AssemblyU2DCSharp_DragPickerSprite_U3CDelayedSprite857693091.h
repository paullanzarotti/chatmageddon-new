﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DragPickerSprite
struct DragPickerSprite_t1100164761;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1
struct  U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091  : public Il2CppObject
{
public:
	// System.Single DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::delay
	float ___delay_0;
	// UnityEngine.Vector3 DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::touchLocalPosInCycler
	Vector3_t2243707580  ___touchLocalPosInCycler_1;
	// System.Single DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::<exitDistanceFromCenter>__0
	float ___U3CexitDistanceFromCenterU3E__0_2;
	// System.Int32 DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::<deltaIndex>__1
	int32_t ___U3CdeltaIndexU3E__1_3;
	// System.Int32 DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::<spriteIndex>__2
	int32_t ___U3CspriteIndexU3E__2_4;
	// DragPickerSprite DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::$this
	DragPickerSprite_t1100164761 * ___U24this_5;
	// System.Object DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_touchLocalPosInCycler_1() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___touchLocalPosInCycler_1)); }
	inline Vector3_t2243707580  get_touchLocalPosInCycler_1() const { return ___touchLocalPosInCycler_1; }
	inline Vector3_t2243707580 * get_address_of_touchLocalPosInCycler_1() { return &___touchLocalPosInCycler_1; }
	inline void set_touchLocalPosInCycler_1(Vector3_t2243707580  value)
	{
		___touchLocalPosInCycler_1 = value;
	}

	inline static int32_t get_offset_of_U3CexitDistanceFromCenterU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U3CexitDistanceFromCenterU3E__0_2)); }
	inline float get_U3CexitDistanceFromCenterU3E__0_2() const { return ___U3CexitDistanceFromCenterU3E__0_2; }
	inline float* get_address_of_U3CexitDistanceFromCenterU3E__0_2() { return &___U3CexitDistanceFromCenterU3E__0_2; }
	inline void set_U3CexitDistanceFromCenterU3E__0_2(float value)
	{
		___U3CexitDistanceFromCenterU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaIndexU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U3CdeltaIndexU3E__1_3)); }
	inline int32_t get_U3CdeltaIndexU3E__1_3() const { return ___U3CdeltaIndexU3E__1_3; }
	inline int32_t* get_address_of_U3CdeltaIndexU3E__1_3() { return &___U3CdeltaIndexU3E__1_3; }
	inline void set_U3CdeltaIndexU3E__1_3(int32_t value)
	{
		___U3CdeltaIndexU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CspriteIndexU3E__2_4() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U3CspriteIndexU3E__2_4)); }
	inline int32_t get_U3CspriteIndexU3E__2_4() const { return ___U3CspriteIndexU3E__2_4; }
	inline int32_t* get_address_of_U3CspriteIndexU3E__2_4() { return &___U3CspriteIndexU3E__2_4; }
	inline void set_U3CspriteIndexU3E__2_4(int32_t value)
	{
		___U3CspriteIndexU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U24this_5)); }
	inline DragPickerSprite_t1100164761 * get_U24this_5() const { return ___U24this_5; }
	inline DragPickerSprite_t1100164761 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(DragPickerSprite_t1100164761 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
