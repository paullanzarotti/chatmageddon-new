﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchNavScreen
struct AttackSearchNavScreen_t457319073;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackSearchNavScreen::.ctor()
extern "C"  void AttackSearchNavScreen__ctor_m3268324756 (AttackSearchNavScreen_t457319073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavScreen::Start()
extern "C"  void AttackSearchNavScreen_Start_m4244782012 (AttackSearchNavScreen_t457319073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavScreen::UIClosing()
extern "C"  void AttackSearchNavScreen_UIClosing_m3853835755 (AttackSearchNavScreen_t457319073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackSearchNavScreen_ScreenLoad_m3472276292 (AttackSearchNavScreen_t457319073 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackSearchNavScreen_ScreenUnload_m3125695604 (AttackSearchNavScreen_t457319073 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackSearchNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackSearchNavScreen_ValidateScreenNavigation_m1011241115 (AttackSearchNavScreen_t457319073 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
