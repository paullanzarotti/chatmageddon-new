﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen2202283254MethodDeclarations.h"

// System.Void System.Action`3<System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m27048742(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1845567599 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2414365210_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m576476376(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1845567599 *, bool, List_1_t2207118780 *, String_t*, const MethodInfo*))Action_3_Invoke_m1211332736_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m2348137773(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1845567599 *, bool, List_1_t2207118780 *, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m4234685015_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m3105131658(__this, ___result0, method) ((  void (*) (Action_3_t1845567599 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m695059408_gshared)(__this, ___result0, method)
