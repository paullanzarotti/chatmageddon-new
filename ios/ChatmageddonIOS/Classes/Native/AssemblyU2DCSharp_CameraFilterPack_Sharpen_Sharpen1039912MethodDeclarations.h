﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Sharpen_Sharpen
struct CameraFilterPack_Sharpen_Sharpen_t1039912;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Sharpen_Sharpen::.ctor()
extern "C"  void CameraFilterPack_Sharpen_Sharpen__ctor_m3654153643 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Sharpen_Sharpen::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Sharpen_Sharpen_get_material_m3042739882 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::Start()
extern "C"  void CameraFilterPack_Sharpen_Sharpen_Start_m3299972711 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Sharpen_Sharpen_OnRenderImage_m3925775119 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::OnValidate()
extern "C"  void CameraFilterPack_Sharpen_Sharpen_OnValidate_m4057106896 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::Update()
extern "C"  void CameraFilterPack_Sharpen_Sharpen_Update_m1544347626 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::OnDisable()
extern "C"  void CameraFilterPack_Sharpen_Sharpen_OnDisable_m684157780 (CameraFilterPack_Sharpen_Sharpen_t1039912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
