﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<DefendModalNavigationController>::.ctor()
#define MonoSingleton_1__ctor_m3706138077(__this, method) ((  void (*) (MonoSingleton_1_t222835451 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<DefendModalNavigationController>::Awake()
#define MonoSingleton_1_Awake_m1297177058(__this, method) ((  void (*) (MonoSingleton_1_t222835451 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<DefendModalNavigationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m3817535642(__this /* static, unused */, method) ((  DefendModalNavigationController_t472169731 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<DefendModalNavigationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4280179328(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<DefendModalNavigationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m710088227(__this, method) ((  void (*) (MonoSingleton_1_t222835451 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<DefendModalNavigationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2630125511(__this, method) ((  void (*) (MonoSingleton_1_t222835451 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<DefendModalNavigationController>::.cctor()
#define MonoSingleton_1__cctor_m1202468764(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
