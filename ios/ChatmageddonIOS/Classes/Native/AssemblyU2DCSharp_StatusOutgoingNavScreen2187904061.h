﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// StatusILP
struct StatusILP_t1076408775;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusOutgoingNavScreen
struct  StatusOutgoingNavScreen_t2187904061  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject StatusOutgoingNavScreen::hasOutgoingUI
	GameObject_t1756533147 * ___hasOutgoingUI_3;
	// UnityEngine.GameObject StatusOutgoingNavScreen::noOutgoingUI
	GameObject_t1756533147 * ___noOutgoingUI_4;
	// StatusILP StatusOutgoingNavScreen::outgoingList
	StatusILP_t1076408775 * ___outgoingList_5;
	// System.Boolean StatusOutgoingNavScreen::uiClosing
	bool ___uiClosing_6;

public:
	inline static int32_t get_offset_of_hasOutgoingUI_3() { return static_cast<int32_t>(offsetof(StatusOutgoingNavScreen_t2187904061, ___hasOutgoingUI_3)); }
	inline GameObject_t1756533147 * get_hasOutgoingUI_3() const { return ___hasOutgoingUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasOutgoingUI_3() { return &___hasOutgoingUI_3; }
	inline void set_hasOutgoingUI_3(GameObject_t1756533147 * value)
	{
		___hasOutgoingUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasOutgoingUI_3, value);
	}

	inline static int32_t get_offset_of_noOutgoingUI_4() { return static_cast<int32_t>(offsetof(StatusOutgoingNavScreen_t2187904061, ___noOutgoingUI_4)); }
	inline GameObject_t1756533147 * get_noOutgoingUI_4() const { return ___noOutgoingUI_4; }
	inline GameObject_t1756533147 ** get_address_of_noOutgoingUI_4() { return &___noOutgoingUI_4; }
	inline void set_noOutgoingUI_4(GameObject_t1756533147 * value)
	{
		___noOutgoingUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___noOutgoingUI_4, value);
	}

	inline static int32_t get_offset_of_outgoingList_5() { return static_cast<int32_t>(offsetof(StatusOutgoingNavScreen_t2187904061, ___outgoingList_5)); }
	inline StatusILP_t1076408775 * get_outgoingList_5() const { return ___outgoingList_5; }
	inline StatusILP_t1076408775 ** get_address_of_outgoingList_5() { return &___outgoingList_5; }
	inline void set_outgoingList_5(StatusILP_t1076408775 * value)
	{
		___outgoingList_5 = value;
		Il2CppCodeGenWriteBarrier(&___outgoingList_5, value);
	}

	inline static int32_t get_offset_of_uiClosing_6() { return static_cast<int32_t>(offsetof(StatusOutgoingNavScreen_t2187904061, ___uiClosing_6)); }
	inline bool get_uiClosing_6() const { return ___uiClosing_6; }
	inline bool* get_address_of_uiClosing_6() { return &___uiClosing_6; }
	inline void set_uiClosing_6(bool value)
	{
		___uiClosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
