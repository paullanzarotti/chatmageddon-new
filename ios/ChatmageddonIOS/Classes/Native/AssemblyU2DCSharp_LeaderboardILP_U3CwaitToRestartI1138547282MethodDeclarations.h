﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardILP/<waitToRestartIL>c__Iterator0
struct U3CwaitToRestartILU3Ec__Iterator0_t1138547282;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardILP/<waitToRestartIL>c__Iterator0::.ctor()
extern "C"  void U3CwaitToRestartILU3Ec__Iterator0__ctor_m3722473473 (U3CwaitToRestartILU3Ec__Iterator0_t1138547282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LeaderboardILP/<waitToRestartIL>c__Iterator0::MoveNext()
extern "C"  bool U3CwaitToRestartILU3Ec__Iterator0_MoveNext_m3607435051 (U3CwaitToRestartILU3Ec__Iterator0_t1138547282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LeaderboardILP/<waitToRestartIL>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitToRestartILU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1878466507 (U3CwaitToRestartILU3Ec__Iterator0_t1138547282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LeaderboardILP/<waitToRestartIL>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitToRestartILU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2142851731 (U3CwaitToRestartILU3Ec__Iterator0_t1138547282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP/<waitToRestartIL>c__Iterator0::Dispose()
extern "C"  void U3CwaitToRestartILU3Ec__Iterator0_Dispose_m2689441996 (U3CwaitToRestartILU3Ec__Iterator0_t1138547282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP/<waitToRestartIL>c__Iterator0::Reset()
extern "C"  void U3CwaitToRestartILU3Ec__Iterator0_Reset_m2646625666 (U3CwaitToRestartILU3Ec__Iterator0_t1138547282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
