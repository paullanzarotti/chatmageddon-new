﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3264225795(__this, ___l0, method) ((  void (*) (Enumerator_t3884938852 *, List_1_t55241882 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2716866079(__this, method) ((  void (*) (Enumerator_t3884938852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m646980215(__this, method) ((  Il2CppObject * (*) (Enumerator_t3884938852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::Dispose()
#define Enumerator_Dispose_m181004234(__this, method) ((  void (*) (Enumerator_t3884938852 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::VerifyState()
#define Enumerator_VerifyState_m547574865(__this, method) ((  void (*) (Enumerator_t3884938852 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::MoveNext()
#define Enumerator_MoveNext_m158994155(__this, method) ((  bool (*) (Enumerator_t3884938852 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OnlineMapsFindPlacesResult>::get_Current()
#define Enumerator_get_Current_m4252249902(__this, method) ((  OnlineMapsFindPlacesResult_t686120750 * (*) (Enumerator_t3884938852 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
