﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// PlayerProfileModal
struct PlayerProfileModal_t3260625137;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerProfileModal/<SendFriendRequest>c__AnonStorey0
struct  U3CSendFriendRequestU3Ec__AnonStorey0_t2942902044  : public Il2CppObject
{
public:
	// Friend PlayerProfileModal/<SendFriendRequest>c__AnonStorey0::tempFriend
	Friend_t3555014108 * ___tempFriend_0;
	// PlayerProfileModal PlayerProfileModal/<SendFriendRequest>c__AnonStorey0::$this
	PlayerProfileModal_t3260625137 * ___U24this_1;

public:
	inline static int32_t get_offset_of_tempFriend_0() { return static_cast<int32_t>(offsetof(U3CSendFriendRequestU3Ec__AnonStorey0_t2942902044, ___tempFriend_0)); }
	inline Friend_t3555014108 * get_tempFriend_0() const { return ___tempFriend_0; }
	inline Friend_t3555014108 ** get_address_of_tempFriend_0() { return &___tempFriend_0; }
	inline void set_tempFriend_0(Friend_t3555014108 * value)
	{
		___tempFriend_0 = value;
		Il2CppCodeGenWriteBarrier(&___tempFriend_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSendFriendRequestU3Ec__AnonStorey0_t2942902044, ___U24this_1)); }
	inline PlayerProfileModal_t3260625137 * get_U24this_1() const { return ___U24this_1; }
	inline PlayerProfileModal_t3260625137 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PlayerProfileModal_t3260625137 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
