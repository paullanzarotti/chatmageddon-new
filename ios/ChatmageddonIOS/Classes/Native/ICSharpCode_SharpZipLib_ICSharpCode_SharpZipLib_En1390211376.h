﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t3255436806;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t2148616552;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct  ZipAESStream_t1390211376  : public CryptoStream_t3531341937
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_stream
	Stream_t3255436806 * ____stream_17;
	// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_transform
	ZipAESTransform_t2148616552 * ____transform_18;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBuffer
	ByteU5BU5D_t3397334013* ____slideBuffer_19;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufStartPos
	int32_t ____slideBufStartPos_20;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufFreePos
	int32_t ____slideBufFreePos_21;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_blockAndAuth
	int32_t ____blockAndAuth_22;

public:
	inline static int32_t get_offset_of__stream_17() { return static_cast<int32_t>(offsetof(ZipAESStream_t1390211376, ____stream_17)); }
	inline Stream_t3255436806 * get__stream_17() const { return ____stream_17; }
	inline Stream_t3255436806 ** get_address_of__stream_17() { return &____stream_17; }
	inline void set__stream_17(Stream_t3255436806 * value)
	{
		____stream_17 = value;
		Il2CppCodeGenWriteBarrier(&____stream_17, value);
	}

	inline static int32_t get_offset_of__transform_18() { return static_cast<int32_t>(offsetof(ZipAESStream_t1390211376, ____transform_18)); }
	inline ZipAESTransform_t2148616552 * get__transform_18() const { return ____transform_18; }
	inline ZipAESTransform_t2148616552 ** get_address_of__transform_18() { return &____transform_18; }
	inline void set__transform_18(ZipAESTransform_t2148616552 * value)
	{
		____transform_18 = value;
		Il2CppCodeGenWriteBarrier(&____transform_18, value);
	}

	inline static int32_t get_offset_of__slideBuffer_19() { return static_cast<int32_t>(offsetof(ZipAESStream_t1390211376, ____slideBuffer_19)); }
	inline ByteU5BU5D_t3397334013* get__slideBuffer_19() const { return ____slideBuffer_19; }
	inline ByteU5BU5D_t3397334013** get_address_of__slideBuffer_19() { return &____slideBuffer_19; }
	inline void set__slideBuffer_19(ByteU5BU5D_t3397334013* value)
	{
		____slideBuffer_19 = value;
		Il2CppCodeGenWriteBarrier(&____slideBuffer_19, value);
	}

	inline static int32_t get_offset_of__slideBufStartPos_20() { return static_cast<int32_t>(offsetof(ZipAESStream_t1390211376, ____slideBufStartPos_20)); }
	inline int32_t get__slideBufStartPos_20() const { return ____slideBufStartPos_20; }
	inline int32_t* get_address_of__slideBufStartPos_20() { return &____slideBufStartPos_20; }
	inline void set__slideBufStartPos_20(int32_t value)
	{
		____slideBufStartPos_20 = value;
	}

	inline static int32_t get_offset_of__slideBufFreePos_21() { return static_cast<int32_t>(offsetof(ZipAESStream_t1390211376, ____slideBufFreePos_21)); }
	inline int32_t get__slideBufFreePos_21() const { return ____slideBufFreePos_21; }
	inline int32_t* get_address_of__slideBufFreePos_21() { return &____slideBufFreePos_21; }
	inline void set__slideBufFreePos_21(int32_t value)
	{
		____slideBufFreePos_21 = value;
	}

	inline static int32_t get_offset_of__blockAndAuth_22() { return static_cast<int32_t>(offsetof(ZipAESStream_t1390211376, ____blockAndAuth_22)); }
	inline int32_t get__blockAndAuth_22() const { return ____blockAndAuth_22; }
	inline int32_t* get_address_of__blockAndAuth_22() { return &____blockAndAuth_22; }
	inline void set__blockAndAuth_22(int32_t value)
	{
		____blockAndAuth_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
