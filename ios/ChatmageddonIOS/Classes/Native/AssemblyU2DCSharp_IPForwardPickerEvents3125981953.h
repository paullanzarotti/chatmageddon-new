﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPPickerBase
struct IPPickerBase_t4159478266;
// IPForwardPickerEvents/GameObjectAndMessage
struct GameObjectAndMessage_t1917906877;
// IPCycler
struct IPCycler_t1336138445;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPForwardPickerEvents
struct  IPForwardPickerEvents_t3125981953  : public MonoBehaviour_t1158329972
{
public:
	// IPPickerBase IPForwardPickerEvents::observedPicker
	IPPickerBase_t4159478266 * ___observedPicker_2;
	// System.Boolean IPForwardPickerEvents::notifyOnSelectionChange
	bool ___notifyOnSelectionChange_3;
	// IPForwardPickerEvents/GameObjectAndMessage IPForwardPickerEvents::onSelectionChangeNotification
	GameObjectAndMessage_t1917906877 * ___onSelectionChangeNotification_4;
	// System.Boolean IPForwardPickerEvents::notifyOnSelectionStarted
	bool ___notifyOnSelectionStarted_5;
	// IPForwardPickerEvents/GameObjectAndMessage IPForwardPickerEvents::onStartedNotification
	GameObjectAndMessage_t1917906877 * ___onStartedNotification_6;
	// System.Boolean IPForwardPickerEvents::notifyOnCenterOnChildStarted
	bool ___notifyOnCenterOnChildStarted_7;
	// IPForwardPickerEvents/GameObjectAndMessage IPForwardPickerEvents::onCenterOnChildNotification
	GameObjectAndMessage_t1917906877 * ___onCenterOnChildNotification_8;
	// System.Boolean IPForwardPickerEvents::notifyOnPickerStopped
	bool ___notifyOnPickerStopped_9;
	// IPForwardPickerEvents/GameObjectAndMessage IPForwardPickerEvents::onStoppedNotification
	GameObjectAndMessage_t1917906877 * ___onStoppedNotification_10;
	// System.Boolean IPForwardPickerEvents::notifyOnDragExit
	bool ___notifyOnDragExit_11;
	// IPForwardPickerEvents/GameObjectAndMessage IPForwardPickerEvents::onExitNotification
	GameObjectAndMessage_t1917906877 * ___onExitNotification_12;
	// IPCycler IPForwardPickerEvents::_cycler
	IPCycler_t1336138445 * ____cycler_13;

public:
	inline static int32_t get_offset_of_observedPicker_2() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___observedPicker_2)); }
	inline IPPickerBase_t4159478266 * get_observedPicker_2() const { return ___observedPicker_2; }
	inline IPPickerBase_t4159478266 ** get_address_of_observedPicker_2() { return &___observedPicker_2; }
	inline void set_observedPicker_2(IPPickerBase_t4159478266 * value)
	{
		___observedPicker_2 = value;
		Il2CppCodeGenWriteBarrier(&___observedPicker_2, value);
	}

	inline static int32_t get_offset_of_notifyOnSelectionChange_3() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___notifyOnSelectionChange_3)); }
	inline bool get_notifyOnSelectionChange_3() const { return ___notifyOnSelectionChange_3; }
	inline bool* get_address_of_notifyOnSelectionChange_3() { return &___notifyOnSelectionChange_3; }
	inline void set_notifyOnSelectionChange_3(bool value)
	{
		___notifyOnSelectionChange_3 = value;
	}

	inline static int32_t get_offset_of_onSelectionChangeNotification_4() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___onSelectionChangeNotification_4)); }
	inline GameObjectAndMessage_t1917906877 * get_onSelectionChangeNotification_4() const { return ___onSelectionChangeNotification_4; }
	inline GameObjectAndMessage_t1917906877 ** get_address_of_onSelectionChangeNotification_4() { return &___onSelectionChangeNotification_4; }
	inline void set_onSelectionChangeNotification_4(GameObjectAndMessage_t1917906877 * value)
	{
		___onSelectionChangeNotification_4 = value;
		Il2CppCodeGenWriteBarrier(&___onSelectionChangeNotification_4, value);
	}

	inline static int32_t get_offset_of_notifyOnSelectionStarted_5() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___notifyOnSelectionStarted_5)); }
	inline bool get_notifyOnSelectionStarted_5() const { return ___notifyOnSelectionStarted_5; }
	inline bool* get_address_of_notifyOnSelectionStarted_5() { return &___notifyOnSelectionStarted_5; }
	inline void set_notifyOnSelectionStarted_5(bool value)
	{
		___notifyOnSelectionStarted_5 = value;
	}

	inline static int32_t get_offset_of_onStartedNotification_6() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___onStartedNotification_6)); }
	inline GameObjectAndMessage_t1917906877 * get_onStartedNotification_6() const { return ___onStartedNotification_6; }
	inline GameObjectAndMessage_t1917906877 ** get_address_of_onStartedNotification_6() { return &___onStartedNotification_6; }
	inline void set_onStartedNotification_6(GameObjectAndMessage_t1917906877 * value)
	{
		___onStartedNotification_6 = value;
		Il2CppCodeGenWriteBarrier(&___onStartedNotification_6, value);
	}

	inline static int32_t get_offset_of_notifyOnCenterOnChildStarted_7() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___notifyOnCenterOnChildStarted_7)); }
	inline bool get_notifyOnCenterOnChildStarted_7() const { return ___notifyOnCenterOnChildStarted_7; }
	inline bool* get_address_of_notifyOnCenterOnChildStarted_7() { return &___notifyOnCenterOnChildStarted_7; }
	inline void set_notifyOnCenterOnChildStarted_7(bool value)
	{
		___notifyOnCenterOnChildStarted_7 = value;
	}

	inline static int32_t get_offset_of_onCenterOnChildNotification_8() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___onCenterOnChildNotification_8)); }
	inline GameObjectAndMessage_t1917906877 * get_onCenterOnChildNotification_8() const { return ___onCenterOnChildNotification_8; }
	inline GameObjectAndMessage_t1917906877 ** get_address_of_onCenterOnChildNotification_8() { return &___onCenterOnChildNotification_8; }
	inline void set_onCenterOnChildNotification_8(GameObjectAndMessage_t1917906877 * value)
	{
		___onCenterOnChildNotification_8 = value;
		Il2CppCodeGenWriteBarrier(&___onCenterOnChildNotification_8, value);
	}

	inline static int32_t get_offset_of_notifyOnPickerStopped_9() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___notifyOnPickerStopped_9)); }
	inline bool get_notifyOnPickerStopped_9() const { return ___notifyOnPickerStopped_9; }
	inline bool* get_address_of_notifyOnPickerStopped_9() { return &___notifyOnPickerStopped_9; }
	inline void set_notifyOnPickerStopped_9(bool value)
	{
		___notifyOnPickerStopped_9 = value;
	}

	inline static int32_t get_offset_of_onStoppedNotification_10() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___onStoppedNotification_10)); }
	inline GameObjectAndMessage_t1917906877 * get_onStoppedNotification_10() const { return ___onStoppedNotification_10; }
	inline GameObjectAndMessage_t1917906877 ** get_address_of_onStoppedNotification_10() { return &___onStoppedNotification_10; }
	inline void set_onStoppedNotification_10(GameObjectAndMessage_t1917906877 * value)
	{
		___onStoppedNotification_10 = value;
		Il2CppCodeGenWriteBarrier(&___onStoppedNotification_10, value);
	}

	inline static int32_t get_offset_of_notifyOnDragExit_11() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___notifyOnDragExit_11)); }
	inline bool get_notifyOnDragExit_11() const { return ___notifyOnDragExit_11; }
	inline bool* get_address_of_notifyOnDragExit_11() { return &___notifyOnDragExit_11; }
	inline void set_notifyOnDragExit_11(bool value)
	{
		___notifyOnDragExit_11 = value;
	}

	inline static int32_t get_offset_of_onExitNotification_12() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ___onExitNotification_12)); }
	inline GameObjectAndMessage_t1917906877 * get_onExitNotification_12() const { return ___onExitNotification_12; }
	inline GameObjectAndMessage_t1917906877 ** get_address_of_onExitNotification_12() { return &___onExitNotification_12; }
	inline void set_onExitNotification_12(GameObjectAndMessage_t1917906877 * value)
	{
		___onExitNotification_12 = value;
		Il2CppCodeGenWriteBarrier(&___onExitNotification_12, value);
	}

	inline static int32_t get_offset_of__cycler_13() { return static_cast<int32_t>(offsetof(IPForwardPickerEvents_t3125981953, ____cycler_13)); }
	inline IPCycler_t1336138445 * get__cycler_13() const { return ____cycler_13; }
	inline IPCycler_t1336138445 ** get_address_of__cycler_13() { return &____cycler_13; }
	inline void set__cycler_13(IPCycler_t1336138445 * value)
	{
		____cycler_13 = value;
		Il2CppCodeGenWriteBarrier(&____cycler_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
