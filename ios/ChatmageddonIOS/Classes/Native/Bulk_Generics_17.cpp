﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>
struct U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>
struct U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618;
// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_t1516769741;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t4014198702;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4014198703;
// System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>
struct U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t930005165;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870;
// System.Linq.Enumerable/<Min>c__AnonStorey2E`1<System.Object>
struct U3CMinU3Ec__AnonStorey2E_1_t1495211345;
// System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>
struct U3CMinU3Ec__AnonStorey2E_1_t1049469630;
// System.Linq.OrderedEnumerable`1<System.Object>
struct OrderedEnumerable_1_t4214082274;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Linq.OrderedSequence`2<System.Object,System.DateTime>
struct OrderedSequence_2_t331544481;
// System.Func`2<System.Object,System.DateTime>
struct Func_2_t829260555;
// System.Collections.Generic.IComparer`1<System.DateTime>
struct IComparer_1_t2942636087;
// System.Linq.SortContext`1<System.Object>
struct SortContext_1_t1798778454;
// System.Linq.OrderedSequence`2<System.Object,System.Int32>
struct OrderedSequence_2_t1710216260;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t26340570;
// System.Linq.OrderedSequence`2<System.Object,System.Object>
struct OrderedSequence_2_t2327788107;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.Linq.OrderedSequence`2<System.Object,System.Single>
struct OrderedSequence_2_t1714848744;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2212564818;
// System.Collections.Generic.IComparer`1<System.Single>
struct IComparer_1_t30973054;
// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>
struct U3CSortU3Ec__Iterator21_t2488605192;
// System.Linq.QuickSort`1<System.Object>
struct QuickSort_1_t1970792956;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Linq.SortSequenceContext`2<System.Object,System.DateTime>
struct SortSequenceContext_2_t1065051447;
// System.Linq.SortSequenceContext`2<System.Object,System.Int32>
struct SortSequenceContext_2_t2443723226;
// System.Linq.SortSequenceContext`2<System.Object,System.Object>
struct SortSequenceContext_2_t3061295073;
// System.Linq.SortSequenceContext`2<System.Object,System.Single>
struct SortSequenceContext_2_t2448355710;
// System.String
struct String_t;
// System.Predicate`1<AttackHomeNav>
struct Predicate_1_t2387860619;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Predicate`1<AttackNavScreen>
struct Predicate_1_t2774696870;
// System.Predicate`1<AttackSearchNav>
struct Predicate_1_t2700854752;
// System.Predicate`1<ChatNavScreen>
struct Predicate_1_t2780314672;
// System.Predicate`1<DefendNavScreen>
struct Predicate_1_t2281589818;
// System.Predicate`1<FriendHomeNavScreen>
struct Predicate_1_t42008411;
// System.Predicate`1<FriendNavScreen>
struct Predicate_1_t618353568;
// System.Predicate`1<FriendSearchNavScreen>
struct Predicate_1_t1461359278;
// System.Predicate`1<LeaderboardNavScreen>
struct Predicate_1_t799013827;
// System.Predicate`1<LitJson.PropertyMetadata>
struct Predicate_1_t2136796251;
// System.Predicate`1<PhoneNumberNavScreen>
struct Predicate_1_t4001404661;
// System.Predicate`1<PlayerProfileNavScreen>
struct Predicate_1_t1103638374;
// System.Predicate`1<ProfileNavScreen>
struct Predicate_1_t3864595265;
// System.Predicate`1<RegNavScreen>
struct Predicate_1_t3405110770;
// System.Predicate`1<SettingsNavScreen>
struct Predicate_1_t710097349;
// System.Predicate`1<StatusNavScreen>
struct Predicate_1_t1315339198;
// System.Predicate`1<System.Byte>
struct Predicate_1_t2126074551;
// System.Predicate`1<System.Char>
struct Predicate_1_t1897451453;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t2776792056;
// System.Predicate`1<System.Int16>
struct Predicate_1_t2484216029;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2832094954;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t4236135325;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateConcat3066740841.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateConcat3066740841MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateOfType4154598408.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateOfType4154598408MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect2075803647.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect2075803647MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "System_Core_System_Func_2_gen3234924829.h"
#include "System_Core_System_Func_2_gen3234924829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI464576659.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI464576659MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1623697841.h"
#include "System_Core_System_Func_2_gen1623697841MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3018179618.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3018179618MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "System_Core_System_Func_2_gen4177300800.h"
#include "System_Core_System_Func_2_gen4177300800MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect1666382999.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect1666382999MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2825504181.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3073286210.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3073286210MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "System_Core_System_Func_2_gen4232407392.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "System_Core_System_Func_2_gen4232407392MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3073286211.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3073286211MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4232407393.h"
#include "System_Core_System_Func_2_gen4232407393MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSkipIt3116556992.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSkipIt3116556992MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereIt591803617.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereIt591803617MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "System_Core_System_Func_2_gen1675079469.h"
#include "System_Core_System_Func_2_gen1675079469MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI3504167023.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI3504167023MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3377395111.h"
#include "System_Core_System_Func_2_gen3377395111MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CMinU3Ec__Ano1495211345.h"
#include "System_Core_System_Linq_Enumerable_U3CMinU3Ec__Ano1495211345MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2212564818.h"
#include "System_Core_System_Func_2_gen2212564818MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CMinU3Ec__Ano1049469630.h"
#include "System_Core_System_Linq_Enumerable_U3CMinU3Ec__Ano1049469630MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4065209745.h"
#include "System_Core_System_Func_2_gen4065209745MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedEnumerable_1_gen4214082274.h"
#include "System_Core_System_Linq_OrderedEnumerable_1_gen4214082274MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen331544481.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen331544481MethodDeclarations.h"
#include "System_Core_System_Linq_SortDirection759359329.h"
#include "System_Core_System_Func_2_gen829260555.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084.h"
#include "System_Core_System_Linq_SortContext_1_gen1798778454.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen1065051447.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen1065051447MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_gen1970792956MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1710216260.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1710216260MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2207932334.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2443723226.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2443723226MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen2327788107.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen2327788107MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen3061295073.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen3061295073MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1714848744.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1714848744MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen966519051MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen966519051.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2448355710.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2448355710MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I2488605192.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I2488605192MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_gen1970792956.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Linq_SortContext_1_gen1798778454MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "System_Core_System_Func_2_gen829260555MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2207932334MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1447017461.h"
#include "mscorlib_System_Nullable_1_gen1447017461MethodDeclarations.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_ValueType3507792607MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen130574217.h"
#include "mscorlib_System_Nullable_1_gen130574217MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookUnityPlatfor1867507902.h"
#include "mscorlib_System_Nullable_1_gen241159723.h"
#include "mscorlib_System_Nullable_1_gen241159723MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_OGActionType1978093408.h"
#include "mscorlib_System_Nullable_1_gen1881137068.h"
#include "mscorlib_System_Nullable_1_gen1881137068MethodDeclarations.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen2088641033MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1717547653.h"
#include "mscorlib_System_Nullable_1_gen1717547653MethodDeclarations.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3251239280MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3282734688.h"
#include "mscorlib_System_Nullable_1_gen3282734688MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "mscorlib_System_Nullable_1_gen2341081996MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen334943763MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"
#include "mscorlib_System_Nullable_1_gen3467111648MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"
#include "mscorlib_System_Nullable_1_gen339576247MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1693325264.h"
#include "mscorlib_System_Nullable_1_gen1693325264MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2387860619.h"
#include "mscorlib_System_Predicate_1_gen2387860619MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Predicate_1_gen2774696870.h"
#include "mscorlib_System_Predicate_1_gen2774696870MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "mscorlib_System_Predicate_1_gen2700854752.h"
#include "mscorlib_System_Predicate_1_gen2700854752MethodDeclarations.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "mscorlib_System_Predicate_1_gen2780314672.h"
#include "mscorlib_System_Predicate_1_gen2780314672MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "mscorlib_System_Predicate_1_gen2281589818.h"
#include "mscorlib_System_Predicate_1_gen2281589818MethodDeclarations.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "mscorlib_System_Predicate_1_gen42008411.h"
#include "mscorlib_System_Predicate_1_gen42008411MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "mscorlib_System_Predicate_1_gen618353568.h"
#include "mscorlib_System_Predicate_1_gen618353568MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "mscorlib_System_Predicate_1_gen1461359278.h"
#include "mscorlib_System_Predicate_1_gen1461359278MethodDeclarations.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "mscorlib_System_Predicate_1_gen799013827.h"
#include "mscorlib_System_Predicate_1_gen799013827MethodDeclarations.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "mscorlib_System_Predicate_1_gen2136796251.h"
#include "mscorlib_System_Predicate_1_gen2136796251MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_System_Predicate_1_gen4001404661.h"
#include "mscorlib_System_Predicate_1_gen4001404661MethodDeclarations.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "mscorlib_System_Predicate_1_gen1103638374.h"
#include "mscorlib_System_Predicate_1_gen1103638374MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "mscorlib_System_Predicate_1_gen3864595265.h"
#include "mscorlib_System_Predicate_1_gen3864595265MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "mscorlib_System_Predicate_1_gen3405110770.h"
#include "mscorlib_System_Predicate_1_gen3405110770MethodDeclarations.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "mscorlib_System_Predicate_1_gen710097349.h"
#include "mscorlib_System_Predicate_1_gen710097349MethodDeclarations.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "mscorlib_System_Predicate_1_gen1315339198.h"
#include "mscorlib_System_Predicate_1_gen1315339198MethodDeclarations.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "mscorlib_System_Predicate_1_gen2126074551.h"
#include "mscorlib_System_Predicate_1_gen2126074551MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1897451453.h"
#include "mscorlib_System_Predicate_1_gen1897451453MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2776792056.h"
#include "mscorlib_System_Predicate_1_gen2776792056MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Predicate_1_gen2484216029.h"
#include "mscorlib_System_Predicate_1_gen2484216029MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen514847563MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2832094954.h"
#include "mscorlib_System_Predicate_1_gen2832094954MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Predicate_1_gen4236135325.h"
#include "mscorlib_System_Predicate_1_gen4236135325MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"

// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m349912619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m349912619(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m349912619_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::.ctor()
extern "C"  void U3CCreateConcatIteratorU3Ec__Iterator1_1__ctor_m2209438476_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3078800097_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1072057552_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerable_GetEnumerator_m601628501_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1102237490_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * L_2 = (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *)L_2;
		U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Efirst_8();
		NullCheck(L_3);
		L_3->set_first_0(L_4);
		U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esecond_9();
		NullCheck(L_5);
		L_5->set_second_3(L_6);
		U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateConcatIteratorU3Ec__Iterator1_1_MoveNext_m1871966524_MetadataUsageId;
extern "C"  bool U3CCreateConcatIteratorU3Ec__Iterator1_1_MoveNext_m1871966524_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateConcatIteratorU3Ec__Iterator1_1_MoveNext_m1871966524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0027;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_00b9;
			}
		}
	}
	{
		goto IL_012a;
	}

IL_0027:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_first_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_50U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0077;
				}
			}
		}

IL_0047:
		{
			goto IL_0077;
		}

IL_004c:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_50U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_7(L_7);
			__this->set_U24PC_6(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12C, FINALLY_008c);
		}

IL_0077:
		{
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24s_50U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_004c;
			}
		}

IL_0087:
		{
			IL2CPP_LEAVE(0xA5, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0090;
			}
		}

IL_008f:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0090:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_50U3E__0_1();
			if (L_11)
			{
				goto IL_0099;
			}
		}

IL_0098:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0099:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_50U3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(140)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a5:
	{
		Il2CppObject* L_13 = (Il2CppObject*)__this->get_second_3();
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_13);
		__this->set_U3CU24s_51U3E__2_4(L_14);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_00b9:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_15 = V_0;
			switch (((int32_t)((int32_t)L_15-(int32_t)2)))
			{
				case 0:
				{
					goto IL_00f5;
				}
			}
		}

IL_00c5:
		{
			goto IL_00f5;
		}

IL_00ca:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_51U3E__2_4();
			NullCheck((Il2CppObject*)L_16);
			Il2CppObject * L_17 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_16);
			__this->set_U3CelementU3E__3_5(L_17);
			Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3CelementU3E__3_5();
			__this->set_U24current_7(L_18);
			__this->set_U24PC_6(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12C, FINALLY_010a);
		}

IL_00f5:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_51U3E__2_4();
			NullCheck((Il2CppObject *)L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			if (L_20)
			{
				goto IL_00ca;
			}
		}

IL_0105:
		{
			IL2CPP_LEAVE(0x123, FINALLY_010a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_010a;
	}

FINALLY_010a:
	{ // begin finally (depth: 1)
		{
			bool L_21 = V_1;
			if (!L_21)
			{
				goto IL_010e;
			}
		}

IL_010d:
		{
			IL2CPP_END_FINALLY(266)
		}

IL_010e:
		{
			Il2CppObject* L_22 = (Il2CppObject*)__this->get_U3CU24s_51U3E__2_4();
			if (L_22)
			{
				goto IL_0117;
			}
		}

IL_0116:
		{
			IL2CPP_END_FINALLY(266)
		}

IL_0117:
		{
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_51U3E__2_4();
			NullCheck((Il2CppObject *)L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_23);
			IL2CPP_END_FINALLY(266)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(266)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_JUMP_TBL(0x123, IL_0123)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0123:
	{
		__this->set_U24PC_6((-1));
	}

IL_012a:
	{
		return (bool)0;
	}

IL_012c:
	{
		return (bool)1;
	}
	// Dead block : IL_012e: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateConcatIteratorU3Ec__Iterator1_1_Dispose_m4263097911_MetadataUsageId;
extern "C"  void U3CCreateConcatIteratorU3Ec__Iterator1_1_Dispose_m4263097911_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateConcatIteratorU3Ec__Iterator1_1_Dispose_m4263097911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_005e;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_0044;
			}
		}
	}
	{
		goto IL_005e;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_50U3E__0_1();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_50U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		goto IL_005e;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x5E, FINALLY_0049);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_51U3E__2_4();
			if (L_4)
			{
				goto IL_0052;
			}
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(73)
		}

IL_0052:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_51U3E__2_4();
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			IL2CPP_END_FINALLY(73)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateConcatIteratorU3Ec__Iterator1_1_Reset_m2389309517_MetadataUsageId;
extern "C"  void U3CCreateConcatIteratorU3Ec__Iterator1_1_Reset_m2389309517_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateConcatIteratorU3Ec__Iterator1_1_Reset_m2389309517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::.ctor()
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1__ctor_m1442118915_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2657146748_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_IEnumerator_get_Current_m2253659481_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_IEnumerable_GetEnumerator_m3988951508_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2665668891_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_2 = (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)L_2;
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_MetadataUsageId;
extern "C"  bool U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00bf;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_80U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0088;
				}
			}
		}

IL_0043:
		{
			goto IL_0088;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			if (!((Il2CppObject *)IsInst(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
			{
				goto IL_0088;
			}
		}

IL_0069:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC1, FINALLY_009d);
		}

IL_0088:
		{
			Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0048;
			}
		}

IL_0098:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(157)
		}

IL_00a1:
		{
			Il2CppObject * L_12 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_12, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_13 = V_2;
			if (L_13)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(157)
		}

IL_00b1:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(157)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
	// Dead block : IL_00c3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_MetadataUsageId;
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_MetadataUsageId;
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3303917033_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  uint8_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1083457286_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2819521347_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_U24current_5();
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1943430486_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m35409509_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * L_5 = V_0;
		Func_2_t3234924829 * L_6 = (Func_2_t3234924829 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1294791019_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1294791019_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1294791019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_007e;
				}
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3234924829 * L_7 = (Func_2_t3234924829 *)__this->get_selector_3();
			int32_t L_8 = (int32_t)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3234924829 *)L_7);
			uint8_t L_9 = ((  uint8_t (*) (Func_2_t3234924829 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3234924829 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2667867866_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2667867866_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2667867866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1581670096_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1581670096_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1581670096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3475200381_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  int32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3055432360_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1736704983_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m3738367960_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m3728664393_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * L_5 = V_0;
		Func_2_t1623697841 * L_6 = (Func_2_t1623697841 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2856167827_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2856167827_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2856167827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_007e;
				}
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t1623697841 * L_7 = (Func_2_t1623697841 *)__this->get_selector_3();
			int32_t L_8 = (int32_t)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t1623697841 *)L_7);
			int32_t L_9 = ((  int32_t (*) (Func_2_t1623697841 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t1623697841 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3769509896_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3769509896_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3769509896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Int32>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4166950874_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4166950874_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t464576659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4166950874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m689218096_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  int16_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m4047513437_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = (int16_t)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1858536588_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = (int16_t)__this->get_U24current_5();
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2954286825_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m567171742_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * L_5 = V_0;
		Func_2_t4177300800 * L_6 = (Func_2_t4177300800 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3134478936_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3134478936_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3134478936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_007e;
				}
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t4177300800 * L_7 = (Func_2_t4177300800 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t4177300800 *)L_7);
			int16_t L_9 = ((  int16_t (*) (Func_2_t4177300800 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t4177300800 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2091487203_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2091487203_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2091487203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int16>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3265161993_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3265161993_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3018179618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3265161993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2480296441_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1090887326_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2800698283_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m291907278_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m3708752605_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_007e;
				}
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t2825504181 * L_7 = (Func_2_t2825504181 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t2825504181 *)L_7);
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2825504181 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m246445300_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Vector2_t2243707579  U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3897895097_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = (Vector2_t2243707579 )__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1114137638_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = (Vector2_t2243707579 )__this->get_U24current_5();
		Vector2_t2243707579  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2202352717_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m26880472_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * L_5 = V_0;
		Func_2_t4232407392 * L_6 = (Func_2_t4232407392 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3036450880_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3036450880_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3036450880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_007e;
				}
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Vector3_t2243707580  L_6 = InterfaceFuncInvoker0< Vector3_t2243707580  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t4232407392 * L_7 = (Func_2_t4232407392 *)__this->get_selector_3();
			Vector3_t2243707580  L_8 = (Vector3_t2243707580 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t4232407392 *)L_7);
			Vector2_t2243707579  L_9 = ((  Vector2_t2243707579  (*) (Func_2_t4232407392 *, Vector3_t2243707580 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t4232407392 *)L_7, (Vector3_t2243707580 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m784306447_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m784306447_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m784306447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m773198273_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m773198273_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m773198273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3744437497_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Vector3_t2243707580  U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3598759710_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1434257643_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_U24current_5();
		Vector3_t2243707580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2204398158_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1915838749_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * L_5 = V_0;
		Func_2_t4232407393 * L_6 = (Func_2_t4232407393 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2537538495_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2537538495_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2537538495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_007e;
				}
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Vector3_t2243707580  L_6 = InterfaceFuncInvoker0< Vector3_t2243707580  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t4232407393 * L_7 = (Func_2_t4232407393 *)__this->get_selector_3();
			Vector3_t2243707580  L_8 = (Vector3_t2243707580 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t4232407393 *)L_7);
			Vector3_t2243707580  L_9 = ((  Vector3_t2243707580  (*) (Func_2_t4232407393 *, Vector3_t2243707580 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t4232407393 *)L_7, (Vector3_t2243707580 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1758250_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1758250_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1758250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector3>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1027294588_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1027294588_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1027294588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::.ctor()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2739536835_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1340547920_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2607218181_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m1936281608_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m4074570415_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * L_2 = (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)L_2;
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_U3CU24U3Ecount_6();
		NullCheck(L_5);
		L_5->set_count_2(L_6);
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m2317883885_MetadataUsageId;
extern "C"  bool U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m2317883885_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m2317883885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00bf;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CenumeratorU3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0098;
				}
			}
		}

IL_0043:
		{
			goto IL_005d;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_005d;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00ad);
		}

IL_005d:
		{
			int32_t L_7 = (int32_t)__this->get_count_2();
			int32_t L_8 = (int32_t)L_7;
			V_2 = (int32_t)L_8;
			__this->set_count_2(((int32_t)((int32_t)L_8-(int32_t)1)));
			int32_t L_9 = V_2;
			if ((((int32_t)L_9) > ((int32_t)0)))
			{
				goto IL_0048;
			}
		}

IL_0074:
		{
			goto IL_0098;
		}

IL_0079:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject*)L_10);
			Il2CppObject * L_11 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_10);
			__this->set_U24current_4(L_11);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC1, FINALLY_00ad);
		}

IL_0098:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0079;
			}
		}

IL_00a8:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_00ad);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ad;
	}

FINALLY_00ad:
	{ // begin finally (depth: 1)
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(173)
		}

IL_00b1:
		{
			NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)__this);
			((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			IL2CPP_END_FINALLY(173)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(173)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
	// Dead block : IL_00c3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::Dispose()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m3750295900_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002d;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x2D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)__this);
		((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x2D, IL_002d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3328507806_MetadataUsageId;
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3328507806_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3328507806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::<>__Finally0()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1059667724_MetadataUsageId;
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1059667724_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1059667724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3618093320_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppChar U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1333327669_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m4190102536_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_U24current_5();
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3180065609_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2632161126_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * L_5 = V_0;
		Func_2_t1675079469 * L_6 = (Func_2_t1675079469 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m1851388644_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m1851388644_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m1851388644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_110U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0089;
				}
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppChar L_6 = InterfaceFuncInvoker0< Il2CppChar >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t1675079469 * L_7 = (Func_2_t1675079469 *)__this->get_predicate_3();
			Il2CppChar L_8 = (Il2CppChar)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t1675079469 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t1675079469 *, Il2CppChar, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t1675079469 *)L_7, (Il2CppChar)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppChar L_10 = (Il2CppChar)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1718422655_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1718422655_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1718422655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Char>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1005035433_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1005035433_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t591803617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1005035433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m904326150_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  int32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2921628691_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1938324600_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m1874917311_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3932226178_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * L_5 = V_0;
		Func_2_t3377395111 * L_6 = (Func_2_t3377395111 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2577937910_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2577937910_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2577937910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_110U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0089;
				}
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3377395111 * L_7 = (Func_2_t3377395111 *)__this->get_predicate_3();
			int32_t L_8 = (int32_t)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3377395111 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3377395111 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3377395111 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			int32_t L_10 = (int32_t)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m464556617_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m464556617_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m464556617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m4153446011_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m4153446011_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m4153446011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1958283157_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3602665650_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m269113779_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3279674866_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2682676065_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_5 = V_0;
		Func_2_t3961629604 * L_6 = (Func_2_t3961629604 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_110U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0089;
				}
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3961629604 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<Min>c__AnonStorey2E`1<System.Object>::.ctor()
extern "C"  void U3CMinU3Ec__AnonStorey2E_1__ctor_m1436661890_gshared (U3CMinU3Ec__AnonStorey2E_1_t1495211345 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single System.Linq.Enumerable/<Min>c__AnonStorey2E`1<System.Object>::<>m__2F(TSource,System.Single)
extern "C"  float U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m479781021_gshared (U3CMinU3Ec__AnonStorey2E_1_t1495211345 * __this, Il2CppObject * ___a0, float ___b1, const MethodInfo* method)
{
	{
		Func_2_t2212564818 * L_0 = (Func_2_t2212564818 *)__this->get_selector_0();
		Il2CppObject * L_1 = ___a0;
		NullCheck((Func_2_t2212564818 *)L_0);
		float L_2 = ((  float (*) (Func_2_t2212564818 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t2212564818 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		float L_3 = ___b1;
		float L_4 = Math_Min_m105057611(NULL /*static, unused*/, (float)L_2, (float)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CMinU3Ec__AnonStorey2E_1__ctor_m3711901291_gshared (U3CMinU3Ec__AnonStorey2E_1_t1049469630 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>::<>m__2F(TSource,System.Single)
extern "C"  float U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m4227098510_gshared (U3CMinU3Ec__AnonStorey2E_1_t1049469630 * __this, Vector3_t2243707580  ___a0, float ___b1, const MethodInfo* method)
{
	{
		Func_2_t4065209745 * L_0 = (Func_2_t4065209745 *)__this->get_selector_0();
		Vector3_t2243707580  L_1 = ___a0;
		NullCheck((Func_2_t4065209745 *)L_0);
		float L_2 = ((  float (*) (Func_2_t4065209745 *, Vector3_t2243707580 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t4065209745 *)L_0, (Vector3_t2243707580 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		float L_3 = ___b1;
		float L_4 = Math_Min_m105057611(NULL /*static, unused*/, (float)L_2, (float)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Linq.OrderedEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m1650659453_gshared (OrderedEnumerable_1_t4214082274 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		__this->set_source_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m430310697_gshared (OrderedEnumerable_1_t4214082274 * __this, const MethodInfo* method)
{
	{
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (OrderedEnumerable_1_t4214082274 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m2325923724_gshared (OrderedEnumerable_1_t4214082274 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_0();
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		Il2CppObject* L_1 = VirtFuncInvoker1< Il2CppObject*, Il2CppObject* >::Invoke(8 /* System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0);
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.DateTime>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m2131331646_gshared (OrderedSequence_2_t331544481 * __this, Il2CppObject* ___source0, Func_2_t829260555 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t331544481 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t331544481 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		((  void (*) (OrderedEnumerable_1_t4214082274 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t829260555 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t331544481 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t331544481 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		Comparer_1_t3878182084 * L_4 = ((  Comparer_1_t3878182084 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t331544481 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.DateTime>::.ctor(System.Linq.OrderedEnumerable`1<TElement>,System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m3631086545_gshared (OrderedSequence_2_t331544481 * __this, OrderedEnumerable_1_t4214082274 * ___parent0, Il2CppObject* ___source1, Func_2_t829260555 * ___keySelector2, Il2CppObject* ___comparer3, int32_t ___direction4, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source1;
		Func_2_t829260555 * L_1 = ___keySelector2;
		Il2CppObject* L_2 = ___comparer3;
		int32_t L_3 = ___direction4;
		NullCheck((OrderedSequence_2_t331544481 *)__this);
		((  void (*) (OrderedSequence_2_t331544481 *, Il2CppObject*, Func_2_t829260555 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((OrderedSequence_2_t331544481 *)__this, (Il2CppObject*)L_0, (Func_2_t829260555 *)L_1, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		OrderedEnumerable_1_t4214082274 * L_4 = ___parent0;
		__this->set_parent_1(L_4);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.DateTime>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m2667166768_gshared (OrderedSequence_2_t331544481 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method)
{
	SortContext_1_t1798778454 * V_0 = NULL;
	{
		Func_2_t829260555 * L_0 = (Func_2_t829260555 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t1798778454 * L_3 = ___current0;
		SortSequenceContext_2_t1065051447 * L_4 = (SortSequenceContext_2_t1065051447 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (SortSequenceContext_2_t1065051447 *, Func_2_t829260555 *, Il2CppObject*, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_4, (Func_2_t829260555 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (SortContext_1_t1798778454 *)L_4;
		OrderedEnumerable_1_t4214082274 * L_5 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t4214082274 * L_6 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		SortContext_1_t1798778454 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)L_6);
		SortContext_1_t1798778454 * L_8 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)L_6, (SortContext_1_t1798778454 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t1798778454 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.DateTime>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m945510653_gshared (OrderedSequence_2_t331544481 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t331544481 *)__this);
		SortContext_1_t1798778454 * L_1 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.DateTime>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t331544481 *)__this, (SortContext_1_t1798778454 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m1933455561_gshared (OrderedSequence_2_t1710216260 * __this, Il2CppObject* ___source0, Func_2_t2207932334 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t1710216260 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t1710216260 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		((  void (*) (OrderedEnumerable_1_t4214082274 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2207932334 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t1710216260 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t1710216260 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		Comparer_1_t961886567 * L_4 = ((  Comparer_1_t961886567 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t1710216260 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Int32>::.ctor(System.Linq.OrderedEnumerable`1<TElement>,System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m230019908_gshared (OrderedSequence_2_t1710216260 * __this, OrderedEnumerable_1_t4214082274 * ___parent0, Il2CppObject* ___source1, Func_2_t2207932334 * ___keySelector2, Il2CppObject* ___comparer3, int32_t ___direction4, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source1;
		Func_2_t2207932334 * L_1 = ___keySelector2;
		Il2CppObject* L_2 = ___comparer3;
		int32_t L_3 = ___direction4;
		NullCheck((OrderedSequence_2_t1710216260 *)__this);
		((  void (*) (OrderedSequence_2_t1710216260 *, Il2CppObject*, Func_2_t2207932334 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((OrderedSequence_2_t1710216260 *)__this, (Il2CppObject*)L_0, (Func_2_t2207932334 *)L_1, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		OrderedEnumerable_1_t4214082274 * L_4 = ___parent0;
		__this->set_parent_1(L_4);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m2905460903_gshared (OrderedSequence_2_t1710216260 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method)
{
	SortContext_1_t1798778454 * V_0 = NULL;
	{
		Func_2_t2207932334 * L_0 = (Func_2_t2207932334 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t1798778454 * L_3 = ___current0;
		SortSequenceContext_2_t2443723226 * L_4 = (SortSequenceContext_2_t2443723226 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (SortSequenceContext_2_t2443723226 *, Func_2_t2207932334 *, Il2CppObject*, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_4, (Func_2_t2207932334 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (SortContext_1_t1798778454 *)L_4;
		OrderedEnumerable_1_t4214082274 * L_5 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t4214082274 * L_6 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		SortContext_1_t1798778454 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)L_6);
		SortContext_1_t1798778454 * L_8 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)L_6, (SortContext_1_t1798778454 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t1798778454 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m3623126160_gshared (OrderedSequence_2_t1710216260 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t1710216260 *)__this);
		SortContext_1_t1798778454 * L_1 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t1710216260 *)__this, (SortContext_1_t1798778454 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m1061657384_gshared (OrderedSequence_2_t2327788107 * __this, Il2CppObject* ___source0, Func_2_t2825504181 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t2327788107 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t2327788107 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		((  void (*) (OrderedEnumerable_1_t4214082274 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2825504181 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t2327788107 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t2327788107 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		Comparer_1_t1579458414 * L_4 = ((  Comparer_1_t1579458414 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t2327788107 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Object>::.ctor(System.Linq.OrderedEnumerable`1<TElement>,System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m4156317981_gshared (OrderedSequence_2_t2327788107 * __this, OrderedEnumerable_1_t4214082274 * ___parent0, Il2CppObject* ___source1, Func_2_t2825504181 * ___keySelector2, Il2CppObject* ___comparer3, int32_t ___direction4, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source1;
		Func_2_t2825504181 * L_1 = ___keySelector2;
		Il2CppObject* L_2 = ___comparer3;
		int32_t L_3 = ___direction4;
		NullCheck((OrderedSequence_2_t2327788107 *)__this);
		((  void (*) (OrderedSequence_2_t2327788107 *, Il2CppObject*, Func_2_t2825504181 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((OrderedSequence_2_t2327788107 *)__this, (Il2CppObject*)L_0, (Func_2_t2825504181 *)L_1, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		OrderedEnumerable_1_t4214082274 * L_4 = ___parent0;
		__this->set_parent_1(L_4);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m3921228830_gshared (OrderedSequence_2_t2327788107 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method)
{
	SortContext_1_t1798778454 * V_0 = NULL;
	{
		Func_2_t2825504181 * L_0 = (Func_2_t2825504181 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t1798778454 * L_3 = ___current0;
		SortSequenceContext_2_t3061295073 * L_4 = (SortSequenceContext_2_t3061295073 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (SortSequenceContext_2_t3061295073 *, Func_2_t2825504181 *, Il2CppObject*, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_4, (Func_2_t2825504181 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (SortContext_1_t1798778454 *)L_4;
		OrderedEnumerable_1_t4214082274 * L_5 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t4214082274 * L_6 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		SortContext_1_t1798778454 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)L_6);
		SortContext_1_t1798778454 * L_8 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)L_6, (SortContext_1_t1798778454 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t1798778454 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m3566252129_gshared (OrderedSequence_2_t2327788107 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t2327788107 *)__this);
		SortContext_1_t1798778454 * L_1 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t2327788107 *)__this, (SortContext_1_t1798778454 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m3682274073_gshared (OrderedSequence_2_t1714848744 * __this, Il2CppObject* ___source0, Func_2_t2212564818 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t1714848744 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t1714848744 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		((  void (*) (OrderedEnumerable_1_t4214082274 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2212564818 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t1714848744 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t1714848744 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		Comparer_1_t966519051 * L_4 = ((  Comparer_1_t966519051 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t1714848744 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Single>::.ctor(System.Linq.OrderedEnumerable`1<TElement>,System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m2277884556_gshared (OrderedSequence_2_t1714848744 * __this, OrderedEnumerable_1_t4214082274 * ___parent0, Il2CppObject* ___source1, Func_2_t2212564818 * ___keySelector2, Il2CppObject* ___comparer3, int32_t ___direction4, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source1;
		Func_2_t2212564818 * L_1 = ___keySelector2;
		Il2CppObject* L_2 = ___comparer3;
		int32_t L_3 = ___direction4;
		NullCheck((OrderedSequence_2_t1714848744 *)__this);
		((  void (*) (OrderedSequence_2_t1714848744 *, Il2CppObject*, Func_2_t2212564818 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((OrderedSequence_2_t1714848744 *)__this, (Il2CppObject*)L_0, (Func_2_t2212564818 *)L_1, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		OrderedEnumerable_1_t4214082274 * L_4 = ___parent0;
		__this->set_parent_1(L_4);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Single>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m1462603899_gshared (OrderedSequence_2_t1714848744 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method)
{
	SortContext_1_t1798778454 * V_0 = NULL;
	{
		Func_2_t2212564818 * L_0 = (Func_2_t2212564818 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t1798778454 * L_3 = ___current0;
		SortSequenceContext_2_t2448355710 * L_4 = (SortSequenceContext_2_t2448355710 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (SortSequenceContext_2_t2448355710 *, Func_2_t2212564818 *, Il2CppObject*, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_4, (Func_2_t2212564818 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (SortContext_1_t1798778454 *)L_4;
		OrderedEnumerable_1_t4214082274 * L_5 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t4214082274 * L_6 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		SortContext_1_t1798778454 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)L_6);
		SortContext_1_t1798778454 * L_8 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)L_6, (SortContext_1_t1798778454 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t1798778454 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Single>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m2292292704_gshared (OrderedSequence_2_t1714848744 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t1714848744 *)__this);
		SortContext_1_t1798778454 * L_1 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(7 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Single>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t1714848744 *)__this, (SortContext_1_t1798778454 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m435832983_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m3627878530_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m4063555545_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m2220308456_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CSortU3Ec__Iterator21_t2488605192 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CSortU3Ec__Iterator21_t2488605192 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m2832696871_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t2488605192 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CSortU3Ec__Iterator21_t2488605192 * L_2 = (U3CSortU3Ec__Iterator21_t2488605192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_2;
		U3CSortU3Ec__Iterator21_t2488605192 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CSortU3Ec__Iterator21_t2488605192 * L_5 = V_0;
		SortContext_1_t1798778454 * L_6 = (SortContext_1_t1798778454 *)__this->get_U3CU24U3Econtext_7();
		NullCheck(L_5);
		L_5->set_context_1(L_6);
		U3CSortU3Ec__Iterator21_t2488605192 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m2095803797_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0083;
			}
		}
	}
	{
		goto IL_00b0;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		SortContext_1_t1798778454 * L_3 = (SortContext_1_t1798778454 *)__this->get_context_1();
		QuickSort_1_t1970792956 * L_4 = (QuickSort_1_t1970792956 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (QuickSort_1_t1970792956 *, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_4, (Il2CppObject*)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_U3CsorterU3E__0_2(L_4);
		QuickSort_1_t1970792956 * L_5 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck((QuickSort_1_t1970792956 *)L_5);
		((  void (*) (QuickSort_1_t1970792956 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set_U3CiU3E__1_3(0);
		goto IL_0091;
	}

IL_004f:
	{
		QuickSort_1_t1970792956 * L_6 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_6);
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)L_6->get_elements_0();
		QuickSort_1_t1970792956 * L_8 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_indexes_1();
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__1_3();
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		__this->set_U24current_5(L_14);
		__this->set_U24PC_4(1);
		goto IL_00b2;
	}

IL_0083:
	{
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__1_3();
		__this->set_U3CiU3E__1_3(((int32_t)((int32_t)L_15+(int32_t)1)));
	}

IL_0091:
	{
		int32_t L_16 = (int32_t)__this->get_U3CiU3E__1_3();
		QuickSort_1_t1970792956 * L_17 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_17);
		Int32U5BU5D_t3030399641* L_18 = (Int32U5BU5D_t3030399641*)L_17->get_indexes_1();
		NullCheck(L_18);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00b0:
	{
		return (bool)0;
	}

IL_00b2:
	{
		return (bool)1;
	}
	// Dead block : IL_00b4: ldloc.1
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m2211739520_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSortU3Ec__Iterator21_Reset_m2399833958_MetadataUsageId;
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m2399833958_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSortU3Ec__Iterator21_Reset_m2399833958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m1650062348_gshared (QuickSort_1_t1970792956 * __this, Il2CppObject* ___source0, SortContext_1_t1798778454 * ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		ObjectU5BU5D_t3614634134* L_1 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_elements_0(L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_2);
		Int32U5BU5D_t3030399641* L_3 = ((  Int32U5BU5D_t3030399641* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_indexes_1(L_3);
		SortContext_1_t1798778454 * L_4 = ___context1;
		__this->set_context_2(L_4);
		return;
	}
}
// System.Int32[] System.Linq.QuickSort`1<System.Object>::CreateIndexes(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t QuickSort_1_CreateIndexes_m2577858579_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* QuickSort_1_CreateIndexes_m2577858579_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuickSort_1_CreateIndexes_m2577858579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_0016;
	}

IL_000e:
	{
		Int32U5BU5D_t3030399641* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (int32_t)L_3);
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___length0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_7 = V_0;
		return L_7;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m3295377581_gshared (QuickSort_1_t1970792956 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) > ((int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)__this->get_context_2();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		NullCheck(L_3);
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m2598468721_gshared (QuickSort_1_t1970792956 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)__this->get_context_2();
		int32_t L_1 = ___first_index0;
		int32_t L_2 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_0);
		int32_t L_3 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_0, (int32_t)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m968647497_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))/(int32_t)2));
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = ___left0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_10 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_5, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_11 = ___left0;
		int32_t L_12 = V_0;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_002a:
	{
		Int32U5BU5D_t3030399641* L_13 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_14 = ___right1;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		Int32U5BU5D_t3030399641* L_17 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_18 = ___left0;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_21 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_16, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_22 = ___left0;
		int32_t L_23 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_004e:
	{
		Int32U5BU5D_t3030399641* L_24 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_25 = ___right1;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		int32_t L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		Int32U5BU5D_t3030399641* L_28 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_29 = V_0;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		int32_t L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_32 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_27, (int32_t)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_33 = V_0;
		int32_t L_34 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_33, (int32_t)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0072:
	{
		int32_t L_35 = V_0;
		int32_t L_36 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_35, (int32_t)((int32_t)((int32_t)L_36-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Int32U5BU5D_t3030399641* L_37 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_38 = ___right1;
		NullCheck(L_37);
		int32_t L_39 = ((int32_t)((int32_t)L_38-(int32_t)1));
		int32_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		return L_40;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m700141710_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)3))) > ((int32_t)L_1)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = ___left0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___right1;
		V_1 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		int32_t L_4 = ___left0;
		int32_t L_5 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_6 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_2 = (int32_t)L_6;
	}

IL_0018:
	{
		goto IL_001d;
	}

IL_001d:
	{
		Int32U5BU5D_t3030399641* L_7 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_0 = (int32_t)L_9;
		NullCheck(L_7);
		int32_t L_10 = L_9;
		int32_t L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = V_2;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_13 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		goto IL_003b;
	}

IL_003b:
	{
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_15 = V_1;
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15-(int32_t)1));
		V_1 = (int32_t)L_16;
		NullCheck(L_14);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = V_2;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_20 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_18, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_20) > ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		if ((((int32_t)L_21) >= ((int32_t)L_22)))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_006d;
	}

IL_0068:
	{
		goto IL_0072;
	}

IL_006d:
	{
		goto IL_0018;
	}

IL_0072:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_25, (int32_t)((int32_t)((int32_t)L_26-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_27 = ___left0;
		int32_t L_28 = V_0;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_27, (int32_t)((int32_t)((int32_t)L_28-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_29 = V_0;
		int32_t L_30 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)((int32_t)((int32_t)L_29+(int32_t)1)), (int32_t)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		goto IL_009d;
	}

IL_0095:
	{
		int32_t L_31 = ___left0;
		int32_t L_32 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_009d:
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m3575279495_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)1));
		goto IL_005a;
	}

IL_0009:
	{
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)L_4;
		int32_t L_5 = V_0;
		V_1 = (int32_t)L_5;
		goto IL_002f;
	}

IL_0019:
	{
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = V_1;
		Int32U5BU5D_t3030399641* L_8 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)L_9-(int32_t)1));
		int32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (int32_t)L_11);
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_002f:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = ___left0;
		if ((((int32_t)L_13) <= ((int32_t)L_14)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_15 = V_2;
		Int32U5BU5D_t3030399641* L_16 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		int32_t L_18 = ((int32_t)((int32_t)L_17-(int32_t)1));
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_20 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_15, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_20) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}

IL_004d:
	{
		Int32U5BU5D_t3030399641* L_21 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (int32_t)L_23);
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = ___right1;
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m1740429939_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_1 = ___right1;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = (int32_t)L_3;
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_5 = ___right1;
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = ___left0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_9);
		Int32U5BU5D_t3030399641* L_10 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_11 = ___left0;
		int32_t L_12 = V_0;
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int32_t)L_12);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m2490553768_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t1798778454 * ___context1, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t2488605192 * V_0 = NULL;
	{
		U3CSortU3Ec__Iterator21_t2488605192 * L_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_0;
		U3CSortU3Ec__Iterator21_t2488605192 * L_1 = V_0;
		Il2CppObject* L_2 = ___source0;
		NullCheck(L_1);
		L_1->set_source_0(L_2);
		U3CSortU3Ec__Iterator21_t2488605192 * L_3 = V_0;
		SortContext_1_t1798778454 * L_4 = ___context1;
		NullCheck(L_3);
		L_3->set_context_1(L_4);
		U3CSortU3Ec__Iterator21_t2488605192 * L_5 = V_0;
		Il2CppObject* L_6 = ___source0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Esource_6(L_6);
		U3CSortU3Ec__Iterator21_t2488605192 * L_7 = V_0;
		SortContext_1_t1798778454 * L_8 = ___context1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Econtext_7(L_8);
		U3CSortU3Ec__Iterator21_t2488605192 * L_9 = V_0;
		U3CSortU3Ec__Iterator21_t2488605192 * L_10 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Void System.Linq.SortContext`1<System.Object>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m1159198615_gshared (SortContext_1_t1798778454 * __this, int32_t ___direction0, SortContext_1_t1798778454 * ___child_context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___direction0;
		__this->set_direction_0(L_0);
		SortContext_1_t1798778454 * L_1 = ___child_context1;
		__this->set_child_context_1(L_1);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.DateTime>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m2847481508_gshared (SortSequenceContext_2_t1065051447 * __this, Func_2_t829260555 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t829260555 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.DateTime>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2969874778_gshared (SortSequenceContext_2_t1065051447 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((DateTimeU5BU5D_t3111277864*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		DateTimeU5BU5D_t3111277864* L_4 = (DateTimeU5BU5D_t3111277864*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t829260555 * L_6 = (Func_2_t829260555 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t829260555 *)L_6);
		DateTime_t693205669  L_11 = ((  DateTime_t693205669  (*) (Func_2_t829260555 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t829260555 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DateTime_t693205669 )L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		DateTimeU5BU5D_t3111277864* L_14 = (DateTimeU5BU5D_t3111277864*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.DateTime>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m3739660795_gshared (SortSequenceContext_2_t1065051447 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		DateTimeU5BU5D_t3111277864* L_1 = (DateTimeU5BU5D_t3111277864*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		DateTime_t693205669  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		DateTimeU5BU5D_t3111277864* L_5 = (DateTimeU5BU5D_t3111277864*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		DateTime_t693205669  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, DateTime_t693205669 , DateTime_t693205669  >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.DateTime>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (DateTime_t693205669 )L_4, (DateTime_t693205669 )L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Int32>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m1776296909_gshared (SortSequenceContext_2_t2443723226 * __this, Func_2_t2207932334 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2207932334 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Int32>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m3278535173_gshared (SortSequenceContext_2_t2443723226 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2207932334 * L_6 = (Func_2_t2207932334 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2207932334 *)L_6);
		int32_t L_11 = ((  int32_t (*) (Func_2_t2207932334 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2207932334 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Int32>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m2394750062_gshared (SortSequenceContext_2_t2443723226 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_4, (int32_t)L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m2181983522_gshared (SortSequenceContext_2_t3061295073 * __this, Func_2_t2825504181 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2825504181 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2437603572_gshared (SortSequenceContext_2_t3061295073 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2825504181 *)L_6);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2825504181 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Object>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m259092091_gshared (SortSequenceContext_2_t3061295073 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_4, (Il2CppObject *)L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Single>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m517350249_gshared (SortSequenceContext_2_t2448355710 * __this, Func_2_t2212564818 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2212564818 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Single>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2035707041_gshared (SortSequenceContext_2_t2448355710 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((SingleU5BU5D_t577127397*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		SingleU5BU5D_t577127397* L_4 = (SingleU5BU5D_t577127397*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2212564818 * L_6 = (Func_2_t2212564818 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2212564818 *)L_6);
		float L_11 = ((  float (*) (Func_2_t2212564818 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2212564818 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (float)L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		SingleU5BU5D_t577127397* L_14 = (SingleU5BU5D_t577127397*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Single>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m658074834_gshared (SortSequenceContext_2_t2448355710 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		SingleU5BU5D_t577127397* L_1 = (SingleU5BU5D_t577127397*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		float L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		SingleU5BU5D_t577127397* L_5 = (SingleU5BU5D_t577127397*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		float L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (float)L_4, (float)L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Nullable`1<Audience>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2954006471_gshared (Nullable_1_t1447017461 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2954006471_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2954006471(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Audience>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m960751206_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m960751206_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m960751206(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Audience>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1353274088_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1353274088_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1353274088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m1353274088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m1353274088(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Audience>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1014408754_gshared (Nullable_1_t1447017461 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1447017461 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m325067495((Nullable_1_t1447017461 *)__this, (Nullable_1_t1447017461 )((*(Nullable_1_t1447017461 *)((Nullable_1_t1447017461 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1014408754_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1014408754(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Audience>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m325067495_gshared (Nullable_1_t1447017461 * __this, Nullable_1_t1447017461  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m325067495_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1447017461  ___other0, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m325067495(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Audience>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m771049174_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m771049174_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m771049174(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Audience>::GetValueOrDefault()
extern Il2CppClass* Audience_t3183951146_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1912957201_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1912957201_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1912957201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Audience_t3183951146_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1912957201_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1912957201(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Audience>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3601254404_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3601254404_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3601254404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3601254404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1447017461  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3601254404(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1387002517_gshared (Nullable_1_t130574217 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1387002517_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1387002517(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m511325576_gshared (Nullable_1_t130574217 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m511325576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m511325576(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m691755378_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m691755378_gshared (Nullable_1_t130574217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m691755378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m691755378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m691755378(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4083728267_gshared (Nullable_1_t130574217 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t130574217 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m85428064((Nullable_1_t130574217 *)__this, (Nullable_1_t130574217 )((*(Nullable_1_t130574217 *)((Nullable_1_t130574217 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m4083728267_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4083728267(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m85428064_gshared (Nullable_1_t130574217 * __this, Nullable_1_t130574217  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m85428064_AdjustorThunk (Il2CppObject * __this, Nullable_1_t130574217  ___other0, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m85428064(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m732374613_gshared (Nullable_1_t130574217 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m732374613_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m732374613(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::GetValueOrDefault()
extern Il2CppClass* FacebookUnityPlatform_t1867507902_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m337650842_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m337650842_gshared (Nullable_1_t130574217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m337650842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (FacebookUnityPlatform_t1867507902_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m337650842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m337650842(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m226866533_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m226866533_gshared (Nullable_1_t130574217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m226866533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m226866533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t130574217  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m226866533(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Facebook.Unity.OGActionType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2425814835_gshared (Nullable_1_t241159723 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2425814835_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2425814835(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3034365232_gshared (Nullable_1_t241159723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3034365232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3034365232(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Facebook.Unity.OGActionType>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1809768199_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1809768199_gshared (Nullable_1_t241159723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1809768199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m1809768199_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m1809768199(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m331648137_gshared (Nullable_1_t241159723 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t241159723 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m450646124((Nullable_1_t241159723 *)__this, (Nullable_1_t241159723 )((*(Nullable_1_t241159723 *)((Nullable_1_t241159723 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m331648137_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m331648137(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m450646124_gshared (Nullable_1_t241159723 * __this, Nullable_1_t241159723  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m450646124_AdjustorThunk (Il2CppObject * __this, Nullable_1_t241159723  ___other0, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m450646124(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Facebook.Unity.OGActionType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2056989743_gshared (Nullable_1_t241159723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2056989743_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2056989743(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Facebook.Unity.OGActionType>::GetValueOrDefault()
extern Il2CppClass* OGActionType_t1978093408_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3888875471_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3888875471_gshared (Nullable_1_t241159723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3888875471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3888875471_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3888875471(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Facebook.Unity.OGActionType>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1104328003_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1104328003_gshared (Nullable_1_t241159723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1104328003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1104328003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t241159723  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1104328003(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Gender>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1929226934_gshared (Nullable_1_t1881137068 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1929226934_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1929226934(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Gender>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m730290061_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m730290061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m730290061(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Gender>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3168451933_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3168451933_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3168451933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m3168451933_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m3168451933(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Gender>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m958922863_gshared (Nullable_1_t1881137068 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1881137068 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3627789222((Nullable_1_t1881137068 *)__this, (Nullable_1_t1881137068 )((*(Nullable_1_t1881137068 *)((Nullable_1_t1881137068 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m958922863_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m958922863(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Gender>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3627789222_gshared (Nullable_1_t1881137068 * __this, Nullable_1_t1881137068  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3627789222_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1881137068  ___other0, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3627789222(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Gender>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3942747889_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3942747889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3942747889(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Gender>::GetValueOrDefault()
extern Il2CppClass* Gender_t3618070753_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2573044256_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2573044256_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2573044256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Gender_t3618070753_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2573044256_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m2573044256(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Gender>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2830019457_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2830019457_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2830019457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2830019457_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881137068  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2830019457(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3575886808_gshared (Nullable_1_t2088641033 * __this, bool ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		bool L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3575886808_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3575886808(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4031632872_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4031632872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4031632872(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m764562218_MetadataUsageId;
extern "C"  bool Nullable_1_get_Value_m764562218_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m764562218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_value_0();
		return L_2;
	}
}
extern "C"  bool Nullable_1_get_Value_m764562218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_Value_m764562218(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1318699267_gshared (Nullable_1_t2088641033 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2088641033 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2189684888((Nullable_1_t2088641033 *)__this, (Nullable_1_t2088641033 )((*(Nullable_1_t2088641033 *)((Nullable_1_t2088641033 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1318699267_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1318699267(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2189684888_gshared (Nullable_1_t2088641033 * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		bool* L_3 = (bool*)(&___other0)->get_address_of_value_0();
		bool L_4 = (bool)__this->get_value_0();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m2118901528((bool*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2189684888_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2189684888(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		int32_t L_2 = Boolean_GetHashCode_m1894638460((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1645245653(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2478758066_MetadataUsageId;
extern "C"  bool Nullable_1_GetValueOrDefault_m2478758066_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2478758066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool G_B3_0 = false;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  bool Nullable_1_GetValueOrDefault_m2478758066_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_GetValueOrDefault_m2478758066(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Boolean>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m678068069_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m678068069_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m678068069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		String_t* L_2 = Boolean_ToString_m1253164328((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m678068069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m678068069(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Char>::.ctor(T)
extern "C"  void Nullable_1__ctor_m45911704_gshared (Nullable_1_t1717547653 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Il2CppChar L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m45911704_AdjustorThunk (Il2CppObject * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m45911704(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Char>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2585673045_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2585673045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2585673045(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Char>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3846225905_MetadataUsageId;
extern "C"  Il2CppChar Nullable_1_get_Value_m3846225905_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3846225905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppChar L_2 = (Il2CppChar)__this->get_value_0();
		return L_2;
	}
}
extern "C"  Il2CppChar Nullable_1_get_Value_m3846225905_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Il2CppChar _returnValue = Nullable_1_get_Value_m3846225905(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Char>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m848199703_gshared (Nullable_1_t1717547653 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1717547653 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1944572282((Nullable_1_t1717547653 *)__this, (Nullable_1_t1717547653 )((*(Nullable_1_t1717547653 *)((Nullable_1_t1717547653 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m848199703_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m848199703(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Char>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1944572282_gshared (Nullable_1_t1717547653 * __this, Nullable_1_t1717547653  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Il2CppChar* L_3 = (Il2CppChar*)(&___other0)->get_address_of_value_0();
		Il2CppChar L_4 = (Il2CppChar)__this->get_value_0();
		Il2CppChar L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Char_Equals_m1374454116((Il2CppChar*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1944572282_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1717547653  ___other0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1944572282(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Char>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4003870389_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppChar* L_1 = (Il2CppChar*)__this->get_address_of_value_0();
		int32_t L_2 = Char_GetHashCode_m2343577184((Il2CppChar*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m4003870389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m4003870389(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Char>::GetValueOrDefault()
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1517005582_MetadataUsageId;
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m1517005582_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1517005582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	Il2CppChar G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppChar L_1 = (Il2CppChar)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Char_t3454481338_il2cpp_TypeInfo_var, (&V_0));
		Il2CppChar L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m1517005582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Il2CppChar _returnValue = Nullable_1_GetValueOrDefault_m1517005582(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Char>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2704273093_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2704273093_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2704273093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppChar* L_1 = (Il2CppChar*)__this->get_address_of_value_0();
		String_t* L_2 = Char_ToString_m1976753030((Il2CppChar*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2704273093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2704273093(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1729195354_gshared (Nullable_1_t3251239280 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTime_t693205669  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1729195354_AdjustorThunk (Il2CppObject * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1729195354(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2343917509_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2343917509_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2343917509(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3578777665_MetadataUsageId;
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m3578777665_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3578777665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTime_t693205669  L_2 = (DateTime_t693205669 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m3578777665_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t693205669  _returnValue = Nullable_1_get_Value_m3578777665(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1089953100_gshared (Nullable_1_t3251239280 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3251239280 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1817623273((Nullable_1_t3251239280 *)__this, (Nullable_1_t3251239280 )((*(Nullable_1_t3251239280 *)((Nullable_1_t3251239280 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1089953100_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1089953100(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1817623273_gshared (Nullable_1_t3251239280 * __this, Nullable_1_t3251239280  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTime_t693205669 * L_3 = (DateTime_t693205669 *)(&___other0)->get_address_of_value_0();
		DateTime_t693205669  L_4 = (DateTime_t693205669 )__this->get_value_0();
		DateTime_t693205669  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m2562884703((DateTime_t693205669 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1817623273_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3251239280  ___other0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1817623273(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTime_t693205669 * L_1 = (DateTime_t693205669 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTime_GetHashCode_m974799321((DateTime_t693205669 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3047479588(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::GetValueOrDefault()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m706662709_MetadataUsageId;
extern "C"  DateTime_t693205669  Nullable_1_GetValueOrDefault_m706662709_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m706662709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTime_t693205669  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTime_t693205669  L_1 = (DateTime_t693205669 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTime_t693205669_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t693205669  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  DateTime_t693205669  Nullable_1_GetValueOrDefault_m706662709_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t693205669  _returnValue = Nullable_1_GetValueOrDefault_m706662709(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.DateTime>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1419821888_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1419821888_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1419821888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTime_t693205669 * L_1 = (DateTime_t693205669 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTime_ToString_m1117481977((DateTime_t693205669 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1419821888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1419821888(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Decimal>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3611054853_gshared (Nullable_1_t3282734688 * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Decimal_t724701077  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3611054853_AdjustorThunk (Il2CppObject * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3611054853(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Decimal>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1981010305_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1981010305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1981010305(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m477494014_MetadataUsageId;
extern "C"  Decimal_t724701077  Nullable_1_get_Value_m477494014_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m477494014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Decimal_t724701077  L_2 = (Decimal_t724701077 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Decimal_t724701077  Nullable_1_get_Value_m477494014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t724701077  _returnValue = Nullable_1_get_Value_m477494014(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Decimal>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2494697376_gshared (Nullable_1_t3282734688 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3282734688 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3951113015((Nullable_1_t3282734688 *)__this, (Nullable_1_t3282734688 )((*(Nullable_1_t3282734688 *)((Nullable_1_t3282734688 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2494697376_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2494697376(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Decimal>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3951113015_gshared (Nullable_1_t3282734688 * __this, Nullable_1_t3282734688  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Decimal_t724701077 * L_3 = (Decimal_t724701077 *)(&___other0)->get_address_of_value_0();
		Decimal_t724701077  L_4 = (Decimal_t724701077 )__this->get_value_0();
		Decimal_t724701077  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Decimal_Equals_m1798565377((Decimal_t724701077 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3951113015_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3282734688  ___other0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3951113015(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Decimal>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m387122928_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Decimal_t724701077 * L_1 = (Decimal_t724701077 *)__this->get_address_of_value_0();
		int32_t L_2 = Decimal_GetHashCode_m703641627((Decimal_t724701077 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m387122928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m387122928(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::GetValueOrDefault()
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1335859897_MetadataUsageId;
extern "C"  Decimal_t724701077  Nullable_1_GetValueOrDefault_m1335859897_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1335859897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Decimal_t724701077  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Decimal_t724701077  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Decimal_t724701077  L_1 = (Decimal_t724701077 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Decimal_t724701077_il2cpp_TypeInfo_var, (&V_0));
		Decimal_t724701077  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Decimal_t724701077  Nullable_1_GetValueOrDefault_m1335859897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t724701077  _returnValue = Nullable_1_GetValueOrDefault_m1335859897(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Decimal>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m810070050_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m810070050_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m810070050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Decimal_t724701077 * L_1 = (Decimal_t724701077 *)__this->get_address_of_value_0();
		String_t* L_2 = Decimal_ToString_m759431975((Decimal_t724701077 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m810070050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m810070050(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Double>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3378545483_gshared (Nullable_1_t2341081996 * __this, double ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		double L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3378545483_AdjustorThunk (Il2CppObject * __this, double ___value0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3378545483(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Double>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1493650327_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1493650327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1493650327(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m113566496_MetadataUsageId;
extern "C"  double Nullable_1_get_Value_m113566496_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m113566496_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		double L_2 = (double)__this->get_value_0();
		return L_2;
	}
}
extern "C"  double Nullable_1_get_Value_m113566496_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_get_Value_m113566496(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m646915120_gshared (Nullable_1_t2341081996 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2341081996 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3211299085((Nullable_1_t2341081996 *)__this, (Nullable_1_t2341081996 )((*(Nullable_1_t2341081996 *)((Nullable_1_t2341081996 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m646915120_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m646915120(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3211299085_gshared (Nullable_1_t2341081996 * __this, Nullable_1_t2341081996  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		double* L_3 = (double*)(&___other0)->get_address_of_value_0();
		double L_4 = (double)__this->get_value_0();
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Double_Equals_m2792387643((double*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3211299085_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2341081996  ___other0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3211299085(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Double>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m810535872_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		int32_t L_2 = Double_GetHashCode_m3403732029((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m810535872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m810535872(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::GetValueOrDefault()
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m100080307_MetadataUsageId;
extern "C"  double Nullable_1_GetValueOrDefault_m100080307_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m100080307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double G_B3_0 = 0.0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		double L_1 = (double)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Double_t4078015681_il2cpp_TypeInfo_var, (&V_0));
		double L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  double Nullable_1_GetValueOrDefault_m100080307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_GetValueOrDefault_m100080307(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Double>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3380318548_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3380318548_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3380318548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		String_t* L_2 = Double_ToString_m1864290157((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3380318548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3380318548(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m255206972_gshared (Nullable_1_t334943763 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m255206972_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m255206972(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2294837272_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2294837272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2294837272(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3695076182_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3695076182_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3695076182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m3695076182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m3695076182(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2848647165_gshared (Nullable_1_t334943763 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t334943763 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1118562548((Nullable_1_t334943763 *)__this, (Nullable_1_t334943763 )((*(Nullable_1_t334943763 *)((Nullable_1_t334943763 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2848647165_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2848647165(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1118562548_gshared (Nullable_1_t334943763 * __this, Nullable_1_t334943763  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m753832628((int32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1118562548_AdjustorThunk (Il2CppObject * __this, Nullable_1_t334943763  ___other0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1118562548(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int32_GetHashCode_m1381647448((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1859855859(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::GetValueOrDefault()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3117741790_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3117741790_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3117741790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3117741790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3117741790(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2285560203_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2285560203_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2285560203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int32_ToString_m2960866144((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2285560203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2285560203(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3843433798_gshared (Nullable_1_t3467111648 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3843433798_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3843433798(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m594774853_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m594774853_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m594774853(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2017516221_MetadataUsageId;
extern "C"  int64_t Nullable_1_get_Value_m2017516221_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2017516221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int64_t L_2 = (int64_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int64_t Nullable_1_get_Value_m2017516221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_get_Value_m2017516221(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4207337156_gshared (Nullable_1_t3467111648 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3467111648 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3974016973((Nullable_1_t3467111648 *)__this, (Nullable_1_t3467111648 )((*(Nullable_1_t3467111648 *)((Nullable_1_t3467111648 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m4207337156_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4207337156(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3974016973_gshared (Nullable_1_t3467111648 * __this, Nullable_1_t3467111648  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int64_t* L_3 = (int64_t*)(&___other0)->get_address_of_value_0();
		int64_t L_4 = (int64_t)__this->get_value_0();
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int64_Equals_m3608806223((int64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3974016973_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3467111648  ___other0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3974016973(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2841619984_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int64_GetHashCode_m4047499913((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2841619984_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2841619984(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::GetValueOrDefault()
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1774609286_MetadataUsageId;
extern "C"  int64_t Nullable_1_GetValueOrDefault_m1774609286_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1774609286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	int64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = (int64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int64_t909078037_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int64_t Nullable_1_GetValueOrDefault_m1774609286_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_GetValueOrDefault_m1774609286(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3201666422_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3201666422_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3201666422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int64_ToString_m689375889((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3201666422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3201666422(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1978009879_gshared (Nullable_1_t339576247 * __this, float ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		float L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1978009879_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1978009879(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3526318906_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3526318906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3526318906(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m672124572_MetadataUsageId;
extern "C"  float Nullable_1_get_Value_m672124572_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m672124572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		float L_2 = (float)__this->get_value_0();
		return L_2;
	}
}
extern "C"  float Nullable_1_get_Value_m672124572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_get_Value_m672124572(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1038420037_gshared (Nullable_1_t339576247 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t339576247 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3850685758((Nullable_1_t339576247 *)__this, (Nullable_1_t339576247 )((*(Nullable_1_t339576247 *)((Nullable_1_t339576247 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1038420037_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1038420037(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3850685758_gshared (Nullable_1_t339576247 * __this, Nullable_1_t339576247  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		float* L_3 = (float*)(&___other0)->get_address_of_value_0();
		float L_4 = (float)__this->get_value_0();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m3679433096((float*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3850685758_AdjustorThunk (Il2CppObject * __this, Nullable_1_t339576247  ___other0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3850685758(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		int32_t L_2 = Single_GetHashCode_m3102305584((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m572762171(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3764440182_MetadataUsageId;
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3764440182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_GetValueOrDefault_m3764440182(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Single>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m940266439_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m940266439_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m940266439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		String_t* L_2 = Single_ToString_m1813392066((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m940266439_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m940266439(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t3430258949  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m796575255_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m796575255(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3663286555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3663286555(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1743067844_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1743067844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = (TimeSpan_t3430258949 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_get_Value_m1743067844(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1693325264 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1889119397((Nullable_1_t1693325264 *)__this, (Nullable_1_t1693325264 )((*(Nullable_1_t1693325264 *)((Nullable_1_t1693325264 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3860982732_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3860982732(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t3430258949 * L_3 = (TimeSpan_t3430258949 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t3430258949  L_4 = (TimeSpan_t3430258949 )__this->get_value_0();
		TimeSpan_t3430258949  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m4102942751((TimeSpan_t3430258949 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1889119397_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1889119397(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1791015856(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m714352289_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m714352289_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m714352289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t3430258949  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t3430258949  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t3430258949  L_1 = (TimeSpan_t3430258949 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TimeSpan_t3430258949_il2cpp_TypeInfo_var, (&V_0));
		TimeSpan_t3430258949  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m714352289_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_GetValueOrDefault_m714352289(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1238126148_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1238126148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2947282901((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1238126148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1238126148(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<AttackHomeNav>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3276601035_gshared (Predicate_1_t2387860619 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<AttackHomeNav>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m824436387_gshared (Predicate_1_t2387860619 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m824436387((Predicate_1_t2387860619 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<AttackHomeNav>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttackHomeNav_t3944890504_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3541587728_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3541587728_gshared (Predicate_1_t2387860619 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3541587728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttackHomeNav_t3944890504_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<AttackHomeNav>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4241925385_gshared (Predicate_1_t2387860619 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<AttackNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1978137768_gshared (Predicate_1_t2774696870 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<AttackNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2734998080_gshared (Predicate_1_t2774696870 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2734998080((Predicate_1_t2774696870 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<AttackNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttackNavScreen_t36759459_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2462486915_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2462486915_gshared (Predicate_1_t2774696870 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2462486915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttackNavScreen_t36759459_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<AttackNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4045278586_gshared (Predicate_1_t2774696870 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<AttackSearchNav>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1742413688_gshared (Predicate_1_t2700854752 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<AttackSearchNav>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m575632244_gshared (Predicate_1_t2700854752 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m575632244((Predicate_1_t2700854752 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<AttackSearchNav>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AttackSearchNav_t4257884637_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2247654653_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2247654653_gshared (Predicate_1_t2700854752 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2247654653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AttackSearchNav_t4257884637_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<AttackSearchNav>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m168914034_gshared (Predicate_1_t2700854752 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<ChatNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3189408934_gshared (Predicate_1_t2780314672 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<ChatNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3483587738_gshared (Predicate_1_t2780314672 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3483587738((Predicate_1_t2780314672 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<ChatNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ChatNavScreen_t42377261_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1625364621_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1625364621_gshared (Predicate_1_t2780314672 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1625364621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ChatNavScreen_t42377261_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<ChatNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2234517248_gshared (Predicate_1_t2780314672 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<DefendNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1502660980_gshared (Predicate_1_t2281589818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<DefendNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3710371204_gshared (Predicate_1_t2281589818 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3710371204((Predicate_1_t2281589818 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<DefendNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* DefendNavScreen_t3838619703_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4114797979_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4114797979_gshared (Predicate_1_t2281589818 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4114797979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DefendNavScreen_t3838619703_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<DefendNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2806685474_gshared (Predicate_1_t2281589818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<FriendHomeNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2720924637_gshared (Predicate_1_t42008411 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<FriendHomeNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3190083657_gshared (Predicate_1_t42008411 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3190083657((Predicate_1_t42008411 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<FriendHomeNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* FriendHomeNavScreen_t1599038296_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3807181476_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3807181476_gshared (Predicate_1_t42008411 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3807181476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FriendHomeNavScreen_t1599038296_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<FriendHomeNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2502368807_gshared (Predicate_1_t42008411 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<FriendNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1511528208_gshared (Predicate_1_t618353568 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<FriendNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3848451588_gshared (Predicate_1_t618353568 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3848451588((Predicate_1_t618353568 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<FriendNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* FriendNavScreen_t2175383453_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2799115625_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2799115625_gshared (Predicate_1_t618353568 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2799115625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FriendNavScreen_t2175383453_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<FriendNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3005768582_gshared (Predicate_1_t618353568 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<FriendSearchNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4030503752_gshared (Predicate_1_t1461359278 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<FriendSearchNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1842741424_gshared (Predicate_1_t1461359278 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1842741424((Predicate_1_t1461359278 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<FriendSearchNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* FriendSearchNavScreen_t3018389163_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1396856911_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1396856911_gshared (Predicate_1_t1461359278 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1396856911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FriendSearchNavScreen_t3018389163_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<FriendSearchNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m396358326_gshared (Predicate_1_t1461359278 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<LeaderboardNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m48366045_gshared (Predicate_1_t799013827 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<LeaderboardNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m755118513_gshared (Predicate_1_t799013827 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m755118513((Predicate_1_t799013827 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<LeaderboardNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* LeaderboardNavScreen_t2356043712_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4124968808_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4124968808_gshared (Predicate_1_t799013827 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4124968808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(LeaderboardNavScreen_t2356043712_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<LeaderboardNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3549831751_gshared (Predicate_1_t799013827 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<LitJson.PropertyMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m392394250_gshared (Predicate_1_t2136796251 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<LitJson.PropertyMetadata>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3182349066_gshared (Predicate_1_t2136796251 * __this, PropertyMetadata_t3693826136  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3182349066((Predicate_1_t2136796251 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, PropertyMetadata_t3693826136  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, PropertyMetadata_t3693826136  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<LitJson.PropertyMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t3693826136_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2570142987_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2570142987_gshared (Predicate_1_t2136796251 * __this, PropertyMetadata_t3693826136  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2570142987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PropertyMetadata_t3693826136_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<LitJson.PropertyMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3627768672_gshared (Predicate_1_t2136796251 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<PhoneNumberNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1710141615_gshared (Predicate_1_t4001404661 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<PhoneNumberNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1094882871_gshared (Predicate_1_t4001404661 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1094882871((Predicate_1_t4001404661 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<PhoneNumberNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PhoneNumberNavScreen_t1263467250_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m803892654_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m803892654_gshared (Predicate_1_t4001404661 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m803892654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PhoneNumberNavScreen_t1263467250_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<PhoneNumberNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2969333365_gshared (Predicate_1_t4001404661 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<PlayerProfileNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1629582666_gshared (Predicate_1_t1103638374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<PlayerProfileNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1313941130_gshared (Predicate_1_t1103638374 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1313941130((Predicate_1_t1103638374 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<PlayerProfileNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PlayerProfileNavScreen_t2660668259_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1793081995_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1793081995_gshared (Predicate_1_t1103638374 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1793081995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PlayerProfileNavScreen_t2660668259_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<PlayerProfileNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1395197664_gshared (Predicate_1_t1103638374 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<ProfileNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2812863939_gshared (Predicate_1_t3864595265 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<ProfileNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2767454667_gshared (Predicate_1_t3864595265 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2767454667((Predicate_1_t3864595265 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<ProfileNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ProfileNavScreen_t1126657854_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2063117698_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2063117698_gshared (Predicate_1_t3864595265 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2063117698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ProfileNavScreen_t1126657854_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<ProfileNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3638254601_gshared (Predicate_1_t3864595265 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<RegNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2071153590_gshared (Predicate_1_t3405110770 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<RegNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2339352174_gshared (Predicate_1_t3405110770 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2339352174((Predicate_1_t3405110770 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<RegNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RegNavScreen_t667173359_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1988580459_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1988580459_gshared (Predicate_1_t3405110770 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1988580459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RegNavScreen_t667173359_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<RegNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3643873552_gshared (Predicate_1_t3405110770 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<SettingsNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3421597761_gshared (Predicate_1_t710097349 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<SettingsNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3254518381_gshared (Predicate_1_t710097349 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3254518381((Predicate_1_t710097349 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<SettingsNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* SettingsNavScreen_t2267127234_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2512791974_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2512791974_gshared (Predicate_1_t710097349 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2512791974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SettingsNavScreen_t2267127234_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<SettingsNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2137110451_gshared (Predicate_1_t710097349 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<StatusNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1550285418_gshared (Predicate_1_t1315339198 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<StatusNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m133054658_gshared (Predicate_1_t1315339198 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m133054658((Predicate_1_t1315339198 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<StatusNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* StatusNavScreen_t2872369083_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3727652419_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3727652419_gshared (Predicate_1_t1315339198 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3727652419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(StatusNavScreen_t2872369083_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<StatusNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4237512412_gshared (Predicate_1_t1315339198 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m461821738_gshared (Predicate_1_t2126074551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Byte>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2886027714_gshared (Predicate_1_t2126074551 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2886027714((Predicate_1_t2126074551 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m767759683_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m767759683_gshared (Predicate_1_t2126074551 * __this, uint8_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m767759683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2487624220_gshared (Predicate_1_t2126074551 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3460606476_gshared (Predicate_1_t1897451453 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m762264976_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m762264976((Predicate_1_t1897451453 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856533029_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856533029_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856533029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t3454481338_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1807093618_gshared (Predicate_1_t1897451453 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2359416570_gshared (Predicate_1_t2776792056 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m332691618_gshared (Predicate_1_t2776792056 * __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m332691618((Predicate_1_t2776792056 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t38854645_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3157529563_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3157529563_gshared (Predicate_1_t2776792056 * __this, KeyValuePair_2_t38854645  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3157529563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t38854645_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m548603360_gshared (Predicate_1_t2776792056 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2170364528_gshared (Predicate_1_t2484216029 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int16>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3072524500_gshared (Predicate_1_t2484216029 * __this, int16_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3072524500((Predicate_1_t2484216029 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int16_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int16_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int16>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1777010513_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1777010513_gshared (Predicate_1_t2484216029 * __this, int16_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1777010513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int16>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2234368510_gshared (Predicate_1_t2484216029 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2826800414_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m695569038((Predicate_1_t514847563 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2559992383_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2559992383_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2559992383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1202813828_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2289454599_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4047721271_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4047721271((Predicate_1_t1132419410 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3556950370_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3656575065_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1767993638_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m527131606_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m527131606((Predicate_1_t2832094954 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1448216027_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1448216027_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1448216027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m215026240_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1292402863_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2060780095_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2060780095((Predicate_1_t4236135325 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856151290_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856151290_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856151290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m259774785_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
