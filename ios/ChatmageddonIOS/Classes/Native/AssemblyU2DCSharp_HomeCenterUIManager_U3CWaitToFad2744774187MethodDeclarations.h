﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1
struct U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1::.ctor()
extern "C"  void U3CWaitToFadeMapViewU3Ec__Iterator1__ctor_m2876455404 (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitToFadeMapViewU3Ec__Iterator1_MoveNext_m965196224 (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToFadeMapViewU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2568860166 (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToFadeMapViewU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2622322462 (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1::Dispose()
extern "C"  void U3CWaitToFadeMapViewU3Ec__Iterator1_Dispose_m3151011077 (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager/<WaitToFadeMapView>c__Iterator1::Reset()
extern "C"  void U3CWaitToFadeMapViewU3Ec__Iterator1_Reset_m730473207 (U3CWaitToFadeMapViewU3Ec__Iterator1_t2744774187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
