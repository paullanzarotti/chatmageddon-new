﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeaderUIScaler
struct HeaderUIScaler_t1298375071;

#include "codegen/il2cpp-codegen.h"

// System.Void HeaderUIScaler::.ctor()
extern "C"  void HeaderUIScaler__ctor_m2275348182 (HeaderUIScaler_t1298375071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderUIScaler::GlobalUIScale()
extern "C"  void HeaderUIScaler_GlobalUIScale_m2038915057 (HeaderUIScaler_t1298375071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
