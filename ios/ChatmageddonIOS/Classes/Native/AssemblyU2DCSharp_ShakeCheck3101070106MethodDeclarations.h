﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShakeCheck
struct ShakeCheck_t3101070106;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ShakeCheck::.ctor()
extern "C"  void ShakeCheck__ctor_m3723852037 (ShakeCheck_t3101070106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCheck::StartShake()
extern "C"  void ShakeCheck_StartShake_m3111947317 (ShakeCheck_t3101070106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShakeCheck::CheckShake()
extern "C"  Il2CppObject * ShakeCheck_CheckShake_m1367404089 (ShakeCheck_t3101070106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCheck::UpdateShake(System.Boolean)
extern "C"  void ShakeCheck_UpdateShake_m621150843 (ShakeCheck_t3101070106 * __this, bool ___increase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ShakeCheck::GetShake()
extern "C"  int32_t ShakeCheck_GetShake_m868526649 (ShakeCheck_t3101070106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
