﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<BestHTTP.ServerSentEvents.EventSourceResponse,BestHTTP.ServerSentEvents.Message>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2722890227(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t172185580 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2967680750_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<BestHTTP.ServerSentEvents.EventSourceResponse,BestHTTP.ServerSentEvents.Message>::Invoke(T1,T2)
#define Action_2_Invoke_m250128158(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t172185580 *, EventSourceResponse_t2287402344 *, Message_t1650395211 *, const MethodInfo*))Action_2_Invoke_m715425719_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<BestHTTP.ServerSentEvents.EventSourceResponse,BestHTTP.ServerSentEvents.Message>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3149836611(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t172185580 *, EventSourceResponse_t2287402344 *, Message_t1650395211 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<BestHTTP.ServerSentEvents.EventSourceResponse,BestHTTP.ServerSentEvents.Message>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m940639849(__this, ___result0, method) ((  void (*) (Action_2_t172185580 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
