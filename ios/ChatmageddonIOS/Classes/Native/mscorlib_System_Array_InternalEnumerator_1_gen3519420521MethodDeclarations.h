﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3519420521.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"

// System.Void System.Array/InternalEnumerator`1<PlayerProfileNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3564909288_gshared (InternalEnumerator_1_t3519420521 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3564909288(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3519420521 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3564909288_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PlayerProfileNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3730100480_gshared (InternalEnumerator_1_t3519420521 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3730100480(__this, method) ((  void (*) (InternalEnumerator_1_t3519420521 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3730100480_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PlayerProfileNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m333217772_gshared (InternalEnumerator_1_t3519420521 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m333217772(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3519420521 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m333217772_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PlayerProfileNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2982650175_gshared (InternalEnumerator_1_t3519420521 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2982650175(__this, method) ((  void (*) (InternalEnumerator_1_t3519420521 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2982650175_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PlayerProfileNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1037753248_gshared (InternalEnumerator_1_t3519420521 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1037753248(__this, method) ((  bool (*) (InternalEnumerator_1_t3519420521 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1037753248_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PlayerProfileNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1687591751_gshared (InternalEnumerator_1_t3519420521 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1687591751(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3519420521 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1687591751_gshared)(__this, method)
