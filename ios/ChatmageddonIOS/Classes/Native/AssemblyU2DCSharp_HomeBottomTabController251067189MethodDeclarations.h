﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeBottomTabController
struct HomeBottomTabController_t251067189;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeBottomTabController::.ctor()
extern "C"  void HomeBottomTabController__ctor_m3073849054 (HomeBottomTabController_t251067189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeBottomTabController::Start()
extern "C"  void HomeBottomTabController_Start_m241384678 (HomeBottomTabController_t251067189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeBottomTabController::OnTweenFinished()
extern "C"  void HomeBottomTabController_OnTweenFinished_m616523956 (HomeBottomTabController_t251067189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeBottomTabController::ShowTab(System.Boolean)
extern "C"  void HomeBottomTabController_ShowTab_m4270360675 (HomeBottomTabController_t251067189 * __this, bool ___show0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeBottomTabController::PlayPositionTween(System.Boolean,System.Boolean,System.Single)
extern "C"  void HomeBottomTabController_PlayPositionTween_m3816899921 (HomeBottomTabController_t251067189 * __this, bool ___forward0, bool ___instant1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
