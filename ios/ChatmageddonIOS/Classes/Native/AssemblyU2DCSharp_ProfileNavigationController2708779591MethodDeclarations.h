﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProfileNavigationController
struct ProfileNavigationController_t2708779591;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void ProfileNavigationController::.ctor()
extern "C"  void ProfileNavigationController__ctor_m3087884034 (ProfileNavigationController_t2708779591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigationController::Start()
extern "C"  void ProfileNavigationController_Start_m2080621006 (ProfileNavigationController_t2708779591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigationController::NavigateBackwards()
extern "C"  void ProfileNavigationController_NavigateBackwards_m3070432965 (ProfileNavigationController_t2708779591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigationController::NavigateForwards(ProfileNavScreen)
extern "C"  void ProfileNavigationController_NavigateForwards_m2673824307 (ProfileNavigationController_t2708779591 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProfileNavigationController::ValidateNavigation(ProfileNavScreen,NavigationDirection)
extern "C"  bool ProfileNavigationController_ValidateNavigation_m3887812571 (ProfileNavigationController_t2708779591 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigationController::LoadNavScreen(ProfileNavScreen,NavigationDirection,System.Boolean)
extern "C"  void ProfileNavigationController_LoadNavScreen_m2965364567 (ProfileNavigationController_t2708779591 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigationController::UnloadNavScreen(ProfileNavScreen,NavigationDirection)
extern "C"  void ProfileNavigationController_UnloadNavScreen_m4264104623 (ProfileNavigationController_t2708779591 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProfileNavigationController::PopulateTitleBar(ProfileNavScreen)
extern "C"  void ProfileNavigationController_PopulateTitleBar_m1843848323 (ProfileNavigationController_t2708779591 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
