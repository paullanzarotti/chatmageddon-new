﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.CSharp.CSharpCodeProvider
struct CSharpCodeProvider_t2882678967;
// System.CodeDom.Compiler.ICodeCompiler
struct ICodeCompiler_t2548888251;

#include "codegen/il2cpp-codegen.h"

// System.Void Microsoft.CSharp.CSharpCodeProvider::.ctor()
extern "C"  void CSharpCodeProvider__ctor_m446429861 (CSharpCodeProvider_t2882678967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.ICodeCompiler Microsoft.CSharp.CSharpCodeProvider::CreateCompiler()
extern "C"  Il2CppObject * CSharpCodeProvider_CreateCompiler_m455323048 (CSharpCodeProvider_t2882678967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
