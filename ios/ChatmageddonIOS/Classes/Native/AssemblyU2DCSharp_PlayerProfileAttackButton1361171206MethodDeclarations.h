﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileAttackButton
struct PlayerProfileAttackButton_t1361171206;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerProfileAttackButton::.ctor()
extern "C"  void PlayerProfileAttackButton__ctor_m4237001169 (PlayerProfileAttackButton_t1361171206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileAttackButton::OnButtonClick()
extern "C"  void PlayerProfileAttackButton_OnButtonClick_m3861670398 (PlayerProfileAttackButton_t1361171206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileAttackButton::AttackScreenfunctionality()
extern "C"  void PlayerProfileAttackButton_AttackScreenfunctionality_m1520522570 (PlayerProfileAttackButton_t1361171206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
