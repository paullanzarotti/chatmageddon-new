﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnibillManager
struct UnibillManager_t3725383138;
// PurchaseEvent
struct PurchaseEvent_t743554429;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillState4272135008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseEvent743554429.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnibillManager::.ctor()
extern "C"  void UnibillManager__ctor_m2544794205 (UnibillManager_t3725383138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::InitUniBill()
extern "C"  void UnibillManager_InitUniBill_m3090035400 (UnibillManager_t3725383138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::onBillerReady(UnibillState)
extern "C"  void UnibillManager_onBillerReady_m4156283233 (UnibillManager_t3725383138 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::onTransactionsRestored(System.Boolean)
extern "C"  void UnibillManager_onTransactionsRestored_m143372654 (UnibillManager_t3725383138 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::onPurchased(PurchaseEvent)
extern "C"  void UnibillManager_onPurchased_m3583371024 (UnibillManager_t3725383138 * __this, PurchaseEvent_t743554429 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::onDeferred(PurchasableItem)
extern "C"  void UnibillManager_onDeferred_m2871327162 (UnibillManager_t3725383138 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::MakePurchase(PurchasableItem)
extern "C"  void UnibillManager_MakePurchase_m1304399525 (UnibillManager_t3725383138 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::RestorePurchases()
extern "C"  void UnibillManager_RestorePurchases_m2050649057 (UnibillManager_t3725383138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::RestorePurchaseItem(PurchasableItem)
extern "C"  void UnibillManager_RestorePurchaseItem_m1695208614 (UnibillManager_t3725383138 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnibillManager::GetPurchaseLocalizedString(System.String)
extern "C"  String_t* UnibillManager_GetPurchaseLocalizedString_m2890061501 (UnibillManager_t3725383138 * __this, String_t* ___purchaseItemID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnibillManager::<InitUniBill>m__0(System.Boolean,System.String)
extern "C"  void UnibillManager_U3CInitUniBillU3Em__0_m1754596986 (UnibillManager_t3725383138 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
