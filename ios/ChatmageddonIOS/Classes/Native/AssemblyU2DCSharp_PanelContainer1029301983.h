﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Panel>
struct List_1_t1156867826;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelContainer
struct  PanelContainer_t1029301983  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Panel> PanelContainer::panelsContained
	List_1_t1156867826 * ___panelsContained_2;

public:
	inline static int32_t get_offset_of_panelsContained_2() { return static_cast<int32_t>(offsetof(PanelContainer_t1029301983, ___panelsContained_2)); }
	inline List_1_t1156867826 * get_panelsContained_2() const { return ___panelsContained_2; }
	inline List_1_t1156867826 ** get_address_of_panelsContained_2() { return &___panelsContained_2; }
	inline void set_panelsContained_2(List_1_t1156867826 * value)
	{
		___panelsContained_2 = value;
		Il2CppCodeGenWriteBarrier(&___panelsContained_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
