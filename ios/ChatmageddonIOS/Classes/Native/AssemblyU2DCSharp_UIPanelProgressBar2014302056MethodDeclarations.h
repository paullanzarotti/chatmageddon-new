﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPanelProgressBar
struct UIPanelProgressBar_t2014302056;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPanelProgressBar::.ctor()
extern "C"  void UIPanelProgressBar__ctor_m3938345493 (UIPanelProgressBar_t2014302056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelProgressBar::TransitionToPercent(System.Single,System.Single)
extern "C"  void UIPanelProgressBar_TransitionToPercent_m435502286 (UIPanelProgressBar_t2014302056 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelProgressBar::ProgressUpdated()
extern "C"  void UIPanelProgressBar_ProgressUpdated_m3858567425 (UIPanelProgressBar_t2014302056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelProgressBar::ProgressFinished()
extern "C"  void UIPanelProgressBar_ProgressFinished_m2992062594 (UIPanelProgressBar_t2014302056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
