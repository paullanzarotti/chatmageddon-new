﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITable
struct UITable_t3717403602;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;
// SettingsHomeContent
struct SettingsHomeContent_t249573869;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsHomeScreenList
struct  SettingsHomeScreenList_t2265775114  : public MonoBehaviour_t1158329972
{
public:
	// UITable SettingsHomeScreenList::table
	UITable_t3717403602 * ___table_2;
	// UIScrollView SettingsHomeScreenList::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_3;
	// System.Boolean SettingsHomeScreenList::listLoaded
	bool ___listLoaded_4;
	// UnityEngine.Transform SettingsHomeScreenList::itemPrefab
	Transform_t3275118058 * ___itemPrefab_5;
	// SettingsHomeContent SettingsHomeScreenList::content
	SettingsHomeContent_t249573869 * ___content_6;
	// UnityEngine.Color SettingsHomeScreenList::smActiveColour
	Color_t2020392075  ___smActiveColour_7;
	// UnityEngine.Color SettingsHomeScreenList::smInactiveColour
	Color_t2020392075  ___smInactiveColour_8;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___table_2)); }
	inline UITable_t3717403602 * get_table_2() const { return ___table_2; }
	inline UITable_t3717403602 ** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(UITable_t3717403602 * value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier(&___table_2, value);
	}

	inline static int32_t get_offset_of_draggablePanel_3() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___draggablePanel_3)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_3() const { return ___draggablePanel_3; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_3() { return &___draggablePanel_3; }
	inline void set_draggablePanel_3(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_3, value);
	}

	inline static int32_t get_offset_of_listLoaded_4() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___listLoaded_4)); }
	inline bool get_listLoaded_4() const { return ___listLoaded_4; }
	inline bool* get_address_of_listLoaded_4() { return &___listLoaded_4; }
	inline void set_listLoaded_4(bool value)
	{
		___listLoaded_4 = value;
	}

	inline static int32_t get_offset_of_itemPrefab_5() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___itemPrefab_5)); }
	inline Transform_t3275118058 * get_itemPrefab_5() const { return ___itemPrefab_5; }
	inline Transform_t3275118058 ** get_address_of_itemPrefab_5() { return &___itemPrefab_5; }
	inline void set_itemPrefab_5(Transform_t3275118058 * value)
	{
		___itemPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_5, value);
	}

	inline static int32_t get_offset_of_content_6() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___content_6)); }
	inline SettingsHomeContent_t249573869 * get_content_6() const { return ___content_6; }
	inline SettingsHomeContent_t249573869 ** get_address_of_content_6() { return &___content_6; }
	inline void set_content_6(SettingsHomeContent_t249573869 * value)
	{
		___content_6 = value;
		Il2CppCodeGenWriteBarrier(&___content_6, value);
	}

	inline static int32_t get_offset_of_smActiveColour_7() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___smActiveColour_7)); }
	inline Color_t2020392075  get_smActiveColour_7() const { return ___smActiveColour_7; }
	inline Color_t2020392075 * get_address_of_smActiveColour_7() { return &___smActiveColour_7; }
	inline void set_smActiveColour_7(Color_t2020392075  value)
	{
		___smActiveColour_7 = value;
	}

	inline static int32_t get_offset_of_smInactiveColour_8() { return static_cast<int32_t>(offsetof(SettingsHomeScreenList_t2265775114, ___smInactiveColour_8)); }
	inline Color_t2020392075  get_smInactiveColour_8() const { return ___smInactiveColour_8; }
	inline Color_t2020392075 * get_address_of_smInactiveColour_8() { return &___smInactiveColour_8; }
	inline void set_smInactiveColour_8(Color_t2020392075  value)
	{
		___smInactiveColour_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
