﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,PlayerProfileNavScreen>
struct NavigationController_2_t548570015;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m3648062236_gshared (NavigationController_2_t548570015 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m3648062236(__this, method) ((  void (*) (NavigationController_2_t548570015 *, const MethodInfo*))NavigationController_2__ctor_m3648062236_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1507831522_gshared (NavigationController_2_t548570015 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1507831522(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t548570015 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1507831522_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3154846949_gshared (NavigationController_2_t548570015 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m3154846949(__this, method) ((  void (*) (NavigationController_2_t548570015 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3154846949_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1086604804_gshared (NavigationController_2_t548570015 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m1086604804(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t548570015 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1086604804_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,PlayerProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m680059976_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m680059976(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t548570015 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m680059976_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,PlayerProfileNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m998082891_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m998082891(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t548570015 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m998082891_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3845100182_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m3845100182(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t548570015 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3845100182_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2033329720_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m2033329720(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t548570015 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2033329720_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,PlayerProfileNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m3772286536_gshared (NavigationController_2_t548570015 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m3772286536(__this, ___screen0, method) ((  void (*) (NavigationController_2_t548570015 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m3772286536_gshared)(__this, ___screen0, method)
