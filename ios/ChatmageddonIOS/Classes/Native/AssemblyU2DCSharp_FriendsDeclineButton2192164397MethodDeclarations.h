﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsDeclineButton
struct FriendsDeclineButton_t2192164397;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsDeclineButton::.ctor()
extern "C"  void FriendsDeclineButton__ctor_m3573845160 (FriendsDeclineButton_t2192164397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsDeclineButton::OnButtonClick()
extern "C"  void FriendsDeclineButton_OnButtonClick_m4019546449 (FriendsDeclineButton_t2192164397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
