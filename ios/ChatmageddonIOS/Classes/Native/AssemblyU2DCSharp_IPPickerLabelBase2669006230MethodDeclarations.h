﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPPickerLabelBase
struct IPPickerLabelBase_t2669006230;
// UIWidget
struct UIWidget_t1453041918;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void IPPickerLabelBase::.ctor()
extern "C"  void IPPickerLabelBase__ctor_m3766474663 (IPPickerLabelBase_t2669006230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget IPPickerLabelBase::GetCenterWidget()
extern "C"  UIWidget_t1453041918 * IPPickerLabelBase_GetCenterWidget_m3547959305 (IPPickerLabelBase_t2669006230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget IPPickerLabelBase::GetWidgetAtScreenPos(UnityEngine.Vector2)
extern "C"  UIWidget_t1453041918 * IPPickerLabelBase_GetWidgetAtScreenPos_m1174363259 (IPPickerLabelBase_t2669006230 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerLabelBase::InitWidgets()
extern "C"  void IPPickerLabelBase_InitWidgets_m3928224266 (IPPickerLabelBase_t2669006230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerLabelBase::MakeWidgetComponents()
extern "C"  void IPPickerLabelBase_MakeWidgetComponents_m3816736013 (IPPickerLabelBase_t2669006230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerLabelBase::EnableWidgets(System.Boolean)
extern "C"  void IPPickerLabelBase_EnableWidgets_m3655796292 (IPPickerLabelBase_t2669006230 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPPickerLabelBase::WidgetsNeedRebuild()
extern "C"  bool IPPickerLabelBase_WidgetsNeedRebuild_m1982853987 (IPPickerLabelBase_t2669006230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
