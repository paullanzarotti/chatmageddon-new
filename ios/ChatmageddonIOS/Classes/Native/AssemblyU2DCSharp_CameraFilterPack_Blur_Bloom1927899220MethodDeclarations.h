﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Bloom
struct CameraFilterPack_Blur_Bloom_t1927899220;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Bloom::.ctor()
extern "C"  void CameraFilterPack_Blur_Bloom__ctor_m2909290621 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Bloom::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Bloom_get_material_m576896168 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::Start()
extern "C"  void CameraFilterPack_Blur_Bloom_Start_m2065560197 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Bloom_OnRenderImage_m2173665165 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::OnValidate()
extern "C"  void CameraFilterPack_Blur_Bloom_OnValidate_m3037632268 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::Update()
extern "C"  void CameraFilterPack_Blur_Bloom_Update_m1402496802 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::OnDisable()
extern "C"  void CameraFilterPack_Blur_Bloom_OnDisable_m1141665420 (CameraFilterPack_Blur_Bloom_t1927899220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
