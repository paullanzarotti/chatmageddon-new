﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PUBread
struct PUBread_t493011471;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

// System.Void PUBread::.ctor(System.String,System.Boolean,System.String,System.String,UnityEngine.Texture,SFX)
extern "C"  void PUBread__ctor_m2405340544 (PUBread_t493011471 * __this, String_t* ___title0, bool ___showFade1, String_t* ___message2, String_t* ___closeText3, Texture_t2243626319 * ___texture4, int32_t ___sfx5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
