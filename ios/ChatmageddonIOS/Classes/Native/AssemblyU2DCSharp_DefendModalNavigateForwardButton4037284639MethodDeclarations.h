﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendModalNavigateForwardButton
struct DefendModalNavigateForwardButton_t4037284639;

#include "codegen/il2cpp-codegen.h"

// System.Void DefendModalNavigateForwardButton::.ctor()
extern "C"  void DefendModalNavigateForwardButton__ctor_m4287112406 (DefendModalNavigateForwardButton_t4037284639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModalNavigateForwardButton::OnButtonClick()
extern "C"  void DefendModalNavigateForwardButton_OnButtonClick_m1555798931 (DefendModalNavigateForwardButton_t4037284639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
