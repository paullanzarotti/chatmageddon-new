﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_IPPickerLabelBase2669006230.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPTextPicker
struct  IPTextPicker_t3074051364  : public IPPickerLabelBase_t2669006230
{
public:
	// System.Collections.Generic.List`1<System.String> IPTextPicker::labelsText
	List_1_t1398341365 * ___labelsText_15;
	// System.Int32 IPTextPicker::initIndex
	int32_t ___initIndex_16;
	// System.String IPTextPicker::<CurrentLabelText>k__BackingField
	String_t* ___U3CCurrentLabelTextU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_labelsText_15() { return static_cast<int32_t>(offsetof(IPTextPicker_t3074051364, ___labelsText_15)); }
	inline List_1_t1398341365 * get_labelsText_15() const { return ___labelsText_15; }
	inline List_1_t1398341365 ** get_address_of_labelsText_15() { return &___labelsText_15; }
	inline void set_labelsText_15(List_1_t1398341365 * value)
	{
		___labelsText_15 = value;
		Il2CppCodeGenWriteBarrier(&___labelsText_15, value);
	}

	inline static int32_t get_offset_of_initIndex_16() { return static_cast<int32_t>(offsetof(IPTextPicker_t3074051364, ___initIndex_16)); }
	inline int32_t get_initIndex_16() const { return ___initIndex_16; }
	inline int32_t* get_address_of_initIndex_16() { return &___initIndex_16; }
	inline void set_initIndex_16(int32_t value)
	{
		___initIndex_16 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentLabelTextU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(IPTextPicker_t3074051364, ___U3CCurrentLabelTextU3Ek__BackingField_17)); }
	inline String_t* get_U3CCurrentLabelTextU3Ek__BackingField_17() const { return ___U3CCurrentLabelTextU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CCurrentLabelTextU3Ek__BackingField_17() { return &___U3CCurrentLabelTextU3Ek__BackingField_17; }
	inline void set_U3CCurrentLabelTextU3Ek__BackingField_17(String_t* value)
	{
		___U3CCurrentLabelTextU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentLabelTextU3Ek__BackingField_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
