﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Desert
struct CameraFilterPack_Gradients_Desert_t1681323834;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Desert::.ctor()
extern "C"  void CameraFilterPack_Gradients_Desert__ctor_m1363881815 (CameraFilterPack_Gradients_Desert_t1681323834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Desert::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Desert_get_material_m47530406 (CameraFilterPack_Gradients_Desert_t1681323834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::Start()
extern "C"  void CameraFilterPack_Gradients_Desert_Start_m1806451115 (CameraFilterPack_Gradients_Desert_t1681323834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Desert_OnRenderImage_m2290415067 (CameraFilterPack_Gradients_Desert_t1681323834 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::Update()
extern "C"  void CameraFilterPack_Gradients_Desert_Update_m1995692452 (CameraFilterPack_Gradients_Desert_t1681323834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Desert_OnDisable_m2455461794 (CameraFilterPack_Gradients_Desert_t1681323834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
