﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tests.FakeBillingService
struct  FakeBillingService_t881210075  : public Il2CppObject
{
public:
	// Unibill.Impl.IBillingServiceCallback Tests.FakeBillingService::biller
	Il2CppObject * ___biller_0;
	// System.Collections.Generic.List`1<System.String> Tests.FakeBillingService::purchasedItems
	List_1_t1398341365 * ___purchasedItems_1;
	// Unibill.Impl.ProductIdRemapper Tests.FakeBillingService::remapper
	ProductIdRemapper_t3313438456 * ___remapper_2;
	// System.Boolean Tests.FakeBillingService::reportError
	bool ___reportError_3;
	// System.Boolean Tests.FakeBillingService::reportCriticalError
	bool ___reportCriticalError_4;
	// System.Boolean Tests.FakeBillingService::purchaseCalled
	bool ___purchaseCalled_5;
	// System.Boolean Tests.FakeBillingService::restoreCalled
	bool ___restoreCalled_6;

public:
	inline static int32_t get_offset_of_biller_0() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___biller_0)); }
	inline Il2CppObject * get_biller_0() const { return ___biller_0; }
	inline Il2CppObject ** get_address_of_biller_0() { return &___biller_0; }
	inline void set_biller_0(Il2CppObject * value)
	{
		___biller_0 = value;
		Il2CppCodeGenWriteBarrier(&___biller_0, value);
	}

	inline static int32_t get_offset_of_purchasedItems_1() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___purchasedItems_1)); }
	inline List_1_t1398341365 * get_purchasedItems_1() const { return ___purchasedItems_1; }
	inline List_1_t1398341365 ** get_address_of_purchasedItems_1() { return &___purchasedItems_1; }
	inline void set_purchasedItems_1(List_1_t1398341365 * value)
	{
		___purchasedItems_1 = value;
		Il2CppCodeGenWriteBarrier(&___purchasedItems_1, value);
	}

	inline static int32_t get_offset_of_remapper_2() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___remapper_2)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_2() const { return ___remapper_2; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_2() { return &___remapper_2; }
	inline void set_remapper_2(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_2 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_2, value);
	}

	inline static int32_t get_offset_of_reportError_3() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___reportError_3)); }
	inline bool get_reportError_3() const { return ___reportError_3; }
	inline bool* get_address_of_reportError_3() { return &___reportError_3; }
	inline void set_reportError_3(bool value)
	{
		___reportError_3 = value;
	}

	inline static int32_t get_offset_of_reportCriticalError_4() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___reportCriticalError_4)); }
	inline bool get_reportCriticalError_4() const { return ___reportCriticalError_4; }
	inline bool* get_address_of_reportCriticalError_4() { return &___reportCriticalError_4; }
	inline void set_reportCriticalError_4(bool value)
	{
		___reportCriticalError_4 = value;
	}

	inline static int32_t get_offset_of_purchaseCalled_5() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___purchaseCalled_5)); }
	inline bool get_purchaseCalled_5() const { return ___purchaseCalled_5; }
	inline bool* get_address_of_purchaseCalled_5() { return &___purchaseCalled_5; }
	inline void set_purchaseCalled_5(bool value)
	{
		___purchaseCalled_5 = value;
	}

	inline static int32_t get_offset_of_restoreCalled_6() { return static_cast<int32_t>(offsetof(FakeBillingService_t881210075, ___restoreCalled_6)); }
	inline bool get_restoreCalled_6() const { return ___restoreCalled_6; }
	inline bool* get_address_of_restoreCalled_6() { return &___restoreCalled_6; }
	inline void set_restoreCalled_6(bool value)
	{
		___restoreCalled_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
