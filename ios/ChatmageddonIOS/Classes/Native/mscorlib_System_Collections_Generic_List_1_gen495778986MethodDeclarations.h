﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<ProfileNavScreen>
struct List_1_t495778986;
// System.Collections.Generic.IEnumerable`1<ProfileNavScreen>
struct IEnumerable_1_t1418784899;
// ProfileNavScreen[]
struct ProfileNavScreenU5BU5D_t3517801995;
// System.Collections.Generic.IEnumerator`1<ProfileNavScreen>
struct IEnumerator_1_t2897148977;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<ProfileNavScreen>
struct ICollection_1_t2078733159;
// System.Collections.ObjectModel.ReadOnlyCollection`1<ProfileNavScreen>
struct ReadOnlyCollection_1_t1312443546;
// System.Predicate`1<ProfileNavScreen>
struct Predicate_1_t3864595265;
// System.Action`1<ProfileNavScreen>
struct Action_1_t928457236;
// System.Collections.Generic.IComparer`1<ProfileNavScreen>
struct IComparer_1_t3376088272;
// System.Comparison`1<ProfileNavScreen>
struct Comparison_1_t2388396705;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato30508660.h"

// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::.ctor()
extern "C"  void List_1__ctor_m2042120408_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1__ctor_m2042120408(__this, method) ((  void (*) (List_1_t495778986 *, const MethodInfo*))List_1__ctor_m2042120408_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3230460731_gshared (List_1_t495778986 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3230460731(__this, ___collection0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3230460731_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1958797753_gshared (List_1_t495778986 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1958797753(__this, ___capacity0, method) ((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1__ctor_m1958797753_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m3348342431_gshared (List_1_t495778986 * __this, ProfileNavScreenU5BU5D_t3517801995* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m3348342431(__this, ___data0, ___size1, method) ((  void (*) (List_1_t495778986 *, ProfileNavScreenU5BU5D_t3517801995*, int32_t, const MethodInfo*))List_1__ctor_m3348342431_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::.cctor()
extern "C"  void List_1__cctor_m3501755195_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3501755195(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3501755195_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m510612394_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m510612394(__this, method) ((  Il2CppObject* (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m510612394_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1244106272_gshared (List_1_t495778986 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1244106272(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t495778986 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1244106272_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m207805701_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m207805701(__this, method) ((  Il2CppObject * (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m207805701_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m440684716_gshared (List_1_t495778986 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m440684716(__this, ___item0, method) ((  int32_t (*) (List_1_t495778986 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m440684716_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m709742006_gshared (List_1_t495778986 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m709742006(__this, ___item0, method) ((  bool (*) (List_1_t495778986 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m709742006_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3231661534_gshared (List_1_t495778986 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3231661534(__this, ___item0, method) ((  int32_t (*) (List_1_t495778986 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3231661534_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1319091673_gshared (List_1_t495778986 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1319091673(__this, ___index0, ___item1, method) ((  void (*) (List_1_t495778986 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1319091673_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m888158633_gshared (List_1_t495778986 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m888158633(__this, ___item0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m888158633_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1701777589_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1701777589(__this, method) ((  bool (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1701777589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m404175952_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m404175952(__this, method) ((  bool (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m404175952_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m833317542_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m833317542(__this, method) ((  Il2CppObject * (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m833317542_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m62150101_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m62150101(__this, method) ((  bool (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m62150101_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1694868796_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1694868796(__this, method) ((  bool (*) (List_1_t495778986 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1694868796_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3472036329_gshared (List_1_t495778986 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3472036329(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3472036329_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3822706010_gshared (List_1_t495778986 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3822706010(__this, ___index0, ___value1, method) ((  void (*) (List_1_t495778986 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3822706010_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Add(T)
extern "C"  void List_1_Add_m1606338957_gshared (List_1_t495778986 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1606338957(__this, ___item0, method) ((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_Add_m1606338957_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1245486914_gshared (List_1_t495778986 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1245486914(__this, ___newCount0, method) ((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1245486914_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1415767929_gshared (List_1_t495778986 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1415767929(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t495778986 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1415767929_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2986561650_gshared (List_1_t495778986 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2986561650(__this, ___collection0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2986561650_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1273302226_gshared (List_1_t495778986 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1273302226(__this, ___enumerable0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1273302226_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2948500409_gshared (List_1_t495778986 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2948500409(__this, ___collection0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2948500409_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<ProfileNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1312443546 * List_1_AsReadOnly_m3308563284_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3308563284(__this, method) ((  ReadOnlyCollection_1_t1312443546 * (*) (List_1_t495778986 *, const MethodInfo*))List_1_AsReadOnly_m3308563284_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Clear()
extern "C"  void List_1_Clear_m3735596073_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_Clear_m3735596073(__this, method) ((  void (*) (List_1_t495778986 *, const MethodInfo*))List_1_Clear_m3735596073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m330343035_gshared (List_1_t495778986 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m330343035(__this, ___item0, method) ((  bool (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_Contains_m330343035_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m121612808_gshared (List_1_t495778986 * __this, ProfileNavScreenU5BU5D_t3517801995* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m121612808(__this, ___array0, method) ((  void (*) (List_1_t495778986 *, ProfileNavScreenU5BU5D_t3517801995*, const MethodInfo*))List_1_CopyTo_m121612808_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m586279909_gshared (List_1_t495778986 * __this, ProfileNavScreenU5BU5D_t3517801995* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m586279909(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t495778986 *, ProfileNavScreenU5BU5D_t3517801995*, int32_t, const MethodInfo*))List_1_CopyTo_m586279909_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m1071888389_gshared (List_1_t495778986 * __this, int32_t ___index0, ProfileNavScreenU5BU5D_t3517801995* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m1071888389(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t495778986 *, int32_t, ProfileNavScreenU5BU5D_t3517801995*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1071888389_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m2449327773_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_Exists_m2449327773(__this, ___match0, method) ((  bool (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_Exists_m2449327773_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<ProfileNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2632511995_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_Find_m2632511995(__this, ___match0, method) ((  int32_t (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_Find_m2632511995_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1150074136_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1150074136(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3864595265 *, const MethodInfo*))List_1_CheckMatch_m1150074136_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ProfileNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t495778986 * List_1_FindAll_m2257898144_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2257898144(__this, ___match0, method) ((  List_1_t495778986 * (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_FindAll_m2257898144_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ProfileNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t495778986 * List_1_FindAllStackBits_m1865594832_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1865594832(__this, ___match0, method) ((  List_1_t495778986 * (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_FindAllStackBits_m1865594832_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ProfileNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t495778986 * List_1_FindAllList_m3723073616_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m3723073616(__this, ___match0, method) ((  List_1_t495778986 * (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_FindAllList_m3723073616_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m2325395126_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m2325395126(__this, ___match0, method) ((  int32_t (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_FindIndex_m2325395126_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m604158387_gshared (List_1_t495778986 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3864595265 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m604158387(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t495778986 *, int32_t, int32_t, Predicate_1_t3864595265 *, const MethodInfo*))List_1_GetIndex_m604158387_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m3428405784_gshared (List_1_t495778986 * __this, Action_1_t928457236 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m3428405784(__this, ___action0, method) ((  void (*) (List_1_t495778986 *, Action_1_t928457236 *, const MethodInfo*))List_1_ForEach_m3428405784_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<ProfileNavScreen>::GetEnumerator()
extern "C"  Enumerator_t30508660  List_1_GetEnumerator_m4178777420_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4178777420(__this, method) ((  Enumerator_t30508660  (*) (List_1_t495778986 *, const MethodInfo*))List_1_GetEnumerator_m4178777420_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m217250965_gshared (List_1_t495778986 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m217250965(__this, ___item0, method) ((  int32_t (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_IndexOf_m217250965_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m247329212_gshared (List_1_t495778986 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m247329212(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t495778986 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m247329212_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3906658065_gshared (List_1_t495778986 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3906658065(__this, ___index0, method) ((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3906658065_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3806015850_gshared (List_1_t495778986 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3806015850(__this, ___index0, ___item1, method) ((  void (*) (List_1_t495778986 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m3806015850_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m490464423_gshared (List_1_t495778986 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m490464423(__this, ___collection0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m490464423_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<ProfileNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m2916848596_gshared (List_1_t495778986 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2916848596(__this, ___item0, method) ((  bool (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_Remove_m2916848596_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m205470828_gshared (List_1_t495778986 * __this, Predicate_1_t3864595265 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m205470828(__this, ___match0, method) ((  int32_t (*) (List_1_t495778986 *, Predicate_1_t3864595265 *, const MethodInfo*))List_1_RemoveAll_m205470828_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2707273046_gshared (List_1_t495778986 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2707273046(__this, ___index0, method) ((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2707273046_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2484240805_gshared (List_1_t495778986 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2484240805(__this, ___index0, ___count1, method) ((  void (*) (List_1_t495778986 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2484240805_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m1186080254_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_Reverse_m1186080254(__this, method) ((  void (*) (List_1_t495778986 *, const MethodInfo*))List_1_Reverse_m1186080254_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Sort()
extern "C"  void List_1_Sort_m3276518654_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_Sort_m3276518654(__this, method) ((  void (*) (List_1_t495778986 *, const MethodInfo*))List_1_Sort_m3276518654_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3127019530_gshared (List_1_t495778986 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3127019530(__this, ___comparer0, method) ((  void (*) (List_1_t495778986 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3127019530_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3778752451_gshared (List_1_t495778986 * __this, Comparison_1_t2388396705 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3778752451(__this, ___comparison0, method) ((  void (*) (List_1_t495778986 *, Comparison_1_t2388396705 *, const MethodInfo*))List_1_Sort_m3778752451_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<ProfileNavScreen>::ToArray()
extern "C"  ProfileNavScreenU5BU5D_t3517801995* List_1_ToArray_m3783065095_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_ToArray_m3783065095(__this, method) ((  ProfileNavScreenU5BU5D_t3517801995* (*) (List_1_t495778986 *, const MethodInfo*))List_1_ToArray_m3783065095_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1344210501_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1344210501(__this, method) ((  void (*) (List_1_t495778986 *, const MethodInfo*))List_1_TrimExcess_m1344210501_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m572308267_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m572308267(__this, method) ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))List_1_get_Capacity_m572308267_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m969901906_gshared (List_1_t495778986 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m969901906(__this, ___value0, method) ((  void (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_set_Capacity_m969901906_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<ProfileNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m3972730636_gshared (List_1_t495778986 * __this, const MethodInfo* method);
#define List_1_get_Count_m3972730636(__this, method) ((  int32_t (*) (List_1_t495778986 *, const MethodInfo*))List_1_get_Count_m3972730636_gshared)(__this, method)
// T System.Collections.Generic.List`1<ProfileNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1796273498_gshared (List_1_t495778986 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1796273498(__this, ___index0, method) ((  int32_t (*) (List_1_t495778986 *, int32_t, const MethodInfo*))List_1_get_Item_m1796273498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ProfileNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3302474373_gshared (List_1_t495778986 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3302474373(__this, ___index0, ___value1, method) ((  void (*) (List_1_t495778986 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3302474373_gshared)(__this, ___index0, ___value1, method)
