﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>
struct ReadOnlyCollection_1_t222545151;
// System.Collections.Generic.IList`1<AttackNavScreen>
struct IList_1_t577700060;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AttackNavScreen[]
struct AttackNavScreenU5BU5D_t3684257394;
// System.Collections.Generic.IEnumerator`1<AttackNavScreen>
struct IEnumerator_1_t1807250582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2162026229_gshared (ReadOnlyCollection_1_t222545151 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2162026229(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2162026229_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3928995183_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3928995183(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3928995183_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1551015531_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1551015531(__this, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1551015531_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3318722098_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3318722098(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3318722098_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3942369340_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3942369340(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3942369340_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m679853702_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m679853702(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m679853702_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4292473168_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4292473168(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4292473168_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2157739779_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2157739779(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2157739779_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1348261335_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1348261335(__this, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1348261335_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1147384852_gshared (ReadOnlyCollection_1_t222545151 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1147384852(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1147384852_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1885740195_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1885740195(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1885740195_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3967884544_gshared (ReadOnlyCollection_1_t222545151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3967884544(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t222545151 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3967884544_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2824186106_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2824186106(__this, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2824186106_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1863117958_gshared (ReadOnlyCollection_1_t222545151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1863117958(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1863117958_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m508251182_gshared (ReadOnlyCollection_1_t222545151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m508251182(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t222545151 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m508251182_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3483860667_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3483860667(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3483860667_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1293006179_gshared (ReadOnlyCollection_1_t222545151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1293006179(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1293006179_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m234771917_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m234771917(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m234771917_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3541729752_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3541729752(__this, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3541729752_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m689964324_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m689964324(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m689964324_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m139503699_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m139503699(__this, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m139503699_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2982358156_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2982358156(__this, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2982358156_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1413036107_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1413036107(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1413036107_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2685592274_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2685592274(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2685592274_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3659377153_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3659377153(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3659377153_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m423649731_gshared (ReadOnlyCollection_1_t222545151 * __this, AttackNavScreenU5BU5D_t3684257394* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m423649731(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t222545151 *, AttackNavScreenU5BU5D_t3684257394*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m423649731_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2041177404_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2041177404(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2041177404_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2929500503_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2929500503(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2929500503_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m814965424_gshared (ReadOnlyCollection_1_t222545151 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m814965424(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t222545151 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m814965424_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AttackNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m2565478836_gshared (ReadOnlyCollection_1_t222545151 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2565478836(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t222545151 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2565478836_gshared)(__this, ___index0, method)
