﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchSequenceUIScaler
struct  LaunchSequenceUIScaler_t2928648964  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite LaunchSequenceUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UITexture LaunchSequenceUIScaler::animationRender
	UITexture_t2537039969 * ___animationRender_15;
	// UISprite LaunchSequenceUIScaler::whiteout
	UISprite_t603616735 * ___whiteout_16;
	// UITexture LaunchSequenceUIScaler::animationOutline
	UITexture_t2537039969 * ___animationOutline_17;
	// UITexture LaunchSequenceUIScaler::animationMask
	UITexture_t2537039969 * ___animationMask_18;
	// UnityEngine.Transform LaunchSequenceUIScaler::crossHairs
	Transform_t3275118058 * ___crossHairs_19;
	// UnityEngine.Transform LaunchSequenceUIScaler::labels
	Transform_t3275118058 * ___labels_20;
	// UnityEngine.Transform LaunchSequenceUIScaler::countdownTimers
	Transform_t3275118058 * ___countdownTimers_21;
	// UnityEngine.Transform LaunchSequenceUIScaler::statusButton
	Transform_t3275118058 * ___statusButton_22;
	// UISprite LaunchSequenceUIScaler::statusBackground
	UISprite_t603616735 * ___statusBackground_23;
	// UnityEngine.Transform LaunchSequenceUIScaler::initBulb
	Transform_t3275118058 * ___initBulb_24;
	// UnityEngine.Transform LaunchSequenceUIScaler::launchBulb
	Transform_t3275118058 * ___launchBulb_25;
	// UnityEngine.Transform LaunchSequenceUIScaler::skipButton
	Transform_t3275118058 * ___skipButton_26;
	// UnityEngine.BoxCollider LaunchSequenceUIScaler::skipCollider
	BoxCollider_t22920061 * ___skipCollider_27;
	// UISprite LaunchSequenceUIScaler::skipBackground
	UISprite_t603616735 * ___skipBackground_28;
	// UILabel LaunchSequenceUIScaler::skipLabel
	UILabel_t1795115428 * ___skipLabel_29;
	// UITexture LaunchSequenceUIScaler::reportBackground
	UITexture_t2537039969 * ___reportBackground_30;
	// UnityEngine.Transform LaunchSequenceUIScaler::reportTitle
	Transform_t3275118058 * ___reportTitle_31;
	// System.Collections.Generic.List`1<UnityEngine.Transform> LaunchSequenceUIScaler::reportLabels
	List_1_t2644239190 * ___reportLabels_32;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_animationRender_15() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___animationRender_15)); }
	inline UITexture_t2537039969 * get_animationRender_15() const { return ___animationRender_15; }
	inline UITexture_t2537039969 ** get_address_of_animationRender_15() { return &___animationRender_15; }
	inline void set_animationRender_15(UITexture_t2537039969 * value)
	{
		___animationRender_15 = value;
		Il2CppCodeGenWriteBarrier(&___animationRender_15, value);
	}

	inline static int32_t get_offset_of_whiteout_16() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___whiteout_16)); }
	inline UISprite_t603616735 * get_whiteout_16() const { return ___whiteout_16; }
	inline UISprite_t603616735 ** get_address_of_whiteout_16() { return &___whiteout_16; }
	inline void set_whiteout_16(UISprite_t603616735 * value)
	{
		___whiteout_16 = value;
		Il2CppCodeGenWriteBarrier(&___whiteout_16, value);
	}

	inline static int32_t get_offset_of_animationOutline_17() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___animationOutline_17)); }
	inline UITexture_t2537039969 * get_animationOutline_17() const { return ___animationOutline_17; }
	inline UITexture_t2537039969 ** get_address_of_animationOutline_17() { return &___animationOutline_17; }
	inline void set_animationOutline_17(UITexture_t2537039969 * value)
	{
		___animationOutline_17 = value;
		Il2CppCodeGenWriteBarrier(&___animationOutline_17, value);
	}

	inline static int32_t get_offset_of_animationMask_18() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___animationMask_18)); }
	inline UITexture_t2537039969 * get_animationMask_18() const { return ___animationMask_18; }
	inline UITexture_t2537039969 ** get_address_of_animationMask_18() { return &___animationMask_18; }
	inline void set_animationMask_18(UITexture_t2537039969 * value)
	{
		___animationMask_18 = value;
		Il2CppCodeGenWriteBarrier(&___animationMask_18, value);
	}

	inline static int32_t get_offset_of_crossHairs_19() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___crossHairs_19)); }
	inline Transform_t3275118058 * get_crossHairs_19() const { return ___crossHairs_19; }
	inline Transform_t3275118058 ** get_address_of_crossHairs_19() { return &___crossHairs_19; }
	inline void set_crossHairs_19(Transform_t3275118058 * value)
	{
		___crossHairs_19 = value;
		Il2CppCodeGenWriteBarrier(&___crossHairs_19, value);
	}

	inline static int32_t get_offset_of_labels_20() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___labels_20)); }
	inline Transform_t3275118058 * get_labels_20() const { return ___labels_20; }
	inline Transform_t3275118058 ** get_address_of_labels_20() { return &___labels_20; }
	inline void set_labels_20(Transform_t3275118058 * value)
	{
		___labels_20 = value;
		Il2CppCodeGenWriteBarrier(&___labels_20, value);
	}

	inline static int32_t get_offset_of_countdownTimers_21() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___countdownTimers_21)); }
	inline Transform_t3275118058 * get_countdownTimers_21() const { return ___countdownTimers_21; }
	inline Transform_t3275118058 ** get_address_of_countdownTimers_21() { return &___countdownTimers_21; }
	inline void set_countdownTimers_21(Transform_t3275118058 * value)
	{
		___countdownTimers_21 = value;
		Il2CppCodeGenWriteBarrier(&___countdownTimers_21, value);
	}

	inline static int32_t get_offset_of_statusButton_22() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___statusButton_22)); }
	inline Transform_t3275118058 * get_statusButton_22() const { return ___statusButton_22; }
	inline Transform_t3275118058 ** get_address_of_statusButton_22() { return &___statusButton_22; }
	inline void set_statusButton_22(Transform_t3275118058 * value)
	{
		___statusButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___statusButton_22, value);
	}

	inline static int32_t get_offset_of_statusBackground_23() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___statusBackground_23)); }
	inline UISprite_t603616735 * get_statusBackground_23() const { return ___statusBackground_23; }
	inline UISprite_t603616735 ** get_address_of_statusBackground_23() { return &___statusBackground_23; }
	inline void set_statusBackground_23(UISprite_t603616735 * value)
	{
		___statusBackground_23 = value;
		Il2CppCodeGenWriteBarrier(&___statusBackground_23, value);
	}

	inline static int32_t get_offset_of_initBulb_24() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___initBulb_24)); }
	inline Transform_t3275118058 * get_initBulb_24() const { return ___initBulb_24; }
	inline Transform_t3275118058 ** get_address_of_initBulb_24() { return &___initBulb_24; }
	inline void set_initBulb_24(Transform_t3275118058 * value)
	{
		___initBulb_24 = value;
		Il2CppCodeGenWriteBarrier(&___initBulb_24, value);
	}

	inline static int32_t get_offset_of_launchBulb_25() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___launchBulb_25)); }
	inline Transform_t3275118058 * get_launchBulb_25() const { return ___launchBulb_25; }
	inline Transform_t3275118058 ** get_address_of_launchBulb_25() { return &___launchBulb_25; }
	inline void set_launchBulb_25(Transform_t3275118058 * value)
	{
		___launchBulb_25 = value;
		Il2CppCodeGenWriteBarrier(&___launchBulb_25, value);
	}

	inline static int32_t get_offset_of_skipButton_26() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___skipButton_26)); }
	inline Transform_t3275118058 * get_skipButton_26() const { return ___skipButton_26; }
	inline Transform_t3275118058 ** get_address_of_skipButton_26() { return &___skipButton_26; }
	inline void set_skipButton_26(Transform_t3275118058 * value)
	{
		___skipButton_26 = value;
		Il2CppCodeGenWriteBarrier(&___skipButton_26, value);
	}

	inline static int32_t get_offset_of_skipCollider_27() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___skipCollider_27)); }
	inline BoxCollider_t22920061 * get_skipCollider_27() const { return ___skipCollider_27; }
	inline BoxCollider_t22920061 ** get_address_of_skipCollider_27() { return &___skipCollider_27; }
	inline void set_skipCollider_27(BoxCollider_t22920061 * value)
	{
		___skipCollider_27 = value;
		Il2CppCodeGenWriteBarrier(&___skipCollider_27, value);
	}

	inline static int32_t get_offset_of_skipBackground_28() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___skipBackground_28)); }
	inline UISprite_t603616735 * get_skipBackground_28() const { return ___skipBackground_28; }
	inline UISprite_t603616735 ** get_address_of_skipBackground_28() { return &___skipBackground_28; }
	inline void set_skipBackground_28(UISprite_t603616735 * value)
	{
		___skipBackground_28 = value;
		Il2CppCodeGenWriteBarrier(&___skipBackground_28, value);
	}

	inline static int32_t get_offset_of_skipLabel_29() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___skipLabel_29)); }
	inline UILabel_t1795115428 * get_skipLabel_29() const { return ___skipLabel_29; }
	inline UILabel_t1795115428 ** get_address_of_skipLabel_29() { return &___skipLabel_29; }
	inline void set_skipLabel_29(UILabel_t1795115428 * value)
	{
		___skipLabel_29 = value;
		Il2CppCodeGenWriteBarrier(&___skipLabel_29, value);
	}

	inline static int32_t get_offset_of_reportBackground_30() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___reportBackground_30)); }
	inline UITexture_t2537039969 * get_reportBackground_30() const { return ___reportBackground_30; }
	inline UITexture_t2537039969 ** get_address_of_reportBackground_30() { return &___reportBackground_30; }
	inline void set_reportBackground_30(UITexture_t2537039969 * value)
	{
		___reportBackground_30 = value;
		Il2CppCodeGenWriteBarrier(&___reportBackground_30, value);
	}

	inline static int32_t get_offset_of_reportTitle_31() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___reportTitle_31)); }
	inline Transform_t3275118058 * get_reportTitle_31() const { return ___reportTitle_31; }
	inline Transform_t3275118058 ** get_address_of_reportTitle_31() { return &___reportTitle_31; }
	inline void set_reportTitle_31(Transform_t3275118058 * value)
	{
		___reportTitle_31 = value;
		Il2CppCodeGenWriteBarrier(&___reportTitle_31, value);
	}

	inline static int32_t get_offset_of_reportLabels_32() { return static_cast<int32_t>(offsetof(LaunchSequenceUIScaler_t2928648964, ___reportLabels_32)); }
	inline List_1_t2644239190 * get_reportLabels_32() const { return ___reportLabels_32; }
	inline List_1_t2644239190 ** get_address_of_reportLabels_32() { return &___reportLabels_32; }
	inline void set_reportLabels_32(List_1_t2644239190 * value)
	{
		___reportLabels_32 = value;
		Il2CppCodeGenWriteBarrier(&___reportLabels_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
