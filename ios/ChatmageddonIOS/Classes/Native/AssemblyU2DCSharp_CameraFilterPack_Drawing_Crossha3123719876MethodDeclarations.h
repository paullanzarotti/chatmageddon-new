﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Crosshatch
struct CameraFilterPack_Drawing_Crosshatch_t3123719876;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Crosshatch::.ctor()
extern "C"  void CameraFilterPack_Drawing_Crosshatch__ctor_m4272403175 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Crosshatch::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Crosshatch_get_material_m4113880656 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::Start()
extern "C"  void CameraFilterPack_Drawing_Crosshatch_Start_m1934681035 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Crosshatch_OnRenderImage_m3278263715 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Crosshatch_OnValidate_m298693496 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::Update()
extern "C"  void CameraFilterPack_Drawing_Crosshatch_Update_m3638846754 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Crosshatch_OnDisable_m475741060 (CameraFilterPack_Drawing_Crosshatch_t3123719876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
