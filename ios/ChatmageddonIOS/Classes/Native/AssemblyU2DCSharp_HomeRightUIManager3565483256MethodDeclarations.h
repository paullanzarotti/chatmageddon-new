﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeRightUIManager
struct HomeRightUIManager_t3565483256;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeRightUIManager::.ctor()
extern "C"  void HomeRightUIManager__ctor_m3393434779 (HomeRightUIManager_t3565483256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeRightUIManager::Awake()
extern "C"  void HomeRightUIManager_Awake_m1507637528 (HomeRightUIManager_t3565483256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeRightUIManager::SetUIActive(System.Boolean)
extern "C"  void HomeRightUIManager_SetUIActive_m64445102 (HomeRightUIManager_t3565483256 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeRightUIManager::PlayScaleTween(System.Boolean,System.Single)
extern "C"  void HomeRightUIManager_PlayScaleTween_m2705758326 (HomeRightUIManager_t3565483256 * __this, bool ___forward0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
