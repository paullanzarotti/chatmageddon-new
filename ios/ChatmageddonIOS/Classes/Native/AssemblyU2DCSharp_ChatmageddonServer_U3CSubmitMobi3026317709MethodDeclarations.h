﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<SubmitMobileNumber>c__AnonStorey2
struct U3CSubmitMobileNumberU3Ec__AnonStorey2_t3026317709;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<SubmitMobileNumber>c__AnonStorey2::.ctor()
extern "C"  void U3CSubmitMobileNumberU3Ec__AnonStorey2__ctor_m3475455556 (U3CSubmitMobileNumberU3Ec__AnonStorey2_t3026317709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<SubmitMobileNumber>c__AnonStorey2::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CSubmitMobileNumberU3Ec__AnonStorey2_U3CU3Em__0_m1258759925 (U3CSubmitMobileNumberU3Ec__AnonStorey2_t3026317709 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
