﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenURLButton
struct OpenURLButton_t170290035;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenURLButton::.ctor()
extern "C"  void OpenURLButton__ctor_m487325380 (OpenURLButton_t170290035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenURLButton::OnButtonClick()
extern "C"  void OpenURLButton_OnButtonClick_m3884232315 (OpenURLButton_t170290035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
