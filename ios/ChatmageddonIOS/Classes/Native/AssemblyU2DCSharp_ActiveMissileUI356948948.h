﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MissileTravelController
struct MissileTravelController_t534821896;
// UILabel
struct UILabel_t1795115428;
// ShieldDisplayController
struct ShieldDisplayController_t1783798351;
// UISprite
struct UISprite_t603616735;
// TweenScale
struct TweenScale_t2697902175;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_StatusUI2174902556.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveMissileUI
struct  ActiveMissileUI_t356948948  : public StatusUI_t2174902556
{
public:
	// MissileTravelController ActiveMissileUI::travelController
	MissileTravelController_t534821896 * ___travelController_4;
	// UILabel ActiveMissileUI::initalsLabel
	UILabel_t1795115428 * ___initalsLabel_5;
	// ShieldDisplayController ActiveMissileUI::shieldDisplay
	ShieldDisplayController_t1783798351 * ___shieldDisplay_6;
	// UISprite ActiveMissileUI::flameSprite
	UISprite_t603616735 * ___flameSprite_7;
	// TweenScale ActiveMissileUI::scaleTween
	TweenScale_t2697902175 * ___scaleTween_8;
	// System.Boolean ActiveMissileUI::shieldActive
	bool ___shieldActive_9;
	// UnityEngine.GameObject ActiveMissileUI::warningTriangle
	GameObject_t1756533147 * ___warningTriangle_10;
	// System.Boolean ActiveMissileUI::zeroHit
	bool ___zeroHit_11;
	// System.Boolean ActiveMissileUI::uiUnloading
	bool ___uiUnloading_12;

public:
	inline static int32_t get_offset_of_travelController_4() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___travelController_4)); }
	inline MissileTravelController_t534821896 * get_travelController_4() const { return ___travelController_4; }
	inline MissileTravelController_t534821896 ** get_address_of_travelController_4() { return &___travelController_4; }
	inline void set_travelController_4(MissileTravelController_t534821896 * value)
	{
		___travelController_4 = value;
		Il2CppCodeGenWriteBarrier(&___travelController_4, value);
	}

	inline static int32_t get_offset_of_initalsLabel_5() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___initalsLabel_5)); }
	inline UILabel_t1795115428 * get_initalsLabel_5() const { return ___initalsLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_initalsLabel_5() { return &___initalsLabel_5; }
	inline void set_initalsLabel_5(UILabel_t1795115428 * value)
	{
		___initalsLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___initalsLabel_5, value);
	}

	inline static int32_t get_offset_of_shieldDisplay_6() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___shieldDisplay_6)); }
	inline ShieldDisplayController_t1783798351 * get_shieldDisplay_6() const { return ___shieldDisplay_6; }
	inline ShieldDisplayController_t1783798351 ** get_address_of_shieldDisplay_6() { return &___shieldDisplay_6; }
	inline void set_shieldDisplay_6(ShieldDisplayController_t1783798351 * value)
	{
		___shieldDisplay_6 = value;
		Il2CppCodeGenWriteBarrier(&___shieldDisplay_6, value);
	}

	inline static int32_t get_offset_of_flameSprite_7() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___flameSprite_7)); }
	inline UISprite_t603616735 * get_flameSprite_7() const { return ___flameSprite_7; }
	inline UISprite_t603616735 ** get_address_of_flameSprite_7() { return &___flameSprite_7; }
	inline void set_flameSprite_7(UISprite_t603616735 * value)
	{
		___flameSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___flameSprite_7, value);
	}

	inline static int32_t get_offset_of_scaleTween_8() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___scaleTween_8)); }
	inline TweenScale_t2697902175 * get_scaleTween_8() const { return ___scaleTween_8; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_8() { return &___scaleTween_8; }
	inline void set_scaleTween_8(TweenScale_t2697902175 * value)
	{
		___scaleTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_8, value);
	}

	inline static int32_t get_offset_of_shieldActive_9() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___shieldActive_9)); }
	inline bool get_shieldActive_9() const { return ___shieldActive_9; }
	inline bool* get_address_of_shieldActive_9() { return &___shieldActive_9; }
	inline void set_shieldActive_9(bool value)
	{
		___shieldActive_9 = value;
	}

	inline static int32_t get_offset_of_warningTriangle_10() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___warningTriangle_10)); }
	inline GameObject_t1756533147 * get_warningTriangle_10() const { return ___warningTriangle_10; }
	inline GameObject_t1756533147 ** get_address_of_warningTriangle_10() { return &___warningTriangle_10; }
	inline void set_warningTriangle_10(GameObject_t1756533147 * value)
	{
		___warningTriangle_10 = value;
		Il2CppCodeGenWriteBarrier(&___warningTriangle_10, value);
	}

	inline static int32_t get_offset_of_zeroHit_11() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___zeroHit_11)); }
	inline bool get_zeroHit_11() const { return ___zeroHit_11; }
	inline bool* get_address_of_zeroHit_11() { return &___zeroHit_11; }
	inline void set_zeroHit_11(bool value)
	{
		___zeroHit_11 = value;
	}

	inline static int32_t get_offset_of_uiUnloading_12() { return static_cast<int32_t>(offsetof(ActiveMissileUI_t356948948, ___uiUnloading_12)); }
	inline bool get_uiUnloading_12() const { return ___uiUnloading_12; }
	inline bool* get_address_of_uiUnloading_12() { return &___uiUnloading_12; }
	inline void set_uiUnloading_12(bool value)
	{
		___uiUnloading_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
