﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1237956080.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PanelType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1170240069_gshared (Enumerator_t1237956080 * __this, Dictionary_2_t3846390612 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1170240069(__this, ___host0, method) ((  void (*) (Enumerator_t1237956080 *, Dictionary_2_t3846390612 *, const MethodInfo*))Enumerator__ctor_m1170240069_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PanelType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2468359346_gshared (Enumerator_t1237956080 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2468359346(__this, method) ((  Il2CppObject * (*) (Enumerator_t1237956080 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2468359346_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PanelType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m447179610_gshared (Enumerator_t1237956080 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m447179610(__this, method) ((  void (*) (Enumerator_t1237956080 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m447179610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PanelType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1520049669_gshared (Enumerator_t1237956080 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1520049669(__this, method) ((  void (*) (Enumerator_t1237956080 *, const MethodInfo*))Enumerator_Dispose_m1520049669_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PanelType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m913108718_gshared (Enumerator_t1237956080 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m913108718(__this, method) ((  bool (*) (Enumerator_t1237956080 *, const MethodInfo*))Enumerator_MoveNext_m913108718_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PanelType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1103534340_gshared (Enumerator_t1237956080 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1103534340(__this, method) ((  Il2CppObject * (*) (Enumerator_t1237956080 *, const MethodInfo*))Enumerator_get_Current_m1103534340_gshared)(__this, method)
