﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnumSwitch`1<Audience>
struct EnumSwitch_1_t2186774145;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"

// System.Void EnumSwitch`1<Audience>::.ctor()
extern "C"  void EnumSwitch_1__ctor_m2926677_gshared (EnumSwitch_1_t2186774145 * __this, const MethodInfo* method);
#define EnumSwitch_1__ctor_m2926677(__this, method) ((  void (*) (EnumSwitch_1_t2186774145 *, const MethodInfo*))EnumSwitch_1__ctor_m2926677_gshared)(__this, method)
// System.Void EnumSwitch`1<Audience>::SelectSwitchState(SwitchState)
extern "C"  void EnumSwitch_1_SelectSwitchState_m2683477613_gshared (EnumSwitch_1_t2186774145 * __this, int32_t ___newState0, const MethodInfo* method);
#define EnumSwitch_1_SelectSwitchState_m2683477613(__this, ___newState0, method) ((  void (*) (EnumSwitch_1_t2186774145 *, int32_t, const MethodInfo*))EnumSwitch_1_SelectSwitchState_m2683477613_gshared)(__this, ___newState0, method)
// System.Void EnumSwitch`1<Audience>::OnSwitch(SwitchState)
extern "C"  void EnumSwitch_1_OnSwitch_m3378858131_gshared (EnumSwitch_1_t2186774145 * __this, int32_t ___newState0, const MethodInfo* method);
#define EnumSwitch_1_OnSwitch_m3378858131(__this, ___newState0, method) ((  void (*) (EnumSwitch_1_t2186774145 *, int32_t, const MethodInfo*))EnumSwitch_1_OnSwitch_m3378858131_gshared)(__this, ___newState0, method)
