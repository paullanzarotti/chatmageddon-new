﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_ShieldType96263953.h"
#include "AssemblyU2DCSharp_ShieldProtection4135544898.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shield
struct  Shield_t3327121081  : public Item_t2440468191
{
public:
	// ShieldType Shield::shieldType
	int32_t ___shieldType_4;
	// ShieldProtection Shield::protectionType
	int32_t ___protectionType_5;
	// System.Boolean Shield::perfectLaunchProtection
	bool ___perfectLaunchProtection_6;
	// System.Int32 Shield::duration
	int32_t ___duration_7;
	// System.Boolean Shield::active
	bool ___active_8;
	// System.DateTime Shield::activatedAt
	DateTime_t693205669  ___activatedAt_9;
	// System.DateTime Shield::deactivatedAt
	DateTime_t693205669  ___deactivatedAt_10;

public:
	inline static int32_t get_offset_of_shieldType_4() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___shieldType_4)); }
	inline int32_t get_shieldType_4() const { return ___shieldType_4; }
	inline int32_t* get_address_of_shieldType_4() { return &___shieldType_4; }
	inline void set_shieldType_4(int32_t value)
	{
		___shieldType_4 = value;
	}

	inline static int32_t get_offset_of_protectionType_5() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___protectionType_5)); }
	inline int32_t get_protectionType_5() const { return ___protectionType_5; }
	inline int32_t* get_address_of_protectionType_5() { return &___protectionType_5; }
	inline void set_protectionType_5(int32_t value)
	{
		___protectionType_5 = value;
	}

	inline static int32_t get_offset_of_perfectLaunchProtection_6() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___perfectLaunchProtection_6)); }
	inline bool get_perfectLaunchProtection_6() const { return ___perfectLaunchProtection_6; }
	inline bool* get_address_of_perfectLaunchProtection_6() { return &___perfectLaunchProtection_6; }
	inline void set_perfectLaunchProtection_6(bool value)
	{
		___perfectLaunchProtection_6 = value;
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___duration_7)); }
	inline int32_t get_duration_7() const { return ___duration_7; }
	inline int32_t* get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(int32_t value)
	{
		___duration_7 = value;
	}

	inline static int32_t get_offset_of_active_8() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___active_8)); }
	inline bool get_active_8() const { return ___active_8; }
	inline bool* get_address_of_active_8() { return &___active_8; }
	inline void set_active_8(bool value)
	{
		___active_8 = value;
	}

	inline static int32_t get_offset_of_activatedAt_9() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___activatedAt_9)); }
	inline DateTime_t693205669  get_activatedAt_9() const { return ___activatedAt_9; }
	inline DateTime_t693205669 * get_address_of_activatedAt_9() { return &___activatedAt_9; }
	inline void set_activatedAt_9(DateTime_t693205669  value)
	{
		___activatedAt_9 = value;
	}

	inline static int32_t get_offset_of_deactivatedAt_10() { return static_cast<int32_t>(offsetof(Shield_t3327121081, ___deactivatedAt_10)); }
	inline DateTime_t693205669  get_deactivatedAt_10() const { return ___deactivatedAt_10; }
	inline DateTime_t693205669 * get_address_of_deactivatedAt_10() { return &___deactivatedAt_10; }
	inline void set_deactivatedAt_10(DateTime_t693205669  value)
	{
		___deactivatedAt_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
