﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0
struct U3CFindNumbersU3Ec__AnonStorey0_t3641809394;
// System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch>
struct IEnumerator_1_t3934349703;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::.ctor()
extern "C"  void U3CFindNumbersU3Ec__AnonStorey0__ctor_m2603593517 (U3CFindNumbersU3Ec__AnonStorey0_t3641809394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch> PhoneNumbers.PhoneNumberUtil/<FindNumbers>c__AnonStorey0::<>m__0()
extern "C"  Il2CppObject* U3CFindNumbersU3Ec__AnonStorey0_U3CU3Em__0_m3787587412 (U3CFindNumbersU3Ec__AnonStorey0_t3641809394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
