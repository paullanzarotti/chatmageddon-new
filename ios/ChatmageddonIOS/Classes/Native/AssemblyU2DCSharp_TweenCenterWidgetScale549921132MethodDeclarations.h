﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenCenterWidgetScale
struct TweenCenterWidgetScale_t549921132;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void TweenCenterWidgetScale::.ctor()
extern "C"  void TweenCenterWidgetScale__ctor_m2830654833 (TweenCenterWidgetScale_t549921132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScale::OnSelectionStart()
extern "C"  void TweenCenterWidgetScale_OnSelectionStart_m1042054112 (TweenCenterWidgetScale_t549921132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TweenCenterWidgetScale::StopTweenOnDirection()
extern "C"  Il2CppObject * TweenCenterWidgetScale_StopTweenOnDirection_m220920736 (TweenCenterWidgetScale_t549921132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScale::OnPickerStopped()
extern "C"  void TweenCenterWidgetScale_OnPickerStopped_m3313262611 (TweenCenterWidgetScale_t549921132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenCenterWidgetScale::GetOrAddTweener()
extern "C"  void TweenCenterWidgetScale_GetOrAddTweener_m299939981 (TweenCenterWidgetScale_t549921132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
