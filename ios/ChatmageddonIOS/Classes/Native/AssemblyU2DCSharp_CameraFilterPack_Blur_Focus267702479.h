﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blur_Focus
struct  CameraFilterPack_Blur_Focus_t267702479  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Blur_Focus::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Focus::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Focus::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Focus::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Focus::CenterX
	float ___CenterX_6;
	// System.Single CameraFilterPack_Blur_Focus::CenterY
	float ___CenterY_7;
	// System.Single CameraFilterPack_Blur_Focus::_Size
	float ____Size_8;
	// System.Single CameraFilterPack_Blur_Focus::_Eyes
	float ____Eyes_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_CenterX_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ___CenterX_6)); }
	inline float get_CenterX_6() const { return ___CenterX_6; }
	inline float* get_address_of_CenterX_6() { return &___CenterX_6; }
	inline void set_CenterX_6(float value)
	{
		___CenterX_6 = value;
	}

	inline static int32_t get_offset_of_CenterY_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ___CenterY_7)); }
	inline float get_CenterY_7() const { return ___CenterY_7; }
	inline float* get_address_of_CenterY_7() { return &___CenterY_7; }
	inline void set_CenterY_7(float value)
	{
		___CenterY_7 = value;
	}

	inline static int32_t get_offset_of__Size_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ____Size_8)); }
	inline float get__Size_8() const { return ____Size_8; }
	inline float* get_address_of__Size_8() { return &____Size_8; }
	inline void set__Size_8(float value)
	{
		____Size_8 = value;
	}

	inline static int32_t get_offset_of__Eyes_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479, ____Eyes_9)); }
	inline float get__Eyes_9() const { return ____Eyes_9; }
	inline float* get_address_of__Eyes_9() { return &____Eyes_9; }
	inline void set__Eyes_9(float value)
	{
		____Eyes_9 = value;
	}
};

struct CameraFilterPack_Blur_Focus_t267702479_StaticFields
{
public:
	// System.Single CameraFilterPack_Blur_Focus::ChangeCenterX
	float ___ChangeCenterX_10;
	// System.Single CameraFilterPack_Blur_Focus::ChangeCenterY
	float ___ChangeCenterY_11;
	// System.Single CameraFilterPack_Blur_Focus::ChangeSize
	float ___ChangeSize_12;
	// System.Single CameraFilterPack_Blur_Focus::ChangeEyes
	float ___ChangeEyes_13;

public:
	inline static int32_t get_offset_of_ChangeCenterX_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479_StaticFields, ___ChangeCenterX_10)); }
	inline float get_ChangeCenterX_10() const { return ___ChangeCenterX_10; }
	inline float* get_address_of_ChangeCenterX_10() { return &___ChangeCenterX_10; }
	inline void set_ChangeCenterX_10(float value)
	{
		___ChangeCenterX_10 = value;
	}

	inline static int32_t get_offset_of_ChangeCenterY_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479_StaticFields, ___ChangeCenterY_11)); }
	inline float get_ChangeCenterY_11() const { return ___ChangeCenterY_11; }
	inline float* get_address_of_ChangeCenterY_11() { return &___ChangeCenterY_11; }
	inline void set_ChangeCenterY_11(float value)
	{
		___ChangeCenterY_11 = value;
	}

	inline static int32_t get_offset_of_ChangeSize_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479_StaticFields, ___ChangeSize_12)); }
	inline float get_ChangeSize_12() const { return ___ChangeSize_12; }
	inline float* get_address_of_ChangeSize_12() { return &___ChangeSize_12; }
	inline void set_ChangeSize_12(float value)
	{
		___ChangeSize_12 = value;
	}

	inline static int32_t get_offset_of_ChangeEyes_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_Focus_t267702479_StaticFields, ___ChangeEyes_13)); }
	inline float get_ChangeEyes_13() const { return ___ChangeEyes_13; }
	inline float* get_address_of_ChangeEyes_13() { return &___ChangeEyes_13; }
	inline void set_ChangeEyes_13(float value)
	{
		___ChangeEyes_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
