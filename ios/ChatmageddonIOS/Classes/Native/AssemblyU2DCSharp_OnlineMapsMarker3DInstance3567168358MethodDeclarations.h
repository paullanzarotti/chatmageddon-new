﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsMarker3DInstance
struct OnlineMapsMarker3DInstance_t3567168358;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsMarker3DInstance::.ctor()
extern "C"  void OnlineMapsMarker3DInstance__ctor_m3798647803 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::Awake()
extern "C"  void OnlineMapsMarker3DInstance_Awake_m1391752646 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::OnPress()
extern "C"  void OnlineMapsMarker3DInstance_OnPress_m2035113929 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::OnRelease()
extern "C"  void OnlineMapsMarker3DInstance_OnRelease_m1102728635 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::Start()
extern "C"  void OnlineMapsMarker3DInstance_Start_m3829959239 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::Update()
extern "C"  void OnlineMapsMarker3DInstance_Update_m3560076308 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::UpdateBaseProps()
extern "C"  void OnlineMapsMarker3DInstance_UpdateBaseProps_m1403085473 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsMarker3DInstance::UpdateDefaultMarkerEvens()
extern "C"  void OnlineMapsMarker3DInstance_UpdateDefaultMarkerEvens_m1310615564 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OnlineMapsMarker3DInstance::WaitLongPress()
extern "C"  Il2CppObject * OnlineMapsMarker3DInstance_WaitLongPress_m1888431283 (OnlineMapsMarker3DInstance_t3567168358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
