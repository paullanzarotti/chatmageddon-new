﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsNavigateForwardsButton
struct SettingsNavigateForwardsButton_t3659026136;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsNavigateForwardsButton::.ctor()
extern "C"  void SettingsNavigateForwardsButton__ctor_m3939293959 (SettingsNavigateForwardsButton_t3659026136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigateForwardsButton::OnButtonClick()
extern "C"  void SettingsNavigateForwardsButton_OnButtonClick_m1117055996 (SettingsNavigateForwardsButton_t3659026136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
