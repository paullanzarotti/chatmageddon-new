﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceShareManager`2<System.Object,ShareRequestLocation>
struct DeviceShareManager_2_t3099449981;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ShareRequestLocation2965642179.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DeviceShareManager`2<System.Object,ShareRequestLocation>::.ctor()
extern "C"  void DeviceShareManager_2__ctor_m2042261962_gshared (DeviceShareManager_2_t3099449981 * __this, const MethodInfo* method);
#define DeviceShareManager_2__ctor_m2042261962(__this, method) ((  void (*) (DeviceShareManager_2_t3099449981 *, const MethodInfo*))DeviceShareManager_2__ctor_m2042261962_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,ShareRequestLocation>::OpenUI(RequestLocation)
extern "C"  void DeviceShareManager_2_OpenUI_m3055098682_gshared (DeviceShareManager_2_t3099449981 * __this, int32_t ___location0, const MethodInfo* method);
#define DeviceShareManager_2_OpenUI_m3055098682(__this, ___location0, method) ((  void (*) (DeviceShareManager_2_t3099449981 *, int32_t, const MethodInfo*))DeviceShareManager_2_OpenUI_m3055098682_gshared)(__this, ___location0, method)
// System.Void DeviceShareManager`2<System.Object,ShareRequestLocation>::CloseUI()
extern "C"  void DeviceShareManager_2_CloseUI_m3020373880_gshared (DeviceShareManager_2_t3099449981 * __this, const MethodInfo* method);
#define DeviceShareManager_2_CloseUI_m3020373880(__this, method) ((  void (*) (DeviceShareManager_2_t3099449981 *, const MethodInfo*))DeviceShareManager_2_CloseUI_m3020373880_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,ShareRequestLocation>::SetLocationAtlas(RequestLocation)
extern "C"  void DeviceShareManager_2_SetLocationAtlas_m3523870158_gshared (DeviceShareManager_2_t3099449981 * __this, int32_t ___location0, const MethodInfo* method);
#define DeviceShareManager_2_SetLocationAtlas_m3523870158(__this, ___location0, method) ((  void (*) (DeviceShareManager_2_t3099449981 *, int32_t, const MethodInfo*))DeviceShareManager_2_SetLocationAtlas_m3523870158_gshared)(__this, ___location0, method)
// System.Boolean DeviceShareManager`2<System.Object,ShareRequestLocation>::CheckEmailAvailability()
extern "C"  bool DeviceShareManager_2_CheckEmailAvailability_m334056221_gshared (DeviceShareManager_2_t3099449981 * __this, const MethodInfo* method);
#define DeviceShareManager_2_CheckEmailAvailability_m334056221(__this, method) ((  bool (*) (DeviceShareManager_2_t3099449981 *, const MethodInfo*))DeviceShareManager_2_CheckEmailAvailability_m334056221_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,ShareRequestLocation>::SendEmail(System.String,System.String,System.String,System.Boolean)
extern "C"  void DeviceShareManager_2_SendEmail_m1948246187_gshared (DeviceShareManager_2_t3099449981 * __this, String_t* ___toAddress0, String_t* ___subject1, String_t* ___messageBody2, bool ___isHTML3, const MethodInfo* method);
#define DeviceShareManager_2_SendEmail_m1948246187(__this, ___toAddress0, ___subject1, ___messageBody2, ___isHTML3, method) ((  void (*) (DeviceShareManager_2_t3099449981 *, String_t*, String_t*, String_t*, bool, const MethodInfo*))DeviceShareManager_2_SendEmail_m1948246187_gshared)(__this, ___toAddress0, ___subject1, ___messageBody2, ___isHTML3, method)
// System.Boolean DeviceShareManager`2<System.Object,ShareRequestLocation>::CheckSMSAvailability()
extern "C"  bool DeviceShareManager_2_CheckSMSAvailability_m425375544_gshared (DeviceShareManager_2_t3099449981 * __this, const MethodInfo* method);
#define DeviceShareManager_2_CheckSMSAvailability_m425375544(__this, method) ((  bool (*) (DeviceShareManager_2_t3099449981 *, const MethodInfo*))DeviceShareManager_2_CheckSMSAvailability_m425375544_gshared)(__this, method)
// System.Void DeviceShareManager`2<System.Object,ShareRequestLocation>::SendSMS(System.String)
extern "C"  void DeviceShareManager_2_SendSMS_m211121685_gshared (DeviceShareManager_2_t3099449981 * __this, String_t* ___messageBody0, const MethodInfo* method);
#define DeviceShareManager_2_SendSMS_m211121685(__this, ___messageBody0, method) ((  void (*) (DeviceShareManager_2_t3099449981 *, String_t*, const MethodInfo*))DeviceShareManager_2_SendSMS_m211121685_gshared)(__this, ___messageBody0, method)
