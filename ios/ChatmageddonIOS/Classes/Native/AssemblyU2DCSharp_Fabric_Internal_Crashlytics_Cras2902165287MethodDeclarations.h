﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Internal.Crashlytics.CrashlyticsInit
struct CrashlyticsInit_t2902165287;
// System.Object
struct Il2CppObject;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t3067050131;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3067050131.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::.ctor()
extern "C"  void CrashlyticsInit__ctor_m2684058797 (CrashlyticsInit_t2902165287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Fabric.Internal.Crashlytics.CrashlyticsInit::CLUIsInitialized()
extern "C"  bool CrashlyticsInit_CLUIsInitialized_m3998361849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::Awake()
extern "C"  void CrashlyticsInit_Awake_m4027675766 (CrashlyticsInit_t2902165287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::AwakeOnce()
extern "C"  void CrashlyticsInit_AwakeOnce_m3866908019 (CrashlyticsInit_t2902165287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::RegisterExceptionHandlers()
extern "C"  void CrashlyticsInit_RegisterExceptionHandlers_m2202376162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Fabric.Internal.Crashlytics.CrashlyticsInit::IsSDKInitialized()
extern "C"  bool CrashlyticsInit_IsSDKInitialized_m2997353163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::HandleException(System.Object,System.UnhandledExceptionEventArgs)
extern "C"  void CrashlyticsInit_HandleException_m1718896874 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, UnhandledExceptionEventArgs_t3067050131 * ___eArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::HandleLog(System.String,System.String,UnityEngine.LogType)
extern "C"  void CrashlyticsInit_HandleLog_m3063141508 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___stackTraceString1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Fabric.Internal.Crashlytics.CrashlyticsInit::getMessageParts(System.String)
extern "C"  StringU5BU5D_t1642385972* CrashlyticsInit_getMessageParts_m4083281481 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.Crashlytics.CrashlyticsInit::.cctor()
extern "C"  void CrashlyticsInit__cctor_m1106735176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
