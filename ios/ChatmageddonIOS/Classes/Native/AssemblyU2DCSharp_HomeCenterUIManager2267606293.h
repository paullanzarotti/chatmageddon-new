﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenAlpha
struct TweenAlpha_t2421518635;
// UILabel
struct UILabel_t1795115428;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2018272013.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeCenterUIManager
struct  HomeCenterUIManager_t2267606293  : public MonoSingleton_1_t2018272013
{
public:
	// TweenAlpha HomeCenterUIManager::radarAlpha
	TweenAlpha_t2421518635 * ___radarAlpha_3;
	// TweenAlpha HomeCenterUIManager::mapFade
	TweenAlpha_t2421518635 * ___mapFade_4;
	// UILabel HomeCenterUIManager::mapViewLabel
	UILabel_t1795115428 * ___mapViewLabel_5;
	// TweenAlpha HomeCenterUIManager::mapViewTween
	TweenAlpha_t2421518635 * ___mapViewTween_6;
	// UITexture HomeCenterUIManager::mapBlur
	UITexture_t2537039969 * ___mapBlur_7;
	// System.Boolean HomeCenterUIManager::radarOpening
	bool ___radarOpening_8;
	// System.Boolean HomeCenterUIManager::mapOpening
	bool ___mapOpening_9;
	// System.Boolean HomeCenterUIManager::mapViewOpening
	bool ___mapViewOpening_10;
	// UnityEngine.Coroutine HomeCenterUIManager::mapViewTweenRoutine
	Coroutine_t2299508840 * ___mapViewTweenRoutine_11;

public:
	inline static int32_t get_offset_of_radarAlpha_3() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___radarAlpha_3)); }
	inline TweenAlpha_t2421518635 * get_radarAlpha_3() const { return ___radarAlpha_3; }
	inline TweenAlpha_t2421518635 ** get_address_of_radarAlpha_3() { return &___radarAlpha_3; }
	inline void set_radarAlpha_3(TweenAlpha_t2421518635 * value)
	{
		___radarAlpha_3 = value;
		Il2CppCodeGenWriteBarrier(&___radarAlpha_3, value);
	}

	inline static int32_t get_offset_of_mapFade_4() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapFade_4)); }
	inline TweenAlpha_t2421518635 * get_mapFade_4() const { return ___mapFade_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_mapFade_4() { return &___mapFade_4; }
	inline void set_mapFade_4(TweenAlpha_t2421518635 * value)
	{
		___mapFade_4 = value;
		Il2CppCodeGenWriteBarrier(&___mapFade_4, value);
	}

	inline static int32_t get_offset_of_mapViewLabel_5() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapViewLabel_5)); }
	inline UILabel_t1795115428 * get_mapViewLabel_5() const { return ___mapViewLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_mapViewLabel_5() { return &___mapViewLabel_5; }
	inline void set_mapViewLabel_5(UILabel_t1795115428 * value)
	{
		___mapViewLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___mapViewLabel_5, value);
	}

	inline static int32_t get_offset_of_mapViewTween_6() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapViewTween_6)); }
	inline TweenAlpha_t2421518635 * get_mapViewTween_6() const { return ___mapViewTween_6; }
	inline TweenAlpha_t2421518635 ** get_address_of_mapViewTween_6() { return &___mapViewTween_6; }
	inline void set_mapViewTween_6(TweenAlpha_t2421518635 * value)
	{
		___mapViewTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___mapViewTween_6, value);
	}

	inline static int32_t get_offset_of_mapBlur_7() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapBlur_7)); }
	inline UITexture_t2537039969 * get_mapBlur_7() const { return ___mapBlur_7; }
	inline UITexture_t2537039969 ** get_address_of_mapBlur_7() { return &___mapBlur_7; }
	inline void set_mapBlur_7(UITexture_t2537039969 * value)
	{
		___mapBlur_7 = value;
		Il2CppCodeGenWriteBarrier(&___mapBlur_7, value);
	}

	inline static int32_t get_offset_of_radarOpening_8() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___radarOpening_8)); }
	inline bool get_radarOpening_8() const { return ___radarOpening_8; }
	inline bool* get_address_of_radarOpening_8() { return &___radarOpening_8; }
	inline void set_radarOpening_8(bool value)
	{
		___radarOpening_8 = value;
	}

	inline static int32_t get_offset_of_mapOpening_9() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapOpening_9)); }
	inline bool get_mapOpening_9() const { return ___mapOpening_9; }
	inline bool* get_address_of_mapOpening_9() { return &___mapOpening_9; }
	inline void set_mapOpening_9(bool value)
	{
		___mapOpening_9 = value;
	}

	inline static int32_t get_offset_of_mapViewOpening_10() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapViewOpening_10)); }
	inline bool get_mapViewOpening_10() const { return ___mapViewOpening_10; }
	inline bool* get_address_of_mapViewOpening_10() { return &___mapViewOpening_10; }
	inline void set_mapViewOpening_10(bool value)
	{
		___mapViewOpening_10 = value;
	}

	inline static int32_t get_offset_of_mapViewTweenRoutine_11() { return static_cast<int32_t>(offsetof(HomeCenterUIManager_t2267606293, ___mapViewTweenRoutine_11)); }
	inline Coroutine_t2299508840 * get_mapViewTweenRoutine_11() const { return ___mapViewTweenRoutine_11; }
	inline Coroutine_t2299508840 ** get_address_of_mapViewTweenRoutine_11() { return &___mapViewTweenRoutine_11; }
	inline void set_mapViewTweenRoutine_11(Coroutine_t2299508840 * value)
	{
		___mapViewTweenRoutine_11 = value;
		Il2CppCodeGenWriteBarrier(&___mapViewTweenRoutine_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
