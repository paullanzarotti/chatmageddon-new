﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseInfiniteListPopulator`1<System.Object>
struct BaseInfiniteListPopulator_1_t3248597474;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_List2313305479.h"

// System.Void BaseInfiniteListPopulator`1<System.Object>::.ctor()
extern "C"  void BaseInfiniteListPopulator_1__ctor_m2641845277_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1__ctor_m2641845277(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1__ctor_m2641845277_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::Awake()
extern "C"  void BaseInfiniteListPopulator_1_Awake_m758246746_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_Awake_m758246746(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_Awake_m758246746_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::ResetPanel()
extern "C"  void BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_ResetPanel_m2816930074(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422(__this, ___item0, ___dataIndex1, ___carouselIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, Transform_t3275118058 *, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared)(__this, ___item0, ___dataIndex1, ___carouselIndex2, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetStartIndex(System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___inStartIndex0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetStartIndex_m814882572(__this, ___inStartIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared)(__this, ___inStartIndex0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetOriginalData(System.Collections.ArrayList)
extern "C"  void BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, ArrayList_t4252133567 * ___inDataList0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetOriginalData_m3487250981(__this, ___inDataList0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, ArrayList_t4252133567 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared)(__this, ___inDataList0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetSectionIndices(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, List_1_t1440998580 * ___inSectionsIndices0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658(__this, ___inSectionsIndices0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, List_1_t1440998580 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared)(__this, ___inSectionsIndices0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::RefreshTableView()
extern "C"  void BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_RefreshTableView_m3255066675(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::InitTableView(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, ArrayList_t4252133567 * ___inDataList0, List_1_t1440998580 * ___inSectionsIndices1, int32_t ___inStartIndex2, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_InitTableView_m3216652517(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::InitTableViewImp(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, ArrayList_t4252133567 * ___inDataList0, List_1_t1440998580 * ___inSectionsIndices1, int32_t ___inStartIndex2, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::RepositionList()
extern "C"  void BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_RepositionList_m111137877(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::UponPostRepositionReq()
extern "C"  void BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, Transform_t3275118058 *, int32_t, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared)(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::PrepareListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, Transform_t3275118058 * ___item0, int32_t ___newIndex1, int32_t ___oldIndex2, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464(__this, ___item0, ___newIndex1, ___oldIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, Transform_t3275118058 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::PrepareCarouselListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, Transform_t3275118058 * ___item0, int32_t ___newIndex1, int32_t ___oldIndex2, int32_t ___actualIndex3, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, Transform_t3275118058 *, int32_t, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<System.Object>::ItemIsInvisible(System.Int32)
extern "C"  Il2CppObject * BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___itemNumber0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614(__this, ___itemNumber0, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetItemInvisible(Item,System.Boolean)
extern "C"  void BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, Il2CppObject * ___item0, bool ___forward1, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353(__this, ___item0, ___forward1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, Il2CppObject *, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared)(__this, ___item0, ___forward1, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::UpdateList(System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___itemNumber0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_UpdateList_m2236403487(__this, ___itemNumber0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::UpdateListDirection(BaseInfiniteListPopulator`1/ListMovementDirection<Item>,System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___direction0, int32_t ___itemNumber1, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970(__this, ___direction0, ___itemNumber1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared)(__this, ___direction0, ___itemNumber1, method)
// System.Int32 BaseInfiniteListPopulator`1<System.Object>::GetJumpIndexForItem(System.Int32)
extern "C"  int32_t BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___itemDataIndex0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared)(__this, ___itemDataIndex0, method)
// System.Int32 BaseInfiniteListPopulator`1<System.Object>::GetRealIndexForItem(System.Int32)
extern "C"  int32_t BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___itemDataIndex0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::itemIsPressed(System.Int32,System.Boolean)
extern "C"  void BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___itemDataIndex0, bool ___isDown1, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_itemIsPressed_m852036530(__this, ___itemDataIndex0, ___isDown1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, bool, const MethodInfo*))BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared)(__this, ___itemDataIndex0, ___isDown1, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::itemClicked(System.Int32)
extern "C"  void BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___itemDataIndex0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_itemClicked_m3174637120(__this, ___itemDataIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::StopScrollViewMomentum()
extern "C"  void BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared)(__this, method)
// UnityEngine.Transform BaseInfiniteListPopulator`1<System.Object>::GetItemFromPool(System.Int32)
extern "C"  Transform_t3275118058 * BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, int32_t ___i0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773(__this, ___i0, method) ((  Transform_t3275118058 * (*) (BaseInfiniteListPopulator_1_t3248597474 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared)(__this, ___i0, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::RefreshPool()
extern "C"  void BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_RefreshPool_m4033485912(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetScrollToBottom()
extern "C"  void BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetScrollPosition(System.Single,System.Boolean)
extern "C"  void BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, float ___exactPosition0, bool ___up1, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381(__this, ___exactPosition0, ___up1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, float, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared)(__this, ___exactPosition0, ___up1, method)
// System.Void BaseInfiniteListPopulator`1<System.Object>::SetScrollPositionOverTime(System.Single)
extern "C"  void BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, float ___exactPosition0, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221(__this, ___exactPosition0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3248597474 *, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared)(__this, ___exactPosition0, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<System.Object>::SetPanelPosOverTime(System.Single,System.Single)
extern "C"  Il2CppObject * BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared (BaseInfiniteListPopulator_1_t3248597474 * __this, float ___seconds0, float ___position1, const MethodInfo* method);
#define BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792(__this, ___seconds0, ___position1, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t3248597474 *, float, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared)(__this, ___seconds0, ___position1, method)
