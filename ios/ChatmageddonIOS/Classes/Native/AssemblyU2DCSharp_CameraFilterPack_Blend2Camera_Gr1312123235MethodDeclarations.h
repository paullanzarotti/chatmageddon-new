﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_GreenScreen
struct CameraFilterPack_Blend2Camera_GreenScreen_t1312123235;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_GreenScreen::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_GreenScreen__ctor_m267232200 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_GreenScreen::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_GreenScreen_get_material_m2168943363 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::Start()
extern "C"  void CameraFilterPack_Blend2Camera_GreenScreen_Start_m992997076 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_GreenScreen_OnRenderImage_m2592331140 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::Update()
extern "C"  void CameraFilterPack_Blend2Camera_GreenScreen_Update_m2434514571 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_GreenScreen_OnEnable_m2828909236 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_GreenScreen_OnDisable_m2684764739 (CameraFilterPack_Blend2Camera_GreenScreen_t1312123235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
