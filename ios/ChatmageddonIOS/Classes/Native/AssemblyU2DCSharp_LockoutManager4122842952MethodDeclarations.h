﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LockoutManager
struct LockoutManager_t4122842952;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LockoutState173023818.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void LockoutManager::.ctor()
extern "C"  void LockoutManager__ctor_m2871510829 (LockoutManager_t4122842952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::Init()
extern "C"  void LockoutManager_Init_m1665469219 (LockoutManager_t4122842952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::LoadLockoutState(LockoutState,System.Boolean)
extern "C"  void LockoutManager_LoadLockoutState_m3054560894 (LockoutManager_t4122842952 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::UnloadLockoutState(LockoutState,System.Boolean)
extern "C"  void LockoutManager_UnloadLockoutState_m1438471479 (LockoutManager_t4122842952 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::ShowActiveLockoutUI(System.Boolean)
extern "C"  void LockoutManager_ShowActiveLockoutUI_m460368732 (LockoutManager_t4122842952 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::ShowLockoutUI(LockoutState,System.Boolean)
extern "C"  void LockoutManager_ShowLockoutUI_m3027711756 (LockoutManager_t4122842952 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::HideActiveLockoutUI(System.Boolean)
extern "C"  void LockoutManager_HideActiveLockoutUI_m1295338443 (LockoutManager_t4122842952 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::HideLockoutUI(LockoutState,System.Boolean)
extern "C"  void LockoutManager_HideLockoutUI_m2932379773 (LockoutManager_t4122842952 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LockoutManager::LockoutUIActive(LockoutState)
extern "C"  bool LockoutManager_LockoutUIActive_m2911161970 (LockoutManager_t4122842952 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LockoutManager::CheckRemainingLockoutStates()
extern "C"  bool LockoutManager_CheckRemainingLockoutStates_m3980993910 (LockoutManager_t4122842952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::LoadLockoutUI(LockoutState,System.Boolean)
extern "C"  void LockoutManager_LoadLockoutUI_m3127292607 (LockoutManager_t4122842952 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::SetUIInActiveScene(UnityEngine.GameObject)
extern "C"  void LockoutManager_SetUIInActiveScene_m2157763832 (LockoutManager_t4122842952 * __this, GameObject_t1756533147 * ___ui0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutManager::UnloadActiveLockoutUI(LockoutState,System.Boolean)
extern "C"  void LockoutManager_UnloadActiveLockoutUI_m4120141000 (LockoutManager_t4122842952 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
