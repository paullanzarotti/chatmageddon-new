﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ElementInformation
struct ElementInformation_t3165583784;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.Configuration.PropertyInformation
struct PropertyInformation_t2089433965;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;
// System.Configuration.PropertyInformationCollection
struct PropertyInformationCollection_t954922393;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "System_Configuration_System_Configuration_Property2089433965.h"
#include "System_Configuration_System_Configuration_ElementI3165583784.h"

// System.Void System.Configuration.ElementInformation::.ctor(System.Configuration.ConfigurationElement,System.Configuration.PropertyInformation)
extern "C"  void ElementInformation__ctor_m815008552 (ElementInformation_t3165583784 * __this, ConfigurationElement_t1776195828 * ___owner0, PropertyInformation_t2089433965 * ___propertyInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Configuration.ElementInformation::get_Errors()
extern "C"  Il2CppObject * ElementInformation_get_Errors_m391798293 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ElementInformation::get_IsCollection()
extern "C"  bool ElementInformation_get_IsCollection_m1533087168 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ElementInformation::get_IsLocked()
extern "C"  bool ElementInformation_get_IsLocked_m2316143358 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ElementInformation::get_IsPresent()
extern "C"  bool ElementInformation_get_IsPresent_m703740443 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Configuration.ElementInformation::get_LineNumber()
extern "C"  int32_t ElementInformation_get_LineNumber_m2058477925 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ElementInformation::get_Source()
extern "C"  String_t* ElementInformation_get_Source_m2328947178 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Configuration.ElementInformation::get_Type()
extern "C"  Type_t * ElementInformation_get_Type_m891870314 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.ElementInformation::get_Validator()
extern "C"  ConfigurationValidatorBase_t210547623 * ElementInformation_get_Validator_m2127787601 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.PropertyInformationCollection System.Configuration.ElementInformation::get_Properties()
extern "C"  PropertyInformationCollection_t954922393 * ElementInformation_get_Properties_m1533763968 (ElementInformation_t3165583784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ElementInformation::Reset(System.Configuration.ElementInformation)
extern "C"  void ElementInformation_Reset_m3203551617 (ElementInformation_t3165583784 * __this, ElementInformation_t3165583784 * ___parentInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
