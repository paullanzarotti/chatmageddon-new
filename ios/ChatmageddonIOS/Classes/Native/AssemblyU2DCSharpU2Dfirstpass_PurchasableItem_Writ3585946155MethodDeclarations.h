﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchasableItem/Writer
struct Writer_t3585946155;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PurchasableItem/Writer::.ctor()
extern "C"  void Writer__ctor_m3130420456 (Writer_t3585946155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setLocalizedPrice(PurchasableItem,System.Decimal)
extern "C"  void Writer_setLocalizedPrice_m1476804821 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, Decimal_t724701077  ___price1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setLocalizedPrice(PurchasableItem,System.String)
extern "C"  void Writer_setLocalizedPrice_m1932028235 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, String_t* ___price1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setLocalizedTitle(PurchasableItem,System.String)
extern "C"  void Writer_setLocalizedTitle_m2456356400 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, String_t* ___title1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setLocalizedDescription(PurchasableItem,System.String)
extern "C"  void Writer_setLocalizedDescription_m2084437804 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, String_t* ___description1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setPriceInLocalCurrency(PurchasableItem,System.Decimal)
extern "C"  void Writer_setPriceInLocalCurrency_m273500179 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, Decimal_t724701077  ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setISOCurrencySymbol(PurchasableItem,System.String)
extern "C"  void Writer_setISOCurrencySymbol_m2452430797 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, String_t* ___code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem/Writer::setAvailable(PurchasableItem,System.Boolean)
extern "C"  void Writer_setAvailable_m2939871297 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___item0, bool ___available1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
