﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileSwipeSelection
struct  MissileSwipeSelection_t3143859656  : public NGUISwipe_t4135300327
{
public:
	// UnityEngine.GameObject MissileSwipeSelection::carouselObject
	GameObject_t1756533147 * ___carouselObject_10;
	// UnityEngine.Vector3 MissileSwipeSelection::itemStart
	Vector3_t2243707580  ___itemStart_11;

public:
	inline static int32_t get_offset_of_carouselObject_10() { return static_cast<int32_t>(offsetof(MissileSwipeSelection_t3143859656, ___carouselObject_10)); }
	inline GameObject_t1756533147 * get_carouselObject_10() const { return ___carouselObject_10; }
	inline GameObject_t1756533147 ** get_address_of_carouselObject_10() { return &___carouselObject_10; }
	inline void set_carouselObject_10(GameObject_t1756533147 * value)
	{
		___carouselObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___carouselObject_10, value);
	}

	inline static int32_t get_offset_of_itemStart_11() { return static_cast<int32_t>(offsetof(MissileSwipeSelection_t3143859656, ___itemStart_11)); }
	inline Vector3_t2243707580  get_itemStart_11() const { return ___itemStart_11; }
	inline Vector3_t2243707580 * get_address_of_itemStart_11() { return &___itemStart_11; }
	inline void set_itemStart_11(Vector3_t2243707580  value)
	{
		___itemStart_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
