﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Missile
struct Missile_t813944928;
// AttackMiniGameController
struct AttackMiniGameController_t105133407;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackMiniGameController/<LaunchMissile>c__AnonStorey2
struct  U3CLaunchMissileU3Ec__AnonStorey2_t265198302  : public Il2CppObject
{
public:
	// Missile AttackMiniGameController/<LaunchMissile>c__AnonStorey2::inventoryMissile
	Missile_t813944928 * ___inventoryMissile_0;
	// System.Boolean AttackMiniGameController/<LaunchMissile>c__AnonStorey2::standard
	bool ___standard_1;
	// AttackMiniGameController AttackMiniGameController/<LaunchMissile>c__AnonStorey2::$this
	AttackMiniGameController_t105133407 * ___U24this_2;

public:
	inline static int32_t get_offset_of_inventoryMissile_0() { return static_cast<int32_t>(offsetof(U3CLaunchMissileU3Ec__AnonStorey2_t265198302, ___inventoryMissile_0)); }
	inline Missile_t813944928 * get_inventoryMissile_0() const { return ___inventoryMissile_0; }
	inline Missile_t813944928 ** get_address_of_inventoryMissile_0() { return &___inventoryMissile_0; }
	inline void set_inventoryMissile_0(Missile_t813944928 * value)
	{
		___inventoryMissile_0 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryMissile_0, value);
	}

	inline static int32_t get_offset_of_standard_1() { return static_cast<int32_t>(offsetof(U3CLaunchMissileU3Ec__AnonStorey2_t265198302, ___standard_1)); }
	inline bool get_standard_1() const { return ___standard_1; }
	inline bool* get_address_of_standard_1() { return &___standard_1; }
	inline void set_standard_1(bool value)
	{
		___standard_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLaunchMissileU3Ec__AnonStorey2_t265198302, ___U24this_2)); }
	inline AttackMiniGameController_t105133407 * get_U24this_2() const { return ___U24this_2; }
	inline AttackMiniGameController_t105133407 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AttackMiniGameController_t105133407 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

struct U3CLaunchMissileU3Ec__AnonStorey2_t265198302_StaticFields
{
public:
	// System.Action`1<System.Boolean> AttackMiniGameController/<LaunchMissile>c__AnonStorey2::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(U3CLaunchMissileU3Ec__AnonStorey2_t265198302_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
