﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;

#include "AssemblyU2DCSharp_ProgressBar192201240.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircularProgressBar
struct  CircularProgressBar_t2980095063  : public ProgressBar_t192201240
{
public:
	// UnityEngine.Transform CircularProgressBar::progressSection
	Transform_t3275118058 * ___progressSection_8;
	// UIPanel CircularProgressBar::progressPanel
	UIPanel_t1795085332 * ___progressPanel_9;
	// UnityEngine.Transform[] CircularProgressBar::sections
	TransformU5BU5D_t3764228911* ___sections_10;
	// System.Single CircularProgressBar::sectionDegrees
	float ___sectionDegrees_11;
	// System.Single CircularProgressBar::sectionPercentage
	float ___sectionPercentage_12;

public:
	inline static int32_t get_offset_of_progressSection_8() { return static_cast<int32_t>(offsetof(CircularProgressBar_t2980095063, ___progressSection_8)); }
	inline Transform_t3275118058 * get_progressSection_8() const { return ___progressSection_8; }
	inline Transform_t3275118058 ** get_address_of_progressSection_8() { return &___progressSection_8; }
	inline void set_progressSection_8(Transform_t3275118058 * value)
	{
		___progressSection_8 = value;
		Il2CppCodeGenWriteBarrier(&___progressSection_8, value);
	}

	inline static int32_t get_offset_of_progressPanel_9() { return static_cast<int32_t>(offsetof(CircularProgressBar_t2980095063, ___progressPanel_9)); }
	inline UIPanel_t1795085332 * get_progressPanel_9() const { return ___progressPanel_9; }
	inline UIPanel_t1795085332 ** get_address_of_progressPanel_9() { return &___progressPanel_9; }
	inline void set_progressPanel_9(UIPanel_t1795085332 * value)
	{
		___progressPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___progressPanel_9, value);
	}

	inline static int32_t get_offset_of_sections_10() { return static_cast<int32_t>(offsetof(CircularProgressBar_t2980095063, ___sections_10)); }
	inline TransformU5BU5D_t3764228911* get_sections_10() const { return ___sections_10; }
	inline TransformU5BU5D_t3764228911** get_address_of_sections_10() { return &___sections_10; }
	inline void set_sections_10(TransformU5BU5D_t3764228911* value)
	{
		___sections_10 = value;
		Il2CppCodeGenWriteBarrier(&___sections_10, value);
	}

	inline static int32_t get_offset_of_sectionDegrees_11() { return static_cast<int32_t>(offsetof(CircularProgressBar_t2980095063, ___sectionDegrees_11)); }
	inline float get_sectionDegrees_11() const { return ___sectionDegrees_11; }
	inline float* get_address_of_sectionDegrees_11() { return &___sectionDegrees_11; }
	inline void set_sectionDegrees_11(float value)
	{
		___sectionDegrees_11 = value;
	}

	inline static int32_t get_offset_of_sectionPercentage_12() { return static_cast<int32_t>(offsetof(CircularProgressBar_t2980095063, ___sectionPercentage_12)); }
	inline float get_sectionPercentage_12() const { return ___sectionPercentage_12; }
	inline float* get_address_of_sectionPercentage_12() { return &___sectionPercentage_12; }
	inline void set_sectionPercentage_12(float value)
	{
		___sectionPercentage_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
