﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackGameUIScaler
struct AttackGameUIScaler_t784344924;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackGameUIScaler::.ctor()
extern "C"  void AttackGameUIScaler__ctor_m812207683 (AttackGameUIScaler_t784344924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackGameUIScaler::GlobalUIScale()
extern "C"  void AttackGameUIScaler_GlobalUIScale_m696247726 (AttackGameUIScaler_t784344924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
