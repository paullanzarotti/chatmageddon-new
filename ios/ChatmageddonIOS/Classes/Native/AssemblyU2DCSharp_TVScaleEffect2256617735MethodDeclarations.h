﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TVScaleEffect
struct TVScaleEffect_t2256617735;

#include "codegen/il2cpp-codegen.h"

// System.Void TVScaleEffect::.ctor()
extern "C"  void TVScaleEffect__ctor_m3172787696 (TVScaleEffect_t2256617735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVScaleEffect::Start()
extern "C"  void TVScaleEffect_Start_m2760938700 (TVScaleEffect_t2256617735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVScaleEffect::Update()
extern "C"  void TVScaleEffect_Update_m3559478803 (TVScaleEffect_t2256617735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
