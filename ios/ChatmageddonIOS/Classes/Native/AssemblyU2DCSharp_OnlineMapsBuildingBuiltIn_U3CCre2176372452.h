﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1
struct  U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452  : public Il2CppObject
{
public:
	// System.Single OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1::minX
	float ___minX_0;
	// System.Single OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1::offX
	float ___offX_1;
	// System.Single OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1::minZ
	float ___minZ_2;
	// System.Single OnlineMapsBuildingBuiltIn/<CreateHouseRoof>c__AnonStorey1::offZ
	float ___offZ_3;

public:
	inline static int32_t get_offset_of_minX_0() { return static_cast<int32_t>(offsetof(U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452, ___minX_0)); }
	inline float get_minX_0() const { return ___minX_0; }
	inline float* get_address_of_minX_0() { return &___minX_0; }
	inline void set_minX_0(float value)
	{
		___minX_0 = value;
	}

	inline static int32_t get_offset_of_offX_1() { return static_cast<int32_t>(offsetof(U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452, ___offX_1)); }
	inline float get_offX_1() const { return ___offX_1; }
	inline float* get_address_of_offX_1() { return &___offX_1; }
	inline void set_offX_1(float value)
	{
		___offX_1 = value;
	}

	inline static int32_t get_offset_of_minZ_2() { return static_cast<int32_t>(offsetof(U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452, ___minZ_2)); }
	inline float get_minZ_2() const { return ___minZ_2; }
	inline float* get_address_of_minZ_2() { return &___minZ_2; }
	inline void set_minZ_2(float value)
	{
		___minZ_2 = value;
	}

	inline static int32_t get_offset_of_offZ_3() { return static_cast<int32_t>(offsetof(U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452, ___offZ_3)); }
	inline float get_offZ_3() const { return ___offZ_3; }
	inline float* get_address_of_offZ_3() { return &___offZ_3; }
	inline void set_offZ_3(float value)
	{
		___offZ_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
