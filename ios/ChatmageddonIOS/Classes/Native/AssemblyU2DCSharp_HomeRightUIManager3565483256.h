﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenScale
struct TweenScale_t2697902175;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3316148976.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeRightUIManager
struct  HomeRightUIManager_t3565483256  : public MonoSingleton_1_t3316148976
{
public:
	// TweenScale HomeRightUIManager::uiTween
	TweenScale_t2697902175 * ___uiTween_3;

public:
	inline static int32_t get_offset_of_uiTween_3() { return static_cast<int32_t>(offsetof(HomeRightUIManager_t3565483256, ___uiTween_3)); }
	inline TweenScale_t2697902175 * get_uiTween_3() const { return ___uiTween_3; }
	inline TweenScale_t2697902175 ** get_address_of_uiTween_3() { return &___uiTween_3; }
	inline void set_uiTween_3(TweenScale_t2697902175 * value)
	{
		___uiTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___uiTween_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
