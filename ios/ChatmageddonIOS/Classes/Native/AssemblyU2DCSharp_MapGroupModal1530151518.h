﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenScale
struct TweenScale_t2697902175;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// GroupCircleAnimation
struct GroupCircleAnimation_t1271941897;
// UnityEngine.Transform
struct Transform_t3275118058;
// Friend
struct Friend_t3555014108;
// System.Func`2<Friend,System.Int32>
struct Func_2_t2787752525;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapGroupModal
struct  MapGroupModal_t1530151518  : public ModalUI_t2568752073
{
public:
	// System.Int32 MapGroupModal::maxGroupAmount
	int32_t ___maxGroupAmount_3;
	// UIPanel MapGroupModal::panel
	UIPanel_t1795085332 * ___panel_4;
	// TweenAlpha MapGroupModal::backgroundFade
	TweenAlpha_t2421518635 * ___backgroundFade_5;
	// TweenScale MapGroupModal::UITween
	TweenScale_t2697902175 * ___UITween_6;
	// System.Collections.Generic.List`1<Friend> MapGroupModal::friendsGroup
	List_1_t2924135240 * ___friendsGroup_7;
	// GroupCircleAnimation MapGroupModal::groupAnim
	GroupCircleAnimation_t1271941897 * ___groupAnim_8;
	// UnityEngine.Transform MapGroupModal::groupParent
	Transform_t3275118058 * ___groupParent_9;
	// Friend MapGroupModal::profileToOpen
	Friend_t3555014108 * ___profileToOpen_10;
	// System.Boolean MapGroupModal::modalClosing
	bool ___modalClosing_11;

public:
	inline static int32_t get_offset_of_maxGroupAmount_3() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___maxGroupAmount_3)); }
	inline int32_t get_maxGroupAmount_3() const { return ___maxGroupAmount_3; }
	inline int32_t* get_address_of_maxGroupAmount_3() { return &___maxGroupAmount_3; }
	inline void set_maxGroupAmount_3(int32_t value)
	{
		___maxGroupAmount_3 = value;
	}

	inline static int32_t get_offset_of_panel_4() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___panel_4)); }
	inline UIPanel_t1795085332 * get_panel_4() const { return ___panel_4; }
	inline UIPanel_t1795085332 ** get_address_of_panel_4() { return &___panel_4; }
	inline void set_panel_4(UIPanel_t1795085332 * value)
	{
		___panel_4 = value;
		Il2CppCodeGenWriteBarrier(&___panel_4, value);
	}

	inline static int32_t get_offset_of_backgroundFade_5() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___backgroundFade_5)); }
	inline TweenAlpha_t2421518635 * get_backgroundFade_5() const { return ___backgroundFade_5; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundFade_5() { return &___backgroundFade_5; }
	inline void set_backgroundFade_5(TweenAlpha_t2421518635 * value)
	{
		___backgroundFade_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundFade_5, value);
	}

	inline static int32_t get_offset_of_UITween_6() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___UITween_6)); }
	inline TweenScale_t2697902175 * get_UITween_6() const { return ___UITween_6; }
	inline TweenScale_t2697902175 ** get_address_of_UITween_6() { return &___UITween_6; }
	inline void set_UITween_6(TweenScale_t2697902175 * value)
	{
		___UITween_6 = value;
		Il2CppCodeGenWriteBarrier(&___UITween_6, value);
	}

	inline static int32_t get_offset_of_friendsGroup_7() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___friendsGroup_7)); }
	inline List_1_t2924135240 * get_friendsGroup_7() const { return ___friendsGroup_7; }
	inline List_1_t2924135240 ** get_address_of_friendsGroup_7() { return &___friendsGroup_7; }
	inline void set_friendsGroup_7(List_1_t2924135240 * value)
	{
		___friendsGroup_7 = value;
		Il2CppCodeGenWriteBarrier(&___friendsGroup_7, value);
	}

	inline static int32_t get_offset_of_groupAnim_8() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___groupAnim_8)); }
	inline GroupCircleAnimation_t1271941897 * get_groupAnim_8() const { return ___groupAnim_8; }
	inline GroupCircleAnimation_t1271941897 ** get_address_of_groupAnim_8() { return &___groupAnim_8; }
	inline void set_groupAnim_8(GroupCircleAnimation_t1271941897 * value)
	{
		___groupAnim_8 = value;
		Il2CppCodeGenWriteBarrier(&___groupAnim_8, value);
	}

	inline static int32_t get_offset_of_groupParent_9() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___groupParent_9)); }
	inline Transform_t3275118058 * get_groupParent_9() const { return ___groupParent_9; }
	inline Transform_t3275118058 ** get_address_of_groupParent_9() { return &___groupParent_9; }
	inline void set_groupParent_9(Transform_t3275118058 * value)
	{
		___groupParent_9 = value;
		Il2CppCodeGenWriteBarrier(&___groupParent_9, value);
	}

	inline static int32_t get_offset_of_profileToOpen_10() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___profileToOpen_10)); }
	inline Friend_t3555014108 * get_profileToOpen_10() const { return ___profileToOpen_10; }
	inline Friend_t3555014108 ** get_address_of_profileToOpen_10() { return &___profileToOpen_10; }
	inline void set_profileToOpen_10(Friend_t3555014108 * value)
	{
		___profileToOpen_10 = value;
		Il2CppCodeGenWriteBarrier(&___profileToOpen_10, value);
	}

	inline static int32_t get_offset_of_modalClosing_11() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518, ___modalClosing_11)); }
	inline bool get_modalClosing_11() const { return ___modalClosing_11; }
	inline bool* get_address_of_modalClosing_11() { return &___modalClosing_11; }
	inline void set_modalClosing_11(bool value)
	{
		___modalClosing_11 = value;
	}
};

struct MapGroupModal_t1530151518_StaticFields
{
public:
	// System.Func`2<Friend,System.Int32> MapGroupModal::<>f__am$cache0
	Func_2_t2787752525 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(MapGroupModal_t1530151518_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Func_2_t2787752525 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Func_2_t2787752525 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Func_2_t2787752525 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
