﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// UILabel
struct UILabel_t1795115428;
// System.Func`2<ChatContact,System.String>
struct Func_2_t2801705332;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen266752119.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatContactsILP
struct  ChatContactsILP_t1780035494  : public BaseInfiniteListPopulator_1_t266752119
{
public:
	// System.Boolean ChatContactsILP::listLoaded
	bool ___listLoaded_33;
	// System.Collections.ArrayList ChatContactsILP::chatContactsArray
	ArrayList_t4252133567 * ___chatContactsArray_34;
	// System.Single ChatContactsILP::seperatorCellHeight
	float ___seperatorCellHeight_35;
	// UILabel ChatContactsILP::noResultsLabel
	UILabel_t1795115428 * ___noResultsLabel_36;

public:
	inline static int32_t get_offset_of_listLoaded_33() { return static_cast<int32_t>(offsetof(ChatContactsILP_t1780035494, ___listLoaded_33)); }
	inline bool get_listLoaded_33() const { return ___listLoaded_33; }
	inline bool* get_address_of_listLoaded_33() { return &___listLoaded_33; }
	inline void set_listLoaded_33(bool value)
	{
		___listLoaded_33 = value;
	}

	inline static int32_t get_offset_of_chatContactsArray_34() { return static_cast<int32_t>(offsetof(ChatContactsILP_t1780035494, ___chatContactsArray_34)); }
	inline ArrayList_t4252133567 * get_chatContactsArray_34() const { return ___chatContactsArray_34; }
	inline ArrayList_t4252133567 ** get_address_of_chatContactsArray_34() { return &___chatContactsArray_34; }
	inline void set_chatContactsArray_34(ArrayList_t4252133567 * value)
	{
		___chatContactsArray_34 = value;
		Il2CppCodeGenWriteBarrier(&___chatContactsArray_34, value);
	}

	inline static int32_t get_offset_of_seperatorCellHeight_35() { return static_cast<int32_t>(offsetof(ChatContactsILP_t1780035494, ___seperatorCellHeight_35)); }
	inline float get_seperatorCellHeight_35() const { return ___seperatorCellHeight_35; }
	inline float* get_address_of_seperatorCellHeight_35() { return &___seperatorCellHeight_35; }
	inline void set_seperatorCellHeight_35(float value)
	{
		___seperatorCellHeight_35 = value;
	}

	inline static int32_t get_offset_of_noResultsLabel_36() { return static_cast<int32_t>(offsetof(ChatContactsILP_t1780035494, ___noResultsLabel_36)); }
	inline UILabel_t1795115428 * get_noResultsLabel_36() const { return ___noResultsLabel_36; }
	inline UILabel_t1795115428 ** get_address_of_noResultsLabel_36() { return &___noResultsLabel_36; }
	inline void set_noResultsLabel_36(UILabel_t1795115428 * value)
	{
		___noResultsLabel_36 = value;
		Il2CppCodeGenWriteBarrier(&___noResultsLabel_36, value);
	}
};

struct ChatContactsILP_t1780035494_StaticFields
{
public:
	// System.Func`2<ChatContact,System.String> ChatContactsILP::<>f__am$cache0
	Func_2_t2801705332 * ___U3CU3Ef__amU24cache0_37;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_37() { return static_cast<int32_t>(offsetof(ChatContactsILP_t1780035494_StaticFields, ___U3CU3Ef__amU24cache0_37)); }
	inline Func_2_t2801705332 * get_U3CU3Ef__amU24cache0_37() const { return ___U3CU3Ef__amU24cache0_37; }
	inline Func_2_t2801705332 ** get_address_of_U3CU3Ef__amU24cache0_37() { return &___U3CU3Ef__amU24cache0_37; }
	inline void set_U3CU3Ef__amU24cache0_37(Func_2_t2801705332 * value)
	{
		___U3CU3Ef__amU24cache0_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
