﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Data
struct Data_t3569509720;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Data::.ctor(System.Collections.Hashtable)
extern "C"  void Data__ctor_m775167519 (Data_t3569509720 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Data::Contains(System.String)
extern "C"  bool Data_Contains_m265885326 (Data_t3569509720 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::Set(System.String,System.String)
extern "C"  void Data_Set_m3013203617 (Data_t3569509720 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
