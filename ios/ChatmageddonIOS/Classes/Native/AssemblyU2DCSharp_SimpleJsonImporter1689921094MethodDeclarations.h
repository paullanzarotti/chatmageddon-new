﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJsonImporter
struct SimpleJsonImporter_t1689921094;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SimpleJsonImporter::.ctor()
extern "C"  void SimpleJsonImporter__ctor_m2508559387 (SimpleJsonImporter_t1689921094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SimpleJsonImporter::Import(System.String)
extern "C"  Hashtable_t909839986 * SimpleJsonImporter_Import_m770214703 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SimpleJsonImporter::Import(System.String,System.Boolean)
extern "C"  Hashtable_t909839986 * SimpleJsonImporter_Import_m4239599806 (Il2CppObject * __this /* static, unused */, String_t* ___json0, bool ___caseInsensitive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SimpleJsonImporter::ReadHashtable(System.String,System.Int32&,System.Int32,System.Boolean)
extern "C"  Hashtable_t909839986 * SimpleJsonImporter_ReadHashtable_m2060027193 (Il2CppObject * __this /* static, unused */, String_t* ___json0, int32_t* ___begin1, int32_t ___end2, bool ___caseInsensitive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList SimpleJsonImporter::ReadArrayList(System.String,System.Int32&,System.Int32,System.Boolean)
extern "C"  ArrayList_t4252133567 * SimpleJsonImporter_ReadArrayList_m854511321 (Il2CppObject * __this /* static, unused */, String_t* ___json0, int32_t* ___begin1, int32_t ___end2, bool ___caseInsensitive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJsonImporter::MoveToNextNode(System.String,System.Int32&,System.Int32)
extern "C"  void SimpleJsonImporter_MoveToNextNode_m1308676162 (Il2CppObject * __this /* static, unused */, String_t* ___json0, int32_t* ___begin1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SimpleJsonImporter::FindMatchingEnd(System.String,System.Int32,System.Int32)
extern "C"  int32_t SimpleJsonImporter_FindMatchingEnd_m740708870 (Il2CppObject * __this /* static, unused */, String_t* ___json0, int32_t ___begin1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJsonImporter::TrimPropertyValue(System.String)
extern "C"  String_t* SimpleJsonImporter_TrimPropertyValue_m3262634926 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
