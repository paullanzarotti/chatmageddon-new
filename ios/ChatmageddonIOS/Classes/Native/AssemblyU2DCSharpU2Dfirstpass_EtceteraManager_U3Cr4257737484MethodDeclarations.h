﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraManager/<registerDeviceWithPushIO>c__Iterator1
struct U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::.ctor()
extern "C"  void U3CregisterDeviceWithPushIOU3Ec__Iterator1__ctor_m735193769 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::MoveNext()
extern "C"  bool U3CregisterDeviceWithPushIOU3Ec__Iterator1_MoveNext_m1043413175 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CregisterDeviceWithPushIOU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m853070471 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CregisterDeviceWithPushIOU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1358460303 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::Dispose()
extern "C"  void U3CregisterDeviceWithPushIOU3Ec__Iterator1_Dispose_m2521470904 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::Reset()
extern "C"  void U3CregisterDeviceWithPushIOU3Ec__Iterator1_Reset_m4233433970 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraManager/<registerDeviceWithPushIO>c__Iterator1::<>__Finally0()
extern "C"  void U3CregisterDeviceWithPushIOU3Ec__Iterator1_U3CU3E__Finally0_m4054175868 (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
