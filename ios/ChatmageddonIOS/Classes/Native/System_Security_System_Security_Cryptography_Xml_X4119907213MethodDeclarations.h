﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigXPathTransform
struct XmlDsigXPathTransform_t4119907213;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigXPathTransform::.ctor()
extern "C"  void XmlDsigXPathTransform__ctor_m2004988331 (XmlDsigXPathTransform_t4119907213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigXPathTransform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDsigXPathTransform_GetInnerXml_m2380164448 (XmlDsigXPathTransform_t4119907213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigXPathTransform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDsigXPathTransform_LoadInnerXml_m2468739893 (XmlDsigXPathTransform_t4119907213 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
