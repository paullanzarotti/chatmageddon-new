﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatThreadsListItem
struct ChatThreadsListItem_t303868848;
// ChatThreadSwipe
struct ChatThreadSwipe_t397952508;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeleteThreadButton
struct  DeleteThreadButton_t3881568975  : public SFXButton_t792651341
{
public:
	// ChatThreadsListItem DeleteThreadButton::listItem
	ChatThreadsListItem_t303868848 * ___listItem_5;
	// ChatThreadSwipe DeleteThreadButton::listItemSwipe
	ChatThreadSwipe_t397952508 * ___listItemSwipe_6;

public:
	inline static int32_t get_offset_of_listItem_5() { return static_cast<int32_t>(offsetof(DeleteThreadButton_t3881568975, ___listItem_5)); }
	inline ChatThreadsListItem_t303868848 * get_listItem_5() const { return ___listItem_5; }
	inline ChatThreadsListItem_t303868848 ** get_address_of_listItem_5() { return &___listItem_5; }
	inline void set_listItem_5(ChatThreadsListItem_t303868848 * value)
	{
		___listItem_5 = value;
		Il2CppCodeGenWriteBarrier(&___listItem_5, value);
	}

	inline static int32_t get_offset_of_listItemSwipe_6() { return static_cast<int32_t>(offsetof(DeleteThreadButton_t3881568975, ___listItemSwipe_6)); }
	inline ChatThreadSwipe_t397952508 * get_listItemSwipe_6() const { return ___listItemSwipe_6; }
	inline ChatThreadSwipe_t397952508 ** get_address_of_listItemSwipe_6() { return &___listItemSwipe_6; }
	inline void set_listItemSwipe_6(ChatThreadSwipe_t397952508 * value)
	{
		___listItemSwipe_6 = value;
		Il2CppCodeGenWriteBarrier(&___listItemSwipe_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
