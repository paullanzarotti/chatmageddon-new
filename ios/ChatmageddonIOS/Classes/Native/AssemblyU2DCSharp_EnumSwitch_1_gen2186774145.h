﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumSwitch`1<Audience>
struct  EnumSwitch_1_t2186774145  : public MonoBehaviour_t1158329972
{
public:
	// SwitchState EnumSwitch`1::currentState
	int32_t ___currentState_2;

public:
	inline static int32_t get_offset_of_currentState_2() { return static_cast<int32_t>(offsetof(EnumSwitch_1_t2186774145, ___currentState_2)); }
	inline int32_t get_currentState_2() const { return ___currentState_2; }
	inline int32_t* get_address_of_currentState_2() { return &___currentState_2; }
	inline void set_currentState_2(int32_t value)
	{
		___currentState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
