﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t3931121312;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.SortedDictionary`2/Node<System.Object,System.Object>
struct Node_t2513450244;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary1004715516.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1530705605_gshared (Enumerator_t1004715516 * __this, SortedDictionary_2_t3931121312 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m1530705605(__this, ___dic0, method) ((  void (*) (Enumerator_t1004715516 *, SortedDictionary_2_t3931121312 *, const MethodInfo*))Enumerator__ctor_m1530705605_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3759775480_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3759775480(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3759775480_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2082684101_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2082684101(__this, method) ((  Il2CppObject * (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2082684101_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8986413_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8986413(__this, method) ((  Il2CppObject * (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8986413_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3256116175_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3256116175(__this, method) ((  Il2CppObject * (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3256116175_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1941930227_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1941930227(__this, method) ((  void (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1941930227_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m2922944951_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2922944951(__this, method) ((  KeyValuePair_2_t38854645  (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_get_Current_m2922944951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1169387503_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1169387503(__this, method) ((  bool (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_MoveNext_m1169387503_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m427679586_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m427679586(__this, method) ((  void (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_Dispose_m427679586_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::get_CurrentNode()
extern "C"  Node_t2513450244 * Enumerator_get_CurrentNode_m1168849237_gshared (Enumerator_t1004715516 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentNode_m1168849237(__this, method) ((  Node_t2513450244 * (*) (Enumerator_t1004715516 *, const MethodInfo*))Enumerator_get_CurrentNode_m1168849237_gshared)(__this, method)
