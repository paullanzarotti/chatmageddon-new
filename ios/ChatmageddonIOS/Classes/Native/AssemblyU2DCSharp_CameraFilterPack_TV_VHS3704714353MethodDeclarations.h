﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_VHS
struct CameraFilterPack_TV_VHS_t3704714353;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_VHS::.ctor()
extern "C"  void CameraFilterPack_TV_VHS__ctor_m1706445658 (CameraFilterPack_TV_VHS_t3704714353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_VHS::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_VHS_get_material_m1965712201 (CameraFilterPack_TV_VHS_t3704714353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::Start()
extern "C"  void CameraFilterPack_TV_VHS_Start_m3193018018 (CameraFilterPack_TV_VHS_t3704714353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_VHS_OnRenderImage_m2917078706 (CameraFilterPack_TV_VHS_t3704714353 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::OnValidate()
extern "C"  void CameraFilterPack_TV_VHS_OnValidate_m1012732687 (CameraFilterPack_TV_VHS_t3704714353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::Update()
extern "C"  void CameraFilterPack_TV_VHS_Update_m2517351069 (CameraFilterPack_TV_VHS_t3704714353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::OnDisable()
extern "C"  void CameraFilterPack_TV_VHS_OnDisable_m3933686897 (CameraFilterPack_TV_VHS_t3704714353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
