﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>
struct ReadOnlyCollection_1_t1939645479;
// System.Collections.Generic.IList`1<UnibillError>
struct IList_1_t2294800388;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnibillError[]
struct UnibillErrorU5BU5D_t4092036522;
// System.Collections.Generic.IEnumerator`1<UnibillError>
struct IEnumerator_1_t3524350910;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2330523773_gshared (ReadOnlyCollection_1_t1939645479 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2330523773(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2330523773_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2925644571_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2925644571(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2925644571_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m844809087_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m844809087(__this, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m844809087_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1256841612_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1256841612(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1256841612_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3311325538_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3311325538(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3311325538_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3834699816_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3834699816(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3834699816_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m903159164_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m903159164(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m903159164_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m346228759_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m346228759(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m346228759_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1288079947_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1288079947(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1288079947_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m845254042_gshared (ReadOnlyCollection_1_t1939645479 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m845254042(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m845254042_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1478211799_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1478211799(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1478211799_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1335137374_gshared (ReadOnlyCollection_1_t1939645479 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1335137374(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1939645479 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1335137374_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3725317004_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3725317004(__this, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3725317004_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2480501096_gshared (ReadOnlyCollection_1_t1939645479 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2480501096(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2480501096_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2995211084_gshared (ReadOnlyCollection_1_t1939645479 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2995211084(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1939645479 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2995211084_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2049052927_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2049052927(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2049052927_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2990143983_gshared (ReadOnlyCollection_1_t1939645479 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2990143983(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2990143983_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1138058885_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1138058885(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1138058885_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2637322854_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2637322854(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2637322854_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m748899616_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m748899616(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m748899616_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1207856375_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1207856375(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1207856375_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m951197210_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m951197210(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m951197210_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3099254131_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3099254131(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3099254131_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1037141660_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1037141660(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1037141660_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2686257177_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2686257177(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m2686257177_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1246527871_gshared (ReadOnlyCollection_1_t1939645479 * __this, UnibillErrorU5BU5D_t4092036522* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1246527871(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1939645479 *, UnibillErrorU5BU5D_t4092036522*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1246527871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m4234824322_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m4234824322(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m4234824322_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1974162419_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1974162419(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1974162419_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m669922718_gshared (ReadOnlyCollection_1_t1939645479 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m669922718(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1939645479 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m669922718_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m478014848_gshared (ReadOnlyCollection_1_t1939645479 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m478014848(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1939645479 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m478014848_gshared)(__this, ___index0, method)
