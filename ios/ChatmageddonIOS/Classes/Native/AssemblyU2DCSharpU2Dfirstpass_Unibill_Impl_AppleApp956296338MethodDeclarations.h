﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.AppleAppStoreBillingService
struct AppleAppStoreBillingService_t956296338;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.IStoreKitPlugin
struct IStoreKitPlugin_t1675751103;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;
// PurchasableItem
struct PurchasableItem_t3963353899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void Unibill.Impl.AppleAppStoreBillingService::.ctor(Unibill.Impl.UnibillConfiguration,Unibill.Impl.ProductIdRemapper,Unibill.Impl.IStoreKitPlugin,Uniject.ILogger)
extern "C"  void AppleAppStoreBillingService__ctor_m189077292 (AppleAppStoreBillingService_t956296338 * __this, UnibillConfiguration_t2915611853 * ___db0, ProductIdRemapper_t3313438456 * ___mapper1, Il2CppObject * ___storekit2, Il2CppObject * ___logger3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IStoreKitPlugin Unibill.Impl.AppleAppStoreBillingService::get_storekit()
extern "C"  Il2CppObject * AppleAppStoreBillingService_get_storekit_m3834094675 (AppleAppStoreBillingService_t956296338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::set_storekit(Unibill.Impl.IStoreKitPlugin)
extern "C"  void AppleAppStoreBillingService_set_storekit_m1127607710 (AppleAppStoreBillingService_t956296338 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void AppleAppStoreBillingService_initialise_m366033400 (AppleAppStoreBillingService_t956296338 * __this, Il2CppObject * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::purchase(System.String,System.String)
extern "C"  void AppleAppStoreBillingService_purchase_m2199551903 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::restoreTransactions()
extern "C"  void AppleAppStoreBillingService_restoreTransactions_m3060055995 (AppleAppStoreBillingService_t956296338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onProductListReceived(System.String)
extern "C"  void AppleAppStoreBillingService_onProductListReceived_m3886656121 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___productListString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onPurchaseSucceeded(System.String)
extern "C"  void AppleAppStoreBillingService_onPurchaseSucceeded_m459024223 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onPurchaseCancelled(System.String)
extern "C"  void AppleAppStoreBillingService_onPurchaseCancelled_m3894501723 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onPurchaseFailed(System.String)
extern "C"  void AppleAppStoreBillingService_onPurchaseFailed_m3657809471 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onPurchaseDeferred(System.String)
extern "C"  void AppleAppStoreBillingService_onPurchaseDeferred_m3385740251 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onTransactionsRestoredSuccess()
extern "C"  void AppleAppStoreBillingService_onTransactionsRestoredSuccess_m592264445 (AppleAppStoreBillingService_t956296338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onTransactionsRestoredFail(System.String)
extern "C"  void AppleAppStoreBillingService_onTransactionsRestoredFail_m2085960536 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AppleAppStoreBillingService::onFailedToRetrieveProductList()
extern "C"  void AppleAppStoreBillingService_onFailedToRetrieveProductList_m1934610482 (AppleAppStoreBillingService_t956296338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.AppleAppStoreBillingService::hasReceipt(System.String)
extern "C"  bool AppleAppStoreBillingService_hasReceipt_m461037020 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.AppleAppStoreBillingService::getReceipt(System.String)
extern "C"  String_t* AppleAppStoreBillingService_getReceipt_m4237351317 (AppleAppStoreBillingService_t956296338 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.AppleAppStoreBillingService::<onProductListReceived>m__0(PurchasableItem)
extern "C"  String_t* AppleAppStoreBillingService_U3ConProductListReceivedU3Em__0_m466688440 (AppleAppStoreBillingService_t956296338 * __this, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
