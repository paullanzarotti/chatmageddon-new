﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardButton
struct LeaderboardButton_t679202217;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardButton::.ctor()
extern "C"  void LeaderboardButton__ctor_m3123941322 (LeaderboardButton_t679202217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardButton::OnButtonClick()
extern "C"  void LeaderboardButton_OnButtonClick_m3949350253 (LeaderboardButton_t679202217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
