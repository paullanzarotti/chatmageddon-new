﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumberNavigateBackButton
struct PhoneNumberNavigateBackButton_t3971219339;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void PhoneNumberNavigateBackButton::.ctor()
extern "C"  void PhoneNumberNavigateBackButton__ctor_m2184004608 (PhoneNumberNavigateBackButton_t3971219339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberNavigateBackButton::OnButtonClick()
extern "C"  void PhoneNumberNavigateBackButton_OnButtonClick_m1987300815 (PhoneNumberNavigateBackButton_t3971219339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberNavigateBackButton::SetButtonDirection(ButtonDirection)
extern "C"  void PhoneNumberNavigateBackButton_SetButtonDirection_m548073860 (PhoneNumberNavigateBackButton_t3971219339 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
