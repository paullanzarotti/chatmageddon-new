﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Water_Drop
struct CameraFilterPack_Distortion_Water_Drop_t844112946;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Water_Drop::.ctor()
extern "C"  void CameraFilterPack_Distortion_Water_Drop__ctor_m800986191 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Water_Drop::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Water_Drop_get_material_m2751164528 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::Start()
extern "C"  void CameraFilterPack_Distortion_Water_Drop_Start_m1630152339 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Water_Drop_OnRenderImage_m770837067 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Water_Drop_OnValidate_m441172558 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::Update()
extern "C"  void CameraFilterPack_Distortion_Water_Drop_Update_m2949532736 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Water_Drop_OnDisable_m2626537366 (CameraFilterPack_Distortion_Water_Drop_t844112946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
