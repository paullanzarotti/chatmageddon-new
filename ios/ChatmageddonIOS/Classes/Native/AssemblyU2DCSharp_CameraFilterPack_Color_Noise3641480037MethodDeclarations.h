﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_Noise
struct CameraFilterPack_Color_Noise_t3641480037;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_Noise::.ctor()
extern "C"  void CameraFilterPack_Color_Noise__ctor_m799539856 (CameraFilterPack_Color_Noise_t3641480037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Noise::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_Noise_get_material_m982047093 (CameraFilterPack_Color_Noise_t3641480037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::Start()
extern "C"  void CameraFilterPack_Color_Noise_Start_m4044034960 (CameraFilterPack_Color_Noise_t3641480037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_Noise_OnRenderImage_m1008698408 (CameraFilterPack_Color_Noise_t3641480037 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::OnValidate()
extern "C"  void CameraFilterPack_Color_Noise_OnValidate_m2823627787 (CameraFilterPack_Color_Noise_t3641480037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::Update()
extern "C"  void CameraFilterPack_Color_Noise_Update_m2217037937 (CameraFilterPack_Color_Noise_t3641480037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::OnDisable()
extern "C"  void CameraFilterPack_Color_Noise_OnDisable_m3243562149 (CameraFilterPack_Color_Noise_t3641480037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
