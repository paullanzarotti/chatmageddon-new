﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_VHS_Rewind
struct CameraFilterPack_TV_VHS_Rewind_t3553104481;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_VHS_Rewind::.ctor()
extern "C"  void CameraFilterPack_TV_VHS_Rewind__ctor_m1196282954 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_VHS_Rewind::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_VHS_Rewind_get_material_m4022952573 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::Start()
extern "C"  void CameraFilterPack_TV_VHS_Rewind_Start_m1950102402 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_VHS_Rewind_OnRenderImage_m4152806618 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::OnValidate()
extern "C"  void CameraFilterPack_TV_VHS_Rewind_OnValidate_m2188327247 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::Update()
extern "C"  void CameraFilterPack_TV_VHS_Rewind_Update_m1057027233 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::OnDisable()
extern "C"  void CameraFilterPack_TV_VHS_Rewind_OnDisable_m3746491797 (CameraFilterPack_TV_VHS_Rewind_t3553104481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
