﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendManager/<SendFriendRequest>c__AnonStorey0
struct U3CSendFriendRequestU3Ec__AnonStorey0_t1266135576;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FriendManager/<SendFriendRequest>c__AnonStorey0::.ctor()
extern "C"  void U3CSendFriendRequestU3Ec__AnonStorey0__ctor_m2131044031 (U3CSendFriendRequestU3Ec__AnonStorey0_t1266135576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager/<SendFriendRequest>c__AnonStorey0::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CSendFriendRequestU3Ec__AnonStorey0_U3CU3Em__0_m1303920685 (U3CSendFriendRequestU3Ec__AnonStorey0_t1266135576 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
