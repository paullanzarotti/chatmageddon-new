﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<OnlineMapsMarker3D>
struct Action_1_t378614921;
// OnlineMapsControlBase3D
struct OnlineMapsControlBase3D_t2801313279;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsMarker3D
struct  OnlineMapsMarker3D_t576815539  : public OnlineMapsMarkerBase_t3900955221
{
public:
	// System.Action`1<OnlineMapsMarker3D> OnlineMapsMarker3D::OnPositionChanged
	Action_1_t378614921 * ___OnPositionChanged_17;
	// System.Boolean OnlineMapsMarker3D::allowDefaultMarkerEvents
	bool ___allowDefaultMarkerEvents_18;
	// OnlineMapsControlBase3D OnlineMapsMarker3D::control
	OnlineMapsControlBase3D_t2801313279 * ___control_19;
	// System.Boolean OnlineMapsMarker3D::inited
	bool ___inited_20;
	// UnityEngine.GameObject OnlineMapsMarker3D::instance
	GameObject_t1756533147 * ___instance_21;
	// UnityEngine.GameObject OnlineMapsMarker3D::prefab
	GameObject_t1756533147 * ___prefab_22;
	// UnityEngine.GameObject OnlineMapsMarker3D::_prefab
	GameObject_t1756533147 * ____prefab_23;
	// UnityEngine.Vector3 OnlineMapsMarker3D::_relativePosition
	Vector3_t2243707580  ____relativePosition_24;

public:
	inline static int32_t get_offset_of_OnPositionChanged_17() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ___OnPositionChanged_17)); }
	inline Action_1_t378614921 * get_OnPositionChanged_17() const { return ___OnPositionChanged_17; }
	inline Action_1_t378614921 ** get_address_of_OnPositionChanged_17() { return &___OnPositionChanged_17; }
	inline void set_OnPositionChanged_17(Action_1_t378614921 * value)
	{
		___OnPositionChanged_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnPositionChanged_17, value);
	}

	inline static int32_t get_offset_of_allowDefaultMarkerEvents_18() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ___allowDefaultMarkerEvents_18)); }
	inline bool get_allowDefaultMarkerEvents_18() const { return ___allowDefaultMarkerEvents_18; }
	inline bool* get_address_of_allowDefaultMarkerEvents_18() { return &___allowDefaultMarkerEvents_18; }
	inline void set_allowDefaultMarkerEvents_18(bool value)
	{
		___allowDefaultMarkerEvents_18 = value;
	}

	inline static int32_t get_offset_of_control_19() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ___control_19)); }
	inline OnlineMapsControlBase3D_t2801313279 * get_control_19() const { return ___control_19; }
	inline OnlineMapsControlBase3D_t2801313279 ** get_address_of_control_19() { return &___control_19; }
	inline void set_control_19(OnlineMapsControlBase3D_t2801313279 * value)
	{
		___control_19 = value;
		Il2CppCodeGenWriteBarrier(&___control_19, value);
	}

	inline static int32_t get_offset_of_inited_20() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ___inited_20)); }
	inline bool get_inited_20() const { return ___inited_20; }
	inline bool* get_address_of_inited_20() { return &___inited_20; }
	inline void set_inited_20(bool value)
	{
		___inited_20 = value;
	}

	inline static int32_t get_offset_of_instance_21() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ___instance_21)); }
	inline GameObject_t1756533147 * get_instance_21() const { return ___instance_21; }
	inline GameObject_t1756533147 ** get_address_of_instance_21() { return &___instance_21; }
	inline void set_instance_21(GameObject_t1756533147 * value)
	{
		___instance_21 = value;
		Il2CppCodeGenWriteBarrier(&___instance_21, value);
	}

	inline static int32_t get_offset_of_prefab_22() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ___prefab_22)); }
	inline GameObject_t1756533147 * get_prefab_22() const { return ___prefab_22; }
	inline GameObject_t1756533147 ** get_address_of_prefab_22() { return &___prefab_22; }
	inline void set_prefab_22(GameObject_t1756533147 * value)
	{
		___prefab_22 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_22, value);
	}

	inline static int32_t get_offset_of__prefab_23() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ____prefab_23)); }
	inline GameObject_t1756533147 * get__prefab_23() const { return ____prefab_23; }
	inline GameObject_t1756533147 ** get_address_of__prefab_23() { return &____prefab_23; }
	inline void set__prefab_23(GameObject_t1756533147 * value)
	{
		____prefab_23 = value;
		Il2CppCodeGenWriteBarrier(&____prefab_23, value);
	}

	inline static int32_t get_offset_of__relativePosition_24() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3D_t576815539, ____relativePosition_24)); }
	inline Vector3_t2243707580  get__relativePosition_24() const { return ____relativePosition_24; }
	inline Vector3_t2243707580 * get_address_of__relativePosition_24() { return &____relativePosition_24; }
	inline void set__relativePosition_24(Vector3_t2243707580  value)
	{
		____relativePosition_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
