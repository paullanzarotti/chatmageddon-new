﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;

#include "codegen/il2cpp-codegen.h"

// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::AESKeyWrapEncrypt(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_AESKeyWrapEncrypt_m3225433037 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___rgbKey0, ByteU5BU5D_t3397334013* ___rgbWrappedKeyData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::AESKeyWrapDecrypt(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_AESKeyWrapDecrypt_m3544690603 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___rgbKey0, ByteU5BU5D_t3397334013* ___rgbEncryptedWrappedKeyData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::TripleDESKeyWrapEncrypt(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_TripleDESKeyWrapEncrypt_m2927479150 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___rgbKey0, ByteU5BU5D_t3397334013* ___rgbWrappedKeyData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::TripleDESKeyWrapDecrypt(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_TripleDESKeyWrapDecrypt_m2562089098 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___rgbKey0, ByteU5BU5D_t3397334013* ___rgbEncryptedWrappedKeyData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::Transform(System.Byte[],System.Security.Cryptography.ICryptoTransform)
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_Transform_m3397198218 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, Il2CppObject * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::ComputeCMSKeyChecksum(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_ComputeCMSKeyChecksum_m28389111 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::Concatenate(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_Concatenate_m959220249 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___buf10, ByteU5BU5D_t3397334013* ___buf21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::MSB(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_MSB_m2133958833 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::MSB(System.Byte[],System.Int32)
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_MSB_m1249662780 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___input0, int32_t ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::LSB(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_LSB_m4087890956 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::LSB(System.Byte[],System.Int32)
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_LSB_m4008668737 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___input0, int32_t ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.SymmetricKeyWrap::Xor(System.Byte[],System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* SymmetricKeyWrap_Xor_m3895594427 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___x0, ByteU5BU5D_t3397334013* ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
