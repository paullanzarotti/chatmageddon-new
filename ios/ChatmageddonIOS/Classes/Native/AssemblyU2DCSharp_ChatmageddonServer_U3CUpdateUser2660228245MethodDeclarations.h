﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserDisplayLocation>c__AnonStorey17
struct U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_t2660228245;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserDisplayLocation>c__AnonStorey17::.ctor()
extern "C"  void U3CUpdateUserDisplayLocationU3Ec__AnonStorey17__ctor_m231157322 (U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_t2660228245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserDisplayLocation>c__AnonStorey17::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_U3CU3Em__0_m2278155321 (U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_t2660228245 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
