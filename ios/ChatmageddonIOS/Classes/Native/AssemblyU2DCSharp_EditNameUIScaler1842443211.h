﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditNameUIScaler
struct  EditNameUIScaler_t1842443211  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UILabel EditNameUIScaler::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_14;
	// UnityEngine.BoxCollider EditNameUIScaler::firstNameCollider
	BoxCollider_t22920061 * ___firstNameCollider_15;
	// UISprite EditNameUIScaler::firstNameDivider
	UISprite_t603616735 * ___firstNameDivider_16;
	// UILabel EditNameUIScaler::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_17;
	// UnityEngine.BoxCollider EditNameUIScaler::lastNameCollider
	BoxCollider_t22920061 * ___lastNameCollider_18;
	// UISprite EditNameUIScaler::lastNameDivider
	UISprite_t603616735 * ___lastNameDivider_19;

public:
	inline static int32_t get_offset_of_firstNameLabel_14() { return static_cast<int32_t>(offsetof(EditNameUIScaler_t1842443211, ___firstNameLabel_14)); }
	inline UILabel_t1795115428 * get_firstNameLabel_14() const { return ___firstNameLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_14() { return &___firstNameLabel_14; }
	inline void set_firstNameLabel_14(UILabel_t1795115428 * value)
	{
		___firstNameLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_14, value);
	}

	inline static int32_t get_offset_of_firstNameCollider_15() { return static_cast<int32_t>(offsetof(EditNameUIScaler_t1842443211, ___firstNameCollider_15)); }
	inline BoxCollider_t22920061 * get_firstNameCollider_15() const { return ___firstNameCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_firstNameCollider_15() { return &___firstNameCollider_15; }
	inline void set_firstNameCollider_15(BoxCollider_t22920061 * value)
	{
		___firstNameCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameCollider_15, value);
	}

	inline static int32_t get_offset_of_firstNameDivider_16() { return static_cast<int32_t>(offsetof(EditNameUIScaler_t1842443211, ___firstNameDivider_16)); }
	inline UISprite_t603616735 * get_firstNameDivider_16() const { return ___firstNameDivider_16; }
	inline UISprite_t603616735 ** get_address_of_firstNameDivider_16() { return &___firstNameDivider_16; }
	inline void set_firstNameDivider_16(UISprite_t603616735 * value)
	{
		___firstNameDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameDivider_16, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_17() { return static_cast<int32_t>(offsetof(EditNameUIScaler_t1842443211, ___lastNameLabel_17)); }
	inline UILabel_t1795115428 * get_lastNameLabel_17() const { return ___lastNameLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_17() { return &___lastNameLabel_17; }
	inline void set_lastNameLabel_17(UILabel_t1795115428 * value)
	{
		___lastNameLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_17, value);
	}

	inline static int32_t get_offset_of_lastNameCollider_18() { return static_cast<int32_t>(offsetof(EditNameUIScaler_t1842443211, ___lastNameCollider_18)); }
	inline BoxCollider_t22920061 * get_lastNameCollider_18() const { return ___lastNameCollider_18; }
	inline BoxCollider_t22920061 ** get_address_of_lastNameCollider_18() { return &___lastNameCollider_18; }
	inline void set_lastNameCollider_18(BoxCollider_t22920061 * value)
	{
		___lastNameCollider_18 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameCollider_18, value);
	}

	inline static int32_t get_offset_of_lastNameDivider_19() { return static_cast<int32_t>(offsetof(EditNameUIScaler_t1842443211, ___lastNameDivider_19)); }
	inline UISprite_t603616735 * get_lastNameDivider_19() const { return ___lastNameDivider_19; }
	inline UISprite_t603616735 ** get_address_of_lastNameDivider_19() { return &___lastNameDivider_19; }
	inline void set_lastNameDivider_19(UISprite_t603616735 * value)
	{
		___lastNameDivider_19 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameDivider_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
