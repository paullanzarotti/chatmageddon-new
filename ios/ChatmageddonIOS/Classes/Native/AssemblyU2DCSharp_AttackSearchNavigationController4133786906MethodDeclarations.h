﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchNavigationController
struct AttackSearchNavigationController_t4133786906;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackSearchNavigationController::.ctor()
extern "C"  void AttackSearchNavigationController__ctor_m2061561543 (AttackSearchNavigationController_t4133786906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigationController::Start()
extern "C"  void AttackSearchNavigationController_Start_m2892352315 (AttackSearchNavigationController_t4133786906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigationController::NavigateBackwards()
extern "C"  void AttackSearchNavigationController_NavigateBackwards_m3059954670 (AttackSearchNavigationController_t4133786906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigationController::NavigateForwards(AttackSearchNav)
extern "C"  void AttackSearchNavigationController_NavigateForwards_m1002562595 (AttackSearchNavigationController_t4133786906 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackSearchNavigationController::ValidateNavigation(AttackSearchNav,NavigationDirection)
extern "C"  bool AttackSearchNavigationController_ValidateNavigation_m222970023 (AttackSearchNavigationController_t4133786906 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigationController::LoadNavScreen(AttackSearchNav,NavigationDirection,System.Boolean)
extern "C"  void AttackSearchNavigationController_LoadNavScreen_m2039459763 (AttackSearchNavigationController_t4133786906 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigationController::UnloadNavScreen(AttackSearchNav,NavigationDirection)
extern "C"  void AttackSearchNavigationController_UnloadNavScreen_m3261339511 (AttackSearchNavigationController_t4133786906 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigationController::PopulateTitleBar(AttackSearchNav)
extern "C"  void AttackSearchNavigationController_PopulateTitleBar_m1350607931 (AttackSearchNavigationController_t4133786906 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
