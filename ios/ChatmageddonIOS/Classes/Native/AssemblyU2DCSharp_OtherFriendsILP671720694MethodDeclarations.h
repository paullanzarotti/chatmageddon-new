﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherFriendsILP
struct OtherFriendsILP_t671720694;
// System.String
struct String_t;
// OtherFriend
struct OtherFriend_t3976068630;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OtherFriend3976068630.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void OtherFriendsILP::.ctor()
extern "C"  void OtherFriendsILP__ctor_m129710707 (OtherFriendsILP_t671720694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsILP::Start()
extern "C"  void OtherFriendsILP_Start_m3297736359 (OtherFriendsILP_t671720694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsILP::StartIL()
extern "C"  void OtherFriendsILP_StartIL_m1593574828 (OtherFriendsILP_t671720694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsILP::SetDataArray()
extern "C"  void OtherFriendsILP_SetDataArray_m727663684 (OtherFriendsILP_t671720694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OtherFriendsILP::CheckMobileHashExists(System.String)
extern "C"  bool OtherFriendsILP_CheckMobileHashExists_m352254229 (OtherFriendsILP_t671720694 * __this, String_t* ___mobileHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsILP::OpenMultiNumberUI(OtherFriend,System.Boolean)
extern "C"  void OtherFriendsILP_OpenMultiNumberUI_m746979536 (OtherFriendsILP_t671720694 * __this, OtherFriend_t3976068630 * ___oFriend0, bool ___attack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void OtherFriendsILP_InitListItemWithIndex_m2618758449 (OtherFriendsILP_t671720694 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendsILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void OtherFriendsILP_PopulateListItemWithIndex_m1410121542 (OtherFriendsILP_t671720694 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
