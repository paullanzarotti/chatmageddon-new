﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MultiPhoneNumberILP
struct MultiPhoneNumberILP_t3109982769;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseMultiPhoneNumberUI
struct  CloseMultiPhoneNumberUI_t914254952  : public SFXButton_t792651341
{
public:
	// MultiPhoneNumberILP CloseMultiPhoneNumberUI::multiNumberUI
	MultiPhoneNumberILP_t3109982769 * ___multiNumberUI_5;

public:
	inline static int32_t get_offset_of_multiNumberUI_5() { return static_cast<int32_t>(offsetof(CloseMultiPhoneNumberUI_t914254952, ___multiNumberUI_5)); }
	inline MultiPhoneNumberILP_t3109982769 * get_multiNumberUI_5() const { return ___multiNumberUI_5; }
	inline MultiPhoneNumberILP_t3109982769 ** get_address_of_multiNumberUI_5() { return &___multiNumberUI_5; }
	inline void set_multiNumberUI_5(MultiPhoneNumberILP_t3109982769 * value)
	{
		___multiNumberUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___multiNumberUI_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
