﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectAudienceSwitchButton
struct SelectAudienceSwitchButton_t3845187086;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectAudienceSwitchButton::.ctor()
extern "C"  void SelectAudienceSwitchButton__ctor_m3908077287 (SelectAudienceSwitchButton_t3845187086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceSwitchButton::OnButtonClick()
extern "C"  void SelectAudienceSwitchButton_OnButtonClick_m3404265394 (SelectAudienceSwitchButton_t3845187086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
