﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchedItem
struct LaunchedItem_t3670634427;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// MultiImageNGUIAnimation
struct MultiImageNGUIAnimation_t45560503;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.Transform
struct Transform_t3275118058;
// TweenAlpha
struct TweenAlpha_t2421518635;
// MissileResultsChatButton
struct MissileResultsChatButton_t36425224;
// MissileResultsAttackButton
struct MissileResultsAttackButton_t3918897298;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultModalUI
struct  ResultModalUI_t969511824  : public ModalUI_t2568752073
{
public:
	// LaunchedItem ResultModalUI::item
	LaunchedItem_t3670634427 * ___item_3;
	// UnityEngine.GameObject ResultModalUI::currentItemUI
	GameObject_t1756533147 * ___currentItemUI_4;
	// UnityEngine.Color ResultModalUI::positiveColour
	Color_t2020392075  ___positiveColour_5;
	// UnityEngine.Color ResultModalUI::neutralColour
	Color_t2020392075  ___neutralColour_6;
	// UnityEngine.Color ResultModalUI::negativeColour
	Color_t2020392075  ___negativeColour_7;
	// AvatarUpdater ResultModalUI::avatar
	AvatarUpdater_t2404165026 * ___avatar_8;
	// MultiImageNGUIAnimation ResultModalUI::flameAnim
	MultiImageNGUIAnimation_t45560503 * ___flameAnim_9;
	// UnityEngine.Color ResultModalUI::avatarFadeColour
	Color_t2020392075  ___avatarFadeColour_10;
	// UILabel ResultModalUI::nameLabel
	UILabel_t1795115428 * ___nameLabel_11;
	// UILabel ResultModalUI::ptsLabel
	UILabel_t1795115428 * ___ptsLabel_12;
	// UILabel ResultModalUI::actionLabel
	UILabel_t1795115428 * ___actionLabel_13;
	// UILabel ResultModalUI::inLabel
	UILabel_t1795115428 * ___inLabel_14;
	// UILabel ResultModalUI::timeLabel
	UILabel_t1795115428 * ___timeLabel_15;
	// UnityEngine.Transform ResultModalUI::itemUI
	Transform_t3275118058 * ___itemUI_16;
	// UnityEngine.Transform ResultModalUI::resultLabels
	Transform_t3275118058 * ___resultLabels_17;
	// UILabel ResultModalUI::resultLabel
	UILabel_t1795115428 * ___resultLabel_18;
	// UILabel ResultModalUI::resultPointsLabel
	UILabel_t1795115428 * ___resultPointsLabel_19;
	// UILabel ResultModalUI::resultStateLabel
	UILabel_t1795115428 * ___resultStateLabel_20;
	// TweenAlpha ResultModalUI::resultStateTween
	TweenAlpha_t2421518635 * ___resultStateTween_21;
	// MissileResultsChatButton ResultModalUI::chatButton
	MissileResultsChatButton_t36425224 * ___chatButton_22;
	// MissileResultsAttackButton ResultModalUI::attackButton
	MissileResultsAttackButton_t3918897298 * ___attackButton_23;

public:
	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___item_3)); }
	inline LaunchedItem_t3670634427 * get_item_3() const { return ___item_3; }
	inline LaunchedItem_t3670634427 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(LaunchedItem_t3670634427 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier(&___item_3, value);
	}

	inline static int32_t get_offset_of_currentItemUI_4() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___currentItemUI_4)); }
	inline GameObject_t1756533147 * get_currentItemUI_4() const { return ___currentItemUI_4; }
	inline GameObject_t1756533147 ** get_address_of_currentItemUI_4() { return &___currentItemUI_4; }
	inline void set_currentItemUI_4(GameObject_t1756533147 * value)
	{
		___currentItemUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentItemUI_4, value);
	}

	inline static int32_t get_offset_of_positiveColour_5() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___positiveColour_5)); }
	inline Color_t2020392075  get_positiveColour_5() const { return ___positiveColour_5; }
	inline Color_t2020392075 * get_address_of_positiveColour_5() { return &___positiveColour_5; }
	inline void set_positiveColour_5(Color_t2020392075  value)
	{
		___positiveColour_5 = value;
	}

	inline static int32_t get_offset_of_neutralColour_6() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___neutralColour_6)); }
	inline Color_t2020392075  get_neutralColour_6() const { return ___neutralColour_6; }
	inline Color_t2020392075 * get_address_of_neutralColour_6() { return &___neutralColour_6; }
	inline void set_neutralColour_6(Color_t2020392075  value)
	{
		___neutralColour_6 = value;
	}

	inline static int32_t get_offset_of_negativeColour_7() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___negativeColour_7)); }
	inline Color_t2020392075  get_negativeColour_7() const { return ___negativeColour_7; }
	inline Color_t2020392075 * get_address_of_negativeColour_7() { return &___negativeColour_7; }
	inline void set_negativeColour_7(Color_t2020392075  value)
	{
		___negativeColour_7 = value;
	}

	inline static int32_t get_offset_of_avatar_8() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___avatar_8)); }
	inline AvatarUpdater_t2404165026 * get_avatar_8() const { return ___avatar_8; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_8() { return &___avatar_8; }
	inline void set_avatar_8(AvatarUpdater_t2404165026 * value)
	{
		___avatar_8 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_8, value);
	}

	inline static int32_t get_offset_of_flameAnim_9() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___flameAnim_9)); }
	inline MultiImageNGUIAnimation_t45560503 * get_flameAnim_9() const { return ___flameAnim_9; }
	inline MultiImageNGUIAnimation_t45560503 ** get_address_of_flameAnim_9() { return &___flameAnim_9; }
	inline void set_flameAnim_9(MultiImageNGUIAnimation_t45560503 * value)
	{
		___flameAnim_9 = value;
		Il2CppCodeGenWriteBarrier(&___flameAnim_9, value);
	}

	inline static int32_t get_offset_of_avatarFadeColour_10() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___avatarFadeColour_10)); }
	inline Color_t2020392075  get_avatarFadeColour_10() const { return ___avatarFadeColour_10; }
	inline Color_t2020392075 * get_address_of_avatarFadeColour_10() { return &___avatarFadeColour_10; }
	inline void set_avatarFadeColour_10(Color_t2020392075  value)
	{
		___avatarFadeColour_10 = value;
	}

	inline static int32_t get_offset_of_nameLabel_11() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___nameLabel_11)); }
	inline UILabel_t1795115428 * get_nameLabel_11() const { return ___nameLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_11() { return &___nameLabel_11; }
	inline void set_nameLabel_11(UILabel_t1795115428 * value)
	{
		___nameLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_11, value);
	}

	inline static int32_t get_offset_of_ptsLabel_12() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___ptsLabel_12)); }
	inline UILabel_t1795115428 * get_ptsLabel_12() const { return ___ptsLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_ptsLabel_12() { return &___ptsLabel_12; }
	inline void set_ptsLabel_12(UILabel_t1795115428 * value)
	{
		___ptsLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___ptsLabel_12, value);
	}

	inline static int32_t get_offset_of_actionLabel_13() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___actionLabel_13)); }
	inline UILabel_t1795115428 * get_actionLabel_13() const { return ___actionLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_actionLabel_13() { return &___actionLabel_13; }
	inline void set_actionLabel_13(UILabel_t1795115428 * value)
	{
		___actionLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_13, value);
	}

	inline static int32_t get_offset_of_inLabel_14() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___inLabel_14)); }
	inline UILabel_t1795115428 * get_inLabel_14() const { return ___inLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_inLabel_14() { return &___inLabel_14; }
	inline void set_inLabel_14(UILabel_t1795115428 * value)
	{
		___inLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___inLabel_14, value);
	}

	inline static int32_t get_offset_of_timeLabel_15() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___timeLabel_15)); }
	inline UILabel_t1795115428 * get_timeLabel_15() const { return ___timeLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_15() { return &___timeLabel_15; }
	inline void set_timeLabel_15(UILabel_t1795115428 * value)
	{
		___timeLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_15, value);
	}

	inline static int32_t get_offset_of_itemUI_16() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___itemUI_16)); }
	inline Transform_t3275118058 * get_itemUI_16() const { return ___itemUI_16; }
	inline Transform_t3275118058 ** get_address_of_itemUI_16() { return &___itemUI_16; }
	inline void set_itemUI_16(Transform_t3275118058 * value)
	{
		___itemUI_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemUI_16, value);
	}

	inline static int32_t get_offset_of_resultLabels_17() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___resultLabels_17)); }
	inline Transform_t3275118058 * get_resultLabels_17() const { return ___resultLabels_17; }
	inline Transform_t3275118058 ** get_address_of_resultLabels_17() { return &___resultLabels_17; }
	inline void set_resultLabels_17(Transform_t3275118058 * value)
	{
		___resultLabels_17 = value;
		Il2CppCodeGenWriteBarrier(&___resultLabels_17, value);
	}

	inline static int32_t get_offset_of_resultLabel_18() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___resultLabel_18)); }
	inline UILabel_t1795115428 * get_resultLabel_18() const { return ___resultLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_resultLabel_18() { return &___resultLabel_18; }
	inline void set_resultLabel_18(UILabel_t1795115428 * value)
	{
		___resultLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___resultLabel_18, value);
	}

	inline static int32_t get_offset_of_resultPointsLabel_19() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___resultPointsLabel_19)); }
	inline UILabel_t1795115428 * get_resultPointsLabel_19() const { return ___resultPointsLabel_19; }
	inline UILabel_t1795115428 ** get_address_of_resultPointsLabel_19() { return &___resultPointsLabel_19; }
	inline void set_resultPointsLabel_19(UILabel_t1795115428 * value)
	{
		___resultPointsLabel_19 = value;
		Il2CppCodeGenWriteBarrier(&___resultPointsLabel_19, value);
	}

	inline static int32_t get_offset_of_resultStateLabel_20() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___resultStateLabel_20)); }
	inline UILabel_t1795115428 * get_resultStateLabel_20() const { return ___resultStateLabel_20; }
	inline UILabel_t1795115428 ** get_address_of_resultStateLabel_20() { return &___resultStateLabel_20; }
	inline void set_resultStateLabel_20(UILabel_t1795115428 * value)
	{
		___resultStateLabel_20 = value;
		Il2CppCodeGenWriteBarrier(&___resultStateLabel_20, value);
	}

	inline static int32_t get_offset_of_resultStateTween_21() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___resultStateTween_21)); }
	inline TweenAlpha_t2421518635 * get_resultStateTween_21() const { return ___resultStateTween_21; }
	inline TweenAlpha_t2421518635 ** get_address_of_resultStateTween_21() { return &___resultStateTween_21; }
	inline void set_resultStateTween_21(TweenAlpha_t2421518635 * value)
	{
		___resultStateTween_21 = value;
		Il2CppCodeGenWriteBarrier(&___resultStateTween_21, value);
	}

	inline static int32_t get_offset_of_chatButton_22() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___chatButton_22)); }
	inline MissileResultsChatButton_t36425224 * get_chatButton_22() const { return ___chatButton_22; }
	inline MissileResultsChatButton_t36425224 ** get_address_of_chatButton_22() { return &___chatButton_22; }
	inline void set_chatButton_22(MissileResultsChatButton_t36425224 * value)
	{
		___chatButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___chatButton_22, value);
	}

	inline static int32_t get_offset_of_attackButton_23() { return static_cast<int32_t>(offsetof(ResultModalUI_t969511824, ___attackButton_23)); }
	inline MissileResultsAttackButton_t3918897298 * get_attackButton_23() const { return ___attackButton_23; }
	inline MissileResultsAttackButton_t3918897298 ** get_address_of_attackButton_23() { return &___attackButton_23; }
	inline void set_attackButton_23(MissileResultsAttackButton_t3918897298 * value)
	{
		___attackButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___attackButton_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
