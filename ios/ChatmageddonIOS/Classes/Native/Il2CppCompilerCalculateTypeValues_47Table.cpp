﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_F1115807031.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_P3558640258.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_Pr143967646.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Brightnes697012433.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_DarkColo3998561101.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_HSV1033920197.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_HUE_Rota1922462430.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_NewPoster493511893.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Threshold416943559.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_BigF2835595270.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Blac1661860308.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Dissi959159612.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Dream823945338.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Drea4230247840.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Fish1356577622.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Flag220877849.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Flus3189943185.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Half2025856470.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Heat1800516877.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Lens4173169989.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Nois3048789779.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Shoc1472034506.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Water844112946.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Wave2357197985.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CellShad478413216.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CellSha2201161330.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Comics1419606548.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Crossha3123719876.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Enhance4277757686.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Halfton1044673501.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Laplacia213511471.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga2205931982.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga23914194624.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga33914194625.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga43914194626.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga53914194627.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga_C3082198838.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_NewCell1609771822.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Toon3523968834.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_BlackLine301124114.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Edge_filte2364446753.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Golden3392862448.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Neon2536731203.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Sigmoid3119666199.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Sobel3042228618.h"
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_11036579273.h"
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_21036579270.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits1352499132.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits_gb2819315972.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Ascii2132038257.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Dot_Circle950732968.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk4195520962.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk23160974908.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_EarthQuake694922649.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Funk3816888158.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch163583076.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch363583074.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Grid3480861000.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon382883320.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon_Blac1004384256.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hypno1332127830.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_InverChromiLu695765168.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Mirror1330003333.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Plasma932080402.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Psycho3893392668.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Screens1869481571.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Spot1081357216.h"
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ZebraColor3324297331.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Film_ColorPerfe1035791658.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Film_Grain1407189687.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Ansi4141626376.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Deser1681323834.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Elect1587039484.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_FireG1659791133.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Hue363696739.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_NeonG2703802219.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Rainb3966350505.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Strip3929419426.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Tech464753049.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Therm1493044570.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow1179232882.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow21290162980.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water3954840705.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water23567750323.h"
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX1351189223.h"
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_p1426196141.h"
#include "AssemblyU2DCSharp_CameraFilterPack_NightVision_44012815426.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVis3199654410.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVis2796369883.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVisio67486528.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVis1230285942.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_ThermaVi1714024826.h"
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting1382199430.h"
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting1382199431.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Pixel_Pixelisat2392528693.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_Dot214826704.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_Oi2695555097.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_Oi1416101270.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Sharpen_Sharpen1039912.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Special_Bubble2761025777.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4700 = { sizeof (CameraFilterPack_Colors_Adjust_FullColors_t1115807031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4700[16] = 
{
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_TimeX_3(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Red_R_6(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Red_G_7(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Red_B_8(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Red_Constant_9(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Green_R_10(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Green_G_11(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Green_B_12(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Green_Constant_13(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Blue_R_14(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Blue_G_15(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Blue_B_16(),
	CameraFilterPack_Colors_Adjust_FullColors_t1115807031::get_offset_of_Blue_Constant_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4701 = { sizeof (CameraFilterPack_Colors_Adjust_PreFilters_t3558640258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4701[8] = 
{
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_ShaderName_2(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_SCShader_3(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_filterchoice_4(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_FadeFX_5(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_TimeX_6(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_ScreenResolution_7(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_SCMaterial_8(),
	CameraFilterPack_Colors_Adjust_PreFilters_t3558640258::get_offset_of_Matrix9_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4702 = { sizeof (filters_t143967646)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4702[33] = 
{
	filters_t143967646::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4703 = { sizeof (CameraFilterPack_Colors_Brightness_t697012433), -1, sizeof(CameraFilterPack_Colors_Brightness_t697012433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4703[4] = 
{
	CameraFilterPack_Colors_Brightness_t697012433::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_Brightness_t697012433::get_offset_of__Brightness_3(),
	CameraFilterPack_Colors_Brightness_t697012433::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Colors_Brightness_t697012433_StaticFields::get_offset_of_ChangeBrightness_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4704 = { sizeof (CameraFilterPack_Colors_DarkColor_t3998561101), -1, sizeof(CameraFilterPack_Colors_DarkColor_t3998561101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4704[12] = 
{
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_TimeX_3(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_Alpha_6(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_Colors_7(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_Green_Mod_8(),
	CameraFilterPack_Colors_DarkColor_t3998561101::get_offset_of_Value4_9(),
	CameraFilterPack_Colors_DarkColor_t3998561101_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Colors_DarkColor_t3998561101_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Colors_DarkColor_t3998561101_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Colors_DarkColor_t3998561101_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4705 = { sizeof (CameraFilterPack_Colors_HSV_t1033920197), -1, sizeof(CameraFilterPack_Colors_HSV_t1033920197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4705[8] = 
{
	CameraFilterPack_Colors_HSV_t1033920197::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_HSV_t1033920197::get_offset_of__HueShift_3(),
	CameraFilterPack_Colors_HSV_t1033920197::get_offset_of__Saturation_4(),
	CameraFilterPack_Colors_HSV_t1033920197::get_offset_of__ValueBrightness_5(),
	CameraFilterPack_Colors_HSV_t1033920197::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Colors_HSV_t1033920197_StaticFields::get_offset_of_ChangeHueShift_7(),
	CameraFilterPack_Colors_HSV_t1033920197_StaticFields::get_offset_of_ChangeSaturation_8(),
	CameraFilterPack_Colors_HSV_t1033920197_StaticFields::get_offset_of_ChangeValueBrightness_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4706 = { sizeof (CameraFilterPack_Colors_HUE_Rotate_t1922462430), -1, sizeof(CameraFilterPack_Colors_HUE_Rotate_t1922462430_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4706[6] = 
{
	CameraFilterPack_Colors_HUE_Rotate_t1922462430::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_HUE_Rotate_t1922462430::get_offset_of_TimeX_3(),
	CameraFilterPack_Colors_HUE_Rotate_t1922462430::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Colors_HUE_Rotate_t1922462430::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Colors_HUE_Rotate_t1922462430::get_offset_of_Speed_6(),
	CameraFilterPack_Colors_HUE_Rotate_t1922462430_StaticFields::get_offset_of_ChangeSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4707 = { sizeof (CameraFilterPack_Colors_NewPosterize_t493511893), -1, sizeof(CameraFilterPack_Colors_NewPosterize_t493511893_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4707[12] = 
{
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_TimeX_3(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_Gamma_6(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_Colors_7(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_Green_Mod_8(),
	CameraFilterPack_Colors_NewPosterize_t493511893::get_offset_of_Value4_9(),
	CameraFilterPack_Colors_NewPosterize_t493511893_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Colors_NewPosterize_t493511893_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Colors_NewPosterize_t493511893_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Colors_NewPosterize_t493511893_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4708 = { sizeof (CameraFilterPack_Colors_Threshold_t416943559), -1, sizeof(CameraFilterPack_Colors_Threshold_t416943559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4708[5] = 
{
	CameraFilterPack_Colors_Threshold_t416943559::get_offset_of_SCShader_2(),
	CameraFilterPack_Colors_Threshold_t416943559::get_offset_of_TimeX_3(),
	CameraFilterPack_Colors_Threshold_t416943559::get_offset_of_Threshold_4(),
	CameraFilterPack_Colors_Threshold_t416943559::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Colors_Threshold_t416943559_StaticFields::get_offset_of_ChangeThreshold_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4709 = { sizeof (CameraFilterPack_Distortion_BigFace_t2835595270), -1, sizeof(CameraFilterPack_Distortion_BigFace_t2835595270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4709[8] = 
{
	CameraFilterPack_Distortion_BigFace_t2835595270::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_BigFace_t2835595270::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_BigFace_t2835595270::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_BigFace_t2835595270::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_BigFace_t2835595270::get_offset_of__Size_6(),
	CameraFilterPack_Distortion_BigFace_t2835595270::get_offset_of_Distortion_7(),
	CameraFilterPack_Distortion_BigFace_t2835595270_StaticFields::get_offset_of_ChangeSize_8(),
	CameraFilterPack_Distortion_BigFace_t2835595270_StaticFields::get_offset_of_ChangeDistortion_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4710 = { sizeof (CameraFilterPack_Distortion_BlackHole_t1661860308), -1, sizeof(CameraFilterPack_Distortion_BlackHole_t1661860308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4710[12] = 
{
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_PositionX_6(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_PositionY_7(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_Size_8(),
	CameraFilterPack_Distortion_BlackHole_t1661860308::get_offset_of_Distortion_9(),
	CameraFilterPack_Distortion_BlackHole_t1661860308_StaticFields::get_offset_of_ChangePositionX_10(),
	CameraFilterPack_Distortion_BlackHole_t1661860308_StaticFields::get_offset_of_ChangePositionY_11(),
	CameraFilterPack_Distortion_BlackHole_t1661860308_StaticFields::get_offset_of_ChangeSize_12(),
	CameraFilterPack_Distortion_BlackHole_t1661860308_StaticFields::get_offset_of_ChangeDistortion_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4711 = { sizeof (CameraFilterPack_Distortion_Dissipation_t959159612), -1, sizeof(CameraFilterPack_Distortion_Dissipation_t959159612_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4711[12] = 
{
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_Dissipation_6(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_Colors_7(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_Green_Mod_8(),
	CameraFilterPack_Distortion_Dissipation_t959159612::get_offset_of_Value4_9(),
	CameraFilterPack_Distortion_Dissipation_t959159612_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Distortion_Dissipation_t959159612_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Distortion_Dissipation_t959159612_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Distortion_Dissipation_t959159612_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4712 = { sizeof (CameraFilterPack_Distortion_Dream_t823945338), -1, sizeof(CameraFilterPack_Distortion_Dream_t823945338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4712[5] = 
{
	CameraFilterPack_Distortion_Dream_t823945338::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Dream_t823945338::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Dream_t823945338::get_offset_of_Distortion_4(),
	CameraFilterPack_Distortion_Dream_t823945338::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Dream_t823945338_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4713 = { sizeof (CameraFilterPack_Distortion_Dream2_t4230247840), -1, sizeof(CameraFilterPack_Distortion_Dream2_t4230247840_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4713[8] = 
{
	CameraFilterPack_Distortion_Dream2_t4230247840::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Dream2_t4230247840::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Dream2_t4230247840::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Dream2_t4230247840::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Dream2_t4230247840::get_offset_of_Distortion_6(),
	CameraFilterPack_Distortion_Dream2_t4230247840::get_offset_of_Speed_7(),
	CameraFilterPack_Distortion_Dream2_t4230247840_StaticFields::get_offset_of_ChangeDistortion_8(),
	CameraFilterPack_Distortion_Dream2_t4230247840_StaticFields::get_offset_of_ChangeSpeed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4714 = { sizeof (CameraFilterPack_Distortion_FishEye_t1356577622), -1, sizeof(CameraFilterPack_Distortion_FishEye_t1356577622_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4714[6] = 
{
	CameraFilterPack_Distortion_FishEye_t1356577622::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_FishEye_t1356577622::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_FishEye_t1356577622::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_FishEye_t1356577622::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_FishEye_t1356577622::get_offset_of_Distortion_6(),
	CameraFilterPack_Distortion_FishEye_t1356577622_StaticFields::get_offset_of_ChangeDistortion_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4715 = { sizeof (CameraFilterPack_Distortion_Flag_t220877849), -1, sizeof(CameraFilterPack_Distortion_Flag_t220877849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4715[6] = 
{
	CameraFilterPack_Distortion_Flag_t220877849::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Flag_t220877849::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Flag_t220877849::get_offset_of_Distortion_4(),
	CameraFilterPack_Distortion_Flag_t220877849::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Distortion_Flag_t220877849::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Distortion_Flag_t220877849_StaticFields::get_offset_of_ChangeDistortion_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4716 = { sizeof (CameraFilterPack_Distortion_Flush_t3189943185), -1, sizeof(CameraFilterPack_Distortion_Flush_t3189943185_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4716[12] = 
{
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_Size_6(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_LightBackGround_7(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_Speed_8(),
	CameraFilterPack_Distortion_Flush_t3189943185::get_offset_of_Size2_9(),
	CameraFilterPack_Distortion_Flush_t3189943185_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Distortion_Flush_t3189943185_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Distortion_Flush_t3189943185_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Distortion_Flush_t3189943185_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4717 = { sizeof (CameraFilterPack_Distortion_Half_Sphere_t2025856470), -1, sizeof(CameraFilterPack_Distortion_Half_Sphere_t2025856470_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4717[12] = 
{
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_SphereSize_6(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_SpherePositionX_7(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_SpherePositionY_8(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470::get_offset_of_Strength_9(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470_StaticFields::get_offset_of_ChangeSphereSize_10(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470_StaticFields::get_offset_of_ChangeSpherePositionX_11(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470_StaticFields::get_offset_of_ChangeSpherePositionY_12(),
	CameraFilterPack_Distortion_Half_Sphere_t2025856470_StaticFields::get_offset_of_ChangeStrength_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4718 = { sizeof (CameraFilterPack_Distortion_Heat_t1800516877), -1, sizeof(CameraFilterPack_Distortion_Heat_t1800516877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4718[6] = 
{
	CameraFilterPack_Distortion_Heat_t1800516877::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Heat_t1800516877::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Heat_t1800516877::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Heat_t1800516877::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Heat_t1800516877::get_offset_of_Distortion_6(),
	CameraFilterPack_Distortion_Heat_t1800516877_StaticFields::get_offset_of_ChangeDistortion_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4719 = { sizeof (CameraFilterPack_Distortion_Lens_t4173169989), -1, sizeof(CameraFilterPack_Distortion_Lens_t4173169989_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4719[10] = 
{
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_CenterX_6(),
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_CenterY_7(),
	CameraFilterPack_Distortion_Lens_t4173169989::get_offset_of_Distortion_8(),
	CameraFilterPack_Distortion_Lens_t4173169989_StaticFields::get_offset_of_ChangeCenterX_9(),
	CameraFilterPack_Distortion_Lens_t4173169989_StaticFields::get_offset_of_ChangeCenterY_10(),
	CameraFilterPack_Distortion_Lens_t4173169989_StaticFields::get_offset_of_ChangeDistortion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4720 = { sizeof (CameraFilterPack_Distortion_Noise_t3048789779), -1, sizeof(CameraFilterPack_Distortion_Noise_t3048789779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4720[6] = 
{
	CameraFilterPack_Distortion_Noise_t3048789779::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Noise_t3048789779::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Noise_t3048789779::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Noise_t3048789779::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Noise_t3048789779::get_offset_of_Distortion_6(),
	CameraFilterPack_Distortion_Noise_t3048789779_StaticFields::get_offset_of_ChangeDistortion_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4721 = { sizeof (CameraFilterPack_Distortion_ShockWave_t1472034506), -1, sizeof(CameraFilterPack_Distortion_ShockWave_t1472034506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4721[12] = 
{
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_PosX_6(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_PosY_7(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_Speed_8(),
	CameraFilterPack_Distortion_ShockWave_t1472034506::get_offset_of_Size_9(),
	CameraFilterPack_Distortion_ShockWave_t1472034506_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Distortion_ShockWave_t1472034506_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Distortion_ShockWave_t1472034506_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Distortion_ShockWave_t1472034506_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4722 = { sizeof (CameraFilterPack_Distortion_Water_Drop_t844112946), -1, sizeof(CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4722[12] = 
{
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_CenterX_6(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_CenterY_7(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_WaveIntensity_8(),
	CameraFilterPack_Distortion_Water_Drop_t844112946::get_offset_of_NumberOfWaves_9(),
	CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields::get_offset_of_ChangeCenterX_10(),
	CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields::get_offset_of_ChangeCenterY_11(),
	CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields::get_offset_of_ChangeWaveIntensity_12(),
	CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields::get_offset_of_ChangeNumberOfWaves_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4723 = { sizeof (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985), -1, sizeof(CameraFilterPack_Distortion_Wave_Horizontal_t2357197985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4723[6] = 
{
	CameraFilterPack_Distortion_Wave_Horizontal_t2357197985::get_offset_of_SCShader_2(),
	CameraFilterPack_Distortion_Wave_Horizontal_t2357197985::get_offset_of_TimeX_3(),
	CameraFilterPack_Distortion_Wave_Horizontal_t2357197985::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Distortion_Wave_Horizontal_t2357197985::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Distortion_Wave_Horizontal_t2357197985::get_offset_of_WaveIntensity_6(),
	CameraFilterPack_Distortion_Wave_Horizontal_t2357197985_StaticFields::get_offset_of_ChangeWaveIntensity_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4724 = { sizeof (CameraFilterPack_Drawing_CellShading_t478413216), -1, sizeof(CameraFilterPack_Drawing_CellShading_t478413216_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4724[8] = 
{
	CameraFilterPack_Drawing_CellShading_t478413216::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_CellShading_t478413216::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_CellShading_t478413216::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Drawing_CellShading_t478413216::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Drawing_CellShading_t478413216::get_offset_of_EdgeSize_6(),
	CameraFilterPack_Drawing_CellShading_t478413216::get_offset_of_ColorLevel_7(),
	CameraFilterPack_Drawing_CellShading_t478413216_StaticFields::get_offset_of_ChangeEdgeSize_8(),
	CameraFilterPack_Drawing_CellShading_t478413216_StaticFields::get_offset_of_ChangeColorLevel_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4725 = { sizeof (CameraFilterPack_Drawing_CellShading2_t2201161330), -1, sizeof(CameraFilterPack_Drawing_CellShading2_t2201161330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4725[9] = 
{
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_EdgeSize_6(),
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_ColorLevel_7(),
	CameraFilterPack_Drawing_CellShading2_t2201161330::get_offset_of_Blur_8(),
	CameraFilterPack_Drawing_CellShading2_t2201161330_StaticFields::get_offset_of_ChangeEdgeSize_9(),
	CameraFilterPack_Drawing_CellShading2_t2201161330_StaticFields::get_offset_of_ChangeColorLevel_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4726 = { sizeof (CameraFilterPack_Drawing_Comics_t1419606548), -1, sizeof(CameraFilterPack_Drawing_Comics_t1419606548_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4726[5] = 
{
	CameraFilterPack_Drawing_Comics_t1419606548::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Comics_t1419606548::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Comics_t1419606548::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Comics_t1419606548::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Comics_t1419606548_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4727 = { sizeof (CameraFilterPack_Drawing_Crosshatch_t3123719876), -1, sizeof(CameraFilterPack_Drawing_Crosshatch_t3123719876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4727[6] = 
{
	CameraFilterPack_Drawing_Crosshatch_t3123719876::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Crosshatch_t3123719876::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Crosshatch_t3123719876::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Drawing_Crosshatch_t3123719876::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Drawing_Crosshatch_t3123719876::get_offset_of_Width_6(),
	CameraFilterPack_Drawing_Crosshatch_t3123719876_StaticFields::get_offset_of_ChangeWidth_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4728 = { sizeof (CameraFilterPack_Drawing_EnhancedComics_t4277757686), -1, sizeof(CameraFilterPack_Drawing_EnhancedComics_t4277757686_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4728[12] = 
{
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of__ColorR_6(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of__ColorG_7(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of__ColorB_8(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of__Blood_9(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of__SmoothStart_10(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of__SmoothEnd_11(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686::get_offset_of_ColorRGB_12(),
	CameraFilterPack_Drawing_EnhancedComics_t4277757686_StaticFields::get_offset_of_ChangeDotSize_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4729 = { sizeof (CameraFilterPack_Drawing_Halftone_t1044673501), -1, sizeof(CameraFilterPack_Drawing_Halftone_t1044673501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4729[7] = 
{
	CameraFilterPack_Drawing_Halftone_t1044673501::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Halftone_t1044673501::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Halftone_t1044673501::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Halftone_t1044673501::get_offset_of_Threshold_5(),
	CameraFilterPack_Drawing_Halftone_t1044673501::get_offset_of_DotSize_6(),
	CameraFilterPack_Drawing_Halftone_t1044673501_StaticFields::get_offset_of_ChangeThreshold_7(),
	CameraFilterPack_Drawing_Halftone_t1044673501_StaticFields::get_offset_of_ChangeDotSize_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4730 = { sizeof (CameraFilterPack_Drawing_Laplacian_t213511471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4730[4] = 
{
	CameraFilterPack_Drawing_Laplacian_t213511471::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Laplacian_t213511471::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Laplacian_t213511471::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Drawing_Laplacian_t213511471::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4731 = { sizeof (CameraFilterPack_Drawing_Manga_t2205931982), -1, sizeof(CameraFilterPack_Drawing_Manga_t2205931982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4731[5] = 
{
	CameraFilterPack_Drawing_Manga_t2205931982::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Manga_t2205931982::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Manga_t2205931982::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Manga_t2205931982::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Manga_t2205931982_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4732 = { sizeof (CameraFilterPack_Drawing_Manga2_t3914194624), -1, sizeof(CameraFilterPack_Drawing_Manga2_t3914194624_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4732[5] = 
{
	CameraFilterPack_Drawing_Manga2_t3914194624::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Manga2_t3914194624::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Manga2_t3914194624::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Manga2_t3914194624::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Manga2_t3914194624_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4733 = { sizeof (CameraFilterPack_Drawing_Manga3_t3914194625), -1, sizeof(CameraFilterPack_Drawing_Manga3_t3914194625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4733[5] = 
{
	CameraFilterPack_Drawing_Manga3_t3914194625::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Manga3_t3914194625::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Manga3_t3914194625::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Manga3_t3914194625::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Manga3_t3914194625_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4734 = { sizeof (CameraFilterPack_Drawing_Manga4_t3914194626), -1, sizeof(CameraFilterPack_Drawing_Manga4_t3914194626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4734[5] = 
{
	CameraFilterPack_Drawing_Manga4_t3914194626::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Manga4_t3914194626::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Manga4_t3914194626::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Manga4_t3914194626::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Manga4_t3914194626_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4735 = { sizeof (CameraFilterPack_Drawing_Manga5_t3914194627), -1, sizeof(CameraFilterPack_Drawing_Manga5_t3914194627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4735[5] = 
{
	CameraFilterPack_Drawing_Manga5_t3914194627::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Manga5_t3914194627::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Manga5_t3914194627::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Manga5_t3914194627::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Manga5_t3914194627_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4736 = { sizeof (CameraFilterPack_Drawing_Manga_Color_t3082198838), -1, sizeof(CameraFilterPack_Drawing_Manga_Color_t3082198838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4736[5] = 
{
	CameraFilterPack_Drawing_Manga_Color_t3082198838::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Manga_Color_t3082198838::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_Manga_Color_t3082198838::get_offset_of_SCMaterial_4(),
	CameraFilterPack_Drawing_Manga_Color_t3082198838::get_offset_of_DotSize_5(),
	CameraFilterPack_Drawing_Manga_Color_t3082198838_StaticFields::get_offset_of_ChangeDotSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4737 = { sizeof (CameraFilterPack_Drawing_NewCellShading_t1609771822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4737[5] = 
{
	CameraFilterPack_Drawing_NewCellShading_t1609771822::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_NewCellShading_t1609771822::get_offset_of_TimeX_3(),
	CameraFilterPack_Drawing_NewCellShading_t1609771822::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Drawing_NewCellShading_t1609771822::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Drawing_NewCellShading_t1609771822::get_offset_of_Threshold_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4738 = { sizeof (CameraFilterPack_Drawing_Toon_t3523968834), -1, sizeof(CameraFilterPack_Drawing_Toon_t3523968834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4738[7] = 
{
	CameraFilterPack_Drawing_Toon_t3523968834::get_offset_of_SCShader_2(),
	CameraFilterPack_Drawing_Toon_t3523968834::get_offset_of_SCMaterial_3(),
	CameraFilterPack_Drawing_Toon_t3523968834::get_offset_of_TimeX_4(),
	CameraFilterPack_Drawing_Toon_t3523968834::get_offset_of_Threshold_5(),
	CameraFilterPack_Drawing_Toon_t3523968834::get_offset_of_DotSize_6(),
	CameraFilterPack_Drawing_Toon_t3523968834_StaticFields::get_offset_of_ChangeThreshold_7(),
	CameraFilterPack_Drawing_Toon_t3523968834_StaticFields::get_offset_of_ChangeDotSize_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4739 = { sizeof (CameraFilterPack_Edge_BlackLine_t301124114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4739[4] = 
{
	CameraFilterPack_Edge_BlackLine_t301124114::get_offset_of_SCShader_2(),
	CameraFilterPack_Edge_BlackLine_t301124114::get_offset_of_TimeX_3(),
	CameraFilterPack_Edge_BlackLine_t301124114::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Edge_BlackLine_t301124114::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4740 = { sizeof (CameraFilterPack_Edge_Edge_filter_t2364446753), -1, sizeof(CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4740[10] = 
{
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_SCShader_2(),
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_TimeX_3(),
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_RedAmplifier_6(),
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_GreenAmplifier_7(),
	CameraFilterPack_Edge_Edge_filter_t2364446753::get_offset_of_BlueAmplifier_8(),
	CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields::get_offset_of_ChangeRedAmplifier_9(),
	CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields::get_offset_of_ChangeGreenAmplifier_10(),
	CameraFilterPack_Edge_Edge_filter_t2364446753_StaticFields::get_offset_of_ChangeBlueAmplifier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4741 = { sizeof (CameraFilterPack_Edge_Golden_t3392862448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4741[4] = 
{
	CameraFilterPack_Edge_Golden_t3392862448::get_offset_of_SCShader_2(),
	CameraFilterPack_Edge_Golden_t3392862448::get_offset_of_TimeX_3(),
	CameraFilterPack_Edge_Golden_t3392862448::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Edge_Golden_t3392862448::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4742 = { sizeof (CameraFilterPack_Edge_Neon_t2536731203), -1, sizeof(CameraFilterPack_Edge_Neon_t2536731203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4742[6] = 
{
	CameraFilterPack_Edge_Neon_t2536731203::get_offset_of_SCShader_2(),
	CameraFilterPack_Edge_Neon_t2536731203::get_offset_of_TimeX_3(),
	CameraFilterPack_Edge_Neon_t2536731203::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Edge_Neon_t2536731203::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Edge_Neon_t2536731203::get_offset_of_EdgeWeight_6(),
	CameraFilterPack_Edge_Neon_t2536731203_StaticFields::get_offset_of_ChangeEdgeWeight_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4743 = { sizeof (CameraFilterPack_Edge_Sigmoid_t3119666199), -1, sizeof(CameraFilterPack_Edge_Sigmoid_t3119666199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4743[6] = 
{
	CameraFilterPack_Edge_Sigmoid_t3119666199::get_offset_of_SCShader_2(),
	CameraFilterPack_Edge_Sigmoid_t3119666199::get_offset_of_TimeX_3(),
	CameraFilterPack_Edge_Sigmoid_t3119666199::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Edge_Sigmoid_t3119666199::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Edge_Sigmoid_t3119666199::get_offset_of_Gain_6(),
	CameraFilterPack_Edge_Sigmoid_t3119666199_StaticFields::get_offset_of_ChangeGain_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4744 = { sizeof (CameraFilterPack_Edge_Sobel_t3042228618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4744[4] = 
{
	CameraFilterPack_Edge_Sobel_t3042228618::get_offset_of_SCShader_2(),
	CameraFilterPack_Edge_Sobel_t3042228618::get_offset_of_TimeX_3(),
	CameraFilterPack_Edge_Sobel_t3042228618::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Edge_Sobel_t3042228618::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4745 = { sizeof (CameraFilterPack_EyesVision_1_t1036579273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4745[8] = 
{
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of_SCShader_2(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of_TimeX_3(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of__EyeWave_4(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of__EyeSpeed_5(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of__EyeMove_6(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of__EyeBlink_7(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of_SCMaterial_8(),
	CameraFilterPack_EyesVision_1_t1036579273::get_offset_of_Texture2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4746 = { sizeof (CameraFilterPack_EyesVision_2_t1036579270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4746[8] = 
{
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of_SCShader_2(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of_TimeX_3(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of__EyeWave_4(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of__EyeSpeed_5(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of__EyeMove_6(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of__EyeBlink_7(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of_SCMaterial_8(),
	CameraFilterPack_EyesVision_2_t1036579270::get_offset_of_Texture2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4747 = { sizeof (CameraFilterPack_FX_8bits_t1352499132), -1, sizeof(CameraFilterPack_FX_8bits_t1352499132_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4747[9] = 
{
	CameraFilterPack_FX_8bits_t1352499132::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_8bits_t1352499132::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_8bits_t1352499132::get_offset_of_SCMaterial_4(),
	CameraFilterPack_FX_8bits_t1352499132::get_offset_of_Brightness_5(),
	CameraFilterPack_FX_8bits_t1352499132::get_offset_of_ResolutionX_6(),
	CameraFilterPack_FX_8bits_t1352499132::get_offset_of_ResolutionY_7(),
	CameraFilterPack_FX_8bits_t1352499132_StaticFields::get_offset_of_ChangeBrightness_8(),
	CameraFilterPack_FX_8bits_t1352499132_StaticFields::get_offset_of_ChangeResolutionX_9(),
	CameraFilterPack_FX_8bits_t1352499132_StaticFields::get_offset_of_ChangeResolutionY_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4748 = { sizeof (CameraFilterPack_FX_8bits_gb_t2819315972), -1, sizeof(CameraFilterPack_FX_8bits_gb_t2819315972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4748[5] = 
{
	CameraFilterPack_FX_8bits_gb_t2819315972::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_8bits_gb_t2819315972::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_8bits_gb_t2819315972::get_offset_of_SCMaterial_4(),
	CameraFilterPack_FX_8bits_gb_t2819315972::get_offset_of_Brightness_5(),
	CameraFilterPack_FX_8bits_gb_t2819315972_StaticFields::get_offset_of_ChangeBrightness_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4749 = { sizeof (CameraFilterPack_FX_Ascii_t2132038257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4749[4] = 
{
	CameraFilterPack_FX_Ascii_t2132038257::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Ascii_t2132038257::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Ascii_t2132038257::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Ascii_t2132038257::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4750 = { sizeof (CameraFilterPack_FX_Dot_Circle_t950732968), -1, sizeof(CameraFilterPack_FX_Dot_Circle_t950732968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4750[6] = 
{
	CameraFilterPack_FX_Dot_Circle_t950732968::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Dot_Circle_t950732968::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Dot_Circle_t950732968::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Dot_Circle_t950732968::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Dot_Circle_t950732968::get_offset_of_Value_6(),
	CameraFilterPack_FX_Dot_Circle_t950732968_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4751 = { sizeof (CameraFilterPack_FX_Drunk_t4195520962), -1, sizeof(CameraFilterPack_FX_Drunk_t4195520962_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4751[6] = 
{
	CameraFilterPack_FX_Drunk_t4195520962::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Drunk_t4195520962::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Drunk_t4195520962::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Drunk_t4195520962::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Drunk_t4195520962::get_offset_of_Value_6(),
	CameraFilterPack_FX_Drunk_t4195520962_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4752 = { sizeof (CameraFilterPack_FX_Drunk2_t3160974908), -1, sizeof(CameraFilterPack_FX_Drunk2_t3160974908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4752[12] = 
{
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_Value_6(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_Value2_7(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_Value3_8(),
	CameraFilterPack_FX_Drunk2_t3160974908::get_offset_of_Value4_9(),
	CameraFilterPack_FX_Drunk2_t3160974908_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_FX_Drunk2_t3160974908_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_FX_Drunk2_t3160974908_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_FX_Drunk2_t3160974908_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4753 = { sizeof (CameraFilterPack_FX_EarthQuake_t694922649), -1, sizeof(CameraFilterPack_FX_EarthQuake_t694922649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4753[12] = 
{
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_Speed_6(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_X_7(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_Y_8(),
	CameraFilterPack_FX_EarthQuake_t694922649::get_offset_of_Value4_9(),
	CameraFilterPack_FX_EarthQuake_t694922649_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_FX_EarthQuake_t694922649_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_FX_EarthQuake_t694922649_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_FX_EarthQuake_t694922649_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4754 = { sizeof (CameraFilterPack_FX_Funk_t3816888158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4754[4] = 
{
	CameraFilterPack_FX_Funk_t3816888158::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Funk_t3816888158::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Funk_t3816888158::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Funk_t3816888158::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4755 = { sizeof (CameraFilterPack_FX_Glitch1_t63583076), -1, sizeof(CameraFilterPack_FX_Glitch1_t63583076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4755[6] = 
{
	CameraFilterPack_FX_Glitch1_t63583076::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Glitch1_t63583076::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Glitch1_t63583076::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Glitch1_t63583076::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Glitch1_t63583076::get_offset_of_Value_6(),
	CameraFilterPack_FX_Glitch1_t63583076_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4756 = { sizeof (CameraFilterPack_FX_Glitch3_t63583074), -1, sizeof(CameraFilterPack_FX_Glitch3_t63583074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4756[6] = 
{
	CameraFilterPack_FX_Glitch3_t63583074::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Glitch3_t63583074::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Glitch3_t63583074::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Glitch3_t63583074::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Glitch3_t63583074::get_offset_of_Value_6(),
	CameraFilterPack_FX_Glitch3_t63583074_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4757 = { sizeof (CameraFilterPack_FX_Grid_t3480861000), -1, sizeof(CameraFilterPack_FX_Grid_t3480861000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4757[5] = 
{
	CameraFilterPack_FX_Grid_t3480861000::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Grid_t3480861000::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Grid_t3480861000::get_offset_of_SCMaterial_4(),
	CameraFilterPack_FX_Grid_t3480861000::get_offset_of_Distortion_5(),
	CameraFilterPack_FX_Grid_t3480861000_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4758 = { sizeof (CameraFilterPack_FX_Hexagon_t382883320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4758[4] = 
{
	CameraFilterPack_FX_Hexagon_t382883320::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Hexagon_t382883320::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Hexagon_t382883320::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Hexagon_t382883320::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4759 = { sizeof (CameraFilterPack_FX_Hexagon_Black_t1004384256), -1, sizeof(CameraFilterPack_FX_Hexagon_Black_t1004384256_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4759[6] = 
{
	CameraFilterPack_FX_Hexagon_Black_t1004384256::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Hexagon_Black_t1004384256::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Hexagon_Black_t1004384256::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Hexagon_Black_t1004384256::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Hexagon_Black_t1004384256::get_offset_of_Value_6(),
	CameraFilterPack_FX_Hexagon_Black_t1004384256_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4760 = { sizeof (CameraFilterPack_FX_Hypno_t1332127830), -1, sizeof(CameraFilterPack_FX_Hypno_t1332127830_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4760[12] = 
{
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_Speed_6(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_Red_7(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_Green_8(),
	CameraFilterPack_FX_Hypno_t1332127830::get_offset_of_Blue_9(),
	CameraFilterPack_FX_Hypno_t1332127830_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_FX_Hypno_t1332127830_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_FX_Hypno_t1332127830_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_FX_Hypno_t1332127830_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4761 = { sizeof (CameraFilterPack_FX_InverChromiLum_t695765168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4761[4] = 
{
	CameraFilterPack_FX_InverChromiLum_t695765168::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_InverChromiLum_t695765168::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_InverChromiLum_t695765168::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_InverChromiLum_t695765168::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4762 = { sizeof (CameraFilterPack_FX_Mirror_t1330003333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4762[4] = 
{
	CameraFilterPack_FX_Mirror_t1330003333::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Mirror_t1330003333::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Mirror_t1330003333::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Mirror_t1330003333::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4763 = { sizeof (CameraFilterPack_FX_Plasma_t932080402), -1, sizeof(CameraFilterPack_FX_Plasma_t932080402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4763[6] = 
{
	CameraFilterPack_FX_Plasma_t932080402::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Plasma_t932080402::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Plasma_t932080402::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Plasma_t932080402::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Plasma_t932080402::get_offset_of_Value_6(),
	CameraFilterPack_FX_Plasma_t932080402_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4764 = { sizeof (CameraFilterPack_FX_Psycho_t3893392668), -1, sizeof(CameraFilterPack_FX_Psycho_t3893392668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4764[5] = 
{
	CameraFilterPack_FX_Psycho_t3893392668::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Psycho_t3893392668::get_offset_of_SCMaterial_3(),
	CameraFilterPack_FX_Psycho_t3893392668::get_offset_of_TimeX_4(),
	CameraFilterPack_FX_Psycho_t3893392668::get_offset_of_Distortion_5(),
	CameraFilterPack_FX_Psycho_t3893392668_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4765 = { sizeof (CameraFilterPack_FX_Screens_t1869481571), -1, sizeof(CameraFilterPack_FX_Screens_t1869481571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4765[12] = 
{
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_Tiles_6(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_Speed_7(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_PosX_8(),
	CameraFilterPack_FX_Screens_t1869481571::get_offset_of_PosY_9(),
	CameraFilterPack_FX_Screens_t1869481571_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_FX_Screens_t1869481571_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_FX_Screens_t1869481571_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_FX_Screens_t1869481571_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4766 = { sizeof (CameraFilterPack_FX_Spot_t1081357216), -1, sizeof(CameraFilterPack_FX_Spot_t1081357216_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4766[8] = 
{
	CameraFilterPack_FX_Spot_t1081357216::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_Spot_t1081357216::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_Spot_t1081357216::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_Spot_t1081357216::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_Spot_t1081357216::get_offset_of_center_6(),
	CameraFilterPack_FX_Spot_t1081357216::get_offset_of_Radius_7(),
	CameraFilterPack_FX_Spot_t1081357216_StaticFields::get_offset_of_Changecenter_8(),
	CameraFilterPack_FX_Spot_t1081357216_StaticFields::get_offset_of_ChangeRadius_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4767 = { sizeof (CameraFilterPack_FX_ZebraColor_t3324297331), -1, sizeof(CameraFilterPack_FX_ZebraColor_t3324297331_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4767[6] = 
{
	CameraFilterPack_FX_ZebraColor_t3324297331::get_offset_of_SCShader_2(),
	CameraFilterPack_FX_ZebraColor_t3324297331::get_offset_of_TimeX_3(),
	CameraFilterPack_FX_ZebraColor_t3324297331::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_FX_ZebraColor_t3324297331::get_offset_of_SCMaterial_5(),
	CameraFilterPack_FX_ZebraColor_t3324297331::get_offset_of_Value_6(),
	CameraFilterPack_FX_ZebraColor_t3324297331_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4768 = { sizeof (CameraFilterPack_Film_ColorPerfection_t1035791658), -1, sizeof(CameraFilterPack_Film_ColorPerfection_t1035791658_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4768[12] = 
{
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_SCShader_2(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_TimeX_3(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_Gamma_6(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_Value2_7(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_Value3_8(),
	CameraFilterPack_Film_ColorPerfection_t1035791658::get_offset_of_Value4_9(),
	CameraFilterPack_Film_ColorPerfection_t1035791658_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Film_ColorPerfection_t1035791658_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Film_ColorPerfection_t1035791658_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Film_ColorPerfection_t1035791658_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4769 = { sizeof (CameraFilterPack_Film_Grain_t1407189687), -1, sizeof(CameraFilterPack_Film_Grain_t1407189687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4769[6] = 
{
	CameraFilterPack_Film_Grain_t1407189687::get_offset_of_SCShader_2(),
	CameraFilterPack_Film_Grain_t1407189687::get_offset_of_TimeX_3(),
	CameraFilterPack_Film_Grain_t1407189687::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Film_Grain_t1407189687::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Film_Grain_t1407189687::get_offset_of_Value_6(),
	CameraFilterPack_Film_Grain_t1407189687_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4770 = { sizeof (CameraFilterPack_Gradients_Ansi_t4141626376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4770[7] = 
{
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Ansi_t4141626376::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4771 = { sizeof (CameraFilterPack_Gradients_Desert_t1681323834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4771[7] = 
{
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Desert_t1681323834::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4772 = { sizeof (CameraFilterPack_Gradients_ElectricGradient_t1587039484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4772[7] = 
{
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_ElectricGradient_t1587039484::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4773 = { sizeof (CameraFilterPack_Gradients_FireGradient_t1659791133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4773[7] = 
{
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_FireGradient_t1659791133::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4774 = { sizeof (CameraFilterPack_Gradients_Hue_t363696739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4774[7] = 
{
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Hue_t363696739::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4775 = { sizeof (CameraFilterPack_Gradients_NeonGradient_t2703802219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4775[7] = 
{
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_NeonGradient_t2703802219::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4776 = { sizeof (CameraFilterPack_Gradients_Rainbow_t3966350505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4776[7] = 
{
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Rainbow_t3966350505::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4777 = { sizeof (CameraFilterPack_Gradients_Stripe_t3929419426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4777[7] = 
{
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Stripe_t3929419426::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4778 = { sizeof (CameraFilterPack_Gradients_Tech_t464753049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4778[7] = 
{
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Tech_t464753049::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4779 = { sizeof (CameraFilterPack_Gradients_Therma_t1493044570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4779[7] = 
{
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_SCShader_2(),
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_ShaderName_3(),
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_TimeX_4(),
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_Switch_7(),
	CameraFilterPack_Gradients_Therma_t1493044570::get_offset_of_Fade_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4780 = { sizeof (CameraFilterPack_Light_Rainbow_t1179232882), -1, sizeof(CameraFilterPack_Light_Rainbow_t1179232882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4780[6] = 
{
	CameraFilterPack_Light_Rainbow_t1179232882::get_offset_of_SCShader_2(),
	CameraFilterPack_Light_Rainbow_t1179232882::get_offset_of_TimeX_3(),
	CameraFilterPack_Light_Rainbow_t1179232882::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Light_Rainbow_t1179232882::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Light_Rainbow_t1179232882::get_offset_of_Value_6(),
	CameraFilterPack_Light_Rainbow_t1179232882_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4781 = { sizeof (CameraFilterPack_Light_Rainbow2_t1290162980), -1, sizeof(CameraFilterPack_Light_Rainbow2_t1290162980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4781[6] = 
{
	CameraFilterPack_Light_Rainbow2_t1290162980::get_offset_of_SCShader_2(),
	CameraFilterPack_Light_Rainbow2_t1290162980::get_offset_of_TimeX_3(),
	CameraFilterPack_Light_Rainbow2_t1290162980::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Light_Rainbow2_t1290162980::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Light_Rainbow2_t1290162980::get_offset_of_Value_6(),
	CameraFilterPack_Light_Rainbow2_t1290162980_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4782 = { sizeof (CameraFilterPack_Light_Water_t3954840705), -1, sizeof(CameraFilterPack_Light_Water_t3954840705_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4782[11] = 
{
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_SCShader_2(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_TimeX_3(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_Size_6(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_Alpha_7(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_Distance_8(),
	CameraFilterPack_Light_Water_t3954840705::get_offset_of_Speed_9(),
	CameraFilterPack_Light_Water_t3954840705_StaticFields::get_offset_of_ChangeAlpha_10(),
	CameraFilterPack_Light_Water_t3954840705_StaticFields::get_offset_of_ChangeDistance_11(),
	CameraFilterPack_Light_Water_t3954840705_StaticFields::get_offset_of_ChangeSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4783 = { sizeof (CameraFilterPack_Light_Water2_t3567750323), -1, sizeof(CameraFilterPack_Light_Water2_t3567750323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4783[12] = 
{
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_SCShader_2(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_TimeX_3(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_Speed_6(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_Speed_X_7(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_Speed_Y_8(),
	CameraFilterPack_Light_Water2_t3567750323::get_offset_of_Intensity_9(),
	CameraFilterPack_Light_Water2_t3567750323_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Light_Water2_t3567750323_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Light_Water2_t3567750323_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Light_Water2_t3567750323_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4784 = { sizeof (CameraFilterPack_NightVisionFX_t1351189223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4784[21] = 
{
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_SCShader_2(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Preset_3(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_PresetMemo_4(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_TimeX_5(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_ScreenResolution_6(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_SCMaterial_7(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Greenness_8(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Vignette_9(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Vignette_Alpha_10(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Distortion_11(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Noise_12(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Intensity_13(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Light_14(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Light2_15(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Line_16(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Color_R_17(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Color_G_18(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of_Color_B_19(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of__Binocular_Size_20(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of__Binocular_Smooth_21(),
	CameraFilterPack_NightVisionFX_t1351189223::get_offset_of__Binocular_Dist_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4785 = { sizeof (preset_t1426196141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4785[12] = 
{
	preset_t1426196141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4786 = { sizeof (CameraFilterPack_NightVision_4_t4012815426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4786[7] = 
{
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_ShaderName_2(),
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_SCShader_3(),
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_FadeFX_4(),
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_TimeX_5(),
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_ScreenResolution_6(),
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_SCMaterial_7(),
	CameraFilterPack_NightVision_4_t4012815426::get_offset_of_Matrix9_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4787 = { sizeof (CameraFilterPack_Oculus_NightVision1_t3199654410), -1, sizeof(CameraFilterPack_Oculus_NightVision1_t3199654410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4787[9] = 
{
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_SCShader_2(),
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_TimeX_3(),
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_Distortion_4(),
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_ScreenResolution_6(),
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_Vignette_7(),
	CameraFilterPack_Oculus_NightVision1_t3199654410::get_offset_of_Linecount_8(),
	CameraFilterPack_Oculus_NightVision1_t3199654410_StaticFields::get_offset_of_ChangeVignette_9(),
	CameraFilterPack_Oculus_NightVision1_t3199654410_StaticFields::get_offset_of_ChangeLinecount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4788 = { sizeof (CameraFilterPack_Oculus_NightVision2_t2796369883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4788[7] = 
{
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_ShaderName_2(),
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_SCShader_3(),
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_FadeFX_4(),
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_TimeX_5(),
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_ScreenResolution_6(),
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_SCMaterial_7(),
	CameraFilterPack_Oculus_NightVision2_t2796369883::get_offset_of_Matrix9_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4789 = { sizeof (CameraFilterPack_Oculus_NightVision3_t67486528), -1, sizeof(CameraFilterPack_Oculus_NightVision3_t67486528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4789[8] = 
{
	CameraFilterPack_Oculus_NightVision3_t67486528::get_offset_of_SCShader_2(),
	CameraFilterPack_Oculus_NightVision3_t67486528::get_offset_of_TimeX_3(),
	CameraFilterPack_Oculus_NightVision3_t67486528::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Oculus_NightVision3_t67486528::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Oculus_NightVision3_t67486528::get_offset_of_Greenness_6(),
	CameraFilterPack_Oculus_NightVision3_t67486528_StaticFields::get_offset_of_ChangeBinocularSize_7(),
	CameraFilterPack_Oculus_NightVision3_t67486528_StaticFields::get_offset_of_ChangeBinocularDistance_8(),
	CameraFilterPack_Oculus_NightVision3_t67486528_StaticFields::get_offset_of_ChangeGreenness_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4790 = { sizeof (CameraFilterPack_Oculus_NightVision5_t1230285942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4790[10] = 
{
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_ShaderName_2(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_SCShader_3(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_FadeFX_4(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of__Size_5(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of__Smooth_6(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of__Dist_7(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_TimeX_8(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_ScreenResolution_9(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_SCMaterial_10(),
	CameraFilterPack_Oculus_NightVision5_t1230285942::get_offset_of_Matrix9_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4791 = { sizeof (CameraFilterPack_Oculus_ThermaVision_t1714024826), -1, sizeof(CameraFilterPack_Oculus_ThermaVision_t1714024826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4791[12] = 
{
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_SCShader_2(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_TimeX_3(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_Therma_Variation_6(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_Contrast_7(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_Burn_8(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826::get_offset_of_SceneCut_9(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Oculus_ThermaVision_t1714024826_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4792 = { sizeof (CameraFilterPack_OldFilm_Cutting1_t1382199430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4792[8] = 
{
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_SCShader_2(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_TimeX_3(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_Speed_4(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_Luminosity_5(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_Vignette_6(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_Negative_7(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_SCMaterial_8(),
	CameraFilterPack_OldFilm_Cutting1_t1382199430::get_offset_of_Texture2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4793 = { sizeof (CameraFilterPack_OldFilm_Cutting2_t1382199431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4793[8] = 
{
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_SCShader_2(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_TimeX_3(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_Speed_4(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_Luminosity_5(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_Vignette_6(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_Negative_7(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_SCMaterial_8(),
	CameraFilterPack_OldFilm_Cutting2_t1382199431::get_offset_of_Texture2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4794 = { sizeof (CameraFilterPack_Pixel_Pixelisation_t2392528693), -1, sizeof(CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4794[8] = 
{
	CameraFilterPack_Pixel_Pixelisation_t2392528693::get_offset_of_SCShader_2(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693::get_offset_of__Pixelisation_3(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693::get_offset_of__SizeX_4(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693::get_offset_of__SizeY_5(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields::get_offset_of_ChangePixel_7(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields::get_offset_of_ChangePixelX_8(),
	CameraFilterPack_Pixel_Pixelisation_t2392528693_StaticFields::get_offset_of_ChangePixelY_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4795 = { sizeof (CameraFilterPack_Pixelisation_Dot_t214826704), -1, sizeof(CameraFilterPack_Pixelisation_Dot_t214826704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4795[12] = 
{
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_SCShader_2(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_TimeX_3(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_Size_6(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_LightBackGround_7(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_Speed_8(),
	CameraFilterPack_Pixelisation_Dot_t214826704::get_offset_of_Size2_9(),
	CameraFilterPack_Pixelisation_Dot_t214826704_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Pixelisation_Dot_t214826704_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Pixelisation_Dot_t214826704_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Pixelisation_Dot_t214826704_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4796 = { sizeof (CameraFilterPack_Pixelisation_OilPaint_t2695555097), -1, sizeof(CameraFilterPack_Pixelisation_OilPaint_t2695555097_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4796[6] = 
{
	CameraFilterPack_Pixelisation_OilPaint_t2695555097::get_offset_of_SCShader_2(),
	CameraFilterPack_Pixelisation_OilPaint_t2695555097::get_offset_of_TimeX_3(),
	CameraFilterPack_Pixelisation_OilPaint_t2695555097::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Pixelisation_OilPaint_t2695555097::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Pixelisation_OilPaint_t2695555097::get_offset_of_Value_6(),
	CameraFilterPack_Pixelisation_OilPaint_t2695555097_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4797 = { sizeof (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270), -1, sizeof(CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4797[6] = 
{
	CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270::get_offset_of_SCShader_2(),
	CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270::get_offset_of_TimeX_3(),
	CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270::get_offset_of_Value_6(),
	CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4798 = { sizeof (CameraFilterPack_Sharpen_Sharpen_t1039912), -1, sizeof(CameraFilterPack_Sharpen_Sharpen_t1039912_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4798[6] = 
{
	CameraFilterPack_Sharpen_Sharpen_t1039912::get_offset_of_SCShader_2(),
	CameraFilterPack_Sharpen_Sharpen_t1039912::get_offset_of_Value_3(),
	CameraFilterPack_Sharpen_Sharpen_t1039912::get_offset_of_TimeX_4(),
	CameraFilterPack_Sharpen_Sharpen_t1039912::get_offset_of_ScreenResolution_5(),
	CameraFilterPack_Sharpen_Sharpen_t1039912::get_offset_of_SCMaterial_6(),
	CameraFilterPack_Sharpen_Sharpen_t1039912_StaticFields::get_offset_of_ChangeValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4799 = { sizeof (CameraFilterPack_Special_Bubble_t2761025777), -1, sizeof(CameraFilterPack_Special_Bubble_t2761025777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4799[12] = 
{
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_SCShader_2(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_TimeX_3(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_X_6(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_Y_7(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_Rate_8(),
	CameraFilterPack_Special_Bubble_t2761025777::get_offset_of_Value4_9(),
	CameraFilterPack_Special_Bubble_t2761025777_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Special_Bubble_t2761025777_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Special_Bubble_t2761025777_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Special_Bubble_t2761025777_StaticFields::get_offset_of_ChangeValue4_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
