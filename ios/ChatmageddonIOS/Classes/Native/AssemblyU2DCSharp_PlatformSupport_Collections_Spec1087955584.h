﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IList
struct IList_t3321498491;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.Specialized.ReadOnlyList
struct  ReadOnlyList_t1087955584  : public Il2CppObject
{
public:
	// System.Collections.IList PlatformSupport.Collections.Specialized.ReadOnlyList::_list
	Il2CppObject * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(ReadOnlyList_t1087955584, ____list_0)); }
	inline Il2CppObject * get__list_0() const { return ____list_0; }
	inline Il2CppObject ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(Il2CppObject * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier(&____list_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
