﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_UITweener2986641582.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenPanelClipRange
struct  TweenPanelClipRange_t596408274  : public UITweener_t2986641582
{
public:
	// UnityEngine.Vector4 TweenPanelClipRange::from
	Vector4_t2243707581  ___from_20;
	// UnityEngine.Vector4 TweenPanelClipRange::to
	Vector4_t2243707581  ___to_21;
	// UIPanel TweenPanelClipRange::_panel
	UIPanel_t1795085332 * ____panel_22;

public:
	inline static int32_t get_offset_of_from_20() { return static_cast<int32_t>(offsetof(TweenPanelClipRange_t596408274, ___from_20)); }
	inline Vector4_t2243707581  get_from_20() const { return ___from_20; }
	inline Vector4_t2243707581 * get_address_of_from_20() { return &___from_20; }
	inline void set_from_20(Vector4_t2243707581  value)
	{
		___from_20 = value;
	}

	inline static int32_t get_offset_of_to_21() { return static_cast<int32_t>(offsetof(TweenPanelClipRange_t596408274, ___to_21)); }
	inline Vector4_t2243707581  get_to_21() const { return ___to_21; }
	inline Vector4_t2243707581 * get_address_of_to_21() { return &___to_21; }
	inline void set_to_21(Vector4_t2243707581  value)
	{
		___to_21 = value;
	}

	inline static int32_t get_offset_of__panel_22() { return static_cast<int32_t>(offsetof(TweenPanelClipRange_t596408274, ____panel_22)); }
	inline UIPanel_t1795085332 * get__panel_22() const { return ____panel_22; }
	inline UIPanel_t1795085332 ** get_address_of__panel_22() { return &____panel_22; }
	inline void set__panel_22(UIPanel_t1795085332 * value)
	{
		____panel_22 = value;
		Il2CppCodeGenWriteBarrier(&____panel_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
