﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EULAUI
struct EULAUI_t3168190395;

#include "codegen/il2cpp-codegen.h"

// System.Void EULAUI::.ctor()
extern "C"  void EULAUI__ctor_m2106533670 (EULAUI_t3168190395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EULAUI::LoadUI()
extern "C"  void EULAUI_LoadUI_m4185399426 (EULAUI_t3168190395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
