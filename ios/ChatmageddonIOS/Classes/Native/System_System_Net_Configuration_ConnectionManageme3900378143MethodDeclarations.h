﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Configuration.ConnectionManagementHandler
struct ConnectionManagementHandler_t3900378143;
// System.Object
struct Il2CppObject;
// System.Xml.XmlNode
struct XmlNode_t616554813;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"

// System.Void System.Net.Configuration.ConnectionManagementHandler::.ctor()
extern "C"  void ConnectionManagementHandler__ctor_m622082051 (ConnectionManagementHandler_t3900378143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Net.Configuration.ConnectionManagementHandler::Create(System.Object,System.Object,System.Xml.XmlNode)
extern "C"  Il2CppObject * ConnectionManagementHandler_Create_m817604307 (ConnectionManagementHandler_t3900378143 * __this, Il2CppObject * ___parent0, Il2CppObject * ___configContext1, XmlNode_t616554813 * ___section2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
