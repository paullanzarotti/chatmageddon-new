﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.RegionCode
struct RegionCode_t982551659;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.RegionCode::.ctor()
extern "C"  void RegionCode__ctor_m2615738164 (RegionCode_t982551659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
