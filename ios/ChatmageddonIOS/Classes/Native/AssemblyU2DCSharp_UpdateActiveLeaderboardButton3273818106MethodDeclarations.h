﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateActiveLeaderboardButton
struct UpdateActiveLeaderboardButton_t3273818106;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateActiveLeaderboardButton::.ctor()
extern "C"  void UpdateActiveLeaderboardButton__ctor_m1293716381 (UpdateActiveLeaderboardButton_t3273818106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateActiveLeaderboardButton::OnButtonClick()
extern "C"  void UpdateActiveLeaderboardButton_OnButtonClick_m1183314706 (UpdateActiveLeaderboardButton_t3273818106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
