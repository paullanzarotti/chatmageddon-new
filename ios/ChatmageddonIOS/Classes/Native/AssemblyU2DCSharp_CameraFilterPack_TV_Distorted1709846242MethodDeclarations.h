﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Distorted
struct CameraFilterPack_TV_Distorted_t1709846242;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Distorted::.ctor()
extern "C"  void CameraFilterPack_TV_Distorted__ctor_m2775724891 (CameraFilterPack_TV_Distorted_t1709846242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Distorted::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Distorted_get_material_m1938683650 (CameraFilterPack_TV_Distorted_t1709846242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::Start()
extern "C"  void CameraFilterPack_TV_Distorted_Start_m842566311 (CameraFilterPack_TV_Distorted_t1709846242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Distorted_OnRenderImage_m1296115567 (CameraFilterPack_TV_Distorted_t1709846242 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::OnValidate()
extern "C"  void CameraFilterPack_TV_Distorted_OnValidate_m1466564762 (CameraFilterPack_TV_Distorted_t1709846242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::Update()
extern "C"  void CameraFilterPack_TV_Distorted_Update_m1452057904 (CameraFilterPack_TV_Distorted_t1709846242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::OnDisable()
extern "C"  void CameraFilterPack_TV_Distorted_OnDisable_m2194037374 (CameraFilterPack_TV_Distorted_t1709846242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
