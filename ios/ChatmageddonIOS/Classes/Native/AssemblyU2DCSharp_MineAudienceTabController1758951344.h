﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<MineAudienceTab>
struct List_1_t395000804;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineAudienceTabController
struct  MineAudienceTabController_t1758951344  : public MonoBehaviour_t1158329972
{
public:
	// MineAudience MineAudienceTabController::currentAudience
	int32_t ___currentAudience_2;
	// System.Collections.Generic.List`1<MineAudienceTab> MineAudienceTabController::tabs
	List_1_t395000804 * ___tabs_3;

public:
	inline static int32_t get_offset_of_currentAudience_2() { return static_cast<int32_t>(offsetof(MineAudienceTabController_t1758951344, ___currentAudience_2)); }
	inline int32_t get_currentAudience_2() const { return ___currentAudience_2; }
	inline int32_t* get_address_of_currentAudience_2() { return &___currentAudience_2; }
	inline void set_currentAudience_2(int32_t value)
	{
		___currentAudience_2 = value;
	}

	inline static int32_t get_offset_of_tabs_3() { return static_cast<int32_t>(offsetof(MineAudienceTabController_t1758951344, ___tabs_3)); }
	inline List_1_t395000804 * get_tabs_3() const { return ___tabs_3; }
	inline List_1_t395000804 ** get_address_of_tabs_3() { return &___tabs_3; }
	inline void set_tabs_3(List_1_t395000804 * value)
	{
		___tabs_3 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
