﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptionPropertyCollection
struct EncryptionPropertyCollection_t2509585554;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.Xml.EncryptionProperty
struct EncryptionProperty_t773336898;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Security_System_Security_Cryptography_Xml_En773336898.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::.ctor()
extern "C"  void EncryptionPropertyCollection__ctor_m2611930738 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * EncryptionPropertyCollection_System_Collections_IList_get_Item_m3050749749 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void EncryptionPropertyCollection_System_Collections_IList_set_Item_m696369448 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.Contains(System.Object)
extern "C"  bool EncryptionPropertyCollection_System_Collections_IList_Contains_m3444567752 (EncryptionPropertyCollection_t2509585554 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.Add(System.Object)
extern "C"  int32_t EncryptionPropertyCollection_System_Collections_IList_Add_m971256378 (EncryptionPropertyCollection_t2509585554 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t EncryptionPropertyCollection_System_Collections_IList_IndexOf_m3890075316 (EncryptionPropertyCollection_t2509585554 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void EncryptionPropertyCollection_System_Collections_IList_Insert_m2116161149 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::System.Collections.IList.Remove(System.Object)
extern "C"  void EncryptionPropertyCollection_System_Collections_IList_Remove_m1141294957 (EncryptionPropertyCollection_t2509585554 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.EncryptionPropertyCollection::get_Count()
extern "C"  int32_t EncryptionPropertyCollection_get_Count_m3806868490 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.EncryptionPropertyCollection::get_IsFixedSize()
extern "C"  bool EncryptionPropertyCollection_get_IsFixedSize_m2080520026 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.EncryptionPropertyCollection::get_IsReadOnly()
extern "C"  bool EncryptionPropertyCollection_get_IsReadOnly_m2031723769 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.EncryptionPropertyCollection::get_IsSynchronized()
extern "C"  bool EncryptionPropertyCollection_get_IsSynchronized_m554614589 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.EncryptionProperty System.Security.Cryptography.Xml.EncryptionPropertyCollection::get_ItemOf(System.Int32)
extern "C"  EncryptionProperty_t773336898 * EncryptionPropertyCollection_get_ItemOf_m1474016091 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::set_ItemOf(System.Int32,System.Security.Cryptography.Xml.EncryptionProperty)
extern "C"  void EncryptionPropertyCollection_set_ItemOf_m3337575194 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, EncryptionProperty_t773336898 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.Xml.EncryptionPropertyCollection::get_SyncRoot()
extern "C"  Il2CppObject * EncryptionPropertyCollection_get_SyncRoot_m4288111741 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.EncryptionPropertyCollection::Add(System.Security.Cryptography.Xml.EncryptionProperty)
extern "C"  int32_t EncryptionPropertyCollection_Add_m3670455915 (EncryptionPropertyCollection_t2509585554 * __this, EncryptionProperty_t773336898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::Clear()
extern "C"  void EncryptionPropertyCollection_Clear_m2908409517 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.EncryptionPropertyCollection::Contains(System.Security.Cryptography.Xml.EncryptionProperty)
extern "C"  bool EncryptionPropertyCollection_Contains_m1727273835 (EncryptionPropertyCollection_t2509585554 * __this, EncryptionProperty_t773336898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::CopyTo(System.Array,System.Int32)
extern "C"  void EncryptionPropertyCollection_CopyTo_m2324636653 (EncryptionPropertyCollection_t2509585554 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.Xml.EncryptionPropertyCollection::GetEnumerator()
extern "C"  Il2CppObject * EncryptionPropertyCollection_GetEnumerator_m2547392314 (EncryptionPropertyCollection_t2509585554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.EncryptionPropertyCollection::IndexOf(System.Security.Cryptography.Xml.EncryptionProperty)
extern "C"  int32_t EncryptionPropertyCollection_IndexOf_m3436002621 (EncryptionPropertyCollection_t2509585554 * __this, EncryptionProperty_t773336898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::Insert(System.Int32,System.Security.Cryptography.Xml.EncryptionProperty)
extern "C"  void EncryptionPropertyCollection_Insert_m1731284136 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, EncryptionProperty_t773336898 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::Remove(System.Security.Cryptography.Xml.EncryptionProperty)
extern "C"  void EncryptionPropertyCollection_Remove_m2408699886 (EncryptionPropertyCollection_t2509585554 * __this, EncryptionProperty_t773336898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionPropertyCollection::RemoveAt(System.Int32)
extern "C"  void EncryptionPropertyCollection_RemoveAt_m2666200692 (EncryptionPropertyCollection_t2509585554 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
