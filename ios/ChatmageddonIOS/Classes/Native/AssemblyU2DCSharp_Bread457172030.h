﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ToastType1578101939.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bread
struct  Bread_t457172030  : public Il2CppObject
{
public:
	// ToastType Bread::toastType
	int32_t ___toastType_0;
	// System.Boolean Bread::showFade
	bool ___showFade_1;
	// System.String Bread::toastMessage
	String_t* ___toastMessage_2;
	// SFX Bread::toastSFX
	int32_t ___toastSFX_3;

public:
	inline static int32_t get_offset_of_toastType_0() { return static_cast<int32_t>(offsetof(Bread_t457172030, ___toastType_0)); }
	inline int32_t get_toastType_0() const { return ___toastType_0; }
	inline int32_t* get_address_of_toastType_0() { return &___toastType_0; }
	inline void set_toastType_0(int32_t value)
	{
		___toastType_0 = value;
	}

	inline static int32_t get_offset_of_showFade_1() { return static_cast<int32_t>(offsetof(Bread_t457172030, ___showFade_1)); }
	inline bool get_showFade_1() const { return ___showFade_1; }
	inline bool* get_address_of_showFade_1() { return &___showFade_1; }
	inline void set_showFade_1(bool value)
	{
		___showFade_1 = value;
	}

	inline static int32_t get_offset_of_toastMessage_2() { return static_cast<int32_t>(offsetof(Bread_t457172030, ___toastMessage_2)); }
	inline String_t* get_toastMessage_2() const { return ___toastMessage_2; }
	inline String_t** get_address_of_toastMessage_2() { return &___toastMessage_2; }
	inline void set_toastMessage_2(String_t* value)
	{
		___toastMessage_2 = value;
		Il2CppCodeGenWriteBarrier(&___toastMessage_2, value);
	}

	inline static int32_t get_offset_of_toastSFX_3() { return static_cast<int32_t>(offsetof(Bread_t457172030, ___toastSFX_3)); }
	inline int32_t get_toastSFX_3() const { return ___toastSFX_3; }
	inline int32_t* get_address_of_toastSFX_3() { return &___toastSFX_3; }
	inline void set_toastSFX_3(int32_t value)
	{
		___toastSFX_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
