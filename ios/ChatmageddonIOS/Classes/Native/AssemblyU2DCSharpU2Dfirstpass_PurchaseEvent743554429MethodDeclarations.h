﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchaseEvent
struct PurchaseEvent_t743554429;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PurchaseEvent::.ctor(PurchasableItem,System.String)
extern "C"  void PurchaseEvent__ctor_m3354619961 (PurchaseEvent_t743554429 * __this, PurchasableItem_t3963353899 * ___purchasedItem0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem PurchaseEvent::get_PurchasedItem()
extern "C"  PurchasableItem_t3963353899 * PurchaseEvent_get_PurchasedItem_m2741253279 (PurchaseEvent_t743554429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchaseEvent::set_PurchasedItem(PurchasableItem)
extern "C"  void PurchaseEvent_set_PurchasedItem_m2906069734 (PurchaseEvent_t743554429 * __this, PurchasableItem_t3963353899 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchaseEvent::get_Receipt()
extern "C"  String_t* PurchaseEvent_get_Receipt_m2167638012 (PurchaseEvent_t743554429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchaseEvent::set_Receipt(System.String)
extern "C"  void PurchaseEvent_set_Receipt_m3983220465 (PurchaseEvent_t743554429 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
