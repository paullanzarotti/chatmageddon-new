﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>
struct Transform_1_t1561902999;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21020750381.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4012450678_gshared (Transform_1_t1561902999 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m4012450678(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1561902999 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4012450678_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1020750381  Transform_1_Invoke_m3120150646_gshared (Transform_1_t1561902999 * __this, Vector3Pair_t2859078138  ___key0, bool ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3120150646(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1020750381  (*) (Transform_1_t1561902999 *, Vector3Pair_t2859078138 , bool, const MethodInfo*))Transform_1_Invoke_m3120150646_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1337385759_gshared (Transform_1_t1561902999 * __this, Vector3Pair_t2859078138  ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1337385759(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1561902999 *, Vector3Pair_t2859078138 , bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1337385759_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vectrosity.Vector3Pair,System.Boolean,System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1020750381  Transform_1_EndInvoke_m3747003980_gshared (Transform_1_t1561902999 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3747003980(__this, ___result0, method) ((  KeyValuePair_2_t1020750381  (*) (Transform_1_t1561902999 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3747003980_gshared)(__this, ___result0, method)
