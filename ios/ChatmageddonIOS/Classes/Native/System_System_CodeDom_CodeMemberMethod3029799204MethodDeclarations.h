﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeMemberMethod
struct CodeMemberMethod_t3029799204;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeMemberMethod::.ctor()
extern "C"  void CodeMemberMethod__ctor_m3319869916 (CodeMemberMethod_t3029799204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
