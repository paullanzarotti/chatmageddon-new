﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TilesetFadeExample/<OnChangeMaterialTexture>c__AnonStorey0
struct  U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675  : public Il2CppObject
{
public:
	// OnlineMapsTile TilesetFadeExample/<OnChangeMaterialTexture>c__AnonStorey0::tile
	OnlineMapsTile_t21329940 * ___tile_0;

public:
	inline static int32_t get_offset_of_tile_0() { return static_cast<int32_t>(offsetof(U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675, ___tile_0)); }
	inline OnlineMapsTile_t21329940 * get_tile_0() const { return ___tile_0; }
	inline OnlineMapsTile_t21329940 ** get_address_of_tile_0() { return &___tile_0; }
	inline void set_tile_0(OnlineMapsTile_t21329940 * value)
	{
		___tile_0 = value;
		Il2CppCodeGenWriteBarrier(&___tile_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
