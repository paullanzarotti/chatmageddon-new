﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SwipeType596416270.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUISwipe
struct  NGUISwipe_t4135300327  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.BoxCollider NGUISwipe::collision
	BoxCollider_t22920061 * ___collision_2;
	// System.Single NGUISwipe::swipeXThreshold
	float ___swipeXThreshold_3;
	// System.Single NGUISwipe::swipeYThreashold
	float ___swipeYThreashold_4;
	// SwipeType NGUISwipe::swipeType
	int32_t ___swipeType_5;
	// UnityEngine.Vector2 NGUISwipe::dragStart
	Vector2_t2243707579  ___dragStart_6;
	// UnityEngine.Vector2 NGUISwipe::dragcurrent
	Vector2_t2243707579  ___dragcurrent_7;
	// System.Boolean NGUISwipe::swipeCompleted
	bool ___swipeCompleted_8;
	// System.Boolean NGUISwipe::swipeVoid
	bool ___swipeVoid_9;

public:
	inline static int32_t get_offset_of_collision_2() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___collision_2)); }
	inline BoxCollider_t22920061 * get_collision_2() const { return ___collision_2; }
	inline BoxCollider_t22920061 ** get_address_of_collision_2() { return &___collision_2; }
	inline void set_collision_2(BoxCollider_t22920061 * value)
	{
		___collision_2 = value;
		Il2CppCodeGenWriteBarrier(&___collision_2, value);
	}

	inline static int32_t get_offset_of_swipeXThreshold_3() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___swipeXThreshold_3)); }
	inline float get_swipeXThreshold_3() const { return ___swipeXThreshold_3; }
	inline float* get_address_of_swipeXThreshold_3() { return &___swipeXThreshold_3; }
	inline void set_swipeXThreshold_3(float value)
	{
		___swipeXThreshold_3 = value;
	}

	inline static int32_t get_offset_of_swipeYThreashold_4() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___swipeYThreashold_4)); }
	inline float get_swipeYThreashold_4() const { return ___swipeYThreashold_4; }
	inline float* get_address_of_swipeYThreashold_4() { return &___swipeYThreashold_4; }
	inline void set_swipeYThreashold_4(float value)
	{
		___swipeYThreashold_4 = value;
	}

	inline static int32_t get_offset_of_swipeType_5() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___swipeType_5)); }
	inline int32_t get_swipeType_5() const { return ___swipeType_5; }
	inline int32_t* get_address_of_swipeType_5() { return &___swipeType_5; }
	inline void set_swipeType_5(int32_t value)
	{
		___swipeType_5 = value;
	}

	inline static int32_t get_offset_of_dragStart_6() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___dragStart_6)); }
	inline Vector2_t2243707579  get_dragStart_6() const { return ___dragStart_6; }
	inline Vector2_t2243707579 * get_address_of_dragStart_6() { return &___dragStart_6; }
	inline void set_dragStart_6(Vector2_t2243707579  value)
	{
		___dragStart_6 = value;
	}

	inline static int32_t get_offset_of_dragcurrent_7() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___dragcurrent_7)); }
	inline Vector2_t2243707579  get_dragcurrent_7() const { return ___dragcurrent_7; }
	inline Vector2_t2243707579 * get_address_of_dragcurrent_7() { return &___dragcurrent_7; }
	inline void set_dragcurrent_7(Vector2_t2243707579  value)
	{
		___dragcurrent_7 = value;
	}

	inline static int32_t get_offset_of_swipeCompleted_8() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___swipeCompleted_8)); }
	inline bool get_swipeCompleted_8() const { return ___swipeCompleted_8; }
	inline bool* get_address_of_swipeCompleted_8() { return &___swipeCompleted_8; }
	inline void set_swipeCompleted_8(bool value)
	{
		___swipeCompleted_8 = value;
	}

	inline static int32_t get_offset_of_swipeVoid_9() { return static_cast<int32_t>(offsetof(NGUISwipe_t4135300327, ___swipeVoid_9)); }
	inline bool get_swipeVoid_9() const { return ___swipeVoid_9; }
	inline bool* get_address_of_swipeVoid_9() { return &___swipeVoid_9; }
	inline void set_swipeVoid_9(bool value)
	{
		___swipeVoid_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
