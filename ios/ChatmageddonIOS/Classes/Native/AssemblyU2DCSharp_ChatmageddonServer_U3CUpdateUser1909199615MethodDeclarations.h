﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserFullName>c__AnonStorey12
struct U3CUpdateUserFullNameU3Ec__AnonStorey12_t1909199615;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserFullName>c__AnonStorey12::.ctor()
extern "C"  void U3CUpdateUserFullNameU3Ec__AnonStorey12__ctor_m3811220704 (U3CUpdateUserFullNameU3Ec__AnonStorey12_t1909199615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserFullName>c__AnonStorey12::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserFullNameU3Ec__AnonStorey12_U3CU3Em__0_m3230933219 (U3CUpdateUserFullNameU3Ec__AnonStorey12_t1909199615 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
