﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.HandledEventArgs
struct HandledEventArgs_t1685735245;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ComponentModel.HandledEventArgs::.ctor()
extern "C"  void HandledEventArgs__ctor_m932247312 (HandledEventArgs_t1685735245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.HandledEventArgs::.ctor(System.Boolean)
extern "C"  void HandledEventArgs__ctor_m3360161297 (HandledEventArgs_t1685735245 * __this, bool ___defaultHandledValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.HandledEventArgs::get_Handled()
extern "C"  bool HandledEventArgs_get_Handled_m3582679253 (HandledEventArgs_t1685735245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.HandledEventArgs::set_Handled(System.Boolean)
extern "C"  void HandledEventArgs_set_Handled_m751224744 (HandledEventArgs_t1685735245 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
