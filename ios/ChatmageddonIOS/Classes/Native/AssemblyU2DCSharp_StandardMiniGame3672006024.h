﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Light
struct Light_t494725636;
// RotatingTabController
struct RotatingTabController_t2617193705;
// CountdownTimer
struct CountdownTimer_t4123051008;

#include "AssemblyU2DCSharp_DefendMiniGameController1844356491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StandardMiniGame
struct  StandardMiniGame_t3672006024  : public DefendMiniGameController_t1844356491
{
public:
	// UnityEngine.GameObject StandardMiniGame::modelParent
	GameObject_t1756533147 * ___modelParent_6;
	// UnityEngine.GameObject StandardMiniGame::currentModel
	GameObject_t1756533147 * ___currentModel_7;
	// UnityEngine.Light StandardMiniGame::itemLight
	Light_t494725636 * ___itemLight_8;
	// RotatingTabController StandardMiniGame::tabController
	RotatingTabController_t2617193705 * ___tabController_9;
	// CountdownTimer StandardMiniGame::countdown
	CountdownTimer_t4123051008 * ___countdown_10;
	// System.Boolean StandardMiniGame::defendSuccess
	bool ___defendSuccess_11;

public:
	inline static int32_t get_offset_of_modelParent_6() { return static_cast<int32_t>(offsetof(StandardMiniGame_t3672006024, ___modelParent_6)); }
	inline GameObject_t1756533147 * get_modelParent_6() const { return ___modelParent_6; }
	inline GameObject_t1756533147 ** get_address_of_modelParent_6() { return &___modelParent_6; }
	inline void set_modelParent_6(GameObject_t1756533147 * value)
	{
		___modelParent_6 = value;
		Il2CppCodeGenWriteBarrier(&___modelParent_6, value);
	}

	inline static int32_t get_offset_of_currentModel_7() { return static_cast<int32_t>(offsetof(StandardMiniGame_t3672006024, ___currentModel_7)); }
	inline GameObject_t1756533147 * get_currentModel_7() const { return ___currentModel_7; }
	inline GameObject_t1756533147 ** get_address_of_currentModel_7() { return &___currentModel_7; }
	inline void set_currentModel_7(GameObject_t1756533147 * value)
	{
		___currentModel_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentModel_7, value);
	}

	inline static int32_t get_offset_of_itemLight_8() { return static_cast<int32_t>(offsetof(StandardMiniGame_t3672006024, ___itemLight_8)); }
	inline Light_t494725636 * get_itemLight_8() const { return ___itemLight_8; }
	inline Light_t494725636 ** get_address_of_itemLight_8() { return &___itemLight_8; }
	inline void set_itemLight_8(Light_t494725636 * value)
	{
		___itemLight_8 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_8, value);
	}

	inline static int32_t get_offset_of_tabController_9() { return static_cast<int32_t>(offsetof(StandardMiniGame_t3672006024, ___tabController_9)); }
	inline RotatingTabController_t2617193705 * get_tabController_9() const { return ___tabController_9; }
	inline RotatingTabController_t2617193705 ** get_address_of_tabController_9() { return &___tabController_9; }
	inline void set_tabController_9(RotatingTabController_t2617193705 * value)
	{
		___tabController_9 = value;
		Il2CppCodeGenWriteBarrier(&___tabController_9, value);
	}

	inline static int32_t get_offset_of_countdown_10() { return static_cast<int32_t>(offsetof(StandardMiniGame_t3672006024, ___countdown_10)); }
	inline CountdownTimer_t4123051008 * get_countdown_10() const { return ___countdown_10; }
	inline CountdownTimer_t4123051008 ** get_address_of_countdown_10() { return &___countdown_10; }
	inline void set_countdown_10(CountdownTimer_t4123051008 * value)
	{
		___countdown_10 = value;
		Il2CppCodeGenWriteBarrier(&___countdown_10, value);
	}

	inline static int32_t get_offset_of_defendSuccess_11() { return static_cast<int32_t>(offsetof(StandardMiniGame_t3672006024, ___defendSuccess_11)); }
	inline bool get_defendSuccess_11() const { return ___defendSuccess_11; }
	inline bool* get_address_of_defendSuccess_11() { return &___defendSuccess_11; }
	inline void set_defendSuccess_11(bool value)
	{
		___defendSuccess_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
