﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player/<UpdateInnerFriendsList>c__AnonStorey0
struct U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"

// System.Void Player/<UpdateInnerFriendsList>c__AnonStorey0::.ctor()
extern "C"  void U3CUpdateInnerFriendsListU3Ec__AnonStorey0__ctor_m2616897230 (U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<UpdateInnerFriendsList>c__AnonStorey0::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CUpdateInnerFriendsListU3Ec__AnonStorey0_U3CU3Em__0_m2100545990 (U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player/<UpdateInnerFriendsList>c__AnonStorey0::<>m__1(Friend)
extern "C"  int32_t U3CUpdateInnerFriendsListU3Ec__AnonStorey0_U3CU3Em__1_m160890912 (Il2CppObject * __this /* static, unused */, Friend_t3555014108 * ___innerF0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
