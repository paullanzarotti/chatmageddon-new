﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsProfileUIScaler
struct SettingsProfileUIScaler_t1908809364;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsProfileUIScaler::.ctor()
extern "C"  void SettingsProfileUIScaler__ctor_m498835383 (SettingsProfileUIScaler_t1908809364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileUIScaler::GlobalUIScale()
extern "C"  void SettingsProfileUIScaler_GlobalUIScale_m967685722 (SettingsProfileUIScaler_t1908809364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
