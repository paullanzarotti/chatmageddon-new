﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountryCodeILP
struct CountryCodeILP_t3216166666;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void CountryCodeILP::.ctor()
extern "C"  void CountryCodeILP__ctor_m4164851223 (CountryCodeILP_t3216166666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeILP::StartIL()
extern "C"  void CountryCodeILP_StartIL_m1444073668 (CountryCodeILP_t3216166666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeILP::SetDataArray()
extern "C"  void CountryCodeILP_SetDataArray_m2581802884 (CountryCodeILP_t3216166666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void CountryCodeILP_InitListItemWithIndex_m2893357309 (CountryCodeILP_t3216166666 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void CountryCodeILP_PopulateListItemWithIndex_m862721014 (CountryCodeILP_t3216166666 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
