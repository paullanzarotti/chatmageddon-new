﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<PanelType,Panel>
struct Dictionary_2_t2944688011;
// Panel
struct Panel_t1787746694;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1149681085.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelTransitionManager`1<ChatmageddonPanelTransitionManager>
struct  PanelTransitionManager_1_t2526031859  : public MonoSingleton_1_t1149681085
{
public:
	// System.Collections.Generic.Dictionary`2<PanelType,Panel> PanelTransitionManager`1::panelsList
	Dictionary_2_t2944688011 * ___panelsList_3;
	// Panel PanelTransitionManager`1::activePanel
	Panel_t1787746694 * ___activePanel_4;
	// Panel PanelTransitionManager`1::nextPanel
	Panel_t1787746694 * ___nextPanel_5;
	// System.Boolean PanelTransitionManager`1::switchingPanels
	bool ___switchingPanels_6;
	// PanelType PanelTransitionManager`1::switchPanel
	int32_t ___switchPanel_7;

public:
	inline static int32_t get_offset_of_panelsList_3() { return static_cast<int32_t>(offsetof(PanelTransitionManager_1_t2526031859, ___panelsList_3)); }
	inline Dictionary_2_t2944688011 * get_panelsList_3() const { return ___panelsList_3; }
	inline Dictionary_2_t2944688011 ** get_address_of_panelsList_3() { return &___panelsList_3; }
	inline void set_panelsList_3(Dictionary_2_t2944688011 * value)
	{
		___panelsList_3 = value;
		Il2CppCodeGenWriteBarrier(&___panelsList_3, value);
	}

	inline static int32_t get_offset_of_activePanel_4() { return static_cast<int32_t>(offsetof(PanelTransitionManager_1_t2526031859, ___activePanel_4)); }
	inline Panel_t1787746694 * get_activePanel_4() const { return ___activePanel_4; }
	inline Panel_t1787746694 ** get_address_of_activePanel_4() { return &___activePanel_4; }
	inline void set_activePanel_4(Panel_t1787746694 * value)
	{
		___activePanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___activePanel_4, value);
	}

	inline static int32_t get_offset_of_nextPanel_5() { return static_cast<int32_t>(offsetof(PanelTransitionManager_1_t2526031859, ___nextPanel_5)); }
	inline Panel_t1787746694 * get_nextPanel_5() const { return ___nextPanel_5; }
	inline Panel_t1787746694 ** get_address_of_nextPanel_5() { return &___nextPanel_5; }
	inline void set_nextPanel_5(Panel_t1787746694 * value)
	{
		___nextPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___nextPanel_5, value);
	}

	inline static int32_t get_offset_of_switchingPanels_6() { return static_cast<int32_t>(offsetof(PanelTransitionManager_1_t2526031859, ___switchingPanels_6)); }
	inline bool get_switchingPanels_6() const { return ___switchingPanels_6; }
	inline bool* get_address_of_switchingPanels_6() { return &___switchingPanels_6; }
	inline void set_switchingPanels_6(bool value)
	{
		___switchingPanels_6 = value;
	}

	inline static int32_t get_offset_of_switchPanel_7() { return static_cast<int32_t>(offsetof(PanelTransitionManager_1_t2526031859, ___switchPanel_7)); }
	inline int32_t get_switchPanel_7() const { return ___switchPanel_7; }
	inline int32_t* get_address_of_switchPanel_7() { return &___switchPanel_7; }
	inline void set_switchPanel_7(int32_t value)
	{
		___switchPanel_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
