﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Uniject.IHTTPRequest
struct IHTTPRequest_t54590966;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.IO.FileStream
struct FileStream_t1695958676;
// Unibill.Impl.DownloadManager
struct DownloadManager_t32803451;
// System.Object
struct Il2CppObject;
// Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3
struct U3CdownloadU3Ec__AnonStorey3_t2573312903;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.DownloadManager/<download>c__Iterator2
struct  U3CdownloadU3Ec__Iterator2_t3409902454  : public Il2CppObject
{
public:
	// System.String Unibill.Impl.DownloadManager/<download>c__Iterator2::bundleId
	String_t* ___bundleId_0;
	// System.String Unibill.Impl.DownloadManager/<download>c__Iterator2::<downloadToken>__0
	String_t* ___U3CdownloadTokenU3E__0_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Unibill.Impl.DownloadManager/<download>c__Iterator2::<parameters>__1
	Dictionary_2_t3943999495 * ___U3CparametersU3E__1_2;
	// Uniject.IHTTPRequest Unibill.Impl.DownloadManager/<download>c__Iterator2::<response>__2
	Il2CppObject * ___U3CresponseU3E__2_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.DownloadManager/<download>c__Iterator2::<downloadTokenHash>__3
	Dictionary_2_t309261261 * ___U3CdownloadTokenHashU3E__3_4;
	// System.Boolean Unibill.Impl.DownloadManager/<download>c__Iterator2::<success>__4
	bool ___U3CsuccessU3E__4_5;
	// System.String Unibill.Impl.DownloadManager/<download>c__Iterator2::<version>__5
	String_t* ___U3CversionU3E__5_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Unibill.Impl.DownloadManager/<download>c__Iterator2::<headers>__6
	Dictionary_2_t3943999495 * ___U3CheadersU3E__6_7;
	// Uniject.IHTTPRequest Unibill.Impl.DownloadManager/<download>c__Iterator2::<response>__7
	Il2CppObject * ___U3CresponseU3E__7_8;
	// System.String Unibill.Impl.DownloadManager/<download>c__Iterator2::<contentRange>__8
	String_t* ___U3CcontentRangeU3E__8_9;
	// System.Int64 Unibill.Impl.DownloadManager/<download>c__Iterator2::<contentLength>__9
	int64_t ___U3CcontentLengthU3E__9_10;
	// System.IO.FileStream Unibill.Impl.DownloadManager/<download>c__Iterator2::$locvar0
	FileStream_t1695958676 * ___U24locvar0_11;
	// System.Int64 Unibill.Impl.DownloadManager/<download>c__Iterator2::<rangeStart>__A
	int64_t ___U3CrangeStartU3E__A_12;
	// System.Int64 Unibill.Impl.DownloadManager/<download>c__Iterator2::<rangeEnd>__B
	int64_t ___U3CrangeEndU3E__B_13;
	// System.Int32 Unibill.Impl.DownloadManager/<download>c__Iterator2::<lastProgress>__C
	int32_t ___U3ClastProgressU3E__C_14;
	// System.String Unibill.Impl.DownloadManager/<download>c__Iterator2::<header>__D
	String_t* ___U3CheaderU3E__D_15;
	// Uniject.IHTTPRequest Unibill.Impl.DownloadManager/<download>c__Iterator2::<response>__E
	Il2CppObject * ___U3CresponseU3E__E_16;
	// System.Int32 Unibill.Impl.DownloadManager/<download>c__Iterator2::<progress>__F
	int32_t ___U3CprogressU3E__F_17;
	// Unibill.Impl.DownloadManager Unibill.Impl.DownloadManager/<download>c__Iterator2::$this
	DownloadManager_t32803451 * ___U24this_18;
	// System.Object Unibill.Impl.DownloadManager/<download>c__Iterator2::$current
	Il2CppObject * ___U24current_19;
	// System.Boolean Unibill.Impl.DownloadManager/<download>c__Iterator2::$disposing
	bool ___U24disposing_20;
	// System.Int32 Unibill.Impl.DownloadManager/<download>c__Iterator2::$PC
	int32_t ___U24PC_21;
	// Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3 Unibill.Impl.DownloadManager/<download>c__Iterator2::$locvar1
	U3CdownloadU3Ec__AnonStorey3_t2573312903 * ___U24locvar1_22;

public:
	inline static int32_t get_offset_of_bundleId_0() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___bundleId_0)); }
	inline String_t* get_bundleId_0() const { return ___bundleId_0; }
	inline String_t** get_address_of_bundleId_0() { return &___bundleId_0; }
	inline void set_bundleId_0(String_t* value)
	{
		___bundleId_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleId_0, value);
	}

	inline static int32_t get_offset_of_U3CdownloadTokenU3E__0_1() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CdownloadTokenU3E__0_1)); }
	inline String_t* get_U3CdownloadTokenU3E__0_1() const { return ___U3CdownloadTokenU3E__0_1; }
	inline String_t** get_address_of_U3CdownloadTokenU3E__0_1() { return &___U3CdownloadTokenU3E__0_1; }
	inline void set_U3CdownloadTokenU3E__0_1(String_t* value)
	{
		___U3CdownloadTokenU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdownloadTokenU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CparametersU3E__1_2() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CparametersU3E__1_2)); }
	inline Dictionary_2_t3943999495 * get_U3CparametersU3E__1_2() const { return ___U3CparametersU3E__1_2; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CparametersU3E__1_2() { return &___U3CparametersU3E__1_2; }
	inline void set_U3CparametersU3E__1_2(Dictionary_2_t3943999495 * value)
	{
		___U3CparametersU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparametersU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E__2_3() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CresponseU3E__2_3)); }
	inline Il2CppObject * get_U3CresponseU3E__2_3() const { return ___U3CresponseU3E__2_3; }
	inline Il2CppObject ** get_address_of_U3CresponseU3E__2_3() { return &___U3CresponseU3E__2_3; }
	inline void set_U3CresponseU3E__2_3(Il2CppObject * value)
	{
		___U3CresponseU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresponseU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CdownloadTokenHashU3E__3_4() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CdownloadTokenHashU3E__3_4)); }
	inline Dictionary_2_t309261261 * get_U3CdownloadTokenHashU3E__3_4() const { return ___U3CdownloadTokenHashU3E__3_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CdownloadTokenHashU3E__3_4() { return &___U3CdownloadTokenHashU3E__3_4; }
	inline void set_U3CdownloadTokenHashU3E__3_4(Dictionary_2_t309261261 * value)
	{
		___U3CdownloadTokenHashU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdownloadTokenHashU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CsuccessU3E__4_5() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CsuccessU3E__4_5)); }
	inline bool get_U3CsuccessU3E__4_5() const { return ___U3CsuccessU3E__4_5; }
	inline bool* get_address_of_U3CsuccessU3E__4_5() { return &___U3CsuccessU3E__4_5; }
	inline void set_U3CsuccessU3E__4_5(bool value)
	{
		___U3CsuccessU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CversionU3E__5_6() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CversionU3E__5_6)); }
	inline String_t* get_U3CversionU3E__5_6() const { return ___U3CversionU3E__5_6; }
	inline String_t** get_address_of_U3CversionU3E__5_6() { return &___U3CversionU3E__5_6; }
	inline void set_U3CversionU3E__5_6(String_t* value)
	{
		___U3CversionU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CversionU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__6_7() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CheadersU3E__6_7)); }
	inline Dictionary_2_t3943999495 * get_U3CheadersU3E__6_7() const { return ___U3CheadersU3E__6_7; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CheadersU3E__6_7() { return &___U3CheadersU3E__6_7; }
	inline void set_U3CheadersU3E__6_7(Dictionary_2_t3943999495 * value)
	{
		___U3CheadersU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E__7_8() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CresponseU3E__7_8)); }
	inline Il2CppObject * get_U3CresponseU3E__7_8() const { return ___U3CresponseU3E__7_8; }
	inline Il2CppObject ** get_address_of_U3CresponseU3E__7_8() { return &___U3CresponseU3E__7_8; }
	inline void set_U3CresponseU3E__7_8(Il2CppObject * value)
	{
		___U3CresponseU3E__7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresponseU3E__7_8, value);
	}

	inline static int32_t get_offset_of_U3CcontentRangeU3E__8_9() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CcontentRangeU3E__8_9)); }
	inline String_t* get_U3CcontentRangeU3E__8_9() const { return ___U3CcontentRangeU3E__8_9; }
	inline String_t** get_address_of_U3CcontentRangeU3E__8_9() { return &___U3CcontentRangeU3E__8_9; }
	inline void set_U3CcontentRangeU3E__8_9(String_t* value)
	{
		___U3CcontentRangeU3E__8_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontentRangeU3E__8_9, value);
	}

	inline static int32_t get_offset_of_U3CcontentLengthU3E__9_10() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CcontentLengthU3E__9_10)); }
	inline int64_t get_U3CcontentLengthU3E__9_10() const { return ___U3CcontentLengthU3E__9_10; }
	inline int64_t* get_address_of_U3CcontentLengthU3E__9_10() { return &___U3CcontentLengthU3E__9_10; }
	inline void set_U3CcontentLengthU3E__9_10(int64_t value)
	{
		___U3CcontentLengthU3E__9_10 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_11() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U24locvar0_11)); }
	inline FileStream_t1695958676 * get_U24locvar0_11() const { return ___U24locvar0_11; }
	inline FileStream_t1695958676 ** get_address_of_U24locvar0_11() { return &___U24locvar0_11; }
	inline void set_U24locvar0_11(FileStream_t1695958676 * value)
	{
		___U24locvar0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_11, value);
	}

	inline static int32_t get_offset_of_U3CrangeStartU3E__A_12() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CrangeStartU3E__A_12)); }
	inline int64_t get_U3CrangeStartU3E__A_12() const { return ___U3CrangeStartU3E__A_12; }
	inline int64_t* get_address_of_U3CrangeStartU3E__A_12() { return &___U3CrangeStartU3E__A_12; }
	inline void set_U3CrangeStartU3E__A_12(int64_t value)
	{
		___U3CrangeStartU3E__A_12 = value;
	}

	inline static int32_t get_offset_of_U3CrangeEndU3E__B_13() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CrangeEndU3E__B_13)); }
	inline int64_t get_U3CrangeEndU3E__B_13() const { return ___U3CrangeEndU3E__B_13; }
	inline int64_t* get_address_of_U3CrangeEndU3E__B_13() { return &___U3CrangeEndU3E__B_13; }
	inline void set_U3CrangeEndU3E__B_13(int64_t value)
	{
		___U3CrangeEndU3E__B_13 = value;
	}

	inline static int32_t get_offset_of_U3ClastProgressU3E__C_14() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3ClastProgressU3E__C_14)); }
	inline int32_t get_U3ClastProgressU3E__C_14() const { return ___U3ClastProgressU3E__C_14; }
	inline int32_t* get_address_of_U3ClastProgressU3E__C_14() { return &___U3ClastProgressU3E__C_14; }
	inline void set_U3ClastProgressU3E__C_14(int32_t value)
	{
		___U3ClastProgressU3E__C_14 = value;
	}

	inline static int32_t get_offset_of_U3CheaderU3E__D_15() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CheaderU3E__D_15)); }
	inline String_t* get_U3CheaderU3E__D_15() const { return ___U3CheaderU3E__D_15; }
	inline String_t** get_address_of_U3CheaderU3E__D_15() { return &___U3CheaderU3E__D_15; }
	inline void set_U3CheaderU3E__D_15(String_t* value)
	{
		___U3CheaderU3E__D_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheaderU3E__D_15, value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E__E_16() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CresponseU3E__E_16)); }
	inline Il2CppObject * get_U3CresponseU3E__E_16() const { return ___U3CresponseU3E__E_16; }
	inline Il2CppObject ** get_address_of_U3CresponseU3E__E_16() { return &___U3CresponseU3E__E_16; }
	inline void set_U3CresponseU3E__E_16(Il2CppObject * value)
	{
		___U3CresponseU3E__E_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresponseU3E__E_16, value);
	}

	inline static int32_t get_offset_of_U3CprogressU3E__F_17() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U3CprogressU3E__F_17)); }
	inline int32_t get_U3CprogressU3E__F_17() const { return ___U3CprogressU3E__F_17; }
	inline int32_t* get_address_of_U3CprogressU3E__F_17() { return &___U3CprogressU3E__F_17; }
	inline void set_U3CprogressU3E__F_17(int32_t value)
	{
		___U3CprogressU3E__F_17 = value;
	}

	inline static int32_t get_offset_of_U24this_18() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U24this_18)); }
	inline DownloadManager_t32803451 * get_U24this_18() const { return ___U24this_18; }
	inline DownloadManager_t32803451 ** get_address_of_U24this_18() { return &___U24this_18; }
	inline void set_U24this_18(DownloadManager_t32803451 * value)
	{
		___U24this_18 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_18, value);
	}

	inline static int32_t get_offset_of_U24current_19() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U24current_19)); }
	inline Il2CppObject * get_U24current_19() const { return ___U24current_19; }
	inline Il2CppObject ** get_address_of_U24current_19() { return &___U24current_19; }
	inline void set_U24current_19(Il2CppObject * value)
	{
		___U24current_19 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_19, value);
	}

	inline static int32_t get_offset_of_U24disposing_20() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U24disposing_20)); }
	inline bool get_U24disposing_20() const { return ___U24disposing_20; }
	inline bool* get_address_of_U24disposing_20() { return &___U24disposing_20; }
	inline void set_U24disposing_20(bool value)
	{
		___U24disposing_20 = value;
	}

	inline static int32_t get_offset_of_U24PC_21() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U24PC_21)); }
	inline int32_t get_U24PC_21() const { return ___U24PC_21; }
	inline int32_t* get_address_of_U24PC_21() { return &___U24PC_21; }
	inline void set_U24PC_21(int32_t value)
	{
		___U24PC_21 = value;
	}

	inline static int32_t get_offset_of_U24locvar1_22() { return static_cast<int32_t>(offsetof(U3CdownloadU3Ec__Iterator2_t3409902454, ___U24locvar1_22)); }
	inline U3CdownloadU3Ec__AnonStorey3_t2573312903 * get_U24locvar1_22() const { return ___U24locvar1_22; }
	inline U3CdownloadU3Ec__AnonStorey3_t2573312903 ** get_address_of_U24locvar1_22() { return &___U24locvar1_22; }
	inline void set_U24locvar1_22(U3CdownloadU3Ec__AnonStorey3_t2573312903 * value)
	{
		___U24locvar1_22 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar1_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
