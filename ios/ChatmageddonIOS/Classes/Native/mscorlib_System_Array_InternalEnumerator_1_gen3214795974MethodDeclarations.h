﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3214795974.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"

// System.Void System.Array/InternalEnumerator`1<LeaderboardNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m765168559_gshared (InternalEnumerator_1_t3214795974 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m765168559(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3214795974 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m765168559_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<LeaderboardNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2591226679_gshared (InternalEnumerator_1_t3214795974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2591226679(__this, method) ((  void (*) (InternalEnumerator_1_t3214795974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2591226679_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<LeaderboardNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2318093715_gshared (InternalEnumerator_1_t3214795974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2318093715(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3214795974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2318093715_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<LeaderboardNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m682498360_gshared (InternalEnumerator_1_t3214795974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m682498360(__this, method) ((  void (*) (InternalEnumerator_1_t3214795974 *, const MethodInfo*))InternalEnumerator_1_Dispose_m682498360_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<LeaderboardNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1352603763_gshared (InternalEnumerator_1_t3214795974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1352603763(__this, method) ((  bool (*) (InternalEnumerator_1_t3214795974 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1352603763_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<LeaderboardNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3407846446_gshared (InternalEnumerator_1_t3214795974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3407846446(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3214795974 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3407846446_gshared)(__this, method)
