﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"

// System.Void PhoneNumbers.PhoneRegex::.ctor(System.String)
extern "C"  void PhoneRegex__ctor_m4060397520 (PhoneRegex_t3216508019 * __this, String_t* ___pattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneRegex::.ctor(System.String,System.Text.RegularExpressions.RegexOptions)
extern "C"  void PhoneRegex__ctor_m3317551076 (PhoneRegex_t3216508019 * __this, String_t* ___pattern0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Match PhoneNumbers.PhoneRegex::MatchAll(System.String)
extern "C"  Match_t3164245899 * PhoneRegex_MatchAll_m1755090653 (PhoneRegex_t3216508019 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Match PhoneNumbers.PhoneRegex::MatchBeginning(System.String)
extern "C"  Match_t3164245899 * PhoneRegex_MatchBeginning_m678372693 (PhoneRegex_t3216508019 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
