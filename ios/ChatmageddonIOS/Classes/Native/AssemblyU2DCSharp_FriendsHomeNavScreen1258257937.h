﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsHomeNavScreen
struct  FriendsHomeNavScreen_t1258257937  : public NavigationScreen_t2333230110
{
public:
	// System.Boolean FriendsHomeNavScreen::homeUIclosing
	bool ___homeUIclosing_3;

public:
	inline static int32_t get_offset_of_homeUIclosing_3() { return static_cast<int32_t>(offsetof(FriendsHomeNavScreen_t1258257937, ___homeUIclosing_3)); }
	inline bool get_homeUIclosing_3() const { return ___homeUIclosing_3; }
	inline bool* get_address_of_homeUIclosing_3() { return &___homeUIclosing_3; }
	inline void set_homeUIclosing_3(bool value)
	{
		___homeUIclosing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
