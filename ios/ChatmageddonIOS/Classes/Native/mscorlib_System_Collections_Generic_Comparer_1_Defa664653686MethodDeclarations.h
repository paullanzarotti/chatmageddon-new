﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<FriendNavScreen>
struct DefaultComparer_t664653686;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<FriendNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m4071942863_gshared (DefaultComparer_t664653686 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4071942863(__this, method) ((  void (*) (DefaultComparer_t664653686 *, const MethodInfo*))DefaultComparer__ctor_m4071942863_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<FriendNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1535335350_gshared (DefaultComparer_t664653686 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1535335350(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t664653686 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m1535335350_gshared)(__this, ___x0, ___y1, method)
