﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;
// Prime31.FacebookAuthHelper
struct FacebookAuthHelper_t1964649404;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookAuthHelper
struct  FacebookAuthHelper_t1964649404  : public Il2CppObject
{
public:
	// System.Action Prime31.FacebookAuthHelper::afterAuthAction
	Action_t3226471752 * ___afterAuthAction_0;
	// System.Boolean Prime31.FacebookAuthHelper::requiresPublishPermissions
	bool ___requiresPublishPermissions_1;

public:
	inline static int32_t get_offset_of_afterAuthAction_0() { return static_cast<int32_t>(offsetof(FacebookAuthHelper_t1964649404, ___afterAuthAction_0)); }
	inline Action_t3226471752 * get_afterAuthAction_0() const { return ___afterAuthAction_0; }
	inline Action_t3226471752 ** get_address_of_afterAuthAction_0() { return &___afterAuthAction_0; }
	inline void set_afterAuthAction_0(Action_t3226471752 * value)
	{
		___afterAuthAction_0 = value;
		Il2CppCodeGenWriteBarrier(&___afterAuthAction_0, value);
	}

	inline static int32_t get_offset_of_requiresPublishPermissions_1() { return static_cast<int32_t>(offsetof(FacebookAuthHelper_t1964649404, ___requiresPublishPermissions_1)); }
	inline bool get_requiresPublishPermissions_1() const { return ___requiresPublishPermissions_1; }
	inline bool* get_address_of_requiresPublishPermissions_1() { return &___requiresPublishPermissions_1; }
	inline void set_requiresPublishPermissions_1(bool value)
	{
		___requiresPublishPermissions_1 = value;
	}
};

struct FacebookAuthHelper_t1964649404_StaticFields
{
public:
	// Prime31.FacebookAuthHelper Prime31.FacebookAuthHelper::_instance
	FacebookAuthHelper_t1964649404 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(FacebookAuthHelper_t1964649404_StaticFields, ____instance_2)); }
	inline FacebookAuthHelper_t1964649404 * get__instance_2() const { return ____instance_2; }
	inline FacebookAuthHelper_t1964649404 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(FacebookAuthHelper_t1964649404 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
