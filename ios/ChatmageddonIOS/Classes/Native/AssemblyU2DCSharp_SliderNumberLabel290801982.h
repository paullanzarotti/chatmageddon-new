﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliderNumberLabel
struct  SliderNumberLabel_t290801982  : public Il2CppObject
{
public:
	// UILabel SliderNumberLabel::label
	UILabel_t1795115428 * ___label_0;
	// System.String SliderNumberLabel::measurementUnits
	String_t* ___measurementUnits_1;
	// System.String SliderNumberLabel::singleUnit
	String_t* ___singleUnit_2;
	// System.String SliderNumberLabel::pluralUnit
	String_t* ___pluralUnit_3;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(SliderNumberLabel_t290801982, ___label_0)); }
	inline UILabel_t1795115428 * get_label_0() const { return ___label_0; }
	inline UILabel_t1795115428 ** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(UILabel_t1795115428 * value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier(&___label_0, value);
	}

	inline static int32_t get_offset_of_measurementUnits_1() { return static_cast<int32_t>(offsetof(SliderNumberLabel_t290801982, ___measurementUnits_1)); }
	inline String_t* get_measurementUnits_1() const { return ___measurementUnits_1; }
	inline String_t** get_address_of_measurementUnits_1() { return &___measurementUnits_1; }
	inline void set_measurementUnits_1(String_t* value)
	{
		___measurementUnits_1 = value;
		Il2CppCodeGenWriteBarrier(&___measurementUnits_1, value);
	}

	inline static int32_t get_offset_of_singleUnit_2() { return static_cast<int32_t>(offsetof(SliderNumberLabel_t290801982, ___singleUnit_2)); }
	inline String_t* get_singleUnit_2() const { return ___singleUnit_2; }
	inline String_t** get_address_of_singleUnit_2() { return &___singleUnit_2; }
	inline void set_singleUnit_2(String_t* value)
	{
		___singleUnit_2 = value;
		Il2CppCodeGenWriteBarrier(&___singleUnit_2, value);
	}

	inline static int32_t get_offset_of_pluralUnit_3() { return static_cast<int32_t>(offsetof(SliderNumberLabel_t290801982, ___pluralUnit_3)); }
	inline String_t* get_pluralUnit_3() const { return ___pluralUnit_3; }
	inline String_t** get_address_of_pluralUnit_3() { return &___pluralUnit_3; }
	inline void set_pluralUnit_3(String_t* value)
	{
		___pluralUnit_3 = value;
		Il2CppCodeGenWriteBarrier(&___pluralUnit_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
