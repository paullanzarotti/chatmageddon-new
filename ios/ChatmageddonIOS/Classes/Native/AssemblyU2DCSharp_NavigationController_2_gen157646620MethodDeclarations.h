﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen1832792260MethodDeclarations.h"

// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::.ctor()
#define NavigationController_2__ctor_m459263440(__this, method) ((  void (*) (NavigationController_2_t157646620 *, const MethodInfo*))NavigationController_2__ctor_m890861153_gshared)(__this, method)
// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m4202566018(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t157646620 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1452032531_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m1555621637(__this, method) ((  void (*) (NavigationController_2_t157646620 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1579095628_gshared)(__this, method)
// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3708739716(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t157646620 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m2230020555_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m324106112(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t157646620 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m4172558399_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m3218000823(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t157646620 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1897244818_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m2108771006(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t157646620 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3898453107_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m4256627208(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t157646620 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m602541607_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<AttackHomeNavigationController,AttackHomeNav>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m4084901208(__this, ___screen0, method) ((  void (*) (NavigationController_2_t157646620 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2502305939_gshared)(__this, ___screen0, method)
