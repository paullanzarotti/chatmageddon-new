﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen1850157623.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsSearchNavigationController
struct  FriendsSearchNavigationController_t1013932095  : public NavigationController_2_t1850157623
{
public:
	// UILabel FriendsSearchNavigationController::friendsLabel
	UILabel_t1795115428 * ___friendsLabel_8;
	// UILabel FriendsSearchNavigationController::globalLabel
	UILabel_t1795115428 * ___globalLabel_9;
	// UnityEngine.GameObject FriendsSearchNavigationController::friendsObject
	GameObject_t1756533147 * ___friendsObject_10;
	// UnityEngine.GameObject FriendsSearchNavigationController::globalObject
	GameObject_t1756533147 * ___globalObject_11;
	// UnityEngine.Vector3 FriendsSearchNavigationController::activePos
	Vector3_t2243707580  ___activePos_12;
	// UnityEngine.Vector3 FriendsSearchNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_13;
	// UnityEngine.Vector3 FriendsSearchNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_14;

public:
	inline static int32_t get_offset_of_friendsLabel_8() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___friendsLabel_8)); }
	inline UILabel_t1795115428 * get_friendsLabel_8() const { return ___friendsLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_friendsLabel_8() { return &___friendsLabel_8; }
	inline void set_friendsLabel_8(UILabel_t1795115428 * value)
	{
		___friendsLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___friendsLabel_8, value);
	}

	inline static int32_t get_offset_of_globalLabel_9() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___globalLabel_9)); }
	inline UILabel_t1795115428 * get_globalLabel_9() const { return ___globalLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_globalLabel_9() { return &___globalLabel_9; }
	inline void set_globalLabel_9(UILabel_t1795115428 * value)
	{
		___globalLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___globalLabel_9, value);
	}

	inline static int32_t get_offset_of_friendsObject_10() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___friendsObject_10)); }
	inline GameObject_t1756533147 * get_friendsObject_10() const { return ___friendsObject_10; }
	inline GameObject_t1756533147 ** get_address_of_friendsObject_10() { return &___friendsObject_10; }
	inline void set_friendsObject_10(GameObject_t1756533147 * value)
	{
		___friendsObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___friendsObject_10, value);
	}

	inline static int32_t get_offset_of_globalObject_11() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___globalObject_11)); }
	inline GameObject_t1756533147 * get_globalObject_11() const { return ___globalObject_11; }
	inline GameObject_t1756533147 ** get_address_of_globalObject_11() { return &___globalObject_11; }
	inline void set_globalObject_11(GameObject_t1756533147 * value)
	{
		___globalObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___globalObject_11, value);
	}

	inline static int32_t get_offset_of_activePos_12() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___activePos_12)); }
	inline Vector3_t2243707580  get_activePos_12() const { return ___activePos_12; }
	inline Vector3_t2243707580 * get_address_of_activePos_12() { return &___activePos_12; }
	inline void set_activePos_12(Vector3_t2243707580  value)
	{
		___activePos_12 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_13() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___leftInactivePos_13)); }
	inline Vector3_t2243707580  get_leftInactivePos_13() const { return ___leftInactivePos_13; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_13() { return &___leftInactivePos_13; }
	inline void set_leftInactivePos_13(Vector3_t2243707580  value)
	{
		___leftInactivePos_13 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_14() { return static_cast<int32_t>(offsetof(FriendsSearchNavigationController_t1013932095, ___rightInactivePos_14)); }
	inline Vector3_t2243707580  get_rightInactivePos_14() const { return ___rightInactivePos_14; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_14() { return &___rightInactivePos_14; }
	inline void set_rightInactivePos_14(Vector3_t2243707580  value)
	{
		___rightInactivePos_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
