﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_LED
struct CameraFilterPack_TV_LED_t2945198757;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_LED::.ctor()
extern "C"  void CameraFilterPack_TV_LED__ctor_m844443340 (CameraFilterPack_TV_LED_t2945198757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_LED::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_LED_get_material_m220383333 (CameraFilterPack_TV_LED_t2945198757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::Start()
extern "C"  void CameraFilterPack_TV_LED_Start_m3849029676 (CameraFilterPack_TV_LED_t2945198757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_LED_OnRenderImage_m211379196 (CameraFilterPack_TV_LED_t2945198757 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::Update()
extern "C"  void CameraFilterPack_TV_LED_Update_m569715209 (CameraFilterPack_TV_LED_t2945198757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::OnDisable()
extern "C"  void CameraFilterPack_TV_LED_OnDisable_m1869627005 (CameraFilterPack_TV_LED_t2945198757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
