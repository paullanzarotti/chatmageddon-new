﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreZoneController
struct ScoreZoneController_t1333878292;
// ResultModalUI
struct ResultModalUI_t969511824;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultModalUI969511824.h"

// System.Void ScoreZoneController::.ctor()
extern "C"  void ScoreZoneController__ctor_m873504253 (ScoreZoneController_t1333878292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreZoneController::SetUpScoreZone(ResultModalUI)
extern "C"  void ScoreZoneController_SetUpScoreZone_m4192300382 (ScoreZoneController_t1333878292 * __this, ResultModalUI_t969511824 * ___modal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreZoneController::SetResultTrajectory()
extern "C"  void ScoreZoneController_SetResultTrajectory_m1052979125 (ScoreZoneController_t1333878292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreZoneController::SetUpTimeLabels()
extern "C"  void ScoreZoneController_SetUpTimeLabels_m1241308652 (ScoreZoneController_t1333878292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreZoneController::SetUpStealthMissile()
extern "C"  void ScoreZoneController_SetUpStealthMissile_m1222614433 (ScoreZoneController_t1333878292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
