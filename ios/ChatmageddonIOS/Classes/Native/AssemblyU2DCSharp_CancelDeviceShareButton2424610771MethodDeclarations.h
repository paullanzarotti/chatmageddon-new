﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CancelDeviceShareButton
struct CancelDeviceShareButton_t2424610771;

#include "codegen/il2cpp-codegen.h"

// System.Void CancelDeviceShareButton::.ctor()
extern "C"  void CancelDeviceShareButton__ctor_m262987466 (CancelDeviceShareButton_t2424610771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CancelDeviceShareButton::OnButtonClick()
extern "C"  void CancelDeviceShareButton_OnButtonClick_m833787819 (CancelDeviceShareButton_t2424610771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
