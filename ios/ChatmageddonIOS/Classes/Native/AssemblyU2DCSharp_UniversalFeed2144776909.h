﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_Feed3408144164.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniversalFeed
struct  UniversalFeed_t2144776909  : public Feed_t3408144164
{
public:

public:
};

struct UniversalFeed_t2144776909_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_6;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache1
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache1_7;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache2
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache2_8;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache3
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache3_9;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache4
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache4_10;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache5
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache5_11;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache6
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache6_12;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache7
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache7_13;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache8
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache8_14;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> UniversalFeed::<>f__am$cache9
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache9_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_10() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache4_10)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache4_10() const { return ___U3CU3Ef__amU24cache4_10; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache4_10() { return &___U3CU3Ef__amU24cache4_10; }
	inline void set_U3CU3Ef__amU24cache4_10(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache4_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_11() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache5_11)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache5_11() const { return ___U3CU3Ef__amU24cache5_11; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache5_11() { return &___U3CU3Ef__amU24cache5_11; }
	inline void set_U3CU3Ef__amU24cache5_11(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache5_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_12() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache6_12)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache6_12() const { return ___U3CU3Ef__amU24cache6_12; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache6_12() { return &___U3CU3Ef__amU24cache6_12; }
	inline void set_U3CU3Ef__amU24cache6_12(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache6_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_13() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache7_13)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache7_13() const { return ___U3CU3Ef__amU24cache7_13; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache7_13() { return &___U3CU3Ef__amU24cache7_13; }
	inline void set_U3CU3Ef__amU24cache7_13(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache7_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_14() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache8_14)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache8_14() const { return ___U3CU3Ef__amU24cache8_14; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache8_14() { return &___U3CU3Ef__amU24cache8_14; }
	inline void set_U3CU3Ef__amU24cache8_14(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache8_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_15() { return static_cast<int32_t>(offsetof(UniversalFeed_t2144776909_StaticFields, ___U3CU3Ef__amU24cache9_15)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache9_15() const { return ___U3CU3Ef__amU24cache9_15; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache9_15() { return &___U3CU3Ef__amU24cache9_15; }
	inline void set_U3CU3Ef__amU24cache9_15(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache9_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
