﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageModel
struct LanguageModel_t2428340347;
// System.String
struct String_t;
// User
struct User_t719925459;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// Missile
struct Missile_t813944928;
// LaunchedMine
struct LaunchedMine_t3677282773;
// Mine
struct Mine_t2729441277;
// Shield
struct Shield_t3327121081;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_LaunchedMine3677282773.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"

// System.Void LanguageModel::.ctor()
extern "C"  void LanguageModel__ctor_m1803072634 (LanguageModel_t2428340347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LanguageModel::GetAttributeString(System.String)
extern "C"  String_t* LanguageModel_GetAttributeString_m3135156556 (LanguageModel_t2428340347 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,System.String)
extern "C"  void LanguageModel_AddObject_m3467800580 (LanguageModel_t2428340347 * __this, String_t* ___key0, String_t* ___objectString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,User)
extern "C"  void LanguageModel_AddObject_m3911119665 (LanguageModel_t2428340347 * __this, String_t* ___key0, User_t719925459 * ___user1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,LaunchedMissile)
extern "C"  void LanguageModel_AddObject_m1127973986 (LanguageModel_t2428340347 * __this, String_t* ___key0, LaunchedMissile_t1542515810 * ___missile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,Missile)
extern "C"  void LanguageModel_AddObject_m84577100 (LanguageModel_t2428340347 * __this, String_t* ___key0, Missile_t813944928 * ___missile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,LaunchedMine)
extern "C"  void LanguageModel_AddObject_m2635184791 (LanguageModel_t2428340347 * __this, String_t* ___key0, LaunchedMine_t3677282773 * ___mine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,Mine)
extern "C"  void LanguageModel_AddObject_m3746138263 (LanguageModel_t2428340347 * __this, String_t* ___key0, Mine_t2729441277 * ___mine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::AddObject(System.String,Shield)
extern "C"  void LanguageModel_AddObject_m730701563 (LanguageModel_t2428340347 * __this, String_t* ___key0, Shield_t3327121081 * ___shield1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageModel::RemoveActiveKey(System.String)
extern "C"  void LanguageModel_RemoveActiveKey_m2922013309 (LanguageModel_t2428340347 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
