﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<LoginViaUsername>c__AnonStorey5
struct U3CLoginViaUsernameU3Ec__AnonStorey5_t3823169292;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<LoginViaUsername>c__AnonStorey5::.ctor()
extern "C"  void U3CLoginViaUsernameU3Ec__AnonStorey5__ctor_m565503499 (U3CLoginViaUsernameU3Ec__AnonStorey5_t3823169292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<LoginViaUsername>c__AnonStorey5::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLoginViaUsernameU3Ec__AnonStorey5_U3CU3Em__0_m1898589284 (U3CLoginViaUsernameU3Ec__AnonStorey5_t3823169292 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
