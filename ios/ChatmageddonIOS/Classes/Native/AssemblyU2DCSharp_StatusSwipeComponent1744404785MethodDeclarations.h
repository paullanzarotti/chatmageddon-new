﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusSwipeComponent
struct StatusSwipeComponent_t1744404785;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void StatusSwipeComponent::.ctor()
extern "C"  void StatusSwipeComponent__ctor_m2924832666 (StatusSwipeComponent_t1744404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusSwipeComponent::ValidateSwipeLeft()
extern "C"  bool StatusSwipeComponent_ValidateSwipeLeft_m2395076337 (StatusSwipeComponent_t1744404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusSwipeComponent::OnSwipeLeft()
extern "C"  void StatusSwipeComponent_OnSwipeLeft_m1783936322 (StatusSwipeComponent_t1744404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusSwipeComponent::ValidateSwipeRight()
extern "C"  bool StatusSwipeComponent_ValidateSwipeRight_m1772388284 (StatusSwipeComponent_t1744404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusSwipeComponent::OnSwipeRight()
extern "C"  void StatusSwipeComponent_OnSwipeRight_m3478756433 (StatusSwipeComponent_t1744404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusSwipeComponent::OnDragUp(UnityEngine.Vector2)
extern "C"  void StatusSwipeComponent_OnDragUp_m2358705558 (StatusSwipeComponent_t1744404785 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
