﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_LinearBurn
struct CameraFilterPack_Blend2Camera_LinearBurn_t1186093094;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_LinearBurn::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn__ctor_m1918420603 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LinearBurn::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_LinearBurn_get_material_m1884230388 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::Start()
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn_Start_m1633806151 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn_OnRenderImage_m1143223255 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn_OnValidate_m3493199554 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::Update()
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn_Update_m1915333428 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn_OnEnable_m3121947111 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_LinearBurn_OnDisable_m778208682 (CameraFilterPack_Blend2Camera_LinearBurn_t1186093094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
