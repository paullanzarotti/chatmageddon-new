﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AttackSearchNav>
struct List_1_t3627005769;
// System.Collections.Generic.IEnumerable`1<AttackSearchNav>
struct IEnumerable_1_t255044386;
// AttackSearchNav[]
struct AttackSearchNavU5BU5D_t406956560;
// System.Collections.Generic.IEnumerator`1<AttackSearchNav>
struct IEnumerator_1_t1733408464;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<AttackSearchNav>
struct ICollection_1_t914992646;
// System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>
struct ReadOnlyCollection_1_t148703033;
// System.Predicate`1<AttackSearchNav>
struct Predicate_1_t2700854752;
// System.Action`1<AttackSearchNav>
struct Action_1_t4059684019;
// System.Collections.Generic.IComparer`1<AttackSearchNav>
struct IComparer_1_t2212347759;
// System.Comparison`1<AttackSearchNav>
struct Comparison_1_t1224656192;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3161735443.h"

// System.Void System.Collections.Generic.List`1<AttackSearchNav>::.ctor()
extern "C"  void List_1__ctor_m3831569989_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1__ctor_m3831569989(__this, method) ((  void (*) (List_1_t3627005769 *, const MethodInfo*))List_1__ctor_m3831569989_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3962822870_gshared (List_1_t3627005769 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3962822870(__this, ___collection0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3962822870_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m144888568_gshared (List_1_t3627005769 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m144888568(__this, ___capacity0, method) ((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1__ctor_m144888568_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m371267508_gshared (List_1_t3627005769 * __this, AttackSearchNavU5BU5D_t406956560* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m371267508(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3627005769 *, AttackSearchNavU5BU5D_t406956560*, int32_t, const MethodInfo*))List_1__ctor_m371267508_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::.cctor()
extern "C"  void List_1__cctor_m4076618400_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4076618400(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4076618400_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1627887849_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1627887849(__this, method) ((  Il2CppObject* (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1627887849_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1437929813_gshared (List_1_t3627005769 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1437929813(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3627005769 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1437929813_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3292238550_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3292238550(__this, method) ((  Il2CppObject * (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3292238550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3153279385_gshared (List_1_t3627005769 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3153279385(__this, ___item0, method) ((  int32_t (*) (List_1_t3627005769 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3153279385_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1391829925_gshared (List_1_t3627005769 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1391829925(__this, ___item0, method) ((  bool (*) (List_1_t3627005769 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1391829925_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m110572587_gshared (List_1_t3627005769 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m110572587(__this, ___item0, method) ((  int32_t (*) (List_1_t3627005769 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m110572587_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3046021120_gshared (List_1_t3627005769 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3046021120(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3627005769 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3046021120_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m487821578_gshared (List_1_t3627005769 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m487821578(__this, ___item0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m487821578_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3160230610_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3160230610(__this, method) ((  bool (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3160230610_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m239648465_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m239648465(__this, method) ((  bool (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m239648465_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1385296693_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1385296693(__this, method) ((  Il2CppObject * (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1385296693_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m193317384_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m193317384(__this, method) ((  bool (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m193317384_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1359137597_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1359137597(__this, method) ((  bool (*) (List_1_t3627005769 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1359137597_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3289194028_gshared (List_1_t3627005769 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3289194028(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3289194028_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3520645531_gshared (List_1_t3627005769 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3520645531(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3627005769 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3520645531_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Add(T)
extern "C"  void List_1_Add_m2470589652_gshared (List_1_t3627005769 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2470589652(__this, ___item0, method) ((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_Add_m2470589652_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m4044222877_gshared (List_1_t3627005769 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m4044222877(__this, ___newCount0, method) ((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4044222877_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2244386696_gshared (List_1_t3627005769 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2244386696(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3627005769 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2244386696_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2695352877_gshared (List_1_t3627005769 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2695352877(__this, ___collection0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2695352877_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3982989325_gshared (List_1_t3627005769 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3982989325(__this, ___enumerable0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3982989325_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m859678786_gshared (List_1_t3627005769 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m859678786(__this, ___collection0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m859678786_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AttackSearchNav>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t148703033 * List_1_AsReadOnly_m2678676677_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2678676677(__this, method) ((  ReadOnlyCollection_1_t148703033 * (*) (List_1_t3627005769 *, const MethodInfo*))List_1_AsReadOnly_m2678676677_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Clear()
extern "C"  void List_1_Clear_m1929205936_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_Clear_m1929205936(__this, method) ((  void (*) (List_1_t3627005769 *, const MethodInfo*))List_1_Clear_m1929205936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::Contains(T)
extern "C"  bool List_1_Contains_m226115866_gshared (List_1_t3627005769 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m226115866(__this, ___item0, method) ((  bool (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_Contains_m226115866_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1627091941_gshared (List_1_t3627005769 * __this, AttackSearchNavU5BU5D_t406956560* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1627091941(__this, ___array0, method) ((  void (*) (List_1_t3627005769 *, AttackSearchNavU5BU5D_t406956560*, const MethodInfo*))List_1_CopyTo_m1627091941_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2147393940_gshared (List_1_t3627005769 * __this, AttackSearchNavU5BU5D_t406956560* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2147393940(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3627005769 *, AttackSearchNavU5BU5D_t406956560*, int32_t, const MethodInfo*))List_1_CopyTo_m2147393940_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m1805187414_gshared (List_1_t3627005769 * __this, int32_t ___index0, AttackSearchNavU5BU5D_t406956560* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m1805187414(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3627005769 *, int32_t, AttackSearchNavU5BU5D_t406956560*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1805187414_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m1885466666_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_Exists_m1885466666(__this, ___match0, method) ((  bool (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_Exists_m1885466666_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<AttackSearchNav>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1847139380_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_Find_m1847139380(__this, ___match0, method) ((  int32_t (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_Find_m1847139380_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m290374141_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m290374141(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2700854752 *, const MethodInfo*))List_1_CheckMatch_m290374141_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackSearchNav>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3627005769 * List_1_FindAll_m717371221_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m717371221(__this, ___match0, method) ((  List_1_t3627005769 * (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_FindAll_m717371221_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackSearchNav>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3627005769 * List_1_FindAllStackBits_m3393351239_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3393351239(__this, ___match0, method) ((  List_1_t3627005769 * (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_FindAllStackBits_m3393351239_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<AttackSearchNav>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3627005769 * List_1_FindAllList_m85593447_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m85593447(__this, ___match0, method) ((  List_1_t3627005769 * (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_FindAllList_m85593447_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m3646012257_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m3646012257(__this, ___match0, method) ((  int32_t (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_FindIndex_m3646012257_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m865027772_gshared (List_1_t3627005769 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2700854752 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m865027772(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3627005769 *, int32_t, int32_t, Predicate_1_t2700854752 *, const MethodInfo*))List_1_GetIndex_m865027772_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m771445943_gshared (List_1_t3627005769 * __this, Action_1_t4059684019 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m771445943(__this, ___action0, method) ((  void (*) (List_1_t3627005769 *, Action_1_t4059684019 *, const MethodInfo*))List_1_ForEach_m771445943_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AttackSearchNav>::GetEnumerator()
extern "C"  Enumerator_t3161735443  List_1_GetEnumerator_m174912181_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m174912181(__this, method) ((  Enumerator_t3161735443  (*) (List_1_t3627005769 *, const MethodInfo*))List_1_GetEnumerator_m174912181_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m881175102_gshared (List_1_t3627005769 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m881175102(__this, ___item0, method) ((  int32_t (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_IndexOf_m881175102_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4095570913_gshared (List_1_t3627005769 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4095570913(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3627005769 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4095570913_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m198739496_gshared (List_1_t3627005769 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m198739496(__this, ___index0, method) ((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_CheckIndex_m198739496_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m440547851_gshared (List_1_t3627005769 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m440547851(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3627005769 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m440547851_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3585436242_gshared (List_1_t3627005769 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3585436242(__this, ___collection0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3585436242_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<AttackSearchNav>::Remove(T)
extern "C"  bool List_1_Remove_m2031953183_gshared (List_1_t3627005769 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2031953183(__this, ___item0, method) ((  bool (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_Remove_m2031953183_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1591609113_gshared (List_1_t3627005769 * __this, Predicate_1_t2700854752 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1591609113(__this, ___match0, method) ((  int32_t (*) (List_1_t3627005769 *, Predicate_1_t2700854752 *, const MethodInfo*))List_1_RemoveAll_m1591609113_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2162585527_gshared (List_1_t3627005769 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2162585527(__this, ___index0, method) ((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2162585527_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1801992044_gshared (List_1_t3627005769 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1801992044(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3627005769 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1801992044_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Reverse()
extern "C"  void List_1_Reverse_m393872545_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_Reverse_m393872545(__this, method) ((  void (*) (List_1_t3627005769 *, const MethodInfo*))List_1_Reverse_m393872545_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Sort()
extern "C"  void List_1_Sort_m2565389311_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_Sort_m2565389311(__this, method) ((  void (*) (List_1_t3627005769 *, const MethodInfo*))List_1_Sort_m2565389311_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2129227931_gshared (List_1_t3627005769 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2129227931(__this, ___comparer0, method) ((  void (*) (List_1_t3627005769 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2129227931_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2404991544_gshared (List_1_t3627005769 * __this, Comparison_1_t1224656192 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2404991544(__this, ___comparison0, method) ((  void (*) (List_1_t3627005769 *, Comparison_1_t1224656192 *, const MethodInfo*))List_1_Sort_m2404991544_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<AttackSearchNav>::ToArray()
extern "C"  AttackSearchNavU5BU5D_t406956560* List_1_ToArray_m942876448_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_ToArray_m942876448(__this, method) ((  AttackSearchNavU5BU5D_t406956560* (*) (List_1_t3627005769 *, const MethodInfo*))List_1_ToArray_m942876448_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2274933110_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2274933110(__this, method) ((  void (*) (List_1_t3627005769 *, const MethodInfo*))List_1_TrimExcess_m2274933110_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3952413316_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3952413316(__this, method) ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))List_1_get_Capacity_m3952413316_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2162091917_gshared (List_1_t3627005769 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2162091917(__this, ___value0, method) ((  void (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2162091917_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<AttackSearchNav>::get_Count()
extern "C"  int32_t List_1_get_Count_m3425430745_gshared (List_1_t3627005769 * __this, const MethodInfo* method);
#define List_1_get_Count_m3425430745(__this, method) ((  int32_t (*) (List_1_t3627005769 *, const MethodInfo*))List_1_get_Count_m3425430745_gshared)(__this, method)
// T System.Collections.Generic.List`1<AttackSearchNav>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m4009063_gshared (List_1_t3627005769 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4009063(__this, ___index0, method) ((  int32_t (*) (List_1_t3627005769 *, int32_t, const MethodInfo*))List_1_get_Item_m4009063_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AttackSearchNav>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2098427542_gshared (List_1_t3627005769 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m2098427542(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3627005769 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m2098427542_gshared)(__this, ___index0, ___value1, method)
