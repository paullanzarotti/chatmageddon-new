﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.Int16>
struct EqualityComparer_1_t2614881185;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.Int16>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1396533254_gshared (EqualityComparer_1_t2614881185 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1396533254(__this, method) ((  void (*) (EqualityComparer_1_t2614881185 *, const MethodInfo*))EqualityComparer_1__ctor_m1396533254_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int16>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3609988661_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3609988661(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3609988661_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int16>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m55248833_gshared (EqualityComparer_1_t2614881185 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m55248833(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2614881185 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m55248833_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int16>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m590872539_gshared (EqualityComparer_1_t2614881185 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m590872539(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2614881185 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m590872539_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int16>::get_Default()
extern "C"  EqualityComparer_1_t2614881185 * EqualityComparer_1_get_Default_m4047747474_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m4047747474(__this /* static, unused */, method) ((  EqualityComparer_1_t2614881185 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m4047747474_gshared)(__this /* static, unused */, method)
