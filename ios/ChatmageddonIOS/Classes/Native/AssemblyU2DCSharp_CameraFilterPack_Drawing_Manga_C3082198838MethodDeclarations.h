﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Manga_Color
struct CameraFilterPack_Drawing_Manga_Color_t3082198838;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Manga_Color::.ctor()
extern "C"  void CameraFilterPack_Drawing_Manga_Color__ctor_m3677422197 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga_Color::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Manga_Color_get_material_m4050232636 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::Start()
extern "C"  void CameraFilterPack_Drawing_Manga_Color_Start_m3027359797 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Manga_Color_OnRenderImage_m3460196357 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Manga_Color_OnValidate_m1654790978 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::Update()
extern "C"  void CameraFilterPack_Drawing_Manga_Color_Update_m4087434516 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Manga_Color_OnDisable_m3080976298 (CameraFilterPack_Drawing_Manga_Color_t3082198838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
