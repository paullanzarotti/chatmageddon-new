﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnibillError>
struct EqualityComparer_1_t327495058;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnibillError>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m893361688_gshared (EqualityComparer_1_t327495058 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m893361688(__this, method) ((  void (*) (EqualityComparer_1_t327495058 *, const MethodInfo*))EqualityComparer_1__ctor_m893361688_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnibillError>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3468338107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3468338107(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3468338107_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnibillError>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4097231263_gshared (EqualityComparer_1_t327495058 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4097231263(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t327495058 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4097231263_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnibillError>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1914072325_gshared (EqualityComparer_1_t327495058 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1914072325(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t327495058 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1914072325_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnibillError>::get_Default()
extern "C"  EqualityComparer_1_t327495058 * EqualityComparer_1_get_Default_m2191678352_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2191678352(__this /* static, unused */, method) ((  EqualityComparer_1_t327495058 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2191678352_gshared)(__this /* static, unused */, method)
