﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<VerifyMobileNumber>c__AnonStorey3
struct U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<VerifyMobileNumber>c__AnonStorey3::.ctor()
extern "C"  void U3CVerifyMobileNumberU3Ec__AnonStorey3__ctor_m1009692792 (U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<VerifyMobileNumber>c__AnonStorey3::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CVerifyMobileNumberU3Ec__AnonStorey3_U3CU3Em__0_m348480511 (U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
