﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3717830400.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"

// System.Void System.Array/InternalEnumerator`1<Vectrosity.Vector3Pair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4244062723_gshared (InternalEnumerator_1_t3717830400 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4244062723(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3717830400 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4244062723_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Vectrosity.Vector3Pair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3155752499_gshared (InternalEnumerator_1_t3717830400 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3155752499(__this, method) ((  void (*) (InternalEnumerator_1_t3717830400 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3155752499_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vectrosity.Vector3Pair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435100407_gshared (InternalEnumerator_1_t3717830400 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435100407(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3717830400 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435100407_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vectrosity.Vector3Pair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2187267252_gshared (InternalEnumerator_1_t3717830400 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2187267252(__this, method) ((  void (*) (InternalEnumerator_1_t3717830400 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2187267252_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vectrosity.Vector3Pair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2525809639_gshared (InternalEnumerator_1_t3717830400 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2525809639(__this, method) ((  bool (*) (InternalEnumerator_1_t3717830400 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2525809639_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vectrosity.Vector3Pair>::get_Current()
extern "C"  Vector3Pair_t2859078138  InternalEnumerator_1_get_Current_m689646834_gshared (InternalEnumerator_1_t3717830400 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m689646834(__this, method) ((  Vector3Pair_t2859078138  (*) (InternalEnumerator_1_t3717830400 *, const MethodInfo*))InternalEnumerator_1_get_Current_m689646834_gshared)(__this, method)
