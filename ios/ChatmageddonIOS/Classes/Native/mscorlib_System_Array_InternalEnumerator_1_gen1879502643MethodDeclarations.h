﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1879502643.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21020750381.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2591608649_gshared (InternalEnumerator_1_t1879502643 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2591608649(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1879502643 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2591608649_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3089402269_gshared (InternalEnumerator_1_t1879502643 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3089402269(__this, method) ((  void (*) (InternalEnumerator_1_t1879502643 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3089402269_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1978921333_gshared (InternalEnumerator_1_t1879502643 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1978921333(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1879502643 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1978921333_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1631048970_gshared (InternalEnumerator_1_t1879502643 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1631048970(__this, method) ((  void (*) (InternalEnumerator_1_t1879502643 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1631048970_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2631370261_gshared (InternalEnumerator_1_t1879502643 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2631370261(__this, method) ((  bool (*) (InternalEnumerator_1_t1879502643 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2631370261_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>>::get_Current()
extern "C"  KeyValuePair_2_t1020750381  InternalEnumerator_1_get_Current_m4007999376_gshared (InternalEnumerator_1_t1879502643 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4007999376(__this, method) ((  KeyValuePair_2_t1020750381  (*) (InternalEnumerator_1_t1879502643 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4007999376_gshared)(__this, method)
