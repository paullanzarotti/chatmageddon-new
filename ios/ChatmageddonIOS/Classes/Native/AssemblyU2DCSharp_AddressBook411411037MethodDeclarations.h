﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddressBook
struct AddressBook_t411411037;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// ValidContact
struct ValidContact_t1479914934;
// Contact
struct Contact_t4014196408;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Contact4014196408.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ValidContact1479914934.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void AddressBook::.ctor()
extern "C"  void AddressBook__ctor_m66286360 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::Awake()
extern "C"  void AddressBook_Awake_m2739227337 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil AddressBook::InitializePhoneUtilForTesting()
extern "C"  PhoneNumberUtil_t4155573397 * AddressBook_InitializePhoneUtilForTesting_m4196339523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::GetContacts(System.Int32,System.Int64)
extern "C"  void AddressBook_GetContacts_m433000718 (AddressBook_t411411037 * __this, int32_t ___startIndex0, int64_t ___amountOfContacts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AddressBook::LoadIosContacts(System.Int32,System.Int64)
extern "C"  Il2CppObject * AddressBook_LoadIosContacts_m3871688001 (AddressBook_t411411037 * __this, int32_t ___index0, int64_t ___contactAmount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::ContactLoadingComplete()
extern "C"  void AddressBook_ContactLoadingComplete_m3168025499 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::KeyExistingContacts()
extern "C"  void AddressBook_KeyExistingContacts_m2792706787 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::DiffContacts()
extern "C"  void AddressBook_DiffContacts_m4165539472 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AddressBook::ChecksumStringList(System.Collections.Generic.List`1<System.String>,System.Boolean)
extern "C"  String_t* AddressBook_ChecksumStringList_m4140605642 (AddressBook_t411411037 * __this, List_1_t1398341365 * ___strs0, bool ___sort1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AddressBook::GenerateAddressBookChecksum()
extern "C"  String_t* AddressBook_GenerateAddressBookChecksum_m333111546 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ValidContact AddressBook::CreateValidContact(Contact,System.String)
extern "C"  ValidContact_t1479914934 * AddressBook_CreateValidContact_m174827105 (AddressBook_t411411037 * __this, Contact_t4014196408 * ___contact0, String_t* ___rawChecksum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::DeleteUnfoundExistingContacts()
extern "C"  void AddressBook_DeleteUnfoundExistingContacts_m1737172424 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::ImportNewContacts()
extern "C"  void AddressBook_ImportNewContacts_m138612532 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::ContactClearingComplete()
extern "C"  void AddressBook_ContactClearingComplete_m3094280892 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::ContactImportComplete()
extern "C"  void AddressBook_ContactImportComplete_m733440174 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::BuildFinalValidContactsList()
extern "C"  void AddressBook_BuildFinalValidContactsList_m2908973161 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::SaveContactsBatchToServer(System.Collections.Generic.List`1<System.String>,System.Int32)
extern "C"  void AddressBook_SaveContactsBatchToServer_m826971025 (AddressBook_t411411037 * __this, List_1_t1398341365 * ___batch0, int32_t ___batchID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::DeleteContactsBatchFromServer(System.Collections.Generic.List`1<System.String>,System.Int32)
extern "C"  void AddressBook_DeleteContactsBatchFromServer_m130082040 (AddressBook_t411411037 * __this, List_1_t1398341365 * ___batch0, int32_t ___batchID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::ClearAllContactsFromServer()
extern "C"  void AddressBook_ClearAllContactsFromServer_m2983113940 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::SetContactsLoaded()
extern "C"  void AddressBook_SetContactsLoaded_m536778808 (AddressBook_t411411037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AddressBook::Md5Sum(System.String)
extern "C"  String_t* AddressBook_Md5Sum_m958752352 (Il2CppObject * __this /* static, unused */, String_t* ___strToEncrypt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::.cctor()
extern "C"  void AddressBook__cctor_m2459288231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AddressBook::<ContactLoadingComplete>m__0(ValidContact)
extern "C"  String_t* AddressBook_U3CContactLoadingCompleteU3Em__0_m3004524615 (Il2CppObject * __this /* static, unused */, ValidContact_t1479914934 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AddressBook::<BuildFinalValidContactsList>m__1(ValidContact)
extern "C"  bool AddressBook_U3CBuildFinalValidContactsListU3Em__1_m2395996741 (Il2CppObject * __this /* static, unused */, ValidContact_t1479914934 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AddressBook::<BuildFinalValidContactsList>m__2(ValidContact)
extern "C"  String_t* AddressBook_U3CBuildFinalValidContactsListU3Em__2_m2599074999 (Il2CppObject * __this /* static, unused */, ValidContact_t1479914934 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::<SaveContactsBatchToServer>m__3(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void AddressBook_U3CSaveContactsBatchToServerU3Em__3_m2750041629 (AddressBook_t411411037 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook::<ClearAllContactsFromServer>m__4(System.Boolean,System.String)
extern "C"  void AddressBook_U3CClearAllContactsFromServerU3Em__4_m1049434944 (AddressBook_t411411037 * __this, bool ___success0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
