﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<Min>c__AnonStorey2E`1<System.Object>
struct U3CMinU3Ec__AnonStorey2E_1_t1495211345;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Linq.Enumerable/<Min>c__AnonStorey2E`1<System.Object>::.ctor()
extern "C"  void U3CMinU3Ec__AnonStorey2E_1__ctor_m1436661890_gshared (U3CMinU3Ec__AnonStorey2E_1_t1495211345 * __this, const MethodInfo* method);
#define U3CMinU3Ec__AnonStorey2E_1__ctor_m1436661890(__this, method) ((  void (*) (U3CMinU3Ec__AnonStorey2E_1_t1495211345 *, const MethodInfo*))U3CMinU3Ec__AnonStorey2E_1__ctor_m1436661890_gshared)(__this, method)
// System.Single System.Linq.Enumerable/<Min>c__AnonStorey2E`1<System.Object>::<>m__2F(TSource,System.Single)
extern "C"  float U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m479781021_gshared (U3CMinU3Ec__AnonStorey2E_1_t1495211345 * __this, Il2CppObject * ___a0, float ___b1, const MethodInfo* method);
#define U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m479781021(__this, ___a0, ___b1, method) ((  float (*) (U3CMinU3Ec__AnonStorey2E_1_t1495211345 *, Il2CppObject *, float, const MethodInfo*))U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m479781021_gshared)(__this, ___a0, ___b1, method)
