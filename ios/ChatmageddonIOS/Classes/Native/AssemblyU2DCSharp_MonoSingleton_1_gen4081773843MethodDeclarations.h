﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<FriendManager>::.ctor()
#define MonoSingleton_1__ctor_m2134000725(__this, method) ((  void (*) (MonoSingleton_1_t4081773843 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendManager>::Awake()
#define MonoSingleton_1_Awake_m1506757610(__this, method) ((  void (*) (MonoSingleton_1_t4081773843 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<FriendManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2787360518(__this /* static, unused */, method) ((  FriendManager_t36140827 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<FriendManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1423037380(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<FriendManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2006673903(__this, method) ((  void (*) (MonoSingleton_1_t4081773843 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2512528331(__this, method) ((  void (*) (MonoSingleton_1_t4081773843 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendManager>::.cctor()
#define MonoSingleton_1__cctor_m2869724632(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
