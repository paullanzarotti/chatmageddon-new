﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// OnlineMapsTile/OnGetResourcesPathDelegate
struct OnGetResourcesPathDelegate_t2869117133;
// System.Action`1<OnlineMapsTile>
struct Action_1_t4118096618;
// System.Collections.Generic.List`1<OnlineMapsTile>
struct List_1_t3685418368;
// OnlineMaps
struct OnlineMaps_t1893290312;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.WWW
struct WWW_t2919945039;
// OnlineMapsTile[]
struct OnlineMapsTileU5BU5D_t2146591005;
// System.Func`2<OnlineMapsTile,System.Boolean>
struct Func_2_t2493586475;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsProviderEnum1784547284.h"
#include "AssemblyU2DCSharp_OnlineMapsTileStatus3389371110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsTile
struct  OnlineMapsTile_t21329940  : public Il2CppObject
{
public:
	// System.Action`1<OnlineMapsTile> OnlineMapsTile::OnSetColor
	Action_1_t4118096618 * ___OnSetColor_5;
	// UnityEngine.Vector2 OnlineMapsTile::bottomRight
	Vector2_t2243707579  ___bottomRight_7;
	// System.Object OnlineMapsTile::customData
	Il2CppObject * ___customData_8;
	// System.Byte[] OnlineMapsTile::data
	ByteU5BU5D_t3397334013* ___data_9;
	// UnityEngine.Vector2 OnlineMapsTile::globalPosition
	Vector2_t2243707579  ___globalPosition_10;
	// System.Boolean OnlineMapsTile::hasColors
	bool ___hasColors_11;
	// System.Boolean OnlineMapsTile::isMapTile
	bool ___isMapTile_12;
	// System.Boolean OnlineMapsTile::labels
	bool ___labels_13;
	// System.String OnlineMapsTile::language
	String_t* ___language_14;
	// OnlineMapsTile OnlineMapsTile::parent
	OnlineMapsTile_t21329940 * ___parent_15;
	// System.Int16 OnlineMapsTile::priority
	int16_t ___priority_16;
	// OnlineMapsProviderEnum OnlineMapsTile::provider
	int32_t ___provider_17;
	// OnlineMapsTileStatus OnlineMapsTile::status
	int32_t ___status_18;
	// UnityEngine.Texture2D OnlineMapsTile::texture
	Texture2D_t3542995729 * ___texture_19;
	// UnityEngine.Vector2 OnlineMapsTile::topLeft
	Vector2_t2243707579  ___topLeft_20;
	// UnityEngine.Texture2D OnlineMapsTile::trafficTexture
	Texture2D_t3542995729 * ___trafficTexture_21;
	// System.String OnlineMapsTile::trafficURL
	String_t* ___trafficURL_22;
	// UnityEngine.WWW OnlineMapsTile::trafficWWW
	WWW_t2919945039 * ___trafficWWW_23;
	// System.Int32 OnlineMapsTile::type
	int32_t ___type_24;
	// System.Boolean OnlineMapsTile::used
	bool ___used_25;
	// UnityEngine.WWW OnlineMapsTile::www
	WWW_t2919945039 * ___www_26;
	// System.Int32 OnlineMapsTile::x
	int32_t ___x_27;
	// System.Int32 OnlineMapsTile::y
	int32_t ___y_28;
	// System.Int32 OnlineMapsTile::zoom
	int32_t ___zoom_29;
	// System.String OnlineMapsTile::_cacheFilename
	String_t* ____cacheFilename_30;
	// UnityEngine.Color[] OnlineMapsTile::_colors
	ColorU5BU5D_t672350442* ____colors_31;
	// System.String OnlineMapsTile::_url
	String_t* ____url_32;
	// OnlineMapsTile[] OnlineMapsTile::childs
	OnlineMapsTileU5BU5D_t2146591005* ___childs_33;
	// System.Boolean OnlineMapsTile::hasChilds
	bool ___hasChilds_34;
	// System.Byte[] OnlineMapsTile::labelData
	ByteU5BU5D_t3397334013* ___labelData_35;
	// UnityEngine.Color[] OnlineMapsTile::labelColors
	ColorU5BU5D_t672350442* ___labelColors_36;

public:
	inline static int32_t get_offset_of_OnSetColor_5() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___OnSetColor_5)); }
	inline Action_1_t4118096618 * get_OnSetColor_5() const { return ___OnSetColor_5; }
	inline Action_1_t4118096618 ** get_address_of_OnSetColor_5() { return &___OnSetColor_5; }
	inline void set_OnSetColor_5(Action_1_t4118096618 * value)
	{
		___OnSetColor_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnSetColor_5, value);
	}

	inline static int32_t get_offset_of_bottomRight_7() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___bottomRight_7)); }
	inline Vector2_t2243707579  get_bottomRight_7() const { return ___bottomRight_7; }
	inline Vector2_t2243707579 * get_address_of_bottomRight_7() { return &___bottomRight_7; }
	inline void set_bottomRight_7(Vector2_t2243707579  value)
	{
		___bottomRight_7 = value;
	}

	inline static int32_t get_offset_of_customData_8() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___customData_8)); }
	inline Il2CppObject * get_customData_8() const { return ___customData_8; }
	inline Il2CppObject ** get_address_of_customData_8() { return &___customData_8; }
	inline void set_customData_8(Il2CppObject * value)
	{
		___customData_8 = value;
		Il2CppCodeGenWriteBarrier(&___customData_8, value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___data_9)); }
	inline ByteU5BU5D_t3397334013* get_data_9() const { return ___data_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(ByteU5BU5D_t3397334013* value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier(&___data_9, value);
	}

	inline static int32_t get_offset_of_globalPosition_10() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___globalPosition_10)); }
	inline Vector2_t2243707579  get_globalPosition_10() const { return ___globalPosition_10; }
	inline Vector2_t2243707579 * get_address_of_globalPosition_10() { return &___globalPosition_10; }
	inline void set_globalPosition_10(Vector2_t2243707579  value)
	{
		___globalPosition_10 = value;
	}

	inline static int32_t get_offset_of_hasColors_11() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___hasColors_11)); }
	inline bool get_hasColors_11() const { return ___hasColors_11; }
	inline bool* get_address_of_hasColors_11() { return &___hasColors_11; }
	inline void set_hasColors_11(bool value)
	{
		___hasColors_11 = value;
	}

	inline static int32_t get_offset_of_isMapTile_12() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___isMapTile_12)); }
	inline bool get_isMapTile_12() const { return ___isMapTile_12; }
	inline bool* get_address_of_isMapTile_12() { return &___isMapTile_12; }
	inline void set_isMapTile_12(bool value)
	{
		___isMapTile_12 = value;
	}

	inline static int32_t get_offset_of_labels_13() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___labels_13)); }
	inline bool get_labels_13() const { return ___labels_13; }
	inline bool* get_address_of_labels_13() { return &___labels_13; }
	inline void set_labels_13(bool value)
	{
		___labels_13 = value;
	}

	inline static int32_t get_offset_of_language_14() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___language_14)); }
	inline String_t* get_language_14() const { return ___language_14; }
	inline String_t** get_address_of_language_14() { return &___language_14; }
	inline void set_language_14(String_t* value)
	{
		___language_14 = value;
		Il2CppCodeGenWriteBarrier(&___language_14, value);
	}

	inline static int32_t get_offset_of_parent_15() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___parent_15)); }
	inline OnlineMapsTile_t21329940 * get_parent_15() const { return ___parent_15; }
	inline OnlineMapsTile_t21329940 ** get_address_of_parent_15() { return &___parent_15; }
	inline void set_parent_15(OnlineMapsTile_t21329940 * value)
	{
		___parent_15 = value;
		Il2CppCodeGenWriteBarrier(&___parent_15, value);
	}

	inline static int32_t get_offset_of_priority_16() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___priority_16)); }
	inline int16_t get_priority_16() const { return ___priority_16; }
	inline int16_t* get_address_of_priority_16() { return &___priority_16; }
	inline void set_priority_16(int16_t value)
	{
		___priority_16 = value;
	}

	inline static int32_t get_offset_of_provider_17() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___provider_17)); }
	inline int32_t get_provider_17() const { return ___provider_17; }
	inline int32_t* get_address_of_provider_17() { return &___provider_17; }
	inline void set_provider_17(int32_t value)
	{
		___provider_17 = value;
	}

	inline static int32_t get_offset_of_status_18() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___status_18)); }
	inline int32_t get_status_18() const { return ___status_18; }
	inline int32_t* get_address_of_status_18() { return &___status_18; }
	inline void set_status_18(int32_t value)
	{
		___status_18 = value;
	}

	inline static int32_t get_offset_of_texture_19() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___texture_19)); }
	inline Texture2D_t3542995729 * get_texture_19() const { return ___texture_19; }
	inline Texture2D_t3542995729 ** get_address_of_texture_19() { return &___texture_19; }
	inline void set_texture_19(Texture2D_t3542995729 * value)
	{
		___texture_19 = value;
		Il2CppCodeGenWriteBarrier(&___texture_19, value);
	}

	inline static int32_t get_offset_of_topLeft_20() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___topLeft_20)); }
	inline Vector2_t2243707579  get_topLeft_20() const { return ___topLeft_20; }
	inline Vector2_t2243707579 * get_address_of_topLeft_20() { return &___topLeft_20; }
	inline void set_topLeft_20(Vector2_t2243707579  value)
	{
		___topLeft_20 = value;
	}

	inline static int32_t get_offset_of_trafficTexture_21() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___trafficTexture_21)); }
	inline Texture2D_t3542995729 * get_trafficTexture_21() const { return ___trafficTexture_21; }
	inline Texture2D_t3542995729 ** get_address_of_trafficTexture_21() { return &___trafficTexture_21; }
	inline void set_trafficTexture_21(Texture2D_t3542995729 * value)
	{
		___trafficTexture_21 = value;
		Il2CppCodeGenWriteBarrier(&___trafficTexture_21, value);
	}

	inline static int32_t get_offset_of_trafficURL_22() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___trafficURL_22)); }
	inline String_t* get_trafficURL_22() const { return ___trafficURL_22; }
	inline String_t** get_address_of_trafficURL_22() { return &___trafficURL_22; }
	inline void set_trafficURL_22(String_t* value)
	{
		___trafficURL_22 = value;
		Il2CppCodeGenWriteBarrier(&___trafficURL_22, value);
	}

	inline static int32_t get_offset_of_trafficWWW_23() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___trafficWWW_23)); }
	inline WWW_t2919945039 * get_trafficWWW_23() const { return ___trafficWWW_23; }
	inline WWW_t2919945039 ** get_address_of_trafficWWW_23() { return &___trafficWWW_23; }
	inline void set_trafficWWW_23(WWW_t2919945039 * value)
	{
		___trafficWWW_23 = value;
		Il2CppCodeGenWriteBarrier(&___trafficWWW_23, value);
	}

	inline static int32_t get_offset_of_type_24() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___type_24)); }
	inline int32_t get_type_24() const { return ___type_24; }
	inline int32_t* get_address_of_type_24() { return &___type_24; }
	inline void set_type_24(int32_t value)
	{
		___type_24 = value;
	}

	inline static int32_t get_offset_of_used_25() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___used_25)); }
	inline bool get_used_25() const { return ___used_25; }
	inline bool* get_address_of_used_25() { return &___used_25; }
	inline void set_used_25(bool value)
	{
		___used_25 = value;
	}

	inline static int32_t get_offset_of_www_26() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___www_26)); }
	inline WWW_t2919945039 * get_www_26() const { return ___www_26; }
	inline WWW_t2919945039 ** get_address_of_www_26() { return &___www_26; }
	inline void set_www_26(WWW_t2919945039 * value)
	{
		___www_26 = value;
		Il2CppCodeGenWriteBarrier(&___www_26, value);
	}

	inline static int32_t get_offset_of_x_27() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___x_27)); }
	inline int32_t get_x_27() const { return ___x_27; }
	inline int32_t* get_address_of_x_27() { return &___x_27; }
	inline void set_x_27(int32_t value)
	{
		___x_27 = value;
	}

	inline static int32_t get_offset_of_y_28() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___y_28)); }
	inline int32_t get_y_28() const { return ___y_28; }
	inline int32_t* get_address_of_y_28() { return &___y_28; }
	inline void set_y_28(int32_t value)
	{
		___y_28 = value;
	}

	inline static int32_t get_offset_of_zoom_29() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___zoom_29)); }
	inline int32_t get_zoom_29() const { return ___zoom_29; }
	inline int32_t* get_address_of_zoom_29() { return &___zoom_29; }
	inline void set_zoom_29(int32_t value)
	{
		___zoom_29 = value;
	}

	inline static int32_t get_offset_of__cacheFilename_30() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ____cacheFilename_30)); }
	inline String_t* get__cacheFilename_30() const { return ____cacheFilename_30; }
	inline String_t** get_address_of__cacheFilename_30() { return &____cacheFilename_30; }
	inline void set__cacheFilename_30(String_t* value)
	{
		____cacheFilename_30 = value;
		Il2CppCodeGenWriteBarrier(&____cacheFilename_30, value);
	}

	inline static int32_t get_offset_of__colors_31() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ____colors_31)); }
	inline ColorU5BU5D_t672350442* get__colors_31() const { return ____colors_31; }
	inline ColorU5BU5D_t672350442** get_address_of__colors_31() { return &____colors_31; }
	inline void set__colors_31(ColorU5BU5D_t672350442* value)
	{
		____colors_31 = value;
		Il2CppCodeGenWriteBarrier(&____colors_31, value);
	}

	inline static int32_t get_offset_of__url_32() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ____url_32)); }
	inline String_t* get__url_32() const { return ____url_32; }
	inline String_t** get_address_of__url_32() { return &____url_32; }
	inline void set__url_32(String_t* value)
	{
		____url_32 = value;
		Il2CppCodeGenWriteBarrier(&____url_32, value);
	}

	inline static int32_t get_offset_of_childs_33() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___childs_33)); }
	inline OnlineMapsTileU5BU5D_t2146591005* get_childs_33() const { return ___childs_33; }
	inline OnlineMapsTileU5BU5D_t2146591005** get_address_of_childs_33() { return &___childs_33; }
	inline void set_childs_33(OnlineMapsTileU5BU5D_t2146591005* value)
	{
		___childs_33 = value;
		Il2CppCodeGenWriteBarrier(&___childs_33, value);
	}

	inline static int32_t get_offset_of_hasChilds_34() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___hasChilds_34)); }
	inline bool get_hasChilds_34() const { return ___hasChilds_34; }
	inline bool* get_address_of_hasChilds_34() { return &___hasChilds_34; }
	inline void set_hasChilds_34(bool value)
	{
		___hasChilds_34 = value;
	}

	inline static int32_t get_offset_of_labelData_35() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___labelData_35)); }
	inline ByteU5BU5D_t3397334013* get_labelData_35() const { return ___labelData_35; }
	inline ByteU5BU5D_t3397334013** get_address_of_labelData_35() { return &___labelData_35; }
	inline void set_labelData_35(ByteU5BU5D_t3397334013* value)
	{
		___labelData_35 = value;
		Il2CppCodeGenWriteBarrier(&___labelData_35, value);
	}

	inline static int32_t get_offset_of_labelColors_36() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940, ___labelColors_36)); }
	inline ColorU5BU5D_t672350442* get_labelColors_36() const { return ___labelColors_36; }
	inline ColorU5BU5D_t672350442** get_address_of_labelColors_36() { return &___labelColors_36; }
	inline void set_labelColors_36(ColorU5BU5D_t672350442* value)
	{
		___labelColors_36 = value;
		Il2CppCodeGenWriteBarrier(&___labelColors_36, value);
	}
};

struct OnlineMapsTile_t21329940_StaticFields
{
public:
	// UnityEngine.Color[] OnlineMapsTile::defaultColors
	ColorU5BU5D_t672350442* ___defaultColors_0;
	// OnlineMapsTile/OnGetResourcesPathDelegate OnlineMapsTile::OnGetResourcesPath
	OnGetResourcesPathDelegate_t2869117133 * ___OnGetResourcesPath_1;
	// System.Action`1<OnlineMapsTile> OnlineMapsTile::OnTileDownloaded
	Action_1_t4118096618 * ___OnTileDownloaded_2;
	// System.Action`1<OnlineMapsTile> OnlineMapsTile::OnTrafficDownloaded
	Action_1_t4118096618 * ___OnTrafficDownloaded_3;
	// System.Collections.Generic.List`1<OnlineMapsTile> OnlineMapsTile::_tiles
	List_1_t3685418368 * ____tiles_4;
	// OnlineMaps OnlineMapsTile::api
	OnlineMaps_t1893290312 * ___api_6;
	// System.Func`2<OnlineMapsTile,System.Boolean> OnlineMapsTile::<>f__am$cache0
	Func_2_t2493586475 * ___U3CU3Ef__amU24cache0_37;

public:
	inline static int32_t get_offset_of_defaultColors_0() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ___defaultColors_0)); }
	inline ColorU5BU5D_t672350442* get_defaultColors_0() const { return ___defaultColors_0; }
	inline ColorU5BU5D_t672350442** get_address_of_defaultColors_0() { return &___defaultColors_0; }
	inline void set_defaultColors_0(ColorU5BU5D_t672350442* value)
	{
		___defaultColors_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultColors_0, value);
	}

	inline static int32_t get_offset_of_OnGetResourcesPath_1() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ___OnGetResourcesPath_1)); }
	inline OnGetResourcesPathDelegate_t2869117133 * get_OnGetResourcesPath_1() const { return ___OnGetResourcesPath_1; }
	inline OnGetResourcesPathDelegate_t2869117133 ** get_address_of_OnGetResourcesPath_1() { return &___OnGetResourcesPath_1; }
	inline void set_OnGetResourcesPath_1(OnGetResourcesPathDelegate_t2869117133 * value)
	{
		___OnGetResourcesPath_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetResourcesPath_1, value);
	}

	inline static int32_t get_offset_of_OnTileDownloaded_2() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ___OnTileDownloaded_2)); }
	inline Action_1_t4118096618 * get_OnTileDownloaded_2() const { return ___OnTileDownloaded_2; }
	inline Action_1_t4118096618 ** get_address_of_OnTileDownloaded_2() { return &___OnTileDownloaded_2; }
	inline void set_OnTileDownloaded_2(Action_1_t4118096618 * value)
	{
		___OnTileDownloaded_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnTileDownloaded_2, value);
	}

	inline static int32_t get_offset_of_OnTrafficDownloaded_3() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ___OnTrafficDownloaded_3)); }
	inline Action_1_t4118096618 * get_OnTrafficDownloaded_3() const { return ___OnTrafficDownloaded_3; }
	inline Action_1_t4118096618 ** get_address_of_OnTrafficDownloaded_3() { return &___OnTrafficDownloaded_3; }
	inline void set_OnTrafficDownloaded_3(Action_1_t4118096618 * value)
	{
		___OnTrafficDownloaded_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnTrafficDownloaded_3, value);
	}

	inline static int32_t get_offset_of__tiles_4() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ____tiles_4)); }
	inline List_1_t3685418368 * get__tiles_4() const { return ____tiles_4; }
	inline List_1_t3685418368 ** get_address_of__tiles_4() { return &____tiles_4; }
	inline void set__tiles_4(List_1_t3685418368 * value)
	{
		____tiles_4 = value;
		Il2CppCodeGenWriteBarrier(&____tiles_4, value);
	}

	inline static int32_t get_offset_of_api_6() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ___api_6)); }
	inline OnlineMaps_t1893290312 * get_api_6() const { return ___api_6; }
	inline OnlineMaps_t1893290312 ** get_address_of_api_6() { return &___api_6; }
	inline void set_api_6(OnlineMaps_t1893290312 * value)
	{
		___api_6 = value;
		Il2CppCodeGenWriteBarrier(&___api_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_37() { return static_cast<int32_t>(offsetof(OnlineMapsTile_t21329940_StaticFields, ___U3CU3Ef__amU24cache0_37)); }
	inline Func_2_t2493586475 * get_U3CU3Ef__amU24cache0_37() const { return ___U3CU3Ef__amU24cache0_37; }
	inline Func_2_t2493586475 ** get_address_of_U3CU3Ef__amU24cache0_37() { return &___U3CU3Ef__amU24cache0_37; }
	inline void set_U3CU3Ef__amU24cache0_37(Func_2_t2493586475 * value)
	{
		___U3CU3Ef__amU24cache0_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
