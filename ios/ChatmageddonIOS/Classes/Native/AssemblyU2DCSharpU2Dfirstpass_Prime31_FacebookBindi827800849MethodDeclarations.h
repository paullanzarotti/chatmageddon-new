﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookBinding/<showDialog>c__AnonStorey0
struct U3CshowDialogU3Ec__AnonStorey0_t827800849;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookBinding/<showDialog>c__AnonStorey0::.ctor()
extern "C"  void U3CshowDialogU3Ec__AnonStorey0__ctor_m1459942912 (U3CshowDialogU3Ec__AnonStorey0_t827800849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding/<showDialog>c__AnonStorey0::<>m__0()
extern "C"  void U3CshowDialogU3Ec__AnonStorey0_U3CU3Em__0_m1104999009 (U3CshowDialogU3Ec__AnonStorey0_t827800849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
