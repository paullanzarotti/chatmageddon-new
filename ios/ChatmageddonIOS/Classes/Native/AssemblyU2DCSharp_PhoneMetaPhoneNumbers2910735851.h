﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneMetaTerritory[]
struct PhoneMetaTerritoryU5BU5D_t1914005746;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneMetaPhoneNumbers
struct  PhoneMetaPhoneNumbers_t2910735851  : public Il2CppObject
{
public:
	// PhoneMetaTerritory[] PhoneMetaPhoneNumbers::territories
	PhoneMetaTerritoryU5BU5D_t1914005746* ___territories_0;

public:
	inline static int32_t get_offset_of_territories_0() { return static_cast<int32_t>(offsetof(PhoneMetaPhoneNumbers_t2910735851, ___territories_0)); }
	inline PhoneMetaTerritoryU5BU5D_t1914005746* get_territories_0() const { return ___territories_0; }
	inline PhoneMetaTerritoryU5BU5D_t1914005746** get_address_of_territories_0() { return &___territories_0; }
	inline void set_territories_0(PhoneMetaTerritoryU5BU5D_t1914005746* value)
	{
		___territories_0 = value;
		Il2CppCodeGenWriteBarrier(&___territories_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
