﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// TweenHeight
struct TweenHeight_t161374028;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttemptTab
struct  AttemptTab_t3964708364  : public MonoBehaviour_t1158329972
{
public:
	// UISprite AttemptTab::tabSprite
	UISprite_t603616735 * ___tabSprite_2;
	// TweenHeight AttemptTab::tabHeightTween
	TweenHeight_t161374028 * ___tabHeightTween_3;

public:
	inline static int32_t get_offset_of_tabSprite_2() { return static_cast<int32_t>(offsetof(AttemptTab_t3964708364, ___tabSprite_2)); }
	inline UISprite_t603616735 * get_tabSprite_2() const { return ___tabSprite_2; }
	inline UISprite_t603616735 ** get_address_of_tabSprite_2() { return &___tabSprite_2; }
	inline void set_tabSprite_2(UISprite_t603616735 * value)
	{
		___tabSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___tabSprite_2, value);
	}

	inline static int32_t get_offset_of_tabHeightTween_3() { return static_cast<int32_t>(offsetof(AttemptTab_t3964708364, ___tabHeightTween_3)); }
	inline TweenHeight_t161374028 * get_tabHeightTween_3() const { return ___tabHeightTween_3; }
	inline TweenHeight_t161374028 ** get_address_of_tabHeightTween_3() { return &___tabHeightTween_3; }
	inline void set_tabHeightTween_3(TweenHeight_t161374028 * value)
	{
		___tabHeightTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___tabHeightTween_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
