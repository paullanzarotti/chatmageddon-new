﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.P31RestKit/<send>c__Iterator0
struct U3CsendU3Ec__Iterator0_t1543352849;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.P31RestKit/<send>c__Iterator0::.ctor()
extern "C"  void U3CsendU3Ec__Iterator0__ctor_m663545858 (U3CsendU3Ec__Iterator0_t1543352849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.P31RestKit/<send>c__Iterator0::MoveNext()
extern "C"  bool U3CsendU3Ec__Iterator0_MoveNext_m2399067794 (U3CsendU3Ec__Iterator0_t1543352849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Prime31.P31RestKit/<send>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CsendU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3172069260 (U3CsendU3Ec__Iterator0_t1543352849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Prime31.P31RestKit/<send>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CsendU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1990010004 (U3CsendU3Ec__Iterator0_t1543352849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit/<send>c__Iterator0::Dispose()
extern "C"  void U3CsendU3Ec__Iterator0_Dispose_m1246460799 (U3CsendU3Ec__Iterator0_t1543352849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit/<send>c__Iterator0::Reset()
extern "C"  void U3CsendU3Ec__Iterator0_Reset_m3518355481 (U3CsendU3Ec__Iterator0_t1543352849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
