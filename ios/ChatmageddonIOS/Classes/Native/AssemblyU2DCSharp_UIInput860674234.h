﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t2865430313;
// UIInput/OnValidate
struct OnValidate_t2431313412;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t601950206;
// UITexture
struct UITexture_t2537039969;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UICamera
struct UICamera_t1496819779;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_UIInput_KeyboardMode2358216949.h"
#include "AssemblyU2DCSharp_UIInput_InputType1695544313.h"
#include "AssemblyU2DCSharp_UIInput_OnReturnKey1716013855.h"
#include "AssemblyU2DCSharp_UIInput_KeyboardType865779294.h"
#include "AssemblyU2DCSharp_UIInput_Validation3865409168.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot752586349.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInput
struct  UIInput_t860674234  : public MonoBehaviour_t1158329972
{
public:
	// UILabel UIInput::label
	UILabel_t1795115428 * ___label_4;
	// UIInput/KeyboardMode UIInput::keyboardMode
	int32_t ___keyboardMode_5;
	// UIInput/InputType UIInput::inputType
	int32_t ___inputType_6;
	// UIInput/OnReturnKey UIInput::onReturnKey
	int32_t ___onReturnKey_7;
	// UIInput/KeyboardType UIInput::keyboardType
	int32_t ___keyboardType_8;
	// System.Boolean UIInput::hideInput
	bool ___hideInput_9;
	// System.Boolean UIInput::selectAllTextOnFocus
	bool ___selectAllTextOnFocus_10;
	// UIInput/Validation UIInput::validation
	int32_t ___validation_11;
	// System.Int32 UIInput::characterLimit
	int32_t ___characterLimit_12;
	// System.String UIInput::savedAs
	String_t* ___savedAs_13;
	// UnityEngine.GameObject UIInput::selectOnTab
	GameObject_t1756533147 * ___selectOnTab_14;
	// UnityEngine.Color UIInput::activeTextColor
	Color_t2020392075  ___activeTextColor_15;
	// UnityEngine.Color UIInput::caretColor
	Color_t2020392075  ___caretColor_16;
	// UnityEngine.Color UIInput::selectionColor
	Color_t2020392075  ___selectionColor_17;
	// System.Collections.Generic.List`1<EventDelegate> UIInput::onSubmit
	List_1_t2865430313 * ___onSubmit_18;
	// System.Collections.Generic.List`1<EventDelegate> UIInput::onChange
	List_1_t2865430313 * ___onChange_19;
	// UIInput/OnValidate UIInput::onValidate
	OnValidate_t2431313412 * ___onValidate_20;
	// System.String UIInput::mValue
	String_t* ___mValue_21;
	// System.String UIInput::mDefaultText
	String_t* ___mDefaultText_22;
	// UnityEngine.Color UIInput::mDefaultColor
	Color_t2020392075  ___mDefaultColor_23;
	// System.Single UIInput::mPosition
	float ___mPosition_24;
	// System.Boolean UIInput::mDoInit
	bool ___mDoInit_25;
	// UIWidget/Pivot UIInput::mPivot
	int32_t ___mPivot_26;
	// System.Boolean UIInput::mLoadSavedValue
	bool ___mLoadSavedValue_27;
	// System.Int32 UIInput::mSelectionStart
	int32_t ___mSelectionStart_32;
	// System.Int32 UIInput::mSelectionEnd
	int32_t ___mSelectionEnd_33;
	// UITexture UIInput::mHighlight
	UITexture_t2537039969 * ___mHighlight_34;
	// UITexture UIInput::mCaret
	UITexture_t2537039969 * ___mCaret_35;
	// UnityEngine.Texture2D UIInput::mBlankTex
	Texture2D_t3542995729 * ___mBlankTex_36;
	// System.Single UIInput::mNextBlink
	float ___mNextBlink_37;
	// System.Single UIInput::mLastAlpha
	float ___mLastAlpha_38;
	// System.String UIInput::mCached
	String_t* ___mCached_39;
	// System.Int32 UIInput::mSelectMe
	int32_t ___mSelectMe_40;
	// System.Int32 UIInput::mSelectTime
	int32_t ___mSelectTime_41;
	// UICamera UIInput::mCam
	UICamera_t1496819779 * ___mCam_42;

public:
	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___label_4)); }
	inline UILabel_t1795115428 * get_label_4() const { return ___label_4; }
	inline UILabel_t1795115428 ** get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(UILabel_t1795115428 * value)
	{
		___label_4 = value;
		Il2CppCodeGenWriteBarrier(&___label_4, value);
	}

	inline static int32_t get_offset_of_keyboardMode_5() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___keyboardMode_5)); }
	inline int32_t get_keyboardMode_5() const { return ___keyboardMode_5; }
	inline int32_t* get_address_of_keyboardMode_5() { return &___keyboardMode_5; }
	inline void set_keyboardMode_5(int32_t value)
	{
		___keyboardMode_5 = value;
	}

	inline static int32_t get_offset_of_inputType_6() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___inputType_6)); }
	inline int32_t get_inputType_6() const { return ___inputType_6; }
	inline int32_t* get_address_of_inputType_6() { return &___inputType_6; }
	inline void set_inputType_6(int32_t value)
	{
		___inputType_6 = value;
	}

	inline static int32_t get_offset_of_onReturnKey_7() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___onReturnKey_7)); }
	inline int32_t get_onReturnKey_7() const { return ___onReturnKey_7; }
	inline int32_t* get_address_of_onReturnKey_7() { return &___onReturnKey_7; }
	inline void set_onReturnKey_7(int32_t value)
	{
		___onReturnKey_7 = value;
	}

	inline static int32_t get_offset_of_keyboardType_8() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___keyboardType_8)); }
	inline int32_t get_keyboardType_8() const { return ___keyboardType_8; }
	inline int32_t* get_address_of_keyboardType_8() { return &___keyboardType_8; }
	inline void set_keyboardType_8(int32_t value)
	{
		___keyboardType_8 = value;
	}

	inline static int32_t get_offset_of_hideInput_9() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___hideInput_9)); }
	inline bool get_hideInput_9() const { return ___hideInput_9; }
	inline bool* get_address_of_hideInput_9() { return &___hideInput_9; }
	inline void set_hideInput_9(bool value)
	{
		___hideInput_9 = value;
	}

	inline static int32_t get_offset_of_selectAllTextOnFocus_10() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___selectAllTextOnFocus_10)); }
	inline bool get_selectAllTextOnFocus_10() const { return ___selectAllTextOnFocus_10; }
	inline bool* get_address_of_selectAllTextOnFocus_10() { return &___selectAllTextOnFocus_10; }
	inline void set_selectAllTextOnFocus_10(bool value)
	{
		___selectAllTextOnFocus_10 = value;
	}

	inline static int32_t get_offset_of_validation_11() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___validation_11)); }
	inline int32_t get_validation_11() const { return ___validation_11; }
	inline int32_t* get_address_of_validation_11() { return &___validation_11; }
	inline void set_validation_11(int32_t value)
	{
		___validation_11 = value;
	}

	inline static int32_t get_offset_of_characterLimit_12() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___characterLimit_12)); }
	inline int32_t get_characterLimit_12() const { return ___characterLimit_12; }
	inline int32_t* get_address_of_characterLimit_12() { return &___characterLimit_12; }
	inline void set_characterLimit_12(int32_t value)
	{
		___characterLimit_12 = value;
	}

	inline static int32_t get_offset_of_savedAs_13() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___savedAs_13)); }
	inline String_t* get_savedAs_13() const { return ___savedAs_13; }
	inline String_t** get_address_of_savedAs_13() { return &___savedAs_13; }
	inline void set_savedAs_13(String_t* value)
	{
		___savedAs_13 = value;
		Il2CppCodeGenWriteBarrier(&___savedAs_13, value);
	}

	inline static int32_t get_offset_of_selectOnTab_14() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___selectOnTab_14)); }
	inline GameObject_t1756533147 * get_selectOnTab_14() const { return ___selectOnTab_14; }
	inline GameObject_t1756533147 ** get_address_of_selectOnTab_14() { return &___selectOnTab_14; }
	inline void set_selectOnTab_14(GameObject_t1756533147 * value)
	{
		___selectOnTab_14 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnTab_14, value);
	}

	inline static int32_t get_offset_of_activeTextColor_15() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___activeTextColor_15)); }
	inline Color_t2020392075  get_activeTextColor_15() const { return ___activeTextColor_15; }
	inline Color_t2020392075 * get_address_of_activeTextColor_15() { return &___activeTextColor_15; }
	inline void set_activeTextColor_15(Color_t2020392075  value)
	{
		___activeTextColor_15 = value;
	}

	inline static int32_t get_offset_of_caretColor_16() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___caretColor_16)); }
	inline Color_t2020392075  get_caretColor_16() const { return ___caretColor_16; }
	inline Color_t2020392075 * get_address_of_caretColor_16() { return &___caretColor_16; }
	inline void set_caretColor_16(Color_t2020392075  value)
	{
		___caretColor_16 = value;
	}

	inline static int32_t get_offset_of_selectionColor_17() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___selectionColor_17)); }
	inline Color_t2020392075  get_selectionColor_17() const { return ___selectionColor_17; }
	inline Color_t2020392075 * get_address_of_selectionColor_17() { return &___selectionColor_17; }
	inline void set_selectionColor_17(Color_t2020392075  value)
	{
		___selectionColor_17 = value;
	}

	inline static int32_t get_offset_of_onSubmit_18() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___onSubmit_18)); }
	inline List_1_t2865430313 * get_onSubmit_18() const { return ___onSubmit_18; }
	inline List_1_t2865430313 ** get_address_of_onSubmit_18() { return &___onSubmit_18; }
	inline void set_onSubmit_18(List_1_t2865430313 * value)
	{
		___onSubmit_18 = value;
		Il2CppCodeGenWriteBarrier(&___onSubmit_18, value);
	}

	inline static int32_t get_offset_of_onChange_19() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___onChange_19)); }
	inline List_1_t2865430313 * get_onChange_19() const { return ___onChange_19; }
	inline List_1_t2865430313 ** get_address_of_onChange_19() { return &___onChange_19; }
	inline void set_onChange_19(List_1_t2865430313 * value)
	{
		___onChange_19 = value;
		Il2CppCodeGenWriteBarrier(&___onChange_19, value);
	}

	inline static int32_t get_offset_of_onValidate_20() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___onValidate_20)); }
	inline OnValidate_t2431313412 * get_onValidate_20() const { return ___onValidate_20; }
	inline OnValidate_t2431313412 ** get_address_of_onValidate_20() { return &___onValidate_20; }
	inline void set_onValidate_20(OnValidate_t2431313412 * value)
	{
		___onValidate_20 = value;
		Il2CppCodeGenWriteBarrier(&___onValidate_20, value);
	}

	inline static int32_t get_offset_of_mValue_21() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mValue_21)); }
	inline String_t* get_mValue_21() const { return ___mValue_21; }
	inline String_t** get_address_of_mValue_21() { return &___mValue_21; }
	inline void set_mValue_21(String_t* value)
	{
		___mValue_21 = value;
		Il2CppCodeGenWriteBarrier(&___mValue_21, value);
	}

	inline static int32_t get_offset_of_mDefaultText_22() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mDefaultText_22)); }
	inline String_t* get_mDefaultText_22() const { return ___mDefaultText_22; }
	inline String_t** get_address_of_mDefaultText_22() { return &___mDefaultText_22; }
	inline void set_mDefaultText_22(String_t* value)
	{
		___mDefaultText_22 = value;
		Il2CppCodeGenWriteBarrier(&___mDefaultText_22, value);
	}

	inline static int32_t get_offset_of_mDefaultColor_23() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mDefaultColor_23)); }
	inline Color_t2020392075  get_mDefaultColor_23() const { return ___mDefaultColor_23; }
	inline Color_t2020392075 * get_address_of_mDefaultColor_23() { return &___mDefaultColor_23; }
	inline void set_mDefaultColor_23(Color_t2020392075  value)
	{
		___mDefaultColor_23 = value;
	}

	inline static int32_t get_offset_of_mPosition_24() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mPosition_24)); }
	inline float get_mPosition_24() const { return ___mPosition_24; }
	inline float* get_address_of_mPosition_24() { return &___mPosition_24; }
	inline void set_mPosition_24(float value)
	{
		___mPosition_24 = value;
	}

	inline static int32_t get_offset_of_mDoInit_25() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mDoInit_25)); }
	inline bool get_mDoInit_25() const { return ___mDoInit_25; }
	inline bool* get_address_of_mDoInit_25() { return &___mDoInit_25; }
	inline void set_mDoInit_25(bool value)
	{
		___mDoInit_25 = value;
	}

	inline static int32_t get_offset_of_mPivot_26() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mPivot_26)); }
	inline int32_t get_mPivot_26() const { return ___mPivot_26; }
	inline int32_t* get_address_of_mPivot_26() { return &___mPivot_26; }
	inline void set_mPivot_26(int32_t value)
	{
		___mPivot_26 = value;
	}

	inline static int32_t get_offset_of_mLoadSavedValue_27() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mLoadSavedValue_27)); }
	inline bool get_mLoadSavedValue_27() const { return ___mLoadSavedValue_27; }
	inline bool* get_address_of_mLoadSavedValue_27() { return &___mLoadSavedValue_27; }
	inline void set_mLoadSavedValue_27(bool value)
	{
		___mLoadSavedValue_27 = value;
	}

	inline static int32_t get_offset_of_mSelectionStart_32() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mSelectionStart_32)); }
	inline int32_t get_mSelectionStart_32() const { return ___mSelectionStart_32; }
	inline int32_t* get_address_of_mSelectionStart_32() { return &___mSelectionStart_32; }
	inline void set_mSelectionStart_32(int32_t value)
	{
		___mSelectionStart_32 = value;
	}

	inline static int32_t get_offset_of_mSelectionEnd_33() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mSelectionEnd_33)); }
	inline int32_t get_mSelectionEnd_33() const { return ___mSelectionEnd_33; }
	inline int32_t* get_address_of_mSelectionEnd_33() { return &___mSelectionEnd_33; }
	inline void set_mSelectionEnd_33(int32_t value)
	{
		___mSelectionEnd_33 = value;
	}

	inline static int32_t get_offset_of_mHighlight_34() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mHighlight_34)); }
	inline UITexture_t2537039969 * get_mHighlight_34() const { return ___mHighlight_34; }
	inline UITexture_t2537039969 ** get_address_of_mHighlight_34() { return &___mHighlight_34; }
	inline void set_mHighlight_34(UITexture_t2537039969 * value)
	{
		___mHighlight_34 = value;
		Il2CppCodeGenWriteBarrier(&___mHighlight_34, value);
	}

	inline static int32_t get_offset_of_mCaret_35() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mCaret_35)); }
	inline UITexture_t2537039969 * get_mCaret_35() const { return ___mCaret_35; }
	inline UITexture_t2537039969 ** get_address_of_mCaret_35() { return &___mCaret_35; }
	inline void set_mCaret_35(UITexture_t2537039969 * value)
	{
		___mCaret_35 = value;
		Il2CppCodeGenWriteBarrier(&___mCaret_35, value);
	}

	inline static int32_t get_offset_of_mBlankTex_36() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mBlankTex_36)); }
	inline Texture2D_t3542995729 * get_mBlankTex_36() const { return ___mBlankTex_36; }
	inline Texture2D_t3542995729 ** get_address_of_mBlankTex_36() { return &___mBlankTex_36; }
	inline void set_mBlankTex_36(Texture2D_t3542995729 * value)
	{
		___mBlankTex_36 = value;
		Il2CppCodeGenWriteBarrier(&___mBlankTex_36, value);
	}

	inline static int32_t get_offset_of_mNextBlink_37() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mNextBlink_37)); }
	inline float get_mNextBlink_37() const { return ___mNextBlink_37; }
	inline float* get_address_of_mNextBlink_37() { return &___mNextBlink_37; }
	inline void set_mNextBlink_37(float value)
	{
		___mNextBlink_37 = value;
	}

	inline static int32_t get_offset_of_mLastAlpha_38() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mLastAlpha_38)); }
	inline float get_mLastAlpha_38() const { return ___mLastAlpha_38; }
	inline float* get_address_of_mLastAlpha_38() { return &___mLastAlpha_38; }
	inline void set_mLastAlpha_38(float value)
	{
		___mLastAlpha_38 = value;
	}

	inline static int32_t get_offset_of_mCached_39() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mCached_39)); }
	inline String_t* get_mCached_39() const { return ___mCached_39; }
	inline String_t** get_address_of_mCached_39() { return &___mCached_39; }
	inline void set_mCached_39(String_t* value)
	{
		___mCached_39 = value;
		Il2CppCodeGenWriteBarrier(&___mCached_39, value);
	}

	inline static int32_t get_offset_of_mSelectMe_40() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mSelectMe_40)); }
	inline int32_t get_mSelectMe_40() const { return ___mSelectMe_40; }
	inline int32_t* get_address_of_mSelectMe_40() { return &___mSelectMe_40; }
	inline void set_mSelectMe_40(int32_t value)
	{
		___mSelectMe_40 = value;
	}

	inline static int32_t get_offset_of_mSelectTime_41() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mSelectTime_41)); }
	inline int32_t get_mSelectTime_41() const { return ___mSelectTime_41; }
	inline int32_t* get_address_of_mSelectTime_41() { return &___mSelectTime_41; }
	inline void set_mSelectTime_41(int32_t value)
	{
		___mSelectTime_41 = value;
	}

	inline static int32_t get_offset_of_mCam_42() { return static_cast<int32_t>(offsetof(UIInput_t860674234, ___mCam_42)); }
	inline UICamera_t1496819779 * get_mCam_42() const { return ___mCam_42; }
	inline UICamera_t1496819779 ** get_address_of_mCam_42() { return &___mCam_42; }
	inline void set_mCam_42(UICamera_t1496819779 * value)
	{
		___mCam_42 = value;
		Il2CppCodeGenWriteBarrier(&___mCam_42, value);
	}
};

struct UIInput_t860674234_StaticFields
{
public:
	// UIInput UIInput::current
	UIInput_t860674234 * ___current_2;
	// UIInput UIInput::selection
	UIInput_t860674234 * ___selection_3;
	// System.Int32 UIInput::mDrawStart
	int32_t ___mDrawStart_28;
	// System.String UIInput::mLastIME
	String_t* ___mLastIME_29;
	// UnityEngine.TouchScreenKeyboard UIInput::mKeyboard
	TouchScreenKeyboard_t601950206 * ___mKeyboard_30;
	// System.Boolean UIInput::mWaitForKeyboard
	bool ___mWaitForKeyboard_31;
	// System.Int32 UIInput::mIgnoreKey
	int32_t ___mIgnoreKey_43;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___current_2)); }
	inline UIInput_t860674234 * get_current_2() const { return ___current_2; }
	inline UIInput_t860674234 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIInput_t860674234 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}

	inline static int32_t get_offset_of_selection_3() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___selection_3)); }
	inline UIInput_t860674234 * get_selection_3() const { return ___selection_3; }
	inline UIInput_t860674234 ** get_address_of_selection_3() { return &___selection_3; }
	inline void set_selection_3(UIInput_t860674234 * value)
	{
		___selection_3 = value;
		Il2CppCodeGenWriteBarrier(&___selection_3, value);
	}

	inline static int32_t get_offset_of_mDrawStart_28() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___mDrawStart_28)); }
	inline int32_t get_mDrawStart_28() const { return ___mDrawStart_28; }
	inline int32_t* get_address_of_mDrawStart_28() { return &___mDrawStart_28; }
	inline void set_mDrawStart_28(int32_t value)
	{
		___mDrawStart_28 = value;
	}

	inline static int32_t get_offset_of_mLastIME_29() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___mLastIME_29)); }
	inline String_t* get_mLastIME_29() const { return ___mLastIME_29; }
	inline String_t** get_address_of_mLastIME_29() { return &___mLastIME_29; }
	inline void set_mLastIME_29(String_t* value)
	{
		___mLastIME_29 = value;
		Il2CppCodeGenWriteBarrier(&___mLastIME_29, value);
	}

	inline static int32_t get_offset_of_mKeyboard_30() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___mKeyboard_30)); }
	inline TouchScreenKeyboard_t601950206 * get_mKeyboard_30() const { return ___mKeyboard_30; }
	inline TouchScreenKeyboard_t601950206 ** get_address_of_mKeyboard_30() { return &___mKeyboard_30; }
	inline void set_mKeyboard_30(TouchScreenKeyboard_t601950206 * value)
	{
		___mKeyboard_30 = value;
		Il2CppCodeGenWriteBarrier(&___mKeyboard_30, value);
	}

	inline static int32_t get_offset_of_mWaitForKeyboard_31() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___mWaitForKeyboard_31)); }
	inline bool get_mWaitForKeyboard_31() const { return ___mWaitForKeyboard_31; }
	inline bool* get_address_of_mWaitForKeyboard_31() { return &___mWaitForKeyboard_31; }
	inline void set_mWaitForKeyboard_31(bool value)
	{
		___mWaitForKeyboard_31 = value;
	}

	inline static int32_t get_offset_of_mIgnoreKey_43() { return static_cast<int32_t>(offsetof(UIInput_t860674234_StaticFields, ___mIgnoreKey_43)); }
	inline int32_t get_mIgnoreKey_43() const { return ___mIgnoreKey_43; }
	inline int32_t* get_address_of_mIgnoreKey_43() { return &___mIgnoreKey_43; }
	inline void set_mIgnoreKey_43(int32_t value)
	{
		___mIgnoreKey_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
