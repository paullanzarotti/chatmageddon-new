﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.Locale
struct Locale_t2376277312;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.Locale::.ctor(System.String,System.String)
extern "C"  void Locale__ctor_m1055616741 (Locale_t2376277312 * __this, String_t* ___language0, String_t* ___countryCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.Locale::GetDisplayCountry(System.String)
extern "C"  String_t* Locale_GetDisplayCountry_m764534178 (Locale_t2376277312 * __this, String_t* ___language0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.Locale::GetCountryName(System.String,System.String)
extern "C"  String_t* Locale_GetCountryName_m104171805 (Locale_t2376277312 * __this, String_t* ___country0, String_t* ___language1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.Locale::.cctor()
extern "C"  void Locale__cctor_m908631964 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
