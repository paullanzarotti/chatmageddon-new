﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "P31RestKit_Prime31_MonoBehaviourGUI1135617291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.TwitterGUIManager
struct  TwitterGUIManager_t3680940761  : public MonoBehaviourGUI_t1135617291
{
public:
	// System.Boolean Prime31.TwitterGUIManager::_canUseTweetSheet
	bool ____canUseTweetSheet_18;

public:
	inline static int32_t get_offset_of__canUseTweetSheet_18() { return static_cast<int32_t>(offsetof(TwitterGUIManager_t3680940761, ____canUseTweetSheet_18)); }
	inline bool get__canUseTweetSheet_18() const { return ____canUseTweetSheet_18; }
	inline bool* get_address_of__canUseTweetSheet_18() { return &____canUseTweetSheet_18; }
	inline void set__canUseTweetSheet_18(bool value)
	{
		____canUseTweetSheet_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
