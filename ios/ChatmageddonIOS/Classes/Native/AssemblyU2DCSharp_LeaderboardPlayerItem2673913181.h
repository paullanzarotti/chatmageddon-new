﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// User
struct User_t719925459;
// UILabel
struct UILabel_t1795115428;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardPlayerItem
struct  LeaderboardPlayerItem_t2673913181  : public MonoBehaviour_t1158329972
{
public:
	// User LeaderboardPlayerItem::user
	User_t719925459 * ___user_2;
	// System.Int32 LeaderboardPlayerItem::maxNameLength
	int32_t ___maxNameLength_3;
	// UILabel LeaderboardPlayerItem::rankLabel
	UILabel_t1795115428 * ___rankLabel_4;
	// UILabel LeaderboardPlayerItem::nameLabel
	UILabel_t1795115428 * ___nameLabel_5;
	// UILabel LeaderboardPlayerItem::userRankLabel
	UILabel_t1795115428 * ___userRankLabel_6;
	// UILabel LeaderboardPlayerItem::scoreLabel
	UILabel_t1795115428 * ___scoreLabel_7;
	// TweenAlpha LeaderboardPlayerItem::panelTween
	TweenAlpha_t2421518635 * ___panelTween_8;
	// System.Boolean LeaderboardPlayerItem::hidingPanel
	bool ___hidingPanel_9;

public:
	inline static int32_t get_offset_of_user_2() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___user_2)); }
	inline User_t719925459 * get_user_2() const { return ___user_2; }
	inline User_t719925459 ** get_address_of_user_2() { return &___user_2; }
	inline void set_user_2(User_t719925459 * value)
	{
		___user_2 = value;
		Il2CppCodeGenWriteBarrier(&___user_2, value);
	}

	inline static int32_t get_offset_of_maxNameLength_3() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___maxNameLength_3)); }
	inline int32_t get_maxNameLength_3() const { return ___maxNameLength_3; }
	inline int32_t* get_address_of_maxNameLength_3() { return &___maxNameLength_3; }
	inline void set_maxNameLength_3(int32_t value)
	{
		___maxNameLength_3 = value;
	}

	inline static int32_t get_offset_of_rankLabel_4() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___rankLabel_4)); }
	inline UILabel_t1795115428 * get_rankLabel_4() const { return ___rankLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_4() { return &___rankLabel_4; }
	inline void set_rankLabel_4(UILabel_t1795115428 * value)
	{
		___rankLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_4, value);
	}

	inline static int32_t get_offset_of_nameLabel_5() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___nameLabel_5)); }
	inline UILabel_t1795115428 * get_nameLabel_5() const { return ___nameLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_5() { return &___nameLabel_5; }
	inline void set_nameLabel_5(UILabel_t1795115428 * value)
	{
		___nameLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_5, value);
	}

	inline static int32_t get_offset_of_userRankLabel_6() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___userRankLabel_6)); }
	inline UILabel_t1795115428 * get_userRankLabel_6() const { return ___userRankLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_userRankLabel_6() { return &___userRankLabel_6; }
	inline void set_userRankLabel_6(UILabel_t1795115428 * value)
	{
		___userRankLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___userRankLabel_6, value);
	}

	inline static int32_t get_offset_of_scoreLabel_7() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___scoreLabel_7)); }
	inline UILabel_t1795115428 * get_scoreLabel_7() const { return ___scoreLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_scoreLabel_7() { return &___scoreLabel_7; }
	inline void set_scoreLabel_7(UILabel_t1795115428 * value)
	{
		___scoreLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___scoreLabel_7, value);
	}

	inline static int32_t get_offset_of_panelTween_8() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___panelTween_8)); }
	inline TweenAlpha_t2421518635 * get_panelTween_8() const { return ___panelTween_8; }
	inline TweenAlpha_t2421518635 ** get_address_of_panelTween_8() { return &___panelTween_8; }
	inline void set_panelTween_8(TweenAlpha_t2421518635 * value)
	{
		___panelTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___panelTween_8, value);
	}

	inline static int32_t get_offset_of_hidingPanel_9() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t2673913181, ___hidingPanel_9)); }
	inline bool get_hidingPanel_9() const { return ___hidingPanel_9; }
	inline bool* get_address_of_hidingPanel_9() { return &___hidingPanel_9; }
	inline void set_hidingPanel_9(bool value)
	{
		___hidingPanel_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
