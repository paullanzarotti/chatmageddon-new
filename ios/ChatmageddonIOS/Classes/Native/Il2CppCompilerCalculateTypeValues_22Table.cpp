﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_ServicePointManager745663000.h"
#include "System_System_Net_ServicePointManager_SPKey1552752485.h"
#include "System_System_Net_ServicePointManager_ChainValidat1155887809.h"
#include "System_System_Net_SocketAddress838303055.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_IPv6MulticastOption1621622330.h"
#include "System_System_Net_Sockets_LingerOption1165263720.h"
#include "System_System_Net_Sockets_MulticastOption2505469155.h"
#include "System_System_Net_Sockets_NetworkStream581172200.h"
#include "System_System_Net_Sockets_ProtocolType2178963134.h"
#include "System_System_Net_Sockets_SelectMode3413969319.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_Sockets_Socket_SocketOperation3328960782.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult2959281146.h"
#include "System_System_Net_Sockets_Socket_Worker1317165246.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall3737776727.h"
#include "System_System_Net_Sockets_SocketError307542793.h"
#include "System_System_Net_Sockets_SocketException1618573604.h"
#include "System_System_Net_Sockets_SocketFlags2353657790.h"
#include "System_System_Net_Sockets_SocketOptionLevel1505247880.h"
#include "System_System_Net_Sockets_SocketOptionName1089121285.h"
#include "System_System_Net_Sockets_SocketShutdown3247039417.h"
#include "System_System_Net_Sockets_SocketType1143498533.h"
#include "System_System_Net_WebAsyncResult905414499.h"
#include "System_System_Net_ReadState657568301.h"
#include "System_System_Net_WebConnection324679648.h"
#include "System_System_Net_WebConnection_AbortHelper2895113041.h"
#include "System_System_Net_WebConnectionData3550260432.h"
#include "System_System_Net_WebConnectionGroup3242458773.h"
#include "System_System_Net_WebConnectionStream1922483508.h"
#include "System_System_Net_WebException3368933679.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "System_System_Net_WebHeaderCollection3028142837.h"
#include "System_System_Net_WebProxy1169192840.h"
#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Net_WebResponse1895226051.h"
#include "System_System_Security_Authentication_SslProtocols894678499.h"
#include "System_System_Security_Cryptography_AsnDecodeStatu1962003286.h"
#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"
#include "System_System_Security_Cryptography_OidCollection3790243618.h"
#include "System_System_Security_Cryptography_Oid3221867120.h"
#include "System_System_Security_Cryptography_OidEnumerator3674631724.h"
#include "System_System_Security_Cryptography_X509Certificat2370524385.h"
#include "System_Mono_Security_X509_OSX509Certificates3584809896.h"
#include "System_Mono_Security_X509_OSX509Certificates_SecTr1984565408.h"
#include "System_System_Security_Cryptography_X509Certificates_P870392.h"
#include "System_System_Security_Cryptography_X509Certificat1570828128.h"
#include "System_System_Security_Cryptography_X509Certificat2183514610.h"
#include "System_System_Security_Cryptography_X509Certificate452415348.h"
#include "System_System_Security_Cryptography_X509Certificat2005802885.h"
#include "System_System_Security_Cryptography_X509Certificat1562873317.h"
#include "System_System_Security_Cryptography_X509Certificat1108969367.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "System_System_Security_Cryptography_X509Certificat2356134957.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "System_System_Security_Cryptography_X509Certificat1208230922.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Security_Cryptography_X509Certificat2081831987.h"
#include "System_System_Security_Cryptography_X509Certificate528874471.h"
#include "System_System_Security_Cryptography_X509Certificat3304975821.h"
#include "System_System_Security_Cryptography_X509Certificat3452126517.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "System_System_Security_Cryptography_X509Certificate480677120.h"
#include "System_System_Security_Cryptography_X509Certificat2099881051.h"
#include "System_System_Security_Cryptography_X509Certificate650873211.h"
#include "System_System_Security_Cryptography_X509Certificat1320896183.h"
#include "System_System_Security_Cryptography_X509Certificat3763443773.h"
#include "System_System_Security_Cryptography_X509Certificat3221716179.h"
#include "System_System_Security_Cryptography_X509Certificat1038124237.h"
#include "System_System_Security_Cryptography_X509Certificat2461349531.h"
#include "System_System_Security_Cryptography_X509Certificat2669466891.h"
#include "System_System_Security_Cryptography_X509Certificat2166064554.h"
#include "System_System_Security_Cryptography_X509Certificat2065307963.h"
#include "System_System_Security_Cryptography_X509Certificat1617430119.h"
#include "System_System_Security_Cryptography_X509Certificat2508879999.h"
#include "System_System_Security_Cryptography_X509Certificate110301003.h"
#include "System_System_Security_Cryptography_X509Certificat2169036324.h"
#include "System_System_Text_RegularExpressions_OpCode586571952.h"
#include "System_System_Text_RegularExpressions_OpFlags378191910.h"
#include "System_System_Text_RegularExpressions_Position3781184359.h"
#include "System_System_Text_RegularExpressions_BaseMachine4008011478.h"
#include "System_System_Text_RegularExpressions_BaseMachine_1618777330.h"
#include "System_System_Text_RegularExpressions_FactoryCache2051534610.h"
#include "System_System_Text_RegularExpressions_FactoryCache_655155419.h"
#include "System_System_Text_RegularExpressions_MRUList33178162.h"
#include "System_System_Text_RegularExpressions_MRUList_Node1107172180.h"
#include "System_System_Text_RegularExpressions_CaptureColle1671345504.h"
#include "System_System_Text_RegularExpressions_Capture4157900610.h"
#include "System_System_Text_RegularExpressions_CILCompiler1740644799.h"
#include "System_System_Text_RegularExpressions_CILCompiler_F997927490.h"
#include "System_System_Text_RegularExpressions_Category1984577050.h"
#include "System_System_Text_RegularExpressions_CategoryUtil3840220623.h"
#include "System_System_Text_RegularExpressions_LinkRef2090853131.h"
#include "System_System_Text_RegularExpressions_InterpreterFa556462562.h"
#include "System_System_Text_RegularExpressions_PatternCompil637049905.h"
#include "System_System_Text_RegularExpressions_PatternCompi3979537293.h"
#include "System_System_Text_RegularExpressions_PatternCompi3337276394.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ServicePointManager_t745663000), -1, sizeof(ServicePointManager_t745663000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[11] = 
{
	ServicePointManager_t745663000_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_server_cert_cb_9(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_manager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (SPKey_t1552752485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[2] = 
{
	SPKey_t1552752485::get_offset_of_uri_0(),
	SPKey_t1552752485::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (ChainValidationHelper_t1155887809), -1, sizeof(ChainValidationHelper_t1155887809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	ChainValidationHelper_t1155887809::get_offset_of_sender_0(),
	ChainValidationHelper_t1155887809::get_offset_of_host_1(),
	ChainValidationHelper_t1155887809_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t1155887809_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (SocketAddress_t838303055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[1] = 
{
	SocketAddress_t838303055::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (AddressFamily_t303362630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2204[32] = 
{
	AddressFamily_t303362630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (IPv6MulticastOption_t1621622330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (LingerOption_t1165263720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[2] = 
{
	LingerOption_t1165263720::get_offset_of_enabled_0(),
	LingerOption_t1165263720::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (MulticastOption_t2505469155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (NetworkStream_t581172200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[6] = 
{
	NetworkStream_t581172200::get_offset_of_access_2(),
	NetworkStream_t581172200::get_offset_of_socket_3(),
	NetworkStream_t581172200::get_offset_of_owns_socket_4(),
	NetworkStream_t581172200::get_offset_of_readable_5(),
	NetworkStream_t581172200::get_offset_of_writeable_6(),
	NetworkStream_t581172200::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (ProtocolType_t2178963134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[26] = 
{
	ProtocolType_t2178963134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (SelectMode_t3413969319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[4] = 
{
	SelectMode_t3413969319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (Socket_t3821512045), -1, sizeof(Socket_t3821512045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2211[22] = 
{
	Socket_t3821512045::get_offset_of_readQ_0(),
	Socket_t3821512045::get_offset_of_writeQ_1(),
	Socket_t3821512045::get_offset_of_islistening_2(),
	Socket_t3821512045::get_offset_of_MinListenPort_3(),
	Socket_t3821512045::get_offset_of_MaxListenPort_4(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t3821512045::get_offset_of_linger_timeout_7(),
	Socket_t3821512045::get_offset_of_socket_8(),
	Socket_t3821512045::get_offset_of_address_family_9(),
	Socket_t3821512045::get_offset_of_socket_type_10(),
	Socket_t3821512045::get_offset_of_protocol_type_11(),
	Socket_t3821512045::get_offset_of_blocking_12(),
	Socket_t3821512045::get_offset_of_blocking_thread_13(),
	Socket_t3821512045::get_offset_of_isbound_14(),
	Socket_t3821512045_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t3821512045::get_offset_of_max_bind_count_16(),
	Socket_t3821512045::get_offset_of_connected_17(),
	Socket_t3821512045::get_offset_of_closed_18(),
	Socket_t3821512045::get_offset_of_disposed_19(),
	Socket_t3821512045::get_offset_of_seed_endpoint_20(),
	Socket_t3821512045_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (SocketOperation_t3328960782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2212[15] = 
{
	SocketOperation_t3328960782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (SocketAsyncResult_t2959281146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[25] = 
{
	SocketAsyncResult_t2959281146::get_offset_of_Sock_0(),
	SocketAsyncResult_t2959281146::get_offset_of_handle_1(),
	SocketAsyncResult_t2959281146::get_offset_of_state_2(),
	SocketAsyncResult_t2959281146::get_offset_of_callback_3(),
	SocketAsyncResult_t2959281146::get_offset_of_waithandle_4(),
	SocketAsyncResult_t2959281146::get_offset_of_delayedException_5(),
	SocketAsyncResult_t2959281146::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t2959281146::get_offset_of_Buffer_7(),
	SocketAsyncResult_t2959281146::get_offset_of_Offset_8(),
	SocketAsyncResult_t2959281146::get_offset_of_Size_9(),
	SocketAsyncResult_t2959281146::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t2959281146::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t2959281146::get_offset_of_Addresses_12(),
	SocketAsyncResult_t2959281146::get_offset_of_Port_13(),
	SocketAsyncResult_t2959281146::get_offset_of_Buffers_14(),
	SocketAsyncResult_t2959281146::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t2959281146::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t2959281146::get_offset_of_total_17(),
	SocketAsyncResult_t2959281146::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t2959281146::get_offset_of_completed_19(),
	SocketAsyncResult_t2959281146::get_offset_of_blocking_20(),
	SocketAsyncResult_t2959281146::get_offset_of_error_21(),
	SocketAsyncResult_t2959281146::get_offset_of_operation_22(),
	SocketAsyncResult_t2959281146::get_offset_of_ares_23(),
	SocketAsyncResult_t2959281146::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (Worker_t1317165246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	Worker_t1317165246::get_offset_of_result_0(),
	Worker_t1317165246::get_offset_of_requireSocketSecurity_1(),
	Worker_t1317165246::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (SocketAsyncCall_t3737776727), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (SocketError_t307542793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[48] = 
{
	SocketError_t307542793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (SocketException_t1618573604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (SocketFlags_t2353657790)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2218[11] = 
{
	SocketFlags_t2353657790::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (SocketOptionLevel_t1505247880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2219[6] = 
{
	SocketOptionLevel_t1505247880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (SocketOptionName_t1089121285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2220[44] = 
{
	SocketOptionName_t1089121285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (SocketShutdown_t3247039417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2221[4] = 
{
	SocketShutdown_t3247039417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (SocketType_t1143498533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2222[7] = 
{
	SocketType_t1143498533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (WebAsyncResult_t905414499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[17] = 
{
	WebAsyncResult_t905414499::get_offset_of_handle_0(),
	WebAsyncResult_t905414499::get_offset_of_synch_1(),
	WebAsyncResult_t905414499::get_offset_of_isCompleted_2(),
	WebAsyncResult_t905414499::get_offset_of_cb_3(),
	WebAsyncResult_t905414499::get_offset_of_state_4(),
	WebAsyncResult_t905414499::get_offset_of_nbytes_5(),
	WebAsyncResult_t905414499::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t905414499::get_offset_of_callbackDone_7(),
	WebAsyncResult_t905414499::get_offset_of_exc_8(),
	WebAsyncResult_t905414499::get_offset_of_response_9(),
	WebAsyncResult_t905414499::get_offset_of_writeStream_10(),
	WebAsyncResult_t905414499::get_offset_of_buffer_11(),
	WebAsyncResult_t905414499::get_offset_of_offset_12(),
	WebAsyncResult_t905414499::get_offset_of_size_13(),
	WebAsyncResult_t905414499::get_offset_of_locker_14(),
	WebAsyncResult_t905414499::get_offset_of_EndCalled_15(),
	WebAsyncResult_t905414499::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (ReadState_t657568301)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[5] = 
{
	ReadState_t657568301::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (WebConnection_t324679648), -1, sizeof(WebConnection_t324679648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[32] = 
{
	WebConnection_t324679648::get_offset_of_sPoint_0(),
	WebConnection_t324679648::get_offset_of_nstream_1(),
	WebConnection_t324679648::get_offset_of_socket_2(),
	WebConnection_t324679648::get_offset_of_socketLock_3(),
	WebConnection_t324679648::get_offset_of_status_4(),
	WebConnection_t324679648::get_offset_of_initConn_5(),
	WebConnection_t324679648::get_offset_of_keepAlive_6(),
	WebConnection_t324679648::get_offset_of_buffer_7(),
	WebConnection_t324679648_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t324679648::get_offset_of_abortHandler_9(),
	WebConnection_t324679648::get_offset_of_abortHelper_10(),
	WebConnection_t324679648::get_offset_of_readState_11(),
	WebConnection_t324679648::get_offset_of_Data_12(),
	WebConnection_t324679648::get_offset_of_chunkedRead_13(),
	WebConnection_t324679648::get_offset_of_chunkStream_14(),
	WebConnection_t324679648::get_offset_of_queue_15(),
	WebConnection_t324679648::get_offset_of_reused_16(),
	WebConnection_t324679648::get_offset_of_position_17(),
	WebConnection_t324679648::get_offset_of_busy_18(),
	WebConnection_t324679648::get_offset_of_priority_request_19(),
	WebConnection_t324679648::get_offset_of_ntlm_credentials_20(),
	WebConnection_t324679648::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t324679648::get_offset_of_unsafe_sharing_22(),
	WebConnection_t324679648::get_offset_of_ssl_23(),
	WebConnection_t324679648::get_offset_of_certsAvailable_24(),
	WebConnection_t324679648::get_offset_of_connect_exception_25(),
	WebConnection_t324679648_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t324679648_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t324679648_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t324679648_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t324679648_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t324679648_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (AbortHelper_t2895113041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	AbortHelper_t2895113041::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (WebConnectionData_t3550260432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[7] = 
{
	WebConnectionData_t3550260432::get_offset_of_request_0(),
	WebConnectionData_t3550260432::get_offset_of_StatusCode_1(),
	WebConnectionData_t3550260432::get_offset_of_StatusDescription_2(),
	WebConnectionData_t3550260432::get_offset_of_Headers_3(),
	WebConnectionData_t3550260432::get_offset_of_Version_4(),
	WebConnectionData_t3550260432::get_offset_of_stream_5(),
	WebConnectionData_t3550260432::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (WebConnectionGroup_t3242458773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[5] = 
{
	WebConnectionGroup_t3242458773::get_offset_of_sPoint_0(),
	WebConnectionGroup_t3242458773::get_offset_of_name_1(),
	WebConnectionGroup_t3242458773::get_offset_of_connections_2(),
	WebConnectionGroup_t3242458773::get_offset_of_rnd_3(),
	WebConnectionGroup_t3242458773::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (WebConnectionStream_t1922483508), -1, sizeof(WebConnectionStream_t1922483508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2229[27] = 
{
	WebConnectionStream_t1922483508_StaticFields::get_offset_of_crlf_2(),
	WebConnectionStream_t1922483508::get_offset_of_isRead_3(),
	WebConnectionStream_t1922483508::get_offset_of_cnc_4(),
	WebConnectionStream_t1922483508::get_offset_of_request_5(),
	WebConnectionStream_t1922483508::get_offset_of_readBuffer_6(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferOffset_7(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferSize_8(),
	WebConnectionStream_t1922483508::get_offset_of_contentLength_9(),
	WebConnectionStream_t1922483508::get_offset_of_totalRead_10(),
	WebConnectionStream_t1922483508::get_offset_of_totalWritten_11(),
	WebConnectionStream_t1922483508::get_offset_of_nextReadCalled_12(),
	WebConnectionStream_t1922483508::get_offset_of_pendingReads_13(),
	WebConnectionStream_t1922483508::get_offset_of_pendingWrites_14(),
	WebConnectionStream_t1922483508::get_offset_of_pending_15(),
	WebConnectionStream_t1922483508::get_offset_of_allowBuffering_16(),
	WebConnectionStream_t1922483508::get_offset_of_sendChunked_17(),
	WebConnectionStream_t1922483508::get_offset_of_writeBuffer_18(),
	WebConnectionStream_t1922483508::get_offset_of_requestWritten_19(),
	WebConnectionStream_t1922483508::get_offset_of_headers_20(),
	WebConnectionStream_t1922483508::get_offset_of_disposed_21(),
	WebConnectionStream_t1922483508::get_offset_of_headersSent_22(),
	WebConnectionStream_t1922483508::get_offset_of_locker_23(),
	WebConnectionStream_t1922483508::get_offset_of_initRead_24(),
	WebConnectionStream_t1922483508::get_offset_of_read_eof_25(),
	WebConnectionStream_t1922483508::get_offset_of_complete_request_written_26(),
	WebConnectionStream_t1922483508::get_offset_of_read_timeout_27(),
	WebConnectionStream_t1922483508::get_offset_of_write_timeout_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (WebException_t3368933679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[2] = 
{
	WebException_t3368933679::get_offset_of_response_12(),
	WebException_t3368933679::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (WebExceptionStatus_t1169373531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2231[22] = 
{
	WebExceptionStatus_t1169373531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (WebHeaderCollection_t3028142837), -1, sizeof(WebHeaderCollection_t3028142837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[5] = 
{
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t3028142837::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (WebProxy_t1169192840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[5] = 
{
	WebProxy_t1169192840::get_offset_of_address_0(),
	WebProxy_t1169192840::get_offset_of_bypassOnLocal_1(),
	WebProxy_t1169192840::get_offset_of_bypassList_2(),
	WebProxy_t1169192840::get_offset_of_credentials_3(),
	WebProxy_t1169192840::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (WebRequest_t1365124353), -1, sizeof(WebRequest_t1365124353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2234[5] = 
{
	WebRequest_t1365124353_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t1365124353_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t1365124353_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t1365124353::get_offset_of_authentication_level_4(),
	WebRequest_t1365124353_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (WebResponse_t1895226051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (SslProtocols_t894678499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2236[6] = 
{
	SslProtocols_t894678499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (AsnDecodeStatus_t1962003286)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2237[7] = 
{
	AsnDecodeStatus_t1962003286::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (AsnEncodedData_t463456204), -1, sizeof(AsnEncodedData_t463456204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2238[3] = 
{
	AsnEncodedData_t463456204::get_offset_of__oid_0(),
	AsnEncodedData_t463456204::get_offset_of__raw_1(),
	AsnEncodedData_t463456204_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (OidCollection_t3790243618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[2] = 
{
	OidCollection_t3790243618::get_offset_of__list_0(),
	OidCollection_t3790243618::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (Oid_t3221867120), -1, sizeof(Oid_t3221867120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2240[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Oid_t3221867120::get_offset_of__value_20(),
	Oid_t3221867120::get_offset_of__name_21(),
	Oid_t3221867120_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_22(),
	Oid_t3221867120_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (OidEnumerator_t3674631724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	OidEnumerator_t3674631724::get_offset_of__collection_0(),
	OidEnumerator_t3674631724::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (OpenFlags_t2370524385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2242[6] = 
{
	OpenFlags_t2370524385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (OSX509Certificates_t3584809896), -1, sizeof(OSX509Certificates_t3584809896_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[1] = 
{
	OSX509Certificates_t3584809896_StaticFields::get_offset_of_sslsecpolicy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (SecTrustResult_t1984565408)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[9] = 
{
	SecTrustResult_t1984565408::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (PublicKey_t870392), -1, sizeof(PublicKey_t870392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2245[7] = 
{
	0,
	0,
	PublicKey_t870392::get_offset_of__key_2(),
	PublicKey_t870392::get_offset_of__keyValue_3(),
	PublicKey_t870392::get_offset_of__params_4(),
	PublicKey_t870392::get_offset_of__oid_5(),
	PublicKey_t870392_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (StoreLocation_t1570828128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2246[3] = 
{
	StoreLocation_t1570828128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (StoreName_t2183514610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2247[9] = 
{
	StoreName_t2183514610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (X500DistinguishedName_t452415348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[2] = 
{
	0,
	X500DistinguishedName_t452415348::get_offset_of_name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (X500DistinguishedNameFlags_t2005802885)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2249[11] = 
{
	X500DistinguishedNameFlags_t2005802885::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (X509BasicConstraintsExtension_t1562873317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t1562873317::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (X509Certificate2Collection_t1108969367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (X509Certificate2_t4056456767), -1, sizeof(X509Certificate2_t4056456767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2252[13] = 
{
	X509Certificate2_t4056456767::get_offset_of__archived_5(),
	X509Certificate2_t4056456767::get_offset_of__extensions_6(),
	X509Certificate2_t4056456767::get_offset_of__name_7(),
	X509Certificate2_t4056456767::get_offset_of__serial_8(),
	X509Certificate2_t4056456767::get_offset_of__publicKey_9(),
	X509Certificate2_t4056456767::get_offset_of_issuer_name_10(),
	X509Certificate2_t4056456767::get_offset_of_subject_name_11(),
	X509Certificate2_t4056456767::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t4056456767::get_offset_of__cert_13(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (X509Certificate2Enumerator_t2356134957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[1] = 
{
	X509Certificate2Enumerator_t2356134957::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (X509CertificateCollection_t1197680765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (X509CertificateEnumerator_t1208230922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[1] = 
{
	X509CertificateEnumerator_t1208230922::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (X509Chain_t777637347), -1, sizeof(X509Chain_t777637347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2256[15] = 
{
	X509Chain_t777637347::get_offset_of_location_0(),
	X509Chain_t777637347::get_offset_of_elements_1(),
	X509Chain_t777637347::get_offset_of_policy_2(),
	X509Chain_t777637347::get_offset_of_status_3(),
	X509Chain_t777637347_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t777637347::get_offset_of_max_path_length_5(),
	X509Chain_t777637347::get_offset_of_working_issuer_name_6(),
	X509Chain_t777637347::get_offset_of_working_public_key_7(),
	X509Chain_t777637347::get_offset_of_bce_restriction_8(),
	X509Chain_t777637347::get_offset_of_roots_9(),
	X509Chain_t777637347::get_offset_of_cas_10(),
	X509Chain_t777637347::get_offset_of_collection_11(),
	X509Chain_t777637347_StaticFields::get_offset_of_U3CU3Ef__switchU24map17_12(),
	X509Chain_t777637347_StaticFields::get_offset_of_U3CU3Ef__switchU24map18_13(),
	X509Chain_t777637347_StaticFields::get_offset_of_U3CU3Ef__switchU24map19_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (X509ChainElementCollection_t2081831987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[1] = 
{
	X509ChainElementCollection_t2081831987::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (X509ChainElement_t528874471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[4] = 
{
	X509ChainElement_t528874471::get_offset_of_certificate_0(),
	X509ChainElement_t528874471::get_offset_of_status_1(),
	X509ChainElement_t528874471::get_offset_of_info_2(),
	X509ChainElement_t528874471::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (X509ChainElementEnumerator_t3304975821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	X509ChainElementEnumerator_t3304975821::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (X509ChainPolicy_t3452126517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[8] = 
{
	X509ChainPolicy_t3452126517::get_offset_of_apps_0(),
	X509ChainPolicy_t3452126517::get_offset_of_cert_1(),
	X509ChainPolicy_t3452126517::get_offset_of_store_2(),
	X509ChainPolicy_t3452126517::get_offset_of_rflag_3(),
	X509ChainPolicy_t3452126517::get_offset_of_mode_4(),
	X509ChainPolicy_t3452126517::get_offset_of_timeout_5(),
	X509ChainPolicy_t3452126517::get_offset_of_vflags_6(),
	X509ChainPolicy_t3452126517::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (X509ChainStatus_t4278378721)+ sizeof (Il2CppObject), sizeof(X509ChainStatus_t4278378721_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[2] = 
{
	X509ChainStatus_t4278378721::get_offset_of_status_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509ChainStatus_t4278378721::get_offset_of_info_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (X509ChainStatusFlags_t480677120)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[24] = 
{
	X509ChainStatusFlags_t480677120::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (X509EnhancedKeyUsageExtension_t2099881051), -1, sizeof(X509EnhancedKeyUsageExtension_t2099881051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t2099881051_StaticFields::get_offset_of_U3CU3Ef__switchU24map1A_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (X509ExtensionCollection_t650873211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[1] = 
{
	X509ExtensionCollection_t650873211::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (X509Extension_t1320896183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	X509Extension_t1320896183::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (X509ExtensionEnumerator_t3763443773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[1] = 
{
	X509ExtensionEnumerator_t3763443773::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (X509FindType_t3221716179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2267[16] = 
{
	X509FindType_t3221716179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (X509KeyUsageExtension_t1038124237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t1038124237::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t1038124237::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (X509KeyUsageFlags_t2461349531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[11] = 
{
	X509KeyUsageFlags_t2461349531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (X509NameType_t2669466891)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[7] = 
{
	X509NameType_t2669466891::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (X509RevocationFlag_t2166064554)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[4] = 
{
	X509RevocationFlag_t2166064554::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (X509RevocationMode_t2065307963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[4] = 
{
	X509RevocationMode_t2065307963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (X509Store_t1617430119), -1, sizeof(X509Store_t1617430119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2273[6] = 
{
	X509Store_t1617430119::get_offset_of__name_0(),
	X509Store_t1617430119::get_offset_of__location_1(),
	X509Store_t1617430119::get_offset_of_list_2(),
	X509Store_t1617430119::get_offset_of__flags_3(),
	X509Store_t1617430119::get_offset_of_store_4(),
	X509Store_t1617430119_StaticFields::get_offset_of_U3CU3Ef__switchU24map1B_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (X509SubjectKeyIdentifierExtension_t2508879999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t110301003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2275[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t110301003::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (X509VerificationFlags_t2169036324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[15] = 
{
	X509VerificationFlags_t2169036324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (OpCode_t586571952)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2277[26] = 
{
	OpCode_t586571952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (OpFlags_t378191910)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2278[6] = 
{
	OpFlags_t378191910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (Position_t3781184359)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[11] = 
{
	Position_t3781184359::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (BaseMachine_t4008011478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[1] = 
{
	BaseMachine_t4008011478::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (MatchAppendEvaluator_t1618777330), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (FactoryCache_t2051534610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[3] = 
{
	FactoryCache_t2051534610::get_offset_of_capacity_0(),
	FactoryCache_t2051534610::get_offset_of_factories_1(),
	FactoryCache_t2051534610::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (Key_t655155419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[2] = 
{
	Key_t655155419::get_offset_of_pattern_0(),
	Key_t655155419::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (MRUList_t33178162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[2] = 
{
	MRUList_t33178162::get_offset_of_head_0(),
	MRUList_t33178162::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (Node_t1107172180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[3] = 
{
	Node_t1107172180::get_offset_of_value_0(),
	Node_t1107172180::get_offset_of_previous_1(),
	Node_t1107172180::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (CaptureCollection_t1671345504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[1] = 
{
	CaptureCollection_t1671345504::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[3] = 
{
	Capture_t4157900610::get_offset_of_index_0(),
	Capture_t4157900610::get_offset_of_length_1(),
	Capture_t4157900610::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (CILCompiler_t1740644799), -1, sizeof(CILCompiler_t1740644799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2290[32] = 
{
	CILCompiler_t1740644799::get_offset_of_eval_methods_2(),
	CILCompiler_t1740644799::get_offset_of_eval_methods_defined_3(),
	CILCompiler_t1740644799::get_offset_of_generic_ops_4(),
	CILCompiler_t1740644799::get_offset_of_op_flags_5(),
	CILCompiler_t1740644799::get_offset_of_labels_6(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_str_7(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_string_start_8(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_string_end_9(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_program_10(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_marks_11(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_groups_12(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_deep_13(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_stack_14(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_start_15(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_end_16(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_index_17(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_get_count_18(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_set_count_19(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_push_20(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_pop_21(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_set_start_of_match_22(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_is_word_char_23(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_reset_groups_24(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_checkpoint_25(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_backtrack_26(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_open_27(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_close_28(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_get_last_defined_29(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_mark_get_index_30(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_mark_get_length_31(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_trace_compile_32(),
	CILCompiler_t1740644799::get_offset_of_local_textinfo_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (Frame_t997927490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[2] = 
{
	Frame_t997927490::get_offset_of_label_pass_0(),
	Frame_t997927490::get_offset_of_label_fail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (Category_t1984577050)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2292[146] = 
{
	Category_t1984577050::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (CategoryUtils_t3840220623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (LinkRef_t2090853131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (InterpreterFactory_t556462562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[4] = 
{
	InterpreterFactory_t556462562::get_offset_of_mapping_0(),
	InterpreterFactory_t556462562::get_offset_of_pattern_1(),
	InterpreterFactory_t556462562::get_offset_of_namesMapping_2(),
	InterpreterFactory_t556462562::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (PatternCompiler_t637049905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[1] = 
{
	PatternCompiler_t637049905::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (PatternLinkStack_t3979537293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[1] = 
{
	PatternLinkStack_t3979537293::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (Link_t3337276394)+ sizeof (Il2CppObject), sizeof(Link_t3337276394 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2299[2] = 
{
	Link_t3337276394::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t3337276394::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
