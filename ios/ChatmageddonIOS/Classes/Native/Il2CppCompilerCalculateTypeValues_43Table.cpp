﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S594014466.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2003641865.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2302259227.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4145020727.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S706509685.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2287957350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_Se69573822.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4117265220.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3465175809.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4055644322.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3640352222.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2226336700.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1633071870.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4124360053.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1196028363.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1982981439.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S771642946.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2917740615.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3721703645.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3774205409.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S696024791.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2277472452.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4038813788.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4106780194.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S369756392.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2764394868.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1172060768.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1521970469.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1172053409.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1521962978.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4283726064.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2707684392.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2372832748.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1833811373.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2372829481.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1833808238.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4196096651.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1728460721.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S132931622.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3683937883.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S245426841.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3656182376.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S245419482.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3656174885.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4283899217.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1894758407.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4210650542.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3860742633.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2378546603.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1839525484.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4283905371.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S598377489.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4211010036.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3861101235.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2432108964.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1170406292.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1564258929.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1967734354.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3077597332.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1785192879.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3878961688.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3079341752.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3466749709.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1833857086.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3579244928.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_Se28239691.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S686715528.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3353873688.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1442275455.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1092367290.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3905138812.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3366117437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Endo_Gl2692134550.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Endo_Glv500309695.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl2477536561.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli762726336.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli404341464.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl4261770478.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli643117704.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl2813560483.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli485024160.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl1510522174.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl1999582299.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl3900730752.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_Finit239290640.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_GF2P3071691857.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_Gene3677945350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_Prime623492065.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Interl4139004379.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Mod3464319674.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat2301520307.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat128435875714.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { sizeof (SecP224K1Curve_t594014466), -1, sizeof(SecP224K1Curve_t594014466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4300[3] = 
{
	SecP224K1Curve_t594014466_StaticFields::get_offset_of_q_16(),
	0,
	SecP224K1Curve_t594014466::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (SecP224K1Field_t2003641865), -1, sizeof(SecP224K1Field_t2003641865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4301[6] = 
{
	SecP224K1Field_t2003641865_StaticFields::get_offset_of_P_0(),
	SecP224K1Field_t2003641865_StaticFields::get_offset_of_PExt_1(),
	SecP224K1Field_t2003641865_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (SecP224K1FieldElement_t2302259227), -1, sizeof(SecP224K1FieldElement_t2302259227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4302[3] = 
{
	SecP224K1FieldElement_t2302259227_StaticFields::get_offset_of_Q_0(),
	SecP224K1FieldElement_t2302259227_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	SecP224K1FieldElement_t2302259227::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (SecP224K1Point_t4145020727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (SecP224R1Curve_t706509685), -1, sizeof(SecP224R1Curve_t706509685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4304[3] = 
{
	SecP224R1Curve_t706509685_StaticFields::get_offset_of_q_16(),
	0,
	SecP224R1Curve_t706509685::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (SecP224R1Field_t2287957350), -1, sizeof(SecP224R1Field_t2287957350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4305[5] = 
{
	SecP224R1Field_t2287957350_StaticFields::get_offset_of_P_0(),
	SecP224R1Field_t2287957350_StaticFields::get_offset_of_PExt_1(),
	SecP224R1Field_t2287957350_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (SecP224R1FieldElement_t69573822), -1, sizeof(SecP224R1FieldElement_t69573822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4306[2] = 
{
	SecP224R1FieldElement_t69573822_StaticFields::get_offset_of_Q_0(),
	SecP224R1FieldElement_t69573822::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { sizeof (SecP224R1Point_t4117265220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (SecP256K1Curve_t3465175809), -1, sizeof(SecP256K1Curve_t3465175809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4308[3] = 
{
	SecP256K1Curve_t3465175809_StaticFields::get_offset_of_q_16(),
	0,
	SecP256K1Curve_t3465175809::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (SecP256K1Field_t4055644322), -1, sizeof(SecP256K1Field_t4055644322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4309[6] = 
{
	SecP256K1Field_t4055644322_StaticFields::get_offset_of_P_0(),
	SecP256K1Field_t4055644322_StaticFields::get_offset_of_PExt_1(),
	SecP256K1Field_t4055644322_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (SecP256K1FieldElement_t3640352222), -1, sizeof(SecP256K1FieldElement_t3640352222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4310[2] = 
{
	SecP256K1FieldElement_t3640352222_StaticFields::get_offset_of_Q_0(),
	SecP256K1FieldElement_t3640352222::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (SecP256K1Point_t2226336700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (SecP256R1Curve_t1633071870), -1, sizeof(SecP256R1Curve_t1633071870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4312[3] = 
{
	SecP256R1Curve_t1633071870_StaticFields::get_offset_of_q_16(),
	0,
	SecP256R1Curve_t1633071870::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { sizeof (SecP256R1Field_t4124360053), -1, sizeof(SecP256R1Field_t4124360053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4313[4] = 
{
	SecP256R1Field_t4124360053_StaticFields::get_offset_of_P_0(),
	SecP256R1Field_t4124360053_StaticFields::get_offset_of_PExt_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (SecP256R1FieldElement_t1196028363), -1, sizeof(SecP256R1FieldElement_t1196028363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4314[2] = 
{
	SecP256R1FieldElement_t1196028363_StaticFields::get_offset_of_Q_0(),
	SecP256R1FieldElement_t1196028363::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (SecP256R1Point_t1982981439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (SecP384R1Curve_t771642946), -1, sizeof(SecP384R1Curve_t771642946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4316[3] = 
{
	SecP384R1Curve_t771642946_StaticFields::get_offset_of_q_16(),
	0,
	SecP384R1Curve_t771642946::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (SecP384R1Field_t2917740615), -1, sizeof(SecP384R1Field_t2917740615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4317[5] = 
{
	SecP384R1Field_t2917740615_StaticFields::get_offset_of_P_0(),
	SecP384R1Field_t2917740615_StaticFields::get_offset_of_PExt_1(),
	SecP384R1Field_t2917740615_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { sizeof (SecP384R1FieldElement_t3721703645), -1, sizeof(SecP384R1FieldElement_t3721703645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4318[2] = 
{
	SecP384R1FieldElement_t3721703645_StaticFields::get_offset_of_Q_0(),
	SecP384R1FieldElement_t3721703645::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { sizeof (SecP384R1Point_t3774205409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (SecP521R1Curve_t696024791), -1, sizeof(SecP521R1Curve_t696024791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4320[3] = 
{
	SecP521R1Curve_t696024791_StaticFields::get_offset_of_q_16(),
	0,
	SecP521R1Curve_t696024791::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (SecP521R1Field_t2277472452), -1, sizeof(SecP521R1Field_t2277472452_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4321[2] = 
{
	SecP521R1Field_t2277472452_StaticFields::get_offset_of_P_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (SecP521R1FieldElement_t4038813788), -1, sizeof(SecP521R1FieldElement_t4038813788_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4322[2] = 
{
	SecP521R1FieldElement_t4038813788_StaticFields::get_offset_of_Q_0(),
	SecP521R1FieldElement_t4038813788::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (SecP521R1Point_t4106780194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { sizeof (SecT113Field_t369756392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4324[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (SecT113FieldElement_t2764394868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4325[1] = 
{
	SecT113FieldElement_t2764394868::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (SecT113R1Curve_t1172060768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4326[2] = 
{
	0,
	SecT113R1Curve_t1172060768::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { sizeof (SecT113R1Point_t1521970469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (SecT113R2Curve_t1172053409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4328[2] = 
{
	0,
	SecT113R2Curve_t1172053409::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { sizeof (SecT113R2Point_t1521962978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (SecT131Field_t4283726064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4330[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (SecT131FieldElement_t2707684392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4331[1] = 
{
	SecT131FieldElement_t2707684392::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (SecT131R1Curve_t2372832748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4332[2] = 
{
	0,
	SecT131R1Curve_t2372832748::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (SecT131R1Point_t1833811373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (SecT131R2Curve_t2372829481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4334[2] = 
{
	0,
	SecT131R2Curve_t2372829481::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (SecT131R2Point_t1833808238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (SecT163Field_t4196096651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4336[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { sizeof (SecT163FieldElement_t1728460721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4337[1] = 
{
	SecT163FieldElement_t1728460721::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (SecT163K1Curve_t132931622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4338[2] = 
{
	0,
	SecT163K1Curve_t132931622::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (SecT163K1Point_t3683937883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { sizeof (SecT163R1Curve_t245426841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4340[2] = 
{
	0,
	SecT163R1Curve_t245426841::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { sizeof (SecT163R1Point_t3656182376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (SecT163R2Curve_t245419482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4342[2] = 
{
	0,
	SecT163R2Curve_t245419482::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (SecT163R2Point_t3656174885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (SecT233Field_t4283899217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4344[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (SecT233FieldElement_t1894758407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4345[1] = 
{
	SecT233FieldElement_t1894758407::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (SecT233K1Curve_t4210650542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4346[2] = 
{
	0,
	SecT233K1Curve_t4210650542::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (SecT233K1Point_t3860742633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { sizeof (SecT233R1Curve_t2378546603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4348[2] = 
{
	0,
	SecT233R1Curve_t2378546603::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (SecT233R1Point_t1839525484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (SecT239Field_t4283905371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4350[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (SecT239FieldElement_t598377489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4351[1] = 
{
	SecT239FieldElement_t598377489::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (SecT239K1Curve_t4211010036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4352[2] = 
{
	0,
	SecT239K1Curve_t4211010036::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (SecT239K1Point_t3861101235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (SecT283Field_t2432108964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4354[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (SecT283FieldElement_t1170406292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4355[1] = 
{
	SecT283FieldElement_t1170406292::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (SecT283K1Curve_t1564258929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4356[2] = 
{
	0,
	SecT283K1Curve_t1564258929::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (SecT283K1Point_t1967734354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (SecT283R1Curve_t3077597332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4358[2] = 
{
	0,
	SecT283R1Curve_t3077597332::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (SecT283R1Point_t1785192879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (SecT409Field_t3878961688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4360[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (SecT409FieldElement_t3079341752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4361[1] = 
{
	SecT409FieldElement_t3079341752::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (SecT409K1Curve_t3466749709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4362[2] = 
{
	0,
	SecT409K1Curve_t3466749709::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (SecT409K1Point_t1833857086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (SecT409R1Curve_t3579244928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4364[2] = 
{
	0,
	SecT409R1Curve_t3579244928::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (SecT409R1Point_t28239691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (SecT571Field_t686715528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4366[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (SecT571FieldElement_t3353873688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4367[1] = 
{
	SecT571FieldElement_t3353873688::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (SecT571K1Curve_t1442275455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4368[2] = 
{
	0,
	SecT571K1Curve_t1442275455::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { sizeof (SecT571K1Point_t1092367290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { sizeof (SecT571R1Curve_t3905138812), -1, sizeof(SecT571R1Curve_t3905138812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4370[4] = 
{
	0,
	SecT571R1Curve_t3905138812::get_offset_of_m_infinity_18(),
	SecT571R1Curve_t3905138812_StaticFields::get_offset_of_SecT571R1_B_19(),
	SecT571R1Curve_t3905138812_StaticFields::get_offset_of_SecT571R1_B_SQRT_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { sizeof (SecT571R1Point_t3366117437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { sizeof (GlvTypeBEndomorphism_t2692134550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4374[3] = 
{
	GlvTypeBEndomorphism_t2692134550::get_offset_of_m_curve_0(),
	GlvTypeBEndomorphism_t2692134550::get_offset_of_m_parameters_1(),
	GlvTypeBEndomorphism_t2692134550::get_offset_of_m_pointMap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (GlvTypeBParameters_t500309695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4375[7] = 
{
	GlvTypeBParameters_t500309695::get_offset_of_m_beta_0(),
	GlvTypeBParameters_t500309695::get_offset_of_m_lambda_1(),
	GlvTypeBParameters_t500309695::get_offset_of_m_v1_2(),
	GlvTypeBParameters_t500309695::get_offset_of_m_v2_3(),
	GlvTypeBParameters_t500309695::get_offset_of_m_g1_4(),
	GlvTypeBParameters_t500309695::get_offset_of_m_g2_5(),
	GlvTypeBParameters_t500309695::get_offset_of_m_bits_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (AbstractECMultiplier_t2477536561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (FixedPointCombMultiplier_t762726336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (FixedPointPreCompInfo_t404341464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4379[2] = 
{
	FixedPointPreCompInfo_t404341464::get_offset_of_m_preComp_0(),
	FixedPointPreCompInfo_t404341464::get_offset_of_m_width_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (FixedPointUtilities_t4261770478), -1, sizeof(FixedPointUtilities_t4261770478_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4380[1] = 
{
	FixedPointUtilities_t4261770478_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (GlvMultiplier_t643117704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4381[2] = 
{
	GlvMultiplier_t643117704::get_offset_of_curve_0(),
	GlvMultiplier_t643117704::get_offset_of_glvEndomorphism_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (WNafL2RMultiplier_t2813560483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (WNafPreCompInfo_t485024160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4384[3] = 
{
	WNafPreCompInfo_t485024160::get_offset_of_m_preComp_0(),
	WNafPreCompInfo_t485024160::get_offset_of_m_preCompNeg_1(),
	WNafPreCompInfo_t485024160::get_offset_of_m_twice_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { sizeof (WNafUtilities_t1510522174), -1, sizeof(WNafUtilities_t1510522174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4385[5] = 
{
	WNafUtilities_t1510522174_StaticFields::get_offset_of_PRECOMP_NAME_0(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_EMPTY_BYTES_2(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_EMPTY_INTS_3(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_EMPTY_POINTS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { sizeof (WTauNafMultiplier_t1999582299), -1, sizeof(WTauNafMultiplier_t1999582299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4386[1] = 
{
	WTauNafMultiplier_t1999582299_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { sizeof (WTauNafPreCompInfo_t3900730752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4387[1] = 
{
	WTauNafPreCompInfo_t3900730752::get_offset_of_m_preComp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { sizeof (FiniteFields_t239290640), -1, sizeof(FiniteFields_t239290640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4388[2] = 
{
	FiniteFields_t239290640_StaticFields::get_offset_of_GF_2_0(),
	FiniteFields_t239290640_StaticFields::get_offset_of_GF_3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { sizeof (GF2Polynomial_t3071691857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4389[1] = 
{
	GF2Polynomial_t3071691857::get_offset_of_exponents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { sizeof (GenericPolynomialExtensionField_t3677945350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4390[2] = 
{
	GenericPolynomialExtensionField_t3677945350::get_offset_of_subfield_0(),
	GenericPolynomialExtensionField_t3677945350::get_offset_of_minimalPolynomial_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (PrimeField_t623492065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4395[1] = 
{
	PrimeField_t623492065::get_offset_of_characteristic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (Interleave_t4139004379), -1, sizeof(Interleave_t4139004379_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4396[1] = 
{
	Interleave_t4139004379_StaticFields::get_offset_of_INTERLEAVE2_TABLE_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (Mod_t3464319674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (Nat_t2301520307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4398[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (Nat128_t435875714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4399[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
