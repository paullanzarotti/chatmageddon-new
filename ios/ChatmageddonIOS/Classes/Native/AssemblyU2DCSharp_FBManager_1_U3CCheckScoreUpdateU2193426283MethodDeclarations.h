﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>
struct U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CCheckScoreUpdateU3Ec__Iterator0__ctor_m4149393446_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method);
#define U3CCheckScoreUpdateU3Ec__Iterator0__ctor_m4149393446(__this, method) ((  void (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))U3CCheckScoreUpdateU3Ec__Iterator0__ctor_m4149393446_gshared)(__this, method)
// System.Boolean FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CCheckScoreUpdateU3Ec__Iterator0_MoveNext_m1213825570_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method);
#define U3CCheckScoreUpdateU3Ec__Iterator0_MoveNext_m1213825570(__this, method) ((  bool (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))U3CCheckScoreUpdateU3Ec__Iterator0_MoveNext_m1213825570_gshared)(__this, method)
// System.Object FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2850736412_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method);
#define U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2850736412(__this, method) ((  Il2CppObject * (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2850736412_gshared)(__this, method)
// System.Object FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2924679796_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method);
#define U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2924679796(__this, method) ((  Il2CppObject * (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))U3CCheckScoreUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2924679796_gshared)(__this, method)
// System.Void FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CCheckScoreUpdateU3Ec__Iterator0_Dispose_m2009072621_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method);
#define U3CCheckScoreUpdateU3Ec__Iterator0_Dispose_m2009072621(__this, method) ((  void (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))U3CCheckScoreUpdateU3Ec__Iterator0_Dispose_m2009072621_gshared)(__this, method)
// System.Void FBManager`1/<CheckScoreUpdate>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CCheckScoreUpdateU3Ec__Iterator0_Reset_m3007712715_gshared (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 * __this, const MethodInfo* method);
#define U3CCheckScoreUpdateU3Ec__Iterator0_Reset_m3007712715(__this, method) ((  void (*) (U3CCheckScoreUpdateU3Ec__Iterator0_t2193426283 *, const MethodInfo*))U3CCheckScoreUpdateU3Ec__Iterator0_Reset_m3007712715_gshared)(__this, method)
