﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Internal.ThirdParty.MiniJSON.Json/Parser
struct Parser_t4164574466;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Fabric_Internal_ThirdParty_MiniJSO67851492.h"

// System.Void Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m1828697493 (Parser_t4164574466 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern "C"  bool Parser_IsWordBreak_m3349857011 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::Parse(System.String)
extern "C"  Il2CppObject * Parser_Parse_m2532960489 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m3043430252 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::ParseObject()
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m1208723491 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::ParseArray()
extern "C"  List_1_t2058570427 * Parser_ParseArray_m1919918338 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m3250677648 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::ParseByToken(Fabric.Internal.ThirdParty.MiniJSON.Json/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m4003524387 (Parser_t4164574466 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m786141878 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m2388159432 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m2392031122 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::get_PeekChar()
extern "C"  Il2CppChar Parser_get_PeekChar_m1512775753 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::get_NextChar()
extern "C"  Il2CppChar Parser_get_NextChar_m2870472629 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m1035144230 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Fabric.Internal.ThirdParty.MiniJSON.Json/Parser/TOKEN Fabric.Internal.ThirdParty.MiniJSON.Json/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m2469064505 (Parser_t4164574466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
