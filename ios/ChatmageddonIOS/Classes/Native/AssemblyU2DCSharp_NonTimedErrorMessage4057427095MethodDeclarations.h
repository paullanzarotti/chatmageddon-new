﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NonTimedErrorMessage
struct NonTimedErrorMessage_t4057427095;

#include "codegen/il2cpp-codegen.h"

// System.Void NonTimedErrorMessage::.ctor()
extern "C"  void NonTimedErrorMessage__ctor_m174480010 (NonTimedErrorMessage_t4057427095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NonTimedErrorMessage::OnButtonClick()
extern "C"  void NonTimedErrorMessage_OnButtonClick_m636241071 (NonTimedErrorMessage_t4057427095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
