﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.AreaCodeMapStorageStrategy
struct AreaCodeMapStorageStrategy_t3709015996;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.AreaCodeMapStorageStrategy::.ctor()
extern "C"  void AreaCodeMapStorageStrategy__ctor_m746136773 (AreaCodeMapStorageStrategy_t3709015996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.AreaCodeMapStorageStrategy::getNumOfEntries()
extern "C"  int32_t AreaCodeMapStorageStrategy_getNumOfEntries_m1432596408 (AreaCodeMapStorageStrategy_t3709015996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> PhoneNumbers.AreaCodeMapStorageStrategy::getPossibleLengths()
extern "C"  List_1_t1440998580 * AreaCodeMapStorageStrategy_getPossibleLengths_m637484801 (AreaCodeMapStorageStrategy_t3709015996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.AreaCodeMapStorageStrategy::ToString()
extern "C"  String_t* AreaCodeMapStorageStrategy_ToString_m1061838754 (AreaCodeMapStorageStrategy_t3709015996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
