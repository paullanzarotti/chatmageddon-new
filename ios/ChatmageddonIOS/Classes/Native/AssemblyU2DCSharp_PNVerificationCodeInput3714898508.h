﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PNVerificationCodeInput
struct  PNVerificationCodeInput_t3714898508  : public SFXButton_t792651341
{
public:
	// UIInput PNVerificationCodeInput::codeinput
	UIInput_t860674234 * ___codeinput_5;

public:
	inline static int32_t get_offset_of_codeinput_5() { return static_cast<int32_t>(offsetof(PNVerificationCodeInput_t3714898508, ___codeinput_5)); }
	inline UIInput_t860674234 * get_codeinput_5() const { return ___codeinput_5; }
	inline UIInput_t860674234 ** get_address_of_codeinput_5() { return &___codeinput_5; }
	inline void set_codeinput_5(UIInput_t860674234 * value)
	{
		___codeinput_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeinput_5, value);
	}
};

struct PNVerificationCodeInput_t3714898508_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> PNVerificationCodeInput::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_6;
	// System.Action`2<System.Boolean,System.String> PNVerificationCodeInput::<>f__am$cache1
	Action_2_t1865222972 * ___U3CU3Ef__amU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(PNVerificationCodeInput_t3714898508_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(PNVerificationCodeInput_t3714898508_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_2_t1865222972 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_2_t1865222972 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_2_t1865222972 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
