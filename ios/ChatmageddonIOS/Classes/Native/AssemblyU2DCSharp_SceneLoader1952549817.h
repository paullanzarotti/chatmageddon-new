﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLoader
struct  SceneLoader_t1952549817  : public MonoBehaviour_t1158329972
{
public:
	// System.String SceneLoader::sceneToLoad
	String_t* ___sceneToLoad_2;
	// UnityEngine.Transform SceneLoader::UIParent
	Transform_t3275118058 * ___UIParent_3;
	// UnityEngine.GameObject SceneLoader::sceneUI
	GameObject_t1756533147 * ___sceneUI_4;
	// System.Boolean SceneLoader::UIclosing
	bool ___UIclosing_5;

public:
	inline static int32_t get_offset_of_sceneToLoad_2() { return static_cast<int32_t>(offsetof(SceneLoader_t1952549817, ___sceneToLoad_2)); }
	inline String_t* get_sceneToLoad_2() const { return ___sceneToLoad_2; }
	inline String_t** get_address_of_sceneToLoad_2() { return &___sceneToLoad_2; }
	inline void set_sceneToLoad_2(String_t* value)
	{
		___sceneToLoad_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneToLoad_2, value);
	}

	inline static int32_t get_offset_of_UIParent_3() { return static_cast<int32_t>(offsetof(SceneLoader_t1952549817, ___UIParent_3)); }
	inline Transform_t3275118058 * get_UIParent_3() const { return ___UIParent_3; }
	inline Transform_t3275118058 ** get_address_of_UIParent_3() { return &___UIParent_3; }
	inline void set_UIParent_3(Transform_t3275118058 * value)
	{
		___UIParent_3 = value;
		Il2CppCodeGenWriteBarrier(&___UIParent_3, value);
	}

	inline static int32_t get_offset_of_sceneUI_4() { return static_cast<int32_t>(offsetof(SceneLoader_t1952549817, ___sceneUI_4)); }
	inline GameObject_t1756533147 * get_sceneUI_4() const { return ___sceneUI_4; }
	inline GameObject_t1756533147 ** get_address_of_sceneUI_4() { return &___sceneUI_4; }
	inline void set_sceneUI_4(GameObject_t1756533147 * value)
	{
		___sceneUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___sceneUI_4, value);
	}

	inline static int32_t get_offset_of_UIclosing_5() { return static_cast<int32_t>(offsetof(SceneLoader_t1952549817, ___UIclosing_5)); }
	inline bool get_UIclosing_5() const { return ___UIclosing_5; }
	inline bool* get_address_of_UIclosing_5() { return &___UIclosing_5; }
	inline void set_UIclosing_5(bool value)
	{
		___UIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
