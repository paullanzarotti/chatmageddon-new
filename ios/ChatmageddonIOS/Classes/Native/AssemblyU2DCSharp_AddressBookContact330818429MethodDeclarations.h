﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddressBookContact
struct AddressBookContact_t330818429;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AddressBookContact::.ctor(System.String)
extern "C"  void AddressBookContact__ctor_m4151908612 (AddressBookContact_t330818429 * __this, String_t* ___newName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
