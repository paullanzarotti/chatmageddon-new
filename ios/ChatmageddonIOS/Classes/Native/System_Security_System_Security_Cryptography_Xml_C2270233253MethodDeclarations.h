﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.CipherData
struct CipherData_t2270233253;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Security.Cryptography.Xml.CipherReference
struct CipherReference_t2913200258;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "System_Security_System_Security_Cryptography_Xml_C2913200258.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.CipherData::.ctor()
extern "C"  void CipherData__ctor_m4162900283 (CipherData_t2270233253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.CipherData::.ctor(System.Byte[])
extern "C"  void CipherData__ctor_m3166464562 (CipherData_t2270233253 * __this, ByteU5BU5D_t3397334013* ___cipherValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.CipherReference System.Security.Cryptography.Xml.CipherData::get_CipherReference()
extern "C"  CipherReference_t2913200258 * CipherData_get_CipherReference_m4159537813 (CipherData_t2270233253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.CipherData::set_CipherReference(System.Security.Cryptography.Xml.CipherReference)
extern "C"  void CipherData_set_CipherReference_m616412548 (CipherData_t2270233253 * __this, CipherReference_t2913200258 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.CipherData::get_CipherValue()
extern "C"  ByteU5BU5D_t3397334013* CipherData_get_CipherValue_m4271897636 (CipherData_t2270233253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.CipherData::set_CipherValue(System.Byte[])
extern "C"  void CipherData_set_CipherValue_m3176316759 (CipherData_t2270233253 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.CipherData::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * CipherData_GetXml_m2620905254 (CipherData_t2270233253 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.CipherData::LoadXml(System.Xml.XmlElement)
extern "C"  void CipherData_LoadXml_m3135632995 (CipherData_t2270233253 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
