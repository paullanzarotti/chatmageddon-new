﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<PingServer>c__AnonStorey0
struct U3CPingServerU3Ec__AnonStorey0_t2343871673;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<PingServer>c__AnonStorey0::.ctor()
extern "C"  void U3CPingServerU3Ec__AnonStorey0__ctor_m3156187442 (U3CPingServerU3Ec__AnonStorey0_t2343871673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<PingServer>c__AnonStorey0::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CPingServerU3Ec__AnonStorey0_U3CU3Em__0_m1992597153 (U3CPingServerU3Ec__AnonStorey0_t2343871673 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
