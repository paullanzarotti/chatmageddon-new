﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Unibill.Impl.PostParameter[]
struct PostParameterU5BU5D_t2629828016;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.HTTPClient/PostRequest
struct  PostRequest_t4132869030  : public Il2CppObject
{
public:
	// System.String Unibill.Impl.HTTPClient/PostRequest::url
	String_t* ___url_0;
	// Unibill.Impl.PostParameter[] Unibill.Impl.HTTPClient/PostRequest::parameters
	PostParameterU5BU5D_t2629828016* ___parameters_1;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(PostRequest_t4132869030, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(PostRequest_t4132869030, ___parameters_1)); }
	inline PostParameterU5BU5D_t2629828016* get_parameters_1() const { return ___parameters_1; }
	inline PostParameterU5BU5D_t2629828016** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(PostParameterU5BU5D_t2629828016* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
