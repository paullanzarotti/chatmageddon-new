﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotEnoughBucksPU
struct NotEnoughBucksPU_t2637555884;

#include "codegen/il2cpp-codegen.h"

// System.Void NotEnoughBucksPU::.ctor()
extern "C"  void NotEnoughBucksPU__ctor_m1990263829 (NotEnoughBucksPU_t2637555884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotEnoughBucksPU::Start()
extern "C"  void NotEnoughBucksPU_Start_m4227167117 (NotEnoughBucksPU_t2637555884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotEnoughBucksPU::PopUptweenFinish()
extern "C"  void NotEnoughBucksPU_PopUptweenFinish_m4210845861 (NotEnoughBucksPU_t2637555884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotEnoughBucksPU::OpenPU(System.Int32)
extern "C"  void NotEnoughBucksPU_OpenPU_m2576738505 (NotEnoughBucksPU_t2637555884 * __this, int32_t ___bucksAmount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotEnoughBucksPU::ClosePU()
extern "C"  void NotEnoughBucksPU_ClosePU_m1704982288 (NotEnoughBucksPU_t2637555884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
