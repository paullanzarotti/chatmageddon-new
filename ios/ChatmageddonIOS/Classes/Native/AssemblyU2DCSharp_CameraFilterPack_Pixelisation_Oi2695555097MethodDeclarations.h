﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Pixelisation_OilPaint
struct CameraFilterPack_Pixelisation_OilPaint_t2695555097;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Pixelisation_OilPaint::.ctor()
extern "C"  void CameraFilterPack_Pixelisation_OilPaint__ctor_m758666204 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaint::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Pixelisation_OilPaint_get_material_m3746029993 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::Start()
extern "C"  void CameraFilterPack_Pixelisation_OilPaint_Start_m497163140 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Pixelisation_OilPaint_OnRenderImage_m860527124 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnValidate()
extern "C"  void CameraFilterPack_Pixelisation_OilPaint_OnValidate_m1590668351 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::Update()
extern "C"  void CameraFilterPack_Pixelisation_OilPaint_Update_m3711804453 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnDisable()
extern "C"  void CameraFilterPack_Pixelisation_OilPaint_OnDisable_m621085561 (CameraFilterPack_Pixelisation_OilPaint_t2695555097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
