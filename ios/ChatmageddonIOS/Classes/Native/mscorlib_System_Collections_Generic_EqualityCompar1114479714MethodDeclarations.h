﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Prime31.JsonFormatter/JsonContextType>
struct DefaultComparer_t1114479714;

#include "codegen/il2cpp-codegen.h"
#include "P31RestKit_Prime31_JsonFormatter_JsonContextType3787516849.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Prime31.JsonFormatter/JsonContextType>::.ctor()
extern "C"  void DefaultComparer__ctor_m4257794051_gshared (DefaultComparer_t1114479714 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4257794051(__this, method) ((  void (*) (DefaultComparer_t1114479714 *, const MethodInfo*))DefaultComparer__ctor_m4257794051_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Prime31.JsonFormatter/JsonContextType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m338981294_gshared (DefaultComparer_t1114479714 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m338981294(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1114479714 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m338981294_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Prime31.JsonFormatter/JsonContextType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1632094446_gshared (DefaultComparer_t1114479714 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1632094446(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1114479714 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1632094446_gshared)(__this, ___x0, ___y1, method)
