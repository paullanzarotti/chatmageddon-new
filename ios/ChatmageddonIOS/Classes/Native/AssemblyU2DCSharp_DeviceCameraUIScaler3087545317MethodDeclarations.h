﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceCameraUIScaler
struct DeviceCameraUIScaler_t3087545317;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceCameraUIScaler::.ctor()
extern "C"  void DeviceCameraUIScaler__ctor_m4037507950 (DeviceCameraUIScaler_t3087545317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraUIScaler::GlobalUIScale()
extern "C"  void DeviceCameraUIScaler_GlobalUIScale_m213407383 (DeviceCameraUIScaler_t3087545317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
