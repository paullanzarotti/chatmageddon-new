﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContactUsUI
struct ContactUsUI_t1917862594;

#include "codegen/il2cpp-codegen.h"

// System.Void ContactUsUI::.ctor()
extern "C"  void ContactUsUI__ctor_m3185766077 (ContactUsUI_t1917862594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactUsUI::LoadUI()
extern "C"  void ContactUsUI_LoadUI_m2220450023 (ContactUsUI_t1917862594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
