﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPForwardPickerEvents
struct IPForwardPickerEvents_t3125981953;
// IPForwardPickerEvents/GameObjectAndMessage
struct GameObjectAndMessage_t1917906877;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IPForwardPickerEvents_GameObject1917906877.h"

// System.Void IPForwardPickerEvents::.ctor()
extern "C"  void IPForwardPickerEvents__ctor_m3954111292 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::Start()
extern "C"  void IPForwardPickerEvents_Start_m1600962716 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::SetDelegates()
extern "C"  void IPForwardPickerEvents_SetDelegates_m1480027278 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnEnable()
extern "C"  void IPForwardPickerEvents_OnEnable_m1386909404 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnDisable()
extern "C"  void IPForwardPickerEvents_OnDisable_m3512109069 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnCyclerSelectionStarted()
extern "C"  void IPForwardPickerEvents_OnCyclerSelectionStarted_m4029795048 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnCenterOnChildStarted()
extern "C"  void IPForwardPickerEvents_OnCenterOnChildStarted_m4281266928 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnCyclerStopped()
extern "C"  void IPForwardPickerEvents_OnCyclerStopped_m245809866 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnPickerSelectionChange()
extern "C"  void IPForwardPickerEvents_OnPickerSelectionChange_m2726973597 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::OnDragExit()
extern "C"  void IPForwardPickerEvents_OnDragExit_m156371601 (IPForwardPickerEvents_t3125981953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPForwardPickerEvents::Notify(IPForwardPickerEvents/GameObjectAndMessage)
extern "C"  void IPForwardPickerEvents_Notify_m814758010 (IPForwardPickerEvents_t3125981953 * __this, GameObjectAndMessage_t1917906877 * ___goAndMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
