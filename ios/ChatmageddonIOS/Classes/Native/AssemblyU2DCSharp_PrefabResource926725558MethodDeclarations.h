﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrefabResource
struct PrefabResource_t926725558;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PrefabResource::.ctor()
extern "C"  void PrefabResource__ctor_m1495531207 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabResource::GetAttackPanelPrefab()
extern "C"  String_t* PrefabResource_GetAttackPanelPrefab_m13182312 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabResource::GetFriendsPanelPrefab()
extern "C"  String_t* PrefabResource_GetFriendsPanelPrefab_m2586046949 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabResource::GetStorePanelPrefab()
extern "C"  String_t* PrefabResource_GetStorePanelPrefab_m3915576911 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabResource::GetStatusPanelPrefab()
extern "C"  String_t* PrefabResource_GetStatusPanelPrefab_m55069298 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabResource::GetLeaderBoardPanelPrefab()
extern "C"  String_t* PrefabResource_GetLeaderBoardPanelPrefab_m1176076777 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabResource::GetPanelContainerPrefab()
extern "C"  String_t* PrefabResource_GetPanelContainerPrefab_m2157219847 (PrefabResource_t926725558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
