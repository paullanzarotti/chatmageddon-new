﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidget
struct UIWidget_t1453041918;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIWidget1453041918.h"

// System.Int32 IPTools::GetSelectedIndexAfterRemoveElement(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t IPTools_GetSelectedIndexAfterRemoveElement_m2238628307 (Il2CppObject * __this /* static, unused */, int32_t ___removedIndex0, int32_t ___selectedIndex1, int32_t ___nbOfElements2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTools::NormalizeWidget(UIWidget,System.Single)
extern "C"  void IPTools_NormalizeWidget_m4073617489 (Il2CppObject * __this /* static, unused */, UIWidget_t1453041918 * ___widget0, float ___normalizedMax1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
