﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegistrationManager
struct RegistrationManager_t1533517116;
// CountryCodeData
struct CountryCodeData_t1765037603;
// FacebookPlayer
struct FacebookPlayer_t2645999341;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CountryCodeData1765037603.h"
#include "AssemblyU2DCSharp_FacebookPlayer2645999341.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void RegistrationManager::.ctor()
extern "C"  void RegistrationManager__ctor_m1168536085 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::Awake()
extern "C"  void RegistrationManager_Awake_m1371457276 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::ClearPhoneNumberData()
extern "C"  void RegistrationManager_ClearPhoneNumberData_m587751007 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::SetInputsInactive()
extern "C"  void RegistrationManager_SetInputsInactive_m4082569441 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::ValidateFacebookAccount()
extern "C"  void RegistrationManager_ValidateFacebookAccount_m3163361558 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::CompleteRegistrationProcess()
extern "C"  void RegistrationManager_CompleteRegistrationProcess_m3837572400 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::SubmitExtraAccountInfo()
extern "C"  void RegistrationManager_SubmitExtraAccountInfo_m2943037594 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::SubmitProfileImage()
extern "C"  void RegistrationManager_SubmitProfileImage_m1734497161 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::AttachFacebook()
extern "C"  void RegistrationManager_AttachFacebook_m2571987354 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::RegUploadComplete()
extern "C"  void RegistrationManager_RegUploadComplete_m567244739 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::RegUploadFailed()
extern "C"  void RegistrationManager_RegUploadFailed_m526405625 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::RegTweenFinished()
extern "C"  void RegistrationManager_RegTweenFinished_m1832936924 (RegistrationManager_t1533517116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::SetSelectedCountryCode(CountryCodeData)
extern "C"  void RegistrationManager_SetSelectedCountryCode_m3766751442 (RegistrationManager_t1533517116 * __this, CountryCodeData_t1765037603 * ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::PopulateWithFBDetails(FacebookPlayer)
extern "C"  void RegistrationManager_PopulateWithFBDetails_m485676472 (RegistrationManager_t1533517116 * __this, FacebookPlayer_t2645999341 * ___fbPlayer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::PopulateProfileTexture(UnityEngine.Texture,System.String)
extern "C"  void RegistrationManager_PopulateProfileTexture_m2257907281 (RegistrationManager_t1533517116 * __this, Texture_t2243626319 * ___profileImage0, String_t* ___imageURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::<ValidateFacebookAccount>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void RegistrationManager_U3CValidateFacebookAccountU3Em__0_m2058173006 (RegistrationManager_t1533517116 * __this, bool ___attachSuccess0, Hashtable_t909839986 * ___info1, String_t* ___attachErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::<CompleteRegistrationProcess>m__1(System.Boolean,System.String)
extern "C"  void RegistrationManager_U3CCompleteRegistrationProcessU3Em__1_m1586465957 (RegistrationManager_t1533517116 * __this, bool ___regSuccess0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::<SubmitExtraAccountInfo>m__2(System.Boolean,System.String)
extern "C"  void RegistrationManager_U3CSubmitExtraAccountInfoU3Em__2_m2483696170 (RegistrationManager_t1533517116 * __this, bool ___submitSuccess0, String_t* ___submitErrorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::<SubmitProfileImage>m__3(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void RegistrationManager_U3CSubmitProfileImageU3Em__3_m1807484834 (RegistrationManager_t1533517116 * __this, bool ___imageSuccess0, Hashtable_t909839986 * ___info1, String_t* ___imagErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::<SubmitProfileImage>m__4(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void RegistrationManager_U3CSubmitProfileImageU3Em__4_m4079992123 (RegistrationManager_t1533517116 * __this, bool ___imageSuccess0, Hashtable_t909839986 * ___info1, String_t* ___imagErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationManager::<AttachFacebook>m__5(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void RegistrationManager_U3CAttachFacebookU3Em__5_m1675664821 (RegistrationManager_t1533517116 * __this, bool ___attachSuccess0, Hashtable_t909839986 * ___info1, String_t* ___imagErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
