﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPUserInteraction/<DelayedDragExit>c__Iterator0
struct U3CDelayedDragExitU3Ec__Iterator0_t2879712700;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IPUserInteraction/<DelayedDragExit>c__Iterator0::.ctor()
extern "C"  void U3CDelayedDragExitU3Ec__Iterator0__ctor_m1568389773 (U3CDelayedDragExitU3Ec__Iterator0_t2879712700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPUserInteraction/<DelayedDragExit>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayedDragExitU3Ec__Iterator0_MoveNext_m838995467 (U3CDelayedDragExitU3Ec__Iterator0_t2879712700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IPUserInteraction/<DelayedDragExit>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayedDragExitU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2512576599 (U3CDelayedDragExitU3Ec__Iterator0_t2879712700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IPUserInteraction/<DelayedDragExit>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayedDragExitU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4209250671 (U3CDelayedDragExitU3Ec__Iterator0_t2879712700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction/<DelayedDragExit>c__Iterator0::Dispose()
extern "C"  void U3CDelayedDragExitU3Ec__Iterator0_Dispose_m4156392902 (U3CDelayedDragExitU3Ec__Iterator0_t2879712700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction/<DelayedDragExit>c__Iterator0::Reset()
extern "C"  void U3CDelayedDragExitU3Ec__Iterator0_Reset_m1989486984 (U3CDelayedDragExitU3Ec__Iterator0_t2879712700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
