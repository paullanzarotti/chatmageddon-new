﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<HomeBottomTabController>::.ctor()
#define MonoSingleton_1__ctor_m709739315(__this, method) ((  void (*) (MonoSingleton_1_t1732909 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeBottomTabController>::Awake()
#define MonoSingleton_1_Awake_m1343172206(__this, method) ((  void (*) (MonoSingleton_1_t1732909 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<HomeBottomTabController>::get_Instance()
#define MonoSingleton_1_get_Instance_m2254375938(__this /* static, unused */, method) ((  HomeBottomTabController_t251067189 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<HomeBottomTabController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2657081456(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<HomeBottomTabController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m674772057(__this, method) ((  void (*) (MonoSingleton_1_t1732909 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeBottomTabController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m4169008545(__this, method) ((  void (*) (MonoSingleton_1_t1732909 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeBottomTabController>::.cctor()
#define MonoSingleton_1__cctor_m5061664(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
