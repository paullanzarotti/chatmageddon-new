﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen3446336302MethodDeclarations.h"

// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::.ctor()
#define NavigationController_2__ctor_m2488955403(__this, method) ((  void (*) (NavigationController_2_t65533235 *, const MethodInfo*))NavigationController_2__ctor_m1308199617_gshared)(__this, method)
// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m3430011481(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t65533235 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m45824959_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m1688197238(__this, method) ((  void (*) (NavigationController_2_t65533235 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1132032114_gshared)(__this, method)
// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3146261909(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t65533235 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3705288999_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m1582819445(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t65533235 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m2067710955_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m3694093960(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t65533235 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m3105950792_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m3331685693(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t65533235 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3049343775_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m1457971089(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t65533235 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2052677691_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<ContactsNaviagationController,PhoneNumberNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m4178524449(__this, ___screen0, method) ((  void (*) (NavigationController_2_t65533235 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m290611767_gshared)(__this, ___screen0, method)
