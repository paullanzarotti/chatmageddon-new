﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<DeleteFacebookAuth>c__AnonStoreyA<System.Object>
struct U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t3060134317;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<DeleteFacebookAuth>c__AnonStoreyA<System.Object>::.ctor()
extern "C"  void U3CDeleteFacebookAuthU3Ec__AnonStoreyA__ctor_m1969878208_gshared (U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t3060134317 * __this, const MethodInfo* method);
#define U3CDeleteFacebookAuthU3Ec__AnonStoreyA__ctor_m1969878208(__this, method) ((  void (*) (U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t3060134317 *, const MethodInfo*))U3CDeleteFacebookAuthU3Ec__AnonStoreyA__ctor_m1969878208_gshared)(__this, method)
// System.Void BaseServer`1/<DeleteFacebookAuth>c__AnonStoreyA<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeleteFacebookAuthU3Ec__AnonStoreyA_U3CU3Em__0_m2951211549_gshared (U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t3060134317 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeleteFacebookAuthU3Ec__AnonStoreyA_U3CU3Em__0_m2951211549(__this, ___request0, ___response1, method) ((  void (*) (U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t3060134317 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeleteFacebookAuthU3Ec__AnonStoreyA_U3CU3Em__0_m2951211549_gshared)(__this, ___request0, ___response1, method)
