﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeTabUIScaler
struct HomeTabUIScaler_t2633114472;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeTabUIScaler::.ctor()
extern "C"  void HomeTabUIScaler__ctor_m4106669453 (HomeTabUIScaler_t2633114472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeTabUIScaler::GlobalUIScale()
extern "C"  void HomeTabUIScaler_GlobalUIScale_m1586365482 (HomeTabUIScaler_t2633114472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
