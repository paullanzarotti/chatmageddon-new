﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<RegNavScreen>
struct List_1_t36294491;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3865991461.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"

// System.Void System.Collections.Generic.List`1/Enumerator<RegNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1060006658_gshared (Enumerator_t3865991461 * __this, List_1_t36294491 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1060006658(__this, ___l0, method) ((  void (*) (Enumerator_t3865991461 *, List_1_t36294491 *, const MethodInfo*))Enumerator__ctor_m1060006658_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RegNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3043061536_gshared (Enumerator_t3865991461 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3043061536(__this, method) ((  void (*) (Enumerator_t3865991461 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3043061536_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<RegNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m396403222_gshared (Enumerator_t3865991461 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m396403222(__this, method) ((  Il2CppObject * (*) (Enumerator_t3865991461 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m396403222_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RegNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m3259263787_gshared (Enumerator_t3865991461 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3259263787(__this, method) ((  void (*) (Enumerator_t3865991461 *, const MethodInfo*))Enumerator_Dispose_m3259263787_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RegNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2797203216_gshared (Enumerator_t3865991461 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2797203216(__this, method) ((  void (*) (Enumerator_t3865991461 *, const MethodInfo*))Enumerator_VerifyState_m2797203216_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<RegNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m552802672_gshared (Enumerator_t3865991461 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m552802672(__this, method) ((  bool (*) (Enumerator_t3865991461 *, const MethodInfo*))Enumerator_MoveNext_m552802672_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<RegNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2010523663_gshared (Enumerator_t3865991461 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2010523663(__this, method) ((  int32_t (*) (Enumerator_t3865991461 *, const MethodInfo*))Enumerator_get_Current_m2010523663_gshared)(__this, method)
