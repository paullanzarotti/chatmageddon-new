﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMRelation
struct OnlineMapsOSMRelation_t1871982797;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"

// System.Void OnlineMapsOSMRelation::.ctor(OnlineMapsXML)
extern "C"  void OnlineMapsOSMRelation__ctor_m2277826585 (OnlineMapsOSMRelation_t1871982797 * __this, OnlineMapsXML_t3341520387 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
