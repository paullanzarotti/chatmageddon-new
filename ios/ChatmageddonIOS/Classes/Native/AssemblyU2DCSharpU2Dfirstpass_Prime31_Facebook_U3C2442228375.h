﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Action`2<System.String,System.Object>
struct Action_2_t599803691;
// Prime31.Facebook
struct Facebook_t3852591386;

#include "mscorlib_System_Object2689449295.h"
#include "P31RestKit_Prime31_HTTPVerb1395544859.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook/<graphRequest>c__AnonStorey1
struct  U3CgraphRequestU3Ec__AnonStorey1_t2442228375  : public Il2CppObject
{
public:
	// System.String Prime31.Facebook/<graphRequest>c__AnonStorey1::path
	String_t* ___path_0;
	// Prime31.HTTPVerb Prime31.Facebook/<graphRequest>c__AnonStorey1::verb
	int32_t ___verb_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.Facebook/<graphRequest>c__AnonStorey1::parameters
	Dictionary_2_t309261261 * ___parameters_2;
	// System.Action`2<System.String,System.Object> Prime31.Facebook/<graphRequest>c__AnonStorey1::completionHandler
	Action_2_t599803691 * ___completionHandler_3;
	// Prime31.Facebook Prime31.Facebook/<graphRequest>c__AnonStorey1::$this
	Facebook_t3852591386 * ___U24this_4;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t2442228375, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_verb_1() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t2442228375, ___verb_1)); }
	inline int32_t get_verb_1() const { return ___verb_1; }
	inline int32_t* get_address_of_verb_1() { return &___verb_1; }
	inline void set_verb_1(int32_t value)
	{
		___verb_1 = value;
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t2442228375, ___parameters_2)); }
	inline Dictionary_2_t309261261 * get_parameters_2() const { return ___parameters_2; }
	inline Dictionary_2_t309261261 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(Dictionary_2_t309261261 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}

	inline static int32_t get_offset_of_completionHandler_3() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t2442228375, ___completionHandler_3)); }
	inline Action_2_t599803691 * get_completionHandler_3() const { return ___completionHandler_3; }
	inline Action_2_t599803691 ** get_address_of_completionHandler_3() { return &___completionHandler_3; }
	inline void set_completionHandler_3(Action_2_t599803691 * value)
	{
		___completionHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___completionHandler_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t2442228375, ___U24this_4)); }
	inline Facebook_t3852591386 * get_U24this_4() const { return ___U24this_4; }
	inline Facebook_t3852591386 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Facebook_t3852591386 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
