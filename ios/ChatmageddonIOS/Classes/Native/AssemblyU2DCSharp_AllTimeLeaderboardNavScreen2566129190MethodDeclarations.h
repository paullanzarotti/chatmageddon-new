﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllTimeLeaderboardNavScreen
struct AllTimeLeaderboardNavScreen_t2566129190;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AllTimeLeaderboardNavScreen::.ctor()
extern "C"  void AllTimeLeaderboardNavScreen__ctor_m3096275171 (AllTimeLeaderboardNavScreen_t2566129190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllTimeLeaderboardNavScreen::Start()
extern "C"  void AllTimeLeaderboardNavScreen_Start_m1208521687 (AllTimeLeaderboardNavScreen_t2566129190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllTimeLeaderboardNavScreen::UIClosing()
extern "C"  void AllTimeLeaderboardNavScreen_UIClosing_m2821471628 (AllTimeLeaderboardNavScreen_t2566129190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllTimeLeaderboardNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AllTimeLeaderboardNavScreen_ScreenLoad_m2629847349 (AllTimeLeaderboardNavScreen_t2566129190 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllTimeLeaderboardNavScreen::UpdateUI()
extern "C"  void AllTimeLeaderboardNavScreen_UpdateUI_m1645172998 (AllTimeLeaderboardNavScreen_t2566129190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllTimeLeaderboardNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AllTimeLeaderboardNavScreen_ScreenUnload_m1336762793 (AllTimeLeaderboardNavScreen_t2566129190 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AllTimeLeaderboardNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AllTimeLeaderboardNavScreen_ValidateScreenNavigation_m1897784462 (AllTimeLeaderboardNavScreen_t2566129190 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
