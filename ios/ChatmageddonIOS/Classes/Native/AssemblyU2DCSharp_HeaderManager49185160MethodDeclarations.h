﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeaderManager
struct HeaderManager_t49185160;

#include "codegen/il2cpp-codegen.h"

// System.Void HeaderManager::.ctor()
extern "C"  void HeaderManager__ctor_m2595179819 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::UpdateHeaderBar()
extern "C"  void HeaderManager_UpdateHeaderBar_m843624554 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::PlayTweenPosition(System.Boolean)
extern "C"  void HeaderManager_PlayTweenPosition_m2154914320 (HeaderManager_t49185160 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::SetAvatar()
extern "C"  void HeaderManager_SetAvatar_m1608946114 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::SetDailyPointsLabel()
extern "C"  void HeaderManager_SetDailyPointsLabel_m4255293245 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::SetBucksLabel()
extern "C"  void HeaderManager_SetBucksLabel_m4159429163 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::SetFuelBar()
extern "C"  void HeaderManager_SetFuelBar_m3291002710 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::SetShieldBar()
extern "C"  void HeaderManager_SetShieldBar_m2077645363 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeaderManager::SetUnreadChatMessages()
extern "C"  void HeaderManager_SetUnreadChatMessages_m3175748322 (HeaderManager_t49185160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
