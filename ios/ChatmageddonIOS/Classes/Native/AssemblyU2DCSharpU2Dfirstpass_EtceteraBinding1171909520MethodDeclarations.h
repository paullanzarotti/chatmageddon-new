﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraBinding
struct EtceteraBinding_t1171909520;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Action`1<UnityEngine.WWW>
struct Action_1_t2721744421;
// System.Collections.Generic.List`1<Contact>
struct List_1_t3383317540;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PhotoPromptType3400315462.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharpU2Dfirstpass_P31RemoteNotificatio2463552857.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UIInterfaceOrientatio177348777.h"

// System.Void EtceteraBinding::.ctor()
extern "C"  void EtceteraBinding__ctor_m518297969 (EtceteraBinding_t1171909520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraBinding::takeScreenShot(System.String)
extern "C"  Il2CppObject * EtceteraBinding_takeScreenShot_m3397567952 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraBinding::takeScreenShot(System.String,System.Action`1<System.String>)
extern "C"  Il2CppObject * EtceteraBinding_takeScreenShot_m209130868 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, Action_1_t1831019615 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraBinding::getScreenShotTexture(System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * EtceteraBinding_getScreenShotTexture_m1409365844 (Il2CppObject * __this /* static, unused */, Action_1_t3344795111 * ___completionHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding::_etceteraApplicationCanOpenUrl(System.String)
extern "C"  bool EtceteraBinding__etceteraApplicationCanOpenUrl_m4028991694 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding::applicationCanOpenUrl(System.String)
extern "C"  bool EtceteraBinding_applicationCanOpenUrl_m1458600124 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::_etceteraGetPasteboardString()
extern "C"  String_t* EtceteraBinding__etceteraGetPasteboardString_m4040076096 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::getPasteboardString()
extern "C"  String_t* EtceteraBinding_getPasteboardString_m3595913152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraSetPasteboardString(System.String)
extern "C"  void EtceteraBinding__etceteraSetPasteboardString_m155693659 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setPasteboardString(System.String)
extern "C"  void EtceteraBinding_setPasteboardString_m3967849549 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraSetPasteboardImage(System.Byte[],System.Int32)
extern "C"  void EtceteraBinding__etceteraSetPasteboardImage_m3087816549 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setPasteboardImage(System.Byte[])
extern "C"  void EtceteraBinding_setPasteboardImage_m1555412090 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___imageBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::_etceteraGetCurrentLanguage()
extern "C"  String_t* EtceteraBinding__etceteraGetCurrentLanguage_m2542025059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::getCurrentLanguage()
extern "C"  String_t* EtceteraBinding_getCurrentLanguage_m120589889 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::_etceteraLocaleObjectForKey(System.Boolean,System.String)
extern "C"  String_t* EtceteraBinding__etceteraLocaleObjectForKey_m908494516 (Il2CppObject * __this /* static, unused */, bool ___useAutoupdatingLocale0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::localeObjectForKey(System.Boolean,System.String)
extern "C"  String_t* EtceteraBinding_localeObjectForKey_m259909820 (Il2CppObject * __this /* static, unused */, bool ___useAutoUpdatingLocale0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::_etceteraGetLocalizedString(System.String,System.String)
extern "C"  String_t* EtceteraBinding__etceteraGetLocalizedString_m3690286744 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::getLocalizedString(System.String,System.String)
extern "C"  String_t* EtceteraBinding_getLocalizedString_m779513744 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showAlertWithTitleMessageAndButton(System.String,System.String,System.String)
extern "C"  void EtceteraBinding_showAlertWithTitleMessageAndButton_m3004641110 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___buttonTitle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowAlertWithTitleMessageAndButtons(System.String,System.String,System.String)
extern "C"  void EtceteraBinding__etceteraShowAlertWithTitleMessageAndButtons_m2204183675 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___buttons2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showAlertWithTitleMessageAndButtons(System.String,System.String,System.String[])
extern "C"  void EtceteraBinding_showAlertWithTitleMessageAndButtons_m793146183 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, StringU5BU5D_t1642385972* ___buttons2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowPromptWithOneField(System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding__etceteraShowPromptWithOneField_m2041860517 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___placeHolder2, bool ___autocomplete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showPromptWithOneField(System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding_showPromptWithOneField_m3207850899 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___placeHolder2, bool ___autocomplete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowPromptWithTwoFields(System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding__etceteraShowPromptWithTwoFields_m865510904 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___placeHolder12, String_t* ___placeHolder23, bool ___autocomplete4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showPromptWithTwoFields(System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding_showPromptWithTwoFields_m728729534 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___placeHolder12, String_t* ___placeHolder23, bool ___autocomplete4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowWebPage(System.String,System.Boolean)
extern "C"  void EtceteraBinding__etceteraShowWebPage_m3074973900 (Il2CppObject * __this /* static, unused */, String_t* ___url0, bool ___showControls1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showWebPage(System.String,System.Boolean)
extern "C"  void EtceteraBinding_showWebPage_m2527442638 (Il2CppObject * __this /* static, unused */, String_t* ___url0, bool ___showControls1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding::_etceteraIsEmailAvailable()
extern "C"  bool EtceteraBinding__etceteraIsEmailAvailable_m907133354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding::isEmailAvailable()
extern "C"  bool EtceteraBinding_isEmailAvailable_m1718721180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding::_etceteraIsSMSAvailable()
extern "C"  bool EtceteraBinding__etceteraIsSMSAvailable_m1778113625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding::isSMSAvailable()
extern "C"  bool EtceteraBinding_isSMSAvailable_m3639636087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowMailComposer(System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding__etceteraShowMailComposer_m1486797654 (Il2CppObject * __this /* static, unused */, String_t* ___toAddress0, String_t* ___subject1, String_t* ___body2, bool ___isHTML3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showMailComposer(System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding_showMailComposer_m2445638024 (Il2CppObject * __this /* static, unused */, String_t* ___toAddress0, String_t* ___subject1, String_t* ___body2, bool ___isHTML3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraBinding::showMailComposerWithScreenshot(System.String,System.String,System.String,System.Boolean)
extern "C"  Il2CppObject * EtceteraBinding_showMailComposerWithScreenshot_m41992448 (Il2CppObject * __this /* static, unused */, String_t* ___toAddress0, String_t* ___subject1, String_t* ___body2, bool ___isHTML3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showMailComposerWithAttachment(System.String,System.String,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding_showMailComposerWithAttachment_m3328592101 (Il2CppObject * __this /* static, unused */, String_t* ___filePathToAttachment0, String_t* ___attachmentMimeType1, String_t* ___attachmentFilename2, String_t* ___toAddress3, String_t* ___subject4, String_t* ___body5, bool ___isHTML6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowMailComposerWithRawAttachment(System.Byte[],System.Int32,System.String,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding__etceteraShowMailComposerWithRawAttachment_m3911210723 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___length1, String_t* ___attachmentMimeType2, String_t* ___attachmentFilename3, String_t* ___toAddress4, String_t* ___subject5, String_t* ___body6, bool ___isHTML7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showMailComposerWithAttachment(System.Byte[],System.String,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void EtceteraBinding_showMailComposerWithAttachment_m1071747170 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___attachmentData0, String_t* ___attachmentMimeType1, String_t* ___attachmentFilename2, String_t* ___toAddress3, String_t* ___subject4, String_t* ___body5, bool ___isHTML6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowSMSComposer(System.String,System.String)
extern "C"  void EtceteraBinding__etceteraShowSMSComposer_m2452471931 (Il2CppObject * __this /* static, unused */, String_t* ___recipients0, String_t* ___body1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showSMSComposer(System.String)
extern "C"  void EtceteraBinding_showSMSComposer_m284760827 (Il2CppObject * __this /* static, unused */, String_t* ___body0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showSMSComposer(System.String[],System.String)
extern "C"  void EtceteraBinding_showSMSComposer_m4156708415 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___recipients0, String_t* ___body1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowActivityView()
extern "C"  void EtceteraBinding__etceteraShowActivityView_m1946340554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showActivityView()
extern "C"  void EtceteraBinding_showActivityView_m2673006064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraHideActivityView()
extern "C"  void EtceteraBinding__etceteraHideActivityView_m3353406203 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::hideActivityView()
extern "C"  void EtceteraBinding_hideActivityView_m3794296657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowBezelActivityViewWithLabel(System.String)
extern "C"  void EtceteraBinding__etceteraShowBezelActivityViewWithLabel_m993126510 (Il2CppObject * __this /* static, unused */, String_t* ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showBezelActivityViewWithLabel(System.String)
extern "C"  void EtceteraBinding_showBezelActivityViewWithLabel_m4083265260 (Il2CppObject * __this /* static, unused */, String_t* ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraShowBezelActivityViewWithImage(System.String,System.String)
extern "C"  void EtceteraBinding__etceteraShowBezelActivityViewWithImage_m2530115253 (Il2CppObject * __this /* static, unused */, String_t* ___label0, String_t* ___imagePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::showBezelActivityViewWithImage(System.String,System.String)
extern "C"  void EtceteraBinding_showBezelActivityViewWithImage_m362323507 (Il2CppObject * __this /* static, unused */, String_t* ___label0, String_t* ___imagePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraAskForReview(System.Int32,System.Single,System.String,System.String,System.String)
extern "C"  void EtceteraBinding__etceteraAskForReview_m3768835893 (Il2CppObject * __this /* static, unused */, int32_t ___launchCount0, float ___hoursBetweenPrompts1, String_t* ___title2, String_t* ___message3, String_t* ___iTunesAppId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::askForReview(System.Int32,System.Single,System.String,System.String,System.String)
extern "C"  void EtceteraBinding_askForReview_m2204465667 (Il2CppObject * __this /* static, unused */, int32_t ___launchCount0, float ___hoursBetweenPrompts1, String_t* ___title2, String_t* ___message3, String_t* ___iTunesAppId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraAskForReviewImmediately(System.String,System.String,System.String)
extern "C"  void EtceteraBinding__etceteraAskForReviewImmediately_m1752802693 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___iTunesAppId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::askForReview(System.String,System.String,System.String)
extern "C"  void EtceteraBinding_askForReview_m1052656155 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___iTunesAppId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraOpenAppStoreReviewPage(System.String)
extern "C"  void EtceteraBinding__etceteraOpenAppStoreReviewPage_m2980695638 (Il2CppObject * __this /* static, unused */, String_t* ___iTunesAppId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::openAppStoreReviewPage(System.String)
extern "C"  void EtceteraBinding_openAppStoreReviewPage_m3688757000 (Il2CppObject * __this /* static, unused */, String_t* ___iTunesAppId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraSetPopoverPoint(System.Single,System.Single)
extern "C"  void EtceteraBinding__etceteraSetPopoverPoint_m2535932496 (Il2CppObject * __this /* static, unused */, float ___xPos0, float ___yPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setPopoverPoint(System.Single,System.Single)
extern "C"  void EtceteraBinding_setPopoverPoint_m3619919082 (Il2CppObject * __this /* static, unused */, float ___xPos0, float ___yPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraPromptForPhoto(System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  void EtceteraBinding__etceteraPromptForPhoto_m31218890 (Il2CppObject * __this /* static, unused */, float ___scaledToSize0, int32_t ___promptType1, float ___jpegCompression2, bool ___allowsEditing3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::promptForPhoto(System.Single)
extern "C"  void EtceteraBinding_promptForPhoto_m2330684603 (Il2CppObject * __this /* static, unused */, float ___scaledToSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::promptForPhoto(System.Single,PhotoPromptType)
extern "C"  void EtceteraBinding_promptForPhoto_m2056950635 (Il2CppObject * __this /* static, unused */, float ___scaledToSize0, int32_t ___promptType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::promptForPhoto(System.Single,PhotoPromptType,System.Single,System.Boolean)
extern "C"  void EtceteraBinding_promptForPhoto_m3327410821 (Il2CppObject * __this /* static, unused */, float ___scaledToSize0, int32_t ___promptType1, float ___jpegCompression2, bool ___allowsEditing3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraPromptForMultiplePhotos(System.Int32,System.Single,System.Single)
extern "C"  void EtceteraBinding__etceteraPromptForMultiplePhotos_m862311858 (Il2CppObject * __this /* static, unused */, int32_t ___maxNumberOfPhotos0, float ___scaledToSize1, float ___jpegCompression2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::promptForMultiplePhotos(System.Int32,System.Single,System.Single)
extern "C"  void EtceteraBinding_promptForMultiplePhotos_m1499308336 (Il2CppObject * __this /* static, unused */, int32_t ___maxNumberOfPhotos0, float ___scaledToSize1, float ___jpegCompression2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraResizeImageAtPath(System.String,System.Single,System.Single)
extern "C"  void EtceteraBinding__etceteraResizeImageAtPath_m4060497518 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, float ___width1, float ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::resizeImageAtPath(System.String,System.Single,System.Single)
extern "C"  void EtceteraBinding_resizeImageAtPath_m1815465064 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, float ___width1, float ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::_etceteraGetImageSize(System.String)
extern "C"  String_t* EtceteraBinding__etceteraGetImageSize_m2574680232 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 EtceteraBinding::getImageSize(System.String)
extern "C"  Vector2_t2243707579  EtceteraBinding_getImageSize_m368249462 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraSaveImageToPhotoAlbum(System.String)
extern "C"  void EtceteraBinding__etceteraSaveImageToPhotoAlbum_m1740908363 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::saveImageToPhotoAlbum(System.String)
extern "C"  void EtceteraBinding_saveImageToPhotoAlbum_m1356385617 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraSetUrbanAirshipCredentials(System.String,System.String,System.String)
extern "C"  void EtceteraBinding__etceteraSetUrbanAirshipCredentials_m644258297 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___appSecret1, String_t* ___alias2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setUrbanAirshipCredentials(System.String,System.String)
extern "C"  void EtceteraBinding_setUrbanAirshipCredentials_m888798757 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___appSecret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setUrbanAirshipCredentials(System.String,System.String,System.String)
extern "C"  void EtceteraBinding_setUrbanAirshipCredentials_m3892589347 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___appSecret1, String_t* ___alias2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setPushIOCredentials(System.String)
extern "C"  void EtceteraBinding_setPushIOCredentials_m3442354529 (Il2CppObject * __this /* static, unused */, String_t* ___apiKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setPushIOCredentials(System.String,System.String[])
extern "C"  void EtceteraBinding_setPushIOCredentials_m2805211477 (Il2CppObject * __this /* static, unused */, String_t* ___apiKey0, StringU5BU5D_t1642385972* ___categories1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraRegisterForRemoteNotifications(System.Int32)
extern "C"  void EtceteraBinding__etceteraRegisterForRemoteNotifications_m3515830160 (Il2CppObject * __this /* static, unused */, int32_t ___types0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::registerForRemoteNotifcations(P31RemoteNotificationType)
extern "C"  void EtceteraBinding_registerForRemoteNotifcations_m4004721871 (Il2CppObject * __this /* static, unused */, int32_t ___types0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EtceteraBinding::registerDeviceWithGameThrive(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Action`1<UnityEngine.WWW>)
extern "C"  Il2CppObject * EtceteraBinding_registerDeviceWithGameThrive_m4091048347 (Il2CppObject * __this /* static, unused */, String_t* ___gameThriveAppId0, String_t* ___deviceToken1, Dictionary_2_t3943999495 * ___additionalParameters2, Action_1_t2721744421 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EtceteraBinding::_etceteraGetEnabledRemoteNotificationTypes()
extern "C"  int32_t EtceteraBinding__etceteraGetEnabledRemoteNotificationTypes_m1094596124 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// P31RemoteNotificationType EtceteraBinding::getEnabledRemoteNotificationTypes()
extern "C"  int32_t EtceteraBinding_getEnabledRemoteNotificationTypes_m1141018302 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EtceteraBinding::_etceteraGetBadgeCount()
extern "C"  int32_t EtceteraBinding__etceteraGetBadgeCount_m3653768081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EtceteraBinding::getBadgeCount()
extern "C"  int32_t EtceteraBinding_getBadgeCount_m2963559943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraSetBadgeCount(System.Int32)
extern "C"  void EtceteraBinding__etceteraSetBadgeCount_m3134969324 (Il2CppObject * __this /* static, unused */, int32_t ___badgeCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::setBadgeCount(System.Int32)
extern "C"  void EtceteraBinding_setBadgeCount_m1396226734 (Il2CppObject * __this /* static, unused */, int32_t ___badgeCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EtceteraBinding::_etceteraGetStatusBarOrientation()
extern "C"  int32_t EtceteraBinding__etceteraGetStatusBarOrientation_m3625285502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIInterfaceOrientation EtceteraBinding::getStatusBarOrientation()
extern "C"  int32_t EtceteraBinding_getStatusBarOrientation_m1864117958 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraInlineWebViewShow(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void EtceteraBinding__etceteraInlineWebViewShow_m555304632 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::inlineWebViewShow(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void EtceteraBinding_inlineWebViewShow_m1655293218 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraInlineWebViewClose()
extern "C"  void EtceteraBinding__etceteraInlineWebViewClose_m200997045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::inlineWebViewClose()
extern "C"  void EtceteraBinding_inlineWebViewClose_m2127107259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraInlineWebViewSetUrl(System.String)
extern "C"  void EtceteraBinding__etceteraInlineWebViewSetUrl_m2002127100 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::inlineWebViewSetUrl(System.String)
extern "C"  void EtceteraBinding_inlineWebViewSetUrl_m3975237074 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::_etceteraInlineWebViewSetFrame(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void EtceteraBinding__etceteraInlineWebViewSetFrame_m3893186396 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding::inlineWebViewSetFrame(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void EtceteraBinding_inlineWebViewSetFrame_m3436341526 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EtceteraBinding::_etceteraGetContacts(System.Int32,System.Int64)
extern "C"  String_t* EtceteraBinding__etceteraGetContacts_m3748266038 (Il2CppObject * __this /* static, unused */, int32_t ___startIndex0, int64_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Contact> EtceteraBinding::getContacts(System.Int32,System.Int64)
extern "C"  List_1_t3383317540 * EtceteraBinding_getContacts_m563905264 (Il2CppObject * __this /* static, unused */, int32_t ___startIndex0, int64_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
