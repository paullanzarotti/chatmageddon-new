﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherFriendListItemUIScaler
struct OtherFriendListItemUIScaler_t173824305;

#include "codegen/il2cpp-codegen.h"

// System.Void OtherFriendListItemUIScaler::.ctor()
extern "C"  void OtherFriendListItemUIScaler__ctor_m89585446 (OtherFriendListItemUIScaler_t173824305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherFriendListItemUIScaler::GlobalUIScale()
extern "C"  void OtherFriendListItemUIScaler_GlobalUIScale_m1658127299 (OtherFriendListItemUIScaler_t173824305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
