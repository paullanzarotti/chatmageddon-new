﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>
struct ValueCollection_t1966465002;
// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t1301098545;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va654970627.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m923162825_gshared (ValueCollection_t1966465002 * __this, Dictionary_2_t3263405159 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m923162825(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1966465002 *, Dictionary_2_t3263405159 *, const MethodInfo*))ValueCollection__ctor_m923162825_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4273947287_gshared (ValueCollection_t1966465002 * __this, bool ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4273947287(__this, ___item0, method) ((  void (*) (ValueCollection_t1966465002 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4273947287_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1459909036_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1459909036(__this, method) ((  void (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1459909036_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m82800705_gshared (ValueCollection_t1966465002 * __this, bool ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m82800705(__this, ___item0, method) ((  bool (*) (ValueCollection_t1966465002 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m82800705_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2969003502_gshared (ValueCollection_t1966465002 * __this, bool ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2969003502(__this, ___item0, method) ((  bool (*) (ValueCollection_t1966465002 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2969003502_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2549915424_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2549915424(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2549915424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m465086696_gshared (ValueCollection_t1966465002 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m465086696(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1966465002 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m465086696_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1187123591_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1187123591(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1187123591_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19603714_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19603714(__this, method) ((  bool (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19603714_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1367732684_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1367732684(__this, method) ((  bool (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1367732684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m123087336_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m123087336(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m123087336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1960725200_gshared (ValueCollection_t1966465002 * __this, BooleanU5BU5D_t3568034315* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1960725200(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1966465002 *, BooleanU5BU5D_t3568034315*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1960725200_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t654970627  ValueCollection_GetEnumerator_m3842081365_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3842081365(__this, method) ((  Enumerator_t654970627  (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_GetEnumerator_m3842081365_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Vectrosity.Vector3Pair,System.Boolean>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3577472012_gshared (ValueCollection_t1966465002 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3577472012(__this, method) ((  int32_t (*) (ValueCollection_t1966465002 *, const MethodInfo*))ValueCollection_get_Count_m3577472012_gshared)(__this, method)
