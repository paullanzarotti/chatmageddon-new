﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<FeedManager>::.ctor()
#define MonoSingleton_1__ctor_m495273361(__this, method) ((  void (*) (MonoSingleton_1_t2460531543 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<FeedManager>::Awake()
#define MonoSingleton_1_Awake_m2902278470(__this, method) ((  void (*) (MonoSingleton_1_t2460531543 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<FeedManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2697228158(__this /* static, unused */, method) ((  FeedManager_t2709865823 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<FeedManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1659205500(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<FeedManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m4189152199(__this, method) ((  void (*) (MonoSingleton_1_t2460531543 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<FeedManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3877967635(__this, method) ((  void (*) (MonoSingleton_1_t2460531543 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<FeedManager>::.cctor()
#define MonoSingleton_1__cctor_m3755032912(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
