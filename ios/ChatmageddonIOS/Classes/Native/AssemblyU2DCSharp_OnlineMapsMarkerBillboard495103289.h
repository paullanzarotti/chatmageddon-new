﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_OnlineMapsMarkerInstanceBase538187336.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsMarkerBillboard
struct  OnlineMapsMarkerBillboard_t495103289  : public OnlineMapsMarkerInstanceBase_t538187336
{
public:
	// System.Boolean OnlineMapsMarkerBillboard::used
	bool ___used_3;

public:
	inline static int32_t get_offset_of_used_3() { return static_cast<int32_t>(offsetof(OnlineMapsMarkerBillboard_t495103289, ___used_3)); }
	inline bool get_used_3() const { return ___used_3; }
	inline bool* get_address_of_used_3() { return &___used_3; }
	inline void set_used_3(bool value)
	{
		___used_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
