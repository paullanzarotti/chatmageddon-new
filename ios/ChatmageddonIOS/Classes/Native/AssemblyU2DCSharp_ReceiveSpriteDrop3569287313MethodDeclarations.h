﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReceiveSpriteDrop
struct ReceiveSpriteDrop_t3569287313;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ReceiveSpriteDrop::.ctor()
extern "C"  void ReceiveSpriteDrop__ctor_m3709358348 (ReceiveSpriteDrop_t3569287313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReceiveSpriteDrop::OnSpriteDrop(System.String)
extern "C"  void ReceiveSpriteDrop_OnSpriteDrop_m201312767 (ReceiveSpriteDrop_t3569287313 * __this, String_t* ___spriteName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
