﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m647840311(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2347980832 *, Dictionary_2_t1027956130 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3437489912(__this, method) ((  Il2CppObject * (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4037118270(__this, method) ((  void (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2224327881(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2802618534(__this, method) ((  Il2CppObject * (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2359320252(__this, method) ((  Il2CppObject * (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::MoveNext()
#define Enumerator_MoveNext_m2323007917(__this, method) ((  bool (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::get_Current()
#define Enumerator_get_Current_m2613451352(__this, method) ((  KeyValuePair_2_t3080268648  (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2106465571(__this, method) ((  String_t* (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3276264011(__this, method) ((  Feed_t3408144164 * (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::Reset()
#define Enumerator_Reset_m3018142145(__this, method) ((  void (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::VerifyState()
#define Enumerator_VerifyState_m2723329230(__this, method) ((  void (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3726714542(__this, method) ((  void (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Feed>::Dispose()
#define Enumerator_Dispose_m3766794915(__this, method) ((  void (*) (Enumerator_t2347980832 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
