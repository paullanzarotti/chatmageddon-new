﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<SettingsNavScreen>
struct DefaultComparer_t756397467;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<SettingsNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2051679532_gshared (DefaultComparer_t756397467 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2051679532(__this, method) ((  void (*) (DefaultComparer_t756397467 *, const MethodInfo*))DefaultComparer__ctor_m2051679532_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<SettingsNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3886451061_gshared (DefaultComparer_t756397467 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3886451061(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t756397467 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m3886451061_gshared)(__this, ___x0, ___y1, method)
