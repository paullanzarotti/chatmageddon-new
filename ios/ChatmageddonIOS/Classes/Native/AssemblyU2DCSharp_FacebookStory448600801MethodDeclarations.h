﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookStory
struct FacebookStory_t448600801;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FacebookStory::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void FacebookStory__ctor_m4104264714 (FacebookStory_t448600801 * __this, String_t* ___ToID0, String_t* ___LinkURL1, String_t* ___LinkName2, String_t* ___LinkCaption3, String_t* ___LinkDescription4, String_t* ___LinkImageURL5, String_t* ___LinkMediaSource6, String_t* ___ActionLinkURL7, String_t* ___ActionLinkName8, String_t* ___LinkReference9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
