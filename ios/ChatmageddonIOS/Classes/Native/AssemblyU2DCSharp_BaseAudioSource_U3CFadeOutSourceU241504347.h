﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BaseAudioSource
struct BaseAudioSource_t2241787848;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseAudioSource/<FadeOutSource>c__Iterator2
struct  U3CFadeOutSourceU3Ec__Iterator2_t241504347  : public Il2CppObject
{
public:
	// System.Boolean BaseAudioSource/<FadeOutSource>c__Iterator2::<updateFade>__0
	bool ___U3CupdateFadeU3E__0_0;
	// System.Single BaseAudioSource/<FadeOutSource>c__Iterator2::fadeDuration
	float ___fadeDuration_1;
	// System.Single BaseAudioSource/<FadeOutSource>c__Iterator2::<durationFraction>__1
	float ___U3CdurationFractionU3E__1_2;
	// System.Int32 BaseAudioSource/<FadeOutSource>c__Iterator2::<count>__2
	int32_t ___U3CcountU3E__2_3;
	// System.Single BaseAudioSource/<FadeOutSource>c__Iterator2::<volumeAddition>__3
	float ___U3CvolumeAdditionU3E__3_4;
	// System.Boolean BaseAudioSource/<FadeOutSource>c__Iterator2::pause
	bool ___pause_5;
	// BaseAudioSource BaseAudioSource/<FadeOutSource>c__Iterator2::$this
	BaseAudioSource_t2241787848 * ___U24this_6;
	// System.Object BaseAudioSource/<FadeOutSource>c__Iterator2::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean BaseAudioSource/<FadeOutSource>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 BaseAudioSource/<FadeOutSource>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CupdateFadeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U3CupdateFadeU3E__0_0)); }
	inline bool get_U3CupdateFadeU3E__0_0() const { return ___U3CupdateFadeU3E__0_0; }
	inline bool* get_address_of_U3CupdateFadeU3E__0_0() { return &___U3CupdateFadeU3E__0_0; }
	inline void set_U3CupdateFadeU3E__0_0(bool value)
	{
		___U3CupdateFadeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_fadeDuration_1() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___fadeDuration_1)); }
	inline float get_fadeDuration_1() const { return ___fadeDuration_1; }
	inline float* get_address_of_fadeDuration_1() { return &___fadeDuration_1; }
	inline void set_fadeDuration_1(float value)
	{
		___fadeDuration_1 = value;
	}

	inline static int32_t get_offset_of_U3CdurationFractionU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U3CdurationFractionU3E__1_2)); }
	inline float get_U3CdurationFractionU3E__1_2() const { return ___U3CdurationFractionU3E__1_2; }
	inline float* get_address_of_U3CdurationFractionU3E__1_2() { return &___U3CdurationFractionU3E__1_2; }
	inline void set_U3CdurationFractionU3E__1_2(float value)
	{
		___U3CdurationFractionU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E__2_3() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U3CcountU3E__2_3)); }
	inline int32_t get_U3CcountU3E__2_3() const { return ___U3CcountU3E__2_3; }
	inline int32_t* get_address_of_U3CcountU3E__2_3() { return &___U3CcountU3E__2_3; }
	inline void set_U3CcountU3E__2_3(int32_t value)
	{
		___U3CcountU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeAdditionU3E__3_4() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U3CvolumeAdditionU3E__3_4)); }
	inline float get_U3CvolumeAdditionU3E__3_4() const { return ___U3CvolumeAdditionU3E__3_4; }
	inline float* get_address_of_U3CvolumeAdditionU3E__3_4() { return &___U3CvolumeAdditionU3E__3_4; }
	inline void set_U3CvolumeAdditionU3E__3_4(float value)
	{
		___U3CvolumeAdditionU3E__3_4 = value;
	}

	inline static int32_t get_offset_of_pause_5() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___pause_5)); }
	inline bool get_pause_5() const { return ___pause_5; }
	inline bool* get_address_of_pause_5() { return &___pause_5; }
	inline void set_pause_5(bool value)
	{
		___pause_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U24this_6)); }
	inline BaseAudioSource_t2241787848 * get_U24this_6() const { return ___U24this_6; }
	inline BaseAudioSource_t2241787848 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(BaseAudioSource_t2241787848 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CFadeOutSourceU3Ec__Iterator2_t241504347, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
