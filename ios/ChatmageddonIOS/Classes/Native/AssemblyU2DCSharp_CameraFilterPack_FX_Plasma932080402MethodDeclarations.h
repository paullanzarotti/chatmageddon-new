﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Plasma
struct CameraFilterPack_FX_Plasma_t932080402;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Plasma::.ctor()
extern "C"  void CameraFilterPack_FX_Plasma__ctor_m2432149603 (CameraFilterPack_FX_Plasma_t932080402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Plasma::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Plasma_get_material_m134344492 (CameraFilterPack_FX_Plasma_t932080402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::Start()
extern "C"  void CameraFilterPack_FX_Plasma_Start_m3588803103 (CameraFilterPack_FX_Plasma_t932080402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Plasma_OnRenderImage_m2795650415 (CameraFilterPack_FX_Plasma_t932080402 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::OnValidate()
extern "C"  void CameraFilterPack_FX_Plasma_OnValidate_m1763270806 (CameraFilterPack_FX_Plasma_t932080402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::Update()
extern "C"  void CameraFilterPack_FX_Plasma_Update_m2958352156 (CameraFilterPack_FX_Plasma_t932080402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::OnDisable()
extern "C"  void CameraFilterPack_FX_Plasma_OnDisable_m1603572738 (CameraFilterPack_FX_Plasma_t932080402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
