﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.BuildMetadataFromXml
struct BuildMetadataFromXml_t3731095708;
// PhoneNumbers.PhoneMetadataCollection
struct PhoneMetadataCollection_t4114095021;
// System.IO.Stream
struct Stream_t3255436806;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t406167000;
// System.String
struct String_t;
// System.Xml.Linq.XElement
struct XElement_t553821050;
// PhoneNumbers.PhoneMetadata/Builder
struct Builder_t1569388123;
// PhoneNumbers.NumberFormat/Builder
struct Builder_t3378404562;
// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadataCollec4114095021.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata_Build1569388123.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat_Builde3378404562.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc922391174.h"

// System.Void PhoneNumbers.BuildMetadataFromXml::.ctor()
extern "C"  void BuildMetadataFromXml__ctor_m2855252989 (BuildMetadataFromXml_t3731095708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.BuildMetadataFromXml::BuildPhoneMetadataCollection(System.IO.Stream,System.Boolean)
extern "C"  PhoneMetadataCollection_t4114095021 * BuildMetadataFromXml_BuildPhoneMetadataCollection_m352930546 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___input0, bool ___liteBuild1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>> PhoneNumbers.BuildMetadataFromXml::BuildCountryCodeToRegionCodeMap(PhoneNumbers.PhoneMetadataCollection)
extern "C"  Dictionary_2_t406167000 * BuildMetadataFromXml_BuildCountryCodeToRegionCodeMap_m51254414 (Il2CppObject * __this /* static, unused */, PhoneMetadataCollection_t4114095021 * ___metadataCollection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::ValidateRE(System.String)
extern "C"  String_t* BuildMetadataFromXml_ValidateRE_m1322350327 (Il2CppObject * __this /* static, unused */, String_t* ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::ValidateRE(System.String,System.Boolean)
extern "C"  String_t* BuildMetadataFromXml_ValidateRE_m2326172124 (Il2CppObject * __this /* static, unused */, String_t* ___regex0, bool ___removeWhitespace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::GetNationalPrefix(System.Xml.Linq.XElement)
extern "C"  String_t* BuildMetadataFromXml_GetNationalPrefix_m1081422994 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.BuildMetadataFromXml::LoadTerritoryTagMetadata(System.String,System.Xml.Linq.XElement,System.String)
extern "C"  Builder_t1569388123 * BuildMetadataFromXml_LoadTerritoryTagMetadata_m4202161210 (Il2CppObject * __this /* static, unused */, String_t* ___regionCode0, XElement_t553821050 * ___element1, String_t* ___nationalPrefix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.BuildMetadataFromXml::LoadInternationalFormat(PhoneNumbers.PhoneMetadata/Builder,System.Xml.Linq.XElement,System.String)
extern "C"  bool BuildMetadataFromXml_LoadInternationalFormat_m2556683623 (Il2CppObject * __this /* static, unused */, Builder_t1569388123 * ___metadata0, XElement_t553821050 * ___numberFormatElement1, String_t* ___nationalFormat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::LoadNationalFormat(PhoneNumbers.PhoneMetadata/Builder,System.Xml.Linq.XElement,PhoneNumbers.NumberFormat/Builder)
extern "C"  String_t* BuildMetadataFromXml_LoadNationalFormat_m3409300212 (Il2CppObject * __this /* static, unused */, Builder_t1569388123 * ___metadata0, XElement_t553821050 * ___numberFormatElement1, Builder_t3378404562 * ___format2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.BuildMetadataFromXml::LoadAvailableFormats(PhoneNumbers.PhoneMetadata/Builder,System.Xml.Linq.XElement,System.String,System.String,System.Boolean)
extern "C"  void BuildMetadataFromXml_LoadAvailableFormats_m983433278 (Il2CppObject * __this /* static, unused */, Builder_t1569388123 * ___metadata0, XElement_t553821050 * ___element1, String_t* ___nationalPrefix2, String_t* ___nationalPrefixFormattingRule3, bool ___nationalPrefixOptionalWhenFormatting4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.BuildMetadataFromXml::SetLeadingDigitsPatterns(System.Xml.Linq.XElement,PhoneNumbers.NumberFormat/Builder)
extern "C"  void BuildMetadataFromXml_SetLeadingDigitsPatterns_m1230739080 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___numberFormatElement0, Builder_t3378404562 * ___format1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::GetNationalPrefixFormattingRuleFromElement(System.Xml.Linq.XElement,System.String)
extern "C"  String_t* BuildMetadataFromXml_GetNationalPrefixFormattingRuleFromElement_m2406419925 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___element0, String_t* ___nationalPrefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::GetDomesticCarrierCodeFormattingRuleFromElement(System.Xml.Linq.XElement,System.String)
extern "C"  String_t* BuildMetadataFromXml_GetDomesticCarrierCodeFormattingRuleFromElement_m3330699584 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___element0, String_t* ___nationalPrefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.BuildMetadataFromXml::IsValidNumberType(System.String)
extern "C"  bool BuildMetadataFromXml_IsValidNumberType_m1244776894 (Il2CppObject * __this /* static, unused */, String_t* ___numberType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.BuildMetadataFromXml::ProcessPhoneNumberDescElement(PhoneNumbers.PhoneNumberDesc,System.Xml.Linq.XElement,System.String,System.Boolean)
extern "C"  PhoneNumberDesc_t922391174 * BuildMetadataFromXml_ProcessPhoneNumberDescElement_m3857626834 (Il2CppObject * __this /* static, unused */, PhoneNumberDesc_t922391174 * ___generalDesc0, XElement_t553821050 * ___countryElement1, String_t* ___numberType2, bool ___liteBuild3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.BuildMetadataFromXml::ReplaceFirst(System.String,System.String,System.String)
extern "C"  String_t* BuildMetadataFromXml_ReplaceFirst_m1569055490 (Il2CppObject * __this /* static, unused */, String_t* ___input0, String_t* ___value1, String_t* ___replacement2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.BuildMetadataFromXml::LoadGeneralDesc(PhoneNumbers.PhoneMetadata/Builder,System.Xml.Linq.XElement,System.Boolean)
extern "C"  void BuildMetadataFromXml_LoadGeneralDesc_m4250777202 (Il2CppObject * __this /* static, unused */, Builder_t1569388123 * ___metadata0, XElement_t553821050 * ___element1, bool ___liteBuild2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.BuildMetadataFromXml::LoadCountryMetadata(System.String,System.Xml.Linq.XElement,System.Boolean)
extern "C"  PhoneMetadata_t366861403 * BuildMetadataFromXml_LoadCountryMetadata_m4230528885 (Il2CppObject * __this /* static, unused */, String_t* ___regionCode0, XElement_t553821050 * ___element1, bool ___liteBuild2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>> PhoneNumbers.BuildMetadataFromXml::GetCountryCodeToRegionCodeMap(System.String)
extern "C"  Dictionary_2_t406167000 * BuildMetadataFromXml_GetCountryCodeToRegionCodeMap_m3874776419 (Il2CppObject * __this /* static, unused */, String_t* ___filePrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.BuildMetadataFromXml::.cctor()
extern "C"  void BuildMetadataFromXml__cctor_m3863948712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
