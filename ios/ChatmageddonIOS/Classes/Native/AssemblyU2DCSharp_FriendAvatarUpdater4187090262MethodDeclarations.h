﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendAvatarUpdater
struct FriendAvatarUpdater_t4187090262;
// Friend
struct Friend_t3555014108;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"

// System.Void FriendAvatarUpdater::.ctor()
extern "C"  void FriendAvatarUpdater__ctor_m658766707 (FriendAvatarUpdater_t4187090262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendAvatarUpdater::UpdateAvatar(Friend)
extern "C"  void FriendAvatarUpdater_UpdateAvatar_m1251447669 (FriendAvatarUpdater_t4187090262 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendAvatarUpdater::SetAsInitials(Friend)
extern "C"  void FriendAvatarUpdater_SetAsInitials_m1952790296 (FriendAvatarUpdater_t4187090262 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FriendAvatarUpdater::CheckAvatarLoaded(Friend)
extern "C"  Il2CppObject * FriendAvatarUpdater_CheckAvatarLoaded_m1830600115 (FriendAvatarUpdater_t4187090262 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendAvatarUpdater::SetAsProfilePicture(Friend)
extern "C"  void FriendAvatarUpdater_SetAsProfilePicture_m104885564 (FriendAvatarUpdater_t4187090262 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
