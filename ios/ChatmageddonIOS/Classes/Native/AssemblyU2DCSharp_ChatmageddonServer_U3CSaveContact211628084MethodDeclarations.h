﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<SaveContactsToServer>c__AnonStorey2F
struct U3CSaveContactsToServerU3Ec__AnonStorey2F_t211628084;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<SaveContactsToServer>c__AnonStorey2F::.ctor()
extern "C"  void U3CSaveContactsToServerU3Ec__AnonStorey2F__ctor_m2473089929 (U3CSaveContactsToServerU3Ec__AnonStorey2F_t211628084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<SaveContactsToServer>c__AnonStorey2F::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CSaveContactsToServerU3Ec__AnonStorey2F_U3CU3Em__0_m2331966348 (U3CSaveContactsToServerU3Ec__AnonStorey2F_t211628084 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
