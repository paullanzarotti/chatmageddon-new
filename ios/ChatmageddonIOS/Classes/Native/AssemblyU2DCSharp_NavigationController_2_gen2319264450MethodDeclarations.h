﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2145786393MethodDeclarations.h"

// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::.ctor()
#define NavigationController_2__ctor_m6951338(__this, method) ((  void (*) (NavigationController_2_t2319264450 *, const MethodInfo*))NavigationController_2__ctor_m2534835378_gshared)(__this, method)
// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m671708316(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2319264450 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m3679967428_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m995379945(__this, method) ((  void (*) (NavigationController_2_t2319264450 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3208631707_gshared)(__this, method)
// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3560854122(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2319264450 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m346844242_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m499303974(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2319264450 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m1184525474_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m3987547635(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2319264450 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m579444669_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m3458162008(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2319264450 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1577172356_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m1678352270(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2319264450 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m1887059318_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<AttackSearchNavigationController,AttackSearchNav>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m1830718(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2319264450 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m898731946_gshared)(__this, ___screen0, method)
