﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.AmbientValueAttribute
struct AmbientValueAttribute_t1570695927;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Boolean)
extern "C"  void AmbientValueAttribute__ctor_m4085555463 (AmbientValueAttribute_t1570695927 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Byte)
extern "C"  void AmbientValueAttribute__ctor_m3958188425 (AmbientValueAttribute_t1570695927 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Char)
extern "C"  void AmbientValueAttribute__ctor_m2357829599 (AmbientValueAttribute_t1570695927 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Double)
extern "C"  void AmbientValueAttribute__ctor_m3266770638 (AmbientValueAttribute_t1570695927 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Int16)
extern "C"  void AmbientValueAttribute__ctor_m3403704395 (AmbientValueAttribute_t1570695927 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Int32)
extern "C"  void AmbientValueAttribute__ctor_m1078105633 (AmbientValueAttribute_t1570695927 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Int64)
extern "C"  void AmbientValueAttribute__ctor_m271536480 (AmbientValueAttribute_t1570695927 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Object)
extern "C"  void AmbientValueAttribute__ctor_m3945895996 (AmbientValueAttribute_t1570695927 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Single)
extern "C"  void AmbientValueAttribute__ctor_m3343967313 (AmbientValueAttribute_t1570695927 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.String)
extern "C"  void AmbientValueAttribute__ctor_m1718787286 (AmbientValueAttribute_t1570695927 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.AmbientValueAttribute::.ctor(System.Type,System.String)
extern "C"  void AmbientValueAttribute__ctor_m1414513317 (AmbientValueAttribute_t1570695927 * __this, Type_t * ___type0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.AmbientValueAttribute::get_Value()
extern "C"  Il2CppObject * AmbientValueAttribute_get_Value_m3307794191 (AmbientValueAttribute_t1570695927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.AmbientValueAttribute::Equals(System.Object)
extern "C"  bool AmbientValueAttribute_Equals_m2425392261 (AmbientValueAttribute_t1570695927 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.AmbientValueAttribute::GetHashCode()
extern "C"  int32_t AmbientValueAttribute_GetHashCode_m3562782863 (AmbientValueAttribute_t1570695927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
