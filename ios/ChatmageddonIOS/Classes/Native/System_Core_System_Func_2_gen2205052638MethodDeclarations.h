﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen4177300800MethodDeclarations.h"

// System.Void System.Func`2<System.String,System.Int16>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m848037659(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2205052638 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1999898795_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.String,System.Int16>::Invoke(T)
#define Func_2_Invoke_m1217640405(__this, ___arg10, method) ((  int16_t (*) (Func_2_t2205052638 *, String_t*, const MethodInfo*))Func_2_Invoke_m1754263233_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.String,System.Int16>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3688675580(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2205052638 *, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1924500642_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.String,System.Int16>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2149493295(__this, ___result0, method) ((  int16_t (*) (Func_2_t2205052638 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2312002491_gshared)(__this, ___result0, method)
