﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.SectionInformation
struct SectionInformation_t2754609709;
// System.String
struct String_t;
// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t3971982415;
// System.Configuration.ConfigurationSection
struct ConfigurationSection_t2600766927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Configur3250313246.h"
#include "System_Configuration_System_Configuration_Configur3860111898.h"
#include "System_Configuration_System_Configuration_Configur2600766927.h"

// System.Void System.Configuration.SectionInformation::.ctor()
extern "C"  void SectionInformation__ctor_m118501942 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.SectionInformation::get_ConfigFilePath()
extern "C"  String_t* SectionInformation_get_ConfigFilePath_m966874317 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_ConfigFilePath(System.String)
extern "C"  void SectionInformation_set_ConfigFilePath_m2908232936 (SectionInformation_t2754609709 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInformation::get_AllowDefinition()
extern "C"  int32_t SectionInformation_get_AllowDefinition_m3785173147 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_AllowDefinition(System.Configuration.ConfigurationAllowDefinition)
extern "C"  void SectionInformation_set_AllowDefinition_m391233744 (SectionInformation_t2754609709 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInformation::get_AllowExeDefinition()
extern "C"  int32_t SectionInformation_get_AllowExeDefinition_m508400263 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_AllowExeDefinition(System.Configuration.ConfigurationAllowExeDefinition)
extern "C"  void SectionInformation_set_AllowExeDefinition_m2089738550 (SectionInformation_t2754609709 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_AllowLocation()
extern "C"  bool SectionInformation_get_AllowLocation_m3018303069 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_AllowLocation(System.Boolean)
extern "C"  void SectionInformation_set_AllowLocation_m1178294342 (SectionInformation_t2754609709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_AllowOverride()
extern "C"  bool SectionInformation_get_AllowOverride_m2072098576 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_AllowOverride(System.Boolean)
extern "C"  void SectionInformation_set_AllowOverride_m1373051375 (SectionInformation_t2754609709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.SectionInformation::get_ConfigSource()
extern "C"  String_t* SectionInformation_get_ConfigSource_m1613439595 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_ConfigSource(System.String)
extern "C"  void SectionInformation_set_ConfigSource_m2152533200 (SectionInformation_t2754609709 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_ForceSave()
extern "C"  bool SectionInformation_get_ForceSave_m4279050281 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_ForceSave(System.Boolean)
extern "C"  void SectionInformation_set_ForceSave_m3576276886 (SectionInformation_t2754609709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_InheritInChildApplications()
extern "C"  bool SectionInformation_get_InheritInChildApplications_m1358810652 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_InheritInChildApplications(System.Boolean)
extern "C"  void SectionInformation_set_InheritInChildApplications_m3364997415 (SectionInformation_t2754609709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_IsDeclarationRequired()
extern "C"  bool SectionInformation_get_IsDeclarationRequired_m709078072 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_IsDeclared()
extern "C"  bool SectionInformation_get_IsDeclared_m2759151931 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_IsLocked()
extern "C"  bool SectionInformation_get_IsLocked_m2124350829 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_IsProtected()
extern "C"  bool SectionInformation_get_IsProtected_m1991969457 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.SectionInformation::get_Name()
extern "C"  String_t* SectionInformation_get_Name_m156854131 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ProtectedConfigurationProvider System.Configuration.SectionInformation::get_ProtectionProvider()
extern "C"  ProtectedConfigurationProvider_t3971982415 * SectionInformation_get_ProtectionProvider_m1737752840 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_RequirePermission()
extern "C"  bool SectionInformation_get_RequirePermission_m2314733851 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_RequirePermission(System.Boolean)
extern "C"  void SectionInformation_set_RequirePermission_m1040565520 (SectionInformation_t2754609709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInformation::get_RestartOnExternalChanges()
extern "C"  bool SectionInformation_get_RestartOnExternalChanges_m686172019 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_RestartOnExternalChanges(System.Boolean)
extern "C"  void SectionInformation_set_RestartOnExternalChanges_m2815572962 (SectionInformation_t2754609709 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.SectionInformation::get_SectionName()
extern "C"  String_t* SectionInformation_get_SectionName_m978856732 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.SectionInformation::get_Type()
extern "C"  String_t* SectionInformation_get_Type_m2351565766 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::set_Type(System.String)
extern "C"  void SectionInformation_set_Type_m2018131003 (SectionInformation_t2754609709 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSection System.Configuration.SectionInformation::GetParentSection()
extern "C"  ConfigurationSection_t2600766927 * SectionInformation_GetParentSection_m3638988220 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::SetParentSection(System.Configuration.ConfigurationSection)
extern "C"  void SectionInformation_SetParentSection_m321631079 (SectionInformation_t2754609709 * __this, ConfigurationSection_t2600766927 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.SectionInformation::GetRawXml()
extern "C"  String_t* SectionInformation_GetRawXml_m4042077272 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::ProtectSection(System.String)
extern "C"  void SectionInformation_ProtectSection_m230982508 (SectionInformation_t2754609709 * __this, String_t* ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::ForceDeclaration(System.Boolean)
extern "C"  void SectionInformation_ForceDeclaration_m2676503728 (SectionInformation_t2754609709 * __this, bool ___require0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::ForceDeclaration()
extern "C"  void SectionInformation_ForceDeclaration_m2034007625 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::RevertToParent()
extern "C"  void SectionInformation_RevertToParent_m1628774201 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::UnprotectSection()
extern "C"  void SectionInformation_UnprotectSection_m1467770437 (SectionInformation_t2754609709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::SetRawXml(System.String)
extern "C"  void SectionInformation_SetRawXml_m1162572473 (SectionInformation_t2754609709 * __this, String_t* ___xml0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInformation::SetName(System.String)
extern "C"  void SectionInformation_SetName_m2381998591 (SectionInformation_t2754609709 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
