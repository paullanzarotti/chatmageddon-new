﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_AttackHomeNavigateForwardButton3562703987.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackOuterPhoneNumberButton
struct  AttackOuterPhoneNumberButton_t2666821364  : public AttackHomeNavigateForwardButton_t3562703987
{
public:
	// UILabel AttackOuterPhoneNumberButton::phoneNumberlabel
	UILabel_t1795115428 * ___phoneNumberlabel_6;

public:
	inline static int32_t get_offset_of_phoneNumberlabel_6() { return static_cast<int32_t>(offsetof(AttackOuterPhoneNumberButton_t2666821364, ___phoneNumberlabel_6)); }
	inline UILabel_t1795115428 * get_phoneNumberlabel_6() const { return ___phoneNumberlabel_6; }
	inline UILabel_t1795115428 ** get_address_of_phoneNumberlabel_6() { return &___phoneNumberlabel_6; }
	inline void set_phoneNumberlabel_6(UILabel_t1795115428 * value)
	{
		___phoneNumberlabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberlabel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
