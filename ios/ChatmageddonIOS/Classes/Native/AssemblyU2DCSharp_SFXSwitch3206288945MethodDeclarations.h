﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SFXSwitch
struct SFXSwitch_t3206288945;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SFXSwitch::.ctor()
extern "C"  void SFXSwitch__ctor_m2591862850 (SFXSwitch_t3206288945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXSwitch::OnSwitch(System.Boolean)
extern "C"  void SFXSwitch_OnSwitch_m2214449524 (SFXSwitch_t3206288945 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXSwitch::<OnSwitch>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void SFXSwitch_U3COnSwitchU3Em__0_m723920973 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
