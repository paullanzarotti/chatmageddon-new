﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookUser
struct  FacebookUser_t244724091  : public Il2CppObject
{
public:
	// System.String FacebookUser::firstName
	String_t* ___firstName_0;
	// System.String FacebookUser::lastName
	String_t* ___lastName_1;
	// System.String FacebookUser::id
	String_t* ___id_2;
	// System.String FacebookUser::email
	String_t* ___email_3;
	// UnityEngine.Texture FacebookUser::profilePicture
	Texture_t2243626319 * ___profilePicture_4;

public:
	inline static int32_t get_offset_of_firstName_0() { return static_cast<int32_t>(offsetof(FacebookUser_t244724091, ___firstName_0)); }
	inline String_t* get_firstName_0() const { return ___firstName_0; }
	inline String_t** get_address_of_firstName_0() { return &___firstName_0; }
	inline void set_firstName_0(String_t* value)
	{
		___firstName_0 = value;
		Il2CppCodeGenWriteBarrier(&___firstName_0, value);
	}

	inline static int32_t get_offset_of_lastName_1() { return static_cast<int32_t>(offsetof(FacebookUser_t244724091, ___lastName_1)); }
	inline String_t* get_lastName_1() const { return ___lastName_1; }
	inline String_t** get_address_of_lastName_1() { return &___lastName_1; }
	inline void set_lastName_1(String_t* value)
	{
		___lastName_1 = value;
		Il2CppCodeGenWriteBarrier(&___lastName_1, value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(FacebookUser_t244724091, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier(&___id_2, value);
	}

	inline static int32_t get_offset_of_email_3() { return static_cast<int32_t>(offsetof(FacebookUser_t244724091, ___email_3)); }
	inline String_t* get_email_3() const { return ___email_3; }
	inline String_t** get_address_of_email_3() { return &___email_3; }
	inline void set_email_3(String_t* value)
	{
		___email_3 = value;
		Il2CppCodeGenWriteBarrier(&___email_3, value);
	}

	inline static int32_t get_offset_of_profilePicture_4() { return static_cast<int32_t>(offsetof(FacebookUser_t244724091, ___profilePicture_4)); }
	inline Texture_t2243626319 * get_profilePicture_4() const { return ___profilePicture_4; }
	inline Texture_t2243626319 ** get_address_of_profilePicture_4() { return &___profilePicture_4; }
	inline void set_profilePicture_4(Texture_t2243626319 * value)
	{
		___profilePicture_4 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
