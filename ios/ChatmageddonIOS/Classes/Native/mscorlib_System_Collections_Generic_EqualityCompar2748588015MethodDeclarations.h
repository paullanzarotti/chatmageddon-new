﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProfileNavScreen>
struct DefaultComparer_t2748588015;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProfileNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m3561690302_gshared (DefaultComparer_t2748588015 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3561690302(__this, method) ((  void (*) (DefaultComparer_t2748588015 *, const MethodInfo*))DefaultComparer__ctor_m3561690302_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProfileNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2134068333_gshared (DefaultComparer_t2748588015 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2134068333(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2748588015 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2134068333_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProfileNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1245381593_gshared (DefaultComparer_t2748588015 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1245381593(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2748588015 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1245381593_gshared)(__this, ___x0, ___y1, method)
