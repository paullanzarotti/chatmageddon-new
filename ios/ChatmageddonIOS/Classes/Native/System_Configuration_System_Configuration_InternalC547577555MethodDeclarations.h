﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.InternalConfigurationHost
struct InternalConfigurationHost_t547577555;
// System.String
struct String_t;
// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t3971982415;
// System.Configuration.ProtectedConfigurationSection
struct ProtectedConfigurationSection_t3541826375;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Configuration.Internal.IInternalConfigRecord
struct IInternalConfigRecord_t3033563987;
// System.Security.PermissionSet
struct PermissionSet_t1941658161;
// System.IDisposable
struct IDisposable_t2427283555;
// System.IO.Stream
struct Stream_t3255436806;
// System.Configuration.Internal.StreamChangeCallback
struct StreamChangeCallback_t1492982741;
// System.Configuration.Internal.IConfigErrorInfo
struct IConfigErrorInfo_t3123854099;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Protecte3971982415.h"
#include "System_Configuration_System_Configuration_Protecte3541826375.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Security_PermissionSet1941658161.h"
#include "System_Configuration_System_Configuration_Configur3250313246.h"
#include "System_Configuration_System_Configuration_Configur3860111898.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Configuration_System_Configuration_Internal1492982741.h"

// System.Void System.Configuration.InternalConfigurationHost::.ctor()
extern "C"  void InternalConfigurationHost__ctor_m1120670670 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationHost::System.Configuration.Internal.IInternalConfigHost.DecryptSection(System.String,System.Configuration.ProtectedConfigurationProvider,System.Configuration.ProtectedConfigurationSection)
extern "C"  String_t* InternalConfigurationHost_System_Configuration_Internal_IInternalConfigHost_DecryptSection_m1309954051 (InternalConfigurationHost_t547577555 * __this, String_t* ___encryptedXml0, ProtectedConfigurationProvider_t3971982415 * ___protectionProvider1, ProtectedConfigurationSection_t3541826375 * ___protectedSection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationHost::System.Configuration.Internal.IInternalConfigHost.EncryptSection(System.String,System.Configuration.ProtectedConfigurationProvider,System.Configuration.ProtectedConfigurationSection)
extern "C"  String_t* InternalConfigurationHost_System_Configuration_Internal_IInternalConfigHost_EncryptSection_m1792197465 (InternalConfigurationHost_t547577555 * __this, String_t* ___clearXml0, ProtectedConfigurationProvider_t3971982415 * ___protectionProvider1, ProtectedConfigurationSection_t3541826375 * ___protectedSection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InternalConfigurationHost::CreateConfigurationContext(System.String,System.String)
extern "C"  Il2CppObject * InternalConfigurationHost_CreateConfigurationContext_m1758275780 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, String_t* ___locationSubPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InternalConfigurationHost::CreateDeprecatedConfigContext(System.String)
extern "C"  Il2CppObject * InternalConfigurationHost_CreateDeprecatedConfigContext_m3055903953 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::DeleteStream(System.String)
extern "C"  void InternalConfigurationHost_DeleteStream_m2860710565 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationHost::GetConfigPathFromLocationSubPath(System.String,System.String)
extern "C"  String_t* InternalConfigurationHost_GetConfigPathFromLocationSubPath_m3102336364 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, String_t* ___locationSubPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Configuration.InternalConfigurationHost::GetConfigType(System.String,System.Boolean)
extern "C"  Type_t * InternalConfigurationHost_GetConfigType_m2265190311 (InternalConfigurationHost_t547577555 * __this, String_t* ___typeName0, bool ___throwOnError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationHost::GetConfigTypeName(System.Type)
extern "C"  String_t* InternalConfigurationHost_GetConfigTypeName_m1379593467 (InternalConfigurationHost_t547577555 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::GetRestrictedPermissions(System.Configuration.Internal.IInternalConfigRecord,System.Security.PermissionSet&,System.Boolean&)
extern "C"  void InternalConfigurationHost_GetRestrictedPermissions_m3199508237 (InternalConfigurationHost_t547577555 * __this, Il2CppObject * ___configRecord0, PermissionSet_t1941658161 ** ___permissionSet1, bool* ___isHostReady2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationHost::GetStreamNameForConfigSource(System.String,System.String)
extern "C"  String_t* InternalConfigurationHost_GetStreamNameForConfigSource_m159687670 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, String_t* ___configSource1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InternalConfigurationHost::GetStreamVersion(System.String)
extern "C"  Il2CppObject * InternalConfigurationHost_GetStreamVersion_m1540143901 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable System.Configuration.InternalConfigurationHost::Impersonate()
extern "C"  Il2CppObject * InternalConfigurationHost_Impersonate_m2808303774 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsAboveApplication(System.String)
extern "C"  bool InternalConfigurationHost_IsAboveApplication_m3079114139 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsConfigRecordRequired(System.String)
extern "C"  bool InternalConfigurationHost_IsConfigRecordRequired_m820472808 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsDefinitionAllowed(System.String,System.Configuration.ConfigurationAllowDefinition,System.Configuration.ConfigurationAllowExeDefinition)
extern "C"  bool InternalConfigurationHost_IsDefinitionAllowed_m2472316359 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, int32_t ___allowDefinition1, int32_t ___allowExeDefinition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsFile(System.String)
extern "C"  bool InternalConfigurationHost_IsFile_m408431020 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsFullTrustSectionWithoutAptcaAllowed(System.Configuration.Internal.IInternalConfigRecord)
extern "C"  bool InternalConfigurationHost_IsFullTrustSectionWithoutAptcaAllowed_m1290690662 (InternalConfigurationHost_t547577555 * __this, Il2CppObject * ___configRecord0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsInitDelayed(System.Configuration.Internal.IInternalConfigRecord)
extern "C"  bool InternalConfigurationHost_IsInitDelayed_m3358225567 (InternalConfigurationHost_t547577555 * __this, Il2CppObject * ___configRecord0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsLocationApplicable(System.String)
extern "C"  bool InternalConfigurationHost_IsLocationApplicable_m1190240900 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::get_IsRemote()
extern "C"  bool InternalConfigurationHost_get_IsRemote_m1719859333 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsSecondaryRoot(System.String)
extern "C"  bool InternalConfigurationHost_IsSecondaryRoot_m2150103610 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::IsTrustedConfigPath(System.String)
extern "C"  bool InternalConfigurationHost_IsTrustedConfigPath_m643490954 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.InternalConfigurationHost::get_bundled_machine_config()
extern "C"  String_t* InternalConfigurationHost_get_bundled_machine_config_m171293269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Configuration.InternalConfigurationHost::OpenStreamForRead(System.String)
extern "C"  Stream_t3255436806 * InternalConfigurationHost_OpenStreamForRead_m1823276945 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Configuration.InternalConfigurationHost::OpenStreamForRead(System.String,System.Boolean)
extern "C"  Stream_t3255436806 * InternalConfigurationHost_OpenStreamForRead_m947327990 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, bool ___assertPermissions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Configuration.InternalConfigurationHost::OpenStreamForWrite(System.String,System.String,System.Object&)
extern "C"  Stream_t3255436806 * InternalConfigurationHost_OpenStreamForWrite_m40979488 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, String_t* ___templateStreamName1, Il2CppObject ** ___writeContext2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Configuration.InternalConfigurationHost::OpenStreamForWrite(System.String,System.String,System.Object&,System.Boolean)
extern "C"  Stream_t3255436806 * InternalConfigurationHost_OpenStreamForWrite_m1739518619 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, String_t* ___templateStreamName1, Il2CppObject ** ___writeContext2, bool ___assertPermissions3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::PrefetchAll(System.String,System.String)
extern "C"  bool InternalConfigurationHost_PrefetchAll_m2425825322 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, String_t* ___streamName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::PrefetchSection(System.String,System.String)
extern "C"  bool InternalConfigurationHost_PrefetchSection_m590797086 (InternalConfigurationHost_t547577555 * __this, String_t* ___sectionGroupName0, String_t* ___sectionName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::RequireCompleteInit(System.Configuration.Internal.IInternalConfigRecord)
extern "C"  void InternalConfigurationHost_RequireCompleteInit_m1297469917 (InternalConfigurationHost_t547577555 * __this, Il2CppObject * ___configRecord0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.InternalConfigurationHost::StartMonitoringStreamForChanges(System.String,System.Configuration.Internal.StreamChangeCallback)
extern "C"  Il2CppObject * InternalConfigurationHost_StartMonitoringStreamForChanges_m1544171234 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, StreamChangeCallback_t1492982741 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::StopMonitoringStreamForChanges(System.String,System.Configuration.Internal.StreamChangeCallback)
extern "C"  void InternalConfigurationHost_StopMonitoringStreamForChanges_m2041130041 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, StreamChangeCallback_t1492982741 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::VerifyDefinitionAllowed(System.String,System.Configuration.ConfigurationAllowDefinition,System.Configuration.ConfigurationAllowExeDefinition,System.Configuration.Internal.IConfigErrorInfo)
extern "C"  void InternalConfigurationHost_VerifyDefinitionAllowed_m492543287 (InternalConfigurationHost_t547577555 * __this, String_t* ___configPath0, int32_t ___allowDefinition1, int32_t ___allowExeDefinition2, Il2CppObject * ___errorInfo3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::WriteCompleted(System.String,System.Boolean,System.Object)
extern "C"  void InternalConfigurationHost_WriteCompleted_m3974784745 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, bool ___success1, Il2CppObject * ___writeContext2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.InternalConfigurationHost::WriteCompleted(System.String,System.Boolean,System.Object,System.Boolean)
extern "C"  void InternalConfigurationHost_WriteCompleted_m2037624422 (InternalConfigurationHost_t547577555 * __this, String_t* ___streamName0, bool ___success1, Il2CppObject * ___writeContext2, bool ___assertPermissions3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::get_SupportsChangeNotifications()
extern "C"  bool InternalConfigurationHost_get_SupportsChangeNotifications_m244592271 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::get_SupportsLocation()
extern "C"  bool InternalConfigurationHost_get_SupportsLocation_m2282226770 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::get_SupportsPath()
extern "C"  bool InternalConfigurationHost_get_SupportsPath_m1704688002 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.InternalConfigurationHost::get_SupportsRefresh()
extern "C"  bool InternalConfigurationHost_get_SupportsRefresh_m1392414090 (InternalConfigurationHost_t547577555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
