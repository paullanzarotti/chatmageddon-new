﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>
struct U3CGetEnumeratorU3Ec__Iterator0_t1679028045;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry974746545.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2120312359_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m2120312359(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m2120312359_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m57319577_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m57319577(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m57319577_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  DepthEntry_t974746545  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1807105252_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1807105252(__this, method) ((  DepthEntry_t974746545  (*) (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1807105252_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2900995249_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2900995249(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2900995249_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3635329320_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3635329320(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3635329320_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<UICamera/DepthEntry>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2961153050_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m2961153050(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1679028045 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m2961153050_gshared)(__this, method)
