﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<BestHTTP.HTTPRequest>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m911826605(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t4235252565 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2192865289_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<BestHTTP.HTTPRequest>::Invoke(T)
#define Action_1_Invoke_m2973592469(__this, ___obj0, method) ((  void (*) (Action_1_t4235252565 *, HTTPRequest_t138485887 *, const MethodInfo*))Action_1_Invoke_m3960033342_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<BestHTTP.HTTPRequest>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m4252650548(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t4235252565 *, HTTPRequest_t138485887 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<BestHTTP.HTTPRequest>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1681797779(__this, ___result0, method) ((  void (*) (Action_1_t4235252565 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
