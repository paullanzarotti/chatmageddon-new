﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsOSMWay
struct OnlineMapsOSMWay_t3319895272;
// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_t4065209745;
// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_3_t246581160;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t1623697841;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t3377395111;

#include "AssemblyU2DCSharp_OnlineMapsBuildingBase650727021.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuildingBuiltIn
struct  OnlineMapsBuildingBuiltIn_t544190665  : public OnlineMapsBuildingBase_t650727021
{
public:
	// OnlineMapsOSMWay OnlineMapsBuildingBuiltIn::way
	OnlineMapsOSMWay_t3319895272 * ___way_17;

public:
	inline static int32_t get_offset_of_way_17() { return static_cast<int32_t>(offsetof(OnlineMapsBuildingBuiltIn_t544190665, ___way_17)); }
	inline OnlineMapsOSMWay_t3319895272 * get_way_17() const { return ___way_17; }
	inline OnlineMapsOSMWay_t3319895272 ** get_address_of_way_17() { return &___way_17; }
	inline void set_way_17(OnlineMapsOSMWay_t3319895272 * value)
	{
		___way_17 = value;
		Il2CppCodeGenWriteBarrier(&___way_17, value);
	}
};

struct OnlineMapsBuildingBuiltIn_t544190665_StaticFields
{
public:
	// System.Func`2<UnityEngine.Vector3,System.Single> OnlineMapsBuildingBuiltIn::<>f__am$cache0
	Func_2_t4065209745 * ___U3CU3Ef__amU24cache0_18;
	// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3> OnlineMapsBuildingBuiltIn::<>f__am$cache1
	Func_3_t246581160 * ___U3CU3Ef__amU24cache1_19;
	// System.Func`2<System.Int32,System.Int32> OnlineMapsBuildingBuiltIn::<>f__am$cache2
	Func_2_t1623697841 * ___U3CU3Ef__amU24cache2_20;
	// System.Func`2<System.Int32,System.Boolean> OnlineMapsBuildingBuiltIn::<>f__am$cache3
	Func_2_t3377395111 * ___U3CU3Ef__amU24cache3_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_18() { return static_cast<int32_t>(offsetof(OnlineMapsBuildingBuiltIn_t544190665_StaticFields, ___U3CU3Ef__amU24cache0_18)); }
	inline Func_2_t4065209745 * get_U3CU3Ef__amU24cache0_18() const { return ___U3CU3Ef__amU24cache0_18; }
	inline Func_2_t4065209745 ** get_address_of_U3CU3Ef__amU24cache0_18() { return &___U3CU3Ef__amU24cache0_18; }
	inline void set_U3CU3Ef__amU24cache0_18(Func_2_t4065209745 * value)
	{
		___U3CU3Ef__amU24cache0_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_19() { return static_cast<int32_t>(offsetof(OnlineMapsBuildingBuiltIn_t544190665_StaticFields, ___U3CU3Ef__amU24cache1_19)); }
	inline Func_3_t246581160 * get_U3CU3Ef__amU24cache1_19() const { return ___U3CU3Ef__amU24cache1_19; }
	inline Func_3_t246581160 ** get_address_of_U3CU3Ef__amU24cache1_19() { return &___U3CU3Ef__amU24cache1_19; }
	inline void set_U3CU3Ef__amU24cache1_19(Func_3_t246581160 * value)
	{
		___U3CU3Ef__amU24cache1_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_20() { return static_cast<int32_t>(offsetof(OnlineMapsBuildingBuiltIn_t544190665_StaticFields, ___U3CU3Ef__amU24cache2_20)); }
	inline Func_2_t1623697841 * get_U3CU3Ef__amU24cache2_20() const { return ___U3CU3Ef__amU24cache2_20; }
	inline Func_2_t1623697841 ** get_address_of_U3CU3Ef__amU24cache2_20() { return &___U3CU3Ef__amU24cache2_20; }
	inline void set_U3CU3Ef__amU24cache2_20(Func_2_t1623697841 * value)
	{
		___U3CU3Ef__amU24cache2_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_21() { return static_cast<int32_t>(offsetof(OnlineMapsBuildingBuiltIn_t544190665_StaticFields, ___U3CU3Ef__amU24cache3_21)); }
	inline Func_2_t3377395111 * get_U3CU3Ef__amU24cache3_21() const { return ___U3CU3Ef__amU24cache3_21; }
	inline Func_2_t3377395111 ** get_address_of_U3CU3Ef__amU24cache3_21() { return &___U3CU3Ef__amU24cache3_21; }
	inline void set_U3CU3Ef__amU24cache3_21(Func_2_t3377395111 * value)
	{
		___U3CU3Ef__amU24cache3_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
