﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseStorePUButton
struct CloseStorePUButton_t1133311818;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseStorePUButton::.ctor()
extern "C"  void CloseStorePUButton__ctor_m3324313089 (CloseStorePUButton_t1133311818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseStorePUButton::OnButtonClick()
extern "C"  void CloseStorePUButton_OnButtonClick_m4162976562 (CloseStorePUButton_t1133311818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
