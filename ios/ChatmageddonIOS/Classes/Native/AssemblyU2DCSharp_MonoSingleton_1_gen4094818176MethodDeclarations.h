﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<HeaderManager>::.ctor()
#define MonoSingleton_1__ctor_m1389838234(__this, method) ((  void (*) (MonoSingleton_1_t4094818176 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<HeaderManager>::Awake()
#define MonoSingleton_1_Awake_m758217637(__this, method) ((  void (*) (MonoSingleton_1_t4094818176 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<HeaderManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1428073779(__this /* static, unused */, method) ((  HeaderManager_t49185160 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<HeaderManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1265861587(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<HeaderManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3708263348(__this, method) ((  void (*) (MonoSingleton_1_t4094818176 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<HeaderManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m400193152(__this, method) ((  void (*) (MonoSingleton_1_t4094818176 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<HeaderManager>::.cctor()
#define MonoSingleton_1__cctor_m3588039435(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
