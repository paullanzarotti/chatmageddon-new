﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsAcceptButton
struct FriendsAcceptButton_t784515205;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsAcceptButton::.ctor()
extern "C"  void FriendsAcceptButton__ctor_m18204272 (FriendsAcceptButton_t784515205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsAcceptButton::OnButtonClick()
extern "C"  void FriendsAcceptButton_OnButtonClick_m3531455149 (FriendsAcceptButton_t784515205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
