﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StatusListItem
struct StatusListItem_t459202613;

#include "AssemblyU2DCSharp_StatusDoubleStateButton3675410548.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusChatButton
struct  StatusChatButton_t749649916  : public StatusDoubleStateButton_t3675410548
{
public:
	// StatusListItem StatusChatButton::item
	StatusListItem_t459202613 * ___item_19;

public:
	inline static int32_t get_offset_of_item_19() { return static_cast<int32_t>(offsetof(StatusChatButton_t749649916, ___item_19)); }
	inline StatusListItem_t459202613 * get_item_19() const { return ___item_19; }
	inline StatusListItem_t459202613 ** get_address_of_item_19() { return &___item_19; }
	inline void set_item_19(StatusListItem_t459202613 * value)
	{
		___item_19 = value;
		Il2CppCodeGenWriteBarrier(&___item_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
