﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UnblockUser>c__AnonStoreyB
struct U3CUnblockUserU3Ec__AnonStoreyB_t3544433809;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UnblockUser>c__AnonStoreyB::.ctor()
extern "C"  void U3CUnblockUserU3Ec__AnonStoreyB__ctor_m1714121110 (U3CUnblockUserU3Ec__AnonStoreyB_t3544433809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UnblockUser>c__AnonStoreyB::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUnblockUserU3Ec__AnonStoreyB_U3CU3Em__0_m171774169 (U3CUnblockUserU3Ec__AnonStoreyB_t3544433809 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
