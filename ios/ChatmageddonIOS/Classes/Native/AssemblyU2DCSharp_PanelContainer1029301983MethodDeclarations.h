﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelContainer
struct PanelContainer_t1029301983;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelContainer::.ctor()
extern "C"  void PanelContainer__ctor_m3113031436 (PanelContainer_t1029301983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
