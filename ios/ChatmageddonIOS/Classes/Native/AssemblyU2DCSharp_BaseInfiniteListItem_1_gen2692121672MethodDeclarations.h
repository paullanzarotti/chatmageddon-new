﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<OtherFriendsListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m4234042002(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2692121672 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<OtherFriendsListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m3588007011(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t2692121672 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<OtherFriendsListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m695511773(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2692121672 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<OtherFriendsListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m1618898812(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2692121672 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<OtherFriendsListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m525659687(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t2692121672 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<OtherFriendsListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m4185220946(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2692121672 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<OtherFriendsListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m97307941(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2692121672 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
