﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickerPopper
struct PickerPopper_t344149016;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void PickerPopper::.ctor()
extern "C"  void PickerPopper__ctor_m3406171305 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper::Start()
extern "C"  void PickerPopper_Start_m2031928057 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PickerPopper::OpenPicker()
extern "C"  Il2CppObject * PickerPopper_OpenPicker_m2224665961 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PickerPopper::ClosePicker()
extern "C"  Il2CppObject * PickerPopper_ClosePicker_m3681084891 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper::OnClick()
extern "C"  void PickerPopper_OnClick_m3028884240 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper::PickerClicked()
extern "C"  void PickerPopper_PickerClicked_m1465244800 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper::OnPickerSelectionStarted()
extern "C"  void PickerPopper_OnPickerSelectionStarted_m3158301171 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper::OnPickerStopped()
extern "C"  void PickerPopper_OnPickerStopped_m1251462703 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper::OnTweenClipRangeFinished()
extern "C"  void PickerPopper_OnTweenClipRangeFinished_m4144849210 (PickerPopper_t344149016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PickerPopper::BlinkWidget(System.Int32)
extern "C"  Il2CppObject * PickerPopper_BlinkWidget_m393310984 (PickerPopper_t344149016 * __this, int32_t ___nbOfBlinks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PickerPopper::LerpBackgroundColor(UnityEngine.Color,System.Single)
extern "C"  Il2CppObject * PickerPopper_LerpBackgroundColor_m1447202714 (PickerPopper_t344149016 * __this, Color_t2020392075  ___targetColor0, float ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
