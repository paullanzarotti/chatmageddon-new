﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitSceneManager/<WaitForScreenResize>c__Iterator0
struct U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void InitSceneManager/<WaitForScreenResize>c__Iterator0::.ctor()
extern "C"  void U3CWaitForScreenResizeU3Ec__Iterator0__ctor_m55160866 (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InitSceneManager/<WaitForScreenResize>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForScreenResizeU3Ec__Iterator0_MoveNext_m2111989442 (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InitSceneManager/<WaitForScreenResize>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForScreenResizeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1337836394 (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InitSceneManager/<WaitForScreenResize>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForScreenResizeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m215437282 (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager/<WaitForScreenResize>c__Iterator0::Dispose()
extern "C"  void U3CWaitForScreenResizeU3Ec__Iterator0_Dispose_m3534389105 (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager/<WaitForScreenResize>c__Iterator0::Reset()
extern "C"  void U3CWaitForScreenResizeU3Ec__Iterator0_Reset_m4167514795 (U3CWaitForScreenResizeU3Ec__Iterator0_t2950940155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
