﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsHomeScreenList
struct SettingsHomeScreenList_t2265775114;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SettingsHomeScreenList::.ctor()
extern "C"  void SettingsHomeScreenList__ctor_m978927091 (SettingsHomeScreenList_t2265775114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsHomeScreenList::SetUpList()
extern "C"  void SettingsHomeScreenList_SetUpList_m3735455792 (SettingsHomeScreenList_t2265775114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsHomeScreenList::AssignProfileData(System.String,System.String,System.String)
extern "C"  void SettingsHomeScreenList_AssignProfileData_m3466787891 (SettingsHomeScreenList_t2265775114 * __this, String_t* ___firstName0, String_t* ___lastName1, String_t* ___userName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsHomeScreenList::AssignStealthModeData(System.Boolean,System.String,System.String)
extern "C"  void SettingsHomeScreenList_AssignStealthModeData_m3779602281 (SettingsHomeScreenList_t2265775114 * __this, bool ___active0, String_t* ___startTime1, String_t* ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
