﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.BoxCollider>
struct List_1_t3687008489;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t1164236560;

#include "AssemblyU2DCSharp_ScrollViewUIScaler198225486.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextPageUIScaler
struct  TextPageUIScaler_t3568234746  : public ScrollViewUIScaler_t198225486
{
public:
	// System.Collections.Generic.List`1<UnityEngine.BoxCollider> TextPageUIScaler::colliders
	List_1_t3687008489 * ___colliders_15;
	// System.Collections.Generic.List`1<UILabel> TextPageUIScaler::labels
	List_1_t1164236560 * ___labels_16;

public:
	inline static int32_t get_offset_of_colliders_15() { return static_cast<int32_t>(offsetof(TextPageUIScaler_t3568234746, ___colliders_15)); }
	inline List_1_t3687008489 * get_colliders_15() const { return ___colliders_15; }
	inline List_1_t3687008489 ** get_address_of_colliders_15() { return &___colliders_15; }
	inline void set_colliders_15(List_1_t3687008489 * value)
	{
		___colliders_15 = value;
		Il2CppCodeGenWriteBarrier(&___colliders_15, value);
	}

	inline static int32_t get_offset_of_labels_16() { return static_cast<int32_t>(offsetof(TextPageUIScaler_t3568234746, ___labels_16)); }
	inline List_1_t1164236560 * get_labels_16() const { return ___labels_16; }
	inline List_1_t1164236560 ** get_address_of_labels_16() { return &___labels_16; }
	inline void set_labels_16(List_1_t1164236560 * value)
	{
		___labels_16 = value;
		Il2CppCodeGenWriteBarrier(&___labels_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
