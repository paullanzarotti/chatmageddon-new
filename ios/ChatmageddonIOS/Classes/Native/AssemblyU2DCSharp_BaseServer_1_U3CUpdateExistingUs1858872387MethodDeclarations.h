﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<UpdateExistingUser>c__AnonStorey1D<System.Object>
struct U3CUpdateExistingUserU3Ec__AnonStorey1D_t1858872387;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<UpdateExistingUser>c__AnonStorey1D<System.Object>::.ctor()
extern "C"  void U3CUpdateExistingUserU3Ec__AnonStorey1D__ctor_m3420101248_gshared (U3CUpdateExistingUserU3Ec__AnonStorey1D_t1858872387 * __this, const MethodInfo* method);
#define U3CUpdateExistingUserU3Ec__AnonStorey1D__ctor_m3420101248(__this, method) ((  void (*) (U3CUpdateExistingUserU3Ec__AnonStorey1D_t1858872387 *, const MethodInfo*))U3CUpdateExistingUserU3Ec__AnonStorey1D__ctor_m3420101248_gshared)(__this, method)
// System.Void BaseServer`1/<UpdateExistingUser>c__AnonStorey1D<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateExistingUserU3Ec__AnonStorey1D_U3CU3Em__0_m2184467975_gshared (U3CUpdateExistingUserU3Ec__AnonStorey1D_t1858872387 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CUpdateExistingUserU3Ec__AnonStorey1D_U3CU3Em__0_m2184467975(__this, ___request0, ___response1, method) ((  void (*) (U3CUpdateExistingUserU3Ec__AnonStorey1D_t1858872387 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CUpdateExistingUserU3Ec__AnonStorey1D_U3CU3Em__0_m2184467975_gshared)(__this, ___request0, ___response1, method)
