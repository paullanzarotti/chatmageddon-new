﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct Dictionary_2_t283153520;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2972656958.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m921961640_gshared (Enumerator_t2972656958 * __this, Dictionary_2_t283153520 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m921961640(__this, ___host0, method) ((  void (*) (Enumerator_t2972656958 *, Dictionary_2_t283153520 *, const MethodInfo*))Enumerator__ctor_m921961640_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1120448039_gshared (Enumerator_t2972656958 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1120448039(__this, method) ((  Il2CppObject * (*) (Enumerator_t2972656958 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1120448039_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1137087215_gshared (Enumerator_t2972656958 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1137087215(__this, method) ((  void (*) (Enumerator_t2972656958 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1137087215_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3894127796_gshared (Enumerator_t2972656958 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3894127796(__this, method) ((  void (*) (Enumerator_t2972656958 *, const MethodInfo*))Enumerator_Dispose_m3894127796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1281020475_gshared (Enumerator_t2972656958 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1281020475(__this, method) ((  bool (*) (Enumerator_t2972656958 *, const MethodInfo*))Enumerator_MoveNext_m1281020475_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3759474221_gshared (Enumerator_t2972656958 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3759474221(__this, method) ((  int32_t (*) (Enumerator_t2972656958 *, const MethodInfo*))Enumerator_get_Current_m3759474221_gshared)(__this, method)
