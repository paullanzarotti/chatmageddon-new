﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_ShareRequestLocation2965642179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceShareButton
struct  DeviceShareButton_t1871825377  : public SFXButton_t792651341
{
public:
	// ShareRequestLocation DeviceShareButton::location
	int32_t ___location_5;

public:
	inline static int32_t get_offset_of_location_5() { return static_cast<int32_t>(offsetof(DeviceShareButton_t1871825377, ___location_5)); }
	inline int32_t get_location_5() const { return ___location_5; }
	inline int32_t* get_address_of_location_5() { return &___location_5; }
	inline void set_location_5(int32_t value)
	{
		___location_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
