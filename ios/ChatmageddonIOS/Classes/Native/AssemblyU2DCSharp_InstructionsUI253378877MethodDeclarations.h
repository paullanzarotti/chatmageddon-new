﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InstructionsUI
struct InstructionsUI_t253378877;

#include "codegen/il2cpp-codegen.h"

// System.Void InstructionsUI::.ctor()
extern "C"  void InstructionsUI__ctor_m3784946626 (InstructionsUI_t253378877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsUI::LoadUI()
extern "C"  void InstructionsUI_LoadUI_m3871729502 (InstructionsUI_t253378877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
