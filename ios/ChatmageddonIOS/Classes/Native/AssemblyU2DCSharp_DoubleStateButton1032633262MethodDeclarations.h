﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DoubleStateButton
struct DoubleStateButton_t1032633262;

#include "codegen/il2cpp-codegen.h"

// System.Void DoubleStateButton::.ctor()
extern "C"  void DoubleStateButton__ctor_m1911784807 (DoubleStateButton_t1032633262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoubleStateButton::SetButtonActive(System.Boolean)
extern "C"  void DoubleStateButton_SetButtonActive_m485683066 (DoubleStateButton_t1032633262 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
