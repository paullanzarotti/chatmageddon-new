﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VerificationCodeInput
struct  VerificationCodeInput_t924401746  : public SFXButton_t792651341
{
public:
	// UIInput VerificationCodeInput::codeinput
	UIInput_t860674234 * ___codeinput_5;

public:
	inline static int32_t get_offset_of_codeinput_5() { return static_cast<int32_t>(offsetof(VerificationCodeInput_t924401746, ___codeinput_5)); }
	inline UIInput_t860674234 * get_codeinput_5() const { return ___codeinput_5; }
	inline UIInput_t860674234 ** get_address_of_codeinput_5() { return &___codeinput_5; }
	inline void set_codeinput_5(UIInput_t860674234 * value)
	{
		___codeinput_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeinput_5, value);
	}
};

struct VerificationCodeInput_t924401746_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> VerificationCodeInput::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(VerificationCodeInput_t924401746_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
