﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildManager
struct BuildManager_t19508555;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void BuildManager::.ctor()
extern "C"  void BuildManager__ctor_m179633386 (BuildManager_t19508555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildManager::BuildInit()
extern "C"  void BuildManager_BuildInit_m3383443664 (BuildManager_t19508555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildManager::SetUniqueDeviceID()
extern "C"  void BuildManager_SetUniqueDeviceID_m2033295034 (BuildManager_t19508555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BuildManager::CreateRFC4122()
extern "C"  String_t* BuildManager_CreateRFC4122_m1440804737 (BuildManager_t19508555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildManager::SetUICalculation()
extern "C"  void BuildManager_SetUICalculation_m1299697089 (BuildManager_t19508555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildManager::CalulateUIScale()
extern "C"  void BuildManager_CalulateUIScale_m4158351957 (BuildManager_t19508555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
