﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlockListNavScreen
struct BlockListNavScreen_t522977310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void BlockListNavScreen::.ctor()
extern "C"  void BlockListNavScreen__ctor_m3489071701 (BlockListNavScreen_t522977310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListNavScreen::Start()
extern "C"  void BlockListNavScreen_Start_m3131044189 (BlockListNavScreen_t522977310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListNavScreen::UIClosing()
extern "C"  void BlockListNavScreen_UIClosing_m1727136680 (BlockListNavScreen_t522977310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void BlockListNavScreen_ScreenLoad_m3095593651 (BlockListNavScreen_t522977310 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void BlockListNavScreen_ScreenUnload_m393985483 (BlockListNavScreen_t522977310 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListNavScreen::UnloadSettings(System.Boolean)
extern "C"  void BlockListNavScreen_UnloadSettings_m2920616170 (BlockListNavScreen_t522977310 * __this, bool ___chat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListNavScreen::SettingsUIClosing()
extern "C"  void BlockListNavScreen_SettingsUIClosing_m2303615947 (BlockListNavScreen_t522977310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlockListNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool BlockListNavScreen_ValidateScreenNavigation_m3310354538 (BlockListNavScreen_t522977310 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
