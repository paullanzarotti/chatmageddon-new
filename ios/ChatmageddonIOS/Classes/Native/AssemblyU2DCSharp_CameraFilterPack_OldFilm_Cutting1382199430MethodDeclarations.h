﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_OldFilm_Cutting1
struct CameraFilterPack_OldFilm_Cutting1_t1382199430;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_OldFilm_Cutting1::.ctor()
extern "C"  void CameraFilterPack_OldFilm_Cutting1__ctor_m2940663277 (CameraFilterPack_OldFilm_Cutting1_t1382199430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_OldFilm_Cutting1::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_OldFilm_Cutting1_get_material_m1342786426 (CameraFilterPack_OldFilm_Cutting1_t1382199430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::Start()
extern "C"  void CameraFilterPack_OldFilm_Cutting1_Start_m602945229 (CameraFilterPack_OldFilm_Cutting1_t1382199430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_OldFilm_Cutting1_OnRenderImage_m1929093149 (CameraFilterPack_OldFilm_Cutting1_t1382199430 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::Update()
extern "C"  void CameraFilterPack_OldFilm_Cutting1_Update_m3649972072 (CameraFilterPack_OldFilm_Cutting1_t1382199430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::OnDisable()
extern "C"  void CameraFilterPack_OldFilm_Cutting1_OnDisable_m1918989822 (CameraFilterPack_OldFilm_Cutting1_t1382199430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
