﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadFromTextFile
struct LoadFromTextFile_t2022748909;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoadFromTextFile::.ctor()
extern "C"  void LoadFromTextFile__ctor_m727912732 (LoadFromTextFile_t2022748909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadFromTextFile::Load(System.String)
extern "C"  bool LoadFromTextFile_Load_m713227590 (LoadFromTextFile_t2022748909 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] LoadFromTextFile::GetLoadedContent()
extern "C"  StringU5BU5D_t1642385972* LoadFromTextFile_GetLoadedContent_m2648460355 (LoadFromTextFile_t2022748909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
