﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackItemAttackButton
struct AttackItemAttackButton_t1001821753;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackItemAttackButton::.ctor()
extern "C"  void AttackItemAttackButton__ctor_m160509542 (AttackItemAttackButton_t1001821753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackItemAttackButton::OnButtonClick()
extern "C"  void AttackItemAttackButton_OnButtonClick_m2137138461 (AttackItemAttackButton_t1001821753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
