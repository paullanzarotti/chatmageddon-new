﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameController
struct  MiniGameController_t3067676539  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean MiniGameController::paused
	bool ___paused_2;
	// System.Boolean MiniGameController::stopped
	bool ___stopped_3;

public:
	inline static int32_t get_offset_of_paused_2() { return static_cast<int32_t>(offsetof(MiniGameController_t3067676539, ___paused_2)); }
	inline bool get_paused_2() const { return ___paused_2; }
	inline bool* get_address_of_paused_2() { return &___paused_2; }
	inline void set_paused_2(bool value)
	{
		___paused_2 = value;
	}

	inline static int32_t get_offset_of_stopped_3() { return static_cast<int32_t>(offsetof(MiniGameController_t3067676539, ___stopped_3)); }
	inline bool get_stopped_3() const { return ___stopped_3; }
	inline bool* get_address_of_stopped_3() { return &___stopped_3; }
	inline void set_stopped_3(bool value)
	{
		___stopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
