﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookBatchRequest
struct  FacebookBatchRequest_t2176192103  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Prime31.FacebookBatchRequest::_parameters
	Dictionary_2_t3943999495 * ____parameters_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.FacebookBatchRequest::_requestDict
	Dictionary_2_t309261261 * ____requestDict_1;

public:
	inline static int32_t get_offset_of__parameters_0() { return static_cast<int32_t>(offsetof(FacebookBatchRequest_t2176192103, ____parameters_0)); }
	inline Dictionary_2_t3943999495 * get__parameters_0() const { return ____parameters_0; }
	inline Dictionary_2_t3943999495 ** get_address_of__parameters_0() { return &____parameters_0; }
	inline void set__parameters_0(Dictionary_2_t3943999495 * value)
	{
		____parameters_0 = value;
		Il2CppCodeGenWriteBarrier(&____parameters_0, value);
	}

	inline static int32_t get_offset_of__requestDict_1() { return static_cast<int32_t>(offsetof(FacebookBatchRequest_t2176192103, ____requestDict_1)); }
	inline Dictionary_2_t309261261 * get__requestDict_1() const { return ____requestDict_1; }
	inline Dictionary_2_t309261261 ** get_address_of__requestDict_1() { return &____requestDict_1; }
	inline void set__requestDict_1(Dictionary_2_t309261261 * value)
	{
		____requestDict_1 = value;
		Il2CppCodeGenWriteBarrier(&____requestDict_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
