﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITextureAnimation
struct UITextureAnimation_t3587053699;

#include "codegen/il2cpp-codegen.h"

// System.Void UITextureAnimation::.ctor()
extern "C"  void UITextureAnimation__ctor_m2260777384 (UITextureAnimation_t3587053699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureAnimation::SetAnimDepth(System.Int32)
extern "C"  void UITextureAnimation_SetAnimDepth_m3839696215 (UITextureAnimation_t3587053699 * __this, int32_t ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextureAnimation::GetAnimDepth()
extern "C"  int32_t UITextureAnimation_GetAnimDepth_m1415700806 (UITextureAnimation_t3587053699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureAnimation::SetAnimWidth(System.Int32)
extern "C"  void UITextureAnimation_SetAnimWidth_m1807674264 (UITextureAnimation_t3587053699 * __this, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextureAnimation::GetAnimWidth()
extern "C"  int32_t UITextureAnimation_GetAnimWidth_m1766109991 (UITextureAnimation_t3587053699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureAnimation::SetAnimHeight(System.Int32)
extern "C"  void UITextureAnimation_SetAnimHeight_m1656728445 (UITextureAnimation_t3587053699 * __this, int32_t ___height0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextureAnimation::GetAnimHeight()
extern "C"  int32_t UITextureAnimation_GetAnimHeight_m2810319480 (UITextureAnimation_t3587053699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
