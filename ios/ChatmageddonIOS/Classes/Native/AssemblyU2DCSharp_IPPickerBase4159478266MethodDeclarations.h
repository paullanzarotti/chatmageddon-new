﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPPickerBase
struct IPPickerBase_t4159478266;

#include "codegen/il2cpp-codegen.h"

// System.Void IPPickerBase::.ctor()
extern "C"  void IPPickerBase__ctor_m221042673 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPPickerBase::get_SelectedIndex()
extern "C"  int32_t IPPickerBase_get_SelectedIndex_m2616615157 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPPickerBase::get_VirtualElementsCount()
extern "C"  int32_t IPPickerBase_get_VirtualElementsCount_m406979457 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPPickerBase::get_IsCyclerMoving()
extern "C"  bool IPPickerBase_get_IsCyclerMoving_m212583820 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::Awake()
extern "C"  void IPPickerBase_Awake_m847885798 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::OnDestroy()
extern "C"  void IPPickerBase_OnDestroy_m2461568840 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::Setup()
extern "C"  void IPPickerBase_Setup_m2660622660 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::Init()
extern "C"  void IPPickerBase_Init_m2976377123 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::ResetPickerAtIndex(System.Int32)
extern "C"  void IPPickerBase_ResetPickerAtIndex_m1194860808 (IPPickerBase_t4159478266 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::ResetPicker()
extern "C"  void IPPickerBase_ResetPicker_m962685896 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::CyclerIndexChange(System.Boolean,System.Int32)
extern "C"  void IPPickerBase_CyclerIndexChange_m1938084133 (IPPickerBase_t4159478266 * __this, bool ___increment0, int32_t ___widgetIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::CycleWidgets(System.Boolean,System.Int32)
extern "C"  void IPPickerBase_CycleWidgets_m2192589150 (IPPickerBase_t4159478266 * __this, bool ___indexIncremented0, int32_t ___widgetIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::RebuildWidgets()
extern "C"  void IPPickerBase_RebuildWidgets_m1217335035 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::ResetWidgets()
extern "C"  void IPPickerBase_ResetWidgets_m2318628839 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase::ResetWidgetsContent()
extern "C"  void IPPickerBase_ResetWidgetsContent_m3718435208 (IPPickerBase_t4159478266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
