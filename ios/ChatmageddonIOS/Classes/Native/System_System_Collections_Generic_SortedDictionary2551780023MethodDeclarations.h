﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3212009085MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.String>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define Enumerator__ctor_m600813756(__this, ___dic0, method) ((  void (*) (Enumerator_t2551780023 *, SortedDictionary_2_t2686657757 *, const MethodInfo*))Enumerator__ctor_m245074370_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1014382084(__this, method) ((  Il2CppObject * (*) (Enumerator_t2551780023 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m451777246_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m256107034(__this, method) ((  void (*) (Enumerator_t2551780023 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1175503104_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.String>::get_Current()
#define Enumerator_get_Current_m4012679535(__this, method) ((  int32_t (*) (Enumerator_t2551780023 *, const MethodInfo*))Enumerator_get_Current_m4004123576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.String>::MoveNext()
#define Enumerator_MoveNext_m1411050811(__this, method) ((  bool (*) (Enumerator_t2551780023 *, const MethodInfo*))Enumerator_MoveNext_m17822024_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.String>::Dispose()
#define Enumerator_Dispose_m489788863(__this, method) ((  void (*) (Enumerator_t2551780023 *, const MethodInfo*))Enumerator_Dispose_m4250516907_gshared)(__this, method)
