﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMRelationMember
struct OnlineMapsOSMRelationMember_t1040319881;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"

// System.Void OnlineMapsOSMRelationMember::.ctor(OnlineMapsXML)
extern "C"  void OnlineMapsOSMRelationMember__ctor_m3317080189 (OnlineMapsOSMRelationMember_t1040319881 * __this, OnlineMapsXML_t3341520387 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
