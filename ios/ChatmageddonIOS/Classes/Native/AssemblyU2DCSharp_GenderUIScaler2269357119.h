﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GenderSwitch
struct GenderSwitch_t696294539;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenderUIScaler
struct  GenderUIScaler_t2269357119  : public ChatmageddonUIScaler_t3374094653
{
public:
	// GenderSwitch GenderUIScaler::genderSwitch
	GenderSwitch_t696294539 * ___genderSwitch_14;
	// UISprite GenderUIScaler::tickSprite
	UISprite_t603616735 * ___tickSprite_15;
	// UILabel GenderUIScaler::maleLabel
	UILabel_t1795115428 * ___maleLabel_16;
	// UnityEngine.BoxCollider GenderUIScaler::maleCollider
	BoxCollider_t22920061 * ___maleCollider_17;
	// UISprite GenderUIScaler::maleDivider
	UISprite_t603616735 * ___maleDivider_18;
	// UILabel GenderUIScaler::femaleLabel
	UILabel_t1795115428 * ___femaleLabel_19;
	// UnityEngine.BoxCollider GenderUIScaler::femaleCollider
	BoxCollider_t22920061 * ___femaleCollider_20;
	// UISprite GenderUIScaler::femaleDivider
	UISprite_t603616735 * ___femaleDivider_21;

public:
	inline static int32_t get_offset_of_genderSwitch_14() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___genderSwitch_14)); }
	inline GenderSwitch_t696294539 * get_genderSwitch_14() const { return ___genderSwitch_14; }
	inline GenderSwitch_t696294539 ** get_address_of_genderSwitch_14() { return &___genderSwitch_14; }
	inline void set_genderSwitch_14(GenderSwitch_t696294539 * value)
	{
		___genderSwitch_14 = value;
		Il2CppCodeGenWriteBarrier(&___genderSwitch_14, value);
	}

	inline static int32_t get_offset_of_tickSprite_15() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___tickSprite_15)); }
	inline UISprite_t603616735 * get_tickSprite_15() const { return ___tickSprite_15; }
	inline UISprite_t603616735 ** get_address_of_tickSprite_15() { return &___tickSprite_15; }
	inline void set_tickSprite_15(UISprite_t603616735 * value)
	{
		___tickSprite_15 = value;
		Il2CppCodeGenWriteBarrier(&___tickSprite_15, value);
	}

	inline static int32_t get_offset_of_maleLabel_16() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___maleLabel_16)); }
	inline UILabel_t1795115428 * get_maleLabel_16() const { return ___maleLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_maleLabel_16() { return &___maleLabel_16; }
	inline void set_maleLabel_16(UILabel_t1795115428 * value)
	{
		___maleLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___maleLabel_16, value);
	}

	inline static int32_t get_offset_of_maleCollider_17() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___maleCollider_17)); }
	inline BoxCollider_t22920061 * get_maleCollider_17() const { return ___maleCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_maleCollider_17() { return &___maleCollider_17; }
	inline void set_maleCollider_17(BoxCollider_t22920061 * value)
	{
		___maleCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___maleCollider_17, value);
	}

	inline static int32_t get_offset_of_maleDivider_18() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___maleDivider_18)); }
	inline UISprite_t603616735 * get_maleDivider_18() const { return ___maleDivider_18; }
	inline UISprite_t603616735 ** get_address_of_maleDivider_18() { return &___maleDivider_18; }
	inline void set_maleDivider_18(UISprite_t603616735 * value)
	{
		___maleDivider_18 = value;
		Il2CppCodeGenWriteBarrier(&___maleDivider_18, value);
	}

	inline static int32_t get_offset_of_femaleLabel_19() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___femaleLabel_19)); }
	inline UILabel_t1795115428 * get_femaleLabel_19() const { return ___femaleLabel_19; }
	inline UILabel_t1795115428 ** get_address_of_femaleLabel_19() { return &___femaleLabel_19; }
	inline void set_femaleLabel_19(UILabel_t1795115428 * value)
	{
		___femaleLabel_19 = value;
		Il2CppCodeGenWriteBarrier(&___femaleLabel_19, value);
	}

	inline static int32_t get_offset_of_femaleCollider_20() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___femaleCollider_20)); }
	inline BoxCollider_t22920061 * get_femaleCollider_20() const { return ___femaleCollider_20; }
	inline BoxCollider_t22920061 ** get_address_of_femaleCollider_20() { return &___femaleCollider_20; }
	inline void set_femaleCollider_20(BoxCollider_t22920061 * value)
	{
		___femaleCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&___femaleCollider_20, value);
	}

	inline static int32_t get_offset_of_femaleDivider_21() { return static_cast<int32_t>(offsetof(GenderUIScaler_t2269357119, ___femaleDivider_21)); }
	inline UISprite_t603616735 * get_femaleDivider_21() const { return ___femaleDivider_21; }
	inline UISprite_t603616735 ** get_address_of_femaleDivider_21() { return &___femaleDivider_21; }
	inline void set_femaleDivider_21(UISprite_t603616735 * value)
	{
		___femaleDivider_21 = value;
		Il2CppCodeGenWriteBarrier(&___femaleDivider_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
