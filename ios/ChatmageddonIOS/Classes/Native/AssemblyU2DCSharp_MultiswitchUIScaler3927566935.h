﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<UISprite>
struct List_1_t4267705163;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiswitchUIScaler
struct  MultiswitchUIScaler_t3927566935  : public ChatmageddonUIScaler_t3374094653
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> MultiswitchUIScaler::leftTransforms
	List_1_t2644239190 * ___leftTransforms_14;
	// System.Collections.Generic.List`1<UnityEngine.Transform> MultiswitchUIScaler::rightTransforms
	List_1_t2644239190 * ___rightTransforms_15;
	// System.Collections.Generic.List`1<UISprite> MultiswitchUIScaler::dividers
	List_1_t4267705163 * ___dividers_16;

public:
	inline static int32_t get_offset_of_leftTransforms_14() { return static_cast<int32_t>(offsetof(MultiswitchUIScaler_t3927566935, ___leftTransforms_14)); }
	inline List_1_t2644239190 * get_leftTransforms_14() const { return ___leftTransforms_14; }
	inline List_1_t2644239190 ** get_address_of_leftTransforms_14() { return &___leftTransforms_14; }
	inline void set_leftTransforms_14(List_1_t2644239190 * value)
	{
		___leftTransforms_14 = value;
		Il2CppCodeGenWriteBarrier(&___leftTransforms_14, value);
	}

	inline static int32_t get_offset_of_rightTransforms_15() { return static_cast<int32_t>(offsetof(MultiswitchUIScaler_t3927566935, ___rightTransforms_15)); }
	inline List_1_t2644239190 * get_rightTransforms_15() const { return ___rightTransforms_15; }
	inline List_1_t2644239190 ** get_address_of_rightTransforms_15() { return &___rightTransforms_15; }
	inline void set_rightTransforms_15(List_1_t2644239190 * value)
	{
		___rightTransforms_15 = value;
		Il2CppCodeGenWriteBarrier(&___rightTransforms_15, value);
	}

	inline static int32_t get_offset_of_dividers_16() { return static_cast<int32_t>(offsetof(MultiswitchUIScaler_t3927566935, ___dividers_16)); }
	inline List_1_t4267705163 * get_dividers_16() const { return ___dividers_16; }
	inline List_1_t4267705163 ** get_address_of_dividers_16() { return &___dividers_16; }
	inline void set_dividers_16(List_1_t4267705163 * value)
	{
		___dividers_16 = value;
		Il2CppCodeGenWriteBarrier(&___dividers_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
