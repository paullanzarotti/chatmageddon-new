﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AvatarUpdater/<CheckAvatarLoaded>c__Iterator0
struct U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AvatarUpdater/<CheckAvatarLoaded>c__Iterator0::.ctor()
extern "C"  void U3CCheckAvatarLoadedU3Ec__Iterator0__ctor_m2135021877 (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AvatarUpdater/<CheckAvatarLoaded>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckAvatarLoadedU3Ec__Iterator0_MoveNext_m246109343 (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AvatarUpdater/<CheckAvatarLoaded>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckAvatarLoadedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1244811267 (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AvatarUpdater/<CheckAvatarLoaded>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckAvatarLoadedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m674041003 (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater/<CheckAvatarLoaded>c__Iterator0::Dispose()
extern "C"  void U3CCheckAvatarLoadedU3Ec__Iterator0_Dispose_m156707274 (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater/<CheckAvatarLoaded>c__Iterator0::Reset()
extern "C"  void U3CCheckAvatarLoadedU3Ec__Iterator0_Reset_m386457736 (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
