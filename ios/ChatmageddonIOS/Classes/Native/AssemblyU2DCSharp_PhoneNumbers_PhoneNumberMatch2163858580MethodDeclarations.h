﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberMatch
struct PhoneNumberMatch_t2163858580;
// System.String
struct String_t;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PhoneNumbers.PhoneNumberMatch::.ctor(System.Int32,System.String,PhoneNumbers.PhoneNumber)
extern "C"  void PhoneNumberMatch__ctor_m4065359083 (PhoneNumberMatch_t2163858580 * __this, int32_t ___start0, String_t* ___rawString1, PhoneNumber_t814071929 * ___number2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberMatch::get_Start()
extern "C"  int32_t PhoneNumberMatch_get_Start_m2799187184 (PhoneNumberMatch_t2163858580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberMatch::set_Start(System.Int32)
extern "C"  void PhoneNumberMatch_set_Start_m236282797 (PhoneNumberMatch_t2163858580 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberMatch::get_Length()
extern "C"  int32_t PhoneNumberMatch_get_Length_m1289493470 (PhoneNumberMatch_t2163858580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberMatch::get_RawString()
extern "C"  String_t* PhoneNumberMatch_get_RawString_m327483008 (PhoneNumberMatch_t2163858580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberMatch::set_RawString(System.String)
extern "C"  void PhoneNumberMatch_set_RawString_m4205701367 (PhoneNumberMatch_t2163858580 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberMatch::get_Number()
extern "C"  PhoneNumber_t814071929 * PhoneNumberMatch_get_Number_m2189180517 (PhoneNumberMatch_t2163858580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberMatch::set_Number(PhoneNumbers.PhoneNumber)
extern "C"  void PhoneNumberMatch_set_Number_m71428630 (PhoneNumberMatch_t2163858580 * __this, PhoneNumber_t814071929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatch::Equals(System.Object)
extern "C"  bool PhoneNumberMatch_Equals_m2995274382 (PhoneNumberMatch_t2163858580 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberMatch::GetHashCode()
extern "C"  int32_t PhoneNumberMatch_GetHashCode_m56146898 (PhoneNumberMatch_t2163858580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberMatch::ToString()
extern "C"  String_t* PhoneNumberMatch_ToString_m498058138 (PhoneNumberMatch_t2163858580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
