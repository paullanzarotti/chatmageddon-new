﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1603178222MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2104942386(__this, ___dictionary0, method) ((  void (*) (Enumerator_t942949160 *, Dictionary_2_t3917891754 *, const MethodInfo*))Enumerator__ctor_m1777707756_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2491942437(__this, method) ((  Il2CppObject * (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3540632177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2424980697(__this, method) ((  void (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m919527565_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3130040242(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m382383752_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2754811543(__this, method) ((  Il2CppObject * (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3716184131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3114560143(__this, method) ((  Il2CppObject * (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1529692723_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::MoveNext()
#define Enumerator_MoveNext_m2329327913(__this, method) ((  bool (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_MoveNext_m2401982917_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::get_Current()
#define Enumerator_get_Current_m1055211185(__this, method) ((  KeyValuePair_2_t1675236976  (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_get_Current_m2065485869_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3062307226(__this, method) ((  int32_t (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_get_CurrentKey_m1491320644_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m730395642(__this, method) ((  String_t* (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_get_CurrentValue_m4242886148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::Reset()
#define Enumerator_Reset_m2522263792(__this, method) ((  void (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_Reset_m2622064810_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::VerifyState()
#define Enumerator_VerifyState_m733867899(__this, method) ((  void (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_VerifyState_m3422172663_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1520057025(__this, method) ((  void (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_VerifyCurrent_m4229930837_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.String>::Dispose()
#define Enumerator_Dispose_m3196761202(__this, method) ((  void (*) (Enumerator_t942949160 *, const MethodInfo*))Enumerator_Dispose_m2279604588_gshared)(__this, method)
