﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CancelButton
struct CancelButton_t1146598418;

#include "codegen/il2cpp-codegen.h"

// System.Void CancelButton::.ctor()
extern "C"  void CancelButton__ctor_m1060777207 (CancelButton_t1146598418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CancelButton::OnButtonClick()
extern "C"  void CancelButton_OnButtonClick_m3986532298 (CancelButton_t1146598418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
