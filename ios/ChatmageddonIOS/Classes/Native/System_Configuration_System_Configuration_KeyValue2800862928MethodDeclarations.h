﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.KeyValueConfigurationCollection
struct KeyValueConfigurationCollection_t2800862928;
// System.Configuration.KeyValueConfigurationElement
struct KeyValueConfigurationElement_t1726605464;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.Object
struct Il2CppObject;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_KeyValue1726605464.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Void System.Configuration.KeyValueConfigurationCollection::.ctor()
extern "C"  void KeyValueConfigurationCollection__ctor_m437898655 (KeyValueConfigurationCollection_t2800862928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationCollection::Add(System.Configuration.KeyValueConfigurationElement)
extern "C"  void KeyValueConfigurationCollection_Add_m2546239573 (KeyValueConfigurationCollection_t2800862928 * __this, KeyValueConfigurationElement_t1726605464 * ___keyValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationCollection::Add(System.String,System.String)
extern "C"  void KeyValueConfigurationCollection_Add_m3146692432 (KeyValueConfigurationCollection_t2800862928 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationCollection::Clear()
extern "C"  void KeyValueConfigurationCollection_Clear_m662106292 (KeyValueConfigurationCollection_t2800862928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueConfigurationCollection::Remove(System.String)
extern "C"  void KeyValueConfigurationCollection_Remove_m966527099 (KeyValueConfigurationCollection_t2800862928 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Configuration.KeyValueConfigurationCollection::get_AllKeys()
extern "C"  StringU5BU5D_t1642385972* KeyValueConfigurationCollection_get_AllKeys_m1155801702 (KeyValueConfigurationCollection_t2800862928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.KeyValueConfigurationElement System.Configuration.KeyValueConfigurationCollection::get_Item(System.String)
extern "C"  KeyValueConfigurationElement_t1726605464 * KeyValueConfigurationCollection_get_Item_m4270059433 (KeyValueConfigurationCollection_t2800862928 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElement System.Configuration.KeyValueConfigurationCollection::CreateNewElement()
extern "C"  ConfigurationElement_t1776195828 * KeyValueConfigurationCollection_CreateNewElement_m1508827289 (KeyValueConfigurationCollection_t2800862928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.KeyValueConfigurationCollection::GetElementKey(System.Configuration.ConfigurationElement)
extern "C"  Il2CppObject * KeyValueConfigurationCollection_GetElementKey_m1384416082 (KeyValueConfigurationCollection_t2800862928 * __this, ConfigurationElement_t1776195828 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.KeyValueConfigurationCollection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * KeyValueConfigurationCollection_get_Properties_m1242504872 (KeyValueConfigurationCollection_t2800862928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.KeyValueConfigurationCollection::get_ThrowOnDuplicate()
extern "C"  bool KeyValueConfigurationCollection_get_ThrowOnDuplicate_m472585310 (KeyValueConfigurationCollection_t2800862928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
