﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int16>
struct DefaultComparer_t2530516147;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int16>::.ctor()
extern "C"  void DefaultComparer__ctor_m732320783_gshared (DefaultComparer_t2530516147 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m732320783(__this, method) ((  void (*) (DefaultComparer_t2530516147 *, const MethodInfo*))DefaultComparer__ctor_m732320783_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int16>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4156703908_gshared (DefaultComparer_t2530516147 * __this, int16_t ___x0, int16_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m4156703908(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2530516147 *, int16_t, int16_t, const MethodInfo*))DefaultComparer_Compare_m4156703908_gshared)(__this, ___x0, ___y1, method)
