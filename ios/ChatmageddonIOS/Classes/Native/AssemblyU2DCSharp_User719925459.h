﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;
// Shield
struct Shield_t3327121081;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// User
struct  User_t719925459  : public Il2CppObject
{
public:
	// System.String User::userName
	String_t* ___userName_0;
	// System.String User::fullName
	String_t* ___fullName_1;
	// System.String User::firstName
	String_t* ___firstName_2;
	// System.String User::secondName
	String_t* ___secondName_3;
	// System.String User::email
	String_t* ___email_4;
	// System.String User::ID
	String_t* ___ID_5;
	// System.String User::profileImageURL
	String_t* ___profileImageURL_6;
	// System.String User::rankName
	String_t* ___rankName_7;
	// System.Int32 User::rankLevel
	int32_t ___rankLevel_8;
	// Gender User::userGender
	int32_t ___userGender_9;
	// UnityEngine.Texture User::userAvatar
	Texture_t2243626319 * ___userAvatar_10;
	// System.Boolean User::loadingAvatar
	bool ___loadingAvatar_11;
	// Audience User::userAudience
	int32_t ___userAudience_12;
	// System.Boolean User::allowLocation
	bool ___allowLocation_13;
	// System.Single User::latitude
	float ___latitude_14;
	// System.Single User::longitude
	float ___longitude_15;
	// Shield User::currentMissileShield
	Shield_t3327121081 * ___currentMissileShield_16;
	// Shield User::currentMineShield
	Shield_t3327121081 * ___currentMineShield_17;
	// System.Boolean User::inStealth
	bool ___inStealth_18;
	// System.Int32 User::stealthDuration
	int32_t ___stealthDuration_19;
	// System.Int32 User::stealthStartHour
	int32_t ___stealthStartHour_20;
	// System.Int32 User::stealthStartMin
	int32_t ___stealthStartMin_21;
	// System.DateTime User::deactivatesStealthAt
	DateTime_t693205669  ___deactivatesStealthAt_22;
	// System.DateTime User::activatesStealthAt
	DateTime_t693205669  ___activatesStealthAt_23;
	// System.DateTime User::lastStealthReset
	DateTime_t693205669  ___lastStealthReset_24;
	// System.Int32 User::dailyPoints
	int32_t ___dailyPoints_25;
	// System.Int32 User::totalPoints
	int32_t ___totalPoints_26;
	// System.Boolean User::knockedOut
	bool ___knockedOut_27;
	// System.DateTime User::knockedOutUntil
	DateTime_t693205669  ___knockedOutUntil_28;
	// System.String User::knockedOutByID
	String_t* ___knockedOutByID_29;
	// System.String User::knockedOutByName
	String_t* ___knockedOutByName_30;
	// System.String User::mobileHash
	String_t* ___mobileHash_31;
	// System.Int32 User::missilesFired
	int32_t ___missilesFired_32;
	// System.Single User::missilesSuccessRate
	float ___missilesSuccessRate_33;
	// System.Int32 User::attackDirectHits
	int32_t ___attackDirectHits_34;
	// System.Int32 User::incomingAttacks
	int32_t ___incomingAttacks_35;
	// System.Int32 User::hitBy
	int32_t ___hitBy_36;
	// System.Single User::missilesDefenceRate
	float ___missilesDefenceRate_37;
	// System.Int32 User::minesLaid
	int32_t ___minesLaid_38;
	// System.Int32 User::minesTriggerred
	int32_t ___minesTriggerred_39;
	// System.Int32 User::minesDefused
	int32_t ___minesDefused_40;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(User_t719925459, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier(&___userName_0, value);
	}

	inline static int32_t get_offset_of_fullName_1() { return static_cast<int32_t>(offsetof(User_t719925459, ___fullName_1)); }
	inline String_t* get_fullName_1() const { return ___fullName_1; }
	inline String_t** get_address_of_fullName_1() { return &___fullName_1; }
	inline void set_fullName_1(String_t* value)
	{
		___fullName_1 = value;
		Il2CppCodeGenWriteBarrier(&___fullName_1, value);
	}

	inline static int32_t get_offset_of_firstName_2() { return static_cast<int32_t>(offsetof(User_t719925459, ___firstName_2)); }
	inline String_t* get_firstName_2() const { return ___firstName_2; }
	inline String_t** get_address_of_firstName_2() { return &___firstName_2; }
	inline void set_firstName_2(String_t* value)
	{
		___firstName_2 = value;
		Il2CppCodeGenWriteBarrier(&___firstName_2, value);
	}

	inline static int32_t get_offset_of_secondName_3() { return static_cast<int32_t>(offsetof(User_t719925459, ___secondName_3)); }
	inline String_t* get_secondName_3() const { return ___secondName_3; }
	inline String_t** get_address_of_secondName_3() { return &___secondName_3; }
	inline void set_secondName_3(String_t* value)
	{
		___secondName_3 = value;
		Il2CppCodeGenWriteBarrier(&___secondName_3, value);
	}

	inline static int32_t get_offset_of_email_4() { return static_cast<int32_t>(offsetof(User_t719925459, ___email_4)); }
	inline String_t* get_email_4() const { return ___email_4; }
	inline String_t** get_address_of_email_4() { return &___email_4; }
	inline void set_email_4(String_t* value)
	{
		___email_4 = value;
		Il2CppCodeGenWriteBarrier(&___email_4, value);
	}

	inline static int32_t get_offset_of_ID_5() { return static_cast<int32_t>(offsetof(User_t719925459, ___ID_5)); }
	inline String_t* get_ID_5() const { return ___ID_5; }
	inline String_t** get_address_of_ID_5() { return &___ID_5; }
	inline void set_ID_5(String_t* value)
	{
		___ID_5 = value;
		Il2CppCodeGenWriteBarrier(&___ID_5, value);
	}

	inline static int32_t get_offset_of_profileImageURL_6() { return static_cast<int32_t>(offsetof(User_t719925459, ___profileImageURL_6)); }
	inline String_t* get_profileImageURL_6() const { return ___profileImageURL_6; }
	inline String_t** get_address_of_profileImageURL_6() { return &___profileImageURL_6; }
	inline void set_profileImageURL_6(String_t* value)
	{
		___profileImageURL_6 = value;
		Il2CppCodeGenWriteBarrier(&___profileImageURL_6, value);
	}

	inline static int32_t get_offset_of_rankName_7() { return static_cast<int32_t>(offsetof(User_t719925459, ___rankName_7)); }
	inline String_t* get_rankName_7() const { return ___rankName_7; }
	inline String_t** get_address_of_rankName_7() { return &___rankName_7; }
	inline void set_rankName_7(String_t* value)
	{
		___rankName_7 = value;
		Il2CppCodeGenWriteBarrier(&___rankName_7, value);
	}

	inline static int32_t get_offset_of_rankLevel_8() { return static_cast<int32_t>(offsetof(User_t719925459, ___rankLevel_8)); }
	inline int32_t get_rankLevel_8() const { return ___rankLevel_8; }
	inline int32_t* get_address_of_rankLevel_8() { return &___rankLevel_8; }
	inline void set_rankLevel_8(int32_t value)
	{
		___rankLevel_8 = value;
	}

	inline static int32_t get_offset_of_userGender_9() { return static_cast<int32_t>(offsetof(User_t719925459, ___userGender_9)); }
	inline int32_t get_userGender_9() const { return ___userGender_9; }
	inline int32_t* get_address_of_userGender_9() { return &___userGender_9; }
	inline void set_userGender_9(int32_t value)
	{
		___userGender_9 = value;
	}

	inline static int32_t get_offset_of_userAvatar_10() { return static_cast<int32_t>(offsetof(User_t719925459, ___userAvatar_10)); }
	inline Texture_t2243626319 * get_userAvatar_10() const { return ___userAvatar_10; }
	inline Texture_t2243626319 ** get_address_of_userAvatar_10() { return &___userAvatar_10; }
	inline void set_userAvatar_10(Texture_t2243626319 * value)
	{
		___userAvatar_10 = value;
		Il2CppCodeGenWriteBarrier(&___userAvatar_10, value);
	}

	inline static int32_t get_offset_of_loadingAvatar_11() { return static_cast<int32_t>(offsetof(User_t719925459, ___loadingAvatar_11)); }
	inline bool get_loadingAvatar_11() const { return ___loadingAvatar_11; }
	inline bool* get_address_of_loadingAvatar_11() { return &___loadingAvatar_11; }
	inline void set_loadingAvatar_11(bool value)
	{
		___loadingAvatar_11 = value;
	}

	inline static int32_t get_offset_of_userAudience_12() { return static_cast<int32_t>(offsetof(User_t719925459, ___userAudience_12)); }
	inline int32_t get_userAudience_12() const { return ___userAudience_12; }
	inline int32_t* get_address_of_userAudience_12() { return &___userAudience_12; }
	inline void set_userAudience_12(int32_t value)
	{
		___userAudience_12 = value;
	}

	inline static int32_t get_offset_of_allowLocation_13() { return static_cast<int32_t>(offsetof(User_t719925459, ___allowLocation_13)); }
	inline bool get_allowLocation_13() const { return ___allowLocation_13; }
	inline bool* get_address_of_allowLocation_13() { return &___allowLocation_13; }
	inline void set_allowLocation_13(bool value)
	{
		___allowLocation_13 = value;
	}

	inline static int32_t get_offset_of_latitude_14() { return static_cast<int32_t>(offsetof(User_t719925459, ___latitude_14)); }
	inline float get_latitude_14() const { return ___latitude_14; }
	inline float* get_address_of_latitude_14() { return &___latitude_14; }
	inline void set_latitude_14(float value)
	{
		___latitude_14 = value;
	}

	inline static int32_t get_offset_of_longitude_15() { return static_cast<int32_t>(offsetof(User_t719925459, ___longitude_15)); }
	inline float get_longitude_15() const { return ___longitude_15; }
	inline float* get_address_of_longitude_15() { return &___longitude_15; }
	inline void set_longitude_15(float value)
	{
		___longitude_15 = value;
	}

	inline static int32_t get_offset_of_currentMissileShield_16() { return static_cast<int32_t>(offsetof(User_t719925459, ___currentMissileShield_16)); }
	inline Shield_t3327121081 * get_currentMissileShield_16() const { return ___currentMissileShield_16; }
	inline Shield_t3327121081 ** get_address_of_currentMissileShield_16() { return &___currentMissileShield_16; }
	inline void set_currentMissileShield_16(Shield_t3327121081 * value)
	{
		___currentMissileShield_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentMissileShield_16, value);
	}

	inline static int32_t get_offset_of_currentMineShield_17() { return static_cast<int32_t>(offsetof(User_t719925459, ___currentMineShield_17)); }
	inline Shield_t3327121081 * get_currentMineShield_17() const { return ___currentMineShield_17; }
	inline Shield_t3327121081 ** get_address_of_currentMineShield_17() { return &___currentMineShield_17; }
	inline void set_currentMineShield_17(Shield_t3327121081 * value)
	{
		___currentMineShield_17 = value;
		Il2CppCodeGenWriteBarrier(&___currentMineShield_17, value);
	}

	inline static int32_t get_offset_of_inStealth_18() { return static_cast<int32_t>(offsetof(User_t719925459, ___inStealth_18)); }
	inline bool get_inStealth_18() const { return ___inStealth_18; }
	inline bool* get_address_of_inStealth_18() { return &___inStealth_18; }
	inline void set_inStealth_18(bool value)
	{
		___inStealth_18 = value;
	}

	inline static int32_t get_offset_of_stealthDuration_19() { return static_cast<int32_t>(offsetof(User_t719925459, ___stealthDuration_19)); }
	inline int32_t get_stealthDuration_19() const { return ___stealthDuration_19; }
	inline int32_t* get_address_of_stealthDuration_19() { return &___stealthDuration_19; }
	inline void set_stealthDuration_19(int32_t value)
	{
		___stealthDuration_19 = value;
	}

	inline static int32_t get_offset_of_stealthStartHour_20() { return static_cast<int32_t>(offsetof(User_t719925459, ___stealthStartHour_20)); }
	inline int32_t get_stealthStartHour_20() const { return ___stealthStartHour_20; }
	inline int32_t* get_address_of_stealthStartHour_20() { return &___stealthStartHour_20; }
	inline void set_stealthStartHour_20(int32_t value)
	{
		___stealthStartHour_20 = value;
	}

	inline static int32_t get_offset_of_stealthStartMin_21() { return static_cast<int32_t>(offsetof(User_t719925459, ___stealthStartMin_21)); }
	inline int32_t get_stealthStartMin_21() const { return ___stealthStartMin_21; }
	inline int32_t* get_address_of_stealthStartMin_21() { return &___stealthStartMin_21; }
	inline void set_stealthStartMin_21(int32_t value)
	{
		___stealthStartMin_21 = value;
	}

	inline static int32_t get_offset_of_deactivatesStealthAt_22() { return static_cast<int32_t>(offsetof(User_t719925459, ___deactivatesStealthAt_22)); }
	inline DateTime_t693205669  get_deactivatesStealthAt_22() const { return ___deactivatesStealthAt_22; }
	inline DateTime_t693205669 * get_address_of_deactivatesStealthAt_22() { return &___deactivatesStealthAt_22; }
	inline void set_deactivatesStealthAt_22(DateTime_t693205669  value)
	{
		___deactivatesStealthAt_22 = value;
	}

	inline static int32_t get_offset_of_activatesStealthAt_23() { return static_cast<int32_t>(offsetof(User_t719925459, ___activatesStealthAt_23)); }
	inline DateTime_t693205669  get_activatesStealthAt_23() const { return ___activatesStealthAt_23; }
	inline DateTime_t693205669 * get_address_of_activatesStealthAt_23() { return &___activatesStealthAt_23; }
	inline void set_activatesStealthAt_23(DateTime_t693205669  value)
	{
		___activatesStealthAt_23 = value;
	}

	inline static int32_t get_offset_of_lastStealthReset_24() { return static_cast<int32_t>(offsetof(User_t719925459, ___lastStealthReset_24)); }
	inline DateTime_t693205669  get_lastStealthReset_24() const { return ___lastStealthReset_24; }
	inline DateTime_t693205669 * get_address_of_lastStealthReset_24() { return &___lastStealthReset_24; }
	inline void set_lastStealthReset_24(DateTime_t693205669  value)
	{
		___lastStealthReset_24 = value;
	}

	inline static int32_t get_offset_of_dailyPoints_25() { return static_cast<int32_t>(offsetof(User_t719925459, ___dailyPoints_25)); }
	inline int32_t get_dailyPoints_25() const { return ___dailyPoints_25; }
	inline int32_t* get_address_of_dailyPoints_25() { return &___dailyPoints_25; }
	inline void set_dailyPoints_25(int32_t value)
	{
		___dailyPoints_25 = value;
	}

	inline static int32_t get_offset_of_totalPoints_26() { return static_cast<int32_t>(offsetof(User_t719925459, ___totalPoints_26)); }
	inline int32_t get_totalPoints_26() const { return ___totalPoints_26; }
	inline int32_t* get_address_of_totalPoints_26() { return &___totalPoints_26; }
	inline void set_totalPoints_26(int32_t value)
	{
		___totalPoints_26 = value;
	}

	inline static int32_t get_offset_of_knockedOut_27() { return static_cast<int32_t>(offsetof(User_t719925459, ___knockedOut_27)); }
	inline bool get_knockedOut_27() const { return ___knockedOut_27; }
	inline bool* get_address_of_knockedOut_27() { return &___knockedOut_27; }
	inline void set_knockedOut_27(bool value)
	{
		___knockedOut_27 = value;
	}

	inline static int32_t get_offset_of_knockedOutUntil_28() { return static_cast<int32_t>(offsetof(User_t719925459, ___knockedOutUntil_28)); }
	inline DateTime_t693205669  get_knockedOutUntil_28() const { return ___knockedOutUntil_28; }
	inline DateTime_t693205669 * get_address_of_knockedOutUntil_28() { return &___knockedOutUntil_28; }
	inline void set_knockedOutUntil_28(DateTime_t693205669  value)
	{
		___knockedOutUntil_28 = value;
	}

	inline static int32_t get_offset_of_knockedOutByID_29() { return static_cast<int32_t>(offsetof(User_t719925459, ___knockedOutByID_29)); }
	inline String_t* get_knockedOutByID_29() const { return ___knockedOutByID_29; }
	inline String_t** get_address_of_knockedOutByID_29() { return &___knockedOutByID_29; }
	inline void set_knockedOutByID_29(String_t* value)
	{
		___knockedOutByID_29 = value;
		Il2CppCodeGenWriteBarrier(&___knockedOutByID_29, value);
	}

	inline static int32_t get_offset_of_knockedOutByName_30() { return static_cast<int32_t>(offsetof(User_t719925459, ___knockedOutByName_30)); }
	inline String_t* get_knockedOutByName_30() const { return ___knockedOutByName_30; }
	inline String_t** get_address_of_knockedOutByName_30() { return &___knockedOutByName_30; }
	inline void set_knockedOutByName_30(String_t* value)
	{
		___knockedOutByName_30 = value;
		Il2CppCodeGenWriteBarrier(&___knockedOutByName_30, value);
	}

	inline static int32_t get_offset_of_mobileHash_31() { return static_cast<int32_t>(offsetof(User_t719925459, ___mobileHash_31)); }
	inline String_t* get_mobileHash_31() const { return ___mobileHash_31; }
	inline String_t** get_address_of_mobileHash_31() { return &___mobileHash_31; }
	inline void set_mobileHash_31(String_t* value)
	{
		___mobileHash_31 = value;
		Il2CppCodeGenWriteBarrier(&___mobileHash_31, value);
	}

	inline static int32_t get_offset_of_missilesFired_32() { return static_cast<int32_t>(offsetof(User_t719925459, ___missilesFired_32)); }
	inline int32_t get_missilesFired_32() const { return ___missilesFired_32; }
	inline int32_t* get_address_of_missilesFired_32() { return &___missilesFired_32; }
	inline void set_missilesFired_32(int32_t value)
	{
		___missilesFired_32 = value;
	}

	inline static int32_t get_offset_of_missilesSuccessRate_33() { return static_cast<int32_t>(offsetof(User_t719925459, ___missilesSuccessRate_33)); }
	inline float get_missilesSuccessRate_33() const { return ___missilesSuccessRate_33; }
	inline float* get_address_of_missilesSuccessRate_33() { return &___missilesSuccessRate_33; }
	inline void set_missilesSuccessRate_33(float value)
	{
		___missilesSuccessRate_33 = value;
	}

	inline static int32_t get_offset_of_attackDirectHits_34() { return static_cast<int32_t>(offsetof(User_t719925459, ___attackDirectHits_34)); }
	inline int32_t get_attackDirectHits_34() const { return ___attackDirectHits_34; }
	inline int32_t* get_address_of_attackDirectHits_34() { return &___attackDirectHits_34; }
	inline void set_attackDirectHits_34(int32_t value)
	{
		___attackDirectHits_34 = value;
	}

	inline static int32_t get_offset_of_incomingAttacks_35() { return static_cast<int32_t>(offsetof(User_t719925459, ___incomingAttacks_35)); }
	inline int32_t get_incomingAttacks_35() const { return ___incomingAttacks_35; }
	inline int32_t* get_address_of_incomingAttacks_35() { return &___incomingAttacks_35; }
	inline void set_incomingAttacks_35(int32_t value)
	{
		___incomingAttacks_35 = value;
	}

	inline static int32_t get_offset_of_hitBy_36() { return static_cast<int32_t>(offsetof(User_t719925459, ___hitBy_36)); }
	inline int32_t get_hitBy_36() const { return ___hitBy_36; }
	inline int32_t* get_address_of_hitBy_36() { return &___hitBy_36; }
	inline void set_hitBy_36(int32_t value)
	{
		___hitBy_36 = value;
	}

	inline static int32_t get_offset_of_missilesDefenceRate_37() { return static_cast<int32_t>(offsetof(User_t719925459, ___missilesDefenceRate_37)); }
	inline float get_missilesDefenceRate_37() const { return ___missilesDefenceRate_37; }
	inline float* get_address_of_missilesDefenceRate_37() { return &___missilesDefenceRate_37; }
	inline void set_missilesDefenceRate_37(float value)
	{
		___missilesDefenceRate_37 = value;
	}

	inline static int32_t get_offset_of_minesLaid_38() { return static_cast<int32_t>(offsetof(User_t719925459, ___minesLaid_38)); }
	inline int32_t get_minesLaid_38() const { return ___minesLaid_38; }
	inline int32_t* get_address_of_minesLaid_38() { return &___minesLaid_38; }
	inline void set_minesLaid_38(int32_t value)
	{
		___minesLaid_38 = value;
	}

	inline static int32_t get_offset_of_minesTriggerred_39() { return static_cast<int32_t>(offsetof(User_t719925459, ___minesTriggerred_39)); }
	inline int32_t get_minesTriggerred_39() const { return ___minesTriggerred_39; }
	inline int32_t* get_address_of_minesTriggerred_39() { return &___minesTriggerred_39; }
	inline void set_minesTriggerred_39(int32_t value)
	{
		___minesTriggerred_39 = value;
	}

	inline static int32_t get_offset_of_minesDefused_40() { return static_cast<int32_t>(offsetof(User_t719925459, ___minesDefused_40)); }
	inline int32_t get_minesDefused_40() const { return ___minesDefused_40; }
	inline int32_t* get_address_of_minesDefused_40() { return &___minesDefused_40; }
	inline void set_minesDefused_40(int32_t value)
	{
		___minesDefused_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
