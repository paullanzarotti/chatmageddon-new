﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Player
struct Player_t1147783557;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AddressBook
struct AddressBook_t411411037;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1347319308.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t1596653588  : public MonoSingleton_1_t1347319308
{
public:
	// Player PlayerManager::user
	Player_t1147783557 * ___user_3;
	// System.Int32 PlayerManager::totalFuel
	int32_t ___totalFuel_4;
	// MapState PlayerManager::chosenMapState
	int32_t ___chosenMapState_5;
	// UnityEngine.GameObject PlayerManager::addressBookObject
	GameObject_t1756533147 * ___addressBookObject_6;
	// AddressBook PlayerManager::addressBook
	AddressBook_t411411037 * ___addressBook_7;
	// System.Boolean PlayerManager::avatarLoadedFromServer
	bool ___avatarLoadedFromServer_8;

public:
	inline static int32_t get_offset_of_user_3() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___user_3)); }
	inline Player_t1147783557 * get_user_3() const { return ___user_3; }
	inline Player_t1147783557 ** get_address_of_user_3() { return &___user_3; }
	inline void set_user_3(Player_t1147783557 * value)
	{
		___user_3 = value;
		Il2CppCodeGenWriteBarrier(&___user_3, value);
	}

	inline static int32_t get_offset_of_totalFuel_4() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___totalFuel_4)); }
	inline int32_t get_totalFuel_4() const { return ___totalFuel_4; }
	inline int32_t* get_address_of_totalFuel_4() { return &___totalFuel_4; }
	inline void set_totalFuel_4(int32_t value)
	{
		___totalFuel_4 = value;
	}

	inline static int32_t get_offset_of_chosenMapState_5() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___chosenMapState_5)); }
	inline int32_t get_chosenMapState_5() const { return ___chosenMapState_5; }
	inline int32_t* get_address_of_chosenMapState_5() { return &___chosenMapState_5; }
	inline void set_chosenMapState_5(int32_t value)
	{
		___chosenMapState_5 = value;
	}

	inline static int32_t get_offset_of_addressBookObject_6() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___addressBookObject_6)); }
	inline GameObject_t1756533147 * get_addressBookObject_6() const { return ___addressBookObject_6; }
	inline GameObject_t1756533147 ** get_address_of_addressBookObject_6() { return &___addressBookObject_6; }
	inline void set_addressBookObject_6(GameObject_t1756533147 * value)
	{
		___addressBookObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___addressBookObject_6, value);
	}

	inline static int32_t get_offset_of_addressBook_7() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___addressBook_7)); }
	inline AddressBook_t411411037 * get_addressBook_7() const { return ___addressBook_7; }
	inline AddressBook_t411411037 ** get_address_of_addressBook_7() { return &___addressBook_7; }
	inline void set_addressBook_7(AddressBook_t411411037 * value)
	{
		___addressBook_7 = value;
		Il2CppCodeGenWriteBarrier(&___addressBook_7, value);
	}

	inline static int32_t get_offset_of_avatarLoadedFromServer_8() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___avatarLoadedFromServer_8)); }
	inline bool get_avatarLoadedFromServer_8() const { return ___avatarLoadedFromServer_8; }
	inline bool* get_address_of_avatarLoadedFromServer_8() { return &___avatarLoadedFromServer_8; }
	inline void set_avatarLoadedFromServer_8(bool value)
	{
		___avatarLoadedFromServer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
