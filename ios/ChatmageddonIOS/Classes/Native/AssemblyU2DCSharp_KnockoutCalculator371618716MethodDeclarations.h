﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KnockoutCalculator
struct KnockoutCalculator_t371618716;
// KnockoutCalculator/OnZeroHit
struct OnZeroHit_t581944179;
// User
struct User_t719925459;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_KnockoutCalculator_OnZeroHit581944179.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void KnockoutCalculator::.ctor()
extern "C"  void KnockoutCalculator__ctor_m3743883395 (KnockoutCalculator_t371618716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::add_onZeroHit(KnockoutCalculator/OnZeroHit)
extern "C"  void KnockoutCalculator_add_onZeroHit_m3237163418 (KnockoutCalculator_t371618716 * __this, OnZeroHit_t581944179 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::remove_onZeroHit(KnockoutCalculator/OnZeroHit)
extern "C"  void KnockoutCalculator_remove_onZeroHit_m1184591441 (KnockoutCalculator_t371618716 * __this, OnZeroHit_t581944179 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::StartCalculator(User)
extern "C"  void KnockoutCalculator_StartCalculator_m202198318 (KnockoutCalculator_t371618716 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::StopCalculator()
extern "C"  void KnockoutCalculator_StopCalculator_m3554069023 (KnockoutCalculator_t371618716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KnockoutCalculator::CalculateKnockoutTime(User)
extern "C"  Il2CppObject * KnockoutCalculator_CalculateKnockoutTime_m3956499257 (KnockoutCalculator_t371618716 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::SetTimeLabel(System.TimeSpan)
extern "C"  void KnockoutCalculator_SetTimeLabel_m638805264 (KnockoutCalculator_t371618716 * __this, TimeSpan_t3430258949  ___span0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::SetTimeToPending()
extern "C"  void KnockoutCalculator_SetTimeToPending_m898478372 (KnockoutCalculator_t371618716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnockoutCalculator::SendZeroHitEvent(System.Boolean)
extern "C"  void KnockoutCalculator_SendZeroHitEvent_m558610335 (KnockoutCalculator_t371618716 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
