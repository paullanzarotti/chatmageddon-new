﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectMissileButton
struct SelectMissileButton_t3959346244;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectMissileButton::.ctor()
extern "C"  void SelectMissileButton__ctor_m2736902445 (SelectMissileButton_t3959346244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectMissileButton::SetButtonActive(System.Int32,System.Boolean)
extern "C"  void SelectMissileButton_SetButtonActive_m659600479 (SelectMissileButton_t3959346244 * __this, int32_t ___inventoryAmount0, bool ___locked1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectMissileButton::OnButtonClick()
extern "C"  void SelectMissileButton_OnButtonClick_m4093390152 (SelectMissileButton_t3959346244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
