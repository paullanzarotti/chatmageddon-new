﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioListener
struct AudioListener_t1996719162;
// System.Collections.Generic.List`1<SFXSource>
struct List_1_t4047102090;
// System.Collections.Generic.List`1<MusicSource>
struct List_1_t273302338;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen517622716.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager`1<ChatmageddonAudioManager>
struct  AudioManager_1_t1777033667  : public MonoSingleton_1_t517622716
{
public:
	// System.Boolean AudioManager`1::audioOn
	bool ___audioOn_3;
	// System.Boolean AudioManager`1::musicOn
	bool ___musicOn_4;
	// System.Boolean AudioManager`1::sfxOn
	bool ___sfxOn_5;
	// UnityEngine.AudioListener AudioManager`1::listener
	AudioListener_t1996719162 * ___listener_6;
	// System.Collections.Generic.List`1<SFXSource> AudioManager`1::sfxObjects
	List_1_t4047102090 * ___sfxObjects_7;
	// System.Collections.Generic.List`1<MusicSource> AudioManager`1::musicObjects
	List_1_t273302338 * ___musicObjects_8;
	// System.Boolean AudioManager`1::objectsInit
	bool ___objectsInit_9;
	// System.String AudioManager`1::resourcePath
	String_t* ___resourcePath_10;
	// UnityEngine.GameObject AudioManager`1::sfxParent
	GameObject_t1756533147 * ___sfxParent_11;
	// UnityEngine.GameObject AudioManager`1::musicParent
	GameObject_t1756533147 * ___musicParent_12;

public:
	inline static int32_t get_offset_of_audioOn_3() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___audioOn_3)); }
	inline bool get_audioOn_3() const { return ___audioOn_3; }
	inline bool* get_address_of_audioOn_3() { return &___audioOn_3; }
	inline void set_audioOn_3(bool value)
	{
		___audioOn_3 = value;
	}

	inline static int32_t get_offset_of_musicOn_4() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___musicOn_4)); }
	inline bool get_musicOn_4() const { return ___musicOn_4; }
	inline bool* get_address_of_musicOn_4() { return &___musicOn_4; }
	inline void set_musicOn_4(bool value)
	{
		___musicOn_4 = value;
	}

	inline static int32_t get_offset_of_sfxOn_5() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___sfxOn_5)); }
	inline bool get_sfxOn_5() const { return ___sfxOn_5; }
	inline bool* get_address_of_sfxOn_5() { return &___sfxOn_5; }
	inline void set_sfxOn_5(bool value)
	{
		___sfxOn_5 = value;
	}

	inline static int32_t get_offset_of_listener_6() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___listener_6)); }
	inline AudioListener_t1996719162 * get_listener_6() const { return ___listener_6; }
	inline AudioListener_t1996719162 ** get_address_of_listener_6() { return &___listener_6; }
	inline void set_listener_6(AudioListener_t1996719162 * value)
	{
		___listener_6 = value;
		Il2CppCodeGenWriteBarrier(&___listener_6, value);
	}

	inline static int32_t get_offset_of_sfxObjects_7() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___sfxObjects_7)); }
	inline List_1_t4047102090 * get_sfxObjects_7() const { return ___sfxObjects_7; }
	inline List_1_t4047102090 ** get_address_of_sfxObjects_7() { return &___sfxObjects_7; }
	inline void set_sfxObjects_7(List_1_t4047102090 * value)
	{
		___sfxObjects_7 = value;
		Il2CppCodeGenWriteBarrier(&___sfxObjects_7, value);
	}

	inline static int32_t get_offset_of_musicObjects_8() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___musicObjects_8)); }
	inline List_1_t273302338 * get_musicObjects_8() const { return ___musicObjects_8; }
	inline List_1_t273302338 ** get_address_of_musicObjects_8() { return &___musicObjects_8; }
	inline void set_musicObjects_8(List_1_t273302338 * value)
	{
		___musicObjects_8 = value;
		Il2CppCodeGenWriteBarrier(&___musicObjects_8, value);
	}

	inline static int32_t get_offset_of_objectsInit_9() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___objectsInit_9)); }
	inline bool get_objectsInit_9() const { return ___objectsInit_9; }
	inline bool* get_address_of_objectsInit_9() { return &___objectsInit_9; }
	inline void set_objectsInit_9(bool value)
	{
		___objectsInit_9 = value;
	}

	inline static int32_t get_offset_of_resourcePath_10() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___resourcePath_10)); }
	inline String_t* get_resourcePath_10() const { return ___resourcePath_10; }
	inline String_t** get_address_of_resourcePath_10() { return &___resourcePath_10; }
	inline void set_resourcePath_10(String_t* value)
	{
		___resourcePath_10 = value;
		Il2CppCodeGenWriteBarrier(&___resourcePath_10, value);
	}

	inline static int32_t get_offset_of_sfxParent_11() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___sfxParent_11)); }
	inline GameObject_t1756533147 * get_sfxParent_11() const { return ___sfxParent_11; }
	inline GameObject_t1756533147 ** get_address_of_sfxParent_11() { return &___sfxParent_11; }
	inline void set_sfxParent_11(GameObject_t1756533147 * value)
	{
		___sfxParent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sfxParent_11, value);
	}

	inline static int32_t get_offset_of_musicParent_12() { return static_cast<int32_t>(offsetof(AudioManager_1_t1777033667, ___musicParent_12)); }
	inline GameObject_t1756533147 * get_musicParent_12() const { return ___musicParent_12; }
	inline GameObject_t1756533147 ** get_address_of_musicParent_12() { return &___musicParent_12; }
	inline void set_musicParent_12(GameObject_t1756533147 * value)
	{
		___musicParent_12 = value;
		Il2CppCodeGenWriteBarrier(&___musicParent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
