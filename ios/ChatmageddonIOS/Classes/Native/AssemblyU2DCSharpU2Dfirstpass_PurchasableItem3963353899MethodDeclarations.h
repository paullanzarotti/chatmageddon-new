﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.String>
struct Dictionary_2_t3917891754;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void PurchasableItem::.ctor()
extern "C"  void PurchasableItem__ctor_m3471757644 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::.ctor(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Unibill.Impl.BillingPlatform)
extern "C"  void PurchasableItem__ctor_m1898277114 (PurchasableItem_t3963353899 * __this, String_t* ___id0, Dictionary_2_t309261261 * ___hash1, int32_t ___platform2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PurchasableItem::get_AvailableToPurchase()
extern "C"  bool PurchasableItem_get_AvailableToPurchase_m3164915952 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_AvailableToPurchase(System.Boolean)
extern "C"  void PurchasableItem_set_AvailableToPurchase_m1595759799 (PurchasableItem_t3963353899 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_Id()
extern "C"  String_t* PurchasableItem_get_Id_m1681277861 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_Id(System.String)
extern "C"  void PurchasableItem_set_Id_m2646856872 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchaseType PurchasableItem::get_PurchaseType()
extern "C"  int32_t PurchasableItem_get_PurchaseType_m1696585922 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_PurchaseType(PurchaseType)
extern "C"  void PurchasableItem_set_PurchaseType_m3232309393 (PurchasableItem_t3963353899 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_name()
extern "C"  String_t* PurchasableItem_get_name_m784179687 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_name(System.String)
extern "C"  void PurchasableItem_set_name_m405702134 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_description()
extern "C"  String_t* PurchasableItem_get_description_m3503355048 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_description(System.String)
extern "C"  void PurchasableItem_set_description_m2654249289 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal PurchasableItem::get_localizedPrice()
extern "C"  Decimal_t724701077  PurchasableItem_get_localizedPrice_m1546430596 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_localizedPrice(System.Decimal)
extern "C"  void PurchasableItem_set_localizedPrice_m2864814203 (PurchasableItem_t3963353899 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_localizedPriceString()
extern "C"  String_t* PurchasableItem_get_localizedPriceString_m807780071 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_localizedPriceString(System.String)
extern "C"  void PurchasableItem_set_localizedPriceString_m1583907382 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_localizedTitle()
extern "C"  String_t* PurchasableItem_get_localizedTitle_m3955193363 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_localizedTitle(System.String)
extern "C"  void PurchasableItem_set_localizedTitle_m3513083700 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_localizedDescription()
extern "C"  String_t* PurchasableItem_get_localizedDescription_m192317433 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_localizedDescription(System.String)
extern "C"  void PurchasableItem_set_localizedDescription_m323307356 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_isoCurrencySymbol()
extern "C"  String_t* PurchasableItem_get_isoCurrencySymbol_m523608680 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_isoCurrencySymbol(System.String)
extern "C"  void PurchasableItem_set_isoCurrencySymbol_m3015768875 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal PurchasableItem::get_priceInLocalCurrency()
extern "C"  Decimal_t724701077  PurchasableItem_get_priceInLocalCurrency_m380071648 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_priceInLocalCurrency(System.Decimal)
extern "C"  void PurchasableItem_set_priceInLocalCurrency_m3786969997 (PurchasableItem_t3963353899 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PurchasableItem::get_hasDownloadableContent()
extern "C"  bool PurchasableItem_get_hasDownloadableContent_m711503796 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_downloadableContentId()
extern "C"  String_t* PurchasableItem_get_downloadableContentId_m2903865246 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_downloadableContentId(System.String)
extern "C"  void PurchasableItem_set_downloadableContentId_m2029746313 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_LocalId()
extern "C"  String_t* PurchasableItem_get_LocalId_m1950095224 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PurchasableItem::get_receipt()
extern "C"  String_t* PurchasableItem_get_receipt_m219824718 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_receipt(System.String)
extern "C"  void PurchasableItem_set_receipt_m1670140175 (PurchasableItem_t3963353899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.String> PurchasableItem::get_LocalIds()
extern "C"  Dictionary_2_t3917891754 * PurchasableItem_get_LocalIds_m3475230577 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::set_LocalIds(System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.String>)
extern "C"  void PurchasableItem_set_LocalIds_m1694799474 (PurchasableItem_t3963353899 * __this, Dictionary_2_t3917891754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PurchasableItem::Deserialize(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void PurchasableItem_Deserialize_m1343263228 (PurchasableItem_t3963353899 * __this, Dictionary_2_t309261261 * ___hash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> PurchasableItem::Serialize()
extern "C"  Dictionary_2_t309261261 * PurchasableItem_Serialize_m2661342710 (PurchasableItem_t3963353899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PurchasableItem::Equals(PurchasableItem)
extern "C"  bool PurchasableItem_Equals_m2343005104 (PurchasableItem_t3963353899 * __this, PurchasableItem_t3963353899 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
