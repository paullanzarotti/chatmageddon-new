﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>
struct IEnumerable_1_t845948095;
// System.Xml.Linq.XDocument
struct XDocument_t2733326047;
// System.String
struct String_t;
// System.Xml.Linq.XElement
struct XElement_t553821050;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XDocument2733326047.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XElement553821050.h"

// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> libphonenumber_csharp_portable.XDocumentLegacy::GetElementsByTagName(System.Xml.Linq.XDocument,System.String)
extern "C"  Il2CppObject* XDocumentLegacy_GetElementsByTagName_m3170978269 (Il2CppObject * __this /* static, unused */, XDocument_t2733326047 * ___document0, String_t* ___tagName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> libphonenumber_csharp_portable.XDocumentLegacy::GetElementsByTagName(System.Xml.Linq.XElement,System.String)
extern "C"  Il2CppObject* XDocumentLegacy_GetElementsByTagName_m4156346258 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___document0, String_t* ___tagName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean libphonenumber_csharp_portable.XDocumentLegacy::HasAttribute(System.Xml.Linq.XElement,System.String)
extern "C"  bool XDocumentLegacy_HasAttribute_m1054411721 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___element0, String_t* ___attribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String libphonenumber_csharp_portable.XDocumentLegacy::GetAttribute(System.Xml.Linq.XElement,System.String)
extern "C"  String_t* XDocumentLegacy_GetAttribute_m1651820978 (Il2CppObject * __this /* static, unused */, XElement_t553821050 * ___element0, String_t* ___attribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
