﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountdownTimer
struct CountdownTimer_t4123051008;
// CountdownTimer/OnTimerFinished
struct OnTimerFinished_t1234642247;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CountdownTimer_OnTimerFinished1234642247.h"

// System.Void CountdownTimer::.ctor()
extern "C"  void CountdownTimer__ctor_m1695761161 (CountdownTimer_t4123051008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::add_onTimerFinished(CountdownTimer/OnTimerFinished)
extern "C"  void CountdownTimer_add_onTimerFinished_m3384575202 (CountdownTimer_t4123051008 * __this, OnTimerFinished_t1234642247 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::remove_onTimerFinished(CountdownTimer/OnTimerFinished)
extern "C"  void CountdownTimer_remove_onTimerFinished_m3951314785 (CountdownTimer_t4123051008 * __this, OnTimerFinished_t1234642247 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::TransitionToPercent(System.Single,System.Single)
extern "C"  void CountdownTimer_TransitionToPercent_m2557501446 (CountdownTimer_t4123051008 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::ProgressUpdated()
extern "C"  void CountdownTimer_ProgressUpdated_m4057334781 (CountdownTimer_t4123051008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::ProgressFinished()
extern "C"  void CountdownTimer_ProgressFinished_m700538458 (CountdownTimer_t4123051008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::StartCountDownTimer(System.Single)
extern "C"  void CountdownTimer_StartCountDownTimer_m1797631624 (CountdownTimer_t4123051008 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
