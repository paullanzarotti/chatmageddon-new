﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickerPopper/<OpenPicker>c__Iterator0
struct U3COpenPickerU3Ec__Iterator0_t2620335406;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PickerPopper/<OpenPicker>c__Iterator0::.ctor()
extern "C"  void U3COpenPickerU3Ec__Iterator0__ctor_m1013054051 (U3COpenPickerU3Ec__Iterator0_t2620335406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickerPopper/<OpenPicker>c__Iterator0::MoveNext()
extern "C"  bool U3COpenPickerU3Ec__Iterator0_MoveNext_m1347873769 (U3COpenPickerU3Ec__Iterator0_t2620335406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<OpenPicker>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COpenPickerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4165904809 (U3COpenPickerU3Ec__Iterator0_t2620335406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<OpenPicker>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COpenPickerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2476261393 (U3COpenPickerU3Ec__Iterator0_t2620335406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<OpenPicker>c__Iterator0::Dispose()
extern "C"  void U3COpenPickerU3Ec__Iterator0_Dispose_m550256256 (U3COpenPickerU3Ec__Iterator0_t2620335406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<OpenPicker>c__Iterator0::Reset()
extern "C"  void U3COpenPickerU3Ec__Iterator0_Reset_m909385518 (U3COpenPickerU3Ec__Iterator0_t2620335406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
