﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PUToastUI
struct PUToastUI_t4217418532;
// Bread
struct Bread_t457172030;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Bread457172030.h"

// System.Void PUToastUI::.ctor()
extern "C"  void PUToastUI__ctor_m783760827 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::OnAwake()
extern "C"  void PUToastUI_OnAwake_m1815773879 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::LoadToastUI(Bread)
extern "C"  void PUToastUI_LoadToastUI_m2058231554 (PUToastUI_t4217418532 * __this, Bread_t457172030 * ___bread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::ScaleBackground()
extern "C"  void PUToastUI_ScaleBackground_m101271741 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::OffsetContent()
extern "C"  void PUToastUI_OffsetContent_m3734299307 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::OnScaleTweenFinished()
extern "C"  void PUToastUI_OnScaleTweenFinished_m1766205237 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PUToastUI::DisplayWait()
extern "C"  Il2CppObject * PUToastUI_DisplayWait_m1437913980 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::UnloadToastUI()
extern "C"  void PUToastUI_UnloadToastUI_m2799694995 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PUToastUI::OnAlphaFadeComplete()
extern "C"  void PUToastUI_OnAlphaFadeComplete_m2673009533 (PUToastUI_t4217418532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
