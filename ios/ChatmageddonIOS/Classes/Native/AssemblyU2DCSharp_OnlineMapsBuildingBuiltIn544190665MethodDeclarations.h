﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuildingBuiltIn
struct OnlineMapsBuildingBuiltIn_t544190665;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// OnlineMapsOSMWay
struct OnlineMapsOSMWay_t3319895272;
// UnityEngine.Material
struct Material_t193706927;
// OnlineMapsBuildingBase
struct OnlineMapsBuildingBase_t650727021;
// OnlineMapsBuildings
struct OnlineMapsBuildings_t3411229051;
// System.Collections.Generic.Dictionary`2<System.String,OnlineMapsOSMNode>
struct Dictionary_2_t1003802369;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String
struct String_t;
// OnlineMapsBuildingMaterial
struct OnlineMapsBuildingMaterial_t302108695;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMWay3319895272.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBase_OnlineMap1421320360.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildings3411229051.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void OnlineMapsBuildingBuiltIn::.ctor()
extern "C"  void OnlineMapsBuildingBuiltIn__ctor_m2176530282 (OnlineMapsBuildingBuiltIn_t544190665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::AddHouseWallVerticle(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern "C"  void OnlineMapsBuildingBuiltIn_AddHouseWallVerticle_m1460341548 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___vertices0, Vector3_t2243707580  ___p1, float ___topPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::AnalizeHouseRoofType(OnlineMapsOSMWay,System.Single&,OnlineMapsBuildingBase/OnlineMapsBuildingRoofType&,System.Single&)
extern "C"  void OnlineMapsBuildingBuiltIn_AnalizeHouseRoofType_m3550625554 (Il2CppObject * __this /* static, unused */, OnlineMapsOSMWay_t3319895272 * ___way0, float* ___baseHeight1, int32_t* ___roofType2, float* ___roofHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::AnalizeHouseTags(OnlineMapsOSMWay,UnityEngine.Material&,UnityEngine.Material&,System.Single&)
extern "C"  void OnlineMapsBuildingBuiltIn_AnalizeHouseTags_m3889634566 (Il2CppObject * __this /* static, unused */, OnlineMapsOSMWay_t3319895272 * ___way0, Material_t193706927 ** ___wallMaterial1, Material_t193706927 ** ___roofMaterial2, float* ___baseHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsBuildingBase OnlineMapsBuildingBuiltIn::Create(OnlineMapsBuildings,OnlineMapsOSMWay,System.Collections.Generic.Dictionary`2<System.String,OnlineMapsOSMNode>)
extern "C"  OnlineMapsBuildingBase_t650727021 * OnlineMapsBuildingBuiltIn_Create_m1034639155 (Il2CppObject * __this /* static, unused */, OnlineMapsBuildings_t3411229051 * ___container0, OnlineMapsOSMWay_t3319895272 * ___way1, Dictionary_2_t1003802369 * ___nodes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseRoof(UnityEngine.Vector3[],System.Single,System.Single,OnlineMapsBuildingBase/OnlineMapsBuildingRoofType,System.Collections.Generic.List`1<UnityEngine.Vector3>&,System.Collections.Generic.List`1<UnityEngine.Vector2>&,System.Collections.Generic.List`1<System.Int32>&)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseRoof_m129302929 (OnlineMapsBuildingBuiltIn_t544190665 * __this, Vector3U5BU5D_t1172311765* ___baseVerticles0, float ___baseHeight1, float ___roofHeight2, int32_t ___roofType3, List_1_t1612828712 ** ___vertices4, List_1_t1612828711 ** ___uvs5, List_1_t1440998580 ** ___triangles6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseRoofDome(System.Single,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseRoofDome_m804215084 (Il2CppObject * __this /* static, unused */, float ___height0, List_1_t1612828712 * ___vertices1, List_1_t1440998580 * ___triangles2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseRoofTriangles(System.Collections.Generic.List`1<UnityEngine.Vector3>,OnlineMapsBuildingBase/OnlineMapsBuildingRoofType,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single,System.Single,System.Collections.Generic.List`1<System.Int32>&)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseRoofTriangles_m710897059 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___vertices0, int32_t ___roofType1, List_1_t1612828711 * ___roofPoints2, float ___baseHeight3, float ___roofHeight4, List_1_t1440998580 ** ___triangles5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseRoofVerticles(UnityEngine.Vector3[],System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseRoofVerticles_m693044170 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___baseVerticles0, List_1_t1612828712 * ___verticles1, List_1_t1612828711 * ___roofPoints2, float ___baseHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseWall(UnityEngine.Vector3[],System.Single,UnityEngine.Material,UnityEngine.Vector2,System.Collections.Generic.List`1<UnityEngine.Vector3>&,System.Collections.Generic.List`1<UnityEngine.Vector2>&,System.Collections.Generic.List`1<System.Int32>&)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseWall_m2790393784 (OnlineMapsBuildingBuiltIn_t544190665 * __this, Vector3U5BU5D_t1172311765* ___baseVerticles0, float ___baseHeight1, Material_t193706927 * ___material2, Vector2_t2243707579  ___materialScale3, List_1_t1612828712 ** ___vertices4, List_1_t1612828711 ** ___uvs5, List_1_t1440998580 ** ___triangles6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseWallMesh(UnityEngine.Vector3[],System.Single,System.Boolean,System.Collections.Generic.List`1<UnityEngine.Vector3>&,System.Collections.Generic.List`1<UnityEngine.Vector2>&,System.Collections.Generic.List`1<System.Int32>&)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseWallMesh_m2781161580 (OnlineMapsBuildingBuiltIn_t544190665 * __this, Vector3U5BU5D_t1172311765* ___baseVerticles0, float ___baseHeight1, bool ___inverted2, List_1_t1612828712 ** ___vertices3, List_1_t1612828711 ** ___uvs4, List_1_t1440998580 ** ___triangles5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::CreateHouseWallTriangles(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean,System.Collections.Generic.List`1<System.Int32>&)
extern "C"  void OnlineMapsBuildingBuiltIn_CreateHouseWallTriangles_m568166752 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___vertices0, bool ___reversed1, List_1_t1440998580 ** ___triangles2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsBuildingBuiltIn::CreateHouseWallVerticles(System.Single,UnityEngine.Vector3[],System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  bool OnlineMapsBuildingBuiltIn_CreateHouseWallVerticles_m812495362 (OnlineMapsBuildingBuiltIn_t544190665 * __this, float ___baseHeight0, Vector3U5BU5D_t1172311765* ___baseVerticles1, List_1_t1612828712 * ___vertices2, List_1_t1612828711 * ___uvs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] OnlineMapsBuildingBuiltIn::GetHouseWallTriangle(System.Int32,System.Boolean,System.Int32)
extern "C"  Int32U5BU5D_t3030399641* OnlineMapsBuildingBuiltIn_GetHouseWallTriangle_m2646476309 (Il2CppObject * __this /* static, unused */, int32_t ___countVertices0, bool ___reversed1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsBuildingBuiltIn::GetHeightFromString(System.String,System.Single&)
extern "C"  void OnlineMapsBuildingBuiltIn_GetHeightFromString_m181902043 (Il2CppObject * __this /* static, unused */, String_t* ___str0, float* ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsBuildingMaterial OnlineMapsBuildingBuiltIn::GetRandomMaterial(OnlineMapsBuildings)
extern "C"  OnlineMapsBuildingMaterial_t302108695 * OnlineMapsBuildingBuiltIn_GetRandomMaterial_m2662270705 (Il2CppObject * __this /* static, unused */, OnlineMapsBuildings_t3411229051 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color OnlineMapsBuildingBuiltIn::StringToColor(System.String)
extern "C"  Color_t2020392075  OnlineMapsBuildingBuiltIn_StringToColor_m3760972016 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsBuildingBuiltIn::<Create>m__0(UnityEngine.Vector3)
extern "C"  float OnlineMapsBuildingBuiltIn_U3CCreateU3Em__0_m1633473664 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 OnlineMapsBuildingBuiltIn::<CreateHouseRoofDome>m__1(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  OnlineMapsBuildingBuiltIn_U3CCreateHouseRoofDomeU3Em__1_m224543195 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___current0, Vector3_t2243707580  ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OnlineMapsBuildingBuiltIn::<CreateHouseRoofTriangles>m__2(System.Int32)
extern "C"  int32_t OnlineMapsBuildingBuiltIn_U3CCreateHouseRoofTrianglesU3Em__2_m1596795291 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsBuildingBuiltIn::<StringToColor>m__3(System.Int32)
extern "C"  bool OnlineMapsBuildingBuiltIn_U3CStringToColorU3Em__3_m1986836288 (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
