﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21020750381.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2936936088_gshared (KeyValuePair_2_t1020750381 * __this, Vector3Pair_t2859078138  ___key0, bool ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2936936088(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1020750381 *, Vector3Pair_t2859078138 , bool, const MethodInfo*))KeyValuePair_2__ctor_m2936936088_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>::get_Key()
extern "C"  Vector3Pair_t2859078138  KeyValuePair_2_get_Key_m2709617834_gshared (KeyValuePair_2_t1020750381 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2709617834(__this, method) ((  Vector3Pair_t2859078138  (*) (KeyValuePair_2_t1020750381 *, const MethodInfo*))KeyValuePair_2_get_Key_m2709617834_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3755441663_gshared (KeyValuePair_2_t1020750381 * __this, Vector3Pair_t2859078138  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3755441663(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1020750381 *, Vector3Pair_t2859078138 , const MethodInfo*))KeyValuePair_2_set_Key_m3755441663_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m962505002_gshared (KeyValuePair_2_t1020750381 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m962505002(__this, method) ((  bool (*) (KeyValuePair_2_t1020750381 *, const MethodInfo*))KeyValuePair_2_get_Value_m962505002_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4200298991_gshared (KeyValuePair_2_t1020750381 * __this, bool ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4200298991(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1020750381 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m4200298991_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vectrosity.Vector3Pair,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2431108737_gshared (KeyValuePair_2_t1020750381 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2431108737(__this, method) ((  String_t* (*) (KeyValuePair_2_t1020750381 *, const MethodInfo*))KeyValuePair_2_ToString_m2431108737_gshared)(__this, method)
