﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelUIScaler
struct PanelUIScaler_t4049405372;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelUIScaler::.ctor()
extern "C"  void PanelUIScaler__ctor_m2627911599 (PanelUIScaler_t4049405372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelUIScaler::GlobalUIScale()
extern "C"  void PanelUIScaler_GlobalUIScale_m2354184130 (PanelUIScaler_t4049405372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
