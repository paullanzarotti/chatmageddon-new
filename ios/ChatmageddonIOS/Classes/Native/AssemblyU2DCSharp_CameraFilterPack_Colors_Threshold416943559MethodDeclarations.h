﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_Threshold
struct CameraFilterPack_Colors_Threshold_t416943559;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_Threshold::.ctor()
extern "C"  void CameraFilterPack_Colors_Threshold__ctor_m795422828 (CameraFilterPack_Colors_Threshold_t416943559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Threshold::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_Threshold_get_material_m870642503 (CameraFilterPack_Colors_Threshold_t416943559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::Start()
extern "C"  void CameraFilterPack_Colors_Threshold_Start_m4246619832 (CameraFilterPack_Colors_Threshold_t416943559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_Threshold_OnRenderImage_m2243124448 (CameraFilterPack_Colors_Threshold_t416943559 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::OnValidate()
extern "C"  void CameraFilterPack_Colors_Threshold_OnValidate_m1480019357 (CameraFilterPack_Colors_Threshold_t416943559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::Update()
extern "C"  void CameraFilterPack_Colors_Threshold_Update_m299409359 (CameraFilterPack_Colors_Threshold_t416943559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::OnDisable()
extern "C"  void CameraFilterPack_Colors_Threshold_OnDisable_m1239879295 (CameraFilterPack_Colors_Threshold_t416943559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
