﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BestHTTP.WebSocket.Frames.WebSocketFrame
struct WebSocketFrame_t4163283394;
// BestHTTP.WebSocket.WebSocket
struct WebSocket_t71448861;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// BestHTTP.WebSocket.Frames.WebSocketFrame[]
struct WebSocketFrameU5BU5D_t3220438263;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocket71448861.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSoc3499449257.h"

// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::.ctor(BestHTTP.WebSocket.WebSocket,BestHTTP.WebSocket.Frames.WebSocketFrameTypes,System.Byte[])
extern "C"  void WebSocketFrame__ctor_m2948009939 (WebSocketFrame_t4163283394 * __this, WebSocket_t71448861 * ___webSocket0, uint8_t ___type1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::.ctor(BestHTTP.WebSocket.WebSocket,BestHTTP.WebSocket.Frames.WebSocketFrameTypes,System.Byte[],System.Boolean)
extern "C"  void WebSocketFrame__ctor_m2483168208 (WebSocketFrame_t4163283394 * __this, WebSocket_t71448861 * ___webSocket0, uint8_t ___type1, ByteU5BU5D_t3397334013* ___data2, bool ___useExtensions3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::.ctor(BestHTTP.WebSocket.WebSocket,BestHTTP.WebSocket.Frames.WebSocketFrameTypes,System.Byte[],System.Boolean,System.Boolean)
extern "C"  void WebSocketFrame__ctor_m3874182047 (WebSocketFrame_t4163283394 * __this, WebSocket_t71448861 * ___webSocket0, uint8_t ___type1, ByteU5BU5D_t3397334013* ___data2, bool ___isFinal3, bool ___useExtensions4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::.ctor(BestHTTP.WebSocket.WebSocket,BestHTTP.WebSocket.Frames.WebSocketFrameTypes,System.Byte[],System.UInt64,System.UInt64,System.Boolean,System.Boolean)
extern "C"  void WebSocketFrame__ctor_m1803704063 (WebSocketFrame_t4163283394 * __this, WebSocket_t71448861 * ___webSocket0, uint8_t ___type1, ByteU5BU5D_t3397334013* ___data2, uint64_t ___pos3, uint64_t ___length4, bool ___isFinal5, bool ___useExtensions6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BestHTTP.WebSocket.Frames.WebSocketFrameTypes BestHTTP.WebSocket.Frames.WebSocketFrame::get_Type()
extern "C"  uint8_t WebSocketFrame_get_Type_m312482620 (WebSocketFrame_t4163283394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::set_Type(BestHTTP.WebSocket.Frames.WebSocketFrameTypes)
extern "C"  void WebSocketFrame_set_Type_m278821921 (WebSocketFrame_t4163283394 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.WebSocket.Frames.WebSocketFrame::get_IsFinal()
extern "C"  bool WebSocketFrame_get_IsFinal_m914027425 (WebSocketFrame_t4163283394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::set_IsFinal(System.Boolean)
extern "C"  void WebSocketFrame_set_IsFinal_m584119698 (WebSocketFrame_t4163283394 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte BestHTTP.WebSocket.Frames.WebSocketFrame::get_Header()
extern "C"  uint8_t WebSocketFrame_get_Header_m2819579142 (WebSocketFrame_t4163283394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::set_Header(System.Byte)
extern "C"  void WebSocketFrame_set_Header_m3320902041 (WebSocketFrame_t4163283394 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BestHTTP.WebSocket.Frames.WebSocketFrame::get_Data()
extern "C"  ByteU5BU5D_t3397334013* WebSocketFrame_get_Data_m2009932511 (WebSocketFrame_t4163283394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::set_Data(System.Byte[])
extern "C"  void WebSocketFrame_set_Data_m956506126 (WebSocketFrame_t4163283394 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.WebSocket.Frames.WebSocketFrame::get_UseExtensions()
extern "C"  bool WebSocketFrame_get_UseExtensions_m3585339364 (WebSocketFrame_t4163283394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::set_UseExtensions(System.Boolean)
extern "C"  void WebSocketFrame_set_UseExtensions_m4104053437 (WebSocketFrame_t4163283394 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BestHTTP.WebSocket.Frames.WebSocketFrame::Get()
extern "C"  ByteU5BU5D_t3397334013* WebSocketFrame_Get_m862762440 (WebSocketFrame_t4163283394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BestHTTP.WebSocket.Frames.WebSocketFrame[] BestHTTP.WebSocket.Frames.WebSocketFrame::Fragment(System.UInt16)
extern "C"  WebSocketFrameU5BU5D_t3220438263* WebSocketFrame_Fragment_m2702284854 (WebSocketFrame_t4163283394 * __this, uint16_t ___maxFragmentSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Frames.WebSocketFrame::.cctor()
extern "C"  void WebSocketFrame__cctor_m2868856065 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
