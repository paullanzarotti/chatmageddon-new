﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FriendsILP
struct FriendsILP_t207575760;
// OtherFriendsILP
struct OtherFriendsILP_t671720694;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen4081773843.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendManager
struct  FriendManager_t36140827  : public MonoSingleton_1_t4081773843
{
public:
	// FriendsILP FriendManager::globalSearchList
	FriendsILP_t207575760 * ___globalSearchList_3;
	// FriendsILP FriendManager::friendsSearchList
	FriendsILP_t207575760 * ___friendsSearchList_4;
	// OtherFriendsILP FriendManager::otherFriendsList
	OtherFriendsILP_t671720694 * ___otherFriendsList_5;
	// System.Collections.Generic.List`1<Friend> FriendManager::globalUserSearchList
	List_1_t2924135240 * ___globalUserSearchList_6;
	// System.Boolean FriendManager::globalListUpdated
	bool ___globalListUpdated_7;
	// System.Collections.Generic.List`1<Friend> FriendManager::localUserSearchList
	List_1_t2924135240 * ___localUserSearchList_8;
	// System.Boolean FriendManager::localListUpdated
	bool ___localListUpdated_9;
	// System.Boolean FriendManager::innerListUpdated
	bool ___innerListUpdated_10;
	// System.Boolean FriendManager::outerListUpdated
	bool ___outerListUpdated_11;
	// System.Boolean FriendManager::otherListUpdated
	bool ___otherListUpdated_12;
	// UnityEngine.GameObject FriendManager::phoneNumberUI
	GameObject_t1756533147 * ___phoneNumberUI_13;

public:
	inline static int32_t get_offset_of_globalSearchList_3() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___globalSearchList_3)); }
	inline FriendsILP_t207575760 * get_globalSearchList_3() const { return ___globalSearchList_3; }
	inline FriendsILP_t207575760 ** get_address_of_globalSearchList_3() { return &___globalSearchList_3; }
	inline void set_globalSearchList_3(FriendsILP_t207575760 * value)
	{
		___globalSearchList_3 = value;
		Il2CppCodeGenWriteBarrier(&___globalSearchList_3, value);
	}

	inline static int32_t get_offset_of_friendsSearchList_4() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___friendsSearchList_4)); }
	inline FriendsILP_t207575760 * get_friendsSearchList_4() const { return ___friendsSearchList_4; }
	inline FriendsILP_t207575760 ** get_address_of_friendsSearchList_4() { return &___friendsSearchList_4; }
	inline void set_friendsSearchList_4(FriendsILP_t207575760 * value)
	{
		___friendsSearchList_4 = value;
		Il2CppCodeGenWriteBarrier(&___friendsSearchList_4, value);
	}

	inline static int32_t get_offset_of_otherFriendsList_5() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___otherFriendsList_5)); }
	inline OtherFriendsILP_t671720694 * get_otherFriendsList_5() const { return ___otherFriendsList_5; }
	inline OtherFriendsILP_t671720694 ** get_address_of_otherFriendsList_5() { return &___otherFriendsList_5; }
	inline void set_otherFriendsList_5(OtherFriendsILP_t671720694 * value)
	{
		___otherFriendsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___otherFriendsList_5, value);
	}

	inline static int32_t get_offset_of_globalUserSearchList_6() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___globalUserSearchList_6)); }
	inline List_1_t2924135240 * get_globalUserSearchList_6() const { return ___globalUserSearchList_6; }
	inline List_1_t2924135240 ** get_address_of_globalUserSearchList_6() { return &___globalUserSearchList_6; }
	inline void set_globalUserSearchList_6(List_1_t2924135240 * value)
	{
		___globalUserSearchList_6 = value;
		Il2CppCodeGenWriteBarrier(&___globalUserSearchList_6, value);
	}

	inline static int32_t get_offset_of_globalListUpdated_7() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___globalListUpdated_7)); }
	inline bool get_globalListUpdated_7() const { return ___globalListUpdated_7; }
	inline bool* get_address_of_globalListUpdated_7() { return &___globalListUpdated_7; }
	inline void set_globalListUpdated_7(bool value)
	{
		___globalListUpdated_7 = value;
	}

	inline static int32_t get_offset_of_localUserSearchList_8() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___localUserSearchList_8)); }
	inline List_1_t2924135240 * get_localUserSearchList_8() const { return ___localUserSearchList_8; }
	inline List_1_t2924135240 ** get_address_of_localUserSearchList_8() { return &___localUserSearchList_8; }
	inline void set_localUserSearchList_8(List_1_t2924135240 * value)
	{
		___localUserSearchList_8 = value;
		Il2CppCodeGenWriteBarrier(&___localUserSearchList_8, value);
	}

	inline static int32_t get_offset_of_localListUpdated_9() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___localListUpdated_9)); }
	inline bool get_localListUpdated_9() const { return ___localListUpdated_9; }
	inline bool* get_address_of_localListUpdated_9() { return &___localListUpdated_9; }
	inline void set_localListUpdated_9(bool value)
	{
		___localListUpdated_9 = value;
	}

	inline static int32_t get_offset_of_innerListUpdated_10() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___innerListUpdated_10)); }
	inline bool get_innerListUpdated_10() const { return ___innerListUpdated_10; }
	inline bool* get_address_of_innerListUpdated_10() { return &___innerListUpdated_10; }
	inline void set_innerListUpdated_10(bool value)
	{
		___innerListUpdated_10 = value;
	}

	inline static int32_t get_offset_of_outerListUpdated_11() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___outerListUpdated_11)); }
	inline bool get_outerListUpdated_11() const { return ___outerListUpdated_11; }
	inline bool* get_address_of_outerListUpdated_11() { return &___outerListUpdated_11; }
	inline void set_outerListUpdated_11(bool value)
	{
		___outerListUpdated_11 = value;
	}

	inline static int32_t get_offset_of_otherListUpdated_12() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___otherListUpdated_12)); }
	inline bool get_otherListUpdated_12() const { return ___otherListUpdated_12; }
	inline bool* get_address_of_otherListUpdated_12() { return &___otherListUpdated_12; }
	inline void set_otherListUpdated_12(bool value)
	{
		___otherListUpdated_12 = value;
	}

	inline static int32_t get_offset_of_phoneNumberUI_13() { return static_cast<int32_t>(offsetof(FriendManager_t36140827, ___phoneNumberUI_13)); }
	inline GameObject_t1756533147 * get_phoneNumberUI_13() const { return ___phoneNumberUI_13; }
	inline GameObject_t1756533147 ** get_address_of_phoneNumberUI_13() { return &___phoneNumberUI_13; }
	inline void set_phoneNumberUI_13(GameObject_t1756533147 * value)
	{
		___phoneNumberUI_13 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberUI_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
