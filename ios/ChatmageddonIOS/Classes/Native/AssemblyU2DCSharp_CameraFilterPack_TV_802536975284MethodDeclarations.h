﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_80
struct CameraFilterPack_TV_80_t2536975284;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_80::.ctor()
extern "C"  void CameraFilterPack_TV_80__ctor_m4274441737 (CameraFilterPack_TV_80_t2536975284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_80::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_80_get_material_m892459578 (CameraFilterPack_TV_80_t2536975284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::Start()
extern "C"  void CameraFilterPack_TV_80_Start_m3892110593 (CameraFilterPack_TV_80_t2536975284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_80_OnRenderImage_m4029875017 (CameraFilterPack_TV_80_t2536975284 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::Update()
extern "C"  void CameraFilterPack_TV_80_Update_m1025724578 (CameraFilterPack_TV_80_t2536975284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::OnDisable()
extern "C"  void CameraFilterPack_TV_80_OnDisable_m761506940 (CameraFilterPack_TV_80_t2536975284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
