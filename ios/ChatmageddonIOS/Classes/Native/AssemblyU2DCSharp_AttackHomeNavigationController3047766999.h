﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenColor
struct TweenColor_t3390486518;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen157646620.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackHomeNavigationController
struct  AttackHomeNavigationController_t3047766999  : public NavigationController_2_t157646620
{
public:
	// TweenColor AttackHomeNavigationController::innerTab
	TweenColor_t3390486518 * ___innerTab_8;
	// TweenColor AttackHomeNavigationController::outerTab
	TweenColor_t3390486518 * ___outerTab_9;
	// TweenColor AttackHomeNavigationController::worldTab
	TweenColor_t3390486518 * ___worldTab_10;
	// UnityEngine.GameObject AttackHomeNavigationController::innerObject
	GameObject_t1756533147 * ___innerObject_11;
	// UnityEngine.GameObject AttackHomeNavigationController::outerObject
	GameObject_t1756533147 * ___outerObject_12;
	// UnityEngine.GameObject AttackHomeNavigationController::worldObject
	GameObject_t1756533147 * ___worldObject_13;
	// UnityEngine.Vector3 AttackHomeNavigationController::activePos
	Vector3_t2243707580  ___activePos_14;
	// UnityEngine.Vector3 AttackHomeNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_15;
	// UnityEngine.Vector3 AttackHomeNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_16;

public:
	inline static int32_t get_offset_of_innerTab_8() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___innerTab_8)); }
	inline TweenColor_t3390486518 * get_innerTab_8() const { return ___innerTab_8; }
	inline TweenColor_t3390486518 ** get_address_of_innerTab_8() { return &___innerTab_8; }
	inline void set_innerTab_8(TweenColor_t3390486518 * value)
	{
		___innerTab_8 = value;
		Il2CppCodeGenWriteBarrier(&___innerTab_8, value);
	}

	inline static int32_t get_offset_of_outerTab_9() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___outerTab_9)); }
	inline TweenColor_t3390486518 * get_outerTab_9() const { return ___outerTab_9; }
	inline TweenColor_t3390486518 ** get_address_of_outerTab_9() { return &___outerTab_9; }
	inline void set_outerTab_9(TweenColor_t3390486518 * value)
	{
		___outerTab_9 = value;
		Il2CppCodeGenWriteBarrier(&___outerTab_9, value);
	}

	inline static int32_t get_offset_of_worldTab_10() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___worldTab_10)); }
	inline TweenColor_t3390486518 * get_worldTab_10() const { return ___worldTab_10; }
	inline TweenColor_t3390486518 ** get_address_of_worldTab_10() { return &___worldTab_10; }
	inline void set_worldTab_10(TweenColor_t3390486518 * value)
	{
		___worldTab_10 = value;
		Il2CppCodeGenWriteBarrier(&___worldTab_10, value);
	}

	inline static int32_t get_offset_of_innerObject_11() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___innerObject_11)); }
	inline GameObject_t1756533147 * get_innerObject_11() const { return ___innerObject_11; }
	inline GameObject_t1756533147 ** get_address_of_innerObject_11() { return &___innerObject_11; }
	inline void set_innerObject_11(GameObject_t1756533147 * value)
	{
		___innerObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___innerObject_11, value);
	}

	inline static int32_t get_offset_of_outerObject_12() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___outerObject_12)); }
	inline GameObject_t1756533147 * get_outerObject_12() const { return ___outerObject_12; }
	inline GameObject_t1756533147 ** get_address_of_outerObject_12() { return &___outerObject_12; }
	inline void set_outerObject_12(GameObject_t1756533147 * value)
	{
		___outerObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___outerObject_12, value);
	}

	inline static int32_t get_offset_of_worldObject_13() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___worldObject_13)); }
	inline GameObject_t1756533147 * get_worldObject_13() const { return ___worldObject_13; }
	inline GameObject_t1756533147 ** get_address_of_worldObject_13() { return &___worldObject_13; }
	inline void set_worldObject_13(GameObject_t1756533147 * value)
	{
		___worldObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___worldObject_13, value);
	}

	inline static int32_t get_offset_of_activePos_14() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___activePos_14)); }
	inline Vector3_t2243707580  get_activePos_14() const { return ___activePos_14; }
	inline Vector3_t2243707580 * get_address_of_activePos_14() { return &___activePos_14; }
	inline void set_activePos_14(Vector3_t2243707580  value)
	{
		___activePos_14 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_15() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___leftInactivePos_15)); }
	inline Vector3_t2243707580  get_leftInactivePos_15() const { return ___leftInactivePos_15; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_15() { return &___leftInactivePos_15; }
	inline void set_leftInactivePos_15(Vector3_t2243707580  value)
	{
		___leftInactivePos_15 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_16() { return static_cast<int32_t>(offsetof(AttackHomeNavigationController_t3047766999, ___rightInactivePos_16)); }
	inline Vector3_t2243707580  get_rightInactivePos_16() const { return ___rightInactivePos_16; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_16() { return &___rightInactivePos_16; }
	inline void set_rightInactivePos_16(Vector3_t2243707580  value)
	{
		___rightInactivePos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
