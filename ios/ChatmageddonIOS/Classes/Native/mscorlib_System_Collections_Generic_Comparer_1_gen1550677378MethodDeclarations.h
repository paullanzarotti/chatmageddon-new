﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<PlayerProfileNavScreen>
struct Comparer_1_t1550677378;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<PlayerProfileNavScreen>::.ctor()
extern "C"  void Comparer_1__ctor_m3815525336_gshared (Comparer_1_t1550677378 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3815525336(__this, method) ((  void (*) (Comparer_1_t1550677378 *, const MethodInfo*))Comparer_1__ctor_m3815525336_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<PlayerProfileNavScreen>::.cctor()
extern "C"  void Comparer_1__cctor_m464839607_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m464839607(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m464839607_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<PlayerProfileNavScreen>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2233276703_gshared (Comparer_1_t1550677378 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m2233276703(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1550677378 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m2233276703_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<PlayerProfileNavScreen>::get_Default()
extern "C"  Comparer_1_t1550677378 * Comparer_1_get_Default_m545047208_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m545047208(__this /* static, unused */, method) ((  Comparer_1_t1550677378 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m545047208_gshared)(__this /* static, unused */, method)
