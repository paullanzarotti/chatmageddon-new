﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen4229148245.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatSceneManager
struct  ChatSceneManager_t555354597  : public BaseSceneManager_1_t4229148245
{
public:
	// UnityEngine.GameObject ChatSceneManager::ChatCenterUI
	GameObject_t1756533147 * ___ChatCenterUI_17;
	// UnityEngine.GameObject ChatSceneManager::SceneLoader
	GameObject_t1756533147 * ___SceneLoader_18;

public:
	inline static int32_t get_offset_of_ChatCenterUI_17() { return static_cast<int32_t>(offsetof(ChatSceneManager_t555354597, ___ChatCenterUI_17)); }
	inline GameObject_t1756533147 * get_ChatCenterUI_17() const { return ___ChatCenterUI_17; }
	inline GameObject_t1756533147 ** get_address_of_ChatCenterUI_17() { return &___ChatCenterUI_17; }
	inline void set_ChatCenterUI_17(GameObject_t1756533147 * value)
	{
		___ChatCenterUI_17 = value;
		Il2CppCodeGenWriteBarrier(&___ChatCenterUI_17, value);
	}

	inline static int32_t get_offset_of_SceneLoader_18() { return static_cast<int32_t>(offsetof(ChatSceneManager_t555354597, ___SceneLoader_18)); }
	inline GameObject_t1756533147 * get_SceneLoader_18() const { return ___SceneLoader_18; }
	inline GameObject_t1756533147 ** get_address_of_SceneLoader_18() { return &___SceneLoader_18; }
	inline void set_SceneLoader_18(GameObject_t1756533147 * value)
	{
		___SceneLoader_18 = value;
		Il2CppCodeGenWriteBarrier(&___SceneLoader_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
