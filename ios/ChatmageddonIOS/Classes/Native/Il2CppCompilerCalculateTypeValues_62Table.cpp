﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GpsService_U3CTrackDistanceU3Ec_1867411235.h"
#include "AssemblyU2DCSharp_MicService591677994.h"
#include "AssemblyU2DCSharp_MicService_U3CMicUpdateU3Ec__Ite2245991590.h"
#include "AssemblyU2DCSharp_ShakeCheck3101070106.h"
#include "AssemblyU2DCSharp_ShakeCheck_U3CCheckShakeU3Ec__Ite195906568.h"
#include "AssemblyU2DCSharp_JSON3623987362.h"
#include "AssemblyU2DCSharp_ChatmageddonLoadingIndicator472672888.h"
#include "AssemblyU2DCSharp_FullScreenLoadingIndicator2986689576.h"
#include "AssemblyU2DCSharp_FullScreenLoadingUIScaler2632697573.h"
#include "AssemblyU2DCSharp_LoadingIndicator3396405409.h"
#include "AssemblyU2DCSharp_LoadingIndicator_IndicatorShown76852832.h"
#include "AssemblyU2DCSharp_LoadingIndicator_IndicatorHidden1529613817.h"
#include "AssemblyU2DCSharp_NGUIAnimation2962118329.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationStart3313445917.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationDrawn3207005891.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationUpdated4231811366.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationLooped2655604368.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationFinishe3859016523.h"
#include "AssemblyU2DCSharp_NGUIAnimation_U3CPlayAnimU3Ec__It303236222.h"
#include "AssemblyU2DCSharp_SpriteSheetNGUIAnimation2524734547.h"
#include "AssemblyU2DCSharp_UISriteAnimation265093205.h"
#include "AssemblyU2DCSharp_MultiImageNGUIAnimation45560503.h"
#include "AssemblyU2DCSharp_UITextureAnimation3587053699.h"
#include "AssemblyU2DCSharp_Data3569509720.h"
#include "AssemblyU2DCSharp_CircularProgressBar2980095063.h"
#include "AssemblyU2DCSharp_CountdownTimer4123051008.h"
#include "AssemblyU2DCSharp_CountdownTimer_OnTimerFinished1234642247.h"
#include "AssemblyU2DCSharp_CountdownTimerProgressBar1742287524.h"
#include "AssemblyU2DCSharp_ProgressBar192201240.h"
#include "AssemblyU2DCSharp_ProgressBar_OnProgressUpdated2843909178.h"
#include "AssemblyU2DCSharp_ProgressBar_OnProgressFinished739551803.h"
#include "AssemblyU2DCSharp_ProgressBar_U3CTransitionU3Ec__I3581714401.h"
#include "AssemblyU2DCSharp_UIPanelProgressBar2014302056.h"
#include "AssemblyU2DCSharp_UISliderProgressBar2494061391.h"
#include "AssemblyU2DCSharp_BaseUIScaler568866473.h"
#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"
#include "AssemblyU2DCSharp_RootScaler2927939400.h"
#include "AssemblyU2DCSharp_UIPanelScaleFixer3587123562.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"
#include "AssemblyU2DCSharp_ChatmageddonPanelTransitionManag1399015365.h"
#include "AssemblyU2DCSharp_HomeBottomTabController251067189.h"
#include "AssemblyU2DCSharp_Panel1787746694.h"
#include "AssemblyU2DCSharp_Panel_U3COpenPanelCoroutineU3Ec_4147259658.h"
#include "AssemblyU2DCSharp_Panel_U3CClosePanelCoroutineU3Ec4130515019.h"
#include "AssemblyU2DCSharp_PanelContainer1029301983.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "AssemblyU2DCSharp_SwitchPanelButton319351498.h"
#include "AssemblyU2DCSharp_LoginEntrance2243089009.h"
#include "AssemblyU2DCSharp_ChatmageddonFBManager234400784.h"
#include "AssemblyU2DCSharp_FacebookFriend758270532.h"
#include "AssemblyU2DCSharp_FacebookPlayer2645999341.h"
#include "AssemblyU2DCSharp_FacebookStory448600801.h"
#include "AssemblyU2DCSharp_FacebookUser244724091.h"
#include "AssemblyU2DCSharp_InternetReachability3219127626.h"
#include "AssemblyU2DCSharp_InternetReachability_U3CInternet3202327804.h"
#include "AssemblyU2DCSharp_InternetReachability_U3CInternet3087003569.h"
#include "AssemblyU2DCSharp_TwitterLoginEntrance1132015978.h"
#include "AssemblyU2DCSharp_ChatmageddonTwitterManager3244794565.h"
#include "AssemblyU2DCSharp_LPNController1966948550.h"
#include "AssemblyU2DCSharp_PhoneNumbers_AreaCodeMap3230759372.h"
#include "AssemblyU2DCSharp_PhoneNumbers_AreaCodeMapStorageS3709015996.h"
#include "AssemblyU2DCSharp_PhoneNumbers_AreaCodeParser1488945279.h"
#include "AssemblyU2DCSharp_PhoneNumbers_AsYouTypeFormatter993724987.h"
#include "AssemblyU2DCSharp_PhoneNumbers_BuildMetadataFromXm3731095708.h"
#include "AssemblyU2DCSharp_PhoneNumbers_CountryCodeToRegion1751248913.h"
#include "AssemblyU2DCSharp_PhoneNumbers_DefaultMapStorage2292289748.h"
#include "AssemblyU2DCSharp_PhoneNumbers_FlyweightMapStorage1600303080.h"
#include "AssemblyU2DCSharp_PhoneNumbers_FlyweightMapStorage1505679999.h"
#include "AssemblyU2DCSharp_libphonenumber_csharp_portable_L2231244243.h"
#include "AssemblyU2DCSharp_PhoneNumbers_LocaleData3425842736.h"
#include "AssemblyU2DCSharp_PhoneNumbers_MappingFileProvider2052216305.h"
#include "AssemblyU2DCSharp_PhoneNumbers_MetadataManager2196949690.h"
#include "AssemblyU2DCSharp_PhoneNumbers_MetadataManager_U3C1433166160.h"
#include "AssemblyU2DCSharp_PhoneNumbers_ErrorType3615858992.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberParseExceptio2449753599.h"
#include "AssemblyU2DCSharp_PhoneMetaPhoneNumbers2910735851.h"
#include "AssemblyU2DCSharp_PhoneMetaTerritory2073820963.h"
#include "AssemblyU2DCSharp_PhoneMetaNumberFormat4038650015.h"
#include "AssemblyU2DCSharp_PhoneMetaNumberInfo3299665448.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberMatch2163858580.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberMatcher3898172501.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberMatcher_2785688338.h"
#include "AssemblyU2DCSharp_PhoneNumbers_Locale2376277312.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberOfflineG3035616292.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6200 = { sizeof (U3CTrackDistanceU3Ec__Iterator0_t1867411235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6200[8] = 
{
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U3CdLatU3E__0_0(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U3CdLonU3E__1_1(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U3CaU3E__2_2(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U3CcU3E__3_3(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U24this_4(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U24current_5(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U24disposing_6(),
	U3CTrackDistanceU3Ec__Iterator0_t1867411235::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6201 = { sizeof (MicService_t591677994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6201[11] = 
{
	MicService_t591677994::get_offset_of_sensitivity_2(),
	MicService_t591677994::get_offset_of_U3CloudnessU3Ek__BackingField_3(),
	MicService_t591677994::get_offset_of_timeLeft_4(),
	MicService_t591677994::get_offset_of_testing_5(),
	MicService_t591677994::get_offset_of_samplerate_6(),
	MicService_t591677994::get_offset_of_sourceVolume_7(),
	MicService_t591677994::get_offset_of_deviceName_8(),
	MicService_t591677994::get_offset_of_amountSamples_9(),
	MicService_t591677994::get_offset_of_minFreq_10(),
	MicService_t591677994::get_offset_of_maxFreq_11(),
	MicService_t591677994::get_offset_of_goAudioSource_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6202 = { sizeof (U3CMicUpdateU3Ec__Iterator0_t2245991590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6202[4] = 
{
	U3CMicUpdateU3Ec__Iterator0_t2245991590::get_offset_of_U24this_0(),
	U3CMicUpdateU3Ec__Iterator0_t2245991590::get_offset_of_U24current_1(),
	U3CMicUpdateU3Ec__Iterator0_t2245991590::get_offset_of_U24disposing_2(),
	U3CMicUpdateU3Ec__Iterator0_t2245991590::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6203 = { sizeof (ShakeCheck_t3101070106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6203[9] = 
{
	ShakeCheck_t3101070106::get_offset_of_accelerometerUpdateInterval_2(),
	ShakeCheck_t3101070106::get_offset_of_lowPassKernalWidthInSeconds_3(),
	ShakeCheck_t3101070106::get_offset_of_shakeDetectionThreshold_4(),
	ShakeCheck_t3101070106::get_offset_of_lowPassFilterFactor_5(),
	ShakeCheck_t3101070106::get_offset_of_lowPassValue_6(),
	ShakeCheck_t3101070106::get_offset_of_acceleration_7(),
	ShakeCheck_t3101070106::get_offset_of_deltaAcceleration_8(),
	ShakeCheck_t3101070106::get_offset_of_shakeCount_9(),
	ShakeCheck_t3101070106::get_offset_of_shaking_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6204 = { sizeof (U3CCheckShakeU3Ec__Iterator0_t195906568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6204[4] = 
{
	U3CCheckShakeU3Ec__Iterator0_t195906568::get_offset_of_U24this_0(),
	U3CCheckShakeU3Ec__Iterator0_t195906568::get_offset_of_U24current_1(),
	U3CCheckShakeU3Ec__Iterator0_t195906568::get_offset_of_U24disposing_2(),
	U3CCheckShakeU3Ec__Iterator0_t195906568::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6205 = { sizeof (JSON_t3623987362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6205[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6206 = { sizeof (ChatmageddonLoadingIndicator_t472672888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6206[4] = 
{
	ChatmageddonLoadingIndicator_t472672888::get_offset_of_indicatorImage_5(),
	ChatmageddonLoadingIndicator_t472672888::get_offset_of_rotationTween_6(),
	ChatmageddonLoadingIndicator_t472672888::get_offset_of_scaleTween_7(),
	ChatmageddonLoadingIndicator_t472672888::get_offset_of_closing_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6207 = { sizeof (FullScreenLoadingIndicator_t2986689576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6207[3] = 
{
	FullScreenLoadingIndicator_t2986689576::get_offset_of_indicator_3(),
	FullScreenLoadingIndicator_t2986689576::get_offset_of_backgroundTween_4(),
	FullScreenLoadingIndicator_t2986689576::get_offset_of_backgroundShowing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6208 = { sizeof (FullScreenLoadingUIScaler_t2632697573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6208[2] = 
{
	FullScreenLoadingUIScaler_t2632697573::get_offset_of_background_14(),
	FullScreenLoadingUIScaler_t2632697573::get_offset_of_backgroundCollider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6209 = { sizeof (LoadingIndicator_t3396405409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6209[3] = 
{
	LoadingIndicator_t3396405409::get_offset_of_showingIndicator_2(),
	LoadingIndicator_t3396405409::get_offset_of_onIndicatorShown_3(),
	LoadingIndicator_t3396405409::get_offset_of_onIndicatorHidden_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6210 = { sizeof (IndicatorShown_t76852832), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6211 = { sizeof (IndicatorHidden_t1529613817), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6212 = { sizeof (NGUIAnimation_t2962118329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6212[10] = 
{
	NGUIAnimation_t2962118329::get_offset_of_fps_2(),
	NGUIAnimation_t2962118329::get_offset_of_totalFrames_3(),
	NGUIAnimation_t2962118329::get_offset_of_paused_4(),
	NGUIAnimation_t2962118329::get_offset_of_currentFrame_5(),
	NGUIAnimation_t2962118329::get_offset_of_animationRoutine_6(),
	NGUIAnimation_t2962118329::get_offset_of_onAnimationStart_7(),
	NGUIAnimation_t2962118329::get_offset_of_onAnimationDrawn_8(),
	NGUIAnimation_t2962118329::get_offset_of_onAnimationUpdated_9(),
	NGUIAnimation_t2962118329::get_offset_of_onAnimationLooped_10(),
	NGUIAnimation_t2962118329::get_offset_of_onAnimationFinished_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6213 = { sizeof (OnAnimationStart_t3313445917), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6214 = { sizeof (OnAnimationDrawn_t3207005891), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6215 = { sizeof (OnAnimationUpdated_t4231811366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6216 = { sizeof (OnAnimationLooped_t2655604368), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6217 = { sizeof (OnAnimationFinished_t3859016523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6218 = { sizeof (U3CPlayAnimU3Ec__Iterator0_t303236222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6218[7] = 
{
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_U3CtimeStepU3E__0_0(),
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_U3CupdatedU3E__1_1(),
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_loop_2(),
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_U24this_3(),
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_U24current_4(),
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_U24disposing_5(),
	U3CPlayAnimU3Ec__Iterator0_t303236222::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6219 = { sizeof (SpriteSheetNGUIAnimation_t2524734547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6219[6] = 
{
	SpriteSheetNGUIAnimation_t2524734547::get_offset_of_totalRows_13(),
	SpriteSheetNGUIAnimation_t2524734547::get_offset_of_totalCollumns_14(),
	SpriteSheetNGUIAnimation_t2524734547::get_offset_of_currentRow_15(),
	SpriteSheetNGUIAnimation_t2524734547::get_offset_of_currentCollumn_16(),
	SpriteSheetNGUIAnimation_t2524734547::get_offset_of_spriteWidth_17(),
	SpriteSheetNGUIAnimation_t2524734547::get_offset_of_spriteHeight_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6220 = { sizeof (UISriteAnimation_t265093205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6220[1] = 
{
	UISriteAnimation_t265093205::get_offset_of_animSprite_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6221 = { sizeof (MultiImageNGUIAnimation_t45560503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6221[1] = 
{
	MultiImageNGUIAnimation_t45560503::get_offset_of_textures_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6222 = { sizeof (UITextureAnimation_t3587053699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6222[1] = 
{
	UITextureAnimation_t3587053699::get_offset_of_animTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6223 = { sizeof (Data_t3569509720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6223[1] = 
{
	Data_t3569509720::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6224 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6225 = { sizeof (CircularProgressBar_t2980095063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6225[5] = 
{
	CircularProgressBar_t2980095063::get_offset_of_progressSection_8(),
	CircularProgressBar_t2980095063::get_offset_of_progressPanel_9(),
	CircularProgressBar_t2980095063::get_offset_of_sections_10(),
	CircularProgressBar_t2980095063::get_offset_of_sectionDegrees_11(),
	CircularProgressBar_t2980095063::get_offset_of_sectionPercentage_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6226 = { sizeof (CountdownTimer_t4123051008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6226[4] = 
{
	CountdownTimer_t4123051008::get_offset_of_countdownLabel_8(),
	CountdownTimer_t4123051008::get_offset_of_countdownTimeAmount_9(),
	CountdownTimer_t4123051008::get_offset_of_onTimerFinished_10(),
	CountdownTimer_t4123051008::get_offset_of_numberOn_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6227 = { sizeof (OnTimerFinished_t1234642247), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6228 = { sizeof (CountdownTimerProgressBar_t1742287524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6228[3] = 
{
	CountdownTimerProgressBar_t1742287524::get_offset_of_countdownLabel_13(),
	CountdownTimerProgressBar_t1742287524::get_offset_of_countdownTimeAmount_14(),
	CountdownTimerProgressBar_t1742287524::get_offset_of_numberOn_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6229 = { sizeof (ProgressBar_t192201240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6229[6] = 
{
	ProgressBar_t192201240::get_offset_of_currentPercent_2(),
	ProgressBar_t192201240::get_offset_of_targetPercent_3(),
	ProgressBar_t192201240::get_offset_of_currentRoutine_4(),
	ProgressBar_t192201240::get_offset_of_onProgressUpdated_5(),
	ProgressBar_t192201240::get_offset_of_onProgressFinished_6(),
	ProgressBar_t192201240::get_offset_of_totalTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6230 = { sizeof (OnProgressUpdated_t2843909178), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6231 = { sizeof (OnProgressFinished_t739551803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6232 = { sizeof (U3CTransitionU3Ec__Iterator0_t3581714401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6232[9] = 
{
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U3CamountToTransitionU3E__0_0(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U3CdecreasingU3E__1_1(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_percent_2(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_time_3(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U3CamountToChangeU3E__2_4(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U24this_5(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U24current_6(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U24disposing_7(),
	U3CTransitionU3Ec__Iterator0_t3581714401::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6233 = { sizeof (UIPanelProgressBar_t2014302056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6233[2] = 
{
	UIPanelProgressBar_t2014302056::get_offset_of_progressPanel_8(),
	UIPanelProgressBar_t2014302056::get_offset_of_maxPanelHeight_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6234 = { sizeof (UISliderProgressBar_t2494061391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6234[1] = 
{
	UISliderProgressBar_t2494061391::get_offset_of_slider_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6235 = { sizeof (BaseUIScaler_t568866473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6235[12] = 
{
	BaseUIScaler_t568866473::get_offset_of_scaleOnAwake_2(),
	BaseUIScaler_t568866473::get_offset_of_scaleOnStart_3(),
	BaseUIScaler_t568866473::get_offset_of_devResWidth_4(),
	BaseUIScaler_t568866473::get_offset_of_devResHeight_5(),
	BaseUIScaler_t568866473::get_offset_of_calcScaleAspect_6(),
	BaseUIScaler_t568866473::get_offset_of_startScaleWidth_7(),
	BaseUIScaler_t568866473::get_offset_of_startScaleHeight_8(),
	BaseUIScaler_t568866473::get_offset_of_calcScaleWidth_9(),
	BaseUIScaler_t568866473::get_offset_of_calcScaleHeight_10(),
	BaseUIScaler_t568866473::get_offset_of_calcScaleVector_11(),
	BaseUIScaler_t568866473::get_offset_of_evenCalcScaleVectorWidth_12(),
	BaseUIScaler_t568866473::get_offset_of_evenCalcScaleVectorHeight_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6236 = { sizeof (ChatmageddonUIScaler_t3374094653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6237 = { sizeof (RootScaler_t2927939400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6237[1] = 
{
	RootScaler_t2927939400::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6238 = { sizeof (UIPanelScaleFixer_t3587123562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6238[3] = 
{
	UIPanelScaleFixer_t3587123562::get_offset_of_scale_2(),
	UIPanelScaleFixer_t3587123562::get_offset_of_panel_3(),
	UIPanelScaleFixer_t3587123562::get_offset_of_scaleParent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6239 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6239[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6240 = { sizeof (Timer_t2917042437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6240[2] = 
{
	Timer_t2917042437::get_offset_of_myTimer_0(),
	Timer_t2917042437::get_offset_of_timerActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6241 = { sizeof (ChatmageddonPanelTransitionManager_t1399015365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6241[4] = 
{
	ChatmageddonPanelTransitionManager_t1399015365::get_offset_of_bottomUI_8(),
	ChatmageddonPanelTransitionManager_t1399015365::get_offset_of_topTabs_9(),
	ChatmageddonPanelTransitionManager_t1399015365::get_offset_of_clipPanel_10(),
	ChatmageddonPanelTransitionManager_t1399015365::get_offset_of_showMapUI_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6242 = { sizeof (HomeBottomTabController_t251067189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6242[2] = 
{
	HomeBottomTabController_t251067189::get_offset_of_posTween_3(),
	HomeBottomTabController_t251067189::get_offset_of_tabClosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6243 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6243[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6244 = { sizeof (Panel_t1787746694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6244[11] = 
{
	Panel_t1787746694::get_offset_of_panelName_2(),
	Panel_t1787746694::get_offset_of_panelPosTween_3(),
	Panel_t1787746694::get_offset_of_container_4(),
	Panel_t1787746694::get_offset_of_destructable_5(),
	Panel_t1787746694::get_offset_of_panelEnabled_6(),
	Panel_t1787746694::get_offset_of_panelOpening_7(),
	Panel_t1787746694::get_offset_of_panelClosing_8(),
	Panel_t1787746694::get_offset_of_allowsBottomUI_9(),
	Panel_t1787746694::get_offset_of_allowsTopTabs_10(),
	Panel_t1787746694::get_offset_of_allowsTopUI_11(),
	Panel_t1787746694::get_offset_of_delayBottomUI_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6245 = { sizeof (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6245[4] = 
{
	U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658::get_offset_of_U24this_0(),
	U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658::get_offset_of_U24current_1(),
	U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658::get_offset_of_U24disposing_2(),
	U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6246 = { sizeof (U3CClosePanelCoroutineU3Ec__Iterator1_t4130515019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6246[4] = 
{
	U3CClosePanelCoroutineU3Ec__Iterator1_t4130515019::get_offset_of_U24this_0(),
	U3CClosePanelCoroutineU3Ec__Iterator1_t4130515019::get_offset_of_U24current_1(),
	U3CClosePanelCoroutineU3Ec__Iterator1_t4130515019::get_offset_of_U24disposing_2(),
	U3CClosePanelCoroutineU3Ec__Iterator1_t4130515019::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6247 = { sizeof (PanelContainer_t1029301983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6247[1] = 
{
	PanelContainer_t1029301983::get_offset_of_panelsContained_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6248 = { sizeof (PanelType_t482769230)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6248[6] = 
{
	PanelType_t482769230::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6249 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6249[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6250 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6250[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6251 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6251[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6252 = { sizeof (SwitchPanelButton_t319351498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6252[1] = 
{
	SwitchPanelButton_t319351498::get_offset_of_panel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6253 = { sizeof (LoginEntrance_t2243089009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6253[5] = 
{
	LoginEntrance_t2243089009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6254 = { sizeof (ChatmageddonFBManager_t234400784), -1, sizeof(ChatmageddonFBManager_t234400784_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6254[2] = 
{
	ChatmageddonFBManager_t234400784_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	ChatmageddonFBManager_t234400784_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6255 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6255[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6256 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6257 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6257[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6258 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6258[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6259 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6259[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6260 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6260[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6261 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6261[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6262 = { sizeof (FacebookFriend_t758270532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6262[1] = 
{
	FacebookFriend_t758270532::get_offset_of_score_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6263 = { sizeof (FacebookPlayer_t2645999341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6263[2] = 
{
	FacebookPlayer_t2645999341::get_offset_of_accessToken_5(),
	FacebookPlayer_t2645999341::get_offset_of_friendsList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6264 = { sizeof (FacebookStory_t448600801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6264[10] = 
{
	FacebookStory_t448600801::get_offset_of_toID_0(),
	FacebookStory_t448600801::get_offset_of_linkURL_1(),
	FacebookStory_t448600801::get_offset_of_linkName_2(),
	FacebookStory_t448600801::get_offset_of_linkCaption_3(),
	FacebookStory_t448600801::get_offset_of_linkDescription_4(),
	FacebookStory_t448600801::get_offset_of_linkImage_5(),
	FacebookStory_t448600801::get_offset_of_linkMedia_6(),
	FacebookStory_t448600801::get_offset_of_actionLink_7(),
	FacebookStory_t448600801::get_offset_of_actionName_8(),
	FacebookStory_t448600801::get_offset_of_linkReference_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6265 = { sizeof (FacebookUser_t244724091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6265[5] = 
{
	FacebookUser_t244724091::get_offset_of_firstName_0(),
	FacebookUser_t244724091::get_offset_of_lastName_1(),
	FacebookUser_t244724091::get_offset_of_id_2(),
	FacebookUser_t244724091::get_offset_of_email_3(),
	FacebookUser_t244724091::get_offset_of_profilePicture_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6266 = { sizeof (InternetReachability_t3219127626), -1, sizeof(InternetReachability_t3219127626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6266[6] = 
{
	InternetReachability_t3219127626::get_offset_of_internetConnection_3(),
	InternetReachability_t3219127626_StaticFields::get_offset_of_timeoutMiliseconds_4(),
	InternetReachability_t3219127626_StaticFields::get_offset_of_checkURL_5(),
	InternetReachability_t3219127626::get_offset_of_internetPoll_6(),
	InternetReachability_t3219127626::get_offset_of_failedConnectionCount_7(),
	InternetReachability_t3219127626::get_offset_of_connectionLockoutAmount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6267 = { sizeof (U3CInternetPollCheckU3Ec__Iterator0_t3202327804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6267[6] = 
{
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804::get_offset_of_pollTime_0(),
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804::get_offset_of_U24this_1(),
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804::get_offset_of_U24current_2(),
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804::get_offset_of_U24disposing_3(),
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804::get_offset_of_U24PC_4(),
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6268 = { sizeof (U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6268[3] = 
{
	U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569::get_offset_of_polled_0(),
	U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569::get_offset_of_checkingInternet_1(),
	U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569::get_offset_of_U3CU3Ef__refU240_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6269 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6269[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6270 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6270[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6271 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6271[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6272 = { sizeof (TwitterLoginEntrance_t1132015978)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6272[3] = 
{
	TwitterLoginEntrance_t1132015978::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6273 = { sizeof (ChatmageddonTwitterManager_t3244794565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6274 = { sizeof (LPNController_t1966948550), -1, sizeof(LPNController_t1966948550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6274[2] = 
{
	LPNController_t1966948550::get_offset_of_util_2(),
	LPNController_t1966948550_StaticFields::get_offset_of_TEST_META_DATA_FILE_PREFIX_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6275 = { sizeof (AreaCodeMap_t3230759372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6275[2] = 
{
	AreaCodeMap_t3230759372::get_offset_of_phoneUtil_0(),
	AreaCodeMap_t3230759372::get_offset_of_areaCodeMapStorage_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6276 = { sizeof (AreaCodeMapStorageStrategy_t3709015996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6276[2] = 
{
	AreaCodeMapStorageStrategy_t3709015996::get_offset_of_numOfEntries_0(),
	AreaCodeMapStorageStrategy_t3709015996::get_offset_of_possibleLengths_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6277 = { sizeof (AreaCodeParser_t1488945279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6278 = { sizeof (AsYouTypeFormatter_t993724987), -1, sizeof(AsYouTypeFormatter_t993724987_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6278[28] = 
{
	AsYouTypeFormatter_t993724987::get_offset_of_currentOutput_0(),
	AsYouTypeFormatter_t993724987::get_offset_of_formattingTemplate_1(),
	AsYouTypeFormatter_t993724987::get_offset_of_currentFormattingPattern_2(),
	AsYouTypeFormatter_t993724987::get_offset_of_accruedInput_3(),
	AsYouTypeFormatter_t993724987::get_offset_of_accruedInputWithoutFormatting_4(),
	AsYouTypeFormatter_t993724987::get_offset_of_ableToFormat_5(),
	AsYouTypeFormatter_t993724987::get_offset_of_inputHasFormatting_6(),
	AsYouTypeFormatter_t993724987::get_offset_of_isInternationalFormatting_7(),
	AsYouTypeFormatter_t993724987::get_offset_of_isExpectingCountryCallingCode_8(),
	AsYouTypeFormatter_t993724987::get_offset_of_phoneUtil_9(),
	AsYouTypeFormatter_t993724987::get_offset_of_defaultCountry_10(),
	AsYouTypeFormatter_t993724987_StaticFields::get_offset_of_EMPTY_METADATA_11(),
	AsYouTypeFormatter_t993724987::get_offset_of_defaultMetaData_12(),
	AsYouTypeFormatter_t993724987::get_offset_of_currentMetaData_13(),
	AsYouTypeFormatter_t993724987_StaticFields::get_offset_of_CHARACTER_CLASS_PATTERN_14(),
	AsYouTypeFormatter_t993724987_StaticFields::get_offset_of_STANDALONE_DIGIT_PATTERN_15(),
	AsYouTypeFormatter_t993724987_StaticFields::get_offset_of_ELIGIBLE_FORMAT_PATTERN_16(),
	AsYouTypeFormatter_t993724987_StaticFields::get_offset_of_MIN_LEADING_DIGITS_LENGTH_17(),
	AsYouTypeFormatter_t993724987::get_offset_of_digitPlaceholder_18(),
	AsYouTypeFormatter_t993724987::get_offset_of_digitPattern_19(),
	AsYouTypeFormatter_t993724987::get_offset_of_lastMatchPosition_20(),
	AsYouTypeFormatter_t993724987::get_offset_of_originalPosition_21(),
	AsYouTypeFormatter_t993724987::get_offset_of_positionToRemember_22(),
	AsYouTypeFormatter_t993724987::get_offset_of_prefixBeforeNationalNumber_23(),
	AsYouTypeFormatter_t993724987::get_offset_of_nationalPrefixExtracted_24(),
	AsYouTypeFormatter_t993724987::get_offset_of_nationalNumber_25(),
	AsYouTypeFormatter_t993724987::get_offset_of_possibleFormats_26(),
	AsYouTypeFormatter_t993724987::get_offset_of_regexCache_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6279 = { sizeof (BuildMetadataFromXml_t3731095708), -1, sizeof(BuildMetadataFromXml_t3731095708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6279[33] = 
{
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_CARRIER_CODE_FORMATTING_RULE_0(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_COUNTRY_CODE_1(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_EMERGENCY_2(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_EXAMPLE_NUMBER_3(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_FIXED_LINE_4(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_FORMAT_5(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_GENERAL_DESC_6(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_INTERNATIONAL_PREFIX_7(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_INTL_FORMAT_8(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_LEADING_DIGITS_9(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_LEADING_ZERO_POSSIBLE_10(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_MAIN_COUNTRY_FOR_CODE_11(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_MOBILE_12(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NATIONAL_NUMBER_PATTERN_13(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NATIONAL_PREFIX_14(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NATIONAL_PREFIX_FORMATTING_RULE_15(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NATIONAL_PREFIX_OPTIONAL_WHEN_FORMATTING_16(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NATIONAL_PREFIX_FOR_PARSING_17(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NATIONAL_PREFIX_TRANSFORM_RULE_18(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NO_INTERNATIONAL_DIALLING_19(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_NUMBER_FORMAT_20(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_PAGER_21(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_PATTERN_22(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_PERSONAL_NUMBER_23(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_POSSIBLE_NUMBER_PATTERN_24(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_PREFERRED_EXTN_PREFIX_25(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_PREFERRED_INTERNATIONAL_PREFIX_26(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_PREMIUM_RATE_27(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_SHARED_COST_28(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_TOLL_FREE_29(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_UAN_30(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_VOICEMAIL_31(),
	BuildMetadataFromXml_t3731095708_StaticFields::get_offset_of_VOIP_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6280 = { sizeof (CountryCodeToRegionCodeMap_t1751248913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6281 = { sizeof (DefaultMapStorage_t2292289748), -1, sizeof(DefaultMapStorage_t2292289748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6281[3] = 
{
	DefaultMapStorage_t2292289748::get_offset_of_phoneNumberPrefixes_2(),
	DefaultMapStorage_t2292289748::get_offset_of_descriptions_3(),
	DefaultMapStorage_t2292289748_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6282 = { sizeof (FlyweightMapStorage_t1600303080), -1, sizeof(FlyweightMapStorage_t1600303080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6282[8] = 
{
	FlyweightMapStorage_t1600303080_StaticFields::get_offset_of_SHORT_NUM_BYTES_2(),
	FlyweightMapStorage_t1600303080_StaticFields::get_offset_of_INT_NUM_BYTES_3(),
	FlyweightMapStorage_t1600303080::get_offset_of_prefixSizeInBytes_4(),
	FlyweightMapStorage_t1600303080::get_offset_of_descIndexSizeInBytes_5(),
	FlyweightMapStorage_t1600303080::get_offset_of_phoneNumberPrefixes_6(),
	FlyweightMapStorage_t1600303080::get_offset_of_descriptionIndexes_7(),
	FlyweightMapStorage_t1600303080::get_offset_of_descriptionPool_8(),
	FlyweightMapStorage_t1600303080_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6283 = { sizeof (ByteBuffer_t1505679999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6283[3] = 
{
	ByteBuffer_t1505679999::get_offset_of_stream_0(),
	ByteBuffer_t1505679999::get_offset_of_reader_1(),
	ByteBuffer_t1505679999::get_offset_of_writer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6284 = { sizeof (ListExtension_t2231244243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6285 = { sizeof (LocaleData_t3425842736), -1, sizeof(LocaleData_t3425842736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6285[2] = 
{
	LocaleData_t3425842736_StaticFields::get_offset_of__Data_0(),
	LocaleData_t3425842736_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6286 = { sizeof (MappingFileProvider_t2052216305), -1, sizeof(MappingFileProvider_t2052216305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6286[5] = 
{
	MappingFileProvider_t2052216305::get_offset_of_numOfEntries_0(),
	MappingFileProvider_t2052216305::get_offset_of_countryCallingCodes_1(),
	MappingFileProvider_t2052216305::get_offset_of_availableLanguages_2(),
	MappingFileProvider_t2052216305_StaticFields::get_offset_of_LOCALE_NORMALIZATION_MAP_3(),
	MappingFileProvider_t2052216305_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6287 = { sizeof (MetadataManager_t2196949690), -1, sizeof(MetadataManager_t2196949690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6287[3] = 
{
	0,
	MetadataManager_t2196949690_StaticFields::get_offset_of_callingCodeToAlternateFormatsMap_1(),
	MetadataManager_t2196949690_StaticFields::get_offset_of_countryCodeSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6288 = { sizeof (U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6288[1] = 
{
	U3CLoadMedataFromFileU3Ec__AnonStorey0_t1433166160::get_offset_of_filePrefix_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6289 = { sizeof (ErrorType_t3615858992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6289[6] = 
{
	ErrorType_t3615858992::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6290 = { sizeof (NumberParseException_t2449753599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6290[1] = 
{
	NumberParseException_t2449753599::get_offset_of_ErrorType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6291 = { sizeof (PhoneMetaPhoneNumbers_t2910735851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6291[1] = 
{
	PhoneMetaPhoneNumbers_t2910735851::get_offset_of_territories_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6292 = { sizeof (PhoneMetaTerritory_t2073820963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6292[30] = 
{
	PhoneMetaTerritory_t2073820963::get_offset_of_m_id_0(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_countryCode_1(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_leadingDigits_2(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_preferredInternationalprefix_3(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_internationalPrefix_4(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_nationalPrefix_5(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_nationalPrefixForParsing_6(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_nationalPrefixTransformRule_7(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_preferredExtnPrefix_8(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_nationalPrefixFormattingRule_9(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_carrierCodeFormattingRule_10(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_mainCountryForCode_11(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_nationalPrefixOptionalWhenFormatting_12(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_leadingZeroPossible_13(),
	PhoneMetaTerritory_t2073820963::get_offset_of_m_mobileNumberPortableRegion_14(),
	PhoneMetaTerritory_t2073820963::get_offset_of_references_15(),
	PhoneMetaTerritory_t2073820963::get_offset_of_availableFormats_16(),
	PhoneMetaTerritory_t2073820963::get_offset_of_generalDesc_17(),
	PhoneMetaTerritory_t2073820963::get_offset_of_noInternationalDialling_18(),
	PhoneMetaTerritory_t2073820963::get_offset_of_areaCodeOptional_19(),
	PhoneMetaTerritory_t2073820963::get_offset_of_fixedLine_20(),
	PhoneMetaTerritory_t2073820963::get_offset_of_mobile_21(),
	PhoneMetaTerritory_t2073820963::get_offset_of_pager_22(),
	PhoneMetaTerritory_t2073820963::get_offset_of_tollFree_23(),
	PhoneMetaTerritory_t2073820963::get_offset_of_premiumRate_24(),
	PhoneMetaTerritory_t2073820963::get_offset_of_sharedCost_25(),
	PhoneMetaTerritory_t2073820963::get_offset_of_personalNumber_26(),
	PhoneMetaTerritory_t2073820963::get_offset_of_voip_27(),
	PhoneMetaTerritory_t2073820963::get_offset_of_uan_28(),
	PhoneMetaTerritory_t2073820963::get_offset_of_voicemail_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6293 = { sizeof (PhoneMetaNumberFormat_t4038650015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6293[7] = 
{
	PhoneMetaNumberFormat_t4038650015::get_offset_of_m_pattern_0(),
	PhoneMetaNumberFormat_t4038650015::get_offset_of_m_nationalPrefixFormattingRule_1(),
	PhoneMetaNumberFormat_t4038650015::get_offset_of_m_carrierCodeFormattingRule_2(),
	PhoneMetaNumberFormat_t4038650015::get_offset_of_m_nationalPrefixOptionalWhenFormatting_3(),
	PhoneMetaNumberFormat_t4038650015::get_offset_of_format_4(),
	PhoneMetaNumberFormat_t4038650015::get_offset_of_intlFormat_5(),
	PhoneMetaNumberFormat_t4038650015::get_offset_of_leadingDigits_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6294 = { sizeof (PhoneMetaNumberInfo_t3299665448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6294[3] = 
{
	PhoneMetaNumberInfo_t3299665448::get_offset_of_nationalNumberPattern_0(),
	PhoneMetaNumberInfo_t3299665448::get_offset_of_possibleNumberPattern_1(),
	PhoneMetaNumberInfo_t3299665448::get_offset_of_exampleNumber_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6295 = { sizeof (PhoneNumberMatch_t2163858580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6295[3] = 
{
	PhoneNumberMatch_t2163858580::get_offset_of_U3CStartU3Ek__BackingField_0(),
	PhoneNumberMatch_t2163858580::get_offset_of_U3CRawStringU3Ek__BackingField_1(),
	PhoneNumberMatch_t2163858580::get_offset_of_U3CNumberU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6296 = { sizeof (PhoneNumberMatcher_t3898172501), -1, sizeof(PhoneNumberMatcher_t3898172501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6296[15] = 
{
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_PATTERN_0(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_PUB_PAGES_1(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_SLASH_SEPARATED_DATES_2(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_TIME_STAMPS_3(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_TIME_STAMPS_SUFFIX_4(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_MATCHING_BRACKETS_5(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_LEAD_CLASS_6(),
	PhoneNumberMatcher_t3898172501_StaticFields::get_offset_of_GROUP_SEPARATOR_7(),
	PhoneNumberMatcher_t3898172501::get_offset_of_phoneUtil_8(),
	PhoneNumberMatcher_t3898172501::get_offset_of_text_9(),
	PhoneNumberMatcher_t3898172501::get_offset_of_preferredRegion_10(),
	PhoneNumberMatcher_t3898172501::get_offset_of_leniency_11(),
	PhoneNumberMatcher_t3898172501::get_offset_of_maxTries_12(),
	PhoneNumberMatcher_t3898172501::get_offset_of_lastMatch_13(),
	PhoneNumberMatcher_t3898172501::get_offset_of_searchIndex_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6297 = { sizeof (CheckGroups_t2785688338), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6298 = { sizeof (Locale_t2376277312), -1, sizeof(Locale_t2376277312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6298[8] = 
{
	Locale_t2376277312_StaticFields::get_offset_of_ENGLISH_0(),
	Locale_t2376277312_StaticFields::get_offset_of_FRENCH_1(),
	Locale_t2376277312_StaticFields::get_offset_of_GERMAN_2(),
	Locale_t2376277312_StaticFields::get_offset_of_ITALIAN_3(),
	Locale_t2376277312_StaticFields::get_offset_of_KOREAN_4(),
	Locale_t2376277312_StaticFields::get_offset_of_SIMPLIFIED_CHINESE_5(),
	Locale_t2376277312::get_offset_of_Language_6(),
	Locale_t2376277312::get_offset_of_Country_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6299 = { sizeof (PhoneNumberOfflineGeocoder_t3035616292), -1, sizeof(PhoneNumberOfflineGeocoder_t3035616292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6299[7] = 
{
	PhoneNumberOfflineGeocoder_t3035616292_StaticFields::get_offset_of_instance_0(),
	0,
	PhoneNumberOfflineGeocoder_t3035616292_StaticFields::get_offset_of_thisLock_2(),
	PhoneNumberOfflineGeocoder_t3035616292::get_offset_of_phoneUtil_3(),
	PhoneNumberOfflineGeocoder_t3035616292::get_offset_of_phonePrefixDataDirectory_4(),
	PhoneNumberOfflineGeocoder_t3035616292::get_offset_of_mappingFileProvider_5(),
	PhoneNumberOfflineGeocoder_t3035616292::get_offset_of_availablePhonePrefixMaps_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
