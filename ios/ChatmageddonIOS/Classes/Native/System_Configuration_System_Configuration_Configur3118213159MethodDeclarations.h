﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationSaveEventHandler
struct ConfigurationSaveEventHandler_t3118213159;
// System.Object
struct Il2CppObject;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Configuration.ConfigurationSaveEventArgs
struct ConfigurationSaveEventArgs_t3884867164;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Configuration_System_Configuration_Configur3884867164.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Configuration.ConfigurationSaveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ConfigurationSaveEventHandler__ctor_m1951017930 (ConfigurationSaveEventHandler_t3118213159 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventHandler::Invoke(System.Configuration.Configuration,System.Configuration.ConfigurationSaveEventArgs)
extern "C"  void ConfigurationSaveEventHandler_Invoke_m3964324988 (ConfigurationSaveEventHandler_t3118213159 * __this, Configuration_t3335372970 * ___sender0, ConfigurationSaveEventArgs_t3884867164 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Configuration.ConfigurationSaveEventHandler::BeginInvoke(System.Configuration.Configuration,System.Configuration.ConfigurationSaveEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConfigurationSaveEventHandler_BeginInvoke_m2258769201 (ConfigurationSaveEventHandler_t3118213159 * __this, Configuration_t3335372970 * ___sender0, ConfigurationSaveEventArgs_t3884867164 * ___args1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ConfigurationSaveEventHandler_EndInvoke_m1806851696 (ConfigurationSaveEventHandler_t3118213159 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
