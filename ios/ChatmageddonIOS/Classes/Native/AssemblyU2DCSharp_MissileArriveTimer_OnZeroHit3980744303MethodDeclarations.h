﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileArriveTimer/OnZeroHit
struct OnZeroHit_t3980744303;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void MissileArriveTimer/OnZeroHit::.ctor(System.Object,System.IntPtr)
extern "C"  void OnZeroHit__ctor_m2109394034 (OnZeroHit_t3980744303 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer/OnZeroHit::Invoke()
extern "C"  void OnZeroHit_Invoke_m3399458872 (OnZeroHit_t3980744303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MissileArriveTimer/OnZeroHit::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnZeroHit_BeginInvoke_m2903451713 (OnZeroHit_t3980744303 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer/OnZeroHit::EndInvoke(System.IAsyncResult)
extern "C"  void OnZeroHit_EndInvoke_m2461012960 (OnZeroHit_t3980744303 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
