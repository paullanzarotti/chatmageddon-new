﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberOfflineGeocoder/<LoadMappingFileProvider>c__AnonStorey0
struct  U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703  : public Il2CppObject
{
public:
	// System.String PhoneNumbers.PhoneNumberOfflineGeocoder/<LoadMappingFileProvider>c__AnonStorey0::prefix
	String_t* ___prefix_0;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
