﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineAudienceTab
struct MineAudienceTab_t1025879672;

#include "codegen/il2cpp-codegen.h"

// System.Void MineAudienceTab::.ctor()
extern "C"  void MineAudienceTab__ctor_m2821507507 (MineAudienceTab_t1025879672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineAudienceTab::OnButtonClick()
extern "C"  void MineAudienceTab_OnButtonClick_m3574760924 (MineAudienceTab_t1025879672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineAudienceTab::ActivateTab(System.Boolean)
extern "C"  void MineAudienceTab_ActivateTab_m3642495510 (MineAudienceTab_t1025879672 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
