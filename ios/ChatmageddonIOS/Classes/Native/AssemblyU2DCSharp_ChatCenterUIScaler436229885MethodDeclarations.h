﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatCenterUIScaler
struct ChatCenterUIScaler_t436229885;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatCenterUIScaler::.ctor()
extern "C"  void ChatCenterUIScaler__ctor_m497026400 (ChatCenterUIScaler_t436229885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatCenterUIScaler::GlobalUIScale()
extern "C"  void ChatCenterUIScaler_GlobalUIScale_m1647900807 (ChatCenterUIScaler_t436229885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
