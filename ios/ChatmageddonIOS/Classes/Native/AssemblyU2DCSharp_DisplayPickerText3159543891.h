﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPTextPicker
struct IPTextPicker_t3074051364;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisplayPickerText
struct  DisplayPickerText_t3159543891  : public MonoBehaviour_t1158329972
{
public:
	// IPTextPicker DisplayPickerText::picker
	IPTextPicker_t3074051364 * ___picker_2;
	// UILabel DisplayPickerText::_label
	UILabel_t1795115428 * ____label_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(DisplayPickerText_t3159543891, ___picker_2)); }
	inline IPTextPicker_t3074051364 * get_picker_2() const { return ___picker_2; }
	inline IPTextPicker_t3074051364 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(IPTextPicker_t3074051364 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier(&___picker_2, value);
	}

	inline static int32_t get_offset_of__label_3() { return static_cast<int32_t>(offsetof(DisplayPickerText_t3159543891, ____label_3)); }
	inline UILabel_t1795115428 * get__label_3() const { return ____label_3; }
	inline UILabel_t1795115428 ** get_address_of__label_3() { return &____label_3; }
	inline void set__label_3(UILabel_t1795115428 * value)
	{
		____label_3 = value;
		Il2CppCodeGenWriteBarrier(&____label_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
