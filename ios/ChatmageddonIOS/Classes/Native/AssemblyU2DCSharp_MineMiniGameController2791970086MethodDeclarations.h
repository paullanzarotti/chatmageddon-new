﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineMiniGameController
struct MineMiniGameController_t2791970086;

#include "codegen/il2cpp-codegen.h"

// System.Void MineMiniGameController::.ctor()
extern "C"  void MineMiniGameController__ctor_m2124946849 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::Awake()
extern "C"  void MineMiniGameController_Awake_m234583190 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::PopulateMineGame()
extern "C"  void MineMiniGameController_PopulateMineGame_m1556053682 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::RemoveMineGame()
extern "C"  void MineMiniGameController_RemoveMineGame_m3175519576 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::OnPosTweenFinished()
extern "C"  void MineMiniGameController_OnPosTweenFinished_m3275174991 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::StartMiniGame()
extern "C"  void MineMiniGameController_StartMiniGame_m3570043768 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::IntroAnimationFinished()
extern "C"  void MineMiniGameController_IntroAnimationFinished_m3540135607 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::CheckSwipeWin(System.Int32)
extern "C"  void MineMiniGameController_CheckSwipeWin_m714150032 (MineMiniGameController_t2791970086 * __this, int32_t ___swipeNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::SetSwipeCuts(System.Int32)
extern "C"  void MineMiniGameController_SetSwipeCuts_m3445857327 (MineMiniGameController_t2791970086 * __this, int32_t ___wireNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::SetSwipesActive(System.Boolean)
extern "C"  void MineMiniGameController_SetSwipesActive_m1303988119 (MineMiniGameController_t2791970086 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMiniGameController::OnGameCountdownFinished()
extern "C"  void MineMiniGameController_OnGameCountdownFinished_m4217042253 (MineMiniGameController_t2791970086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
