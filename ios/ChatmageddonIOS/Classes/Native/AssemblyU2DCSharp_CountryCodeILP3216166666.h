﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3026611799.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountryCodeILP
struct  CountryCodeILP_t3216166666  : public BaseInfiniteListPopulator_1_t3026611799
{
public:
	// System.Boolean CountryCodeILP::listLoaded
	bool ___listLoaded_33;
	// System.Collections.ArrayList CountryCodeILP::ccArray
	ArrayList_t4252133567 * ___ccArray_34;

public:
	inline static int32_t get_offset_of_listLoaded_33() { return static_cast<int32_t>(offsetof(CountryCodeILP_t3216166666, ___listLoaded_33)); }
	inline bool get_listLoaded_33() const { return ___listLoaded_33; }
	inline bool* get_address_of_listLoaded_33() { return &___listLoaded_33; }
	inline void set_listLoaded_33(bool value)
	{
		___listLoaded_33 = value;
	}

	inline static int32_t get_offset_of_ccArray_34() { return static_cast<int32_t>(offsetof(CountryCodeILP_t3216166666, ___ccArray_34)); }
	inline ArrayList_t4252133567 * get_ccArray_34() const { return ___ccArray_34; }
	inline ArrayList_t4252133567 ** get_address_of_ccArray_34() { return &___ccArray_34; }
	inline void set_ccArray_34(ArrayList_t4252133567 * value)
	{
		___ccArray_34 = value;
		Il2CppCodeGenWriteBarrier(&___ccArray_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
