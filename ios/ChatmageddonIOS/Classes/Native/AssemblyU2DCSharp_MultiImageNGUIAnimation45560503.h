﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t1612747451;

#include "AssemblyU2DCSharp_UITextureAnimation3587053699.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiImageNGUIAnimation
struct  MultiImageNGUIAnimation_t45560503  : public UITextureAnimation_t3587053699
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture> MultiImageNGUIAnimation::textures
	List_1_t1612747451 * ___textures_13;

public:
	inline static int32_t get_offset_of_textures_13() { return static_cast<int32_t>(offsetof(MultiImageNGUIAnimation_t45560503, ___textures_13)); }
	inline List_1_t1612747451 * get_textures_13() const { return ___textures_13; }
	inline List_1_t1612747451 ** get_address_of_textures_13() { return &___textures_13; }
	inline void set_textures_13(List_1_t1612747451 * value)
	{
		___textures_13 = value;
		Il2CppCodeGenWriteBarrier(&___textures_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
