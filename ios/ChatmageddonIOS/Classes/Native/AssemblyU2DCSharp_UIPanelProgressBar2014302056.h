﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_ProgressBar192201240.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanelProgressBar
struct  UIPanelProgressBar_t2014302056  : public ProgressBar_t192201240
{
public:
	// UIPanel UIPanelProgressBar::progressPanel
	UIPanel_t1795085332 * ___progressPanel_8;
	// System.Single UIPanelProgressBar::maxPanelHeight
	float ___maxPanelHeight_9;

public:
	inline static int32_t get_offset_of_progressPanel_8() { return static_cast<int32_t>(offsetof(UIPanelProgressBar_t2014302056, ___progressPanel_8)); }
	inline UIPanel_t1795085332 * get_progressPanel_8() const { return ___progressPanel_8; }
	inline UIPanel_t1795085332 ** get_address_of_progressPanel_8() { return &___progressPanel_8; }
	inline void set_progressPanel_8(UIPanel_t1795085332 * value)
	{
		___progressPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___progressPanel_8, value);
	}

	inline static int32_t get_offset_of_maxPanelHeight_9() { return static_cast<int32_t>(offsetof(UIPanelProgressBar_t2014302056, ___maxPanelHeight_9)); }
	inline float get_maxPanelHeight_9() const { return ___maxPanelHeight_9; }
	inline float* get_address_of_maxPanelHeight_9() { return &___maxPanelHeight_9; }
	inline void set_maxPanelHeight_9(float value)
	{
		___maxPanelHeight_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
