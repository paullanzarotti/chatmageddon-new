﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddressBook/<DeleteContactsBatchFromServer>c__AnonStorey1
struct U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AddressBook/<DeleteContactsBatchFromServer>c__AnonStorey1::.ctor()
extern "C"  void U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1__ctor_m1820463466 (U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook/<DeleteContactsBatchFromServer>c__AnonStorey1::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_U3CU3Em__0_m1470888354 (U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
