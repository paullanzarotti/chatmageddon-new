﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenScale
struct TweenScale_t2697902175;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitConfirmPopUp
struct  ExitConfirmPopUp_t674592142  : public MonoBehaviour_t1158329972
{
public:
	// TweenAlpha ExitConfirmPopUp::backgroundAlphaTween
	TweenAlpha_t2421518635 * ___backgroundAlphaTween_2;
	// TweenScale ExitConfirmPopUp::contentScaleTween
	TweenScale_t2697902175 * ___contentScaleTween_3;
	// System.Boolean ExitConfirmPopUp::popUpClosing
	bool ___popUpClosing_4;

public:
	inline static int32_t get_offset_of_backgroundAlphaTween_2() { return static_cast<int32_t>(offsetof(ExitConfirmPopUp_t674592142, ___backgroundAlphaTween_2)); }
	inline TweenAlpha_t2421518635 * get_backgroundAlphaTween_2() const { return ___backgroundAlphaTween_2; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundAlphaTween_2() { return &___backgroundAlphaTween_2; }
	inline void set_backgroundAlphaTween_2(TweenAlpha_t2421518635 * value)
	{
		___backgroundAlphaTween_2 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundAlphaTween_2, value);
	}

	inline static int32_t get_offset_of_contentScaleTween_3() { return static_cast<int32_t>(offsetof(ExitConfirmPopUp_t674592142, ___contentScaleTween_3)); }
	inline TweenScale_t2697902175 * get_contentScaleTween_3() const { return ___contentScaleTween_3; }
	inline TweenScale_t2697902175 ** get_address_of_contentScaleTween_3() { return &___contentScaleTween_3; }
	inline void set_contentScaleTween_3(TweenScale_t2697902175 * value)
	{
		___contentScaleTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___contentScaleTween_3, value);
	}

	inline static int32_t get_offset_of_popUpClosing_4() { return static_cast<int32_t>(offsetof(ExitConfirmPopUp_t674592142, ___popUpClosing_4)); }
	inline bool get_popUpClosing_4() const { return ___popUpClosing_4; }
	inline bool* get_address_of_popUpClosing_4() { return &___popUpClosing_4; }
	inline void set_popUpClosing_4(bool value)
	{
		___popUpClosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
