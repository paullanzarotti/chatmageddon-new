﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Posterize
struct CameraFilterPack_TV_Posterize_t2336945505;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Posterize::.ctor()
extern "C"  void CameraFilterPack_TV_Posterize__ctor_m1142670492 (CameraFilterPack_TV_Posterize_t2336945505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Posterize::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Posterize_get_material_m1706012261 (CameraFilterPack_TV_Posterize_t2336945505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::Start()
extern "C"  void CameraFilterPack_TV_Posterize_Start_m785432060 (CameraFilterPack_TV_Posterize_t2336945505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Posterize_OnRenderImage_m3388359684 (CameraFilterPack_TV_Posterize_t2336945505 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::OnValidate()
extern "C"  void CameraFilterPack_TV_Posterize_OnValidate_m500725807 (CameraFilterPack_TV_Posterize_t2336945505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::Update()
extern "C"  void CameraFilterPack_TV_Posterize_Update_m4128193409 (CameraFilterPack_TV_Posterize_t2336945505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::OnDisable()
extern "C"  void CameraFilterPack_TV_Posterize_OnDisable_m3325677 (CameraFilterPack_TV_Posterize_t2336945505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
