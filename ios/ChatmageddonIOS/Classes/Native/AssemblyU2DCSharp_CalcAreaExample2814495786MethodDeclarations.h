﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CalcAreaExample
struct CalcAreaExample_t2814495786;

#include "codegen/il2cpp-codegen.h"

// System.Void CalcAreaExample::.ctor()
extern "C"  void CalcAreaExample__ctor_m3100282261 (CalcAreaExample_t2814495786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CalcAreaExample::CheckMarkerPositions()
extern "C"  void CalcAreaExample_CheckMarkerPositions_m2135256339 (CalcAreaExample_t2814495786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CalcAreaExample::OnMouseUp()
extern "C"  void CalcAreaExample_OnMouseUp_m4208250704 (CalcAreaExample_t2814495786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CalcAreaExample::Start()
extern "C"  void CalcAreaExample_Start_m2347099653 (CalcAreaExample_t2814495786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CalcAreaExample::Update()
extern "C"  void CalcAreaExample_Update_m2571000136 (CalcAreaExample_t2814495786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
