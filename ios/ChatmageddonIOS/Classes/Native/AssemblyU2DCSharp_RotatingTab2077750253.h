﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RotatingTabController
struct RotatingTabController_t2617193705;
// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_PlayState1063998429.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotatingTab
struct  RotatingTab_t2077750253  : public MonoBehaviour_t1158329972
{
public:
	// RotatingTabController RotatingTab::controller
	RotatingTabController_t2617193705 * ___controller_2;
	// UISprite RotatingTab::tabSprite
	UISprite_t603616735 * ___tabSprite_3;
	// UnityEngine.Transform RotatingTab::rotationAnchor
	Transform_t3275118058 * ___rotationAnchor_4;
	// System.Single RotatingTab::speed
	float ___speed_5;
	// PlayState RotatingTab::currentState
	int32_t ___currentState_6;
	// UnityEngine.Coroutine RotatingTab::rotationRoutine
	Coroutine_t2299508840 * ___rotationRoutine_7;

public:
	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(RotatingTab_t2077750253, ___controller_2)); }
	inline RotatingTabController_t2617193705 * get_controller_2() const { return ___controller_2; }
	inline RotatingTabController_t2617193705 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(RotatingTabController_t2617193705 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___controller_2, value);
	}

	inline static int32_t get_offset_of_tabSprite_3() { return static_cast<int32_t>(offsetof(RotatingTab_t2077750253, ___tabSprite_3)); }
	inline UISprite_t603616735 * get_tabSprite_3() const { return ___tabSprite_3; }
	inline UISprite_t603616735 ** get_address_of_tabSprite_3() { return &___tabSprite_3; }
	inline void set_tabSprite_3(UISprite_t603616735 * value)
	{
		___tabSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___tabSprite_3, value);
	}

	inline static int32_t get_offset_of_rotationAnchor_4() { return static_cast<int32_t>(offsetof(RotatingTab_t2077750253, ___rotationAnchor_4)); }
	inline Transform_t3275118058 * get_rotationAnchor_4() const { return ___rotationAnchor_4; }
	inline Transform_t3275118058 ** get_address_of_rotationAnchor_4() { return &___rotationAnchor_4; }
	inline void set_rotationAnchor_4(Transform_t3275118058 * value)
	{
		___rotationAnchor_4 = value;
		Il2CppCodeGenWriteBarrier(&___rotationAnchor_4, value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(RotatingTab_t2077750253, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_currentState_6() { return static_cast<int32_t>(offsetof(RotatingTab_t2077750253, ___currentState_6)); }
	inline int32_t get_currentState_6() const { return ___currentState_6; }
	inline int32_t* get_address_of_currentState_6() { return &___currentState_6; }
	inline void set_currentState_6(int32_t value)
	{
		___currentState_6 = value;
	}

	inline static int32_t get_offset_of_rotationRoutine_7() { return static_cast<int32_t>(offsetof(RotatingTab_t2077750253, ___rotationRoutine_7)); }
	inline Coroutine_t2299508840 * get_rotationRoutine_7() const { return ___rotationRoutine_7; }
	inline Coroutine_t2299508840 ** get_address_of_rotationRoutine_7() { return &___rotationRoutine_7; }
	inline void set_rotationRoutine_7(Coroutine_t2299508840 * value)
	{
		___rotationRoutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___rotationRoutine_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
