﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigBase64Transform
struct XmlDsigBase64Transform_t3785997915;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigBase64Transform::.ctor()
extern "C"  void XmlDsigBase64Transform__ctor_m4076918245 (XmlDsigBase64Transform_t3785997915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigBase64Transform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDsigBase64Transform_GetInnerXml_m4251397162 (XmlDsigBase64Transform_t3785997915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigBase64Transform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDsigBase64Transform_LoadInnerXml_m3882880483 (XmlDsigBase64Transform_t3785997915 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
