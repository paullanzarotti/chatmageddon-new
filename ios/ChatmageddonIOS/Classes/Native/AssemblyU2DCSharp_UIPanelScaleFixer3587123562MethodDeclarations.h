﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPanelScaleFixer
struct UIPanelScaleFixer_t3587123562;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPanelScaleFixer::.ctor()
extern "C"  void UIPanelScaleFixer__ctor_m116984097 (UIPanelScaleFixer_t3587123562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelScaleFixer::FixedUpdate()
extern "C"  void UIPanelScaleFixer_FixedUpdate_m1089143110 (UIPanelScaleFixer_t3587123562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelScaleFixer::CalcScale()
extern "C"  void UIPanelScaleFixer_CalcScale_m1378518000 (UIPanelScaleFixer_t3587123562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelScaleFixer::FixScale()
extern "C"  void UIPanelScaleFixer_FixScale_m1574658902 (UIPanelScaleFixer_t3587123562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
