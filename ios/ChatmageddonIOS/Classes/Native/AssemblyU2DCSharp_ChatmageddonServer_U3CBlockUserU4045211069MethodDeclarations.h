﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<BlockUser>c__AnonStoreyA
struct U3CBlockUserU3Ec__AnonStoreyA_t4045211069;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<BlockUser>c__AnonStoreyA::.ctor()
extern "C"  void U3CBlockUserU3Ec__AnonStoreyA__ctor_m1778402936 (U3CBlockUserU3Ec__AnonStoreyA_t4045211069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<BlockUser>c__AnonStoreyA::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CBlockUserU3Ec__AnonStoreyA_U3CU3Em__0_m598606037 (U3CBlockUserU3Ec__AnonStoreyA_t4045211069 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
