﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<FriendSearchNavScreen>
struct Comparer_1_t1908398282;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<FriendSearchNavScreen>::.ctor()
extern "C"  void Comparer_1__ctor_m4170128194_gshared (Comparer_1_t1908398282 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m4170128194(__this, method) ((  void (*) (Comparer_1_t1908398282 *, const MethodInfo*))Comparer_1__ctor_m4170128194_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<FriendSearchNavScreen>::.cctor()
extern "C"  void Comparer_1__cctor_m4105215187_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m4105215187(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m4105215187_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<FriendSearchNavScreen>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m112972899_gshared (Comparer_1_t1908398282 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m112972899(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1908398282 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m112972899_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<FriendSearchNavScreen>::get_Default()
extern "C"  Comparer_1_t1908398282 * Comparer_1_get_Default_m2612523966_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2612523966(__this /* static, unused */, method) ((  Comparer_1_t1908398282 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2612523966_gshared)(__this /* static, unused */, method)
