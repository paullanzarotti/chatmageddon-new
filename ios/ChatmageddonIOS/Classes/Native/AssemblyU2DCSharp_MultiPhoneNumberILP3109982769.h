﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen736275958.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiPhoneNumberILP
struct  MultiPhoneNumberILP_t3109982769  : public BaseInfiniteListPopulator_1_t736275958
{
public:
	// System.Int32 MultiPhoneNumberILP::maxNameLength
	int32_t ___maxNameLength_33;
	// System.Boolean MultiPhoneNumberILP::listLoaded
	bool ___listLoaded_34;
	// System.Collections.ArrayList MultiPhoneNumberILP::numbersArray
	ArrayList_t4252133567 * ___numbersArray_35;
	// UnityEngine.GameObject MultiPhoneNumberILP::contentObject
	GameObject_t1756533147 * ___contentObject_36;
	// UILabel MultiPhoneNumberILP::nameLabel
	UILabel_t1795115428 * ___nameLabel_37;
	// TweenAlpha MultiPhoneNumberILP::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_38;
	// UnityEngine.GameObject MultiPhoneNumberILP::bottomDivider
	GameObject_t1756533147 * ___bottomDivider_39;
	// System.Boolean MultiPhoneNumberILP::attackList
	bool ___attackList_40;
	// System.Boolean MultiPhoneNumberILP::uiClosing
	bool ___uiClosing_41;

public:
	inline static int32_t get_offset_of_maxNameLength_33() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___maxNameLength_33)); }
	inline int32_t get_maxNameLength_33() const { return ___maxNameLength_33; }
	inline int32_t* get_address_of_maxNameLength_33() { return &___maxNameLength_33; }
	inline void set_maxNameLength_33(int32_t value)
	{
		___maxNameLength_33 = value;
	}

	inline static int32_t get_offset_of_listLoaded_34() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___listLoaded_34)); }
	inline bool get_listLoaded_34() const { return ___listLoaded_34; }
	inline bool* get_address_of_listLoaded_34() { return &___listLoaded_34; }
	inline void set_listLoaded_34(bool value)
	{
		___listLoaded_34 = value;
	}

	inline static int32_t get_offset_of_numbersArray_35() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___numbersArray_35)); }
	inline ArrayList_t4252133567 * get_numbersArray_35() const { return ___numbersArray_35; }
	inline ArrayList_t4252133567 ** get_address_of_numbersArray_35() { return &___numbersArray_35; }
	inline void set_numbersArray_35(ArrayList_t4252133567 * value)
	{
		___numbersArray_35 = value;
		Il2CppCodeGenWriteBarrier(&___numbersArray_35, value);
	}

	inline static int32_t get_offset_of_contentObject_36() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___contentObject_36)); }
	inline GameObject_t1756533147 * get_contentObject_36() const { return ___contentObject_36; }
	inline GameObject_t1756533147 ** get_address_of_contentObject_36() { return &___contentObject_36; }
	inline void set_contentObject_36(GameObject_t1756533147 * value)
	{
		___contentObject_36 = value;
		Il2CppCodeGenWriteBarrier(&___contentObject_36, value);
	}

	inline static int32_t get_offset_of_nameLabel_37() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___nameLabel_37)); }
	inline UILabel_t1795115428 * get_nameLabel_37() const { return ___nameLabel_37; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_37() { return &___nameLabel_37; }
	inline void set_nameLabel_37(UILabel_t1795115428 * value)
	{
		___nameLabel_37 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_37, value);
	}

	inline static int32_t get_offset_of_alphaTween_38() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___alphaTween_38)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_38() const { return ___alphaTween_38; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_38() { return &___alphaTween_38; }
	inline void set_alphaTween_38(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_38 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_38, value);
	}

	inline static int32_t get_offset_of_bottomDivider_39() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___bottomDivider_39)); }
	inline GameObject_t1756533147 * get_bottomDivider_39() const { return ___bottomDivider_39; }
	inline GameObject_t1756533147 ** get_address_of_bottomDivider_39() { return &___bottomDivider_39; }
	inline void set_bottomDivider_39(GameObject_t1756533147 * value)
	{
		___bottomDivider_39 = value;
		Il2CppCodeGenWriteBarrier(&___bottomDivider_39, value);
	}

	inline static int32_t get_offset_of_attackList_40() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___attackList_40)); }
	inline bool get_attackList_40() const { return ___attackList_40; }
	inline bool* get_address_of_attackList_40() { return &___attackList_40; }
	inline void set_attackList_40(bool value)
	{
		___attackList_40 = value;
	}

	inline static int32_t get_offset_of_uiClosing_41() { return static_cast<int32_t>(offsetof(MultiPhoneNumberILP_t3109982769, ___uiClosing_41)); }
	inline bool get_uiClosing_41() const { return ___uiClosing_41; }
	inline bool* get_address_of_uiClosing_41() { return &___uiClosing_41; }
	inline void set_uiClosing_41(bool value)
	{
		___uiClosing_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
