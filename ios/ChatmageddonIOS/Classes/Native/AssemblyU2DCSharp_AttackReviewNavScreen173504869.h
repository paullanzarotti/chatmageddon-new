﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ReviewItemLoader
struct ReviewItemLoader_t43765888;
// ReviewTargetLoader
struct ReviewTargetLoader_t163570306;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// BuyActiveMissileButton
struct BuyActiveMissileButton_t2138134254;
// TweenScale
struct TweenScale_t2697902175;
// TweenRotation
struct TweenRotation_t1747194511;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackReviewNavScreen
struct  AttackReviewNavScreen_t173504869  : public NavigationScreen_t2333230110
{
public:
	// ReviewItemLoader AttackReviewNavScreen::itemLoader
	ReviewItemLoader_t43765888 * ___itemLoader_3;
	// ReviewTargetLoader AttackReviewNavScreen::targetLoader
	ReviewTargetLoader_t163570306 * ___targetLoader_4;
	// UnityEngine.GameObject AttackReviewNavScreen::itemLight
	GameObject_t1756533147 * ___itemLight_5;
	// BuyActiveMissileButton AttackReviewNavScreen::buyButton
	BuyActiveMissileButton_t2138134254 * ___buyButton_6;
	// TweenScale AttackReviewNavScreen::aimScaleTween
	TweenScale_t2697902175 * ___aimScaleTween_7;
	// TweenRotation AttackReviewNavScreen::aimRotationTween
	TweenRotation_t1747194511 * ___aimRotationTween_8;
	// System.Boolean AttackReviewNavScreen::reviewUIClosing
	bool ___reviewUIClosing_9;

public:
	inline static int32_t get_offset_of_itemLoader_3() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___itemLoader_3)); }
	inline ReviewItemLoader_t43765888 * get_itemLoader_3() const { return ___itemLoader_3; }
	inline ReviewItemLoader_t43765888 ** get_address_of_itemLoader_3() { return &___itemLoader_3; }
	inline void set_itemLoader_3(ReviewItemLoader_t43765888 * value)
	{
		___itemLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemLoader_3, value);
	}

	inline static int32_t get_offset_of_targetLoader_4() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___targetLoader_4)); }
	inline ReviewTargetLoader_t163570306 * get_targetLoader_4() const { return ___targetLoader_4; }
	inline ReviewTargetLoader_t163570306 ** get_address_of_targetLoader_4() { return &___targetLoader_4; }
	inline void set_targetLoader_4(ReviewTargetLoader_t163570306 * value)
	{
		___targetLoader_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetLoader_4, value);
	}

	inline static int32_t get_offset_of_itemLight_5() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___itemLight_5)); }
	inline GameObject_t1756533147 * get_itemLight_5() const { return ___itemLight_5; }
	inline GameObject_t1756533147 ** get_address_of_itemLight_5() { return &___itemLight_5; }
	inline void set_itemLight_5(GameObject_t1756533147 * value)
	{
		___itemLight_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_5, value);
	}

	inline static int32_t get_offset_of_buyButton_6() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___buyButton_6)); }
	inline BuyActiveMissileButton_t2138134254 * get_buyButton_6() const { return ___buyButton_6; }
	inline BuyActiveMissileButton_t2138134254 ** get_address_of_buyButton_6() { return &___buyButton_6; }
	inline void set_buyButton_6(BuyActiveMissileButton_t2138134254 * value)
	{
		___buyButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___buyButton_6, value);
	}

	inline static int32_t get_offset_of_aimScaleTween_7() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___aimScaleTween_7)); }
	inline TweenScale_t2697902175 * get_aimScaleTween_7() const { return ___aimScaleTween_7; }
	inline TweenScale_t2697902175 ** get_address_of_aimScaleTween_7() { return &___aimScaleTween_7; }
	inline void set_aimScaleTween_7(TweenScale_t2697902175 * value)
	{
		___aimScaleTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___aimScaleTween_7, value);
	}

	inline static int32_t get_offset_of_aimRotationTween_8() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___aimRotationTween_8)); }
	inline TweenRotation_t1747194511 * get_aimRotationTween_8() const { return ___aimRotationTween_8; }
	inline TweenRotation_t1747194511 ** get_address_of_aimRotationTween_8() { return &___aimRotationTween_8; }
	inline void set_aimRotationTween_8(TweenRotation_t1747194511 * value)
	{
		___aimRotationTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___aimRotationTween_8, value);
	}

	inline static int32_t get_offset_of_reviewUIClosing_9() { return static_cast<int32_t>(offsetof(AttackReviewNavScreen_t173504869, ___reviewUIClosing_9)); }
	inline bool get_reviewUIClosing_9() const { return ___reviewUIClosing_9; }
	inline bool* get_address_of_reviewUIClosing_9() { return &___reviewUIClosing_9; }
	inline void set_reviewUIClosing_9(bool value)
	{
		___reviewUIClosing_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
