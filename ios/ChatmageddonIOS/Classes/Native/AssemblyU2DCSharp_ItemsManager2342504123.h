﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Bucks>
struct List_1_t3301136852;
// System.Collections.Generic.List`1<Missile>
struct List_1_t183066060;
// System.Collections.Generic.List`1<Shield>
struct List_1_t2696242213;
// System.Collections.Generic.List`1<Mine>
struct List_1_t2098562409;
// System.Collections.Generic.List`1<Fuel>
struct List_1_t384667656;
// System.Collections.Generic.List`1<PurchaseableItem>
struct List_1_t2720244128;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2093169843.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsManager
struct  ItemsManager_t2342504123  : public MonoSingleton_1_t2093169843
{
public:
	// System.Collections.Generic.List`1<Bucks> ItemsManager::bucksItems
	List_1_t3301136852 * ___bucksItems_3;
	// System.Collections.Generic.List`1<Missile> ItemsManager::missileItems
	List_1_t183066060 * ___missileItems_4;
	// System.Collections.Generic.List`1<Shield> ItemsManager::shieldItems
	List_1_t2696242213 * ___shieldItems_5;
	// System.Collections.Generic.List`1<Mine> ItemsManager::mineItems
	List_1_t2098562409 * ___mineItems_6;
	// System.Collections.Generic.List`1<Fuel> ItemsManager::fuelItems
	List_1_t384667656 * ___fuelItems_7;
	// System.Collections.Generic.List`1<PurchaseableItem> ItemsManager::missilePurchaseableItems
	List_1_t2720244128 * ___missilePurchaseableItems_8;
	// System.Collections.Generic.List`1<PurchaseableItem> ItemsManager::shieldPurchaseableItems
	List_1_t2720244128 * ___shieldPurchaseableItems_9;
	// System.Collections.Generic.List`1<PurchaseableItem> ItemsManager::minePurchaseableItems
	List_1_t2720244128 * ___minePurchaseableItems_10;
	// System.Collections.Generic.List`1<PurchaseableItem> ItemsManager::fuelPurchaseableItems
	List_1_t2720244128 * ___fuelPurchaseableItems_11;
	// System.Collections.Generic.List`1<PurchaseableItem> ItemsManager::bundlePurchaseableItems
	List_1_t2720244128 * ___bundlePurchaseableItems_12;

public:
	inline static int32_t get_offset_of_bucksItems_3() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___bucksItems_3)); }
	inline List_1_t3301136852 * get_bucksItems_3() const { return ___bucksItems_3; }
	inline List_1_t3301136852 ** get_address_of_bucksItems_3() { return &___bucksItems_3; }
	inline void set_bucksItems_3(List_1_t3301136852 * value)
	{
		___bucksItems_3 = value;
		Il2CppCodeGenWriteBarrier(&___bucksItems_3, value);
	}

	inline static int32_t get_offset_of_missileItems_4() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___missileItems_4)); }
	inline List_1_t183066060 * get_missileItems_4() const { return ___missileItems_4; }
	inline List_1_t183066060 ** get_address_of_missileItems_4() { return &___missileItems_4; }
	inline void set_missileItems_4(List_1_t183066060 * value)
	{
		___missileItems_4 = value;
		Il2CppCodeGenWriteBarrier(&___missileItems_4, value);
	}

	inline static int32_t get_offset_of_shieldItems_5() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___shieldItems_5)); }
	inline List_1_t2696242213 * get_shieldItems_5() const { return ___shieldItems_5; }
	inline List_1_t2696242213 ** get_address_of_shieldItems_5() { return &___shieldItems_5; }
	inline void set_shieldItems_5(List_1_t2696242213 * value)
	{
		___shieldItems_5 = value;
		Il2CppCodeGenWriteBarrier(&___shieldItems_5, value);
	}

	inline static int32_t get_offset_of_mineItems_6() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___mineItems_6)); }
	inline List_1_t2098562409 * get_mineItems_6() const { return ___mineItems_6; }
	inline List_1_t2098562409 ** get_address_of_mineItems_6() { return &___mineItems_6; }
	inline void set_mineItems_6(List_1_t2098562409 * value)
	{
		___mineItems_6 = value;
		Il2CppCodeGenWriteBarrier(&___mineItems_6, value);
	}

	inline static int32_t get_offset_of_fuelItems_7() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___fuelItems_7)); }
	inline List_1_t384667656 * get_fuelItems_7() const { return ___fuelItems_7; }
	inline List_1_t384667656 ** get_address_of_fuelItems_7() { return &___fuelItems_7; }
	inline void set_fuelItems_7(List_1_t384667656 * value)
	{
		___fuelItems_7 = value;
		Il2CppCodeGenWriteBarrier(&___fuelItems_7, value);
	}

	inline static int32_t get_offset_of_missilePurchaseableItems_8() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___missilePurchaseableItems_8)); }
	inline List_1_t2720244128 * get_missilePurchaseableItems_8() const { return ___missilePurchaseableItems_8; }
	inline List_1_t2720244128 ** get_address_of_missilePurchaseableItems_8() { return &___missilePurchaseableItems_8; }
	inline void set_missilePurchaseableItems_8(List_1_t2720244128 * value)
	{
		___missilePurchaseableItems_8 = value;
		Il2CppCodeGenWriteBarrier(&___missilePurchaseableItems_8, value);
	}

	inline static int32_t get_offset_of_shieldPurchaseableItems_9() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___shieldPurchaseableItems_9)); }
	inline List_1_t2720244128 * get_shieldPurchaseableItems_9() const { return ___shieldPurchaseableItems_9; }
	inline List_1_t2720244128 ** get_address_of_shieldPurchaseableItems_9() { return &___shieldPurchaseableItems_9; }
	inline void set_shieldPurchaseableItems_9(List_1_t2720244128 * value)
	{
		___shieldPurchaseableItems_9 = value;
		Il2CppCodeGenWriteBarrier(&___shieldPurchaseableItems_9, value);
	}

	inline static int32_t get_offset_of_minePurchaseableItems_10() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___minePurchaseableItems_10)); }
	inline List_1_t2720244128 * get_minePurchaseableItems_10() const { return ___minePurchaseableItems_10; }
	inline List_1_t2720244128 ** get_address_of_minePurchaseableItems_10() { return &___minePurchaseableItems_10; }
	inline void set_minePurchaseableItems_10(List_1_t2720244128 * value)
	{
		___minePurchaseableItems_10 = value;
		Il2CppCodeGenWriteBarrier(&___minePurchaseableItems_10, value);
	}

	inline static int32_t get_offset_of_fuelPurchaseableItems_11() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___fuelPurchaseableItems_11)); }
	inline List_1_t2720244128 * get_fuelPurchaseableItems_11() const { return ___fuelPurchaseableItems_11; }
	inline List_1_t2720244128 ** get_address_of_fuelPurchaseableItems_11() { return &___fuelPurchaseableItems_11; }
	inline void set_fuelPurchaseableItems_11(List_1_t2720244128 * value)
	{
		___fuelPurchaseableItems_11 = value;
		Il2CppCodeGenWriteBarrier(&___fuelPurchaseableItems_11, value);
	}

	inline static int32_t get_offset_of_bundlePurchaseableItems_12() { return static_cast<int32_t>(offsetof(ItemsManager_t2342504123, ___bundlePurchaseableItems_12)); }
	inline List_1_t2720244128 * get_bundlePurchaseableItems_12() const { return ___bundlePurchaseableItems_12; }
	inline List_1_t2720244128 ** get_address_of_bundlePurchaseableItems_12() { return &___bundlePurchaseableItems_12; }
	inline void set_bundlePurchaseableItems_12(List_1_t2720244128 * value)
	{
		___bundlePurchaseableItems_12 = value;
		Il2CppCodeGenWriteBarrier(&___bundlePurchaseableItems_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
