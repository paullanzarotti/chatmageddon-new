﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.Locale
struct Locale_t2376277312;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.Locale
struct  Locale_t2376277312  : public Il2CppObject
{
public:
	// System.String PhoneNumbers.Locale::Language
	String_t* ___Language_6;
	// System.String PhoneNumbers.Locale::Country
	String_t* ___Country_7;

public:
	inline static int32_t get_offset_of_Language_6() { return static_cast<int32_t>(offsetof(Locale_t2376277312, ___Language_6)); }
	inline String_t* get_Language_6() const { return ___Language_6; }
	inline String_t** get_address_of_Language_6() { return &___Language_6; }
	inline void set_Language_6(String_t* value)
	{
		___Language_6 = value;
		Il2CppCodeGenWriteBarrier(&___Language_6, value);
	}

	inline static int32_t get_offset_of_Country_7() { return static_cast<int32_t>(offsetof(Locale_t2376277312, ___Country_7)); }
	inline String_t* get_Country_7() const { return ___Country_7; }
	inline String_t** get_address_of_Country_7() { return &___Country_7; }
	inline void set_Country_7(String_t* value)
	{
		___Country_7 = value;
		Il2CppCodeGenWriteBarrier(&___Country_7, value);
	}
};

struct Locale_t2376277312_StaticFields
{
public:
	// PhoneNumbers.Locale PhoneNumbers.Locale::ENGLISH
	Locale_t2376277312 * ___ENGLISH_0;
	// PhoneNumbers.Locale PhoneNumbers.Locale::FRENCH
	Locale_t2376277312 * ___FRENCH_1;
	// PhoneNumbers.Locale PhoneNumbers.Locale::GERMAN
	Locale_t2376277312 * ___GERMAN_2;
	// PhoneNumbers.Locale PhoneNumbers.Locale::ITALIAN
	Locale_t2376277312 * ___ITALIAN_3;
	// PhoneNumbers.Locale PhoneNumbers.Locale::KOREAN
	Locale_t2376277312 * ___KOREAN_4;
	// PhoneNumbers.Locale PhoneNumbers.Locale::SIMPLIFIED_CHINESE
	Locale_t2376277312 * ___SIMPLIFIED_CHINESE_5;

public:
	inline static int32_t get_offset_of_ENGLISH_0() { return static_cast<int32_t>(offsetof(Locale_t2376277312_StaticFields, ___ENGLISH_0)); }
	inline Locale_t2376277312 * get_ENGLISH_0() const { return ___ENGLISH_0; }
	inline Locale_t2376277312 ** get_address_of_ENGLISH_0() { return &___ENGLISH_0; }
	inline void set_ENGLISH_0(Locale_t2376277312 * value)
	{
		___ENGLISH_0 = value;
		Il2CppCodeGenWriteBarrier(&___ENGLISH_0, value);
	}

	inline static int32_t get_offset_of_FRENCH_1() { return static_cast<int32_t>(offsetof(Locale_t2376277312_StaticFields, ___FRENCH_1)); }
	inline Locale_t2376277312 * get_FRENCH_1() const { return ___FRENCH_1; }
	inline Locale_t2376277312 ** get_address_of_FRENCH_1() { return &___FRENCH_1; }
	inline void set_FRENCH_1(Locale_t2376277312 * value)
	{
		___FRENCH_1 = value;
		Il2CppCodeGenWriteBarrier(&___FRENCH_1, value);
	}

	inline static int32_t get_offset_of_GERMAN_2() { return static_cast<int32_t>(offsetof(Locale_t2376277312_StaticFields, ___GERMAN_2)); }
	inline Locale_t2376277312 * get_GERMAN_2() const { return ___GERMAN_2; }
	inline Locale_t2376277312 ** get_address_of_GERMAN_2() { return &___GERMAN_2; }
	inline void set_GERMAN_2(Locale_t2376277312 * value)
	{
		___GERMAN_2 = value;
		Il2CppCodeGenWriteBarrier(&___GERMAN_2, value);
	}

	inline static int32_t get_offset_of_ITALIAN_3() { return static_cast<int32_t>(offsetof(Locale_t2376277312_StaticFields, ___ITALIAN_3)); }
	inline Locale_t2376277312 * get_ITALIAN_3() const { return ___ITALIAN_3; }
	inline Locale_t2376277312 ** get_address_of_ITALIAN_3() { return &___ITALIAN_3; }
	inline void set_ITALIAN_3(Locale_t2376277312 * value)
	{
		___ITALIAN_3 = value;
		Il2CppCodeGenWriteBarrier(&___ITALIAN_3, value);
	}

	inline static int32_t get_offset_of_KOREAN_4() { return static_cast<int32_t>(offsetof(Locale_t2376277312_StaticFields, ___KOREAN_4)); }
	inline Locale_t2376277312 * get_KOREAN_4() const { return ___KOREAN_4; }
	inline Locale_t2376277312 ** get_address_of_KOREAN_4() { return &___KOREAN_4; }
	inline void set_KOREAN_4(Locale_t2376277312 * value)
	{
		___KOREAN_4 = value;
		Il2CppCodeGenWriteBarrier(&___KOREAN_4, value);
	}

	inline static int32_t get_offset_of_SIMPLIFIED_CHINESE_5() { return static_cast<int32_t>(offsetof(Locale_t2376277312_StaticFields, ___SIMPLIFIED_CHINESE_5)); }
	inline Locale_t2376277312 * get_SIMPLIFIED_CHINESE_5() const { return ___SIMPLIFIED_CHINESE_5; }
	inline Locale_t2376277312 ** get_address_of_SIMPLIFIED_CHINESE_5() { return &___SIMPLIFIED_CHINESE_5; }
	inline void set_SIMPLIFIED_CHINESE_5(Locale_t2376277312 * value)
	{
		___SIMPLIFIED_CHINESE_5 = value;
		Il2CppCodeGenWriteBarrier(&___SIMPLIFIED_CHINESE_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
