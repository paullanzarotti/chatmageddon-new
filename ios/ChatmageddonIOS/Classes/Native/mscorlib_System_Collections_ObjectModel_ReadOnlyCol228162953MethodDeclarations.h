﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>
struct ReadOnlyCollection_1_t228162953;
// System.Collections.Generic.IList`1<ChatNavScreen>
struct IList_1_t583317862;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ChatNavScreen[]
struct ChatNavScreenU5BU5D_t2994307200;
// System.Collections.Generic.IEnumerator`1<ChatNavScreen>
struct IEnumerator_1_t1812868384;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3801305583_gshared (ReadOnlyCollection_1_t228162953 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3801305583(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3801305583_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1252933617_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1252933617(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1252933617_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1953600453_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1953600453(__this, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1953600453_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m513567184_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m513567184(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m513567184_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2157447346_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2157447346(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2157447346_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2287205612_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2287205612(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2287205612_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2346179090_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2346179090(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2346179090_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m702365153_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m702365153(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m702365153_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m906506061_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m906506061(__this, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m906506061_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1817449270_gshared (ReadOnlyCollection_1_t228162953 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1817449270(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1817449270_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3446021921_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3446021921(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3446021921_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3775362110_gshared (ReadOnlyCollection_1_t228162953 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3775362110(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t228162953 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3775362110_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m862183324_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m862183324(__this, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m862183324_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2776882512_gshared (ReadOnlyCollection_1_t228162953 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2776882512(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2776882512_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4282978456_gshared (ReadOnlyCollection_1_t228162953 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4282978456(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t228162953 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4282978456_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3890387193_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3890387193(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3890387193_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3225805097_gshared (ReadOnlyCollection_1_t228162953 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3225805097(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3225805097_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3977410995_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3977410995(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3977410995_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3818311534_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3818311534(__this, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3818311534_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1861809894_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1861809894(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1861809894_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2702117685_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2702117685(__this, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2702117685_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3875569890_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3875569890(__this, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3875569890_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1368708237_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1368708237(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1368708237_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3619231700_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3619231700(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3619231700_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3045357471_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3045357471(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3045357471_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1520621541_gshared (ReadOnlyCollection_1_t228162953 * __this, ChatNavScreenU5BU5D_t2994307200* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1520621541(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t228162953 *, ChatNavScreenU5BU5D_t2994307200*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1520621541_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1327263226_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1327263226(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1327263226_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3538616025_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3538616025(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3538616025_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2595104302_gshared (ReadOnlyCollection_1_t228162953 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2595104302(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t228162953 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2595104302_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m1570905430_gshared (ReadOnlyCollection_1_t228162953 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1570905430(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t228162953 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1570905430_gshared)(__this, ___index0, method)
