﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileArriveTimer/OnTimerUpdate
struct OnTimerUpdate_t2669454040;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void MissileArriveTimer/OnTimerUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTimerUpdate__ctor_m4148146271 (OnTimerUpdate_t2669454040 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer/OnTimerUpdate::Invoke(System.TimeSpan)
extern "C"  void OnTimerUpdate_Invoke_m3259205897 (OnTimerUpdate_t2669454040 * __this, TimeSpan_t3430258949  ___timeDifference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MissileArriveTimer/OnTimerUpdate::BeginInvoke(System.TimeSpan,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnTimerUpdate_BeginInvoke_m2125966078 (OnTimerUpdate_t2669454040 * __this, TimeSpan_t3430258949  ___timeDifference0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer/OnTimerUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void OnTimerUpdate_EndInvoke_m3677864705 (OnTimerUpdate_t2669454040 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
