﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.DefaultBindingPropertyAttribute
struct DefaultBindingPropertyAttribute_t2993619957;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.DefaultBindingPropertyAttribute::.ctor()
extern "C"  void DefaultBindingPropertyAttribute__ctor_m2912282782 (DefaultBindingPropertyAttribute_t2993619957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DefaultBindingPropertyAttribute::.ctor(System.String)
extern "C"  void DefaultBindingPropertyAttribute__ctor_m2712874340 (DefaultBindingPropertyAttribute_t2993619957 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DefaultBindingPropertyAttribute::.cctor()
extern "C"  void DefaultBindingPropertyAttribute__cctor_m3030221949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DefaultBindingPropertyAttribute::Equals(System.Object)
extern "C"  bool DefaultBindingPropertyAttribute_Equals_m2199551027 (DefaultBindingPropertyAttribute_t2993619957 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.DefaultBindingPropertyAttribute::GetHashCode()
extern "C"  int32_t DefaultBindingPropertyAttribute_GetHashCode_m1902198405 (DefaultBindingPropertyAttribute_t2993619957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.DefaultBindingPropertyAttribute::get_Name()
extern "C"  String_t* DefaultBindingPropertyAttribute_get_Name_m58067809 (DefaultBindingPropertyAttribute_t2993619957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
