﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3674688404(__this, ___l0, method) ((  void (*) (Enumerator_t3508079363 *, List_1_t3973349689 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3627378630(__this, method) ((  void (*) (Enumerator_t3508079363 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3445846096(__this, method) ((  Il2CppObject * (*) (Enumerator_t3508079363 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Dispose()
#define Enumerator_Dispose_m1736722093(__this, method) ((  void (*) (Enumerator_t3508079363 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m4288461210(__this, method) ((  void (*) (Enumerator_t3508079363 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m4079870722(__this, method) ((  bool (*) (Enumerator_t3508079363 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Current()
#define Enumerator_get_Current_m785186509(__this, method) ((  Dictionary_2_t309261261 * (*) (Enumerator_t3508079363 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
