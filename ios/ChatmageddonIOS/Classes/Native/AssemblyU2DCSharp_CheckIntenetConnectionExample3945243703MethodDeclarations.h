﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CheckIntenetConnectionExample
struct CheckIntenetConnectionExample_t3945243703;

#include "codegen/il2cpp-codegen.h"

// System.Void CheckIntenetConnectionExample::.ctor()
extern "C"  void CheckIntenetConnectionExample__ctor_m645300870 (CheckIntenetConnectionExample_t3945243703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckIntenetConnectionExample::Start()
extern "C"  void CheckIntenetConnectionExample_Start_m4078488402 (CheckIntenetConnectionExample_t3945243703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckIntenetConnectionExample::OnCheckConnectionComplete(System.Boolean)
extern "C"  void CheckIntenetConnectionExample_OnCheckConnectionComplete_m2974555049 (CheckIntenetConnectionExample_t3945243703 * __this, bool ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
