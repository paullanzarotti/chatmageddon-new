﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.Facebook/<postScore>c__AnonStorey8
struct U3CpostScoreU3Ec__AnonStorey8_t4068392667;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Prime31.Facebook/<postScore>c__AnonStorey8::.ctor()
extern "C"  void U3CpostScoreU3Ec__AnonStorey8__ctor_m3468874170 (U3CpostScoreU3Ec__AnonStorey8_t4068392667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook/<postScore>c__AnonStorey8::<>m__0(System.String,System.Object)
extern "C"  void U3CpostScoreU3Ec__AnonStorey8_U3CU3Em__0_m1122830139 (U3CpostScoreU3Ec__AnonStorey8_t4068392667 * __this, String_t* ___error0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
