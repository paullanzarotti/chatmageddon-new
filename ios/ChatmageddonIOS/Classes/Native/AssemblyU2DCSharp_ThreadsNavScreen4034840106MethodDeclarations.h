﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThreadsNavScreen
struct ThreadsNavScreen_t4034840106;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void ThreadsNavScreen::.ctor()
extern "C"  void ThreadsNavScreen__ctor_m56654271 (ThreadsNavScreen_t4034840106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThreadsNavScreen::Start()
extern "C"  void ThreadsNavScreen_Start_m1867147595 (ThreadsNavScreen_t4034840106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThreadsNavScreen::UIClosing()
extern "C"  void ThreadsNavScreen_UIClosing_m2394161124 (ThreadsNavScreen_t4034840106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThreadsNavScreen::ChatUIClosing()
extern "C"  void ThreadsNavScreen_ChatUIClosing_m503666546 (ThreadsNavScreen_t4034840106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThreadsNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void ThreadsNavScreen_ScreenLoad_m505282721 (ThreadsNavScreen_t4034840106 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThreadsNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void ThreadsNavScreen_ScreenUnload_m40972277 (ThreadsNavScreen_t4034840106 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThreadsNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool ThreadsNavScreen_ValidateScreenNavigation_m2522857062 (ThreadsNavScreen_t4034840106 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
