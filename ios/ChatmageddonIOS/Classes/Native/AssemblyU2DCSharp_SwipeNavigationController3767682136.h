﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t764358782;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,UnityEngine.Vector3>
struct Dictionary_2_t1279085728;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "AssemblyU2DCSharp_UIScrollView3033954930.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwipeNavigationController
struct  SwipeNavigationController_t3767682136  : public UIScrollView_t3033954930
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> SwipeNavigationController::navItems
	Dictionary_2_t764358782 * ___navItems_39;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,UnityEngine.Vector3> SwipeNavigationController::itemPositions
	Dictionary_2_t1279085728 * ___itemPositions_40;
	// UnityEngine.GameObject SwipeNavigationController::container
	GameObject_t1756533147 * ___container_41;
	// UIPanel SwipeNavigationController::uiPanel
	UIPanel_t1795085332 * ___uiPanel_42;
	// System.Int32 SwipeNavigationController::current
	int32_t ___current_43;
	// PurchaseableCategory SwipeNavigationController::currentCategory
	int32_t ___currentCategory_44;
	// System.Boolean SwipeNavigationController::store
	bool ___store_45;
	// UnityEngine.AnimationCurve SwipeNavigationController::activeCurve
	AnimationCurve_t3306541151 * ___activeCurve_46;
	// UnityEngine.AnimationCurve SwipeNavigationController::inactiveCurve
	AnimationCurve_t3306541151 * ___inactiveCurve_47;

public:
	inline static int32_t get_offset_of_navItems_39() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___navItems_39)); }
	inline Dictionary_2_t764358782 * get_navItems_39() const { return ___navItems_39; }
	inline Dictionary_2_t764358782 ** get_address_of_navItems_39() { return &___navItems_39; }
	inline void set_navItems_39(Dictionary_2_t764358782 * value)
	{
		___navItems_39 = value;
		Il2CppCodeGenWriteBarrier(&___navItems_39, value);
	}

	inline static int32_t get_offset_of_itemPositions_40() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___itemPositions_40)); }
	inline Dictionary_2_t1279085728 * get_itemPositions_40() const { return ___itemPositions_40; }
	inline Dictionary_2_t1279085728 ** get_address_of_itemPositions_40() { return &___itemPositions_40; }
	inline void set_itemPositions_40(Dictionary_2_t1279085728 * value)
	{
		___itemPositions_40 = value;
		Il2CppCodeGenWriteBarrier(&___itemPositions_40, value);
	}

	inline static int32_t get_offset_of_container_41() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___container_41)); }
	inline GameObject_t1756533147 * get_container_41() const { return ___container_41; }
	inline GameObject_t1756533147 ** get_address_of_container_41() { return &___container_41; }
	inline void set_container_41(GameObject_t1756533147 * value)
	{
		___container_41 = value;
		Il2CppCodeGenWriteBarrier(&___container_41, value);
	}

	inline static int32_t get_offset_of_uiPanel_42() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___uiPanel_42)); }
	inline UIPanel_t1795085332 * get_uiPanel_42() const { return ___uiPanel_42; }
	inline UIPanel_t1795085332 ** get_address_of_uiPanel_42() { return &___uiPanel_42; }
	inline void set_uiPanel_42(UIPanel_t1795085332 * value)
	{
		___uiPanel_42 = value;
		Il2CppCodeGenWriteBarrier(&___uiPanel_42, value);
	}

	inline static int32_t get_offset_of_current_43() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___current_43)); }
	inline int32_t get_current_43() const { return ___current_43; }
	inline int32_t* get_address_of_current_43() { return &___current_43; }
	inline void set_current_43(int32_t value)
	{
		___current_43 = value;
	}

	inline static int32_t get_offset_of_currentCategory_44() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___currentCategory_44)); }
	inline int32_t get_currentCategory_44() const { return ___currentCategory_44; }
	inline int32_t* get_address_of_currentCategory_44() { return &___currentCategory_44; }
	inline void set_currentCategory_44(int32_t value)
	{
		___currentCategory_44 = value;
	}

	inline static int32_t get_offset_of_store_45() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___store_45)); }
	inline bool get_store_45() const { return ___store_45; }
	inline bool* get_address_of_store_45() { return &___store_45; }
	inline void set_store_45(bool value)
	{
		___store_45 = value;
	}

	inline static int32_t get_offset_of_activeCurve_46() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___activeCurve_46)); }
	inline AnimationCurve_t3306541151 * get_activeCurve_46() const { return ___activeCurve_46; }
	inline AnimationCurve_t3306541151 ** get_address_of_activeCurve_46() { return &___activeCurve_46; }
	inline void set_activeCurve_46(AnimationCurve_t3306541151 * value)
	{
		___activeCurve_46 = value;
		Il2CppCodeGenWriteBarrier(&___activeCurve_46, value);
	}

	inline static int32_t get_offset_of_inactiveCurve_47() { return static_cast<int32_t>(offsetof(SwipeNavigationController_t3767682136, ___inactiveCurve_47)); }
	inline AnimationCurve_t3306541151 * get_inactiveCurve_47() const { return ___inactiveCurve_47; }
	inline AnimationCurve_t3306541151 ** get_address_of_inactiveCurve_47() { return &___inactiveCurve_47; }
	inline void set_inactiveCurve_47(AnimationCurve_t3306541151 * value)
	{
		___inactiveCurve_47 = value;
		Il2CppCodeGenWriteBarrier(&___inactiveCurve_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
