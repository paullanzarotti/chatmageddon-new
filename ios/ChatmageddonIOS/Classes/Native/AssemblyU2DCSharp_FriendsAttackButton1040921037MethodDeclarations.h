﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsAttackButton
struct FriendsAttackButton_t1040921037;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsAttackButton::.ctor()
extern "C"  void FriendsAttackButton__ctor_m2018164144 (FriendsAttackButton_t1040921037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsAttackButton::OnButtonClick()
extern "C"  void FriendsAttackButton_OnButtonClick_m2791954437 (FriendsAttackButton_t1040921037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
