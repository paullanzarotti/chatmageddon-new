﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary1929215751MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.String>::.ctor(TKey)
#define Node__ctor_m2188154477(__this, ___key0, method) ((  void (*) (Node_t1268986689 *, int32_t, const MethodInfo*))Node__ctor_m1387298521_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.String>::.ctor(TKey,TValue)
#define Node__ctor_m4107665496(__this, ___key0, ___value1, method) ((  void (*) (Node_t1268986689 *, int32_t, String_t*, const MethodInfo*))Node__ctor_m1596926594_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.String>::SwapValue(System.Collections.Generic.RBTree/Node)
#define Node_SwapValue_m3667214188(__this, ___other0, method) ((  void (*) (Node_t1268986689 *, Node_t2499136326 *, const MethodInfo*))Node_SwapValue_m1870921446_gshared)(__this, ___other0, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.String>::AsKV()
#define Node_AsKV_m534591987(__this, method) ((  KeyValuePair_2_t3089358386  (*) (Node_t1268986689 *, const MethodInfo*))Node_AsKV_m2459827919_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.String>::AsDE()
#define Node_AsDE_m3206757262(__this, method) ((  DictionaryEntry_t3048875398  (*) (Node_t1268986689 *, const MethodInfo*))Node_AsDE_m4001353572_gshared)(__this, method)
