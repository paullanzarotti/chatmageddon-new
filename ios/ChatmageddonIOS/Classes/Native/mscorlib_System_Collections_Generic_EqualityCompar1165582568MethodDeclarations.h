﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<DefendNavScreen>
struct DefaultComparer_t1165582568;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<DefendNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2876326533_gshared (DefaultComparer_t1165582568 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2876326533(__this, method) ((  void (*) (DefaultComparer_t1165582568 *, const MethodInfo*))DefaultComparer__ctor_m2876326533_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<DefendNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2442730312_gshared (DefaultComparer_t1165582568 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2442730312(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1165582568 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2442730312_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<DefendNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m519903752_gshared (DefaultComparer_t1165582568 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m519903752(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1165582568 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m519903752_gshared)(__this, ___x0, ___y1, method)
