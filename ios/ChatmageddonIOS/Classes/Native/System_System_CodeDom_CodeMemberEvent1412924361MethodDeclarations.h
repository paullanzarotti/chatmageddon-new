﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeMemberEvent
struct CodeMemberEvent_t1412924361;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeMemberEvent::.ctor()
extern "C"  void CodeMemberEvent__ctor_m156260953 (CodeMemberEvent_t1412924361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
