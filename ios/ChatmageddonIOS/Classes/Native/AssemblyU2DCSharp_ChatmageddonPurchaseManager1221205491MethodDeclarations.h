﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonPurchaseManager
struct ChatmageddonPurchaseManager_t1221205491;
// Bucks
struct Bucks_t3932015720;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Bucks3932015720.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatmageddonPurchaseManager::.ctor()
extern "C"  void ChatmageddonPurchaseManager__ctor_m1894564612 (ChatmageddonPurchaseManager_t1221205491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPurchaseManager::PurchaseBucks(Bucks)
extern "C"  void ChatmageddonPurchaseManager_PurchaseBucks_m3446052541 (ChatmageddonPurchaseManager_t1221205491 * __this, Bucks_t3932015720 * ___bucksItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPurchaseManager::FinaliseBucksPurchase(System.Int32,System.String)
extern "C"  void ChatmageddonPurchaseManager_FinaliseBucksPurchase_m3415454325 (ChatmageddonPurchaseManager_t1221205491 * __this, int32_t ___index0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPurchaseManager::ProcessItem(System.String,System.String)
extern "C"  void ChatmageddonPurchaseManager_ProcessItem_m3094612016 (ChatmageddonPurchaseManager_t1221205491 * __this, String_t* ___id0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
