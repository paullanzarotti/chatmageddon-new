﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1
struct U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::.ctor()
extern "C"  void U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1__ctor_m3292045682 (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::MoveNext()
extern "C"  bool U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_MoveNext_m1832512138 (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m796521366 (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m678795934 (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::Dispose()
extern "C"  void U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_Dispose_m4048486159 (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::Reset()
extern "C"  void U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_Reset_m1850805609 (U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
