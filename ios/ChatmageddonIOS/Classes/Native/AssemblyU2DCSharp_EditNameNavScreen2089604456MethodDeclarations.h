﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditNameNavScreen
struct EditNameNavScreen_t2089604456;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void EditNameNavScreen::.ctor()
extern "C"  void EditNameNavScreen__ctor_m3009295117 (EditNameNavScreen_t2089604456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditNameNavScreen::Start()
extern "C"  void EditNameNavScreen_Start_m1471997357 (EditNameNavScreen_t2089604456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditNameNavScreen::UIClosing()
extern "C"  void EditNameNavScreen_UIClosing_m1152994338 (EditNameNavScreen_t2089604456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditNameNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void EditNameNavScreen_ScreenLoad_m3666635187 (EditNameNavScreen_t2089604456 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditNameNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void EditNameNavScreen_ScreenUnload_m855971451 (EditNameNavScreen_t2089604456 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EditNameNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool EditNameNavScreen_ValidateScreenNavigation_m2886632864 (EditNameNavScreen_t2089604456 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EditNameNavScreen::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * EditNameNavScreen_SaveScreenContent_m653006649 (EditNameNavScreen_t2089604456 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
