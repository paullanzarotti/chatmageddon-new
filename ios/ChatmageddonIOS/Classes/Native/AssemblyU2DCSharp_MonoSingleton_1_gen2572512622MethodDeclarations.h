﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<MineManager>::.ctor()
#define MonoSingleton_1__ctor_m394630608(__this, method) ((  void (*) (MonoSingleton_1_t2572512622 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<MineManager>::Awake()
#define MonoSingleton_1_Awake_m1026217285(__this, method) ((  void (*) (MonoSingleton_1_t2572512622 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<MineManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1574472535(__this /* static, unused */, method) ((  MineManager_t2821846902 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<MineManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2885404279(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<MineManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2844434262(__this, method) ((  void (*) (MonoSingleton_1_t2572512622 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<MineManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2536772682(__this, method) ((  void (*) (MonoSingleton_1_t2572512622 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<MineManager>::.cctor()
#define MonoSingleton_1__cctor_m159101567(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
