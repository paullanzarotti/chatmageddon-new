﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_MineStatus2219122381.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mine
struct  Mine_t2729441277  : public Item_t2440468191
{
public:
	// MineStatus Mine::status
	int32_t ___status_4;
	// System.Int32 Mine::damagePoints
	int32_t ___damagePoints_5;
	// System.Int32 Mine::ownerLevelPointsMultiplier
	int32_t ___ownerLevelPointsMultiplier_6;
	// System.Int32 Mine::triggerRadius
	int32_t ___triggerRadius_7;
	// System.Single Mine::latitude
	float ___latitude_8;
	// System.Single Mine::longitude
	float ___longitude_9;
	// System.Double Mine::distance
	double ___distance_10;
	// System.DateTime Mine::triggeredTime
	DateTime_t693205669  ___triggeredTime_11;
	// System.String Mine::triggeredById
	String_t* ___triggeredById_12;

public:
	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_damagePoints_5() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___damagePoints_5)); }
	inline int32_t get_damagePoints_5() const { return ___damagePoints_5; }
	inline int32_t* get_address_of_damagePoints_5() { return &___damagePoints_5; }
	inline void set_damagePoints_5(int32_t value)
	{
		___damagePoints_5 = value;
	}

	inline static int32_t get_offset_of_ownerLevelPointsMultiplier_6() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___ownerLevelPointsMultiplier_6)); }
	inline int32_t get_ownerLevelPointsMultiplier_6() const { return ___ownerLevelPointsMultiplier_6; }
	inline int32_t* get_address_of_ownerLevelPointsMultiplier_6() { return &___ownerLevelPointsMultiplier_6; }
	inline void set_ownerLevelPointsMultiplier_6(int32_t value)
	{
		___ownerLevelPointsMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_triggerRadius_7() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___triggerRadius_7)); }
	inline int32_t get_triggerRadius_7() const { return ___triggerRadius_7; }
	inline int32_t* get_address_of_triggerRadius_7() { return &___triggerRadius_7; }
	inline void set_triggerRadius_7(int32_t value)
	{
		___triggerRadius_7 = value;
	}

	inline static int32_t get_offset_of_latitude_8() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___latitude_8)); }
	inline float get_latitude_8() const { return ___latitude_8; }
	inline float* get_address_of_latitude_8() { return &___latitude_8; }
	inline void set_latitude_8(float value)
	{
		___latitude_8 = value;
	}

	inline static int32_t get_offset_of_longitude_9() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___longitude_9)); }
	inline float get_longitude_9() const { return ___longitude_9; }
	inline float* get_address_of_longitude_9() { return &___longitude_9; }
	inline void set_longitude_9(float value)
	{
		___longitude_9 = value;
	}

	inline static int32_t get_offset_of_distance_10() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___distance_10)); }
	inline double get_distance_10() const { return ___distance_10; }
	inline double* get_address_of_distance_10() { return &___distance_10; }
	inline void set_distance_10(double value)
	{
		___distance_10 = value;
	}

	inline static int32_t get_offset_of_triggeredTime_11() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___triggeredTime_11)); }
	inline DateTime_t693205669  get_triggeredTime_11() const { return ___triggeredTime_11; }
	inline DateTime_t693205669 * get_address_of_triggeredTime_11() { return &___triggeredTime_11; }
	inline void set_triggeredTime_11(DateTime_t693205669  value)
	{
		___triggeredTime_11 = value;
	}

	inline static int32_t get_offset_of_triggeredById_12() { return static_cast<int32_t>(offsetof(Mine_t2729441277, ___triggeredById_12)); }
	inline String_t* get_triggeredById_12() const { return ___triggeredById_12; }
	inline String_t** get_address_of_triggeredById_12() { return &___triggeredById_12; }
	inline void set_triggeredById_12(String_t* value)
	{
		___triggeredById_12 = value;
		Il2CppCodeGenWriteBarrier(&___triggeredById_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
