﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileTrajectoryDisplay
struct MissileTrajectoryDisplay_t1441887151;
// LaunchedMissile
struct LaunchedMissile_t1542515810;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "AssemblyU2DCSharp_LaunchDirection3410220992.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_ResultSprite623266392.h"

// System.Void MissileTrajectoryDisplay::.ctor()
extern "C"  void MissileTrajectoryDisplay__ctor_m3449972952 (MissileTrajectoryDisplay_t1441887151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::SetupTrajectory(LaunchedMissile)
extern "C"  void MissileTrajectoryDisplay_SetupTrajectory_m228865944 (MissileTrajectoryDisplay_t1441887151 * __this, LaunchedMissile_t1542515810 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::ResetTrajectory()
extern "C"  void MissileTrajectoryDisplay_ResetTrajectory_m3648853222 (MissileTrajectoryDisplay_t1441887151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::SetDirection()
extern "C"  void MissileTrajectoryDisplay_SetDirection_m1490856925 (MissileTrajectoryDisplay_t1441887151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::SetTravelStartPosition(LaunchDirection)
extern "C"  void MissileTrajectoryDisplay_SetTravelStartPosition_m4209788097 (MissileTrajectoryDisplay_t1441887151 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::JumpToPoint(System.Boolean)
extern "C"  void MissileTrajectoryDisplay_JumpToPoint_m42700952 (MissileTrajectoryDisplay_t1441887151 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::SetContentColour(UnityEngine.Color)
extern "C"  void MissileTrajectoryDisplay_SetContentColour_m1573594495 (MissileTrajectoryDisplay_t1441887151 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::SetResultSpriteActive(System.Boolean,ResultSprite)
extern "C"  void MissileTrajectoryDisplay_SetResultSpriteActive_m3755482227 (MissileTrajectoryDisplay_t1441887151 * __this, bool ___active0, int32_t ___spriteType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileTrajectoryDisplay::SetStealthResultAnchor()
extern "C"  void MissileTrajectoryDisplay_SetStealthResultAnchor_m3643987739 (MissileTrajectoryDisplay_t1441887151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
