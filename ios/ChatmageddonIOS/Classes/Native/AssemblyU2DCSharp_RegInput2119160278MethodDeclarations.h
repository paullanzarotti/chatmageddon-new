﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegInput
struct RegInput_t2119160278;

#include "codegen/il2cpp-codegen.h"

// System.Void RegInput::.ctor()
extern "C"  void RegInput__ctor_m4286615293 (RegInput_t2119160278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegInput::OnButtonClick()
extern "C"  void RegInput_OnButtonClick_m4049359998 (RegInput_t2119160278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegInput::OnSelect(System.Boolean)
extern "C"  void RegInput_OnSelect_m954719167 (RegInput_t2119160278 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegInput::InputSubmit()
extern "C"  void RegInput_InputSubmit_m1198867263 (RegInput_t2119160278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegInput::InputChange()
extern "C"  void RegInput_InputChange_m3011354837 (RegInput_t2119160278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegInput::SetInputActive(System.Boolean)
extern "C"  void RegInput_SetInputActive_m383022450 (RegInput_t2119160278 * __this, bool ___InputActive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
