﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerManager
struct PlayerManager_t1596653588;
// Friend
struct Friend_t3555014108;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"

// System.Void PlayerManager::.ctor()
extern "C"  void PlayerManager__ctor_m2438461151 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::Init()
extern "C"  void PlayerManager_Init_m3661303753 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::GetExistingFriend(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_GetExistingFriend_m4019571299 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::GetExistingFriendAndIncPending(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_GetExistingFriendAndIncPending_m3648964003 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::GetExistingPlayer(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_GetExistingPlayer_m4237454126 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::GetExistingPendingFriend(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_GetExistingPendingFriend_m1995413980 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::CheckIncomingPendingList(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_CheckIncomingPendingList_m3291663153 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::CheckOutgoingPendingList(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_CheckOutgoingPendingList_m2926296793 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::CheckInnerFriendsList(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_CheckInnerFriendsList_m3041799543 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend PlayerManager::CheckOuterFriendsList(System.String)
extern "C"  Friend_t3555014108 * PlayerManager_CheckOuterFriendsList_m2536400142 (PlayerManager_t1596653588 * __this, String_t* ___ID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::UpdateContacts(System.Int32,System.Int64)
extern "C"  void PlayerManager_UpdateContacts_m3684138460 (PlayerManager_t1596653588 * __this, int32_t ___startIndex0, int64_t ___amountOfContacts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadSettings()
extern "C"  void PlayerManager_LoadSettings_m502984938 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::SaveIGNotificationSetting(System.Boolean)
extern "C"  void PlayerManager_SaveIGNotificationSetting_m1713733508 (PlayerManager_t1596653588 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadIGNotificationSetting()
extern "C"  void PlayerManager_LoadIGNotificationSetting_m688571922 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::SaveIGNSoundSetting(System.Boolean)
extern "C"  void PlayerManager_SaveIGNSoundSetting_m233516606 (PlayerManager_t1596653588 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadIGNSoundSetting()
extern "C"  void PlayerManager_LoadIGNSoundSetting_m2731366800 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::SaveIGNVibrationSetting(System.Boolean)
extern "C"  void PlayerManager_SaveIGNVibrationSetting_m941393993 (PlayerManager_t1596653588 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadIGNVibrationSetting()
extern "C"  void PlayerManager_LoadIGNVibrationSetting_m3527347013 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::SaveDisplayLocationSetting(System.Boolean)
extern "C"  void PlayerManager_SaveDisplayLocationSetting_m2886836556 (PlayerManager_t1596653588 * __this, bool ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadDisplayLocationSetting()
extern "C"  void PlayerManager_LoadDisplayLocationSetting_m433838192 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::SaveUserAudienceSetting(Audience)
extern "C"  void PlayerManager_SaveUserAudienceSetting_m1475258929 (PlayerManager_t1596653588 * __this, int32_t ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadUserAudienceSetting()
extern "C"  void PlayerManager_LoadUserAudienceSetting_m2272439628 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::SaveUserGenderSetting(Gender)
extern "C"  void PlayerManager_SaveUserGenderSetting_m1974994385 (PlayerManager_t1596653588 * __this, int32_t ___setting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::LoadUserGenderSetting()
extern "C"  void PlayerManager_LoadUserGenderSetting_m317100969 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::ClearPlayerData()
extern "C"  void PlayerManager_ClearPlayerData_m3005915645 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
