﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AimMarker
struct AimMarker_t327093161;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "codegen/il2cpp-codegen.h"

// System.Void AimMarker::.ctor()
extern "C"  void AimMarker__ctor_m2710597026 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::Start()
extern "C"  void AimMarker_Start_m1758176282 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::SetLevelSpeed()
extern "C"  void AimMarker_SetLevelSpeed_m3566439951 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::StartMarkerMovement()
extern "C"  void AimMarker_StartMarkerMovement_m524304289 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::PauseMarkerMovement()
extern "C"  void AimMarker_PauseMarkerMovement_m2310772247 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AimMarker::GetMarkerPercent()
extern "C"  float AimMarker_GetMarkerPercent_m1798832661 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::SetDataUI()
extern "C"  void AimMarker_SetDataUI_m1254001672 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AimMarker::UpdatePositionUIRoutine()
extern "C"  Il2CppObject * AimMarker_UpdatePositionUIRoutine_m698794726 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::UpdatePositionUI()
extern "C"  void AimMarker_UpdatePositionUI_m1658129164 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::FigureOfEight()
extern "C"  void AimMarker_FigureOfEight_m4281970502 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> AimMarker::GenerateSpirograph()
extern "C"  List_1_t1612828712 * AimMarker_GenerateSpirograph_m4099727658 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::PlaySpirograph()
extern "C"  void AimMarker_PlaySpirograph_m690478675 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::StopSpirograph()
extern "C"  void AimMarker_StopSpirograph_m461811387 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AimMarker::Spirograph()
extern "C"  Il2CppObject * AimMarker_Spirograph_m2349013701 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::RandomiseSpirograph()
extern "C"  void AimMarker_RandomiseSpirograph_m833578683 (AimMarker_t327093161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::SmoothUpdate(System.Single)
extern "C"  void AimMarker_SmoothUpdate_m474369220 (AimMarker_t327093161 * __this, float ___currentPercent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker::SmoothFinished(System.Single)
extern "C"  void AimMarker_SmoothFinished_m455209957 (AimMarker_t327093161 * __this, float ___finishedPercent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AimMarker::GetRandomNumber(System.Double,System.Double)
extern "C"  double AimMarker_GetRandomNumber_m2338137247 (AimMarker_t327093161 * __this, double ___minimum0, double ___maximum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AimMarker::ParametricX(System.Double,System.Double,System.Double,System.Double)
extern "C"  double AimMarker_ParametricX_m3564902271 (AimMarker_t327093161 * __this, double ___t0, double ___A1, double ___B2, double ___C3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AimMarker::ParametricY(System.Double,System.Double,System.Double,System.Double)
extern "C"  double AimMarker_ParametricY_m3234302436 (AimMarker_t327093161 * __this, double ___t0, double ___A1, double ___B2, double ___C3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double AimMarker::GCD(System.Double,System.Double)
extern "C"  double AimMarker_GCD_m847390233 (AimMarker_t327093161 * __this, double ___a0, double ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
