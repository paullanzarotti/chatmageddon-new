﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<PlayerProfileNavScreen>
struct Predicate_1_t1103638374;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Predicate`1<PlayerProfileNavScreen>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1629582666_gshared (Predicate_1_t1103638374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m1629582666(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1103638374 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m1629582666_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<PlayerProfileNavScreen>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1313941130_gshared (Predicate_1_t1103638374 * __this, int32_t ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m1313941130(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1103638374 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m1313941130_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<PlayerProfileNavScreen>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1793081995_gshared (Predicate_1_t1103638374 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m1793081995(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1103638374 *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m1793081995_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<PlayerProfileNavScreen>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1395197664_gshared (Predicate_1_t1103638374 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m1395197664(__this, ___result0, method) ((  bool (*) (Predicate_1_t1103638374 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m1395197664_gshared)(__this, ___result0, method)
