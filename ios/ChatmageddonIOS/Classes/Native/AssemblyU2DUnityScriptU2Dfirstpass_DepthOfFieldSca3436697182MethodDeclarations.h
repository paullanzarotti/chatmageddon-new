﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DepthOfFieldScatter
struct DepthOfFieldScatter_t3436697182;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void DepthOfFieldScatter::.ctor()
extern "C"  void DepthOfFieldScatter__ctor_m2883809816 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DepthOfFieldScatter::CheckResources()
extern "C"  bool DepthOfFieldScatter_CheckResources_m804066479 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::OnEnable()
extern "C"  void DepthOfFieldScatter_OnEnable_m1134277680 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::OnDisable()
extern "C"  void DepthOfFieldScatter_OnDisable_m1448502813 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::ReleaseComputeResources()
extern "C"  void DepthOfFieldScatter_ReleaseComputeResources_m531260305 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::CreateComputeResources()
extern "C"  void DepthOfFieldScatter_CreateComputeResources_m2785239116 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DepthOfFieldScatter::FocalDistance01(System.Single)
extern "C"  float DepthOfFieldScatter_FocalDistance01_m681212524 (DepthOfFieldScatter_t3436697182 * __this, float ___worldDist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void DepthOfFieldScatter_WriteCoc_m4096832655 (DepthOfFieldScatter_t3436697182 * __this, RenderTexture_t2666733923 * ___fromTo0, bool ___fgDilate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void DepthOfFieldScatter_OnRenderImage_m293600224 (DepthOfFieldScatter_t3436697182 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::Main()
extern "C"  void DepthOfFieldScatter_Main_m2700390855 (DepthOfFieldScatter_t3436697182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
