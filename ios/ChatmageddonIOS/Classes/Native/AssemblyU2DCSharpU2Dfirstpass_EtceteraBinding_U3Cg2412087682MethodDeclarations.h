﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraBinding/<getScreenShotTexture>c__Iterator1
struct U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraBinding/<getScreenShotTexture>c__Iterator1::.ctor()
extern "C"  void U3CgetScreenShotTextureU3Ec__Iterator1__ctor_m3791565193 (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding/<getScreenShotTexture>c__Iterator1::MoveNext()
extern "C"  bool U3CgetScreenShotTextureU3Ec__Iterator1_MoveNext_m4127886391 (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<getScreenShotTexture>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CgetScreenShotTextureU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2708237447 (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<getScreenShotTexture>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CgetScreenShotTextureU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1005807823 (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<getScreenShotTexture>c__Iterator1::Dispose()
extern "C"  void U3CgetScreenShotTextureU3Ec__Iterator1_Dispose_m402320542 (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<getScreenShotTexture>c__Iterator1::Reset()
extern "C"  void U3CgetScreenShotTextureU3Ec__Iterator1_Reset_m1830314728 (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
