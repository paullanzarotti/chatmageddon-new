﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultBonusController
struct ResultBonusController_t3929995048;
// ResultModalUI
struct ResultModalUI_t969511824;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultModalUI969511824.h"

// System.Void ResultBonusController::.ctor()
extern "C"  void ResultBonusController__ctor_m194068951 (ResultBonusController_t3929995048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultBonusController::Init(ResultModalUI)
extern "C"  void ResultBonusController_Init_m1101630339 (ResultBonusController_t3929995048 * __this, ResultModalUI_t969511824 * ___modalUI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultBonusController::SetBonuses(System.Int32,System.Boolean)
extern "C"  void ResultBonusController_SetBonuses_m313871708 (ResultBonusController_t3929995048 * __this, int32_t ___firstStrikePoints0, bool ___perfectLaunch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultBonusController::SetFirstStrikeActive(System.Int32,System.Single)
extern "C"  void ResultBonusController_SetFirstStrikeActive_m3253218269 (ResultBonusController_t3929995048 * __this, int32_t ___points0, float ___xPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultBonusController::SetPerfectLaunchActive(System.Boolean,System.Single)
extern "C"  void ResultBonusController_SetPerfectLaunchActive_m2081972933 (ResultBonusController_t3929995048 * __this, bool ___active0, float ___xPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
