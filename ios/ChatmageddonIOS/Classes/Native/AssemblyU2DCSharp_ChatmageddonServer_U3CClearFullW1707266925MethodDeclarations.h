﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<ClearFullWarefareStatus>c__AnonStorey28
struct U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<ClearFullWarefareStatus>c__AnonStorey28::.ctor()
extern "C"  void U3CClearFullWarefareStatusU3Ec__AnonStorey28__ctor_m1416628892 (U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<ClearFullWarefareStatus>c__AnonStorey28::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CClearFullWarefareStatusU3Ec__AnonStorey28_U3CU3Em__0_m4265716213 (U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
