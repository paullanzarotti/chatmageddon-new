﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Light_Rainbow2
struct CameraFilterPack_Light_Rainbow2_t1290162980;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Light_Rainbow2::.ctor()
extern "C"  void CameraFilterPack_Light_Rainbow2__ctor_m998597337 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Rainbow2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Light_Rainbow2_get_material_m427721196 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::Start()
extern "C"  void CameraFilterPack_Light_Rainbow2_Start_m610824617 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Light_Rainbow2_OnRenderImage_m3170868657 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::OnValidate()
extern "C"  void CameraFilterPack_Light_Rainbow2_OnValidate_m1834500124 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::Update()
extern "C"  void CameraFilterPack_Light_Rainbow2_Update_m1948330382 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::OnDisable()
extern "C"  void CameraFilterPack_Light_Rainbow2_OnDisable_m2979735264 (CameraFilterPack_Light_Rainbow2_t1290162980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
