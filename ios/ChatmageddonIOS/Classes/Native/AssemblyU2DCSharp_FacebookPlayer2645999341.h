﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<FacebookFriend>
struct List_1_t127391664;

#include "AssemblyU2DCSharp_FacebookUser244724091.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookPlayer
struct  FacebookPlayer_t2645999341  : public FacebookUser_t244724091
{
public:
	// System.String FacebookPlayer::accessToken
	String_t* ___accessToken_5;
	// System.Collections.Generic.List`1<FacebookFriend> FacebookPlayer::friendsList
	List_1_t127391664 * ___friendsList_6;

public:
	inline static int32_t get_offset_of_accessToken_5() { return static_cast<int32_t>(offsetof(FacebookPlayer_t2645999341, ___accessToken_5)); }
	inline String_t* get_accessToken_5() const { return ___accessToken_5; }
	inline String_t** get_address_of_accessToken_5() { return &___accessToken_5; }
	inline void set_accessToken_5(String_t* value)
	{
		___accessToken_5 = value;
		Il2CppCodeGenWriteBarrier(&___accessToken_5, value);
	}

	inline static int32_t get_offset_of_friendsList_6() { return static_cast<int32_t>(offsetof(FacebookPlayer_t2645999341, ___friendsList_6)); }
	inline List_1_t127391664 * get_friendsList_6() const { return ___friendsList_6; }
	inline List_1_t127391664 ** get_address_of_friendsList_6() { return &___friendsList_6; }
	inline void set_friendsList_6(List_1_t127391664 * value)
	{
		___friendsList_6 = value;
		Il2CppCodeGenWriteBarrier(&___friendsList_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
