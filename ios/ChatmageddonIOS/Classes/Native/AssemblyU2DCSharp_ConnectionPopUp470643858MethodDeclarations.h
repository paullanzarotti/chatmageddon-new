﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectionPopUp
struct ConnectionPopUp_t470643858;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ConnectionPopUp::.ctor()
extern "C"  void ConnectionPopUp__ctor_m3190502175 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::Awake()
extern "C"  void ConnectionPopUp_Awake_m804058366 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::BackgroundTweenFinished()
extern "C"  void ConnectionPopUp_BackgroundTweenFinished_m342407708 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::SetUIActive(System.String)
extern "C"  void ConnectionPopUp_SetUIActive_m4000179827 (ConnectionPopUp_t470643858 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::SetUIInactive()
extern "C"  void ConnectionPopUp_SetUIInactive_m4058719626 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::RetryConnection()
extern "C"  void ConnectionPopUp_RetryConnection_m1799572829 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::OnMessageLabelScaleComplete()
extern "C"  void ConnectionPopUp_OnMessageLabelScaleComplete_m2304238690 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::OnReconnectLabelScaleComplete()
extern "C"  void ConnectionPopUp_OnReconnectLabelScaleComplete_m2896389132 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::AttemptReconnect()
extern "C"  void ConnectionPopUp_AttemptReconnect_m3898795895 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ConnectionPopUp::CheckHideAvailable()
extern "C"  Il2CppObject * ConnectionPopUp_CheckHideAvailable_m1493632562 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ConnectionPopUp::WaitToHideConnectingUI()
extern "C"  Il2CppObject * ConnectionPopUp_WaitToHideConnectingUI_m2736522211 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::StopWaitRoutine()
extern "C"  void ConnectionPopUp_StopWaitRoutine_m3337179058 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::ActivateConnectingUI(System.Boolean)
extern "C"  void ConnectionPopUp_ActivateConnectingUI_m3657983563 (ConnectionPopUp_t470643858 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::ResetPopUpUI()
extern "C"  void ConnectionPopUp_ResetPopUpUI_m1529672044 (ConnectionPopUp_t470643858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp::<AttemptReconnect>m__0(System.Boolean,System.String)
extern "C"  void ConnectionPopUp_U3CAttemptReconnectU3Em__0_m447199993 (ConnectionPopUp_t470643858 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
