﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4113099591MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.GameObject,UnityEngine.Vector3,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1108302968(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2097368499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3886908164_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.GameObject,UnityEngine.Vector3,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2108074024(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2097368499 *, GameObject_t1756533147 *, Vector3_t2243707580 , const MethodInfo*))Transform_1_Invoke_m544618940_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.GameObject,UnityEngine.Vector3,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m670029599(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2097368499 *, GameObject_t1756533147 *, Vector3_t2243707580 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m629477401_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.GameObject,UnityEngine.Vector3,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3547118662(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2097368499 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2446839750_gshared)(__this, ___result0, method)
