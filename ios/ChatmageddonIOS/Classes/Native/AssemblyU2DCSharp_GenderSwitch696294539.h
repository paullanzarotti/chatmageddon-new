﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_EnumSwitch_1_gen2620893752.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenderSwitch
struct  GenderSwitch_t696294539  : public EnumSwitch_1_t2620893752
{
public:
	// UnityEngine.GameObject GenderSwitch::selectionObject
	GameObject_t1756533147 * ___selectionObject_3;
	// UnityEngine.Vector3 GenderSwitch::selectionObjectPosition
	Vector3_t2243707580  ___selectionObjectPosition_4;
	// UnityEngine.GameObject GenderSwitch::maleObject
	GameObject_t1756533147 * ___maleObject_5;
	// UnityEngine.GameObject GenderSwitch::femaleObject
	GameObject_t1756533147 * ___femaleObject_6;

public:
	inline static int32_t get_offset_of_selectionObject_3() { return static_cast<int32_t>(offsetof(GenderSwitch_t696294539, ___selectionObject_3)); }
	inline GameObject_t1756533147 * get_selectionObject_3() const { return ___selectionObject_3; }
	inline GameObject_t1756533147 ** get_address_of_selectionObject_3() { return &___selectionObject_3; }
	inline void set_selectionObject_3(GameObject_t1756533147 * value)
	{
		___selectionObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectionObject_3, value);
	}

	inline static int32_t get_offset_of_selectionObjectPosition_4() { return static_cast<int32_t>(offsetof(GenderSwitch_t696294539, ___selectionObjectPosition_4)); }
	inline Vector3_t2243707580  get_selectionObjectPosition_4() const { return ___selectionObjectPosition_4; }
	inline Vector3_t2243707580 * get_address_of_selectionObjectPosition_4() { return &___selectionObjectPosition_4; }
	inline void set_selectionObjectPosition_4(Vector3_t2243707580  value)
	{
		___selectionObjectPosition_4 = value;
	}

	inline static int32_t get_offset_of_maleObject_5() { return static_cast<int32_t>(offsetof(GenderSwitch_t696294539, ___maleObject_5)); }
	inline GameObject_t1756533147 * get_maleObject_5() const { return ___maleObject_5; }
	inline GameObject_t1756533147 ** get_address_of_maleObject_5() { return &___maleObject_5; }
	inline void set_maleObject_5(GameObject_t1756533147 * value)
	{
		___maleObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___maleObject_5, value);
	}

	inline static int32_t get_offset_of_femaleObject_6() { return static_cast<int32_t>(offsetof(GenderSwitch_t696294539, ___femaleObject_6)); }
	inline GameObject_t1756533147 * get_femaleObject_6() const { return ___femaleObject_6; }
	inline GameObject_t1756533147 ** get_address_of_femaleObject_6() { return &___femaleObject_6; }
	inline void set_femaleObject_6(GameObject_t1756533147 * value)
	{
		___femaleObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___femaleObject_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
