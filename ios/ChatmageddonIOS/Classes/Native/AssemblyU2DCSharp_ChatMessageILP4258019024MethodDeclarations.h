﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatMessageILP
struct ChatMessageILP_t4258019024;
// ChatMessage
struct ChatMessage_t2384228687;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatMessage2384228687.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void ChatMessageILP::.ctor()
extern "C"  void ChatMessageILP__ctor_m3641453723 (ChatMessageILP_t4258019024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::StartIL()
extern "C"  void ChatMessageILP_StartIL_m636947774 (ChatMessageILP_t4258019024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::AddToPopulation(ChatMessage)
extern "C"  void ChatMessageILP_AddToPopulation_m3537459999 (ChatMessageILP_t4258019024 * __this, ChatMessage_t2384228687 * ___newMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::SetDataArray()
extern "C"  void ChatMessageILP_SetDataArray_m3379126202 (ChatMessageILP_t4258019024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::RepositionList()
extern "C"  void ChatMessageILP_RepositionList_m466118531 (ChatMessageILP_t4258019024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::SetScrollToBottom()
extern "C"  void ChatMessageILP_SetScrollToBottom_m2541336536 (ChatMessageILP_t4258019024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void ChatMessageILP_InitListItemWithIndex_m1386465361 (ChatMessageILP_t4258019024 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void ChatMessageILP_PopulateListItemWithIndex_m2414002992 (ChatMessageILP_t4258019024 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
