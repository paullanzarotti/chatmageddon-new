﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardItemUIScaler
struct  LeaderboardItemUIScaler_t1432927178  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite LeaderboardItemUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider LeaderboardItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UILabel LeaderboardItemUIScaler::rankLabel
	UILabel_t1795115428 * ___rankLabel_16;
	// UILabel LeaderboardItemUIScaler::nameLabel
	UILabel_t1795115428 * ___nameLabel_17;
	// UILabel LeaderboardItemUIScaler::userRankLabel
	UILabel_t1795115428 * ___userRankLabel_18;
	// UILabel LeaderboardItemUIScaler::scoreLabel
	UILabel_t1795115428 * ___scoreLabel_19;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(LeaderboardItemUIScaler_t1432927178, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(LeaderboardItemUIScaler_t1432927178, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_rankLabel_16() { return static_cast<int32_t>(offsetof(LeaderboardItemUIScaler_t1432927178, ___rankLabel_16)); }
	inline UILabel_t1795115428 * get_rankLabel_16() const { return ___rankLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_16() { return &___rankLabel_16; }
	inline void set_rankLabel_16(UILabel_t1795115428 * value)
	{
		___rankLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_16, value);
	}

	inline static int32_t get_offset_of_nameLabel_17() { return static_cast<int32_t>(offsetof(LeaderboardItemUIScaler_t1432927178, ___nameLabel_17)); }
	inline UILabel_t1795115428 * get_nameLabel_17() const { return ___nameLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_17() { return &___nameLabel_17; }
	inline void set_nameLabel_17(UILabel_t1795115428 * value)
	{
		___nameLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_17, value);
	}

	inline static int32_t get_offset_of_userRankLabel_18() { return static_cast<int32_t>(offsetof(LeaderboardItemUIScaler_t1432927178, ___userRankLabel_18)); }
	inline UILabel_t1795115428 * get_userRankLabel_18() const { return ___userRankLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_userRankLabel_18() { return &___userRankLabel_18; }
	inline void set_userRankLabel_18(UILabel_t1795115428 * value)
	{
		___userRankLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___userRankLabel_18, value);
	}

	inline static int32_t get_offset_of_scoreLabel_19() { return static_cast<int32_t>(offsetof(LeaderboardItemUIScaler_t1432927178, ___scoreLabel_19)); }
	inline UILabel_t1795115428 * get_scoreLabel_19() const { return ___scoreLabel_19; }
	inline UILabel_t1795115428 ** get_address_of_scoreLabel_19() { return &___scoreLabel_19; }
	inline void set_scoreLabel_19(UILabel_t1795115428 * value)
	{
		___scoreLabel_19 = value;
		Il2CppCodeGenWriteBarrier(&___scoreLabel_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
