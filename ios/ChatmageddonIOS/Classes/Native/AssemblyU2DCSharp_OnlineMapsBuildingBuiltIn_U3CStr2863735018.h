﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuildingBuiltIn/<StringToColor>c__AnonStorey2
struct  U3CStringToColorU3Ec__AnonStorey2_t2863735018  : public Il2CppObject
{
public:
	// System.String OnlineMapsBuildingBuiltIn/<StringToColor>c__AnonStorey2::hex
	String_t* ___hex_0;

public:
	inline static int32_t get_offset_of_hex_0() { return static_cast<int32_t>(offsetof(U3CStringToColorU3Ec__AnonStorey2_t2863735018, ___hex_0)); }
	inline String_t* get_hex_0() const { return ___hex_0; }
	inline String_t** get_address_of_hex_0() { return &___hex_0; }
	inline void set_hex_0(String_t* value)
	{
		___hex_0 = value;
		Il2CppCodeGenWriteBarrier(&___hex_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
