﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraUIManagerTwo
struct EtceteraUIManagerTwo_t948262750;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraUIManagerTwo::.ctor()
extern "C"  void EtceteraUIManagerTwo__ctor_m2302370131 (EtceteraUIManagerTwo_t948262750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
