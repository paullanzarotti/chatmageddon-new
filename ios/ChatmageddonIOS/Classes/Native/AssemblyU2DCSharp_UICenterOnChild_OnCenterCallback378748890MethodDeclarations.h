﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICenterOnChild/OnCenterCallback
struct OnCenterCallback_t378748890;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICenterOnChild/OnCenterCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCenterCallback__ctor_m1386102931 (OnCenterCallback_t378748890 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChild/OnCenterCallback::Invoke(UnityEngine.GameObject)
extern "C"  void OnCenterCallback_Invoke_m1186976991 (OnCenterCallback_t378748890 * __this, GameObject_t1756533147 * ___centeredObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICenterOnChild/OnCenterCallback::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCenterCallback_BeginInvoke_m2580837292 (OnCenterCallback_t378748890 * __this, GameObject_t1756533147 * ___centeredObject0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChild/OnCenterCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnCenterCallback_EndInvoke_m573681781 (OnCenterCallback_t378748890 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
