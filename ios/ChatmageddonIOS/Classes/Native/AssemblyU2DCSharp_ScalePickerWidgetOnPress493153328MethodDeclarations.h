﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScalePickerWidgetOnPress
struct ScalePickerWidgetOnPress_t493153328;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void ScalePickerWidgetOnPress::.ctor()
extern "C"  void ScalePickerWidgetOnPress__ctor_m1428645807 (ScalePickerWidgetOnPress_t493153328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScalePickerWidgetOnPress::OnPress(System.Boolean)
extern "C"  void ScalePickerWidgetOnPress_OnPress_m4017640278 (ScalePickerWidgetOnPress_t493153328 * __this, bool ___press0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScalePickerWidgetOnPress::AddScaleTween(UnityEngine.GameObject)
extern "C"  void ScalePickerWidgetOnPress_AddScaleTween_m866355995 (ScalePickerWidgetOnPress_t493153328 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
