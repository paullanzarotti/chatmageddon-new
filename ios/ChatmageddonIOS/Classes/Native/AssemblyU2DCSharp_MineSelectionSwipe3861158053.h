﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineModal
struct MineModal_t1141032720;

#include "AssemblyU2DCSharp_ItemSelectionSwipe2104393431.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineSelectionSwipe
struct  MineSelectionSwipe_t3861158053  : public ItemSelectionSwipe_t2104393431
{
public:
	// MineModal MineSelectionSwipe::modal
	MineModal_t1141032720 * ___modal_11;

public:
	inline static int32_t get_offset_of_modal_11() { return static_cast<int32_t>(offsetof(MineSelectionSwipe_t3861158053, ___modal_11)); }
	inline MineModal_t1141032720 * get_modal_11() const { return ___modal_11; }
	inline MineModal_t1141032720 ** get_address_of_modal_11() { return &___modal_11; }
	inline void set_modal_11(MineModal_t1141032720 * value)
	{
		___modal_11 = value;
		Il2CppCodeGenWriteBarrier(&___modal_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
