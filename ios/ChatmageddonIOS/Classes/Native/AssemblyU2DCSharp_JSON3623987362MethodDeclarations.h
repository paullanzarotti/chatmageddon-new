﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSON
struct JSON_t3623987362;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void JSON::.ctor()
extern "C"  void JSON__ctor_m1797046335 (JSON_t3623987362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSON::JsonDecode(System.String)
extern "C"  Il2CppObject * JSON_JsonDecode_m1043082852 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSON::JsonDecode(System.String,System.Boolean&)
extern "C"  Il2CppObject * JSON_JsonDecode_m927884185 (Il2CppObject * __this /* static, unused */, String_t* ___json0, bool* ___success1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSON::JsonEncode(System.Object)
extern "C"  String_t* JSON_JsonEncode_m2316516888 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable JSON::ParseObject(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Hashtable_t909839986 * JSON_ParseObject_m4171384427 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList JSON::ParseArray(System.Char[],System.Int32&,System.Boolean&)
extern "C"  ArrayList_t4252133567 * JSON_ParseArray_m2977200866 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSON::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * JSON_ParseValue_m104944531 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSON::ParseString(System.Char[],System.Int32&,System.Boolean&)
extern "C"  String_t* JSON_ParseString_m3599092729 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double JSON::ParseNumber(System.Char[],System.Int32&)
extern "C"  double JSON_ParseNumber_m2502009904 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSON::GetLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t JSON_GetLastIndexOfNumber_m4461745 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSON::EatWhitespace(System.Char[],System.Int32&)
extern "C"  void JSON_EatWhitespace_m3154451942 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSON::LookAhead(System.Char[],System.Int32)
extern "C"  int32_t JSON_LookAhead_m1312866267 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSON::NextToken(System.Char[],System.Int32&)
extern "C"  int32_t JSON_NextToken_m3367226307 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSON::SerializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool JSON_SerializeValue_m1359367316 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSON::SerializeObject(System.Collections.Hashtable,System.Text.StringBuilder)
extern "C"  bool JSON_SerializeObject_m362291658 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___anObject0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSON::SerializeArray(System.Collections.ArrayList,System.Text.StringBuilder)
extern "C"  bool JSON_SerializeArray_m2301052027 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___anArray0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSON::SerializeString(System.String,System.Text.StringBuilder)
extern "C"  bool JSON_SerializeString_m2910343502 (Il2CppObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSON::SerializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  bool JSON_SerializeNumber_m592437066 (Il2CppObject * __this /* static, unused */, double ___number0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSON::IsNumeric(System.Object)
extern "C"  bool JSON_IsNumeric_m1655752640 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
