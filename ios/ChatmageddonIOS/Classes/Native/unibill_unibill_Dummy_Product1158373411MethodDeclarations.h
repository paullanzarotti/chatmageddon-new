﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// unibill.Dummy.Product
struct Product_t1158373411;

#include "codegen/il2cpp-codegen.h"

// System.Void unibill.Dummy.Product::.ctor()
extern "C"  void Product__ctor_m1876249810 (Product_t1158373411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
