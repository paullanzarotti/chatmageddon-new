﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsTile/OnGetResourcesPathDelegate
struct OnGetResourcesPathDelegate_t2869117133;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void OnlineMapsTile/OnGetResourcesPathDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnGetResourcesPathDelegate__ctor_m1866033868 (OnGetResourcesPathDelegate_t2869117133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnlineMapsTile/OnGetResourcesPathDelegate::Invoke(OnlineMapsTile)
extern "C"  String_t* OnGetResourcesPathDelegate_Invoke_m202633985 (OnGetResourcesPathDelegate_t2869117133 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnlineMapsTile/OnGetResourcesPathDelegate::BeginInvoke(OnlineMapsTile,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnGetResourcesPathDelegate_BeginInvoke_m2200100503 (OnGetResourcesPathDelegate_t2869117133 * __this, OnlineMapsTile_t21329940 * ___tile0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnlineMapsTile/OnGetResourcesPathDelegate::EndInvoke(System.IAsyncResult)
extern "C"  String_t* OnGetResourcesPathDelegate_EndInvoke_m3475897679 (OnGetResourcesPathDelegate_t2869117133 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
