﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerProfileModal
struct PlayerProfileModal_t3260625137;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PPUserNavScreen
struct  PPUserNavScreen_t245777140  : public NavigationScreen_t2333230110
{
public:
	// PlayerProfileModal PPUserNavScreen::modal
	PlayerProfileModal_t3260625137 * ___modal_3;
	// UISprite PPUserNavScreen::tabBackground
	UISprite_t603616735 * ___tabBackground_4;
	// UISprite PPUserNavScreen::tabMidDivider
	UISprite_t603616735 * ___tabMidDivider_5;
	// UISprite PPUserNavScreen::tabBotDivider
	UISprite_t603616735 * ___tabBotDivider_6;
	// UISprite PPUserNavScreen::profileOutline
	UISprite_t603616735 * ___profileOutline_7;
	// UISprite PPUserNavScreen::profileOverlay
	UISprite_t603616735 * ___profileOverlay_8;
	// UISprite PPUserNavScreen::profileDivider
	UISprite_t603616735 * ___profileDivider_9;
	// UISprite PPUserNavScreen::stealthDivider
	UISprite_t603616735 * ___stealthDivider_10;
	// UISprite PPUserNavScreen::pointsDivider
	UISprite_t603616735 * ___pointsDivider_11;
	// UISprite PPUserNavScreen::chatSprite
	UISprite_t603616735 * ___chatSprite_12;
	// UISprite PPUserNavScreen::attackForeground
	UISprite_t603616735 * ___attackForeground_13;
	// UISprite PPUserNavScreen::attackBackground
	UISprite_t603616735 * ___attackBackground_14;
	// UISprite PPUserNavScreen::blockForeground
	UISprite_t603616735 * ___blockForeground_15;
	// UISprite PPUserNavScreen::blockBackground
	UISprite_t603616735 * ___blockBackground_16;
	// UISprite PPUserNavScreen::settingsSprite
	UISprite_t603616735 * ___settingsSprite_17;
	// UISprite PPUserNavScreen::friendSprite
	UISprite_t603616735 * ___friendSprite_18;
	// UISprite PPUserNavScreen::acceptSprite
	UISprite_t603616735 * ___acceptSprite_19;
	// UISprite PPUserNavScreen::declineForeground
	UISprite_t603616735 * ___declineForeground_20;
	// UISprite PPUserNavScreen::declineBackground
	UISprite_t603616735 * ___declineBackground_21;
	// UISprite PPUserNavScreen::firstStrikeSprite
	UISprite_t603616735 * ___firstStrikeSprite_22;
	// System.Boolean PPUserNavScreen::UIclosing
	bool ___UIclosing_23;

public:
	inline static int32_t get_offset_of_modal_3() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___modal_3)); }
	inline PlayerProfileModal_t3260625137 * get_modal_3() const { return ___modal_3; }
	inline PlayerProfileModal_t3260625137 ** get_address_of_modal_3() { return &___modal_3; }
	inline void set_modal_3(PlayerProfileModal_t3260625137 * value)
	{
		___modal_3 = value;
		Il2CppCodeGenWriteBarrier(&___modal_3, value);
	}

	inline static int32_t get_offset_of_tabBackground_4() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___tabBackground_4)); }
	inline UISprite_t603616735 * get_tabBackground_4() const { return ___tabBackground_4; }
	inline UISprite_t603616735 ** get_address_of_tabBackground_4() { return &___tabBackground_4; }
	inline void set_tabBackground_4(UISprite_t603616735 * value)
	{
		___tabBackground_4 = value;
		Il2CppCodeGenWriteBarrier(&___tabBackground_4, value);
	}

	inline static int32_t get_offset_of_tabMidDivider_5() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___tabMidDivider_5)); }
	inline UISprite_t603616735 * get_tabMidDivider_5() const { return ___tabMidDivider_5; }
	inline UISprite_t603616735 ** get_address_of_tabMidDivider_5() { return &___tabMidDivider_5; }
	inline void set_tabMidDivider_5(UISprite_t603616735 * value)
	{
		___tabMidDivider_5 = value;
		Il2CppCodeGenWriteBarrier(&___tabMidDivider_5, value);
	}

	inline static int32_t get_offset_of_tabBotDivider_6() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___tabBotDivider_6)); }
	inline UISprite_t603616735 * get_tabBotDivider_6() const { return ___tabBotDivider_6; }
	inline UISprite_t603616735 ** get_address_of_tabBotDivider_6() { return &___tabBotDivider_6; }
	inline void set_tabBotDivider_6(UISprite_t603616735 * value)
	{
		___tabBotDivider_6 = value;
		Il2CppCodeGenWriteBarrier(&___tabBotDivider_6, value);
	}

	inline static int32_t get_offset_of_profileOutline_7() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___profileOutline_7)); }
	inline UISprite_t603616735 * get_profileOutline_7() const { return ___profileOutline_7; }
	inline UISprite_t603616735 ** get_address_of_profileOutline_7() { return &___profileOutline_7; }
	inline void set_profileOutline_7(UISprite_t603616735 * value)
	{
		___profileOutline_7 = value;
		Il2CppCodeGenWriteBarrier(&___profileOutline_7, value);
	}

	inline static int32_t get_offset_of_profileOverlay_8() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___profileOverlay_8)); }
	inline UISprite_t603616735 * get_profileOverlay_8() const { return ___profileOverlay_8; }
	inline UISprite_t603616735 ** get_address_of_profileOverlay_8() { return &___profileOverlay_8; }
	inline void set_profileOverlay_8(UISprite_t603616735 * value)
	{
		___profileOverlay_8 = value;
		Il2CppCodeGenWriteBarrier(&___profileOverlay_8, value);
	}

	inline static int32_t get_offset_of_profileDivider_9() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___profileDivider_9)); }
	inline UISprite_t603616735 * get_profileDivider_9() const { return ___profileDivider_9; }
	inline UISprite_t603616735 ** get_address_of_profileDivider_9() { return &___profileDivider_9; }
	inline void set_profileDivider_9(UISprite_t603616735 * value)
	{
		___profileDivider_9 = value;
		Il2CppCodeGenWriteBarrier(&___profileDivider_9, value);
	}

	inline static int32_t get_offset_of_stealthDivider_10() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___stealthDivider_10)); }
	inline UISprite_t603616735 * get_stealthDivider_10() const { return ___stealthDivider_10; }
	inline UISprite_t603616735 ** get_address_of_stealthDivider_10() { return &___stealthDivider_10; }
	inline void set_stealthDivider_10(UISprite_t603616735 * value)
	{
		___stealthDivider_10 = value;
		Il2CppCodeGenWriteBarrier(&___stealthDivider_10, value);
	}

	inline static int32_t get_offset_of_pointsDivider_11() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___pointsDivider_11)); }
	inline UISprite_t603616735 * get_pointsDivider_11() const { return ___pointsDivider_11; }
	inline UISprite_t603616735 ** get_address_of_pointsDivider_11() { return &___pointsDivider_11; }
	inline void set_pointsDivider_11(UISprite_t603616735 * value)
	{
		___pointsDivider_11 = value;
		Il2CppCodeGenWriteBarrier(&___pointsDivider_11, value);
	}

	inline static int32_t get_offset_of_chatSprite_12() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___chatSprite_12)); }
	inline UISprite_t603616735 * get_chatSprite_12() const { return ___chatSprite_12; }
	inline UISprite_t603616735 ** get_address_of_chatSprite_12() { return &___chatSprite_12; }
	inline void set_chatSprite_12(UISprite_t603616735 * value)
	{
		___chatSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___chatSprite_12, value);
	}

	inline static int32_t get_offset_of_attackForeground_13() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___attackForeground_13)); }
	inline UISprite_t603616735 * get_attackForeground_13() const { return ___attackForeground_13; }
	inline UISprite_t603616735 ** get_address_of_attackForeground_13() { return &___attackForeground_13; }
	inline void set_attackForeground_13(UISprite_t603616735 * value)
	{
		___attackForeground_13 = value;
		Il2CppCodeGenWriteBarrier(&___attackForeground_13, value);
	}

	inline static int32_t get_offset_of_attackBackground_14() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___attackBackground_14)); }
	inline UISprite_t603616735 * get_attackBackground_14() const { return ___attackBackground_14; }
	inline UISprite_t603616735 ** get_address_of_attackBackground_14() { return &___attackBackground_14; }
	inline void set_attackBackground_14(UISprite_t603616735 * value)
	{
		___attackBackground_14 = value;
		Il2CppCodeGenWriteBarrier(&___attackBackground_14, value);
	}

	inline static int32_t get_offset_of_blockForeground_15() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___blockForeground_15)); }
	inline UISprite_t603616735 * get_blockForeground_15() const { return ___blockForeground_15; }
	inline UISprite_t603616735 ** get_address_of_blockForeground_15() { return &___blockForeground_15; }
	inline void set_blockForeground_15(UISprite_t603616735 * value)
	{
		___blockForeground_15 = value;
		Il2CppCodeGenWriteBarrier(&___blockForeground_15, value);
	}

	inline static int32_t get_offset_of_blockBackground_16() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___blockBackground_16)); }
	inline UISprite_t603616735 * get_blockBackground_16() const { return ___blockBackground_16; }
	inline UISprite_t603616735 ** get_address_of_blockBackground_16() { return &___blockBackground_16; }
	inline void set_blockBackground_16(UISprite_t603616735 * value)
	{
		___blockBackground_16 = value;
		Il2CppCodeGenWriteBarrier(&___blockBackground_16, value);
	}

	inline static int32_t get_offset_of_settingsSprite_17() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___settingsSprite_17)); }
	inline UISprite_t603616735 * get_settingsSprite_17() const { return ___settingsSprite_17; }
	inline UISprite_t603616735 ** get_address_of_settingsSprite_17() { return &___settingsSprite_17; }
	inline void set_settingsSprite_17(UISprite_t603616735 * value)
	{
		___settingsSprite_17 = value;
		Il2CppCodeGenWriteBarrier(&___settingsSprite_17, value);
	}

	inline static int32_t get_offset_of_friendSprite_18() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___friendSprite_18)); }
	inline UISprite_t603616735 * get_friendSprite_18() const { return ___friendSprite_18; }
	inline UISprite_t603616735 ** get_address_of_friendSprite_18() { return &___friendSprite_18; }
	inline void set_friendSprite_18(UISprite_t603616735 * value)
	{
		___friendSprite_18 = value;
		Il2CppCodeGenWriteBarrier(&___friendSprite_18, value);
	}

	inline static int32_t get_offset_of_acceptSprite_19() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___acceptSprite_19)); }
	inline UISprite_t603616735 * get_acceptSprite_19() const { return ___acceptSprite_19; }
	inline UISprite_t603616735 ** get_address_of_acceptSprite_19() { return &___acceptSprite_19; }
	inline void set_acceptSprite_19(UISprite_t603616735 * value)
	{
		___acceptSprite_19 = value;
		Il2CppCodeGenWriteBarrier(&___acceptSprite_19, value);
	}

	inline static int32_t get_offset_of_declineForeground_20() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___declineForeground_20)); }
	inline UISprite_t603616735 * get_declineForeground_20() const { return ___declineForeground_20; }
	inline UISprite_t603616735 ** get_address_of_declineForeground_20() { return &___declineForeground_20; }
	inline void set_declineForeground_20(UISprite_t603616735 * value)
	{
		___declineForeground_20 = value;
		Il2CppCodeGenWriteBarrier(&___declineForeground_20, value);
	}

	inline static int32_t get_offset_of_declineBackground_21() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___declineBackground_21)); }
	inline UISprite_t603616735 * get_declineBackground_21() const { return ___declineBackground_21; }
	inline UISprite_t603616735 ** get_address_of_declineBackground_21() { return &___declineBackground_21; }
	inline void set_declineBackground_21(UISprite_t603616735 * value)
	{
		___declineBackground_21 = value;
		Il2CppCodeGenWriteBarrier(&___declineBackground_21, value);
	}

	inline static int32_t get_offset_of_firstStrikeSprite_22() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___firstStrikeSprite_22)); }
	inline UISprite_t603616735 * get_firstStrikeSprite_22() const { return ___firstStrikeSprite_22; }
	inline UISprite_t603616735 ** get_address_of_firstStrikeSprite_22() { return &___firstStrikeSprite_22; }
	inline void set_firstStrikeSprite_22(UISprite_t603616735 * value)
	{
		___firstStrikeSprite_22 = value;
		Il2CppCodeGenWriteBarrier(&___firstStrikeSprite_22, value);
	}

	inline static int32_t get_offset_of_UIclosing_23() { return static_cast<int32_t>(offsetof(PPUserNavScreen_t245777140, ___UIclosing_23)); }
	inline bool get_UIclosing_23() const { return ___UIclosing_23; }
	inline bool* get_address_of_UIclosing_23() { return &___UIclosing_23; }
	inline void set_UIclosing_23(bool value)
	{
		___UIclosing_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
