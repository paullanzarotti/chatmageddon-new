﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.HTTPClient/PostRequest
struct PostRequest_t4132869030;
// System.String
struct String_t;
// Unibill.Impl.PostParameter[]
struct PostParameterU5BU5D_t2629828016;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.HTTPClient/PostRequest::.ctor(System.String,Unibill.Impl.PostParameter[])
extern "C"  void PostRequest__ctor_m3120196303 (PostRequest_t4132869030 * __this, String_t* ___url0, PostParameterU5BU5D_t2629828016* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
