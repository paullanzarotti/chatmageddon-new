﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatLoader
struct ChatLoader_t2661087257;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void ChatLoader::.ctor()
extern "C"  void ChatLoader__ctor_m2255395144 (ChatLoader_t2661087257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatLoader::LoadChat(ChatNavScreen,Friend)
extern "C"  void ChatLoader_LoadChat_m1523431461 (ChatLoader_t2661087257 * __this, int32_t ___startScreen0, Friend_t3555014108 * ___friend1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatLoader::UnloadChat(ChatNavScreen)
extern "C"  void ChatLoader_UnloadChat_m2731263270 (ChatLoader_t2661087257 * __this, int32_t ___endScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatLoader::PopulateTitle(ChatNavScreen)
extern "C"  void ChatLoader_PopulateTitle_m664701049 (ChatLoader_t2661087257 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatLoader::SetButtonDirection(ButtonDirection)
extern "C"  void ChatLoader_SetButtonDirection_m143118164 (ChatLoader_t2661087257 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
