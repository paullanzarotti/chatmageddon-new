﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendManager
struct FriendManager_t36140827;
// System.String
struct String_t;
// Friend
struct Friend_t3555014108;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendListType2635913116.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void FriendManager::.ctor()
extern "C"  void FriendManager__ctor_m2885012430 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::StartList(FriendListType)
extern "C"  void FriendManager_StartList_m3996806706 (FriendManager_t36140827 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::SetListDrawn(FriendListType)
extern "C"  void FriendManager_SetListDrawn_m4098218656 (FriendManager_t36140827 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::SearchGlobalUserBase(System.String)
extern "C"  void FriendManager_SearchGlobalUserBase_m3779413629 (FriendManager_t36140827 * __this, String_t* ___searchTerm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::SendFriendRequest(Friend)
extern "C"  void FriendManager_SendFriendRequest_m3178032371 (FriendManager_t36140827 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::AcceptFriendRequest(Friend)
extern "C"  void FriendManager_AcceptFriendRequest_m1171321843 (FriendManager_t36140827 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::DeclineFriendRequest(Friend)
extern "C"  void FriendManager_DeclineFriendRequest_m3315162843 (FriendManager_t36140827 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::FriendProfileUpdate()
extern "C"  void FriendManager_FriendProfileUpdate_m2997001962 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::InnerFriendUpdate()
extern "C"  void FriendManager_InnerFriendUpdate_m3799450959 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::IncomingFriendRequestUpdate()
extern "C"  void FriendManager_IncomingFriendRequestUpdate_m56842502 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::ContactFriendAddition()
extern "C"  void FriendManager_ContactFriendAddition_m1693357766 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::OutgoingFriendRequestUpdate(System.Boolean,System.String)
extern "C"  void FriendManager_OutgoingFriendRequestUpdate_m2474854607 (FriendManager_t36140827 * __this, bool ___accepted0, String_t* ___recieverID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::OpenPhoneNumberUI()
extern "C"  void FriendManager_OpenPhoneNumberUI_m1653890897 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::RemovePhoneNumberUI()
extern "C"  void FriendManager_RemovePhoneNumberUI_m4192783867 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::PhoneNumberSubmit()
extern "C"  void FriendManager_PhoneNumberSubmit_m1360146181 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::ContactsUpdated()
extern "C"  void FriendManager_ContactsUpdated_m2931014582 (FriendManager_t36140827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendManager::<SearchGlobalUserBase>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void FriendManager_U3CSearchGlobalUserBaseU3Em__0_m1108116161 (FriendManager_t36140827 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
