﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "AssemblyU2DCSharp_OnlineMapsMarkerInstanceBase538187336.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsMarker3DInstance
struct  OnlineMapsMarker3DInstance_t3567168358  : public OnlineMapsMarkerInstanceBase_t538187336
{
public:
	// UnityEngine.Vector2 OnlineMapsMarker3DInstance::_position
	Vector2_t2243707579  ____position_3;
	// System.Single OnlineMapsMarker3DInstance::_scale
	float ____scale_4;
	// System.Int32 OnlineMapsMarker3DInstance::lastTouchCount
	int32_t ___lastTouchCount_5;
	// System.Boolean OnlineMapsMarker3DInstance::isPressed
	bool ___isPressed_6;
	// System.Int64[] OnlineMapsMarker3DInstance::lastClickTimes
	Int64U5BU5D_t717125112* ___lastClickTimes_7;
	// System.Collections.IEnumerator OnlineMapsMarker3DInstance::longPressEnumenator
	Il2CppObject * ___longPressEnumenator_8;

public:
	inline static int32_t get_offset_of__position_3() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3DInstance_t3567168358, ____position_3)); }
	inline Vector2_t2243707579  get__position_3() const { return ____position_3; }
	inline Vector2_t2243707579 * get_address_of__position_3() { return &____position_3; }
	inline void set__position_3(Vector2_t2243707579  value)
	{
		____position_3 = value;
	}

	inline static int32_t get_offset_of__scale_4() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3DInstance_t3567168358, ____scale_4)); }
	inline float get__scale_4() const { return ____scale_4; }
	inline float* get_address_of__scale_4() { return &____scale_4; }
	inline void set__scale_4(float value)
	{
		____scale_4 = value;
	}

	inline static int32_t get_offset_of_lastTouchCount_5() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3DInstance_t3567168358, ___lastTouchCount_5)); }
	inline int32_t get_lastTouchCount_5() const { return ___lastTouchCount_5; }
	inline int32_t* get_address_of_lastTouchCount_5() { return &___lastTouchCount_5; }
	inline void set_lastTouchCount_5(int32_t value)
	{
		___lastTouchCount_5 = value;
	}

	inline static int32_t get_offset_of_isPressed_6() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3DInstance_t3567168358, ___isPressed_6)); }
	inline bool get_isPressed_6() const { return ___isPressed_6; }
	inline bool* get_address_of_isPressed_6() { return &___isPressed_6; }
	inline void set_isPressed_6(bool value)
	{
		___isPressed_6 = value;
	}

	inline static int32_t get_offset_of_lastClickTimes_7() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3DInstance_t3567168358, ___lastClickTimes_7)); }
	inline Int64U5BU5D_t717125112* get_lastClickTimes_7() const { return ___lastClickTimes_7; }
	inline Int64U5BU5D_t717125112** get_address_of_lastClickTimes_7() { return &___lastClickTimes_7; }
	inline void set_lastClickTimes_7(Int64U5BU5D_t717125112* value)
	{
		___lastClickTimes_7 = value;
		Il2CppCodeGenWriteBarrier(&___lastClickTimes_7, value);
	}

	inline static int32_t get_offset_of_longPressEnumenator_8() { return static_cast<int32_t>(offsetof(OnlineMapsMarker3DInstance_t3567168358, ___longPressEnumenator_8)); }
	inline Il2CppObject * get_longPressEnumenator_8() const { return ___longPressEnumenator_8; }
	inline Il2CppObject ** get_address_of_longPressEnumenator_8() { return &___longPressEnumenator_8; }
	inline void set_longPressEnumenator_8(Il2CppObject * value)
	{
		___longPressEnumenator_8 = value;
		Il2CppCodeGenWriteBarrier(&___longPressEnumenator_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
