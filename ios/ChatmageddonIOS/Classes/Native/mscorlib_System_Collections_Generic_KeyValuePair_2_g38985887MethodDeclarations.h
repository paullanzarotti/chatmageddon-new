﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.PhoneMetadata>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m677705630(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t38985887 *, String_t*, PhoneMetadata_t366861403 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.PhoneMetadata>::get_Key()
#define KeyValuePair_2_get_Key_m2337119892(__this, method) ((  String_t* (*) (KeyValuePair_2_t38985887 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.PhoneMetadata>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m579195741(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t38985887 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.PhoneMetadata>::get_Value()
#define KeyValuePair_2_get_Value_m410256020(__this, method) ((  PhoneMetadata_t366861403 * (*) (KeyValuePair_2_t38985887 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.PhoneMetadata>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2252016557(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t38985887 *, PhoneMetadata_t366861403 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.PhoneMetadata>::ToString()
#define KeyValuePair_2_ToString_m2465480351(__this, method) ((  String_t* (*) (KeyValuePair_2_t38985887 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
