﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>
struct U3CSwitchPanelsU3Ec__Iterator0_t2843932824;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CSwitchPanelsU3Ec__Iterator0__ctor_m803570807_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method);
#define U3CSwitchPanelsU3Ec__Iterator0__ctor_m803570807(__this, method) ((  void (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))U3CSwitchPanelsU3Ec__Iterator0__ctor_m803570807_gshared)(__this, method)
// System.Boolean PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CSwitchPanelsU3Ec__Iterator0_MoveNext_m2574030129_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method);
#define U3CSwitchPanelsU3Ec__Iterator0_MoveNext_m2574030129(__this, method) ((  bool (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))U3CSwitchPanelsU3Ec__Iterator0_MoveNext_m2574030129_gshared)(__this, method)
// System.Object PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSwitchPanelsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1377570573_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method);
#define U3CSwitchPanelsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1377570573(__this, method) ((  Il2CppObject * (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))U3CSwitchPanelsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1377570573_gshared)(__this, method)
// System.Object PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSwitchPanelsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m6903685_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method);
#define U3CSwitchPanelsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m6903685(__this, method) ((  Il2CppObject * (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))U3CSwitchPanelsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m6903685_gshared)(__this, method)
// System.Void PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CSwitchPanelsU3Ec__Iterator0_Dispose_m3518941590_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method);
#define U3CSwitchPanelsU3Ec__Iterator0_Dispose_m3518941590(__this, method) ((  void (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))U3CSwitchPanelsU3Ec__Iterator0_Dispose_m3518941590_gshared)(__this, method)
// System.Void PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CSwitchPanelsU3Ec__Iterator0_Reset_m3658082208_gshared (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 * __this, const MethodInfo* method);
#define U3CSwitchPanelsU3Ec__Iterator0_Reset_m3658082208(__this, method) ((  void (*) (U3CSwitchPanelsU3Ec__Iterator0_t2843932824 *, const MethodInfo*))U3CSwitchPanelsU3Ec__Iterator0_Reset_m3658082208_gshared)(__this, method)
