﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Screens
struct CameraFilterPack_FX_Screens_t1869481571;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Screens::.ctor()
extern "C"  void CameraFilterPack_FX_Screens__ctor_m3247270428 (CameraFilterPack_FX_Screens_t1869481571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Screens::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Screens_get_material_m1889626887 (CameraFilterPack_FX_Screens_t1869481571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::Start()
extern "C"  void CameraFilterPack_FX_Screens_Start_m4009023976 (CameraFilterPack_FX_Screens_t1869481571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Screens_OnRenderImage_m1029152496 (CameraFilterPack_FX_Screens_t1869481571 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::OnValidate()
extern "C"  void CameraFilterPack_FX_Screens_OnValidate_m2780138745 (CameraFilterPack_FX_Screens_t1869481571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::Update()
extern "C"  void CameraFilterPack_FX_Screens_Update_m3503301487 (CameraFilterPack_FX_Screens_t1869481571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::OnDisable()
extern "C"  void CameraFilterPack_FX_Screens_OnDisable_m397426719 (CameraFilterPack_FX_Screens_t1869481571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
