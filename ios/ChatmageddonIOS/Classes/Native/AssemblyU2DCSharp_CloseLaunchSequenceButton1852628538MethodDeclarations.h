﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseLaunchSequenceButton
struct CloseLaunchSequenceButton_t1852628538;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseLaunchSequenceButton::.ctor()
extern "C"  void CloseLaunchSequenceButton__ctor_m826746405 (CloseLaunchSequenceButton_t1852628538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseLaunchSequenceButton::OnButtonClick()
extern "C"  void CloseLaunchSequenceButton_OnButtonClick_m3827542562 (CloseLaunchSequenceButton_t1852628538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
