﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPTexturePickerBase
struct IPTexturePickerBase_t1323538019;
// UIWidget
struct UIWidget_t1453041918;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void IPTexturePickerBase::.ctor()
extern "C"  void IPTexturePickerBase__ctor_m3511168794 (IPTexturePickerBase_t1323538019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget IPTexturePickerBase::GetCenterWidget()
extern "C"  UIWidget_t1453041918 * IPTexturePickerBase_GetCenterWidget_m593091026 (IPTexturePickerBase_t1323538019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget IPTexturePickerBase::GetWidgetAtScreenPos(UnityEngine.Vector2)
extern "C"  UIWidget_t1453041918 * IPTexturePickerBase_GetWidgetAtScreenPos_m134648402 (IPTexturePickerBase_t1323538019 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePickerBase::InitWidgets()
extern "C"  void IPTexturePickerBase_InitWidgets_m3654911447 (IPTexturePickerBase_t1323538019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePickerBase::MakeWidgetComponents()
extern "C"  void IPTexturePickerBase_MakeWidgetComponents_m2032077946 (IPTexturePickerBase_t1323538019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePickerBase::EnableWidgets(System.Boolean)
extern "C"  void IPTexturePickerBase_EnableWidgets_m2640619815 (IPTexturePickerBase_t1323538019 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPTexturePickerBase::WidgetsNeedRebuild()
extern "C"  bool IPTexturePickerBase_WidgetsNeedRebuild_m2890761898 (IPTexturePickerBase_t1323538019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
