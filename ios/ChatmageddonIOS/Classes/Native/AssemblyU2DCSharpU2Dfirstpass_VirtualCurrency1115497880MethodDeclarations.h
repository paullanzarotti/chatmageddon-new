﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VirtualCurrency
struct VirtualCurrency_t1115497880;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Decimal>
struct Dictionary_2_t2639480339;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VirtualCurrency::.ctor(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Decimal>)
extern "C"  void VirtualCurrency__ctor_m1073338258 (VirtualCurrency_t1115497880 * __this, String_t* ___id0, Dictionary_2_t2639480339 * ___mappings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VirtualCurrency::get_currencyId()
extern "C"  String_t* VirtualCurrency_get_currencyId_m689510161 (VirtualCurrency_t1115497880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VirtualCurrency::set_currencyId(System.String)
extern "C"  void VirtualCurrency_set_currencyId_m4250057680 (VirtualCurrency_t1115497880 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Decimal> VirtualCurrency::get_mappings()
extern "C"  Dictionary_2_t2639480339 * VirtualCurrency_get_mappings_m3031349423 (VirtualCurrency_t1115497880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VirtualCurrency::set_mappings(System.Collections.Generic.Dictionary`2<System.String,System.Decimal>)
extern "C"  void VirtualCurrency_set_mappings_m4089362864 (VirtualCurrency_t1115497880 * __this, Dictionary_2_t2639480339 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> VirtualCurrency::Serialize()
extern "C"  Dictionary_2_t309261261 * VirtualCurrency_Serialize_m731870781 (VirtualCurrency_t1115497880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
