﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Blend
struct CameraFilterPack_Blend2Camera_Blend_t3468651737;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Blend::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Blend__ctor_m3052499160 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Blend::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Blend_get_material_m4287357817 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Blend_Start_m1448217824 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Blend_OnRenderImage_m2407732424 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Blend_OnValidate_m2705370535 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Blend_Update_m849916797 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Blend_OnEnable_m2100204384 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Blend_OnDisable_m2308324657 (CameraFilterPack_Blend2Camera_Blend_t3468651737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
