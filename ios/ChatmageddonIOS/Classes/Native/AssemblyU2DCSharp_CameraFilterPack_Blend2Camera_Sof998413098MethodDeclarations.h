﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_SoftLight
struct CameraFilterPack_Blend2Camera_SoftLight_t998413098;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_SoftLight::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight__ctor_m3443862953 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_SoftLight::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_SoftLight_get_material_m2146600158 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::Start()
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight_Start_m3982084785 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight_OnRenderImage_m845646265 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight_OnValidate_m1694468886 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::Update()
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight_Update_m2167083692 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight_OnEnable_m677801457 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_SoftLight_OnDisable_m779065954 (CameraFilterPack_Blend2Camera_SoftLight_t998413098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
