﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.HelpCentre
struct  HelpCentre_t214342444  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.HelpCentre::helpMap
	Dictionary_2_t309261261 * ___helpMap_0;

public:
	inline static int32_t get_offset_of_helpMap_0() { return static_cast<int32_t>(offsetof(HelpCentre_t214342444, ___helpMap_0)); }
	inline Dictionary_2_t309261261 * get_helpMap_0() const { return ___helpMap_0; }
	inline Dictionary_2_t309261261 ** get_address_of_helpMap_0() { return &___helpMap_0; }
	inline void set_helpMap_0(Dictionary_2_t309261261 * value)
	{
		___helpMap_0 = value;
		Il2CppCodeGenWriteBarrier(&___helpMap_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
