﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsLoaderUIScaler
struct SettingsLoaderUIScaler_t190890564;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsLoaderUIScaler::.ctor()
extern "C"  void SettingsLoaderUIScaler__ctor_m1698280101 (SettingsLoaderUIScaler_t190890564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsLoaderUIScaler::GlobalUIScale()
extern "C"  void SettingsLoaderUIScaler_GlobalUIScale_m1713286846 (SettingsLoaderUIScaler_t190890564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
