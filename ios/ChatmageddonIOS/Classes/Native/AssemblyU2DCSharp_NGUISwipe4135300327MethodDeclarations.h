﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUISwipe
struct NGUISwipe_t4135300327;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void NGUISwipe::.ctor()
extern "C"  void NGUISwipe__ctor_m2629126096 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnDragStart()
extern "C"  void NGUISwipe_OnDragStart_m250272851 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnDrag(UnityEngine.Vector2)
extern "C"  void NGUISwipe_OnDrag_m285825611 (NGUISwipe_t4135300327 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnClick()
extern "C"  void NGUISwipe_OnClick_m1524159433 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnSwipeLeft()
extern "C"  void NGUISwipe_OnSwipeLeft_m2926134060 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUISwipe::ValidateSwipeLeft()
extern "C"  bool NGUISwipe_ValidateSwipeLeft_m2259270999 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnSwipeRight()
extern "C"  void NGUISwipe_OnSwipeRight_m4156092683 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUISwipe::ValidateSwipeRight()
extern "C"  bool NGUISwipe_ValidateSwipeRight_m25062282 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUISwipe::ValidateSwipeUp()
extern "C"  bool NGUISwipe_ValidateSwipeUp_m1716300785 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnSwipeUp()
extern "C"  void NGUISwipe_OnSwipeUp_m2236040136 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUISwipe::ValidateSwipeDown()
extern "C"  bool NGUISwipe_ValidateSwipeDown_m629282410 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnSwipeDown()
extern "C"  void NGUISwipe_OnSwipeDown_m4001017013 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnDragUp(UnityEngine.Vector2)
extern "C"  void NGUISwipe_OnDragUp_m955005028 (NGUISwipe_t4135300327 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnDragDown(UnityEngine.Vector2)
extern "C"  void NGUISwipe_OnDragDown_m3605450633 (NGUISwipe_t4135300327 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnDragRight(UnityEngine.Vector2)
extern "C"  void NGUISwipe_OnDragRight_m1677038115 (NGUISwipe_t4135300327 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnDragLeft(UnityEngine.Vector2)
extern "C"  void NGUISwipe_OnDragLeft_m1630827756 (NGUISwipe_t4135300327 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUISwipe::OnStaticClick()
extern "C"  void NGUISwipe_OnStaticClick_m2384777073 (NGUISwipe_t4135300327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
