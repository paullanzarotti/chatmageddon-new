﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserAudience>c__AnonStorey16
struct U3CUpdateUserAudienceU3Ec__AnonStorey16_t1914041783;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserAudience>c__AnonStorey16::.ctor()
extern "C"  void U3CUpdateUserAudienceU3Ec__AnonStorey16__ctor_m2345166268 (U3CUpdateUserAudienceU3Ec__AnonStorey16_t1914041783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserAudience>c__AnonStorey16::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserAudienceU3Ec__AnonStorey16_U3CU3Em__0_m3663486911 (U3CUpdateUserAudienceU3Ec__AnonStorey16_t1914041783 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
