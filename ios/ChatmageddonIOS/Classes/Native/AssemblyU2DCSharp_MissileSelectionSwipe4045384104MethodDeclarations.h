﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileSelectionSwipe
struct MissileSelectionSwipe_t4045384104;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileSelectionSwipe::.ctor()
extern "C"  void MissileSelectionSwipe__ctor_m3485594723 (MissileSelectionSwipe_t4045384104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSelectionSwipe::OnSwipeLeft()
extern "C"  void MissileSelectionSwipe_OnSwipeLeft_m578943853 (MissileSelectionSwipe_t4045384104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSelectionSwipe::OnSwipeRight()
extern "C"  void MissileSelectionSwipe_OnSwipeRight_m3312686270 (MissileSelectionSwipe_t4045384104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSelectionSwipe::OnStaticClick()
extern "C"  void MissileSelectionSwipe_OnStaticClick_m2678893842 (MissileSelectionSwipe_t4045384104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
