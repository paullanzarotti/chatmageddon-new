﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeNavScreen/<SaveScreenContent>c__Iterator0
struct U3CSaveScreenContentU3Ec__Iterator0_t664457975;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeNavScreen/<SaveScreenContent>c__Iterator0::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0__ctor_m3446083528 (U3CSaveScreenContentU3Ec__Iterator0_t664457975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StealthModeNavScreen/<SaveScreenContent>c__Iterator0::MoveNext()
extern "C"  bool U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m647893852 (U3CSaveScreenContentU3Ec__Iterator0_t664457975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StealthModeNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m230537180 (U3CSaveScreenContentU3Ec__Iterator0_t664457975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StealthModeNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m724071908 (U3CSaveScreenContentU3Ec__Iterator0_t664457975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen/<SaveScreenContent>c__Iterator0::Dispose()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Dispose_m2698884837 (U3CSaveScreenContentU3Ec__Iterator0_t664457975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeNavScreen/<SaveScreenContent>c__Iterator0::Reset()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Reset_m2609520463 (U3CSaveScreenContentU3Ec__Iterator0_t664457975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
