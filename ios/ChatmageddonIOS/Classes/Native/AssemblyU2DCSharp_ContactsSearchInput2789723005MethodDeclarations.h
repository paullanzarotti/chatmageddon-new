﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContactsSearchInput
struct ContactsSearchInput_t2789723005;

#include "codegen/il2cpp-codegen.h"

// System.Void ContactsSearchInput::.ctor()
extern "C"  void ContactsSearchInput__ctor_m966060750 (ContactsSearchInput_t2789723005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsSearchInput::OnSearchSubmit()
extern "C"  void ContactsSearchInput_OnSearchSubmit_m1269137187 (ContactsSearchInput_t2789723005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
