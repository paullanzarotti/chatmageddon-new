﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MiniGameController
struct MiniGameController_t3067676539;

#include "codegen/il2cpp-codegen.h"

// System.Void MiniGameController::.ctor()
extern "C"  void MiniGameController__ctor_m3567842244 (MiniGameController_t3067676539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniGameController::StartMiniGame()
extern "C"  void MiniGameController_StartMiniGame_m3771796383 (MiniGameController_t3067676539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniGameController::PauseMiniGame(System.Boolean)
extern "C"  void MiniGameController_PauseMiniGame_m285174634 (MiniGameController_t3067676539 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniGameController::OnPaused()
extern "C"  void MiniGameController_OnPaused_m1979317569 (MiniGameController_t3067676539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniGameController::StopMiniGame()
extern "C"  void MiniGameController_StopMiniGame_m2067588271 (MiniGameController_t3067676539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniGameController::ResetMiniGame()
extern "C"  void MiniGameController_ResetMiniGame_m3712504490 (MiniGameController_t3067676539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
