﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2
struct U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2::.ctor()
extern "C"  void U3CshowMailComposerWithScreenshotU3Ec__Iterator2__ctor_m3708263249 (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2::MoveNext()
extern "C"  bool U3CshowMailComposerWithScreenshotU3Ec__Iterator2_MoveNext_m2057956367 (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshowMailComposerWithScreenshotU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3770452383 (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshowMailComposerWithScreenshotU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1814737879 (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2::Dispose()
extern "C"  void U3CshowMailComposerWithScreenshotU3Ec__Iterator2_Dispose_m1025495366 (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<showMailComposerWithScreenshot>c__Iterator2::Reset()
extern "C"  void U3CshowMailComposerWithScreenshotU3Ec__Iterator2_Reset_m3525382112 (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
