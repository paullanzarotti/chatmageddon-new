﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>
struct ReadOnlyCollection_1_t4024405395;
// System.Collections.Generic.IList`1<DefendNavScreen>
struct IList_1_t84593008;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// DefendNavScreen[]
struct DefendNavScreenU5BU5D_t1548979470;
// System.Collections.Generic.IEnumerator`1<DefendNavScreen>
struct IEnumerator_1_t1314143530;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m483875737_gshared (ReadOnlyCollection_1_t4024405395 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m483875737(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m483875737_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3226799863_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3226799863(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3226799863_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m354613987_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m354613987(__this, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m354613987_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2900379614_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2900379614(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2900379614_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3005158440_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3005158440(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3005158440_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1532468066_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1532468066(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1532468066_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m881524116_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m881524116(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m881524116_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m599534603_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m599534603(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m599534603_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3923686975_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3923686975(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3923686975_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1011506204_gshared (ReadOnlyCollection_1_t4024405395 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1011506204(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1011506204_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3891987499_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3891987499(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3891987499_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m4031966824_gshared (ReadOnlyCollection_1_t4024405395 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4031966824(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4024405395 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m4031966824_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2296037538_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2296037538(__this, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2296037538_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3576715582_gshared (ReadOnlyCollection_1_t4024405395 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3576715582(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3576715582_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m565054690_gshared (ReadOnlyCollection_1_t4024405395 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m565054690(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4024405395 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m565054690_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1815965011_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1815965011(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1815965011_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1859237883_gshared (ReadOnlyCollection_1_t4024405395 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1859237883(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1859237883_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1068964825_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1068964825(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1068964825_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2751970816_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2751970816(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2751970816_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3050505740_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3050505740(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3050505740_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3495726075_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3495726075(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3495726075_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3503239460_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3503239460(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3503239460_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m737030883_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m737030883(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m737030883_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3784575510_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3784575510(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3784575510_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1724256413_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1724256413(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1724256413_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2710157547_gshared (ReadOnlyCollection_1_t4024405395 * __this, DefendNavScreenU5BU5D_t1548979470* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2710157547(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4024405395 *, DefendNavScreenU5BU5D_t1548979470*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2710157547_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m620061972_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m620061972(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m620061972_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3976530495_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3976530495(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3976530495_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2614092856_gshared (ReadOnlyCollection_1_t4024405395 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2614092856(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4024405395 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2614092856_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m228581096_gshared (ReadOnlyCollection_1_t4024405395 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m228581096(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4024405395 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m228581096_gshared)(__this, ___index0, method)
