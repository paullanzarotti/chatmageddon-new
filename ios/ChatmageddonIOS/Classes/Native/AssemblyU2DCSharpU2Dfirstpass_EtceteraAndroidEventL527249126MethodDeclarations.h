﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraAndroidEventListener
struct EtceteraAndroidEventListener_t527249126;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraAndroidEventListener::.ctor()
extern "C"  void EtceteraAndroidEventListener__ctor_m2622568717 (EtceteraAndroidEventListener_t527249126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
