﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.Internal.StreamChangeCallback
struct StreamChangeCallback_t1492982741;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Configuration.Internal.StreamChangeCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void StreamChangeCallback__ctor_m1531531075 (StreamChangeCallback_t1492982741 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Internal.StreamChangeCallback::Invoke(System.String)
extern "C"  void StreamChangeCallback_Invoke_m2966776365 (StreamChangeCallback_t1492982741 * __this, String_t* ___streamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Configuration.Internal.StreamChangeCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StreamChangeCallback_BeginInvoke_m2667059428 (StreamChangeCallback_t1492982741 * __this, String_t* ___streamName0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Internal.StreamChangeCallback::EndInvoke(System.IAsyncResult)
extern "C"  void StreamChangeCallback_EndInvoke_m2699250513 (StreamChangeCallback_t1492982741 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
