﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendHoldButton/<UpdateHoldProgress>c__Iterator0
struct U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DefendHoldButton/<UpdateHoldProgress>c__Iterator0::.ctor()
extern "C"  void U3CUpdateHoldProgressU3Ec__Iterator0__ctor_m1064458689 (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DefendHoldButton/<UpdateHoldProgress>c__Iterator0::MoveNext()
extern "C"  bool U3CUpdateHoldProgressU3Ec__Iterator0_MoveNext_m126512523 (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DefendHoldButton/<UpdateHoldProgress>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateHoldProgressU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m609257231 (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DefendHoldButton/<UpdateHoldProgress>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateHoldProgressU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m726402727 (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton/<UpdateHoldProgress>c__Iterator0::Dispose()
extern "C"  void U3CUpdateHoldProgressU3Ec__Iterator0_Dispose_m2709234688 (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendHoldButton/<UpdateHoldProgress>c__Iterator0::Reset()
extern "C"  void U3CUpdateHoldProgressU3Ec__Iterator0_Reset_m4158286846 (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
