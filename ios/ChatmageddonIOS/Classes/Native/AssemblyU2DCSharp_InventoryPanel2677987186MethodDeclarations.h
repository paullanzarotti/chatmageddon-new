﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InventoryPanel
struct InventoryPanel_t2677987186;

#include "codegen/il2cpp-codegen.h"

// System.Void InventoryPanel::.ctor()
extern "C"  void InventoryPanel__ctor_m1511447747 (InventoryPanel_t2677987186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
