﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Marker_GPS_Example
struct Marker_GPS_Example_t1835250040;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Marker_GPS_Example::.ctor()
extern "C"  void Marker_GPS_Example__ctor_m2235486025 (Marker_GPS_Example_t1835250040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker_GPS_Example::Start()
extern "C"  void Marker_GPS_Example_Start_m2265961241 (Marker_GPS_Example_t1835250040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker_GPS_Example::OnLocationChanged(UnityEngine.Vector2)
extern "C"  void Marker_GPS_Example_OnLocationChanged_m3716947327 (Marker_GPS_Example_t1835250040 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
