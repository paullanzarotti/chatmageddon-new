﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<InternetReachability>::.ctor()
#define MonoSingleton_1__ctor_m1961451486(__this, method) ((  void (*) (MonoSingleton_1_t2969793346 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<InternetReachability>::Awake()
#define MonoSingleton_1_Awake_m3831476781(__this, method) ((  void (*) (MonoSingleton_1_t2969793346 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<InternetReachability>::get_Instance()
#define MonoSingleton_1_get_Instance_m1131606715(__this /* static, unused */, method) ((  InternetReachability_t3219127626 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<InternetReachability>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3510454471(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<InternetReachability>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3715704156(__this, method) ((  void (*) (MonoSingleton_1_t2969793346 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<InternetReachability>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2188160800(__this, method) ((  void (*) (MonoSingleton_1_t2969793346 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<InternetReachability>::.cctor()
#define MonoSingleton_1__cctor_m264260847(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
