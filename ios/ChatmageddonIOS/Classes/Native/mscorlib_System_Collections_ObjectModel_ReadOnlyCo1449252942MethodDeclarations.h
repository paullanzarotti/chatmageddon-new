﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>
struct ReadOnlyCollection_1_t1449252942;
// System.Collections.Generic.IList`1<PhoneNumberNavScreen>
struct IList_1_t1804407851;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PhoneNumberNavScreen[]
struct PhoneNumberNavScreenU5BU5D_t4021408007;
// System.Collections.Generic.IEnumerator`1<PhoneNumberNavScreen>
struct IEnumerator_1_t3033958373;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2850445006_gshared (ReadOnlyCollection_1_t1449252942 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2850445006(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2850445006_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4064502910_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4064502910(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4064502910_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1452481610_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1452481610(__this, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1452481610_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1284255587_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1284255587(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1284255587_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2734584651_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2734584651(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2734584651_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2554408775_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2554408775(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2554408775_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1577755587_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1577755587(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1577755587_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3052140788_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3052140788(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3052140788_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2921249244_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2921249244(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2921249244_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2071945989_gshared (ReadOnlyCollection_1_t1449252942 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2071945989(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2071945989_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m819238052_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m819238052(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m819238052_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1460696205_gshared (ReadOnlyCollection_1_t1449252942 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1460696205(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1449252942 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1460696205_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1822894285_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1822894285(__this, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1822894285_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2207876741_gshared (ReadOnlyCollection_1_t1449252942 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2207876741(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2207876741_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3198358495_gshared (ReadOnlyCollection_1_t1449252942 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3198358495(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1449252942 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3198358495_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2585551846_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2585551846(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2585551846_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3301528444_gshared (ReadOnlyCollection_1_t1449252942 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3301528444(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3301528444_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3832472084_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3832472084(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3832472084_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1513351201_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1513351201(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1513351201_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3832697369_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3832697369(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3832697369_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m551968774_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m551968774(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m551968774_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4181528461_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4181528461(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4181528461_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3452558044_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3452558044(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3452558044_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m633245583_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m633245583(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m633245583_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3905260100_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3905260100(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3905260100_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3623254298_gshared (ReadOnlyCollection_1_t1449252942 * __this, PhoneNumberNavScreenU5BU5D_t4021408007* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3623254298(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1449252942 *, PhoneNumberNavScreenU5BU5D_t4021408007*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3623254298_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3208500685_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3208500685(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3208500685_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3964454084_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3964454084(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3964454084_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1397307965_gshared (ReadOnlyCollection_1_t1449252942 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1397307965(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1449252942 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1397307965_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PhoneNumberNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m1589437679_gshared (ReadOnlyCollection_1_t1449252942 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1589437679(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1449252942 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1589437679_gshared)(__this, ___index0, method)
