﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<AttemptTab>
struct List_1_t3333829496;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendAttemptController
struct  DefendAttemptController_t232561863  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 DefendAttemptController::maxAttempts
	int32_t ___maxAttempts_2;
	// UnityEngine.Color DefendAttemptController::attemptFailedColour
	Color_t2020392075  ___attemptFailedColour_3;
	// UnityEngine.Color DefendAttemptController::attemptSuccessColour
	Color_t2020392075  ___attemptSuccessColour_4;
	// System.Collections.Generic.List`1<AttemptTab> DefendAttemptController::tabs
	List_1_t3333829496 * ___tabs_5;
	// System.Single DefendAttemptController::tabDistance
	float ___tabDistance_6;
	// System.Int32 DefendAttemptController::attemptCount
	int32_t ___attemptCount_7;
	// System.String DefendAttemptController::miniGameResourcePath
	String_t* ___miniGameResourcePath_8;

public:
	inline static int32_t get_offset_of_maxAttempts_2() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___maxAttempts_2)); }
	inline int32_t get_maxAttempts_2() const { return ___maxAttempts_2; }
	inline int32_t* get_address_of_maxAttempts_2() { return &___maxAttempts_2; }
	inline void set_maxAttempts_2(int32_t value)
	{
		___maxAttempts_2 = value;
	}

	inline static int32_t get_offset_of_attemptFailedColour_3() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___attemptFailedColour_3)); }
	inline Color_t2020392075  get_attemptFailedColour_3() const { return ___attemptFailedColour_3; }
	inline Color_t2020392075 * get_address_of_attemptFailedColour_3() { return &___attemptFailedColour_3; }
	inline void set_attemptFailedColour_3(Color_t2020392075  value)
	{
		___attemptFailedColour_3 = value;
	}

	inline static int32_t get_offset_of_attemptSuccessColour_4() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___attemptSuccessColour_4)); }
	inline Color_t2020392075  get_attemptSuccessColour_4() const { return ___attemptSuccessColour_4; }
	inline Color_t2020392075 * get_address_of_attemptSuccessColour_4() { return &___attemptSuccessColour_4; }
	inline void set_attemptSuccessColour_4(Color_t2020392075  value)
	{
		___attemptSuccessColour_4 = value;
	}

	inline static int32_t get_offset_of_tabs_5() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___tabs_5)); }
	inline List_1_t3333829496 * get_tabs_5() const { return ___tabs_5; }
	inline List_1_t3333829496 ** get_address_of_tabs_5() { return &___tabs_5; }
	inline void set_tabs_5(List_1_t3333829496 * value)
	{
		___tabs_5 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_5, value);
	}

	inline static int32_t get_offset_of_tabDistance_6() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___tabDistance_6)); }
	inline float get_tabDistance_6() const { return ___tabDistance_6; }
	inline float* get_address_of_tabDistance_6() { return &___tabDistance_6; }
	inline void set_tabDistance_6(float value)
	{
		___tabDistance_6 = value;
	}

	inline static int32_t get_offset_of_attemptCount_7() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___attemptCount_7)); }
	inline int32_t get_attemptCount_7() const { return ___attemptCount_7; }
	inline int32_t* get_address_of_attemptCount_7() { return &___attemptCount_7; }
	inline void set_attemptCount_7(int32_t value)
	{
		___attemptCount_7 = value;
	}

	inline static int32_t get_offset_of_miniGameResourcePath_8() { return static_cast<int32_t>(offsetof(DefendAttemptController_t232561863, ___miniGameResourcePath_8)); }
	inline String_t* get_miniGameResourcePath_8() const { return ___miniGameResourcePath_8; }
	inline String_t** get_address_of_miniGameResourcePath_8() { return &___miniGameResourcePath_8; }
	inline void set_miniGameResourcePath_8(String_t* value)
	{
		___miniGameResourcePath_8 = value;
		Il2CppCodeGenWriteBarrier(&___miniGameResourcePath_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
