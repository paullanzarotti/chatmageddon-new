﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XCData
struct XCData_t1306050895;
// System.String
struct String_t;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XCData1306050895.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"

// System.Void System.Xml.Linq.XCData::.ctor(System.String)
extern "C"  void XCData__ctor_m482046048 (XCData_t1306050895 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XCData::.ctor(System.Xml.Linq.XCData)
extern "C"  void XCData__ctor_m1754149489 (XCData_t1306050895 * __this, XCData_t1306050895 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XCData::get_NodeType()
extern "C"  int32_t XCData_get_NodeType_m3940428509 (XCData_t1306050895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XCData::WriteTo(System.Xml.XmlWriter)
extern "C"  void XCData_WriteTo_m2964317266 (XCData_t1306050895 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
