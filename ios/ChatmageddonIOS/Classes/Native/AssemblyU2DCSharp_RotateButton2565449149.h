﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateButton
struct  RotateButton_t2565449149  : public SFXButton_t792651341
{
public:
	// System.Boolean RotateButton::pressed
	bool ___pressed_5;
	// UISprite RotateButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_6;
	// System.String RotateButton::buttonSpriteName
	String_t* ___buttonSpriteName_7;
	// System.String RotateButton::pressedSpriteName
	String_t* ___pressedSpriteName_8;
	// System.Boolean RotateButton::rotating
	bool ___rotating_9;

public:
	inline static int32_t get_offset_of_pressed_5() { return static_cast<int32_t>(offsetof(RotateButton_t2565449149, ___pressed_5)); }
	inline bool get_pressed_5() const { return ___pressed_5; }
	inline bool* get_address_of_pressed_5() { return &___pressed_5; }
	inline void set_pressed_5(bool value)
	{
		___pressed_5 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_6() { return static_cast<int32_t>(offsetof(RotateButton_t2565449149, ___buttonSprite_6)); }
	inline UISprite_t603616735 * get_buttonSprite_6() const { return ___buttonSprite_6; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_6() { return &___buttonSprite_6; }
	inline void set_buttonSprite_6(UISprite_t603616735 * value)
	{
		___buttonSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_6, value);
	}

	inline static int32_t get_offset_of_buttonSpriteName_7() { return static_cast<int32_t>(offsetof(RotateButton_t2565449149, ___buttonSpriteName_7)); }
	inline String_t* get_buttonSpriteName_7() const { return ___buttonSpriteName_7; }
	inline String_t** get_address_of_buttonSpriteName_7() { return &___buttonSpriteName_7; }
	inline void set_buttonSpriteName_7(String_t* value)
	{
		___buttonSpriteName_7 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSpriteName_7, value);
	}

	inline static int32_t get_offset_of_pressedSpriteName_8() { return static_cast<int32_t>(offsetof(RotateButton_t2565449149, ___pressedSpriteName_8)); }
	inline String_t* get_pressedSpriteName_8() const { return ___pressedSpriteName_8; }
	inline String_t** get_address_of_pressedSpriteName_8() { return &___pressedSpriteName_8; }
	inline void set_pressedSpriteName_8(String_t* value)
	{
		___pressedSpriteName_8 = value;
		Il2CppCodeGenWriteBarrier(&___pressedSpriteName_8, value);
	}

	inline static int32_t get_offset_of_rotating_9() { return static_cast<int32_t>(offsetof(RotateButton_t2565449149, ___rotating_9)); }
	inline bool get_rotating_9() const { return ___rotating_9; }
	inline bool* get_address_of_rotating_9() { return &___rotating_9; }
	inline void set_rotating_9(bool value)
	{
		___rotating_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
