﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackAcceptButton
struct AttackAcceptButton_t1767177984;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackAcceptButton::.ctor()
extern "C"  void AttackAcceptButton__ctor_m4115640605 (AttackAcceptButton_t1767177984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackAcceptButton::OnButtonClick()
extern "C"  void AttackAcceptButton_OnButtonClick_m1954387044 (AttackAcceptButton_t1767177984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
