﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3248597474MethodDeclarations.h"

// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::.ctor()
#define BaseInfiniteListPopulator_1__ctor_m591612349(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1__ctor_m2641845277_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::Awake()
#define BaseInfiniteListPopulator_1_Awake_m373406692(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_Awake_m758246746_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::ResetPanel()
#define BaseInfiniteListPopulator_1_ResetPanel_m3729044580(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m3403306384(__this, ___item0, ___dataIndex1, ___carouselIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, Transform_t3275118058 *, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared)(__this, ___item0, ___dataIndex1, ___carouselIndex2, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetStartIndex(System.Int32)
#define BaseInfiniteListPopulator_1_SetStartIndex_m2347470506(__this, ___inStartIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared)(__this, ___inStartIndex0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetOriginalData(System.Collections.ArrayList)
#define BaseInfiniteListPopulator_1_SetOriginalData_m3858637389(__this, ___inDataList0, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, ArrayList_t4252133567 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared)(__this, ___inDataList0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetSectionIndices(System.Collections.Generic.List`1<System.Int32>)
#define BaseInfiniteListPopulator_1_SetSectionIndices_m4235983252(__this, ___inSectionsIndices0, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, List_1_t1440998580 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared)(__this, ___inSectionsIndices0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::RefreshTableView()
#define BaseInfiniteListPopulator_1_RefreshTableView_m1528807367(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::InitTableView(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableView_m3052086717(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::InitTableViewImp(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableViewImp_m1140199781(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::RepositionList()
#define BaseInfiniteListPopulator_1_RepositionList_m3522463965(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::UponPostRepositionReq()
#define BaseInfiniteListPopulator_1_UponPostRepositionReq_m3705884077(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_InitListItemWithIndex_m808044919(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, Transform_t3275118058 *, int32_t, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared)(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::PrepareListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m3631202886(__this, ___item0, ___newIndex1, ___oldIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, Transform_t3275118058 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::PrepareCarouselListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m3125516161(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, Transform_t3275118058 *, int32_t, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<ChatContactsListItem>::ItemIsInvisible(System.Int32)
#define BaseInfiniteListPopulator_1_ItemIsInvisible_m2268781188(__this, ___itemNumber0, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetItemInvisible(Item,System.Boolean)
#define BaseInfiniteListPopulator_1_SetItemInvisible_m17622873(__this, ___item0, ___forward1, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, ChatContactsListItem_t4002571236 *, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared)(__this, ___item0, ___forward1, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::UpdateList(System.Int32)
#define BaseInfiniteListPopulator_1_UpdateList_m625612763(__this, ___itemNumber0, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::UpdateListDirection(BaseInfiniteListPopulator`1/ListMovementDirection<Item>,System.Int32)
#define BaseInfiniteListPopulator_1_UpdateListDirection_m2314249060(__this, ___direction0, ___itemNumber1, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared)(__this, ___direction0, ___itemNumber1, method)
// System.Int32 BaseInfiniteListPopulator`1<ChatContactsListItem>::GetJumpIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetJumpIndexForItem_m2573526092(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared)(__this, ___itemDataIndex0, method)
// System.Int32 BaseInfiniteListPopulator`1<ChatContactsListItem>::GetRealIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetRealIndexForItem_m2105385572(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::itemIsPressed(System.Int32,System.Boolean)
#define BaseInfiniteListPopulator_1_itemIsPressed_m1292671708(__this, ___itemDataIndex0, ___isDown1, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, bool, const MethodInfo*))BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared)(__this, ___itemDataIndex0, ___isDown1, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::itemClicked(System.Int32)
#define BaseInfiniteListPopulator_1_itemClicked_m2034666262(__this, ___itemDataIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::StopScrollViewMomentum()
#define BaseInfiniteListPopulator_1_StopScrollViewMomentum_m3673982513(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared)(__this, method)
// UnityEngine.Transform BaseInfiniteListPopulator`1<ChatContactsListItem>::GetItemFromPool(System.Int32)
#define BaseInfiniteListPopulator_1_GetItemFromPool_m1967626405(__this, ___i0, method) ((  Transform_t3275118058 * (*) (BaseInfiniteListPopulator_1_t266752119 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared)(__this, ___i0, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::RefreshPool()
#define BaseInfiniteListPopulator_1_RefreshPool_m1267260622(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetScrollToBottom()
#define BaseInfiniteListPopulator_1_SetScrollToBottom_m2564749828(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetScrollPosition(System.Single,System.Boolean)
#define BaseInfiniteListPopulator_1_SetScrollPosition_m3899008349(__this, ___exactPosition0, ___up1, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, float, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared)(__this, ___exactPosition0, ___up1, method)
// System.Void BaseInfiniteListPopulator`1<ChatContactsListItem>::SetScrollPositionOverTime(System.Single)
#define BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m452347157(__this, ___exactPosition0, method) ((  void (*) (BaseInfiniteListPopulator_1_t266752119 *, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared)(__this, ___exactPosition0, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<ChatContactsListItem>::SetPanelPosOverTime(System.Single,System.Single)
#define BaseInfiniteListPopulator_1_SetPanelPosOverTime_m2044747802(__this, ___seconds0, ___position1, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t266752119 *, float, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared)(__this, ___seconds0, ___position1, method)
