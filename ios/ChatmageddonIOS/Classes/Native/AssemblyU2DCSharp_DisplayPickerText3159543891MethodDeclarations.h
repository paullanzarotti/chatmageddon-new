﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisplayPickerText
struct DisplayPickerText_t3159543891;

#include "codegen/il2cpp-codegen.h"

// System.Void DisplayPickerText::.ctor()
extern "C"  void DisplayPickerText__ctor_m3278538846 (DisplayPickerText_t3159543891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisplayPickerText::Awake()
extern "C"  void DisplayPickerText_Awake_m161358687 (DisplayPickerText_t3159543891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisplayPickerText::DisplayText()
extern "C"  void DisplayPickerText_DisplayText_m2100324393 (DisplayPickerText_t3159543891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
