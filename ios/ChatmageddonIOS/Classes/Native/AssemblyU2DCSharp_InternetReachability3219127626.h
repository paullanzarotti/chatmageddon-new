﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2969793346.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InternetReachability
struct  InternetReachability_t3219127626  : public MonoSingleton_1_t2969793346
{
public:
	// System.Boolean InternetReachability::internetConnection
	bool ___internetConnection_3;
	// UnityEngine.Coroutine InternetReachability::internetPoll
	Coroutine_t2299508840 * ___internetPoll_6;
	// System.Int32 InternetReachability::failedConnectionCount
	int32_t ___failedConnectionCount_7;
	// System.Int32 InternetReachability::connectionLockoutAmount
	int32_t ___connectionLockoutAmount_8;

public:
	inline static int32_t get_offset_of_internetConnection_3() { return static_cast<int32_t>(offsetof(InternetReachability_t3219127626, ___internetConnection_3)); }
	inline bool get_internetConnection_3() const { return ___internetConnection_3; }
	inline bool* get_address_of_internetConnection_3() { return &___internetConnection_3; }
	inline void set_internetConnection_3(bool value)
	{
		___internetConnection_3 = value;
	}

	inline static int32_t get_offset_of_internetPoll_6() { return static_cast<int32_t>(offsetof(InternetReachability_t3219127626, ___internetPoll_6)); }
	inline Coroutine_t2299508840 * get_internetPoll_6() const { return ___internetPoll_6; }
	inline Coroutine_t2299508840 ** get_address_of_internetPoll_6() { return &___internetPoll_6; }
	inline void set_internetPoll_6(Coroutine_t2299508840 * value)
	{
		___internetPoll_6 = value;
		Il2CppCodeGenWriteBarrier(&___internetPoll_6, value);
	}

	inline static int32_t get_offset_of_failedConnectionCount_7() { return static_cast<int32_t>(offsetof(InternetReachability_t3219127626, ___failedConnectionCount_7)); }
	inline int32_t get_failedConnectionCount_7() const { return ___failedConnectionCount_7; }
	inline int32_t* get_address_of_failedConnectionCount_7() { return &___failedConnectionCount_7; }
	inline void set_failedConnectionCount_7(int32_t value)
	{
		___failedConnectionCount_7 = value;
	}

	inline static int32_t get_offset_of_connectionLockoutAmount_8() { return static_cast<int32_t>(offsetof(InternetReachability_t3219127626, ___connectionLockoutAmount_8)); }
	inline int32_t get_connectionLockoutAmount_8() const { return ___connectionLockoutAmount_8; }
	inline int32_t* get_address_of_connectionLockoutAmount_8() { return &___connectionLockoutAmount_8; }
	inline void set_connectionLockoutAmount_8(int32_t value)
	{
		___connectionLockoutAmount_8 = value;
	}
};

struct InternetReachability_t3219127626_StaticFields
{
public:
	// System.Int32 InternetReachability::timeoutMiliseconds
	int32_t ___timeoutMiliseconds_4;
	// System.String InternetReachability::checkURL
	String_t* ___checkURL_5;

public:
	inline static int32_t get_offset_of_timeoutMiliseconds_4() { return static_cast<int32_t>(offsetof(InternetReachability_t3219127626_StaticFields, ___timeoutMiliseconds_4)); }
	inline int32_t get_timeoutMiliseconds_4() const { return ___timeoutMiliseconds_4; }
	inline int32_t* get_address_of_timeoutMiliseconds_4() { return &___timeoutMiliseconds_4; }
	inline void set_timeoutMiliseconds_4(int32_t value)
	{
		___timeoutMiliseconds_4 = value;
	}

	inline static int32_t get_offset_of_checkURL_5() { return static_cast<int32_t>(offsetof(InternetReachability_t3219127626_StaticFields, ___checkURL_5)); }
	inline String_t* get_checkURL_5() const { return ___checkURL_5; }
	inline String_t** get_address_of_checkURL_5() { return &___checkURL_5; }
	inline void set_checkURL_5(String_t* value)
	{
		___checkURL_5 = value;
		Il2CppCodeGenWriteBarrier(&___checkURL_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
