﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen901129523.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"

// System.Void System.Array/InternalEnumerator`1<ChatNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m154880432_gshared (InternalEnumerator_1_t901129523 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m154880432(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t901129523 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m154880432_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ChatNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1922998684_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1922998684(__this, method) ((  void (*) (InternalEnumerator_1_t901129523 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1922998684_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ChatNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3846825882_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3846825882(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t901129523 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3846825882_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ChatNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2998198293_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2998198293(__this, method) ((  void (*) (InternalEnumerator_1_t901129523 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2998198293_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ChatNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3954121856_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3954121856(__this, method) ((  bool (*) (InternalEnumerator_1_t901129523 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3954121856_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ChatNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2578937965_gshared (InternalEnumerator_1_t901129523 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2578937965(__this, method) ((  int32_t (*) (InternalEnumerator_1_t901129523 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2578937965_gshared)(__this, method)
