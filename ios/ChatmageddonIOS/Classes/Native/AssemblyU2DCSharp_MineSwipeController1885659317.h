﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineModal
struct MineModal_t1141032720;

#include "AssemblyU2DCSharp_ItemSwipeController3011021799.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineSwipeController
struct  MineSwipeController_t1885659317  : public ItemSwipeController_t3011021799
{
public:
	// MineModal MineSwipeController::modal
	MineModal_t1141032720 * ___modal_23;

public:
	inline static int32_t get_offset_of_modal_23() { return static_cast<int32_t>(offsetof(MineSwipeController_t1885659317, ___modal_23)); }
	inline MineModal_t1141032720 * get_modal_23() const { return ___modal_23; }
	inline MineModal_t1141032720 ** get_address_of_modal_23() { return &___modal_23; }
	inline void set_modal_23(MineModal_t1141032720 * value)
	{
		___modal_23 = value;
		Il2CppCodeGenWriteBarrier(&___modal_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
