﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Xml.XPath.XPathFunctionTranslate
struct XPathFunctionTranslate_t2720664953;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t2900945452;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t2454437973;
// System.String
struct String_t;
// System.Xml.XPath.XPathFunctionTrue
struct XPathFunctionTrue_t1672815575;
// System.Xml.XPath.XPathItem
struct XPathItem_t3130801258;
// System.Xml.XPath.XPathIteratorComparer
struct XPathIteratorComparer_t2522471076;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t3981235968;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t452251917;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t3192332357;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0
struct U3CEnumerateChildrenU3Ec__Iterator0_t1235609798;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Xml.XPath.XPathNavigator/EnumerableIterator
struct EnumerableIterator_t1602910416;
// System.Xml.XPath.XPathNavigatorComparer
struct XPathNavigatorComparer_t2670709129;
// System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2
struct U3CGetEnumeratorU3Ec__Iterator2_t959253998;
// System.Xml.XPath.XPathNumericFunction
struct XPathNumericFunction_t2367269690;
// System.Xml.XPath.XPathSortElement
struct XPathSortElement_t1040291071;
// System.Xml.XPath.XPathSorter
struct XPathSorter_t3491953490;
// System.Xml.XPath.XPathSorters
struct XPathSorters_t4019574815;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.Xsl.XsltContext
struct XsltContext_t2013960098;
// System.Xml.Xsl.IXsltContextVariable
struct IXsltContextVariable_t3385767465;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Xsl.IXsltContextFunction
struct IXsltContextFunction_t1467867933;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t2966113519;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate2720664953.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate2720664953MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2900945452.h"
#include "mscorlib_System_Void1841601450.h"
#include "System_Xml_System_Xml_XPath_XPathFunction759167395MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2900945452MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathException1503722168MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XPath_XPathException1503722168.h"
#include "System_Xml_System_Xml_XPath_Expression1283317256.h"
#include "System_Xml_System_Xml_XPath_XPathResultType1521569578.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_Xml_System_Xml_XPath_Expression1283317256MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_BaseIterator2454437973.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue1672815575.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue1672815575MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction2511646183MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathItem3130801258.h"
#include "System_Xml_System_Xml_XPath_XPathItem3130801258MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer2522471076.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer2522471076MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator3192332357.h"
#include "System_Xml_System_Xml_XmlNodeOrder930453011.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator3192332357MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator3981235968.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator3981235968MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope3601604274.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope3601604274MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType817388867.h"
#include "System_Xml_System_Xml_XPath_XPathExpression452251917.h"
#include "System_Xml_System_Xml_XPath_XPathExpression452251917MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression3686330919MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NullIterator2539636145MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression3686330919.h"
#include "System_Xml_System_Xml_XPath_NullIterator2539636145.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1235609798MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1235609798.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera1602910416MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2718786873MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera1602910416.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2718786873.h"
#include "System_Xml_System_Xml_XQueryConvert3510797773MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "System_Xml_System_Xml_XmlConvert1936105738MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2670709129.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2670709129MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe959253998MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe959253998.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType817388867MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction2367269690.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction2367269690MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathResultType1521569578MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement1040291071.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement1040291071MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3491953490.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3491953490MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XmlDataType315095065.h"
#include "System_Xml_System_Xml_XPath_XPathSorters4019574815.h"
#include "System_Xml_System_Xml_XPath_XPathSorters4019574815MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_BaseIterator2454437973MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ListIterator3804705054MethodDeclarations.h"
#include "System.Xml_ArrayTypes.h"
#include "System_Xml_System_Xml_XPath_ListIterator3804705054.h"
#include "System_Xml_System_Xml_XQueryConvert3510797773.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "System_Xml_System_Xml_Xsl_XsltContext2013960098.h"
#include "System_Xml_System_Xml_Xsl_XsltContext2013960098MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XPath.XPathFunctionTranslate::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1503722168_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2932583186;
extern const uint32_t XPathFunctionTranslate__ctor_m1312115182_MetadataUsageId;
extern "C"  void XPathFunctionTranslate__ctor_m1312115182 (XPathFunctionTranslate_t2720664953 * __this, FunctionArguments_t2900945452 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate__ctor_m1312115182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2900945452 * L_6 = FunctionArguments_get_Tail_m3947258239(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t2900945452 * L_8 = FunctionArguments_get_Tail_m3947258239(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t2900945452 * L_9 = FunctionArguments_get_Tail_m3947258239(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t2900945452 * L_10 = FunctionArguments_get_Tail_m3947258239(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t1503722168 * L_11 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_11, _stringLiteral2932583186, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t2900945452 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t1283317256 * L_13 = FunctionArguments_get_Arg_m1790516491(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t2900945452 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t2900945452 * L_15 = FunctionArguments_get_Tail_m3947258239(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t1283317256 * L_16 = FunctionArguments_get_Arg_m1790516491(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t2900945452 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t2900945452 * L_18 = FunctionArguments_get_Tail_m3947258239(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t2900945452 * L_19 = FunctionArguments_get_Tail_m3947258239(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Expression_t1283317256 * L_20 = FunctionArguments_get_Arg_m1790516491(L_19, /*hidden argument*/NULL);
		__this->set_arg2_2(L_20);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionTranslate::get_ReturnType()
extern "C"  int32_t XPathFunctionTranslate_get_ReturnType_m933147468 (XPathFunctionTranslate_t2720664953 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTranslate::get_Peer()
extern "C"  bool XPathFunctionTranslate_get_Peer_m3798702666 (XPathFunctionTranslate_t2720664953 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t1283317256 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t1283317256 * L_4 = __this->get_arg2_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B4_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B4_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTranslate::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionTranslate_Evaluate_m944986293_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionTranslate_Evaluate_m944986293 (XPathFunctionTranslate_t2720664953 * __this, BaseIterator_t2454437973 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_Evaluate_m944986293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t1221177846 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t1283317256 * L_3 = __this->get_arg1_1();
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		Expression_t1283317256 * L_6 = __this->get_arg2_2();
		BaseIterator_t2454437973 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		V_2 = L_8;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1606060069(L_9, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_11 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		V_4 = 0;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1606060069(L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		V_6 = L_15;
		goto IL_0095;
	}

IL_004b:
	{
		String_t* L_16 = V_1;
		String_t* L_17 = V_0;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		Il2CppChar L_19 = String_get_Chars_m4230566705(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_20 = String_IndexOf_m2358239236(L_16, L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		int32_t L_21 = V_7;
		if ((((int32_t)L_21) == ((int32_t)(-1))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_22 = V_7;
		int32_t L_23 = V_6;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_007b;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_3;
		String_t* L_25 = V_2;
		int32_t L_26 = V_7;
		NullCheck(L_25);
		Il2CppChar L_27 = String_get_Chars_m4230566705(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m3618697540(L_24, L_27, /*hidden argument*/NULL);
	}

IL_007b:
	{
		goto IL_008f;
	}

IL_0080:
	{
		StringBuilder_t1221177846 * L_28 = V_3;
		String_t* L_29 = V_0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		Il2CppChar L_31 = String_get_Chars_m4230566705(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		StringBuilder_Append_m3618697540(L_28, L_31, /*hidden argument*/NULL);
	}

IL_008f:
	{
		int32_t L_32 = V_4;
		V_4 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0095:
	{
		int32_t L_33 = V_4;
		int32_t L_34 = V_5;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_004b;
		}
	}
	{
		StringBuilder_t1221177846 * L_35 = V_3;
		NullCheck(L_35);
		String_t* L_36 = StringBuilder_ToString_m1507807375(L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.String System.Xml.XPath.XPathFunctionTranslate::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1827829842;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t XPathFunctionTranslate_ToString_m1307664662_MetadataUsageId;
extern "C"  String_t* XPathFunctionTranslate_ToString_m1307664662 (XPathFunctionTranslate_t2720664953 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_ToString_m1307664662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1827829842);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1827829842);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Expression_t1283317256 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029314);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_9 = L_8;
		Expression_t1283317256 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029317);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTrue::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1503722168_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2583897385;
extern const uint32_t XPathFunctionTrue__ctor_m3471779148_MetadataUsageId;
extern "C"  void XPathFunctionTrue__ctor_m3471779148 (XPathFunctionTrue_t1672815575 * __this, FunctionArguments_t2900945452 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue__ctor_m3471779148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m620012104(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1503722168 * L_2 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_2, _stringLiteral2583897385, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_HasStaticValue()
extern "C"  bool XPathFunctionTrue_get_HasStaticValue_m3813180071 (XPathFunctionTrue_t1672815575 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionTrue_get_StaticValueAsBoolean_m1308627295 (XPathFunctionTrue_t1672815575 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_Peer()
extern "C"  bool XPathFunctionTrue_get_Peer_m2350064100 (XPathFunctionTrue_t1672815575 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTrue::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionTrue_Evaluate_m164285465_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionTrue_Evaluate_m164285465 (XPathFunctionTrue_t1672815575 * __this, BaseIterator_t2454437973 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_Evaluate_m164285465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((bool)1);
		Il2CppObject * L_1 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.XPathFunctionTrue::ToString()
extern Il2CppCodeGenString* _stringLiteral2078431091;
extern const uint32_t XPathFunctionTrue_ToString_m1131923630_MetadataUsageId;
extern "C"  String_t* XPathFunctionTrue_ToString_m1131923630 (XPathFunctionTrue_t1672815575 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_ToString_m1131923630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral2078431091;
	}
}
// System.Void System.Xml.XPath.XPathItem::.ctor()
extern "C"  void XPathItem__ctor_m341749466 (XPathItem_t3130801258 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.ctor()
extern "C"  void XPathIteratorComparer__ctor_m2708098576 (XPathIteratorComparer_t2522471076 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.cctor()
extern Il2CppClass* XPathIteratorComparer_t2522471076_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer__cctor_m1438945887_MetadataUsageId;
extern "C"  void XPathIteratorComparer__cctor_m1438945887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer__cctor_m1438945887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XPathIteratorComparer_t2522471076 * L_0 = (XPathIteratorComparer_t2522471076 *)il2cpp_codegen_object_new(XPathIteratorComparer_t2522471076_il2cpp_TypeInfo_var);
		XPathIteratorComparer__ctor_m2708098576(L_0, /*hidden argument*/NULL);
		((XPathIteratorComparer_t2522471076_StaticFields*)XPathIteratorComparer_t2522471076_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Int32 System.Xml.XPath.XPathIteratorComparer::Compare(System.Object,System.Object)
extern Il2CppClass* XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer_Compare_m2647634021_MetadataUsageId;
extern "C"  int32_t XPathIteratorComparer_Compare_m2647634021 (XPathIteratorComparer_t2522471076 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer_Compare_m2647634021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNodeIterator_t3192332357 * V_0 = NULL;
	XPathNodeIterator_t3192332357 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNodeIterator_t3192332357 *)IsInstClass(L_0, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNodeIterator_t3192332357 *)IsInstClass(L_1, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var));
		XPathNodeIterator_t3192332357 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNodeIterator_t3192332357 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNodeIterator_t3192332357 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t3981235968 * L_5 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		XPathNodeIterator_t3192332357 * L_6 = V_1;
		NullCheck(L_6);
		XPathNavigator_t3981235968 * L_7 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		NullCheck(L_5);
		int32_t L_8 = VirtFuncInvoker1< int32_t, XPathNavigator_t3981235968 * >::Invoke(21 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_5, L_7);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0047;
	}

IL_0043:
	{
		return 0;
	}

IL_0045:
	{
		return (-1);
	}

IL_0047:
	{
		return 1;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.ctor()
extern "C"  void XPathNavigator__ctor_m3760155520 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		XPathItem__ctor_m341749466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.cctor()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305138____U24U24fieldU2D18_9_FieldInfo_var;
extern const uint32_t XPathNavigator__cctor_m1668901011_MetadataUsageId;
extern "C"  void XPathNavigator__cctor_m1668901011 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator__cctor_m1668901011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		CharU5BU5D_t1328083999* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)60));
		CharU5BU5D_t1328083999* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppChar)((int32_t)62));
		((XPathNavigator_t3981235968_StaticFields*)XPathNavigator_t3981235968_il2cpp_TypeInfo_var->static_fields)->set_escape_text_chars_0(L_2);
		CharU5BU5D_t1328083999* L_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305138____U24U24fieldU2D18_9_FieldInfo_var), /*hidden argument*/NULL);
		((XPathNavigator_t3981235968_StaticFields*)XPathNavigator_t3981235968_il2cpp_TypeInfo_var->static_fields)->set_escape_attr_chars_1(L_3);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator::System.ICloneable.Clone()
extern "C"  Il2CppObject * XPathNavigator_System_ICloneable_Clone_m3220185747 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes()
extern "C"  bool XPathNavigator_get_HasAttributes_m3776636292 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren()
extern "C"  bool XPathNavigator_get_HasChildren_m3105216106 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.String System.Xml.XPath.XPathNavigator::get_XmlLang()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1165419350;
extern Il2CppCodeGenString* _stringLiteral3939301371;
extern const uint32_t XPathNavigator_get_XmlLang_m1158727745_MetadataUsageId;
extern "C"  String_t* XPathNavigator_get_XmlLang_m1158727745 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_get_XmlLang_m1158727745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t3981235968 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	{
		XPathNavigator_t3981235968 * L_5 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		goto IL_002d;
	}

IL_002d:
	{
		XPathNavigator_t3981235968 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(26 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String) */, L_6, _stringLiteral1165419350, _stringLiteral3939301371);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		XPathNavigator_t3981235968 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		return L_9;
	}

IL_0049:
	{
		XPathNavigator_t3981235968 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_10);
		if (L_11)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_12;
	}
}
// System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator)
extern "C"  int32_t XPathNavigator_ComparePosition_m2001546875 (XPathNavigator_t3981235968 * __this, XPathNavigator_t3981235968 * ___nav0, const MethodInfo* method)
{
	XPathNavigator_t3981235968 * V_0 = NULL;
	XPathNavigator_t3981235968 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		XPathNavigator_t3981235968 * L_0 = ___nav0;
		bool L_1 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_0);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000e:
	{
		XPathNavigator_t3981235968 * L_2 = ___nav0;
		bool L_3 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(23 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, __this, L_2);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (int32_t)(0);
	}

IL_001c:
	{
		XPathNavigator_t3981235968 * L_4 = ___nav0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(23 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_4, __this);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		return (int32_t)(1);
	}

IL_002a:
	{
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_6;
		XPathNavigator_t3981235968 * L_7 = ___nav0;
		NullCheck(L_7);
		XPathNavigator_t3981235968 * L_8 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_7);
		V_1 = L_8;
		XPathNavigator_t3981235968 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(29 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_9);
		XPathNavigator_t3981235968 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(29 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_10);
		XPathNavigator_t3981235968 * L_11 = V_0;
		XPathNavigator_t3981235968 * L_12 = V_1;
		NullCheck(L_11);
		bool L_13 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0052;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0052:
	{
		XPathNavigator_t3981235968 * L_14 = V_0;
		NullCheck(L_14);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_14, __this);
		XPathNavigator_t3981235968 * L_15 = V_1;
		XPathNavigator_t3981235968 * L_16 = ___nav0;
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
		V_2 = 0;
		goto IL_006d;
	}

IL_0069:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006d:
	{
		XPathNavigator_t3981235968 * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_18);
		if (L_19)
		{
			goto IL_0069;
		}
	}
	{
		XPathNavigator_t3981235968 * L_20 = V_0;
		NullCheck(L_20);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_20, __this);
		V_3 = 0;
		goto IL_008b;
	}

IL_0087:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_008b:
	{
		XPathNavigator_t3981235968 * L_22 = V_1;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_22);
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		XPathNavigator_t3981235968 * L_24 = V_1;
		XPathNavigator_t3981235968 * L_25 = ___nav0;
		NullCheck(L_24);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_24, L_25);
		int32_t L_26 = V_2;
		V_4 = L_26;
		goto IL_00b3;
	}

IL_00a6:
	{
		XPathNavigator_t3981235968 * L_27 = V_0;
		NullCheck(L_27);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_27);
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00b3:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = V_3;
		if ((((int32_t)L_29) > ((int32_t)L_30)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_31 = V_3;
		V_5 = L_31;
		goto IL_00d0;
	}

IL_00c3:
	{
		XPathNavigator_t3981235968 * L_32 = V_1;
		NullCheck(L_32);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_32);
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33-(int32_t)1));
	}

IL_00d0:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_4;
		if ((((int32_t)L_34) > ((int32_t)L_35)))
		{
			goto IL_00c3;
		}
	}
	{
		goto IL_00f2;
	}

IL_00de:
	{
		XPathNavigator_t3981235968 * L_36 = V_0;
		NullCheck(L_36);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_36);
		XPathNavigator_t3981235968 * L_37 = V_1;
		NullCheck(L_37);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_37);
		int32_t L_38 = V_4;
		V_4 = ((int32_t)((int32_t)L_38-(int32_t)1));
	}

IL_00f2:
	{
		XPathNavigator_t3981235968 * L_39 = V_0;
		XPathNavigator_t3981235968 * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_39, L_40);
		if (!L_41)
		{
			goto IL_00de;
		}
	}
	{
		XPathNavigator_t3981235968 * L_42 = V_0;
		NullCheck(L_42);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_42, __this);
		int32_t L_43 = V_2;
		V_6 = L_43;
		goto IL_011b;
	}

IL_010e:
	{
		XPathNavigator_t3981235968 * L_44 = V_0;
		NullCheck(L_44);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_44);
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45-(int32_t)1));
	}

IL_011b:
	{
		int32_t L_46 = V_6;
		int32_t L_47 = V_4;
		if ((((int32_t)L_46) > ((int32_t)((int32_t)((int32_t)L_47+(int32_t)1)))))
		{
			goto IL_010e;
		}
	}
	{
		XPathNavigator_t3981235968 * L_48 = V_1;
		XPathNavigator_t3981235968 * L_49 = ___nav0;
		NullCheck(L_48);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_48, L_49);
		int32_t L_50 = V_3;
		V_7 = L_50;
		goto IL_0143;
	}

IL_0136:
	{
		XPathNavigator_t3981235968 * L_51 = V_1;
		NullCheck(L_51);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_51);
		int32_t L_52 = V_7;
		V_7 = ((int32_t)((int32_t)L_52-(int32_t)1));
	}

IL_0143:
	{
		int32_t L_53 = V_7;
		int32_t L_54 = V_4;
		if ((((int32_t)L_53) > ((int32_t)((int32_t)((int32_t)L_54+(int32_t)1)))))
		{
			goto IL_0136;
		}
	}
	{
		XPathNavigator_t3981235968 * L_55 = V_0;
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_55);
		if ((!(((uint32_t)L_56) == ((uint32_t)3))))
		{
			goto IL_0188;
		}
	}
	{
		XPathNavigator_t3981235968 * L_57 = V_1;
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_57);
		if ((((int32_t)L_58) == ((int32_t)3)))
		{
			goto IL_0168;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0168:
	{
		goto IL_017b;
	}

IL_016d:
	{
		XPathNavigator_t3981235968 * L_59 = V_0;
		XPathNavigator_t3981235968 * L_60 = V_1;
		NullCheck(L_59);
		bool L_61 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_59, L_60);
		if (!L_61)
		{
			goto IL_017b;
		}
	}
	{
		return (int32_t)(0);
	}

IL_017b:
	{
		XPathNavigator_t3981235968 * L_62 = V_0;
		NullCheck(L_62);
		bool L_63 = XPathNavigator_MoveToNextNamespace_m3661588194(L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_016d;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0188:
	{
		XPathNavigator_t3981235968 * L_64 = V_1;
		NullCheck(L_64);
		int32_t L_65 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_64);
		if ((!(((uint32_t)L_65) == ((uint32_t)3))))
		{
			goto IL_0196;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0196:
	{
		XPathNavigator_t3981235968 * L_66 = V_0;
		NullCheck(L_66);
		int32_t L_67 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_66);
		if ((!(((uint32_t)L_67) == ((uint32_t)2))))
		{
			goto IL_01d0;
		}
	}
	{
		XPathNavigator_t3981235968 * L_68 = V_1;
		NullCheck(L_68);
		int32_t L_69 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_68);
		if ((((int32_t)L_69) == ((int32_t)2)))
		{
			goto IL_01b0;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01b0:
	{
		goto IL_01c3;
	}

IL_01b5:
	{
		XPathNavigator_t3981235968 * L_70 = V_0;
		XPathNavigator_t3981235968 * L_71 = V_1;
		NullCheck(L_70);
		bool L_72 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_70, L_71);
		if (!L_72)
		{
			goto IL_01c3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01c3:
	{
		XPathNavigator_t3981235968 * L_73 = V_0;
		NullCheck(L_73);
		bool L_74 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_73);
		if (L_74)
		{
			goto IL_01b5;
		}
	}
	{
		return (int32_t)(1);
	}

IL_01d0:
	{
		goto IL_01e3;
	}

IL_01d5:
	{
		XPathNavigator_t3981235968 * L_75 = V_0;
		XPathNavigator_t3981235968 * L_76 = V_1;
		NullCheck(L_75);
		bool L_77 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_75, L_76);
		if (!L_77)
		{
			goto IL_01e3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01e3:
	{
		XPathNavigator_t3981235968 * L_78 = V_0;
		NullCheck(L_78);
		bool L_79 = VirtFuncInvoker0< bool >::Invoke(34 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_78);
		if (L_79)
		{
			goto IL_01d5;
		}
	}
	{
		return (int32_t)(1);
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathNavigator::Compile(System.String)
extern "C"  XPathExpression_t452251917 * XPathNavigator_Compile_m2646144820 (XPathNavigator_t3981235968 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t452251917 * L_1 = XPathExpression_Compile_m2615308077(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern "C"  bool XPathNavigator_IsDescendant_m2690514350 (XPathNavigator_t3981235968 * __this, XPathNavigator_t3981235968 * ___nav0, const MethodInfo* method)
{
	{
		XPathNavigator_t3981235968 * L_0 = ___nav0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		XPathNavigator_t3981235968 * L_1 = ___nav0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		___nav0 = L_2;
		goto IL_0021;
	}

IL_0013:
	{
		XPathNavigator_t3981235968 * L_3 = ___nav0;
		bool L_4 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_3);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		XPathNavigator_t3981235968 * L_5 = ___nav0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		if (L_6)
		{
			goto IL_0013;
		}
	}

IL_002c:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_MoveToAttribute_m3716628652_MetadataUsageId;
extern "C"  bool XPathNavigator_MoveToAttribute_m3716628652 (XPathNavigator_t3981235968 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToAttribute_m3716628652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (!L_0)
		{
			goto IL_0041;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, __this);
		String_t* L_5 = ___namespaceURI1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		return (bool)1;
	}

IL_002f:
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, __this);
		if (L_7)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_MoveToNamespace_m419780955_MetadataUsageId;
extern "C"  bool XPathNavigator_MoveToNamespace_m419780955 (XPathNavigator_t3981235968 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToNamespace_m419780955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = XPathNavigator_MoveToFirstNamespace_m3677738759(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)1;
	}

IL_001e:
	{
		bool L_4 = XPathNavigator_MoveToNextNamespace_m3661588194(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst()
extern "C"  bool XPathNavigator_MoveToFirst_m2987745696 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		bool L_0 = XPathNavigator_MoveToFirstImpl_m1135523502(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::MoveToRoot()
extern "C"  void XPathNavigator_MoveToRoot_m3740159050 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstImpl()
extern "C"  bool XPathNavigator_MoveToFirstImpl_m1135523502 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, __this);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_001c;
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace()
extern "C"  bool XPathNavigator_MoveToFirstNamespace_m3677738759 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace()
extern "C"  bool XPathNavigator_MoveToNextNamespace_m3661588194 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(36 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression)
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_Select_m65000375 (XPathNavigator_t3981235968 * __this, XPathExpression_t452251917 * ___expr0, const MethodInfo* method)
{
	{
		XPathExpression_t452251917 * L_0 = ___expr0;
		XPathNodeIterator_t3192332357 * L_1 = XPathNavigator_Select_m2045522580(__this, L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression,System.Xml.IXmlNamespaceResolver)
extern Il2CppClass* CompiledExpression_t3686330919_il2cpp_TypeInfo_var;
extern Il2CppClass* NullIterator_t2539636145_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_Select_m2045522580_MetadataUsageId;
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_Select_m2045522580 (XPathNavigator_t3981235968 * __this, XPathExpression_t452251917 * ___expr0, Il2CppObject * ___ctx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_Select_m2045522580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CompiledExpression_t3686330919 * V_0 = NULL;
	BaseIterator_t2454437973 * V_1 = NULL;
	{
		XPathExpression_t452251917 * L_0 = ___expr0;
		V_0 = ((CompiledExpression_t3686330919 *)CastclassClass(L_0, CompiledExpression_t3686330919_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___ctx1;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		CompiledExpression_t3686330919 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = CompiledExpression_get_NamespaceManager_m4093303772(L_2, /*hidden argument*/NULL);
		___ctx1 = L_3;
	}

IL_0015:
	{
		Il2CppObject * L_4 = ___ctx1;
		NullIterator_t2539636145 * L_5 = (NullIterator_t2539636145 *)il2cpp_codegen_object_new(NullIterator_t2539636145_il2cpp_TypeInfo_var);
		NullIterator__ctor_m2816370923(L_5, __this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		CompiledExpression_t3686330919 * L_6 = V_0;
		BaseIterator_t2454437973 * L_7 = V_1;
		NullCheck(L_6);
		XPathNodeIterator_t3192332357 * L_8 = CompiledExpression_EvaluateNodeSet_m4253810175(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator::EnumerateChildren(System.Xml.XPath.XPathNavigator,System.Xml.XPath.XPathNodeType)
extern Il2CppClass* U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_EnumerateChildren_m1107095336_MetadataUsageId;
extern "C"  Il2CppObject * XPathNavigator_EnumerateChildren_m1107095336 (Il2CppObject * __this /* static, unused */, XPathNavigator_t3981235968 * ___n0, int32_t ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_EnumerateChildren_m1107095336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * V_0 = NULL;
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_0 = (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_1 = V_0;
		XPathNavigator_t3981235968 * L_2 = ___n0;
		NullCheck(L_1);
		L_1->set_n_0(L_2);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_3 = V_0;
		int32_t L_4 = ___type1;
		NullCheck(L_3);
		L_3->set_type_3(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_5 = V_0;
		XPathNavigator_t3981235968 * L_6 = ___n0;
		NullCheck(L_5);
		L_5->set_U3CU24U3En_6(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_7 = V_0;
		int32_t L_8 = ___type1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_7(L_8);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_9 = V_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType)
extern Il2CppClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern Il2CppClass* EnumerableIterator_t1602910416_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2718786873_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_SelectChildren_m4089331250_MetadataUsageId;
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_SelectChildren_m4089331250 (XPathNavigator_t3981235968 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_SelectChildren_m4089331250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t3981235968_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = XPathNavigator_EnumerateChildren_m1107095336(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		EnumerableIterator_t1602910416 * L_2 = (EnumerableIterator_t1602910416 *)il2cpp_codegen_object_new(EnumerableIterator_t1602910416_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m970393097(L_2, L_1, 0, /*hidden argument*/NULL);
		WrapperIterator_t2718786873 * L_3 = (WrapperIterator_t2718786873 *)il2cpp_codegen_object_new(WrapperIterator_t2718786873_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m904768110(L_3, L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathNavigator::ToString()
extern "C"  String_t* XPathNavigator_ToString_m921348895 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		return L_0;
	}
}
// System.String System.Xml.XPath.XPathNavigator::LookupNamespace(System.String)
extern "C"  String_t* XPathNavigator_LookupNamespace_m2791382698 (XPathNavigator_t3981235968 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	XPathNavigator_t3981235968 * V_0 = NULL;
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t3981235968 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigator_t3981235968 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
	}

IL_001a:
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		String_t* L_5 = ___prefix0;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		XPathNavigator_t3981235968 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_7);
		return L_8;
	}

IL_002d:
	{
		return (String_t*)NULL;
	}
}
// System.String System.Xml.XPath.XPathNavigator::LookupPrefix(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_LookupPrefix_m1628967057_MetadataUsageId;
extern "C"  String_t* XPathNavigator_LookupPrefix_m1628967057 (XPathNavigator_t3981235968 * __this, String_t* ___namespaceUri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_LookupPrefix_m1628967057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t3981235968 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigator_t3981235968 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
	}

IL_001a:
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = XPathNavigator_MoveToFirstNamespace_m3677738759(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0027:
	{
		XPathNavigator_t3981235968 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_6);
		String_t* L_8 = ___namespaceUri0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		XPathNavigator_t3981235968 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_10);
		return L_11;
	}

IL_003f:
	{
		XPathNavigator_t3981235968 * L_12 = V_0;
		NullCheck(L_12);
		bool L_13 = XPathNavigator_MoveToNextNamespace_m3661588194(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0027;
		}
	}
	{
		return (String_t*)NULL;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_ValueAsBoolean()
extern "C"  bool XPathNavigator_get_ValueAsBoolean_m3948643614 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		bool L_1 = XQueryConvert_StringToBoolean_m3693957332(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.DateTime System.Xml.XPath.XPathNavigator::get_ValueAsDateTime()
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_get_ValueAsDateTime_m3193226902_MetadataUsageId;
extern "C"  DateTime_t693205669  XPathNavigator_get_ValueAsDateTime_m3193226902 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_get_ValueAsDateTime_m3193226902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = XmlConvert_ToDateTime_m994963552(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathNavigator::get_ValueAsDouble()
extern "C"  double XPathNavigator_get_ValueAsDouble_m1670866478 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		double L_1 = XQueryConvert_StringToDouble_m3569604320(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigator::get_ValueAsInt()
extern "C"  int32_t XPathNavigator_get_ValueAsInt_m1159182917 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		int32_t L_1 = XQueryConvert_StringToInt_m994680325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Xml.XPath.XPathNavigator::get_ValueAsLong()
extern "C"  int64_t XPathNavigator_get_ValueAsLong_m3881570179 (XPathNavigator_t3981235968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		int64_t L_1 = XQueryConvert_StringToInteger_m2677814267(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::.ctor()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m621572829 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2876953237 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m2344063366 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246_MetadataUsageId;
extern "C"  Il2CppObject* U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_2 = (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_3 = V_0;
		XPathNavigator_t3981235968 * L_4 = __this->get_U3CU24U3En_6();
		NullCheck(L_3);
		L_3->set_n_0(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Etype_7();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::MoveNext()
extern "C"  bool U3CEnumerateChildrenU3Ec__Iterator0_MoveNext_m2851675985 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d4;
			}
		}
	}
	{
		goto IL_00eb;
	}

IL_0021:
	{
		XPathNavigator_t3981235968 * L_2 = __this->get_n_0();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_2);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_00eb;
	}

IL_0036:
	{
		XPathNavigator_t3981235968 * L_4 = __this->get_n_0();
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_4);
		XPathNavigator_t3981235968 * L_5 = __this->get_n_0();
		NullCheck(L_5);
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		__this->set_U3CnavU3E__0_1(L_6);
		XPathNavigator_t3981235968 * L_7 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_7);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_7);
		__this->set_U3Cnav2U3E__1_2((XPathNavigator_t3981235968 *)NULL);
	}

IL_0066:
	{
		int32_t L_8 = __this->get_type_3();
		if ((((int32_t)L_8) == ((int32_t)((int32_t)9))))
		{
			goto IL_0089;
		}
	}
	{
		XPathNavigator_t3981235968 * L_9 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_9);
		int32_t L_11 = __this->get_type_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_00d4;
		}
	}

IL_0089:
	{
		XPathNavigator_t3981235968 * L_12 = __this->get_U3Cnav2U3E__1_2();
		if (L_12)
		{
			goto IL_00aa;
		}
	}
	{
		XPathNavigator_t3981235968 * L_13 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_13);
		XPathNavigator_t3981235968 * L_14 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_13);
		__this->set_U3Cnav2U3E__1_2(L_14);
		goto IL_00bc;
	}

IL_00aa:
	{
		XPathNavigator_t3981235968 * L_15 = __this->get_U3Cnav2U3E__1_2();
		XPathNavigator_t3981235968 * L_16 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
	}

IL_00bc:
	{
		XPathNavigator_t3981235968 * L_17 = __this->get_U3Cnav2U3E__1_2();
		__this->set_U24current_5(L_17);
		__this->set_U24PC_4(1);
		goto IL_00ed;
	}

IL_00d4:
	{
		XPathNavigator_t3981235968 * L_18 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(34 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_18);
		if (L_19)
		{
			goto IL_0066;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00eb:
	{
		return (bool)0;
	}

IL_00ed:
	{
		return (bool)1;
	}
	// Dead block : IL_00ef: ldloc.1
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Dispose()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Dispose_m3853171810 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_Reset_m3144178776_MetadataUsageId;
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Reset_m3144178776 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_Reset_m3144178776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNavigator/EnumerableIterator::.ctor(System.Collections.IEnumerable,System.Int32)
extern "C"  void EnumerableIterator__ctor_m970393097 (EnumerableIterator_t1602910416 * __this, Il2CppObject * ___source0, int32_t ___pos1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		XPathNodeIterator__ctor_m2254908861(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___source0;
		__this->set_source_1(L_0);
		V_0 = 0;
		goto IL_001f;
	}

IL_0014:
	{
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext() */, __this);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_2 = V_0;
		int32_t L_3 = ___pos1;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator/EnumerableIterator::Clone()
extern Il2CppClass* EnumerableIterator_t1602910416_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_Clone_m4268561691_MetadataUsageId;
extern "C"  XPathNodeIterator_t3192332357 * EnumerableIterator_Clone_m4268561691 (EnumerableIterator_t1602910416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_Clone_m4268561691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_source_1();
		int32_t L_1 = __this->get_pos_3();
		EnumerableIterator_t1602910416 * L_2 = (EnumerableIterator_t1602910416 *)il2cpp_codegen_object_new(EnumerableIterator_t1602910416_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m970393097(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_MoveNext_m2512854753_MetadataUsageId;
extern "C"  bool EnumerableIterator_MoveNext_m2512854753 (EnumerableIterator_t1602910416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_MoveNext_m2512854753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_e_2();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_source_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		__this->set_e_2(L_2);
	}

IL_001c:
	{
		Il2CppObject * L_3 = __this->get_e_2();
		NullCheck(L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)0;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_pos_3();
		__this->set_pos_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return (bool)1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigator/EnumerableIterator::get_CurrentPosition()
extern "C"  int32_t EnumerableIterator_get_CurrentPosition_m3372306150 (EnumerableIterator_t1602910416 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_pos_3();
		return L_0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/EnumerableIterator::get_Current()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_get_Current_m1712718357_MetadataUsageId;
extern "C"  XPathNavigator_t3981235968 * EnumerableIterator_get_Current_m1712718357 (EnumerableIterator_t1602910416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_get_Current_m1712718357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_pos_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t3981235968 *)(NULL));
		goto IL_0021;
	}

IL_0011:
	{
		Il2CppObject * L_1 = __this->get_e_2();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((XPathNavigator_t3981235968 *)CastclassClass(L_2, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.ctor()
extern "C"  void XPathNavigatorComparer__ctor_m3253571973 (XPathNavigatorComparer_t2670709129 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.cctor()
extern Il2CppClass* XPathNavigatorComparer_t2670709129_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer__cctor_m240886484_MetadataUsageId;
extern "C"  void XPathNavigatorComparer__cctor_m240886484 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer__cctor_m240886484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XPathNavigatorComparer_t2670709129 * L_0 = (XPathNavigatorComparer_t2670709129 *)il2cpp_codegen_object_new(XPathNavigatorComparer_t2670709129_il2cpp_TypeInfo_var);
		XPathNavigatorComparer__ctor_m3253571973(L_0, /*hidden argument*/NULL);
		((XPathNavigatorComparer_t2670709129_StaticFields*)XPathNavigatorComparer_t2670709129_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern Il2CppClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m1841324898_MetadataUsageId;
extern "C"  bool XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m1841324898 (XPathNavigatorComparer_t2670709129 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m1841324898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	XPathNavigator_t3981235968 * V_1 = NULL;
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t3981235968 *)IsInstClass(L_0, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t3981235968 *)IsInstClass(L_1, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		XPathNavigator_t3981235968 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t3981235968 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		XPathNavigator_t3981235968 * L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		G_B4_0 = ((int32_t)(L_6));
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_GetHashCode_m2021225288 (XPathNavigatorComparer_t2670709129 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::Compare(System.Object,System.Object)
extern Il2CppClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer_Compare_m950798190_MetadataUsageId;
extern "C"  int32_t XPathNavigatorComparer_Compare_m950798190 (XPathNavigatorComparer_t2670709129 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_Compare_m950798190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	XPathNavigator_t3981235968 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t3981235968 *)IsInstClass(L_0, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t3981235968 *)IsInstClass(L_1, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		XPathNavigator_t3981235968 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNavigator_t3981235968 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		XPathNavigator_t3981235968 * L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, XPathNavigator_t3981235968 * >::Invoke(21 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		V_2 = L_6;
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_003d;
	}

IL_0039:
	{
		return 0;
	}

IL_003b:
	{
		return 1;
	}

IL_003d:
	{
		return (-1);
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator::.ctor()
extern "C"  void XPathNodeIterator__ctor_m2254908861 (XPathNodeIterator_t3192332357 * __this, const MethodInfo* method)
{
	{
		__this->set__count_0((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator::System.ICloneable.Clone()
extern "C"  Il2CppObject * XPathNodeIterator_System_ICloneable_Clone_m176700494 (XPathNodeIterator_t3192332357 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t3192332357 * L_0 = VirtFuncInvoker0< XPathNodeIterator_t3192332357 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		return L_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count()
extern "C"  int32_t XPathNodeIterator_get_Count_m1320026517 (XPathNodeIterator_t3192332357 * __this, const MethodInfo* method)
{
	XPathNodeIterator_t3192332357 * V_0 = NULL;
	{
		int32_t L_0 = __this->get__count_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		XPathNodeIterator_t3192332357 * L_1 = VirtFuncInvoker0< XPathNodeIterator_t3192332357 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		XPathNodeIterator_t3192332357 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_2);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		XPathNodeIterator_t3192332357 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_CurrentPosition() */, L_4);
		__this->set__count_0(L_5);
	}

IL_002f:
	{
		int32_t L_6 = __this->get__count_0();
		return L_6;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNodeIterator::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator2_t959253998_il2cpp_TypeInfo_var;
extern const uint32_t XPathNodeIterator_GetEnumerator_m1555209557_MetadataUsageId;
extern "C"  Il2CppObject * XPathNodeIterator_GetEnumerator_m1555209557 (XPathNodeIterator_t3192332357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNodeIterator_GetEnumerator_m1555209557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator2_t959253998 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator2_t959253998 * L_0 = (U3CGetEnumeratorU3Ec__Iterator2_t959253998 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator2_t959253998_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator2__ctor_m3138811673(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator2_t959253998 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CGetEnumeratorU3Ec__Iterator2_t959253998 * L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2__ctor_m3138811673 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m706015399 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2717083791 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator2_MoveNext_m2345571559 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0043;
			}
		}
	}
	{
		goto IL_005a;
	}

IL_0021:
	{
		goto IL_0043;
	}

IL_0026:
	{
		XPathNodeIterator_t3192332357 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		XPathNavigator_t3981235968 * L_3 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_2);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_005c;
	}

IL_0043:
	{
		XPathNodeIterator_t3192332357 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_4);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_005a:
	{
		return (bool)0;
	}

IL_005c:
	{
		return (bool)1;
	}
	// Dead block : IL_005e: ldloc.1
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Dispose_m1981482874 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator2_Reset_m3035138684_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Reset_m3035138684 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator2_Reset_m3035138684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNumericFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathNumericFunction__ctor_m2271660427 (XPathNumericFunction_t2367269690 * __this, FunctionArguments_t2900945452 * ___args0, const MethodInfo* method)
{
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathNumericFunction::get_ReturnType()
extern "C"  int32_t XPathNumericFunction_get_ReturnType_m980457913 (XPathNumericFunction_t2367269690 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Object System.Xml.XPath.XPathNumericFunction::get_StaticValue()
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t XPathNumericFunction_get_StaticValue_m766662601_MetadataUsageId;
extern "C"  Il2CppObject * XPathNumericFunction_get_StaticValue_m766662601 (XPathNumericFunction_t2367269690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNumericFunction_get_StaticValue_m766662601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathSortElement::.ctor()
extern "C"  void XPathSortElement__ctor_m4067665647 (XPathSortElement_t1040291071 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathSorter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Evaluate_m4282430526_MetadataUsageId;
extern "C"  Il2CppObject * XPathSorter_Evaluate_m4282430526 (XPathSorter_t3491953490 * __this, BaseIterator_t2454437973 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Evaluate_m4282430526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__type_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get__expr_0();
		BaseIterator_t2454437973 * L_2 = ___iter0;
		NullCheck(L_1);
		double L_3 = VirtFuncInvoker1< double, BaseIterator_t2454437973 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}

IL_001e:
	{
		Expression_t1283317256 * L_6 = __this->get__expr_0();
		BaseIterator_t2454437973 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		return L_8;
	}
}
// System.Int32 System.Xml.XPath.XPathSorter::Compare(System.Object,System.Object)
extern Il2CppClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Compare_m1708989355_MetadataUsageId;
extern "C"  int32_t XPathSorter_Compare_m1708989355 (XPathSorter_t3491953490 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Compare_m1708989355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__cmp_1();
		Il2CppObject * L_1 = ___o10;
		Il2CppObject * L_2 = ___o21;
		NullCheck(L_0);
		int32_t L_3 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Int32 System.Xml.XPath.XPathSorters::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* XPathSortElement_t1040291071_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSorter_t3491953490_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_System_Collections_IComparer_Compare_m845991242_MetadataUsageId;
extern "C"  int32_t XPathSorters_System_Collections_IComparer_Compare_m845991242 (XPathSorters_t4019574815 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_System_Collections_IComparer_Compare_m845991242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathSortElement_t1040291071 * V_0 = NULL;
	XPathSortElement_t1040291071 * V_1 = NULL;
	int32_t V_2 = 0;
	XPathSorter_t3491953490 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathSortElement_t1040291071 *)CastclassClass(L_0, XPathSortElement_t1040291071_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathSortElement_t1040291071 *)CastclassClass(L_1, XPathSortElement_t1040291071_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_004d;
	}

IL_0015:
	{
		ArrayList_t4252133567 * L_2 = __this->get__rgSorters_0();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_3 = ((XPathSorter_t3491953490 *)CastclassClass(L_4, XPathSorter_t3491953490_il2cpp_TypeInfo_var));
		XPathSorter_t3491953490 * L_5 = V_3;
		XPathSortElement_t1040291071 * L_6 = V_0;
		NullCheck(L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_6->get_Values_1();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		XPathSortElement_t1040291071 * L_11 = V_1;
		NullCheck(L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_11->get_Values_1();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_5);
		int32_t L_16 = XPathSorter_Compare_m1708989355(L_5, L_10, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		int32_t L_17 = V_4;
		if (!L_17)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_18 = V_4;
		return L_18;
	}

IL_0049:
	{
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_20 = V_2;
		ArrayList_t4252133567 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0015;
		}
	}
	{
		XPathSortElement_t1040291071 * L_23 = V_0;
		NullCheck(L_23);
		XPathNavigator_t3981235968 * L_24 = L_23->get_Navigator_0();
		XPathSortElement_t1040291071 * L_25 = V_1;
		NullCheck(L_25);
		XPathNavigator_t3981235968 * L_26 = L_25->get_Navigator_0();
		NullCheck(L_24);
		int32_t L_27 = VirtFuncInvoker1< int32_t, XPathNavigator_t3981235968 * >::Invoke(21 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_24, L_26);
		V_5 = L_27;
		int32_t L_28 = V_5;
		if ((((int32_t)L_28) == ((int32_t)1)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_29 = V_5;
		if ((((int32_t)L_29) == ((int32_t)2)))
		{
			goto IL_0086;
		}
	}
	{
		goto IL_008a;
	}

IL_0086:
	{
		return 0;
	}

IL_0088:
	{
		return 1;
	}

IL_008a:
	{
		return (-1);
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Xml.XPath.BaseIterator)
extern "C"  BaseIterator_t2454437973 * XPathSorters_Sort_m3354109990 (XPathSorters_t4019574815 * __this, BaseIterator_t2454437973 * ___iter0, const MethodInfo* method)
{
	ArrayList_t4252133567 * V_0 = NULL;
	{
		BaseIterator_t2454437973 * L_0 = ___iter0;
		ArrayList_t4252133567 * L_1 = XPathSorters_ToSortElementList_m407179934(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ArrayList_t4252133567 * L_2 = V_0;
		BaseIterator_t2454437973 * L_3 = ___iter0;
		NullCheck(L_3);
		Il2CppObject * L_4 = BaseIterator_get_NamespaceManager_m625352966(L_3, /*hidden argument*/NULL);
		BaseIterator_t2454437973 * L_5 = XPathSorters_Sort_m3271859196(__this, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.ArrayList System.Xml.XPath.XPathSorters::ToSortElementList(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSortElement_t1040291071_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSorter_t3491953490_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_ToSortElementList_m407179934_MetadataUsageId;
extern "C"  ArrayList_t4252133567 * XPathSorters_ToSortElementList_m407179934 (XPathSorters_t4019574815 * __this, BaseIterator_t2454437973 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_ToSortElementList_m407179934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t1040291071 * V_2 = NULL;
	int32_t V_3 = 0;
	XPathSorter_t3491953490 * V_4 = NULL;
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t4252133567 * L_1 = __this->get__rgSorters_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0081;
	}

IL_0017:
	{
		XPathSortElement_t1040291071 * L_3 = (XPathSortElement_t1040291071 *)il2cpp_codegen_object_new(XPathSortElement_t1040291071_il2cpp_TypeInfo_var);
		XPathSortElement__ctor_m4067665647(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		XPathSortElement_t1040291071 * L_4 = V_2;
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		XPathNavigator_t3981235968 * L_7 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(20 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		NullCheck(L_4);
		L_4->set_Navigator_0(L_7);
		XPathSortElement_t1040291071 * L_8 = V_2;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		L_8->set_Values_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)L_9)));
		V_3 = 0;
		goto IL_0068;
	}

IL_0041:
	{
		ArrayList_t4252133567 * L_10 = __this->get__rgSorters_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_10, L_11);
		V_4 = ((XPathSorter_t3491953490 *)CastclassClass(L_12, XPathSorter_t3491953490_il2cpp_TypeInfo_var));
		XPathSortElement_t1040291071 * L_13 = V_2;
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = L_13->get_Values_1();
		int32_t L_15 = V_3;
		XPathSorter_t3491953490 * L_16 = V_4;
		BaseIterator_t2454437973 * L_17 = ___iter0;
		NullCheck(L_16);
		Il2CppObject * L_18 = XPathSorter_Evaluate_m4282430526(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_18);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_20 = V_3;
		ArrayList_t4252133567 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0041;
		}
	}
	{
		ArrayList_t4252133567 * L_23 = V_0;
		XPathSortElement_t1040291071 * L_24 = V_2;
		NullCheck(L_23);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_23, L_24);
	}

IL_0081:
	{
		BaseIterator_t2454437973 * L_25 = ___iter0;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_25);
		if (L_26)
		{
			goto IL_0017;
		}
	}
	{
		ArrayList_t4252133567 * L_27 = V_0;
		return L_27;
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Collections.ArrayList,System.Xml.IXmlNamespaceResolver)
extern Il2CppClass* XPathNavigatorU5BU5D_t2529815809_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSortElement_t1040291071_il2cpp_TypeInfo_var;
extern Il2CppClass* ListIterator_t3804705054_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_Sort_m3271859196_MetadataUsageId;
extern "C"  BaseIterator_t2454437973 * XPathSorters_Sort_m3271859196 (XPathSorters_t4019574815 * __this, ArrayList_t4252133567 * ___rgElts0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_Sort_m3271859196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigatorU5BU5D_t2529815809* V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t1040291071 * V_2 = NULL;
	{
		ArrayList_t4252133567 * L_0 = ___rgElts0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(47 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_0, __this);
		ArrayList_t4252133567 * L_1 = ___rgElts0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_0 = ((XPathNavigatorU5BU5D_t2529815809*)SZArrayNew(XPathNavigatorU5BU5D_t2529815809_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_1 = 0;
		goto IL_0034;
	}

IL_001a:
	{
		ArrayList_t4252133567 * L_3 = ___rgElts0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		V_2 = ((XPathSortElement_t1040291071 *)CastclassClass(L_5, XPathSortElement_t1040291071_il2cpp_TypeInfo_var));
		XPathNavigatorU5BU5D_t2529815809* L_6 = V_0;
		int32_t L_7 = V_1;
		XPathSortElement_t1040291071 * L_8 = V_2;
		NullCheck(L_8);
		XPathNavigator_t3981235968 * L_9 = L_8->get_Navigator_0();
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (XPathNavigator_t3981235968 *)L_9);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_11 = V_1;
		ArrayList_t4252133567 * L_12 = ___rgElts0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigatorU5BU5D_t2529815809* L_14 = V_0;
		Il2CppObject * L_15 = ___nsm1;
		ListIterator_t3804705054 * L_16 = (ListIterator_t3804705054 *)il2cpp_codegen_object_new(ListIterator_t3804705054_il2cpp_TypeInfo_var);
		ListIterator__ctor_m1817971076(L_16, (Il2CppObject *)(Il2CppObject *)L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Double System.Xml.XQueryConvert::BooleanToDouble(System.Boolean)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_BooleanToDouble_m2811827158_MetadataUsageId;
extern "C"  double XQueryConvert_BooleanToDouble_m2811827158 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_BooleanToDouble_m2811827158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m204912726(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.XQueryConvert::BooleanToInt(System.Boolean)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_BooleanToInt_m3591054073_MetadataUsageId;
extern "C"  int32_t XQueryConvert_BooleanToInt_m3591054073 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_BooleanToInt_m3591054073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m55151042(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Xml.XQueryConvert::BooleanToInteger(System.Boolean)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_BooleanToInteger_m2975018119_MetadataUsageId;
extern "C"  int64_t XQueryConvert_BooleanToInteger_m2975018119 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_BooleanToInteger_m2975018119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int64_t L_1 = Convert_ToInt64_m1988035458(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Xml.XQueryConvert::BooleanToString(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern const uint32_t XQueryConvert_BooleanToString_m2795835286_MetadataUsageId;
extern "C"  String_t* XQueryConvert_BooleanToString_m2795835286 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_BooleanToString_m2795835286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral3323263070;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral2609877245;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.String System.Xml.XQueryConvert::DateTimeToString(System.DateTime)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DateTimeToString_m3375709472_MetadataUsageId;
extern "C"  String_t* XQueryConvert_DateTimeToString_m3375709472 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DateTimeToString_m3375709472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t693205669  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		String_t* L_1 = XmlConvert_ToString_m3100498468(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XQueryConvert::DecimalToBoolean(System.Decimal)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DecimalToBoolean_m4245554890_MetadataUsageId;
extern "C"  bool XQueryConvert_DecimalToBoolean_m4245554890 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DecimalToBoolean_m4245554890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Decimal_t724701077  L_0 = ___value0;
		Decimal_t724701077  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Decimal__ctor_m1010012873(&L_1, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_2 = Decimal_op_Inequality_m519758535(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Double System.Xml.XQueryConvert::DecimalToDouble(System.Decimal)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DecimalToDouble_m2103912516_MetadataUsageId;
extern "C"  double XQueryConvert_DecimalToDouble_m2103912516 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DecimalToDouble_m2103912516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Decimal_t724701077  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m1210655267(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.XQueryConvert::DecimalToInt(System.Decimal)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DecimalToInt_m2749508921_MetadataUsageId;
extern "C"  int32_t XQueryConvert_DecimalToInt_m2749508921 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DecimalToInt_m2749508921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Decimal_t724701077  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m2734833291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Xml.XQueryConvert::DecimalToInteger(System.Decimal)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DecimalToInteger_m1077984391_MetadataUsageId;
extern "C"  int64_t XQueryConvert_DecimalToInteger_m1077984391 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DecimalToInteger_m1077984391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Decimal_t724701077  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int64_t L_1 = Convert_ToInt64_m1722087499(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Xml.XQueryConvert::DecimalToString(System.Decimal)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DecimalToString_m170035652_MetadataUsageId;
extern "C"  String_t* XQueryConvert_DecimalToString_m170035652 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DecimalToString_m170035652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Decimal_t724701077  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		String_t* L_1 = XmlConvert_ToString_m3945094538(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XQueryConvert::DoubleToBoolean(System.Double)
extern "C"  bool XQueryConvert_DoubleToBoolean_m3660756116 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		return (bool)((((int32_t)((((double)L_0) == ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Xml.XQueryConvert::DoubleToInt(System.Double)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DoubleToInt_m4095163781_MetadataUsageId;
extern "C"  int32_t XQueryConvert_DoubleToInt_m4095163781 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DoubleToInt_m4095163781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m2988544165(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Xml.XQueryConvert::DoubleToInteger(System.Double)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DoubleToInteger_m2191907259_MetadataUsageId;
extern "C"  int64_t XQueryConvert_DoubleToInteger_m2191907259 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DoubleToInteger_m2191907259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int64_t L_1 = Convert_ToInt64_m338790373(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Xml.XQueryConvert::DoubleToString(System.Double)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_DoubleToString_m1707545184_MetadataUsageId;
extern "C"  String_t* XQueryConvert_DoubleToString_m1707545184 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_DoubleToString_m1707545184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		String_t* L_1 = XmlConvert_ToString_m1190359640(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XQueryConvert::FloatToBoolean(System.Single)
extern "C"  bool XQueryConvert_FloatToBoolean_m1454738262 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		return (bool)((((int32_t)((((float)L_0) == ((float)(0.0f)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Double System.Xml.XQueryConvert::FloatToDouble(System.Single)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_FloatToDouble_m2283094462_MetadataUsageId;
extern "C"  double XQueryConvert_FloatToDouble_m2283094462 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_FloatToDouble_m2283094462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m2956884076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.XQueryConvert::FloatToInt(System.Single)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_FloatToInt_m569557607_MetadataUsageId;
extern "C"  int32_t XQueryConvert_FloatToInt_m569557607 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_FloatToInt_m569557607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m403418852(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Xml.XQueryConvert::FloatToInteger(System.Single)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_FloatToInteger_m4188535837_MetadataUsageId;
extern "C"  int64_t XQueryConvert_FloatToInteger_m4188535837 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_FloatToInteger_m4188535837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int64_t L_1 = Convert_ToInt64_m991626788(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Xml.XQueryConvert::FloatToString(System.Single)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_FloatToString_m3138952446_MetadataUsageId;
extern "C"  String_t* XQueryConvert_FloatToString_m3138952446 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_FloatToString_m3138952446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		String_t* L_1 = XmlConvert_ToString_m2664053377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XQueryConvert::IntegerToBoolean(System.Int64)
extern "C"  bool XQueryConvert_IntegerToBoolean_m2636121103 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		return (bool)((((int32_t)((((int64_t)L_0) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Double System.Xml.XQueryConvert::IntegerToDouble(System.Int64)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_IntegerToDouble_m2074918543_MetadataUsageId;
extern "C"  double XQueryConvert_IntegerToDouble_m2074918543 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_IntegerToDouble_m2074918543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m2357261297(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.XQueryConvert::IntegerToInt(System.Int64)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_IntegerToInt_m4127635080_MetadataUsageId;
extern "C"  int32_t XQueryConvert_IntegerToInt_m4127635080 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_IntegerToInt_m4127635080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m3460593061(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Xml.XQueryConvert::IntegerToString(System.Int64)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_IntegerToString_m2834943183_MetadataUsageId;
extern "C"  String_t* XQueryConvert_IntegerToString_m2834943183 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_IntegerToString_m2834943183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		String_t* L_1 = XmlConvert_ToString_m3580968078(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XQueryConvert::IntToBoolean(System.Int32)
extern "C"  bool XQueryConvert_IntToBoolean_m3951992337 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Double System.Xml.XQueryConvert::IntToDouble(System.Int32)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_IntToDouble_m101819909_MetadataUsageId;
extern "C"  double XQueryConvert_IntToDouble_m101819909 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_IntToDouble_m101819909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m1550692470(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Xml.XQueryConvert::IntToString(System.Int32)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_IntToString_m628360325_MetadataUsageId;
extern "C"  String_t* XQueryConvert_IntToString_m628360325 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_IntToString_m628360325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		String_t* L_1 = XmlConvert_ToString_m267231745(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XQueryConvert::StringToBoolean(System.String)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_StringToBoolean_m3693957332_MetadataUsageId;
extern "C"  bool XQueryConvert_StringToBoolean_m3693957332 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_StringToBoolean_m3693957332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		bool L_1 = XmlConvert_ToBoolean_m1218506242(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.DateTime System.Xml.XQueryConvert::StringToDateTime(System.String)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_StringToDateTime_m1984107696_MetadataUsageId;
extern "C"  DateTime_t693205669  XQueryConvert_StringToDateTime_m1984107696 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_StringToDateTime_m1984107696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = XmlConvert_ToDateTime_m994963552(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Double System.Xml.XQueryConvert::StringToDouble(System.String)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_StringToDouble_m3569604320_MetadataUsageId;
extern "C"  double XQueryConvert_StringToDouble_m3569604320 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_StringToDouble_m3569604320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		double L_1 = XmlConvert_ToDouble_m720485344(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.XQueryConvert::StringToInt(System.String)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_StringToInt_m994680325_MetadataUsageId;
extern "C"  int32_t XQueryConvert_StringToInt_m994680325 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_StringToInt_m994680325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		int32_t L_1 = XmlConvert_ToInt32_m1775581778(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Xml.XQueryConvert::StringToInteger(System.String)
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern const uint32_t XQueryConvert_StringToInteger_m2677814267_MetadataUsageId;
extern "C"  int64_t XQueryConvert_StringToInteger_m2677814267 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XQueryConvert_StringToInteger_m2677814267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1936105738_il2cpp_TypeInfo_var);
		int64_t L_1 = XmlConvert_ToInt64_m4199079136(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName)
extern "C"  Il2CppObject * XsltContext_ResolveVariable_m2369667327 (XsltContext_t2013960098 * __this, XmlQualifiedName_t1944712516 * ___name0, const MethodInfo* method)
{
	{
		XmlQualifiedName_t1944712516 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m2734729190(L_0, /*hidden argument*/NULL);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(12 /* System.String System.Xml.XmlNamespaceManager::LookupPrefix(System.String) */, __this, L_1);
		XmlQualifiedName_t1944712516 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m4055250010(L_3, /*hidden argument*/NULL);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, String_t*, String_t* >::Invoke(17 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.String,System.String) */, __this, L_2, L_4);
		return L_5;
	}
}
// System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.XPathResultType[])
extern "C"  Il2CppObject * XsltContext_ResolveFunction_m50907708 (XsltContext_t2013960098 * __this, XmlQualifiedName_t1944712516 * ___name0, XPathResultTypeU5BU5D_t2966113519* ___argTypes1, const MethodInfo* method)
{
	{
		XmlQualifiedName_t1944712516 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Name_m4055250010(L_0, /*hidden argument*/NULL);
		XmlQualifiedName_t1944712516 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m2734729190(L_2, /*hidden argument*/NULL);
		XPathResultTypeU5BU5D_t2966113519* L_4 = ___argTypes1;
		Il2CppObject * L_5 = VirtFuncInvoker3< Il2CppObject *, String_t*, String_t*, XPathResultTypeU5BU5D_t2966113519* >::Invoke(16 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.String,System.String,System.Xml.XPath.XPathResultType[]) */, __this, L_1, L_3, L_4);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
