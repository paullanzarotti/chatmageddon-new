﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonServer/<SubmitMobilePin>c__AnonStorey4
struct  U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827  : public Il2CppObject
{
public:
	// System.Action`2<System.Boolean,System.String> ChatmageddonServer/<SubmitMobilePin>c__AnonStorey4::callBack
	Action_2_t1865222972 * ___callBack_0;

public:
	inline static int32_t get_offset_of_callBack_0() { return static_cast<int32_t>(offsetof(U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827, ___callBack_0)); }
	inline Action_2_t1865222972 * get_callBack_0() const { return ___callBack_0; }
	inline Action_2_t1865222972 ** get_address_of_callBack_0() { return &___callBack_0; }
	inline void set_callBack_0(Action_2_t1865222972 * value)
	{
		___callBack_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
