﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DataReceivedEventArgs
struct DataReceivedEventArgs_t3302745294;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Diagnostics.DataReceivedEventArgs::.ctor(System.String)
extern "C"  void DataReceivedEventArgs__ctor_m2782323061 (DataReceivedEventArgs_t3302745294 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DataReceivedEventArgs::get_Data()
extern "C"  String_t* DataReceivedEventArgs_get_Data_m2080165755 (DataReceivedEventArgs_t3302745294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
