﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UILabel>
struct List_1_t1164236560;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// RecursiveCharacterPopulator
struct RecursiveCharacterPopulator_t271140971;
// ItemInfoController/OnPopulationComplete
struct OnPopulationComplete_t1301655353;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemInfoController
struct  ItemInfoController_t4231956141  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ItemInfoController::titleSeperation
	int32_t ___titleSeperation_2;
	// System.Int32 ItemInfoController::contentSeperation
	int32_t ___contentSeperation_3;
	// System.Single ItemInfoController::timeBetweenSections
	float ___timeBetweenSections_4;
	// System.Boolean ItemInfoController::updateTitlePositions
	bool ___updateTitlePositions_5;
	// System.Boolean ItemInfoController::updateDescriptionPositions
	bool ___updateDescriptionPositions_6;
	// System.Boolean ItemInfoController::updateDescriptionXPos
	bool ___updateDescriptionXPos_7;
	// System.Collections.Generic.List`1<UILabel> ItemInfoController::titleLabels
	List_1_t1164236560 * ___titleLabels_8;
	// System.Collections.Generic.List`1<UILabel> ItemInfoController::descriptionLabels
	List_1_t1164236560 * ___descriptionLabels_9;
	// System.Collections.Generic.List`1<System.String> ItemInfoController::titleStrings
	List_1_t1398341365 * ___titleStrings_10;
	// System.Collections.Generic.List`1<System.String> ItemInfoController::descriptionStrings
	List_1_t1398341365 * ___descriptionStrings_11;
	// RecursiveCharacterPopulator ItemInfoController::populator
	RecursiveCharacterPopulator_t271140971 * ___populator_12;
	// ItemInfoController/OnPopulationComplete ItemInfoController::onPopulationComplete
	OnPopulationComplete_t1301655353 * ___onPopulationComplete_13;
	// UnityEngine.Coroutine ItemInfoController::waitingRoutine
	Coroutine_t2299508840 * ___waitingRoutine_14;
	// System.Boolean ItemInfoController::updatingLabels
	bool ___updatingLabels_15;
	// System.Int32 ItemInfoController::activeLabelNum
	int32_t ___activeLabelNum_16;
	// System.Boolean ItemInfoController::title
	bool ___title_17;

public:
	inline static int32_t get_offset_of_titleSeperation_2() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___titleSeperation_2)); }
	inline int32_t get_titleSeperation_2() const { return ___titleSeperation_2; }
	inline int32_t* get_address_of_titleSeperation_2() { return &___titleSeperation_2; }
	inline void set_titleSeperation_2(int32_t value)
	{
		___titleSeperation_2 = value;
	}

	inline static int32_t get_offset_of_contentSeperation_3() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___contentSeperation_3)); }
	inline int32_t get_contentSeperation_3() const { return ___contentSeperation_3; }
	inline int32_t* get_address_of_contentSeperation_3() { return &___contentSeperation_3; }
	inline void set_contentSeperation_3(int32_t value)
	{
		___contentSeperation_3 = value;
	}

	inline static int32_t get_offset_of_timeBetweenSections_4() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___timeBetweenSections_4)); }
	inline float get_timeBetweenSections_4() const { return ___timeBetweenSections_4; }
	inline float* get_address_of_timeBetweenSections_4() { return &___timeBetweenSections_4; }
	inline void set_timeBetweenSections_4(float value)
	{
		___timeBetweenSections_4 = value;
	}

	inline static int32_t get_offset_of_updateTitlePositions_5() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___updateTitlePositions_5)); }
	inline bool get_updateTitlePositions_5() const { return ___updateTitlePositions_5; }
	inline bool* get_address_of_updateTitlePositions_5() { return &___updateTitlePositions_5; }
	inline void set_updateTitlePositions_5(bool value)
	{
		___updateTitlePositions_5 = value;
	}

	inline static int32_t get_offset_of_updateDescriptionPositions_6() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___updateDescriptionPositions_6)); }
	inline bool get_updateDescriptionPositions_6() const { return ___updateDescriptionPositions_6; }
	inline bool* get_address_of_updateDescriptionPositions_6() { return &___updateDescriptionPositions_6; }
	inline void set_updateDescriptionPositions_6(bool value)
	{
		___updateDescriptionPositions_6 = value;
	}

	inline static int32_t get_offset_of_updateDescriptionXPos_7() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___updateDescriptionXPos_7)); }
	inline bool get_updateDescriptionXPos_7() const { return ___updateDescriptionXPos_7; }
	inline bool* get_address_of_updateDescriptionXPos_7() { return &___updateDescriptionXPos_7; }
	inline void set_updateDescriptionXPos_7(bool value)
	{
		___updateDescriptionXPos_7 = value;
	}

	inline static int32_t get_offset_of_titleLabels_8() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___titleLabels_8)); }
	inline List_1_t1164236560 * get_titleLabels_8() const { return ___titleLabels_8; }
	inline List_1_t1164236560 ** get_address_of_titleLabels_8() { return &___titleLabels_8; }
	inline void set_titleLabels_8(List_1_t1164236560 * value)
	{
		___titleLabels_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabels_8, value);
	}

	inline static int32_t get_offset_of_descriptionLabels_9() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___descriptionLabels_9)); }
	inline List_1_t1164236560 * get_descriptionLabels_9() const { return ___descriptionLabels_9; }
	inline List_1_t1164236560 ** get_address_of_descriptionLabels_9() { return &___descriptionLabels_9; }
	inline void set_descriptionLabels_9(List_1_t1164236560 * value)
	{
		___descriptionLabels_9 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionLabels_9, value);
	}

	inline static int32_t get_offset_of_titleStrings_10() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___titleStrings_10)); }
	inline List_1_t1398341365 * get_titleStrings_10() const { return ___titleStrings_10; }
	inline List_1_t1398341365 ** get_address_of_titleStrings_10() { return &___titleStrings_10; }
	inline void set_titleStrings_10(List_1_t1398341365 * value)
	{
		___titleStrings_10 = value;
		Il2CppCodeGenWriteBarrier(&___titleStrings_10, value);
	}

	inline static int32_t get_offset_of_descriptionStrings_11() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___descriptionStrings_11)); }
	inline List_1_t1398341365 * get_descriptionStrings_11() const { return ___descriptionStrings_11; }
	inline List_1_t1398341365 ** get_address_of_descriptionStrings_11() { return &___descriptionStrings_11; }
	inline void set_descriptionStrings_11(List_1_t1398341365 * value)
	{
		___descriptionStrings_11 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionStrings_11, value);
	}

	inline static int32_t get_offset_of_populator_12() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___populator_12)); }
	inline RecursiveCharacterPopulator_t271140971 * get_populator_12() const { return ___populator_12; }
	inline RecursiveCharacterPopulator_t271140971 ** get_address_of_populator_12() { return &___populator_12; }
	inline void set_populator_12(RecursiveCharacterPopulator_t271140971 * value)
	{
		___populator_12 = value;
		Il2CppCodeGenWriteBarrier(&___populator_12, value);
	}

	inline static int32_t get_offset_of_onPopulationComplete_13() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___onPopulationComplete_13)); }
	inline OnPopulationComplete_t1301655353 * get_onPopulationComplete_13() const { return ___onPopulationComplete_13; }
	inline OnPopulationComplete_t1301655353 ** get_address_of_onPopulationComplete_13() { return &___onPopulationComplete_13; }
	inline void set_onPopulationComplete_13(OnPopulationComplete_t1301655353 * value)
	{
		___onPopulationComplete_13 = value;
		Il2CppCodeGenWriteBarrier(&___onPopulationComplete_13, value);
	}

	inline static int32_t get_offset_of_waitingRoutine_14() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___waitingRoutine_14)); }
	inline Coroutine_t2299508840 * get_waitingRoutine_14() const { return ___waitingRoutine_14; }
	inline Coroutine_t2299508840 ** get_address_of_waitingRoutine_14() { return &___waitingRoutine_14; }
	inline void set_waitingRoutine_14(Coroutine_t2299508840 * value)
	{
		___waitingRoutine_14 = value;
		Il2CppCodeGenWriteBarrier(&___waitingRoutine_14, value);
	}

	inline static int32_t get_offset_of_updatingLabels_15() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___updatingLabels_15)); }
	inline bool get_updatingLabels_15() const { return ___updatingLabels_15; }
	inline bool* get_address_of_updatingLabels_15() { return &___updatingLabels_15; }
	inline void set_updatingLabels_15(bool value)
	{
		___updatingLabels_15 = value;
	}

	inline static int32_t get_offset_of_activeLabelNum_16() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___activeLabelNum_16)); }
	inline int32_t get_activeLabelNum_16() const { return ___activeLabelNum_16; }
	inline int32_t* get_address_of_activeLabelNum_16() { return &___activeLabelNum_16; }
	inline void set_activeLabelNum_16(int32_t value)
	{
		___activeLabelNum_16 = value;
	}

	inline static int32_t get_offset_of_title_17() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141, ___title_17)); }
	inline bool get_title_17() const { return ___title_17; }
	inline bool* get_address_of_title_17() { return &___title_17; }
	inline void set_title_17(bool value)
	{
		___title_17 = value;
	}
};

struct ItemInfoController_t4231956141_StaticFields
{
public:
	// System.Action`1<System.Boolean> ItemInfoController::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_18() { return static_cast<int32_t>(offsetof(ItemInfoController_t4231956141_StaticFields, ___U3CU3Ef__amU24cache0_18)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_18() const { return ___U3CU3Ef__amU24cache0_18; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_18() { return &___U3CU3Ef__amU24cache0_18; }
	inline void set_U3CU3Ef__amU24cache0_18(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
