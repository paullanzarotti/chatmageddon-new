﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen1080368425.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsILP
struct  FriendsILP_t207575760  : public BaseInfiniteListPopulator_1_t1080368425
{
public:
	// UILabel FriendsILP::noResultsLabel
	UILabel_t1795115428 * ___noResultsLabel_33;
	// System.Int32 FriendsILP::maxNameLength
	int32_t ___maxNameLength_34;
	// System.Boolean FriendsILP::listLoaded
	bool ___listLoaded_35;
	// System.Collections.ArrayList FriendsILP::friendsArray
	ArrayList_t4252133567 * ___friendsArray_36;
	// System.Boolean FriendsILP::outerFriendList
	bool ___outerFriendList_37;
	// System.Boolean FriendsILP::searchList
	bool ___searchList_38;
	// System.Boolean FriendsILP::globalSearchList
	bool ___globalSearchList_39;
	// UnityEngine.Color FriendsILP::activeColor
	Color_t2020392075  ___activeColor_40;
	// UnityEngine.Color FriendsILP::inactiveColor
	Color_t2020392075  ___inactiveColor_41;
	// System.Boolean FriendsILP::resetSearch
	bool ___resetSearch_42;

public:
	inline static int32_t get_offset_of_noResultsLabel_33() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___noResultsLabel_33)); }
	inline UILabel_t1795115428 * get_noResultsLabel_33() const { return ___noResultsLabel_33; }
	inline UILabel_t1795115428 ** get_address_of_noResultsLabel_33() { return &___noResultsLabel_33; }
	inline void set_noResultsLabel_33(UILabel_t1795115428 * value)
	{
		___noResultsLabel_33 = value;
		Il2CppCodeGenWriteBarrier(&___noResultsLabel_33, value);
	}

	inline static int32_t get_offset_of_maxNameLength_34() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___maxNameLength_34)); }
	inline int32_t get_maxNameLength_34() const { return ___maxNameLength_34; }
	inline int32_t* get_address_of_maxNameLength_34() { return &___maxNameLength_34; }
	inline void set_maxNameLength_34(int32_t value)
	{
		___maxNameLength_34 = value;
	}

	inline static int32_t get_offset_of_listLoaded_35() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___listLoaded_35)); }
	inline bool get_listLoaded_35() const { return ___listLoaded_35; }
	inline bool* get_address_of_listLoaded_35() { return &___listLoaded_35; }
	inline void set_listLoaded_35(bool value)
	{
		___listLoaded_35 = value;
	}

	inline static int32_t get_offset_of_friendsArray_36() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___friendsArray_36)); }
	inline ArrayList_t4252133567 * get_friendsArray_36() const { return ___friendsArray_36; }
	inline ArrayList_t4252133567 ** get_address_of_friendsArray_36() { return &___friendsArray_36; }
	inline void set_friendsArray_36(ArrayList_t4252133567 * value)
	{
		___friendsArray_36 = value;
		Il2CppCodeGenWriteBarrier(&___friendsArray_36, value);
	}

	inline static int32_t get_offset_of_outerFriendList_37() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___outerFriendList_37)); }
	inline bool get_outerFriendList_37() const { return ___outerFriendList_37; }
	inline bool* get_address_of_outerFriendList_37() { return &___outerFriendList_37; }
	inline void set_outerFriendList_37(bool value)
	{
		___outerFriendList_37 = value;
	}

	inline static int32_t get_offset_of_searchList_38() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___searchList_38)); }
	inline bool get_searchList_38() const { return ___searchList_38; }
	inline bool* get_address_of_searchList_38() { return &___searchList_38; }
	inline void set_searchList_38(bool value)
	{
		___searchList_38 = value;
	}

	inline static int32_t get_offset_of_globalSearchList_39() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___globalSearchList_39)); }
	inline bool get_globalSearchList_39() const { return ___globalSearchList_39; }
	inline bool* get_address_of_globalSearchList_39() { return &___globalSearchList_39; }
	inline void set_globalSearchList_39(bool value)
	{
		___globalSearchList_39 = value;
	}

	inline static int32_t get_offset_of_activeColor_40() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___activeColor_40)); }
	inline Color_t2020392075  get_activeColor_40() const { return ___activeColor_40; }
	inline Color_t2020392075 * get_address_of_activeColor_40() { return &___activeColor_40; }
	inline void set_activeColor_40(Color_t2020392075  value)
	{
		___activeColor_40 = value;
	}

	inline static int32_t get_offset_of_inactiveColor_41() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___inactiveColor_41)); }
	inline Color_t2020392075  get_inactiveColor_41() const { return ___inactiveColor_41; }
	inline Color_t2020392075 * get_address_of_inactiveColor_41() { return &___inactiveColor_41; }
	inline void set_inactiveColor_41(Color_t2020392075  value)
	{
		___inactiveColor_41 = value;
	}

	inline static int32_t get_offset_of_resetSearch_42() { return static_cast<int32_t>(offsetof(FriendsILP_t207575760, ___resetSearch_42)); }
	inline bool get_resetSearch_42() const { return ___resetSearch_42; }
	inline bool* get_address_of_resetSearch_42() { return &___resetSearch_42; }
	inline void set_resetSearch_42(bool value)
	{
		___resetSearch_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
