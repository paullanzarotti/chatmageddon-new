﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<SetTotalPoints>c__AnonStorey3D
struct U3CSetTotalPointsU3Ec__AnonStorey3D_t533271556;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<SetTotalPoints>c__AnonStorey3D::.ctor()
extern "C"  void U3CSetTotalPointsU3Ec__AnonStorey3D__ctor_m2541508633 (U3CSetTotalPointsU3Ec__AnonStorey3D_t533271556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<SetTotalPoints>c__AnonStorey3D::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CSetTotalPointsU3Ec__AnonStorey3D_U3CU3Em__0_m1584953372 (U3CSetTotalPointsU3Ec__AnonStorey3D_t533271556 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
