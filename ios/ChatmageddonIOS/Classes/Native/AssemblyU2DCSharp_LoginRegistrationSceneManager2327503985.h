﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen1706330337.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginRegistrationSceneManager
struct  LoginRegistrationSceneManager_t2327503985  : public BaseSceneManager_1_t1706330337
{
public:
	// UnityEngine.GameObject LoginRegistrationSceneManager::LoginBottomUI
	GameObject_t1756533147 * ___LoginBottomUI_17;
	// UnityEngine.GameObject LoginRegistrationSceneManager::LoginTopUI
	GameObject_t1756533147 * ___LoginTopUI_18;
	// UnityEngine.GameObject LoginRegistrationSceneManager::LoginCenterUI
	GameObject_t1756533147 * ___LoginCenterUI_19;
	// UnityEngine.GameObject LoginRegistrationSceneManager::DeviceCameraUI
	GameObject_t1756533147 * ___DeviceCameraUI_20;
	// UnityEngine.GameObject LoginRegistrationSceneManager::ConnectionPopUpUI
	GameObject_t1756533147 * ___ConnectionPopUpUI_21;
	// UnityEngine.GameObject LoginRegistrationSceneManager::ErrorMessageUI
	GameObject_t1756533147 * ___ErrorMessageUI_22;

public:
	inline static int32_t get_offset_of_LoginBottomUI_17() { return static_cast<int32_t>(offsetof(LoginRegistrationSceneManager_t2327503985, ___LoginBottomUI_17)); }
	inline GameObject_t1756533147 * get_LoginBottomUI_17() const { return ___LoginBottomUI_17; }
	inline GameObject_t1756533147 ** get_address_of_LoginBottomUI_17() { return &___LoginBottomUI_17; }
	inline void set_LoginBottomUI_17(GameObject_t1756533147 * value)
	{
		___LoginBottomUI_17 = value;
		Il2CppCodeGenWriteBarrier(&___LoginBottomUI_17, value);
	}

	inline static int32_t get_offset_of_LoginTopUI_18() { return static_cast<int32_t>(offsetof(LoginRegistrationSceneManager_t2327503985, ___LoginTopUI_18)); }
	inline GameObject_t1756533147 * get_LoginTopUI_18() const { return ___LoginTopUI_18; }
	inline GameObject_t1756533147 ** get_address_of_LoginTopUI_18() { return &___LoginTopUI_18; }
	inline void set_LoginTopUI_18(GameObject_t1756533147 * value)
	{
		___LoginTopUI_18 = value;
		Il2CppCodeGenWriteBarrier(&___LoginTopUI_18, value);
	}

	inline static int32_t get_offset_of_LoginCenterUI_19() { return static_cast<int32_t>(offsetof(LoginRegistrationSceneManager_t2327503985, ___LoginCenterUI_19)); }
	inline GameObject_t1756533147 * get_LoginCenterUI_19() const { return ___LoginCenterUI_19; }
	inline GameObject_t1756533147 ** get_address_of_LoginCenterUI_19() { return &___LoginCenterUI_19; }
	inline void set_LoginCenterUI_19(GameObject_t1756533147 * value)
	{
		___LoginCenterUI_19 = value;
		Il2CppCodeGenWriteBarrier(&___LoginCenterUI_19, value);
	}

	inline static int32_t get_offset_of_DeviceCameraUI_20() { return static_cast<int32_t>(offsetof(LoginRegistrationSceneManager_t2327503985, ___DeviceCameraUI_20)); }
	inline GameObject_t1756533147 * get_DeviceCameraUI_20() const { return ___DeviceCameraUI_20; }
	inline GameObject_t1756533147 ** get_address_of_DeviceCameraUI_20() { return &___DeviceCameraUI_20; }
	inline void set_DeviceCameraUI_20(GameObject_t1756533147 * value)
	{
		___DeviceCameraUI_20 = value;
		Il2CppCodeGenWriteBarrier(&___DeviceCameraUI_20, value);
	}

	inline static int32_t get_offset_of_ConnectionPopUpUI_21() { return static_cast<int32_t>(offsetof(LoginRegistrationSceneManager_t2327503985, ___ConnectionPopUpUI_21)); }
	inline GameObject_t1756533147 * get_ConnectionPopUpUI_21() const { return ___ConnectionPopUpUI_21; }
	inline GameObject_t1756533147 ** get_address_of_ConnectionPopUpUI_21() { return &___ConnectionPopUpUI_21; }
	inline void set_ConnectionPopUpUI_21(GameObject_t1756533147 * value)
	{
		___ConnectionPopUpUI_21 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionPopUpUI_21, value);
	}

	inline static int32_t get_offset_of_ErrorMessageUI_22() { return static_cast<int32_t>(offsetof(LoginRegistrationSceneManager_t2327503985, ___ErrorMessageUI_22)); }
	inline GameObject_t1756533147 * get_ErrorMessageUI_22() const { return ___ErrorMessageUI_22; }
	inline GameObject_t1756533147 ** get_address_of_ErrorMessageUI_22() { return &___ErrorMessageUI_22; }
	inline void set_ErrorMessageUI_22(GameObject_t1756533147 * value)
	{
		___ErrorMessageUI_22 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorMessageUI_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
