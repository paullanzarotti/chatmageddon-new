﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Decimal>
struct DefaultComparer_t2346631238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Decimal>::.ctor()
extern "C"  void DefaultComparer__ctor_m1093453484_gshared (DefaultComparer_t2346631238 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1093453484(__this, method) ((  void (*) (DefaultComparer_t2346631238 *, const MethodInfo*))DefaultComparer__ctor_m1093453484_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Decimal>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m501405223_gshared (DefaultComparer_t2346631238 * __this, Decimal_t724701077  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m501405223(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2346631238 *, Decimal_t724701077 , const MethodInfo*))DefaultComparer_GetHashCode_m501405223_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Decimal>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m281967735_gshared (DefaultComparer_t2346631238 * __this, Decimal_t724701077  ___x0, Decimal_t724701077  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m281967735(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2346631238 *, Decimal_t724701077 , Decimal_t724701077 , const MethodInfo*))DefaultComparer_Equals_m281967735_gshared)(__this, ___x0, ___y1, method)
