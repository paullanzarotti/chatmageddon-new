﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.ProductIdRemapper
struct  ProductIdRemapper_t3313438456  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Unibill.Impl.ProductIdRemapper::genericToPlatformSpecificIds
	Dictionary_2_t3943999495 * ___genericToPlatformSpecificIds_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Unibill.Impl.ProductIdRemapper::platformSpecificToGenericIds
	Dictionary_2_t3943999495 * ___platformSpecificToGenericIds_1;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.ProductIdRemapper::db
	UnibillConfiguration_t2915611853 * ___db_2;

public:
	inline static int32_t get_offset_of_genericToPlatformSpecificIds_0() { return static_cast<int32_t>(offsetof(ProductIdRemapper_t3313438456, ___genericToPlatformSpecificIds_0)); }
	inline Dictionary_2_t3943999495 * get_genericToPlatformSpecificIds_0() const { return ___genericToPlatformSpecificIds_0; }
	inline Dictionary_2_t3943999495 ** get_address_of_genericToPlatformSpecificIds_0() { return &___genericToPlatformSpecificIds_0; }
	inline void set_genericToPlatformSpecificIds_0(Dictionary_2_t3943999495 * value)
	{
		___genericToPlatformSpecificIds_0 = value;
		Il2CppCodeGenWriteBarrier(&___genericToPlatformSpecificIds_0, value);
	}

	inline static int32_t get_offset_of_platformSpecificToGenericIds_1() { return static_cast<int32_t>(offsetof(ProductIdRemapper_t3313438456, ___platformSpecificToGenericIds_1)); }
	inline Dictionary_2_t3943999495 * get_platformSpecificToGenericIds_1() const { return ___platformSpecificToGenericIds_1; }
	inline Dictionary_2_t3943999495 ** get_address_of_platformSpecificToGenericIds_1() { return &___platformSpecificToGenericIds_1; }
	inline void set_platformSpecificToGenericIds_1(Dictionary_2_t3943999495 * value)
	{
		___platformSpecificToGenericIds_1 = value;
		Il2CppCodeGenWriteBarrier(&___platformSpecificToGenericIds_1, value);
	}

	inline static int32_t get_offset_of_db_2() { return static_cast<int32_t>(offsetof(ProductIdRemapper_t3313438456, ___db_2)); }
	inline UnibillConfiguration_t2915611853 * get_db_2() const { return ___db_2; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_db_2() { return &___db_2; }
	inline void set_db_2(UnibillConfiguration_t2915611853 * value)
	{
		___db_2 = value;
		Il2CppCodeGenWriteBarrier(&___db_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
