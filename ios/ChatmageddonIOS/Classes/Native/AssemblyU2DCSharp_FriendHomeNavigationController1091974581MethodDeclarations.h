﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendHomeNavigationController
struct FriendHomeNavigationController_t1091974581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendHomeNavigationController::.ctor()
extern "C"  void FriendHomeNavigationController__ctor_m2828649196 (FriendHomeNavigationController_t1091974581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendHomeNavigationController::Start()
extern "C"  void FriendHomeNavigationController_Start_m342084212 (FriendHomeNavigationController_t1091974581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendHomeNavigationController::NavigateBackwards()
extern "C"  void FriendHomeNavigationController_NavigateBackwards_m127735075 (FriendHomeNavigationController_t1091974581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendHomeNavigationController::NavigateForwards(FriendHomeNavScreen)
extern "C"  void FriendHomeNavigationController_NavigateForwards_m1475377507 (FriendHomeNavigationController_t1091974581 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendHomeNavigationController::ValidateNavigation(FriendHomeNavScreen,NavigationDirection)
extern "C"  bool FriendHomeNavigationController_ValidateNavigation_m3365749535 (FriendHomeNavigationController_t1091974581 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendHomeNavigationController::LoadNavScreen(FriendHomeNavScreen,NavigationDirection,System.Boolean)
extern "C"  void FriendHomeNavigationController_LoadNavScreen_m418134807 (FriendHomeNavigationController_t1091974581 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendHomeNavigationController::UnloadNavScreen(FriendHomeNavScreen,NavigationDirection)
extern "C"  void FriendHomeNavigationController_UnloadNavScreen_m1412585891 (FriendHomeNavigationController_t1091974581 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendHomeNavigationController::PopulateTitleBar(FriendHomeNavScreen)
extern "C"  void FriendHomeNavigationController_PopulateTitleBar_m734933427 (FriendHomeNavigationController_t1091974581 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
