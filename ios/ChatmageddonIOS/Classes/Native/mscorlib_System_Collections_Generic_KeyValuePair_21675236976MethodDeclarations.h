﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22335466038MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1734086780(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1675236976 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m1535297478_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m3510273750(__this, method) ((  int32_t (*) (KeyValuePair_2_t1675236976 *, const MethodInfo*))KeyValuePair_2_get_Key_m3206954976_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m581630567(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1675236976 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3634558371_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m2744265790(__this, method) ((  String_t* (*) (KeyValuePair_2_t1675236976 *, const MethodInfo*))KeyValuePair_2_get_Value_m4245955624_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1886111263(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1675236976 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m241734939_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.String>::ToString()
#define KeyValuePair_2_ToString_m2542894605(__this, method) ((  String_t* (*) (KeyValuePair_2_t1675236976 *, const MethodInfo*))KeyValuePair_2_ToString_m191268585_gshared)(__this, method)
