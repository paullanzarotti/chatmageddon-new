﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissilePointsPopulator
struct MissilePointsPopulator_t2262625279;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void MissilePointsPopulator::.ctor()
extern "C"  void MissilePointsPopulator__ctor_m1272903882 (MissilePointsPopulator_t2262625279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissilePointsPopulator::SetPointsLabels(MissileTravelState,System.Single)
extern "C"  void MissilePointsPopulator_SetPointsLabels_m406791146 (MissilePointsPopulator_t2262625279 * __this, int32_t ___state0, float ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissilePointsPopulator::UpdateLabelColour(UnityEngine.Color)
extern "C"  void MissilePointsPopulator_UpdateLabelColour_m3180087047 (MissilePointsPopulator_t2262625279 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissilePointsPopulator::SetPointsActive(System.Boolean)
extern "C"  void MissilePointsPopulator_SetPointsActive_m1247066230 (MissilePointsPopulator_t2262625279 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
