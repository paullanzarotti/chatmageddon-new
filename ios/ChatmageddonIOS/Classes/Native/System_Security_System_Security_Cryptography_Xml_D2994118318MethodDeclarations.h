﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.DSAKeyValue
struct DSAKeyValue_t2994118318;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.DSAKeyValue::.ctor()
extern "C"  void DSAKeyValue__ctor_m673863968 (DSAKeyValue_t2994118318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.DSAKeyValue::GetXml()
extern "C"  XmlElement_t2877111883 * DSAKeyValue_GetXml_m827560331 (DSAKeyValue_t2994118318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.DSAKeyValue::LoadXml(System.Xml.XmlElement)
extern "C"  void DSAKeyValue_LoadXml_m3670491744 (DSAKeyValue_t2994118318 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
