﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenScale
struct TweenScale_t2697902175;
// UILabel
struct UILabel_t1795115428;
// NoPointsWaitButton
struct NoPointsWaitButton_t1249750581;

#include "AssemblyU2DCSharp_LockoutUI1337294023.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZeroPointsUI
struct  ZeroPointsUI_t1285366563  : public LockoutUI_t1337294023
{
public:
	// TweenAlpha ZeroPointsUI::backgroundTween
	TweenAlpha_t2421518635 * ___backgroundTween_3;
	// TweenScale ZeroPointsUI::contentTween
	TweenScale_t2697902175 * ___contentTween_4;
	// UILabel ZeroPointsUI::attackerNameLabel
	UILabel_t1795115428 * ___attackerNameLabel_5;
	// NoPointsWaitButton ZeroPointsUI::waitTimer
	NoPointsWaitButton_t1249750581 * ___waitTimer_6;

public:
	inline static int32_t get_offset_of_backgroundTween_3() { return static_cast<int32_t>(offsetof(ZeroPointsUI_t1285366563, ___backgroundTween_3)); }
	inline TweenAlpha_t2421518635 * get_backgroundTween_3() const { return ___backgroundTween_3; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundTween_3() { return &___backgroundTween_3; }
	inline void set_backgroundTween_3(TweenAlpha_t2421518635 * value)
	{
		___backgroundTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTween_3, value);
	}

	inline static int32_t get_offset_of_contentTween_4() { return static_cast<int32_t>(offsetof(ZeroPointsUI_t1285366563, ___contentTween_4)); }
	inline TweenScale_t2697902175 * get_contentTween_4() const { return ___contentTween_4; }
	inline TweenScale_t2697902175 ** get_address_of_contentTween_4() { return &___contentTween_4; }
	inline void set_contentTween_4(TweenScale_t2697902175 * value)
	{
		___contentTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___contentTween_4, value);
	}

	inline static int32_t get_offset_of_attackerNameLabel_5() { return static_cast<int32_t>(offsetof(ZeroPointsUI_t1285366563, ___attackerNameLabel_5)); }
	inline UILabel_t1795115428 * get_attackerNameLabel_5() const { return ___attackerNameLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_attackerNameLabel_5() { return &___attackerNameLabel_5; }
	inline void set_attackerNameLabel_5(UILabel_t1795115428 * value)
	{
		___attackerNameLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___attackerNameLabel_5, value);
	}

	inline static int32_t get_offset_of_waitTimer_6() { return static_cast<int32_t>(offsetof(ZeroPointsUI_t1285366563, ___waitTimer_6)); }
	inline NoPointsWaitButton_t1249750581 * get_waitTimer_6() const { return ___waitTimer_6; }
	inline NoPointsWaitButton_t1249750581 ** get_address_of_waitTimer_6() { return &___waitTimer_6; }
	inline void set_waitTimer_6(NoPointsWaitButton_t1249750581 * value)
	{
		___waitTimer_6 = value;
		Il2CppCodeGenWriteBarrier(&___waitTimer_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
