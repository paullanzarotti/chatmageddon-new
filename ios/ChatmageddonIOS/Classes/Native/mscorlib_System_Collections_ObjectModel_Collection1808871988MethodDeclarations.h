﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<SettingsNavScreen>
struct Collection_1_t1808871988;
// System.Collections.Generic.IList`1<SettingsNavScreen>
struct IList_1_t2808067835;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SettingsNavScreen[]
struct SettingsNavScreenU5BU5D_t655014135;
// System.Collections.Generic.IEnumerator`1<SettingsNavScreen>
struct IEnumerator_1_t4037618357;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"

// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m1505297933_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1505297933(__this, method) ((  void (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1__ctor_m1505297933_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m2054574848_gshared (Collection_1_t1808871988 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m2054574848(__this, ___list0, method) ((  void (*) (Collection_1_t1808871988 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m2054574848_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m830756746_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m830756746(__this, method) ((  bool (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m830756746_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2689023557_gshared (Collection_1_t1808871988 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2689023557(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1808871988 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2689023557_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3413343622_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3413343622(__this, method) ((  Il2CppObject * (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3413343622_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1223343297_gshared (Collection_1_t1808871988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1223343297(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1808871988 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1223343297_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3940511669_gshared (Collection_1_t1808871988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3940511669(__this, ___value0, method) ((  bool (*) (Collection_1_t1808871988 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3940511669_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1040464987_gshared (Collection_1_t1808871988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1040464987(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1808871988 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1040464987_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m911628080_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m911628080(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m911628080_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1520226578_gshared (Collection_1_t1808871988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1520226578(__this, ___value0, method) ((  void (*) (Collection_1_t1808871988 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1520226578_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1520590633_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1520590633(__this, method) ((  bool (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1520590633_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1786114917_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1786114917(__this, method) ((  Il2CppObject * (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1786114917_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4231727728_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4231727728(__this, method) ((  bool (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4231727728_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3407629117_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3407629117(__this, method) ((  bool (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3407629117_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m182093932_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m182093932(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m182093932_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2576370867_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2576370867(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2576370867_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m4151670108_gshared (Collection_1_t1808871988 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m4151670108(__this, ___item0, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_Add_m4151670108_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m922324032_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_Clear_m922324032(__this, method) ((  void (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_Clear_m922324032_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2299652042_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2299652042(__this, method) ((  void (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_ClearItems_m2299652042_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m2093756434_gshared (Collection_1_t1808871988 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2093756434(__this, ___item0, method) ((  bool (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_Contains_m2093756434_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1653716036_gshared (Collection_1_t1808871988 * __this, SettingsNavScreenU5BU5D_t655014135* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1653716036(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1808871988 *, SettingsNavScreenU5BU5D_t655014135*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1653716036_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m469975233_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m469975233(__this, method) ((  Il2CppObject* (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_GetEnumerator_m469975233_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3291081638_gshared (Collection_1_t1808871988 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3291081638(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m3291081638_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m545523411_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m545523411(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m545523411_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1168354446_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1168354446(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1168354446_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m1215598335_gshared (Collection_1_t1808871988 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1215598335(__this, ___item0, method) ((  bool (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_Remove_m1215598335_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1639626567_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1639626567(__this, ___index0, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1639626567_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m915665867_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m915665867(__this, ___index0, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m915665867_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m101257665_gshared (Collection_1_t1808871988 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m101257665(__this, method) ((  int32_t (*) (Collection_1_t1808871988 *, const MethodInfo*))Collection_1_get_Count_m101257665_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3804786735_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3804786735(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1808871988 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3804786735_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m411439078_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m411439078(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m411439078_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m691960227_gshared (Collection_1_t1808871988 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m691960227(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1808871988 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m691960227_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1000982528_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1000982528(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1000982528_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m2061953658_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2061953658(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2061953658_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1578456_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1578456(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1578456_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3351612914_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3351612914(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3351612914_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2880599203_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2880599203(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2880599203_gshared)(__this /* static, unused */, ___list0, method)
