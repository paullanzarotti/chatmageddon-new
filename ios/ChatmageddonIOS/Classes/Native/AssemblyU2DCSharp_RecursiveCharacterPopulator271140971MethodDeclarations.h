﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecursiveCharacterPopulator
struct RecursiveCharacterPopulator_t271140971;
// RecursiveCharacterPopulator/OnPopulationUpdate
struct OnPopulationUpdate_t30122867;
// RecursiveCharacterPopulator/OnPopulationComplete
struct OnPopulationComplete_t1952569739;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RecursiveCharacterPopulator_OnPopu30122867.h"
#include "AssemblyU2DCSharp_RecursiveCharacterPopulator_OnPo1952569739.h"
#include "AssemblyU2DCSharp_UILabel1795115428.h"
#include "mscorlib_System_String2029220233.h"

// System.Void RecursiveCharacterPopulator::.ctor()
extern "C"  void RecursiveCharacterPopulator__ctor_m1358460562 (RecursiveCharacterPopulator_t271140971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::add_onPopulationUpdate(RecursiveCharacterPopulator/OnPopulationUpdate)
extern "C"  void RecursiveCharacterPopulator_add_onPopulationUpdate_m194456562 (RecursiveCharacterPopulator_t271140971 * __this, OnPopulationUpdate_t30122867 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::remove_onPopulationUpdate(RecursiveCharacterPopulator/OnPopulationUpdate)
extern "C"  void RecursiveCharacterPopulator_remove_onPopulationUpdate_m1273379573 (RecursiveCharacterPopulator_t271140971 * __this, OnPopulationUpdate_t30122867 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::add_onPopulationComplete(RecursiveCharacterPopulator/OnPopulationComplete)
extern "C"  void RecursiveCharacterPopulator_add_onPopulationComplete_m3864539170 (RecursiveCharacterPopulator_t271140971 * __this, OnPopulationComplete_t1952569739 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::remove_onPopulationComplete(RecursiveCharacterPopulator/OnPopulationComplete)
extern "C"  void RecursiveCharacterPopulator_remove_onPopulationComplete_m2890860709 (RecursiveCharacterPopulator_t271140971 * __this, OnPopulationComplete_t1952569739 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::PopulateField(UILabel,System.String)
extern "C"  void RecursiveCharacterPopulator_PopulateField_m1903861546 (RecursiveCharacterPopulator_t271140971 * __this, UILabel_t1795115428 * ___textLabel0, String_t* ___textToUse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::StopPopulation()
extern "C"  void RecursiveCharacterPopulator_StopPopulation_m3553181833 (RecursiveCharacterPopulator_t271140971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RecursiveCharacterPopulator::TextPopulator(UILabel,System.String)
extern "C"  Il2CppObject * RecursiveCharacterPopulator_TextPopulator_m2995808947 (RecursiveCharacterPopulator_t271140971 * __this, UILabel_t1795115428 * ___label0, String_t* ___textToUse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::SendPopulationUpdateEvent()
extern "C"  void RecursiveCharacterPopulator_SendPopulationUpdateEvent_m2043429158 (RecursiveCharacterPopulator_t271140971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator::SendPopulationCompleteEvent()
extern "C"  void RecursiveCharacterPopulator_SendPopulationCompleteEvent_m4263497084 (RecursiveCharacterPopulator_t271140971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
