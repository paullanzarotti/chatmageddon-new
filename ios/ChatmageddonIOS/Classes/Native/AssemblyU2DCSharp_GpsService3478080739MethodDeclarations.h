﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GpsService
struct GpsService_t3478080739;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"

// System.Void GpsService::.ctor()
extern "C"  void GpsService__ctor_m4032397470 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GpsService::GpsStart()
extern "C"  void GpsService_GpsStart_m1673234188 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GpsService::GpsStop()
extern "C"  void GpsService_GpsStop_m2975076966 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GpsService::ToRad(System.Single)
extern "C"  float GpsService_ToRad_m3646777697 (GpsService_t3478080739 * __this, float ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GpsService::TrackDistance()
extern "C"  Il2CppObject * GpsService_TrackDistance_m1223186670 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GpsService::GpsDisabled(System.Boolean)
extern "C"  void GpsService_GpsDisabled_m3851809565 (GpsService_t3478080739 * __this, bool ___disabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GpsService::GpsFailed()
extern "C"  void GpsService_GpsFailed_m2858288697 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LocationInfo GpsService::GetStartLocation()
extern "C"  LocationInfo_t1364725149  GpsService_GetStartLocation_m1980662432 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LocationInfo GpsService::GetLastLocation()
extern "C"  LocationInfo_t1364725149  GpsService_GetLastLocation_m219321888 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GpsService::GetDistance()
extern "C"  float GpsService_GetDistance_m373099021 (GpsService_t3478080739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
