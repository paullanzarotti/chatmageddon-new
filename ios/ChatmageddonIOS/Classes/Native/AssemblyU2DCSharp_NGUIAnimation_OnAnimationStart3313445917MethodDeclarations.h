﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIAnimation/OnAnimationStart
struct OnAnimationStart_t3313445917;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void NGUIAnimation/OnAnimationStart::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAnimationStart__ctor_m3714922516 (OnAnimationStart_t3313445917 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationStart::Invoke()
extern "C"  void OnAnimationStart_Invoke_m1198320134 (OnAnimationStart_t3313445917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NGUIAnimation/OnAnimationStart::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAnimationStart_BeginInvoke_m379914035 (OnAnimationStart_t3313445917 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationStart::EndInvoke(System.IAsyncResult)
extern "C"  void OnAnimationStart_EndInvoke_m4125064050 (OnAnimationStart_t3313445917 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
