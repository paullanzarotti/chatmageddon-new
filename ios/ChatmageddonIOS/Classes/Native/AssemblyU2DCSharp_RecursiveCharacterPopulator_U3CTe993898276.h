﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;
// RecursiveCharacterPopulator
struct RecursiveCharacterPopulator_t271140971;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecursiveCharacterPopulator/<TextPopulator>c__Iterator0
struct  U3CTextPopulatorU3Ec__Iterator0_t993898276  : public Il2CppObject
{
public:
	// UILabel RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::label
	UILabel_t1795115428 * ___label_0;
	// System.String RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::<desiredText>__0
	String_t* ___U3CdesiredTextU3E__0_1;
	// System.String RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::textToUse
	String_t* ___textToUse_2;
	// System.Int32 RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::<charCount>__1
	int32_t ___U3CcharCountU3E__1_3;
	// System.String RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::<currentText>__2
	String_t* ___U3CcurrentTextU3E__2_4;
	// RecursiveCharacterPopulator RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::$this
	RecursiveCharacterPopulator_t271140971 * ___U24this_5;
	// System.Object RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___label_0)); }
	inline UILabel_t1795115428 * get_label_0() const { return ___label_0; }
	inline UILabel_t1795115428 ** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(UILabel_t1795115428 * value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier(&___label_0, value);
	}

	inline static int32_t get_offset_of_U3CdesiredTextU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U3CdesiredTextU3E__0_1)); }
	inline String_t* get_U3CdesiredTextU3E__0_1() const { return ___U3CdesiredTextU3E__0_1; }
	inline String_t** get_address_of_U3CdesiredTextU3E__0_1() { return &___U3CdesiredTextU3E__0_1; }
	inline void set_U3CdesiredTextU3E__0_1(String_t* value)
	{
		___U3CdesiredTextU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdesiredTextU3E__0_1, value);
	}

	inline static int32_t get_offset_of_textToUse_2() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___textToUse_2)); }
	inline String_t* get_textToUse_2() const { return ___textToUse_2; }
	inline String_t** get_address_of_textToUse_2() { return &___textToUse_2; }
	inline void set_textToUse_2(String_t* value)
	{
		___textToUse_2 = value;
		Il2CppCodeGenWriteBarrier(&___textToUse_2, value);
	}

	inline static int32_t get_offset_of_U3CcharCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U3CcharCountU3E__1_3)); }
	inline int32_t get_U3CcharCountU3E__1_3() const { return ___U3CcharCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharCountU3E__1_3() { return &___U3CcharCountU3E__1_3; }
	inline void set_U3CcharCountU3E__1_3(int32_t value)
	{
		___U3CcharCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTextU3E__2_4() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U3CcurrentTextU3E__2_4)); }
	inline String_t* get_U3CcurrentTextU3E__2_4() const { return ___U3CcurrentTextU3E__2_4; }
	inline String_t** get_address_of_U3CcurrentTextU3E__2_4() { return &___U3CcurrentTextU3E__2_4; }
	inline void set_U3CcurrentTextU3E__2_4(String_t* value)
	{
		___U3CcurrentTextU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentTextU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U24this_5)); }
	inline RecursiveCharacterPopulator_t271140971 * get_U24this_5() const { return ___U24this_5; }
	inline RecursiveCharacterPopulator_t271140971 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(RecursiveCharacterPopulator_t271140971 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTextPopulatorU3Ec__Iterator0_t993898276, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
