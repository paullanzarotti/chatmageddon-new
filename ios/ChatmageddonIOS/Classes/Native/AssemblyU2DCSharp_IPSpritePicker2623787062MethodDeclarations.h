﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPSpritePicker
struct IPSpritePicker_t2623787062;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IPSpritePicker::.ctor()
extern "C"  void IPSpritePicker__ctor_m1630577813 (IPSpritePicker_t2623787062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IPSpritePicker::get_CurrentSpriteName()
extern "C"  String_t* IPSpritePicker_get_CurrentSpriteName_m527121274 (IPSpritePicker_t2623787062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::set_CurrentSpriteName(System.String)
extern "C"  void IPSpritePicker_set_CurrentSpriteName_m3027789605 (IPSpritePicker_t2623787062 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::ResetPickerAtContentIndex(System.Int32)
extern "C"  void IPSpritePicker_ResetPickerAtContentIndex_m2717548041 (IPSpritePicker_t2623787062 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::InsertElement(System.String)
extern "C"  void IPSpritePicker_InsertElement_m224339360 (IPSpritePicker_t2623787062 * __this, String_t* ___spriteName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::InsertElementAtIndex(System.String,System.Int32,System.Boolean)
extern "C"  void IPSpritePicker_InsertElementAtIndex_m2584215311 (IPSpritePicker_t2623787062 * __this, String_t* ___spriteName0, int32_t ___index1, bool ___resetPicker2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::RemoveElementAtIndex(System.Int32,System.Boolean)
extern "C"  void IPSpritePicker_RemoveElementAtIndex_m4256639378 (IPSpritePicker_t2623787062 * __this, int32_t ___index0, bool ___resetPicker1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPSpritePicker::GetInitIndex()
extern "C"  int32_t IPSpritePicker_GetInitIndex_m476591285 (IPSpritePicker_t2623787062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::UpdateCurrentValue()
extern "C"  void IPSpritePicker_UpdateCurrentValue_m2470675278 (IPSpritePicker_t2623787062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::UpdateWidget(System.Int32,System.Int32)
extern "C"  void IPSpritePicker_UpdateWidget_m1557739280 (IPSpritePicker_t2623787062 * __this, int32_t ___widgetIndex0, int32_t ___contentIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPSpritePicker::UpdateVirtualElementsCount()
extern "C"  void IPSpritePicker_UpdateVirtualElementsCount_m2821003439 (IPSpritePicker_t2623787062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
