﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiImageNGUIAnimation
struct MultiImageNGUIAnimation_t45560503;

#include "codegen/il2cpp-codegen.h"

// System.Void MultiImageNGUIAnimation::.ctor()
extern "C"  void MultiImageNGUIAnimation__ctor_m3049218738 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::OnAnimStart()
extern "C"  void MultiImageNGUIAnimation_OnAnimStart_m3881121222 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::OnResetCurrents()
extern "C"  void MultiImageNGUIAnimation_OnResetCurrents_m3071856444 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::OnAnimFrameDraw()
extern "C"  void MultiImageNGUIAnimation_OnAnimFrameDraw_m2959139525 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::OnAnimUpdate()
extern "C"  void MultiImageNGUIAnimation_OnAnimUpdate_m2160812937 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::OnAnimLooped()
extern "C"  void MultiImageNGUIAnimation_OnAnimLooped_m1111030053 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::OnAnimFinished()
extern "C"  void MultiImageNGUIAnimation_OnAnimFinished_m428490846 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiImageNGUIAnimation::UpdateFrameImage()
extern "C"  void MultiImageNGUIAnimation_UpdateFrameImage_m3895477025 (MultiImageNGUIAnimation_t45560503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
