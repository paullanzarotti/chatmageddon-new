﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3974088187(__this, ___l0, method) ((  void (*) (Enumerator_t4224697774 *, List_1_t395000804 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m486686663(__this, method) ((  void (*) (Enumerator_t4224697774 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3811174731(__this, method) ((  Il2CppObject * (*) (Enumerator_t4224697774 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::Dispose()
#define Enumerator_Dispose_m1273461368(__this, method) ((  void (*) (Enumerator_t4224697774 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::VerifyState()
#define Enumerator_VerifyState_m2330468505(__this, method) ((  void (*) (Enumerator_t4224697774 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::MoveNext()
#define Enumerator_MoveNext_m1015045107(__this, method) ((  bool (*) (Enumerator_t4224697774 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<MineAudienceTab>::get_Current()
#define Enumerator_get_Current_m698865478(__this, method) ((  MineAudienceTab_t1025879672 * (*) (Enumerator_t4224697774 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
