﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2813595706.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4201946253.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4201208534.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecObj3901542888.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_3126647113.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T983939287.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T983863441.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T166682126.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T167037012.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1446647738.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1446858876.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_2284807557.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_2284875459.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1625039561.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1625250699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1017516731.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1017871617.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_4283766514.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_4284121400.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_2978504789.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Utilities_4013099873.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Utilities_3440239854.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Algor2670781410.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Basic3459049714.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlDi3073089489.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlNu1759393128.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlRe3883542487.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Certi2288802675.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_DsaPar274855281.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Digest202014884.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Distri769724552.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Distr1765286135.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Genera294965175.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Gener1290955016.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Issui3776707264.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_KeyUsa785552764.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_RsaPub197361851.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Reason677892991.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Subje3547422518.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlEn4200172927.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCer741385336.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCert64679979.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCe2937350180.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCe1391133771.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Time2566907995.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509C3705285294.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509De518486245.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509E2020524125.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509E1384530060.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509N2936077305.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509N1972691209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509N3449226918.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509Ob346896425.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_DHDomai3489907266.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_DHPubli3181392102.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_DHValid4010119324.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_ECNamed3889450962.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2357232190.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1386193459.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1385946192.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1385986417.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam4025924969.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam4025965190.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam4025717927.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Name299104776.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1991767207.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1991663432.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1991695209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3726533506.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2622364672.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2622117405.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2622157630.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3381865698.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3623772131.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3623731910.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3623700133.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3772636393.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3252909381.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3160589090.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3617495651.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2773896637.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Par1137405623.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Curve2327439944.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9ECPar1959966001.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9ECPar1566440261.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9ECPoi3031020555.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9FieldE706679609.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Field2012644846.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Integ1331659747.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Object773243058.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Asymmetr1825508216.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Asymmetr1663727050.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Buffered2590109294.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedA862890205.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedB711630611.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Buffered3241650255.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedI295552809.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedS702588694.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Check1143849288.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_CipherKe2313639529.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (Sect409r1Holder_t2813595706), -1, sizeof(Sect409r1Holder_t2813595706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3800[3] = 
{
	Sect409r1Holder_t2813595706_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (Sect571k1Holder_t4201946253), -1, sizeof(Sect571k1Holder_t4201946253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3801[5] = 
{
	Sect571k1Holder_t4201946253_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (Sect571r1Holder_t4201208534), -1, sizeof(Sect571r1Holder_t4201208534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3802[5] = 
{
	Sect571r1Holder_t4201208534_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (SecObjectIdentifiers_t3901542888), -1, sizeof(SecObjectIdentifiers_t3901542888_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3803[34] = 
{
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_EllipticCurve_0(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT163k1_1(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT163r1_2(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT239k1_3(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT113r1_4(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT113r2_5(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP112r1_6(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP112r2_7(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP160r1_8(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP160k1_9(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP256k1_10(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT163r2_11(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT283k1_12(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT283r1_13(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT131r1_14(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT131r2_15(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT193r1_16(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT193r2_17(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT233k1_18(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT233r1_19(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP128r1_20(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP128r2_21(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP160r2_22(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP192k1_23(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP224k1_24(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP224r1_25(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP384r1_26(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP521r1_27(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT409k1_28(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT409r1_29(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT571k1_30(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT571r1_31(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP192r1_32(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP256r1_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (TeleTrusTNamedCurves_t3126647113), -1, sizeof(TeleTrusTNamedCurves_t3126647113_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3804[3] = 
{
	TeleTrusTNamedCurves_t3126647113_StaticFields::get_offset_of_objIds_0(),
	TeleTrusTNamedCurves_t3126647113_StaticFields::get_offset_of_curves_1(),
	TeleTrusTNamedCurves_t3126647113_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (BrainpoolP160r1Holder_t983939287), -1, sizeof(BrainpoolP160r1Holder_t983939287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3805[1] = 
{
	BrainpoolP160r1Holder_t983939287_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (BrainpoolP160t1Holder_t983863441), -1, sizeof(BrainpoolP160t1Holder_t983863441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3806[1] = 
{
	BrainpoolP160t1Holder_t983863441_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (BrainpoolP192r1Holder_t166682126), -1, sizeof(BrainpoolP192r1Holder_t166682126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3807[1] = 
{
	BrainpoolP192r1Holder_t166682126_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (BrainpoolP192t1Holder_t167037012), -1, sizeof(BrainpoolP192t1Holder_t167037012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3808[1] = 
{
	BrainpoolP192t1Holder_t167037012_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (BrainpoolP224r1Holder_t1446647738), -1, sizeof(BrainpoolP224r1Holder_t1446647738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3809[1] = 
{
	BrainpoolP224r1Holder_t1446647738_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (BrainpoolP224t1Holder_t1446858876), -1, sizeof(BrainpoolP224t1Holder_t1446858876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3810[1] = 
{
	BrainpoolP224t1Holder_t1446858876_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (BrainpoolP256r1Holder_t2284807557), -1, sizeof(BrainpoolP256r1Holder_t2284807557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3811[1] = 
{
	BrainpoolP256r1Holder_t2284807557_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (BrainpoolP256t1Holder_t2284875459), -1, sizeof(BrainpoolP256t1Holder_t2284875459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3812[1] = 
{
	BrainpoolP256t1Holder_t2284875459_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (BrainpoolP320r1Holder_t1625039561), -1, sizeof(BrainpoolP320r1Holder_t1625039561_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3813[1] = 
{
	BrainpoolP320r1Holder_t1625039561_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (BrainpoolP320t1Holder_t1625250699), -1, sizeof(BrainpoolP320t1Holder_t1625250699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3814[1] = 
{
	BrainpoolP320t1Holder_t1625250699_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (BrainpoolP384r1Holder_t1017516731), -1, sizeof(BrainpoolP384r1Holder_t1017516731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3815[1] = 
{
	BrainpoolP384r1Holder_t1017516731_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (BrainpoolP384t1Holder_t1017871617), -1, sizeof(BrainpoolP384t1Holder_t1017871617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3816[1] = 
{
	BrainpoolP384t1Holder_t1017871617_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (BrainpoolP512r1Holder_t4283766514), -1, sizeof(BrainpoolP512r1Holder_t4283766514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3817[1] = 
{
	BrainpoolP512r1Holder_t4283766514_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (BrainpoolP512t1Holder_t4284121400), -1, sizeof(BrainpoolP512t1Holder_t4284121400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3818[1] = 
{
	BrainpoolP512t1Holder_t4284121400_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (TeleTrusTObjectIdentifiers_t2978504789), -1, sizeof(TeleTrusTObjectIdentifiers_t2978504789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3819[28] = 
{
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_TeleTrusTAlgorithm_0(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RipeMD160_1(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RipeMD128_2(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RipeMD256_3(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_TeleTrusTRsaSignatureAlgorithm_4(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RsaSignatureWithRipeMD160_5(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RsaSignatureWithRipeMD128_6(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RsaSignatureWithRipeMD256_7(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_ECSign_8(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_ECSignWithSha1_9(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_ECSignWithRipeMD160_10(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_EccBrainpool_11(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_EllipticCurve_12(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_VersionOne_13(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP160R1_14(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP160T1_15(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP192R1_16(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP192T1_17(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP224R1_18(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP224T1_19(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP256R1_20(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP256T1_21(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP320R1_22(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP320T1_23(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP384R1_24(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP384T1_25(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP512R1_26(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP512T1_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (Asn1Dump_t4013099873), -1, sizeof(Asn1Dump_t4013099873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3820[3] = 
{
	Asn1Dump_t4013099873_StaticFields::get_offset_of_NewLine_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (FilterStream_t3440239854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3821[1] = 
{
	FilterStream_t3440239854::get_offset_of_s_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (AlgorithmIdentifier_t2670781410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3822[3] = 
{
	AlgorithmIdentifier_t2670781410::get_offset_of_objectID_2(),
	AlgorithmIdentifier_t2670781410::get_offset_of_parameters_3(),
	AlgorithmIdentifier_t2670781410::get_offset_of_parametersDefined_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (BasicConstraints_t3459049714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3823[2] = 
{
	BasicConstraints_t3459049714::get_offset_of_cA_2(),
	BasicConstraints_t3459049714::get_offset_of_pathLenConstraint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (CrlDistPoint_t3073089489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[1] = 
{
	CrlDistPoint_t3073089489::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (CrlNumber_t1759393128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (CrlReason_t3883542487), -1, sizeof(CrlReason_t3883542487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3826[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	CrlReason_t3883542487_StaticFields::get_offset_of_ReasonString_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (CertificateList_t2288802675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3827[3] = 
{
	CertificateList_t2288802675::get_offset_of_tbsCertList_2(),
	CertificateList_t2288802675::get_offset_of_sigAlgID_3(),
	CertificateList_t2288802675::get_offset_of_sig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (DsaParameter_t274855281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[3] = 
{
	DsaParameter_t274855281::get_offset_of_p_2(),
	DsaParameter_t274855281::get_offset_of_q_3(),
	DsaParameter_t274855281::get_offset_of_g_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (DigestInfo_t202014884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[2] = 
{
	DigestInfo_t202014884::get_offset_of_digest_2(),
	DigestInfo_t202014884::get_offset_of_algID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (DistributionPoint_t769724552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[3] = 
{
	DistributionPoint_t769724552::get_offset_of_distributionPoint_2(),
	DistributionPoint_t769724552::get_offset_of_reasons_3(),
	DistributionPoint_t769724552::get_offset_of_cRLIssuer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (DistributionPointName_t1765286135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3831[4] = 
{
	DistributionPointName_t1765286135::get_offset_of_name_2(),
	DistributionPointName_t1765286135::get_offset_of_type_3(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (GeneralName_t294965175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3832[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GeneralName_t294965175::get_offset_of_obj_11(),
	GeneralName_t294965175::get_offset_of_tag_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (GeneralNames_t1290955016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3833[1] = 
{
	GeneralNames_t1290955016::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (IssuingDistributionPoint_t3776707264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3834[7] = 
{
	IssuingDistributionPoint_t3776707264::get_offset_of__distributionPoint_2(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlyContainsUserCerts_3(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlyContainsCACerts_4(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlySomeReasons_5(),
	IssuingDistributionPoint_t3776707264::get_offset_of__indirectCRL_6(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlyContainsAttributeCerts_7(),
	IssuingDistributionPoint_t3776707264::get_offset_of_seq_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (KeyUsage_t785552764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3835[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (RsaPublicKeyStructure_t197361851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3836[2] = 
{
	RsaPublicKeyStructure_t197361851::get_offset_of_modulus_2(),
	RsaPublicKeyStructure_t197361851::get_offset_of_publicExponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (ReasonFlags_t677892991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3837[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (SubjectPublicKeyInfo_t3547422518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3838[2] = 
{
	SubjectPublicKeyInfo_t3547422518::get_offset_of_algID_2(),
	SubjectPublicKeyInfo_t3547422518::get_offset_of_keyData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (CrlEntry_t4200172927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3839[4] = 
{
	CrlEntry_t4200172927::get_offset_of_seq_2(),
	CrlEntry_t4200172927::get_offset_of_userCertificate_3(),
	CrlEntry_t4200172927::get_offset_of_revocationDate_4(),
	CrlEntry_t4200172927::get_offset_of_crlEntryExtensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (TbsCertificateList_t741385336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[8] = 
{
	TbsCertificateList_t741385336::get_offset_of_seq_2(),
	TbsCertificateList_t741385336::get_offset_of_version_3(),
	TbsCertificateList_t741385336::get_offset_of_signature_4(),
	TbsCertificateList_t741385336::get_offset_of_issuer_5(),
	TbsCertificateList_t741385336::get_offset_of_thisUpdate_6(),
	TbsCertificateList_t741385336::get_offset_of_nextUpdate_7(),
	TbsCertificateList_t741385336::get_offset_of_revokedCertificates_8(),
	TbsCertificateList_t741385336::get_offset_of_crlExtensions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (RevokedCertificatesEnumeration_t64679979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[1] = 
{
	RevokedCertificatesEnumeration_t64679979::get_offset_of_en_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (RevokedCertificatesEnumerator_t2937350180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3842[1] = 
{
	RevokedCertificatesEnumerator_t2937350180::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (TbsCertificateStructure_t1391133771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3843[12] = 
{
	TbsCertificateStructure_t1391133771::get_offset_of_seq_2(),
	TbsCertificateStructure_t1391133771::get_offset_of_version_3(),
	TbsCertificateStructure_t1391133771::get_offset_of_serialNumber_4(),
	TbsCertificateStructure_t1391133771::get_offset_of_signature_5(),
	TbsCertificateStructure_t1391133771::get_offset_of_issuer_6(),
	TbsCertificateStructure_t1391133771::get_offset_of_startDate_7(),
	TbsCertificateStructure_t1391133771::get_offset_of_endDate_8(),
	TbsCertificateStructure_t1391133771::get_offset_of_subject_9(),
	TbsCertificateStructure_t1391133771::get_offset_of_subjectPublicKeyInfo_10(),
	TbsCertificateStructure_t1391133771::get_offset_of_issuerUniqueID_11(),
	TbsCertificateStructure_t1391133771::get_offset_of_subjectUniqueID_12(),
	TbsCertificateStructure_t1391133771::get_offset_of_extensions_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (Time_t2566907995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[1] = 
{
	Time_t2566907995::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (X509CertificateStructure_t3705285294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3845[3] = 
{
	X509CertificateStructure_t3705285294::get_offset_of_tbsCert_2(),
	X509CertificateStructure_t3705285294::get_offset_of_sigAlgID_3(),
	X509CertificateStructure_t3705285294::get_offset_of_sig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (X509DefaultEntryConverter_t518486245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (X509Extension_t2020524125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3847[2] = 
{
	X509Extension_t2020524125::get_offset_of_critical_0(),
	X509Extension_t2020524125::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (X509Extensions_t1384530060), -1, sizeof(X509Extensions_t1384530060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3848[33] = 
{
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectDirectoryAttributes_2(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectKeyIdentifier_3(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_KeyUsage_4(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_PrivateKeyUsagePeriod_5(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectAlternativeName_6(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_IssuerAlternativeName_7(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_BasicConstraints_8(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CrlNumber_9(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_ReasonCode_10(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_InstructionCode_11(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_InvalidityDate_12(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_DeltaCrlIndicator_13(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_IssuingDistributionPoint_14(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CertificateIssuer_15(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_NameConstraints_16(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CrlDistributionPoints_17(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CertificatePolicies_18(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_PolicyMappings_19(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_AuthorityKeyIdentifier_20(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_PolicyConstraints_21(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_ExtendedKeyUsage_22(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_FreshestCrl_23(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_InhibitAnyPolicy_24(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_AuthorityInfoAccess_25(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectInfoAccess_26(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_LogoType_27(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_BiometricInfo_28(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_QCStatements_29(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_AuditIdentity_30(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_NoRevAvail_31(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_TargetInformation_32(),
	X509Extensions_t1384530060::get_offset_of_extensions_33(),
	X509Extensions_t1384530060::get_offset_of_ordering_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (X509Name_t2936077305), -1, sizeof(X509Name_t2936077305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3849[44] = 
{
	X509Name_t2936077305_StaticFields::get_offset_of_C_2(),
	X509Name_t2936077305_StaticFields::get_offset_of_O_3(),
	X509Name_t2936077305_StaticFields::get_offset_of_OU_4(),
	X509Name_t2936077305_StaticFields::get_offset_of_T_5(),
	X509Name_t2936077305_StaticFields::get_offset_of_CN_6(),
	X509Name_t2936077305_StaticFields::get_offset_of_Street_7(),
	X509Name_t2936077305_StaticFields::get_offset_of_SerialNumber_8(),
	X509Name_t2936077305_StaticFields::get_offset_of_L_9(),
	X509Name_t2936077305_StaticFields::get_offset_of_ST_10(),
	X509Name_t2936077305_StaticFields::get_offset_of_Surname_11(),
	X509Name_t2936077305_StaticFields::get_offset_of_GivenName_12(),
	X509Name_t2936077305_StaticFields::get_offset_of_Initials_13(),
	X509Name_t2936077305_StaticFields::get_offset_of_Generation_14(),
	X509Name_t2936077305_StaticFields::get_offset_of_UniqueIdentifier_15(),
	X509Name_t2936077305_StaticFields::get_offset_of_BusinessCategory_16(),
	X509Name_t2936077305_StaticFields::get_offset_of_PostalCode_17(),
	X509Name_t2936077305_StaticFields::get_offset_of_DnQualifier_18(),
	X509Name_t2936077305_StaticFields::get_offset_of_Pseudonym_19(),
	X509Name_t2936077305_StaticFields::get_offset_of_DateOfBirth_20(),
	X509Name_t2936077305_StaticFields::get_offset_of_PlaceOfBirth_21(),
	X509Name_t2936077305_StaticFields::get_offset_of_Gender_22(),
	X509Name_t2936077305_StaticFields::get_offset_of_CountryOfCitizenship_23(),
	X509Name_t2936077305_StaticFields::get_offset_of_CountryOfResidence_24(),
	X509Name_t2936077305_StaticFields::get_offset_of_NameAtBirth_25(),
	X509Name_t2936077305_StaticFields::get_offset_of_PostalAddress_26(),
	X509Name_t2936077305_StaticFields::get_offset_of_DmdName_27(),
	X509Name_t2936077305_StaticFields::get_offset_of_TelephoneNumber_28(),
	X509Name_t2936077305_StaticFields::get_offset_of_Name_29(),
	X509Name_t2936077305_StaticFields::get_offset_of_EmailAddress_30(),
	X509Name_t2936077305_StaticFields::get_offset_of_UnstructuredName_31(),
	X509Name_t2936077305_StaticFields::get_offset_of_UnstructuredAddress_32(),
	X509Name_t2936077305_StaticFields::get_offset_of_E_33(),
	X509Name_t2936077305_StaticFields::get_offset_of_DC_34(),
	X509Name_t2936077305_StaticFields::get_offset_of_UID_35(),
	X509Name_t2936077305_StaticFields::get_offset_of_defaultReverse_36(),
	X509Name_t2936077305_StaticFields::get_offset_of_DefaultSymbols_37(),
	X509Name_t2936077305_StaticFields::get_offset_of_RFC2253Symbols_38(),
	X509Name_t2936077305_StaticFields::get_offset_of_RFC1779Symbols_39(),
	X509Name_t2936077305_StaticFields::get_offset_of_DefaultLookup_40(),
	X509Name_t2936077305::get_offset_of_ordering_41(),
	X509Name_t2936077305::get_offset_of_converter_42(),
	X509Name_t2936077305::get_offset_of_values_43(),
	X509Name_t2936077305::get_offset_of_added_44(),
	X509Name_t2936077305::get_offset_of_seq_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (X509NameEntryConverter_t1972691209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (X509NameTokenizer_t3449226918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3851[4] = 
{
	X509NameTokenizer_t3449226918::get_offset_of_value_0(),
	X509NameTokenizer_t3449226918::get_offset_of_index_1(),
	X509NameTokenizer_t3449226918::get_offset_of_separator_2(),
	X509NameTokenizer_t3449226918::get_offset_of_buffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (X509ObjectIdentifiers_t346896425), -1, sizeof(X509ObjectIdentifiers_t346896425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3852[20] = 
{
	0,
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_CommonName_1(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_CountryName_2(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_LocalityName_3(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_StateOrProvinceName_4(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_Organization_5(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_OrganizationalUnitName_6(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_id_at_telephoneNumber_7(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_id_at_name_8(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdSha1_9(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_RipeMD160_10(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_RipeMD160WithRsaEncryption_11(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdEARsa_12(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdPkix_13(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdPE_14(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdAD_15(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdADCAIssuers_16(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdADOcsp_17(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_OcspAccessMethod_18(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_CrlAccessMethod_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (DHDomainParameters_t3489907266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[5] = 
{
	DHDomainParameters_t3489907266::get_offset_of_p_2(),
	DHDomainParameters_t3489907266::get_offset_of_g_3(),
	DHDomainParameters_t3489907266::get_offset_of_q_4(),
	DHDomainParameters_t3489907266::get_offset_of_j_5(),
	DHDomainParameters_t3489907266::get_offset_of_validationParms_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (DHPublicKey_t3181392102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[1] = 
{
	DHPublicKey_t3181392102::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (DHValidationParms_t4010119324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3855[2] = 
{
	DHValidationParms_t4010119324::get_offset_of_seed_2(),
	DHValidationParms_t4010119324::get_offset_of_pgenCounter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (ECNamedCurveTable_t3889450962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (X962NamedCurves_t2357232190), -1, sizeof(X962NamedCurves_t2357232190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3857[3] = 
{
	X962NamedCurves_t2357232190_StaticFields::get_offset_of_objIds_0(),
	X962NamedCurves_t2357232190_StaticFields::get_offset_of_curves_1(),
	X962NamedCurves_t2357232190_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (Prime192v1Holder_t1386193459), -1, sizeof(Prime192v1Holder_t1386193459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3858[1] = 
{
	Prime192v1Holder_t1386193459_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (Prime192v2Holder_t1385946192), -1, sizeof(Prime192v2Holder_t1385946192_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3859[1] = 
{
	Prime192v2Holder_t1385946192_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (Prime192v3Holder_t1385986417), -1, sizeof(Prime192v3Holder_t1385986417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3860[1] = 
{
	Prime192v3Holder_t1385986417_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (Prime239v1Holder_t4025924969), -1, sizeof(Prime239v1Holder_t4025924969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3861[1] = 
{
	Prime239v1Holder_t4025924969_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (Prime239v2Holder_t4025965190), -1, sizeof(Prime239v2Holder_t4025965190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3862[1] = 
{
	Prime239v2Holder_t4025965190_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (Prime239v3Holder_t4025717927), -1, sizeof(Prime239v3Holder_t4025717927_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3863[1] = 
{
	Prime239v3Holder_t4025717927_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (Prime256v1Holder_t299104776), -1, sizeof(Prime256v1Holder_t299104776_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3864[1] = 
{
	Prime256v1Holder_t299104776_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (C2pnb163v1Holder_t1991767207), -1, sizeof(C2pnb163v1Holder_t1991767207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3865[1] = 
{
	C2pnb163v1Holder_t1991767207_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (C2pnb163v2Holder_t1991663432), -1, sizeof(C2pnb163v2Holder_t1991663432_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3866[1] = 
{
	C2pnb163v2Holder_t1991663432_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (C2pnb163v3Holder_t1991695209), -1, sizeof(C2pnb163v3Holder_t1991695209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3867[1] = 
{
	C2pnb163v3Holder_t1991695209_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (C2pnb176w1Holder_t3726533506), -1, sizeof(C2pnb176w1Holder_t3726533506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3868[1] = 
{
	C2pnb176w1Holder_t3726533506_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (C2tnb191v1Holder_t2622364672), -1, sizeof(C2tnb191v1Holder_t2622364672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3869[1] = 
{
	C2tnb191v1Holder_t2622364672_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (C2tnb191v2Holder_t2622117405), -1, sizeof(C2tnb191v2Holder_t2622117405_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3870[1] = 
{
	C2tnb191v2Holder_t2622117405_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (C2tnb191v3Holder_t2622157630), -1, sizeof(C2tnb191v3Holder_t2622157630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3871[1] = 
{
	C2tnb191v3Holder_t2622157630_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (C2pnb208w1Holder_t3381865698), -1, sizeof(C2pnb208w1Holder_t3381865698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3872[1] = 
{
	C2pnb208w1Holder_t3381865698_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (C2tnb239v1Holder_t3623772131), -1, sizeof(C2tnb239v1Holder_t3623772131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3873[1] = 
{
	C2tnb239v1Holder_t3623772131_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (C2tnb239v2Holder_t3623731910), -1, sizeof(C2tnb239v2Holder_t3623731910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3874[1] = 
{
	C2tnb239v2Holder_t3623731910_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (C2tnb239v3Holder_t3623700133), -1, sizeof(C2tnb239v3Holder_t3623700133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3875[1] = 
{
	C2tnb239v3Holder_t3623700133_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (C2pnb272w1Holder_t3772636393), -1, sizeof(C2pnb272w1Holder_t3772636393_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3876[1] = 
{
	C2pnb272w1Holder_t3772636393_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (C2pnb304w1Holder_t3252909381), -1, sizeof(C2pnb304w1Holder_t3252909381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3877[1] = 
{
	C2pnb304w1Holder_t3252909381_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (C2tnb359v1Holder_t3160589090), -1, sizeof(C2tnb359v1Holder_t3160589090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3878[1] = 
{
	C2tnb359v1Holder_t3160589090_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (C2pnb368w1Holder_t3617495651), -1, sizeof(C2pnb368w1Holder_t3617495651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3879[1] = 
{
	C2pnb368w1Holder_t3617495651_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (C2tnb431r1Holder_t2773896637), -1, sizeof(C2tnb431r1Holder_t2773896637_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3880[1] = 
{
	C2tnb431r1Holder_t2773896637_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (X962Parameters_t1137405623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3881[1] = 
{
	X962Parameters_t1137405623::get_offset_of__params_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (X9Curve_t2327439944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3882[3] = 
{
	X9Curve_t2327439944::get_offset_of_curve_2(),
	X9Curve_t2327439944::get_offset_of_seed_3(),
	X9Curve_t2327439944::get_offset_of_fieldIdentifier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (X9ECParameters_t1959966001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3883[6] = 
{
	X9ECParameters_t1959966001::get_offset_of_fieldID_2(),
	X9ECParameters_t1959966001::get_offset_of_curve_3(),
	X9ECParameters_t1959966001::get_offset_of_g_4(),
	X9ECParameters_t1959966001::get_offset_of_n_5(),
	X9ECParameters_t1959966001::get_offset_of_h_6(),
	X9ECParameters_t1959966001::get_offset_of_seed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (X9ECParametersHolder_t1566440261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[1] = 
{
	X9ECParametersHolder_t1566440261::get_offset_of_parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (X9ECPoint_t3031020555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[1] = 
{
	X9ECPoint_t3031020555::get_offset_of_p_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (X9FieldElement_t706679609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3886[1] = 
{
	X9FieldElement_t706679609::get_offset_of_f_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (X9FieldID_t2012644846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3887[2] = 
{
	X9FieldID_t2012644846::get_offset_of_id_2(),
	X9FieldID_t2012644846::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (X9IntegerConverter_t1331659747), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (X9ObjectIdentifiers_t773243058), -1, sizeof(X9ObjectIdentifiers_t773243058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3889[66] = 
{
	0,
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ansi_X9_62_1(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdFieldType_2(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_PrimeField_3(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_CharacteristicTwoField_4(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_GNBasis_5(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_TPBasis_6(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_PPBasis_7(),
	0,
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_id_ecSigType_9(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha1_10(),
	0,
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_id_publicKeyType_12(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdECPublicKey_13(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha2_14(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha224_15(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha256_16(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha384_17(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha512_18(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_EllipticCurve_19(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_CTwoCurve_20(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb163v1_21(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb163v2_22(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb163v3_23(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb176w1_24(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb191v1_25(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb191v2_26(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb191v3_27(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb191v4_28(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb191v5_29(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb208w1_30(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb239v1_31(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb239v2_32(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb239v3_33(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb239v4_34(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb239v5_35(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb272w1_36(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb304w1_37(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb359v1_38(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb368w1_39(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb431r1_40(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_PrimeCurve_41(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime192v1_42(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime192v2_43(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime192v3_44(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime239v1_45(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime239v2_46(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime239v3_47(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime256v1_48(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdDsa_49(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdDsaWithSha1_50(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_X9x63Scheme_51(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHSinglePassStdDHSha1KdfScheme_52(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHSinglePassCofactorDHSha1KdfScheme_53(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_MqvSinglePassSha1KdfScheme_54(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ansi_x9_42_55(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHPublicNumber_56(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_X9x42Schemes_57(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHStatic_58(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHEphem_59(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHOneFlow_60(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHHybrid1_61(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHHybrid2_62(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHHybridOneFlow_63(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Mqv2_64(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Mqv1_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (AsymmetricCipherKeyPair_t1825508216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3890[2] = 
{
	AsymmetricCipherKeyPair_t1825508216::get_offset_of_publicParameter_0(),
	AsymmetricCipherKeyPair_t1825508216::get_offset_of_privateParameter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (AsymmetricKeyParameter_t1663727050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3891[1] = 
{
	AsymmetricKeyParameter_t1663727050::get_offset_of_privateKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (BufferedAeadBlockCipher_t2590109294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3892[1] = 
{
	BufferedAeadBlockCipher_t2590109294::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (BufferedAsymmetricBlockCipher_t862890205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3893[3] = 
{
	BufferedAsymmetricBlockCipher_t862890205::get_offset_of_cipher_1(),
	BufferedAsymmetricBlockCipher_t862890205::get_offset_of_buffer_2(),
	BufferedAsymmetricBlockCipher_t862890205::get_offset_of_bufOff_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (BufferedBlockCipher_t711630611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3894[4] = 
{
	BufferedBlockCipher_t711630611::get_offset_of_buf_1(),
	BufferedBlockCipher_t711630611::get_offset_of_bufOff_2(),
	BufferedBlockCipher_t711630611::get_offset_of_forEncryption_3(),
	BufferedBlockCipher_t711630611::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (BufferedCipherBase_t3241650255), -1, sizeof(BufferedCipherBase_t3241650255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3895[1] = 
{
	BufferedCipherBase_t3241650255_StaticFields::get_offset_of_EmptyBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (BufferedIesCipher_t295552809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3896[3] = 
{
	BufferedIesCipher_t295552809::get_offset_of_engine_1(),
	BufferedIesCipher_t295552809::get_offset_of_forEncryption_2(),
	BufferedIesCipher_t295552809::get_offset_of_buffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (BufferedStreamCipher_t702588694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3897[1] = 
{
	BufferedStreamCipher_t702588694::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (Check_t1143849288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (CipherKeyGenerator_t2313639529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3899[4] = 
{
	CipherKeyGenerator_t2313639529::get_offset_of_random_0(),
	CipherKeyGenerator_t2313639529::get_offset_of_strength_1(),
	CipherKeyGenerator_t2313639529::get_offset_of_uninitialised_2(),
	CipherKeyGenerator_t2313639529::get_offset_of_defaultStrength_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
