﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.LocaleData
struct LocaleData_t3425842736;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t1563811461;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.LocaleData::.ctor()
extern "C"  void LocaleData__ctor_m512904669 (LocaleData_t3425842736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> PhoneNumbers.LocaleData::get_Data()
extern "C"  Dictionary_2_t1563811461 * LocaleData_get_Data_m2967423561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.LocaleData::.cctor()
extern "C"  void LocaleData__cctor_m1711144588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.LocaleData::<get_Data>m__0(System.String)
extern "C"  bool LocaleData_U3Cget_DataU3Em__0_m3926842437 (Il2CppObject * __this /* static, unused */, String_t* ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
