﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<OnlineMapsGetElevationResult>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1558879759(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3974088340 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<OnlineMapsGetElevationResult>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3088899807(__this, method) ((  void (*) (InternalEnumerator_1_t3974088340 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<OnlineMapsGetElevationResult>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4227905571(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3974088340 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<OnlineMapsGetElevationResult>::Dispose()
#define InternalEnumerator_1_Dispose_m2406030606(__this, method) ((  void (*) (InternalEnumerator_1_t3974088340 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<OnlineMapsGetElevationResult>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4075536059(__this, method) ((  bool (*) (InternalEnumerator_1_t3974088340 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<OnlineMapsGetElevationResult>::get_Current()
#define InternalEnumerator_1_get_Current_m3150709928(__this, method) ((  OnlineMapsGetElevationResult_t3115336078 * (*) (InternalEnumerator_1_t3974088340 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
