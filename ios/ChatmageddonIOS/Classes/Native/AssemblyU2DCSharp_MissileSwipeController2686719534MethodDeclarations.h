﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileSwipeController
struct MissileSwipeController_t2686719534;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileSwipeController::.ctor()
extern "C"  void MissileSwipeController__ctor_m3645889551 (MissileSwipeController_t2686719534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSwipeController::DisplayItemModel()
extern "C"  void MissileSwipeController_DisplayItemModel_m1844359499 (MissileSwipeController_t2686719534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
