﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClearFullStatusButton
struct ClearFullStatusButton_t3074485066;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ClearFullStatusButton::.ctor()
extern "C"  void ClearFullStatusButton__ctor_m200417459 (ClearFullStatusButton_t3074485066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClearFullStatusButton::OnButtonClick()
extern "C"  void ClearFullStatusButton_OnButtonClick_m120022370 (ClearFullStatusButton_t3074485066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClearFullStatusButton::ClearStatus()
extern "C"  void ClearFullStatusButton_ClearStatus_m843148270 (ClearFullStatusButton_t3074485066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClearFullStatusButton::<ClearStatus>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ClearFullStatusButton_U3CClearStatusU3Em__0_m4248834234 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
