﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// FriendsNavigateBackwardsButton
struct FriendsNavigateBackwardsButton_t1799964784;
// FriendsNavigateForwardsButton
struct FriendsNavigateForwardsButton_t433492960;

#include "AssemblyU2DCSharp_NavigationController_2_gen1797874735.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsNavigationController
struct  FriendsNavigationController_t702141233  : public NavigationController_2_t1797874735
{
public:
	// UILabel FriendsNavigationController::titleLabel
	UILabel_t1795115428 * ___titleLabel_8;
	// UnityEngine.GameObject FriendsNavigationController::homeObject
	GameObject_t1756533147 * ___homeObject_9;
	// UnityEngine.GameObject FriendsNavigationController::searchObject
	GameObject_t1756533147 * ___searchObject_10;
	// FriendsNavigateBackwardsButton FriendsNavigationController::mainNavigateBackwardButton
	FriendsNavigateBackwardsButton_t1799964784 * ___mainNavigateBackwardButton_11;
	// FriendsNavigateForwardsButton FriendsNavigationController::mainNavigateForwardButton
	FriendsNavigateForwardsButton_t433492960 * ___mainNavigateForwardButton_12;
	// UnityEngine.Vector3 FriendsNavigationController::activePos
	Vector3_t2243707580  ___activePos_13;
	// UnityEngine.Vector3 FriendsNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_14;
	// UnityEngine.Vector3 FriendsNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_15;

public:
	inline static int32_t get_offset_of_titleLabel_8() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___titleLabel_8)); }
	inline UILabel_t1795115428 * get_titleLabel_8() const { return ___titleLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_8() { return &___titleLabel_8; }
	inline void set_titleLabel_8(UILabel_t1795115428 * value)
	{
		___titleLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_8, value);
	}

	inline static int32_t get_offset_of_homeObject_9() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___homeObject_9)); }
	inline GameObject_t1756533147 * get_homeObject_9() const { return ___homeObject_9; }
	inline GameObject_t1756533147 ** get_address_of_homeObject_9() { return &___homeObject_9; }
	inline void set_homeObject_9(GameObject_t1756533147 * value)
	{
		___homeObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___homeObject_9, value);
	}

	inline static int32_t get_offset_of_searchObject_10() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___searchObject_10)); }
	inline GameObject_t1756533147 * get_searchObject_10() const { return ___searchObject_10; }
	inline GameObject_t1756533147 ** get_address_of_searchObject_10() { return &___searchObject_10; }
	inline void set_searchObject_10(GameObject_t1756533147 * value)
	{
		___searchObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___searchObject_10, value);
	}

	inline static int32_t get_offset_of_mainNavigateBackwardButton_11() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___mainNavigateBackwardButton_11)); }
	inline FriendsNavigateBackwardsButton_t1799964784 * get_mainNavigateBackwardButton_11() const { return ___mainNavigateBackwardButton_11; }
	inline FriendsNavigateBackwardsButton_t1799964784 ** get_address_of_mainNavigateBackwardButton_11() { return &___mainNavigateBackwardButton_11; }
	inline void set_mainNavigateBackwardButton_11(FriendsNavigateBackwardsButton_t1799964784 * value)
	{
		___mainNavigateBackwardButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateBackwardButton_11, value);
	}

	inline static int32_t get_offset_of_mainNavigateForwardButton_12() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___mainNavigateForwardButton_12)); }
	inline FriendsNavigateForwardsButton_t433492960 * get_mainNavigateForwardButton_12() const { return ___mainNavigateForwardButton_12; }
	inline FriendsNavigateForwardsButton_t433492960 ** get_address_of_mainNavigateForwardButton_12() { return &___mainNavigateForwardButton_12; }
	inline void set_mainNavigateForwardButton_12(FriendsNavigateForwardsButton_t433492960 * value)
	{
		___mainNavigateForwardButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateForwardButton_12, value);
	}

	inline static int32_t get_offset_of_activePos_13() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___activePos_13)); }
	inline Vector3_t2243707580  get_activePos_13() const { return ___activePos_13; }
	inline Vector3_t2243707580 * get_address_of_activePos_13() { return &___activePos_13; }
	inline void set_activePos_13(Vector3_t2243707580  value)
	{
		___activePos_13 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_14() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___leftInactivePos_14)); }
	inline Vector3_t2243707580  get_leftInactivePos_14() const { return ___leftInactivePos_14; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_14() { return &___leftInactivePos_14; }
	inline void set_leftInactivePos_14(Vector3_t2243707580  value)
	{
		___leftInactivePos_14 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_15() { return static_cast<int32_t>(offsetof(FriendsNavigationController_t702141233, ___rightInactivePos_15)); }
	inline Vector3_t2243707580  get_rightInactivePos_15() const { return ___rightInactivePos_15; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_15() { return &___rightInactivePos_15; }
	inline void set_rightInactivePos_15(Vector3_t2243707580  value)
	{
		___rightInactivePos_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
