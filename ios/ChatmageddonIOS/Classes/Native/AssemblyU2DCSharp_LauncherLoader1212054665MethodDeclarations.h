﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LauncherLoader
struct LauncherLoader_t1212054665;

#include "codegen/il2cpp-codegen.h"

// System.Void LauncherLoader::.ctor()
extern "C"  void LauncherLoader__ctor_m4093502498 (LauncherLoader_t1212054665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LauncherLoader::LoadSceneUI()
extern "C"  void LauncherLoader_LoadSceneUI_m4042596984 (LauncherLoader_t1212054665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LauncherLoader::UnloadSceneUI()
extern "C"  void LauncherLoader_UnloadSceneUI_m1009293703 (LauncherLoader_t1212054665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
