﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegImportFBProfile
struct RegImportFBProfile_t542324572;

#include "codegen/il2cpp-codegen.h"

// System.Void RegImportFBProfile::.ctor()
extern "C"  void RegImportFBProfile__ctor_m1840151073 (RegImportFBProfile_t542324572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegImportFBProfile::OnButtonClick()
extern "C"  void RegImportFBProfile_OnButtonClick_m514816752 (RegImportFBProfile_t542324572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
