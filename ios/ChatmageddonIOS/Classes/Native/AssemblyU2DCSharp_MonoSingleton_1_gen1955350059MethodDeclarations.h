﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<LoginSuccessSceneManager>::.ctor()
#define MonoSingleton_1__ctor_m2467521845(__this, method) ((  void (*) (MonoSingleton_1_t1955350059 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<LoginSuccessSceneManager>::Awake()
#define MonoSingleton_1_Awake_m716990028(__this, method) ((  void (*) (MonoSingleton_1_t1955350059 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<LoginSuccessSceneManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3340240506(__this /* static, unused */, method) ((  LoginSuccessSceneManager_t2204684339 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<LoginSuccessSceneManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1618262610(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<LoginSuccessSceneManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m706305315(__this, method) ((  void (*) (MonoSingleton_1_t1955350059 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<LoginSuccessSceneManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1480587487(__this, method) ((  void (*) (MonoSingleton_1_t1955350059 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<LoginSuccessSceneManager>::.cctor()
#define MonoSingleton_1__cctor_m3710421590(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
