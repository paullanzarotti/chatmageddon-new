﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// Player/<UpdateInnerFriendsList>c__AnonStorey1
struct U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968;
// System.Func`2<Friend,System.Int32>
struct Func_2_t2787752525;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/<UpdateInnerFriendsList>c__AnonStorey0
struct  U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967  : public Il2CppObject
{
public:
	// Friend Player/<UpdateInnerFriendsList>c__AnonStorey0::friend
	Friend_t3555014108 * ___friend_0;
	// Player/<UpdateInnerFriendsList>c__AnonStorey1 Player/<UpdateInnerFriendsList>c__AnonStorey0::<>f__ref$1
	U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_friend_0() { return static_cast<int32_t>(offsetof(U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967, ___friend_0)); }
	inline Friend_t3555014108 * get_friend_0() const { return ___friend_0; }
	inline Friend_t3555014108 ** get_address_of_friend_0() { return &___friend_0; }
	inline void set_friend_0(Friend_t3555014108 * value)
	{
		___friend_0 = value;
		Il2CppCodeGenWriteBarrier(&___friend_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967, ___U3CU3Ef__refU241_1)); }
	inline U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

struct U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967_StaticFields
{
public:
	// System.Func`2<Friend,System.Int32> Player/<UpdateInnerFriendsList>c__AnonStorey0::<>f__am$cache0
	Func_2_t2787752525 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2787752525 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2787752525 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2787752525 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
