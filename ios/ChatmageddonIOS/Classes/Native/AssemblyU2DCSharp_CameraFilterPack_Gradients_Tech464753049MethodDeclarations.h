﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Tech
struct CameraFilterPack_Gradients_Tech_t464753049;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Tech::.ctor()
extern "C"  void CameraFilterPack_Gradients_Tech__ctor_m1627241382 (CameraFilterPack_Gradients_Tech_t464753049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Tech::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Tech_get_material_m3636833637 (CameraFilterPack_Gradients_Tech_t464753049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::Start()
extern "C"  void CameraFilterPack_Gradients_Tech_Start_m1399635102 (CameraFilterPack_Gradients_Tech_t464753049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Tech_OnRenderImage_m4020832590 (CameraFilterPack_Gradients_Tech_t464753049 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::Update()
extern "C"  void CameraFilterPack_Gradients_Tech_Update_m3231451393 (CameraFilterPack_Gradients_Tech_t464753049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Tech_OnDisable_m217433253 (CameraFilterPack_Gradients_Tech_t464753049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
