﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdatePasswordUIScaler
struct  UpdatePasswordUIScaler_t4097640474  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UILabel UpdatePasswordUIScaler::currentPasswordLabel
	UILabel_t1795115428 * ___currentPasswordLabel_14;
	// UnityEngine.BoxCollider UpdatePasswordUIScaler::currentPasswordCollider
	BoxCollider_t22920061 * ___currentPasswordCollider_15;
	// UISprite UpdatePasswordUIScaler::currentPasswordDivider
	UISprite_t603616735 * ___currentPasswordDivider_16;
	// UILabel UpdatePasswordUIScaler::newPasswordLabel
	UILabel_t1795115428 * ___newPasswordLabel_17;
	// UnityEngine.BoxCollider UpdatePasswordUIScaler::newPasswordCollider
	BoxCollider_t22920061 * ___newPasswordCollider_18;
	// UISprite UpdatePasswordUIScaler::newPasswordDivider
	UISprite_t603616735 * ___newPasswordDivider_19;
	// UILabel UpdatePasswordUIScaler::confirmPasswordLabel
	UILabel_t1795115428 * ___confirmPasswordLabel_20;
	// UnityEngine.BoxCollider UpdatePasswordUIScaler::confirmPasswordCollider
	BoxCollider_t22920061 * ___confirmPasswordCollider_21;
	// UISprite UpdatePasswordUIScaler::confirmPasswordDivider
	UISprite_t603616735 * ___confirmPasswordDivider_22;

public:
	inline static int32_t get_offset_of_currentPasswordLabel_14() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___currentPasswordLabel_14)); }
	inline UILabel_t1795115428 * get_currentPasswordLabel_14() const { return ___currentPasswordLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_currentPasswordLabel_14() { return &___currentPasswordLabel_14; }
	inline void set_currentPasswordLabel_14(UILabel_t1795115428 * value)
	{
		___currentPasswordLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentPasswordLabel_14, value);
	}

	inline static int32_t get_offset_of_currentPasswordCollider_15() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___currentPasswordCollider_15)); }
	inline BoxCollider_t22920061 * get_currentPasswordCollider_15() const { return ___currentPasswordCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_currentPasswordCollider_15() { return &___currentPasswordCollider_15; }
	inline void set_currentPasswordCollider_15(BoxCollider_t22920061 * value)
	{
		___currentPasswordCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___currentPasswordCollider_15, value);
	}

	inline static int32_t get_offset_of_currentPasswordDivider_16() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___currentPasswordDivider_16)); }
	inline UISprite_t603616735 * get_currentPasswordDivider_16() const { return ___currentPasswordDivider_16; }
	inline UISprite_t603616735 ** get_address_of_currentPasswordDivider_16() { return &___currentPasswordDivider_16; }
	inline void set_currentPasswordDivider_16(UISprite_t603616735 * value)
	{
		___currentPasswordDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentPasswordDivider_16, value);
	}

	inline static int32_t get_offset_of_newPasswordLabel_17() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___newPasswordLabel_17)); }
	inline UILabel_t1795115428 * get_newPasswordLabel_17() const { return ___newPasswordLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_newPasswordLabel_17() { return &___newPasswordLabel_17; }
	inline void set_newPasswordLabel_17(UILabel_t1795115428 * value)
	{
		___newPasswordLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___newPasswordLabel_17, value);
	}

	inline static int32_t get_offset_of_newPasswordCollider_18() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___newPasswordCollider_18)); }
	inline BoxCollider_t22920061 * get_newPasswordCollider_18() const { return ___newPasswordCollider_18; }
	inline BoxCollider_t22920061 ** get_address_of_newPasswordCollider_18() { return &___newPasswordCollider_18; }
	inline void set_newPasswordCollider_18(BoxCollider_t22920061 * value)
	{
		___newPasswordCollider_18 = value;
		Il2CppCodeGenWriteBarrier(&___newPasswordCollider_18, value);
	}

	inline static int32_t get_offset_of_newPasswordDivider_19() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___newPasswordDivider_19)); }
	inline UISprite_t603616735 * get_newPasswordDivider_19() const { return ___newPasswordDivider_19; }
	inline UISprite_t603616735 ** get_address_of_newPasswordDivider_19() { return &___newPasswordDivider_19; }
	inline void set_newPasswordDivider_19(UISprite_t603616735 * value)
	{
		___newPasswordDivider_19 = value;
		Il2CppCodeGenWriteBarrier(&___newPasswordDivider_19, value);
	}

	inline static int32_t get_offset_of_confirmPasswordLabel_20() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___confirmPasswordLabel_20)); }
	inline UILabel_t1795115428 * get_confirmPasswordLabel_20() const { return ___confirmPasswordLabel_20; }
	inline UILabel_t1795115428 ** get_address_of_confirmPasswordLabel_20() { return &___confirmPasswordLabel_20; }
	inline void set_confirmPasswordLabel_20(UILabel_t1795115428 * value)
	{
		___confirmPasswordLabel_20 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPasswordLabel_20, value);
	}

	inline static int32_t get_offset_of_confirmPasswordCollider_21() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___confirmPasswordCollider_21)); }
	inline BoxCollider_t22920061 * get_confirmPasswordCollider_21() const { return ___confirmPasswordCollider_21; }
	inline BoxCollider_t22920061 ** get_address_of_confirmPasswordCollider_21() { return &___confirmPasswordCollider_21; }
	inline void set_confirmPasswordCollider_21(BoxCollider_t22920061 * value)
	{
		___confirmPasswordCollider_21 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPasswordCollider_21, value);
	}

	inline static int32_t get_offset_of_confirmPasswordDivider_22() { return static_cast<int32_t>(offsetof(UpdatePasswordUIScaler_t4097640474, ___confirmPasswordDivider_22)); }
	inline UISprite_t603616735 * get_confirmPasswordDivider_22() const { return ___confirmPasswordDivider_22; }
	inline UISprite_t603616735 ** get_address_of_confirmPasswordDivider_22() { return &___confirmPasswordDivider_22; }
	inline void set_confirmPasswordDivider_22(UISprite_t603616735 * value)
	{
		___confirmPasswordDivider_22 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPasswordDivider_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
