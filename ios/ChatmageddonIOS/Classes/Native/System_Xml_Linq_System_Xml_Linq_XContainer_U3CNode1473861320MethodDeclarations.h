﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XContainer/<Nodes>c__Iterator1A
struct U3CNodesU3Ec__Iterator1A_t1473861320;
// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode>
struct IEnumerator_1_t183028041;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::.ctor()
extern "C"  void U3CNodesU3Ec__Iterator1A__ctor_m4168269813 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::System.Collections.Generic.IEnumerator<System.Xml.Linq.XNode>.get_Current()
extern "C"  XNode_t2707504214 * U3CNodesU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XNodeU3E_get_Current_m554163480 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNodesU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m2663567463 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNodesU3Ec__Iterator1A_System_Collections_IEnumerable_GetEnumerator_m2065522060 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode> System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator()
extern "C"  Il2CppObject* U3CNodesU3Ec__Iterator1A_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XNodeU3E_GetEnumerator_m2816027373 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::MoveNext()
extern "C"  bool U3CNodesU3Ec__Iterator1A_MoveNext_m102305355 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::Dispose()
extern "C"  void U3CNodesU3Ec__Iterator1A_Dispose_m98927568 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::Reset()
extern "C"  void U3CNodesU3Ec__Iterator1A_Reset_m555334754 (U3CNodesU3Ec__Iterator1A_t1473861320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
