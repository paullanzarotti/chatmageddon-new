﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// AttackILP
struct AttackILP_t2111768551;
// UILabel
struct UILabel_t1795115428;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// OpenPlayerProfileButton
struct OpenPlayerProfileButton_t3248004898;
// StealthModeCalculator
struct StealthModeCalculator_t3335204764;
// KnockoutCalculator
struct KnockoutCalculator_t371618716;
// DoubleStateButton
struct DoubleStateButton_t1032633262;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen4181526185.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackListItem
struct  AttackListItem_t3405986821  : public BaseInfiniteListItem_1_t4181526185
{
public:
	// Friend AttackListItem::friend
	Friend_t3555014108 * ___friend_10;
	// AttackILP AttackListItem::listPopulator
	AttackILP_t2111768551 * ___listPopulator_11;
	// UILabel AttackListItem::nameLabel
	UILabel_t1795115428 * ___nameLabel_12;
	// AvatarUpdater AttackListItem::avatarUpdater
	AvatarUpdater_t2404165026 * ___avatarUpdater_13;
	// OpenPlayerProfileButton AttackListItem::profileButton
	OpenPlayerProfileButton_t3248004898 * ___profileButton_14;
	// StealthModeCalculator AttackListItem::stealthTimeCalc
	StealthModeCalculator_t3335204764 * ___stealthTimeCalc_15;
	// KnockoutCalculator AttackListItem::knockoutCalc
	KnockoutCalculator_t371618716 * ___knockoutCalc_16;
	// DoubleStateButton AttackListItem::chatButton
	DoubleStateButton_t1032633262 * ___chatButton_17;
	// DoubleStateButton AttackListItem::attackButton
	DoubleStateButton_t1032633262 * ___attackButton_18;
	// DoubleStateButton AttackListItem::globalFriendButton
	DoubleStateButton_t1032633262 * ___globalFriendButton_19;
	// UILabel AttackListItem::requestSentLabel
	UILabel_t1795115428 * ___requestSentLabel_20;
	// UnityEngine.GameObject AttackListItem::acceptFriendButton
	GameObject_t1756533147 * ___acceptFriendButton_21;
	// UnityEngine.GameObject AttackListItem::declineFriendButton
	GameObject_t1756533147 * ___declineFriendButton_22;
	// UILabel AttackListItem::actionLabel
	UILabel_t1795115428 * ___actionLabel_23;
	// UILabel AttackListItem::timeLabel
	UILabel_t1795115428 * ___timeLabel_24;
	// UnityEngine.Color AttackListItem::activeColour
	Color_t2020392075  ___activeColour_25;
	// UnityEngine.Color AttackListItem::inactiveColour
	Color_t2020392075  ___inactiveColour_26;
	// UnityEngine.Color AttackListItem::friendRequestColour
	Color_t2020392075  ___friendRequestColour_27;
	// UIScrollView AttackListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_28;
	// UIScrollView AttackListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_29;
	// UnityEngine.Transform AttackListItem::mTrans
	Transform_t3275118058 * ___mTrans_30;
	// UIScrollView AttackListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_31;
	// System.Boolean AttackListItem::mAutoFind
	bool ___mAutoFind_32;
	// System.Boolean AttackListItem::mStarted
	bool ___mStarted_33;

public:
	inline static int32_t get_offset_of_friend_10() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___friend_10)); }
	inline Friend_t3555014108 * get_friend_10() const { return ___friend_10; }
	inline Friend_t3555014108 ** get_address_of_friend_10() { return &___friend_10; }
	inline void set_friend_10(Friend_t3555014108 * value)
	{
		___friend_10 = value;
		Il2CppCodeGenWriteBarrier(&___friend_10, value);
	}

	inline static int32_t get_offset_of_listPopulator_11() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___listPopulator_11)); }
	inline AttackILP_t2111768551 * get_listPopulator_11() const { return ___listPopulator_11; }
	inline AttackILP_t2111768551 ** get_address_of_listPopulator_11() { return &___listPopulator_11; }
	inline void set_listPopulator_11(AttackILP_t2111768551 * value)
	{
		___listPopulator_11 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_11, value);
	}

	inline static int32_t get_offset_of_nameLabel_12() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___nameLabel_12)); }
	inline UILabel_t1795115428 * get_nameLabel_12() const { return ___nameLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_12() { return &___nameLabel_12; }
	inline void set_nameLabel_12(UILabel_t1795115428 * value)
	{
		___nameLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_12, value);
	}

	inline static int32_t get_offset_of_avatarUpdater_13() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___avatarUpdater_13)); }
	inline AvatarUpdater_t2404165026 * get_avatarUpdater_13() const { return ___avatarUpdater_13; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatarUpdater_13() { return &___avatarUpdater_13; }
	inline void set_avatarUpdater_13(AvatarUpdater_t2404165026 * value)
	{
		___avatarUpdater_13 = value;
		Il2CppCodeGenWriteBarrier(&___avatarUpdater_13, value);
	}

	inline static int32_t get_offset_of_profileButton_14() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___profileButton_14)); }
	inline OpenPlayerProfileButton_t3248004898 * get_profileButton_14() const { return ___profileButton_14; }
	inline OpenPlayerProfileButton_t3248004898 ** get_address_of_profileButton_14() { return &___profileButton_14; }
	inline void set_profileButton_14(OpenPlayerProfileButton_t3248004898 * value)
	{
		___profileButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___profileButton_14, value);
	}

	inline static int32_t get_offset_of_stealthTimeCalc_15() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___stealthTimeCalc_15)); }
	inline StealthModeCalculator_t3335204764 * get_stealthTimeCalc_15() const { return ___stealthTimeCalc_15; }
	inline StealthModeCalculator_t3335204764 ** get_address_of_stealthTimeCalc_15() { return &___stealthTimeCalc_15; }
	inline void set_stealthTimeCalc_15(StealthModeCalculator_t3335204764 * value)
	{
		___stealthTimeCalc_15 = value;
		Il2CppCodeGenWriteBarrier(&___stealthTimeCalc_15, value);
	}

	inline static int32_t get_offset_of_knockoutCalc_16() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___knockoutCalc_16)); }
	inline KnockoutCalculator_t371618716 * get_knockoutCalc_16() const { return ___knockoutCalc_16; }
	inline KnockoutCalculator_t371618716 ** get_address_of_knockoutCalc_16() { return &___knockoutCalc_16; }
	inline void set_knockoutCalc_16(KnockoutCalculator_t371618716 * value)
	{
		___knockoutCalc_16 = value;
		Il2CppCodeGenWriteBarrier(&___knockoutCalc_16, value);
	}

	inline static int32_t get_offset_of_chatButton_17() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___chatButton_17)); }
	inline DoubleStateButton_t1032633262 * get_chatButton_17() const { return ___chatButton_17; }
	inline DoubleStateButton_t1032633262 ** get_address_of_chatButton_17() { return &___chatButton_17; }
	inline void set_chatButton_17(DoubleStateButton_t1032633262 * value)
	{
		___chatButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___chatButton_17, value);
	}

	inline static int32_t get_offset_of_attackButton_18() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___attackButton_18)); }
	inline DoubleStateButton_t1032633262 * get_attackButton_18() const { return ___attackButton_18; }
	inline DoubleStateButton_t1032633262 ** get_address_of_attackButton_18() { return &___attackButton_18; }
	inline void set_attackButton_18(DoubleStateButton_t1032633262 * value)
	{
		___attackButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___attackButton_18, value);
	}

	inline static int32_t get_offset_of_globalFriendButton_19() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___globalFriendButton_19)); }
	inline DoubleStateButton_t1032633262 * get_globalFriendButton_19() const { return ___globalFriendButton_19; }
	inline DoubleStateButton_t1032633262 ** get_address_of_globalFriendButton_19() { return &___globalFriendButton_19; }
	inline void set_globalFriendButton_19(DoubleStateButton_t1032633262 * value)
	{
		___globalFriendButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___globalFriendButton_19, value);
	}

	inline static int32_t get_offset_of_requestSentLabel_20() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___requestSentLabel_20)); }
	inline UILabel_t1795115428 * get_requestSentLabel_20() const { return ___requestSentLabel_20; }
	inline UILabel_t1795115428 ** get_address_of_requestSentLabel_20() { return &___requestSentLabel_20; }
	inline void set_requestSentLabel_20(UILabel_t1795115428 * value)
	{
		___requestSentLabel_20 = value;
		Il2CppCodeGenWriteBarrier(&___requestSentLabel_20, value);
	}

	inline static int32_t get_offset_of_acceptFriendButton_21() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___acceptFriendButton_21)); }
	inline GameObject_t1756533147 * get_acceptFriendButton_21() const { return ___acceptFriendButton_21; }
	inline GameObject_t1756533147 ** get_address_of_acceptFriendButton_21() { return &___acceptFriendButton_21; }
	inline void set_acceptFriendButton_21(GameObject_t1756533147 * value)
	{
		___acceptFriendButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___acceptFriendButton_21, value);
	}

	inline static int32_t get_offset_of_declineFriendButton_22() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___declineFriendButton_22)); }
	inline GameObject_t1756533147 * get_declineFriendButton_22() const { return ___declineFriendButton_22; }
	inline GameObject_t1756533147 ** get_address_of_declineFriendButton_22() { return &___declineFriendButton_22; }
	inline void set_declineFriendButton_22(GameObject_t1756533147 * value)
	{
		___declineFriendButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___declineFriendButton_22, value);
	}

	inline static int32_t get_offset_of_actionLabel_23() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___actionLabel_23)); }
	inline UILabel_t1795115428 * get_actionLabel_23() const { return ___actionLabel_23; }
	inline UILabel_t1795115428 ** get_address_of_actionLabel_23() { return &___actionLabel_23; }
	inline void set_actionLabel_23(UILabel_t1795115428 * value)
	{
		___actionLabel_23 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_23, value);
	}

	inline static int32_t get_offset_of_timeLabel_24() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___timeLabel_24)); }
	inline UILabel_t1795115428 * get_timeLabel_24() const { return ___timeLabel_24; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_24() { return &___timeLabel_24; }
	inline void set_timeLabel_24(UILabel_t1795115428 * value)
	{
		___timeLabel_24 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_24, value);
	}

	inline static int32_t get_offset_of_activeColour_25() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___activeColour_25)); }
	inline Color_t2020392075  get_activeColour_25() const { return ___activeColour_25; }
	inline Color_t2020392075 * get_address_of_activeColour_25() { return &___activeColour_25; }
	inline void set_activeColour_25(Color_t2020392075  value)
	{
		___activeColour_25 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_26() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___inactiveColour_26)); }
	inline Color_t2020392075  get_inactiveColour_26() const { return ___inactiveColour_26; }
	inline Color_t2020392075 * get_address_of_inactiveColour_26() { return &___inactiveColour_26; }
	inline void set_inactiveColour_26(Color_t2020392075  value)
	{
		___inactiveColour_26 = value;
	}

	inline static int32_t get_offset_of_friendRequestColour_27() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___friendRequestColour_27)); }
	inline Color_t2020392075  get_friendRequestColour_27() const { return ___friendRequestColour_27; }
	inline Color_t2020392075 * get_address_of_friendRequestColour_27() { return &___friendRequestColour_27; }
	inline void set_friendRequestColour_27(Color_t2020392075  value)
	{
		___friendRequestColour_27 = value;
	}

	inline static int32_t get_offset_of_scrollView_28() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___scrollView_28)); }
	inline UIScrollView_t3033954930 * get_scrollView_28() const { return ___scrollView_28; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_28() { return &___scrollView_28; }
	inline void set_scrollView_28(UIScrollView_t3033954930 * value)
	{
		___scrollView_28 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_28, value);
	}

	inline static int32_t get_offset_of_draggablePanel_29() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___draggablePanel_29)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_29() const { return ___draggablePanel_29; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_29() { return &___draggablePanel_29; }
	inline void set_draggablePanel_29(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_29 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_29, value);
	}

	inline static int32_t get_offset_of_mTrans_30() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___mTrans_30)); }
	inline Transform_t3275118058 * get_mTrans_30() const { return ___mTrans_30; }
	inline Transform_t3275118058 ** get_address_of_mTrans_30() { return &___mTrans_30; }
	inline void set_mTrans_30(Transform_t3275118058 * value)
	{
		___mTrans_30 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_30, value);
	}

	inline static int32_t get_offset_of_mScroll_31() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___mScroll_31)); }
	inline UIScrollView_t3033954930 * get_mScroll_31() const { return ___mScroll_31; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_31() { return &___mScroll_31; }
	inline void set_mScroll_31(UIScrollView_t3033954930 * value)
	{
		___mScroll_31 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_31, value);
	}

	inline static int32_t get_offset_of_mAutoFind_32() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___mAutoFind_32)); }
	inline bool get_mAutoFind_32() const { return ___mAutoFind_32; }
	inline bool* get_address_of_mAutoFind_32() { return &___mAutoFind_32; }
	inline void set_mAutoFind_32(bool value)
	{
		___mAutoFind_32 = value;
	}

	inline static int32_t get_offset_of_mStarted_33() { return static_cast<int32_t>(offsetof(AttackListItem_t3405986821, ___mStarted_33)); }
	inline bool get_mStarted_33() const { return ___mStarted_33; }
	inline bool* get_address_of_mStarted_33() { return &___mStarted_33; }
	inline void set_mStarted_33(bool value)
	{
		___mStarted_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
