﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenRotation
struct TweenRotation_t1747194511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadarSweep
struct  RadarSweep_t3474354092  : public MonoBehaviour_t1158329972
{
public:
	// TweenRotation RadarSweep::rotTween
	TweenRotation_t1747194511 * ___rotTween_2;

public:
	inline static int32_t get_offset_of_rotTween_2() { return static_cast<int32_t>(offsetof(RadarSweep_t3474354092, ___rotTween_2)); }
	inline TweenRotation_t1747194511 * get_rotTween_2() const { return ___rotTween_2; }
	inline TweenRotation_t1747194511 ** get_address_of_rotTween_2() { return &___rotTween_2; }
	inline void set_rotTween_2(TweenRotation_t1747194511 * value)
	{
		___rotTween_2 = value;
		Il2CppCodeGenWriteBarrier(&___rotTween_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
