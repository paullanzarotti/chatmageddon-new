﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FullScreenLoadingUIScaler
struct FullScreenLoadingUIScaler_t2632697573;

#include "codegen/il2cpp-codegen.h"

// System.Void FullScreenLoadingUIScaler::.ctor()
extern "C"  void FullScreenLoadingUIScaler__ctor_m2304515404 (FullScreenLoadingUIScaler_t2632697573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingUIScaler::GlobalUIScale()
extern "C"  void FullScreenLoadingUIScaler_GlobalUIScale_m4171047087 (FullScreenLoadingUIScaler_t2632697573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
