﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Uniject.Impl.UnityLevelLoadListener
struct  UnityLevelLoadListener_t2593383553  : public MonoBehaviour_t1158329972
{
public:
	// System.Action Uniject.Impl.UnityLevelLoadListener::listener
	Action_t3226471752 * ___listener_2;

public:
	inline static int32_t get_offset_of_listener_2() { return static_cast<int32_t>(offsetof(UnityLevelLoadListener_t2593383553, ___listener_2)); }
	inline Action_t3226471752 * get_listener_2() const { return ___listener_2; }
	inline Action_t3226471752 ** get_address_of_listener_2() { return &___listener_2; }
	inline void set_listener_2(Action_t3226471752 * value)
	{
		___listener_2 = value;
		Il2CppCodeGenWriteBarrier(&___listener_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
