﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// KnockoutCalculator/OnZeroHit
struct OnZeroHit_t581944179;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KnockoutCalculator
struct  KnockoutCalculator_t371618716  : public MonoBehaviour_t1158329972
{
public:
	// UILabel KnockoutCalculator::timeLabel
	UILabel_t1795115428 * ___timeLabel_2;
	// UnityEngine.Coroutine KnockoutCalculator::calcRoutine
	Coroutine_t2299508840 * ___calcRoutine_3;
	// UnityEngine.Color KnockoutCalculator::negativeColour
	Color_t2020392075  ___negativeColour_4;
	// KnockoutCalculator/OnZeroHit KnockoutCalculator::onZeroHit
	OnZeroHit_t581944179 * ___onZeroHit_5;

public:
	inline static int32_t get_offset_of_timeLabel_2() { return static_cast<int32_t>(offsetof(KnockoutCalculator_t371618716, ___timeLabel_2)); }
	inline UILabel_t1795115428 * get_timeLabel_2() const { return ___timeLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_2() { return &___timeLabel_2; }
	inline void set_timeLabel_2(UILabel_t1795115428 * value)
	{
		___timeLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_2, value);
	}

	inline static int32_t get_offset_of_calcRoutine_3() { return static_cast<int32_t>(offsetof(KnockoutCalculator_t371618716, ___calcRoutine_3)); }
	inline Coroutine_t2299508840 * get_calcRoutine_3() const { return ___calcRoutine_3; }
	inline Coroutine_t2299508840 ** get_address_of_calcRoutine_3() { return &___calcRoutine_3; }
	inline void set_calcRoutine_3(Coroutine_t2299508840 * value)
	{
		___calcRoutine_3 = value;
		Il2CppCodeGenWriteBarrier(&___calcRoutine_3, value);
	}

	inline static int32_t get_offset_of_negativeColour_4() { return static_cast<int32_t>(offsetof(KnockoutCalculator_t371618716, ___negativeColour_4)); }
	inline Color_t2020392075  get_negativeColour_4() const { return ___negativeColour_4; }
	inline Color_t2020392075 * get_address_of_negativeColour_4() { return &___negativeColour_4; }
	inline void set_negativeColour_4(Color_t2020392075  value)
	{
		___negativeColour_4 = value;
	}

	inline static int32_t get_offset_of_onZeroHit_5() { return static_cast<int32_t>(offsetof(KnockoutCalculator_t371618716, ___onZeroHit_5)); }
	inline OnZeroHit_t581944179 * get_onZeroHit_5() const { return ___onZeroHit_5; }
	inline OnZeroHit_t581944179 ** get_address_of_onZeroHit_5() { return &___onZeroHit_5; }
	inline void set_onZeroHit_5(OnZeroHit_t581944179 * value)
	{
		___onZeroHit_5 = value;
		Il2CppCodeGenWriteBarrier(&___onZeroHit_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
