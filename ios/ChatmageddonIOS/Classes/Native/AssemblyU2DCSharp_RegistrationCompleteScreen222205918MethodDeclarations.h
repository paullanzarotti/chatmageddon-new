﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegistrationCompleteScreen
struct RegistrationCompleteScreen_t222205918;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void RegistrationCompleteScreen::.ctor()
extern "C"  void RegistrationCompleteScreen__ctor_m3222671147 (RegistrationCompleteScreen_t222205918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationCompleteScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void RegistrationCompleteScreen_ScreenLoad_m3667870125 (RegistrationCompleteScreen_t222205918 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationCompleteScreen::ScreenUnload(NavigationDirection)
extern "C"  void RegistrationCompleteScreen_ScreenUnload_m3559470073 (RegistrationCompleteScreen_t222205918 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegistrationCompleteScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool RegistrationCompleteScreen_ValidateScreenNavigation_m2621383730 (RegistrationCompleteScreen_t222205918 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegistrationCompleteScreen::CheckContentFilled()
extern "C"  bool RegistrationCompleteScreen_CheckContentFilled_m3974243150 (RegistrationCompleteScreen_t222205918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
