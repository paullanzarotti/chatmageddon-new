﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnterPhoneNumberButton
struct  EnterPhoneNumberButton_t3549119  : public SFXButton_t792651341
{
public:
	// System.Boolean EnterPhoneNumberButton::attack
	bool ___attack_5;

public:
	inline static int32_t get_offset_of_attack_5() { return static_cast<int32_t>(offsetof(EnterPhoneNumberButton_t3549119, ___attack_5)); }
	inline bool get_attack_5() const { return ___attack_5; }
	inline bool* get_address_of_attack_5() { return &___attack_5; }
	inline void set_attack_5(bool value)
	{
		___attack_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
