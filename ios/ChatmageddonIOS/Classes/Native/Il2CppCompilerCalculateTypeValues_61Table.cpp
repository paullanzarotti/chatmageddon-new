﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsHomeScreenList2265775114.h"
#include "AssemblyU2DCSharp_SettingsManager2519859232.h"
#include "AssemblyU2DCSharp_SettingsManager_U3CGetAboutAndHe4234509107.h"
#include "AssemblyU2DCSharp_SettingsNavigateBackwardsButton350955144.h"
#include "AssemblyU2DCSharp_SettingsNavigateForwardsButton3659026136.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "AssemblyU2DCSharp_SettingsNavigationController2581944457.h"
#include "AssemblyU2DCSharp_SettingsNavigationController_U3CS861775105.h"
#include "AssemblyU2DCSharp_SettingsNavigationController_U3C2112152008.h"
#include "AssemblyU2DCSharp_SettingsProfileContent3280006997.h"
#include "AssemblyU2DCSharp_SettingsProfileNavScreen3587426313.h"
#include "AssemblyU2DCSharp_SettingsProfileScreenList677344198.h"
#include "AssemblyU2DCSharp_SettingsSceneManager3860594814.h"
#include "AssemblyU2DCSharp_SettingsSwitchButton2514412055.h"
#include "AssemblyU2DCSharp_SettingsUnblockButton3175045615.h"
#include "AssemblyU2DCSharp_StealthModeResetTimer2296140228.h"
#include "AssemblyU2DCSharp_StealthModeResetTimer_U3CCalcula3966959274.h"
#include "AssemblyU2DCSharp_StealthModeNavScreen2705902227.h"
#include "AssemblyU2DCSharp_StealthModeNavScreen_U3CSaveScree664457975.h"
#include "AssemblyU2DCSharp_StealthModeNavScreen_U3CSaveScree787649662.h"
#include "AssemblyU2DCSharp_StealthModeSlider1957547627.h"
#include "AssemblyU2DCSharp_TOSNavScreen2379335315.h"
#include "AssemblyU2DCSharp_TextPageNavScreen1592130049.h"
#include "AssemblyU2DCSharp_AAndHUIScaler3026888490.h"
#include "AssemblyU2DCSharp_BlockListItemUIScaler2422342852.h"
#include "AssemblyU2DCSharp_EditNameUIScaler1842443211.h"
#include "AssemblyU2DCSharp_EditUsernameUIScaler3788521078.h"
#include "AssemblyU2DCSharp_FAQListItemUIScaler4047552821.h"
#include "AssemblyU2DCSharp_GenderUIScaler2269357119.h"
#include "AssemblyU2DCSharp_MultiswitchUIScaler3927566935.h"
#include "AssemblyU2DCSharp_ScrollViewUIScaler198225486.h"
#include "AssemblyU2DCSharp_SelectAudienceUIScaler620524668.h"
#include "AssemblyU2DCSharp_SettingsHomeUIScaler3836684136.h"
#include "AssemblyU2DCSharp_SettingsProfileUIScaler1908809364.h"
#include "AssemblyU2DCSharp_SettingsUIScaler565922441.h"
#include "AssemblyU2DCSharp_StealthModeScreenUIScaler3797850786.h"
#include "AssemblyU2DCSharp_TextPageUIScaler3568234746.h"
#include "AssemblyU2DCSharp_UpdatePasswordUIScaler4097640474.h"
#include "AssemblyU2DCSharp_UnloadLogoutUIButton2627629715.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav2102371893.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav_U3CSaveScreenC2254993109.h"
#include "AssemblyU2DCSharp_UpdatePasswordNav_U3CSaveScreenC1305955170.h"
#include "AssemblyU2DCSharp_loadLogoutUIButton4103518288.h"
#include "AssemblyU2DCSharp_ShopPurchasableItems522663808.h"
#include "AssemblyU2DCSharp_ChatmageddonPurchaseManager1221205491.h"
#include "AssemblyU2DCSharp_ChatmageddonPurchaseManager_U3CF2861303969.h"
#include "AssemblyU2DCSharp_PurchaseStoreItemButton3998308465.h"
#include "AssemblyU2DCSharp_ePurchaseType3166712560.h"
#include "AssemblyU2DCSharp_UnibillManager3725383138.h"
#include "AssemblyU2DCSharp_Music799847391.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"
#include "AssemblyU2DCSharp_BaseAudioSource2241787848.h"
#include "AssemblyU2DCSharp_BaseAudioSource_U3CHideObjectChe2459230211.h"
#include "AssemblyU2DCSharp_BaseAudioSource_U3CFadeInSourceU4157317973.h"
#include "AssemblyU2DCSharp_BaseAudioSource_U3CFadeOutSourceU241504347.h"
#include "AssemblyU2DCSharp_MusicSource904181206.h"
#include "AssemblyU2DCSharp_SFXSource383013662.h"
#include "AssemblyU2DCSharp_SFXSource_U3CHideObjectCheckU3Ec3897528497.h"
#include "AssemblyU2DCSharp_ListDirection3279055357.h"
#include "AssemblyU2DCSharp_AnchorPoint970610655.h"
#include "AssemblyU2DCSharp_BuildPlatform673526295.h"
#include "AssemblyU2DCSharp_Developer680803182.h"
#include "AssemblyU2DCSharp_BuildManager19508555.h"
#include "AssemblyU2DCSharp_ColourStyleSheet290320840.h"
#include "AssemblyU2DCSharp_GUIComponentStyle3347948829.h"
#include "AssemblyU2DCSharp_LoadFromTextFile2022748909.h"
#include "AssemblyU2DCSharp_PlayerPrefSaveData170835669.h"
#include "AssemblyU2DCSharp_SaveToTextFile3460715323.h"
#include "AssemblyU2DCSharp_StringExtensions1398449501.h"
#include "AssemblyU2DCSharp_RecursiveCharacterPopulator271140971.h"
#include "AssemblyU2DCSharp_RecursiveCharacterPopulator_OnPopu30122867.h"
#include "AssemblyU2DCSharp_RecursiveCharacterPopulator_OnPo1952569739.h"
#include "AssemblyU2DCSharp_RecursiveCharacterPopulator_U3CTe993898276.h"
#include "AssemblyU2DCSharp_VibrationType1951301914.h"
#include "AssemblyU2DCSharp_VibrationController2976556198.h"
#include "AssemblyU2DCSharp_VibrationController_U3CContinuou1905345410.h"
#include "AssemblyU2DCSharp_VibrationController_U3CPulseVibr3172871213.h"
#include "AssemblyU2DCSharp_VibrationController_U3CSingleVib2980562147.h"
#include "AssemblyU2DCSharp_VibrationController_U3CSwitchVib2167946060.h"
#include "AssemblyU2DCSharp_CameraController3555666667.h"
#include "AssemblyU2DCSharp_ClickableObject3964308383.h"
#include "AssemblyU2DCSharp_SwipeType596416270.h"
#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"
#include "AssemblyU2DCSharp_RotateButton2565449149.h"
#include "AssemblyU2DCSharp_RotateButton_U3CRotateU3Ec__Iter2725296188.h"
#include "AssemblyU2DCSharp_SliderNumberLabel290801982.h"
#include "AssemblyU2DCSharp_SliderDirection603010170.h"
#include "AssemblyU2DCSharp_SliderController2812089311.h"
#include "AssemblyU2DCSharp_SwipeNavigationController3767682136.h"
#include "AssemblyU2DCSharp_GpsService3478080739.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6100 = { sizeof (SettingsHomeScreenList_t2265775114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6100[7] = 
{
	SettingsHomeScreenList_t2265775114::get_offset_of_table_2(),
	SettingsHomeScreenList_t2265775114::get_offset_of_draggablePanel_3(),
	SettingsHomeScreenList_t2265775114::get_offset_of_listLoaded_4(),
	SettingsHomeScreenList_t2265775114::get_offset_of_itemPrefab_5(),
	SettingsHomeScreenList_t2265775114::get_offset_of_content_6(),
	SettingsHomeScreenList_t2265775114::get_offset_of_smActiveColour_7(),
	SettingsHomeScreenList_t2265775114::get_offset_of_smInactiveColour_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6101 = { sizeof (SettingsManager_t2519859232), -1, sizeof(SettingsManager_t2519859232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6101[10] = 
{
	SettingsManager_t2519859232::get_offset_of_uiToLoad_3(),
	SettingsManager_t2519859232::get_offset_of_questionText_4(),
	SettingsManager_t2519859232::get_offset_of_anwserText_5(),
	SettingsManager_t2519859232::get_offset_of_instructionsText_6(),
	SettingsManager_t2519859232::get_offset_of_eulaText_7(),
	SettingsManager_t2519859232::get_offset_of_privacyPolicyText_8(),
	SettingsManager_t2519859232::get_offset_of_tosText_9(),
	SettingsManager_t2519859232::get_offset_of_faqList_10(),
	SettingsManager_t2519859232::get_offset_of_blockList_11(),
	SettingsManager_t2519859232_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6102 = { sizeof (U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6102[2] = 
{
	U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107::get_offset_of_type_0(),
	U3CGetAboutAndHelpInformationU3Ec__AnonStorey0_t4234509107::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6103 = { sizeof (SettingsNavigateBackwardsButton_t350955144), -1, sizeof(SettingsNavigateBackwardsButton_t350955144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6103[5] = 
{
	SettingsNavigateBackwardsButton_t350955144::get_offset_of_sfxToPlay_2(),
	SettingsNavigateBackwardsButton_t350955144::get_offset_of_saveContent_3(),
	SettingsNavigateBackwardsButton_t350955144::get_offset_of_buttonSprite_4(),
	SettingsNavigateBackwardsButton_t350955144::get_offset_of_buttonLabel_5(),
	SettingsNavigateBackwardsButton_t350955144_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6104 = { sizeof (SettingsNavigateForwardsButton_t3659026136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6104[1] = 
{
	SettingsNavigateForwardsButton_t3659026136::get_offset_of_navScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6105 = { sizeof (SettingsNavScreen_t2267127234)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6105[22] = 
{
	SettingsNavScreen_t2267127234::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6106 = { sizeof (SettingsNavigationController_t2581944457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6106[23] = 
{
	SettingsNavigationController_t2581944457::get_offset_of_titleLabel_8(),
	SettingsNavigationController_t2581944457::get_offset_of_editUI_9(),
	SettingsNavigationController_t2581944457::get_offset_of_homeObject_10(),
	SettingsNavigationController_t2581944457::get_offset_of_profileObject_11(),
	SettingsNavigationController_t2581944457::get_offset_of_editNameObject_12(),
	SettingsNavigationController_t2581944457::get_offset_of_selectAudienceObject_13(),
	SettingsNavigationController_t2581944457::get_offset_of_stealthModeObject_14(),
	SettingsNavigationController_t2581944457::get_offset_of_aboutAndHelpObject_15(),
	SettingsNavigationController_t2581944457::get_offset_of_faqsObject_16(),
	SettingsNavigationController_t2581944457::get_offset_of_textPageObject_17(),
	SettingsNavigationController_t2581944457::get_offset_of_notificationsObject_18(),
	SettingsNavigationController_t2581944457::get_offset_of_blockListObject_19(),
	SettingsNavigationController_t2581944457::get_offset_of_genderObject_20(),
	SettingsNavigationController_t2581944457::get_offset_of_editUsernameObject_21(),
	SettingsNavigationController_t2581944457::get_offset_of_editEmailObject_22(),
	SettingsNavigationController_t2581944457::get_offset_of_updatePasswordObject_23(),
	SettingsNavigationController_t2581944457::get_offset_of_audioObject_24(),
	SettingsNavigationController_t2581944457::get_offset_of_mainNavigateBackwardButton_25(),
	SettingsNavigationController_t2581944457::get_offset_of_mainNavigateForwardButton_26(),
	SettingsNavigationController_t2581944457::get_offset_of_activePos_27(),
	SettingsNavigationController_t2581944457::get_offset_of_leftInactivePos_28(),
	SettingsNavigationController_t2581944457::get_offset_of_rightInactivePos_29(),
	SettingsNavigationController_t2581944457::get_offset_of_logout_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6107 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t861775105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6107[9] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_U3CsaveObjectU3E__0_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_screen_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_direction_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_savedAction_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_U24this_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_U24current_5(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_U24disposing_6(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_U24PC_7(),
	U3CSaveScreenContentU3Ec__Iterator0_t861775105::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6108 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6108[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t2112152008::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6109 = { sizeof (SettingsProfileContent_t3280006997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6109[7] = 
{
	SettingsProfileContent_t3280006997::get_offset_of_avatar_2(),
	SettingsProfileContent_t3280006997::get_offset_of_firstNameLabel_3(),
	SettingsProfileContent_t3280006997::get_offset_of_lastNameLabel_4(),
	SettingsProfileContent_t3280006997::get_offset_of_userNameLabel_5(),
	SettingsProfileContent_t3280006997::get_offset_of_genderLabel_6(),
	SettingsProfileContent_t3280006997::get_offset_of_audienceLabel_7(),
	SettingsProfileContent_t3280006997::get_offset_of_locationSwitch_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6110 = { sizeof (SettingsProfileNavScreen_t3587426313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6110[2] = 
{
	SettingsProfileNavScreen_t3587426313::get_offset_of_profileList_3(),
	SettingsProfileNavScreen_t3587426313::get_offset_of_UIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6111 = { sizeof (SettingsProfileScreenList_t677344198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6111[5] = 
{
	SettingsProfileScreenList_t677344198::get_offset_of_table_2(),
	SettingsProfileScreenList_t677344198::get_offset_of_draggablePanel_3(),
	SettingsProfileScreenList_t677344198::get_offset_of_listLoaded_4(),
	SettingsProfileScreenList_t677344198::get_offset_of_content_5(),
	SettingsProfileScreenList_t677344198::get_offset_of_itemPrefab_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6112 = { sizeof (SettingsSceneManager_t3860594814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6112[4] = 
{
	SettingsSceneManager_t3860594814::get_offset_of_SettingsCenterUI_17(),
	SettingsSceneManager_t3860594814::get_offset_of_DeviceCameraUI_18(),
	SettingsSceneManager_t3860594814::get_offset_of_DeviceShareUI_19(),
	SettingsSceneManager_t3860594814::get_offset_of_ErrorMessageUI_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6113 = { sizeof (SettingsSwitchButton_t2514412055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6113[8] = 
{
	SettingsSwitchButton_t2514412055::get_offset_of_switchTween_4(),
	SettingsSwitchButton_t2514412055::get_offset_of_backgroundColourTween_5(),
	SettingsSwitchButton_t2514412055::get_offset_of_switchStartState_6(),
	SettingsSwitchButton_t2514412055::get_offset_of_falsePos_7(),
	SettingsSwitchButton_t2514412055::get_offset_of_truePos_8(),
	SettingsSwitchButton_t2514412055::get_offset_of_backgroundInactiveColour_9(),
	SettingsSwitchButton_t2514412055::get_offset_of_backgroundActiveColour_10(),
	SettingsSwitchButton_t2514412055::get_offset_of_startSet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6114 = { sizeof (SettingsUnblockButton_t3175045615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6114[1] = 
{
	SettingsUnblockButton_t3175045615::get_offset_of_item_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6115 = { sizeof (StealthModeResetTimer_t2296140228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6115[3] = 
{
	StealthModeResetTimer_t2296140228::get_offset_of_timeLabel_2(),
	StealthModeResetTimer_t2296140228::get_offset_of_descriptionLabel_3(),
	StealthModeResetTimer_t2296140228::get_offset_of_calcRoutine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6116 = { sizeof (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6116[6] = 
{
	U3CCalculateResetTimeU3Ec__Iterator0_t3966959274::get_offset_of_U3CtempTimeU3E__0_0(),
	U3CCalculateResetTimeU3Ec__Iterator0_t3966959274::get_offset_of_U3CdifferenceU3E__1_1(),
	U3CCalculateResetTimeU3Ec__Iterator0_t3966959274::get_offset_of_U24this_2(),
	U3CCalculateResetTimeU3Ec__Iterator0_t3966959274::get_offset_of_U24current_3(),
	U3CCalculateResetTimeU3Ec__Iterator0_t3966959274::get_offset_of_U24disposing_4(),
	U3CCalculateResetTimeU3Ec__Iterator0_t3966959274::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6117 = { sizeof (StealthModeNavScreen_t2705902227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6117[6] = 
{
	StealthModeNavScreen_t2705902227::get_offset_of_hourPicker_3(),
	StealthModeNavScreen_t2705902227::get_offset_of_minPicker_4(),
	StealthModeNavScreen_t2705902227::get_offset_of_durationSlider_5(),
	StealthModeNavScreen_t2705902227::get_offset_of_endTimeLabel_6(),
	StealthModeNavScreen_t2705902227::get_offset_of_resetTimer_7(),
	StealthModeNavScreen_t2705902227::get_offset_of_UIclosing_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6118 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t664457975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6118[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t664457975::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t664457975::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t664457975::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t664457975::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t664457975::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t664457975::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6119 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t787649662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6119[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t787649662::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t787649662::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6120 = { sizeof (StealthModeSlider_t1957547627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6121 = { sizeof (TOSNavScreen_t2379335315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6121[1] = 
{
	TOSNavScreen_t2379335315::get_offset_of_UIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6122 = { sizeof (TextPageNavScreen_t1592130049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6122[8] = 
{
	TextPageNavScreen_t1592130049::get_offset_of_scrollViewPanel_3(),
	TextPageNavScreen_t1592130049::get_offset_of_questionUI_4(),
	TextPageNavScreen_t1592130049::get_offset_of_contactUI_5(),
	TextPageNavScreen_t1592130049::get_offset_of_instructionsUI_6(),
	TextPageNavScreen_t1592130049::get_offset_of_eulaUI_7(),
	TextPageNavScreen_t1592130049::get_offset_of_ppUI_8(),
	TextPageNavScreen_t1592130049::get_offset_of_tosUI_9(),
	TextPageNavScreen_t1592130049::get_offset_of_UIclosing_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6123 = { sizeof (AAndHUIScaler_t3026888490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6123[4] = 
{
	AAndHUIScaler_t3026888490::get_offset_of_colliders_14(),
	AAndHUIScaler_t3026888490::get_offset_of_leftTransforms_15(),
	AAndHUIScaler_t3026888490::get_offset_of_rightTransforms_16(),
	AAndHUIScaler_t3026888490::get_offset_of_dividers_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6124 = { sizeof (BlockListItemUIScaler_t2422342852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6124[6] = 
{
	BlockListItemUIScaler_t2422342852::get_offset_of_background_14(),
	BlockListItemUIScaler_t2422342852::get_offset_of_backgroundCollider_15(),
	BlockListItemUIScaler_t2422342852::get_offset_of_profilePicture_16(),
	BlockListItemUIScaler_t2422342852::get_offset_of_nameLabel_17(),
	BlockListItemUIScaler_t2422342852::get_offset_of_unblockButton_18(),
	BlockListItemUIScaler_t2422342852::get_offset_of_actionLabel_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6125 = { sizeof (EditNameUIScaler_t1842443211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6125[6] = 
{
	EditNameUIScaler_t1842443211::get_offset_of_firstNameLabel_14(),
	EditNameUIScaler_t1842443211::get_offset_of_firstNameCollider_15(),
	EditNameUIScaler_t1842443211::get_offset_of_firstNameDivider_16(),
	EditNameUIScaler_t1842443211::get_offset_of_lastNameLabel_17(),
	EditNameUIScaler_t1842443211::get_offset_of_lastNameCollider_18(),
	EditNameUIScaler_t1842443211::get_offset_of_lastNameDivider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6126 = { sizeof (EditUsernameUIScaler_t3788521078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6126[3] = 
{
	EditUsernameUIScaler_t3788521078::get_offset_of_usernameLabel_14(),
	EditUsernameUIScaler_t3788521078::get_offset_of_usernameCollider_15(),
	EditUsernameUIScaler_t3788521078::get_offset_of_usernameDivider_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6127 = { sizeof (FAQListItemUIScaler_t4047552821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6127[5] = 
{
	FAQListItemUIScaler_t4047552821::get_offset_of_itemCollider_14(),
	FAQListItemUIScaler_t4047552821::get_offset_of_background_15(),
	FAQListItemUIScaler_t4047552821::get_offset_of_leftTransform_16(),
	FAQListItemUIScaler_t4047552821::get_offset_of_rightTransform_17(),
	FAQListItemUIScaler_t4047552821::get_offset_of_divider_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6128 = { sizeof (GenderUIScaler_t2269357119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6128[8] = 
{
	GenderUIScaler_t2269357119::get_offset_of_genderSwitch_14(),
	GenderUIScaler_t2269357119::get_offset_of_tickSprite_15(),
	GenderUIScaler_t2269357119::get_offset_of_maleLabel_16(),
	GenderUIScaler_t2269357119::get_offset_of_maleCollider_17(),
	GenderUIScaler_t2269357119::get_offset_of_maleDivider_18(),
	GenderUIScaler_t2269357119::get_offset_of_femaleLabel_19(),
	GenderUIScaler_t2269357119::get_offset_of_femaleCollider_20(),
	GenderUIScaler_t2269357119::get_offset_of_femaleDivider_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6129 = { sizeof (MultiswitchUIScaler_t3927566935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6129[3] = 
{
	MultiswitchUIScaler_t3927566935::get_offset_of_leftTransforms_14(),
	MultiswitchUIScaler_t3927566935::get_offset_of_rightTransforms_15(),
	MultiswitchUIScaler_t3927566935::get_offset_of_dividers_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6130 = { sizeof (ScrollViewUIScaler_t198225486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6130[1] = 
{
	ScrollViewUIScaler_t198225486::get_offset_of_scrollView_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6131 = { sizeof (SelectAudienceUIScaler_t620524668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6131[13] = 
{
	SelectAudienceUIScaler_t620524668::get_offset_of_titleLabel_14(),
	SelectAudienceUIScaler_t620524668::get_offset_of_titleDivider_15(),
	SelectAudienceUIScaler_t620524668::get_offset_of_saSwitch_16(),
	SelectAudienceUIScaler_t620524668::get_offset_of_tickSprite_17(),
	SelectAudienceUIScaler_t620524668::get_offset_of_everyoneLabel_18(),
	SelectAudienceUIScaler_t620524668::get_offset_of_everyoneCollider_19(),
	SelectAudienceUIScaler_t620524668::get_offset_of_everyoneDivider_20(),
	SelectAudienceUIScaler_t620524668::get_offset_of_contactsLabel_21(),
	SelectAudienceUIScaler_t620524668::get_offset_of_contactsCollider_22(),
	SelectAudienceUIScaler_t620524668::get_offset_of_contactsDivider_23(),
	SelectAudienceUIScaler_t620524668::get_offset_of_nobodyLabel_24(),
	SelectAudienceUIScaler_t620524668::get_offset_of_nobodyCollider_25(),
	SelectAudienceUIScaler_t620524668::get_offset_of_nobodyDivider_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6132 = { sizeof (SettingsHomeUIScaler_t3836684136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6132[40] = 
{
	SettingsHomeUIScaler_t3836684136::get_offset_of_topBarrier_14(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_profilePicture_15(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_profileNames_16(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_topDivider_17(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateProfileLabel_18(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateProfileNav_19(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateProfileCollider_20(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateProfileDivider_21(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_stealthMode_22(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_stealthModeCollider_23(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_stealthModeDivider_24(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateStealthModeLabel_25(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateStealthModeNav_26(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateStealthModeCollider_27(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_updateStealthModeDivider_28(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_middleCollider_29(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_aAndBTopDivider_30(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_aAndBLabel_31(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_aAndBNav_32(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_aAndBCollider_33(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_aAndBBottomDivider_34(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_notificationsLabel_35(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_notificationsNav_36(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_notificationsCollider_37(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_notificationsDivider_38(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_audioLabel_39(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_audioNav_40(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_audioCollider_41(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_audioDivider_42(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_tellAFriendLabel_43(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_tellAFriendCollider_44(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_tellAFriendDivider_45(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_rateThisAppLabel_46(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_rateThisAppCollider_47(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_rateThisAppDivider_48(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_bottomCollider_49(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_logoutLabel_50(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_logoutNav_51(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_logoutCollider_52(),
	SettingsHomeUIScaler_t3836684136::get_offset_of_logoutDivider_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6133 = { sizeof (SettingsProfileUIScaler_t1908809364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6133[9] = 
{
	SettingsProfileUIScaler_t1908809364::get_offset_of_profilePicture_14(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_profileNames_15(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_displayLoactionSwitch_16(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_displayLocationSwitchCollider_17(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_displayLocationCollider_18(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_labels_19(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_navSprites_20(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_dividers_21(),
	SettingsProfileUIScaler_t1908809364::get_offset_of_colliders_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6134 = { sizeof (SettingsUIScaler_t565922441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6134[11] = 
{
	SettingsUIScaler_t565922441::get_offset_of_background_14(),
	SettingsUIScaler_t565922441::get_offset_of_seperator_15(),
	SettingsUIScaler_t565922441::get_offset_of_navBack_16(),
	SettingsUIScaler_t565922441::get_offset_of_navCancel_17(),
	SettingsUIScaler_t565922441::get_offset_of_navSave_18(),
	SettingsUIScaler_t565922441::get_offset_of_homeScreenPanel_19(),
	SettingsUIScaler_t565922441::get_offset_of_profileScreenPanel_20(),
	SettingsUIScaler_t565922441::get_offset_of_logoutBackground_21(),
	SettingsUIScaler_t565922441::get_offset_of_logoutCollider_22(),
	SettingsUIScaler_t565922441::get_offset_of_activeStartLeftTween_23(),
	SettingsUIScaler_t565922441::get_offset_of_rightStartActiveTweens_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6135 = { sizeof (StealthModeScreenUIScaler_t3797850786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6135[1] = 
{
	StealthModeScreenUIScaler_t3797850786::get_offset_of_leftTransforms_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6136 = { sizeof (TextPageUIScaler_t3568234746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6136[2] = 
{
	TextPageUIScaler_t3568234746::get_offset_of_colliders_15(),
	TextPageUIScaler_t3568234746::get_offset_of_labels_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6137 = { sizeof (UpdatePasswordUIScaler_t4097640474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6137[9] = 
{
	UpdatePasswordUIScaler_t4097640474::get_offset_of_currentPasswordLabel_14(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_currentPasswordCollider_15(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_currentPasswordDivider_16(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_newPasswordLabel_17(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_newPasswordCollider_18(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_newPasswordDivider_19(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_confirmPasswordLabel_20(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_confirmPasswordCollider_21(),
	UpdatePasswordUIScaler_t4097640474::get_offset_of_confirmPasswordDivider_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6138 = { sizeof (UnloadLogoutUIButton_t2627629715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6138[2] = 
{
	UnloadLogoutUIButton_t2627629715::get_offset_of_logoutTransition_5(),
	UnloadLogoutUIButton_t2627629715::get_offset_of_logout_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6139 = { sizeof (UpdatePasswordNav_t2102371893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6139[4] = 
{
	UpdatePasswordNav_t2102371893::get_offset_of_currentPassword_3(),
	UpdatePasswordNav_t2102371893::get_offset_of_newPassword_4(),
	UpdatePasswordNav_t2102371893::get_offset_of_confirmPassword_5(),
	UpdatePasswordNav_t2102371893::get_offset_of_UIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6140 = { sizeof (U3CSaveScreenContentU3Ec__Iterator0_t2254993109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6140[6] = 
{
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109::get_offset_of_U24this_1(),
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109::get_offset_of_U24current_2(),
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109::get_offset_of_U24disposing_3(),
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109::get_offset_of_U24PC_4(),
	U3CSaveScreenContentU3Ec__Iterator0_t2254993109::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6141 = { sizeof (U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6141[2] = 
{
	U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170::get_offset_of_savedAction_0(),
	U3CSaveScreenContentU3Ec__AnonStorey1_t1305955170::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6142 = { sizeof (loadLogoutUIButton_t4103518288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6143 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6144 = { sizeof (ShopPurchasableItems_t522663808)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6144[2] = 
{
	ShopPurchasableItems_t522663808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6145 = { sizeof (ChatmageddonPurchaseManager_t1221205491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6146 = { sizeof (U3CFinaliseBucksPurchaseU3Ec__AnonStorey0_t2861303969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6146[1] = 
{
	U3CFinaliseBucksPurchaseU3Ec__AnonStorey0_t2861303969::get_offset_of_index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6147 = { sizeof (PurchaseStoreItemButton_t3998308465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6148 = { sizeof (ePurchaseType_t3166712560)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6148[4] = 
{
	ePurchaseType_t3166712560::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6149 = { sizeof (UnibillManager_t3725383138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6149[8] = 
{
	UnibillManager_t3725383138::get_offset_of_comboBoxList_3(),
	UnibillManager_t3725383138::get_offset_of_listStyle_4(),
	UnibillManager_t3725383138::get_offset_of_selectedItemIndex_5(),
	UnibillManager_t3725383138::get_offset_of_consumableItems_6(),
	UnibillManager_t3725383138::get_offset_of_nonConsumableItems_7(),
	UnibillManager_t3725383138::get_offset_of_subscriptionItems_8(),
	UnibillManager_t3725383138::get_offset_of_font_9(),
	UnibillManager_t3725383138::get_offset_of_unibillInitialised_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6150 = { sizeof (Music_t799847391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6150[7] = 
{
	Music_t799847391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6151 = { sizeof (SFX_t1271914439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6151[27] = 
{
	SFX_t1271914439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6152 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6152[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6153 = { sizeof (BaseAudioSource_t2241787848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6153[8] = 
{
	BaseAudioSource_t2241787848::get_offset_of_sourceActive_2(),
	BaseAudioSource_t2241787848::get_offset_of_source_3(),
	BaseAudioSource_t2241787848::get_offset_of_maxVolume_4(),
	BaseAudioSource_t2241787848::get_offset_of_minVolume_5(),
	BaseAudioSource_t2241787848::get_offset_of_audioDuration_6(),
	BaseAudioSource_t2241787848::get_offset_of_checking_7(),
	BaseAudioSource_t2241787848::get_offset_of_fadeInRoutine_8(),
	BaseAudioSource_t2241787848::get_offset_of_fadeOutRoutine_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6154 = { sizeof (U3CHideObjectCheckU3Ec__Iterator0_t2459230211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6154[4] = 
{
	U3CHideObjectCheckU3Ec__Iterator0_t2459230211::get_offset_of_U24this_0(),
	U3CHideObjectCheckU3Ec__Iterator0_t2459230211::get_offset_of_U24current_1(),
	U3CHideObjectCheckU3Ec__Iterator0_t2459230211::get_offset_of_U24disposing_2(),
	U3CHideObjectCheckU3Ec__Iterator0_t2459230211::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6155 = { sizeof (U3CFadeInSourceU3Ec__Iterator1_t4157317973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6155[9] = 
{
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U3CupdateFadeU3E__0_0(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_fadeDuration_1(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U3CdurationFractionU3E__1_2(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U3CcountU3E__2_3(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U3CvolumeAdditionU3E__3_4(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U24this_5(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U24current_6(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U24disposing_7(),
	U3CFadeInSourceU3Ec__Iterator1_t4157317973::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6156 = { sizeof (U3CFadeOutSourceU3Ec__Iterator2_t241504347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6156[10] = 
{
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U3CupdateFadeU3E__0_0(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_fadeDuration_1(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U3CdurationFractionU3E__1_2(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U3CcountU3E__2_3(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U3CvolumeAdditionU3E__3_4(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_pause_5(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U24this_6(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U24current_7(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U24disposing_8(),
	U3CFadeOutSourceU3Ec__Iterator2_t241504347::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6157 = { sizeof (MusicSource_t904181206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6157[1] = 
{
	MusicSource_t904181206::get_offset_of_musicType_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6158 = { sizeof (SFXSource_t383013662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6158[1] = 
{
	SFXSource_t383013662::get_offset_of_sfxType_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6159 = { sizeof (U3CHideObjectCheckU3Ec__Iterator0_t3897528497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6159[4] = 
{
	U3CHideObjectCheckU3Ec__Iterator0_t3897528497::get_offset_of_U24this_0(),
	U3CHideObjectCheckU3Ec__Iterator0_t3897528497::get_offset_of_U24current_1(),
	U3CHideObjectCheckU3Ec__Iterator0_t3897528497::get_offset_of_U24disposing_2(),
	U3CHideObjectCheckU3Ec__Iterator0_t3897528497::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6160 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6160[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6161 = { sizeof (ListDirection_t3279055357)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6161[5] = 
{
	ListDirection_t3279055357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6162 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6162[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6163 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6163[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6164 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6164[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6165 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6165[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6166 = { sizeof (AnchorPoint_t970610655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6166[11] = 
{
	AnchorPoint_t970610655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6167 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6167[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6168 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6168[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6169 = { sizeof (BuildPlatform_t673526295)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6169[5] = 
{
	BuildPlatform_t673526295::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6170 = { sizeof (Developer_t680803182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6170[6] = 
{
	Developer_t680803182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6171 = { sizeof (BuildManager_t19508555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6171[13] = 
{
	BuildManager_t19508555::get_offset_of_buildVersion_3(),
	BuildManager_t19508555::get_offset_of_developmentBuild_4(),
	BuildManager_t19508555::get_offset_of_buildPlatform_5(),
	BuildManager_t19508555::get_offset_of_developer_6(),
	BuildManager_t19508555::get_offset_of_iTunesAppId_7(),
	BuildManager_t19508555::get_offset_of_uiScalCalculated_8(),
	BuildManager_t19508555::get_offset_of_devResWidth_9(),
	BuildManager_t19508555::get_offset_of_devResHeight_10(),
	BuildManager_t19508555::get_offset_of_startScaleWidth_11(),
	BuildManager_t19508555::get_offset_of_startScaleHeight_12(),
	BuildManager_t19508555::get_offset_of_calcScaleAspect_13(),
	BuildManager_t19508555::get_offset_of_calcScaleWidth_14(),
	BuildManager_t19508555::get_offset_of_UDID_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6172 = { sizeof (ColourStyleSheet_t290320840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6172[2] = 
{
	ColourStyleSheet_t290320840::get_offset_of_colours_3(),
	ColourStyleSheet_t290320840::get_offset_of_styleNames_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6173 = { sizeof (GUIComponentStyle_t3347948829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6173[6] = 
{
	GUIComponentStyle_t3347948829::get_offset_of_componentSprite_2(),
	GUIComponentStyle_t3347948829::get_offset_of_componentTexture_3(),
	GUIComponentStyle_t3347948829::get_offset_of_componentLabel_4(),
	GUIComponentStyle_t3347948829::get_offset_of_spriteStyleName_5(),
	GUIComponentStyle_t3347948829::get_offset_of_textureName_6(),
	GUIComponentStyle_t3347948829::get_offset_of_labelSpriteName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6174 = { sizeof (LoadFromTextFile_t2022748909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6174[1] = 
{
	LoadFromTextFile_t2022748909::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6175 = { sizeof (PlayerPrefSaveData_t170835669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6176 = { sizeof (SaveToTextFile_t3460715323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6177 = { sizeof (StringExtensions_t1398449501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6178 = { sizeof (RecursiveCharacterPopulator_t271140971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6178[5] = 
{
	RecursiveCharacterPopulator_t271140971::get_offset_of_caretChoice_2(),
	RecursiveCharacterPopulator_t271140971::get_offset_of_timeBetweenCharacters_3(),
	RecursiveCharacterPopulator_t271140971::get_offset_of_onPopulationUpdate_4(),
	RecursiveCharacterPopulator_t271140971::get_offset_of_onPopulationComplete_5(),
	RecursiveCharacterPopulator_t271140971::get_offset_of_populator_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6179 = { sizeof (OnPopulationUpdate_t30122867), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6180 = { sizeof (OnPopulationComplete_t1952569739), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6181 = { sizeof (U3CTextPopulatorU3Ec__Iterator0_t993898276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6181[9] = 
{
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_label_0(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U3CdesiredTextU3E__0_1(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_textToUse_2(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U3CcharCountU3E__1_3(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U3CcurrentTextU3E__2_4(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U24this_5(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U24current_6(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U24disposing_7(),
	U3CTextPopulatorU3Ec__Iterator0_t993898276::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6182 = { sizeof (VibrationType_t1951301914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6182[5] = 
{
	VibrationType_t1951301914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6183 = { sizeof (VibrationController_t2976556198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6183[1] = 
{
	VibrationController_t2976556198::get_offset_of_vibrateActive_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6184 = { sizeof (U3CContinuousVibrateU3Ec__Iterator0_t1905345410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6184[6] = 
{
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410::get_offset_of_U3CtimeStartedU3E__0_0(),
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410::get_offset_of_duration_1(),
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410::get_offset_of_U24this_2(),
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410::get_offset_of_U24current_3(),
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410::get_offset_of_U24disposing_4(),
	U3CContinuousVibrateU3Ec__Iterator0_t1905345410::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6185 = { sizeof (U3CPulseVibrateU3Ec__Iterator1_t3172871213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6185[7] = 
{
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_U3CcountU3E__0_0(),
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_U3CtimeStartedU3E__1_1(),
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_duration_2(),
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_U24this_3(),
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_U24current_4(),
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_U24disposing_5(),
	U3CPulseVibrateU3Ec__Iterator1_t3172871213::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6186 = { sizeof (U3CSingleVibrateU3Ec__Iterator2_t2980562147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6186[6] = 
{
	U3CSingleVibrateU3Ec__Iterator2_t2980562147::get_offset_of_U3CtimeStartedU3E__0_0(),
	U3CSingleVibrateU3Ec__Iterator2_t2980562147::get_offset_of_duration_1(),
	U3CSingleVibrateU3Ec__Iterator2_t2980562147::get_offset_of_U24this_2(),
	U3CSingleVibrateU3Ec__Iterator2_t2980562147::get_offset_of_U24current_3(),
	U3CSingleVibrateU3Ec__Iterator2_t2980562147::get_offset_of_U24disposing_4(),
	U3CSingleVibrateU3Ec__Iterator2_t2980562147::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6187 = { sizeof (U3CSwitchVibrateU3Ec__Iterator3_t2167946060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6187[7] = 
{
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_U3CcountU3E__0_0(),
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_U3CtimeStartedU3E__1_1(),
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_duration_2(),
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_U24this_3(),
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_U24current_4(),
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_U24disposing_5(),
	U3CSwitchVibrateU3Ec__Iterator3_t2167946060::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6188 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6188[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6189 = { sizeof (CameraController_t3555666667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6190 = { sizeof (ClickableObject_t3964308383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6191 = { sizeof (SwipeType_t596416270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6191[3] = 
{
	SwipeType_t596416270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6192 = { sizeof (NGUISwipe_t4135300327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6192[8] = 
{
	NGUISwipe_t4135300327::get_offset_of_collision_2(),
	NGUISwipe_t4135300327::get_offset_of_swipeXThreshold_3(),
	NGUISwipe_t4135300327::get_offset_of_swipeYThreashold_4(),
	NGUISwipe_t4135300327::get_offset_of_swipeType_5(),
	NGUISwipe_t4135300327::get_offset_of_dragStart_6(),
	NGUISwipe_t4135300327::get_offset_of_dragcurrent_7(),
	NGUISwipe_t4135300327::get_offset_of_swipeCompleted_8(),
	NGUISwipe_t4135300327::get_offset_of_swipeVoid_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6193 = { sizeof (RotateButton_t2565449149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6193[5] = 
{
	RotateButton_t2565449149::get_offset_of_pressed_5(),
	RotateButton_t2565449149::get_offset_of_buttonSprite_6(),
	RotateButton_t2565449149::get_offset_of_buttonSpriteName_7(),
	RotateButton_t2565449149::get_offset_of_pressedSpriteName_8(),
	RotateButton_t2565449149::get_offset_of_rotating_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6194 = { sizeof (U3CRotateU3Ec__Iterator0_t2725296188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6194[6] = 
{
	U3CRotateU3Ec__Iterator0_t2725296188::get_offset_of_U3CrotationU3E__0_0(),
	U3CRotateU3Ec__Iterator0_t2725296188::get_offset_of_U3CrotationU3E__1_1(),
	U3CRotateU3Ec__Iterator0_t2725296188::get_offset_of_U24this_2(),
	U3CRotateU3Ec__Iterator0_t2725296188::get_offset_of_U24current_3(),
	U3CRotateU3Ec__Iterator0_t2725296188::get_offset_of_U24disposing_4(),
	U3CRotateU3Ec__Iterator0_t2725296188::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6195 = { sizeof (SliderNumberLabel_t290801982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6195[4] = 
{
	SliderNumberLabel_t290801982::get_offset_of_label_0(),
	SliderNumberLabel_t290801982::get_offset_of_measurementUnits_1(),
	SliderNumberLabel_t290801982::get_offset_of_singleUnit_2(),
	SliderNumberLabel_t290801982::get_offset_of_pluralUnit_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6196 = { sizeof (SliderDirection_t603010170)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6196[3] = 
{
	SliderDirection_t603010170::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6197 = { sizeof (SliderController_t2812089311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6197[11] = 
{
	SliderController_t2812089311::get_offset_of_background_2(),
	SliderController_t2812089311::get_offset_of_slider_3(),
	SliderController_t2812089311::get_offset_of_scaleRoot_4(),
	SliderController_t2812089311::get_offset_of_labels_5(),
	SliderController_t2812089311::get_offset_of_slideComp_6(),
	SliderController_t2812089311::get_offset_of_slideDirection_7(),
	SliderController_t2812089311::get_offset_of_startingLabel_8(),
	SliderController_t2812089311::get_offset_of_endLabel_9(),
	SliderController_t2812089311::get_offset_of_startingPoint_10(),
	SliderController_t2812089311::get_offset_of_points_11(),
	SliderController_t2812089311::get_offset_of_closestPoint_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6198 = { sizeof (SwipeNavigationController_t3767682136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6198[9] = 
{
	SwipeNavigationController_t3767682136::get_offset_of_navItems_39(),
	SwipeNavigationController_t3767682136::get_offset_of_itemPositions_40(),
	SwipeNavigationController_t3767682136::get_offset_of_container_41(),
	SwipeNavigationController_t3767682136::get_offset_of_uiPanel_42(),
	SwipeNavigationController_t3767682136::get_offset_of_current_43(),
	SwipeNavigationController_t3767682136::get_offset_of_currentCategory_44(),
	SwipeNavigationController_t3767682136::get_offset_of_store_45(),
	SwipeNavigationController_t3767682136::get_offset_of_activeCurve_46(),
	SwipeNavigationController_t3767682136::get_offset_of_inactiveCurve_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6199 = { sizeof (GpsService_t3478080739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6199[5] = 
{
	GpsService_t3478080739::get_offset_of_gpsDisabled_2(),
	GpsService_t3478080739::get_offset_of_startLocation_3(),
	GpsService_t3478080739::get_offset_of_lastLocation_4(),
	GpsService_t3478080739::get_offset_of_sphereRadius_5(),
	GpsService_t3478080739::get_offset_of_distanceBetweenPoints_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
