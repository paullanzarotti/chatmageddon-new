﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FireGuage/<MoveFireGuage>c__Iterator0
struct U3CMoveFireGuageU3Ec__Iterator0_t520811751;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FireGuage/<MoveFireGuage>c__Iterator0::.ctor()
extern "C"  void U3CMoveFireGuageU3Ec__Iterator0__ctor_m3229575120 (U3CMoveFireGuageU3Ec__Iterator0_t520811751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FireGuage/<MoveFireGuage>c__Iterator0::MoveNext()
extern "C"  bool U3CMoveFireGuageU3Ec__Iterator0_MoveNext_m649355084 (U3CMoveFireGuageU3Ec__Iterator0_t520811751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FireGuage/<MoveFireGuage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveFireGuageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546085890 (U3CMoveFireGuageU3Ec__Iterator0_t520811751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FireGuage/<MoveFireGuage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveFireGuageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3099146986 (U3CMoveFireGuageU3Ec__Iterator0_t520811751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FireGuage/<MoveFireGuage>c__Iterator0::Dispose()
extern "C"  void U3CMoveFireGuageU3Ec__Iterator0_Dispose_m3232807345 (U3CMoveFireGuageU3Ec__Iterator0_t520811751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FireGuage/<MoveFireGuage>c__Iterator0::Reset()
extern "C"  void U3CMoveFireGuageU3Ec__Iterator0_Reset_m2137056403 (U3CMoveFireGuageU3Ec__Iterator0_t520811751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
