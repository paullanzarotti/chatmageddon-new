﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationScreen
struct NavigationScreen_t2333230110;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationScreen::.ctor()
extern "C"  void NavigationScreen__ctor_m2982757537 (NavigationScreen_t2333230110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigationScreen::UIClosing()
extern "C"  void NavigationScreen_UIClosing_m417595184 (NavigationScreen_t2333230110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigationScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void NavigationScreen_ScreenLoad_m3731291867 (NavigationScreen_t2333230110 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigationScreen::ScreenUnload(NavigationDirection)
extern "C"  void NavigationScreen_ScreenUnload_m1619251955 (NavigationScreen_t2333230110 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NavigationScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool NavigationScreen_ValidateScreenNavigation_m1236417794 (NavigationScreen_t2333230110 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NavigationScreen::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationScreen_SaveScreenContent_m1863290017 (NavigationScreen_t2333230110 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
