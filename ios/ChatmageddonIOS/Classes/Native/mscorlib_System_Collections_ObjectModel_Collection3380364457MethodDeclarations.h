﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<DefendNavScreen>
struct Collection_1_t3380364457;
// System.Collections.Generic.IList`1<DefendNavScreen>
struct IList_1_t84593008;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// DefendNavScreen[]
struct DefendNavScreenU5BU5D_t1548979470;
// System.Collections.Generic.IEnumerator`1<DefendNavScreen>
struct IEnumerator_1_t1314143530;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"

// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m2893534182_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2893534182(__this, method) ((  void (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1__ctor_m2893534182_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1837933519_gshared (Collection_1_t3380364457 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1837933519(__this, ___list0, method) ((  void (*) (Collection_1_t3380364457 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1837933519_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m228813181_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m228813181(__this, method) ((  bool (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m228813181_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2601220934_gshared (Collection_1_t3380364457 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2601220934(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3380364457 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2601220934_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m629709841_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m629709841(__this, method) ((  Il2CppObject * (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m629709841_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1306499694_gshared (Collection_1_t3380364457 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1306499694(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3380364457 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1306499694_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1742670048_gshared (Collection_1_t3380364457 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1742670048(__this, ___value0, method) ((  bool (*) (Collection_1_t3380364457 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1742670048_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2597688264_gshared (Collection_1_t3380364457 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2597688264(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3380364457 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2597688264_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m348733193_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m348733193(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m348733193_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3973800793_gshared (Collection_1_t3380364457 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3973800793(__this, ___value0, method) ((  void (*) (Collection_1_t3380364457 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3973800793_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2786174462_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2786174462(__this, method) ((  bool (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2786174462_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1879040086_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1879040086(__this, method) ((  Il2CppObject * (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1879040086_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1965919157_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1965919157(__this, method) ((  bool (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1965919157_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3001777282_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3001777282(__this, method) ((  bool (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3001777282_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3628243597_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3628243597(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3628243597_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3659052820_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3659052820(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3659052820_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m794592917_gshared (Collection_1_t3380364457 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m794592917(__this, ___item0, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_Add_m794592917_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m1489310081_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1489310081(__this, method) ((  void (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_Clear_m1489310081_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1579328187_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1579328187(__this, method) ((  void (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_ClearItems_m1579328187_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m162083263_gshared (Collection_1_t3380364457 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m162083263(__this, ___item0, method) ((  bool (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_Contains_m162083263_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1368456565_gshared (Collection_1_t3380364457 * __this, DefendNavScreenU5BU5D_t1548979470* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1368456565(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3380364457 *, DefendNavScreenU5BU5D_t1548979470*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1368456565_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<DefendNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m154762250_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m154762250(__this, method) ((  Il2CppObject* (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_GetEnumerator_m154762250_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DefendNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2871371545_gshared (Collection_1_t3380364457 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2871371545(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2871371545_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1504304124_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1504304124(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1504304124_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2425191663_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2425191663(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2425191663_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m385102650_gshared (Collection_1_t3380364457 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m385102650(__this, ___item0, method) ((  bool (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_Remove_m385102650_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1069852200_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1069852200(__this, ___index0, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1069852200_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2193222410_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2193222410(__this, ___index0, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2193222410_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DefendNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1303234334_gshared (Collection_1_t3380364457 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1303234334(__this, method) ((  int32_t (*) (Collection_1_t3380364457 *, const MethodInfo*))Collection_1_get_Count_m1303234334_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<DefendNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m621034086_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m621034086(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3380364457 *, int32_t, const MethodInfo*))Collection_1_get_Item_m621034086_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m4145044917_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m4145044917(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m4145044917_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3056012436_gshared (Collection_1_t3380364457 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3056012436(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3380364457 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m3056012436_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3786008387_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3786008387(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3786008387_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<DefendNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m356647107_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m356647107(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m356647107_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DefendNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m200470863_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m200470863(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m200470863_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2326657783_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2326657783(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2326657783_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DefendNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2640326616_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2640326616(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2640326616_gshared)(__this /* static, unused */, ___list0, method)
