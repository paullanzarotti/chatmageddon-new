﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.SectionInfo
struct SectionInfo_t1739019515;
// System.String
struct String_t;
// System.Configuration.SectionInformation
struct SectionInformation_t2754609709;
// System.Object
struct Il2CppObject;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;
// System.Configuration.ConfigInfo
struct ConfigInfo_t546730838;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_SectionI2754609709.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"
#include "System_Configuration_System_Configuration_ConfigInf546730838.h"

// System.Void System.Configuration.SectionInfo::.ctor()
extern "C"  void SectionInfo__ctor_m3237924430 (SectionInfo_t1739019515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInfo::.ctor(System.String,System.Configuration.SectionInformation)
extern "C"  void SectionInfo__ctor_m3396864394 (SectionInfo_t1739019515 * __this, String_t* ___sectionName0, SectionInformation_t2754609709 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.SectionInfo::CreateInstance()
extern "C"  Il2CppObject * SectionInfo_CreateInstance_m3857848628 (SectionInfo_t1739019515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInfo::HasDataContent(System.Configuration.Configuration)
extern "C"  bool SectionInfo_HasDataContent_m2836931178 (SectionInfo_t1739019515 * __this, Configuration_t3335372970 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.SectionInfo::HasConfigContent(System.Configuration.Configuration)
extern "C"  bool SectionInfo_HasConfigContent_m2124399556 (SectionInfo_t1739019515 * __this, Configuration_t3335372970 * ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInfo::ReadConfig(System.Configuration.Configuration,System.String,System.Xml.XmlReader)
extern "C"  void SectionInfo_ReadConfig_m1508625577 (SectionInfo_t1739019515 * __this, Configuration_t3335372970 * ___cfg0, String_t* ___streamName1, XmlReader_t3675626668 * ___reader2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInfo::WriteConfig(System.Configuration.Configuration,System.Xml.XmlWriter,System.Configuration.ConfigurationSaveMode)
extern "C"  void SectionInfo_WriteConfig_m2831691533 (SectionInfo_t1739019515 * __this, Configuration_t3335372970 * ___cfg0, XmlWriter_t1048088568 * ___writer1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInfo::ReadData(System.Configuration.Configuration,System.Xml.XmlReader,System.Boolean)
extern "C"  void SectionInfo_ReadData_m2814313648 (SectionInfo_t1739019515 * __this, Configuration_t3335372970 * ___config0, XmlReader_t3675626668 * ___reader1, bool ___overrideAllowed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInfo::WriteData(System.Configuration.Configuration,System.Xml.XmlWriter,System.Configuration.ConfigurationSaveMode)
extern "C"  void SectionInfo_WriteData_m1842924571 (SectionInfo_t1739019515 * __this, Configuration_t3335372970 * ___config0, XmlWriter_t1048088568 * ___writer1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.SectionInfo::Merge(System.Configuration.ConfigInfo)
extern "C"  void SectionInfo_Merge_m2513541987 (SectionInfo_t1739019515 * __this, ConfigInfo_t546730838 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
