﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenScale
struct TweenScale_t2697902175;

#include "AssemblyU2DCSharp_StatusUI2174902556.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PendingMissileUI
struct  PendingMissileUI_t3261005045  : public StatusUI_t2174902556
{
public:
	// TweenScale PendingMissileUI::scaleTween
	TweenScale_t2697902175 * ___scaleTween_4;
	// System.Boolean PendingMissileUI::uiUnloading
	bool ___uiUnloading_5;

public:
	inline static int32_t get_offset_of_scaleTween_4() { return static_cast<int32_t>(offsetof(PendingMissileUI_t3261005045, ___scaleTween_4)); }
	inline TweenScale_t2697902175 * get_scaleTween_4() const { return ___scaleTween_4; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_4() { return &___scaleTween_4; }
	inline void set_scaleTween_4(TweenScale_t2697902175 * value)
	{
		___scaleTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_4, value);
	}

	inline static int32_t get_offset_of_uiUnloading_5() { return static_cast<int32_t>(offsetof(PendingMissileUI_t3261005045, ___uiUnloading_5)); }
	inline bool get_uiUnloading_5() const { return ___uiUnloading_5; }
	inline bool* get_address_of_uiUnloading_5() { return &___uiUnloading_5; }
	inline void set_uiUnloading_5(bool value)
	{
		___uiUnloading_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
