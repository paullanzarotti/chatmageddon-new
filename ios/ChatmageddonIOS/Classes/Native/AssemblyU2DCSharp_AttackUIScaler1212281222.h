﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackUIScaler
struct  AttackUIScaler_t1212281222  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite AttackUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UISprite AttackUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_15;
	// UnityEngine.Transform AttackUIScaler::navigateBackButton
	Transform_t3275118058 * ___navigateBackButton_16;
	// UnityEngine.Transform AttackUIScaler::searchButton
	Transform_t3275118058 * ___searchButton_17;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(AttackUIScaler_t1212281222, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_titleSeperator_15() { return static_cast<int32_t>(offsetof(AttackUIScaler_t1212281222, ___titleSeperator_15)); }
	inline UISprite_t603616735 * get_titleSeperator_15() const { return ___titleSeperator_15; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_15() { return &___titleSeperator_15; }
	inline void set_titleSeperator_15(UISprite_t603616735 * value)
	{
		___titleSeperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_15, value);
	}

	inline static int32_t get_offset_of_navigateBackButton_16() { return static_cast<int32_t>(offsetof(AttackUIScaler_t1212281222, ___navigateBackButton_16)); }
	inline Transform_t3275118058 * get_navigateBackButton_16() const { return ___navigateBackButton_16; }
	inline Transform_t3275118058 ** get_address_of_navigateBackButton_16() { return &___navigateBackButton_16; }
	inline void set_navigateBackButton_16(Transform_t3275118058 * value)
	{
		___navigateBackButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___navigateBackButton_16, value);
	}

	inline static int32_t get_offset_of_searchButton_17() { return static_cast<int32_t>(offsetof(AttackUIScaler_t1212281222, ___searchButton_17)); }
	inline Transform_t3275118058 * get_searchButton_17() const { return ___searchButton_17; }
	inline Transform_t3275118058 ** get_address_of_searchButton_17() { return &___searchButton_17; }
	inline void set_searchButton_17(Transform_t3275118058 * value)
	{
		___searchButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___searchButton_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
