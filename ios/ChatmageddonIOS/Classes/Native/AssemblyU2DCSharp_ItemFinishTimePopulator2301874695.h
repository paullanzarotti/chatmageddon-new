﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemFinishTimePopulator
struct  ItemFinishTimePopulator_t2301874695  : public MonoBehaviour_t1158329972
{
public:
	// UILabel ItemFinishTimePopulator::dateLabel
	UILabel_t1795115428 * ___dateLabel_2;
	// UILabel ItemFinishTimePopulator::timeLabel
	UILabel_t1795115428 * ___timeLabel_3;

public:
	inline static int32_t get_offset_of_dateLabel_2() { return static_cast<int32_t>(offsetof(ItemFinishTimePopulator_t2301874695, ___dateLabel_2)); }
	inline UILabel_t1795115428 * get_dateLabel_2() const { return ___dateLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_dateLabel_2() { return &___dateLabel_2; }
	inline void set_dateLabel_2(UILabel_t1795115428 * value)
	{
		___dateLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___dateLabel_2, value);
	}

	inline static int32_t get_offset_of_timeLabel_3() { return static_cast<int32_t>(offsetof(ItemFinishTimePopulator_t2301874695, ___timeLabel_3)); }
	inline UILabel_t1795115428 * get_timeLabel_3() const { return ___timeLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_3() { return &___timeLabel_3; }
	inline void set_timeLabel_3(UILabel_t1795115428 * value)
	{
		___timeLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
