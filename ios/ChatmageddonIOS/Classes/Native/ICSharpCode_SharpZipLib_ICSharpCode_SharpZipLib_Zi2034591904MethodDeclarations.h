﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler
struct ZipTestResultHandler_t2034591904;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Zip.TestStatus
struct TestStatus_t3549853348;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3549853348.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipTestResultHandler__ctor_m1553451706 (ZipTestResultHandler_t2034591904 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::Invoke(ICSharpCode.SharpZipLib.Zip.TestStatus,System.String)
extern "C"  void ZipTestResultHandler_Invoke_m849185304 (ZipTestResultHandler_t2034591904 * __this, TestStatus_t3549853348 * ___status0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::BeginInvoke(ICSharpCode.SharpZipLib.Zip.TestStatus,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipTestResultHandler_BeginInvoke_m3132147729 (ZipTestResultHandler_t2034591904 * __this, TestStatus_t3549853348 * ___status0, String_t* ___message1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ZipTestResultHandler_EndInvoke_m1080382356 (ZipTestResultHandler_t2034591904 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
