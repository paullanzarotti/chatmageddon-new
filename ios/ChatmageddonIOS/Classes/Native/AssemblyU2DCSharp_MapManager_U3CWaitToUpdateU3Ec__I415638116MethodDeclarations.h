﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapManager/<WaitToUpdate>c__Iterator0
struct U3CWaitToUpdateU3Ec__Iterator0_t415638116;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MapManager/<WaitToUpdate>c__Iterator0::.ctor()
extern "C"  void U3CWaitToUpdateU3Ec__Iterator0__ctor_m2912359993 (U3CWaitToUpdateU3Ec__Iterator0_t415638116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MapManager/<WaitToUpdate>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToUpdateU3Ec__Iterator0_MoveNext_m1518123371 (U3CWaitToUpdateU3Ec__Iterator0_t415638116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MapManager/<WaitToUpdate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2197599679 (U3CWaitToUpdateU3Ec__Iterator0_t415638116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MapManager/<WaitToUpdate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1281124183 (U3CWaitToUpdateU3Ec__Iterator0_t415638116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager/<WaitToUpdate>c__Iterator0::Dispose()
extern "C"  void U3CWaitToUpdateU3Ec__Iterator0_Dispose_m3280192278 (U3CWaitToUpdateU3Ec__Iterator0_t415638116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager/<WaitToUpdate>c__Iterator0::Reset()
extern "C"  void U3CWaitToUpdateU3Ec__Iterator0_Reset_m3636406180 (U3CWaitToUpdateU3Ec__Iterator0_t415638116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
