﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// SelectAudienceSwitch
struct SelectAudienceSwitch_t3539699246;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectAudienceUIScaler
struct  SelectAudienceUIScaler_t620524668  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UILabel SelectAudienceUIScaler::titleLabel
	UILabel_t1795115428 * ___titleLabel_14;
	// UISprite SelectAudienceUIScaler::titleDivider
	UISprite_t603616735 * ___titleDivider_15;
	// SelectAudienceSwitch SelectAudienceUIScaler::saSwitch
	SelectAudienceSwitch_t3539699246 * ___saSwitch_16;
	// UISprite SelectAudienceUIScaler::tickSprite
	UISprite_t603616735 * ___tickSprite_17;
	// UILabel SelectAudienceUIScaler::everyoneLabel
	UILabel_t1795115428 * ___everyoneLabel_18;
	// UnityEngine.BoxCollider SelectAudienceUIScaler::everyoneCollider
	BoxCollider_t22920061 * ___everyoneCollider_19;
	// UISprite SelectAudienceUIScaler::everyoneDivider
	UISprite_t603616735 * ___everyoneDivider_20;
	// UILabel SelectAudienceUIScaler::contactsLabel
	UILabel_t1795115428 * ___contactsLabel_21;
	// UnityEngine.BoxCollider SelectAudienceUIScaler::contactsCollider
	BoxCollider_t22920061 * ___contactsCollider_22;
	// UISprite SelectAudienceUIScaler::contactsDivider
	UISprite_t603616735 * ___contactsDivider_23;
	// UILabel SelectAudienceUIScaler::nobodyLabel
	UILabel_t1795115428 * ___nobodyLabel_24;
	// UnityEngine.BoxCollider SelectAudienceUIScaler::nobodyCollider
	BoxCollider_t22920061 * ___nobodyCollider_25;
	// UISprite SelectAudienceUIScaler::nobodyDivider
	UISprite_t603616735 * ___nobodyDivider_26;

public:
	inline static int32_t get_offset_of_titleLabel_14() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___titleLabel_14)); }
	inline UILabel_t1795115428 * get_titleLabel_14() const { return ___titleLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_14() { return &___titleLabel_14; }
	inline void set_titleLabel_14(UILabel_t1795115428 * value)
	{
		___titleLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_14, value);
	}

	inline static int32_t get_offset_of_titleDivider_15() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___titleDivider_15)); }
	inline UISprite_t603616735 * get_titleDivider_15() const { return ___titleDivider_15; }
	inline UISprite_t603616735 ** get_address_of_titleDivider_15() { return &___titleDivider_15; }
	inline void set_titleDivider_15(UISprite_t603616735 * value)
	{
		___titleDivider_15 = value;
		Il2CppCodeGenWriteBarrier(&___titleDivider_15, value);
	}

	inline static int32_t get_offset_of_saSwitch_16() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___saSwitch_16)); }
	inline SelectAudienceSwitch_t3539699246 * get_saSwitch_16() const { return ___saSwitch_16; }
	inline SelectAudienceSwitch_t3539699246 ** get_address_of_saSwitch_16() { return &___saSwitch_16; }
	inline void set_saSwitch_16(SelectAudienceSwitch_t3539699246 * value)
	{
		___saSwitch_16 = value;
		Il2CppCodeGenWriteBarrier(&___saSwitch_16, value);
	}

	inline static int32_t get_offset_of_tickSprite_17() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___tickSprite_17)); }
	inline UISprite_t603616735 * get_tickSprite_17() const { return ___tickSprite_17; }
	inline UISprite_t603616735 ** get_address_of_tickSprite_17() { return &___tickSprite_17; }
	inline void set_tickSprite_17(UISprite_t603616735 * value)
	{
		___tickSprite_17 = value;
		Il2CppCodeGenWriteBarrier(&___tickSprite_17, value);
	}

	inline static int32_t get_offset_of_everyoneLabel_18() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___everyoneLabel_18)); }
	inline UILabel_t1795115428 * get_everyoneLabel_18() const { return ___everyoneLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_everyoneLabel_18() { return &___everyoneLabel_18; }
	inline void set_everyoneLabel_18(UILabel_t1795115428 * value)
	{
		___everyoneLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___everyoneLabel_18, value);
	}

	inline static int32_t get_offset_of_everyoneCollider_19() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___everyoneCollider_19)); }
	inline BoxCollider_t22920061 * get_everyoneCollider_19() const { return ___everyoneCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_everyoneCollider_19() { return &___everyoneCollider_19; }
	inline void set_everyoneCollider_19(BoxCollider_t22920061 * value)
	{
		___everyoneCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___everyoneCollider_19, value);
	}

	inline static int32_t get_offset_of_everyoneDivider_20() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___everyoneDivider_20)); }
	inline UISprite_t603616735 * get_everyoneDivider_20() const { return ___everyoneDivider_20; }
	inline UISprite_t603616735 ** get_address_of_everyoneDivider_20() { return &___everyoneDivider_20; }
	inline void set_everyoneDivider_20(UISprite_t603616735 * value)
	{
		___everyoneDivider_20 = value;
		Il2CppCodeGenWriteBarrier(&___everyoneDivider_20, value);
	}

	inline static int32_t get_offset_of_contactsLabel_21() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___contactsLabel_21)); }
	inline UILabel_t1795115428 * get_contactsLabel_21() const { return ___contactsLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_contactsLabel_21() { return &___contactsLabel_21; }
	inline void set_contactsLabel_21(UILabel_t1795115428 * value)
	{
		___contactsLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___contactsLabel_21, value);
	}

	inline static int32_t get_offset_of_contactsCollider_22() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___contactsCollider_22)); }
	inline BoxCollider_t22920061 * get_contactsCollider_22() const { return ___contactsCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_contactsCollider_22() { return &___contactsCollider_22; }
	inline void set_contactsCollider_22(BoxCollider_t22920061 * value)
	{
		___contactsCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___contactsCollider_22, value);
	}

	inline static int32_t get_offset_of_contactsDivider_23() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___contactsDivider_23)); }
	inline UISprite_t603616735 * get_contactsDivider_23() const { return ___contactsDivider_23; }
	inline UISprite_t603616735 ** get_address_of_contactsDivider_23() { return &___contactsDivider_23; }
	inline void set_contactsDivider_23(UISprite_t603616735 * value)
	{
		___contactsDivider_23 = value;
		Il2CppCodeGenWriteBarrier(&___contactsDivider_23, value);
	}

	inline static int32_t get_offset_of_nobodyLabel_24() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___nobodyLabel_24)); }
	inline UILabel_t1795115428 * get_nobodyLabel_24() const { return ___nobodyLabel_24; }
	inline UILabel_t1795115428 ** get_address_of_nobodyLabel_24() { return &___nobodyLabel_24; }
	inline void set_nobodyLabel_24(UILabel_t1795115428 * value)
	{
		___nobodyLabel_24 = value;
		Il2CppCodeGenWriteBarrier(&___nobodyLabel_24, value);
	}

	inline static int32_t get_offset_of_nobodyCollider_25() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___nobodyCollider_25)); }
	inline BoxCollider_t22920061 * get_nobodyCollider_25() const { return ___nobodyCollider_25; }
	inline BoxCollider_t22920061 ** get_address_of_nobodyCollider_25() { return &___nobodyCollider_25; }
	inline void set_nobodyCollider_25(BoxCollider_t22920061 * value)
	{
		___nobodyCollider_25 = value;
		Il2CppCodeGenWriteBarrier(&___nobodyCollider_25, value);
	}

	inline static int32_t get_offset_of_nobodyDivider_26() { return static_cast<int32_t>(offsetof(SelectAudienceUIScaler_t620524668, ___nobodyDivider_26)); }
	inline UISprite_t603616735 * get_nobodyDivider_26() const { return ___nobodyDivider_26; }
	inline UISprite_t603616735 ** get_address_of_nobodyDivider_26() { return &___nobodyDivider_26; }
	inline void set_nobodyDivider_26(UISprite_t603616735 * value)
	{
		___nobodyDivider_26 = value;
		Il2CppCodeGenWriteBarrier(&___nobodyDivider_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
