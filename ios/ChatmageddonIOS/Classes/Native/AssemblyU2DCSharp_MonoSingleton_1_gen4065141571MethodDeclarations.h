﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<BuildManager>::.ctor()
#define MonoSingleton_1__ctor_m2540984893(__this, method) ((  void (*) (MonoSingleton_1_t4065141571 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<BuildManager>::Awake()
#define MonoSingleton_1_Awake_m1017559884(__this, method) ((  void (*) (MonoSingleton_1_t4065141571 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<BuildManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1463363546(__this /* static, unused */, method) ((  BuildManager_t19508555 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<BuildManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1293829090(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<BuildManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1585635771(__this, method) ((  void (*) (MonoSingleton_1_t4065141571 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<BuildManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3029873023(__this, method) ((  void (*) (MonoSingleton_1_t4065141571 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<BuildManager>::.cctor()
#define MonoSingleton_1__cctor_m3362665838(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
