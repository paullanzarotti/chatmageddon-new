﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardItemUIScaler
struct LeaderboardItemUIScaler_t1432927178;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardItemUIScaler::.ctor()
extern "C"  void LeaderboardItemUIScaler__ctor_m1899945621 (LeaderboardItemUIScaler_t1432927178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardItemUIScaler::GlobalUIScale()
extern "C"  void LeaderboardItemUIScaler_GlobalUIScale_m2789967804 (LeaderboardItemUIScaler_t1432927178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
