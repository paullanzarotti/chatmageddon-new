﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeNavScreen
struct HomeNavScreen_t3588214316;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void HomeNavScreen::.ctor()
extern "C"  void HomeNavScreen__ctor_m3782188413 (HomeNavScreen_t3588214316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeNavScreen::Start()
extern "C"  void HomeNavScreen_Start_m2655242909 (HomeNavScreen_t3588214316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeNavScreen::UIClosing()
extern "C"  void HomeNavScreen_UIClosing_m3665390918 (HomeNavScreen_t3588214316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeNavScreen::SettingsUIClosing()
extern "C"  void HomeNavScreen_SettingsUIClosing_m1860002679 (HomeNavScreen_t3588214316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void HomeNavScreen_ScreenLoad_m1797837039 (HomeNavScreen_t3588214316 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void HomeNavScreen_ScreenUnload_m3467243951 (HomeNavScreen_t3588214316 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool HomeNavScreen_ValidateScreenNavigation_m3985166172 (HomeNavScreen_t3588214316 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
