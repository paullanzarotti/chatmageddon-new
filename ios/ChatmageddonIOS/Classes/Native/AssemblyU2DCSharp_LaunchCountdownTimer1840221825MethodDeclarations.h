﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchCountdownTimer
struct LaunchCountdownTimer_t1840221825;
// LaunchCountdownTimer/OnZeroHit
struct OnZeroHit_t1439474620;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchCountdownTimer_OnZeroHit1439474620.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void LaunchCountdownTimer::.ctor()
extern "C"  void LaunchCountdownTimer__ctor_m3190289374 (LaunchCountdownTimer_t1840221825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::add_onZeroHit(LaunchCountdownTimer/OnZeroHit)
extern "C"  void LaunchCountdownTimer_add_onZeroHit_m1066110298 (LaunchCountdownTimer_t1840221825 * __this, OnZeroHit_t1439474620 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::remove_onZeroHit(LaunchCountdownTimer/OnZeroHit)
extern "C"  void LaunchCountdownTimer_remove_onZeroHit_m977496217 (LaunchCountdownTimer_t1840221825 * __this, OnZeroHit_t1439474620 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::SetCountdownTime(System.Double)
extern "C"  void LaunchCountdownTimer_SetCountdownTime_m1826088068 (LaunchCountdownTimer_t1840221825 * __this, double ___newTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::StartCountdown()
extern "C"  void LaunchCountdownTimer_StartCountdown_m1542149033 (LaunchCountdownTimer_t1840221825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::StopCalculator()
extern "C"  void LaunchCountdownTimer_StopCalculator_m1722010616 (LaunchCountdownTimer_t1840221825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchCountdownTimer::CalculateCountdownTime()
extern "C"  Il2CppObject * LaunchCountdownTimer_CalculateCountdownTime_m2827101946 (LaunchCountdownTimer_t1840221825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::SetTimeLabel(System.TimeSpan)
extern "C"  void LaunchCountdownTimer_SetTimeLabel_m4107548555 (LaunchCountdownTimer_t1840221825 * __this, TimeSpan_t3430258949  ___span0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::SetTimeToZero()
extern "C"  void LaunchCountdownTimer_SetTimeToZero_m3972128948 (LaunchCountdownTimer_t1840221825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer::SendZeroHitEvent()
extern "C"  void LaunchCountdownTimer_SendZeroHitEvent_m1083230191 (LaunchCountdownTimer_t1840221825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
