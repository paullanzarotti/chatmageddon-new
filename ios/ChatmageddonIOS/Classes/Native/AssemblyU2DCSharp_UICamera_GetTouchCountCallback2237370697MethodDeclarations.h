﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t2237370697;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICamera/GetTouchCountCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTouchCountCallback__ctor_m3955278542 (GetTouchCountCallback_t2237370697 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera/GetTouchCountCallback::Invoke()
extern "C"  int32_t GetTouchCountCallback_Invoke_m2268871846 (GetTouchCountCallback_t2237370697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICamera/GetTouchCountCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTouchCountCallback_BeginInvoke_m1837856463 (GetTouchCountCallback_t2237370697 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera/GetTouchCountCallback::EndInvoke(System.IAsyncResult)
extern "C"  int32_t GetTouchCountCallback_EndInvoke_m1782701322 (GetTouchCountCallback_t2237370697 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
