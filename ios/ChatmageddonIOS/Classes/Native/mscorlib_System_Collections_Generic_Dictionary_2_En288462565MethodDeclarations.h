﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En288462565.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21020750381.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3132967640_gshared (Enumerator_t288462565 * __this, Dictionary_2_t3263405159 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3132967640(__this, ___dictionary0, method) ((  void (*) (Enumerator_t288462565 *, Dictionary_2_t3263405159 *, const MethodInfo*))Enumerator__ctor_m3132967640_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2907925341_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2907925341(__this, method) ((  Il2CppObject * (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2907925341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m432953125_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m432953125(__this, method) ((  void (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m432953125_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2008783798_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2008783798(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2008783798_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m706072271_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m706072271(__this, method) ((  Il2CppObject * (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m706072271_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m826327439_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m826327439(__this, method) ((  Il2CppObject * (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m826327439_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1405547069_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1405547069(__this, method) ((  bool (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_MoveNext_m1405547069_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Current()
extern "C"  KeyValuePair_2_t1020750381  Enumerator_get_Current_m3089167009_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3089167009(__this, method) ((  KeyValuePair_2_t1020750381  (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_get_Current_m3089167009_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::get_CurrentKey()
extern "C"  Vector3Pair_t2859078138  Enumerator_get_CurrentKey_m2508139272_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2508139272(__this, method) ((  Vector3Pair_t2859078138  (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_get_CurrentKey_m2508139272_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::get_CurrentValue()
extern "C"  bool Enumerator_get_CurrentValue_m1620934600_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1620934600(__this, method) ((  bool (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_get_CurrentValue_m1620934600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::Reset()
extern "C"  void Enumerator_Reset_m1848832510_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1848832510(__this, method) ((  void (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_Reset_m1848832510_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::VerifyState()
extern "C"  void Enumerator_VerifyState_m954338815_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m954338815(__this, method) ((  void (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_VerifyState_m954338815_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3207836661_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3207836661(__this, method) ((  void (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_VerifyCurrent_m3207836661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m3575995784_gshared (Enumerator_t288462565 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3575995784(__this, method) ((  void (*) (Enumerator_t288462565 *, const MethodInfo*))Enumerator_Dispose_m3575995784_gshared)(__this, method)
