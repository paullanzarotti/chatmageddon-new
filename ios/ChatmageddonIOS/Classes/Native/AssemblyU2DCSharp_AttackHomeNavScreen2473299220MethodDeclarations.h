﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeNavScreen
struct AttackHomeNavScreen_t2473299220;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackHomeNavScreen::.ctor()
extern "C"  void AttackHomeNavScreen__ctor_m2353969737 (AttackHomeNavScreen_t2473299220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavScreen::Start()
extern "C"  void AttackHomeNavScreen_Start_m1966230809 (AttackHomeNavScreen_t2473299220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavScreen::UIClosing()
extern "C"  void AttackHomeNavScreen_UIClosing_m4001925174 (AttackHomeNavScreen_t2473299220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackHomeNavScreen_ScreenLoad_m1841475919 (AttackHomeNavScreen_t2473299220 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackHomeNavScreen_ScreenUnload_m3562688575 (AttackHomeNavScreen_t2473299220 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackHomeNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackHomeNavScreen_ValidateScreenNavigation_m102198732 (AttackHomeNavScreen_t2473299220 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
