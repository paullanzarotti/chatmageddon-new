﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookManager
struct FacebookManager_t1572224887;
// System.Action
struct Action_t3226471752;
// System.Action`1<Prime31.P31Error>
struct Action_1_t2658399990;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_1_t111060643;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.FacebookManager::.cctor()
extern "C"  void FacebookManager__cctor_m172907990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::.ctor()
extern "C"  void FacebookManager__ctor_m2854111797 (FacebookManager_t1572224887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_sessionOpenedEvent(System.Action)
extern "C"  void FacebookManager_add_sessionOpenedEvent_m2440453921 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_sessionOpenedEvent(System.Action)
extern "C"  void FacebookManager_remove_sessionOpenedEvent_m634509888 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_preLoginSucceededEvent(System.Action)
extern "C"  void FacebookManager_add_preLoginSucceededEvent_m1601176717 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_preLoginSucceededEvent(System.Action)
extern "C"  void FacebookManager_remove_preLoginSucceededEvent_m4258095438 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_loginFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_add_loginFailedEvent_m850566640 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_loginFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_remove_loginFailedEvent_m510272411 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_dialogCompletedWithUrlEvent(System.Action`1<System.String>)
extern "C"  void FacebookManager_add_dialogCompletedWithUrlEvent_m1957252687 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_dialogCompletedWithUrlEvent(System.Action`1<System.String>)
extern "C"  void FacebookManager_remove_dialogCompletedWithUrlEvent_m3731955896 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_dialogFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_add_dialogFailedEvent_m3394713075 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_dialogFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_remove_dialogFailedEvent_m4179114812 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_graphRequestCompletedEvent(System.Action`1<System.Object>)
extern "C"  void FacebookManager_add_graphRequestCompletedEvent_m3507304299 (Il2CppObject * __this /* static, unused */, Action_1_t2491248677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_graphRequestCompletedEvent(System.Action`1<System.Object>)
extern "C"  void FacebookManager_remove_graphRequestCompletedEvent_m3834177516 (Il2CppObject * __this /* static, unused */, Action_1_t2491248677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_graphRequestFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_add_graphRequestFailedEvent_m432137428 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_graphRequestFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_remove_graphRequestFailedEvent_m362019517 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_facebookComposerCompletedEvent(System.Action`1<System.Boolean>)
extern "C"  void FacebookManager_add_facebookComposerCompletedEvent_m2691046213 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_facebookComposerCompletedEvent(System.Action`1<System.Boolean>)
extern "C"  void FacebookManager_remove_facebookComposerCompletedEvent_m1367682514 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_reauthorizationSucceededEvent(System.Action)
extern "C"  void FacebookManager_add_reauthorizationSucceededEvent_m2936993877 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_reauthorizationSucceededEvent(System.Action)
extern "C"  void FacebookManager_remove_reauthorizationSucceededEvent_m3653567588 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_reauthorizationFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_add_reauthorizationFailedEvent_m2127911479 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_reauthorizationFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_remove_reauthorizationFailedEvent_m2703236264 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_shareDialogSucceededEvent(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void FacebookManager_add_shareDialogSucceededEvent_m3709245936 (Il2CppObject * __this /* static, unused */, Action_1_t111060643 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_shareDialogSucceededEvent(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void FacebookManager_remove_shareDialogSucceededEvent_m304620835 (Il2CppObject * __this /* static, unused */, Action_1_t111060643 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::add_shareDialogFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_add_shareDialogFailedEvent_m2160571606 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::remove_shareDialogFailedEvent(System.Action`1<Prime31.P31Error>)
extern "C"  void FacebookManager_remove_shareDialogFailedEvent_m1779405265 (Il2CppObject * __this /* static, unused */, Action_1_t2658399990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::sessionOpened(System.String)
extern "C"  void FacebookManager_sessionOpened_m441865888 (FacebookManager_t1572224887 * __this, String_t* ___accessToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::loginFailed(System.String)
extern "C"  void FacebookManager_loginFailed_m3352619147 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::dialogCompletedWithUrl(System.String)
extern "C"  void FacebookManager_dialogCompletedWithUrl_m2542239575 (FacebookManager_t1572224887 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::dialogFailedWithError(System.String)
extern "C"  void FacebookManager_dialogFailedWithError_m491219796 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::graphRequestCompleted(System.String)
extern "C"  void FacebookManager_graphRequestCompleted_m481763407 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::graphRequestFailed(System.String)
extern "C"  void FacebookManager_graphRequestFailed_m1108542657 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::facebookComposerCompleted(System.String)
extern "C"  void FacebookManager_facebookComposerCompleted_m2194766734 (FacebookManager_t1572224887 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::reauthorizationSucceeded(System.String)
extern "C"  void FacebookManager_reauthorizationSucceeded_m2013865022 (FacebookManager_t1572224887 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::reauthorizationFailed(System.String)
extern "C"  void FacebookManager_reauthorizationFailed_m3758152894 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::shareDialogFailed(System.String)
extern "C"  void FacebookManager_shareDialogFailed_m1490535537 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookManager::shareDialogSucceeded(System.String)
extern "C"  void FacebookManager_shareDialogSucceeded_m1822714449 (FacebookManager_t1572224887 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
