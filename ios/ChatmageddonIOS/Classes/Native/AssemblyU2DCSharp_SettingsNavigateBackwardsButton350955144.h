﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsNavigateBackwardsButton
struct  SettingsNavigateBackwardsButton_t350955144  : public MonoBehaviour_t1158329972
{
public:
	// SFX SettingsNavigateBackwardsButton::sfxToPlay
	int32_t ___sfxToPlay_2;
	// System.Boolean SettingsNavigateBackwardsButton::saveContent
	bool ___saveContent_3;
	// UISprite SettingsNavigateBackwardsButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_4;
	// UILabel SettingsNavigateBackwardsButton::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_5;

public:
	inline static int32_t get_offset_of_sfxToPlay_2() { return static_cast<int32_t>(offsetof(SettingsNavigateBackwardsButton_t350955144, ___sfxToPlay_2)); }
	inline int32_t get_sfxToPlay_2() const { return ___sfxToPlay_2; }
	inline int32_t* get_address_of_sfxToPlay_2() { return &___sfxToPlay_2; }
	inline void set_sfxToPlay_2(int32_t value)
	{
		___sfxToPlay_2 = value;
	}

	inline static int32_t get_offset_of_saveContent_3() { return static_cast<int32_t>(offsetof(SettingsNavigateBackwardsButton_t350955144, ___saveContent_3)); }
	inline bool get_saveContent_3() const { return ___saveContent_3; }
	inline bool* get_address_of_saveContent_3() { return &___saveContent_3; }
	inline void set_saveContent_3(bool value)
	{
		___saveContent_3 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_4() { return static_cast<int32_t>(offsetof(SettingsNavigateBackwardsButton_t350955144, ___buttonSprite_4)); }
	inline UISprite_t603616735 * get_buttonSprite_4() const { return ___buttonSprite_4; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_4() { return &___buttonSprite_4; }
	inline void set_buttonSprite_4(UISprite_t603616735 * value)
	{
		___buttonSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_4, value);
	}

	inline static int32_t get_offset_of_buttonLabel_5() { return static_cast<int32_t>(offsetof(SettingsNavigateBackwardsButton_t350955144, ___buttonLabel_5)); }
	inline UILabel_t1795115428 * get_buttonLabel_5() const { return ___buttonLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_5() { return &___buttonLabel_5; }
	inline void set_buttonLabel_5(UILabel_t1795115428 * value)
	{
		___buttonLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_5, value);
	}
};

struct SettingsNavigateBackwardsButton_t350955144_StaticFields
{
public:
	// System.Action`1<System.Boolean> SettingsNavigateBackwardsButton::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(SettingsNavigateBackwardsButton_t350955144_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
