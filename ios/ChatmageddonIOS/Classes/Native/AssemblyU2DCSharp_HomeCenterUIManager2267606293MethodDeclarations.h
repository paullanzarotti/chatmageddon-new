﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCenterUIManager
struct HomeCenterUIManager_t2267606293;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void HomeCenterUIManager::.ctor()
extern "C"  void HomeCenterUIManager__ctor_m1388871586 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::Start()
extern "C"  void HomeCenterUIManager_Start_m2839421722 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::SetRadarActive(System.Boolean)
extern "C"  void HomeCenterUIManager_SetRadarActive_m1675344553 (HomeCenterUIManager_t2267606293 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::OnRadarTweenFinished()
extern "C"  void HomeCenterUIManager_OnRadarTweenFinished_m1192405184 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::SetMapBlur(UnityEngine.RenderTexture)
extern "C"  void HomeCenterUIManager_SetMapBlur_m3313834949 (HomeCenterUIManager_t2267606293 * __this, RenderTexture_t2666733923 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::FadeMapTransition(System.Boolean)
extern "C"  void HomeCenterUIManager_FadeMapTransition_m3062181752 (HomeCenterUIManager_t2267606293 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::OnMapFadeTweenFinished()
extern "C"  void HomeCenterUIManager_OnMapFadeTweenFinished_m3508067720 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HomeCenterUIManager::WaitToFade()
extern "C"  Il2CppObject * HomeCenterUIManager_WaitToFade_m3894243734 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::TransitionMapViewLabel(System.Boolean)
extern "C"  void HomeCenterUIManager_TransitionMapViewLabel_m2125718975 (HomeCenterUIManager_t2267606293 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIManager::OnMapViewLabelTweenFinished()
extern "C"  void HomeCenterUIManager_OnMapViewLabelTweenFinished_m2436475189 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HomeCenterUIManager::WaitToFadeMapView()
extern "C"  Il2CppObject * HomeCenterUIManager_WaitToFadeMapView_m1707245141 (HomeCenterUIManager_t2267606293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
