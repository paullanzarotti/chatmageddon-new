﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Edge_Edge_filter
struct CameraFilterPack_Edge_Edge_filter_t2364446753;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Edge_Edge_filter::.ctor()
extern "C"  void CameraFilterPack_Edge_Edge_filter__ctor_m1896142090 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Edge_filter::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Edge_Edge_filter_get_material_m3635592569 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::Start()
extern "C"  void CameraFilterPack_Edge_Edge_filter_Start_m2621911058 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Edge_Edge_filter_OnRenderImage_m3387444290 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::OnValidate()
extern "C"  void CameraFilterPack_Edge_Edge_filter_OnValidate_m663475007 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::Update()
extern "C"  void CameraFilterPack_Edge_Edge_filter_Update_m1683061005 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::OnDisable()
extern "C"  void CameraFilterPack_Edge_Edge_filter_OnDisable_m2746137345 (CameraFilterPack_Edge_Edge_filter_t2364446753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
