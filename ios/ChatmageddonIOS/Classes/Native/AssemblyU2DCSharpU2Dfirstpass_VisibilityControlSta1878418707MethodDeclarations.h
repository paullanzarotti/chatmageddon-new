﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VisibilityControlStatic
struct VisibilityControlStatic_t1878418707;
// RefInt
struct RefInt_t2938871354;
// Vectrosity.VectorLine
struct VectorLine_t3390220087;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"

// System.Void VisibilityControlStatic::.ctor()
extern "C"  void VisibilityControlStatic__ctor_m793972964 (VisibilityControlStatic_t1878418707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RefInt VisibilityControlStatic::get_objectNumber()
extern "C"  RefInt_t2938871354 * VisibilityControlStatic_get_objectNumber_m1923117598 (VisibilityControlStatic_t1878418707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlStatic::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C"  void VisibilityControlStatic_Setup_m2480214275 (VisibilityControlStatic_t1878418707 * __this, VectorLine_t3390220087 * ___line0, bool ___makeBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VisibilityControlStatic::WaitCheck()
extern "C"  Il2CppObject * VisibilityControlStatic_WaitCheck_m1086170031 (VisibilityControlStatic_t1878418707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlStatic::OnBecameVisible()
extern "C"  void VisibilityControlStatic_OnBecameVisible_m2033207788 (VisibilityControlStatic_t1878418707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlStatic::OnBecameInvisible()
extern "C"  void VisibilityControlStatic_OnBecameInvisible_m1331385253 (VisibilityControlStatic_t1878418707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlStatic::OnDestroy()
extern "C"  void VisibilityControlStatic_OnDestroy_m3664850531 (VisibilityControlStatic_t1878418707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
