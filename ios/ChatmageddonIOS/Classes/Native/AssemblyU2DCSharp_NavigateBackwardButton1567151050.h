﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NavigateBackwardButton
struct  NavigateBackwardButton_t1567151050  : public SFXButton_t792651341
{
public:
	// UISprite NavigateBackwardButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_5;

public:
	inline static int32_t get_offset_of_buttonSprite_5() { return static_cast<int32_t>(offsetof(NavigateBackwardButton_t1567151050, ___buttonSprite_5)); }
	inline UISprite_t603616735 * get_buttonSprite_5() const { return ___buttonSprite_5; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_5() { return &___buttonSprite_5; }
	inline void set_buttonSprite_5(UISprite_t603616735 * value)
	{
		___buttonSprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
