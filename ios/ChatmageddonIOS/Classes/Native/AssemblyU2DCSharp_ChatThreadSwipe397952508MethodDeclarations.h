﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatThreadSwipe
struct ChatThreadSwipe_t397952508;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void ChatThreadSwipe::.ctor()
extern "C"  void ChatThreadSwipe__ctor_m3323508791 (ChatThreadSwipe_t397952508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadSwipe::Start()
extern "C"  void ChatThreadSwipe_Start_m3264619523 (ChatThreadSwipe_t397952508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadSwipe::UIClosing()
extern "C"  void ChatThreadSwipe_UIClosing_m4240989754 (ChatThreadSwipe_t397952508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadSwipe::OnSwipeLeft()
extern "C"  void ChatThreadSwipe_OnSwipeLeft_m1593913337 (ChatThreadSwipe_t397952508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadSwipe::OnSwipeRight()
extern "C"  void ChatThreadSwipe_OnSwipeRight_m3684420954 (ChatThreadSwipe_t397952508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadSwipe::OnDragUp(UnityEngine.Vector2)
extern "C"  void ChatThreadSwipe_OnDragUp_m676111531 (ChatThreadSwipe_t397952508 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
