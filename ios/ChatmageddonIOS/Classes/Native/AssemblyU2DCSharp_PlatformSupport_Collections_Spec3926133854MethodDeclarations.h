﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs
struct NotifyCollectionChangedEventArgs_t3926133854;
// System.Object
struct Il2CppObject;
// System.Collections.IList
struct IList_t3321498491;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2640700633.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m1724250992 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Object)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m862020742 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItem1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Object,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3771221205 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItem1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3851201893 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m4087227018 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, int32_t ___startingIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Object,System.Object)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m384819440 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItem1, Il2CppObject * ___oldItem2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Object,System.Object,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m1875456811 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItem1, Il2CppObject * ___oldItem2, int32_t ___index3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Collections.IList)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m916511866 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItems1, Il2CppObject * ___oldItems2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m1891075039 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItems1, Il2CppObject * ___oldItems2, int32_t ___startingIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Object,System.Int32,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m572509784 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItem1, int32_t ___index2, int32_t ___oldIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Int32,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3373372793 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, int32_t ___index2, int32_t ___oldIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::.ctor(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Collections.IList,System.Int32,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m4170679432 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItems1, Il2CppObject * ___oldItems2, int32_t ___newIndex3, int32_t ___oldIndex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::InitializeAddOrRemove(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs_InitializeAddOrRemove_m4037019006 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, int32_t ___startingIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::InitializeAdd(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs_InitializeAdd_m215605453 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItems1, int32_t ___newStartingIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::InitializeRemove(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs_InitializeRemove_m3587481744 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___oldItems1, int32_t ___oldStartingIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::InitializeMoveOrReplace(PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction,System.Collections.IList,System.Collections.IList,System.Int32,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs_InitializeMoveOrReplace_m912542046 (NotifyCollectionChangedEventArgs_t3926133854 * __this, int32_t ___action0, Il2CppObject * ___newItems1, Il2CppObject * ___oldItems2, int32_t ___startingIndex3, int32_t ___oldStartingIndex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::get_Action()
extern "C"  int32_t NotifyCollectionChangedEventArgs_get_Action_m265460470 (NotifyCollectionChangedEventArgs_t3926133854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::get_NewItems()
extern "C"  Il2CppObject * NotifyCollectionChangedEventArgs_get_NewItems_m2870875730 (NotifyCollectionChangedEventArgs_t3926133854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::get_OldItems()
extern "C"  Il2CppObject * NotifyCollectionChangedEventArgs_get_OldItems_m2486683407 (NotifyCollectionChangedEventArgs_t3926133854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::get_NewStartingIndex()
extern "C"  int32_t NotifyCollectionChangedEventArgs_get_NewStartingIndex_m1236395834 (NotifyCollectionChangedEventArgs_t3926133854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::get_OldStartingIndex()
extern "C"  int32_t NotifyCollectionChangedEventArgs_get_OldStartingIndex_m760473501 (NotifyCollectionChangedEventArgs_t3926133854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
