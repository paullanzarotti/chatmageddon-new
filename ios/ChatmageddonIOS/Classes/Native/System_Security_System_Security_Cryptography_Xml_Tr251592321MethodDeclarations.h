﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.TransformChain
struct TransformChain_t251592321;
// System.Security.Cryptography.Xml.Transform
struct Transform_t729562346;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "System_Security_System_Security_Cryptography_Xml_Tr729562346.h"

// System.Void System.Security.Cryptography.Xml.TransformChain::.ctor()
extern "C"  void TransformChain__ctor_m3860419267 (TransformChain_t251592321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.TransformChain::get_Count()
extern "C"  int32_t TransformChain_get_Count_m1372628351 (TransformChain_t251592321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.TransformChain::Add(System.Security.Cryptography.Xml.Transform)
extern "C"  void TransformChain_Add_m2414736858 (TransformChain_t251592321 * __this, Transform_t729562346 * ___transform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.Xml.TransformChain::GetEnumerator()
extern "C"  Il2CppObject * TransformChain_GetEnumerator_m195950403 (TransformChain_t251592321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
