﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIAtlas
struct UIAtlas_t1304615221;
// UISprite[]
struct UISpriteU5BU5D_t3315594182;

#include "AssemblyU2DCSharp_IPPickerBase4159478266.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPPickerSpriteBase
struct  IPPickerSpriteBase_t2462226553  : public IPPickerBase_t4159478266
{
public:
	// UIAtlas IPPickerSpriteBase::atlas
	UIAtlas_t1304615221 * ___atlas_13;
	// UISprite[] IPPickerSpriteBase::uiSprites
	UISpriteU5BU5D_t3315594182* ___uiSprites_14;

public:
	inline static int32_t get_offset_of_atlas_13() { return static_cast<int32_t>(offsetof(IPPickerSpriteBase_t2462226553, ___atlas_13)); }
	inline UIAtlas_t1304615221 * get_atlas_13() const { return ___atlas_13; }
	inline UIAtlas_t1304615221 ** get_address_of_atlas_13() { return &___atlas_13; }
	inline void set_atlas_13(UIAtlas_t1304615221 * value)
	{
		___atlas_13 = value;
		Il2CppCodeGenWriteBarrier(&___atlas_13, value);
	}

	inline static int32_t get_offset_of_uiSprites_14() { return static_cast<int32_t>(offsetof(IPPickerSpriteBase_t2462226553, ___uiSprites_14)); }
	inline UISpriteU5BU5D_t3315594182* get_uiSprites_14() const { return ___uiSprites_14; }
	inline UISpriteU5BU5D_t3315594182** get_address_of_uiSprites_14() { return &___uiSprites_14; }
	inline void set_uiSprites_14(UISpriteU5BU5D_t3315594182* value)
	{
		___uiSprites_14 = value;
		Il2CppCodeGenWriteBarrier(&___uiSprites_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
