﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_VividLight
struct CameraFilterPack_Blend2Camera_VividLight_t2257112822;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_VividLight::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_VividLight__ctor_m3476908735 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_VividLight::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_VividLight_get_material_m4021727616 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::Start()
extern "C"  void CameraFilterPack_Blend2Camera_VividLight_Start_m2425321539 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_VividLight_OnRenderImage_m2321756827 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_VividLight_OnValidate_m2503181242 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::Update()
extern "C"  void CameraFilterPack_Blend2Camera_VividLight_Update_m2099120800 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_VividLight_OnEnable_m1889713571 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_VividLight_OnDisable_m148761846 (CameraFilterPack_Blend2Camera_VividLight_t2257112822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
