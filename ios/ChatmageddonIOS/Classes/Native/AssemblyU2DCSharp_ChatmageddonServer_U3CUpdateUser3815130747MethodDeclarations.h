﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserFirstName>c__AnonStorey13
struct U3CUpdateUserFirstNameU3Ec__AnonStorey13_t3815130747;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserFirstName>c__AnonStorey13::.ctor()
extern "C"  void U3CUpdateUserFirstNameU3Ec__AnonStorey13__ctor_m3769753122 (U3CUpdateUserFirstNameU3Ec__AnonStorey13_t3815130747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserFirstName>c__AnonStorey13::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserFirstNameU3Ec__AnonStorey13_U3CU3Em__0_m396823699 (U3CUpdateUserFirstNameU3Ec__AnonStorey13_t3815130747 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
