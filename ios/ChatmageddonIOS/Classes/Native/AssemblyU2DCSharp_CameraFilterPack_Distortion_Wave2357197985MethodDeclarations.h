﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Wave_Horizontal
struct CameraFilterPack_Distortion_Wave_Horizontal_t2357197985;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Wave_Horizontal::.ctor()
extern "C"  void CameraFilterPack_Distortion_Wave_Horizontal__ctor_m3444296370 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Wave_Horizontal::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Wave_Horizontal_get_material_m2764379289 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::Start()
extern "C"  void CameraFilterPack_Distortion_Wave_Horizontal_Start_m3886867026 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Wave_Horizontal_OnRenderImage_m3321652290 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Wave_Horizontal_OnValidate_m2447286519 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::Update()
extern "C"  void CameraFilterPack_Distortion_Wave_Horizontal_Update_m1999230445 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Wave_Horizontal_OnDisable_m299365625 (CameraFilterPack_Distortion_Wave_Horizontal_t2357197985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
