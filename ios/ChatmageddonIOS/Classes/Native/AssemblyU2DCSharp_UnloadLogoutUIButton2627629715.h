﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LogOutTransition
struct LogOutTransition_t3259858399;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnloadLogoutUIButton
struct  UnloadLogoutUIButton_t2627629715  : public SFXButton_t792651341
{
public:
	// LogOutTransition UnloadLogoutUIButton::logoutTransition
	LogOutTransition_t3259858399 * ___logoutTransition_5;
	// System.Boolean UnloadLogoutUIButton::logout
	bool ___logout_6;

public:
	inline static int32_t get_offset_of_logoutTransition_5() { return static_cast<int32_t>(offsetof(UnloadLogoutUIButton_t2627629715, ___logoutTransition_5)); }
	inline LogOutTransition_t3259858399 * get_logoutTransition_5() const { return ___logoutTransition_5; }
	inline LogOutTransition_t3259858399 ** get_address_of_logoutTransition_5() { return &___logoutTransition_5; }
	inline void set_logoutTransition_5(LogOutTransition_t3259858399 * value)
	{
		___logoutTransition_5 = value;
		Il2CppCodeGenWriteBarrier(&___logoutTransition_5, value);
	}

	inline static int32_t get_offset_of_logout_6() { return static_cast<int32_t>(offsetof(UnloadLogoutUIButton_t2627629715, ___logout_6)); }
	inline bool get_logout_6() const { return ___logout_6; }
	inline bool* get_address_of_logout_6() { return &___logout_6; }
	inline void set_logout_6(bool value)
	{
		___logout_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
