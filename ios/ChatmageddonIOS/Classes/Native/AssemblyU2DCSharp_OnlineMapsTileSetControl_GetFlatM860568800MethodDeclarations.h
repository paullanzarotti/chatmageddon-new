﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate
struct GetFlatMarkerOffsetYDelegate_t860568800;
// System.Object
struct Il2CppObject;
// OnlineMapsMarker
struct OnlineMapsMarker_t3492166682;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3492166682.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFlatMarkerOffsetYDelegate__ctor_m478303529 (GetFlatMarkerOffsetYDelegate_t860568800 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate::Invoke(OnlineMapsMarker)
extern "C"  float GetFlatMarkerOffsetYDelegate_Invoke_m605949601 (GetFlatMarkerOffsetYDelegate_t860568800 * __this, OnlineMapsMarker_t3492166682 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate::BeginInvoke(OnlineMapsMarker,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFlatMarkerOffsetYDelegate_BeginInvoke_m2225179046 (GetFlatMarkerOffsetYDelegate_t860568800 * __this, OnlineMapsMarker_t3492166682 * ___marker0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate::EndInvoke(System.IAsyncResult)
extern "C"  float GetFlatMarkerOffsetYDelegate_EndInvoke_m1971556381 (GetFlatMarkerOffsetYDelegate_t860568800 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
