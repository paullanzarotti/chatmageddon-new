﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeTabUIScaler
struct  HomeTabUIScaler_t2633114472  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite HomeTabUIScaler::leftDivider
	UISprite_t603616735 * ___leftDivider_14;
	// UISprite HomeTabUIScaler::rightDivider
	UISprite_t603616735 * ___rightDivider_15;
	// UnityEngine.BoxCollider HomeTabUIScaler::friendCollider
	BoxCollider_t22920061 * ___friendCollider_16;
	// UISprite HomeTabUIScaler::friendBackground
	UISprite_t603616735 * ___friendBackground_17;
	// UnityEngine.BoxCollider HomeTabUIScaler::statusCollider
	BoxCollider_t22920061 * ___statusCollider_18;
	// UISprite HomeTabUIScaler::statusBackground
	UISprite_t603616735 * ___statusBackground_19;
	// UnityEngine.BoxCollider HomeTabUIScaler::inventoryCollider
	BoxCollider_t22920061 * ___inventoryCollider_20;
	// UISprite HomeTabUIScaler::inventoryBackground
	UISprite_t603616735 * ___inventoryBackground_21;

public:
	inline static int32_t get_offset_of_leftDivider_14() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___leftDivider_14)); }
	inline UISprite_t603616735 * get_leftDivider_14() const { return ___leftDivider_14; }
	inline UISprite_t603616735 ** get_address_of_leftDivider_14() { return &___leftDivider_14; }
	inline void set_leftDivider_14(UISprite_t603616735 * value)
	{
		___leftDivider_14 = value;
		Il2CppCodeGenWriteBarrier(&___leftDivider_14, value);
	}

	inline static int32_t get_offset_of_rightDivider_15() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___rightDivider_15)); }
	inline UISprite_t603616735 * get_rightDivider_15() const { return ___rightDivider_15; }
	inline UISprite_t603616735 ** get_address_of_rightDivider_15() { return &___rightDivider_15; }
	inline void set_rightDivider_15(UISprite_t603616735 * value)
	{
		___rightDivider_15 = value;
		Il2CppCodeGenWriteBarrier(&___rightDivider_15, value);
	}

	inline static int32_t get_offset_of_friendCollider_16() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___friendCollider_16)); }
	inline BoxCollider_t22920061 * get_friendCollider_16() const { return ___friendCollider_16; }
	inline BoxCollider_t22920061 ** get_address_of_friendCollider_16() { return &___friendCollider_16; }
	inline void set_friendCollider_16(BoxCollider_t22920061 * value)
	{
		___friendCollider_16 = value;
		Il2CppCodeGenWriteBarrier(&___friendCollider_16, value);
	}

	inline static int32_t get_offset_of_friendBackground_17() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___friendBackground_17)); }
	inline UISprite_t603616735 * get_friendBackground_17() const { return ___friendBackground_17; }
	inline UISprite_t603616735 ** get_address_of_friendBackground_17() { return &___friendBackground_17; }
	inline void set_friendBackground_17(UISprite_t603616735 * value)
	{
		___friendBackground_17 = value;
		Il2CppCodeGenWriteBarrier(&___friendBackground_17, value);
	}

	inline static int32_t get_offset_of_statusCollider_18() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___statusCollider_18)); }
	inline BoxCollider_t22920061 * get_statusCollider_18() const { return ___statusCollider_18; }
	inline BoxCollider_t22920061 ** get_address_of_statusCollider_18() { return &___statusCollider_18; }
	inline void set_statusCollider_18(BoxCollider_t22920061 * value)
	{
		___statusCollider_18 = value;
		Il2CppCodeGenWriteBarrier(&___statusCollider_18, value);
	}

	inline static int32_t get_offset_of_statusBackground_19() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___statusBackground_19)); }
	inline UISprite_t603616735 * get_statusBackground_19() const { return ___statusBackground_19; }
	inline UISprite_t603616735 ** get_address_of_statusBackground_19() { return &___statusBackground_19; }
	inline void set_statusBackground_19(UISprite_t603616735 * value)
	{
		___statusBackground_19 = value;
		Il2CppCodeGenWriteBarrier(&___statusBackground_19, value);
	}

	inline static int32_t get_offset_of_inventoryCollider_20() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___inventoryCollider_20)); }
	inline BoxCollider_t22920061 * get_inventoryCollider_20() const { return ___inventoryCollider_20; }
	inline BoxCollider_t22920061 ** get_address_of_inventoryCollider_20() { return &___inventoryCollider_20; }
	inline void set_inventoryCollider_20(BoxCollider_t22920061 * value)
	{
		___inventoryCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryCollider_20, value);
	}

	inline static int32_t get_offset_of_inventoryBackground_21() { return static_cast<int32_t>(offsetof(HomeTabUIScaler_t2633114472, ___inventoryBackground_21)); }
	inline UISprite_t603616735 * get_inventoryBackground_21() const { return ___inventoryBackground_21; }
	inline UISprite_t603616735 ** get_address_of_inventoryBackground_21() { return &___inventoryBackground_21; }
	inline void set_inventoryBackground_21(UISprite_t603616735 * value)
	{
		___inventoryBackground_21 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryBackground_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
