﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1038783543;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2674559435;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync(System.Byte[],System.UInt32)
extern "C"  AssetBundleCreateRequest_t1038783543 * AssetBundle_LoadFromMemoryAsync_m2389877678 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___binary0, uint32_t ___crc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync(System.Byte[])
extern "C"  AssetBundleCreateRequest_t1038783543 * AssetBundle_LoadFromMemoryAsync_m943964534 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern "C"  AssetBundleRequest_t2674559435 * AssetBundle_LoadAssetAsync_m664866985 (AssetBundle_t2054978754 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern "C"  AssetBundleRequest_t2674559435 * AssetBundle_LoadAssetAsync_Internal_m97478989 (AssetBundle_t2054978754 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern "C"  void AssetBundle_Unload_m167529087 (AssetBundle_t2054978754 * __this, bool ___unloadAllLoadedObjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
