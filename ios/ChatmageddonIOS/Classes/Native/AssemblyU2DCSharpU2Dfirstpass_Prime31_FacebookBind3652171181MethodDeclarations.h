﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookBinding
struct FacebookBinding_t3652171181;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookSessi177490973.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookSessi793857653.h"

// System.Void Prime31.FacebookBinding::.cctor()
extern "C"  void FacebookBinding__cctor_m3734407424 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::.ctor()
extern "C"  void FacebookBinding__ctor_m3705586059 (FacebookBinding_t3652171181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::babysitRequest(System.Boolean,System.Action)
extern "C"  void FacebookBinding_babysitRequest_m1963537864 (Il2CppObject * __this /* static, unused */, bool ___requiresPublishPermissions0, Action_t3226471752 * ___afterAuthAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookInit()
extern "C"  void FacebookBinding__facebookInit_m2501700238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::init()
extern "C"  void FacebookBinding_init_m1588036753 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookBinding::_facebookGetAppLaunchUrl()
extern "C"  String_t* FacebookBinding__facebookGetAppLaunchUrl_m2496234736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookBinding::getAppLaunchUrl()
extern "C"  String_t* FacebookBinding_getAppLaunchUrl_m226930271 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookSetSessionLoginBehavior(System.Int32)
extern "C"  void FacebookBinding__facebookSetSessionLoginBehavior_m3149168044 (Il2CppObject * __this /* static, unused */, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::setSessionLoginBehavior(Prime31.FacebookSessionLoginBehavior)
extern "C"  void FacebookBinding_setSessionLoginBehavior_m1742229086 (Il2CppObject * __this /* static, unused */, int32_t ___loginBehavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookEnableFrictionlessRequests()
extern "C"  void FacebookBinding__facebookEnableFrictionlessRequests_m3966658670 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::enableFrictionlessRequests()
extern "C"  void FacebookBinding_enableFrictionlessRequests_m552400007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookRenewCredentialsForAllFacebookAccounts()
extern "C"  void FacebookBinding__facebookRenewCredentialsForAllFacebookAccounts_m2785441383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::renewCredentialsForAllFacebookAccounts()
extern "C"  void FacebookBinding_renewCredentialsForAllFacebookAccounts_m879617814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.FacebookBinding::_facebookIsLoggedIn()
extern "C"  bool FacebookBinding__facebookIsLoggedIn_m883832119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.FacebookBinding::isSessionValid()
extern "C"  bool FacebookBinding_isSessionValid_m670047285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookBinding::_facebookGetFacebookAccessToken()
extern "C"  String_t* FacebookBinding__facebookGetFacebookAccessToken_m4054802230 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookBinding::getAccessToken()
extern "C"  String_t* FacebookBinding_getAccessToken_m1346514059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.FacebookBinding::_facebookGetSessionPermissions()
extern "C"  String_t* FacebookBinding__facebookGetSessionPermissions_m4272589593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Prime31.FacebookBinding::getSessionPermissions()
extern "C"  List_1_t2058570427 * FacebookBinding_getSessionPermissions_m4025367976 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLoginUsingDeprecatedAuthorizationFlowWithRequestedPermissions(System.String,System.String)
extern "C"  void FacebookBinding__facebookLoginUsingDeprecatedAuthorizationFlowWithRequestedPermissions_m1188162579 (Il2CppObject * __this /* static, unused */, String_t* ___perms0, String_t* ___urlSchemeSuffix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::loginUsingDeprecatedAuthorizationFlowWithRequestedPermissions(System.String[])
extern "C"  void FacebookBinding_loginUsingDeprecatedAuthorizationFlowWithRequestedPermissions_m2076216204 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::loginUsingDeprecatedAuthorizationFlowWithRequestedPermissions(System.String[],System.String)
extern "C"  void FacebookBinding_loginUsingDeprecatedAuthorizationFlowWithRequestedPermissions_m2914843364 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, String_t* ___urlSchemeSuffix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::login()
extern "C"  void FacebookBinding_login_m4012167712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::loginWithReadPermissions(System.String[])
extern "C"  void FacebookBinding_loginWithReadPermissions_m2167521328 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLoginWithRequestedPermissions(System.String,System.String)
extern "C"  void FacebookBinding__facebookLoginWithRequestedPermissions_m650329493 (Il2CppObject * __this /* static, unused */, String_t* ___perms0, String_t* ___urlSchemeSuffix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::loginWithReadPermissions(System.String[],System.String)
extern "C"  void FacebookBinding_loginWithReadPermissions_m800832560 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, String_t* ___urlSchemeSuffix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookReauthorizeWithReadPermissions(System.String)
extern "C"  void FacebookBinding__facebookReauthorizeWithReadPermissions_m707810126 (Il2CppObject * __this /* static, unused */, String_t* ___perms0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::reauthorizeWithReadPermissions(System.String[])
extern "C"  void FacebookBinding_reauthorizeWithReadPermissions_m830940907 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookReauthorizeWithPublishPermissions(System.String,System.Int32)
extern "C"  void FacebookBinding__facebookReauthorizeWithPublishPermissions_m3272947290 (Il2CppObject * __this /* static, unused */, String_t* ___perms0, int32_t ___defaultAudience1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::reauthorizeWithPublishPermissions(System.String[],Prime31.FacebookSessionDefaultAudience)
extern "C"  void FacebookBinding_reauthorizeWithPublishPermissions_m1546697156 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___permissions0, int32_t ___defaultAudience1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLogout()
extern "C"  void FacebookBinding__facebookLogout_m3886934006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::logout()
extern "C"  void FacebookBinding_logout_m2495428533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookShowDialog(System.String,System.String)
extern "C"  void FacebookBinding__facebookShowDialog_m2083005955 (Il2CppObject * __this /* static, unused */, String_t* ___dialogType0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::showDialog(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FacebookBinding_showDialog_m909275753 (Il2CppObject * __this /* static, unused */, String_t* ___dialogType0, Dictionary_2_t3943999495 * ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookGraphRequest(System.String,System.String,System.String)
extern "C"  void FacebookBinding__facebookGraphRequest_m976168735 (Il2CppObject * __this /* static, unused */, String_t* ___graphPath0, String_t* ___httpMethod1, String_t* ___jsonDict2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::graphRequest(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookBinding_graphRequest_m3212111745 (Il2CppObject * __this /* static, unused */, String_t* ___graphPath0, String_t* ___httpMethod1, Dictionary_2_t309261261 * ___keyValueHash2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.FacebookBinding::_facebookCanUserUseFacebookComposer()
extern "C"  bool FacebookBinding__facebookCanUserUseFacebookComposer_m3679787592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.FacebookBinding::canUserUseFacebookComposer()
extern "C"  bool FacebookBinding_canUserUseFacebookComposer_m2986040179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookShowFacebookComposer(System.String,System.String,System.String)
extern "C"  void FacebookBinding__facebookShowFacebookComposer_m2924719367 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___imagePath1, String_t* ___link2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::showFacebookComposer(System.String)
extern "C"  void FacebookBinding_showFacebookComposer_m2740212824 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::showFacebookComposer(System.String,System.String,System.String)
extern "C"  void FacebookBinding_showFacebookComposer_m2203575960 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___imagePath1, String_t* ___link2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookShowFacebookShareDialog(System.String)
extern "C"  void FacebookBinding__facebookShowFacebookShareDialog_m4006044688 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::showFacebookShareDialog(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookBinding_showFacebookShareDialog_m1429733526 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookSetAppVersion(System.String)
extern "C"  void FacebookBinding__facebookSetAppVersion_m3940769715 (Il2CppObject * __this /* static, unused */, String_t* ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::setAppVersion(System.String)
extern "C"  void FacebookBinding_setAppVersion_m4289877498 (Il2CppObject * __this /* static, unused */, String_t* ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLogEvent(System.String)
extern "C"  void FacebookBinding__facebookLogEvent_m1820608856 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLogEventWithParameters(System.String,System.String)
extern "C"  void FacebookBinding__facebookLogEventWithParameters_m3776666132 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::logEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookBinding_logEvent_m3874167392 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t309261261 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLogEventAndValueToSum(System.String,System.Double)
extern "C"  void FacebookBinding__facebookLogEventAndValueToSum_m1978405544 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, double ___valueToSum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::_facebookLogEventAndValueToSumWithParameters(System.String,System.Double,System.String)
extern "C"  void FacebookBinding__facebookLogEventAndValueToSumWithParameters_m2616615880 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, double ___valueToSum1, String_t* ___json2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::logEvent(System.String,System.Double,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookBinding_logEvent_m2941705548 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, double ___valueToSum1, Dictionary_2_t309261261 * ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding::<FacebookBinding>m__0()
extern "C"  void FacebookBinding_U3CFacebookBindingU3Em__0_m756178325 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
