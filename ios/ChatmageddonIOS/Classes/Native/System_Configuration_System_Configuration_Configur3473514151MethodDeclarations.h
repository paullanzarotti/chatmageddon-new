﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Array
struct Il2CppArray;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Configuration.ConfigurationProperty[]
struct ConfigurationPropertyU5BU5D_t3364493498;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Configur2048066811.h"

// System.Void System.Configuration.ConfigurationPropertyCollection::.ctor()
extern "C"  void ConfigurationPropertyCollection__ctor_m2795982958 (ConfigurationPropertyCollection_t3473514151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPropertyCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ConfigurationPropertyCollection_System_Collections_ICollection_CopyTo_m3374724930 (ConfigurationPropertyCollection_t3473514151 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Configuration.ConfigurationPropertyCollection::get_Count()
extern "C"  int32_t ConfigurationPropertyCollection_get_Count_m1518273402 (ConfigurationPropertyCollection_t3473514151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationProperty System.Configuration.ConfigurationPropertyCollection::get_Item(System.String)
extern "C"  ConfigurationProperty_t2048066811 * ConfigurationPropertyCollection_get_Item_m1250072351 (ConfigurationPropertyCollection_t3473514151 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationPropertyCollection::get_IsSynchronized()
extern "C"  bool ConfigurationPropertyCollection_get_IsSynchronized_m2330227857 (ConfigurationPropertyCollection_t3473514151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ConfigurationPropertyCollection::get_SyncRoot()
extern "C"  Il2CppObject * ConfigurationPropertyCollection_get_SyncRoot_m2789197201 (ConfigurationPropertyCollection_t3473514151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPropertyCollection::Add(System.Configuration.ConfigurationProperty)
extern "C"  void ConfigurationPropertyCollection_Add_m3481099429 (ConfigurationPropertyCollection_t3473514151 * __this, ConfigurationProperty_t2048066811 * ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationPropertyCollection::Contains(System.String)
extern "C"  bool ConfigurationPropertyCollection_Contains_m3078919313 (ConfigurationPropertyCollection_t3473514151 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPropertyCollection::CopyTo(System.Configuration.ConfigurationProperty[],System.Int32)
extern "C"  void ConfigurationPropertyCollection_CopyTo_m1488538251 (ConfigurationPropertyCollection_t3473514151 * __this, ConfigurationPropertyU5BU5D_t3364493498* ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Configuration.ConfigurationPropertyCollection::GetEnumerator()
extern "C"  Il2CppObject * ConfigurationPropertyCollection_GetEnumerator_m3414286410 (ConfigurationPropertyCollection_t3473514151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationPropertyCollection::Remove(System.String)
extern "C"  bool ConfigurationPropertyCollection_Remove_m3152791538 (ConfigurationPropertyCollection_t3473514151 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPropertyCollection::Clear()
extern "C"  void ConfigurationPropertyCollection_Clear_m3796961277 (ConfigurationPropertyCollection_t3473514151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
