﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.UnibillConfiguration/<getItemById>c__AnonStorey1
struct U3CgetItemByIdU3Ec__AnonStorey1_t469792186;
// PurchasableItem
struct PurchasableItem_t3963353899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void Unibill.Impl.UnibillConfiguration/<getItemById>c__AnonStorey1::.ctor()
extern "C"  void U3CgetItemByIdU3Ec__AnonStorey1__ctor_m4159882923 (U3CgetItemByIdU3Ec__AnonStorey1_t469792186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration/<getItemById>c__AnonStorey1::<>m__0(PurchasableItem)
extern "C"  bool U3CgetItemByIdU3Ec__AnonStorey1_U3CU3Em__0_m1468110205 (U3CgetItemByIdU3Ec__AnonStorey1_t469792186 * __this, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
