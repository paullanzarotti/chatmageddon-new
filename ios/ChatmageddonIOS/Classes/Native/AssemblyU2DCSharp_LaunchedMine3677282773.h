﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;

#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_MineResultStatus1141738764.h"
#include "AssemblyU2DCSharp_LaunchDirection3410220992.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_WarefareState701596006.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchedMine
struct  LaunchedMine_t3677282773  : public Mine_t2729441277
{
public:
	// MineResultStatus LaunchedMine::resultStatus
	int32_t ___resultStatus_13;
	// LaunchDirection LaunchedMine::direction
	int32_t ___direction_14;
	// Friend LaunchedMine::friend
	Friend_t3555014108 * ___friend_15;
	// System.DateTime LaunchedMine::completeTime
	DateTime_t693205669  ___completeTime_16;
	// System.Single LaunchedMine::maxResultPoints
	float ___maxResultPoints_17;
	// System.Int32 LaunchedMine::resultPoints
	int32_t ___resultPoints_18;
	// WarefareState LaunchedMine::resultAwardTo
	int32_t ___resultAwardTo_19;

public:
	inline static int32_t get_offset_of_resultStatus_13() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___resultStatus_13)); }
	inline int32_t get_resultStatus_13() const { return ___resultStatus_13; }
	inline int32_t* get_address_of_resultStatus_13() { return &___resultStatus_13; }
	inline void set_resultStatus_13(int32_t value)
	{
		___resultStatus_13 = value;
	}

	inline static int32_t get_offset_of_direction_14() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___direction_14)); }
	inline int32_t get_direction_14() const { return ___direction_14; }
	inline int32_t* get_address_of_direction_14() { return &___direction_14; }
	inline void set_direction_14(int32_t value)
	{
		___direction_14 = value;
	}

	inline static int32_t get_offset_of_friend_15() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___friend_15)); }
	inline Friend_t3555014108 * get_friend_15() const { return ___friend_15; }
	inline Friend_t3555014108 ** get_address_of_friend_15() { return &___friend_15; }
	inline void set_friend_15(Friend_t3555014108 * value)
	{
		___friend_15 = value;
		Il2CppCodeGenWriteBarrier(&___friend_15, value);
	}

	inline static int32_t get_offset_of_completeTime_16() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___completeTime_16)); }
	inline DateTime_t693205669  get_completeTime_16() const { return ___completeTime_16; }
	inline DateTime_t693205669 * get_address_of_completeTime_16() { return &___completeTime_16; }
	inline void set_completeTime_16(DateTime_t693205669  value)
	{
		___completeTime_16 = value;
	}

	inline static int32_t get_offset_of_maxResultPoints_17() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___maxResultPoints_17)); }
	inline float get_maxResultPoints_17() const { return ___maxResultPoints_17; }
	inline float* get_address_of_maxResultPoints_17() { return &___maxResultPoints_17; }
	inline void set_maxResultPoints_17(float value)
	{
		___maxResultPoints_17 = value;
	}

	inline static int32_t get_offset_of_resultPoints_18() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___resultPoints_18)); }
	inline int32_t get_resultPoints_18() const { return ___resultPoints_18; }
	inline int32_t* get_address_of_resultPoints_18() { return &___resultPoints_18; }
	inline void set_resultPoints_18(int32_t value)
	{
		___resultPoints_18 = value;
	}

	inline static int32_t get_offset_of_resultAwardTo_19() { return static_cast<int32_t>(offsetof(LaunchedMine_t3677282773, ___resultAwardTo_19)); }
	inline int32_t get_resultAwardTo_19() const { return ___resultAwardTo_19; }
	inline int32_t* get_address_of_resultAwardTo_19() { return &___resultAwardTo_19; }
	inline void set_resultAwardTo_19(int32_t value)
	{
		___resultAwardTo_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
