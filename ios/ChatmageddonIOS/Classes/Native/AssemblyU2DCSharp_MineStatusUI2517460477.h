﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// MultiImageNGUIAnimation
struct MultiImageNGUIAnimation_t45560503;

#include "AssemblyU2DCSharp_StatusUI2174902556.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineStatusUI
struct  MineStatusUI_t2517460477  : public StatusUI_t2174902556
{
public:
	// UILabel MineStatusUI::stateInfoLabel
	UILabel_t1795115428 * ___stateInfoLabel_4;
	// UILabel MineStatusUI::stateTimeLabel
	UILabel_t1795115428 * ___stateTimeLabel_5;
	// UILabel MineStatusUI::pointsInfoLabel
	UILabel_t1795115428 * ___pointsInfoLabel_6;
	// UILabel MineStatusUI::pointsLabel
	UILabel_t1795115428 * ___pointsLabel_7;
	// UISprite MineStatusUI::flameSprite
	UISprite_t603616735 * ___flameSprite_8;
	// MultiImageNGUIAnimation MineStatusUI::flameAnim
	MultiImageNGUIAnimation_t45560503 * ___flameAnim_9;
	// UnityEngine.Color MineStatusUI::avatarFadeColour
	Color_t2020392075  ___avatarFadeColour_10;

public:
	inline static int32_t get_offset_of_stateInfoLabel_4() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___stateInfoLabel_4)); }
	inline UILabel_t1795115428 * get_stateInfoLabel_4() const { return ___stateInfoLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_stateInfoLabel_4() { return &___stateInfoLabel_4; }
	inline void set_stateInfoLabel_4(UILabel_t1795115428 * value)
	{
		___stateInfoLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___stateInfoLabel_4, value);
	}

	inline static int32_t get_offset_of_stateTimeLabel_5() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___stateTimeLabel_5)); }
	inline UILabel_t1795115428 * get_stateTimeLabel_5() const { return ___stateTimeLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_stateTimeLabel_5() { return &___stateTimeLabel_5; }
	inline void set_stateTimeLabel_5(UILabel_t1795115428 * value)
	{
		___stateTimeLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___stateTimeLabel_5, value);
	}

	inline static int32_t get_offset_of_pointsInfoLabel_6() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___pointsInfoLabel_6)); }
	inline UILabel_t1795115428 * get_pointsInfoLabel_6() const { return ___pointsInfoLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_pointsInfoLabel_6() { return &___pointsInfoLabel_6; }
	inline void set_pointsInfoLabel_6(UILabel_t1795115428 * value)
	{
		___pointsInfoLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___pointsInfoLabel_6, value);
	}

	inline static int32_t get_offset_of_pointsLabel_7() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___pointsLabel_7)); }
	inline UILabel_t1795115428 * get_pointsLabel_7() const { return ___pointsLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_pointsLabel_7() { return &___pointsLabel_7; }
	inline void set_pointsLabel_7(UILabel_t1795115428 * value)
	{
		___pointsLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___pointsLabel_7, value);
	}

	inline static int32_t get_offset_of_flameSprite_8() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___flameSprite_8)); }
	inline UISprite_t603616735 * get_flameSprite_8() const { return ___flameSprite_8; }
	inline UISprite_t603616735 ** get_address_of_flameSprite_8() { return &___flameSprite_8; }
	inline void set_flameSprite_8(UISprite_t603616735 * value)
	{
		___flameSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___flameSprite_8, value);
	}

	inline static int32_t get_offset_of_flameAnim_9() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___flameAnim_9)); }
	inline MultiImageNGUIAnimation_t45560503 * get_flameAnim_9() const { return ___flameAnim_9; }
	inline MultiImageNGUIAnimation_t45560503 ** get_address_of_flameAnim_9() { return &___flameAnim_9; }
	inline void set_flameAnim_9(MultiImageNGUIAnimation_t45560503 * value)
	{
		___flameAnim_9 = value;
		Il2CppCodeGenWriteBarrier(&___flameAnim_9, value);
	}

	inline static int32_t get_offset_of_avatarFadeColour_10() { return static_cast<int32_t>(offsetof(MineStatusUI_t2517460477, ___avatarFadeColour_10)); }
	inline Color_t2020392075  get_avatarFadeColour_10() const { return ___avatarFadeColour_10; }
	inline Color_t2020392075 * get_address_of_avatarFadeColour_10() { return &___avatarFadeColour_10; }
	inline void set_avatarFadeColour_10(Color_t2020392075  value)
	{
		___avatarFadeColour_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
