﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Artefact
struct CameraFilterPack_TV_Artefact_t2719757314;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Artefact::.ctor()
extern "C"  void CameraFilterPack_TV_Artefact__ctor_m2909557063 (CameraFilterPack_TV_Artefact_t2719757314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Artefact::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Artefact_get_material_m1018898912 (CameraFilterPack_TV_Artefact_t2719757314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::Start()
extern "C"  void CameraFilterPack_TV_Artefact_Start_m3238553251 (CameraFilterPack_TV_Artefact_t2719757314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Artefact_OnRenderImage_m2069029707 (CameraFilterPack_TV_Artefact_t2719757314 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::Update()
extern "C"  void CameraFilterPack_TV_Artefact_Update_m358810512 (CameraFilterPack_TV_Artefact_t2719757314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::OnDisable()
extern "C"  void CameraFilterPack_TV_Artefact_OnDisable_m2009804238 (CameraFilterPack_TV_Artefact_t2719757314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
