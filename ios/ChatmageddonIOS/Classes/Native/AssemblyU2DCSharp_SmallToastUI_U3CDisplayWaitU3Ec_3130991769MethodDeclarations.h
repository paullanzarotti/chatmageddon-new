﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmallToastUI/<DisplayWait>c__Iterator0
struct U3CDisplayWaitU3Ec__Iterator0_t3130991769;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SmallToastUI/<DisplayWait>c__Iterator0::.ctor()
extern "C"  void U3CDisplayWaitU3Ec__Iterator0__ctor_m475551738 (U3CDisplayWaitU3Ec__Iterator0_t3130991769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmallToastUI/<DisplayWait>c__Iterator0::MoveNext()
extern "C"  bool U3CDisplayWaitU3Ec__Iterator0_MoveNext_m2976633422 (U3CDisplayWaitU3Ec__Iterator0_t3130991769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SmallToastUI/<DisplayWait>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDisplayWaitU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m785260274 (U3CDisplayWaitU3Ec__Iterator0_t3130991769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SmallToastUI/<DisplayWait>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDisplayWaitU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1927040314 (U3CDisplayWaitU3Ec__Iterator0_t3130991769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI/<DisplayWait>c__Iterator0::Dispose()
extern "C"  void U3CDisplayWaitU3Ec__Iterator0_Dispose_m903329603 (U3CDisplayWaitU3Ec__Iterator0_t3130991769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI/<DisplayWait>c__Iterator0::Reset()
extern "C"  void U3CDisplayWaitU3Ec__Iterator0_Reset_m3082141065 (U3CDisplayWaitU3Ec__Iterator0_t3130991769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
