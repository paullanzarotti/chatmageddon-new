﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`3<System.Boolean,System.String,System.String>
struct Action_3_t3864773326;
// ChatmageddonServer
struct ChatmageddonServer_t594474938;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AboutAndHelpType4001655409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39
struct  U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541  : public Il2CppObject
{
public:
	// System.Action`3<System.Boolean,System.String,System.String> ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39::callBack
	Action_3_t3864773326 * ___callBack_0;
	// AboutAndHelpType ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39::type
	int32_t ___type_1;
	// ChatmageddonServer ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39::$this
	ChatmageddonServer_t594474938 * ___U24this_2;

public:
	inline static int32_t get_offset_of_callBack_0() { return static_cast<int32_t>(offsetof(U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541, ___callBack_0)); }
	inline Action_3_t3864773326 * get_callBack_0() const { return ___callBack_0; }
	inline Action_3_t3864773326 ** get_address_of_callBack_0() { return &___callBack_0; }
	inline void set_callBack_0(Action_3_t3864773326 * value)
	{
		___callBack_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541, ___U24this_2)); }
	inline ChatmageddonServer_t594474938 * get_U24this_2() const { return ___U24this_2; }
	inline ChatmageddonServer_t594474938 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ChatmageddonServer_t594474938 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
