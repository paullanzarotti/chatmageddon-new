﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResendVerificationButton
struct  ResendVerificationButton_t1304384308  : public SFXButton_t792651341
{
public:
	// UIInput ResendVerificationButton::codeInput
	UIInput_t860674234 * ___codeInput_5;

public:
	inline static int32_t get_offset_of_codeInput_5() { return static_cast<int32_t>(offsetof(ResendVerificationButton_t1304384308, ___codeInput_5)); }
	inline UIInput_t860674234 * get_codeInput_5() const { return ___codeInput_5; }
	inline UIInput_t860674234 ** get_address_of_codeInput_5() { return &___codeInput_5; }
	inline void set_codeInput_5(UIInput_t860674234 * value)
	{
		___codeInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeInput_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
