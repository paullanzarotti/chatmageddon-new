﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<LoginViaFacebook>c__AnonStorey5<System.Object>
struct U3CLoginViaFacebookU3Ec__AnonStorey5_t3975606441;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<LoginViaFacebook>c__AnonStorey5<System.Object>::.ctor()
extern "C"  void U3CLoginViaFacebookU3Ec__AnonStorey5__ctor_m1865496110_gshared (U3CLoginViaFacebookU3Ec__AnonStorey5_t3975606441 * __this, const MethodInfo* method);
#define U3CLoginViaFacebookU3Ec__AnonStorey5__ctor_m1865496110(__this, method) ((  void (*) (U3CLoginViaFacebookU3Ec__AnonStorey5_t3975606441 *, const MethodInfo*))U3CLoginViaFacebookU3Ec__AnonStorey5__ctor_m1865496110_gshared)(__this, method)
// System.Void BaseServer`1/<LoginViaFacebook>c__AnonStorey5<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLoginViaFacebookU3Ec__AnonStorey5_U3CU3Em__0_m1131918825_gshared (U3CLoginViaFacebookU3Ec__AnonStorey5_t3975606441 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CLoginViaFacebookU3Ec__AnonStorey5_U3CU3Em__0_m1131918825(__this, ___request0, ___response1, method) ((  void (*) (U3CLoginViaFacebookU3Ec__AnonStorey5_t3975606441 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CLoginViaFacebookU3Ec__AnonStorey5_U3CU3Em__0_m1131918825_gshared)(__this, ___request0, ___response1, method)
