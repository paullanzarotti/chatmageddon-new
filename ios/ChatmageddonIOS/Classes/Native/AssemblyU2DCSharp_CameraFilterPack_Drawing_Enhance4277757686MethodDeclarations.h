﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_EnhancedComics
struct CameraFilterPack_Drawing_EnhancedComics_t4277757686;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_EnhancedComics::.ctor()
extern "C"  void CameraFilterPack_Drawing_EnhancedComics__ctor_m1022136833 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_EnhancedComics::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_EnhancedComics_get_material_m3777051406 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::Start()
extern "C"  void CameraFilterPack_Drawing_EnhancedComics_Start_m3438475985 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_EnhancedComics_OnRenderImage_m1808486305 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::OnValidate()
extern "C"  void CameraFilterPack_Drawing_EnhancedComics_OnValidate_m3620055018 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::Update()
extern "C"  void CameraFilterPack_Drawing_EnhancedComics_Update_m1000567636 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::OnDisable()
extern "C"  void CameraFilterPack_Drawing_EnhancedComics_OnDisable_m604899962 (CameraFilterPack_Drawing_EnhancedComics_t4277757686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
