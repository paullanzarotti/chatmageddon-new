﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>
struct Dictionary_2_t316761205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1636785907.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22369073723.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m426532933_gshared (Enumerator_t1636785907 * __this, Dictionary_2_t316761205 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m426532933(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1636785907 *, Dictionary_2_t316761205 *, const MethodInfo*))Enumerator__ctor_m426532933_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1826349162_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1826349162(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1826349162_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3415398420_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3415398420(__this, method) ((  void (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3415398420_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1344016491_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1344016491(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1344016491_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4146714620_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4146714620(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4146714620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1681872826_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1681872826(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1681872826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2045677544_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2045677544(__this, method) ((  bool (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_MoveNext_m2045677544_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::get_Current()
extern "C"  KeyValuePair_2_t2369073723  Enumerator_get_Current_m2539709128_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2539709128(__this, method) ((  KeyValuePair_2_t2369073723  (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_get_Current_m2539709128_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3992455305_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3992455305(__this, method) ((  Il2CppObject * (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_get_CurrentKey_m3992455305_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::get_CurrentValue()
extern "C"  Decimal_t724701077  Enumerator_get_CurrentValue_m599508961_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m599508961(__this, method) ((  Decimal_t724701077  (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_get_CurrentValue_m599508961_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::Reset()
extern "C"  void Enumerator_Reset_m652953371_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_Reset_m652953371(__this, method) ((  void (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_Reset_m652953371_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3710756016_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3710756016(__this, method) ((  void (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_VerifyState_m3710756016_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2312053988_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2312053988(__this, method) ((  void (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_VerifyCurrent_m2312053988_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Decimal>::Dispose()
extern "C"  void Enumerator_Dispose_m1228939197_gshared (Enumerator_t1636785907 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1228939197(__this, method) ((  void (*) (Enumerator_t1636785907 *, const MethodInfo*))Enumerator_Dispose_m1228939197_gshared)(__this, method)
