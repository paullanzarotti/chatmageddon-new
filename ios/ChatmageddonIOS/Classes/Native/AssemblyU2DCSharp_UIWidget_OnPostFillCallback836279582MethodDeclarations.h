﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t836279582;
// System.Object
struct Il2CppObject;
// UIWidget
struct UIWidget_t1453041918;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t2464096222;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t2464096221;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t1094906160;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_UIWidget1453041918.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UIWidget/OnPostFillCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostFillCallback__ctor_m1735764407 (OnPostFillCallback_t836279582 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget/OnPostFillCallback::Invoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void OnPostFillCallback_Invoke_m2953004057 (OnPostFillCallback_t836279582 * __this, UIWidget_t1453041918 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UIWidget/OnPostFillCallback::BeginInvoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPostFillCallback_BeginInvoke_m3876064332 (OnPostFillCallback_t836279582 * __this, UIWidget_t1453041918 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t2464096222 * ___verts2, BetterList_1_t2464096221 * ___uvs3, BetterList_1_t1094906160 * ___cols4, AsyncCallback_t163412349 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget/OnPostFillCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostFillCallback_EndInvoke_m1375087617 (OnPostFillCallback_t836279582 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
