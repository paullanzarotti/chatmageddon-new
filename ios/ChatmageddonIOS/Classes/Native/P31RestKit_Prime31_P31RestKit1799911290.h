﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.P31RestKit
struct  P31RestKit_t1799911290  : public Il2CppObject
{
public:
	// System.String Prime31.P31RestKit::_baseUrl
	String_t* ____baseUrl_0;
	// System.Boolean Prime31.P31RestKit::debugRequests
	bool ___debugRequests_1;
	// System.Boolean Prime31.P31RestKit::forceJsonResponse
	bool ___forceJsonResponse_2;
	// UnityEngine.GameObject Prime31.P31RestKit::_surrogateGameObject
	GameObject_t1756533147 * ____surrogateGameObject_3;
	// UnityEngine.MonoBehaviour Prime31.P31RestKit::_surrogateMonobehaviour
	MonoBehaviour_t1158329972 * ____surrogateMonobehaviour_4;

public:
	inline static int32_t get_offset_of__baseUrl_0() { return static_cast<int32_t>(offsetof(P31RestKit_t1799911290, ____baseUrl_0)); }
	inline String_t* get__baseUrl_0() const { return ____baseUrl_0; }
	inline String_t** get_address_of__baseUrl_0() { return &____baseUrl_0; }
	inline void set__baseUrl_0(String_t* value)
	{
		____baseUrl_0 = value;
		Il2CppCodeGenWriteBarrier(&____baseUrl_0, value);
	}

	inline static int32_t get_offset_of_debugRequests_1() { return static_cast<int32_t>(offsetof(P31RestKit_t1799911290, ___debugRequests_1)); }
	inline bool get_debugRequests_1() const { return ___debugRequests_1; }
	inline bool* get_address_of_debugRequests_1() { return &___debugRequests_1; }
	inline void set_debugRequests_1(bool value)
	{
		___debugRequests_1 = value;
	}

	inline static int32_t get_offset_of_forceJsonResponse_2() { return static_cast<int32_t>(offsetof(P31RestKit_t1799911290, ___forceJsonResponse_2)); }
	inline bool get_forceJsonResponse_2() const { return ___forceJsonResponse_2; }
	inline bool* get_address_of_forceJsonResponse_2() { return &___forceJsonResponse_2; }
	inline void set_forceJsonResponse_2(bool value)
	{
		___forceJsonResponse_2 = value;
	}

	inline static int32_t get_offset_of__surrogateGameObject_3() { return static_cast<int32_t>(offsetof(P31RestKit_t1799911290, ____surrogateGameObject_3)); }
	inline GameObject_t1756533147 * get__surrogateGameObject_3() const { return ____surrogateGameObject_3; }
	inline GameObject_t1756533147 ** get_address_of__surrogateGameObject_3() { return &____surrogateGameObject_3; }
	inline void set__surrogateGameObject_3(GameObject_t1756533147 * value)
	{
		____surrogateGameObject_3 = value;
		Il2CppCodeGenWriteBarrier(&____surrogateGameObject_3, value);
	}

	inline static int32_t get_offset_of__surrogateMonobehaviour_4() { return static_cast<int32_t>(offsetof(P31RestKit_t1799911290, ____surrogateMonobehaviour_4)); }
	inline MonoBehaviour_t1158329972 * get__surrogateMonobehaviour_4() const { return ____surrogateMonobehaviour_4; }
	inline MonoBehaviour_t1158329972 ** get_address_of__surrogateMonobehaviour_4() { return &____surrogateMonobehaviour_4; }
	inline void set__surrogateMonobehaviour_4(MonoBehaviour_t1158329972 * value)
	{
		____surrogateMonobehaviour_4 = value;
		Il2CppCodeGenWriteBarrier(&____surrogateMonobehaviour_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
