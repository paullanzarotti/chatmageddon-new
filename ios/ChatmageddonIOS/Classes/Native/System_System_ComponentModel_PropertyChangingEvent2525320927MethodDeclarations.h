﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.PropertyChangingEventArgs
struct PropertyChangingEventArgs_t2525320927;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.ComponentModel.PropertyChangingEventArgs::.ctor(System.String)
extern "C"  void PropertyChangingEventArgs__ctor_m1553868686 (PropertyChangingEventArgs_t2525320927 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.PropertyChangingEventArgs::get_PropertyName()
extern "C"  String_t* PropertyChangingEventArgs_get_PropertyName_m2997079058 (PropertyChangingEventArgs_t2525320927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
