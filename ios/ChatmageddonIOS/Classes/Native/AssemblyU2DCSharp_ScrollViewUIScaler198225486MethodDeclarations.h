﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollViewUIScaler
struct ScrollViewUIScaler_t198225486;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollViewUIScaler::.ctor()
extern "C"  void ScrollViewUIScaler__ctor_m1353654585 (ScrollViewUIScaler_t198225486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewUIScaler::GlobalUIScale()
extern "C"  void ScrollViewUIScaler_GlobalUIScale_m3665025504 (ScrollViewUIScaler_t198225486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
