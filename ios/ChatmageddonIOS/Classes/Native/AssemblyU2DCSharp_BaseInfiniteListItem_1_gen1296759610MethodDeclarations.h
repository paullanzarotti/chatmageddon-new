﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<FriendsListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m1956640270(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1296759610 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<FriendsListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m3538125275(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t1296759610 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FriendsListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m1311749849(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1296759610 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FriendsListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m3956351604(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1296759610 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FriendsListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m1647505079(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t1296759610 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<FriendsListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m2402068398(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1296759610 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FriendsListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m3971083649(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1296759610 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
