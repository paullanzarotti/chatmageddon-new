﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StatusListItem
struct StatusListItem_t459202613;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusUI
struct  StatusUI_t2174902556  : public MonoBehaviour_t1158329972
{
public:
	// StatusListItem StatusUI::item
	StatusListItem_t459202613 * ___item_2;
	// System.Boolean StatusUI::uiActive
	bool ___uiActive_3;

public:
	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(StatusUI_t2174902556, ___item_2)); }
	inline StatusListItem_t459202613 * get_item_2() const { return ___item_2; }
	inline StatusListItem_t459202613 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(StatusListItem_t459202613 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_2, value);
	}

	inline static int32_t get_offset_of_uiActive_3() { return static_cast<int32_t>(offsetof(StatusUI_t2174902556, ___uiActive_3)); }
	inline bool get_uiActive_3() const { return ___uiActive_3; }
	inline bool* get_address_of_uiActive_3() { return &___uiActive_3; }
	inline void set_uiActive_3(bool value)
	{
		___uiActive_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
