﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<LeaderboardNavScreen>
struct DefaultComparer_t845313945;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<LeaderboardNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m3945700736_gshared (DefaultComparer_t845313945 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3945700736(__this, method) ((  void (*) (DefaultComparer_t845313945 *, const MethodInfo*))DefaultComparer__ctor_m3945700736_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<LeaderboardNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2461053241_gshared (DefaultComparer_t845313945 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2461053241(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t845313945 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m2461053241_gshared)(__this, ___x0, ___y1, method)
