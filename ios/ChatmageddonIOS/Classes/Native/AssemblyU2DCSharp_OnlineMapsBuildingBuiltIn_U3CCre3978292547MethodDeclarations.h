﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuildingBuiltIn/<Create>c__AnonStorey0
struct U3CCreateU3Ec__AnonStorey0_t3978292547;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void OnlineMapsBuildingBuiltIn/<Create>c__AnonStorey0::.ctor()
extern "C"  void U3CCreateU3Ec__AnonStorey0__ctor_m2003320584 (U3CCreateU3Ec__AnonStorey0_t3978292547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 OnlineMapsBuildingBuiltIn/<Create>c__AnonStorey0::<>m__0(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  U3CCreateU3Ec__AnonStorey0_U3CU3Em__0_m816259054 (U3CCreateU3Ec__AnonStorey0_t3978292547 * __this, Vector3_t2243707580  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
