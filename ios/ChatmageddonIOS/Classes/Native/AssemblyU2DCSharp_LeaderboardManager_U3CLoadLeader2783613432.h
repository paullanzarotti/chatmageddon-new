﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LeaderboardManager
struct LeaderboardManager_t1534320874;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardManager/<LoadLeaderboardPage>c__AnonStorey0
struct  U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432  : public Il2CppObject
{
public:
	// System.Boolean LeaderboardManager/<LoadLeaderboardPage>c__AnonStorey0::daily
	bool ___daily_0;
	// System.Int32 LeaderboardManager/<LoadLeaderboardPage>c__AnonStorey0::page
	int32_t ___page_1;
	// LeaderboardManager LeaderboardManager/<LoadLeaderboardPage>c__AnonStorey0::$this
	LeaderboardManager_t1534320874 * ___U24this_2;

public:
	inline static int32_t get_offset_of_daily_0() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432, ___daily_0)); }
	inline bool get_daily_0() const { return ___daily_0; }
	inline bool* get_address_of_daily_0() { return &___daily_0; }
	inline void set_daily_0(bool value)
	{
		___daily_0 = value;
	}

	inline static int32_t get_offset_of_page_1() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432, ___page_1)); }
	inline int32_t get_page_1() const { return ___page_1; }
	inline int32_t* get_address_of_page_1() { return &___page_1; }
	inline void set_page_1(int32_t value)
	{
		___page_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432, ___U24this_2)); }
	inline LeaderboardManager_t1534320874 * get_U24this_2() const { return ___U24this_2; }
	inline LeaderboardManager_t1534320874 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LeaderboardManager_t1534320874 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
