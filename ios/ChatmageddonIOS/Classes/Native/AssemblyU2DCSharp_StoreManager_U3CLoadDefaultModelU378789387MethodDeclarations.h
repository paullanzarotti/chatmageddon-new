﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreManager/<LoadDefaultModel>c__Iterator3
struct U3CLoadDefaultModelU3Ec__Iterator3_t378789387;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StoreManager/<LoadDefaultModel>c__Iterator3::.ctor()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator3__ctor_m3916224190 (U3CLoadDefaultModelU3Ec__Iterator3_t378789387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StoreManager/<LoadDefaultModel>c__Iterator3::MoveNext()
extern "C"  bool U3CLoadDefaultModelU3Ec__Iterator3_MoveNext_m3350594570 (U3CLoadDefaultModelU3Ec__Iterator3_t378789387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<LoadDefaultModel>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3970077148 (U3CLoadDefaultModelU3Ec__Iterator3_t378789387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StoreManager/<LoadDefaultModel>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2234233092 (U3CLoadDefaultModelU3Ec__Iterator3_t378789387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<LoadDefaultModel>c__Iterator3::Dispose()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator3_Dispose_m2360670437 (U3CLoadDefaultModelU3Ec__Iterator3_t378789387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager/<LoadDefaultModel>c__Iterator3::Reset()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator3_Reset_m3628094515 (U3CLoadDefaultModelU3Ec__Iterator3_t378789387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
