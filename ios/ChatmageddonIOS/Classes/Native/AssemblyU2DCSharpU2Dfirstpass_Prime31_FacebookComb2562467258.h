﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`2<System.String,Prime31.FacebookFriendsResult>
struct Action_2_t2665012870;

#include "P31RestKit_Prime31_MonoBehaviourGUI1135617291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookComboUI
struct  FacebookComboUI_t2562467258  : public MonoBehaviourGUI_t1135617291
{
public:
	// UnityEngine.GameObject Prime31.FacebookComboUI::cube
	GameObject_t1756533147 * ___cube_18;
	// System.String Prime31.FacebookComboUI::_userId
	String_t* ____userId_19;

public:
	inline static int32_t get_offset_of_cube_18() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258, ___cube_18)); }
	inline GameObject_t1756533147 * get_cube_18() const { return ___cube_18; }
	inline GameObject_t1756533147 ** get_address_of_cube_18() { return &___cube_18; }
	inline void set_cube_18(GameObject_t1756533147 * value)
	{
		___cube_18 = value;
		Il2CppCodeGenWriteBarrier(&___cube_18, value);
	}

	inline static int32_t get_offset_of__userId_19() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258, ____userId_19)); }
	inline String_t* get__userId_19() const { return ____userId_19; }
	inline String_t** get_address_of__userId_19() { return &____userId_19; }
	inline void set__userId_19(String_t* value)
	{
		____userId_19 = value;
		Il2CppCodeGenWriteBarrier(&____userId_19, value);
	}
};

struct FacebookComboUI_t2562467258_StaticFields
{
public:
	// System.String Prime31.FacebookComboUI::screenshotFilename
	String_t* ___screenshotFilename_20;
	// System.Action`1<System.Object> Prime31.FacebookComboUI::<>f__am$cache0
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache0_21;
	// System.Action`1<System.Boolean> Prime31.FacebookComboUI::<>f__am$cache1
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache1_22;
	// System.Action`1<System.Boolean> Prime31.FacebookComboUI::<>f__am$cache2
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache2_23;
	// System.Action`2<System.String,Prime31.FacebookFriendsResult> Prime31.FacebookComboUI::<>f__am$cache3
	Action_2_t2665012870 * ___U3CU3Ef__amU24cache3_24;

public:
	inline static int32_t get_offset_of_screenshotFilename_20() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258_StaticFields, ___screenshotFilename_20)); }
	inline String_t* get_screenshotFilename_20() const { return ___screenshotFilename_20; }
	inline String_t** get_address_of_screenshotFilename_20() { return &___screenshotFilename_20; }
	inline void set_screenshotFilename_20(String_t* value)
	{
		___screenshotFilename_20 = value;
		Il2CppCodeGenWriteBarrier(&___screenshotFilename_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_21() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258_StaticFields, ___U3CU3Ef__amU24cache0_21)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache0_21() const { return ___U3CU3Ef__amU24cache0_21; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache0_21() { return &___U3CU3Ef__amU24cache0_21; }
	inline void set_U3CU3Ef__amU24cache0_21(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_22() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258_StaticFields, ___U3CU3Ef__amU24cache1_22)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache1_22() const { return ___U3CU3Ef__amU24cache1_22; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache1_22() { return &___U3CU3Ef__amU24cache1_22; }
	inline void set_U3CU3Ef__amU24cache1_22(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache1_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_23() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258_StaticFields, ___U3CU3Ef__amU24cache2_23)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache2_23() const { return ___U3CU3Ef__amU24cache2_23; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache2_23() { return &___U3CU3Ef__amU24cache2_23; }
	inline void set_U3CU3Ef__amU24cache2_23(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache2_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_24() { return static_cast<int32_t>(offsetof(FacebookComboUI_t2562467258_StaticFields, ___U3CU3Ef__amU24cache3_24)); }
	inline Action_2_t2665012870 * get_U3CU3Ef__amU24cache3_24() const { return ___U3CU3Ef__amU24cache3_24; }
	inline Action_2_t2665012870 ** get_address_of_U3CU3Ef__amU24cache3_24() { return &___U3CU3Ef__amU24cache3_24; }
	inline void set_U3CU3Ef__amU24cache3_24(Action_2_t2665012870 * value)
	{
		___U3CU3Ef__amU24cache3_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
