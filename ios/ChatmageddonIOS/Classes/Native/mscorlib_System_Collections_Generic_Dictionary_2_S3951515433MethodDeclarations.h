﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>
struct ShimEnumerator_t3951515433;
// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4256289270_gshared (ShimEnumerator_t3951515433 * __this, Dictionary_2_t3846390612 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4256289270(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3951515433 *, Dictionary_2_t3846390612 *, const MethodInfo*))ShimEnumerator__ctor_m4256289270_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1005790025_gshared (ShimEnumerator_t3951515433 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1005790025(__this, method) ((  bool (*) (ShimEnumerator_t3951515433 *, const MethodInfo*))ShimEnumerator_MoveNext_m1005790025_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3675792501_gshared (ShimEnumerator_t3951515433 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3675792501(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3951515433 *, const MethodInfo*))ShimEnumerator_get_Entry_m3675792501_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1393291248_gshared (ShimEnumerator_t3951515433 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1393291248(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3951515433 *, const MethodInfo*))ShimEnumerator_get_Key_m1393291248_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m406822402_gshared (ShimEnumerator_t3951515433 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m406822402(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3951515433 *, const MethodInfo*))ShimEnumerator_get_Value_m406822402_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m341034818_gshared (ShimEnumerator_t3951515433 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m341034818(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3951515433 *, const MethodInfo*))ShimEnumerator_get_Current_m341034818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PanelType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m194187700_gshared (ShimEnumerator_t3951515433 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m194187700(__this, method) ((  void (*) (ShimEnumerator_t3951515433 *, const MethodInfo*))ShimEnumerator_Reset_m194187700_gshared)(__this, method)
