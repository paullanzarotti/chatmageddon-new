﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoCtrl/<DownloadFile>c__Iterator2
struct U3CDownloadFileU3Ec__Iterator2_t1146962742;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoCtrl/<DownloadFile>c__Iterator2::.ctor()
extern "C"  void U3CDownloadFileU3Ec__Iterator2__ctor_m1820047977 (U3CDownloadFileU3Ec__Iterator2_t1146962742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DemoCtrl/<DownloadFile>c__Iterator2::MoveNext()
extern "C"  bool U3CDownloadFileU3Ec__Iterator2_MoveNext_m3692667779 (U3CDownloadFileU3Ec__Iterator2_t1146962742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DemoCtrl/<DownloadFile>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadFileU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4099127687 (U3CDownloadFileU3Ec__Iterator2_t1146962742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DemoCtrl/<DownloadFile>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadFileU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3010335599 (U3CDownloadFileU3Ec__Iterator2_t1146962742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<DownloadFile>c__Iterator2::Dispose()
extern "C"  void U3CDownloadFileU3Ec__Iterator2_Dispose_m2143648296 (U3CDownloadFileU3Ec__Iterator2_t1146962742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<DownloadFile>c__Iterator2::Reset()
extern "C"  void U3CDownloadFileU3Ec__Iterator2_Reset_m1481944534 (U3CDownloadFileU3Ec__Iterator2_t1146962742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
