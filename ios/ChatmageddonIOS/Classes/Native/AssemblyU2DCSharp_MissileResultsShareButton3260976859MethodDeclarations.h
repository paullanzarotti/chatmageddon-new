﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileResultsShareButton
struct MissileResultsShareButton_t3260976859;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileResultsShareButton::.ctor()
extern "C"  void MissileResultsShareButton__ctor_m173228208 (MissileResultsShareButton_t3260976859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsShareButton::OnButtonClick()
extern "C"  void MissileResultsShareButton_OnButtonClick_m1729206447 (MissileResultsShareButton_t3260976859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
