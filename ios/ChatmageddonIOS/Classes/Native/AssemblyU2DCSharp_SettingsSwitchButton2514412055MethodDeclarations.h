﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsSwitchButton
struct SettingsSwitchButton_t2514412055;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsSwitchButton::.ctor()
extern "C"  void SettingsSwitchButton__ctor_m2758936092 (SettingsSwitchButton_t2514412055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsSwitchButton::SetSwitchStart(System.Boolean)
extern "C"  void SettingsSwitchButton_SetSwitchStart_m604992173 (SettingsSwitchButton_t2514412055 * __this, bool ___startState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsSwitchButton::OnSwitch(System.Boolean)
extern "C"  void SettingsSwitchButton_OnSwitch_m313422926 (SettingsSwitchButton_t2514412055 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsSwitchButton::PlaySwitchForward()
extern "C"  void SettingsSwitchButton_PlaySwitchForward_m1155241955 (SettingsSwitchButton_t2514412055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsSwitchButton::PlaySwitchReverse()
extern "C"  void SettingsSwitchButton_PlaySwitchReverse_m3575459652 (SettingsSwitchButton_t2514412055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
