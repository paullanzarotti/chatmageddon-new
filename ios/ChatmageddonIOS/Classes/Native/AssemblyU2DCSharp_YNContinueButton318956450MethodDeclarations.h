﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YNContinueButton
struct YNContinueButton_t318956450;

#include "codegen/il2cpp-codegen.h"

// System.Void YNContinueButton::.ctor()
extern "C"  void YNContinueButton__ctor_m2412689991 (YNContinueButton_t318956450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YNContinueButton::OnButtonClick()
extern "C"  void YNContinueButton_OnButtonClick_m1054378970 (YNContinueButton_t318956450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YNContinueButton::ValidateInput()
extern "C"  bool YNContinueButton_ValidateInput_m2344323863 (YNContinueButton_t318956450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YNContinueButton::<OnButtonClick>__BaseCallProxy0()
extern "C"  void YNContinueButton_U3COnButtonClickU3E__BaseCallProxy0_m3592997299 (YNContinueButton_t318956450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
