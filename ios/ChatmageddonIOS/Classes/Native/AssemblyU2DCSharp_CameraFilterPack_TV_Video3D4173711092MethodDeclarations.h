﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Video3D
struct CameraFilterPack_TV_Video3D_t4173711092;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Video3D::.ctor()
extern "C"  void CameraFilterPack_TV_Video3D__ctor_m2034406123 (CameraFilterPack_TV_Video3D_t4173711092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Video3D::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Video3D_get_material_m3134182020 (CameraFilterPack_TV_Video3D_t4173711092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::Start()
extern "C"  void CameraFilterPack_TV_Video3D_Start_m261338575 (CameraFilterPack_TV_Video3D_t4173711092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Video3D_OnRenderImage_m488290271 (CameraFilterPack_TV_Video3D_t4173711092 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::Update()
extern "C"  void CameraFilterPack_TV_Video3D_Update_m1597111638 (CameraFilterPack_TV_Video3D_t4173711092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::OnDisable()
extern "C"  void CameraFilterPack_TV_Video3D_OnDisable_m3553115536 (CameraFilterPack_TV_Video3D_t4173711092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
