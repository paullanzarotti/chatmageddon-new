﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsLoader
struct SettingsLoader_t2246286398;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsLoader::.ctor()
extern "C"  void SettingsLoader__ctor_m2160778979 (SettingsLoader_t2246286398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsLoader::LoadSceneUI()
extern "C"  void SettingsLoader_LoadSceneUI_m3818846359 (SettingsLoader_t2246286398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsLoader::UnloadSceneUI()
extern "C"  void SettingsLoader_UnloadSceneUI_m1512533806 (SettingsLoader_t2246286398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
