﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusNavigateForwardsButton
struct StatusNavigateForwardsButton_t4116613479;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusNavigateForwardsButton::.ctor()
extern "C"  void StatusNavigateForwardsButton__ctor_m294842594 (StatusNavigateForwardsButton_t4116613479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigateForwardsButton::OnButtonClick()
extern "C"  void StatusNavigateForwardsButton_OnButtonClick_m4177802319 (StatusNavigateForwardsButton_t4116613479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
