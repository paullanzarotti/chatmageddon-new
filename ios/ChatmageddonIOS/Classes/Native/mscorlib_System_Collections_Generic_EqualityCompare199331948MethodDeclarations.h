﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<StatusNavScreen>
struct DefaultComparer_t199331948;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<StatusNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m3899261825_gshared (DefaultComparer_t199331948 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3899261825(__this, method) ((  void (*) (DefaultComparer_t199331948 *, const MethodInfo*))DefaultComparer__ctor_m3899261825_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<StatusNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1441019726_gshared (DefaultComparer_t199331948 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1441019726(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t199331948 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1441019726_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<StatusNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1701726214_gshared (DefaultComparer_t199331948 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1701726214(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t199331948 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1701726214_gshared)(__this, ___x0, ___y1, method)
