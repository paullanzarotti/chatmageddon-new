﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingPopUpUIScaler
struct  LoadingPopUpUIScaler_t4027306684  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite LoadingPopUpUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider LoadingPopUpUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UILabel LoadingPopUpUIScaler::loadingLabel
	UILabel_t1795115428 * ___loadingLabel_16;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(LoadingPopUpUIScaler_t4027306684, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(LoadingPopUpUIScaler_t4027306684, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_loadingLabel_16() { return static_cast<int32_t>(offsetof(LoadingPopUpUIScaler_t4027306684, ___loadingLabel_16)); }
	inline UILabel_t1795115428 * get_loadingLabel_16() const { return ___loadingLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_loadingLabel_16() { return &___loadingLabel_16; }
	inline void set_loadingLabel_16(UILabel_t1795115428 * value)
	{
		___loadingLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___loadingLabel_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
