﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackReviewUIScaler
struct AttackReviewUIScaler_t2473008382;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackReviewUIScaler::.ctor()
extern "C"  void AttackReviewUIScaler__ctor_m3938329451 (AttackReviewUIScaler_t2473008382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackReviewUIScaler::GlobalUIScale()
extern "C"  void AttackReviewUIScaler_GlobalUIScale_m3090892480 (AttackReviewUIScaler_t2473008382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
