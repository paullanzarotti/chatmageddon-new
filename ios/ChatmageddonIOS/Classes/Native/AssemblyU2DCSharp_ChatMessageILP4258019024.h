﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen2984199389.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatMessageILP
struct  ChatMessageILP_t4258019024  : public BaseInfiniteListPopulator_1_t2984199389
{
public:
	// System.Boolean ChatMessageILP::listLoaded
	bool ___listLoaded_33;
	// System.Collections.ArrayList ChatMessageILP::chatMessageArray
	ArrayList_t4252133567 * ___chatMessageArray_34;
	// UnityEngine.Color ChatMessageILP::FriendColour
	Color_t2020392075  ___FriendColour_35;
	// UnityEngine.Color ChatMessageILP::playerColour
	Color_t2020392075  ___playerColour_36;
	// System.DateTime ChatMessageILP::latestDateTime
	DateTime_t693205669  ___latestDateTime_37;
	// System.Int32 ChatMessageILP::maxBackgroundWidth
	int32_t ___maxBackgroundWidth_38;

public:
	inline static int32_t get_offset_of_listLoaded_33() { return static_cast<int32_t>(offsetof(ChatMessageILP_t4258019024, ___listLoaded_33)); }
	inline bool get_listLoaded_33() const { return ___listLoaded_33; }
	inline bool* get_address_of_listLoaded_33() { return &___listLoaded_33; }
	inline void set_listLoaded_33(bool value)
	{
		___listLoaded_33 = value;
	}

	inline static int32_t get_offset_of_chatMessageArray_34() { return static_cast<int32_t>(offsetof(ChatMessageILP_t4258019024, ___chatMessageArray_34)); }
	inline ArrayList_t4252133567 * get_chatMessageArray_34() const { return ___chatMessageArray_34; }
	inline ArrayList_t4252133567 ** get_address_of_chatMessageArray_34() { return &___chatMessageArray_34; }
	inline void set_chatMessageArray_34(ArrayList_t4252133567 * value)
	{
		___chatMessageArray_34 = value;
		Il2CppCodeGenWriteBarrier(&___chatMessageArray_34, value);
	}

	inline static int32_t get_offset_of_FriendColour_35() { return static_cast<int32_t>(offsetof(ChatMessageILP_t4258019024, ___FriendColour_35)); }
	inline Color_t2020392075  get_FriendColour_35() const { return ___FriendColour_35; }
	inline Color_t2020392075 * get_address_of_FriendColour_35() { return &___FriendColour_35; }
	inline void set_FriendColour_35(Color_t2020392075  value)
	{
		___FriendColour_35 = value;
	}

	inline static int32_t get_offset_of_playerColour_36() { return static_cast<int32_t>(offsetof(ChatMessageILP_t4258019024, ___playerColour_36)); }
	inline Color_t2020392075  get_playerColour_36() const { return ___playerColour_36; }
	inline Color_t2020392075 * get_address_of_playerColour_36() { return &___playerColour_36; }
	inline void set_playerColour_36(Color_t2020392075  value)
	{
		___playerColour_36 = value;
	}

	inline static int32_t get_offset_of_latestDateTime_37() { return static_cast<int32_t>(offsetof(ChatMessageILP_t4258019024, ___latestDateTime_37)); }
	inline DateTime_t693205669  get_latestDateTime_37() const { return ___latestDateTime_37; }
	inline DateTime_t693205669 * get_address_of_latestDateTime_37() { return &___latestDateTime_37; }
	inline void set_latestDateTime_37(DateTime_t693205669  value)
	{
		___latestDateTime_37 = value;
	}

	inline static int32_t get_offset_of_maxBackgroundWidth_38() { return static_cast<int32_t>(offsetof(ChatMessageILP_t4258019024, ___maxBackgroundWidth_38)); }
	inline int32_t get_maxBackgroundWidth_38() const { return ___maxBackgroundWidth_38; }
	inline int32_t* get_address_of_maxBackgroundWidth_38() { return &___maxBackgroundWidth_38; }
	inline void set_maxBackgroundWidth_38(int32_t value)
	{
		___maxBackgroundWidth_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
