﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenSmallToastButton
struct OpenSmallToastButton_t3751439810;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenSmallToastButton::.ctor()
extern "C"  void OpenSmallToastButton__ctor_m3558291495 (OpenSmallToastButton_t3751439810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenSmallToastButton::OnButtonClick()
extern "C"  void OpenSmallToastButton_OnButtonClick_m483604858 (OpenSmallToastButton_t3751439810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
