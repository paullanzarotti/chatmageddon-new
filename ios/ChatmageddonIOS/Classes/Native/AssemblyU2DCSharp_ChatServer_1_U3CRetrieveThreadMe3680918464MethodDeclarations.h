﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<RetrieveThreadMessagesForUser>c__AnonStorey4<System.Object>
struct U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t3680918464;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<RetrieveThreadMessagesForUser>c__AnonStorey4<System.Object>::.ctor()
extern "C"  void U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4__ctor_m3337674005_gshared (U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t3680918464 * __this, const MethodInfo* method);
#define U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4__ctor_m3337674005(__this, method) ((  void (*) (U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t3680918464 *, const MethodInfo*))U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4__ctor_m3337674005_gshared)(__this, method)
// System.Void ChatServer`1/<RetrieveThreadMessagesForUser>c__AnonStorey4<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_U3CU3Em__0_m136265476_gshared (U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t3680918464 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_U3CU3Em__0_m136265476(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t3680918464 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_U3CU3Em__0_m136265476_gshared)(__this, ___request0, ___response1, method)
