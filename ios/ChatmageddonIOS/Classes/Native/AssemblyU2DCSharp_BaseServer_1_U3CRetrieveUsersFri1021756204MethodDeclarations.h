﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RetrieveUsersFriendsList>c__AnonStoreyD<System.Object>
struct U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t1021756204;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RetrieveUsersFriendsList>c__AnonStoreyD<System.Object>::.ctor()
extern "C"  void U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD__ctor_m1305142323_gshared (U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t1021756204 * __this, const MethodInfo* method);
#define U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD__ctor_m1305142323(__this, method) ((  void (*) (U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t1021756204 *, const MethodInfo*))U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD__ctor_m1305142323_gshared)(__this, method)
// System.Void BaseServer`1/<RetrieveUsersFriendsList>c__AnonStoreyD<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_U3CU3Em__0_m3591992720_gshared (U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t1021756204 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_U3CU3Em__0_m3591992720(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t1021756204 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_U3CU3Em__0_m3591992720_gshared)(__this, ___request0, ___response1, method)
