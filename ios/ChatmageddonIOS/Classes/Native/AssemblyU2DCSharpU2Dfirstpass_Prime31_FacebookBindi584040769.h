﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Prime31.FacebookBinding/<graphRequest>c__AnonStorey1
struct U3CgraphRequestU3Ec__AnonStorey1_t3312924124;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookBinding/<graphRequest>c__AnonStorey2
struct  U3CgraphRequestU3Ec__AnonStorey2_t584040769  : public Il2CppObject
{
public:
	// System.String Prime31.FacebookBinding/<graphRequest>c__AnonStorey2::jsonDict
	String_t* ___jsonDict_0;
	// Prime31.FacebookBinding/<graphRequest>c__AnonStorey1 Prime31.FacebookBinding/<graphRequest>c__AnonStorey2::<>f__ref$1
	U3CgraphRequestU3Ec__AnonStorey1_t3312924124 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_jsonDict_0() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey2_t584040769, ___jsonDict_0)); }
	inline String_t* get_jsonDict_0() const { return ___jsonDict_0; }
	inline String_t** get_address_of_jsonDict_0() { return &___jsonDict_0; }
	inline void set_jsonDict_0(String_t* value)
	{
		___jsonDict_0 = value;
		Il2CppCodeGenWriteBarrier(&___jsonDict_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey2_t584040769, ___U3CU3Ef__refU241_1)); }
	inline U3CgraphRequestU3Ec__AnonStorey1_t3312924124 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CgraphRequestU3Ec__AnonStorey1_t3312924124 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CgraphRequestU3Ec__AnonStorey1_t3312924124 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
