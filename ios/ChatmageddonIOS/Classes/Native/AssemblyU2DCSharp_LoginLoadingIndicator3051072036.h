﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenColor
struct TweenColor_t3390486518;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2801737756.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginLoadingIndicator
struct  LoginLoadingIndicator_t3051072036  : public MonoSingleton_1_t2801737756
{
public:
	// TweenColor LoginLoadingIndicator::rocketTween1
	TweenColor_t3390486518 * ___rocketTween1_3;
	// TweenColor LoginLoadingIndicator::rocketTween2
	TweenColor_t3390486518 * ___rocketTween2_4;
	// TweenColor LoginLoadingIndicator::rocketTween3
	TweenColor_t3390486518 * ___rocketTween3_5;
	// TweenColor LoginLoadingIndicator::rocketTween4
	TweenColor_t3390486518 * ___rocketTween4_6;
	// TweenColor LoginLoadingIndicator::rocketTween5
	TweenColor_t3390486518 * ___rocketTween5_7;
	// UILabel LoginLoadingIndicator::messageLabel
	UILabel_t1795115428 * ___messageLabel_8;

public:
	inline static int32_t get_offset_of_rocketTween1_3() { return static_cast<int32_t>(offsetof(LoginLoadingIndicator_t3051072036, ___rocketTween1_3)); }
	inline TweenColor_t3390486518 * get_rocketTween1_3() const { return ___rocketTween1_3; }
	inline TweenColor_t3390486518 ** get_address_of_rocketTween1_3() { return &___rocketTween1_3; }
	inline void set_rocketTween1_3(TweenColor_t3390486518 * value)
	{
		___rocketTween1_3 = value;
		Il2CppCodeGenWriteBarrier(&___rocketTween1_3, value);
	}

	inline static int32_t get_offset_of_rocketTween2_4() { return static_cast<int32_t>(offsetof(LoginLoadingIndicator_t3051072036, ___rocketTween2_4)); }
	inline TweenColor_t3390486518 * get_rocketTween2_4() const { return ___rocketTween2_4; }
	inline TweenColor_t3390486518 ** get_address_of_rocketTween2_4() { return &___rocketTween2_4; }
	inline void set_rocketTween2_4(TweenColor_t3390486518 * value)
	{
		___rocketTween2_4 = value;
		Il2CppCodeGenWriteBarrier(&___rocketTween2_4, value);
	}

	inline static int32_t get_offset_of_rocketTween3_5() { return static_cast<int32_t>(offsetof(LoginLoadingIndicator_t3051072036, ___rocketTween3_5)); }
	inline TweenColor_t3390486518 * get_rocketTween3_5() const { return ___rocketTween3_5; }
	inline TweenColor_t3390486518 ** get_address_of_rocketTween3_5() { return &___rocketTween3_5; }
	inline void set_rocketTween3_5(TweenColor_t3390486518 * value)
	{
		___rocketTween3_5 = value;
		Il2CppCodeGenWriteBarrier(&___rocketTween3_5, value);
	}

	inline static int32_t get_offset_of_rocketTween4_6() { return static_cast<int32_t>(offsetof(LoginLoadingIndicator_t3051072036, ___rocketTween4_6)); }
	inline TweenColor_t3390486518 * get_rocketTween4_6() const { return ___rocketTween4_6; }
	inline TweenColor_t3390486518 ** get_address_of_rocketTween4_6() { return &___rocketTween4_6; }
	inline void set_rocketTween4_6(TweenColor_t3390486518 * value)
	{
		___rocketTween4_6 = value;
		Il2CppCodeGenWriteBarrier(&___rocketTween4_6, value);
	}

	inline static int32_t get_offset_of_rocketTween5_7() { return static_cast<int32_t>(offsetof(LoginLoadingIndicator_t3051072036, ___rocketTween5_7)); }
	inline TweenColor_t3390486518 * get_rocketTween5_7() const { return ___rocketTween5_7; }
	inline TweenColor_t3390486518 ** get_address_of_rocketTween5_7() { return &___rocketTween5_7; }
	inline void set_rocketTween5_7(TweenColor_t3390486518 * value)
	{
		___rocketTween5_7 = value;
		Il2CppCodeGenWriteBarrier(&___rocketTween5_7, value);
	}

	inline static int32_t get_offset_of_messageLabel_8() { return static_cast<int32_t>(offsetof(LoginLoadingIndicator_t3051072036, ___messageLabel_8)); }
	inline UILabel_t1795115428 * get_messageLabel_8() const { return ___messageLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_messageLabel_8() { return &___messageLabel_8; }
	inline void set_messageLabel_8(UILabel_t1795115428 * value)
	{
		___messageLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___messageLabel_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
