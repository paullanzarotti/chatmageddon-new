﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUITextureSize
struct GUITextureSize_t1753924715;

#include "codegen/il2cpp-codegen.h"

// System.Void GUITextureSize::.ctor()
extern "C"  void GUITextureSize__ctor_m4257347382 (GUITextureSize_t1753924715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUITextureSize::Start()
extern "C"  void GUITextureSize_Start_m3972331882 (GUITextureSize_t1753924715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
