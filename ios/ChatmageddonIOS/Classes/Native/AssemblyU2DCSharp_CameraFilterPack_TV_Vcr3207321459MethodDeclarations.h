﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Vcr
struct CameraFilterPack_TV_Vcr_t3207321459;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Vcr::.ctor()
extern "C"  void CameraFilterPack_TV_Vcr__ctor_m3830373474 (CameraFilterPack_TV_Vcr_t3207321459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Vcr::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Vcr_get_material_m1569143279 (CameraFilterPack_TV_Vcr_t3207321459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::Start()
extern "C"  void CameraFilterPack_TV_Vcr_Start_m4226746830 (CameraFilterPack_TV_Vcr_t3207321459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Vcr_OnRenderImage_m3353130622 (CameraFilterPack_TV_Vcr_t3207321459 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::OnValidate()
extern "C"  void CameraFilterPack_TV_Vcr_OnValidate_m3839790601 (CameraFilterPack_TV_Vcr_t3207321459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::Update()
extern "C"  void CameraFilterPack_TV_Vcr_Update_m3407063639 (CameraFilterPack_TV_Vcr_t3207321459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::OnDisable()
extern "C"  void CameraFilterPack_TV_Vcr_OnDisable_m2867038039 (CameraFilterPack_TV_Vcr_t3207321459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
