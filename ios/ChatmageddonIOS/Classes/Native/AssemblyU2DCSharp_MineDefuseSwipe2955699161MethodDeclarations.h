﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineDefuseSwipe
struct MineDefuseSwipe_t2955699161;

#include "codegen/il2cpp-codegen.h"

// System.Void MineDefuseSwipe::.ctor()
extern "C"  void MineDefuseSwipe__ctor_m3042616944 (MineDefuseSwipe_t2955699161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineDefuseSwipe::OnSwipeUp()
extern "C"  void MineDefuseSwipe_OnSwipeUp_m2467502696 (MineDefuseSwipe_t2955699161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineDefuseSwipe::OnSwipeDown()
extern "C"  void MineDefuseSwipe_OnSwipeDown_m1476666303 (MineDefuseSwipe_t2955699161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineDefuseSwipe::SetCollisionActive(System.Boolean)
extern "C"  void MineDefuseSwipe_SetCollisionActive_m2009587575 (MineDefuseSwipe_t2955699161 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
