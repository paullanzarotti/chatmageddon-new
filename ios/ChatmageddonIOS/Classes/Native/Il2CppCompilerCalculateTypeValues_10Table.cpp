﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Threading_Monitor3228523394.h"
#include "mscorlib_System_Threading_Mutex297030111.h"
#include "mscorlib_System_Threading_NativeEventCalls1850675218.h"
#include "mscorlib_System_Threading_RegisteredWaitHandle2863394393.h"
#include "mscorlib_System_Threading_SynchronizationContext3857790437.h"
#include "mscorlib_System_Threading_SynchronizationLockExcept117698316.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "mscorlib_System_Threading_ThreadAbortException1150575753.h"
#include "mscorlib_System_Threading_ThreadInterruptedException63303933.h"
#include "mscorlib_System_Threading_ThreadPool3989917080.h"
#include "mscorlib_System_Threading_ThreadState1158972609.h"
#include "mscorlib_System_Threading_ThreadStateException1404755912.h"
#include "mscorlib_System_Threading_Timer791717973.h"
#include "mscorlib_System_Threading_Timer_TimerComparer876299723.h"
#include "mscorlib_System_Threading_Timer_Scheduler697594.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExc1722175009.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTest572679901.h"
#include "mscorlib_System_AppDomainInitializer3898244613.h"
#include "mscorlib_System_AssemblyLoadEventHandler2169307382.h"
#include "mscorlib_System_ConsoleCancelEventHandler1138054497.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_ResolveEventHandler3842432458.h"
#include "mscorlib_System_UnhandledExceptionEventHandler1916531888.h"
#include "mscorlib_System_Reflection_MemberFilter3405857066.h"
#include "mscorlib_System_Reflection_ModuleResolveEventHandle403447254.h"
#include "mscorlib_System_Reflection_TypeFilter2905709404.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCont754146990.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHa324204131.h"
#include "mscorlib_System_Threading_ParameterizedThreadStart2412552885.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742.h"
#include "mscorlib_System_Threading_ThreadStart3437517264.h"
#include "mscorlib_System_Threading_TimerCallback1684927372.h"
#include "mscorlib_System_Threading_WaitCallback2798937288.h"
#include "mscorlib_System_Threading_WaitOrTimerCallback2724438238.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778798.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2866209749.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1703410334.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra116038562.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1892466092.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra540610921.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778804.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra896841275.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2866209745.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778802.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra420490420.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3055265540.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2914102937.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2844921915.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1957337327.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2866209739.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778800.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra116038558.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1703410326.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra702815517.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1703410330.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2038352954.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2672183894.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3604436769.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2441637390.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra116038554.h"
#include "mscorlib_System___Il2CppComObject4064417062.h"
#include "mscorlib_System___Il2CppComDelegate3311706788.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser1120482700.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser_YYRules4159463463.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyException2814964437.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (ManualResetEvent_t926074657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (Monitor_t3228523394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (Mutex_t297030111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (NativeEventCalls_t1850675218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (RegisteredWaitHandle_t2863394393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1004[9] = 
{
	RegisteredWaitHandle_t2863394393::get_offset_of__waitObject_1(),
	RegisteredWaitHandle_t2863394393::get_offset_of__callback_2(),
	RegisteredWaitHandle_t2863394393::get_offset_of__timeout_3(),
	RegisteredWaitHandle_t2863394393::get_offset_of__state_4(),
	RegisteredWaitHandle_t2863394393::get_offset_of__executeOnlyOnce_5(),
	RegisteredWaitHandle_t2863394393::get_offset_of__finalEvent_6(),
	RegisteredWaitHandle_t2863394393::get_offset_of__cancelEvent_7(),
	RegisteredWaitHandle_t2863394393::get_offset_of__callsInProcess_8(),
	RegisteredWaitHandle_t2863394393::get_offset_of__unregistered_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (SynchronizationContext_t3857790437), -1, 0, sizeof(SynchronizationContext_t3857790437_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1005[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (SynchronizationLockException_t117698316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (Thread_t241561612), -1, sizeof(Thread_t241561612_StaticFields), sizeof(Thread_t241561612_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1007[52] = 
{
	Thread_t241561612::get_offset_of_lock_thread_id_0(),
	Thread_t241561612::get_offset_of_system_thread_handle_1(),
	Thread_t241561612::get_offset_of_cached_culture_info_2(),
	Thread_t241561612::get_offset_of_unused0_3(),
	Thread_t241561612::get_offset_of_threadpool_thread_4(),
	Thread_t241561612::get_offset_of_name_5(),
	Thread_t241561612::get_offset_of_name_len_6(),
	Thread_t241561612::get_offset_of_state_7(),
	Thread_t241561612::get_offset_of_abort_exc_8(),
	Thread_t241561612::get_offset_of_abort_state_handle_9(),
	Thread_t241561612::get_offset_of_thread_id_10(),
	Thread_t241561612::get_offset_of_start_notify_11(),
	Thread_t241561612::get_offset_of_stack_ptr_12(),
	Thread_t241561612::get_offset_of_static_data_13(),
	Thread_t241561612::get_offset_of_jit_data_14(),
	Thread_t241561612::get_offset_of_lock_data_15(),
	Thread_t241561612::get_offset_of_current_appcontext_16(),
	Thread_t241561612::get_offset_of_stack_size_17(),
	Thread_t241561612::get_offset_of_start_obj_18(),
	Thread_t241561612::get_offset_of_appdomain_refs_19(),
	Thread_t241561612::get_offset_of_interruption_requested_20(),
	Thread_t241561612::get_offset_of_suspend_event_21(),
	Thread_t241561612::get_offset_of_suspended_event_22(),
	Thread_t241561612::get_offset_of_resume_event_23(),
	Thread_t241561612::get_offset_of_synch_cs_24(),
	Thread_t241561612::get_offset_of_serialized_culture_info_25(),
	Thread_t241561612::get_offset_of_serialized_culture_info_len_26(),
	Thread_t241561612::get_offset_of_serialized_ui_culture_info_27(),
	Thread_t241561612::get_offset_of_serialized_ui_culture_info_len_28(),
	Thread_t241561612::get_offset_of_thread_dump_requested_29(),
	Thread_t241561612::get_offset_of_end_stack_30(),
	Thread_t241561612::get_offset_of_thread_interrupt_requested_31(),
	Thread_t241561612::get_offset_of_apartment_state_32(),
	Thread_t241561612::get_offset_of_critical_region_level_33(),
	Thread_t241561612::get_offset_of_small_id_34(),
	Thread_t241561612::get_offset_of_manage_callback_35(),
	Thread_t241561612::get_offset_of_pending_exception_36(),
	Thread_t241561612::get_offset_of_ec_to_set_37(),
	Thread_t241561612::get_offset_of_interrupt_on_stop_38(),
	Thread_t241561612::get_offset_of_unused3_39(),
	Thread_t241561612::get_offset_of_unused4_40(),
	Thread_t241561612::get_offset_of_unused5_41(),
	Thread_t241561612::get_offset_of_unused6_42(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	Thread_t241561612::get_offset_of_threadstart_45(),
	Thread_t241561612::get_offset_of_managed_id_46(),
	Thread_t241561612::get_offset_of__principal_47(),
	Thread_t241561612_StaticFields::get_offset_of_datastorehash_48(),
	Thread_t241561612_StaticFields::get_offset_of_datastore_lock_49(),
	Thread_t241561612::get_offset_of_in_currentculture_50(),
	Thread_t241561612_StaticFields::get_offset_of_culture_lock_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (ThreadAbortException_t1150575753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (ThreadInterruptedException_t63303933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (ThreadPool_t3989917080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (ThreadState_t1158972609)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1011[11] = 
{
	ThreadState_t1158972609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { sizeof (ThreadStateException_t1404755912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (Timer_t791717973), -1, sizeof(Timer_t791717973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1013[7] = 
{
	Timer_t791717973_StaticFields::get_offset_of_scheduler_1(),
	Timer_t791717973::get_offset_of_callback_2(),
	Timer_t791717973::get_offset_of_state_3(),
	Timer_t791717973::get_offset_of_due_time_ms_4(),
	Timer_t791717973::get_offset_of_period_ms_5(),
	Timer_t791717973::get_offset_of_next_run_6(),
	Timer_t791717973::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { sizeof (TimerComparer_t876299723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (Scheduler_t697594), -1, sizeof(Scheduler_t697594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1015[2] = 
{
	Scheduler_t697594_StaticFields::get_offset_of_instance_0(),
	Scheduler_t697594::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (WaitHandle_t677569169), -1, sizeof(WaitHandle_t677569169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1016[4] = 
{
	0,
	WaitHandle_t677569169::get_offset_of_safe_wait_handle_2(),
	WaitHandle_t677569169_StaticFields::get_offset_of_InvalidHandle_3(),
	WaitHandle_t677569169::get_offset_of_disposed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (Link_t2723257478)+ sizeof (Il2CppObject), sizeof(Link_t2723257478 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1017[2] = 
{
	Link_t2723257478::get_offset_of_HashCode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t2723257478::get_offset_of_Next_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1018[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1019[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1020[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1021[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1022[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1023[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1024[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1029[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1030[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { sizeof (KeyNotFoundException_t1722175009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1034[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1035[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1038[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1039[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1040[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1041[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (PrimalityTest_t572679901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (AppDomainInitializer_t3898244613), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (AssemblyLoadEventHandler_t2169307382), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (ConsoleCancelEventHandler_t1138054497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (EventHandler_t277755526), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (ResolveEventHandler_t3842432458), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (UnhandledExceptionEventHandler_t1916531888), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (MemberFilter_t3405857066), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { sizeof (ModuleResolveEventHandler_t403447254), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (TypeFilter_t2905709404), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (CrossContextDelegate_t754146990), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (HeaderHandler_t324204131), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (ParameterizedThreadStart_t2412552885), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (SendOrPostCallback_t296893742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (ThreadStart_t3437517264), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (TimerCallback_t1684927372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (WaitCallback_t2798937288), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (WaitOrTimerCallback_t2724438238), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305137), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1066[59] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D15_7(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D16_8(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D17_9(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D18_10(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D19_11(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D20_12(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D24_16(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D25_17(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D26_18(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D27_19(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D28_20(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D29_21(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D30_22(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D31_23(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D32_24(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D34_25(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D35_26(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D36_27(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D37_28(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D38_29(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D39_30(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D40_31(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D41_32(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D42_33(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D43_34(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D44_35(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D45_36(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D46_37(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D47_38(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D48_39(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D49_40(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D50_41(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D51_42(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D52_43(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D53_44(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D54_45(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D55_46(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D56_47(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D57_48(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D58_49(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D59_50(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D60_51(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D61_52(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D62_53(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D63_54(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D64_55(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D65_56(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D67_57(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D68_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (U24ArrayTypeU2452_t3672778798)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t3672778798 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (U24ArrayTypeU2424_t2866209749)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2424_t2866209749 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (U24ArrayTypeU2416_t1703410334)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410334 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (U24ArrayTypeU24120_t116038562)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t116038562 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (U24ArrayTypeU243132_t1892466092)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t1892466092 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (U24ArrayTypeU2420_t540610921)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t540610921 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (U24ArrayTypeU2432_t3672778804)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3672778804 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (U24ArrayTypeU2448_t896841275)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t896841275 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (U24ArrayTypeU2464_t2866209745)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209745 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (U24ArrayTypeU2412_t3672778802)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778802 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (U24ArrayTypeU241668_t420490420)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241668_t420490420 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (U24ArrayTypeU242100_t3055265540)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242100_t3055265540 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (U24ArrayTypeU241452_t2914102937)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241452_t2914102937 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (U24ArrayTypeU24136_t2844921915)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t2844921915 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (U24ArrayTypeU248_t1957337327)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337327 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (U24ArrayTypeU2484_t2866209739)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2484_t2866209739 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (U24ArrayTypeU2472_t3672778800)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t3672778800 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (U24ArrayTypeU24124_t116038558)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24124_t116038558 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (U24ArrayTypeU2496_t1703410326)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2496_t1703410326 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (U24ArrayTypeU242048_t702815517)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242048_t702815517 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (U24ArrayTypeU2456_t1703410330)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2456_t1703410330 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (U24ArrayTypeU24256_t2038352954)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352954 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (U24ArrayTypeU241024_t2672183894)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t2672183894 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (U24ArrayTypeU24640_t3604436769)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24640_t3604436769 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { sizeof (U24ArrayTypeU24160_t2441637390)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24160_t2441637390 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (U24ArrayTypeU24128_t116038554)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038554 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (__Il2CppComDelegate_t3311706788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (U3CModuleU3E_t3783534215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (XPathParser_t1120482700), -1, sizeof(XPathParser_t1120482700_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1096[17] = 
{
	XPathParser_t1120482700::get_offset_of_Context_0(),
	XPathParser_t1120482700::get_offset_of_ErrorOutput_1(),
	XPathParser_t1120482700::get_offset_of_eof_token_2(),
	XPathParser_t1120482700::get_offset_of_debug_3(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyFinal_4(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyNames_5(),
	XPathParser_t1120482700::get_offset_of_yyExpectingState_6(),
	XPathParser_t1120482700::get_offset_of_yyMax_7(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyLhs_8(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyLen_9(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyDefRed_10(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyDgoto_11(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yySindex_12(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyRindex_13(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyGindex_14(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyTable_15(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyCheck_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (YYRules_t4159463463), -1, sizeof(YYRules_t4159463463_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1097[1] = 
{
	YYRules_t4159463463_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (yyException_t2814964437), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
