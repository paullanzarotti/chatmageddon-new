﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReviewItemLoader
struct ReviewItemLoader_t43765888;

#include "codegen/il2cpp-codegen.h"

// System.Void ReviewItemLoader::.ctor()
extern "C"  void ReviewItemLoader__ctor_m147645853 (ReviewItemLoader_t43765888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewItemLoader::PopulateItem()
extern "C"  void ReviewItemLoader_PopulateItem_m1762272894 (ReviewItemLoader_t43765888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewItemLoader::PopulateItemName()
extern "C"  void ReviewItemLoader_PopulateItemName_m2736976649 (ReviewItemLoader_t43765888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewItemLoader::DisplayItemModel()
extern "C"  void ReviewItemLoader_DisplayItemModel_m1726224713 (ReviewItemLoader_t43765888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
