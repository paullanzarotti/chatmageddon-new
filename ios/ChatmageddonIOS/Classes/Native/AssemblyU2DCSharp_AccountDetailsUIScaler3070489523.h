﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountDetailsUIScaler
struct  AccountDetailsUIScaler_t3070489523  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.Transform AccountDetailsUIScaler::profilePhoto
	Transform_t3275118058 * ___profilePhoto_14;
	// UnityEngine.Transform AccountDetailsUIScaler::names
	Transform_t3275118058 * ___names_15;
	// UISprite AccountDetailsUIScaler::setPhotoSeperator
	UISprite_t603616735 * ___setPhotoSeperator_16;
	// UILabel AccountDetailsUIScaler::setPhotoLabel
	UILabel_t1795115428 * ___setPhotoLabel_17;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::setPhotoCollider
	BoxCollider_t22920061 * ___setPhotoCollider_18;
	// UISprite AccountDetailsUIScaler::fbSeperator
	UISprite_t603616735 * ___fbSeperator_19;
	// UISprite AccountDetailsUIScaler::fbIcon
	UISprite_t603616735 * ___fbIcon_20;
	// UILabel AccountDetailsUIScaler::fbLabel
	UILabel_t1795115428 * ___fbLabel_21;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::fbCollider
	BoxCollider_t22920061 * ___fbCollider_22;
	// UISprite AccountDetailsUIScaler::emailSeperator
	UISprite_t603616735 * ___emailSeperator_23;
	// UILabel AccountDetailsUIScaler::emailLabel
	UILabel_t1795115428 * ___emailLabel_24;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::emailCollider
	BoxCollider_t22920061 * ___emailCollider_25;
	// UISprite AccountDetailsUIScaler::usernameSeperator
	UISprite_t603616735 * ___usernameSeperator_26;
	// UILabel AccountDetailsUIScaler::usernameLabel
	UILabel_t1795115428 * ___usernameLabel_27;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::usernameCollider
	BoxCollider_t22920061 * ___usernameCollider_28;
	// UISprite AccountDetailsUIScaler::passwordSeperator
	UISprite_t603616735 * ___passwordSeperator_29;
	// UILabel AccountDetailsUIScaler::passwordLabel
	UILabel_t1795115428 * ___passwordLabel_30;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::passwordCollider
	BoxCollider_t22920061 * ___passwordCollider_31;
	// UISprite AccountDetailsUIScaler::confirmPasswordSeperator
	UISprite_t603616735 * ___confirmPasswordSeperator_32;
	// UILabel AccountDetailsUIScaler::confirmPasswordLabel
	UILabel_t1795115428 * ___confirmPasswordLabel_33;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::confirmCollider
	BoxCollider_t22920061 * ___confirmCollider_34;
	// UISprite AccountDetailsUIScaler::phoneSeperator
	UISprite_t603616735 * ___phoneSeperator_35;
	// UILabel AccountDetailsUIScaler::phoneLabel
	UILabel_t1795115428 * ___phoneLabel_36;
	// UnityEngine.BoxCollider AccountDetailsUIScaler::phoneCollider
	BoxCollider_t22920061 * ___phoneCollider_37;
	// UISprite AccountDetailsUIScaler::phoneBottomSeperator
	UISprite_t603616735 * ___phoneBottomSeperator_38;

public:
	inline static int32_t get_offset_of_profilePhoto_14() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___profilePhoto_14)); }
	inline Transform_t3275118058 * get_profilePhoto_14() const { return ___profilePhoto_14; }
	inline Transform_t3275118058 ** get_address_of_profilePhoto_14() { return &___profilePhoto_14; }
	inline void set_profilePhoto_14(Transform_t3275118058 * value)
	{
		___profilePhoto_14 = value;
		Il2CppCodeGenWriteBarrier(&___profilePhoto_14, value);
	}

	inline static int32_t get_offset_of_names_15() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___names_15)); }
	inline Transform_t3275118058 * get_names_15() const { return ___names_15; }
	inline Transform_t3275118058 ** get_address_of_names_15() { return &___names_15; }
	inline void set_names_15(Transform_t3275118058 * value)
	{
		___names_15 = value;
		Il2CppCodeGenWriteBarrier(&___names_15, value);
	}

	inline static int32_t get_offset_of_setPhotoSeperator_16() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___setPhotoSeperator_16)); }
	inline UISprite_t603616735 * get_setPhotoSeperator_16() const { return ___setPhotoSeperator_16; }
	inline UISprite_t603616735 ** get_address_of_setPhotoSeperator_16() { return &___setPhotoSeperator_16; }
	inline void set_setPhotoSeperator_16(UISprite_t603616735 * value)
	{
		___setPhotoSeperator_16 = value;
		Il2CppCodeGenWriteBarrier(&___setPhotoSeperator_16, value);
	}

	inline static int32_t get_offset_of_setPhotoLabel_17() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___setPhotoLabel_17)); }
	inline UILabel_t1795115428 * get_setPhotoLabel_17() const { return ___setPhotoLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_setPhotoLabel_17() { return &___setPhotoLabel_17; }
	inline void set_setPhotoLabel_17(UILabel_t1795115428 * value)
	{
		___setPhotoLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___setPhotoLabel_17, value);
	}

	inline static int32_t get_offset_of_setPhotoCollider_18() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___setPhotoCollider_18)); }
	inline BoxCollider_t22920061 * get_setPhotoCollider_18() const { return ___setPhotoCollider_18; }
	inline BoxCollider_t22920061 ** get_address_of_setPhotoCollider_18() { return &___setPhotoCollider_18; }
	inline void set_setPhotoCollider_18(BoxCollider_t22920061 * value)
	{
		___setPhotoCollider_18 = value;
		Il2CppCodeGenWriteBarrier(&___setPhotoCollider_18, value);
	}

	inline static int32_t get_offset_of_fbSeperator_19() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___fbSeperator_19)); }
	inline UISprite_t603616735 * get_fbSeperator_19() const { return ___fbSeperator_19; }
	inline UISprite_t603616735 ** get_address_of_fbSeperator_19() { return &___fbSeperator_19; }
	inline void set_fbSeperator_19(UISprite_t603616735 * value)
	{
		___fbSeperator_19 = value;
		Il2CppCodeGenWriteBarrier(&___fbSeperator_19, value);
	}

	inline static int32_t get_offset_of_fbIcon_20() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___fbIcon_20)); }
	inline UISprite_t603616735 * get_fbIcon_20() const { return ___fbIcon_20; }
	inline UISprite_t603616735 ** get_address_of_fbIcon_20() { return &___fbIcon_20; }
	inline void set_fbIcon_20(UISprite_t603616735 * value)
	{
		___fbIcon_20 = value;
		Il2CppCodeGenWriteBarrier(&___fbIcon_20, value);
	}

	inline static int32_t get_offset_of_fbLabel_21() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___fbLabel_21)); }
	inline UILabel_t1795115428 * get_fbLabel_21() const { return ___fbLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_fbLabel_21() { return &___fbLabel_21; }
	inline void set_fbLabel_21(UILabel_t1795115428 * value)
	{
		___fbLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___fbLabel_21, value);
	}

	inline static int32_t get_offset_of_fbCollider_22() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___fbCollider_22)); }
	inline BoxCollider_t22920061 * get_fbCollider_22() const { return ___fbCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_fbCollider_22() { return &___fbCollider_22; }
	inline void set_fbCollider_22(BoxCollider_t22920061 * value)
	{
		___fbCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___fbCollider_22, value);
	}

	inline static int32_t get_offset_of_emailSeperator_23() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___emailSeperator_23)); }
	inline UISprite_t603616735 * get_emailSeperator_23() const { return ___emailSeperator_23; }
	inline UISprite_t603616735 ** get_address_of_emailSeperator_23() { return &___emailSeperator_23; }
	inline void set_emailSeperator_23(UISprite_t603616735 * value)
	{
		___emailSeperator_23 = value;
		Il2CppCodeGenWriteBarrier(&___emailSeperator_23, value);
	}

	inline static int32_t get_offset_of_emailLabel_24() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___emailLabel_24)); }
	inline UILabel_t1795115428 * get_emailLabel_24() const { return ___emailLabel_24; }
	inline UILabel_t1795115428 ** get_address_of_emailLabel_24() { return &___emailLabel_24; }
	inline void set_emailLabel_24(UILabel_t1795115428 * value)
	{
		___emailLabel_24 = value;
		Il2CppCodeGenWriteBarrier(&___emailLabel_24, value);
	}

	inline static int32_t get_offset_of_emailCollider_25() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___emailCollider_25)); }
	inline BoxCollider_t22920061 * get_emailCollider_25() const { return ___emailCollider_25; }
	inline BoxCollider_t22920061 ** get_address_of_emailCollider_25() { return &___emailCollider_25; }
	inline void set_emailCollider_25(BoxCollider_t22920061 * value)
	{
		___emailCollider_25 = value;
		Il2CppCodeGenWriteBarrier(&___emailCollider_25, value);
	}

	inline static int32_t get_offset_of_usernameSeperator_26() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___usernameSeperator_26)); }
	inline UISprite_t603616735 * get_usernameSeperator_26() const { return ___usernameSeperator_26; }
	inline UISprite_t603616735 ** get_address_of_usernameSeperator_26() { return &___usernameSeperator_26; }
	inline void set_usernameSeperator_26(UISprite_t603616735 * value)
	{
		___usernameSeperator_26 = value;
		Il2CppCodeGenWriteBarrier(&___usernameSeperator_26, value);
	}

	inline static int32_t get_offset_of_usernameLabel_27() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___usernameLabel_27)); }
	inline UILabel_t1795115428 * get_usernameLabel_27() const { return ___usernameLabel_27; }
	inline UILabel_t1795115428 ** get_address_of_usernameLabel_27() { return &___usernameLabel_27; }
	inline void set_usernameLabel_27(UILabel_t1795115428 * value)
	{
		___usernameLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___usernameLabel_27, value);
	}

	inline static int32_t get_offset_of_usernameCollider_28() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___usernameCollider_28)); }
	inline BoxCollider_t22920061 * get_usernameCollider_28() const { return ___usernameCollider_28; }
	inline BoxCollider_t22920061 ** get_address_of_usernameCollider_28() { return &___usernameCollider_28; }
	inline void set_usernameCollider_28(BoxCollider_t22920061 * value)
	{
		___usernameCollider_28 = value;
		Il2CppCodeGenWriteBarrier(&___usernameCollider_28, value);
	}

	inline static int32_t get_offset_of_passwordSeperator_29() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___passwordSeperator_29)); }
	inline UISprite_t603616735 * get_passwordSeperator_29() const { return ___passwordSeperator_29; }
	inline UISprite_t603616735 ** get_address_of_passwordSeperator_29() { return &___passwordSeperator_29; }
	inline void set_passwordSeperator_29(UISprite_t603616735 * value)
	{
		___passwordSeperator_29 = value;
		Il2CppCodeGenWriteBarrier(&___passwordSeperator_29, value);
	}

	inline static int32_t get_offset_of_passwordLabel_30() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___passwordLabel_30)); }
	inline UILabel_t1795115428 * get_passwordLabel_30() const { return ___passwordLabel_30; }
	inline UILabel_t1795115428 ** get_address_of_passwordLabel_30() { return &___passwordLabel_30; }
	inline void set_passwordLabel_30(UILabel_t1795115428 * value)
	{
		___passwordLabel_30 = value;
		Il2CppCodeGenWriteBarrier(&___passwordLabel_30, value);
	}

	inline static int32_t get_offset_of_passwordCollider_31() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___passwordCollider_31)); }
	inline BoxCollider_t22920061 * get_passwordCollider_31() const { return ___passwordCollider_31; }
	inline BoxCollider_t22920061 ** get_address_of_passwordCollider_31() { return &___passwordCollider_31; }
	inline void set_passwordCollider_31(BoxCollider_t22920061 * value)
	{
		___passwordCollider_31 = value;
		Il2CppCodeGenWriteBarrier(&___passwordCollider_31, value);
	}

	inline static int32_t get_offset_of_confirmPasswordSeperator_32() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___confirmPasswordSeperator_32)); }
	inline UISprite_t603616735 * get_confirmPasswordSeperator_32() const { return ___confirmPasswordSeperator_32; }
	inline UISprite_t603616735 ** get_address_of_confirmPasswordSeperator_32() { return &___confirmPasswordSeperator_32; }
	inline void set_confirmPasswordSeperator_32(UISprite_t603616735 * value)
	{
		___confirmPasswordSeperator_32 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPasswordSeperator_32, value);
	}

	inline static int32_t get_offset_of_confirmPasswordLabel_33() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___confirmPasswordLabel_33)); }
	inline UILabel_t1795115428 * get_confirmPasswordLabel_33() const { return ___confirmPasswordLabel_33; }
	inline UILabel_t1795115428 ** get_address_of_confirmPasswordLabel_33() { return &___confirmPasswordLabel_33; }
	inline void set_confirmPasswordLabel_33(UILabel_t1795115428 * value)
	{
		___confirmPasswordLabel_33 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPasswordLabel_33, value);
	}

	inline static int32_t get_offset_of_confirmCollider_34() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___confirmCollider_34)); }
	inline BoxCollider_t22920061 * get_confirmCollider_34() const { return ___confirmCollider_34; }
	inline BoxCollider_t22920061 ** get_address_of_confirmCollider_34() { return &___confirmCollider_34; }
	inline void set_confirmCollider_34(BoxCollider_t22920061 * value)
	{
		___confirmCollider_34 = value;
		Il2CppCodeGenWriteBarrier(&___confirmCollider_34, value);
	}

	inline static int32_t get_offset_of_phoneSeperator_35() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___phoneSeperator_35)); }
	inline UISprite_t603616735 * get_phoneSeperator_35() const { return ___phoneSeperator_35; }
	inline UISprite_t603616735 ** get_address_of_phoneSeperator_35() { return &___phoneSeperator_35; }
	inline void set_phoneSeperator_35(UISprite_t603616735 * value)
	{
		___phoneSeperator_35 = value;
		Il2CppCodeGenWriteBarrier(&___phoneSeperator_35, value);
	}

	inline static int32_t get_offset_of_phoneLabel_36() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___phoneLabel_36)); }
	inline UILabel_t1795115428 * get_phoneLabel_36() const { return ___phoneLabel_36; }
	inline UILabel_t1795115428 ** get_address_of_phoneLabel_36() { return &___phoneLabel_36; }
	inline void set_phoneLabel_36(UILabel_t1795115428 * value)
	{
		___phoneLabel_36 = value;
		Il2CppCodeGenWriteBarrier(&___phoneLabel_36, value);
	}

	inline static int32_t get_offset_of_phoneCollider_37() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___phoneCollider_37)); }
	inline BoxCollider_t22920061 * get_phoneCollider_37() const { return ___phoneCollider_37; }
	inline BoxCollider_t22920061 ** get_address_of_phoneCollider_37() { return &___phoneCollider_37; }
	inline void set_phoneCollider_37(BoxCollider_t22920061 * value)
	{
		___phoneCollider_37 = value;
		Il2CppCodeGenWriteBarrier(&___phoneCollider_37, value);
	}

	inline static int32_t get_offset_of_phoneBottomSeperator_38() { return static_cast<int32_t>(offsetof(AccountDetailsUIScaler_t3070489523, ___phoneBottomSeperator_38)); }
	inline UISprite_t603616735 * get_phoneBottomSeperator_38() const { return ___phoneBottomSeperator_38; }
	inline UISprite_t603616735 ** get_address_of_phoneBottomSeperator_38() { return &___phoneBottomSeperator_38; }
	inline void set_phoneBottomSeperator_38(UISprite_t603616735 * value)
	{
		___phoneBottomSeperator_38 = value;
		Il2CppCodeGenWriteBarrier(&___phoneBottomSeperator_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
