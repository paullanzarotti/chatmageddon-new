﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardSceneManager
struct LeaderboardSceneManager_t2883297036;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardSceneManager::.ctor()
extern "C"  void LeaderboardSceneManager__ctor_m2041960587 (LeaderboardSceneManager_t2883297036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardSceneManager::InitScene()
extern "C"  void LeaderboardSceneManager_InitScene_m4128802871 (LeaderboardSceneManager_t2883297036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardSceneManager::StartScene()
extern "C"  void LeaderboardSceneManager_StartScene_m4209049981 (LeaderboardSceneManager_t2883297036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
