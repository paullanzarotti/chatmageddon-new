﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.String
struct String_t;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>
struct IDictionary_2_t2700217717;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t406167000;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhoneNumbers.PhoneMetadata>
struct KeyCollection_t1858184809;
// System.Collections.Generic.List`1<PhoneNumbers.NumberFormat>
struct List_1_t4105827652;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat>
struct IList_1_t982679825;
// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// PhoneNumbers.PhoneNumber/Builder
struct Builder_t2361401461;
// PhoneNumbers.AsYouTypeFormatter
struct AsYouTypeFormatter_t993724987;
// System.Collections.Generic.IEnumerable`1<PhoneNumbers.PhoneNumberMatch>
struct IEnumerable_1_t2455985625;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Len3057047663.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil4155573397.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberFormat2833677748.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata366861403.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberType324241265.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc922391174.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Val3210374836.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneRegex3216508019.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Builder2361401461.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Types_C2831594294.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Mat2223543287.h"

// System.Void PhoneNumbers.PhoneNumberUtil::.cctor()
extern "C"  void PhoneNumberUtil__cctor_m3914847521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::.ctor()
extern "C"  void PhoneNumberUtil__ctor_m1162228836 (PhoneNumberUtil_t4155573397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::CreateExtnPattern(System.String)
extern "C"  String_t* PhoneNumberUtil_CreateExtnPattern_m698307742 (Il2CppObject * __this /* static, unused */, String_t* ___singleExtnSymbols0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::Verify(PhoneNumbers.PhoneNumberUtil/Leniency,PhoneNumbers.PhoneNumber,System.String,PhoneNumbers.PhoneNumberUtil)
extern "C"  bool PhoneNumberUtil_Verify_m3292353546 (PhoneNumberUtil_t4155573397 * __this, int32_t ___leniency0, PhoneNumber_t814071929 * ___number1, String_t* ___candidate2, PhoneNumberUtil_t4155573397 * ___util3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::Init(System.String)
extern "C"  void PhoneNumberUtil_Init_m2338880630 (PhoneNumberUtil_t4155573397 * __this, String_t* ___filePrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::LoadMetadataFromFile(System.String,System.String,System.Int32)
extern "C"  void PhoneNumberUtil_LoadMetadataFromFile_m2741613788 (PhoneNumberUtil_t4155573397 * __this, String_t* ___filePrefix0, String_t* ___regionCode1, int32_t ___countryCallingCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::ExtractPossibleNumber(System.String)
extern "C"  String_t* PhoneNumberUtil_ExtractPossibleNumber_m2521198126 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsViablePhoneNumber(System.String)
extern "C"  bool PhoneNumberUtil_IsViablePhoneNumber_m3012739498 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::Normalize(System.String)
extern "C"  String_t* PhoneNumberUtil_Normalize_m1858680906 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::Normalize(System.Text.StringBuilder)
extern "C"  void PhoneNumberUtil_Normalize_m3427623419 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::NormalizeDigitsOnly(System.String)
extern "C"  String_t* PhoneNumberUtil_NormalizeDigitsOnly_m1242497314 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder PhoneNumbers.PhoneNumberUtil::NormalizeDigits(System.String,System.Boolean)
extern "C"  StringBuilder_t1221177846 * PhoneNumberUtil_NormalizeDigits_m3653767333 (Il2CppObject * __this /* static, unused */, String_t* ___number0, bool ___keepNonDigits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::ConvertAlphaCharactersInNumber(System.String)
extern "C"  String_t* PhoneNumberUtil_ConvertAlphaCharactersInNumber_m1168325338 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberUtil::GetLengthOfGeographicalAreaCode(PhoneNumbers.PhoneNumber)
extern "C"  int32_t PhoneNumberUtil_GetLengthOfGeographicalAreaCode_m3470728278 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberUtil::GetLengthOfNationalDestinationCode(PhoneNumbers.PhoneNumber)
extern "C"  int32_t PhoneNumberUtil_GetLengthOfNationalDestinationCode_m1883652041 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::NormalizeHelper(System.String,System.Collections.Generic.Dictionary`2<System.Char,System.Char>,System.Boolean)
extern "C"  String_t* PhoneNumberUtil_NormalizeHelper_m2248310000 (Il2CppObject * __this /* static, unused */, String_t* ___number0, Dictionary_2_t759991331 * ___normalizationReplacements1, bool ___removeNonMatches2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>> PhoneNumbers.PhoneNumberUtil::get_CallingCodeToRegion()
extern "C"  Il2CppObject* PhoneNumberUtil_get_CallingCodeToRegion_m4127650919 (PhoneNumberUtil_t4155573397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil PhoneNumbers.PhoneNumberUtil::GetInstance(System.String,System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>)
extern "C"  PhoneNumberUtil_t4155573397 * PhoneNumberUtil_GetInstance_m2938212795 (Il2CppObject * __this /* static, unused */, String_t* ___baseFileLocation0, Dictionary_2_t406167000 * ___countryCallingCodeToRegionCodeMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::ResetInstance()
extern "C"  void PhoneNumberUtil_ResetInstance_m3087513996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1<System.String> PhoneNumbers.PhoneNumberUtil::GetSupportedRegions()
extern "C"  HashSet_1_t362681087 * PhoneNumberUtil_GetSupportedRegions_m1409216566 (PhoneNumberUtil_t4155573397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhoneNumbers.PhoneMetadata> PhoneNumbers.PhoneNumberUtil::GetSupportedGlobalNetworkCallingCodes()
extern "C"  KeyCollection_t1858184809 * PhoneNumberUtil_GetSupportedGlobalNetworkCallingCodes_m2808350903 (PhoneNumberUtil_t4155573397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil PhoneNumbers.PhoneNumberUtil::GetInstance()
extern "C"  PhoneNumberUtil_t4155573397 * PhoneNumberUtil_GetInstance_m1159735835 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsValidRegionCode(System.String)
extern "C"  bool PhoneNumberUtil_IsValidRegionCode_m29308669 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::HasValidCountryCallingCode(System.Int32)
extern "C"  bool PhoneNumberUtil_HasValidCountryCallingCode_m1731055068 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::Format(PhoneNumbers.PhoneNumber,PhoneNumbers.PhoneNumberFormat)
extern "C"  String_t* PhoneNumberUtil_Format_m1374397719 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, int32_t ___numberFormat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::Format(PhoneNumbers.PhoneNumber,PhoneNumbers.PhoneNumberFormat,System.Text.StringBuilder)
extern "C"  void PhoneNumberUtil_Format_m4166470054 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, int32_t ___numberFormat1, StringBuilder_t1221177846 * ___formattedNumber2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatByPattern(PhoneNumbers.PhoneNumber,PhoneNumbers.PhoneNumberFormat,System.Collections.Generic.List`1<PhoneNumbers.NumberFormat>)
extern "C"  String_t* PhoneNumberUtil_FormatByPattern_m1378452592 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, int32_t ___numberFormat1, List_1_t4105827652 * ___userDefinedFormats2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNationalNumberWithCarrierCode(PhoneNumbers.PhoneNumber,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatNationalNumberWithCarrierCode_m1513630909 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___carrierCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneNumberUtil::GetMetadataForRegionOrCallingCode(System.Int32,System.String)
extern "C"  PhoneMetadata_t366861403 * PhoneNumberUtil_GetMetadataForRegionOrCallingCode_m3710987191 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, String_t* ___regionCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNationalNumberWithPreferredCarrierCode(PhoneNumbers.PhoneNumber,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatNationalNumberWithPreferredCarrierCode_m2515616818 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___fallbackCarrierCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNumberForMobileDialing(PhoneNumbers.PhoneNumber,System.String,System.Boolean)
extern "C"  String_t* PhoneNumberUtil_FormatNumberForMobileDialing_m4157871218 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___regionCallingFrom1, bool ___withFormatting2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatOutOfCountryCallingNumber(PhoneNumbers.PhoneNumber,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatOutOfCountryCallingNumber_m759153381 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___regionCallingFrom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatInOriginalFormat(PhoneNumbers.PhoneNumber,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatInOriginalFormat_m2222004816 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___regionCallingFrom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::RawInputContainsNationalPrefix(System.String,System.String,System.String)
extern "C"  bool PhoneNumberUtil_RawInputContainsNationalPrefix_m4124922351 (PhoneNumberUtil_t4155573397 * __this, String_t* ___rawInput0, String_t* ___nationalPrefix1, String_t* ___regionCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::HasUnexpectedItalianLeadingZero(PhoneNumbers.PhoneNumber)
extern "C"  bool PhoneNumberUtil_HasUnexpectedItalianLeadingZero_m2956884990 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::HasFormattingPatternForNumber(PhoneNumbers.PhoneNumber)
extern "C"  bool PhoneNumberUtil_HasFormattingPatternForNumber_m697363338 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatOutOfCountryKeepingAlphaChars(PhoneNumbers.PhoneNumber,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatOutOfCountryKeepingAlphaChars_m1305105848 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___regionCallingFrom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::GetNationalSignificantNumber(PhoneNumbers.PhoneNumber)
extern "C"  String_t* PhoneNumberUtil_GetNationalSignificantNumber_m2978053920 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::PrefixNumberWithCountryCallingCode(System.Int32,PhoneNumbers.PhoneNumberFormat,System.Text.StringBuilder)
extern "C"  void PhoneNumberUtil_PrefixNumberWithCountryCallingCode_m1420068699 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, int32_t ___numberFormat1, StringBuilder_t1221177846 * ___formattedNumber2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNsn(System.String,PhoneNumbers.PhoneMetadata,PhoneNumbers.PhoneNumberFormat)
extern "C"  String_t* PhoneNumberUtil_FormatNsn_m1118817034 (PhoneNumberUtil_t4155573397 * __this, String_t* ___number0, PhoneMetadata_t366861403 * ___metadata1, int32_t ___numberFormat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNsn(System.String,PhoneNumbers.PhoneMetadata,PhoneNumbers.PhoneNumberFormat,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatNsn_m2987471754 (PhoneNumberUtil_t4155573397 * __this, String_t* ___number0, PhoneMetadata_t366861403 * ___metadata1, int32_t ___numberFormat2, String_t* ___carrierCode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.PhoneNumberUtil::ChooseFormattingPatternForNumber(System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat>,System.String)
extern "C"  NumberFormat_t441739224 * PhoneNumberUtil_ChooseFormattingPatternForNumber_m554374224 (PhoneNumberUtil_t4155573397 * __this, Il2CppObject* ___availableFormats0, String_t* ___nationalNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNsnUsingPattern(System.String,PhoneNumbers.NumberFormat,PhoneNumbers.PhoneNumberFormat)
extern "C"  String_t* PhoneNumberUtil_FormatNsnUsingPattern_m1625900059 (PhoneNumberUtil_t4155573397 * __this, String_t* ___nationalNumber0, NumberFormat_t441739224 * ___formattingPattern1, int32_t ___numberFormat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::FormatNsnUsingPattern(System.String,PhoneNumbers.NumberFormat,PhoneNumbers.PhoneNumberFormat,System.String)
extern "C"  String_t* PhoneNumberUtil_FormatNsnUsingPattern_m2416749477 (PhoneNumberUtil_t4155573397 * __this, String_t* ___nationalNumber0, NumberFormat_t441739224 * ___formattingPattern1, int32_t ___numberFormat2, String_t* ___carrierCode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberUtil::GetExampleNumber(System.String)
extern "C"  PhoneNumber_t814071929 * PhoneNumberUtil_GetExampleNumber_m2996347255 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberUtil::GetExampleNumberForType(System.String,PhoneNumbers.PhoneNumberType)
extern "C"  PhoneNumber_t814071929 * PhoneNumberUtil_GetExampleNumberForType_m2223269541 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberUtil::GetExampleNumberForNonGeoEntity(System.Int32)
extern "C"  PhoneNumber_t814071929 * PhoneNumberUtil_GetExampleNumberForNonGeoEntity_m4076367716 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::MaybeAppendFormattedExtension(PhoneNumbers.PhoneNumber,PhoneNumbers.PhoneMetadata,PhoneNumbers.PhoneNumberFormat,System.Text.StringBuilder)
extern "C"  void PhoneNumberUtil_MaybeAppendFormattedExtension_m2048889277 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, PhoneMetadata_t366861403 * ___metadata1, int32_t ___numberFormat2, StringBuilder_t1221177846 * ___formattedNumber3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneNumberUtil::GetNumberDescByType(PhoneNumbers.PhoneMetadata,PhoneNumbers.PhoneNumberType)
extern "C"  PhoneNumberDesc_t922391174 * PhoneNumberUtil_GetNumberDescByType_m1971467280 (PhoneNumberUtil_t4155573397 * __this, PhoneMetadata_t366861403 * ___metadata0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberType PhoneNumbers.PhoneNumberUtil::GetNumberType(PhoneNumbers.PhoneNumber)
extern "C"  int32_t PhoneNumberUtil_GetNumberType_m2329343814 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberType PhoneNumbers.PhoneNumberUtil::GetNumberTypeHelper(System.String,PhoneNumbers.PhoneMetadata)
extern "C"  int32_t PhoneNumberUtil_GetNumberTypeHelper_m2954567978 (PhoneNumberUtil_t4155573397 * __this, String_t* ___nationalNumber0, PhoneMetadata_t366861403 * ___metadata1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneNumberUtil::GetMetadataForRegion(System.String)
extern "C"  PhoneMetadata_t366861403 * PhoneNumberUtil_GetMetadataForRegion_m1544252556 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneNumberUtil::GetMetadataForNonGeographicalRegion(System.Int32)
extern "C"  PhoneMetadata_t366861403 * PhoneNumberUtil_GetMetadataForNonGeographicalRegion_m2848935868 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsNumberMatchingDesc(System.String,PhoneNumbers.PhoneNumberDesc)
extern "C"  bool PhoneNumberUtil_IsNumberMatchingDesc_m3579212411 (PhoneNumberUtil_t4155573397 * __this, String_t* ___nationalNumber0, PhoneNumberDesc_t922391174 * ___numberDesc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsValidNumber(PhoneNumbers.PhoneNumber)
extern "C"  bool PhoneNumberUtil_IsValidNumber_m4179590562 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsValidNumberForRegion(PhoneNumbers.PhoneNumber,System.String)
extern "C"  bool PhoneNumberUtil_IsValidNumberForRegion_m3292996945 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___regionCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::GetRegionCodeForNumber(PhoneNumbers.PhoneNumber)
extern "C"  String_t* PhoneNumberUtil_GetRegionCodeForNumber_m3281807245 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::GetRegionCodeForNumberFromRegionList(PhoneNumbers.PhoneNumber,System.Collections.Generic.List`1<System.String>)
extern "C"  String_t* PhoneNumberUtil_GetRegionCodeForNumberFromRegionList_m2490002097 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, List_1_t1398341365 * ___regionCodes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::GetRegionCodeForCountryCode(System.Int32)
extern "C"  String_t* PhoneNumberUtil_GetRegionCodeForCountryCode_m2907991719 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberUtil::GetCountryCodeForRegion(System.String)
extern "C"  int32_t PhoneNumberUtil_GetCountryCodeForRegion_m3006004778 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberUtil::GetCountryCodeForValidRegion(System.String)
extern "C"  int32_t PhoneNumberUtil_GetCountryCodeForValidRegion_m1788992198 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::GetNddPrefixForRegion(System.String,System.Boolean)
extern "C"  String_t* PhoneNumberUtil_GetNddPrefixForRegion_m3552620297 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, bool ___stripNonDigits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsNANPACountry(System.String)
extern "C"  bool PhoneNumberUtil_IsNANPACountry_m841878886 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsLeadingZeroPossible(System.Int32)
extern "C"  bool PhoneNumberUtil_IsLeadingZeroPossible_m1861822304 (PhoneNumberUtil_t4155573397 * __this, int32_t ___countryCallingCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsAlphaNumber(System.String)
extern "C"  bool PhoneNumberUtil_IsAlphaNumber_m3054051065 (PhoneNumberUtil_t4155573397 * __this, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsPossibleNumber(PhoneNumbers.PhoneNumber)
extern "C"  bool PhoneNumberUtil_IsPossibleNumber_m2996593491 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil/ValidationResult PhoneNumbers.PhoneNumberUtil::TestNumberLengthAgainstPattern(PhoneNumbers.PhoneRegex,System.String)
extern "C"  int32_t PhoneNumberUtil_TestNumberLengthAgainstPattern_m2243237548 (PhoneNumberUtil_t4155573397 * __this, PhoneRegex_t3216508019 * ___numberPattern0, String_t* ___number1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil/ValidationResult PhoneNumbers.PhoneNumberUtil::IsPossibleNumberWithReason(PhoneNumbers.PhoneNumber)
extern "C"  int32_t PhoneNumberUtil_IsPossibleNumberWithReason_m2665692374 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsPossibleNumber(System.String,System.String)
extern "C"  bool PhoneNumberUtil_IsPossibleNumber_m1452702296 (PhoneNumberUtil_t4155573397 * __this, String_t* ___number0, String_t* ___regionDialingFrom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::TruncateTooLongNumber(PhoneNumbers.PhoneNumber/Builder)
extern "C"  bool PhoneNumberUtil_TruncateTooLongNumber_m1886682794 (PhoneNumberUtil_t4155573397 * __this, Builder_t2361401461 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AsYouTypeFormatter PhoneNumbers.PhoneNumberUtil::GetAsYouTypeFormatter(System.String)
extern "C"  AsYouTypeFormatter_t993724987 * PhoneNumberUtil_GetAsYouTypeFormatter_m3061946071 (PhoneNumberUtil_t4155573397 * __this, String_t* ___regionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberUtil::ExtractCountryCode(System.Text.StringBuilder,System.Text.StringBuilder)
extern "C"  int32_t PhoneNumberUtil_ExtractCountryCode_m69437758 (PhoneNumberUtil_t4155573397 * __this, StringBuilder_t1221177846 * ___fullNumber0, StringBuilder_t1221177846 * ___nationalNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumberUtil::MaybeExtractCountryCode(System.String,PhoneNumbers.PhoneMetadata,System.Text.StringBuilder,System.Boolean,PhoneNumbers.PhoneNumber/Builder)
extern "C"  int32_t PhoneNumberUtil_MaybeExtractCountryCode_m3501716199 (PhoneNumberUtil_t4155573397 * __this, String_t* ___number0, PhoneMetadata_t366861403 * ___defaultRegionMetadata1, StringBuilder_t1221177846 * ___nationalNumber2, bool ___keepRawInput3, Builder_t2361401461 * ___phoneNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::ParsePrefixAsIdd(PhoneNumbers.PhoneRegex,System.Text.StringBuilder)
extern "C"  bool PhoneNumberUtil_ParsePrefixAsIdd_m3097869771 (PhoneNumberUtil_t4155573397 * __this, PhoneRegex_t3216508019 * ___iddPattern0, StringBuilder_t1221177846 * ___number1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Types/CountryCodeSource PhoneNumbers.PhoneNumberUtil::MaybeStripInternationalPrefixAndNormalize(System.Text.StringBuilder,System.String)
extern "C"  int32_t PhoneNumberUtil_MaybeStripInternationalPrefixAndNormalize_m2350825143 (PhoneNumberUtil_t4155573397 * __this, StringBuilder_t1221177846 * ___number0, String_t* ___possibleIddPrefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::MaybeStripNationalPrefixAndCarrierCode(System.Text.StringBuilder,PhoneNumbers.PhoneMetadata,System.Text.StringBuilder)
extern "C"  bool PhoneNumberUtil_MaybeStripNationalPrefixAndCarrierCode_m2213312707 (PhoneNumberUtil_t4155573397 * __this, StringBuilder_t1221177846 * ___number0, PhoneMetadata_t366861403 * ___metadata1, StringBuilder_t1221177846 * ___carrierCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::MaybeStripExtension(System.Text.StringBuilder)
extern "C"  String_t* PhoneNumberUtil_MaybeStripExtension_m3515381554 (PhoneNumberUtil_t4155573397 * __this, StringBuilder_t1221177846 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::CheckRegionForParsing(System.String,System.String)
extern "C"  bool PhoneNumberUtil_CheckRegionForParsing_m3783974263 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, String_t* ___defaultRegion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberUtil::Parse(System.String,System.String)
extern "C"  PhoneNumber_t814071929 * PhoneNumberUtil_Parse_m131980609 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, String_t* ___defaultRegion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::Parse(System.String,System.String,PhoneNumbers.PhoneNumber/Builder)
extern "C"  void PhoneNumberUtil_Parse_m2156874872 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, String_t* ___defaultRegion1, Builder_t2361401461 * ___phoneNumber2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumberUtil::ParseAndKeepRawInput(System.String,System.String)
extern "C"  PhoneNumber_t814071929 * PhoneNumberUtil_ParseAndKeepRawInput_m3969282415 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, String_t* ___defaultRegion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::ParseAndKeepRawInput(System.String,System.String,PhoneNumbers.PhoneNumber/Builder)
extern "C"  void PhoneNumberUtil_ParseAndKeepRawInput_m468479870 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, String_t* ___defaultRegion1, Builder_t2361401461 * ___phoneNumber2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<PhoneNumbers.PhoneNumberMatch> PhoneNumbers.PhoneNumberUtil::FindNumbers(System.String,System.String)
extern "C"  Il2CppObject* PhoneNumberUtil_FindNumbers_m2188463081 (PhoneNumberUtil_t4155573397 * __this, String_t* ___text0, String_t* ___defaultRegion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<PhoneNumbers.PhoneNumberMatch> PhoneNumbers.PhoneNumberUtil::FindNumbers(System.String,System.String,PhoneNumbers.PhoneNumberUtil/Leniency,System.Int64)
extern "C"  Il2CppObject* PhoneNumberUtil_FindNumbers_m3857856632 (PhoneNumberUtil_t4155573397 * __this, String_t* ___text0, String_t* ___defaultRegion1, int32_t ___leniency2, int64_t ___maxTries3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::ParseHelper(System.String,System.String,System.Boolean,System.Boolean,PhoneNumbers.PhoneNumber/Builder)
extern "C"  void PhoneNumberUtil_ParseHelper_m101529810 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, String_t* ___defaultRegion1, bool ___keepRawInput2, bool ___checkRegion3, Builder_t2361401461 * ___phoneNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::AreEqual(PhoneNumbers.PhoneNumber/Builder,PhoneNumbers.PhoneNumber/Builder)
extern "C"  bool PhoneNumberUtil_AreEqual_m3871310062 (Il2CppObject * __this /* static, unused */, Builder_t2361401461 * ___p10, Builder_t2361401461 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberUtil::BuildNationalNumberForParsing(System.String,System.Text.StringBuilder)
extern "C"  void PhoneNumberUtil_BuildNationalNumberForParsing_m2249722420 (PhoneNumberUtil_t4155573397 * __this, String_t* ___numberToParse0, StringBuilder_t1221177846 * ___nationalNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil/MatchType PhoneNumbers.PhoneNumberUtil::IsNumberMatch(PhoneNumbers.PhoneNumber,PhoneNumbers.PhoneNumber)
extern "C"  int32_t PhoneNumberUtil_IsNumberMatch_m3168107796 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___firstNumberIn0, PhoneNumber_t814071929 * ___secondNumberIn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::IsNationalNumberSuffixOfTheOther(PhoneNumbers.PhoneNumber/Builder,PhoneNumbers.PhoneNumber/Builder)
extern "C"  bool PhoneNumberUtil_IsNationalNumberSuffixOfTheOther_m1161305928 (PhoneNumberUtil_t4155573397 * __this, Builder_t2361401461 * ___firstNumber0, Builder_t2361401461 * ___secondNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil/MatchType PhoneNumbers.PhoneNumberUtil::IsNumberMatch(System.String,System.String)
extern "C"  int32_t PhoneNumberUtil_IsNumberMatch_m16160654 (PhoneNumberUtil_t4155573397 * __this, String_t* ___firstNumber0, String_t* ___secondNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberUtil/MatchType PhoneNumbers.PhoneNumberUtil::IsNumberMatch(PhoneNumbers.PhoneNumber,System.String)
extern "C"  int32_t PhoneNumberUtil_IsNumberMatch_m2192666109 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___firstNumber0, String_t* ___secondNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::CanBeInternationallyDialled(PhoneNumbers.PhoneNumber)
extern "C"  bool PhoneNumberUtil_CanBeInternationallyDialled_m2730017324 (PhoneNumberUtil_t4155573397 * __this, PhoneNumber_t814071929 * ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::<PhoneNumberUtil>m__0(System.Char)
extern "C"  bool PhoneNumberUtil_U3CPhoneNumberUtilU3Em__0_m707479665 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::<PhoneNumberUtil>m__1(System.Char)
extern "C"  String_t* PhoneNumberUtil_U3CPhoneNumberUtilU3Em__1_m4004732349 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::<PhoneNumberUtil>m__2(System.Char)
extern "C"  bool PhoneNumberUtil_U3CPhoneNumberUtilU3Em__2_m3304079291 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberUtil::<PhoneNumberUtil>m__3(System.Char)
extern "C"  String_t* PhoneNumberUtil_U3CPhoneNumberUtilU3Em__3_m854631483 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::<Verify>m__4(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,System.Text.StringBuilder,System.String[])
extern "C"  bool PhoneNumberUtil_U3CVerifyU3Em__4_m2342849890 (Il2CppObject * __this /* static, unused */, PhoneNumberUtil_t4155573397 * ___u0, PhoneNumber_t814071929 * ___n1, StringBuilder_t1221177846 * ___nc2, StringU5BU5D_t1642385972* ___eg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberUtil::<Verify>m__5(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,System.Text.StringBuilder,System.String[])
extern "C"  bool PhoneNumberUtil_U3CVerifyU3Em__5_m3823747107 (Il2CppObject * __this /* static, unused */, PhoneNumberUtil_t4155573397 * ___u0, PhoneNumber_t814071929 * ___n1, StringBuilder_t1221177846 * ___normalizedCandidate2, StringU5BU5D_t1642385972* ___expectedNumberGroups3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
