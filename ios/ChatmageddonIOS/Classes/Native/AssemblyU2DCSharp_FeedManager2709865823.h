﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,Feed>
struct Dictionary_2_t1027956130;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2460531543.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeedManager
struct  FeedManager_t2709865823  : public MonoSingleton_1_t2460531543
{
public:
	// System.Boolean FeedManager::connectionAvailable
	bool ___connectionAvailable_3;
	// System.Collections.Generic.Dictionary`2<System.String,Feed> FeedManager::feedStore
	Dictionary_2_t1027956130 * ___feedStore_4;

public:
	inline static int32_t get_offset_of_connectionAvailable_3() { return static_cast<int32_t>(offsetof(FeedManager_t2709865823, ___connectionAvailable_3)); }
	inline bool get_connectionAvailable_3() const { return ___connectionAvailable_3; }
	inline bool* get_address_of_connectionAvailable_3() { return &___connectionAvailable_3; }
	inline void set_connectionAvailable_3(bool value)
	{
		___connectionAvailable_3 = value;
	}

	inline static int32_t get_offset_of_feedStore_4() { return static_cast<int32_t>(offsetof(FeedManager_t2709865823, ___feedStore_4)); }
	inline Dictionary_2_t1027956130 * get_feedStore_4() const { return ___feedStore_4; }
	inline Dictionary_2_t1027956130 ** get_address_of_feedStore_4() { return &___feedStore_4; }
	inline void set_feedStore_4(Dictionary_2_t1027956130 * value)
	{
		___feedStore_4 = value;
		Il2CppCodeGenWriteBarrier(&___feedStore_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
