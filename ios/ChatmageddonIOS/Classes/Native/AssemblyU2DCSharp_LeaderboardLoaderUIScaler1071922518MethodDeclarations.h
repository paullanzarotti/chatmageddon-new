﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardLoaderUIScaler
struct LeaderboardLoaderUIScaler_t1071922518;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardLoaderUIScaler::.ctor()
extern "C"  void LeaderboardLoaderUIScaler__ctor_m1453608713 (LeaderboardLoaderUIScaler_t1071922518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardLoaderUIScaler::GlobalUIScale()
extern "C"  void LeaderboardLoaderUIScaler_GlobalUIScale_m3677845104 (LeaderboardLoaderUIScaler_t1071922518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
