﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<StatusNavScreen>
struct List_1_t2241490215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1776219889.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"

// System.Void System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3050951582_gshared (Enumerator_t1776219889 * __this, List_1_t2241490215 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3050951582(__this, ___l0, method) ((  void (*) (Enumerator_t1776219889 *, List_1_t2241490215 *, const MethodInfo*))Enumerator__ctor_m3050951582_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3006741196_gshared (Enumerator_t1776219889 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3006741196(__this, method) ((  void (*) (Enumerator_t1776219889 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3006741196_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4041533864_gshared (Enumerator_t1776219889 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4041533864(__this, method) ((  Il2CppObject * (*) (Enumerator_t1776219889 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4041533864_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m3182980603_gshared (Enumerator_t1776219889 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3182980603(__this, method) ((  void (*) (Enumerator_t1776219889 *, const MethodInfo*))Enumerator_Dispose_m3182980603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3246452756_gshared (Enumerator_t1776219889 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3246452756(__this, method) ((  void (*) (Enumerator_t1776219889 *, const MethodInfo*))Enumerator_VerifyState_m3246452756_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2180584276_gshared (Enumerator_t1776219889 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2180584276(__this, method) ((  bool (*) (Enumerator_t1776219889 *, const MethodInfo*))Enumerator_MoveNext_m2180584276_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<StatusNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3366968003_gshared (Enumerator_t1776219889 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3366968003(__this, method) ((  int32_t (*) (Enumerator_t1776219889 *, const MethodInfo*))Enumerator_get_Current_m3366968003_gshared)(__this, method)
