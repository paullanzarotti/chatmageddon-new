﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDecryptionTransform
struct XmlDecryptionTransform_t3146098518;
// System.String
struct String_t;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDecryptionTransform::.ctor()
extern "C"  void XmlDecryptionTransform__ctor_m1420680942 (XmlDecryptionTransform_t3146098518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDecryptionTransform::AddExceptUri(System.String)
extern "C"  void XmlDecryptionTransform_AddExceptUri_m2764257946 (XmlDecryptionTransform_t3146098518 * __this, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDecryptionTransform::ClearExceptUris()
extern "C"  void XmlDecryptionTransform_ClearExceptUris_m2621015121 (XmlDecryptionTransform_t3146098518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDecryptionTransform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDecryptionTransform_GetInnerXml_m3735497691 (XmlDecryptionTransform_t3146098518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDecryptionTransform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDecryptionTransform_LoadInnerXml_m3969018206 (XmlDecryptionTransform_t3146098518 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
