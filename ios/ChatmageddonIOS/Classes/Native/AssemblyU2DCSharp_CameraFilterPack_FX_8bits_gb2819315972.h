﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_FX_8bits_gb
struct  CameraFilterPack_FX_8bits_gb_t2819315972  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_FX_8bits_gb::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_8bits_gb::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_FX_8bits_gb::SCMaterial
	Material_t193706927 * ___SCMaterial_4;
	// System.Single CameraFilterPack_FX_8bits_gb::Brightness
	float ___Brightness_5;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_gb_t2819315972, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_gb_t2819315972, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_gb_t2819315972, ___SCMaterial_4)); }
	inline Material_t193706927 * get_SCMaterial_4() const { return ___SCMaterial_4; }
	inline Material_t193706927 ** get_address_of_SCMaterial_4() { return &___SCMaterial_4; }
	inline void set_SCMaterial_4(Material_t193706927 * value)
	{
		___SCMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_4, value);
	}

	inline static int32_t get_offset_of_Brightness_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_gb_t2819315972, ___Brightness_5)); }
	inline float get_Brightness_5() const { return ___Brightness_5; }
	inline float* get_address_of_Brightness_5() { return &___Brightness_5; }
	inline void set_Brightness_5(float value)
	{
		___Brightness_5 = value;
	}
};

struct CameraFilterPack_FX_8bits_gb_t2819315972_StaticFields
{
public:
	// System.Single CameraFilterPack_FX_8bits_gb::ChangeBrightness
	float ___ChangeBrightness_6;

public:
	inline static int32_t get_offset_of_ChangeBrightness_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_FX_8bits_gb_t2819315972_StaticFields, ___ChangeBrightness_6)); }
	inline float get_ChangeBrightness_6() const { return ___ChangeBrightness_6; }
	inline float* get_address_of_ChangeBrightness_6() { return &___ChangeBrightness_6; }
	inline void set_ChangeBrightness_6(float value)
	{
		___ChangeBrightness_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
