﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationPermissionAttribute
struct ConfigurationPermissionAttribute_t1968770041;
// System.Security.IPermission
struct IPermission_t182075948;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction446643378.h"

// System.Void System.Configuration.ConfigurationPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C"  void ConfigurationPermissionAttribute__ctor_m1744450083 (ConfigurationPermissionAttribute_t1968770041 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Configuration.ConfigurationPermissionAttribute::CreatePermission()
extern "C"  Il2CppObject * ConfigurationPermissionAttribute_CreatePermission_m2863867087 (ConfigurationPermissionAttribute_t1968770041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
