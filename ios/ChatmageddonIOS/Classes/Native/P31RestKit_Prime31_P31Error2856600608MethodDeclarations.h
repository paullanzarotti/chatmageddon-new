﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.P31Error
struct P31Error_t2856600608;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.P31Error::.ctor()
extern "C"  void P31Error__ctor_m1526629766 (P31Error_t2856600608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.P31Error::get_message()
extern "C"  String_t* P31Error_get_message_m3889148549 (P31Error_t2856600608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31Error::set_message(System.String)
extern "C"  void P31Error_set_message_m536934456 (P31Error_t2856600608 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31Error::set_domain(System.String)
extern "C"  void P31Error_set_domain_m463845381 (P31Error_t2856600608 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31Error::set_code(System.Int32)
extern "C"  void P31Error_set_code_m2488835931 (P31Error_t2856600608 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31Error::set_userInfo(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void P31Error_set_userInfo_m2084099227 (P31Error_t2856600608 * __this, Dictionary_2_t309261261 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Prime31.P31Error Prime31.P31Error::errorFromJson(System.String)
extern "C"  P31Error_t2856600608 * P31Error_errorFromJson_m2879089896 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Prime31.P31Error::ToString()
extern "C"  String_t* P31Error_ToString_m741706505 (P31Error_t2856600608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
