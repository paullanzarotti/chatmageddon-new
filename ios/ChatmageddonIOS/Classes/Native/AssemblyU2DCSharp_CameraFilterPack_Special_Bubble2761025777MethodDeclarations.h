﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Special_Bubble
struct CameraFilterPack_Special_Bubble_t2761025777;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Special_Bubble::.ctor()
extern "C"  void CameraFilterPack_Special_Bubble__ctor_m2225758338 (CameraFilterPack_Special_Bubble_t2761025777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Special_Bubble::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Special_Bubble_get_material_m3467608409 (CameraFilterPack_Special_Bubble_t2761025777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::Start()
extern "C"  void CameraFilterPack_Special_Bubble_Start_m338058146 (CameraFilterPack_Special_Bubble_t2761025777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Special_Bubble_OnRenderImage_m3620505202 (CameraFilterPack_Special_Bubble_t2761025777 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::OnValidate()
extern "C"  void CameraFilterPack_Special_Bubble_OnValidate_m3701957447 (CameraFilterPack_Special_Bubble_t2761025777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::Update()
extern "C"  void CameraFilterPack_Special_Bubble_Update_m3393740573 (CameraFilterPack_Special_Bubble_t2761025777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::OnDisable()
extern "C"  void CameraFilterPack_Special_Bubble_OnDisable_m2014852681 (CameraFilterPack_Special_Bubble_t2761025777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
