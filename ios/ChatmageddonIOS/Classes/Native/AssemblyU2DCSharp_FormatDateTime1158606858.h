﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen909272578.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FormatDateTime
struct  FormatDateTime_t1158606858  : public MonoSingleton_1_t909272578
{
public:
	// System.String FormatDateTime::formatString
	String_t* ___formatString_3;

public:
	inline static int32_t get_offset_of_formatString_3() { return static_cast<int32_t>(offsetof(FormatDateTime_t1158606858, ___formatString_3)); }
	inline String_t* get_formatString_3() const { return ___formatString_3; }
	inline String_t** get_address_of_formatString_3() { return &___formatString_3; }
	inline void set_formatString_3(String_t* value)
	{
		___formatString_3 = value;
		Il2CppCodeGenWriteBarrier(&___formatString_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
