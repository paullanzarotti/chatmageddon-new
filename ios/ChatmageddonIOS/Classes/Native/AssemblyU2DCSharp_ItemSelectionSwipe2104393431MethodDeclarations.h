﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemSelectionSwipe
struct ItemSelectionSwipe_t2104393431;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemSelectionSwipe::.ctor()
extern "C"  void ItemSelectionSwipe__ctor_m3997248458 (ItemSelectionSwipe_t2104393431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSelectionSwipe::OnSwipeLeft()
extern "C"  void ItemSelectionSwipe_OnSwipeLeft_m4225229326 (ItemSelectionSwipe_t2104393431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSelectionSwipe::OnSwipeRight()
extern "C"  void ItemSelectionSwipe_OnSwipeRight_m1816625547 (ItemSelectionSwipe_t2104393431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSelectionSwipe::OnStaticClick()
extern "C"  void ItemSelectionSwipe_OnStaticClick_m3947288061 (ItemSelectionSwipe_t2104393431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
