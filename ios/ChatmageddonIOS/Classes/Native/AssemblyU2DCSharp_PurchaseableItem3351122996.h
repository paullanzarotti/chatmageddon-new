﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<QuantitativeItem>
struct List_1_t2405634912;

#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_CostType3548590213.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PurchaseableItem
struct  PurchaseableItem_t3351122996  : public Item_t2440468191
{
public:
	// System.Int32 PurchaseableItem::minimumUserLevel
	int32_t ___minimumUserLevel_4;
	// CostType PurchaseableItem::costType
	int32_t ___costType_5;
	// System.Int32 PurchaseableItem::sortOrder
	int32_t ___sortOrder_6;
	// PurchaseableCategory PurchaseableItem::category
	int32_t ___category_7;
	// System.Int32 PurchaseableItem::cost
	int32_t ___cost_8;
	// System.Collections.Generic.List`1<QuantitativeItem> PurchaseableItem::items
	List_1_t2405634912 * ___items_9;

public:
	inline static int32_t get_offset_of_minimumUserLevel_4() { return static_cast<int32_t>(offsetof(PurchaseableItem_t3351122996, ___minimumUserLevel_4)); }
	inline int32_t get_minimumUserLevel_4() const { return ___minimumUserLevel_4; }
	inline int32_t* get_address_of_minimumUserLevel_4() { return &___minimumUserLevel_4; }
	inline void set_minimumUserLevel_4(int32_t value)
	{
		___minimumUserLevel_4 = value;
	}

	inline static int32_t get_offset_of_costType_5() { return static_cast<int32_t>(offsetof(PurchaseableItem_t3351122996, ___costType_5)); }
	inline int32_t get_costType_5() const { return ___costType_5; }
	inline int32_t* get_address_of_costType_5() { return &___costType_5; }
	inline void set_costType_5(int32_t value)
	{
		___costType_5 = value;
	}

	inline static int32_t get_offset_of_sortOrder_6() { return static_cast<int32_t>(offsetof(PurchaseableItem_t3351122996, ___sortOrder_6)); }
	inline int32_t get_sortOrder_6() const { return ___sortOrder_6; }
	inline int32_t* get_address_of_sortOrder_6() { return &___sortOrder_6; }
	inline void set_sortOrder_6(int32_t value)
	{
		___sortOrder_6 = value;
	}

	inline static int32_t get_offset_of_category_7() { return static_cast<int32_t>(offsetof(PurchaseableItem_t3351122996, ___category_7)); }
	inline int32_t get_category_7() const { return ___category_7; }
	inline int32_t* get_address_of_category_7() { return &___category_7; }
	inline void set_category_7(int32_t value)
	{
		___category_7 = value;
	}

	inline static int32_t get_offset_of_cost_8() { return static_cast<int32_t>(offsetof(PurchaseableItem_t3351122996, ___cost_8)); }
	inline int32_t get_cost_8() const { return ___cost_8; }
	inline int32_t* get_address_of_cost_8() { return &___cost_8; }
	inline void set_cost_8(int32_t value)
	{
		___cost_8 = value;
	}

	inline static int32_t get_offset_of_items_9() { return static_cast<int32_t>(offsetof(PurchaseableItem_t3351122996, ___items_9)); }
	inline List_1_t2405634912 * get_items_9() const { return ___items_9; }
	inline List_1_t2405634912 ** get_address_of_items_9() { return &___items_9; }
	inline void set_items_9(List_1_t2405634912 * value)
	{
		___items_9 = value;
		Il2CppCodeGenWriteBarrier(&___items_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
