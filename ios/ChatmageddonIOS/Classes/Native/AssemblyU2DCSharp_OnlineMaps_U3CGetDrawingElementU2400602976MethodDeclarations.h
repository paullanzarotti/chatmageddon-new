﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMaps/<GetDrawingElement>c__AnonStorey0
struct U3CGetDrawingElementU3Ec__AnonStorey0_t2400602976;
// OnlineMapsDrawingElement
struct OnlineMapsDrawingElement_t539447654;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsDrawingElement539447654.h"

// System.Void OnlineMaps/<GetDrawingElement>c__AnonStorey0::.ctor()
extern "C"  void U3CGetDrawingElementU3Ec__AnonStorey0__ctor_m1247833917 (U3CGetDrawingElementU3Ec__AnonStorey0_t2400602976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps/<GetDrawingElement>c__AnonStorey0::<>m__0(OnlineMapsDrawingElement)
extern "C"  bool U3CGetDrawingElementU3Ec__AnonStorey0_U3CU3Em__0_m225221586 (U3CGetDrawingElementU3Ec__AnonStorey0_t2400602976 * __this, OnlineMapsDrawingElement_t539447654 * ___el0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
