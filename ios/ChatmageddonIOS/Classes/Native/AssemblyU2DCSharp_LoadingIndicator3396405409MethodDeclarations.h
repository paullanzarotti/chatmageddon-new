﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingIndicator
struct LoadingIndicator_t3396405409;
// LoadingIndicator/IndicatorShown
struct IndicatorShown_t76852832;
// LoadingIndicator/IndicatorHidden
struct IndicatorHidden_t1529613817;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LoadingIndicator_IndicatorShown76852832.h"
#include "AssemblyU2DCSharp_LoadingIndicator_IndicatorHidden1529613817.h"

// System.Void LoadingIndicator::.ctor()
extern "C"  void LoadingIndicator__ctor_m2347576576 (LoadingIndicator_t3396405409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::add_onIndicatorShown(LoadingIndicator/IndicatorShown)
extern "C"  void LoadingIndicator_add_onIndicatorShown_m22995979 (LoadingIndicator_t3396405409 * __this, IndicatorShown_t76852832 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::remove_onIndicatorShown(LoadingIndicator/IndicatorShown)
extern "C"  void LoadingIndicator_remove_onIndicatorShown_m1923453924 (LoadingIndicator_t3396405409 * __this, IndicatorShown_t76852832 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::add_onIndicatorHidden(LoadingIndicator/IndicatorHidden)
extern "C"  void LoadingIndicator_add_onIndicatorHidden_m3184801255 (LoadingIndicator_t3396405409 * __this, IndicatorHidden_t1529613817 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::remove_onIndicatorHidden(LoadingIndicator/IndicatorHidden)
extern "C"  void LoadingIndicator_remove_onIndicatorHidden_m3854515910 (LoadingIndicator_t3396405409 * __this, IndicatorHidden_t1529613817 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::ShowLoadingIndicator(System.Boolean)
extern "C"  void LoadingIndicator_ShowLoadingIndicator_m1964232199 (LoadingIndicator_t3396405409 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::ShowIndicator()
extern "C"  void LoadingIndicator_ShowIndicator_m13398398 (LoadingIndicator_t3396405409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::SendShownEvent()
extern "C"  void LoadingIndicator_SendShownEvent_m410181505 (LoadingIndicator_t3396405409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::OnShowIndicator(System.Boolean)
extern "C"  void LoadingIndicator_OnShowIndicator_m1756924596 (LoadingIndicator_t3396405409 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::HideLoadingIndicator(System.Boolean)
extern "C"  void LoadingIndicator_HideLoadingIndicator_m3283773122 (LoadingIndicator_t3396405409 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::HideIndicator()
extern "C"  void LoadingIndicator_HideIndicator_m2957341849 (LoadingIndicator_t3396405409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::SendHiddenEvent()
extern "C"  void LoadingIndicator_SendHiddenEvent_m203255798 (LoadingIndicator_t3396405409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator::OnHideIndicator(System.Boolean)
extern "C"  void LoadingIndicator_OnHideIndicator_m476189807 (LoadingIndicator_t3396405409 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
