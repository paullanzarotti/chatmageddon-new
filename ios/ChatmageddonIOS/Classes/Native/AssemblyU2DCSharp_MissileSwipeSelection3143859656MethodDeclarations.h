﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileSwipeSelection
struct MissileSwipeSelection_t3143859656;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileSwipeSelection::.ctor()
extern "C"  void MissileSwipeSelection__ctor_m1253661251 (MissileSwipeSelection_t3143859656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSwipeSelection::OnSwipeLeft()
extern "C"  void MissileSwipeSelection_OnSwipeLeft_m2063283469 (MissileSwipeSelection_t3143859656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSwipeSelection::OnSwipeRight()
extern "C"  void MissileSwipeSelection_OnSwipeRight_m533114542 (MissileSwipeSelection_t3143859656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileSwipeSelection::ResetCarouselToPosition()
extern "C"  void MissileSwipeSelection_ResetCarouselToPosition_m1037710872 (MissileSwipeSelection_t3143859656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
