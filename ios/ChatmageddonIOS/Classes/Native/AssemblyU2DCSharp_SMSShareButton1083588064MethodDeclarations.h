﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SMSShareButton
struct SMSShareButton_t1083588064;

#include "codegen/il2cpp-codegen.h"

// System.Void SMSShareButton::.ctor()
extern "C"  void SMSShareButton__ctor_m4252763073 (SMSShareButton_t1083588064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SMSShareButton::OnButtonClick()
extern "C"  void SMSShareButton_OnButtonClick_m58906552 (SMSShareButton_t1083588064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
