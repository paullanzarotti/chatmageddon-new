﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VisibilityControl
struct VisibilityControl_t484768539;
// RefInt
struct RefInt_t2938871354;
// Vectrosity.VectorLine
struct VectorLine_t3390220087;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"

// System.Void VisibilityControl::.ctor()
extern "C"  void VisibilityControl__ctor_m2584097062 (VisibilityControl_t484768539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RefInt VisibilityControl::get_objectNumber()
extern "C"  RefInt_t2938871354 * VisibilityControl_get_objectNumber_m3543602260 (VisibilityControl_t484768539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControl::Setup(Vectrosity.VectorLine,System.Boolean)
extern "C"  void VisibilityControl_Setup_m3259468591 (VisibilityControl_t484768539 * __this, VectorLine_t3390220087 * ___line0, bool ___makeBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControl::OnBecameVisible()
extern "C"  void VisibilityControl_OnBecameVisible_m2491480246 (VisibilityControl_t484768539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControl::OnBecameInvisible()
extern "C"  void VisibilityControl_OnBecameInvisible_m2352307749 (VisibilityControl_t484768539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControl::OnDestroy()
extern "C"  void VisibilityControl_OnDestroy_m2114126943 (VisibilityControl_t484768539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
