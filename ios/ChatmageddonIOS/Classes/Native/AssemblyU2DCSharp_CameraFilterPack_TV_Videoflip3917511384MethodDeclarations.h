﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Videoflip
struct CameraFilterPack_TV_Videoflip_t3917511384;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Videoflip::.ctor()
extern "C"  void CameraFilterPack_TV_Videoflip__ctor_m1606484679 (CameraFilterPack_TV_Videoflip_t3917511384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Videoflip::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Videoflip_get_material_m1892323944 (CameraFilterPack_TV_Videoflip_t3917511384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::Start()
extern "C"  void CameraFilterPack_TV_Videoflip_Start_m2002872947 (CameraFilterPack_TV_Videoflip_t3917511384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Videoflip_OnRenderImage_m1882784987 (CameraFilterPack_TV_Videoflip_t3917511384 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::Update()
extern "C"  void CameraFilterPack_TV_Videoflip_Update_m869799130 (CameraFilterPack_TV_Videoflip_t3917511384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::OnDisable()
extern "C"  void CameraFilterPack_TV_Videoflip_OnDisable_m3420525108 (CameraFilterPack_TV_Videoflip_t3917511384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
