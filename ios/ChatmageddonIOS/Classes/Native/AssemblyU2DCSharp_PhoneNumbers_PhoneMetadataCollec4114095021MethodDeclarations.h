﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneMetadataCollection
struct PhoneMetadataCollection_t4114095021;
// System.Collections.Generic.IList`1<PhoneNumbers.PhoneMetadata>
struct IList_1_t907802004;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneMetadataCollection/Builder
struct Builder_t1251570781;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadataCollec4114095021.h"

// System.Void PhoneNumbers.PhoneMetadataCollection::.cctor()
extern "C"  void PhoneMetadataCollection__cctor_m75256569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadataCollection::.ctor()
extern "C"  void PhoneMetadataCollection__ctor_m2423268584 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection::get_DefaultInstance()
extern "C"  PhoneMetadataCollection_t4114095021 * PhoneMetadataCollection_get_DefaultInstance_m689358579 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection::get_DefaultInstanceForType()
extern "C"  PhoneMetadataCollection_t4114095021 * PhoneMetadataCollection_get_DefaultInstanceForType_m3193693594 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection::get_ThisMessage()
extern "C"  PhoneMetadataCollection_t4114095021 * PhoneMetadataCollection_get_ThisMessage_m530346108 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<PhoneNumbers.PhoneMetadata> PhoneNumbers.PhoneMetadataCollection::get_MetadataList()
extern "C"  Il2CppObject* PhoneMetadataCollection_get_MetadataList_m3741436247 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadataCollection::get_MetadataCount()
extern "C"  int32_t PhoneMetadataCollection_get_MetadataCount_m360685411 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadataCollection::GetMetadata(System.Int32)
extern "C"  PhoneMetadata_t366861403 * PhoneMetadataCollection_GetMetadata_m196431872 (PhoneMetadataCollection_t4114095021 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadataCollection::get_IsInitialized()
extern "C"  bool PhoneMetadataCollection_get_IsInitialized_m602914737 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadataCollection::GetHashCode()
extern "C"  int32_t PhoneMetadataCollection_GetHashCode_m2711025809 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadataCollection::Equals(System.Object)
extern "C"  bool PhoneMetadataCollection_Equals_m2813332071 (PhoneMetadataCollection_t4114095021 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection::CreateBuilder()
extern "C"  Builder_t1251570781 * PhoneMetadataCollection_CreateBuilder_m18462983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection::ToBuilder()
extern "C"  Builder_t1251570781 * PhoneMetadataCollection_ToBuilder_m3611709900 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection::CreateBuilderForType()
extern "C"  Builder_t1251570781 * PhoneMetadataCollection_CreateBuilderForType_m3138022944 (PhoneMetadataCollection_t4114095021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadataCollection/Builder PhoneNumbers.PhoneMetadataCollection::CreateBuilder(PhoneNumbers.PhoneMetadataCollection)
extern "C"  Builder_t1251570781 * PhoneMetadataCollection_CreateBuilder_m562128472 (Il2CppObject * __this /* static, unused */, PhoneMetadataCollection_t4114095021 * ___prototype0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
