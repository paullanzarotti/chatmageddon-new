﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumberButton
struct  PhoneNumberButton_t1790400683  : public SFXButton_t792651341
{
public:
	// UILabel PhoneNumberButton::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_5;
	// UnityEngine.Color PhoneNumberButton::buttonColour
	Color_t2020392075  ___buttonColour_6;

public:
	inline static int32_t get_offset_of_buttonLabel_5() { return static_cast<int32_t>(offsetof(PhoneNumberButton_t1790400683, ___buttonLabel_5)); }
	inline UILabel_t1795115428 * get_buttonLabel_5() const { return ___buttonLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_5() { return &___buttonLabel_5; }
	inline void set_buttonLabel_5(UILabel_t1795115428 * value)
	{
		___buttonLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_5, value);
	}

	inline static int32_t get_offset_of_buttonColour_6() { return static_cast<int32_t>(offsetof(PhoneNumberButton_t1790400683, ___buttonColour_6)); }
	inline Color_t2020392075  get_buttonColour_6() const { return ___buttonColour_6; }
	inline Color_t2020392075 * get_address_of_buttonColour_6() { return &___buttonColour_6; }
	inline void set_buttonColour_6(Color_t2020392075  value)
	{
		___buttonColour_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
